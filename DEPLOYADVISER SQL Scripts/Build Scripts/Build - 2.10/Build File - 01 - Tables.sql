USE DeployAdviser
GO

--Begin table person.Person
EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username
-- ==================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nIndex INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') + 'XXX' AS LastName
		FROM INSERTED I
		WHERE I.UserName IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM person.Person P
				WHERE P.UserName = I.UserName
					AND P.PersonID <> I.PersonID
				)

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cUserName = UPPER(LEFT(@cLastName, 3))
		SET @nIndex = (SELECT TOP 1 CAST(RIGHT(P.UserName, 3) AS INT) FROM person.Person P WHERE LEFT(P.UserName, 3) = LEFT(@cLastName, 3) ORDER BY 1 DESC)
		SET @nIndex = ISNULL(@nIndex, 0) + 1

		SET @cUserName += RIGHT('000' + CAST(@nIndex AS VARCHAR(10)), 3)

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person