
--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, 'Administrator') CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'ClientNotice'
		SELECT @bHasAccess = 1 FROM client.ClientNotice T WHERE (T.ClientNoticeID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, 'Administrator,ProjectManager') CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR PBP.RoleCode = 'Administrator'
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID AND T.RoleCode IN ('Administrator', 'ProjectManager') OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetPersonProjectExpenseDataByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectExpenseDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return ClientCostCode and ProjectCurrency data based on a PersonProjectID
-- ============================================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PCC.ClientCostCodeID,

		CASE
			WHEN PCC.ProjectCostCodeDescription IS NULL
			THEN CCC.ClientCostCodeDescription
			ELSE PCC.ProjectCostCodeDescription
		END AS ProjectCostCodeDescription

	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
		JOIN project.Project P ON P.ProjectID = PCC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PCC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
	ORDER BY 2, 1

	SELECT 
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM dropdown.Currency C
		JOIN project.ProjectCurrency PC ON PC.ISOCurrencyCode = C.ISOCurrencyCode
		JOIN project.Project P ON P.ProjectID = PC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
			AND PC.IsActive = 1

	UNION

	SELECT 
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM dropdown.Currency C
		JOIN person.PersonProject PP ON PP.ISOCurrencyCode = C.ISOCurrencyCode
			AND PP.PersonProjectID = @PersonProjectID

	ORDER BY 1, 2

END
GO
--End procedure person.GetPersonProjectExpenseDataByPersonProjectID

--Begin procedure person.ResolvePendingAction
EXEC utility.DropObject 'person.AcceptClientPersonByClientPersonID'
EXEC utility.DropObject 'person.RejectClientPersonByClientPersonID'
EXEC utility.DropObject 'person.ResolvePendingAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.31
-- Description:	A stored procedure to manage data in the client.ClientPerson table
-- ===============================================================================
CREATE PROCEDURE person.ResolvePendingAction

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ClientName VARCHAR(500), EmailAddress VARCHAR(320), PersonNameFormatted VARCHAR(250), RoleName VARCHAR(50))

	INSERT INTO @tTable
		(ClientName, EmailAddress, PersonNameFormatted, RoleName)
	SELECT
		C.ClientName,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM person.Person P
		JOIN client.ClientPerson CP ON CP.CreatePersonID = P.PersonID
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @EntityID

	UNION

	SELECT
		C.ClientName,
		CP.NotifyEmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @EntityID
			AND core.NullIfEmpty(CP.NotifyEmailAddress) IS NOT NULL

	IF @EntityTypeCode = 'AcceptClientPerson'
		BEGIN

		UPDATE CP
		SET AcceptedDate = getDate()
		FROM client.ClientPerson CP
		WHERE CP.ClientPersonID = @EntityID

		END
	ELSE
		BEGIN

		DELETE CP
		FROM client.ClientPerson CP
		WHERE CP.ClientPersonID = @EntityID

		END
	--ENDIF

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		T.ClientName,
		T.EmailAddress,
		T.PersonNameFormatted,
		T.RoleName
	FROM @tTable T
	WHERE core.NullIfEmpty(T.EmailAddress) IS NOT NULL
	ORDER BY T.EmailAddress

END
GO
--End procedure person.ResolvePendingAction