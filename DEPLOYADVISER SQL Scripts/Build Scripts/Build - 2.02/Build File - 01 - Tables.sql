USE DeployAdviser
GO

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'PersonProjectName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '1'
EXEC utility.AddColumn @TableName, 'IsVisible', 'BIT', '1'
GO

EXEC utility.DropObject 'person.TR_PersonProject'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER person.TR_PersonProject ON person.PersonProject AFTER INSERT
AS
BEGIN
	SET ARITHABORT ON;

	UPDATE PP
	SET PP.PersonProjectName = PTOR.ProjectTermOfReferenceName + ' / ' + CF.ClientFunctionName
	FROM person.PersonProject PP
		JOIN INSERTED I ON I.PersonProjectID = PP.PersonProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PP.PersonProjectName IS NULL

END
GO
--End table person.PersonProject

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.AddColumn @TableName, 'IsLineManagerApproved', 'BIT', '0'
GO

EXEC utility.DropObject 'person.TR_PersonProjectTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER person.TR_PersonProjectTime ON person.PersonProjectTime AFTER UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	IF UPDATE(HoursWorked) OR UPDATE(ApplyVAT) OR UPDATE(ProjectManagerNotes)
		BEGIN

		UPDATE PPT
		SET PPT.IsLineManagerApproved = 0
		FROM person.PersonProjectTime PPT
			JOIN INSERTED I ON I.PersonProjectTimeID = PPT.PersonProjectTimeID

		END
	--ENDIF

END
GO
--End table person.PersonProjectTime