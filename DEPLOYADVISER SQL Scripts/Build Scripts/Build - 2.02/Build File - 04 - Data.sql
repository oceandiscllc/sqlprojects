USE DeployAdviser
GO

--Begin table person.PersonProject
UPDATE PP
SET PP.PersonProjectName = PTOR.ProjectTermOfReferenceName + ' / ' + CF.ClientFunctionName
FROM person.PersonProject PP
	JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
	JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		AND PP.PersonProjectName IS NULL
--End table person.PersonProject

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Allow the user to regenerate an invoice .pdf and add it to the document library for an existing invoice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.RegenerateInvoice', @PERMISSIONCODE='RegenerateInvoice';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
