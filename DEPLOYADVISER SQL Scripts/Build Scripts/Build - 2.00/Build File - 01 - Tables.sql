USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'HasLineManagerReview', 'BIT', '0'
GO
--End table client.Client

--Begin table invoice.*
EXEC utility.DropObject 'invoice.InvoiceExpenseLog'
EXEC utility.DropObject 'invoice.InvoiceExpenseLogOld'
EXEC utility.DropObject 'invoice.InvoicePerDiemLog'
EXEC utility.DropObject 'invoice.InvoicePerDiemLogOld'
EXEC utility.DropObject 'invoice.InvoiceTimeLog'
EXEC utility.DropObject 'invoice.InvoiceTimeLogOld'
GO
--End table invoice.*

--Begin table person.PersonProofOfLife
DECLARE @TableName VARCHAR(250) = 'person.PersonProofOfLife'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table person.PersonProofOfLife
