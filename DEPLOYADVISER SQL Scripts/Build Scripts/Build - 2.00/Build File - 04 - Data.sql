USE DeployAdviser
GO

--Begin table client.Client
UPDATE C
SET C.HasLineManagerReview = 1
FROM client.Client C
GO
--End table client.Client

--Begin table core.EmailTemplate / core.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Invoice' AND ET.EmailTemplateCode = 'AdditionalLineManagerNotification')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
	VALUES
		('Invoice', 'AdditionalLineManagerNotification', 'Sent when an additional line manager review email is sent', 'An Additional Line Manager Review Request Email Has Been Sent', '<p>A line manager review email has been sent to the following email address: &nbsp;[[LineManagerEmailAddress]]. &nbsp;The email was sent to permit the recipient to process the invoice listed below.</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]</p><p>It is your responsibility to communicate the PIN associated with this invoice to this individual so that the invoice review process can begin.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')

	INSERT INTO core.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
	VALUES
		('Invoice', '[[LineManagerEmailAddress]]', 'Line Manager Email Address')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Invoice' AND ET.EmailTemplateCode = 'SubmitToWorkflow')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
	VALUES
		('Invoice', 'SubmitToWorkflow', 'Sent when an invoice is submitted directly to the first step of the workflow', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>Thank you,<br />The DeployAdviser Team</p>')

	END
--ENDIF
GO

UPDATE ET
SET 
	ET.EmailTemplateDescription = 'Sent when an invoice is submitted for line manager review',
	ET.EmailTemplateCode = 'SubmitToLineManager'
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode = 'Invoice'
	AND ET.EmailTemplateCode = 'Submit'
GO
--End table core.EmailTemplate / core.EmailTemplateField

--Begin table core.EntityType
UPDATE ET
SET ET.EntityTypeName = 'Deployment'
FROM core.EntityType ET
WHERE ET.EntityTypeName = 'Person Project'
GO
--End table core.EntityType

--Begin table permissionable.Permissionable
UPDATE P
SET P.MethodName = 'List'
FROM permissionable.Permissionable P
WHERE P.ControllerName = 'Invoice'
	AND P.MethodName = 'View'
	AND P.PermissionCode = 'Export'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Invoice.List.Export'
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Invoice.View.Export'
GO

UPDATE PTP
SET PTP.PermissionableLineage = 'Invoice.List.Export'
FROM permissionable.PermissionableTemplatePermissionable PTP
WHERE PTP.PermissionableLineage = 'Invoice.View.Export'
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='See the list of invoices on a project view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View.InvoiceList', @PERMISSIONCODE='InvoiceList';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

