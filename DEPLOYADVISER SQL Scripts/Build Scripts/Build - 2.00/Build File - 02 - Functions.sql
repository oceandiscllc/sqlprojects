USE DeployAdviser
GO

--Begin function person.GetPersonProjectDaysBilled
EXEC utility.DropObject 'person.GetPersonProjectDaysBilled'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.06
-- Description:	A function to return a count of the days billed on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysBilled
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,5)

AS
BEGIN

	DECLARE @nDaysBilled NUMERIC(18,2)

	SELECT @nDaysBilled = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND PPT.InvoiceID > 0

	RETURN ISNULL(@nDaysBilled, 0)
	
END
GO
--End function person.GetPersonProjectDaysBilled