-- File Name:	Build - 2.6 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.6 - 2018.05.01 20.14.07

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Functions:
--		core.FormatDate
--		core.FormatDateWithDay
--		invoice.GetInvoiceRecordsByPersonID
--
-- Procedures:
--		client.GetClientLegalEntitiesByClientID
--		invoice.GetInvoiceByInvoiceID
--		invoice.GetInvoiceExpenseLogByInvoiceID
--		invoice.GetInvoiceLinemanagerEmailDataByInvoiceID
--		invoice.GetInvoicePerDiemLogByInvoiceID
--		invoice.GetInvoicePreviewData
--		invoice.GetInvoiceSummaryDataByInvoiceID
--		invoice.GetInvoiceTimeLogByInvoiceID
--		person.AcceptClientPersonByClientPersonID
--		person.CheckAccess
--		person.GetPersonByPersonID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN
	
	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateWithDay
EXEC utility.DropObject 'core.FormatDateWithDay'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2018.04.18
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDateWithDay
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113) + ' (' + LEFT(DATENAME(dw, @DateTimeData), 3) + ')'

END
GO
--End function core.FormatDateWithDay


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure client.GetClientLegalEntitiesByClientID
EXEC utility.DropObject 'client.GetClientLegalEntitiesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.04.10
-- Description:	A stored procedure to accept a client engagement based on a ClientPersonID
-- =======================================================================================
CREATE PROCEDURE client.GetClientLegalEntitiesByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientName,
		C.ClientLegalEntities
	FROM client.Client C
	WHERE C.ClientID = @ClientID

END
GO
--End procedure client.GetClientLegalEntitiesByClientID

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.LineManagerPIN,
		I.Notes,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoiceExpenseLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectExpense table
-- ======================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLog
	SELECT
		core.FormatDateWithDay(PPE.ExpenseDate) AS ExpenseDateFormatted,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate,
		CAST(PPE.ExchangeRate * PPE.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(PPE.ExchangeRate * PPE.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID) IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' 
				+ (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID)
				+ '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName 

END
GO
--End procedure invoice.GetInvoiceExpenseLogByInvoiceID

--Begin procedure invoice.GetInvoiceLinemanagerEmailDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceLinemanagerEmailDataByInvoiceID'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.12.05
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceLinemanagerEmailDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE I
	SET I.InvalidLineManagerLoginAttempts = 0
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	--InvoiceSummaryData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.LineManagerPIN,
		I.LineManagerToken,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

	--InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowPeopleByInvoiceID @InvoiceID

END
GO
--End procedure invoice.GetInvoiceLinemanagerEmailDataByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode2 AS PerDiemCode,
		'DPA' AS PerdiemType,
		PL.DPAAmount * PPT.HasDPA AS Amount,
		PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DPAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	UNION

	SELECT 
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode3 AS PerDiemCode,
		'DSA' AS PerdiemType,
		PPT.DSAAmount AS Amount,
		PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DSAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	ORDER BY 1, 3, 2, 9

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate, ApplyVAT)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PL.DPAAmount * PPT.HasDPA AS DPAAmount,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.ApplyVAT
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, ApplyVAT, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PPT.ApplyVAT,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		NULL AS InvoiceStatus,

		CASE
			WHEN PN.BillAddress1 IS NULL 
				OR PN.BillISOCountryCode2 IS NULL 
				OR PN.BillMunicipality IS NULL 
				OR PN.ContractingTypeID = 0 
				OR 
					(
					(SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company' 
						AND PN.SendInvoicesFrom IS NULL 
						AND PN.OwnCompanyName IS NULL
					)
			THEN 1
			ELSE 0
		END AS IsAddressUpdateRequired,

		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PINT.PersonInvoiceNumberTypeCode,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.CellPhone,
		PN.ContractingTypeID,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		PN.OwnCompanyName,
		PN.SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		PN.PersonInvoiceNumberIncrement,
		PN.PersonInvoiceNumberPrefix,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID,
		PN.TaxRate
	FROM project.Project PJ
	CROSS JOIN person.Person PN
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND PJ.ProjectID = @ProjectID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = PN.PersonInvoiceNumberTypeID
			AND PN.PersonID = @PersonID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDateWithDay(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		TPL.DPAAmount AS Amount,
		TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DPAExchangeRate AS ExchangeRate,
		TPL.DPAAmount * TPL.DPAExchangeRate AS InvoiceAmount,
		TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DPAAmount * TPL.DPAExchangeRate) + (TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	UNION

	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		TPL.DSAAmount AS Amount,
		TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DSAExchangeRate AS ExchangeRate,
		TPL.DSAAmount * TPL.DSAExchangeRate AS InvoiceAmount,
		TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DSAAmount * TPL.DSAExchangeRate) + (TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	ORDER BY 1, 3, 2, 8

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DPAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DPAAmount) AS Amount,
		SUM(TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) + SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DPAISOCurrencyCode

	UNION

	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DSAAmount) AS Amount,
		SUM(TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) + SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 3

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDateWithDay(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		NULL AS LaborCode,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS InvoiceAmount,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(TEL.ExpenseAmount * TEL.ExchangeRate) AS InvoiceAmount,
			SUM(TEL.TaxAmount * TEL.ExchangeRate) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.IsDefault
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin function invoice.GetInvoiceRecordsByPersonID
EXEC utility.DropObject 'invoice.GetInvoiceRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.26
-- Description:	A function to return a table with the InvoiceID of all invoices accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION invoice.GetInvoiceRecordsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (InvoiceID INT NOT NULL, CanHaveList BIT, CanHavePDF BIT, CanHaveView BIT, CanHaveVoucher BIT)

AS
BEGIN

	WITH CP AS 
		(
		SELECT
			CP.ClientID,
			0 AS ProjectID,
			CP.ClientPersonRoleCode
		FROM client.ClientPerson CP
		WHERE CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.AcceptedDate IS NOT NULL

		UNION

		SELECT
			P.ClientID,
			P.ProjectID,
			'ProjectManager' AS ClientPersonRoleCode
		FROM project.Project P
			JOIN project.ProjectPerson PP ON PP.ProjectID = P.ProjectID
				AND PP.PersonID = @PersonID
				AND EXISTS
					(
					SELECT 1
					FROM client.ClientPerson CP
					WHERE CP.PersonID = PP.PersonID
						AND CP.ClientPersonRoleCode = 'ProjectManager'
						AND CP.AcceptedDate IS NOT NULL
					)
		)

	INSERT INTO @tTable
		(InvoiceID, CanHaveList, CanHavePDF, CanHaveView, CanHaveVoucher)
	SELECT
		I.InvoiceID,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHaveList,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHavePDF,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHaveView,

		CASE
			WHEN I.InvoiceStatus NOT IN ('Line Manager Review','Rejected') 
				AND
					(
					(I.InvoiceStatus = 'Approved' AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) 
						OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1
						OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
						OR person.IsSuperAdministrator(@PersonID) = 1 
					)
			THEN 1
			ELSE 0
		END AS CanHaveVoucher

	FROM invoice.Invoice I
		JOIN project.Project P ON P.ProjectID = I.ProjectID
			AND 
				(
				I.PersonID = @PersonID
					OR person.IsSuperAdministrator(@PersonID) = 1
					OR EXISTS
						(
						SELECT 1
						FROM CP
						WHERE CP.ClientID = P.ClientID
							AND 
								(
								CP.ClientPersonRoleCode = 'Administrator'
									OR CP.ProjectID = P.ProjectID
								)
						)
				)
	
	RETURN

END
GO
--End function invoice.GetInvoiceRecordsByPersonID

--Begin procedure invoice.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get data from the invoice.Invoice Table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDocumentGUID VARCHAR(50) = (SELECT TOP 1 D.DocumentGUID FROM document.DocumentEntity DE JOIN document.Document D ON D.DocumentID = DE.DocumentID AND DE.EntityTypeCode = 'Invoice' AND DE.EntityID = @InvoiceID ORDER BY DE.DocumentEntityID DESC)

	--InvoiceSummaryData
	SELECT

		CASE
			WHEN @cDocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + @cDocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		@PersonID AS PersonID,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		person.FormatPersonNameByPersonID(@PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.InvoiceAmount,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		I.LineManagerPIN,
		I.LineManagerToken,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		IRR.InvoiceRejectionReasonName,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		PN.PersonID AS InvoicePersonID,
		person.HasPermission('Main.Error.ViewCFErrors', PN.PersonID) AS HasViewCFErrorsPermission,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted,
		PN.UserName AS InvoicePersonUserName
	FROM invoice.Invoice I
		JOIN dropdown.InvoiceRejectionReason IRR ON IRR.InvoiceRejectionReasonID = I.InvoiceRejectionReasonID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceSummaryDataByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLog
	SELECT 
		C.FinanceCode1 AS LaborCode,
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		PP.FeeRate,
		PPT.HoursWorked,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		PP.ISOCurrencyCode,
		PPT.ExchangeRate,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
	ORDER BY PPT.DateWorked

END
GO
--End procedure invoice.GetInvoiceTimeLogByInvoiceID

--Begin procedure person.AcceptClientPersonByClientPersonID
EXEC utility.DropObject 'person.AcceptClientPersonByClientPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.04.10
-- Description:	A stored procedure to accept a client engagement based on a ClientPersonID
-- =======================================================================================
CREATE PROCEDURE person.AcceptClientPersonByClientPersonID

@ClientPersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET AcceptedDate = getDate()
	FROM client.ClientPerson CP
	WHERE CP.ClientPersonID = @ClientPersonID

END
GO
--End procedure person.AcceptClientPersonByClientPersonID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Administrator')) OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID AND T.RoleCode IN ('Administrator', 'ProjectManager')
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID


--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit the contents of the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Delete an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Next of Kin data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Proof of Life data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Merge user accounts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonMerge', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonMerge', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export person data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Layout', @DESCRIPTION='Allow access to the Feedback icon in the site header', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Layout.Default.CanHaveFeedback', @PERMISSIONCODE='CanHaveFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Allow change access to selected fields on a deployment record AFTER the deployment has been accpted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='See the list of invoices on a project view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View.InvoiceList', @PERMISSIONCODE='InvoiceList';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Export invoice data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Allow the user to regenerate an invoice .pdf and add it to the document library for an existing invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.RegenerateInvoice', @PERMISSIONCODE='RegenerateInvoice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.6 - 2018.05.01 20.14.07')
GO
--End build tracking

