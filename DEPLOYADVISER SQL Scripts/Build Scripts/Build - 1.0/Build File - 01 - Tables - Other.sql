/* Build File - 01 - Tables - Other */
USE DeployAdviser
GO

--Begin table document.Document
IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'DocumentSearchCatalog')
	CREATE FULLTEXT CATALOG DocumentSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentGUID VARCHAR(50),
	DocumentName VARCHAR(250),
	DocumentTypeID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	DocumentData VARBINARY(MAX),
	DocumentSize BIGINT,
	ThumbnailData VARBINARY(MAX),
	ThumbnailSize BIGINT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ThumbnailSize', 'BIGINT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentName), CHARINDEX('.', REVERSE(I.DocumentName)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET 
			D.DocumentSize = ISNULL(DATALENGTH(D.DocumentData), 0),
			D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
			D.ThumbNailSize = ISNULL(DATALENGTH(D.ThumbNailData), 0)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentEntity
	(
	DocumentEntityID INT IDENTITY(1,1) NOT NULL,
	DocumentID INT,
	EntityTypeCode VARCHAR(50),
	EntityTypeSubCode VARCHAR(50),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'
GO
--End table document.DocumentEntity

--Begin table document.FileType
DECLARE @TableName VARCHAR(250) = 'document.FileType'

EXEC utility.DropObject @TableName

CREATE TABLE document.FileType
	(
	FileTypeID INT IDENTITY(1,1) NOT NULL,
	Extension VARCHAR(10),
	MimeType VARCHAR(100)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FileTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_FileType', 'Extension,MimeType'
GO
--End table document.FileType

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.Permissionable
	(
	PermissionableID INT IDENTITY(1,1) NOT NULL,
	ControllerName VARCHAR(50),
	MethodName VARCHAR(250),
	PermissionCode VARCHAR(50),
	PermissionableLineage VARCHAR(355),
	PermissionableGroupID INT,
	Description VARCHAR(MAX),
	IsActive BIT,
	IsGlobal BIT,
	IsSuperAdministrator BIT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsGlobal', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableID'
GO

EXEC utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @PermissionableLineage VARCHAR(355)
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID
	WHILE @@fetch_status = 0
		BEGIN

		UPDATE P
		SET P.PermissionableLineage = 			
			P.ControllerName 
				+ '.' 
				+ P.MethodName
				+ CASE
						WHEN P.PermissionCode IS NOT NULL
						THEN '.' + P.PermissionCode
						ELSE ''
					END

		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		FETCH oCursor INTO @nPermissionableID
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableGroup
	(
	PermissionableGroupID INT IDENTITY(1,1) NOT NULL,
	PermissionableGroupCode VARCHAR(50),
	PermissionableGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableGroup', 'DisplayOrder,PermissionableGroupName,PermissionableGroupID'
GO
--Begin table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplate
	(
	PermissionableTemplateID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplatePermissionable
	(
	PermissionableTemplatePermissionableID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableTemplateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableTemplatePermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableTemplatePermissionable', 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplatePermissionable