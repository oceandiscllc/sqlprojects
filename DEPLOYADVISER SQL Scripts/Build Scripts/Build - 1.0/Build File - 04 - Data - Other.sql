﻿/* Build File - 04 - Data - Other */
USE DeployAdviser
GO

--Begin table client.Client 
TRUNCATE TABLE client.Client
GO

INSERT INTO client.Client 
	(ClientName, EmailAddress, IsActive, Phone, FAX, BillAddress1, BillAddress2, BillAddress3, BillMunicipality, BillRegion, BillPostalCode, BillISOCountryCode2, MailAddress1, MailAddress2, MailAddress3, MailMunicipality, MailRegion, MailPostalCode, MailISOCountryCode2, CanProjectAlterCostCodeDescription, HREmailAddress, InvoiceDueReminderInterval, PersonAccountEmailAddress, SendOverdueInvoiceEmails, TaxID, TaxRate, Website) 
VALUES 
	('OceanDisc LLC', 'todd.pires@oceandisc.com', 1, '555-1212', '555-1313', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1, 'todd.pires@oceandisc.com', 0, 'todd.pires@oceandisc.com', 0, '987654321', CAST(10.0000 AS Numeric(18, 4)), NULL),
	('HofGothi LLC', 'john.lyons@oceandisc.com', 1, '555-2222', '555-2323', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1, 'john.lyons@oceandisc.com', 0, 'john.lyons@oceandisc.com', 0, '123456789', CAST(10.0000 AS Numeric(18, 4)), NULL),
	('Digital Roar LLC', 'john.lyons@oceandisc.com', 1, '555-2222', '555-2323', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1, 'john.lyons@oceandisc.com', 0, 'john.lyons@oceandisc.com', 0, '123456789', CAST(10.0000 AS Numeric(18, 4)), NULL)
GO
--End table client.Client 

--Begin table client.ClientCostCode 
TRUNCATE TABLE client.ClientCostCode
GO

INSERT INTO client.ClientCostCode 
	(ClientID, ClientCostCodeName, ClientCostCodeDescription, IsActive) 
VALUES 
	(1, 'CC11', 'Cost code description for CC11', 1),
	(1, 'CC12', 'Cost code description for CC11', 1),
	(1, 'CC13', 'Cost code description for CC11', 1),
	(1, 'CC14', 'Cost code description for CC11', 1),
	(1, 'CC15', 'Cost code description for CC11', 1),
	(2, 'CC21', 'Cost code description for CC21', 1),
	(2, 'CC22', 'Cost code description for CC21', 1),
	(2, 'CC23', 'Cost code description for CC21', 1),
	(2, 'CC24', 'Cost code description for CC21', 1),
	(2, 'CC25', 'Cost code description for CC21', 1)
GO
--End table client.ClientCostCode 

--Begin table client.ClientFunction 
TRUNCATE TABLE client.ClientFunction
GO

INSERT INTO client.ClientFunction 
	(ClientID, ClientFunctionName, ClientFunctionDescription, IsActive) 
VALUES 
	(1, 'F11', 'Function 11', 1),
	(1, 'F12', 'Function 12', 1),
	(1, 'F13', 'Function 13', 1),
	(1, 'F14', 'Function 14', 1),
	(1, 'F15', 'Function 15', 1),
	(2, 'F21', 'Function 21', 1),
	(2, 'F22', 'Function 22', 1),
	(2, 'F23', 'Function 23', 1),
	(2, 'F24', 'Function 24', 1),
	(2, 'F25', 'Function 25', 1)
GO
--End table client.ClientFunction

--Begin table client.ClientPerson
TRUNCATE TABLE client.ClientPerson
GO

INSERT INTO client.ClientPerson 
	(ClientID, PersonID, ClientPersonRoleCode) 
VALUES 
	(1, 3, 'Administrator'),
	(1, 3, 'ProjectManager'),
	(1, 5, 'Administrator'),
	(1, 5, 'ProjectManager'),
	(2, 1, 'Administrator'),
	(2, 1, 'ProjectManager'),
	(2, 2, 'Administrator'),
	(2, 2, 'ProjectManager')
GO
--End table client.ClientPerson

--Begin table client.ClientTermOfReference
TRUNCATE TABLE client.ClientTermOfReference
GO

INSERT INTO client.ClientTermOfReference 
	(ClientID, ClientTermOfReferenceName, ClientTermOfReferenceDescription, IsActive) 
VALUES 
	(1, 'TOR11', 'Term Of Reference 11', 1),
	(1, 'TOR12', 'Term Of Reference 12', 1),
	(1, 'TOR13', 'Term Of Reference 13', 1),
	(1, 'TOR14', 'Term Of Reference 14', 1),
	(1, 'TOR15', 'Term Of Reference 15', 1),
	(2, 'TOR21', 'Term Of Reference 21', 1),
	(2, 'TOR22', 'Term Of Reference 22', 1),
	(2, 'TOR23', 'Term Of Reference 23', 1),
	(2, 'TOR24', 'Term Of Reference 24', 1),
	(2, 'TOR25', 'Term Of Reference 25', 1)
GO
--End table client.ClientTermOfReference

--Begin table client.ProjectInvoiceTo
TRUNCATE TABLE client.ProjectInvoiceTo
GO

INSERT INTO client.ProjectInvoiceTo 
	(ClientID, ProjectInvoiceToAddressee, ProjectInvoiceToEmailAddress, ProjectInvoiceToPhone, ProjectInvoiceToName, ProjectInvoiceToAddress1, ProjectInvoiceToAddress2, ProjectInvoiceToAddress3, ProjectInvoiceToMunicipality, ProjectInvoiceToRegion, ProjectInvoiceToPostalCode, ProjectInvoiceToISOCountryCode2, IsActive)
VALUES 
	(1, 'Todd', 'todd.pires@oceandisc.com', '727.579.1066', 'Todd''s', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1),
	(1, 'John', 'john.lyons@oceandisc.com', '555.555.5555', 'John''s', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1),
	(1, 'Kevin', 'kevin.ross@oceandisc.com', '555.555.6666', 'Kevin''s', '1201 Hummingbird Hill Road', NULL, NULL, 'Chapel Hill', 'NC', '27517', 'US', 1),
	(2, 'Todd', 'todd.pires@oceandisc.com', '727.579.1066', 'Todd''s', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1),
	(2, 'John', 'john.lyons@oceandisc.com', '555.555.5555', 'John''s', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1),
	(2, 'Kevin', 'kevin.ross@oceandisc.com', '555.555.6666', 'Kevin''s', '1201 Hummingbird Hill Road', NULL, NULL, 'Chapel Hill', 'NC', '27517', 'US', 1)
GO
--End table client.ProjectInvoiceTo

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('PersonProject', '[[ClientFunctionName]]', 'Function'),
	('PersonProject', '[[ClientName]]', 'Client Name'),
	('PersonProject', '[[ClientTermOfReferenceName]]', 'Term Of Reference'),
	('PersonProject', '[[PersonNameFormatted]]', 'Person Name'),
	('PersonProject', '[[ProjectName]]', 'Project Name')
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Client', 
	@EntityTypeName = 'Client', 
	@EntityTypeNamePlural = 'Clients',
	@SchemaName = 'client', 
	@TableName = 'Client', 
	@PrimaryKeyFieldName = 'ClientID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Document',
	@EntityTypeName = 'Document',
	@EntityTypeNamePlural = 'Documents',
	@SchemaName = 'document',
	@TableName = 'Document',
	@PrimaryKeyFieldName = 'DocumentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Invoice', 
	@EntityTypeName = 'Invoice', 
	@EntityTypeNamePlural = 'Invoices',
	@SchemaName = 'invoice', 
	@TableName = 'Invoice', 
	@PrimaryKeyFieldName = 'InvoiceID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProject', 
	@EntityTypeName = 'Person Project', 
	@EntityTypeNamePlural = 'Person Projects',
	@SchemaName = 'personproject', 
	@TableName = 'PersonProject', 
	@PrimaryKeyFieldName = 'PersonProjectID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProjectTime', 
	@EntityTypeName = 'Time Record', 
	@EntityTypeNamePlural = 'Time Records',
	@SchemaName = 'person', 
	@TableName = 'PersonProjectTime', 
	@PrimaryKeyFieldName = 'PersonProjectTimeID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProjectExpense', 
	@EntityTypeName = 'Expense Record', 
	@EntityTypeNamePlural = 'Expense Records',
	@SchemaName = 'person', 
	@TableName = 'PersonProjectExpense', 
	@PrimaryKeyFieldName = 'PersonProjectExpenseID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Project', 
	@EntityTypeName = 'Project', 
	@EntityTypeNamePlural = 'Projects',
	@SchemaName = 'project', 
	@TableName = 'Project', 
	@PrimaryKeyFieldName = 'ProjectID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'Core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-building',
	@NewMenuItemCode = 'ClientList',
	@NewMenuItemLink = '/client/list',
	@NewMenuItemText = 'Clients',
	@PermissionableLineageList = 'Client.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ClientList',
	@Icon = 'fa fa-fw fa-tasks',
	@NewMenuItemCode = 'ProjectList',
	@NewMenuItemLink = '/project/list',
	@NewMenuItemText = 'Projects',
	@PermissionableLineageList = 'Project.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ProjectList',
	@Icon = 'fa fa-fw fa-flag',
	@NewMenuItemCode = 'PersonProjectList',
	@NewMenuItemLink = '/personproject/list',
	@NewMenuItemText = 'Deployments',
	@PermissionableLineageList = 'PersonProject.List'
GO

--Begin Time & Expenses Submenu --
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectList',
	@Icon = 'fa fa-fw fa-calendar',
	@NewMenuItemCode = 'TimeExpense',
	@NewMenuItemText = 'Time &amp; Expenses'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PersonProjectTimeList',
	@NewMenuItemLink = '/personprojecttime/list',
	@NewMenuItemText = 'Time Log',
	@ParentMenuItemCode = 'TimeExpense',
	@PermissionableLineageList = 'PersonProjectTime.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectTimeList',
	@NewMenuItemCode = 'PersonProjectExpenseList',
	@NewMenuItemLink = '/personprojectExpense/list',
	@NewMenuItemText = 'Expense Log',
	@ParentMenuItemCode = 'TimeExpense',
	@PermissionableLineageList = 'PersonProjectExpense.List'
GO
--End Time & Expenses Submenu --

--Begin Admin Submenu --
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TimeExpense',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO
--End Admin Submenu --

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'TimeExpense'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationKey', NULL, 'DIUZQ1ICXUIMHKKK6TCX'
EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationSecretKey', NULL, 'MylcNoqlXVduD3LBn981ZHRY7OGOcKjsZ8LDTOTH'
EXEC core.SystemSetupAddUpdate 'DuoApiEndPoint', NULL, 'api-8a6e671f.duosecurity.com'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationKey', NULL, 'DIT96UH6EI8OVAUXJVT2'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationSecretKey', NULL, 'q7NjG8UbJzEMEDKNTH9m3FFBTSkEmAxUodib025p'
EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@deployadviser.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '30'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://da2.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'DeployAdviser'
EXEC core.SystemSetupAddUpdate 'TwoFactorEnabled', NULL, '0'
GO
--End table core.SystemSetup

--Begin table document.FileType
TRUNCATE TABLE document.FileType
GO

INSERT INTO document.FileType
	(Extension, MimeType)
VALUES
	('.doc', 'application/msword'),
	('.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('.gif', 'image/gif'),
	('.htm', 'text/html'),
	('.html', 'text/html'),
	('.jpeg', 'image/jpeg'),
	('.jpg', 'image/jpeg'),
	('.pdf', 'application/pdf'),
	('.png', 'image/png'),
	('.pps', 'application/mspowerpoint'),
	('.ppt', 'application/mspowerpoint'),
	('.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
	('.rtf', 'application/rtf'),
	('.txt', 'text/plain'),
	('.xls', 'application/excel'),
	('.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
GO
--End table document.FileType

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Add a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, EmailAddress, LastLoginDateTime, InvalidLoginAttempts, IsAccountLockedOut, IsActive, IsSuperAdministrator, Password, PasswordSalt, PasswordExpirationDateTime) 
VALUES
	('Nameer', 'Al-Hadithi', 'Mr.', 'Nameer', 'nalhadithi@skotkonung.com', NULL, 0, 0, 1, 0, '214676C4D305E151B0FB4FA87637F0C356750B288C6DA3FE7259CDCDFEE2E709', '500918F4-F98F-4DEC-80FB-1087E993A021', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Burnham', 'Mr.', 'jburnham', 'jonathan.burnham@oceandisc.com', NULL, 0, 0, 1, 1, 'EB065F0E9CFE4AA399624ADFEF14427FA39070F25A92B0DFF210D0F13C11B46F', 'AA47A377-16D5-4319-9C92-EEB2C67C05E5', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Cole', 'Mr.', 'JCole', 'jcole@skotkonung.com', NULL, 0, 0, 1, 1, '67589DA2DA1F9EFEB1B78C407E1A66B4CDE8577D259BBAA420FB17C798B6D3C5', 'D2A94546-77A0-448B-B754-E78A6C458CC9', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('John', 'Lyons', 'Mr.', 'jlyons', 'john.lyons@oceandisc.com', NULL, 0, 0, 1, 0, '138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', '71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Usamah', 'Mahmood', 'Mr.', 'usamah', 'usamah852@gmail.com', NULL, 0, 0, 1, 0, '71F3FF82541088FEAAD1651D1CB5A50B70A7C955FD20F12462D6C1E74278A9B9', 'B4EABF7F-315A-48CF-BDC8-DD84093DF57C', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Dave', 'Roberts', 'Mr.', 'daver', 'david.roberts@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'kevin.ross@oceandisc.com', NULL, 0, 0, 1, 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Ian', 'van Mourik', 'Mr.', 'IanVM', 'ian.van.mourik@gmail.com', NULL, 0, 0, 1, 0, 'DC8013389868E63E539C553780046DB1DF3A80090FC66A2972A5416643E26D23', 'CCB4FD4C-0453-4245-A43C-01F1C620939C', CAST('2020-01-01T00:00:00.000' AS DateTime))
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.Person

--Begin table person.PersonAccount
TRUNCATE TABLE person.PersonAccount
GO

INSERT INTO person.PersonAccount
	(PersonID,PersonAccountName,IntermediateAccountNumber,IntermediateBankBranch,IntermediateBankName,IntermediateBankRoutingNumber,IntermediateIBAN,IntermediateSWIFTCode,IntermediateISOCurrencyCode,TerminalAccountNumber,TerminalBankBranch,TerminalBankName,TerminalBankRoutingNumber,TerminalIBAN,TerminalSWIFTCode,TerminalISOCurrencyCode)
SELECT
	P.PersonID,
	'PersonAccountName 1',
	'IntermediateAccountNumber 1',
	'IntermediateBankBranch 1',
	'IntermediateBankName 1',
	'1234567890',
	'IntermediateIBAN 1',
	'123456789012345',
	'USD',
	'TerminalAccountNumber 1',
	'TerminalBankBranch 1',
	'TerminalBankName 1',
	'9876543210',
	'TerminalIBAN 1',
	'987654321098765',
	'GBP'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonAccount
	(PersonID,PersonAccountName,IntermediateAccountNumber,IntermediateBankBranch,IntermediateBankName,IntermediateBankRoutingNumber,IntermediateIBAN,IntermediateSWIFTCode,IntermediateISOCurrencyCode,TerminalAccountNumber,TerminalBankBranch,TerminalBankName,TerminalBankRoutingNumber,TerminalIBAN,TerminalSWIFTCode,TerminalISOCurrencyCode)
SELECT
	P.PersonID,
	'PersonAccountName 2',
	'IntermediateAccountNumber 2',
	'IntermediateBankBranch 2',
	'IntermediateBankName 2',
	'1234567890',
	'IntermediateIBAN 2',
	'123456789012345',
	'USD',
	'TerminalAccountNumber 2',
	'TerminalBankBranch 2',
	'TerminalBankName 2',
	'9876543210',
	'TerminalIBAN 2',
	'987654321098765',
	'GBP'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonAccount

--Begin table person.PersonLanguage
TRUNCATE TABLE person.PersonLanguage
GO

INSERT INTO person.PersonLanguage
	(PersonID,ISOLanguageCode2,OralLevel,ReadLevel,WriteLevel)
SELECT
	P.PersonID,
	'PT',
	1,
	2,
	3
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonLanguage
	(PersonID,ISOLanguageCode2,OralLevel,ReadLevel,WriteLevel)
SELECT
	P.PersonID,
	'QN',
	2,
	2,
	2
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonLanguage

--Begin table person.PersonNextOfKin
TRUNCATE TABLE person.PersonNextOfKin
GO

INSERT INTO person.PersonNextOfKin
	(PersonID,PersonNextOfKinName,Relationship,Phone,CellPhone,WorkPhone,Address1,Address2,Address3,Municipality,Region,PostalCode,ISOCountryCode2)
SELECT
	P.PersonID,
	'Tony Pires',
	'Father',
	'555-1111',
	'555-2222',
	'555-3333',
	'Address1',
	'Address2',
	'Address3',
	'Ponet Vedra',
	'FL',
	'32081',
	'US'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonNextOfKin
	(PersonID,PersonNextOfKinName,Relationship,Phone,CellPhone,WorkPhone,Address1,Address2,Address3,Municipality,Region,PostalCode,ISOCountryCode2)
SELECT
	P.PersonID,
	'Nancy Pires',
	'Mother',
	'555-4444',
	'555-5555',
	'555-6666',
	'Address1',
	'Address2',
	'Address3',
	'Ponet Vedra',
	'FL',
	'32081',
	'US'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonNextOfKin

--Begin table person.PersonPasswordSecurity
TRUNCATE TABLE person.PersonPasswordSecurity
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	1,
	'Scott'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 1
		)
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	6,
	'Deane'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 6
		)
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	12,
	'Norwalk'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 12
		)
GO
--End table person.PersonPasswordSecurity

--Begin table person.PersonProofOfLife
TRUNCATE TABLE person.PersonProofOfLife
GO

INSERT INTO person.PersonProofOfLife
	(PersonID,ProofOfLifeQuestion,ProofOfLifeAnswer)
SELECT
	P.PersonID,
	'Question 1',
	'Answer 1'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonProofOfLife
	(PersonID,ProofOfLifeQuestion,ProofOfLifeAnswer)
SELECT
	P.PersonID,
	'Question 2',
	'Answer 2'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonProofOfLife
