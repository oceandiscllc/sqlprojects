/* Build File - 03 - Procedures - Person */
USE DeployAdviser
GO

--Begin procedure person.AcceptPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.AcceptPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.AcceptPersonProjectByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nPersonID INT = (SELECT PP.PersonID FROM person.PersonProject PP WHERE PP.PersonProjectID = @PersonProjectID)

	UPDATE PP
	SET PP.AcceptedDate = getDate()
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	EXEC eventlog.LogPersonProjectAction @PersonProjectID, 'update', @nPersonID, 'Assignment Accepted'

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND PP.PersonProjectID = @PersonProjectID

	SELECT
		PP.ManagerEmailAddress AS EmailAddress
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID
		AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND PP2.PersonProjectID = @PersonProjectID
			AND P.EmailAddress IS NOT NULL

	ORDER BY 1

END
GO
--End procedure person.AcceptPersonProjectByPersonProjectID

--Begin procedure person.AddPersonPermissionable
EXEC utility.DropObject 'person.AddPersonPermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.AddPersonPermissionable

@PersonID INT, 
@PermissionableLineage VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	VALUES
		(@PersonID, @PermissionableLineage)

END
GO
--End procedure person.AddPersonPermissionable

--Begin procedure person.ApplyPermissionableTemplate
EXEC utility.DropObject 'person.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE person.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM core.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PTP.PermissionableLineage
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM person.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

END
GO
--End procedure person.ApplyPermissionableTemplate

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE T.ClientID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND EXISTS 
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 

					UNION 

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1
					)
				AND (@AccessCode <> 'AddUpdate' OR (T.EndDate >= getDate() AND T.AcceptedDate IS NULL))

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.IsInvoiced = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.IsInvoiced = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM project.Project T WHERE T.ProjectID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.expirePersonProjectByPersonProjectIDList
EXEC utility.DropObject 'person.expirePersonProjectByPersonProjectIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.expirePersonProjectByPersonProjectIDList

@PersonProjectIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PP
	SET PP.EndDate = getDate()
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	EXEC eventlog.LogPersonProjectAction 0, 'update', @PersonID, 'Assignment Expired', @PersonProjectIDList

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	SELECT
		P.EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND P.EmailAddress IS NOT NULL

	UNION

	SELECT
		PP.ManagerEmailAddress AS EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress,
		PP2.PersonProjectID
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP2.PersonProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND P.EmailAddress IS NOT NULL

	ORDER BY 2, 1

END
GO
--End procedure person.expirePersonProjectByPersonProjectIDList

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'person.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- =====================================================================
CREATE PROCEDURE person.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		P.EmailAddress,
		P.PersonID
	FROM person.Person P
	WHERE EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
			JOIN core.ListToTable(@PermissionableLineage, ',') LTT ON LTT.ListItem = PP.PermissionableLineage
				AND PP.PersonID = P.PersonID
		)
		AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure person.GetEmailAddressesByPermissionableLineage

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.NickName,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the person.Person table based on a Token
-- ============================================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@Token VARCHAR(36)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PersonID,
		P.TokenCreateDateTime,

		CASE
			WHEN DATEDIFF("hour", P.TokenCreateDateTime, getdate()) <= 24
			THEN 0
			ELSE 1
		END AS IsTokenExpired

	FROM person.Person P
	WHERE P.Token = @Token

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.GetPersonPermissionables
EXEC utility.DropObject 'person.GetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.PersonPermissionable table
-- ======================================================================================
CREATE PROCEDURE person.GetPersonPermissionables

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PP.PermissionableLineage
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
	ORDER BY PP.PermissionableLineage

END
GO
--End procedure person.GetPersonPermissionables

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		IsActive BIT,
		ProjectLocationID INT,
		ProjectLocationName VARCHAR(100),
		Days INT NOT NULL DEFAULT 0,
		HoursPerDay INT NOT NULL DEFAULT 8,
		Rate NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS ClientTermOfReferenceID,
			'' AS ClientTermOfReferenceName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName
		FROM project.Project P
		WHERE P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CTOR.ClientTermOfReferenceID,
			CTOR.ClientTermOfReferenceName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET 
			TPPL.Days = PPL.Days,
			TPPL.HoursPerDay = PPL.HoursPerDay,
			TPPL.Rate = PPL.Rate 
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		T.IsActive,
		T.ProjectLocationID,
		T.ProjectLocationName,
		T.Days,
		T.HoursPerDay,
		T.Rate
	FROM @tPersonProjectLocation T
	ORDER BY T.ProjectLocationName, T.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectExpenseDataByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectExpenseDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return ClientCostCode and ProjectCurrency data based on a PersonProjectID
-- ============================================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PCC.ClientCostCodeID,

		CASE
			WHEN PCC.ProjectCostCodeDescription IS NULL
			THEN CCC.ClientCostCodeDescription
			ELSE PCC.ProjectCostCodeDescription
		END AS ProjectCostCodeDescription

	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
		JOIN project.Project P ON P.ProjectID = PCC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PCC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
	ORDER BY 2, 1

	SELECT 
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM dropdown.Currency C
		JOIN project.ProjectCurrency PC ON PC.ISOCurrencyCode = C.ISOCurrencyCode
		JOIN project.Project P ON P.ProjectID = PC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
	ORDER BY 1, 2

END
GO
--End procedure person.GetPersonProjectExpenseDataByPersonProjectID

--Begin procedure person.GetPersonProjectLocationsByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectLocationsByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProjectLocation table based on a PersonID
-- ==============================================================================================================
CREATE PROCEDURE person.GetPersonProjectLocationsByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			AND PPL.PersonProjectID = @PersonProjectID
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectLocationsByPersonProjectID

--Begin procedure person.GetPersonProjectsByPersonID
EXEC utility.DropObject 'person.GetPersonProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.PersonProjectID,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectID, CTOR.ClientTermOfReferenceName, CTOR.ClientTermOfReferenceID, CF.ClientFunctionName, CF.ClientFunctionID

END
GO
--End procedure person.GetPersonProjectsByPersonID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.SavePersonPermissionables
EXEC Utility.DropObject 'person.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.SavePersonPermissionables

@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM core.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PermissionableLineage = P.PermissionableLineage
				AND PP.PersonID = @PersonID
			)

END
GO
--End procedure person.SavePersonPermissionables

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.SetPersonPermissionables
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionables table based on a PersonID
-- =========================================================================================================
CREATE PROCEDURE person.SetPersonPermissionables

@PersonID INT,
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	IF @PermissionableIDList IS NOT NULL AND LEN(RTRIM(@PermissionableIDList)) > 0
		BEGIN

		INSERT INTO person.PersonPermissionable
			(PersonID, PermissionableLineage)
		SELECT
			@PersonID,
			P.PermissionableLineage
		FROM permissionable.Permissionable P
			JOIN core.ListToTable(@PermissionableIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PermissionableID

		END
	--ENDIF

END
GO
--End procedure person.SetPersonPermissionables

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		UserName VARCHAR(250),
		CountryCallingCodeID INT,
		CellPhone VARCHAR(64),
		IsPhoneVerified BIT,
		ClientAdministratorCount INT,
		ClientProjectManagerCount INT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR (@bIsTwoFactorEnabled = 1 AND (P.CellPhone IS NULL OR LEN(LTRIM(P.CellPhone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cCellPhone = P.CellPhone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,UserName,CountryCallingCodeID,CellPhone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cUserName,
			@nCountryCallingCodeID,
			@cCellPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure person.ValidateUserName
EXEC Utility.DropObject 'person.ValidateUserName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.19
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateUserName

@UserName VARCHAR(250),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.UserName = @UserName
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateUserName