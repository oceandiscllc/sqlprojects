/* Build File - 01 - Tables - Client & Project */
USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropObject @TableName

CREATE TABLE client.Client
	(
	--Begin standard fields
	ClientID INT IDENTITY(1, 1) NOT NULL,
	ClientName VARCHAR(500),
	EmailAddress VARCHAR(320),
	IsActive BIT,
	Phone VARCHAR(15),
	FAX VARCHAR(15),

	--Begin Address fields
	BillAddress1 VARCHAR(50),
	BillAddress2 VARCHAR(50),
	BillAddress3 VARCHAR(50),
	BillMunicipality VARCHAR(50),
	BillRegion VARCHAR(50),
	BillPostalCode VARCHAR(10),
	BillISOCountryCode2 CHAR(2),
	MailAddress1 VARCHAR(50),
	MailAddress2 VARCHAR(50),
	MailAddress3 VARCHAR(50),
	MailMunicipality VARCHAR(50),
	MailRegion VARCHAR(50),
	MailPostalCode VARCHAR(10),
	MailISOCountryCode2 CHAR(2),

	--Begin Consultancy / Project fields
	CanProjectAlterCostCodeDescription BIT,
	HREmailAddress VARCHAR(320),
	InvoiceDueReminderInterval INT,
	PersonAccountEmailAddress VARCHAR(320),
	SendOverdueInvoiceEmails BIT,
	TaxID VARCHAR(50),
	TaxRate NUMERIC(18, 4),
	Website VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanProjectAlterCostCodeDescription', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDueReminderInterval', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'SendOverdueInvoiceEmails', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18, 4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientID'
GO
--End table client.Client

--Begin table client.ClientCostCode
DECLARE @TableName VARCHAR(250) = 'client.ClientCostCode'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientCostCode
	(
	ClientCostCodeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientCostCodeName VARCHAR(50),
	ClientCostCodeDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientCostCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientCostCode', 'ClientID,ClientCostCodeName'
GO
--End table client.ClientCostCode

--Begin table client.ClientFunction
DECLARE @TableName VARCHAR(250) = 'client.ClientFunction'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientFunction
	(
	ClientFunctionID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientFunctionName VARCHAR(50),
	ClientFunctionDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientFunctionID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientFunction', 'ClientID,ClientFunctionName'
GO
--End table client.ClientFunction

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPerson
	(
	ClientPersonID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	PersonID INT,
	ClientPersonRoleCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPerson', 'ClientID,PersonID'
GO
--End table client.ClientPerson

--Begin table client.ClientTermOfReference
DECLARE @TableName VARCHAR(250) = 'client.ClientTermOfReference'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientTermOfReference
	(
	ClientTermOfReferenceID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientTermOfReferenceName VARCHAR(50),
	ClientTermOfReferenceDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientTermOfReferenceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientTermOfReference', 'ClientID,ClientTermOfReferenceName'
GO
--End table client.ClientTermOfReference

--Begin table client.ProjectInvoiceTo
DECLARE @TableName VARCHAR(250) = 'client.ProjectInvoiceTo'

EXEC utility.DropObject @TableName

CREATE TABLE client.ProjectInvoiceTo
	(
	ProjectInvoiceToID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ProjectInvoiceToEmailAddress VARCHAR(320),
	ProjectInvoiceToPhone VARCHAR(15),
	ProjectInvoiceToName VARCHAR(50),
	ProjectInvoiceToAddress1 VARCHAR(50),
	ProjectInvoiceToAddress2 VARCHAR(50),
	ProjectInvoiceToAddress3 VARCHAR(50),
	ProjectInvoiceToAddressee VARCHAR(500),
	ProjectInvoiceToMunicipality VARCHAR(50),
	ProjectInvoiceToRegion VARCHAR(50),
	ProjectInvoiceToPostalCode VARCHAR(10),
	ProjectInvoiceToISOCountryCode2 CHAR(2),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectInvoiceToID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectInvoiceTo', 'ClientID,ProjectInvoiceToName'
GO
--End table client.ProjectInvoiceTo

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.DropObject @TableName

CREATE TABLE project.Project
	(
	ProjectID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	ProjectName VARCHAR(500),
	ProjectCode VARCHAR(50),
	CustomerName VARCHAR(500),
	ISOCountryCode2 CHAR(2),
	StartDate DATE,
	EndDate DATE,
	IsNextOfKinInformationRequired BIT,
	InvoiceDueDayOfMonth INT,
	ProjectInvoiceToID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNextOfKinInformationRequired', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDueDayOfMonth', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectInvoiceToID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_Project', 'ClientID,ProjectID'
GO
--End table project.Project

--Begin table project.ProjectCostCode
DECLARE @TableName VARCHAR(250) = 'project.ProjectCostCode'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCostCode
	(
	ProjectCostCodeID INT IDENTITY(1,1) NOT NULL,
	ClientCostCodeID INT,
	ProjectID INT,
	ProjectCostCodeDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientCostCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCostCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectCostCode', 'ProjectID,ClientCostCodeID'
GO
--End table project.ProjectCostCode

--Begin table project.ProjectCurrency
DECLARE @TableName VARCHAR(250) = 'project.ProjectCurrency'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCurrency
	(
	ProjectCurrencyID INT IDENTITY(1, 1) NOT NULL,
	ProjectID INT,
	ISOCurrencyCode CHAR(3),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCurrencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectCurrency', 'ProjectID,ISOCurrencyCode'
GO
--End table project.ProjectCurrency

--Begin table project.ProjectLocation
DECLARE @TableName VARCHAR(250) = 'project.ProjectLocation'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectLocation
	(
	ProjectLocationID INT IDENTITY(1, 1) NOT NULL,
	ProjectID INT,
	ProjectLocationName VARCHAR(100),
	ISOCountryCode2 CHAR(2),
	DPAISOCurrencyCode CHAR(3),
	DPAAmount NUMERIC(18, 4),
	DSAISOCurrencyCode CHAR(3),
	DSACeiling NUMERIC(18, 4),
	IsActive BIT,
	CanWorkDay1 BIT,
	CanWorkDay2 BIT,
	CanWorkDay3 BIT,
	CanWorkDay4 BIT,
	CanWorkDay5 BIT,
	CanWorkDay6 BIT,
	CanWorkDay7 BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DPAAmount', 'NUMERIC(18, 4)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSACeiling', 'NUMERIC(18, 4)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectLocation', 'ProjectID,ProjectLocationName'
GO
--End table project.ProjectLocation

--Begin table project.ProjectPerson
DECLARE @TableName VARCHAR(250) = 'project.ProjectPerson'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectPerson
	(
	ProjectPersonID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	PersonID INT,
	ProjectPersonRoleCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectPerson', 'ProjectID,PersonID'
GO
--End table project.ProjectPerson
