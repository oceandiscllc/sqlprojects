/* Build File - 03 - Procedures - Dropdown */
USE DeployAdviser
GO

--Begin table dropdown.GetContractingTypeData
EXEC Utility.DropObject 'dropdown.GetContractingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.ContractingType table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetContractingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractingTypeID,
		T.ContractingTypeCode,
		T.ContractingTypeName
	FROM dropdown.ContractingType T
	WHERE (T.ContractingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContractingTypeName, T.ContractingTypeID

END
GO
--End procedure dropdown.GetContractingTypeData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin table dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin table dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.CountryCallingCode table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryCallingCodeID,
		C.CountryName + ' (' + CAST(T.CountryCallingCode AS VARCHAR(5)) + ')' AS CountryCallingCodeName
	FROM dropdown.CountryCallingCode T
		JOIN dropdown.Country C ON C.ISOCountryCode2 = T.ISOCountryCode2
			AND (T.CountryCallingCodeID > 0 OR @IncludeZero = 1)
			AND C.IsActive = 1
	ORDER BY C.DisplayOrder, C.CountryName, T.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin table dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.16
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
	WHERE (T.CurrencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin table dropdown.GetInsuranceTypeData
EXEC Utility.DropObject 'dropdown.GetInsuranceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.InsuranceType table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetInsuranceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InsuranceTypeID,
		T.InsuranceTypeCode,
		T.InsuranceTypeName
	FROM dropdown.InsuranceType T
	WHERE (T.InsuranceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InsuranceTypeName, T.InsuranceTypeID

END
GO
--End procedure dropdown.GetInsuranceTypeData

--Begin procedure dropdown.GetISOCountryCodeByCountryCallingCodeID
EXEC Utility.DropObject 'dropdown.GetISOCountryCodeByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to return data from the dropdown.CountryCallingCode table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetISOCountryCodeByCountryCallingCodeID

@CountryCallingCodeID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CCC.ISOCountryCode2
	FROM dropdown.CountryCallingCode CCC
	WHERE CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetISOCountryCodeByCountryCallingCodeID

--Begin procedure dropdown.GetLanguageData
EXEC Utility.DropObject 'dropdown.GetLanguageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.16
-- Description:	A stored procedure to return data from the dropdown.Language table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetLanguageData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LanguageID,
		T.ISOLanguageCode2,
		T.LanguageName
	FROM dropdown.Language T
	WHERE (T.LanguageID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LanguageName, T.LanguageID

END
GO
--End procedure dropdown.GetLanguageData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin table dropdown.GetPasswordSecurityQuestionData
EXEC Utility.DropObject 'dropdown.GetPasswordSecurityQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the dropdown.PasswordSecurityQuestion table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetPasswordSecurityQuestionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PasswordSecurityQuestionID,
		T.GroupID,
		T.PasswordSecurityQuestionName
	FROM dropdown.PasswordSecurityQuestion T
	WHERE (T.PasswordSecurityQuestionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.GroupID, T.DisplayOrder, T.PasswordSecurityQuestionName, T.PasswordSecurityQuestionID

END
GO
--End procedure dropdown.GetPasswordSecurityQuestionData

--Begin procedure dropdown.GetPaymentMethodData
EXEC Utility.DropObject 'dropdown.GetPaymentMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the dropdown.PaymentMethod table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetPaymentMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PaymentMethodID,
		T.PaymentMethodCode,
		T.PaymentMethodName
	FROM dropdown.PaymentMethod T
	WHERE (T.PaymentMethodID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PaymentMethodName, T.PaymentMethodID

END
GO
--End procedure dropdown.GetPaymentMethodData

--Begin procedure dropdown.GetProjectRoleData
EXEC Utility.DropObject 'dropdown.GetProjectRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the dropdown.ProjectRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProjectRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectRoleID,
		T.ProjectRoleCode,
		T.ProjectRoleName
	FROM dropdown.ProjectRole T
	WHERE (T.ProjectRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectRoleName, T.ProjectRoleID

END
GO
--End procedure dropdown.GetProjectRoleData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData