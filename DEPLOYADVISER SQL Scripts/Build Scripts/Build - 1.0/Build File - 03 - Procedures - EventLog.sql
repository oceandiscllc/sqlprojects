/* Build File - 03 - Procedures - EventLog */
USE DeployAdviser
GO

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.05
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cClientCostCodes VARCHAR(MAX) = ''
		
		SELECT @cClientCostCodes = COALESCE(@cClientCostCodes, '') + D.ClientCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientCostCode'), ELEMENTS) AS ClientCostCode
			FROM client.ClientCostCode T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientFunctions VARCHAR(MAX) = ''
		
		SELECT @cClientFunctions = COALESCE(@cClientFunctions, '') + D.ClientFunction
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientFunction'), ELEMENTS) AS ClientFunction
			FROM client.ClientFunction T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientTermOfReference VARCHAR(MAX) = ''
		
		SELECT @cClientTermOfReference = COALESCE(@cClientTermOfReference, '') + D.ClientTermOfReference
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientTermOfReference'), ELEMENTS) AS ClientTermOfReference
			FROM client.ClientTermOfReference T 
			WHERE T.ClientID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cClientCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Functions>' + ISNULL(@cClientFunctions, '') + '</Functions>' AS XML),
			CAST('<TermOfReference>' + ISNULL(@cClientTermOfReference, '') + '</TermOfReference>' AS XML)
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PermissionableTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPermissionableTemplatePermissionables VARCHAR(MAX) = ''
		
		SELECT @cPermissionableTemplatePermissionables = COALESCE(@cPermissionableTemplatePermissionables, '') + D.PermissionableTemplatePermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PermissionableTemplatePermissionable
			FROM permissionable.PermissionableTemplatePermissionable T 
			WHERE T.PermissionableTemplateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PermissionableTemplatePermissionables>' + ISNULL(@cPermissionableTemplatePermissionables, '') + '</PermissionableTemplatePermissionables>' AS XML)
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T
		WHERE T.PermissionableTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonAccounts VARCHAR(MAX) = ''
		
		SELECT @cPersonAccounts = COALESCE(@cPersonAccounts, '') + D.PersonAccount
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonAccount'), ELEMENTS) AS PersonAccount
			FROM person.PersonAccount T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonLanguages VARCHAR(MAX) = ''
		
		SELECT @cPersonLanguages = COALESCE(@cPersonLanguages, '') + D.PersonLanguage
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonLanguage'), ELEMENTS) AS PersonLanguage
			FROM person.PersonLanguage T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonNextOfKin VARCHAR(MAX) = ''
		
		SELECT @cPersonNextOfKin = COALESCE(@cPersonNextOfKin, '') + D.PersonNextOfKin
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonNextOfKin'), ELEMENTS) AS PersonNextOfKin
			FROM person.PersonNextOfKin T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPasswordSecurity VARCHAR(MAX) = ''
		
		SELECT @cPersonPasswordSecurity = COALESCE(@cPersonPasswordSecurity, '') + D.PersonPasswordSecurity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPasswordSecurity'), ELEMENTS) AS PersonPasswordSecurity
			FROM person.PersonPasswordSecurity T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonProofOfLife VARCHAR(MAX) = ''
		
		SELECT @cPersonProofOfLife = COALESCE(@cPersonProofOfLife, '') + D.PersonProofOfLife
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProofOfLife'), ELEMENTS) AS PersonProofOfLife
			FROM person.PersonProofOfLife T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<Accounts>' + ISNULL(@cPersonAccounts, '') + '</Accounts>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Languages>' + ISNULL(@cPersonLanguages, '') + '</Languages>' AS XML),
			CAST('<NextOfKin>' + ISNULL(@cPersonNextOfKin, '') + '</NextOfKin>' AS XML),
			CAST('<PasswordSecurity>' + ISNULL(@cPersonPasswordSecurity, '') + '</PasswordSecurity>' AS XML),
			CAST('<Permissionables>' + ISNULL(@cPersonPermissionables, '') + '</Permissionables>' AS XML),
			CAST('<ProofOfLife>' + ISNULL(@cPersonProofOfLife, '') + '</ProofOfLife>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogPersonProjectAction
EXEC utility.DropObject 'eventlog.LogPersonProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectAction

@EntityID INT = 0,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProject'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cPersonProjectLocations = COALESCE(@cPersonProjectLocations, '') + D.PersonProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProjectLocation'), ELEMENTS) AS PersonProjectLocation
			FROM person.PersonProjectLocation T 
			WHERE T.PersonProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			T.PersonProjectID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Locations>' + ISNULL(@cPersonProjectLocations, '') + '</Locations>' AS XML)
			FOR XML RAW('PersonProject'), ELEMENTS
			)
		FROM person.PersonProject T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonProjectID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			T.PersonProjectID,
			@Comments
		FROM person.PersonProject T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonProjectID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectAction

--Begin procedure eventlog.LogPersonProjectExpenseAction
EXEC utility.DropObject 'eventlog.LogPersonProjectExpenseAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectExpenseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProjectExpense'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('PersonProjectExpense'), ELEMENTS
			)
		FROM person.PersonProjectExpense T
		WHERE T.PersonProjectExpenseID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectExpenseAction

--Begin procedure eventlog.LogPersonProjectTimeAction
EXEC utility.DropObject 'eventlog.LogPersonProjectTimeAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectTimeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProjectTime'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('PersonProjectTime'), ELEMENTS
			)
		FROM person.PersonProjectTime T
		WHERE T.PersonProjectTimeID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectTimeAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cProjectCostCodes VARCHAR(MAX) = ''
		
		SELECT @cProjectCostCodes = COALESCE(@cProjectCostCodes, '') + D.ProjectCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCostCode'), ELEMENTS) AS ProjectCostCode
			FROM project.ProjectCostCode T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectCurrencies VARCHAR(MAX) = ''
		
		SELECT @cProjectCurrencies = COALESCE(@cProjectCurrencies, '') + D.ProjectCurrency
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCurrency'), ELEMENTS) AS ProjectCurrency
			FROM project.ProjectCurrency T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cProjectLocations = COALESCE(@cProjectLocations, '') + D.ProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectLocation'), ELEMENTS) AS ProjectLocation
			FROM project.ProjectLocation T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cProjectCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Currencies>' + ISNULL(@cProjectCurrencies, '') + '</Currencies>' AS XML),
			CAST('<Locations>' + ISNULL(@cProjectLocations, '') + '</Locations>' AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction