/* Build File - 03 - Procedures - Client & Project */
USE DeployAdviser
GO

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	--ClientTermOfReference
	SELECT
		newID() AS ClientTermOfReferenceGUID,
		CTOR.ClientTermOfReferenceID,
		CTOR.ClientTermOfReferenceName,
		CTOR.ClientTermOfReferenceDescription,
		CTOR.IsActive
	FROM client.ClientTermOfReference CTOR
	WHERE CTOR.ClientID = @ClientID
	ORDER BY CTOR.ClientTermOfReferenceName, CTOR.ClientTermOfReferenceID

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetClientsByPersonID
EXEC utility.DropObject 'client.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the client.Client table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientsByPersonID

@PersonID INT,
@ClientPersonRoleCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.CLientName
	FROM client.Client C
		JOIN client.ClientPerson CP ON CP.ClientID = C.ClientID
			AND CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = @ClientPersonRoleCode
			AND C.IsActive = 1

	UNION

	SELECT
		C.ClientID,
		C.CLientName
	FROM client.Client C, person.Person P
	WHERE P.PersonID = @PersonID
		AND P.IsSuperAdministrator = 1
		AND C.IsActive = 1

	ORDER BY 2, 1

END
GO
--End procedure client.GetClientsByPersonID

--Begin procedure client.GetProjectInvoiceToByProjectInvoiceToID
EXEC utility.DropObject 'client.GetProjectInvoiceToByProjectInvoiceToID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A stored procedure to return data from the client.ProjectInvoiceTo table based on a ProjectInvoiceToID
-- ===================================================================================================================
CREATE PROCEDURE client.GetProjectInvoiceToByProjectInvoiceToID

@ProjectInvoiceToID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ProjectInvoiceToID = @ProjectInvoiceToID

END
GO
--End procedure client.GetProjectInvoiceToByProjectInvoiceToID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.EndDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		C.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C1.CountryName,
		C2.CurrencyName AS DPAISOCurrencyName,
		C3.CurrencyName AS DSAISOCurrencyName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C1 ON C1.ISOCountryCode2 = PL.ISOCountryCode2
		JOIN dropdown.Currency C2 ON C2.ISOCurrencyCode = PL.DPAISOCurrencyCode
		JOIN dropdown.Currency C3 ON C3.ISOCurrencyCode = PL.DSAISOCurrencyCode
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.GetProjects
EXEC utility.DropObject 'project.GetProjects'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the project.Project table
-- =============================================================================
CREATE PROCEDURE project.GetProjects

@PersonID INT,
@IsActive INT = -1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.ClientName,
		P.ProjectID,
		P.ProjectName
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND (@IsActive = -1 OR P.IsActive = @IsActive)
			AND EXISTS
				(
				SELECT 1
				FROM client.ClientPerson CP
				WHERE CP.ClientID = P.ClientID
					AND CP.PersonID = @PersonID

				UNION

				SELECT 1
				FROM person.Person P
				WHERE P.PersonID = @PersonID
					AND P.IsSuperAdministrator = 1
				)
	ORDER BY C.ClientName, P.ProjectName

END
GO
--End procedure project.GetProjects

--Begin procedure project.ValidateDateWorked
EXEC utility.DropObject 'project.ValidateDateWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.19
-- Description:	A stored procedure to validate a DateWorked from a ProjectLocationID
-- =================================================================================
CREATE PROCEDURE project.ValidateDateWorked

@dDateWorked DATE,
@ProjectLocationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nDayOfWeek INT = DATEPART(dw, @dDateWorked)

	;
	WITH DD AS
		(
		SELECT 

			CASE 
				WHEN @nDayOfWeek = 1 AND PL.CanWorkDay1 = 1
				THEN 1
				WHEN @nDayOfWeek = 2 AND PL.CanWorkDay2 = 1
				THEN 1
				WHEN @nDayOfWeek = 3 AND PL.CanWorkDay3 = 1
				THEN 1
				WHEN @nDayOfWeek = 4 AND PL.CanWorkDay4 = 1
				THEN 1
				WHEN @nDayOfWeek = 5 AND PL.CanWorkDay5 = 1
				THEN 1
				WHEN @nDayOfWeek = 6 AND PL.CanWorkDay6 = 1
				THEN 1
				WHEN @nDayOfWeek = 7 AND PL.CanWorkDay7 = 1
				THEN 1
				ELSE 0
			END AS IsValidDay,

			P.IsActive,
			P.StartDate,
			P.EndDate
		FROM project.Project P
			JOIN project.ProjectLocation PL ON PL.ProjectID = P.ProjectID
				AND PL.ProjectLocationID = @ProjectLocationID
		)

	SELECT
		CASE
			WHEN DD.IsValidDay = 1 AND DD.IsActive = 1 AND @dDateWorked >= DD.StartDate AND DD.EndDate >= @dDateWorked
			THEN 1
			ELSE 0
		END AS IsValid
	FROM DD

END
GO
--End procedure project.ValidateDateWorked

--Begin procedure project.ValidateExpenseDate
EXEC utility.DropObject 'project.ValidateExpenseDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.19
-- Description:	A stored procedure to validate a ExpenseDate from a PersonProjectID
-- ================================================================================
CREATE PROCEDURE project.ValidateExpenseDate

@dExpenseDate DATE,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	;
	WITH DD AS
		(
		SELECT 
			P.IsActive,
			P.StartDate,
			P.EndDate
		FROM project.Project P
			JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
				AND PP.PersonProjectID = @PersonProjectID
		)

	SELECT
		CASE
			WHEN DD.IsActive = 1 AND @dExpenseDate >= DD.StartDate AND DD.EndDate >= @dExpenseDate
			THEN 1
			ELSE 0
		END AS IsValid
	FROM DD

END
GO
--End procedure project.ValidateExpenseDate
