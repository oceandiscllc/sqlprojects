-- File Name:	Build - 1.0 - DEPLOYADVISER.sql
-- Build Key:	Build - 1.0 - 2017.11.01 22.36.40

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Schemas:
--		client
--		core
--		document
--		dropdown
--		eventlog
--		permissionable
--		person
--		project
--		syslog
--
-- Tables:
--		client.Client
--		client.ClientCostCode
--		client.ClientFunction
--		client.ClientPerson
--		client.ClientTermOfReference
--		client.ProjectInvoiceTo
--		core.Announcement
--		core.EmailTemplate
--		core.EmailTemplateField
--		core.EntityType
--		core.EntityTypeStatus
--		core.MenuItem
--		core.MenuItemPermissionableLineage
--		core.SystemSetup
--		document.Document
--		document.DocumentEntity
--		document.FileType
--		dropdown.ContractingType
--		dropdown.Country
--		dropdown.CountryCallingCode
--		dropdown.Currency
--		dropdown.InsuranceType
--		dropdown.Language
--		dropdown.PasswordSecurityQuestion
--		dropdown.PaymentMethod
--		dropdown.ProjectRole
--		dropdown.Role
--		eventlog.EventLog
--		permissionable.Permissionable
--		permissionable.PermissionableGroup
--		permissionable.PermissionableTemplate
--		permissionable.PermissionableTemplatePermissionable
--		person.Person
--		person.PersonAccount
--		person.PersonLanguage
--		person.PersonNextOfKin
--		person.PersonPasswordSecurity
--		person.PersonPermissionable
--		person.PersonProject
--		person.PersonProjectExpense
--		person.PersonProjectLocation
--		person.PersonProjectTime
--		person.PersonProofOfLife
--		project.Project
--		project.ProjectCostCode
--		project.ProjectCurrency
--		project.ProjectLocation
--		project.ProjectPerson
--		syslog.ApplicationErrorLog
--		syslog.BuildLog
--		syslog.DuoWebTwoFactorLog
--
-- Functions:
--		core.FormatDate
--		core.FormatDateTime
--		core.FormatTime
--		core.GetDescendantMenuItemsByMenuItemCode
--		core.GetEntityTypeNameByEntityTypeCode
--		core.GetEntityTypeNamePluralByEntityTypeCode
--		core.GetSystemSetupValueBySystemSetupKey
--		core.ListToTable
--		document.FormatFileSize
--		dropdown.GetCountryNameByISOCountryCode
--		eventlog.GetDocumentEntityXML
--		eventlog.GetEventNameByEventCode
--		person.FormatPersonNameByPersonID
--		person.GetPersonProjectDays
--		person.GetPersonProjectStatus
--		person.HashPassword
--		person.HasPermission
--		person.ValidatePassword
--
-- Procedures:
--		client.GetClientByClientID
--		client.GetClientsByPersonID
--		client.GetProjectInvoiceToByProjectInvoiceToID
--		core.DeleteAnnouncementByAnnouncementID
--		core.EntityTypeAddUpdate
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		core.GetAnnouncementsByDate
--		core.GetEmailTemplateByEmailTemplateID
--		core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
--		core.GetEmailTemplateFieldsByEntityTypeCode
--		core.GetMenuItemsByPersonID
--		core.GetSystemSetupDataBySystemSetupID
--		core.GetSystemSetupValuesBySystemSetupKey
--		core.GetSystemSetupValuesBySystemSetupKeyList
--		core.MenuItemAddUpdate
--		core.SystemSetupAddUpdate
--		core.UpdateParentPermissionableLineageByMenuItemCode
--		document.GetDocumentByDocumentData
--		document.GetDocumentByDocumentGUID
--		document.GetEntityDocuments
--		document.PurgeEntityDocuments
--		document.SaveEntityDocuments
--		dropdown.GetContractingTypeData
--		dropdown.GetControllerData
--		dropdown.GetCountryCallingCodeData
--		dropdown.GetCountryData
--		dropdown.GetCurrencyData
--		dropdown.GetEntityTypeNameData
--		dropdown.GetEventCodeNameData
--		dropdown.GetInsuranceTypeData
--		dropdown.GetISOCountryCodeByCountryCallingCodeID
--		dropdown.GetLanguageData
--		dropdown.GetMethodData
--		dropdown.GetPasswordSecurityQuestionData
--		dropdown.GetPaymentMethodData
--		dropdown.GetProjectRoleData
--		dropdown.GetRoleData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogAnnouncementAction
--		eventlog.LogClientAction
--		eventlog.LogEmailTemplateAction
--		eventlog.LogLoginAction
--		eventlog.LogPermissionableTemplateAction
--		eventlog.LogPersonAction
--		eventlog.LogPersonProjectAction
--		eventlog.LogPersonProjectExpenseAction
--		eventlog.LogPersonProjectTimeAction
--		eventlog.LogProjectAction
--		permissionable.DeletePermissionable
--		permissionable.DeletePermissionableTemplate
--		permissionable.GetPermissionableByPermissionableID
--		permissionable.GetPermissionableGroups
--		permissionable.GetPermissionables
--		permissionable.GetPermissionableTemplateByPermissionableTemplateID
--		permissionable.GetPermissionsByPermissionableTemplateID
--		permissionable.SavePermissionable
--		permissionable.SavePermissionableGroup
--		permissionable.UpdateParentPermissionableLineageByMenuItemCode
--		permissionable.UpdateSuperAdministratorPersonPermissionables
--		person.AcceptPersonProjectByPersonProjectID
--		person.AddPersonPermissionable
--		person.ApplyPermissionableTemplate
--		person.CheckAccess
--		person.expirePersonProjectByPersonProjectIDList
--		person.GeneratePassword
--		person.GetEmailAddressesByPermissionableLineage
--		person.GetPersonByPersonID
--		person.GetPersonByPersonToken
--		person.GetPersonPermissionables
--		person.GetPersonProjectByPersonProjectID
--		person.GetPersonProjectExpenseDataByPersonProjectID
--		person.GetPersonProjectLocationsByPersonProjectID
--		person.GetPersonProjectsByPersonID
--		person.GetPersonProjectTimeByPersonProjectTimeID
--		person.SavePersonPermissionables
--		person.SetAccountLockedOut
--		person.SetPersonPermissionables
--		person.ValidateEmailAddress
--		person.ValidateLogin
--		person.ValidateUserName
--		project.GetProjectByProjectID
--		project.GetProjects
--		project.ValidateDateWorked
--		project.ValidateExpenseDate
--		utility.AddColumn
--		utility.AddSchema
--		utility.DropColumn
--		utility.DropConstraintsAndIndexes
--		utility.DropFullTextIndex
--		utility.DropIndex
--		utility.DropObject
--		utility.InsertIdentityValue
--		utility.SetDefaultConstraint
--		utility.SetIndexClustered
--		utility.SetIndexNonClustered
--		utility.SetPrimaryKeyClustered
--		utility.SetPrimaryKeyNonClustered
--		utility.UpdateSuperAdministratorPersonPermissionables
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
/* Build File - 00 - Prerequisites */
--USE DeployAdviser
GO

--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin dependencies

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject
--End dependencies

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN [' + @ColumnName + ']'
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropConstraintsAndIndexes
EXEC Utility.DropObject 'utility.DropConstraintsAndIndexes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropConstraintsAndIndexes

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + DC.Name + ']' AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + FK.Name + ']' AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor
END
GO
--End  procedure utility.DropConstraintsAndIndexes

--Begin procedure utility.DropFullTextIndex
EXEC utility.DropObject 'utility.DropFullTextIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropFullTextIndex

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT * FROM sys.fulltext_indexes FTI WHERE FTI.object_id = OBJECT_ID(@TableName))
		BEGIN

		SET @cSQL = 'ALTER FULLTEXT INDEX ON ' + @TableName + ' DISABLE'
		EXEC (@cSQL)

		SET @cSQL = 'DROP FULLTEXT INDEX ON ' + @TableName
		EXEC (@cSQL)

		END
	--ENDIF
	
END
GO
--End procedure utility.DropFullTextIndex

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex

@TableName VARCHAR(250),
@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.09.03
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX),
@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE utility.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM person.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1

END	
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables

EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'client'
EXEC utility.AddSchema 'document'
EXEC utility.AddSchema 'dropdown'
EXEC utility.AddSchema 'eventlog'
EXEC utility.AddSchema 'permissionable'
EXEC utility.AddSchema 'person'
EXEC utility.AddSchema 'project'
EXEC utility.AddSchema 'syslog'

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables - Client & Project.sql
/* Build File - 01 - Tables - Client & Project */
--USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropObject @TableName

CREATE TABLE client.Client
	(
	--Begin standard fields
	ClientID INT IDENTITY(1, 1) NOT NULL,
	ClientName VARCHAR(500),
	EmailAddress VARCHAR(320),
	IsActive BIT,
	Phone VARCHAR(15),
	FAX VARCHAR(15),

	--Begin Address fields
	BillAddress1 VARCHAR(50),
	BillAddress2 VARCHAR(50),
	BillAddress3 VARCHAR(50),
	BillMunicipality VARCHAR(50),
	BillRegion VARCHAR(50),
	BillPostalCode VARCHAR(10),
	BillISOCountryCode2 CHAR(2),
	MailAddress1 VARCHAR(50),
	MailAddress2 VARCHAR(50),
	MailAddress3 VARCHAR(50),
	MailMunicipality VARCHAR(50),
	MailRegion VARCHAR(50),
	MailPostalCode VARCHAR(10),
	MailISOCountryCode2 CHAR(2),

	--Begin Consultancy / Project fields
	CanProjectAlterCostCodeDescription BIT,
	HREmailAddress VARCHAR(320),
	InvoiceDueReminderInterval INT,
	PersonAccountEmailAddress VARCHAR(320),
	SendOverdueInvoiceEmails BIT,
	TaxID VARCHAR(50),
	TaxRate NUMERIC(18, 4),
	Website VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanProjectAlterCostCodeDescription', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDueReminderInterval', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'SendOverdueInvoiceEmails', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18, 4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientID'
GO
--End table client.Client

--Begin table client.ClientCostCode
DECLARE @TableName VARCHAR(250) = 'client.ClientCostCode'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientCostCode
	(
	ClientCostCodeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientCostCodeName VARCHAR(50),
	ClientCostCodeDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientCostCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientCostCode', 'ClientID,ClientCostCodeName'
GO
--End table client.ClientCostCode

--Begin table client.ClientFunction
DECLARE @TableName VARCHAR(250) = 'client.ClientFunction'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientFunction
	(
	ClientFunctionID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientFunctionName VARCHAR(50),
	ClientFunctionDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientFunctionID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientFunction', 'ClientID,ClientFunctionName'
GO
--End table client.ClientFunction

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPerson
	(
	ClientPersonID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	PersonID INT,
	ClientPersonRoleCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPerson', 'ClientID,PersonID'
GO
--End table client.ClientPerson

--Begin table client.ClientTermOfReference
DECLARE @TableName VARCHAR(250) = 'client.ClientTermOfReference'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientTermOfReference
	(
	ClientTermOfReferenceID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientTermOfReferenceName VARCHAR(50),
	ClientTermOfReferenceDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientTermOfReferenceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientTermOfReference', 'ClientID,ClientTermOfReferenceName'
GO
--End table client.ClientTermOfReference

--Begin table client.ProjectInvoiceTo
DECLARE @TableName VARCHAR(250) = 'client.ProjectInvoiceTo'

EXEC utility.DropObject @TableName

CREATE TABLE client.ProjectInvoiceTo
	(
	ProjectInvoiceToID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ProjectInvoiceToEmailAddress VARCHAR(320),
	ProjectInvoiceToPhone VARCHAR(15),
	ProjectInvoiceToName VARCHAR(50),
	ProjectInvoiceToAddress1 VARCHAR(50),
	ProjectInvoiceToAddress2 VARCHAR(50),
	ProjectInvoiceToAddress3 VARCHAR(50),
	ProjectInvoiceToAddressee VARCHAR(500),
	ProjectInvoiceToMunicipality VARCHAR(50),
	ProjectInvoiceToRegion VARCHAR(50),
	ProjectInvoiceToPostalCode VARCHAR(10),
	ProjectInvoiceToISOCountryCode2 CHAR(2),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectInvoiceToID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectInvoiceTo', 'ClientID,ProjectInvoiceToName'
GO
--End table client.ProjectInvoiceTo

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.DropObject @TableName

CREATE TABLE project.Project
	(
	ProjectID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	ProjectName VARCHAR(500),
	ProjectCode VARCHAR(50),
	CustomerName VARCHAR(500),
	ISOCountryCode2 CHAR(2),
	StartDate DATE,
	EndDate DATE,
	IsNextOfKinInformationRequired BIT,
	InvoiceDueDayOfMonth INT,
	ProjectInvoiceToID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNextOfKinInformationRequired', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDueDayOfMonth', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectInvoiceToID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_Project', 'ClientID,ProjectID'
GO
--End table project.Project

--Begin table project.ProjectCostCode
DECLARE @TableName VARCHAR(250) = 'project.ProjectCostCode'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCostCode
	(
	ProjectCostCodeID INT IDENTITY(1,1) NOT NULL,
	ClientCostCodeID INT,
	ProjectID INT,
	ProjectCostCodeDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientCostCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCostCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectCostCode', 'ProjectID,ClientCostCodeID'
GO
--End table project.ProjectCostCode

--Begin table project.ProjectCurrency
DECLARE @TableName VARCHAR(250) = 'project.ProjectCurrency'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCurrency
	(
	ProjectCurrencyID INT IDENTITY(1, 1) NOT NULL,
	ProjectID INT,
	ISOCurrencyCode CHAR(3),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCurrencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectCurrency', 'ProjectID,ISOCurrencyCode'
GO
--End table project.ProjectCurrency

--Begin table project.ProjectLocation
DECLARE @TableName VARCHAR(250) = 'project.ProjectLocation'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectLocation
	(
	ProjectLocationID INT IDENTITY(1, 1) NOT NULL,
	ProjectID INT,
	ProjectLocationName VARCHAR(100),
	ISOCountryCode2 CHAR(2),
	DPAISOCurrencyCode CHAR(3),
	DPAAmount NUMERIC(18, 4),
	DSAISOCurrencyCode CHAR(3),
	DSACeiling NUMERIC(18, 4),
	IsActive BIT,
	CanWorkDay1 BIT,
	CanWorkDay2 BIT,
	CanWorkDay3 BIT,
	CanWorkDay4 BIT,
	CanWorkDay5 BIT,
	CanWorkDay6 BIT,
	CanWorkDay7 BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CanWorkDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DPAAmount', 'NUMERIC(18, 4)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSACeiling', 'NUMERIC(18, 4)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectLocation', 'ProjectID,ProjectLocationName'
GO
--End table project.ProjectLocation

--Begin table project.ProjectPerson
DECLARE @TableName VARCHAR(250) = 'project.ProjectPerson'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectPerson
	(
	ProjectPersonID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	PersonID INT,
	ProjectPersonRoleCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectPerson', 'ProjectID,PersonID'
GO
--End table project.ProjectPerson

--End file Build File - 01 - Tables - Client & Project.sql

--Begin file Build File - 01 - Tables - Core.sql
/* Build File - 01 - Tables - Core */
--USE DeployAdviser
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	EmailTemplateDescription VARCHAR(500),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode'
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplateField'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplateField
	(
	EmailTemplateFieldID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PlaceHolderText VARCHAR(50),
	PlaceHolderDescription VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateFieldID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode,DisplayOrder'
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.EntityTypeStatus
DECLARE @TableName VARCHAR(250) = 'core.EntityTypeStatus'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityTypeStatus
	(
	EntityTypeStatusID INT IDENTITY(1,1) NOT NULL,
	EntityTypeID INT,
	StatusID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EntityTypeStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_EntityTypeStatus', 'EntityTypeID,DisplayOrder,StatusID'
GO
--End table core.EntityTypeStatus

--Begin table core.MenuItem
DECLARE @TableName VARCHAR(250) = 'core.MenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItem
	(
	MenuItemID INT IDENTITY(1,1) NOT NULL,
	ParentMenuItemID INT,
	MenuItemCode VARCHAR(50),
	MenuItemText VARCHAR(250),
	MenuItemLink VARCHAR(500),
	DisplayOrder INT,
	Icon VARCHAR(50),
	IsForNewTab BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsForNewTab', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentMenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MenuItemID'
GO
--End table core.MenuItem

--Begin table core.MenuItemPermissionableLineage
DECLARE @TableName VARCHAR(250) = 'core.MenuItemPermissionableLineage'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemPermissionableLineage
	(
	MenuItemPermissionableLineageID INT IDENTITY(1,1) NOT NULL,
	MenuItemID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemPermissionableLineageID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemPermissionable', 'MenuItemID'
GO
--End table core.MenuItemPermissionableLineage

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table syslog.ApplicationErrorLog
DECLARE @TableName VARCHAR(250) = 'syslog.ApplicationErrorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.ApplicationErrorLog
	(
	ApplicationErrorLogID INT IDENTITY(1,1) NOT NULL,
	ErrorDateTime	DATETIME,
	Error	VARCHAR(MAX),
	ErrorRCData	VARCHAR(MAX),
	ErrorSession VARCHAR(MAX),
	ErrorCGIData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ErrorDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationErrorLogID'
GO
--End table syslog.ApplicationErrorLog

--Begin table syslog.BuildLog
DECLARE @TableName VARCHAR(250) = 'syslog.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100) NULL,
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table syslog.BuildLog

--Begin table syslog.DuoWebTwoFactorLog
DECLARE @TableName VARCHAR(250) = 'syslog.DuoWebTwoFactorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.DuoWebTwoFactorLog
	(
	DuoWebTwoFactorLogID INT IDENTITY(1,1) NOT NULL,
	HttpBaseURL NVARCHAR(MAX),
	DuoWebRequest NVARCHAR(MAX),
	DuoWebAuth NVARCHAR(MAX),
	DuoWebSig NVARCHAR(MAX),
	DuoMessage NVARCHAR(MAX),
	DuoWebRequestHttpDateTime	NVARCHAR(MAX),
	DuoWebResponse NVARCHAR(MAX),
	RequestDateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'RequestDateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DuoWebTwoFactorLogID DESC'
GO
--End table syslog.DuoWebTwoFactorLog

--End file Build File - 01 - Tables - Core.sql

--Begin file Build File - 01 - Tables - Dropdown.sql
/* Build File - 01 - Tables - Dropdown */
--USE DeployAdviser
GO

--Begin table dropdown.ContractingType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContractingType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContractingType
	(
	ContractingTypeID INT IDENTITY(0,1) NOT NULL,
	ContractingTypeCode VARCHAR(50),
	ContractingTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContractingTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContractingType', 'DisplayOrder,ContractingTypeName'
GO
--End table dropdown.ContractingType

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	ISOCurrencyCode CHAR(3),
	CountryName NVARCHAR(250),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_Country', 'DisplayOrder,CountryName'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
DECLARE @TableName VARCHAR(250) = 'dropdown.CountryCallingCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CountryCallingCode
	(
	CountryCallingCodeID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	CountryCallingCode INT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCode', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryCallingCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_CountryCallingCode', 'ISOCountryCode2,CountryCallingCode'
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
DECLARE @TableName VARCHAR(250) = 'dropdown.Currency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Currency
	(
	CurrencyID INT IDENTITY(0,1) NOT NULL,
	ISOCurrencyCode CHAR(3),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CurrencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_Currency', 'DisplayOrder,CurrencyName'
GO
--End table dropdown.Currency

--Begin table dropdown.InsuranceType
DECLARE @TableName VARCHAR(250) = 'dropdown.InsuranceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InsuranceType
	(
	InsuranceTypeID INT IDENTITY(0,1) NOT NULL,
	InsuranceTypeCode VARCHAR(50),
	InsuranceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InsuranceTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_InsuranceType', 'DisplayOrder,InsuranceTypeName'
GO
--End table dropdown.InsuranceType

--Begin table dropdown.Language
DECLARE @TableName VARCHAR(250) = 'dropdown.Language'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Language
	(
	LanguageID INT IDENTITY(0,1) NOT NULL,
	ISOLanguageCode2 CHAR(2),
	LanguageName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LanguageID'
EXEC utility.SetIndexClustered @TableName, 'IX_Language', 'DisplayOrder,LanguageName'
GO
--End table dropdown.Language

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PasswordSecurityQuestion
	(
	PasswordSecurityQuestionID INT IDENTITY(0, 1) NOT NULL,
	GroupID INT,
	PasswordSecurityQuestionName VARCHAR(500),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GroupID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PasswordSecurityQuestionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PasswordSecurityQuestion', 'GroupID,DisplayOrder,PasswordSecurityQuestionName'
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.PaymentMethod
DECLARE @TableName VARCHAR(250) = 'dropdown.PaymentMethod'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PaymentMethod
	(
	PaymentMethodID INT IDENTITY(0,1) NOT NULL,
	PaymentMethodName VARCHAR(50),
	PaymentMethodCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PaymentMethodID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PaymentMethod', 'DisplayOrder,PaymentMethodName', 'PaymentMethodID'
GO
--End table dropdown.PaymentMethod

--Begin table dropdown.ProjectRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectRole
	(
	ProjectRoleID INT IDENTITY(0,1) NOT NULL,
	ProjectRoleName VARCHAR(50),
	ProjectRoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProjectRole', 'DisplayOrder,ProjectRoleName', 'ProjectRoleID'
GO
--End table dropdown.ProjectRole

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	RoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role
--End file Build File - 01 - Tables - Dropdown.sql

--Begin file Build File - 01 - Tables - Other.sql
/* Build File - 01 - Tables - Other */
--USE DeployAdviser
GO

--Begin table document.Document
IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'DocumentSearchCatalog')
	CREATE FULLTEXT CATALOG DocumentSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentGUID VARCHAR(50),
	DocumentName VARCHAR(250),
	DocumentTypeID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	DocumentData VARBINARY(MAX),
	DocumentSize BIGINT,
	ThumbnailData VARBINARY(MAX),
	ThumbnailSize BIGINT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ThumbnailSize', 'BIGINT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentName), CHARINDEX('.', REVERSE(I.DocumentName)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET 
			D.DocumentSize = ISNULL(DATALENGTH(D.DocumentData), 0),
			D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
			D.ThumbNailSize = ISNULL(DATALENGTH(D.ThumbNailData), 0)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentEntity
	(
	DocumentEntityID INT IDENTITY(1,1) NOT NULL,
	DocumentID INT,
	EntityTypeCode VARCHAR(50),
	EntityTypeSubCode VARCHAR(50),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'
GO
--End table document.DocumentEntity

--Begin table document.FileType
DECLARE @TableName VARCHAR(250) = 'document.FileType'

EXEC utility.DropObject @TableName

CREATE TABLE document.FileType
	(
	FileTypeID INT IDENTITY(1,1) NOT NULL,
	Extension VARCHAR(10),
	MimeType VARCHAR(100)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FileTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_FileType', 'Extension,MimeType'
GO
--End table document.FileType

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.Permissionable
	(
	PermissionableID INT IDENTITY(1,1) NOT NULL,
	ControllerName VARCHAR(50),
	MethodName VARCHAR(250),
	PermissionCode VARCHAR(50),
	PermissionableLineage VARCHAR(355),
	PermissionableGroupID INT,
	Description VARCHAR(MAX),
	IsActive BIT,
	IsGlobal BIT,
	IsSuperAdministrator BIT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsGlobal', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableID'
GO

EXEC utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @PermissionableLineage VARCHAR(355)
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID
	WHILE @@fetch_status = 0
		BEGIN

		UPDATE P
		SET P.PermissionableLineage = 			
			P.ControllerName 
				+ '.' 
				+ P.MethodName
				+ CASE
						WHEN P.PermissionCode IS NOT NULL
						THEN '.' + P.PermissionCode
						ELSE ''
					END

		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		FETCH oCursor INTO @nPermissionableID
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableGroup
	(
	PermissionableGroupID INT IDENTITY(1,1) NOT NULL,
	PermissionableGroupCode VARCHAR(50),
	PermissionableGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableGroup', 'DisplayOrder,PermissionableGroupName,PermissionableGroupID'
GO
--Begin table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplate
	(
	PermissionableTemplateID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplatePermissionable
	(
	PermissionableTemplatePermissionableID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableTemplateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableTemplatePermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableTemplatePermissionable', 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplatePermissionable
--End file Build File - 01 - Tables - Other.sql

--Begin file Build File - 01 - Tables - Person.sql
/* Build File - 01 - Tables - Person */
--USE DeployAdviser
GO

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	--Begin standard fields
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	NickName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	CountryCallingCodeID INT,
	CellPhone VARCHAR(15),
	IsPhoneVerified BIT,
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,

	--Begin Address fields
	BillAddress1 VARCHAR(50),
	BillAddress2 VARCHAR(50),
	BillAddress3 VARCHAR(50),
	BillMunicipality VARCHAR(50),
	BillRegion VARCHAR(50),
	BillPostalCode VARCHAR(10),
	BillISOCountryCode2 CHAR(2),
	MailAddress1 VARCHAR(50),
	MailAddress2 VARCHAR(50),
	MailAddress3 VARCHAR(50),
	MailMunicipality VARCHAR(50),
	MailRegion VARCHAR(50),
	MailPostalCode VARCHAR(10),
	MailISOCountryCode2 CHAR(2),

	--Begin Miscellaneous fields
	BirthDate DATE,
	ChestSize NUMERIC(18,2),
	CollarSize NUMERIC(18,2),
	GenderCode CHAR(1),
	HeadSize NUMERIC(18,2),
	Height NUMERIC(18,2),
	IsUKEUNational BIT,

	--Begin Mobile & Consultancy fields
	ContractingTypeID INT,
	IsRegisteredForUKTax BIT,
	MobilePIN VARCHAR(50),
	OwnCompanyName VARCHAR(50),
	RegistrationCode BIGINT,
	SendInvoicesFrom VARCHAR(50),
	TaxID VARCHAR(50),
	TaxRate NUMERIC(18,4)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ChestSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CollarSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContractingTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HeadSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Height', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsPhoneVerified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsRegisteredForUKTax', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUKEUNational', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RegistrationCode', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18,4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonAccount
	(
	PersonAccountID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonAccountName VARCHAR(50),
	IntermediateAccountNumber VARCHAR(50),
	IntermediateBankBranch VARCHAR(50),
	IntermediateBankName VARCHAR(50),
	IntermediateBankRoutingNumber VARCHAR(10),
	IntermediateIBAN VARCHAR(50),
	IntermediateSWIFTCode VARCHAR(15),
	IntermediateISOCurrencyCode CHAR(3),
	TerminalAccountNumber VARCHAR(50),
	TerminalBankBranch VARCHAR(50),
	TerminalBankName VARCHAR(50),
	TerminalBankRoutingNumber VARCHAR(10),
	TerminalIBAN VARCHAR(50),
	TerminalSWIFTCode VARCHAR(15),
	TerminalISOCurrencyCode CHAR(3)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonAccountID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonAccount', 'PersonID,PersonAccountName'
GO
--End table person.PersonAccount

--Begin table person.PersonLanguage
DECLARE @TableName VARCHAR(250) = 'person.PersonLanguage'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonLanguage
	(
	PersonLanguageID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ISOLanguageCode2 CHAR(2),
	OralLevel INT,
	ReadLevel INT,
	WriteLevel INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'OralLevel', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReadLevel', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WriteLevel', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonLanguageID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonLanguage', 'PersonID,ISOLanguageCode2'
GO
--End table person.PersonLanguage

--Begin table person.PersonNextOfKin
DECLARE @TableName VARCHAR(250) = 'person.PersonNextOfKin'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonNextOfKin
	(
	PersonNextOfKinID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonNextOfKinName VARCHAR(100),
	Relationship VARCHAR(25),
	Phone VARCHAR(25),
	CellPhone VARCHAR(25),
	WorkPhone VARCHAR(25),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonNextOfKinID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonNextOfKin', 'PersonID'
GO
--End table person.PersonNextOfKin

--Begin table person.PersonPasswordSecurity
DECLARE @TableName VARCHAR(250) = 'person.PersonPasswordSecurity'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonPasswordSecurity
	(
	PersonPasswordSecurityID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PasswordSecurityQuestionID INT,
	PasswordSecurityQuestionAnswer VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PasswordSecurityQuestionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPasswordSecurityID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPasswordSecurity', 'PersonID,PasswordSecurityQuestionID'
GO
--End table person.PersonPasswordSecurity

--Begin table person.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'person.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonPermissionable
	(
	PersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableLineage VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPermissionable', 'PersonID,PermissionableLineage'
GO
--End table person.PersonPermissionable

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProject
	(
	PersonProjectID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT,
	ProjectRoleID INT,
	ManagerName VARCHAR(100),
	ManagerEmailAddress VARCHAR(320),
	ClientTermOfReferenceID INT,
	ClientFunctionID INT,
	InsuranceTypeID INT,
	ISOCurrencyCode CHAR(3),
	StartDate DATE,
	EndDate DATE,
	AcceptedDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientFunctionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientTermOfReferenceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InsuranceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProject', 'PersonID,ProjectID,ClientTermOfReferenceID,ClientFunctionID'
GO
--End table person.PersonProject

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectExpense
	(
	PersonProjectExpenseID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ExpenseDate DATE,
	ClientCostCodeID BIT,
	ISOCurrencyCode CHAR(3),
	ExpenseAmount NUMERIC(18,2),
	TaxAmount NUMERIC(18,2),
	PaymentMethodID INT,
	IsProjectExpense BIT,
	OwnNotes VARCHAR(250),
	ProjectManagerNotes VARCHAR(250),
	IsInvoiced BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientCostCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInvoiced', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsProjectExpense', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMethodID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxAmount', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectExpenseID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectExpense', 'PersonProjectID'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectLocation
	(
	PersonProjectLocationID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ProjectLocationID INT,
	Days INT,
	HoursPerDay INT,
	Rate NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'Days', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursPerDay', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Rate', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectLocation', 'PersonProjectID,ProjectLocationID'
GO
--End table person.PersonProjectLocation

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectTime
	(
	PersonProjectTimeID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ProjectLocationID INT,
	HasDPA BIT,
	DSAAmount NUMERIC(18,2),
	DateWorked DATE,
	HoursWorked INT,
	ApplyVAT BIT,
	OwnNotes VARCHAR(250),
	ProjectManagerNotes VARCHAR(250),
	IsInvoiced BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplyVAT', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasDPA', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInvoiced', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLocationID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectTimeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectTime', 'PersonProjectID,ProjectLocationID'
GO
--End table person.PersonProjectTime

--Begin table person.PersonProofOfLife
DECLARE @TableName VARCHAR(250) = 'person.PersonProofOfLife'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProofOfLife
	(
	PersonProofOfLifeID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProofOfLifeQuestion VARCHAR(500),
	ProofOfLifeAnswer VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProofOfLifeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProofOfLife', 'PersonID'
GO
--End table person.PersonProofOfLife
--End file Build File - 01 - Tables - Person.sql

--Begin file Build File - 02 - Functions.sql
/* Build File - 02 - Functions */
--USE DeployAdviser
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function document.FormatFileSize
EXEC utility.DropObject 'document.FormatFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatFileSize
(
@FileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @FileSize < 1000
		THEN CAST(@FileSize as VARCHAR(50)) + ' b'
		WHEN @FileSize < 1000000
		THEN CAST(ROUND((@FileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @FileSize < 1000000000
		THEN CAST(ROUND((@FileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @FileSize < 1000000000000
		THEN CAST(ROUND((@FileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatFileSize

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function eventlog.GetDocumentEntityXML
EXEC utility.DropObject 'eventlog.GetDocumentEntityXML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return document data from an EntitytypeCode and an EntityID
-- ======================================================================================
CREATE FUNCTION eventlog.GetDocumentEntityXML
(
@cEntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cEntityDocuments VARCHAR(MAX) = ''
	
	SELECT @cEntityDocuments = COALESCE(@cEntityDocuments, '') + D.EntityDocument
	FROM
		(
		SELECT
			(SELECT DE.DocumentID FOR XML RAW(''), ELEMENTS) AS EntityDocument
		FROM document.DocumentEntity DE
		WHERE DE.EntityID = @EntityID
			AND DE.EntityTypeCode = @cEntityTypeCode
		) D

	RETURN '<Documents>' + ISNULL(@cEntityDocuments, '') + '</Documents>'

END
GO
--End function eventlog.GetDocumentEntityXML

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode IN ('add', 'create')
			THEN 'Add'
			WHEN @EventCode = 'addupdate'
			THEN 'Update'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode IN ('read', 'view')
			THEN 'View'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE @EventCodeName
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	DECLARE @cSuffix VARCHAR(50)
	DECLARE @cTitle VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cSuffix = ISNULL(P.Suffix, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format LIKE '%FirstLast%'
		BEGIN
			
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @Format LIKE '%Suffix'
			SET @cRetVal += @cSuffix
		--ENDIF
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cRetVal + @cTitle
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.GetPersonProjectDays
EXEC utility.DropObject 'person.GetPersonProjectDays'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to get a data from a person.PersonProject record
-- ========================================================================

CREATE FUNCTION person.GetPersonProjectDays
(
@PersonProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nPersonProjectDays INT

	SELECT @nPersonProjectDays = SUM(PPL.Days)
	FROM person.PersonProjectLocation PPL
	WHERE PPL.PersonProjectID = @PersonProjectID

	RETURN ISNULL(@nPersonProjectDays, 0)

END
GO
--End function person.GetPersonProjectDays

--Begin function person.GetPersonProjectStatus
EXEC utility.DropObject 'person.GetPersonProjectStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to get a data from a person.PersonProject record
-- ========================================================================

CREATE FUNCTION person.GetPersonProjectStatus
(
@PersonProjectID INT,
@Mode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cPersonProjectStatus VARCHAR(250)
	DECLARE @cPersonProjectStatusIcon VARCHAR(200)
	DECLARE @cPersonProjectStatusName VARCHAR(50)

	SELECT 
		@cPersonProjectStatusIcon = 
			CASE
				WHEN PP.EndDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Assignment Expired"></a>'
				WHEN PP.AcceptedDate IS NULL
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-warning m-r-10" title="Assignment Pending"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-success m-r-10" title="Assignment Active"></a>'
			END,

		@cPersonProjectStatusName = 
			CASE
				WHEN PP.EndDate < getDate()
				THEN 'Expired'
				WHEN PP.AcceptedDate IS NULL
				THEN 'Pending'
				ELSE 'Active'
			END
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	IF @Mode = 'Icon'
		SET @cPersonProjectStatus = @cPersonProjectStatusIcon
	ELSE IF @Mode = 'Name'
		SET @cPersonProjectStatus = @cPersonProjectStatusName
	ELSE
		SET @cPersonProjectStatus = @cPersonProjectStatusIcon + ' ' + @cPersonProjectStatusName
	--ENDIF

	RETURN @cPersonProjectStatus

END
GO
--End function person.GetPersonProjectStatus

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = P.Password,
		@cPasswordSalt = P.PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures - Client & Project.sql
/* Build File - 03 - Procedures - Client & Project */
--USE DeployAdviser
GO

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	--ClientTermOfReference
	SELECT
		newID() AS ClientTermOfReferenceGUID,
		CTOR.ClientTermOfReferenceID,
		CTOR.ClientTermOfReferenceName,
		CTOR.ClientTermOfReferenceDescription,
		CTOR.IsActive
	FROM client.ClientTermOfReference CTOR
	WHERE CTOR.ClientID = @ClientID
	ORDER BY CTOR.ClientTermOfReferenceName, CTOR.ClientTermOfReferenceID

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetClientsByPersonID
EXEC utility.DropObject 'client.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the client.Client table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientsByPersonID

@PersonID INT,
@ClientPersonRoleCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.CLientName
	FROM client.Client C
		JOIN client.ClientPerson CP ON CP.ClientID = C.ClientID
			AND CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = @ClientPersonRoleCode
			AND C.IsActive = 1

	UNION

	SELECT
		C.ClientID,
		C.CLientName
	FROM client.Client C, person.Person P
	WHERE P.PersonID = @PersonID
		AND P.IsSuperAdministrator = 1
		AND C.IsActive = 1

	ORDER BY 2, 1

END
GO
--End procedure client.GetClientsByPersonID

--Begin procedure client.GetProjectInvoiceToByProjectInvoiceToID
EXEC utility.DropObject 'client.GetProjectInvoiceToByProjectInvoiceToID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A stored procedure to return data from the client.ProjectInvoiceTo table based on a ProjectInvoiceToID
-- ===================================================================================================================
CREATE PROCEDURE client.GetProjectInvoiceToByProjectInvoiceToID

@ProjectInvoiceToID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ProjectInvoiceToID = @ProjectInvoiceToID

END
GO
--End procedure client.GetProjectInvoiceToByProjectInvoiceToID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.EndDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		C.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C1.CountryName,
		C2.CurrencyName AS DPAISOCurrencyName,
		C3.CurrencyName AS DSAISOCurrencyName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C1 ON C1.ISOCountryCode2 = PL.ISOCountryCode2
		JOIN dropdown.Currency C2 ON C2.ISOCurrencyCode = PL.DPAISOCurrencyCode
		JOIN dropdown.Currency C3 ON C3.ISOCurrencyCode = PL.DSAISOCurrencyCode
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.GetProjects
EXEC utility.DropObject 'project.GetProjects'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the project.Project table
-- =============================================================================
CREATE PROCEDURE project.GetProjects

@PersonID INT,
@IsActive INT = -1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.ClientName,
		P.ProjectID,
		P.ProjectName
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND (@IsActive = -1 OR P.IsActive = @IsActive)
			AND EXISTS
				(
				SELECT 1
				FROM client.ClientPerson CP
				WHERE CP.ClientID = P.ClientID
					AND CP.PersonID = @PersonID

				UNION

				SELECT 1
				FROM person.Person P
				WHERE P.PersonID = @PersonID
					AND P.IsSuperAdministrator = 1
				)
	ORDER BY C.ClientName, P.ProjectName

END
GO
--End procedure project.GetProjects

--Begin procedure project.ValidateDateWorked
EXEC utility.DropObject 'project.ValidateDateWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.19
-- Description:	A stored procedure to validate a DateWorked from a ProjectLocationID
-- =================================================================================
CREATE PROCEDURE project.ValidateDateWorked

@dDateWorked DATE,
@ProjectLocationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nDayOfWeek INT = DATEPART(dw, @dDateWorked)

	;
	WITH DD AS
		(
		SELECT 

			CASE 
				WHEN @nDayOfWeek = 1 AND PL.CanWorkDay1 = 1
				THEN 1
				WHEN @nDayOfWeek = 2 AND PL.CanWorkDay2 = 1
				THEN 1
				WHEN @nDayOfWeek = 3 AND PL.CanWorkDay3 = 1
				THEN 1
				WHEN @nDayOfWeek = 4 AND PL.CanWorkDay4 = 1
				THEN 1
				WHEN @nDayOfWeek = 5 AND PL.CanWorkDay5 = 1
				THEN 1
				WHEN @nDayOfWeek = 6 AND PL.CanWorkDay6 = 1
				THEN 1
				WHEN @nDayOfWeek = 7 AND PL.CanWorkDay7 = 1
				THEN 1
				ELSE 0
			END AS IsValidDay,

			P.IsActive,
			P.StartDate,
			P.EndDate
		FROM project.Project P
			JOIN project.ProjectLocation PL ON PL.ProjectID = P.ProjectID
				AND PL.ProjectLocationID = @ProjectLocationID
		)

	SELECT
		CASE
			WHEN DD.IsValidDay = 1 AND DD.IsActive = 1 AND @dDateWorked >= DD.StartDate AND DD.EndDate >= @dDateWorked
			THEN 1
			ELSE 0
		END AS IsValid
	FROM DD

END
GO
--End procedure project.ValidateDateWorked

--Begin procedure project.ValidateExpenseDate
EXEC utility.DropObject 'project.ValidateExpenseDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.19
-- Description:	A stored procedure to validate a ExpenseDate from a PersonProjectID
-- ================================================================================
CREATE PROCEDURE project.ValidateExpenseDate

@dExpenseDate DATE,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	;
	WITH DD AS
		(
		SELECT 
			P.IsActive,
			P.StartDate,
			P.EndDate
		FROM project.Project P
			JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
				AND PP.PersonProjectID = @PersonProjectID
		)

	SELECT
		CASE
			WHEN DD.IsActive = 1 AND @dExpenseDate >= DD.StartDate AND DD.EndDate >= @dExpenseDate
			THEN 1
			ELSE 0
		END AS IsValid
	FROM DD

END
GO
--End procedure project.ValidateExpenseDate

--End file Build File - 03 - Procedures - Client & Project.sql

--Begin file Build File - 03 - Procedures - Core.sql
/* Build File - 03 - Procedures - Core */
--USE DeployAdviser
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- ==================================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- ==================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- =============================================================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		MI.IsForNewTab,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetServerSetupDataByServerSetupID'
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetServerSetupValuesByServerSetupKey'
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE MI
			SET 
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab
			FROM core.MenuItem MI
			WHERE MI.MenuItemID = @nNewMenuItemID

			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin core.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE core.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
		JOIN core.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO core.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM core.MenuItemPermissionableLineage MIPL1
		JOIN core.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode
--End file Build File - 03 - Procedures - Core.sql

--Begin file Build File - 03 - Procedures - Dropdown.sql
/* Build File - 03 - Procedures - Dropdown */
--USE DeployAdviser
GO

--Begin table dropdown.GetContractingTypeData
EXEC Utility.DropObject 'dropdown.GetContractingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.ContractingType table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetContractingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractingTypeID,
		T.ContractingTypeCode,
		T.ContractingTypeName
	FROM dropdown.ContractingType T
	WHERE (T.ContractingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContractingTypeName, T.ContractingTypeID

END
GO
--End procedure dropdown.GetContractingTypeData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin table dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin table dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.CountryCallingCode table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryCallingCodeID,
		C.CountryName + ' (' + CAST(T.CountryCallingCode AS VARCHAR(5)) + ')' AS CountryCallingCodeName
	FROM dropdown.CountryCallingCode T
		JOIN dropdown.Country C ON C.ISOCountryCode2 = T.ISOCountryCode2
			AND (T.CountryCallingCodeID > 0 OR @IncludeZero = 1)
			AND C.IsActive = 1
	ORDER BY C.DisplayOrder, C.CountryName, T.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin table dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.16
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
	WHERE (T.CurrencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin table dropdown.GetInsuranceTypeData
EXEC Utility.DropObject 'dropdown.GetInsuranceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.InsuranceType table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetInsuranceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InsuranceTypeID,
		T.InsuranceTypeCode,
		T.InsuranceTypeName
	FROM dropdown.InsuranceType T
	WHERE (T.InsuranceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InsuranceTypeName, T.InsuranceTypeID

END
GO
--End procedure dropdown.GetInsuranceTypeData

--Begin procedure dropdown.GetISOCountryCodeByCountryCallingCodeID
EXEC Utility.DropObject 'dropdown.GetISOCountryCodeByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to return data from the dropdown.CountryCallingCode table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetISOCountryCodeByCountryCallingCodeID

@CountryCallingCodeID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CCC.ISOCountryCode2
	FROM dropdown.CountryCallingCode CCC
	WHERE CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetISOCountryCodeByCountryCallingCodeID

--Begin procedure dropdown.GetLanguageData
EXEC Utility.DropObject 'dropdown.GetLanguageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.16
-- Description:	A stored procedure to return data from the dropdown.Language table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetLanguageData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LanguageID,
		T.ISOLanguageCode2,
		T.LanguageName
	FROM dropdown.Language T
	WHERE (T.LanguageID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LanguageName, T.LanguageID

END
GO
--End procedure dropdown.GetLanguageData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin table dropdown.GetPasswordSecurityQuestionData
EXEC Utility.DropObject 'dropdown.GetPasswordSecurityQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the dropdown.PasswordSecurityQuestion table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetPasswordSecurityQuestionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PasswordSecurityQuestionID,
		T.GroupID,
		T.PasswordSecurityQuestionName
	FROM dropdown.PasswordSecurityQuestion T
	WHERE (T.PasswordSecurityQuestionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.GroupID, T.DisplayOrder, T.PasswordSecurityQuestionName, T.PasswordSecurityQuestionID

END
GO
--End procedure dropdown.GetPasswordSecurityQuestionData

--Begin procedure dropdown.GetPaymentMethodData
EXEC Utility.DropObject 'dropdown.GetPaymentMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the dropdown.PaymentMethod table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetPaymentMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PaymentMethodID,
		T.PaymentMethodCode,
		T.PaymentMethodName
	FROM dropdown.PaymentMethod T
	WHERE (T.PaymentMethodID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PaymentMethodName, T.PaymentMethodID

END
GO
--End procedure dropdown.GetPaymentMethodData

--Begin procedure dropdown.GetProjectRoleData
EXEC Utility.DropObject 'dropdown.GetProjectRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the dropdown.ProjectRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProjectRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectRoleID,
		T.ProjectRoleCode,
		T.ProjectRoleName
	FROM dropdown.ProjectRole T
	WHERE (T.ProjectRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectRoleName, T.ProjectRoleID

END
GO
--End procedure dropdown.GetProjectRoleData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData
--End file Build File - 03 - Procedures - Dropdown.sql

--Begin file Build File - 03 - Procedures - EventLog.sql
/* Build File - 03 - Procedures - EventLog */
--USE DeployAdviser
GO

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.05
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cClientCostCodes VARCHAR(MAX) = ''
		
		SELECT @cClientCostCodes = COALESCE(@cClientCostCodes, '') + D.ClientCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientCostCode'), ELEMENTS) AS ClientCostCode
			FROM client.ClientCostCode T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientFunctions VARCHAR(MAX) = ''
		
		SELECT @cClientFunctions = COALESCE(@cClientFunctions, '') + D.ClientFunction
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientFunction'), ELEMENTS) AS ClientFunction
			FROM client.ClientFunction T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientTermOfReference VARCHAR(MAX) = ''
		
		SELECT @cClientTermOfReference = COALESCE(@cClientTermOfReference, '') + D.ClientTermOfReference
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientTermOfReference'), ELEMENTS) AS ClientTermOfReference
			FROM client.ClientTermOfReference T 
			WHERE T.ClientID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cClientCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Functions>' + ISNULL(@cClientFunctions, '') + '</Functions>' AS XML),
			CAST('<TermOfReference>' + ISNULL(@cClientTermOfReference, '') + '</TermOfReference>' AS XML)
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PermissionableTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPermissionableTemplatePermissionables VARCHAR(MAX) = ''
		
		SELECT @cPermissionableTemplatePermissionables = COALESCE(@cPermissionableTemplatePermissionables, '') + D.PermissionableTemplatePermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PermissionableTemplatePermissionable
			FROM permissionable.PermissionableTemplatePermissionable T 
			WHERE T.PermissionableTemplateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PermissionableTemplatePermissionables>' + ISNULL(@cPermissionableTemplatePermissionables, '') + '</PermissionableTemplatePermissionables>' AS XML)
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T
		WHERE T.PermissionableTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonAccounts VARCHAR(MAX) = ''
		
		SELECT @cPersonAccounts = COALESCE(@cPersonAccounts, '') + D.PersonAccount
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonAccount'), ELEMENTS) AS PersonAccount
			FROM person.PersonAccount T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonLanguages VARCHAR(MAX) = ''
		
		SELECT @cPersonLanguages = COALESCE(@cPersonLanguages, '') + D.PersonLanguage
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonLanguage'), ELEMENTS) AS PersonLanguage
			FROM person.PersonLanguage T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonNextOfKin VARCHAR(MAX) = ''
		
		SELECT @cPersonNextOfKin = COALESCE(@cPersonNextOfKin, '') + D.PersonNextOfKin
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonNextOfKin'), ELEMENTS) AS PersonNextOfKin
			FROM person.PersonNextOfKin T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPasswordSecurity VARCHAR(MAX) = ''
		
		SELECT @cPersonPasswordSecurity = COALESCE(@cPersonPasswordSecurity, '') + D.PersonPasswordSecurity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPasswordSecurity'), ELEMENTS) AS PersonPasswordSecurity
			FROM person.PersonPasswordSecurity T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonProofOfLife VARCHAR(MAX) = ''
		
		SELECT @cPersonProofOfLife = COALESCE(@cPersonProofOfLife, '') + D.PersonProofOfLife
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProofOfLife'), ELEMENTS) AS PersonProofOfLife
			FROM person.PersonProofOfLife T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<Accounts>' + ISNULL(@cPersonAccounts, '') + '</Accounts>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Languages>' + ISNULL(@cPersonLanguages, '') + '</Languages>' AS XML),
			CAST('<NextOfKin>' + ISNULL(@cPersonNextOfKin, '') + '</NextOfKin>' AS XML),
			CAST('<PasswordSecurity>' + ISNULL(@cPersonPasswordSecurity, '') + '</PasswordSecurity>' AS XML),
			CAST('<Permissionables>' + ISNULL(@cPersonPermissionables, '') + '</Permissionables>' AS XML),
			CAST('<ProofOfLife>' + ISNULL(@cPersonProofOfLife, '') + '</ProofOfLife>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogPersonProjectAction
EXEC utility.DropObject 'eventlog.LogPersonProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectAction

@EntityID INT = 0,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProject'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cPersonProjectLocations = COALESCE(@cPersonProjectLocations, '') + D.PersonProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProjectLocation'), ELEMENTS) AS PersonProjectLocation
			FROM person.PersonProjectLocation T 
			WHERE T.PersonProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			T.PersonProjectID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Locations>' + ISNULL(@cPersonProjectLocations, '') + '</Locations>' AS XML)
			FOR XML RAW('PersonProject'), ELEMENTS
			)
		FROM person.PersonProject T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonProjectID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			T.PersonProjectID,
			@Comments
		FROM person.PersonProject T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonProjectID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectAction

--Begin procedure eventlog.LogPersonProjectExpenseAction
EXEC utility.DropObject 'eventlog.LogPersonProjectExpenseAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectExpenseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProjectExpense'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('PersonProjectExpense'), ELEMENTS
			)
		FROM person.PersonProjectExpense T
		WHERE T.PersonProjectExpenseID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectExpenseAction

--Begin procedure eventlog.LogPersonProjectTimeAction
EXEC utility.DropObject 'eventlog.LogPersonProjectTimeAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonProjectTimeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonProjectTime'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('PersonProjectTime'), ELEMENTS
			)
		FROM person.PersonProjectTime T
		WHERE T.PersonProjectTimeID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonProjectTimeAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cProjectCostCodes VARCHAR(MAX) = ''
		
		SELECT @cProjectCostCodes = COALESCE(@cProjectCostCodes, '') + D.ProjectCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCostCode'), ELEMENTS) AS ProjectCostCode
			FROM project.ProjectCostCode T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectCurrencies VARCHAR(MAX) = ''
		
		SELECT @cProjectCurrencies = COALESCE(@cProjectCurrencies, '') + D.ProjectCurrency
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCurrency'), ELEMENTS) AS ProjectCurrency
			FROM project.ProjectCurrency T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cProjectLocations = COALESCE(@cProjectLocations, '') + D.ProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectLocation'), ELEMENTS) AS ProjectLocation
			FROM project.ProjectLocation T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cProjectCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Currencies>' + ISNULL(@cProjectCurrencies, '') + '</Currencies>' AS XML),
			CAST('<Locations>' + ISNULL(@cProjectLocations, '') + '</Locations>' AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction
--End file Build File - 03 - Procedures - EventLog.sql

--Begin file Build File - 03 - Procedures - Other.sql
/* Build File - 03 - Procedures - Other */
--USE DeployAdviser
GO

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentSize,
		D.ThumbnailData, 
		D.ThumbnailSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription, 
		D.DocumentName,
		D.DocumentSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure document.GetEntityDocuments
EXEC Utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get records from the document.DocumentEntity table
-- =====================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@ProjectID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentType, 
		D.ContentSubtype,
		D.DocumentGUID,
		D.DocumentID,
		D.DocumentName,
		D.DocumentSize,
		D.ThumbnailData,
		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentID INT,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = @DocumentID
		AND DE.EntityTypeCode = @EntityTypeCode
		AND ((@EntityTypeSubCode = '' AND DE.EntityTypeSubCode IS NULL) OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID 

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure document.SaveEntityDocuments
EXEC Utility.DropObject 'document.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure document.SaveEntityDocuments
--End file Build File - 03 - Procedures - Other.sql

--Begin file Build File - 03 - Procedures - Permissionable.sql
/* Build File - 03 - Procedures - Permissionable */
--USE DeployAdviser
GO

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to delete data from the permissionable.Permissionable and person.PersonPermissionable tables
-- ============================================================================================================================
CREATE PROCEDURE permissionable.DeletePermissionable

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPermissionableLineage VARCHAR(MAX)

	SELECT
		@cPermissionableLineage = P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE P
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = MIPL.PermissionableLineage
		)

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PP.PermissionableLineage
		)

	DELETE PTP
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PTP.PermissionableLineage
		)

	SELECT @cPermissionableLineage AS PermissionableLineage
		
END
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to delete Permissionable Template data
-- ======================================================================
CREATE PROCEDURE permissionable.DeletePermissionableTemplate

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PT
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID

END
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableGroup table
-- =============================================================================================
CREATE PROCEDURE permissionable.GetPermissionableGroups

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PG.PermissionableGroupID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		PG.DisplayOrder,
		(SELECT COUNT(P.PermissionableGroupID) FROM permissionable.Permissionable P WHERE P.PermissionableGroupID = PG.PermissionableGroupID) AS ItemCount
	FROM permissionable.PermissionableGroup PG
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID
		
END
GO
--End procedure permissionable.GetPermissionableGroups

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplate table
-- ================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplateByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PT.PermissionableTemplateID,
		PT.PermissionableTemplateName
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionsByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionsByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplatePermission table
-- ==========================================================================================================
CREATE PROCEDURE permissionable.GetPermissionsByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PermissionableID
	FROM permissionable.PermissionableTemplatePermissionable PTP
		JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
			AND PTP.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionsByPermissionableTemplateID

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE permissionable.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE permissionable.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure permissionable.UpdateParentPermissionableLineageByMenuItemCode
EXEC utility.DropObject 'permissionable.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE permissionable.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
		JOIN core.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO core.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM core.MenuItemPermissionableLineage MIPL1
		JOIN core.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure permissionable.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure permissionable.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'permissionable.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE permissionable.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM person.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1
		AND P1.PersonID > 0

END	
GO
--End procedure permissionable.UpdateSuperAdministratorPersonPermissionables
--End file Build File - 03 - Procedures - Permissionable.sql

--Begin file Build File - 03 - Procedures - Person.sql
/* Build File - 03 - Procedures - Person */
--USE DeployAdviser
GO

--Begin procedure person.AcceptPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.AcceptPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.AcceptPersonProjectByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nPersonID INT = (SELECT PP.PersonID FROM person.PersonProject PP WHERE PP.PersonProjectID = @PersonProjectID)

	UPDATE PP
	SET PP.AcceptedDate = getDate()
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	EXEC eventlog.LogPersonProjectAction @PersonProjectID, 'update', @nPersonID, 'Assignment Accepted'

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND PP.PersonProjectID = @PersonProjectID

	SELECT
		PP.ManagerEmailAddress AS EmailAddress
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID
		AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND PP2.PersonProjectID = @PersonProjectID
			AND P.EmailAddress IS NOT NULL

	ORDER BY 1

END
GO
--End procedure person.AcceptPersonProjectByPersonProjectID

--Begin procedure person.AddPersonPermissionable
EXEC utility.DropObject 'person.AddPersonPermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.AddPersonPermissionable

@PersonID INT, 
@PermissionableLineage VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	VALUES
		(@PersonID, @PermissionableLineage)

END
GO
--End procedure person.AddPersonPermissionable

--Begin procedure person.ApplyPermissionableTemplate
EXEC utility.DropObject 'person.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE person.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM core.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PTP.PermissionableLineage
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM person.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

END
GO
--End procedure person.ApplyPermissionableTemplate

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE T.ClientID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND EXISTS 
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 

					UNION 

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1
					)
				AND (@AccessCode <> 'AddUpdate' OR (T.EndDate >= getDate() AND T.AcceptedDate IS NULL))

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.IsInvoiced = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.IsInvoiced = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM project.Project T WHERE T.ProjectID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.expirePersonProjectByPersonProjectIDList
EXEC utility.DropObject 'person.expirePersonProjectByPersonProjectIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.expirePersonProjectByPersonProjectIDList

@PersonProjectIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PP
	SET PP.EndDate = getDate()
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	EXEC eventlog.LogPersonProjectAction 0, 'update', @PersonID, 'Assignment Expired', @PersonProjectIDList

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	SELECT
		P.EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND P.EmailAddress IS NOT NULL

	UNION

	SELECT
		PP.ManagerEmailAddress AS EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress,
		PP2.PersonProjectID
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP2.PersonProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND P.EmailAddress IS NOT NULL

	ORDER BY 2, 1

END
GO
--End procedure person.expirePersonProjectByPersonProjectIDList

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'person.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- =====================================================================
CREATE PROCEDURE person.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		P.EmailAddress,
		P.PersonID
	FROM person.Person P
	WHERE EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
			JOIN core.ListToTable(@PermissionableLineage, ',') LTT ON LTT.ListItem = PP.PermissionableLineage
				AND PP.PersonID = P.PersonID
		)
		AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure person.GetEmailAddressesByPermissionableLineage

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.NickName,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the person.Person table based on a Token
-- ============================================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@Token VARCHAR(36)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PersonID,
		P.TokenCreateDateTime,

		CASE
			WHEN DATEDIFF("hour", P.TokenCreateDateTime, getdate()) <= 24
			THEN 0
			ELSE 1
		END AS IsTokenExpired

	FROM person.Person P
	WHERE P.Token = @Token

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.GetPersonPermissionables
EXEC utility.DropObject 'person.GetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.PersonPermissionable table
-- ======================================================================================
CREATE PROCEDURE person.GetPersonPermissionables

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PP.PermissionableLineage
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
	ORDER BY PP.PermissionableLineage

END
GO
--End procedure person.GetPersonPermissionables

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		IsActive BIT,
		ProjectLocationID INT,
		ProjectLocationName VARCHAR(100),
		Days INT NOT NULL DEFAULT 0,
		HoursPerDay INT NOT NULL DEFAULT 8,
		Rate NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS ClientTermOfReferenceID,
			'' AS ClientTermOfReferenceName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName
		FROM project.Project P
		WHERE P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CTOR.ClientTermOfReferenceID,
			CTOR.ClientTermOfReferenceName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET 
			TPPL.Days = PPL.Days,
			TPPL.HoursPerDay = PPL.HoursPerDay,
			TPPL.Rate = PPL.Rate 
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		T.IsActive,
		T.ProjectLocationID,
		T.ProjectLocationName,
		T.Days,
		T.HoursPerDay,
		T.Rate
	FROM @tPersonProjectLocation T
	ORDER BY T.ProjectLocationName, T.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectExpenseDataByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectExpenseDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return ClientCostCode and ProjectCurrency data based on a PersonProjectID
-- ============================================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PCC.ClientCostCodeID,

		CASE
			WHEN PCC.ProjectCostCodeDescription IS NULL
			THEN CCC.ClientCostCodeDescription
			ELSE PCC.ProjectCostCodeDescription
		END AS ProjectCostCodeDescription

	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
		JOIN project.Project P ON P.ProjectID = PCC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PCC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
	ORDER BY 2, 1

	SELECT 
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM dropdown.Currency C
		JOIN project.ProjectCurrency PC ON PC.ISOCurrencyCode = C.ISOCurrencyCode
		JOIN project.Project P ON P.ProjectID = PC.ProjectID
		JOIN person.PersonProject PP ON PP.ProjectID = PC.ProjectID
			AND PP.PersonProjectID = @PersonProjectID
	ORDER BY 1, 2

END
GO
--End procedure person.GetPersonProjectExpenseDataByPersonProjectID

--Begin procedure person.GetPersonProjectLocationsByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectLocationsByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProjectLocation table based on a PersonID
-- ==============================================================================================================
CREATE PROCEDURE person.GetPersonProjectLocationsByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			AND PPL.PersonProjectID = @PersonProjectID
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectLocationsByPersonProjectID

--Begin procedure person.GetPersonProjectsByPersonID
EXEC utility.DropObject 'person.GetPersonProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.PersonProjectID,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectID, CTOR.ClientTermOfReferenceName, CTOR.ClientTermOfReferenceID, CF.ClientFunctionName, CF.ClientFunctionID

END
GO
--End procedure person.GetPersonProjectsByPersonID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.SavePersonPermissionables
EXEC Utility.DropObject 'person.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.SavePersonPermissionables

@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM core.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PermissionableLineage = P.PermissionableLineage
				AND PP.PersonID = @PersonID
			)

END
GO
--End procedure person.SavePersonPermissionables

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.SetPersonPermissionables
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionables table based on a PersonID
-- =========================================================================================================
CREATE PROCEDURE person.SetPersonPermissionables

@PersonID INT,
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	IF @PermissionableIDList IS NOT NULL AND LEN(RTRIM(@PermissionableIDList)) > 0
		BEGIN

		INSERT INTO person.PersonPermissionable
			(PersonID, PermissionableLineage)
		SELECT
			@PersonID,
			P.PermissionableLineage
		FROM permissionable.Permissionable P
			JOIN core.ListToTable(@PermissionableIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PermissionableID

		END
	--ENDIF

END
GO
--End procedure person.SetPersonPermissionables

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		UserName VARCHAR(250),
		CountryCallingCodeID INT,
		CellPhone VARCHAR(64),
		IsPhoneVerified BIT,
		ClientAdministratorCount INT,
		ClientProjectManagerCount INT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR (@bIsTwoFactorEnabled = 1 AND (P.CellPhone IS NULL OR LEN(LTRIM(P.CellPhone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cCellPhone = P.CellPhone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,UserName,CountryCallingCodeID,CellPhone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cUserName,
			@nCountryCallingCodeID,
			@cCellPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure person.ValidateUserName
EXEC Utility.DropObject 'person.ValidateUserName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.19
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateUserName

@UserName VARCHAR(250),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.UserName = @UserName
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateUserName
--End file Build File - 03 - Procedures - Person.sql

--Begin file Build File - 04 - Data - Dropdown.sql
/* Build File - 04 - Data - Dropdown */
--USE DeployAdviser
GO

--Begin table dropdown.ContractingType
TRUNCATE TABLE dropdown.ContractingType
GO

EXEC utility.InsertIdentityValue 'dropdown.ContractingType', 'ContractingTypeID', 0
GO

INSERT INTO dropdown.ContractingType 
	(ContractingTypeCode, ContractingTypeName) 
VALUES
	('Individual', 'Individual'),
	('Company', 'Professional Services Company')
GO
--End table dropdown.ContractingType

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder, IsActive) 
VALUES 
	(N'GB', N'GBR', N'United Kingdom', 1, 1),
	(N'AD', N'AND', N'Andorra', 100, 1),
	(N'AE', N'ARE', N'United Arab Emirates', 100, 1),
	(N'AF', N'AFG', N'Afghanistan', 100, 1),
	(N'AG', N'ATG', N'Antigua and Barbuda', 100, 1),
	(N'AI', N'AIA', N'Anguilla', 100, 1),
	(N'AL', N'ALB', N'Albania', 100, 1),
	(N'AM', N'ARM', N'Armenia', 100, 1),
	(N'AO', N'AGO', N'Angola', 100, 1),
	(N'AR', N'ARG', N'Argentina', 100, 1),
	(N'AS', N'ASM', N'American Samoa', 100, 1),
	(N'AT', N'AUT', N'Austria', 100, 1),
	(N'AU', N'AUS', N'Australia', 100, 1),
	(N'AW', N'ABW', N'Aruba', 100, 1),
	(N'AX', N'ALA', N'Ãland Islands', 100, 1),
	(N'AZ', N'AZE', N'Azerbaijan', 100, 1),
	(N'BA', N'BIH', N'Bosnia and Herzegovina', 100, 1),
	(N'BB', N'BRB', N'Barbados', 100, 1),
	(N'BD', N'BGD', N'Bangladesh', 100, 1),
	(N'BE', N'BEL', N'Belgium', 100, 1),
	(N'BF', N'BFA', N'Burkina Faso', 100, 1),
	(N'BG', N'BGR', N'Bulgaria', 100, 1),
	(N'BH', N'BHR', N'Bahrain', 100, 1),
	(N'BI', N'BDI', N'Burundi', 100, 1),
	(N'BJ', N'BEN', N'Benin', 100, 1),
	(N'BL', N'BLM', N'Saint BarthÃ©lemy', 100, 1),
	(N'BM', N'BMU', N'Bermuda', 100, 1),
	(N'BN', N'BRN', N'Brunei Darussalam', 100, 1),
	(N'BO', N'BOL', N'Bolivia', 100, 1),
	(N'BR', N'BRA', N'Brazil', 100, 1),
	(N'BS', N'BHS', N'Bahamas', 100, 1),
	(N'BT', N'BTN', N'Bhutan', 100, 1),
	(N'BU', N'BES', N'Bonaire, Sint Eustatius and Saba', 100, 1),
	(N'BV', N'BVT', N'Bouvet Island', 100, 1),
	(N'BW', N'BWA', N'Botswana', 100, 1),
	(N'BY', N'BLR', N'Belarus', 100, 1),
	(N'BZ', N'BLZ', N'Belize', 100, 1),
	(N'CA', N'CAN', N'Canada', 100, 1),
	(N'CC', N'CCK', N'Cocos (Keeling) Islands', 100, 1),
	(N'CD', N'COD', N'Congo, The Democratic Republic Of The', 100, 1),
	(N'CF', N'CAF', N'Central African Republic', 100, 1),
	(N'CG', N'COG', N'Congo', 100, 1),
	(N'CH', N'CHE', N'Switzerland', 100, 1),
	(N'CI', N'CIV', N'CÃ´te D''Ivoire', 100, 1),
	(N'CK', N'COK', N'Cook Islands', 100, 1),
	(N'CL', N'CHL', N'Chile', 100, 1),
	(N'CM', N'CMR', N'Cameroon', 100, 1),
	(N'CN', N'CHN', N'China', 100, 1),
	(N'CO', N'COL', N'Colombia', 100, 1),
	(N'CR', N'CRI', N'Costa Rica', 100, 1),
	(N'CU', N'CUB', N'Cuba', 100, 1),
	(N'CV', N'CPV', N'Cape Verde', 100, 1),
	(N'CW', N'CUW', N'CuraÃ§ao', 100, 1),
	(N'CX', N'CXR', N'Christmas Island', 100, 1),
	(N'CY', N'CYP', N'Cyprus', 100, 1),
	(N'CZ', N'CZE', N'Czech Republic', 100, 1),
	(N'DE', N'DEU', N'Germany', 100, 1),
	(N'DJ', N'DJI', N'Djibouti', 100, 1),
	(N'DK', N'DNK', N'Denmark', 100, 1),
	(N'DM', N'DMA', N'Dominica', 100, 1),
	(N'DO', N'DOM', N'Dominican Republic', 100, 1),
	(N'DZ', N'DZA', N'Algeria', 100, 1),
	(N'EC', N'ECU', N'Ecuador', 100, 1),
	(N'EE', N'EST', N'Estonia', 100, 1),
	(N'EG', N'EGY', N'Egypt', 100, 1),
	(N'EH', N'ESH', N'Western Sahara', 100, 1),
	(N'ER', N'ERI', N'Eritrea', 100, 1),
	(N'ES', N'ESP', N'Spain', 100, 1),
	(N'ET', N'ETH', N'Ethiopia', 100, 1),
	(N'EU', N'EUR', N'European Union', 100, 1),
	(N'FI', N'FIN', N'Finland', 100, 1),
	(N'FJ', N'FJI', N'Fiji', 100, 1),
	(N'FK', N'FLK', N'Falkland Islands', 100, 1),
	(N'FM', N'FSM', N'Micronesia, Federated States Of', 100, 1),
	(N'FO', N'FRO', N'Faroe Islands', 100, 1),
	(N'FR', N'FRA', N'France', 100, 1),
	(N'GA', N'GAB', N'Gabon', 100, 1),
	(N'GD', N'GRD', N'Grenada', 100, 1),
	(N'GE', N'GEO', N'Georgia', 100, 1),
	(N'GF', N'GUF', N'French Guiana', 100, 1),
	(N'GG', N'GGY', N'Guernsey', 100, 1),
	(N'GH', N'GHA', N'Ghana', 100, 1),
	(N'GI', N'GIB', N'Gibraltar', 100, 1),
	(N'GL', N'GRL', N'Greenland', 100, 1),
	(N'GM', N'GMB', N'Gambia', 100, 1),
	(N'GN', N'GIN', N'Guinea', 100, 1),
	(N'GP', N'GLP', N'Guadeloupe', 100, 1),
	(N'GQ', N'GNQ', N'Equatorial Guinea', 100, 1),
	(N'GR', N'GRC', N'Greece', 100, 1),
	(N'GT', N'GTM', N'Guatemala', 100, 1),
	(N'GU', N'GUM', N'Guam', 100, 1),
	(N'GW', N'GNB', N'Guinea-Bissau', 100, 1),
	(N'GY', N'GUY', N'Guyana', 100, 1),
	(N'HK', N'HKG', N'Hong Kong', 100, 1),
	(N'HM', N'HMD', N'Heard Island and McDonald Islands', 100, 1),
	(N'HN', N'HND', N'Honduras', 100, 1),
	(N'HR', N'HRV', N'Croatia', 100, 1),
	(N'HT', N'HTI', N'Haiti', 100, 1),
	(N'HU', N'HUN', N'Hungary', 100, 1),
	(N'ID', N'IDN', N'Indonesia', 100, 1),
	(N'IE', N'IRL', N'Ireland', 100, 1),
	(N'IL', N'ISR', N'Israel', 100, 1),
	(N'IM', N'IMN', N'Isle of Man', 100, 1),
	(N'IN', N'IND', N'India', 100, 1),
	(N'IO', N'IOT', N'British Indian Ocean Territory', 100, 1),
	(N'IQ', N'IRQ', N'Iraq', 100, 1),
	(N'IR', N'IRN', N'Iran, Islamic Republic Of', 100, 1),
	(N'IS', N'ISL', N'Iceland', 100, 1),
	(N'IT', N'ITA', N'Italy', 100, 1),
	(N'JE', N'JEY', N'Jersey', 100, 1),
	(N'JM', N'JAM', N'Jamaica', 100, 1),
	(N'JO', N'JOR', N'Jordan', 100, 1),
	(N'JP', N'JPN', N'Japan', 100, 1),
	(N'KE', N'KEN', N'Kenya', 100, 1),
	(N'KG', N'KGZ', N'Kyrgyzstan', 100, 1),
	(N'KH', N'KHM', N'Cambodia', 100, 1),
	(N'KI', N'KIR', N'Kiribati', 100, 1),
	(N'KM', N'COM', N'Comoros', 100, 1),
	(N'KN', N'KNA', N'Saint Kitts And Nevis', 100, 1),
	(N'KP', N'PRK', N'Korea, Democratic People''s Republic Of', 100, 1),
	(N'KR', N'KOR', N'Korea, Republic of', 100, 1),
	(N'KW', N'KWT', N'Kuwait', 100, 1),
	(N'KY', N'CYM', N'Cayman Islands', 100, 1),
	(N'KZ', N'KAZ', N'Kazakhstan', 100, 1),
	(N'LA', N'LAO', N'Lao People''s Democratic Republic', 100, 1),
	(N'LB', N'LBN', N'Lebanon', 100, 1),
	(N'LC', N'LCA', N'Saint Lucia', 100, 1),
	(N'LI', N'LIE', N'Liechtenstein', 100, 1),
	(N'LK', N'LKA', N'Sri Lanka', 100, 1),
	(N'LR', N'LBR', N'Liberia', 100, 1),
	(N'LS', N'LSO', N'Lesotho', 100, 1),
	(N'LT', N'LTU', N'Lithuania', 100, 1),
	(N'LU', N'LUX', N'Luxembourg', 100, 1),
	(N'LV', N'LVA', N'Latvia', 100, 1),
	(N'LY', N'LBY', N'Libya', 100, 1),
	(N'MA', N'MAR', N'Morocco', 100, 1),
	(N'MC', N'MCO', N'Monaco', 100, 1),
	(N'MD', N'MDA', N'Moldova, Republic of', 100, 1),
	(N'ME', N'MNE', N'Montenegro', 100, 1),
	(N'MF', N'MAF', N'Saint Martin (French Part)', 100, 1),
	(N'MG', N'MDG', N'Madagascar', 100, 1),
	(N'MH', N'MHL', N'Marshall Islands', 100, 1),
	(N'MK', N'MKD', N'Macedonia', 100, 1),
	(N'ML', N'MLI', N'Mali', 100, 1),
	(N'MM', N'MMR', N'Myanmar', 100, 1),
	(N'MN', N'MNG', N'Mongolia', 100, 1),
	(N'MO', N'MAC', N'Macao', 100, 1),
	(N'MP', N'MNP', N'Northern Mariana Islands', 100, 1),
	(N'MQ', N'MTQ', N'Martinique', 100, 1),
	(N'MR', N'MRT', N'Mauritania', 100, 1),
	(N'MS', N'MSR', N'Montserrat', 100, 1),
	(N'MT', N'MLT', N'Malta', 100, 1),
	(N'MU', N'MUS', N'Mauritius', 100, 1),
	(N'MV', N'MDV', N'Maldives', 100, 1),
	(N'MW', N'MWI', N'Malawi', 100, 1),
	(N'MX', N'MEX', N'Mexico', 100, 1),
	(N'MY', N'MYS', N'Malaysia', 100, 1),
	(N'MZ', N'MOZ', N'Mozambique', 100, 1),
	(N'NA', N'NAM', N'Namibia', 100, 1),
	(N'NC', N'NCL', N'New Caledonia', 100, 1),
	(N'NE', N'NER', N'Niger', 100, 1),
	(N'NF', N'NFK', N'Norfolk Island', 100, 1),
	(N'NG', N'NGA', N'Nigeria', 100, 1),
	(N'NI', N'NIC', N'Nicaragua', 100, 1),
	(N'NL', N'NLD', N'Netherlands', 100, 1),
	(N'NO', N'NOR', N'Norway', 100, 1),
	(N'NP', N'NPL', N'Nepal', 100, 1),
	(N'NR', N'NRU', N'Nauru', 100, 1),
	(N'NU', N'NIU', N'Niue', 100, 1),
	(N'NZ', N'NZL', N'New Zealand', 100, 1),
	(N'OM', N'OMN', N'Oman', 100, 1),
	(N'PA', N'PAN', N'Panama', 100, 1),
	(N'PE', N'PER', N'Peru', 100, 1),
	(N'PF', N'PYF', N'French Polynesia', 100, 1),
	(N'PG', N'PNG', N'Papua New Guinea', 100, 1),
	(N'PH', N'PHL', N'Philippines', 100, 1),
	(N'PK', N'PAK', N'Pakistan', 100, 1),
	(N'PL', N'POL', N'Poland', 100, 1),
	(N'PM', N'SPM', N'Saint Pierre And Miquelon', 100, 1),
	(N'PN', N'PCN', N'Pitcairn', 100, 1),
	(N'PR', N'PRI', N'Puerto Rico', 100, 1),
	(N'PT', N'PRT', N'Portugal', 100, 1),
	(N'PW', N'PLW', N'Palau', 100, 1),
	(N'PY', N'PRY', N'Paraguay', 100, 1),
	(N'QA', N'QAT', N'Qatar', 100, 1),
	(N'RE', N'REU', N'RÃ©union', 100, 1),
	(N'RO', N'ROU', N'Romania', 100, 1),
	(N'RS', N'SRB', N'Serbia', 100, 1),
	(N'RU', N'RUS', N'Russian Federation', 100, 1),
	(N'RW', N'RWA', N'Rwanda', 100, 1),
	(N'SA', N'SAU', N'Saudi Arabia', 100, 1),
	(N'SB', N'SLB', N'Solomon Islands', 100, 1),
	(N'SC', N'SYC', N'Seychelles', 100, 1),
	(N'SD', N'SDN', N'Sudan', 100, 1),
	(N'SE', N'SWE', N'Sweden', 100, 1),
	(N'SG', N'SGP', N'Singapore', 100, 1),
	(N'SH', N'SHN', N'Saint Helena, Ascension and Tristan Da Cunha', 100, 1),
	(N'SI', N'SVN', N'Slovenia', 100, 1),
	(N'SJ', N'SJM', N'Svalbard And Jan Mayen', 100, 1),
	(N'SK', N'SVK', N'Slovakia', 100, 1),
	(N'SL', N'SLE', N'Sierra Leone', 100, 1),
	(N'SM', N'SMR', N'San Marino', 100, 1),
	(N'SN', N'SEN', N'Senegal', 100, 1),
	(N'SO', N'SOM', N'Somalia', 100, 1),
	(N'SR', N'SUR', N'Suriname', 100, 1),
	(N'SS', N'SSD', N'South Sudan', 100, 1),
	(N'ST', N'STP', N'Sao Tome and Principe', 100, 1),
	(N'SV', N'SLV', N'El Salvador', 100, 1),
	(N'SX', N'SXM', N'Sint Maarten (Dutch part)', 100, 1),
	(N'SY', N'SYR', N'Syrian Arab Republic', 100, 1),
	(N'SZ', N'SWZ', N'Swaziland', 100, 1),
	(N'TC', N'TCA', N'Turks and Caicos Islands', 100, 1),
	(N'TD', N'TCD', N'Chad', 100, 1),
	(N'TF', N'ATF', N'French Southern Territories', 100, 1),
	(N'TG', N'TGO', N'Togo', 100, 1),
	(N'TH', N'THA', N'Thailand', 100, 1),
	(N'TJ', N'TJK', N'Tajikistan', 100, 1),
	(N'TK', N'TKL', N'Tokelau', 100, 1),
	(N'TL', N'TLS', N'Timor-Leste', 100, 1),
	(N'TM', N'TKM', N'Turkmenistan', 100, 1),
	(N'TN', N'TUN', N'Tunisia', 100, 1),
	(N'TO', N'TON', N'Tonga', 100, 1),
	(N'TR', N'TUR', N'Turkey', 100, 1),
	(N'TT', N'TTO', N'Trinidad and Tobago', 100, 1),
	(N'TV', N'TUV', N'Tuvalu', 100, 1),
	(N'TW', N'TWN', N'Taiwan', 100, 1),
	(N'TZ', N'TZA', N'Tanzania, United Republic of', 100, 1),
	(N'UA', N'UKR', N'Ukraine', 100, 1),
	(N'UG', N'UGA', N'Uganda', 100, 1),
	(N'UM', N'UMI', N'United States Minor Outlying Islands', 100, 1),
	(N'US', N'USA', N'United States', 2, 1),
	(N'UY', N'URY', N'Uruguay', 100, 1),
	(N'UZ', N'UZB', N'Uzbekistan', 100, 1),
	(N'VA', N'VAT', N'Holy See (Vatican City State)', 100, 1),
	(N'VC', N'VCT', N'Saint Vincent And The Grenadines', 100, 1),
	(N'VE', N'VEN', N'Venezuela, Bolivarian Republic of', 100, 1),
	(N'VG', N'VGB', N'Virgin Islands, British', 100, 1),
	(N'VI', N'VIR', N'Virgin Islands, U.S.', 100, 1),
	(N'VN', N'VNM', N'Viet Nam', 100, 1),
	(N'VU', N'VUT', N'Vanuatu', 100, 1),
	(N'WF', N'WLF', N'Wallis and Futuna', 100, 1),
	(N'WS', N'WSM', N'Samoa', 100, 1),
	(N'YE', N'YEM', N'Yemen', 100, 1),
	(N'YT', N'MYT', N'Mayotte', 100, 1),
	(N'ZA', N'ZAF', N'South Africa', 100, 1),
	(N'ZM', N'ZMB', N'Zambia', 100, 1),
	(N'ZW', N'ZWE', N'Zimbabwe', 100, 1)
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
TRUNCATE TABLE dropdown.CountryCallingCode
GO

EXEC utility.InsertIdentityValue 'dropdown.CountryCallingCode', 'CountryCallingCodeID', 0
GO

INSERT INTO dropdown.CountryCallingCode
	(ISOCountryCode2, CountryCallingCode)
VALUES
	('AD', 376),
	('AE', 971),
	('AF', 93),
	('AG', 1268),
	('AI', 1264),
	('AL', 355),
	('AM', 374),
	('AO', 244),
	('AR', 54),
	('AS', 1684),
	('AT', 43),
	('AU', 61),
	('AW', 297),
	('AX', 358),
	('AZ', 994),
	('BA', 387),
	('BB', 1246),
	('BD', 880),
	('BE', 32),
	('BF', 226),
	('BG', 359),
	('BH', 973),
	('BI', 257),
	('BJ', 229),
	('BL', 590),
	('BM', 1441),
	('BN', 673),
	('BO', 591),
	('BR', 55),
	('BS', 1242),
	('BT', 975),
	('BW', 267),
	('BY', 375),
	('BZ', 501),
	('CA', 1),
	('CC', 61),
	('CD', 243),
	('CF', 236),
	('CG', 242),
	('CH', 41),
	('CI', 225),
	('CK', 682),
	('CL', 56),
	('CM', 237),
	('CN', 86),
	('CO', 57),
	('CR', 506),
	('CU', 53),
	('CV', 238),
	('CW', 5999),
	('CX', 61),
	('CY', 357),
	('CZ', 420),
	('DE', 49),
	('DJ', 253),
	('DK', 45),
	('DM', 1767),
	('DO', 180),
	('DO', 291),
	('DO', 849),
	('DO', 918),
	('DZ', 213),
	('EC', 593),
	('EE', 372),
	('EG', 20),
	('EH', 212),
	('ER', 291),
	('ES', 34),
	('ET', 251),
	('FI', 358),
	('FJ', 679),
	('FK', 500),
	('FM', 691),
	('FO', 298),
	('FR', 33),
	('GA', 241),
	('GB', 44),
	('GD', 1473),
	('GE', 995),
	('GF', 594),
	('GG', 44),
	('GH', 233),
	('GI', 350),
	('GL', 299),
	('GM', 220),
	('GN', 224),
	('GP', 590),
	('GQ', 240),
	('GR', 30),
	('GT', 502),
	('GU', 1671),
	('GW', 245),
	('GY', 592),
	('HK', 852),
	('HN', 504),
	('HR', 385),
	('HT', 509),
	('HU', 36),
	('ID', 62),
	('IE', 353),
	('IL', 972),
	('IM', 44),
	('IN', 91),
	('IO', 246),
	('IQ', 964),
	('IR', 98),
	('IS', 354),
	('IT', 39),
	('JE', 44),
	('JM', 1876),
	('JO', 962),
	('JP', 81),
	('KE', 254),
	('KG', 996),
	('KH', 855),
	('KI', 686),
	('KM', 269),
	('KN', 1869),
	('KP', 850),
	('KR', 82),
	('KW', 965),
	('KY', 1345),
	('KZ', 76),
	('KZ', 77),
	('LA', 856),
	('LB', 961),
	('LC', 1758),
	('LI', 423),
	('LK', 94),
	('LR', 231),
	('LS', 266),
	('LT', 370),
	('LU', 352),
	('LV', 371),
	('LY', 218),
	('MA', 212),
	('MC', 377),
	('MD', 373),
	('ME', 382),
	('MF', 590),
	('MG', 261),
	('MH', 692),
	('MK', 389),
	('ML', 223),
	('MM', 95),
	('MN', 976),
	('MO', 853),
	('MP', 1670),
	('MQ', 596),
	('MR', 222),
	('MS', 1664),
	('MT', 356),
	('MU', 230),
	('MV', 960),
	('MW', 265),
	('MX', 52),
	('MY', 60),
	('MZ', 258),
	('NA', 264),
	('NC', 687),
	('NE', 227),
	('NF', 672),
	('NG', 234),
	('NI', 505),
	('NL', 31),
	('NO', 47),
	('NP', 977),
	('NR', 674),
	('NU', 683),
	('NZ', 64),
	('OM', 968),
	('PA', 507),
	('PE', 51),
	('PF', 689),
	('PG', 675),
	('PH', 63),
	('PK', 92),
	('PL', 48),
	('PM', 508),
	('PN', 64),
	('PR', 17),
	('PR', 871),
	('PR', 939),
	('PT', 351),
	('PW', 680),
	('PY', 595),
	('QA', 974),
	('RE', 262),
	('RO', 40),
	('RS', 381),
	('RU', 7),
	('RW', 250),
	('SA', 966),
	('SB', 677),
	('SC', 248),
	('SD', 249),
	('SE', 46),
	('SG', 65),
	('SI', 386),
	('SJ', 4779),
	('SK', 421),
	('SL', 232),
	('SM', 378),
	('SN', 221),
	('SO', 252),
	('SR', 597),
	('SS', 211),
	('ST', 239),
	('SV', 503),
	('SX', 1721),
	('SY', 963),
	('SZ', 268),
	('TC', 1649),
	('TD', 235),
	('TG', 228),
	('TH', 66),
	('TJ', 992),
	('TK', 690),
	('TL', 670),
	('TM', 993),
	('TN', 216),
	('TO', 676),
	('TR', 90),
	('TT', 1868),
	('TV', 688),
	('TW', 886),
	('TZ', 255),
	('UA', 380),
	('UG', 256),
	('US', 1),
	('UY', 598),
	('UZ', 998),
	('VA', 3),
	('VA', 379),
	('VA', 698),
	('VA', 906),
	('VC', 1784),
	('VE', 58),
	('VG', 1284),
	('VI', 1340),
	('VN', 84),
	('VU', 678),
	('WF', 681),
	('WS', 685),
	('YE', 967),
	('YT', 262),
	('ZA', 27),
	('ZM', 260),
	('ZW', 263)
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
TRUNCATE TABLE dropdown.Currency
GO

EXEC utility.InsertIdentityValue 'dropdown.Currency', 'CurrencyID', 0
GO

INSERT INTO dropdown.Currency
	(ISOCurrencyCode, CurrencyName, DisplayOrder, IsActive) 
VALUES
	(N'AED', N'UAE Dirham', 100, 1),
	(N'AZN', N'Azerbaijanian Manat', 100, 1),
	(N'NGN', N'Nigerian Naira', 100, 1),
	(N'NIO', N'Nicaraguan Cordoba Oro', 100, 1),
	(N'NOK', N'Norwegian Krone', 100, 1),
	(N'NPR', N'Nepalese Rupee', 100, 1),
	(N'NZD', N'New Zealand Dollar', 100, 1),
	(N'OMR', N'Omani Rial', 100, 1),
	(N'PAB', N'Panamean Balboa', 100, 1),
	(N'PEN', N'Puruvian Nuevo Sol', 100, 1),
	(N'PGK', N'Papa New Guinean Kina', 100, 1),
	(N'PHP', N'Philippine Peso', 100, 1),
	(N'BAM', N'Bosnia and Herzegovina Convertible Mark', 100, 1),
	(N'PKR', N'Pakistan Rupee', 100, 1),
	(N'PLN', N'Polish Zloty', 100, 1),
	(N'PYG', N'Paraguay Guarani', 100, 1),
	(N'QAR', N'Qatari Rial', 100, 1),
	(N'RON', N'New Romanian Leu', 100, 1),
	(N'RSD', N'Serbian Dinar', 100, 1),
	(N'RUB', N'Russian Ruble', 100, 1),
	(N'RWF', N'Rwandan Franc', 100, 1),
	(N'SAR', N'Saudi Riyal', 100, 1),
	(N'SBD', N'Solomon Islands Dollar', 100, 1),
	(N'BBD', N'Barbados Dollar', 100, 1),
	(N'SCR', N'Seychelles Rupee', 100, 1),
	(N'SDG', N'Sudanese Pound', 100, 1),
	(N'SEK', N'Swedish Krona', 100, 1),
	(N'SGD', N'Singapore Dollar', 100, 1),
	(N'SHP', N'Saint Helena Pound', 100, 1),
	(N'SLL', N'Sierra Leone Leone', 100, 1),
	(N'SOS', N'Somali Shilling', 100, 1),
	(N'SRD', N'Surinam Dollar', 100, 1),
	(N'SSP', N'South Sudanese Pound', 100, 1),
	(N'STD', N'Sao Tome and Principe Dobra', 100, 1),
	(N'BDT', N'Bangladeshi Taka', 100, 1),
	(N'SVC', N'El Salvador Colon', 100, 1),
	(N'SYP', N'Syrian Pound', 100, 1),
	(N'SZL', N'Swaziland Lilangeni', 100, 1),
	(N'THB', N'Thai Baht', 100, 1),
	(N'TJS', N'Tajikistan Somoni', 100, 1),
	(N'TMT', N'Turkmenistan New Manat', 100, 1),
	(N'TND', N'Tunisian Dinar', 100, 1),
	(N'TOP', N'Tongan Pa''anga', 100, 1),
	(N'TRY', N'Turkish Lira', 100, 1),
	(N'TTD', N'Trinidad and Tobago Dollar', 100, 1),
	(N'BGN', N'Bulgarian Lev', 100, 1),
	(N'TWD', N'New Taiwan Dollar', 100, 1),
	(N'TZS', N'Tanzanian Shilling', 100, 1),
	(N'UAH', N'Ukranian Hryvnia', 100, 1),
	(N'UGX', N'Uganda Shilling', 100, 1),
	(N'USD', N'US Dollar', 3, 1),
	(N'UYU', N'Uragyan Peso Uruguayo', 100, 1),
	(N'UZS', N'Uzbekistan Sum', 100, 1),
	(N'VEF', N'Venezuelan Bolivar Fuerte', 100, 1),
	(N'VND', N'Vietnamise Dong', 100, 1),
	(N'VUV', N'Vanuatun Vatu', 100, 1),
	(N'BHD', N'Bahraini Dinar', 100, 1),
	(N'WST', N'Samoan Tala', 100, 1),
	(N'XAF', N'CFA Franc BEAC', 100, 1),
	(N'XCD', N'East Caribbean Dollar', 100, 1),
	(N'XOF', N'CFA Franc BCEAO', 100, 1),
	(N'XPF', N'CFP Franc', 100, 1),
	(N'YER', N'Yemeni Rial', 100, 1),
	(N'ZAR', N'South African Rand', 100, 1),
	(N'ZMW', N'New Zambian Kwacha', 100, 1),
	(N'ZWL', N'Zimbabwe Dollar', 100, 1),
	(N'BIF', N'Burundi Franc', 100, 1),
	(N'BMD', N'Bermudian Dollar', 100, 1),
	(N'BND', N'Brunei Dollar', 100, 1),
	(N'BOB', N'Bolivian Boliviano', 100, 1),
	(N'AFN', N'Afghani Afghani', 100, 1),
	(N'BRL', N'Brazilian Real', 100, 1),
	(N'BSD', N'Bahamian Dollar', 100, 1),
	(N'BTN', N'Bhutanise Ngultrum', 100, 1),
	(N'BWP', N'Botswanan Pula', 100, 1),
	(N'BYR', N'Belarussian Ruble', 100, 1),
	(N'BZD', N'Belize Dollar', 100, 1),
	(N'CAD', N'Canadian Dollar', 100, 1),
	(N'CDF', N'Congolais Franc', 100, 1),
	(N'CHF', N'Swiss Franc', 100, 1),
	(N'CLP', N'Chilean Peso', 100, 1),
	(N'ALL', N'Albanian Lek', 100, 1),
	(N'CNY', N'Chinese Yuan Renminbi', 100, 1),
	(N'COP', N'Colombian Peso', 100, 1),
	(N'CRC', N'Costa Rican Colon', 100, 1),
	(N'CUP', N'Cuban Peso', 100, 1),
	(N'CVE', N'Cape Verde Escudo', 100, 1),
	(N'CZK', N'Czech Koruna', 100, 1),
	(N'DJF', N'Djibouti Franc', 100, 1),
	(N'DKK', N'Danish Krone', 100, 1),
	(N'DOP', N'Dominican Peso', 100, 1),
	(N'DZD', N'Algerian Dinar', 100, 1),
	(N'AMD', N'Armenian Dram', 100, 1),
	(N'EGP', N'Egyptian Pound', 100, 1),
	(N'ERN', N'Eritrean Nakfa', 100, 1),
	(N'ETB', N'Ethiopian Birr', 100, 1),
	(N'EUR', N'Euro', 2, 1),
	(N'FJD', N'Fiji Dollar', 100, 1),
	(N'FKP', N'Falkland Islands Pound', 100, 1),
	(N'GBP', N'UK Pound Sterling', 1, 1),
	(N'GEL', N'Geogrian Lari', 100, 1),
	(N'GHS', N'Ghanan Cedi', 100, 1),
	(N'GIP', N'Gibraltar Pound', 100, 1),
	(N'ANG', N'Netherlands Antillean Guilder', 100, 1),
	(N'GMD', N'Gambian Dalasi', 100, 1),
	(N'GNF', N'Guinea Franc', 100, 1),
	(N'GTQ', N'Guatemalian Quetzal', 100, 1),
	(N'GYD', N'Guyana Dollar', 100, 1),
	(N'HKD', N'Hong Kong Dollar', 100, 1),
	(N'HNL', N'Honduran Lempira', 100, 1),
	(N'HRK', N'Croatian Kuna', 100, 1),
	(N'HTG', N'Hatian Gourde', 100, 1),
	(N'HUF', N'Hungarian Forint', 100, 1),
	(N'IDR', N'Indonesian Rupiah', 100, 1),
	(N'AOA', N'Angolan Kwanza', 100, 1),
	(N'ILS', N'Israeli New Shekel', 100, 1),
	(N'INR', N'Indian Rupee', 100, 1),
	(N'IQD', N'Iraqi Dinar', 100, 1),
	(N'IRR', N'Iranian Rial', 100, 1),
	(N'ISK', N'Iceland Krona', 100, 1),
	(N'JMD', N'Jamaican Dollar', 100, 1),
	(N'JOD', N'Jordanian Dinar', 100, 1),
	(N'JPY', N'Japanese Yen', 100, 1),
	(N'KES', N'Kenyan Shilling', 100, 1),
	(N'KGS', N'Kyrgyzstan Som', 100, 1),
	(N'ARS', N'Argentine Peso', 100, 1),
	(N'KHR', N'Cambodian Riel', 100, 1),
	(N'KMF', N'Comoro Franc', 100, 1),
	(N'KPW', N'North Korean Won', 100, 1),
	(N'KRW', N'South Korean Won', 100, 1),
	(N'KWD', N'Kuwaiti Dinar', 100, 1),
	(N'KYD', N'Cayman Islands Dollar', 100, 1),
	(N'KZT', N'Kazakhstan Tenge', 100, 1),
	(N'LAK', N'Lao Kip', 100, 1),
	(N'LBP', N'Lebanese Pound', 100, 1),
	(N'LKR', N'Sri Lanka Rupee', 100, 1),
	(N'AUD', N'Australian Dollar', 100, 1),
	(N'LRD', N'Liberian Dollar', 100, 1),
	(N'LSL', N'Lesotho Loti', 100, 1),
	(N'LTL', N'Lithuanian Litas', 100, 1),
	(N'LVL', N'Latvian Lats', 100, 1),
	(N'LYD', N'Libyan Dinar', 100, 1),
	(N'MAD', N'Moroccan Dirham', 100, 1),
	(N'MDL', N'Moldovan Leu', 100, 1),
	(N'MGA', N'Malagasy Ariary', 100, 1),
	(N'MKD', N'Macedonian Denar', 100, 1),
	(N'MMK', N'Myanmar Kyat', 100, 1),
	(N'AWG', N'Aruban Florin', 100, 1),
	(N'MNT', N'Mongolian Tugrik', 100, 1),
	(N'MOP', N'Macaoan Pataca', 100, 1),
	(N'MRO', N'Maurutanian Ouguiya', 100, 1),
	(N'MUR', N'Mauritius Rupee', 100, 1),
	(N'MVR', N'Maldiven Rufiyaa', 100, 1),
	(N'MWK', N'Malawian Kwacha', 100, 1),
	(N'MXN', N'Mexican Peso', 100, 1),
	(N'MYR', N'Malaysian Ringgit', 100, 1),
	(N'MZN', N'Mozambique Metical', 100, 1),
	(N'NAD', N'Namibia Dollar', 100, 1)
GO
--End table dropdown.Currency

--Begin table dropdown.InsuranceType
TRUNCATE TABLE dropdown.InsuranceType
GO

EXEC utility.InsertIdentityValue 'dropdown.InsuranceType', 'InsuranceTypeID', 0
GO

INSERT INTO dropdown.InsuranceType 
	(InsuranceTypeCode, InsuranceTypeName, DisplayOrder) 
VALUES
	('NotRequired', 'Not Required', 1),
	('Company', 'Provided By Consultancy', 2),
	('Individual', 'Provided By Consultant', 3)
GO
--End table dropdown.InsuranceType

--Begin table dropdown.Language
TRUNCATE TABLE dropdown.Language
GO

EXEC utility.InsertIdentityValue 'dropdown.Language', 'LanguageID', 0
GO

INSERT INTO dropdown.Language
	(ISOLanguageCode2, LanguageName)
VALUES
	('AB', 'Abkhazian'),
	('AA', 'Afar'),
	('AF', 'Afrikaans'),
	('SQ', 'Albanian'),
	('AM', 'Amharic'),
	('AR', 'Arabic'),
	('HY', 'Armenian'),
	('AS', 'Assamese'),
	('AY', 'Aymara'),
	('AZ', 'Azerbaijani'),
	('BA', 'Bashkir'),
	('EU', 'Basque'),
	('BN', 'Bengali'),
	('DZ', 'Bhutani'),
	('BH', 'Bihari'),
	('BI', 'Bislama'),
	('BR', 'Breton'),
	('BG', 'Bulgarian'),
	('MY', 'Burmese'),
	('BE', 'Byelorussian'),
	('KM', 'Cambodian'),
	('CA', 'Catalan'),
	('ZH', 'Chinese'),
	('CO', 'Corsican'),
	('HR', 'Croatian'),
	('CS', 'Czech'),
	('DA', 'Danish'),
	('NL', 'Dutch'),
	('EN', 'English'),
	('EO', 'Esperanto'),
	('ET', 'Estonian'),
	('FO', 'Faeroese'),
	('FJ', 'Fiji'),
	('FI', 'Finnish'),
	('FR', 'French'),
	('FY', 'Frisian'),
	('GD', 'Gaelic'),
	('GL', 'Galician'),
	('KA', 'Georgian'),
	('DE', 'German'),
	('EL', 'Greek'),
	('KL', 'Greenlandic'),
	('GN', 'Guarani'),
	('GU', 'Gujarati'),
	('HA', 'Hausa'),
	('IW', 'Hebrew'),
	('HI', 'Hindi'),
	('HU', 'Hungarian'),
	('IS', 'Icelandic'),
	('IN', 'Indonesian'),
	('IA', 'Interlingua'),
	('IE', 'Interlingue'),
	('IK', 'Inupiak'),
	('GA', 'Irish'),
	('IT', 'Italian'),
	('JA', 'Japanese'),
	('JW', 'Javanese'),
	('KN', 'Kannada'),
	('KS', 'Kashmiri'),
	('KK', 'Kazakh'),
	('RW', 'Kinyarwanda'),
	('KY', 'Kirghiz'),
	('RN', 'Kirundi'),
	('QN', 'Klingon'),
	('KO', 'Korean'),
	('KU', 'Kurdish'),
	('LO', 'Laothian'),
	('LA', 'Latin'),
	('LV', 'Latvian'),
	('LN', 'Lingala'),
	('LT', 'Lithuanian'),
	('MK', 'Macedonian'),
	('MG', 'Malagasy'),
	('MS', 'Malay'),
	('ML', 'Malayalam'),
	('MT', 'Maltese'),
	('MI', 'Maori'),
	('MR', 'Marathi'),
	('MO', 'Moldavian'),
	('MN', 'Mongolian'),
	('NA', 'Nauru'),
	('NE', 'Nepali'),
	('NO', 'Norwegian'),
	('OC', 'Occitan'),
	('OR', 'Oriya'),
	('OM', 'Oromo'),
	('PS', 'Pashto'),
	('FA', 'Persian'),
	('PL', 'Polish'),
	('PT', 'Portuguese'),
	('PA', 'Punjabi'),
	('QU', 'Quechua'),
	('RO', 'Romanian'),
	('RU', 'Russian'),
	('SM', 'Samoan'),
	('SG', 'Sangro'),
	('SA', 'Sanskrit'),
	('SR', 'Serbian'),
	('SH', 'Serbo-Croatian'),
	('ST', 'Sesotho'),
	('TN', 'Setswana'),
	('SN', 'Shona'),
	('SD', 'Sindhi'),
	('SI', 'Singhalese'),
	('SS', 'Siswati'),
	('SK', 'Slovak'),
	('SL', 'Slovenian'),
	('SO', 'Somali'),
	('ES', 'Spanish'),
	('SU', 'Sudanese'),
	('SW', 'Swahili'),
	('SV', 'Swedish'),
	('TL', 'Tagalog'),
	('TG', 'Tajik'),
	('TA', 'Tamil'),
	('TT', 'Tatar'),
	('TE', 'Tegulu'),
	('TH', 'Thai'),
	('BO', 'Tibetan'),
	('TI', 'Tigrinya'),
	('TO', 'Tonga'),
	('TS', 'Tsonga'),
	('TR', 'Turkish'),
	('TK', 'Turkmen'),
	('TW', 'Twi'),
	('UK', 'Ukrainian'),
	('UR', 'Urdu'),
	('UZ', 'Uzbek'),
	('VI', 'Vietnamese'),
	('VO', 'Volapuk'),
	('CY', 'Welsh'),
	('WO', 'Wolof'),
	('XH', 'Xhosa'),
	('JI', 'Yiddish'),
	('YO', 'Yoruba'),
	('ZU', 'Zulu')
GO

UPDATE L
SET 
	L.DisplayOrder = 
		CASE
			WHEN L.ISOLanguageCode2 = 'EN'
			THEN 1
			WHEN L.ISOLanguageCode2 = 'QN'
			THEN 2
			ELSE L.DisplayOrder
		END

FROM dropdown.Language L
GO
--End table dropdown.Language

--Begin table dropdown.PasswordSecurityQuestion
TRUNCATE TABLE dropdown.PasswordSecurityQuestion
GO

EXEC utility.InsertIdentityValue 'dropdown.PasswordSecurityQuestion', 'PasswordSecurityQuestionID', 0
GO

INSERT INTO dropdown.PasswordSecurityQuestion 
	(GroupID, PasswordSecurityQuestionName, DisplayOrder) 
VALUES 
	(1, 'What is the name of your first niece/nephew?', 1),
	(1, 'What is your maternal grandmother''s first name?', 2),
	(1, 'What is your maternal grandfather''s first name?', 3),
	(1, 'What was the name of your first pet?', 4),
	(2, 'What is your father''s middle name?', 1),
	(2, 'What is your mother''s middle name?', 2),
	(2, 'In what city were you married?', 3),
	(2, 'What is the first name of your first child?', 4),
	(3, 'In what city was your mother born? (Enter full name of city only)', 1),
	(3, 'In what city was your father born?  (Enter full name of city only)', 2),
	(3, 'In what city was your high school? (Enter only "Charlotte" for Charlotte High School)', 3),
	(3, 'In what city were you born? (Enter full name of city only)', 4)
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.PaymentMethod
TRUNCATE TABLE dropdown.PaymentMethod
GO

EXEC utility.InsertIdentityValue 'dropdown.PaymentMethod', 'PaymentMethodID', 0
GO

INSERT INTO dropdown.PaymentMethod 
	(PaymentMethodCode,PaymentMethodName) 
VALUES
	('AmEx', 'American Express'),
	('Cash', 'Cash'),
	('Check', 'Cheque'),
	('Corporate', 'Corporate Card'),
	('Credit', 'Credit / Debit Card'),
	('Other', 'Other')
GO
--End table dropdown.PaymentMethod

--Begin table dropdown.ProjectRole
TRUNCATE TABLE dropdown.ProjectRole
GO

EXEC utility.InsertIdentityValue 'dropdown.ProjectRole', 'ProjectRoleID', 0
GO

INSERT INTO dropdown.ProjectRole 
	(ProjectRoleName) 
VALUES
	('DFID STTA Junior level'),
	('DFID STTA Mid level'),
	('DFID STTA Senior level'),
	('FCO STTA Junior level'),
	('FCO STTA Mid level'),
	('FCO STTA Senior level'),
	('SU STTA Junior level'),
	('SU STTA Mid level'),
	('SU STTA Senior level')
GO
--End table dropdown.ProjectRole

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleCode, RoleName, DisplayOrder) 
VALUES
	('Consultant', 'Consultant', 1),
	('ProjectManager', 'Project Manager', 2),
	('ConsultancyAdministrator', 'Consultancy Administrator', 3)
GO
--End table dropdown.Role


--End file Build File - 04 - Data - Dropdown.sql

--Begin file Build File - 04 - Data - Other.sql
/* Build File - 04 - Data - Other */
--USE DeployAdviser
GO

--Begin table client.Client 
TRUNCATE TABLE client.Client
GO

INSERT INTO client.Client 
	(ClientName, EmailAddress, IsActive, Phone, FAX, BillAddress1, BillAddress2, BillAddress3, BillMunicipality, BillRegion, BillPostalCode, BillISOCountryCode2, MailAddress1, MailAddress2, MailAddress3, MailMunicipality, MailRegion, MailPostalCode, MailISOCountryCode2, CanProjectAlterCostCodeDescription, HREmailAddress, InvoiceDueReminderInterval, PersonAccountEmailAddress, SendOverdueInvoiceEmails, TaxID, TaxRate, Website) 
VALUES 
	('OceanDisc LLC', 'todd.pires@oceandisc.com', 1, '555-1212', '555-1313', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1, 'todd.pires@oceandisc.com', 0, 'todd.pires@oceandisc.com', 0, '987654321', CAST(10.0000 AS Numeric(18, 4)), NULL),
	('HofGothi LLC', 'john.lyons@oceandisc.com', 1, '555-2222', '555-2323', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1, 'john.lyons@oceandisc.com', 0, 'john.lyons@oceandisc.com', 0, '123456789', CAST(10.0000 AS Numeric(18, 4)), NULL),
	('Digital Roar LLC', 'john.lyons@oceandisc.com', 1, '555-2222', '555-2323', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1, 'john.lyons@oceandisc.com', 0, 'john.lyons@oceandisc.com', 0, '123456789', CAST(10.0000 AS Numeric(18, 4)), NULL)
GO
--End table client.Client 

--Begin table client.ClientCostCode 
TRUNCATE TABLE client.ClientCostCode
GO

INSERT INTO client.ClientCostCode 
	(ClientID, ClientCostCodeName, ClientCostCodeDescription, IsActive) 
VALUES 
	(1, 'CC11', 'Cost code description for CC11', 1),
	(1, 'CC12', 'Cost code description for CC11', 1),
	(1, 'CC13', 'Cost code description for CC11', 1),
	(1, 'CC14', 'Cost code description for CC11', 1),
	(1, 'CC15', 'Cost code description for CC11', 1),
	(2, 'CC21', 'Cost code description for CC21', 1),
	(2, 'CC22', 'Cost code description for CC21', 1),
	(2, 'CC23', 'Cost code description for CC21', 1),
	(2, 'CC24', 'Cost code description for CC21', 1),
	(2, 'CC25', 'Cost code description for CC21', 1)
GO
--End table client.ClientCostCode 

--Begin table client.ClientFunction 
TRUNCATE TABLE client.ClientFunction
GO

INSERT INTO client.ClientFunction 
	(ClientID, ClientFunctionName, ClientFunctionDescription, IsActive) 
VALUES 
	(1, 'F11', 'Function 11', 1),
	(1, 'F12', 'Function 12', 1),
	(1, 'F13', 'Function 13', 1),
	(1, 'F14', 'Function 14', 1),
	(1, 'F15', 'Function 15', 1),
	(2, 'F21', 'Function 21', 1),
	(2, 'F22', 'Function 22', 1),
	(2, 'F23', 'Function 23', 1),
	(2, 'F24', 'Function 24', 1),
	(2, 'F25', 'Function 25', 1)
GO
--End table client.ClientFunction

--Begin table client.ClientPerson
TRUNCATE TABLE client.ClientPerson
GO

INSERT INTO client.ClientPerson 
	(ClientID, PersonID, ClientPersonRoleCode) 
VALUES 
	(1, 3, 'Administrator'),
	(1, 3, 'ProjectManager'),
	(1, 5, 'Administrator'),
	(1, 5, 'ProjectManager'),
	(2, 1, 'Administrator'),
	(2, 1, 'ProjectManager'),
	(2, 2, 'Administrator'),
	(2, 2, 'ProjectManager')
GO
--End table client.ClientPerson

--Begin table client.ClientTermOfReference
TRUNCATE TABLE client.ClientTermOfReference
GO

INSERT INTO client.ClientTermOfReference 
	(ClientID, ClientTermOfReferenceName, ClientTermOfReferenceDescription, IsActive) 
VALUES 
	(1, 'TOR11', 'Term Of Reference 11', 1),
	(1, 'TOR12', 'Term Of Reference 12', 1),
	(1, 'TOR13', 'Term Of Reference 13', 1),
	(1, 'TOR14', 'Term Of Reference 14', 1),
	(1, 'TOR15', 'Term Of Reference 15', 1),
	(2, 'TOR21', 'Term Of Reference 21', 1),
	(2, 'TOR22', 'Term Of Reference 22', 1),
	(2, 'TOR23', 'Term Of Reference 23', 1),
	(2, 'TOR24', 'Term Of Reference 24', 1),
	(2, 'TOR25', 'Term Of Reference 25', 1)
GO
--End table client.ClientTermOfReference

--Begin table client.ProjectInvoiceTo
TRUNCATE TABLE client.ProjectInvoiceTo
GO

INSERT INTO client.ProjectInvoiceTo 
	(ClientID, ProjectInvoiceToAddressee, ProjectInvoiceToEmailAddress, ProjectInvoiceToPhone, ProjectInvoiceToName, ProjectInvoiceToAddress1, ProjectInvoiceToAddress2, ProjectInvoiceToAddress3, ProjectInvoiceToMunicipality, ProjectInvoiceToRegion, ProjectInvoiceToPostalCode, ProjectInvoiceToISOCountryCode2, IsActive)
VALUES 
	(1, 'Todd', 'todd.pires@oceandisc.com', '727.579.1066', 'Todd''s', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1),
	(1, 'John', 'john.lyons@oceandisc.com', '555.555.5555', 'John''s', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1),
	(1, 'Kevin', 'kevin.ross@oceandisc.com', '555.555.6666', 'Kevin''s', '1201 Hummingbird Hill Road', NULL, NULL, 'Chapel Hill', 'NC', '27517', 'US', 1),
	(2, 'Todd', 'todd.pires@oceandisc.com', '727.579.1066', 'Todd''s', '177 Myrtle Brook Bend', NULL, NULL, 'Ponte Vedra', 'FL', '32081', 'US', 1),
	(2, 'John', 'john.lyons@oceandisc.com', '555.555.5555', 'John''s', '6174 Kissengen Spring Ct', NULL, NULL, 'Jacksonville', 'FL', '32258', 'US', 1),
	(2, 'Kevin', 'kevin.ross@oceandisc.com', '555.555.6666', 'Kevin''s', '1201 Hummingbird Hill Road', NULL, NULL, 'Chapel Hill', 'NC', '27517', 'US', 1)
GO
--End table client.ProjectInvoiceTo

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('PersonProject', '[[ClientFunctionName]]', 'Function'),
	('PersonProject', '[[ClientName]]', 'Client Name'),
	('PersonProject', '[[ClientTermOfReferenceName]]', 'Term Of Reference'),
	('PersonProject', '[[PersonNameFormatted]]', 'Person Name'),
	('PersonProject', '[[ProjectName]]', 'Project Name')
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Client', 
	@EntityTypeName = 'Client', 
	@EntityTypeNamePlural = 'Clients',
	@SchemaName = 'client', 
	@TableName = 'Client', 
	@PrimaryKeyFieldName = 'ClientID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Document',
	@EntityTypeName = 'Document',
	@EntityTypeNamePlural = 'Documents',
	@SchemaName = 'document',
	@TableName = 'Document',
	@PrimaryKeyFieldName = 'DocumentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Invoice', 
	@EntityTypeName = 'Invoice', 
	@EntityTypeNamePlural = 'Invoices',
	@SchemaName = 'invoice', 
	@TableName = 'Invoice', 
	@PrimaryKeyFieldName = 'InvoiceID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProject', 
	@EntityTypeName = 'Person Project', 
	@EntityTypeNamePlural = 'Person Projects',
	@SchemaName = 'personproject', 
	@TableName = 'PersonProject', 
	@PrimaryKeyFieldName = 'PersonProjectID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProjectTime', 
	@EntityTypeName = 'Time Record', 
	@EntityTypeNamePlural = 'Time Records',
	@SchemaName = 'person', 
	@TableName = 'PersonProjectTime', 
	@PrimaryKeyFieldName = 'PersonProjectTimeID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonProjectExpense', 
	@EntityTypeName = 'Expense Record', 
	@EntityTypeNamePlural = 'Expense Records',
	@SchemaName = 'person', 
	@TableName = 'PersonProjectExpense', 
	@PrimaryKeyFieldName = 'PersonProjectExpenseID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Project', 
	@EntityTypeName = 'Project', 
	@EntityTypeNamePlural = 'Projects',
	@SchemaName = 'project', 
	@TableName = 'Project', 
	@PrimaryKeyFieldName = 'ProjectID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'Core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-building',
	@NewMenuItemCode = 'ClientList',
	@NewMenuItemLink = '/client/list',
	@NewMenuItemText = 'Clients',
	@PermissionableLineageList = 'Client.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ClientList',
	@Icon = 'fa fa-fw fa-tasks',
	@NewMenuItemCode = 'ProjectList',
	@NewMenuItemLink = '/project/list',
	@NewMenuItemText = 'Projects',
	@PermissionableLineageList = 'Project.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ProjectList',
	@Icon = 'fa fa-fw fa-flag',
	@NewMenuItemCode = 'PersonProjectList',
	@NewMenuItemLink = '/personproject/list',
	@NewMenuItemText = 'Deployments',
	@PermissionableLineageList = 'PersonProject.List'
GO

--Begin Time & Expenses Submenu --
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectList',
	@Icon = 'fa fa-fw fa-calendar',
	@NewMenuItemCode = 'TimeExpense',
	@NewMenuItemText = 'Time &amp; Expenses'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PersonProjectTimeList',
	@NewMenuItemLink = '/personprojecttime/list',
	@NewMenuItemText = 'Time Log',
	@ParentMenuItemCode = 'TimeExpense',
	@PermissionableLineageList = 'PersonProjectTime.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectTimeList',
	@NewMenuItemCode = 'PersonProjectExpenseList',
	@NewMenuItemLink = '/personprojectExpense/list',
	@NewMenuItemText = 'Expense Log',
	@ParentMenuItemCode = 'TimeExpense',
	@PermissionableLineageList = 'PersonProjectExpense.List'
GO
--End Time & Expenses Submenu --

--Begin Admin Submenu --
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TimeExpense',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO
--End Admin Submenu --

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'TimeExpense'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationKey', NULL, 'DIUZQ1ICXUIMHKKK6TCX'
EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationSecretKey', NULL, 'MylcNoqlXVduD3LBn981ZHRY7OGOcKjsZ8LDTOTH'
EXEC core.SystemSetupAddUpdate 'DuoApiEndPoint', NULL, 'api-8a6e671f.duosecurity.com'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationKey', NULL, 'DIT96UH6EI8OVAUXJVT2'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationSecretKey', NULL, 'q7NjG8UbJzEMEDKNTH9m3FFBTSkEmAxUodib025p'
EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@deployadviser.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '30'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/deployadviser-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://da2.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'DeployAdviser'
EXEC core.SystemSetupAddUpdate 'TwoFactorEnabled', NULL, '0'
GO
--End table core.SystemSetup

--Begin table document.FileType
TRUNCATE TABLE document.FileType
GO

INSERT INTO document.FileType
	(Extension, MimeType)
VALUES
	('.doc', 'application/msword'),
	('.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('.gif', 'image/gif'),
	('.htm', 'text/html'),
	('.html', 'text/html'),
	('.jpeg', 'image/jpeg'),
	('.jpg', 'image/jpeg'),
	('.pdf', 'application/pdf'),
	('.png', 'image/png'),
	('.pps', 'application/mspowerpoint'),
	('.ppt', 'application/mspowerpoint'),
	('.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
	('.rtf', 'application/rtf'),
	('.txt', 'text/plain'),
	('.xls', 'application/excel'),
	('.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
GO
--End table document.FileType

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Add a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, EmailAddress, LastLoginDateTime, InvalidLoginAttempts, IsAccountLockedOut, IsActive, IsSuperAdministrator, Password, PasswordSalt, PasswordExpirationDateTime) 
VALUES
	('Nameer', 'Al-Hadithi', 'Mr.', 'Nameer', 'nalhadithi@skotkonung.com', NULL, 0, 0, 1, 0, '214676C4D305E151B0FB4FA87637F0C356750B288C6DA3FE7259CDCDFEE2E709', '500918F4-F98F-4DEC-80FB-1087E993A021', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Burnham', 'Mr.', 'jburnham', 'jonathan.burnham@oceandisc.com', NULL, 0, 0, 1, 1, 'EB065F0E9CFE4AA399624ADFEF14427FA39070F25A92B0DFF210D0F13C11B46F', 'AA47A377-16D5-4319-9C92-EEB2C67C05E5', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Cole', 'Mr.', 'JCole', 'jcole@skotkonung.com', NULL, 0, 0, 1, 1, '67589DA2DA1F9EFEB1B78C407E1A66B4CDE8577D259BBAA420FB17C798B6D3C5', 'D2A94546-77A0-448B-B754-E78A6C458CC9', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('John', 'Lyons', 'Mr.', 'jlyons', 'john.lyons@oceandisc.com', NULL, 0, 0, 1, 0, '138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', '71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Usamah', 'Mahmood', 'Mr.', 'usamah', 'usamah852@gmail.com', NULL, 0, 0, 1, 0, '71F3FF82541088FEAAD1651D1CB5A50B70A7C955FD20F12462D6C1E74278A9B9', 'B4EABF7F-315A-48CF-BDC8-DD84093DF57C', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Dave', 'Roberts', 'Mr.', 'daver', 'david.roberts@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'kevin.ross@oceandisc.com', NULL, 0, 0, 1, 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Ian', 'van Mourik', 'Mr.', 'IanVM', 'ian.van.mourik@gmail.com', NULL, 0, 0, 1, 0, 'DC8013389868E63E539C553780046DB1DF3A80090FC66A2972A5416643E26D23', 'CCB4FD4C-0453-4245-A43C-01F1C620939C', CAST('2020-01-01T00:00:00.000' AS DateTime))
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.Person

--Begin table person.PersonAccount
TRUNCATE TABLE person.PersonAccount
GO

INSERT INTO person.PersonAccount
	(PersonID,PersonAccountName,IntermediateAccountNumber,IntermediateBankBranch,IntermediateBankName,IntermediateBankRoutingNumber,IntermediateIBAN,IntermediateSWIFTCode,IntermediateISOCurrencyCode,TerminalAccountNumber,TerminalBankBranch,TerminalBankName,TerminalBankRoutingNumber,TerminalIBAN,TerminalSWIFTCode,TerminalISOCurrencyCode)
SELECT
	P.PersonID,
	'PersonAccountName 1',
	'IntermediateAccountNumber 1',
	'IntermediateBankBranch 1',
	'IntermediateBankName 1',
	'1234567890',
	'IntermediateIBAN 1',
	'123456789012345',
	'USD',
	'TerminalAccountNumber 1',
	'TerminalBankBranch 1',
	'TerminalBankName 1',
	'9876543210',
	'TerminalIBAN 1',
	'987654321098765',
	'GBP'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonAccount
	(PersonID,PersonAccountName,IntermediateAccountNumber,IntermediateBankBranch,IntermediateBankName,IntermediateBankRoutingNumber,IntermediateIBAN,IntermediateSWIFTCode,IntermediateISOCurrencyCode,TerminalAccountNumber,TerminalBankBranch,TerminalBankName,TerminalBankRoutingNumber,TerminalIBAN,TerminalSWIFTCode,TerminalISOCurrencyCode)
SELECT
	P.PersonID,
	'PersonAccountName 2',
	'IntermediateAccountNumber 2',
	'IntermediateBankBranch 2',
	'IntermediateBankName 2',
	'1234567890',
	'IntermediateIBAN 2',
	'123456789012345',
	'USD',
	'TerminalAccountNumber 2',
	'TerminalBankBranch 2',
	'TerminalBankName 2',
	'9876543210',
	'TerminalIBAN 2',
	'987654321098765',
	'GBP'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonAccount

--Begin table person.PersonLanguage
TRUNCATE TABLE person.PersonLanguage
GO

INSERT INTO person.PersonLanguage
	(PersonID,ISOLanguageCode2,OralLevel,ReadLevel,WriteLevel)
SELECT
	P.PersonID,
	'PT',
	1,
	2,
	3
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonLanguage
	(PersonID,ISOLanguageCode2,OralLevel,ReadLevel,WriteLevel)
SELECT
	P.PersonID,
	'QN',
	2,
	2,
	2
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonLanguage

--Begin table person.PersonNextOfKin
TRUNCATE TABLE person.PersonNextOfKin
GO

INSERT INTO person.PersonNextOfKin
	(PersonID,PersonNextOfKinName,Relationship,Phone,CellPhone,WorkPhone,Address1,Address2,Address3,Municipality,Region,PostalCode,ISOCountryCode2)
SELECT
	P.PersonID,
	'Tony Pires',
	'Father',
	'555-1111',
	'555-2222',
	'555-3333',
	'Address1',
	'Address2',
	'Address3',
	'Ponet Vedra',
	'FL',
	'32081',
	'US'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonNextOfKin
	(PersonID,PersonNextOfKinName,Relationship,Phone,CellPhone,WorkPhone,Address1,Address2,Address3,Municipality,Region,PostalCode,ISOCountryCode2)
SELECT
	P.PersonID,
	'Nancy Pires',
	'Mother',
	'555-4444',
	'555-5555',
	'555-6666',
	'Address1',
	'Address2',
	'Address3',
	'Ponet Vedra',
	'FL',
	'32081',
	'US'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonNextOfKin

--Begin table person.PersonPasswordSecurity
TRUNCATE TABLE person.PersonPasswordSecurity
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	1,
	'Scott'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 1
		)
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	6,
	'Deane'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 6
		)
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	12,
	'Norwalk'
FROM person.Person P
WHERE P.LastName = 'Pires'
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPasswordSecurity PPS
		WHERE PPS.PersonID = P.PersonID
			AND PPS.PasswordSecurityQuestionID = 12
		)
GO
--End table person.PersonPasswordSecurity

--Begin table person.PersonProofOfLife
TRUNCATE TABLE person.PersonProofOfLife
GO

INSERT INTO person.PersonProofOfLife
	(PersonID,ProofOfLifeQuestion,ProofOfLifeAnswer)
SELECT
	P.PersonID,
	'Question 1',
	'Answer 1'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonProofOfLife
	(PersonID,ProofOfLifeQuestion,ProofOfLifeAnswer)
SELECT
	P.PersonID,
	'Question 2',
	'Answer 2'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonProofOfLife

--End file Build File - 04 - Data - Other.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2017.11.01 22.36.40')
GO
--End build tracking

