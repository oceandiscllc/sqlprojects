/* Build File - 01 - Tables - Dropdown */
USE DeployAdviser
GO

--Begin table dropdown.ContractingType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContractingType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContractingType
	(
	ContractingTypeID INT IDENTITY(0,1) NOT NULL,
	ContractingTypeCode VARCHAR(50),
	ContractingTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContractingTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContractingType', 'DisplayOrder,ContractingTypeName'
GO
--End table dropdown.ContractingType

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	ISOCurrencyCode CHAR(3),
	CountryName NVARCHAR(250),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_Country', 'DisplayOrder,CountryName'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
DECLARE @TableName VARCHAR(250) = 'dropdown.CountryCallingCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CountryCallingCode
	(
	CountryCallingCodeID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	CountryCallingCode INT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCode', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryCallingCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_CountryCallingCode', 'ISOCountryCode2,CountryCallingCode'
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
DECLARE @TableName VARCHAR(250) = 'dropdown.Currency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Currency
	(
	CurrencyID INT IDENTITY(0,1) NOT NULL,
	ISOCurrencyCode CHAR(3),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CurrencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_Currency', 'DisplayOrder,CurrencyName'
GO
--End table dropdown.Currency

--Begin table dropdown.InsuranceType
DECLARE @TableName VARCHAR(250) = 'dropdown.InsuranceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InsuranceType
	(
	InsuranceTypeID INT IDENTITY(0,1) NOT NULL,
	InsuranceTypeCode VARCHAR(50),
	InsuranceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InsuranceTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_InsuranceType', 'DisplayOrder,InsuranceTypeName'
GO
--End table dropdown.InsuranceType

--Begin table dropdown.Language
DECLARE @TableName VARCHAR(250) = 'dropdown.Language'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Language
	(
	LanguageID INT IDENTITY(0,1) NOT NULL,
	ISOLanguageCode2 CHAR(2),
	LanguageName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LanguageID'
EXEC utility.SetIndexClustered @TableName, 'IX_Language', 'DisplayOrder,LanguageName'
GO
--End table dropdown.Language

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PasswordSecurityQuestion
	(
	PasswordSecurityQuestionID INT IDENTITY(0, 1) NOT NULL,
	GroupID INT,
	PasswordSecurityQuestionName VARCHAR(500),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GroupID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PasswordSecurityQuestionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PasswordSecurityQuestion', 'GroupID,DisplayOrder,PasswordSecurityQuestionName'
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.PaymentMethod
DECLARE @TableName VARCHAR(250) = 'dropdown.PaymentMethod'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PaymentMethod
	(
	PaymentMethodID INT IDENTITY(0,1) NOT NULL,
	PaymentMethodName VARCHAR(50),
	PaymentMethodCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PaymentMethodID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PaymentMethod', 'DisplayOrder,PaymentMethodName', 'PaymentMethodID'
GO
--End table dropdown.PaymentMethod

--Begin table dropdown.ProjectRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectRole
	(
	ProjectRoleID INT IDENTITY(0,1) NOT NULL,
	ProjectRoleName VARCHAR(50),
	ProjectRoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProjectRole', 'DisplayOrder,ProjectRoleName', 'ProjectRoleID'
GO
--End table dropdown.ProjectRole

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	RoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role