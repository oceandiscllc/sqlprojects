/* Build File - 01 - Tables - Person */
USE DeployAdviser
GO

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	--Begin standard fields
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	NickName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	CountryCallingCodeID INT,
	CellPhone VARCHAR(15),
	IsPhoneVerified BIT,
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,

	--Begin Address fields
	BillAddress1 VARCHAR(50),
	BillAddress2 VARCHAR(50),
	BillAddress3 VARCHAR(50),
	BillMunicipality VARCHAR(50),
	BillRegion VARCHAR(50),
	BillPostalCode VARCHAR(10),
	BillISOCountryCode2 CHAR(2),
	MailAddress1 VARCHAR(50),
	MailAddress2 VARCHAR(50),
	MailAddress3 VARCHAR(50),
	MailMunicipality VARCHAR(50),
	MailRegion VARCHAR(50),
	MailPostalCode VARCHAR(10),
	MailISOCountryCode2 CHAR(2),

	--Begin Miscellaneous fields
	BirthDate DATE,
	ChestSize NUMERIC(18,2),
	CollarSize NUMERIC(18,2),
	GenderCode CHAR(1),
	HeadSize NUMERIC(18,2),
	Height NUMERIC(18,2),
	IsUKEUNational BIT,

	--Begin Mobile & Consultancy fields
	ContractingTypeID INT,
	IsRegisteredForUKTax BIT,
	MobilePIN VARCHAR(50),
	OwnCompanyName VARCHAR(50),
	RegistrationCode BIGINT,
	SendInvoicesFrom VARCHAR(50),
	TaxID VARCHAR(50),
	TaxRate NUMERIC(18,4)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ChestSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CollarSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContractingTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HeadSize', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Height', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsPhoneVerified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsRegisteredForUKTax', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUKEUNational', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RegistrationCode', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18,4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonAccount
	(
	PersonAccountID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonAccountName VARCHAR(50),
	IntermediateAccountNumber VARCHAR(50),
	IntermediateBankBranch VARCHAR(50),
	IntermediateBankName VARCHAR(50),
	IntermediateBankRoutingNumber VARCHAR(10),
	IntermediateIBAN VARCHAR(50),
	IntermediateSWIFTCode VARCHAR(15),
	IntermediateISOCurrencyCode CHAR(3),
	TerminalAccountNumber VARCHAR(50),
	TerminalBankBranch VARCHAR(50),
	TerminalBankName VARCHAR(50),
	TerminalBankRoutingNumber VARCHAR(10),
	TerminalIBAN VARCHAR(50),
	TerminalSWIFTCode VARCHAR(15),
	TerminalISOCurrencyCode CHAR(3)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonAccountID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonAccount', 'PersonID,PersonAccountName'
GO
--End table person.PersonAccount

--Begin table person.PersonLanguage
DECLARE @TableName VARCHAR(250) = 'person.PersonLanguage'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonLanguage
	(
	PersonLanguageID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ISOLanguageCode2 CHAR(2),
	OralLevel INT,
	ReadLevel INT,
	WriteLevel INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'OralLevel', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReadLevel', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WriteLevel', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonLanguageID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonLanguage', 'PersonID,ISOLanguageCode2'
GO
--End table person.PersonLanguage

--Begin table person.PersonNextOfKin
DECLARE @TableName VARCHAR(250) = 'person.PersonNextOfKin'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonNextOfKin
	(
	PersonNextOfKinID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonNextOfKinName VARCHAR(100),
	Relationship VARCHAR(25),
	Phone VARCHAR(25),
	CellPhone VARCHAR(25),
	WorkPhone VARCHAR(25),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonNextOfKinID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonNextOfKin', 'PersonID'
GO
--End table person.PersonNextOfKin

--Begin table person.PersonPasswordSecurity
DECLARE @TableName VARCHAR(250) = 'person.PersonPasswordSecurity'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonPasswordSecurity
	(
	PersonPasswordSecurityID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PasswordSecurityQuestionID INT,
	PasswordSecurityQuestionAnswer VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PasswordSecurityQuestionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPasswordSecurityID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPasswordSecurity', 'PersonID,PasswordSecurityQuestionID'
GO
--End table person.PersonPasswordSecurity

--Begin table person.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'person.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonPermissionable
	(
	PersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableLineage VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPermissionable', 'PersonID,PermissionableLineage'
GO
--End table person.PersonPermissionable

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProject
	(
	PersonProjectID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT,
	ProjectRoleID INT,
	ManagerName VARCHAR(100),
	ManagerEmailAddress VARCHAR(320),
	ClientTermOfReferenceID INT,
	ClientFunctionID INT,
	InsuranceTypeID INT,
	ISOCurrencyCode CHAR(3),
	StartDate DATE,
	EndDate DATE,
	AcceptedDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientFunctionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientTermOfReferenceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InsuranceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProject', 'PersonID,ProjectID,ClientTermOfReferenceID,ClientFunctionID'
GO
--End table person.PersonProject

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectExpense
	(
	PersonProjectExpenseID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ExpenseDate DATE,
	ClientCostCodeID BIT,
	ISOCurrencyCode CHAR(3),
	ExpenseAmount NUMERIC(18,2),
	TaxAmount NUMERIC(18,2),
	PaymentMethodID INT,
	IsProjectExpense BIT,
	OwnNotes VARCHAR(250),
	ProjectManagerNotes VARCHAR(250),
	IsInvoiced BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientCostCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInvoiced', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsProjectExpense', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMethodID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxAmount', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectExpenseID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectExpense', 'PersonProjectID'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectLocation
	(
	PersonProjectLocationID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ProjectLocationID INT,
	Days INT,
	HoursPerDay INT,
	Rate NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'Days', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursPerDay', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Rate', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectLocation', 'PersonProjectID,ProjectLocationID'
GO
--End table person.PersonProjectLocation

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProjectTime
	(
	PersonProjectTimeID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ProjectLocationID INT,
	HasDPA BIT,
	DSAAmount NUMERIC(18,2),
	DateWorked DATE,
	HoursWorked INT,
	ApplyVAT BIT,
	OwnNotes VARCHAR(250),
	ProjectManagerNotes VARCHAR(250),
	IsInvoiced BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplyVAT', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasDPA', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInvoiced', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLocationID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectTimeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectTime', 'PersonProjectID,ProjectLocationID'
GO
--End table person.PersonProjectTime

--Begin table person.PersonProofOfLife
DECLARE @TableName VARCHAR(250) = 'person.PersonProofOfLife'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonProofOfLife
	(
	PersonProofOfLifeID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProofOfLifeQuestion VARCHAR(500),
	ProofOfLifeAnswer VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProofOfLifeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProofOfLife', 'PersonID'
GO
--End table person.PersonProofOfLife