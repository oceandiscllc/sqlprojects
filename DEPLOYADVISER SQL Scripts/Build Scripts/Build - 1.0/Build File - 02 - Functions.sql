/* Build File - 02 - Functions */
USE DeployAdviser
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function document.FormatFileSize
EXEC utility.DropObject 'document.FormatFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatFileSize
(
@FileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @FileSize < 1000
		THEN CAST(@FileSize as VARCHAR(50)) + ' b'
		WHEN @FileSize < 1000000
		THEN CAST(ROUND((@FileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @FileSize < 1000000000
		THEN CAST(ROUND((@FileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @FileSize < 1000000000000
		THEN CAST(ROUND((@FileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatFileSize

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function eventlog.GetDocumentEntityXML
EXEC utility.DropObject 'eventlog.GetDocumentEntityXML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return document data from an EntitytypeCode and an EntityID
-- ======================================================================================
CREATE FUNCTION eventlog.GetDocumentEntityXML
(
@cEntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cEntityDocuments VARCHAR(MAX) = ''
	
	SELECT @cEntityDocuments = COALESCE(@cEntityDocuments, '') + D.EntityDocument
	FROM
		(
		SELECT
			(SELECT DE.DocumentID FOR XML RAW(''), ELEMENTS) AS EntityDocument
		FROM document.DocumentEntity DE
		WHERE DE.EntityID = @EntityID
			AND DE.EntityTypeCode = @cEntityTypeCode
		) D

	RETURN '<Documents>' + ISNULL(@cEntityDocuments, '') + '</Documents>'

END
GO
--End function eventlog.GetDocumentEntityXML

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode IN ('add', 'create')
			THEN 'Add'
			WHEN @EventCode = 'addupdate'
			THEN 'Update'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode IN ('read', 'view')
			THEN 'View'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE @EventCodeName
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	DECLARE @cSuffix VARCHAR(50)
	DECLARE @cTitle VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cSuffix = ISNULL(P.Suffix, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format LIKE '%FirstLast%'
		BEGIN
			
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @Format LIKE '%Suffix'
			SET @cRetVal += @cSuffix
		--ENDIF
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cRetVal + @cTitle
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.GetPersonProjectDays
EXEC utility.DropObject 'person.GetPersonProjectDays'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to get a data from a person.PersonProject record
-- ========================================================================

CREATE FUNCTION person.GetPersonProjectDays
(
@PersonProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nPersonProjectDays INT

	SELECT @nPersonProjectDays = SUM(PPL.Days)
	FROM person.PersonProjectLocation PPL
	WHERE PPL.PersonProjectID = @PersonProjectID

	RETURN ISNULL(@nPersonProjectDays, 0)

END
GO
--End function person.GetPersonProjectDays

--Begin function person.GetPersonProjectStatus
EXEC utility.DropObject 'person.GetPersonProjectStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to get a data from a person.PersonProject record
-- ========================================================================

CREATE FUNCTION person.GetPersonProjectStatus
(
@PersonProjectID INT,
@Mode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cPersonProjectStatus VARCHAR(250)
	DECLARE @cPersonProjectStatusIcon VARCHAR(200)
	DECLARE @cPersonProjectStatusName VARCHAR(50)

	SELECT 
		@cPersonProjectStatusIcon = 
			CASE
				WHEN PP.EndDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Assignment Expired"></a>'
				WHEN PP.AcceptedDate IS NULL
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-warning m-r-10" title="Assignment Pending"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-success m-r-10" title="Assignment Active"></a>'
			END,

		@cPersonProjectStatusName = 
			CASE
				WHEN PP.EndDate < getDate()
				THEN 'Expired'
				WHEN PP.AcceptedDate IS NULL
				THEN 'Pending'
				ELSE 'Active'
			END
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	IF @Mode = 'Icon'
		SET @cPersonProjectStatus = @cPersonProjectStatusIcon
	ELSE IF @Mode = 'Name'
		SET @cPersonProjectStatus = @cPersonProjectStatusName
	ELSE
		SET @cPersonProjectStatus = @cPersonProjectStatusIcon + ' ' + @cPersonProjectStatusName
	--ENDIF

	RETURN @cPersonProjectStatus

END
GO
--End function person.GetPersonProjectStatus

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = P.Password,
		@cPasswordSalt = P.PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword