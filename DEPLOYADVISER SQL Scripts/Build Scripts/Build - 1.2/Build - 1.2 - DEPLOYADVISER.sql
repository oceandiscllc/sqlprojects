-- File Name:	Build - 1.2 - DEPLOYADVISER.sql
-- Build Key:	Build - 1.2 - 2017.11.09 15.10.49

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Schemas:
--		import
--
-- Tables:
--		client.ProjectLaborCode
--		import.Person
--		invoice.ExchangeRate
--		invoice.Invoice
--		invoice.InvoiceExpenseLog
--		invoice.InvoicePerDiemLog
--		invoice.InvoiceTimeLog
--		person.PersonUnavailability
--		project.ProjectTermOfReference
--
-- Functions:
--		invoice.GetExchangeRate
--		invoice.GetExchangeRateDate
--		person.GetProjectsByPersonID
--		workflow.IsPersonInCurrentWorkflowStep
--
-- Procedures:
--		client.GetClientByClientID
--		core.MenuItemAddUpdate
--		eventlog.LogClientAction
--		eventlog.LogProjectAction
--		invoice.GetInvoiceByInvoiceID
--		invoice.GetInvoiceExpenseLogByInvoiceID
--		invoice.GetInvoicePerDiemLogByInvoiceID
--		invoice.GetInvoicePerDiemLogSummaryByInvoiceID
--		invoice.GetInvoicePreviewData
--		invoice.GetInvoiceReviewData
--		invoice.GetInvoiceTimeLogByInvoiceID
--		invoice.GetInvoiceTimeLogSummaryByInvoiceID
--		invoice.GetInvoiceTotalsByInvoiceID
--		invoice.GetMissingExchangeRateData
--		invoice.GetPersonAccountByInvoiceID
--		invoice.InitializeInvoiceDataByInvoiceID
--		invoice.SubmitInvoice
--		person.AcceptPersonProjectByPersonProjectID
--		person.ApplyPermissionableTemplate
--		person.CheckAccess
--		person.ExpirePersonProjectByPersonProjectIDList
--		person.GetPendingActionsByPersonID
--		person.GetPersonAccountByPersonAccountID
--		person.GetPersonByPersonID
--		person.GetPersonImportCount
--		person.GetPersonProjectByPersonProjectID
--		person.GetPersonProjectEmailTemplateDataByPersonProjectID
--		person.GetPersonProjectExpenseByPersonProjectExpenseID
--		person.GetPersonProjectsByPersonID
--		person.GetPersonProjectTimeByPersonProjectTimeID
--		person.GetPersonUnavailabilityByPersonID
--		person.ResolvePendingAction
--		person.SavePersonUnavailabilityByPersonID
--		project.GetProjectByProjectID
--		project.GetProjects
--		project.ValidateHoursWorked
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
/* Build File - 00 - Prerequisites */
--USE DeployAdviser
GO

--Begin schemas
EXEC utility.AddSchema 'import'
GO
--End schemas

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'ClientCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode1', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode2', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode3', 'VARCHAR(50)'
GO
--End table client.Client

--Begin table client.ProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'client.ProjectLaborCode'

EXEC utility.DropObject @TableName

CREATE TABLE client.ProjectLaborCode
	(
	ProjectLaborCodeID INT IDENTITY(0,1) NOT NULL,
	ClientID INT,
	ProjectLaborCode VARCHAR(50),
	ProjectLaborCodeName VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectLaborCodeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProjectLaborCode', 'ProjectLaborCodeName,ProjectLaborCode', 'ProjectLaborCodeID'
GO
--End table client.ProjectLaborCode

--Begin table client.ClientTermOfReference
DECLARE @TableName VARCHAR(250) = 'client.ClientTermOfReference'

EXEC utility.DropObject @TableName
GO
--End table client.ClientTermOfReference

--Begin table import.Person
DECLARE @TableName VARCHAR(250) = 'import.Person'

EXEC utility.DropObject @TableName

CREATE TABLE import.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ClientID INT,
	ClientPersonRoleCodeList VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table import.Person

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.Invoice
	(
	InvoiceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT,
	StartDate DATE,
	EndDate DATE,
	InvoiceDateTime DATETIME,
	InvoiceAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	PersonAccountID INT,
	PersonInvoiceNumber VARCHAR(15),
	TaxRate NUMERIC(18,4),
	ExchangeRateXML VARCHAR(MAX),
	InvoiceRejectionReasonID INT,
	InvoiceStatus VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvoiceAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceRejectionReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceStatus', 'VARCHAR(50)', 'Submitted'
EXEC utility.SetDefaultConstraint @TableName, 'PersonAccountID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18,4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InvoiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Invoice', 'PersonID,ProjectID', 'InvoiceID'
GO
--End table invoice.Invoice

--Begin table invoice.ExchangeRate
DECLARE @TableName VARCHAR(250) = 'invoice.ExchangeRate'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'invoice.InvoiceExchangeRate'

CREATE TABLE invoice.ExchangeRate
	(
	ExchangeRateID INT NOT NULL IDENTITY(1,1),
	ISOCurrencyCodeFrom CHAR(3),
	ISOCurrencyCodeTo CHAR(3),
	ExchangeRate NUMERIC(18,5),
	ExchangeRateDate DATE,
	IsFinalRate BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFinalRate', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ExchangeRateID'
EXEC utility.SetIndexClustered @TableName, 'IX_ExchangeRate', 'ISOCurrencyCodeFrom,ISOCurrencyCodeTo,ExchangeRateDate'
GO
--End table invoice.ExchangeRate

--Begin table invoice.InvoiceExpenseLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoiceExpenseLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoiceExpenseLog
	(
	InvoiceExpenseLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	ExpenseDate DATE,
	ClientCostCodeName VARCHAR(50),
	ExpenseAmount NUMERIC(18,2),
	TaxAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3), 
	ExchangeRate NUMERIC(18,5),
	DocumentGUID VARCHAR(50),
	ProjectManagerNotes VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxAmount', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoiceExpenseLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoiceExpenseLog', 'InvoiceID,ExpenseDate'
GO
--End table invoice.InvoiceExpenseLog

--Begin table invoice.InvoicePerDiemLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoicePerDiemLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoicePerDiemLog
	(
	InvoicePerDiemLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	DateWorked DATE,
	ProjectLocationName VARCHAR(100),
	DPAAmount NUMERIC(18,2),
	DSAAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	ExchangeRate NUMERIC(18,5)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DPAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoicePerDiemLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoicePerDiemLog', 'InvoiceID,DateWorked'
GO
--End table invoice.InvoicePerDiemLog

--Begin table invoice.InvoiceTimeLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoiceTimeLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoiceTimeLog
	(
	InvoiceTimeLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	DateWorked DATE,
	ProjectLocationName VARCHAR(100),
	HoursPerDay NUMERIC(18,2),
	HoursWorked NUMERIC(18,2),
	FeeRate NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	ExchangeRate NUMERIC(18,5),
	ProjectLaborCode VARCHAR(50),
	ProjectManagerNotes VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FeeRate', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursPerDay', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoiceTimeLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoiceTimeLog', 'InvoiceID,DateWorked'
GO
--End table invoice.InvoiceTimeLog

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.AddColumn @TableName, 'PermissionableTemplateCode', 'VARCHAR(50)'
GO
--End table permissionable.PermissionableTemplate

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'HasAccptedTerms', 'BIT', '0'
EXEC utility.DropColumn @TableName, 'NickName'
GO

EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username and manage import data
-- =========================================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nCount INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') + 'XXX' AS LastName
		FROM INSERTED I
		WHERE I.UserName IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM person.Person P
				WHERE P.UserName = I.UserName
					AND P.PersonID <> I.PersonID
				)

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cUserName = UPPER(LEFT(@cLastName, 3))

		SELECT @nCount = COUNT(P.UserName) + 1
		FROM person.Person P
		WHERE LEFT(P.UserName, 3) = @cUserName

		SET @cUserName += RIGHT('000' + CAST(@nCount AS VARCHAR(10)), 3)

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.AddColumn @TableName, 'IntermediateAccountPayee', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TerminalAccountPayee', 'VARCHAR(50)'
GO
--End table person.PersonAccount

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'ProjectTermOfReferenceID', 'INT', '0'

EXEC utility.DropIndex @TableName, 'IX_PersonProject'
EXEC utility.DropColumn @TableName, 'ClientTermOfReferenceID'

EXEC utility.SetIndexClustered @TableName, 'IX_PersonProject', 'PersonID,ProjectID,ProjectTermOfReferenceID,ClientFunctionID'
GO
--End table person.PersonProject

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.AddColumn @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropColumn @TableName, 'HoursPerDay'
GO
--End table person.PersonProjectLocation

--Begin table person.PersonProjectTime
ALTER TABLE person.PersonProjectTime DROP CONSTRAINT DF_PersonProjectTime_HoursWorked
GO

ALTER TABLE person.PersonProjectTime ALTER COLUMN HoursWorked NUMERIC(18,2)
GO

DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'NUMERIC(18,2)', '0'

EXEC utility.AddColumn @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
GO
--End table person.PersonProjectTime

--Begin table person.PersonUnavailability
DECLARE @TableName VARCHAR(250) = 'person.PersonUnavailability'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonUnavailability
	(
	PersonUnavailabilityID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonUnavailabilityDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonUnavailabilityID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonUnavailability', 'PersonID,PersonUnavailabilityDate'
GO
--End table person.PersonUnavailability

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'HoursPerDay', 'NUMERIC(18,2)', '8'
GO
--End table project.Project

--Begin table project.ProjectTermOfReference
DECLARE @TableName VARCHAR(250) = 'project.ProjectTermOfReference'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectTermOfReference
	(
	ProjectTermOfReferenceID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	ProjectTermOfReferenceName VARCHAR(50),
	ProjectTermOfReferenceDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTermOfReferenceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTermOfReference', 'ProjectID,ProjectTermOfReferenceName'
GO
--End table project.ProjectTermOfReference


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO

--Begin function invoice.GetExchangeRate
EXEC utility.DropObject 'invoice.GetExchangeRate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2017.11.05
-- Description:	A function to return an exchange rate
-- ==================================================

CREATE FUNCTION invoice.GetExchangeRate
(
@ISOCurrencyCodeFrom CHAR(3),
@ISOCurrencyCodeTo CHAR(3),
@SourceDate DATE
)

RETURNS NUMERIC(18,5)

AS
BEGIN

	DECLARE @ExchangeRate NUMERIC(18,5) = 1

	IF @ISOCurrencyCodeFrom <> @ISOCurrencyCodeTo
		BEGIN

		SELECT @ExchangeRate = ER.ExchangeRate
		FROM invoice.ExchangeRate ER
		WHERE ER.ISOCurrencyCodeFrom = @ISOCurrencyCodeFrom
			AND ER.ISOCurrencyCodeTo = @ISOCurrencyCodeTo
			AND ER.ExchangeRateDate = invoice.GetExchangeRateDate(@SourceDate)

		END
	--ENDIF

	RETURN ISNULL(@ExchangeRate, 1)
	
END
GO
--End function invoice.GetExchangeRate

--Begin function invoice.GetExchangeRateDate
EXEC utility.DropObject 'invoice.GetExchangeRateDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.05
-- Description:	A function to return the exchange rate date to use based on a date
-- ===============================================================================

CREATE FUNCTION invoice.GetExchangeRateDate
(
@SourceDate DATE
)

RETURNS DATE

AS
BEGIN

	DECLARE @ExchangeRateDate DATE

	SELECT 
		@ExchangeRateDate = 
			CASE 
				WHEN DATEPART(WEEKDAY, @SourceDate) > 5 
				THEN DATEADD(DAY, 4, DATEADD(WEEK, DATEDIFF(WEEK, 0, @SourceDate), 0)) 
				ELSE DATEADD(DAY, -3, DATEADD(WEEK, DATEDIFF(WEEK, 0, @SourceDate), 0)) 
			END

	RETURN FORMAT(@ExchangeRateDate, 'yyyy-MM-dd')
	
END
GO
--End function invoice.GetExchangeRateDate

--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID)
	SELECT 
		PP.ProjectID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID
	FROM project.ProjectPerson PP
	WHERE PP.ProjectPersonRoleCode = 'ProjectManager'
		AND PP.PersonID = @PersonID

	UNION

	SELECT 
		P.ProjectID
	FROM project.Project P
	WHERE EXISTS	
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.PersonID = @PersonID
		)

	UNION

	SELECT 
		PJ.ProjectID
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.28
-- Description:	A function to determine if a PeronID is in the current workflow step of a a specific EntityTypeCode and EntityID
-- =============================================================================================================================
CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
			AND EWSGP.PersonID = @PersonID
		)
		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep
	
END
GO
--End function workflow.IsPersonInCurrentWorkflowStep

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN

			UPDATE MI
			SET 
				MI.Icon = CASE WHEN @Icon IS NOT NULL THEN CASE WHEN LEN(RTRIM(@Icon)) = 0 THEN NULL ELSE @Icon END ELSE MI.Icon END,
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab,
				MI.MenuItemLink = CASE WHEN LEN(RTRIM(@NewMenuItemLink)) = 0 THEN NULL ELSE CASE WHEN @NewMenuItemLink IS NOT NULL THEN @NewMenuItemLink ELSE MI.MenuItemLink END END,
				MI.MenuItemText = CASE WHEN @NewMenuItemText IS NOT NULL THEN @NewMenuItemText ELSE MI.MenuItemText END,
				MI.ParentMenuItemID = CASE WHEN @nOldParentMenuItemID <> @nParentMenuItemID THEN @nParentMenuItemID ELSE MI.ParentMenuItemID END
			FROM core.MenuItem MI 
			WHERE MI.MenuItemID = @nNewMenuItemID

			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		IF @AfterMenuItemCode IS NOT NULL OR @BeforeMenuItemCode IS NOT NULL
			BEGIN

			DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
		
			DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
				SELECT
					MI.MenuItemID,
					MI.MenuItemCode
				FROM core.MenuItem MI
				WHERE MI.ParentMenuItemID = @nParentMenuItemID
					AND MI.MenuItemID <> @nNewMenuItemID
				ORDER BY MI.DisplayOrder
		
			OPEN oCursor
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
			WHILE @@fetch_status = 0
				BEGIN
		
				IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
					BEGIN
					
					INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
					SET @nIsInserted = 1
					
					END
				--ENDIF
		
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
		
				IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
					BEGIN
					
					INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
					SET @nIsInserted = 1
					
					END
				--ENDIF
				
				FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		
				END
			--END WHILE
			
			CLOSE oCursor
			DEALLOCATE oCursor	
		
			UPDATE MI
			SET MI.DisplayOrder = T1.DisplayOrder
			FROM core.MenuItem MI
				JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID

			END
		--ENDIF
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cClientCostCodes VARCHAR(MAX) = ''
		
		SELECT @cClientCostCodes = COALESCE(@cClientCostCodes, '') + D.ClientCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientCostCode'), ELEMENTS) AS ClientCostCode
			FROM client.ClientCostCode T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientFunctions VARCHAR(MAX) = ''
		
		SELECT @cClientFunctions = COALESCE(@cClientFunctions, '') + D.ClientFunction
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientFunction'), ELEMENTS) AS ClientFunction
			FROM client.ClientFunction T 
			WHERE T.ClientID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cClientCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Functions>' + ISNULL(@cClientFunctions, '') + '</Functions>' AS XML)
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cProjectCostCodes VARCHAR(MAX) = ''
		
		SELECT @cProjectCostCodes = COALESCE(@cProjectCostCodes, '') + D.ProjectCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCostCode'), ELEMENTS) AS ProjectCostCode
			FROM project.ProjectCostCode T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectCurrencies VARCHAR(MAX) = ''
		
		SELECT @cProjectCurrencies = COALESCE(@cProjectCurrencies, '') + D.ProjectCurrency
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCurrency'), ELEMENTS) AS ProjectCurrency
			FROM project.ProjectCurrency T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cProjectLocations = COALESCE(@cProjectLocations, '') + D.ProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectLocation'), ELEMENTS) AS ProjectLocation
			FROM project.ProjectLocation T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectTermOfReference VARCHAR(MAX) = ''
		
		SELECT @cProjectTermOfReference = COALESCE(@cProjectTermOfReference, '') + D.ProjectTermOfReference
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectTermOfReference'), ELEMENTS) AS ProjectTermOfReference
			FROM project.ProjectTermOfReference T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cProjectCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Currencies>' + ISNULL(@cProjectCurrencies, '') + '</Currencies>' AS XML),
			CAST('<Locations>' + ISNULL(@cProjectLocations, '') + '</Locations>' AS XML),
			CAST('<TermsOfReference>' + ISNULL(@cProjectTermOfReference, '') + '</TermsOfReference>' AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2),
		DSAAmount NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2),
		HoursWorked NUMERIC(18,2),
		FeeRate NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PPE.ISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DSAAmount, DPAAmount, ISOCurrencyCode, ExchangeRate)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PPT.DSAAmount,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate
	FROM person.Person PN, project.Project PJ
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
	WHERE PN.PersonID = @PersonID
		AND PJ.ProjectID = @ProjectID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		TPL.DPAAmount,
		CAST(TPL.DPAAmount * TPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		CAST(TPL.DSAAmount * TPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TPL.ExchangeRate
	FROM @tPerDiemLog TPL
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		SUM(TPL.DPAAmount) AS DPAAmount,
		CAST(SUM(TPL.DPAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DSAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS DPACount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		TPL.ISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS DSACount
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DSAAmount, TPL.ISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TEL.ExpenseAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TEL.TaxAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	EXEC person.GetPersonUnavailabilityByPersonID @PersonID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceReviewData
EXEC utility.DropObject 'invoice.GetInvoiceReviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoiceReviewData

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	EXEC invoice.InitializeInvoiceDataByInvoiceID @InvoiceID

	--InvoiceData
	EXEC invoice.GetInvoiceByInvoiceID @InvoiceID

	--InvoiceExpenseLog
	EXEC invoice.GetInvoiceExpenseLogByInvoiceID @InvoiceID

	--InvoicePerDiemLog
	EXEC invoice.GetInvoicePerDiemLogByInvoiceID @InvoiceID

	--InvoicePerDiemLogSummary
	EXEC invoice.GetInvoicePerDiemLogSummaryByInvoiceID @InvoiceID

	--InvoiceTimeLog
	EXEC invoice.GetInvoiceTimeLogByInvoiceID @InvoiceID

	--InvoiceTimeLogSummary
	EXEC invoice.GetInvoiceTimeLogSummaryByInvoiceID @InvoiceID

	--InvoiceTotals
	EXEC invoice.GetInvoiceTotalsByInvoiceID @InvoiceID

	--InvoiceWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID

	--InvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID

END
GO
--End procedure invoice.GetInvoiceReviewData

-- ======================================= --
-- Begin Invoice Review & Print procedures --
-- ======================================= --

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoiceExchangeRateByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExchangeRateByInvoiceID'
GO
--End procedure invoice.GetInvoiceExchangeRateByInvoiceID

--Begin procedure invoice.GetInvoiceExpenseLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceExpenseLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLog
	SELECT
		core.FormatDate(IEL.ExpenseDate) AS ExpenseDateFormatted,
		IEL.ClientCostCodeName,
		IEL.ExpenseAmount,
		IEL.TaxAmount,
		IEL.ISOCurrencyCode, 
		IEL.ExchangeRate,
		CAST(IEL.ExchangeRate * IEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(IEL.ExchangeRate * IEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN IEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + IEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		IEL.ProjectManagerNotes
	FROM invoice.InvoiceExpenseLog IEL
	WHERE IEL.InvoiceID = @InvoiceID
	ORDER BY IEL.ExpenseDate

END
GO
--End procedure invoice.GetInvoiceExpenseLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoicePerDiemLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		core.FormatDate(IPL.DateWorked) AS DateWorkedFormatted,
		IPL.ProjectLocationName,
		C.FinanceCode2 AS DPACode,
		IPL.DPAAmount,
		CAST(IPL.DPAAmount * IPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		C.FinanceCode3 AS DSACode,
		IPL.DSAAmount,
		CAST(IPL.DSAAmount * IPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		IPL.ISOCurrencyCode, 
		IPL.ExchangeRate
	FROM invoice.InvoicePerDiemLog IPL
		JOIN invoice.Invoice I ON I.InvoiceID = IPL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND IPL.InvoiceID = @InvoiceID
	ORDER BY IPL.DateWorked

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoicePerDiemLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLogSummary
	SELECT 
		IPL.ProjectLocationName,
		C.FinanceCode2 AS DPACode,
		SUM(IPL.DPAAmount) AS DPAAmount,
		CAST(SUM(IPL.DPAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(IPL1.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL1 WHERE IPL1.InvoiceID = IPL.InvoiceID AND IPL1.DPAAmount > 0 AND IPL1.ProjectLocationName = IPL.ProjectLocationName) AS DPACount,
		C.FinanceCode3 AS DSACode,
		IPL.DSAAmount,
		IPL.ISOCurrencyCode,
		CAST(SUM(IPL.DSAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(IPL2.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL2 WHERE IPL2.InvoiceID = IPL.InvoiceID AND IPL2.DSAAmount > 0 AND IPL2.ProjectLocationName = IPL.ProjectLocationName) AS DSACount
	FROM invoice.InvoicePerDiemLog IPL
		JOIN invoice.Invoice I ON I.InvoiceID = IPL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND IPL.InvoiceID = @InvoiceID
	GROUP BY IPL.ProjectLocationName, IPL.DSAAmount, IPL.ISOCurrencyCode, IPL.InvoiceID, C.FinanceCode2, C.FinanceCode3
	ORDER BY IPL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceTimeLog table
-- =================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLog
	SELECT 
		C.FinanceCode1 + CASE WHEN ITL.ProjectLaborCode IS NULL THEN '' ELSE '.' + ITL.ProjectLaborCode END AS LaborCode,
		core.FormatDate(ITL.DateWorked) AS DateWorkedFormatted,
		ITL.ProjectLocationName,
		ITL.FeeRate,
		ITL.HoursWorked,
		CAST(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		ITL.ISOCurrencyCode,
		ITL.ExchangeRate,
		ITL.ProjectManagerNotes
	FROM invoice.InvoiceTimeLog ITL
		JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND ITL.InvoiceID = @InvoiceID
	ORDER BY ITL.DateWorked

END
GO
--End procedure invoice.GetInvoiceTimeLogByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceTimeLog table
-- =================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLogSummary
	SELECT 
		ITL.ProjectLocationName,
		ITL.FeeRate AS FeeRate,
		SUM(ITL.HoursWorked) AS HoursWorked,
		CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM invoice.InvoiceTimeLog ITL
		JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND ITL.InvoiceID = @InvoiceID
	GROUP BY ITL.ProjectLocationName, ITL.FeeRate, C.FinanceCode1
	ORDER BY ITL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoiceTotalsByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTotalsByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from varions invoice.Invoice% tables
-- ================================================================================
CREATE PROCEDURE invoice.GetInvoiceTotalsByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM invoice.InvoiceTimeLog ITL
			JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
				AND ITL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(IPL.DSAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM invoice.InvoicePerDiemLog IPL
		WHERE IPL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(IPL.DPAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM invoice.InvoicePerDiemLog IPL
		WHERE IPL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(IEL.ExpenseAmount * IEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(IEL.TaxAmount * IEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM invoice.InvoiceExpenseLog IEL
		WHERE IEL.InvoiceID = @InvoiceID
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

END
GO
--End procedure invoice.GetInvoiceTotalsByInvoiceID

--Begin procedure invoice.GetPersonAccountByInvoiceID
EXEC utility.DropObject 'invoice.GetPersonAccountByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.02
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE invoice.GetPersonAccountByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
		JOIN invoice.Invoice I ON I.PersonAccountID = PA.PersonAccountID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetPersonAccountByInvoiceID

--Begin procedure invoice.InitializeInvoiceDataByInvoiceID
EXEC utility.DropObject 'invoice.InitializeInvoiceDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to initialize invoice data for review or printing
-- =================================================================================
CREATE PROCEDURE invoice.InitializeInvoiceDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	DELETE IEL FROM invoice.InvoiceExpenseLog IEL WHERE IEL.InvoiceID = @InvoiceID
	DELETE IPL FROM invoice.InvoicePerDiemLog IPL WHERE IPL.InvoiceID = @InvoiceID
	DELETE ITL FROM invoice.InvoiceTimeLog ITL WHERE ITL.InvoiceID = @InvoiceID

	INSERT INTO invoice.InvoiceExpenseLog
		(InvoiceID, ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, ProjectManagerNotes)
	SELECT
		@InvoiceID,
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate, 
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO invoice.InvoicePerDiemLog
		(InvoiceID, DateWorked, ProjectLocationName, DPAAmount, DSAAmount, ISOCurrencyCode, ExchangeRate)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PPT.DSAAmount,
		PP.ISOCurrencyCode,
		PPT.Exchangerate
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO invoice.InvoiceTimeLog
		(InvoiceID, DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectManagerNotes)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.ExchangeRate,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

END
GO
--End procedure invoice.InitializeInvoiceDataByInvoiceID

-- ===================================== --
-- End Invoice Review & Print procedures --
-- ===================================== --

--Begin procedure invoice.GetMissingExchangeRateData
EXEC utility.DropObject 'invoice.GetProjectISOCurrencyCodes'
EXEC utility.DropObject 'invoice.GetMissingExchangeRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return any exchange rate data needed by an invoice but not present in the invoice.ExchangeRate table
-- =======================================================================================================================================
CREATE PROCEDURE invoice.GetMissingExchangeRateData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cISOCurrencyCodeFrom CHAR(3)

	SELECT
		@cISOCurrencyCodeFrom = PP.ISOCurrencyCode
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID
		AND PP.ProjectID = @ProjectID

	--MissingExchangeRateData
	SELECT DISTINCT
		@cISOCurrencyCodeFrom AS ISOCurrencyCodeFrom,
		PPE.ISOCurrencyCode	AS ISOCurrencyCodeTo,
		Invoice.GetExchangeRateDate(PPE.ExpenseDate) AS ExchangeRateDate
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			AND PPE.IsProjectExpense = 1
			AND PPE.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.ISOCurrencyCode <> @cISOCurrencyCodeFrom
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
						WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
						)
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeFrom = @cISOCurrencyCodeFrom
					AND ER.ISOCurrencyCodeTo = PPE.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)
	ORDER BY ExchangeRateDate, PPE.ISOCurrencyCode

	--ProjectExchangeRateData
	SELECT 
		@cISOCurrencyCodeFrom AS ISOCurrencyCodeFrom,
		core.GetSystemSetupValueBySystemSetupKey('OANDAAPIKey', '') AS OANDAAPIKey

END
GO
--End procedure invoice.GetMissingExchangeRateData

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0,
@PersonUnavailabilityDateList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cInvoiceISOCurrencyCode CHAR(3)
	DECLARE @nClientID INT

	SELECT 
		@cInvoiceISOCurrencyCode = I.ISOCurrencyCode,
		@nClientID = P.ClientID 
	FROM project.Project P 
		JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID 
			AND I.InvoiceID = @InvoiceID

	UPDATE PPE
	SET 
		PPE.ExchangeRate = 
			CASE
				WHEN PPE.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(@cInvoiceISOCurrencyCode, PPE.ISOCurrencyCode, PPE.ExpenseDate)
			END,

		PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET 
		PPT.ExchangeRate = 
			CASE
				WHEN PP.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(@cInvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked)
			END,

		PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	;

	EXEC person.SavePersonUnavailabilityByPersonID @PersonID, @PersonUnavailabilityDateList

	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure person.AcceptPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.AcceptPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.AcceptPersonProjectByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nPersonID INT = (SELECT PP.PersonID FROM person.PersonProject PP WHERE PP.PersonProjectID = @PersonProjectID)

	UPDATE PP
	SET PP.AcceptedDate = getDate()
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	EXEC eventlog.LogPersonProjectAction @PersonProjectID, 'update', @nPersonID, 'Assignment Accepted'

	EXEC person.GetPersonProjectEmailTemplateDataByPersonProjectID @PersonProjectID

	SELECT
		PP.ManagerEmailAddress AS EmailAddress
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID
		AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND PP2.PersonProjectID = @PersonProjectID
			AND P.EmailAddress IS NOT NULL

	ORDER BY 1

END
GO
--End procedure person.AcceptPersonProjectByPersonProjectID

--Begin procedure person.ApplyPermissionableTemplate
EXEC utility.DropObject 'person.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE person.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @nPersonID INT
	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM core.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PTP.PermissionableLineage
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM person.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			O.PersonID
		FROM @tOutput O
		
	OPEN oCursor
	FETCH oCursor INTO @nPersonID
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC eventlog.LogPersonAction @nPersonID, 'Update', @PersonID, 'Bulk permissionables assignment'

		FETCH oCursor INTO @nPersonID
		
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor		

END
GO
--End procedure person.ApplyPermissionableTemplate

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE T.ClientID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND EXISTS 
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 

					UNION 

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1
					)
				AND (@AccessCode <> 'AddUpdate' OR (T.EndDate >= getDate() AND T.AcceptedDate IS NULL))

		END
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF workflow.IsPersonInCurrentWorkflowStep(@EntityTypeCode, @EntityID, @PersonID) = 1 OR (@AccessCode <> 'View.Review' AND EXISTS (SELECT 1 FROM invoice.Invoice I WHERE I.InvoiceID = @EntityID AND I.PersonID = @PersonID))
			SET @bHasAccess = 1
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM project.Project T WHERE T.ProjectID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.ExpirePersonProjectByPersonProjectIDList
EXEC utility.DropObject 'person.ExpirePersonProjectByPersonProjectIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.ExpirePersonProjectByPersonProjectIDList

@PersonProjectIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PP
	SET PP.EndDate = getDate()
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	EXEC eventlog.LogPersonProjectAction 0, 'update', @PersonID, 'Assignment Expired', @PersonProjectIDList

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PP.PersonProjectID,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	SELECT
		P.EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND P.EmailAddress IS NOT NULL

	UNION

	SELECT
		PP.ManagerEmailAddress AS EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress,
		PP2.PersonProjectID
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP2.PersonProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND P.EmailAddress IS NOT NULL

	ORDER BY 2, 1

END
GO
--End procedure person.ExpirePersonProjectByPersonProjectIDList

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button type="button" class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Accept</button>',
		'<button type="button" class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two next of kin records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND PP.AcceptedDate IS NULL
				AND PP.EndDate >= getDate()
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Accept a project engagement',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/projects">Go</a>'
			)

		END
	--ENDIF

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT 
		T.PendingAction, 
		T.Link1, 
		T.Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HasAccptedTerms,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonAccountByPersonAccountID
EXEC utility.DropObject 'person.GetPersonAccountByPersonAccountID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE person.GetPersonAccountByPersonAccountID

@PersonAccountID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonAccountID = @PersonAccountID

END
GO
--End procedure person.GetPersonAccountByPersonAccountID

--Begin procedure person.GetPersonImportCount
EXEC utility.DropObject 'person.GetPersonImportCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return a count of records in the import.Person table
-- =======================================================================================
CREATE PROCEDURE person.GetPersonImportCount

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS ItemCount
	FROM import.Person P

END
GO
--End procedure person.GetPersonImportCount

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		IsActive BIT,
		ProjectLocationID INT,
		ProjectLocationName VARCHAR(100),
		Days INT NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			0 AS FeeRate,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectTermOfReferenceID,
			'' AS ProjectTermOfReferenceName,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectLaborCodeID,
			'' AS ProjectLaborCodeName,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
		WHERE P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		T.IsActive,
		T.ProjectLocationID,
		T.ProjectLocationName,
		T.Days
	FROM @tPersonProjectLocation T
	ORDER BY T.ProjectLocationName, T.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectEmailTemplateDataByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectEmailTemplateDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.04
-- Description:	A stored procedure to get the person project data for an email template
-- ====================================================================================
CREATE PROCEDURE person.GetPersonProjectEmailTemplateDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		P.ProjectName,
		(SELECT PN.EmailAddress FROM person.Person PN WHERE PN.PersonID = PP.PersonID) AS EmailAddress,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			AND PP.PersonProjectID = @PersonProjectID

END
GO
--End procedure person.GetPersonProjectEmailTemplateDataByPersonProjectID

--Begin procedure person.GetPersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.GetPersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return data from the person.PersonProjectExpense table
-- =========================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectID INT = ISNULL((SELECT PPE.PersonProjectID FROM person.PersonProjectExpense PPE WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID), 0)

	--PersonProjectExpense
	SELECT
		C.CurrencyName,
		C.ISOCurrencyCode,
		CF.ClientFunctionName,
		P.ProjectName,
		PM.PaymentMethodID, 
		PM.PaymentMethodName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPE.ClientCostCodeID,
		project.GetProjectCostCodeDescriptionByClientCostCodeID(PPE.ClientCostCodeID) AS ProjectCostCodeDescription,
		PPE.ExpenseAmount,
		PPE.ExpenseDate,
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		PPE.IsProjectExpense,
		PPE.OwnNotes,
		PPE.PersonProjectExpenseID,
		PPE.PersonProjectID,
		PPE.ProjectManagerNotes,
		PPE.TaxAmount,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PPE.ISOCurrencyCode
		JOIN dropdown.PaymentMethod PM ON PM.PaymentMethodID = PPE.PaymentMethodID
			AND PPE.PersonProjectExpenseID = @PersonProjectExpenseID

	--PersonProjectExpenseClientCostCode & PersonProjectExpenseProjectCurrency
	EXEC person.GetPersonProjectExpenseDataByPersonProjectID @nPersonProjectID

	--PersonProjectExpenseDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'PersonProjectExpense' 
			AND DE.EntityID = @PersonProjectExpenseID

END
GO
--End procedure person.GetPersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.GetPersonProjectsByPersonID
EXEC utility.DropObject 'person.GetPersonProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.PersonProjectID,
		CF.ClientFunctionName,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectID, PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID, CF.ClientFunctionName, CF.ClientFunctionID

END
GO
--End procedure person.GetPersonProjectsByPersonID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.GetPersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.GetPersonUnavailabilityByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.30
-- Description:	A stored procedure to get data from the person.PersonUnavailability table
-- ======================================================================================
CREATE PROCEDURE person.GetPersonUnavailabilityByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--PersonUnavailability
	SELECT
		FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS PersonUnavailabilityDate
	FROM person.PersonUnavailability PU
	WHERE PU.PersonID = @PersonID
	ORDER BY PU.PersonUnavailabilityDate

END
GO
--End procedure person.GetPersonUnavailabilityByPersonID

--Begin procedure person.ResolvePendingAction
EXEC utility.DropObject 'person.ResolvePendingAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.31
-- Description:	A stored procedure to manage data in the client.ClientPerson table
-- ===============================================================================
CREATE PROCEDURE person.ResolvePendingAction

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = 'AcceptClientPerson'
		UPDATE CP SET CP.AcceptedDate = getDate() FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	ELSE
		DELETE CP FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	--ENDIF

END
GO
--End procedure person.ResolvePendingAction

--Begin procedure person.SavePersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.SavePersonUnavailabilityByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.30
-- Description:	A stored procedure to save data to the person.PersonUnavailability table
-- =====================================================================================
CREATE PROCEDURE person.SavePersonUnavailabilityByPersonID

@PersonID INT = 0,
@PersonUnavailabilityDateList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PU
	FROM person.PersonUnavailability PU
	WHERE PU.PersonID = @PersonID

	IF @PersonUnavailabilityDateList IS NOT NULL
		BEGIN

		INSERT INTO person.PersonUnavailability
			(PersonID, PersonUnavailabilityDate)
		SELECT
			@PersonID,
			CAST(LTT.ListItem AS DATE)
		FROM core.ListToTable(@PersonUnavailabilityDateList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure person.SavePersonUnavailabilityByPersonID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.GetProjects
EXEC utility.DropObject 'project.GetProjects'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the project.Project table
-- =============================================================================
CREATE PROCEDURE project.GetProjects

@PersonID INT,
@IsActive INT = -1,
@ClientPersonRoleCodeList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.ClientName,
		P.ProjectID,
		P.ProjectName
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND (@IsActive = -1 OR P.IsActive = @IsActive)
			AND EXISTS
				(
				SELECT 1
				FROM client.ClientPerson CP
				WHERE CP.ClientID = P.ClientID
					AND CP.PersonID = @PersonID
					AND 
						(
						@ClientPersonRoleCodeList IS NULL
							OR EXISTS
								(
								SELECT 1
								FROM core.ListToTable(@ClientPersonRoleCodeList, ',') LTT
								WHERE LTT.ListItem = CP.ClientPersonRoleCode
								)
						)

				UNION

				SELECT 1
				FROM person.Person P
				WHERE P.PersonID = @PersonID
					AND P.IsSuperAdministrator = 1
				)
	ORDER BY C.ClientName, P.ProjectName

END
GO
--End procedure project.GetProjects

--Begin procedure project.ValidateHoursWorked
EXEC utility.DropObject 'project.ValidateHoursWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.25
-- Description:	A stored procedure to validate HoursWorked from a PersonProjectID
-- ==============================================================================
CREATE PROCEDURE project.ValidateHoursWorked

@HoursWorked INT,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectTimeAllotted INT
	DECLARE @nPersonProjectTimeExpended INT

	SELECT
		@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PL.ProjectID
			AND PPL.PersonProjectID = @PersonProjectID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID

	SELECT
		CASE
			WHEN ISNULL(@nPersonProjectTimeAllotted, 0) >= ISNULL(@nPersonProjectTimeExpended, 0) + @HoursWorked
			THEN 1
			ELSE 0
		END AS IsValid

END
GO
--End procedure project.ValidateHoursWorked
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data - Client.sql
--USE DeployAdviser
GO

--Begin table client.ProjectLaborCode
TRUNCATE TABLE client.ProjectLaborCode
GO

IF NOT EXISTS (SELECT 1 FROM client.Client C WHERE C.ClientName = 'HSOT')
	INSERT INTO client.Client (C.ClientName) VALUES ('HSOT')
--ENDIF

EXEC utility.InsertIdentityValue 'client.ProjectLaborCode', 'ProjectLaborCodeID', 0
GO

DECLARE @nClientID INT = (SELECT C.ClientID from client.Client C WHERE C.ClientName = 'HSOT')

INSERT INTO client.ProjectLaborCode
	(ClientID,ProjectLaborCode,ProjectLaborCodeName)
VALUES
	(@nClientID, '9ADUA0', 'Humanitarian Advisor UK A0'),
	(@nClientID, '9ADUA5', 'Humanitarian Advisor UK A5'),
	(@nClientID, '9ADUA7', 'Humanitarian Advisor UK A7'),
	(@nClientID, '9ADUAX', 'Humanitarian Advisor UK AX'),
	(@nClientID, '9ADUB0', 'Humanitarian Advisor UK B0'),
	(@nClientID, '9ADUB5', 'Humanitarian Advisor UK B5'),
	(@nClientID, '9ADUB7', 'Humanitarian Advisor UK B7'),
	(@nClientID, '9ADUBX', 'Humanitarian Advisor UK BX'),
	(@nClientID, '9ADUC0', 'Humanitarian Advisor UK C0'),
	(@nClientID, '9ADUC5', 'Humanitarian Advisor UK C5'),
	(@nClientID, '9ADUC7', 'Humanitarian Advisor UK C7'),
	(@nClientID, '9ADUCX', 'Humanitarian Advisor UK CX'),
	(@nClientID, '9ADUD0', 'Humanitarian Advisor UK D0'),
	(@nClientID, '9ADUD5', 'Humanitarian Advisor UK D5'),
	(@nClientID, '9ADUD7', 'Humanitarian Advisor UK D7'),
	(@nClientID, '9ADUDX', 'Humanitarian Advisor UK DX'),
	(@nClientID, '9ADUE0', 'Humanitarian Advisor UK E0'),
	(@nClientID, '9ADUE5', 'Humanitarian Advisor UK E5'),
	(@nClientID, '9ADUE7', 'Humanitarian Advisor UK E7'),
	(@nClientID, '9ADUEX', 'Humanitarian Advisor UK EX'),
	(@nClientID, '9AOIA0', 'Human Affairs Officer Int A0'),
	(@nClientID, '9AOIA5', 'Human Affairs Officer Int A5'),
	(@nClientID, '9AOIA7', 'Human Affairs Officer Int A7'),
	(@nClientID, '9AOIAX', 'Human Affairs Officer Int AX'),
	(@nClientID, '9AOIB0', 'Human Affairs Officer Int B0'),
	(@nClientID, '9AOIB5', 'Human Affairs Officer Int B5'),
	(@nClientID, '9AOIB7', 'Human Affairs Officer Int B7'),
	(@nClientID, '9AOIBX', 'Human Affairs Officer Int BX'),
	(@nClientID, '9AOIC0', 'Human Affairs Officer Int C0'),
	(@nClientID, '9AOIC5', 'Human Affairs Officer Int C5'),
	(@nClientID, '9AOIC7', 'Human Affairs Officer Int C7'),
	(@nClientID, '9AOICX', 'Human Affairs Officer Int CX'),
	(@nClientID, '9AOID0', 'Human Affairs Officer Int D0'),
	(@nClientID, '9AOID5', 'Human Affairs Officer Int D5'),
	(@nClientID, '9AOID7', 'Human Affairs Officer Int D7'),
	(@nClientID, '9AOIDX', 'Human Affairs Officer Int DX'),
	(@nClientID, '9AOIE0', 'Human Affairs Officer Int E0'),
	(@nClientID, '9AOIE5', 'Human Affairs Officer Int E5'),
	(@nClientID, '9AOIE7', 'Human Affairs Officer Int E7'),
	(@nClientID, '9AOIEX', 'Human Affairs Officer Int EX'),
	(@nClientID, '9AOUA0', 'Human Affairs Officer UK A0'),
	(@nClientID, '9AOUA5', 'Human Affairs Officer UK A5'),
	(@nClientID, '9AOUA7', 'Human Affairs Officer UK A7'),
	(@nClientID, '9AOUAX', 'Human Affairs Officer UK AX'),
	(@nClientID, '9AOUB0', 'Human Affairs Officer UK B0'),
	(@nClientID, '9AOUB5', 'Human Affairs Officer UK B5'),
	(@nClientID, '9AOUB7', 'Human Affairs Officer UK B7'),
	(@nClientID, '9AOUBX', 'Human Affairs Officer UK BX'),
	(@nClientID, '9AOUC0', 'Human Affairs Officer UK C0'),
	(@nClientID, '9AOUC5', 'Human Affairs Officer UK C5'),
	(@nClientID, '9AOUC7', 'Human Affairs Officer UK C7'),
	(@nClientID, '9AOUCX', 'Human Affairs Officer UK CX'),
	(@nClientID, '9AOUD0', 'Human Affairs Officer UK D0'),
	(@nClientID, '9AOUD5', 'Human Affairs Officer UK D5'),
	(@nClientID, '9AOUD7', 'Human Affairs Officer UK D7'),
	(@nClientID, '9AOUDX', 'Human Affairs Officer UK DX'),
	(@nClientID, '9AOUE0', 'Human Affairs Officer UK E0'),
	(@nClientID, '9AOUE5', 'Human Affairs Officer UK E5'),
	(@nClientID, '9AOUE7', 'Human Affairs Officer UK E7'),
	(@nClientID, '9AOUEX', 'Human Affairs Officer UK EX'),
	(@nClientID, '9ASIA0', 'Advisor Specialist Int A0'),
	(@nClientID, '9ASIA5', 'Advisor Specialist Int A5'),
	(@nClientID, '9ASIA7', 'Advisor Specialist Int A7'),
	(@nClientID, '9ASIAX', 'Advisor Specialist Int AX'),
	(@nClientID, '9ASIB0', 'Advisor Specialist Int B0'),
	(@nClientID, '9ASIB5', 'Advisor Specialist Int B5'),
	(@nClientID, '9ASIB7', 'Advisor Specialist Int B7'),
	(@nClientID, '9ASIBX', 'Advisor Specialist Int BX'),
	(@nClientID, '9ASIC0', 'Advisor Specialist Int C0'),
	(@nClientID, '9ASIC5', 'Advisor Specialist Int C5'),
	(@nClientID, '9ASIC7', 'Advisor Specialist Int C7'),
	(@nClientID, '9ASICX', 'Advisor Specialist Int CX'),
	(@nClientID, '9ASID0', 'Advisor Specialist Int D0'),
	(@nClientID, '9ASID5', 'Advisor Specialist Int D5'),
	(@nClientID, '9ASID7', 'Advisor Specialist Int D7'),
	(@nClientID, '9ASIDX', 'Advisor Specialist Int DX'),
	(@nClientID, '9ASIE0', 'Advisor Specialist Int E0'),
	(@nClientID, '9ASIE5', 'Advisor Specialist Int E5'),
	(@nClientID, '9ASIE7', 'Advisor Specialist Int E7'),
	(@nClientID, '9ASIEX', 'Advisor Specialist Int EX'),
	(@nClientID, '9CAIA0', 'Clinical Admin Int A0'),
	(@nClientID, '9CAIA5', 'Clinical Admin Int A5'),
	(@nClientID, '9CAIA7', 'Clinical Admin Int A7'),
	(@nClientID, '9CAIAX', 'Clinical Admin Int AX'),
	(@nClientID, '9CAIB0', 'Clinical Admin Int B0'),
	(@nClientID, '9CAIB5', 'Clinical Admin Int B5'),
	(@nClientID, '9CAIB7', 'Clinical Admin Int B7'),
	(@nClientID, '9CAIBX', 'Clinical Admin Int BX'),
	(@nClientID, '9CAIC0', 'Clinical Admin Int C0'),
	(@nClientID, '9CAIC5', 'Clinical Admin Int C5'),
	(@nClientID, '9CAIC7', 'Clinical Admin Int C7'),
	(@nClientID, '9CAICX', 'Clinical Admin Int CX'),
	(@nClientID, '9CAID0', 'Clinical Admin Int D0'),
	(@nClientID, '9CAID5', 'Clinical Admin Int D5'),
	(@nClientID, '9CAID7', 'Clinical Admin Int D7'),
	(@nClientID, '9CAIDX', 'Clinical Admin Int DX'),
	(@nClientID, '9CAIE0', 'Clinical Admin Int E0'),
	(@nClientID, '9CAIE5', 'Clinical Admin Int E5'),
	(@nClientID, '9CAIE7', 'Clinical Admin Int E7'),
	(@nClientID, '9CAIEX', 'Clinical Admin Int EX'),
	(@nClientID, '9CSIA0', 'Clinical Specialist Int A0'),
	(@nClientID, '9CSIA5', 'Clinical Specialist Int A5'),
	(@nClientID, '9CSIA7', 'Clinical Specialist Int A7'),
	(@nClientID, '9CSIAX', 'Clinical Specialist Int AX'),
	(@nClientID, '9CSIB0', 'Clinical Specialist Int B0'),
	(@nClientID, '9CSIB5', 'Clinical Specialist Int B5'),
	(@nClientID, '9CSIB7', 'Clinical Specialist Int B7'),
	(@nClientID, '9CSIBX', 'Clinical Specialist Int BX'),
	(@nClientID, '9CSIC0', 'Clinical Specialist Int C0'),
	(@nClientID, '9CSIC5', 'Clinical Specialist Int C5'),
	(@nClientID, '9CSIC7', 'Clinical Specialist Int C7'),
	(@nClientID, '9CSICX', 'Clinical Specialist Int CX'),
	(@nClientID, '9CSID0', 'Clinical Specialist Int D0'),
	(@nClientID, '9CSID5', 'Clinical Specialist Int D5'),
	(@nClientID, '9CSID7', 'Clinical Specialist Int D7'),
	(@nClientID, '9CSIDX', 'Clinical Specialist Int DX'),
	(@nClientID, '9CSIE0', 'Clinical Specialist Int E0'),
	(@nClientID, '9CSIE5', 'Clinical Specialist Int E5'),
	(@nClientID, '9CSIE7', 'Clinical Specialist Int E7'),
	(@nClientID, '9CSIEX', 'Clinical Specialist Int EX'),
	(@nClientID, '9DSUA0', 'Deployments Specialist UK A0'),
	(@nClientID, '9DSUA5', 'Deployments Specialist UK A5'),
	(@nClientID, '9DSUA7', 'Deployments Specialist UK A7'),
	(@nClientID, '9DSUAX', 'Deployments Specialist UK AX'),
	(@nClientID, '9DSUB0', 'Deployments Specialist UK B0'),
	(@nClientID, '9DSUB5', 'Deployments Specialist UK B5'),
	(@nClientID, '9DSUB7', 'Deployments Specialist UK B7'),
	(@nClientID, '9DSUBX', 'Deployments Specialist UK BX'),
	(@nClientID, '9DSUC0', 'Deployments Specialist UK C0'),
	(@nClientID, '9DSUC5', 'Deployments Specialist UK C5'),
	(@nClientID, '9DSUC7', 'Deployments Specialist UK C7'),
	(@nClientID, '9DSUCX', 'Deployments Specialist UK CX'),
	(@nClientID, '9DSUD0', 'Deployments Specialist UK D0'),
	(@nClientID, '9DSUD5', 'Deployments Specialist UK D5'),
	(@nClientID, '9DSUD7', 'Deployments Specialist UK D7'),
	(@nClientID, '9DSUDX', 'Deployments Specialist UK DX'),
	(@nClientID, '9DSUE0', 'Deployments Specialist UK E0'),
	(@nClientID, '9DSUE5', 'Deployments Specialist UK E5'),
	(@nClientID, '9DSUE7', 'Deployments Specialist UK E7'),
	(@nClientID, '9DSUEX', 'Deployments Specialist UK EX'),
	(@nClientID, '9DTLA0', 'Logistics Dpty Team Leader A0'),
	(@nClientID, '9DTLA5', 'Logistics Dpty Team Leader A5'),
	(@nClientID, '9DTLA7', 'Logistics Dpty Team Leader A7'),
	(@nClientID, '9DTLAX', 'Logistics Dpty Team Leader AX'),
	(@nClientID, '9DTLB0', 'Logistics Dpty Team Leader B0'),
	(@nClientID, '9DTLB5', 'Logistics Dpty Team Leader B5'),
	(@nClientID, '9DTLB7', 'Logistics Dpty Team Leader B7'),
	(@nClientID, '9DTLBX', 'Logistics Dpty Team Leader BX'),
	(@nClientID, '9DTLC0', 'Logistics Dpty Team Leader C0'),
	(@nClientID, '9DTLC5', 'Logistics Dpty Team Leader C5'),
	(@nClientID, '9DTLC7', 'Logistics Dpty Team Leader C7'),
	(@nClientID, '9DTLCX', 'Logistics Dpty Team Leader CX'),
	(@nClientID, '9DTLD0', 'Logistics Dpty Team Leader D0'),
	(@nClientID, '9DTLD5', 'Logistics Dpty Team Leader D5'),
	(@nClientID, '9DTLD7', 'Logistics Dpty Team Leader D7'),
	(@nClientID, '9DTLDX', 'Logistics Dpty Team Leader DX'),
	(@nClientID, '9DTLE0', 'Logistics Dpty Team Leader E0'),
	(@nClientID, '9DTLE5', 'Logistics Dpty Team Leader E5'),
	(@nClientID, '9DTLE7', 'Logistics Dpty Team Leader E7'),
	(@nClientID, '9DTLEX', 'Logistics Dpty Team Leader EX'),
	(@nClientID, '9DTMA0', 'Deputy Team Leader A0'),
	(@nClientID, '9DTMA5', 'Deputy Team Leader A5'),
	(@nClientID, '9DTMA7', 'Deputy Team Leader A7'),
	(@nClientID, '9DTMAX', 'Deputy Team Leader AX'),
	(@nClientID, '9DTMB0', 'Deputy Team Leader B0'),
	(@nClientID, '9DTMB5', 'Deputy Team Leader B5'),
	(@nClientID, '9DTMB7', 'Deputy Team Leader B7'),
	(@nClientID, '9DTMBX', 'Deputy Team Leader BX'),
	(@nClientID, '9DTMC0', 'Deputy Team Leader C0'),
	(@nClientID, '9DTMC5', 'Deputy Team Leader C5'),
	(@nClientID, '9DTMC7', 'Deputy Team Leader C7'),
	(@nClientID, '9DTMCX', 'Deputy Team Leader CX'),
	(@nClientID, '9DTMD0', 'Deputy Team Leader D0'),
	(@nClientID, '9DTMD5', 'Deputy Team Leader D5'),
	(@nClientID, '9DTMD7', 'Deputy Team Leader D7'),
	(@nClientID, '9DTMDX', 'Deputy Team Leader DX'),
	(@nClientID, '9DTME0', 'Deputy Team Leader E0'),
	(@nClientID, '9DTME5', 'Deputy Team Leader E5'),
	(@nClientID, '9DTME7', 'Deputy Team Leader E7'),
	(@nClientID, '9DTMEX', 'Deputy Team Leader EX'),
	(@nClientID, '9FLMA0', 'Fleet Manager A0'),
	(@nClientID, '9FLMA5', 'Fleet Manager A5'),
	(@nClientID, '9FLMA7', 'Fleet Manager A7'),
	(@nClientID, '9FLMAX', 'Fleet Manager AX'),
	(@nClientID, '9FLMB0', 'Fleet Manager B0'),
	(@nClientID, '9FLMB5', 'Fleet Manager B5'),
	(@nClientID, '9FLMB7', 'Fleet Manager B7'),
	(@nClientID, '9FLMBX', 'Fleet Manager BX'),
	(@nClientID, '9FLMC0', 'Fleet Manager C0'),
	(@nClientID, '9FLMC5', 'Fleet Manager C5'),
	(@nClientID, '9FLMC7', 'Fleet Manager C7'),
	(@nClientID, '9FLMCX', 'Fleet Manager CX'),
	(@nClientID, '9FLMD0', 'Fleet Manager D0'),
	(@nClientID, '9FLMD5', 'Fleet Manager D5'),
	(@nClientID, '9FLMD7', 'Fleet Manager D7'),
	(@nClientID, '9FLMDX', 'Fleet Manager DX'),
	(@nClientID, '9FLME0', 'Fleet Manager E0'),
	(@nClientID, '9FLME5', 'Fleet Manager E5'),
	(@nClientID, '9FLME7', 'Fleet Manager E7'),
	(@nClientID, '9FLMEX', 'Fleet Manager EX'),
	(@nClientID, '9FSIA0', 'Finance Ops Specialist Int A0'),
	(@nClientID, '9FSIA5', 'Finance Ops Specialist Int A5'),
	(@nClientID, '9FSIA7', 'Finance Ops Specialist Int A7'),
	(@nClientID, '9FSIAX', 'Finance Ops Specialist Int AX'),
	(@nClientID, '9FSIB0', 'Finance Ops Specialist Int B0'),
	(@nClientID, '9FSIB5', 'Finance Ops Specialist Int B5'),
	(@nClientID, '9FSIB7', 'Finance Ops Specialist Int B7'),
	(@nClientID, '9FSIBX', 'Finance Ops Specialist Int BX'),
	(@nClientID, '9FSIC0', 'Finance Ops Specialist Int C0'),
	(@nClientID, '9FSIC5', 'Finance Ops Specialist Int C5'),
	(@nClientID, '9FSIC7', 'Finance Ops Specialist Int C7'),
	(@nClientID, '9FSICX', 'Finance Ops Specialist Int CX'),
	(@nClientID, '9FSID0', 'Finance Ops Specialist Int D0'),
	(@nClientID, '9FSID5', 'Finance Ops Specialist Int D5'),
	(@nClientID, '9FSID7', 'Finance Ops Specialist Int D7'),
	(@nClientID, '9FSIDX', 'Finance Ops Specialist Int DX'),
	(@nClientID, '9FSIE0', 'Finance Ops Specialist Int E0'),
	(@nClientID, '9FSIE5', 'Finance Ops Specialist Int E5'),
	(@nClientID, '9FSIE7', 'Finance Ops Specialist Int E7'),
	(@nClientID, '9FSIEX', 'Finance Ops Specialist Int EX'),
	(@nClientID, '9FSUA0', 'Finance Ops Specialist UK A0'),
	(@nClientID, '9FSUA5', 'Finance Ops Specialist UK A5'),
	(@nClientID, '9FSUA7', 'Finance Ops Specialist UK A7'),
	(@nClientID, '9FSUAX', 'Finance Ops Specialist UK AX'),
	(@nClientID, '9FSUB0', 'Finance Ops Specialist UK B0'),
	(@nClientID, '9FSUB5', 'Finance Ops Specialist UK B5'),
	(@nClientID, '9FSUB7', 'Finance Ops Specialist UK B7'),
	(@nClientID, '9FSUBX', 'Finance Ops Specialist UK BX'),
	(@nClientID, '9FSUC0', 'Finance Ops Specialist UK C0'),
	(@nClientID, '9FSUC5', 'Finance Ops Specialist UK C5'),
	(@nClientID, '9FSUC7', 'Finance Ops Specialist UK C7'),
	(@nClientID, '9FSUCX', 'Finance Ops Specialist UK CX'),
	(@nClientID, '9FSUD0', 'Finance Ops Specialist UK D0'),
	(@nClientID, '9FSUD5', 'Finance Ops Specialist UK D5'),
	(@nClientID, '9FSUD7', 'Finance Ops Specialist UK D7'),
	(@nClientID, '9FSUDX', 'Finance Ops Specialist UK DX'),
	(@nClientID, '9FSUE0', 'Finance Ops Specialist UK E0'),
	(@nClientID, '9FSUE5', 'Finance Ops Specialist UK E5'),
	(@nClientID, '9FSUE7', 'Finance Ops Specialist UK E7'),
	(@nClientID, '9FSUEX', 'Finance Ops Specialist UK EX'),
	(@nClientID, '9HAIA0', 'Health Ops Admin Int A0'),
	(@nClientID, '9HAIA5', 'Health Ops Admin Int A5'),
	(@nClientID, '9HAIA7', 'Health Ops Admin Int A7'),
	(@nClientID, '9HAIAX', 'Health Ops Admin Int AX'),
	(@nClientID, '9HAIB0', 'Health Ops Admin Int B0'),
	(@nClientID, '9HAIB5', 'Health Ops Admin Int B5'),
	(@nClientID, '9HAIB7', 'Health Ops Admin Int B7'),
	(@nClientID, '9HAIBX', 'Health Ops Admin Int BX'),
	(@nClientID, '9HAIC0', 'Health Ops Admin Int C0'),
	(@nClientID, '9HAIC5', 'Health Ops Admin Int C5'),
	(@nClientID, '9HAIC7', 'Health Ops Admin Int C7'),
	(@nClientID, '9HAICX', 'Health Ops Admin Int CX'),
	(@nClientID, '9HAID0', 'Health Ops Admin Int D0'),
	(@nClientID, '9HAID5', 'Health Ops Admin Int D5'),
	(@nClientID, '9HAID7', 'Health Ops Admin Int D7'),
	(@nClientID, '9HAIDX', 'Health Ops Admin Int DX'),
	(@nClientID, '9HAIE0', 'Health Ops Admin Int E0'),
	(@nClientID, '9HAIE5', 'Health Ops Admin Int E5'),
	(@nClientID, '9HAIE7', 'Health Ops Admin Int E7'),
	(@nClientID, '9HAIEX', 'Health Ops Admin Int EX'),
	(@nClientID, '9HSIA0', 'Health Ops Speciaist Int A0'),
	(@nClientID, '9HSIA5', 'Health Ops Speciaist Int A5'),
	(@nClientID, '9HSIA7', 'Health Ops Speciaist Int A7'),
	(@nClientID, '9HSIAX', 'Health Ops Speciaist Int AX'),
	(@nClientID, '9HSIB0', 'Health Ops Speciaist Int B0'),
	(@nClientID, '9HSIB5', 'Health Ops Speciaist Int B5'),
	(@nClientID, '9HSIB7', 'Health Ops Speciaist Int B7'),
	(@nClientID, '9HSIBX', 'Health Ops Speciaist Int BX'),
	(@nClientID, '9HSIC0', 'Health Ops Speciaist Int C0'),
	(@nClientID, '9HSIC5', 'Health Ops Speciaist Int C5'),
	(@nClientID, '9HSIC7', 'Health Ops Speciaist Int C7'),
	(@nClientID, '9HSICX', 'Health Ops Speciaist Int CX'),
	(@nClientID, '9HSID0', 'Health Ops Speciaist Int D0'),
	(@nClientID, '9HSID5', 'Health Ops Speciaist Int D5'),
	(@nClientID, '9HSID7', 'Health Ops Speciaist Int D7'),
	(@nClientID, '9HSIDX', 'Health Ops Speciaist Int DX'),
	(@nClientID, '9HSIE0', 'Health Ops Speciaist Int E0'),
	(@nClientID, '9HSIE5', 'Health Ops Speciaist Int E5'),
	(@nClientID, '9HSIE7', 'Health Ops Speciaist Int E7'),
	(@nClientID, '9HSIEX', 'Health Ops Speciaist Int EX'),
	(@nClientID, '9IMIA0', 'Info Mgmt Officer Int A0'),
	(@nClientID, '9IMIA5', 'Info Mgmt Officer Int A5'),
	(@nClientID, '9IMIA7', 'Info Mgmt Officer Int A7'),
	(@nClientID, '9IMIAX', 'Info Mgmt Officer Int AX'),
	(@nClientID, '9IMIB0', 'Info Mgmt Officer Int B0'),
	(@nClientID, '9IMIB5', 'Info Mgmt Officer Int B5'),
	(@nClientID, '9IMIB7', 'Info Mgmt Officer Int B7'),
	(@nClientID, '9IMIBX', 'Info Mgmt Officer Int BX'),
	(@nClientID, '9IMIC0', 'Info Mgmt Officer Int C0'),
	(@nClientID, '9IMIC5', 'Info Mgmt Officer Int C5'),
	(@nClientID, '9IMIC7', 'Info Mgmt Officer Int C7'),
	(@nClientID, '9IMICX', 'Info Mgmt Officer Int CX'),
	(@nClientID, '9IMID0', 'Info Mgmt Officer Int D0'),
	(@nClientID, '9IMID5', 'Info Mgmt Officer Int D5'),
	(@nClientID, '9IMID7', 'Info Mgmt Officer Int D7'),
	(@nClientID, '9IMIDX', 'Info Mgmt Officer Int DX'),
	(@nClientID, '9IMIE0', 'Info Mgmt Officer Int E0'),
	(@nClientID, '9IMIE5', 'Info Mgmt Officer Int E5'),
	(@nClientID, '9IMIE7', 'Info Mgmt Officer Int E7'),
	(@nClientID, '9IMIEX', 'Info Mgmt Officer Int EX'),
	(@nClientID, '9LAIA0', 'Logistics Admin Int A0'),
	(@nClientID, '9LAIA5', 'Logistics Admin Int A5'),
	(@nClientID, '9LAIA7', 'Logistics Admin Int A7'),
	(@nClientID, '9LAIAX', 'Logistics Admin Int AX'),
	(@nClientID, '9LAIB0', 'Logistics Admin Int B0'),
	(@nClientID, '9LAIB5', 'Logistics Admin Int B5'),
	(@nClientID, '9LAIB7', 'Logistics Admin Int B7'),
	(@nClientID, '9LAIBX', 'Logistics Admin Int BX'),
	(@nClientID, '9LAIC0', 'Logistics Admin Int C0'),
	(@nClientID, '9LAIC5', 'Logistics Admin Int C5'),
	(@nClientID, '9LAIC7', 'Logistics Admin Int C7'),
	(@nClientID, '9LAICX', 'Logistics Admin Int CX'),
	(@nClientID, '9LAID0', 'Logistics Admin Int D0'),
	(@nClientID, '9LAID5', 'Logistics Admin Int D5'),
	(@nClientID, '9LAID7', 'Logistics Admin Int D7'),
	(@nClientID, '9LAIDX', 'Logistics Admin Int DX'),
	(@nClientID, '9LAIE0', 'Logistics Admin Int E0'),
	(@nClientID, '9LAIE5', 'Logistics Admin Int E5'),
	(@nClientID, '9LAIE7', 'Logistics Admin Int E7'),
	(@nClientID, '9LAIEX', 'Logistics Admin Int EX'),
	(@nClientID, '9LAUA0', 'Logistics Admin UK A0'),
	(@nClientID, '9LAUA5', 'Logistics Admin UK A5'),
	(@nClientID, '9LAUA7', 'Logistics Admin UK A7'),
	(@nClientID, '9LAUAX', 'Logistics Admin UK AX'),
	(@nClientID, '9LAUB0', 'Logistics Admin UK B0'),
	(@nClientID, '9LAUB5', 'Logistics Admin UK B5'),
	(@nClientID, '9LAUB7', 'Logistics Admin UK B7'),
	(@nClientID, '9LAUBX', 'Logistics Admin UK BX'),
	(@nClientID, '9LAUC0', 'Logistics Admin UK C0'),
	(@nClientID, '9LAUC5', 'Logistics Admin UK C5'),
	(@nClientID, '9LAUC7', 'Logistics Admin UK C7'),
	(@nClientID, '9LAUCX', 'Logistics Admin UK CX'),
	(@nClientID, '9LAUD0', 'Logistics Admin UK D0'),
	(@nClientID, '9LAUD5', 'Logistics Admin UK D5'),
	(@nClientID, '9LAUD7', 'Logistics Admin UK D7'),
	(@nClientID, '9LAUDX', 'Logistics Admin UK DX'),
	(@nClientID, '9LAUE0', 'Logistics Admin UK E0'),
	(@nClientID, '9LAUE5', 'Logistics Admin UK E5'),
	(@nClientID, '9LAUE7', 'Logistics Admin UK E7'),
	(@nClientID, '9LAUEX', 'Logistics Admin UK EX'),
	(@nClientID, '9LLIA0', 'Lesson Learning Advisor Int A0'),
	(@nClientID, '9LLIA5', 'Lesson Learning Advisor Int A5'),
	(@nClientID, '9LLIA7', 'Lesson Learning Advisor Int A7'),
	(@nClientID, '9LLIAX', 'Lesson Learning Advisor Int AX'),
	(@nClientID, '9LLIB0', 'Lesson Learning Advisor Int B0'),
	(@nClientID, '9LLIB5', 'Lesson Learning Advisor Int B5'),
	(@nClientID, '9LLIB7', 'Lesson Learning Advisor Int B7'),
	(@nClientID, '9LLIBX', 'Lesson Learning Advisor Int BX'),
	(@nClientID, '9LLIC0', 'Lesson Learning Advisor Int C0'),
	(@nClientID, '9LLIC5', 'Lesson Learning Advisor Int C5'),
	(@nClientID, '9LLIC7', 'Lesson Learning Advisor Int C7'),
	(@nClientID, '9LLICX', 'Lesson Learning Advisor Int CX'),
	(@nClientID, '9LLID0', 'Lesson Learning Advisor Int D0'),
	(@nClientID, '9LLID5', 'Lesson Learning Advisor Int D5'),
	(@nClientID, '9LLID7', 'Lesson Learning Advisor Int D7'),
	(@nClientID, '9LLIDX', 'Lesson Learning Advisor Int DX'),
	(@nClientID, '9LLIE0', 'Lesson Learning Advisor Int E0'),
	(@nClientID, '9LLIE5', 'Lesson Learning Advisor Int E5'),
	(@nClientID, '9LLIE7', 'Lesson Learning Advisor Int E7'),
	(@nClientID, '9LLIEX', 'Lesson Learning Advisor Int EX'),
	(@nClientID, '9LLUA0', 'Lessons Learning Officer UK A0'),
	(@nClientID, '9LLUA5', 'Lessons Learning Officer UK A5'),
	(@nClientID, '9LLUA7', 'Lessons Learning Officer UK A7'),
	(@nClientID, '9LLUAX', 'Lessons Learning Officer UK AX'),
	(@nClientID, '9LLUB0', 'Lessons Learning Officer UK B0'),
	(@nClientID, '9LLUB5', 'Lessons Learning Officer UK B5'),
	(@nClientID, '9LLUB7', 'Lessons Learning Officer UK B7'),
	(@nClientID, '9LLUBX', 'Lessons Learning Officer UK BX'),
	(@nClientID, '9LLUC0', 'Lessons Learning Officer UK C0'),
	(@nClientID, '9LLUC5', 'Lessons Learning Officer UK C5'),
	(@nClientID, '9LLUC7', 'Lessons Learning Officer UK C7'),
	(@nClientID, '9LLUCX', 'Lessons Learning Officer UK CX'),
	(@nClientID, '9LLUD0', 'Lessons Learning Officer UK D0'),
	(@nClientID, '9LLUD5', 'Lessons Learning Officer UK D5'),
	(@nClientID, '9LLUD7', 'Lessons Learning Officer UK D7'),
	(@nClientID, '9LLUDX', 'Lessons Learning Officer UK DX'),
	(@nClientID, '9LLUE0', 'Lessons Learning Officer UK E0'),
	(@nClientID, '9LLUE5', 'Lessons Learning Officer UK E5'),
	(@nClientID, '9LLUE7', 'Lessons Learning Officer UK E7'),
	(@nClientID, '9LLUEX', 'Lessons Learning Officer UK EX'),
	(@nClientID, '9LSIA0', 'Logistics Specialist Int A0'),
	(@nClientID, '9LSIA5', 'Logistics Specialist Int A5'),
	(@nClientID, '9LSIA7', 'Logistics Specialist Int A7'),
	(@nClientID, '9LSIAX', 'Logistics Specialist Int AX'),
	(@nClientID, '9LSIB0', 'Logistics Specialist Int B0'),
	(@nClientID, '9LSIB5', 'Logistics Specialist Int B5'),
	(@nClientID, '9LSIB7', 'Logistics Specialist Int B7'),
	(@nClientID, '9LSIBX', 'Logistics Specialist Int BX'),
	(@nClientID, '9LSIC0', 'Logistics Specialist Int C0'),
	(@nClientID, '9LSIC5', 'Logistics Specialist Int C5'),
	(@nClientID, '9LSIC7', 'Logistics Specialist Int C7'),
	(@nClientID, '9LSICX', 'Logistics Specialist Int CX'),
	(@nClientID, '9LSID0', 'Logistics Specialist Int D0'),
	(@nClientID, '9LSID5', 'Logistics Specialist Int D5'),
	(@nClientID, '9LSID7', 'Logistics Specialist Int D7'),
	(@nClientID, '9LSIDX', 'Logistics Specialist Int DX'),
	(@nClientID, '9LSIE0', 'Logistics Specialist Int E0'),
	(@nClientID, '9LSIE5', 'Logistics Specialist Int E5'),
	(@nClientID, '9LSIE7', 'Logistics Specialist Int E7'),
	(@nClientID, '9LSIEX', 'Logistics Specialist Int EX'),
	(@nClientID, '9LSUA0', 'Logistics Specialist UK A0'),
	(@nClientID, '9LSUA5', 'Logistics Specialist UK A5'),
	(@nClientID, '9LSUA7', 'Logistics Specialist UK A7'),
	(@nClientID, '9LSUAX', 'Logistics Specialist UK AX'),
	(@nClientID, '9LSUB0', 'Logistics Specialist UK B0'),
	(@nClientID, '9LSUB5', 'Logistics Specialist UK B5'),
	(@nClientID, '9LSUB7', 'Logistics Specialist UK B7'),
	(@nClientID, '9LSUBX', 'Logistics Specialist UK BX'),
	(@nClientID, '9LSUC0', 'Logistics Specialist UK C0'),
	(@nClientID, '9LSUC5', 'Logistics Specialist UK C5'),
	(@nClientID, '9LSUC7', 'Logistics Specialist UK C7'),
	(@nClientID, '9LSUCX', 'Logistics Specialist UK CX'),
	(@nClientID, '9LSUD0', 'Logistics Specialist UK D0'),
	(@nClientID, '9LSUD5', 'Logistics Specialist UK D5'),
	(@nClientID, '9LSUD7', 'Logistics Specialist UK D7'),
	(@nClientID, '9LSUDX', 'Logistics Specialist UK DX'),
	(@nClientID, '9LSUE0', 'Logistics Specialist UK E0'),
	(@nClientID, '9LSUE5', 'Logistics Specialist UK E5'),
	(@nClientID, '9LSUE7', 'Logistics Specialist UK E7'),
	(@nClientID, '9LSUEX', 'Logistics Specialist UK EX'),
	(@nClientID, '9LTIA0', 'Lead Trainer Facilitator Int A0'),
	(@nClientID, '9LTIA5', 'Lead Trainer Facilitator Int A5'),
	(@nClientID, '9LTIA7', 'Lead Trainer Facilitator Int A7'),
	(@nClientID, '9LTIAX', 'Lead Trainer Facilitator Int AX'),
	(@nClientID, '9LTIB0', 'Lead Trainer Facilitator Int B0'),
	(@nClientID, '9LTIB5', 'Lead Trainer Facilitator Int B5'),
	(@nClientID, '9LTIB7', 'Lead Trainer Facilitator Int B7'),
	(@nClientID, '9LTIBX', 'Lead Trainer Facilitator Int BX'),
	(@nClientID, '9LTIC0', 'Lead Trainer Facilitator Int C0'),
	(@nClientID, '9LTIC5', 'Lead Trainer Facilitator Int C5'),
	(@nClientID, '9LTIC7', 'Lead Trainer Facilitator Int C7'),
	(@nClientID, '9LTICX', 'Lead Trainer Facilitator Int CX'),
	(@nClientID, '9LTID0', 'Lead Trainer Facilitator Int D0'),
	(@nClientID, '9LTID5', 'Lead Trainer Facilitator Int D5'),
	(@nClientID, '9LTID7', 'Lead Trainer Facilitator Int D7'),
	(@nClientID, '9LTIDX', 'Lead Trainer Facilitator Int DX'),
	(@nClientID, '9LTIE0', 'Lead Trainer Facilitator Int E0'),
	(@nClientID, '9LTIE5', 'Lead Trainer Facilitator Int E5'),
	(@nClientID, '9LTIE7', 'Lead Trainer Facilitator Int E7'),
	(@nClientID, '9LTIEX', 'Lead Trainer Facilitator Int EX'),
	(@nClientID, '9LTUA0', 'Lead Trainer Facilitator UK A0'),
	(@nClientID, '9LTUA5', 'Lead Trainer Facilitator UK A5'),
	(@nClientID, '9LTUA7', 'Lead Trainer Facilitator UK A7'),
	(@nClientID, '9LTUAX', 'Lead Trainer Facilitator UK AX'),
	(@nClientID, '9LTUB0', 'Lead Trainer Facilitator UK B0'),
	(@nClientID, '9LTUB5', 'Lead Trainer Facilitator UK B5'),
	(@nClientID, '9LTUB7', 'Lead Trainer Facilitator UK B7'),
	(@nClientID, '9LTUBX', 'Lead Trainer Facilitator UK BX'),
	(@nClientID, '9LTUC0', 'Lead Trainer Facilitator UK C0'),
	(@nClientID, '9LTUC5', 'Lead Trainer Facilitator UK C5'),
	(@nClientID, '9LTUC7', 'Lead Trainer Facilitator UK C7'),
	(@nClientID, '9LTUCX', 'Lead Trainer Facilitator UK CX'),
	(@nClientID, '9LTUD0', 'Lead Trainer Facilitator UK D0'),
	(@nClientID, '9LTUD5', 'Lead Trainer Facilitator UK D5'),
	(@nClientID, '9LTUD7', 'Lead Trainer Facilitator UK D7'),
	(@nClientID, '9LTUDX', 'Lead Trainer Facilitator UK DX'),
	(@nClientID, '9LTUE0', 'Lead Trainer Facilitator UK E0'),
	(@nClientID, '9LTUE5', 'Lead Trainer Facilitator UK E5'),
	(@nClientID, '9LTUE7', 'Lead Trainer Facilitator UK E7'),
	(@nClientID, '9LTUEX', 'Lead Trainer Facilitator UK EX'),
	(@nClientID, '9OAIA0', 'Ops/Logistics Advisor Int A0'),
	(@nClientID, '9OAIA5', 'Ops/Logistics Advisor Int A5'),
	(@nClientID, '9OAIA7', 'Ops/Logistics Advisor Int A7'),
	(@nClientID, '9OAIAX', 'Ops/Logistics Advisor Int AX'),
	(@nClientID, '9OAIB0', 'Ops/Logistics Advisor Int B0'),
	(@nClientID, '9OAIB5', 'Ops/Logistics Advisor Int B5'),
	(@nClientID, '9OAIB7', 'Ops/Logistics Advisor Int B7'),
	(@nClientID, '9OAIBX', 'Ops/Logistics Advisor Int BX'),
	(@nClientID, '9OAIC0', 'Ops/Logistics Advisor Int C0'),
	(@nClientID, '9OAIC5', 'Ops/Logistics Advisor Int C5'),
	(@nClientID, '9OAIC7', 'Ops/Logistics Advisor Int C7'),
	(@nClientID, '9OAICX', 'Ops/Logistics Advisor Int CX'),
	(@nClientID, '9OAID0', 'Ops/Logistics Advisor Int D0'),
	(@nClientID, '9OAID5', 'Ops/Logistics Advisor Int D5'),
	(@nClientID, '9OAID7', 'Ops/Logistics Advisor Int D7'),
	(@nClientID, '9OAIDX', 'Ops/Logistics Advisor Int DX'),
	(@nClientID, '9OAIE0', 'Ops/Logistics Advisor Int E0'),
	(@nClientID, '9OAIE5', 'Ops/Logistics Advisor Int E5'),
	(@nClientID, '9OAIE7', 'Ops/Logistics Advisor Int E7'),
	(@nClientID, '9OAIEX', 'Ops/Logistics Advisor Int EX'),
	(@nClientID, '9OPMA0', 'Operations Manager A0'),
	(@nClientID, '9OPMA5', 'Operations Manager A5'),
	(@nClientID, '9OPMA7', 'Operations Manager A7'),
	(@nClientID, '9OPMAX', 'Operations Manager AX'),
	(@nClientID, '9OPMB0', 'Operations Manager B0'),
	(@nClientID, '9OPMB5', 'Operations Manager B5'),
	(@nClientID, '9OPMB7', 'Operations Manager B7'),
	(@nClientID, '9OPMBX', 'Operations Manager BX'),
	(@nClientID, '9OPMC0', 'Operations Manager C0'),
	(@nClientID, '9OPMC5', 'Operations Manager C5'),
	(@nClientID, '9OPMC7', 'Operations Manager C7'),
	(@nClientID, '9OPMCX', 'Operations Manager CX'),
	(@nClientID, '9OPMD0', 'Operations Manager D0'),
	(@nClientID, '9OPMD5', 'Operations Manager D5'),
	(@nClientID, '9OPMD7', 'Operations Manager D7'),
	(@nClientID, '9OPMDX', 'Operations Manager DX'),
	(@nClientID, '9OPME0', 'Operations Manager E0'),
	(@nClientID, '9OPME5', 'Operations Manager E5'),
	(@nClientID, '9OPME7', 'Operations Manager E7'),
	(@nClientID, '9OPMEX', 'Operations Manager EX'),
	(@nClientID, '9PLUA0', 'Procure & Logist Mgr UK A0'),
	(@nClientID, '9PLUA5', 'Procure & Logist Mgr UK A5'),
	(@nClientID, '9PLUA7', 'Procure & Logist Mgr UK A7'),
	(@nClientID, '9PLUAX', 'Procure & Logist Mgr UK AX'),
	(@nClientID, '9PLUB0', 'Procure & Logist Mgr UK B0'),
	(@nClientID, '9PLUB5', 'Procure & Logist Mgr UK B5'),
	(@nClientID, '9PLUB7', 'Procure & Logist Mgr UK B7'),
	(@nClientID, '9PLUBX', 'Procure & Logist Mgr UK BX'),
	(@nClientID, '9PLUC0', 'Procure & Logist Mgr UK C0'),
	(@nClientID, '9PLUC5', 'Procure & Logist Mgr UK C5'),
	(@nClientID, '9PLUC7', 'Procure & Logist Mgr UK C7'),
	(@nClientID, '9PLUCX', 'Procure & Logist Mgr UK CX'),
	(@nClientID, '9PLUD0', 'Procure & Logist Mgr UK D0'),
	(@nClientID, '9PLUD5', 'Procure & Logist Mgr UK D5'),
	(@nClientID, '9PLUD7', 'Procure & Logist Mgr UK D7'),
	(@nClientID, '9PLUDX', 'Procure & Logist Mgr UK DX'),
	(@nClientID, '9PLUE0', 'Procure & Logist Mgr UK E0'),
	(@nClientID, '9PLUE5', 'Procure & Logist Mgr UK E5'),
	(@nClientID, '9PLUE7', 'Procure & Logist Mgr UK E7'),
	(@nClientID, '9PLUEX', 'Procure & Logist Mgr UK EX'),
	(@nClientID, '9PMIA0', 'Programme Manager Int A0'),
	(@nClientID, '9PMIA5', 'Programme Manager Int A5'),
	(@nClientID, '9PMIA7', 'Programme Manager Int A7'),
	(@nClientID, '9PMIAX', 'Programme Manager Int AX'),
	(@nClientID, '9PMIB0', 'Programme Manager Int B0'),
	(@nClientID, '9PMIB5', 'Programme Manager Int B5'),
	(@nClientID, '9PMIB7', 'Programme Manager Int B7'),
	(@nClientID, '9PMIBX', 'Programme Manager Int BX'),
	(@nClientID, '9PMIC0', 'Programme Manager Int C0'),
	(@nClientID, '9PMIC5', 'Programme Manager Int C5'),
	(@nClientID, '9PMIC7', 'Programme Manager Int C7'),
	(@nClientID, '9PMICX', 'Programme Manager Int CX'),
	(@nClientID, '9PMID0', 'Programme Manager Int D0'),
	(@nClientID, '9PMID5', 'Programme Manager Int D5'),
	(@nClientID, '9PMID7', 'Programme Manager Int D7'),
	(@nClientID, '9PMIDX', 'Programme Manager Int DX'),
	(@nClientID, '9PMIE0', 'Programme Manager Int E0'),
	(@nClientID, '9PMIE5', 'Programme Manager Int E5'),
	(@nClientID, '9PMIE7', 'Programme Manager Int E7'),
	(@nClientID, '9PMIEX', 'Programme Manager Int EX'),
	(@nClientID, '9PMUA0', 'Programme Manager UK A0'),
	(@nClientID, '9PMUA5', 'Programme Manager UK A5'),
	(@nClientID, '9PMUA7', 'Programme Manager UK A7'),
	(@nClientID, '9PMUAX', 'Programme Manager UK AX'),
	(@nClientID, '9PMUB0', 'Programme Manager UK B0'),
	(@nClientID, '9PMUB5', 'Programme Manager UK B5'),
	(@nClientID, '9PMUB7', 'Programme Manager UK B7'),
	(@nClientID, '9PMUBX', 'Programme Manager UK BX'),
	(@nClientID, '9PMUC0', 'Programme Manager UK C0'),
	(@nClientID, '9PMUC5', 'Programme Manager UK C5'),
	(@nClientID, '9PMUC7', 'Programme Manager UK C7'),
	(@nClientID, '9PMUCX', 'Programme Manager UK CX'),
	(@nClientID, '9PMUD0', 'Programme Manager UK D0'),
	(@nClientID, '9PMUD5', 'Programme Manager UK D5'),
	(@nClientID, '9PMUD7', 'Programme Manager UK D7'),
	(@nClientID, '9PMUDX', 'Programme Manager UK DX'),
	(@nClientID, '9PMUE0', 'Programme Manager UK E0'),
	(@nClientID, '9PMUE5', 'Programme Manager UK E5'),
	(@nClientID, '9PMUE7', 'Programme Manager UK E7'),
	(@nClientID, '9PMUEX', 'Programme Manager UK EX'),
	(@nClientID, '9PRIA0', 'Procure Logist Spec Int A0'),
	(@nClientID, '9PRIA5', 'Procure Logist Spec Int A5'),
	(@nClientID, '9PRIA7', 'Procure Logist Spec Int A7'),
	(@nClientID, '9PRIAX', 'Procure Logist Spec Int AX'),
	(@nClientID, '9PRIB0', 'Procure Logist Spec Int B0'),
	(@nClientID, '9PRIB5', 'Procure Logist Spec Int B5'),
	(@nClientID, '9PRIB7', 'Procure Logist Spec Int B7'),
	(@nClientID, '9PRIBX', 'Procure Logist Spec Int BX'),
	(@nClientID, '9PRIC0', 'Procure Logist Spec Int C0'),
	(@nClientID, '9PRIC5', 'Procure Logist Spec Int C5'),
	(@nClientID, '9PRIC7', 'Procure Logist Spec Int C7'),
	(@nClientID, '9PRICX', 'Procure Logist Spec Int CX'),
	(@nClientID, '9PRID0', 'Procure Logist Spec Int D0'),
	(@nClientID, '9PRID5', 'Procure Logist Spec Int D5'),
	(@nClientID, '9PRID7', 'Procure Logist Spec Int D7'),
	(@nClientID, '9PRIDX', 'Procure Logist Spec Int DX'),
	(@nClientID, '9PRIE0', 'Procure Logist Spec Int E0'),
	(@nClientID, '9PRIE5', 'Procure Logist Spec Int E5'),
	(@nClientID, '9PRIE7', 'Procure Logist Spec Int E7'),
	(@nClientID, '9PRIEX', 'Procure Logist Spec Int EX'),
	(@nClientID, '9PRUA0', 'Procure Logist Spec UK A0'),
	(@nClientID, '9PRUA5', 'Procure Logist Spec UK A5'),
	(@nClientID, '9PRUA7', 'Procure Logist Spec UK A7'),
	(@nClientID, '9PRUAX', 'Procure Logist Spec UK AX'),
	(@nClientID, '9PRUB0', 'Procure Logist Spec UK B0'),
	(@nClientID, '9PRUB5', 'Procure Logist Spec UK B5'),
	(@nClientID, '9PRUB7', 'Procure Logist Spec UK B7'),
	(@nClientID, '9PRUBX', 'Procure Logist Spec UK BX'),
	(@nClientID, '9PRUC0', 'Procure Logist Spec UK C0'),
	(@nClientID, '9PRUC5', 'Procure Logist Spec UK C5'),
	(@nClientID, '9PRUC7', 'Procure Logist Spec UK C7'),
	(@nClientID, '9PRUCX', 'Procure Logist Spec UK CX'),
	(@nClientID, '9PRUD0', 'Procure Logist Spec UK D0'),
	(@nClientID, '9PRUD5', 'Procure Logist Spec UK D5'),
	(@nClientID, '9PRUD7', 'Procure Logist Spec UK D7'),
	(@nClientID, '9PRUDX', 'Procure Logist Spec UK DX'),
	(@nClientID, '9PRUE0', 'Procure Logist Spec UK E0'),
	(@nClientID, '9PRUE5', 'Procure Logist Spec UK E5'),
	(@nClientID, '9PRUE7', 'Procure Logist Spec UK E7'),
	(@nClientID, '9PRUEX', 'Procure Logist Spec UK EX'),
	(@nClientID, '9REUA0', 'Researcher UK A0'),
	(@nClientID, '9REUA5', 'Researcher UK A5'),
	(@nClientID, '9REUA7', 'Researcher UK A7'),
	(@nClientID, '9REUAX', 'Researcher UK AX'),
	(@nClientID, '9REUB0', 'Researcher UK B0'),
	(@nClientID, '9REUB5', 'Researcher UK B5'),
	(@nClientID, '9REUB7', 'Researcher UK B7'),
	(@nClientID, '9REUBX', 'Researcher UK BX'),
	(@nClientID, '9REUC0', 'Researcher UK C0'),
	(@nClientID, '9REUC5', 'Researcher UK C5'),
	(@nClientID, '9REUC7', 'Researcher UK C7'),
	(@nClientID, '9REUCX', 'Researcher UK CX'),
	(@nClientID, '9REUD0', 'Researcher UK D0'),
	(@nClientID, '9REUD5', 'Researcher UK D5'),
	(@nClientID, '9REUD7', 'Researcher UK D7'),
	(@nClientID, '9REUDX', 'Researcher UK DX'),
	(@nClientID, '9REUE0', 'Researcher UK E0'),
	(@nClientID, '9REUE5', 'Researcher UK E5'),
	(@nClientID, '9REUE7', 'Researcher UK E7'),
	(@nClientID, '9REUEX', 'Researcher UK EX'),
	(@nClientID, '9RSIA0', 'Reporting Specialist Int A0'),
	(@nClientID, '9RSIA5', 'Reporting Specialist Int A5'),
	(@nClientID, '9RSIA7', 'Reporting Specialist Int A7'),
	(@nClientID, '9RSIAX', 'Reporting Specialist Int AX'),
	(@nClientID, '9RSIB0', 'Reporting Specialist Int B0'),
	(@nClientID, '9RSIB5', 'Reporting Specialist Int B5'),
	(@nClientID, '9RSIB7', 'Reporting Specialist Int B7'),
	(@nClientID, '9RSIBX', 'Reporting Specialist Int BX'),
	(@nClientID, '9RSIC0', 'Reporting Specialist Int C0'),
	(@nClientID, '9RSIC5', 'Reporting Specialist Int C5'),
	(@nClientID, '9RSIC7', 'Reporting Specialist Int C7'),
	(@nClientID, '9RSICX', 'Reporting Specialist Int CX'),
	(@nClientID, '9RSID0', 'Reporting Specialist Int D0'),
	(@nClientID, '9RSID5', 'Reporting Specialist Int D5'),
	(@nClientID, '9RSID7', 'Reporting Specialist Int D7'),
	(@nClientID, '9RSIDX', 'Reporting Specialist Int DX'),
	(@nClientID, '9RSIE0', 'Reporting Specialist Int E0'),
	(@nClientID, '9RSIE5', 'Reporting Specialist Int E5'),
	(@nClientID, '9RSIE7', 'Reporting Specialist Int E7'),
	(@nClientID, '9RSIEX', 'Reporting Specialist Int EX'),
	(@nClientID, '9RSUA0', 'Reporting Specialist UK A0'),
	(@nClientID, '9RSUA5', 'Reporting Specialist UK A5'),
	(@nClientID, '9RSUA7', 'Reporting Specialist UK A7'),
	(@nClientID, '9RSUAX', 'Reporting Specialist UK AX'),
	(@nClientID, '9RSUB0', 'Reporting Specialist UK B0'),
	(@nClientID, '9RSUB5', 'Reporting Specialist UK B5'),
	(@nClientID, '9RSUB7', 'Reporting Specialist UK B7'),
	(@nClientID, '9RSUBX', 'Reporting Specialist UK BX'),
	(@nClientID, '9RSUC0', 'Reporting Specialist UK C0'),
	(@nClientID, '9RSUC5', 'Reporting Specialist UK C5'),
	(@nClientID, '9RSUC7', 'Reporting Specialist UK C7'),
	(@nClientID, '9RSUCX', 'Reporting Specialist UK CX'),
	(@nClientID, '9RSUD0', 'Reporting Specialist UK D0'),
	(@nClientID, '9RSUD5', 'Reporting Specialist UK D5'),
	(@nClientID, '9RSUD7', 'Reporting Specialist UK D7'),
	(@nClientID, '9RSUDX', 'Reporting Specialist UK DX'),
	(@nClientID, '9RSUE0', 'Reporting Specialist UK E0'),
	(@nClientID, '9RSUE5', 'Reporting Specialist UK E5'),
	(@nClientID, '9RSUE7', 'Reporting Specialist UK E7'),
	(@nClientID, '9RSUEX', 'Reporting Specialist UK EX'),
	(@nClientID, '9SAIA0', 'Stabilisation Advisor A0'),
	(@nClientID, '9SAIA5', 'Stabilisation Advisor A5'),
	(@nClientID, '9SAIA7', 'Stabilisation Advisor A7'),
	(@nClientID, '9SAIAX', 'Stabilisation Advisor AX'),
	(@nClientID, '9SAIB0', 'Stabilisation Advisor B0'),
	(@nClientID, '9SAIB5', 'Stabilisation Advisor B5'),
	(@nClientID, '9SAIB7', 'Stabilisation Advisor B7'),
	(@nClientID, '9SAIBX', 'Stabilisation Advisor BX'),
	(@nClientID, '9SAIC0', 'Stabilisation Advisor C0'),
	(@nClientID, '9SAIC5', 'Stabilisation Advisor C5'),
	(@nClientID, '9SAIC7', 'Stabilisation Advisor C7'),
	(@nClientID, '9SAICX', 'Stabilisation Advisor CX'),
	(@nClientID, '9SAID0', 'Stabilisation Advisor D0'),
	(@nClientID, '9SAID5', 'Stabilisation Advisor D5'),
	(@nClientID, '9SAID7', 'Stabilisation Advisor D7'),
	(@nClientID, '9SAIDX', 'Stabilisation Advisor DX'),
	(@nClientID, '9SAIE0', 'Stabilisation Advisor E0'),
	(@nClientID, '9SAIE5', 'Stabilisation Advisor E5'),
	(@nClientID, '9SAIE7', 'Stabilisation Advisor E7'),
	(@nClientID, '9SAIEX', 'Stabilisation Advisor EX'),
	(@nClientID, '9SEIA0', 'Sr Security Advisor Int A0'),
	(@nClientID, '9SEIA5', 'Sr Security Advisor Int A5'),
	(@nClientID, '9SEIA7', 'Sr Security Advisor Int A7'),
	(@nClientID, '9SEIAX', 'Sr Security Advisor Int AX'),
	(@nClientID, '9SEIB0', 'Sr Security Advisor Int B0'),
	(@nClientID, '9SEIB5', 'Sr Security Advisor Int B5'),
	(@nClientID, '9SEIB7', 'Sr Security Advisor Int B7'),
	(@nClientID, '9SEIBX', 'Sr Security Advisor Int BX'),
	(@nClientID, '9SEIC0', 'Sr Security Advisor Int C0'),
	(@nClientID, '9SEIC5', 'Sr Security Advisor Int C5'),
	(@nClientID, '9SEIC7', 'Sr Security Advisor Int C7'),
	(@nClientID, '9SEICX', 'Sr Security Advisor Int CX'),
	(@nClientID, '9SEID0', 'Sr Security Advisor Int D0'),
	(@nClientID, '9SEID5', 'Sr Security Advisor Int D5'),
	(@nClientID, '9SEID7', 'Sr Security Advisor Int D7'),
	(@nClientID, '9SEIDX', 'Sr Security Advisor Int DX'),
	(@nClientID, '9SEIE0', 'Sr Security Advisor Int E0'),
	(@nClientID, '9SEIE5', 'Sr Security Advisor Int E5'),
	(@nClientID, '9SEIE7', 'Sr Security Advisor Int E7'),
	(@nClientID, '9SEIEX', 'Sr Security Advisor Int EX'),
	(@nClientID, '9SEUA0', 'Sr Security Advisor UK A0'),
	(@nClientID, '9SEUA5', 'Sr Security Advisor UK A5'),
	(@nClientID, '9SEUA7', 'Sr Security Advisor UK A7'),
	(@nClientID, '9SEUAX', 'Sr Security Advisor UK AX'),
	(@nClientID, '9SEUB0', 'Sr Security Advisor UK B0'),
	(@nClientID, '9SEUB5', 'Sr Security Advisor UK B5'),
	(@nClientID, '9SEUB7', 'Sr Security Advisor UK B7'),
	(@nClientID, '9SEUBX', 'Sr Security Advisor UK BX'),
	(@nClientID, '9SEUC0', 'Sr Security Advisor UK C0'),
	(@nClientID, '9SEUC5', 'Sr Security Advisor UK C5'),
	(@nClientID, '9SEUC7', 'Sr Security Advisor UK C7'),
	(@nClientID, '9SEUCX', 'Sr Security Advisor UK CX'),
	(@nClientID, '9SEUD0', 'Sr Security Advisor UK D0'),
	(@nClientID, '9SEUD5', 'Sr Security Advisor UK D5'),
	(@nClientID, '9SEUD7', 'Sr Security Advisor UK D7'),
	(@nClientID, '9SEUDX', 'Sr Security Advisor UK DX'),
	(@nClientID, '9SEUE0', 'Sr Security Advisor UK E0'),
	(@nClientID, '9SEUE5', 'Sr Security Advisor UK E5'),
	(@nClientID, '9SEUE7', 'Sr Security Advisor UK E7'),
	(@nClientID, '9SEUEX', 'Sr Security Advisor UK EX'),
	(@nClientID, '9SHIA0', 'Sr Humanitarian Advisor Int A0'),
	(@nClientID, '9SHIA5', 'Sr Humanitarian Advisor Int A5'),
	(@nClientID, '9SHIA7', 'Sr Humanitarian Advisor Int A7'),
	(@nClientID, '9SHIAX', 'Sr Humanitarian Advisor Int AX'),
	(@nClientID, '9SHIB0', 'Sr Humanitarian Advisor Int B0'),
	(@nClientID, '9SHIB5', 'Sr Humanitarian Advisor Int B5'),
	(@nClientID, '9SHIB7', 'Sr Humanitarian Advisor Int B7'),
	(@nClientID, '9SHIBX', 'Sr Humanitarian Advisor Int BX'),
	(@nClientID, '9SHIC0', 'Sr Humanitarian Advisor Int C0'),
	(@nClientID, '9SHIC5', 'Sr Humanitarian Advisor Int C5'),
	(@nClientID, '9SHIC7', 'Sr Humanitarian Advisor Int C7'),
	(@nClientID, '9SHICX', 'Sr Humanitarian Advisor Int CX'),
	(@nClientID, '9SHID0', 'Sr Humanitarian Advisor Int D0'),
	(@nClientID, '9SHID5', 'Sr Humanitarian Advisor Int D5'),
	(@nClientID, '9SHID7', 'Sr Humanitarian Advisor Int D7'),
	(@nClientID, '9SHIDX', 'Sr Humanitarian Advisor Int DX'),
	(@nClientID, '9SHIE0', 'Sr Humanitarian Advisor Int E0'),
	(@nClientID, '9SHIE5', 'Sr Humanitarian Advisor Int E5'),
	(@nClientID, '9SHIE7', 'Sr Humanitarian Advisor Int E7'),
	(@nClientID, '9SHIEX', 'Sr Humanitarian Advisor Int EX'),
	(@nClientID, '9SSIA0', 'Sr Advisor Specialist Int A0'),
	(@nClientID, '9SSIA5', 'Sr Advisor Specialist Int A5'),
	(@nClientID, '9SSIA7', 'Sr Advisor Specialist Int A7'),
	(@nClientID, '9SSIAX', 'Sr Advisor Specialist Int AX'),
	(@nClientID, '9SSIB0', 'Sr Advisor Specialist Int B0'),
	(@nClientID, '9SSIB5', 'Sr Advisor Specialist Int B5'),
	(@nClientID, '9SSIB7', 'Sr Advisor Specialist Int B7'),
	(@nClientID, '9SSIBX', 'Sr Advisor Specialist Int BX'),
	(@nClientID, '9SSIC0', 'Sr Advisor Specialist Int C0'),
	(@nClientID, '9SSIC5', 'Sr Advisor Specialist Int C5'),
	(@nClientID, '9SSIC7', 'Sr Advisor Specialist Int C7'),
	(@nClientID, '9SSICX', 'Sr Advisor Specialist Int CX'),
	(@nClientID, '9SSID0', 'Sr Advisor Specialist Int D0'),
	(@nClientID, '9SSID5', 'Sr Advisor Specialist Int D5'),
	(@nClientID, '9SSID7', 'Sr Advisor Specialist Int D7'),
	(@nClientID, '9SSIDX', 'Sr Advisor Specialist Int DX'),
	(@nClientID, '9SSIE0', 'Sr Advisor Specialist Int E0'),
	(@nClientID, '9SSIE5', 'Sr Advisor Specialist Int E5'),
	(@nClientID, '9SSIE7', 'Sr Advisor Specialist Int E7'),
	(@nClientID, '9SSIEX', 'Sr Advisor Specialist Int EX'),
	(@nClientID, '9SSUA0', 'Sr Advisor Specialist UK A0'),
	(@nClientID, '9SSUA5', 'Sr Advisor Specialist UK A5'),
	(@nClientID, '9SSUA7', 'Sr Advisor Specialist UK A7'),
	(@nClientID, '9SSUAX', 'Sr Advisor Specialist UK AX'),
	(@nClientID, '9SSUB0', 'Sr Advisor Specialist UK B0'),
	(@nClientID, '9SSUB5', 'Sr Advisor Specialist UK B5'),
	(@nClientID, '9SSUB7', 'Sr Advisor Specialist UK B7'),
	(@nClientID, '9SSUBX', 'Sr Advisor Specialist UK BX'),
	(@nClientID, '9SSUC0', 'Sr Advisor Specialist UK C0'),
	(@nClientID, '9SSUC5', 'Sr Advisor Specialist UK C5'),
	(@nClientID, '9SSUC7', 'Sr Advisor Specialist UK C7'),
	(@nClientID, '9SSUCX', 'Sr Advisor Specialist UK CX'),
	(@nClientID, '9SSUD0', 'Sr Advisor Specialist UK D0'),
	(@nClientID, '9SSUD5', 'Sr Advisor Specialist UK D5'),
	(@nClientID, '9SSUD7', 'Sr Advisor Specialist UK D7'),
	(@nClientID, '9SSUDX', 'Sr Advisor Specialist UK DX'),
	(@nClientID, '9SSUE0', 'Sr Advisor Specialist UK E0'),
	(@nClientID, '9SSUE5', 'Sr Advisor Specialist UK E5'),
	(@nClientID, '9SSUE7', 'Sr Advisor Specialist UK E7'),
	(@nClientID, '9SSUEX', 'Sr Advisor Specialist UK EX'),
	(@nClientID, '9STIA0', 'Sr Technical Specialist Int A0'),
	(@nClientID, '9STIA5', 'Sr Technical Specialist Int A5'),
	(@nClientID, '9STIA7', 'Sr Technical Specialist Int A7'),
	(@nClientID, '9STIAX', 'Sr Technical Specialist Int AX'),
	(@nClientID, '9STIB0', 'Sr Technical Specialist Int B0'),
	(@nClientID, '9STIB5', 'Sr Technical Specialist Int B5'),
	(@nClientID, '9STIB7', 'Sr Technical Specialist Int B7'),
	(@nClientID, '9STIBX', 'Sr Technical Specialist Int BX'),
	(@nClientID, '9STIC0', 'Sr Technical Specialist Int C0'),
	(@nClientID, '9STIC5', 'Sr Technical Specialist Int C5'),
	(@nClientID, '9STIC7', 'Sr Technical Specialist Int C7'),
	(@nClientID, '9STICX', 'Sr Technical Specialist Int CX'),
	(@nClientID, '9STID0', 'Sr Technical Specialist Int D0'),
	(@nClientID, '9STID5', 'Sr Technical Specialist Int D5'),
	(@nClientID, '9STID7', 'Sr Technical Specialist Int D7'),
	(@nClientID, '9STIDX', 'Sr Technical Specialist Int DX'),
	(@nClientID, '9STIE0', 'Sr Technical Specialist Int E0'),
	(@nClientID, '9STIE5', 'Sr Technical Specialist Int E5'),
	(@nClientID, '9STIE7', 'Sr Technical Specialist Int E7'),
	(@nClientID, '9STIEX', 'Sr Technical Specialist Int EX'),
	(@nClientID, '9TMLA0', 'Team Leader A0'),
	(@nClientID, '9TMLA5', 'Team Leader A5'),
	(@nClientID, '9TMLA7', 'Team Leader A7'),
	(@nClientID, '9TMLAX', 'Team Leader AX'),
	(@nClientID, '9TMLB0', 'Team Leader B0'),
	(@nClientID, '9TMLB5', 'Team Leader B5'),
	(@nClientID, '9TMLB7', 'Team Leader B7'),
	(@nClientID, '9TMLBX', 'Team Leader BX'),
	(@nClientID, '9TMLC0', 'Team Leader C0'),
	(@nClientID, '9TMLC5', 'Team Leader C5'),
	(@nClientID, '9TMLC7', 'Team Leader C7'),
	(@nClientID, '9TMLCX', 'Team Leader CX'),
	(@nClientID, '9TMLD0', 'Team Leader D0'),
	(@nClientID, '9TMLD5', 'Team Leader D5'),
	(@nClientID, '9TMLD7', 'Team Leader D7'),
	(@nClientID, '9TMLDX', 'Team Leader DX'),
	(@nClientID, '9TMLE0', 'Team Leader E0'),
	(@nClientID, '9TMLE5', 'Team Leader E5'),
	(@nClientID, '9TMLE7', 'Team Leader E7'),
	(@nClientID, '9TMLEX', 'Team Leader EX'),
	(@nClientID, '9TRUA0', 'Trainer Facilitator UK A0'),
	(@nClientID, '9TRUA5', 'Trainer Facilitator UK A5'),
	(@nClientID, '9TRUA7', 'Trainer Facilitator UK A7'),
	(@nClientID, '9TRUAX', 'Trainer Facilitator UK AX'),
	(@nClientID, '9TRUB0', 'Trainer Facilitator UK B0'),
	(@nClientID, '9TRUB5', 'Trainer Facilitator UK B5'),
	(@nClientID, '9TRUB7', 'Trainer Facilitator UK B7'),
	(@nClientID, '9TRUBX', 'Trainer Facilitator UK BX'),
	(@nClientID, '9TRUC0', 'Trainer Facilitator UK C0'),
	(@nClientID, '9TRUC5', 'Trainer Facilitator UK C5'),
	(@nClientID, '9TRUC7', 'Trainer Facilitator UK C7'),
	(@nClientID, '9TRUCX', 'Trainer Facilitator UK CX'),
	(@nClientID, '9TRUD0', 'Trainer Facilitator UK D0'),
	(@nClientID, '9TRUD5', 'Trainer Facilitator UK D5'),
	(@nClientID, '9TRUD7', 'Trainer Facilitator UK D7'),
	(@nClientID, '9TRUDX', 'Trainer Facilitator UK DX'),
	(@nClientID, '9TRUE0', 'Trainer Facilitator UK E0'),
	(@nClientID, '9TRUE5', 'Trainer Facilitator UK E5'),
	(@nClientID, '9TRUE7', 'Trainer Facilitator UK E7'),
	(@nClientID, '9TRUEX', 'Trainer Facilitator UK EX')
GO
--End table client.ProjectLaborCode

--End file Build File - 04 - Data - Client.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Invoice', 'Approve', 'Sent when an invoice is approved', 'An Invoice You Have Submitted Has Been Approved', '<p>The following invoice has been approved:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'DecrementWorkflow', 'Sent when an invoice is rejected', 'An Invoice You Have Submitted Has Been Rejected', '<p>The following invoice has been rejected:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Rejection Reason: &nbsp;[[InvoiceRejectionReasonName]]<br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'IncrementWorkflow', 'Sent when an invoice is forwarded in the workflow', 'A Consultant Invoice Has Been Forwarded For Your Review', '<p>[[PersonNameFormatted]] has reviewed the following invoice and is forwarding it for your approval:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'Submit', 'Sent when an invoice is submitted', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonExisting', 'Sent when an existing person is invited to DeployAdviser', 'You Have Been Invited To Affiliate With A Client In DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonImport', 'Sent when an imported person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonNew', 'Sent when a new person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'InvitePersonProject', 'Sent when a project assignment offer is made', 'A Project Assignment Invitation Has Been Made', '<p>[[ClientName]] invites you to accept the following project assignment:</p><p>Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ProjectLaborCode', 
	@EntityTypeName = 'Project Labor Code', 
	@EntityTypeNamePlural = 'Project Labor Codes',
	@SchemaName = 'client', 
	@TableName = 'ProjectLaborCode', 
	@PrimaryKeyFieldName = 'ProjectLaborCodeID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-money',
	@NewMenuItemCode = 'TimeExpense'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'PersonImport',
	@NewMenuItemLink = '/person/personimport',
	@NewMenuItemText = 'Import Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.PersonImport'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupKeyAddUpdate 'OANDAAPIKey', 'The API key for the OANDA exchange rate service', 'QZ72iX4YRiKLTkXLjtyMSQt3'
GO
--End table core.SystemSetup

--Begin table permissionable.PermissionableTemplate
TRUNCATE TABLE permissionable.PermissionableTemplate
GO

INSERT INTO permissionable.PermissionableTemplate
	(PermissionableTemplateCode, PermissionableTemplateName) 
VALUES 
	('Administrator', 'Consultancy Administrator'),
	('ProjectManager', 'Project Manager'),
	('Consultant', 'Deployee')
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
TRUNCATE TABLE permissionable.PermissionableTemplatePermissionable
GO

INSERT INTO permissionable.PermissionableTemplatePermissionable
	(PermissionableTemplateID, PermissionableLineage) 
SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'Administrator'
	AND P.PermissionableLineage IN ('Client.AddUpdate', 'Client.List', 'Client.View', 'Document.GetDocumentByDocumentGUID', 'Invoice.List', 'Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'Invoice.View', 'Person.Invite', 'Person.List', 'Person.SendInvite', 'Person.View', 'PersonProject.AddUpdate', 'PersonProject.List', 'PersonProject.List.CanExpirePersonProject', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.AddUpdate', 'Project.List', 'Project.View', 'Workflow.AddUpdate', 'Workflow.List', 'Workflow.View')
	AND P.IsGlobal = 0

UNION

SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'ProjectManager'
	AND P.PermissionableLineage IN ('Document.GetDocumentByDocumentGUID', 'Invoice.List', 'Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'Invoice.View', 'Person.Invite', 'Person.SendInvite', 'PersonProject.AddUpdate', 'PersonProject.List', 'PersonProject.List.CanExpirePersonProject', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.AddUpdate', 'Project.List', 'Project.View')
	AND P.IsGlobal = 0
	
UNION

SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'Consultant'
	AND P.PermissionableLineage IN ('Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.View')
	AND P.IsGlobal = 0
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table project.ProjectTermOfReference
TRUNCATE TABLE project.ProjectTermOfReference
GO

INSERT INTO project.ProjectTermOfReference
	(ProjectID, ProjectTermOfReferenceName, ProjectTermOfReferenceDescription)
SELECT
	P.ProjectID,
	'ToR 1',
	'Term of Reference 1'
FROM project.Project P

UNION

SELECT
	P.ProjectID,
	'ToR 2',
	'Term of Reference 2'
FROM project.Project P

UNION

SELECT
	P.ProjectID,
	'ToR 3',
	'Term of Reference 3'
FROM project.Project P
GO

UPDATE PP
SET PP.ProjectTermOfReferenceID = PTOR2.ProjectTermOfReferenceID
FROM person.PersonProject PP
	JOIN 
		(
		SELECT
			D.ProjectID,
			D.ProjectTermOfReferenceID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY PTOR1.ProjectID ORDER BY PTOR1.ProjectTermOfReferenceID) AS RowIndex,
				PTOR1.ProjectID,
				PTOR1.ProjectTermOfReferenceID
			FROM project.ProjectTermOfReference PTOR1
			) D
		WHERE D.RowIndex = 1
		) PTOR2 ON PTOR2.ProjectID = PP.ProjectID
GO
--End table project.ProjectTermOfReference

--Begin table workflow.EntityWorkflowStepGroupPerson
TRUNCATE TABLE workflow.EntityWorkflowStepGroupPerson
GO
--End table workflow.EntityWorkflowStepGroupPerson



--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the list of permissionabless on the user edit page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.2 - 2017.11.09 15.10.49')
GO
--End build tracking

