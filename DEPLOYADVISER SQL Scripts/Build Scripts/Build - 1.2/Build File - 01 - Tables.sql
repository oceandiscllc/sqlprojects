USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'ClientCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode1', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode2', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'FinanceCode3', 'VARCHAR(50)'
GO
--End table client.Client

--Begin table client.ProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'client.ProjectLaborCode'

EXEC utility.DropObject @TableName

CREATE TABLE client.ProjectLaborCode
	(
	ProjectLaborCodeID INT IDENTITY(0,1) NOT NULL,
	ClientID INT,
	ProjectLaborCode VARCHAR(50),
	ProjectLaborCodeName VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectLaborCodeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProjectLaborCode', 'ProjectLaborCodeName,ProjectLaborCode', 'ProjectLaborCodeID'
GO
--End table client.ProjectLaborCode

--Begin table client.ClientTermOfReference
DECLARE @TableName VARCHAR(250) = 'client.ClientTermOfReference'

EXEC utility.DropObject @TableName
GO
--End table client.ClientTermOfReference

--Begin table import.Person
DECLARE @TableName VARCHAR(250) = 'import.Person'

EXEC utility.DropObject @TableName

CREATE TABLE import.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ClientID INT,
	ClientPersonRoleCodeList VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table import.Person

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.Invoice
	(
	InvoiceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT,
	StartDate DATE,
	EndDate DATE,
	InvoiceDateTime DATETIME,
	InvoiceAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	PersonAccountID INT,
	PersonInvoiceNumber VARCHAR(15),
	TaxRate NUMERIC(18,4),
	ExchangeRateXML VARCHAR(MAX),
	InvoiceRejectionReasonID INT,
	InvoiceStatus VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvoiceAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceRejectionReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceStatus', 'VARCHAR(50)', 'Submitted'
EXEC utility.SetDefaultConstraint @TableName, 'PersonAccountID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18,4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InvoiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Invoice', 'PersonID,ProjectID', 'InvoiceID'
GO
--End table invoice.Invoice

--Begin table invoice.ExchangeRate
DECLARE @TableName VARCHAR(250) = 'invoice.ExchangeRate'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'invoice.InvoiceExchangeRate'

CREATE TABLE invoice.ExchangeRate
	(
	ExchangeRateID INT NOT NULL IDENTITY(1,1),
	ISOCurrencyCodeFrom CHAR(3),
	ISOCurrencyCodeTo CHAR(3),
	ExchangeRate NUMERIC(18,5),
	ExchangeRateDate DATE,
	IsFinalRate BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFinalRate', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ExchangeRateID'
EXEC utility.SetIndexClustered @TableName, 'IX_ExchangeRate', 'ISOCurrencyCodeFrom,ISOCurrencyCodeTo,ExchangeRateDate'
GO
--End table invoice.ExchangeRate

--Begin table invoice.InvoiceExpenseLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoiceExpenseLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoiceExpenseLog
	(
	InvoiceExpenseLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	ExpenseDate DATE,
	ClientCostCodeName VARCHAR(50),
	ExpenseAmount NUMERIC(18,2),
	TaxAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3), 
	ExchangeRate NUMERIC(18,5),
	DocumentGUID VARCHAR(50),
	ProjectManagerNotes VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxAmount', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoiceExpenseLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoiceExpenseLog', 'InvoiceID,ExpenseDate'
GO
--End table invoice.InvoiceExpenseLog

--Begin table invoice.InvoicePerDiemLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoicePerDiemLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoicePerDiemLog
	(
	InvoicePerDiemLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	DateWorked DATE,
	ProjectLocationName VARCHAR(100),
	DPAAmount NUMERIC(18,2),
	DSAAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	ExchangeRate NUMERIC(18,5)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DPAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoicePerDiemLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoicePerDiemLog', 'InvoiceID,DateWorked'
GO
--End table invoice.InvoicePerDiemLog

--Begin table invoice.InvoiceTimeLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoiceTimeLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoiceTimeLog
	(
	InvoiceTimeLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	DateWorked DATE,
	ProjectLocationName VARCHAR(100),
	HoursPerDay NUMERIC(18,2),
	HoursWorked NUMERIC(18,2),
	FeeRate NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	ExchangeRate NUMERIC(18,5),
	ProjectLaborCode VARCHAR(50),
	ProjectManagerNotes VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FeeRate', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursPerDay', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoiceTimeLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoiceTimeLog', 'InvoiceID,DateWorked'
GO
--End table invoice.InvoiceTimeLog

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.AddColumn @TableName, 'PermissionableTemplateCode', 'VARCHAR(50)'
GO
--End table permissionable.PermissionableTemplate

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'HasAccptedTerms', 'BIT', '0'
EXEC utility.DropColumn @TableName, 'NickName'
GO

EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username and manage import data
-- =========================================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nCount INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') + 'XXX' AS LastName
		FROM INSERTED I
		WHERE I.UserName IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM person.Person P
				WHERE P.UserName = I.UserName
					AND P.PersonID <> I.PersonID
				)

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cUserName = UPPER(LEFT(@cLastName, 3))

		SELECT @nCount = COUNT(P.UserName) + 1
		FROM person.Person P
		WHERE LEFT(P.UserName, 3) = @cUserName

		SET @cUserName += RIGHT('000' + CAST(@nCount AS VARCHAR(10)), 3)

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.AddColumn @TableName, 'IntermediateAccountPayee', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TerminalAccountPayee', 'VARCHAR(50)'
GO
--End table person.PersonAccount

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'ProjectTermOfReferenceID', 'INT', '0'

EXEC utility.DropIndex @TableName, 'IX_PersonProject'
EXEC utility.DropColumn @TableName, 'ClientTermOfReferenceID'

EXEC utility.SetIndexClustered @TableName, 'IX_PersonProject', 'PersonID,ProjectID,ProjectTermOfReferenceID,ClientFunctionID'
GO
--End table person.PersonProject

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.AddColumn @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropColumn @TableName, 'HoursPerDay'
GO
--End table person.PersonProjectLocation

--Begin table person.PersonProjectTime
ALTER TABLE person.PersonProjectTime DROP CONSTRAINT DF_PersonProjectTime_HoursWorked
GO

ALTER TABLE person.PersonProjectTime ALTER COLUMN HoursWorked NUMERIC(18,2)
GO

DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.SetDefaultConstraint @TableName, 'HoursWorked', 'NUMERIC(18,2)', '0'

EXEC utility.AddColumn @TableName, 'ExchangeRate', 'NUMERIC(18,5)', '0'
GO
--End table person.PersonProjectTime

--Begin table person.PersonUnavailability
DECLARE @TableName VARCHAR(250) = 'person.PersonUnavailability'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonUnavailability
	(
	PersonUnavailabilityID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PersonUnavailabilityDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonUnavailabilityID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonUnavailability', 'PersonID,PersonUnavailabilityDate'
GO
--End table person.PersonUnavailability

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'HoursPerDay', 'NUMERIC(18,2)', '8'
GO
--End table project.Project

--Begin table project.ProjectTermOfReference
DECLARE @TableName VARCHAR(250) = 'project.ProjectTermOfReference'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectTermOfReference
	(
	ProjectTermOfReferenceID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	ProjectTermOfReferenceName VARCHAR(50),
	ProjectTermOfReferenceDescription VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTermOfReferenceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTermOfReference', 'ProjectID,ProjectTermOfReferenceName'
GO
--End table project.ProjectTermOfReference

