USE DeployAdviser
GO

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Invoice', 'Approve', 'Sent when an invoice is approved', 'An Invoice You Have Submitted Has Been Approved', '<p>The following invoice has been approved:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'DecrementWorkflow', 'Sent when an invoice is rejected', 'An Invoice You Have Submitted Has Been Rejected', '<p>The following invoice has been rejected:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Rejection Reason: &nbsp;[[InvoiceRejectionReasonName]]<br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'IncrementWorkflow', 'Sent when an invoice is forwarded in the workflow', 'A Consultant Invoice Has Been Forwarded For Your Review', '<p>[[PersonNameFormatted]] has reviewed the following invoice and is forwarding it for your approval:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'Submit', 'Sent when an invoice is submitted', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonExisting', 'Sent when an existing person is invited to DeployAdviser', 'You Have Been Invited To Affiliate With A Client In DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonImport', 'Sent when an imported person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonNew', 'Sent when a new person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'InvitePersonProject', 'Sent when a project assignment offer is made', 'A Project Assignment Invitation Has Been Made', '<p>[[ClientName]] invites you to accept the following project assignment:</p><p>Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ProjectLaborCode', 
	@EntityTypeName = 'Project Labor Code', 
	@EntityTypeNamePlural = 'Project Labor Codes',
	@SchemaName = 'client', 
	@TableName = 'ProjectLaborCode', 
	@PrimaryKeyFieldName = 'ProjectLaborCodeID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-money',
	@NewMenuItemCode = 'TimeExpense'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'PersonImport',
	@NewMenuItemLink = '/person/personimport',
	@NewMenuItemText = 'Import Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.PersonImport'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupKeyAddUpdate 'OANDAAPIKey', 'The API key for the OANDA exchange rate service', 'QZ72iX4YRiKLTkXLjtyMSQt3'
GO
--End table core.SystemSetup

--Begin table permissionable.PermissionableTemplate
TRUNCATE TABLE permissionable.PermissionableTemplate
GO

INSERT INTO permissionable.PermissionableTemplate
	(PermissionableTemplateCode, PermissionableTemplateName) 
VALUES 
	('Administrator', 'Consultancy Administrator'),
	('ProjectManager', 'Project Manager'),
	('Consultant', 'Deployee')
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
TRUNCATE TABLE permissionable.PermissionableTemplatePermissionable
GO

INSERT INTO permissionable.PermissionableTemplatePermissionable
	(PermissionableTemplateID, PermissionableLineage) 
SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'Administrator'
	AND P.PermissionableLineage IN ('Client.AddUpdate', 'Client.List', 'Client.View', 'Document.GetDocumentByDocumentGUID', 'Invoice.List', 'Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'Invoice.View', 'Person.Invite', 'Person.List', 'Person.SendInvite', 'Person.View', 'PersonProject.AddUpdate', 'PersonProject.List', 'PersonProject.List.CanExpirePersonProject', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.AddUpdate', 'Project.List', 'Project.View', 'Workflow.AddUpdate', 'Workflow.List', 'Workflow.View')
	AND P.IsGlobal = 0

UNION

SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'ProjectManager'
	AND P.PermissionableLineage IN ('Document.GetDocumentByDocumentGUID', 'Invoice.List', 'Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'Invoice.View', 'Person.Invite', 'Person.SendInvite', 'PersonProject.AddUpdate', 'PersonProject.List', 'PersonProject.List.CanExpirePersonProject', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.AddUpdate', 'Project.List', 'Project.View')
	AND P.IsGlobal = 0
	
UNION

SELECT
	PT.PermissionableTemplateID,
	P.PermissionableLineage
FROM permissionable.PermissionableTemplate PT, permissionable.Permissionable P
WHERE PT.PermissionableTemplateCode = 'Consultant'
	AND P.PermissionableLineage IN ('Invoice.ListCandidateRecords', 'Invoice.PreviewInvoice', 'Invoice.Setup', 'Invoice.ShowSummary', 'Invoice.Submit', 'PersonProject.View', 'PersonProjectExpense.AddUpdate', 'PersonProjectExpense.List', 'PersonProjectExpense.View', 'PersonProjectTime.AddUpdate', 'PersonProjectTime.List', 'PersonProjectTime.View', 'Project.View')
	AND P.IsGlobal = 0
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table project.ProjectTermOfReference
TRUNCATE TABLE project.ProjectTermOfReference
GO

INSERT INTO project.ProjectTermOfReference
	(ProjectID, ProjectTermOfReferenceName, ProjectTermOfReferenceDescription)
SELECT
	P.ProjectID,
	'ToR 1',
	'Term of Reference 1'
FROM project.Project P

UNION

SELECT
	P.ProjectID,
	'ToR 2',
	'Term of Reference 2'
FROM project.Project P

UNION

SELECT
	P.ProjectID,
	'ToR 3',
	'Term of Reference 3'
FROM project.Project P
GO

UPDATE PP
SET PP.ProjectTermOfReferenceID = PTOR2.ProjectTermOfReferenceID
FROM person.PersonProject PP
	JOIN 
		(
		SELECT
			D.ProjectID,
			D.ProjectTermOfReferenceID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY PTOR1.ProjectID ORDER BY PTOR1.ProjectTermOfReferenceID) AS RowIndex,
				PTOR1.ProjectID,
				PTOR1.ProjectTermOfReferenceID
			FROM project.ProjectTermOfReference PTOR1
			) D
		WHERE D.RowIndex = 1
		) PTOR2 ON PTOR2.ProjectID = PP.ProjectID
GO
--End table project.ProjectTermOfReference

--Begin table workflow.EntityWorkflowStepGroupPerson
TRUNCATE TABLE workflow.EntityWorkflowStepGroupPerson
GO
--End table workflow.EntityWorkflowStepGroupPerson


