USE DeployAdviser
GO

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN

			UPDATE MI
			SET 
				MI.Icon = CASE WHEN @Icon IS NOT NULL THEN CASE WHEN LEN(RTRIM(@Icon)) = 0 THEN NULL ELSE @Icon END ELSE MI.Icon END,
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab,
				MI.MenuItemLink = CASE WHEN LEN(RTRIM(@NewMenuItemLink)) = 0 THEN NULL ELSE CASE WHEN @NewMenuItemLink IS NOT NULL THEN @NewMenuItemLink ELSE MI.MenuItemLink END END,
				MI.MenuItemText = CASE WHEN @NewMenuItemText IS NOT NULL THEN @NewMenuItemText ELSE MI.MenuItemText END,
				MI.ParentMenuItemID = CASE WHEN @nOldParentMenuItemID <> @nParentMenuItemID THEN @nParentMenuItemID ELSE MI.ParentMenuItemID END
			FROM core.MenuItem MI 
			WHERE MI.MenuItemID = @nNewMenuItemID

			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		IF @AfterMenuItemCode IS NOT NULL OR @BeforeMenuItemCode IS NOT NULL
			BEGIN

			DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
		
			DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
				SELECT
					MI.MenuItemID,
					MI.MenuItemCode
				FROM core.MenuItem MI
				WHERE MI.ParentMenuItemID = @nParentMenuItemID
					AND MI.MenuItemID <> @nNewMenuItemID
				ORDER BY MI.DisplayOrder
		
			OPEN oCursor
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
			WHILE @@fetch_status = 0
				BEGIN
		
				IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
					BEGIN
					
					INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
					SET @nIsInserted = 1
					
					END
				--ENDIF
		
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
		
				IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
					BEGIN
					
					INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
					SET @nIsInserted = 1
					
					END
				--ENDIF
				
				FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		
				END
			--END WHILE
			
			CLOSE oCursor
			DEALLOCATE oCursor	
		
			UPDATE MI
			SET MI.DisplayOrder = T1.DisplayOrder
			FROM core.MenuItem MI
				JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID

			END
		--ENDIF
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cClientCostCodes VARCHAR(MAX) = ''
		
		SELECT @cClientCostCodes = COALESCE(@cClientCostCodes, '') + D.ClientCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientCostCode'), ELEMENTS) AS ClientCostCode
			FROM client.ClientCostCode T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientFunctions VARCHAR(MAX) = ''
		
		SELECT @cClientFunctions = COALESCE(@cClientFunctions, '') + D.ClientFunction
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientFunction'), ELEMENTS) AS ClientFunction
			FROM client.ClientFunction T 
			WHERE T.ClientID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cClientCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Functions>' + ISNULL(@cClientFunctions, '') + '</Functions>' AS XML)
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cProjectCostCodes VARCHAR(MAX) = ''
		
		SELECT @cProjectCostCodes = COALESCE(@cProjectCostCodes, '') + D.ProjectCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCostCode'), ELEMENTS) AS ProjectCostCode
			FROM project.ProjectCostCode T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectCurrencies VARCHAR(MAX) = ''
		
		SELECT @cProjectCurrencies = COALESCE(@cProjectCurrencies, '') + D.ProjectCurrency
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCurrency'), ELEMENTS) AS ProjectCurrency
			FROM project.ProjectCurrency T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectLocations VARCHAR(MAX) = ''
		
		SELECT @cProjectLocations = COALESCE(@cProjectLocations, '') + D.ProjectLocation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectLocation'), ELEMENTS) AS ProjectLocation
			FROM project.ProjectLocation T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectTermOfReference VARCHAR(MAX) = ''
		
		SELECT @cProjectTermOfReference = COALESCE(@cProjectTermOfReference, '') + D.ProjectTermOfReference
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectTermOfReference'), ELEMENTS) AS ProjectTermOfReference
			FROM project.ProjectTermOfReference T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cProjectCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Currencies>' + ISNULL(@cProjectCurrencies, '') + '</Currencies>' AS XML),
			CAST('<Locations>' + ISNULL(@cProjectLocations, '') + '</Locations>' AS XML),
			CAST('<TermsOfReference>' + ISNULL(@cProjectTermOfReference, '') + '</TermsOfReference>' AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2),
		DSAAmount NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2),
		HoursWorked NUMERIC(18,2),
		FeeRate NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PPE.ISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DSAAmount, DPAAmount, ISOCurrencyCode, ExchangeRate)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PPT.DSAAmount,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(@InvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate
	FROM person.Person PN, project.Project PJ
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
	WHERE PN.PersonID = @PersonID
		AND PJ.ProjectID = @ProjectID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		TPL.DPAAmount,
		CAST(TPL.DPAAmount * TPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		CAST(TPL.DSAAmount * TPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TPL.ExchangeRate
	FROM @tPerDiemLog TPL
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		SUM(TPL.DPAAmount) AS DPAAmount,
		CAST(SUM(TPL.DPAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DSAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS DPACount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		TPL.ISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS DSACount
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DSAAmount, TPL.ISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TEL.ExpenseAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TEL.TaxAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	EXEC person.GetPersonUnavailabilityByPersonID @PersonID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceReviewData
EXEC utility.DropObject 'invoice.GetInvoiceReviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoiceReviewData

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	EXEC invoice.InitializeInvoiceDataByInvoiceID @InvoiceID

	--InvoiceData
	EXEC invoice.GetInvoiceByInvoiceID @InvoiceID

	--InvoiceExpenseLog
	EXEC invoice.GetInvoiceExpenseLogByInvoiceID @InvoiceID

	--InvoicePerDiemLog
	EXEC invoice.GetInvoicePerDiemLogByInvoiceID @InvoiceID

	--InvoicePerDiemLogSummary
	EXEC invoice.GetInvoicePerDiemLogSummaryByInvoiceID @InvoiceID

	--InvoiceTimeLog
	EXEC invoice.GetInvoiceTimeLogByInvoiceID @InvoiceID

	--InvoiceTimeLogSummary
	EXEC invoice.GetInvoiceTimeLogSummaryByInvoiceID @InvoiceID

	--InvoiceTotals
	EXEC invoice.GetInvoiceTotalsByInvoiceID @InvoiceID

	--InvoiceWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID

	--InvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID

END
GO
--End procedure invoice.GetInvoiceReviewData

-- ======================================= --
-- Begin Invoice Review & Print procedures --
-- ======================================= --

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoiceExchangeRateByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExchangeRateByInvoiceID'
GO
--End procedure invoice.GetInvoiceExchangeRateByInvoiceID

--Begin procedure invoice.GetInvoiceExpenseLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceExpenseLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLog
	SELECT
		core.FormatDate(IEL.ExpenseDate) AS ExpenseDateFormatted,
		IEL.ClientCostCodeName,
		IEL.ExpenseAmount,
		IEL.TaxAmount,
		IEL.ISOCurrencyCode, 
		IEL.ExchangeRate,
		CAST(IEL.ExchangeRate * IEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(IEL.ExchangeRate * IEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN IEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + IEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		IEL.ProjectManagerNotes
	FROM invoice.InvoiceExpenseLog IEL
	WHERE IEL.InvoiceID = @InvoiceID
	ORDER BY IEL.ExpenseDate

END
GO
--End procedure invoice.GetInvoiceExpenseLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoicePerDiemLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		core.FormatDate(IPL.DateWorked) AS DateWorkedFormatted,
		IPL.ProjectLocationName,
		C.FinanceCode2 AS DPACode,
		IPL.DPAAmount,
		CAST(IPL.DPAAmount * IPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		C.FinanceCode3 AS DSACode,
		IPL.DSAAmount,
		CAST(IPL.DSAAmount * IPL.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		IPL.ISOCurrencyCode, 
		IPL.ExchangeRate
	FROM invoice.InvoicePerDiemLog IPL
		JOIN invoice.Invoice I ON I.InvoiceID = IPL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND IPL.InvoiceID = @InvoiceID
	ORDER BY IPL.DateWorked

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoicePerDiemLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLogSummary
	SELECT 
		IPL.ProjectLocationName,
		C.FinanceCode2 AS DPACode,
		SUM(IPL.DPAAmount) AS DPAAmount,
		CAST(SUM(IPL.DPAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(IPL1.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL1 WHERE IPL1.InvoiceID = IPL.InvoiceID AND IPL1.DPAAmount > 0 AND IPL1.ProjectLocationName = IPL.ProjectLocationName) AS DPACount,
		C.FinanceCode3 AS DSACode,
		IPL.DSAAmount,
		IPL.ISOCurrencyCode,
		CAST(SUM(IPL.DSAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(IPL2.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL2 WHERE IPL2.InvoiceID = IPL.InvoiceID AND IPL2.DSAAmount > 0 AND IPL2.ProjectLocationName = IPL.ProjectLocationName) AS DSACount
	FROM invoice.InvoicePerDiemLog IPL
		JOIN invoice.Invoice I ON I.InvoiceID = IPL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND IPL.InvoiceID = @InvoiceID
	GROUP BY IPL.ProjectLocationName, IPL.DSAAmount, IPL.ISOCurrencyCode, IPL.InvoiceID, C.FinanceCode2, C.FinanceCode3
	ORDER BY IPL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceTimeLog table
-- =================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLog
	SELECT 
		C.FinanceCode1 + CASE WHEN ITL.ProjectLaborCode IS NULL THEN '' ELSE '.' + ITL.ProjectLaborCode END AS LaborCode,
		core.FormatDate(ITL.DateWorked) AS DateWorkedFormatted,
		ITL.ProjectLocationName,
		ITL.FeeRate,
		ITL.HoursWorked,
		CAST(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		ITL.ISOCurrencyCode,
		ITL.ExchangeRate,
		ITL.ProjectManagerNotes
	FROM invoice.InvoiceTimeLog ITL
		JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND ITL.InvoiceID = @InvoiceID
	ORDER BY ITL.DateWorked

END
GO
--End procedure invoice.GetInvoiceTimeLogByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoiceTimeLog table
-- =================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLogSummary
	SELECT 
		ITL.ProjectLocationName,
		ITL.FeeRate AS FeeRate,
		SUM(ITL.HoursWorked) AS HoursWorked,
		CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM invoice.InvoiceTimeLog ITL
		JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND ITL.InvoiceID = @InvoiceID
	GROUP BY ITL.ProjectLocationName, ITL.FeeRate, C.FinanceCode1
	ORDER BY ITL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoiceTotalsByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTotalsByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from varions invoice.Invoice% tables
-- ================================================================================
CREATE PROCEDURE invoice.GetInvoiceTotalsByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(ITL.HoursWorked * (ITL.FeeRate / ITL.HoursPerDay) * ITL.ExchangeRate * I.TaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM invoice.InvoiceTimeLog ITL
			JOIN invoice.Invoice I ON I.InvoiceID = ITL.InvoiceID
				AND ITL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(IPL.DSAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM invoice.InvoicePerDiemLog IPL
		WHERE IPL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(IPL.DPAAmount * IPL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM invoice.InvoicePerDiemLog IPL
		WHERE IPL.InvoiceID = @InvoiceID

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(IEL.ExpenseAmount * IEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(IEL.TaxAmount * IEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM invoice.InvoiceExpenseLog IEL
		WHERE IEL.InvoiceID = @InvoiceID
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

END
GO
--End procedure invoice.GetInvoiceTotalsByInvoiceID

--Begin procedure invoice.GetPersonAccountByInvoiceID
EXEC utility.DropObject 'invoice.GetPersonAccountByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.02
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE invoice.GetPersonAccountByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
		JOIN invoice.Invoice I ON I.PersonAccountID = PA.PersonAccountID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetPersonAccountByInvoiceID

--Begin procedure invoice.InitializeInvoiceDataByInvoiceID
EXEC utility.DropObject 'invoice.InitializeInvoiceDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to initialize invoice data for review or printing
-- =================================================================================
CREATE PROCEDURE invoice.InitializeInvoiceDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	DELETE IEL FROM invoice.InvoiceExpenseLog IEL WHERE IEL.InvoiceID = @InvoiceID
	DELETE IPL FROM invoice.InvoicePerDiemLog IPL WHERE IPL.InvoiceID = @InvoiceID
	DELETE ITL FROM invoice.InvoiceTimeLog ITL WHERE ITL.InvoiceID = @InvoiceID

	INSERT INTO invoice.InvoiceExpenseLog
		(InvoiceID, ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, ProjectManagerNotes)
	SELECT
		@InvoiceID,
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate, 
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO invoice.InvoicePerDiemLog
		(InvoiceID, DateWorked, ProjectLocationName, DPAAmount, DSAAmount, ISOCurrencyCode, ExchangeRate)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PPT.DSAAmount,
		PP.ISOCurrencyCode,
		PPT.Exchangerate
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO invoice.InvoiceTimeLog
		(InvoiceID, DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectManagerNotes)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.ExchangeRate,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

END
GO
--End procedure invoice.InitializeInvoiceDataByInvoiceID

-- ===================================== --
-- End Invoice Review & Print procedures --
-- ===================================== --

--Begin procedure invoice.GetMissingExchangeRateData
EXEC utility.DropObject 'invoice.GetProjectISOCurrencyCodes'
EXEC utility.DropObject 'invoice.GetMissingExchangeRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return any exchange rate data needed by an invoice but not present in the invoice.ExchangeRate table
-- =======================================================================================================================================
CREATE PROCEDURE invoice.GetMissingExchangeRateData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cISOCurrencyCodeFrom CHAR(3)

	SELECT
		@cISOCurrencyCodeFrom = PP.ISOCurrencyCode
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID
		AND PP.ProjectID = @ProjectID

	--MissingExchangeRateData
	SELECT DISTINCT
		@cISOCurrencyCodeFrom AS ISOCurrencyCodeFrom,
		PPE.ISOCurrencyCode	AS ISOCurrencyCodeTo,
		Invoice.GetExchangeRateDate(PPE.ExpenseDate) AS ExchangeRateDate
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			AND PPE.IsProjectExpense = 1
			AND PPE.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.ISOCurrencyCode <> @cISOCurrencyCodeFrom
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
						WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
						)
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeFrom = @cISOCurrencyCodeFrom
					AND ER.ISOCurrencyCodeTo = PPE.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)
	ORDER BY ExchangeRateDate, PPE.ISOCurrencyCode

	--ProjectExchangeRateData
	SELECT 
		@cISOCurrencyCodeFrom AS ISOCurrencyCodeFrom,
		core.GetSystemSetupValueBySystemSetupKey('OANDAAPIKey', '') AS OANDAAPIKey

END
GO
--End procedure invoice.GetMissingExchangeRateData

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0,
@PersonUnavailabilityDateList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cInvoiceISOCurrencyCode CHAR(3)
	DECLARE @nClientID INT

	SELECT 
		@cInvoiceISOCurrencyCode = I.ISOCurrencyCode,
		@nClientID = P.ClientID 
	FROM project.Project P 
		JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID 
			AND I.InvoiceID = @InvoiceID

	UPDATE PPE
	SET 
		PPE.ExchangeRate = 
			CASE
				WHEN PPE.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(@cInvoiceISOCurrencyCode, PPE.ISOCurrencyCode, PPE.ExpenseDate)
			END,

		PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET 
		PPT.ExchangeRate = 
			CASE
				WHEN PP.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(@cInvoiceISOCurrencyCode, PP.ISOCurrencyCode, PPT.DateWorked)
			END,

		PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	;

	EXEC person.SavePersonUnavailabilityByPersonID @PersonID, @PersonUnavailabilityDateList

	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure person.AcceptPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.AcceptPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.AcceptPersonProjectByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nPersonID INT = (SELECT PP.PersonID FROM person.PersonProject PP WHERE PP.PersonProjectID = @PersonProjectID)

	UPDATE PP
	SET PP.AcceptedDate = getDate()
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	EXEC eventlog.LogPersonProjectAction @PersonProjectID, 'update', @nPersonID, 'Assignment Accepted'

	EXEC person.GetPersonProjectEmailTemplateDataByPersonProjectID @PersonProjectID

	SELECT
		PP.ManagerEmailAddress AS EmailAddress
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID
		AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND PP2.PersonProjectID = @PersonProjectID
			AND P.EmailAddress IS NOT NULL

	ORDER BY 1

END
GO
--End procedure person.AcceptPersonProjectByPersonProjectID

--Begin procedure person.ApplyPermissionableTemplate
EXEC utility.DropObject 'person.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE person.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @nPersonID INT
	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM core.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PTP.PermissionableLineage
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM person.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			O.PersonID
		FROM @tOutput O
		
	OPEN oCursor
	FETCH oCursor INTO @nPersonID
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC eventlog.LogPersonAction @nPersonID, 'Update', @PersonID, 'Bulk permissionables assignment'

		FETCH oCursor INTO @nPersonID
		
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor		

END
GO
--End procedure person.ApplyPermissionableTemplate

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE T.ClientID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND EXISTS 
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 

					UNION 

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1
					)
				AND (@AccessCode <> 'AddUpdate' OR (T.EndDate >= getDate() AND T.AcceptedDate IS NULL))

		END
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF workflow.IsPersonInCurrentWorkflowStep(@EntityTypeCode, @EntityID, @PersonID) = 1 OR (@AccessCode <> 'View.Review' AND EXISTS (SELECT 1 FROM invoice.Invoice I WHERE I.InvoiceID = @EntityID AND I.PersonID = @PersonID))
			SET @bHasAccess = 1
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM project.Project T WHERE T.ProjectID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.ExpirePersonProjectByPersonProjectIDList
EXEC utility.DropObject 'person.ExpirePersonProjectByPersonProjectIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.ExpirePersonProjectByPersonProjectIDList

@PersonProjectIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PP
	SET PP.EndDate = getDate()
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	EXEC eventlog.LogPersonProjectAction 0, 'update', @PersonID, 'Assignment Expired', @PersonProjectIDList

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PP.PersonProjectID,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID

	SELECT
		P.EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND P.EmailAddress IS NOT NULL

	UNION

	SELECT
		PP.ManagerEmailAddress AS EmailAddress,
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonProjectID
			AND PP.ManagerEmailAddress IS NOT NULL

	UNION

	SELECT
		P.EmailAddress,
		PP2.PersonProjectID
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
		JOIN core.ListToTable(@PersonProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP2.PersonProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND P.EmailAddress IS NOT NULL

	ORDER BY 2, 1

END
GO
--End procedure person.ExpirePersonProjectByPersonProjectIDList

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button type="button" class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Accept</button>',
		'<button type="button" class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two next of kin records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND PP.AcceptedDate IS NULL
				AND PP.EndDate >= getDate()
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Accept a project engagement',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/projects">Go</a>'
			)

		END
	--ENDIF

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT 
		T.PendingAction, 
		T.Link1, 
		T.Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HasAccptedTerms,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonAccountByPersonAccountID
EXEC utility.DropObject 'person.GetPersonAccountByPersonAccountID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE person.GetPersonAccountByPersonAccountID

@PersonAccountID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonAccountID = @PersonAccountID

END
GO
--End procedure person.GetPersonAccountByPersonAccountID

--Begin procedure person.GetPersonImportCount
EXEC utility.DropObject 'person.GetPersonImportCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return a count of records in the import.Person table
-- =======================================================================================
CREATE PROCEDURE person.GetPersonImportCount

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS ItemCount
	FROM import.Person P

END
GO
--End procedure person.GetPersonImportCount

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		IsActive BIT,
		ProjectLocationID INT,
		ProjectLocationName VARCHAR(100),
		Days INT NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			0 AS FeeRate,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectTermOfReferenceID,
			'' AS ProjectTermOfReferenceName,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectLaborCodeID,
			'' AS ProjectLaborCodeName,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
		WHERE P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		T.IsActive,
		T.ProjectLocationID,
		T.ProjectLocationName,
		T.Days
	FROM @tPersonProjectLocation T
	ORDER BY T.ProjectLocationName, T.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectEmailTemplateDataByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectEmailTemplateDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.04
-- Description:	A stored procedure to get the person project data for an email template
-- ====================================================================================
CREATE PROCEDURE person.GetPersonProjectEmailTemplateDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		C.ClientName,
		CF.ClientFunctionName,
		P.ProjectName,
		(SELECT PN.EmailAddress FROM person.Person PN WHERE PN.PersonID = PP.PersonID) AS EmailAddress,
		person.FormatPersonNameByPersonID(PP.PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			AND PP.PersonProjectID = @PersonProjectID

END
GO
--End procedure person.GetPersonProjectEmailTemplateDataByPersonProjectID

--Begin procedure person.GetPersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.GetPersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return data from the person.PersonProjectExpense table
-- =========================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectID INT = ISNULL((SELECT PPE.PersonProjectID FROM person.PersonProjectExpense PPE WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID), 0)

	--PersonProjectExpense
	SELECT
		C.CurrencyName,
		C.ISOCurrencyCode,
		CF.ClientFunctionName,
		P.ProjectName,
		PM.PaymentMethodID, 
		PM.PaymentMethodName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPE.ClientCostCodeID,
		project.GetProjectCostCodeDescriptionByClientCostCodeID(PPE.ClientCostCodeID) AS ProjectCostCodeDescription,
		PPE.ExpenseAmount,
		PPE.ExpenseDate,
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		PPE.IsProjectExpense,
		PPE.OwnNotes,
		PPE.PersonProjectExpenseID,
		PPE.PersonProjectID,
		PPE.ProjectManagerNotes,
		PPE.TaxAmount,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PPE.ISOCurrencyCode
		JOIN dropdown.PaymentMethod PM ON PM.PaymentMethodID = PPE.PaymentMethodID
			AND PPE.PersonProjectExpenseID = @PersonProjectExpenseID

	--PersonProjectExpenseClientCostCode & PersonProjectExpenseProjectCurrency
	EXEC person.GetPersonProjectExpenseDataByPersonProjectID @nPersonProjectID

	--PersonProjectExpenseDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'PersonProjectExpense' 
			AND DE.EntityID = @PersonProjectExpenseID

END
GO
--End procedure person.GetPersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.GetPersonProjectsByPersonID
EXEC utility.DropObject 'person.GetPersonProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.PersonProjectID,
		CF.ClientFunctionName,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectID, PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID, CF.ClientFunctionName, CF.ClientFunctionID

END
GO
--End procedure person.GetPersonProjectsByPersonID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.GetPersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.GetPersonUnavailabilityByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.30
-- Description:	A stored procedure to get data from the person.PersonUnavailability table
-- ======================================================================================
CREATE PROCEDURE person.GetPersonUnavailabilityByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--PersonUnavailability
	SELECT
		FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS PersonUnavailabilityDate
	FROM person.PersonUnavailability PU
	WHERE PU.PersonID = @PersonID
	ORDER BY PU.PersonUnavailabilityDate

END
GO
--End procedure person.GetPersonUnavailabilityByPersonID

--Begin procedure person.ResolvePendingAction
EXEC utility.DropObject 'person.ResolvePendingAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.31
-- Description:	A stored procedure to manage data in the client.ClientPerson table
-- ===============================================================================
CREATE PROCEDURE person.ResolvePendingAction

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = 'AcceptClientPerson'
		UPDATE CP SET CP.AcceptedDate = getDate() FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	ELSE
		DELETE CP FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	--ENDIF

END
GO
--End procedure person.ResolvePendingAction

--Begin procedure person.SavePersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.SavePersonUnavailabilityByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.30
-- Description:	A stored procedure to save data to the person.PersonUnavailability table
-- =====================================================================================
CREATE PROCEDURE person.SavePersonUnavailabilityByPersonID

@PersonID INT = 0,
@PersonUnavailabilityDateList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PU
	FROM person.PersonUnavailability PU
	WHERE PU.PersonID = @PersonID

	IF @PersonUnavailabilityDateList IS NOT NULL
		BEGIN

		INSERT INTO person.PersonUnavailability
			(PersonID, PersonUnavailabilityDate)
		SELECT
			@PersonID,
			CAST(LTT.ListItem AS DATE)
		FROM core.ListToTable(@PersonUnavailabilityDateList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure person.SavePersonUnavailabilityByPersonID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.GetProjects
EXEC utility.DropObject 'project.GetProjects'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the project.Project table
-- =============================================================================
CREATE PROCEDURE project.GetProjects

@PersonID INT,
@IsActive INT = -1,
@ClientPersonRoleCodeList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientID,
		C.ClientName,
		P.ProjectID,
		P.ProjectName
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND (@IsActive = -1 OR P.IsActive = @IsActive)
			AND EXISTS
				(
				SELECT 1
				FROM client.ClientPerson CP
				WHERE CP.ClientID = P.ClientID
					AND CP.PersonID = @PersonID
					AND 
						(
						@ClientPersonRoleCodeList IS NULL
							OR EXISTS
								(
								SELECT 1
								FROM core.ListToTable(@ClientPersonRoleCodeList, ',') LTT
								WHERE LTT.ListItem = CP.ClientPersonRoleCode
								)
						)

				UNION

				SELECT 1
				FROM person.Person P
				WHERE P.PersonID = @PersonID
					AND P.IsSuperAdministrator = 1
				)
	ORDER BY C.ClientName, P.ProjectName

END
GO
--End procedure project.GetProjects

--Begin procedure project.ValidateHoursWorked
EXEC utility.DropObject 'project.ValidateHoursWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.25
-- Description:	A stored procedure to validate HoursWorked from a PersonProjectID
-- ==============================================================================
CREATE PROCEDURE project.ValidateHoursWorked

@HoursWorked INT,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectTimeAllotted INT
	DECLARE @nPersonProjectTimeExpended INT

	SELECT
		@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PL.ProjectID
			AND PPL.PersonProjectID = @PersonProjectID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID

	SELECT
		CASE
			WHEN ISNULL(@nPersonProjectTimeAllotted, 0) >= ISNULL(@nPersonProjectTimeExpended, 0) + @HoursWorked
			THEN 1
			ELSE 0
		END AS IsValid

END
GO
--End procedure project.ValidateHoursWorked