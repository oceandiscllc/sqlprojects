USE DeployAdviser
GO

--Begin function invoice.GetExchangeRate
EXEC utility.DropObject 'invoice.GetExchangeRate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2017.11.05
-- Description:	A function to return an exchange rate
-- ==================================================

CREATE FUNCTION invoice.GetExchangeRate
(
@ISOCurrencyCodeFrom CHAR(3),
@ISOCurrencyCodeTo CHAR(3),
@SourceDate DATE
)

RETURNS NUMERIC(18,5)

AS
BEGIN

	DECLARE @ExchangeRate NUMERIC(18,5) = 1

	IF @ISOCurrencyCodeFrom <> @ISOCurrencyCodeTo
		BEGIN

		SELECT @ExchangeRate = ER.ExchangeRate
		FROM invoice.ExchangeRate ER
		WHERE ER.ISOCurrencyCodeFrom = @ISOCurrencyCodeFrom
			AND ER.ISOCurrencyCodeTo = @ISOCurrencyCodeTo
			AND ER.ExchangeRateDate = invoice.GetExchangeRateDate(@SourceDate)

		END
	--ENDIF

	RETURN ISNULL(@ExchangeRate, 1)
	
END
GO
--End function invoice.GetExchangeRate

--Begin function invoice.GetExchangeRateDate
EXEC utility.DropObject 'invoice.GetExchangeRateDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.05
-- Description:	A function to return the exchange rate date to use based on a date
-- ===============================================================================

CREATE FUNCTION invoice.GetExchangeRateDate
(
@SourceDate DATE
)

RETURNS DATE

AS
BEGIN

	DECLARE @ExchangeRateDate DATE

	SELECT 
		@ExchangeRateDate = 
			CASE 
				WHEN DATEPART(WEEKDAY, @SourceDate) > 5 
				THEN DATEADD(DAY, 4, DATEADD(WEEK, DATEDIFF(WEEK, 0, @SourceDate), 0)) 
				ELSE DATEADD(DAY, -3, DATEADD(WEEK, DATEDIFF(WEEK, 0, @SourceDate), 0)) 
			END

	RETURN FORMAT(@ExchangeRateDate, 'yyyy-MM-dd')
	
END
GO
--End function invoice.GetExchangeRateDate

--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID)
	SELECT 
		PP.ProjectID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID
	FROM project.ProjectPerson PP
	WHERE PP.ProjectPersonRoleCode = 'ProjectManager'
		AND PP.PersonID = @PersonID

	UNION

	SELECT 
		P.ProjectID
	FROM project.Project P
	WHERE EXISTS	
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.PersonID = @PersonID
		)

	UNION

	SELECT 
		PJ.ProjectID
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.28
-- Description:	A function to determine if a PeronID is in the current workflow step of a a specific EntityTypeCode and EntityID
-- =============================================================================================================================
CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
			AND EWSGP.PersonID = @PersonID
		)
		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep
	
END
GO
--End function workflow.IsPersonInCurrentWorkflowStep
