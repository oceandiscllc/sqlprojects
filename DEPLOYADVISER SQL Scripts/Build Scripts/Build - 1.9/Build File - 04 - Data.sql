USE DeployAdviser
GO

--Begin table core.SystemSetup
EXEC core.SystemSetupKeyAddUpdate 'SaveWarning', 'The warning message displayed on the user profile page when there is unseaved data', '<strong>Warning!</strong> Please click the Save button to save your changes.'
GO
--End table core.SystemSetup

