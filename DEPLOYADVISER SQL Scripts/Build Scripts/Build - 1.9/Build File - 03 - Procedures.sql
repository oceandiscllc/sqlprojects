USE DeployAdviser
GO

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonAccounts VARCHAR(MAX) = ''
		
		SELECT @cPersonAccounts = COALESCE(@cPersonAccounts, '') + D.PersonAccount
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonAccount'), ELEMENTS) AS PersonAccount
			FROM person.PersonAccount T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonLanguages VARCHAR(MAX) = ''
		
		SELECT @cPersonLanguages = COALESCE(@cPersonLanguages, '') + D.PersonLanguage
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonLanguage'), ELEMENTS) AS PersonLanguage
			FROM person.PersonLanguage T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonNextOfKin VARCHAR(MAX) = ''
		
		SELECT @cPersonNextOfKin = COALESCE(@cPersonNextOfKin, '') + D.PersonNextOfKin
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonNextOfKin'), ELEMENTS) AS PersonNextOfKin
			FROM person.PersonNextOfKin T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPasswordSecurity VARCHAR(MAX) = ''
		
		SELECT @cPersonPasswordSecurity = COALESCE(@cPersonPasswordSecurity, '') + D.PersonPasswordSecurity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPasswordSecurity'), ELEMENTS) AS PersonPasswordSecurity
			FROM person.PersonPasswordSecurity T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonProofOfLife VARCHAR(MAX) = ''
		
		SELECT @cPersonProofOfLife = COALESCE(@cPersonProofOfLife, '') + D.PersonProofOfLife
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProofOfLife'), ELEMENTS) AS PersonProofOfLife
			FROM person.PersonProofOfLife T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<Accounts>' + ISNULL(@cPersonAccounts, '') + '</Accounts>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Languages>' + ISNULL(@cPersonLanguages, '') + '</Languages>' AS XML),
			CAST('<NextOfKin>' + ISNULL(@cPersonNextOfKin, '') + '</NextOfKin>' AS XML),
			CAST('<PasswordSecurity>' + ISNULL(@cPersonPasswordSecurity, '') + '</PasswordSecurity>' AS XML),
			CAST('<Permissionables>' + ISNULL(@cPersonPermissionables, '') + '</Permissionables>' AS XML),
			CAST('<ProofOfLife>' + ISNULL(@cPersonProofOfLife, '') + '</ProofOfLife>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure person.DeletePersonByPersonID
EXEC utility.DropObject 'person.DeletePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.04
-- Description:	A stored procedure to delete a person from the database
-- ====================================================================
CREATE PROCEDURE person.DeletePersonByPersonID

@TargetPersonID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	EXEC eventlog.LogPersonAction @TargetPersonID, 'Delete', @PersonID

	DELETE P FROM person.Person P WHERE P.PersonID = @TargetPersonID
	DELETE T FROM client.ClientPerson T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM eventlog.EventLog T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM import.Person T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM invoice.Invoice T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.Person T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonAccount T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonLanguage T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonNextOfKin T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonPasswordSecurity T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonPermissionable T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonProject T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonProofOfLife T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM person.PersonUnavailability T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM project.ProjectPerson T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM reporting.SearchResult T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM workflow.EntityWorkflowStepGroupPerson T WHERE T.PersonID = @TargetPersonID
	DELETE T FROM workflow.WorkflowStepGroupPerson T WHERE T.PersonID = @TargetPersonID

END
GO
--End procedure person.DeletePersonByPersonID

--Begin procedure person.GetPersonProjectsByPersonID
EXEC utility.DropObject 'person.GetPersonProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CF.ClientFunctionName,
		P.HoursPerDay,
		P.IsOvertimeAllowed,
		P.ProjectID,
		P.ProjectName,
		PP.PersonProjectID,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectID, PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID, CF.ClientFunctionName, CF.ClientFunctionID

END
GO
--End procedure person.GetPersonProjectsByPersonID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		P.HoursPerDay,
		P.IsOvertimeAllowed,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.InvoiceID,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.IsOvertimeAllowed,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.SaveProjectData
EXEC utility.DropObject 'project.SaveProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.01
-- Description:	A stored procedure to perfome post-save cleanup on associated project data
-- =======================================================================================
CREATE PROCEDURE project.SaveProjectData

@ProjectID INT,
@PersonID INT,
@ProjectTermOfReferenceIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectID INT
	DECLARE @nPersonProjectID INT
	DECLARE @tOutput TABLE (PersonProjectLocationID INT NOT NULL PRIMARY KEY)

	DELETE PCC
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
		JOIN project.Project P ON P.ProjectID = PCC.ProjectID
			AND P.ClientID <> CCC.ClientID

	DELETE PTOR
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
		AND
			(
			@ProjectTermOfReferenceIDList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@ProjectTermOfReferenceIDList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PTOR.ProjectTermOfReferenceID
					)
			)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			PP.ProjectID,
			PP.PersonProjectID
		FROM person.PersonProject PP
		ORDER BY 1, 2
		
	OPEN oCursor
	FETCH oCursor INTO @nProjectID, @nPersonProjectID
	WHILE @@fetch_status = 0
		BEGIN
		
		INSERT INTO person.PersonProjectLocation
			(PersonProjectID, ProjectLocationID)
		OUTPUT INSERTED.PersonProjectLocationID INTO @tOutput
		SELECT
			@nPersonProjectID,
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @nProjectID
			AND NOT EXISTS
				(
				SELECT 1
				FROM person.PersonProjectLocation PPL
				WHERE PPL.PersonProjectID = @nPersonProjectID
					AND PPL.ProjectLocationID = PL.ProjectLocationID
				)

		IF EXISTS (SELECT 1 FROM @tOutput)
			EXEC eventlog.LogPersonProjectAction @nPersonProjectID, 'Update', @PersonID
		--ENDIF

		DELETE T FROM @tOutput T

		FETCH oCursor INTO @nProjectID, @nPersonProjectID
		
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure project.SaveProjectData
