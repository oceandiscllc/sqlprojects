USE DeployAdviser
GO

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'IsOvertimeAllowed', 'BIT', '1'
GO
--End table project.Project

