--Begin table eventlog.EventLog
UPDATE EL
SET EL.EventCode = 'LineManagerApproved'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'LineManagerApprove'
GO

UPDATE EL
SET EL.EventCode = 'LineManagerRejected'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'LineManagerReject'
GO

UPDATE EL
SET EL.EventCode = 'update'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'manage'
GO
--End table eventlog.EventLog

--Begin table dropdown.ClientNoticeType
TRUNCATE TABLE dropdown.ClientNoticeType
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientNoticeType', 'ClientNoticeTypeID', 0
GO

INSERT INTO dropdown.ClientNoticeType 
	(ClientNoticeTypeCode, ClientNoticeTypeName, DisplayOrder, Icon) 
VALUES
('Event', 'Event', 0, 'fa-lg fa-ticket'),
('Job', 'Job Vacancy', 0, 'fa-lg fa-suitcase'),
('News', 'News', 0, 'fa-lg fa-rss-square'),
('Other', 'Other', 99, 'fa-lg fa-magic'),
('Policy', 'Policy Update', 0, 'fa-lg fa-book'),
('Security', 'Security Alert', 0, 'fa-lg fa-shield'),
('Training', 'Training Opportunity', 0, 'fa-lg fa-mortar-board')
GO
--End table dropdown.ClientNoticeType

--Begin menu items
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-info-circle',
	@NewMenuItemCode = 'ClientNoticeList',
	@NewMenuItemLink = '/clientnotice/list',
	@NewMenuItemText = 'Noticeboard'
GO
--End menu items

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='Add / edit a client notice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='View a client notice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='View the list of client notices', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable
