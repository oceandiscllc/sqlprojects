--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode IN ('add', 'create')
			THEN 'Add'
			WHEN @EventCode IN ('addupdate', 'manage', 'update')
			THEN 'Update'
			WHEN @EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN @EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Invalid Login Attempt'
			WHEN @EventCode = 'forecast'
			THEN 'View Forecast'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN @EventCode LIKE 'linemanagerapprove%'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN @EventCode LIKE 'linemanagerreject%'
			THEN 'Line Manager Rejected Invoice'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode IN ('read', 'view')
			THEN 'View'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			ELSE @EventCode
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.GetClientsByPersonID
EXEC utility.DropObject 'person.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all clients accessible to a PersonID
-- ====================================================================================================

CREATE FUNCTION person.GetClientsByPersonID
(
@PersonID INT,
@ClientPersonRoleCode VARCHAR(250)
)

RETURNS @tTable TABLE (ClientID INT NOT NULL PRIMARY KEY, ClientName VARCHAR(250), IntegrationCode VARCHAR(50))  

AS
BEGIN

	SET @ClientPersonRoleCode = core.NullIfEmpty(@ClientPersonRoleCode)

	INSERT INTO @tTable
		(ClientID, ClientName, IntegrationCode)
	SELECT 
		CP.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NOT NULL
			AND 
				(
				@ClientPersonRoleCode IS NULL
					OR EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@ClientPersonRoleCode, ',') LTT
						WHERE LTT.ListItem = CP.ClientPersonRoleCode
						)
				)

	UNION

	SELECT 
		C.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.Client C
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person P
		WHERE P.PersonID = @PersonID
			AND P.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetClientsByPersonID

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonPermissionable PP
		WHERE PP.PermissionableLineage = @PermissionableLineage 
			AND PP.PersonID = @PersonID 

		UNION 

		SELECT 1 
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage 
			AND P.IsGlobal = 1
		)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.IsProfileUpdateRequired
EXEC utility.DropObject 'person.IsProfileUpdateRequired'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.24
-- Description:	A function to return data from the person.Person table
-- ===================================================================
CREATE FUNCTION person.IsProfileUpdateRequired
(
@PersonID INT,
@IsTwoFactorEnabled BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsProfileUpdateRequired BIT

	SELECT @bIsProfileUpdateRequired = 
		CASE
			WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
				OR core.NullIfEmpty(P.FirstName) IS NULL
				OR core.NullIfEmpty(P.LastName) IS NULL
				OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				OR P.HasAcceptedTerms = 0
				OR (@IsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
				OR (@IsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
				OR (@IsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
			THEN 1
			ELSE 0
		END

	FROM person.Person P
	WHERE P.PersonID = @PersonID

	RETURN ISNULL(@bIsProfileUpdateRequired, 1)

END
GO
--End function project.IsProfileUpdateRequired