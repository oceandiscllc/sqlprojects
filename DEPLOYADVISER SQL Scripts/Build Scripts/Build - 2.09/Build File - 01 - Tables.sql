--Begin table client.ClientFinanceExport
DECLARE @TableName VARCHAR(250) = 'client.ClientFinanceExport'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientFinanceExport
	(
	ClientFinanceExportID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	PersonID INT,
	ExportDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExportDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientFinanceExportID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientFinanceExport', 'ClientID,ExportDateTime DESC'
GO
--End table client.ClientFinanceExport

--Begin table client.ClientNotice
DECLARE @TableName VARCHAR(250) = 'client.ClientNotice'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientNotice
	(
	ClientNoticeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientNoticeName VARCHAR(250),
	ClientNoticeSynopsis VARCHAR(500),
	ClientNoticeTypeID INT,
	StartDate DATE,
	EndDate DATE,
	ClientNoticeDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientNoticeTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientNoticeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientNotice', 'ClientID,StartDate,ClientNoticeName'
GO
--End table client.ClientNotice

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.AddColumn @TableName, 'NotifyEmailAddress', 'VARCHAR(320)'
GO
--End table client.ClientPerson

--Begin table dropdown.ClientNoticeType
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientNoticeType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientNoticeType
	(
	ClientNoticeTypeID INT IDENTITY(0,1) NOT NULL,
	ClientNoticeTypeCode VARCHAR(50),
	ClientNoticeTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientNoticeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientNoticeType', 'DisplayOrder,ClientNoticeTypeName'
GO
--End table dropdown.ClientNoticeType

--Begin table dropdown.ProjectRole
EXEC utility.DropObject 'dropdown.ProjectRole'
GO
--End table dropdown.ProjectRole

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'ClientFinanceExportID', 'INT', '0'
GO
--End table invoice.Invoice

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.DropColumn @TableName, 'ProjectRoleID'
GO
--End table person.PersonProject
