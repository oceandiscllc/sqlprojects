-- File Name:	Build - 2.9 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.9 - 2018.11.25 15.18.30

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Tables:
--		client.ClientFinanceExport
--		client.ClientNotice
--		dropdown.ClientNoticeType
--
-- Functions:
--		eventlog.GetEventNameByEventCode
--		person.GetClientsByPersonID
--		person.HasPermission
--		person.IsProfileUpdateRequired
--
-- Procedures:
--		client.GetClientByClientID
--		client.GetClientNoticeByClientNoticeID
--		client.GetClientNoticesByPersonID
--		core.GetMenuItemsByPersonID
--		dropdown.GetClientNoticeTypeData
--		eventlog.LogClientNoticeAction
--		person.AcceptClientPersonByClientPersonID
--		person.CheckAccess
--		person.RejectClientPersonByClientPersonID
--		person.ValidateLogin
--		project.GetProjectsByPersonID
--		project.validateProjectDateAndTimeData
--		reporting.GetPersonProjectList
--		utility.ClonePasswordData
--		utility.RevertPasswordData
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin table client.ClientFinanceExport
DECLARE @TableName VARCHAR(250) = 'client.ClientFinanceExport'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientFinanceExport
	(
	ClientFinanceExportID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	PersonID INT,
	ExportDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExportDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientFinanceExportID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientFinanceExport', 'ClientID,ExportDateTime DESC'
GO
--End table client.ClientFinanceExport

--Begin table client.ClientNotice
DECLARE @TableName VARCHAR(250) = 'client.ClientNotice'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientNotice
	(
	ClientNoticeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientNoticeName VARCHAR(250),
	ClientNoticeSynopsis VARCHAR(500),
	ClientNoticeTypeID INT,
	StartDate DATE,
	EndDate DATE,
	ClientNoticeDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientNoticeTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientNoticeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientNotice', 'ClientID,StartDate,ClientNoticeName'
GO
--End table client.ClientNotice

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.AddColumn @TableName, 'NotifyEmailAddress', 'VARCHAR(320)'
GO
--End table client.ClientPerson

--Begin table dropdown.ClientNoticeType
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientNoticeType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientNoticeType
	(
	ClientNoticeTypeID INT IDENTITY(0,1) NOT NULL,
	ClientNoticeTypeCode VARCHAR(50),
	ClientNoticeTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientNoticeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientNoticeType', 'DisplayOrder,ClientNoticeTypeName'
GO
--End table dropdown.ClientNoticeType

--Begin table dropdown.ProjectRole
EXEC utility.DropObject 'dropdown.ProjectRole'
GO
--End table dropdown.ProjectRole

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'ClientFinanceExportID', 'INT', '0'
GO
--End table invoice.Invoice

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.DropColumn @TableName, 'ProjectRoleID'
GO
--End table person.PersonProject

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode IN ('add', 'create')
			THEN 'Add'
			WHEN @EventCode IN ('addupdate', 'manage', 'update')
			THEN 'Update'
			WHEN @EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN @EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Invalid Login Attempt'
			WHEN @EventCode = 'forecast'
			THEN 'View Forecast'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN @EventCode LIKE 'linemanagerapprove%'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN @EventCode LIKE 'linemanagerreject%'
			THEN 'Line Manager Rejected Invoice'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode IN ('read', 'view')
			THEN 'View'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			ELSE @EventCode
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.GetClientsByPersonID
EXEC utility.DropObject 'person.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all clients accessible to a PersonID
-- ====================================================================================================

CREATE FUNCTION person.GetClientsByPersonID
(
@PersonID INT,
@ClientPersonRoleCode VARCHAR(250)
)

RETURNS @tTable TABLE (ClientID INT NOT NULL PRIMARY KEY, ClientName VARCHAR(250), IntegrationCode VARCHAR(50))  

AS
BEGIN

	SET @ClientPersonRoleCode = core.NullIfEmpty(@ClientPersonRoleCode)

	INSERT INTO @tTable
		(ClientID, ClientName, IntegrationCode)
	SELECT 
		CP.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NOT NULL
			AND 
				(
				@ClientPersonRoleCode IS NULL
					OR EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@ClientPersonRoleCode, ',') LTT
						WHERE LTT.ListItem = CP.ClientPersonRoleCode
						)
				)

	UNION

	SELECT 
		C.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.Client C
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person P
		WHERE P.PersonID = @PersonID
			AND P.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetClientsByPersonID

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonPermissionable PP
		WHERE PP.PermissionableLineage = @PermissionableLineage 
			AND PP.PersonID = @PersonID 

		UNION 

		SELECT 1 
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage 
			AND P.IsGlobal = 1
		)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.IsProfileUpdateRequired
EXEC utility.DropObject 'person.IsProfileUpdateRequired'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.24
-- Description:	A function to return data from the person.Person table
-- ===================================================================
CREATE FUNCTION person.IsProfileUpdateRequired
(
@PersonID INT,
@IsTwoFactorEnabled BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsProfileUpdateRequired BIT

	SELECT @bIsProfileUpdateRequired = 
		CASE
			WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
				OR core.NullIfEmpty(P.FirstName) IS NULL
				OR core.NullIfEmpty(P.LastName) IS NULL
				OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				OR P.HasAcceptedTerms = 0
				OR (@IsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
				OR (@IsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
				OR (@IsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
			THEN 1
			ELSE 0
		END

	FROM person.Person P
	WHERE P.PersonID = @PersonID

	RETURN ISNULL(@bIsProfileUpdateRequired, 1)

END
GO
--End function project.IsProfileUpdateRequired
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tClientPersonProjectRole TABLE (ClientPersonProjectRoleGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID(), ClientPersonProjectRoleID INT)

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientLegalEntities,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.FixedAllowanceLabel,
		C.HasFixedAllowance,
		C.HasVariableAllowance,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.MaximumProductiveDays,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.VariableAllowanceLabel,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientCustomer
	SELECT
		newID() AS ClientCustomerGUID,
		CC.ClientCustomerID,
		CC.ClientCustomerName,
		CC.IsActive
	FROM client.ClientCustomer CC
	WHERE CC.ClientID = @ClientID
	ORDER BY CC.ClientCustomerName, CC.ClientCustomerID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	INSERT INTO @tClientPersonProjectRole 
		(ClientPersonProjectRoleID)
	SELECT
		CPPR.ClientPersonProjectRoleID
	FROM client.ClientPersonProjectRole CPPR
	WHERE CPPR.ClientID = @ClientID
		AND CPPR.ClientPersonProjectRoleID > 0

	--ClientLeave
	EXEC client.GetClientLeave @ClientID

	--ClientPerson
	SELECT DISTINCT
		CP.PayrollNumber,
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Consultant'
	ORDER BY 3, 2

	--ClientPersonProjectRole
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPR.ClientPersonProjectRoleID,
		CPPR.ClientPersonProjectRoleName,
		CPPR.FinanceCode,
		CPPR.HasFees,
		CPPR.HasFixedAllowance,
		CPPR.HasVariableAllowance,
		CPPR.HasVAT,
		CPPR.InvoiceRDLCode,
		CPPR.IsActive
	FROM client.ClientPersonProjectRole CPPR
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPR.ClientPersonProjectRoleID
	ORDER BY CPPR.ClientPersonProjectRoleName, CPPR.ClientPersonProjectRoleID

	--ClientPersonProjectRoleClientTimeType
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPRCTT.ClientTimeTypeID,
		CPPRCTT.LeaveDaysPerAnnum
	FROM client.ClientPersonProjectRoleClientTimeType CPPRCTT
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPRCTT.ClientPersonProjectRoleID
	ORDER BY CPPRCTT.ClientPersonProjectRoleID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	IF @ClientID > 0
		BEGIN

		--ClientTimeType
		SELECT
			CTT.AccountCode,
			CTT.ClientTimeTypeID,
			CTT.HasFixedAllowance,
			CTT.HasLocation,
			CTT.HasVariableAllowance,
			CTT.IsAccruable,
			CTT.IsActive,
			CTT.IsBalancePayableUponTermination,
			CTT.IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM client.ClientTimeType CTT
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
				AND CTT.ClientID = @ClientID
				AND TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	ELSE
		BEGIN

		--ClientTimeType
		SELECT
			NULL AS AccountCode,
			TT.TimeTypeID AS ClientTimeTypeID,
			0 AS HasFixedAllowance,
			0 AS HasLocation,
			0 AS HasVariableAllowance,
			0 AS IsAccruable,
			0 AS IsActive,
			0 AS IsBalancePayableUponTermination,
			0 AS IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM dropdown.TimeType TT
		WHERE TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	--ENDIF

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetClientNoticeByClientNoticeID
EXEC utility.DropObject 'client.GetClientNoticeByClientNoticeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.27
-- Description:	A stored procedure to return data from the client.ClientNotice table based on a ClientNoticeID
-- ===========================================================================================================
CREATE PROCEDURE client.GetClientNoticeByClientNoticeID

@ClientNoticeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ClientNotice
	SELECT
		C.ClientID,
		C.ClientName,
		CN.ClientNoticeDescription,
		CN.ClientNoticeID,
		CN.ClientNoticeName,
		CN.ClientNoticeSynopsis,
		CN.EndDate,
		core.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.StartDate,
		core.FormatDate(CN.StartDate) AS StartDateFormatted,
		CNT.ClientNoticeTypeID,
		CNT.ClientNoticeTypeName
	FROM client.ClientNotice CN
		JOIN client.Client C ON C.ClientID = CN.ClientID
		JOIN dropdown.ClientNoticeType CNT ON CNT.ClientNoticeTypeID = CN.ClientNoticeTypeID
			AND CN.ClientNoticeID = @ClientNoticeID

	--ClientNoticeDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ClientNotice'
			AND DE.EntityTypeSubCode = 'Document'
			AND DE.EntityID = @ClientNoticeID

	--ClientNoticeImage
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ClientNotice'
			AND DE.EntityTypeSubCode = 'Image'
			AND DE.EntityID = @ClientNoticeID

END
GO
--End procedure client.GetClientNoticeByClientNoticeID

--Begin procedure client.GetClientNoticesByPersonID
EXEC utility.DropObject 'client.GetClientNoticesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.27
-- Description:	A stored procedure to return data from the client.ClientNotice table based on a PersonID
-- =====================================================================================================
CREATE PROCEDURE client.GetClientNoticesByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ClientNotice
	SELECT TOP 5
		C.ClientName,
		core.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.ClientNoticeID,
		CN.ClientNoticeName,
		DATEDIFF(d, CN.StartDate, getDate()) AS Age,
		CN.ClientNoticeSynopsis,
		CNT.ClientNoticeTypeName,
		CNT.Icon,
		(SELECT D.DocumentGUID FROM document.DocumentEntity DE JOIN document.Document D ON D.DocumentID = DE.DocumentID AND DE.EntityTypeCode = 'ClientNotice' AND DE.EntityTypeSubCode = 'Image' AND DE.EntityID = CN.ClientNoticeID) AS DocumentGUID
	FROM client.ClientNotice CN
		JOIN client.Client C ON C.ClientID = CN.ClientID
		JOIN dropdown.ClientNoticeType CNT ON CNT.ClientNoticeTypeID = CN.ClientNoticeTypeID
		JOIN person.GetClientsByPersonID(@PersonID, NULL) CBP ON CBP.ClientID = CN.ClientID
			AND CN.StartDate <= getDate()
			AND CN.EndDate >= getDate()
	ORDER BY CN.StartDate DESC, CN.ClientID, CN.ClientNoticeName, CN.ClientNoticeID

END
GO
--End procedure client.GetClientNoticesByPersonID

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- ===============================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'

						UNION

						SELECT 1
						FROM permissionable.Permissionable P
							JOIN core.MenuItemPermissionableLineage MIPL ON MIPL.PermissionableLineage = P.PermissionableLineage
								AND MIPL.MenuItemID = MI.MenuItemID
								AND P.IsGlobal = 1
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		MI.IsForNewTab,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure dropdown.GetClientNoticeTypeData
EXEC Utility.DropObject 'dropdown.GetClientNoticeTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.27
-- Description:	A stored procedure to return data from the dropdown.ClientNoticeType table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetClientNoticeTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientNoticeTypeID,
		T.ClientNoticeTypeCode,
		T.ClientNoticeTypeName
	FROM dropdown.ClientNoticeType T
	WHERE (T.ClientNoticeTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientNoticeTypeName, T.ClientNoticeTypeID

END
GO
--End procedure dropdown.GetClientNoticeTypeData

--Begin procedure dropdown.GetProjectRoleData
EXEC utility.DropObject 'dropdown.GetProjectRoleData'
GO
--End table dropdown.GetProjectRoleData

--Begin procedure eventlog.LogClientNoticeAction
EXEC utility.DropObject 'eventlog.LogClientNoticeAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.10.31
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientNoticeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ClientNotice'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ClientNotice'), ELEMENTS
			)
		FROM client.ClientNotice T
		WHERE T.ClientNoticeID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientNoticeAction

--Begin procedure person.AcceptClientPersonByClientPersonID
EXEC utility.DropObject 'person.AcceptClientPersonByClientPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.04.10
-- Description:	A stored procedure to accept a client engagement based on a ClientPersonID
-- =======================================================================================
CREATE PROCEDURE person.AcceptClientPersonByClientPersonID

@ClientPersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ClientName VARCHAR(500), EmailAddress VARCHAR(320), PersonNameFormatted VARCHAR(250), RoleName VARCHAR(50))

	INSERT INTO @tTable
		(ClientName, EmailAddress, PersonNameFormatted, RoleName)
	SELECT
		C.ClientName,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM person.Person P
		JOIN client.ClientPerson CP ON CP.CreatePersonID = P.PersonID
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @ClientPersonID

	UNION

	SELECT
		C.ClientName,
		CP.NotifyEmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @ClientPersonID

	UPDATE CP
	SET AcceptedDate = getDate()
	FROM client.ClientPerson CP
	WHERE CP.ClientPersonID = @ClientPersonID

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		T.ClientName,
		T.EmailAddress,
		T.PersonNameFormatted,
		T.RoleName
	FROM @tTable T
	WHERE T.EmailAddress IS NOT NULL
	ORDER BY T.EmailAddress

END
GO
--End procedure person.AcceptClientPersonByClientPersonID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Administrator')) OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR PBP.RoleCode = 'Administrator'
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID AND T.RoleCode IN ('Administrator', 'ProjectManager') OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.RejectClientPersonByClientPersonID
EXEC utility.DropObject 'person.RejectClientPersonByClientPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.12
-- Description:	A stored procedure to Reject a client engagement based on a ClientPersonID
-- =======================================================================================
CREATE PROCEDURE person.RejectClientPersonByClientPersonID

@ClientPersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ClientName VARCHAR(500), EmailAddress VARCHAR(320), PersonNameFormatted VARCHAR(250), RoleName VARCHAR(50))

	INSERT INTO @tTable
		(ClientName, EmailAddress, PersonNameFormatted, RoleName)
	SELECT
		C.ClientName,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM person.Person P
		JOIN client.ClientPerson CP ON CP.CreatePersonID = P.PersonID
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @ClientPersonID

	UNION

	SELECT
		C.ClientName,
		CP.NotifyEmailAddress,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted,
		R.RoleName
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @ClientPersonID

	DELETE CP
	FROM client.ClientPerson CP
	WHERE CP.ClientPersonID = @ClientPersonID

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailFrom,
		T.ClientName,
		T.EmailAddress,
		T.PersonNameFormatted,
		T.RoleName
	FROM @tTable T
	WHERE T.EmailAddress IS NOT NULL
	ORDER BY T.EmailAddress

END
GO
--End procedure person.RejectClientPersonByClientPersonID

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,
		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND core.NullIfEmpty(P.SummaryBiography) IS NULL THEN ',SummaryBiography' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.EntityTypeCode = 'Person' AND DE.EntityTypeSubCode = 'CV' AND DE.EntityID = P.PersonID) THEN ',CV' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END
	SET @cRequiredProfileUpdate = CASE WHEN LEFT(@cRequiredProfileUpdate, 1) = ',' THEN STUFF(@cRequiredProfileUpdate, 1, 1, '') ELSE @cRequiredProfileUpdate END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			person.IsProfileUpdateRequired(@nPersonID, @bIsTwoFactorEnabled),
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure project.GetProjectsByPersonID
EXEC utility.DropObject 'project.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.15
-- Description:	A stored procedure to return data from the project.Project table based on a PersonID
-- =================================================================================================
CREATE PROCEDURE project.GetProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProjectID,
		P.ProjectName
	FROM project.Project P
	WHERE EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP
		WHERE PP.PersonID = @PersonID
			AND PP.IsActive = 1
			AND PP.IsVisible = 1
			AND PP.ProjectID = P.ProjectID
			AND PP.AcceptedDate IS NOT NULL				
		)
	ORDER BY P.ProjectName, P.ProjectID

END
GO
--End procedure project.GetProjectsByPersonID

--Begin procedure project.validateProjectDateAndTimeData
EXEC utility.DropObject 'project.validateProjectDateAndTimeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.29
-- Description:	A stored procedure to validate a dates and hours worked on a deployment
-- ====================================================================================
CREATE PROCEDURE project.validateProjectDateAndTimeData

@DateWorkedList VARCHAR(MAX) = NULL,
@HoursWorked NUMERIC(18,2) = 0,
@PersonProjectID INT,
@ProjectLocationID INT,
@TimeTypeID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsValidated BIT

	DECLARE @cTimeTypeCode VARCHAR(50)

	DECLARE @dEndDate DATE
	DECLARE @dStartDate DATE

	DECLARE @nDateWorkedCount INT
	DECLARE @nHoursPerDay NUMERIC(18,2)
	DECLARE @nPersonProjectLeaveHours NUMERIC(18,2)
	DECLARE @nPersonProjectTimeAllotted NUMERIC(18,2)
	DECLARE @nPersonProjectTimeExpended NUMERIC(18,2)

	DECLARE @tDateWorked TABLE (DateID INT NOT NULL PRIMARY KEY, DateWorked DATE, IsValidWorkDay BIT)
	DECLARE @tResult TABLE (MessageID INT NOT NULL PRIMARY KEY IDENTITY(1,1), Message VARCHAR(MAX))

	SELECT
		@bIsValidated = CTT.IsValidated,
		@nHoursPerDay = P.HoursPerDay,
		@dEndDate = PP.EndDate,
		@dStartDate = PP.StartDate,
		@nPersonProjectLeaveHours = ISNULL((SELECT PPTT.PersonProjectLeaveHours FROM person.PersonProjectTimeType PPTT WHERE PPTT.PersonProjectID = PP.PersonProjectID AND PPTT.ClientTimeTypeID = CTT.ClientTimeTypeID), 0),
		@cTimeTypeCode = TT.TimeTypeCode
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientTimeType CTT ON CTT.ClientID = P.ClientID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
			AND PP.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeID = @TimeTypeID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID
		AND PPT.TimeTypeID = @TimeTypeID

	IF @DateWorkedList IS NOT NULL
		BEGIN

		INSERT INTO @tDateWorked
			(DateID, DateWorked, IsValidWorkDay)
		SELECT
			LTT.ListItemID,
			CAST(LTT.ListItem AS DATE),
			project.IsValidWorkDay(CAST(LTT.ListItem AS DATE), @ProjectLocationID)
		FROM core.ListToTable(@DateWorkedList, ',') LTT

		END
	--ENDIF

	SELECT
		@nDateWorkedCount = ISNULL(COUNT(DW.DateID), 0)
	FROM @tDateWorked DW

	INSERT INTO @tResult
		(Message)
	SELECT
		core.FormatDate(DW.DateWorked) + ' is outside the allowed date range for this deployment.'
	FROM @tDateWorked DW
	WHERE DW.DateWorked < @dStartDate OR DW.DateWorked > @dEndDate
	ORDER BY DW.DateWorked

	IF @cTimeTypeCode = 'PROD'
		BEGIN

		INSERT INTO @tResult
			(Message)
		SELECT
			core.FormatDate(DW.DateWorked) + ' is not a valid work day for this deployment.'
		FROM @tDateWorked DW
		WHERE DW.IsValidWorkDay = 0
		ORDER BY DW.DateWorked

		SELECT
			@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
		FROM person.PersonProjectLocation PPL
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = PL.ProjectID
				AND PPL.PersonProjectID = @PersonProjectID

		IF ISNULL(@nPersonProjectTimeAllotted, 0) < ISNULL(@nPersonProjectTimeExpended, 0) + (@nDateWorkedCount * @HoursWorked)
			INSERT INTO @tResult (Message) VALUES ('The total hours worked exceeds your hours allocation for this deployment.')
		--ENDIF

		END
	ELSE IF @bIsValidated = 1
		BEGIN

		IF ISNULL(@nPersonProjectLeaveHours, 0) < ISNULL(@nPersonProjectTimeExpended, 0) + (@nDateWorkedCount * @HoursWorked)
			INSERT INTO @tResult (Message) VALUES ('The total hours claimed exceeds your allowance for this deployment.')
		--ENDIF

		END
	--ENDIF

	SELECT R.Message 
	FROM @tResult R
	ORDER BY R.MessageID

END
GO
--End procedure project.validateProjectDateAndTimeData

--Begin procedure reporting.GetPersonProjectList
EXEC utility.DropObject 'reporting.GetPersonProjectList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the person.PersonProject table
-- ==================================================================================
CREATE PROCEDURE reporting.GetPersonProjectList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientName,
		CF.ClientFunctionName,
		CPPR.ClientPersonProjectRoleName AS ProjectRoleName,
		CT.ContractingTypeName,
		PJ.CustomerName,
		core.FormatDate(PJ.EndDate) AS ProjectEndDateFormatted,
		CASE WHEN PJ.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS ProjectStatus,
		dropdown.GetCountryNameByISOCountryCode(PJ.ISOCountryCode2) AS CountryName,
		PJ.ProjectCode,
		PJ.ProjectName,
		core.FormatDate(PJ.StartDate) AS ProjectStartDateFormatted,
		CAST(PL.DPAAmount AS NUMERIC(18,2)) AS LocationDPAAmount,
		PL.DPAISOCurrencyCode,
		CAST(PL.DSACeiling AS NUMERIC(18,2)) AS DSACellingValue,
		PL.DSAISOCurrencyCode,
		PL.ProjectLocationName,
		PLC.ProjectLaborCode,
		PLC.ProjectLaborCodeName,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		core.FormatDate(PP.EndDate) AS DeploymentEndDateFormatted,
		CAST(PP.FeeRate AS NUMERIC(18,2)) AS DeployemntFeeRate,
		CASE WHEN PP.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS DeployemntStatus, --fix the typo here and in the rdl
		PP.ISOCurrencyCode,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted,
		core.FormatDate(PP.StartDate) AS DeploymentStartDateFormatted,
		PPL.Days,
		TOR.ProjectTermOfReferenceCode,
		TOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP 
		JOIN person.Person PN ON PN.PersonID = PP.PersonID 
		JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID  
		JOIN client.Client C ON C.ClientID = PJ.ClientID 
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID 
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID 
		JOIN client.ClientPersonProjectRole CPPR ON CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = PN.ContractingTypeID 
		JOIN person.PersonProjectLocation PPL on PPL.PersonProjectID = PP.PersonProjectID 
		JOIN project.ProjectLocation PL ON PP.ProjectID = PL.ProjectID 
		JOIN project.ProjectTermOfReference TOR ON TOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
		JOIN reporting.SearchResult SR ON SR.EntityID = PP.PersonProjectID
			AND PPL.ProjectLocationID = PL.ProjectLocationID 
			AND SR.EntityTypeCode = 'PersonProject'
			AND SR.PersonID = @PersonID

	ORDER BY PJ.ProjectName, C.ClientName, 25

END
GO
--End procedure reporting.GetPersonProjectList

--Begin procedure utility.ClonePasswordData
EXEC utility.DropObject 'utility.ClonePasswordData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.14
-- Description:	A stored procedure to clone the password and password salt values from a person.Person record to those of another person.Person record
-- ===================================================================================================================================================
CREATE PROCEDURE utility.ClonePasswordData

@PersonID INT,
@UserName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cXMLString XML

	SELECT @cXMLString = 
		(
		SELECT 
			P.Password, 
			P.PasswordSalt
		FROM person.Person P
		WHERE P.UserName = @UserName
		FOR XML PATH('Person')
		)

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
	SELECT
		@PersonID,
		'clonepassword',
		'Person',
		P.PersonID,
		'Password cloned for troubleshooting',
		@cXMLString
	FROM person.Person P
	WHERE P.UserName = @UserName

	UPDATE P1
	SET
		P1.Password = P2.Password,
		P1.PasswordSalt = P2.PasswordSalt
	FROM person.Person P1, person.Person P2
	WHERE P1.UserName = @UserName
		AND P2.PersonID = @PersonID

END
GO
--End procedure utility.ClonePasswordData

--Begin procedure utility.RevertPasswordData
EXEC utility.DropObject 'utility.RevertPasswordData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.14
-- Description:	A stored procedure to revert the password and password salt values of a person.Person record from those of another person.Person record
-- ====================================================================================================================================================
CREATE PROCEDURE utility.RevertPasswordData

@UserName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cXMLString XML

	SELECT TOP 1
		@cXMLString = EL.EventData
	FROM eventlog.EventLog EL
		JOIN person.Person P ON P.PersonID = EL.EntityID
			AND EL.EntityTypeCode = 'Person'
			AND EL.EventCode = 'clonepassword'
	ORDER BY EL.EventLogID DESC

	IF @cXMLString IS NOT NULL
		BEGIN

		DECLARE @cPassword VARCHAR(64)
		DECLARE @cPasswordSalt VARCHAR(50)

		SELECT
			@cPassword = Person.D.value('Password[1]', 'VARCHAR(64)'), 
			@cPasswordSalt = Person.D.value('PasswordSalt[1]', 'VARCHAR(50)')
		FROM @cXMLString.nodes('Person') Person(D)

		UPDATE P
		SET
			P.Password = @cPassword,
			P.PasswordSalt = @cPasswordSalt
		FROM person.Person P
		WHERE P.UserName = @UserName

		END
	--ENDIF

END
GO
--End procedure utility.RevertPasswordData

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--Begin table eventlog.EventLog
UPDATE EL
SET EL.EventCode = 'LineManagerApproved'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'LineManagerApprove'
GO

UPDATE EL
SET EL.EventCode = 'LineManagerRejected'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'LineManagerReject'
GO

UPDATE EL
SET EL.EventCode = 'update'
FROM eventlog.EventLog EL
WHERE EL.EventCode = 'manage'
GO
--End table eventlog.EventLog

--Begin table dropdown.ClientNoticeType
TRUNCATE TABLE dropdown.ClientNoticeType
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientNoticeType', 'ClientNoticeTypeID', 0
GO

INSERT INTO dropdown.ClientNoticeType 
	(ClientNoticeTypeCode, ClientNoticeTypeName, DisplayOrder, Icon) 
VALUES
('Event', 'Event', 0, 'fa-lg fa-ticket'),
('Job', 'Job Vacancy', 0, 'fa-lg fa-suitcase'),
('News', 'News', 0, 'fa-lg fa-rss-square'),
('Other', 'Other', 99, 'fa-lg fa-magic'),
('Policy', 'Policy Update', 0, 'fa-lg fa-book'),
('Security', 'Security Alert', 0, 'fa-lg fa-shield'),
('Training', 'Training Opportunity', 0, 'fa-lg fa-mortar-board')
GO
--End table dropdown.ClientNoticeType

--Begin menu items
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-info-circle',
	@NewMenuItemCode = 'ClientNoticeList',
	@NewMenuItemLink = '/clientnotice/list',
	@NewMenuItemText = 'Noticeboard'
GO
--End menu items

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='Add / edit a client notice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='View a client notice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientNotice', @DESCRIPTION='View the list of client notices', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ClientNotice.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--End file Build File - 04 - Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.9 - 2018.11.25 15.18.30')
GO
--End build tracking

