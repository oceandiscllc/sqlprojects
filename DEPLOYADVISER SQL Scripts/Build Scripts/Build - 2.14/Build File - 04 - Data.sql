USE DeployAdviserCloud
GO

--Begin table core.EntityTypeAddUpdate
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ClientVacancy', 
	@EntityTypeName = 'Client Vacancy', 
	@EntityTypeNamePlural = 'Client Vacancies',
	@PrimaryKeyFieldName = 'ClientVacancyID',
	@SchemaName = 'hrms', 
	@TableName = 'ClientVacancy'
GO
--End table core.EntityTypeAddUpdate

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'Feedback',
	@NewMenuItemLink = '/feedback/list',
	@NewMenuItemText = 'Feedback',
	@ParentMenuItemCode = 'RosterTools',
	@PermissionableLineageList = 'Feedback.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ClientVacancy',
	@NewMenuItemLink = '/clientvacancy/list',
	@NewMenuItemText = 'Vacancies',
	@ParentMenuItemCode = 'RosterTools',
	@PermissionableLineageList = 'ClientVacancy.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'RosterTools'
GO
--End table core.MenuItem

--Begin table core.MenuItemClient
INSERT INTO core.MenuItemClient
	(ClientID,	MenuItemID)
SELECT
	C.ClientID,
	MI.MenuItemID
FROM client.Client C
	CROSS APPLY core.MenuItem MI
WHERE C.IntegrationCode = 'Hermis'
	AND MI.MenuItemCode IN ('ClientVacancy')
GO
--End table core.MenuItemClient

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'ContingentLiabilityProformaLink', 'The hyperlink to the Contingent Liability Proforma required by the vacancy application', '<a href="http://www.google.com" style="font-color:blue;" target="_blank">here</a>'
GO
--End table core.SystemSetup

--Begin table invoice.Invoice
UPDATE I
SET I.HasExpenseRecords = 
	CASE
		WHEN EXISTS 
			(
			SELECT 1 
			FROM document.DocumentEntity DE 
				JOIN person.PersonProjectExpense PPE ON PPE.PersonProjectExpenseID = DE.EntityID 
					AND DE.EntityTypeCode = 'PersonProjectExpense' 
					AND PPE.InvoiceID = I.InvoiceID 
			)
		THEN 1
		ELSE 0
		END
FROM invoice.Invoice I
GO
--End table invoice.Invoice

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View the vacancies list', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=1, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='ClientVacancy.List', 
	@PERMISSIONCODE=NULL;
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=1, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='ClientVacancy.View', 
	@PERMISSIONCODE=NULL;
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Invoice', 
	@DESCRIPTION='Esport the time sheet summary', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=1, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Invoice.List.ExportTimeSheet',
	@PERMISSIONCODE='ExportTimeSheet';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
