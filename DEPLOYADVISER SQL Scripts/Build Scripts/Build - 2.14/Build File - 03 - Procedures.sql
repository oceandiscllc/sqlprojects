USE DeployAdviserCloud
GO

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

	SELECT O.DocumentID
	FROM @tOutput O
	ORDER BY 1

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, 'Administrator') CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'ClientNotice'
		SELECT @bHasAccess = 1 FROM client.ClientNotice T WHERE (T.ClientNoticeID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, NULL) CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'List.ClientFinanceExport'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM person.GetClientsByPersonID(@PersonID, 'Administrator') CBP
				JOIN client.Client C ON C.ClientID = CBP.ClientID
					AND EXISTS
						(
						SELECT 1
						FROM client.ClientConfiguration CC
						WHERE CC.ClientID = CBP.ClientID
							AND CC.ClientConfigurationKey = 'HasClientFinanceExport'
							AND CAST(CC.ClientConfigurationValue AS BIT) = 1
						)
					AND EXISTS
						(
						SELECT 1
						FROM invoice.Invoice T
							JOIN project.Project P ON P.ProjectID = T.ProjectID
								AND P.ClientID = CBP.ClientID
								AND T.InvoiceStatus = 'Approved'
								AND T.ClientFinanceExportID = 0
							)

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE 
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR PBP.RoleCode = 'Administrator'
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID AND T.RoleCode IN ('Administrator', 'ProjectManager') OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'VacancyApplication'
		BEGIN

		SELECT @bHasAccess = 
			CASE WHEN EXISTS
				(
				SELECT 1 
				FROM HermisCloud.hrms.Application A 
					JOIN HermisCloud.person.Person P ON P.PersonID = A.PersonID 
						AND P.PersonID = @PersonID 
						AND A.ApplicationID = @EntityID
						AND (@AccessCode <> 'AddUpdate' OR A.SubmittedDateTime IS NULL)

				UNION

				SELECT 1 
				FROM person.Person P WHERE P.PersonID = @PersonID 
					AND P.IsSuperAdministrator = 1
				)
				THEN 1
				ELSE 0
			END

		END
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (PendingActionID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>',
		'<button class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonFeedback
	IF EXISTS 
		(
		SELECT 1 
		FROM HermisCloud.person.Feedback F 
			JOIN HermisCloud.person.Person P ON P.PersonID = F.CSGMemberPersonID
				AND P.DAPersonID = @PersonID
				AND F.CSGMemberComment IS NULL
				AND 
					(
					(F.IsExternal = 1 AND HermisCloud.workflow.IsWorkflowComplete('Feedback', F.FeedbackID) = 1)
						OR HermisCloud.workflow.IsWorkflowComplete('Feedback', F.FeedbackID) = 1
					)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Review CSG Member Feedback',
			'<a class="btn btn-sm btn-info" href="/feedback/list">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two emergency contact records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	INSERT INTO @tTable 
		(PendingAction, Link1) 
	SELECT
		'Accept deployment ' + PP.PersonProjectName + ' on project ' + P.ProjectName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptPersonProject'', ' + CAST(PP.PersonProjectID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>'
	FROM person.PersonProject PP 
		JOIN project.Project P ON P.ProjectID = PP.ProjectID 
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NULL
			--AND PP.EndDate >= getDate()

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT
		T.PendingActionID,
		T.PendingAction, 
		REPLACE(T.Link1, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link1,
		REPLACE(T.Link2, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure reporting.GetPersonNameByPersonID
EXEC utility.DropObject 'reporting.GetPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Inderjeet Kaur
-- Create date:	2019.04.08
-- Description:	A stored procedure to person name by personid
-- ==========================================================
CREATE PROCEDURE reporting.GetPersonNameByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT	
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.Person P
	WHERE	P.PersonID = @PersonID

END
GO
--End procedure reporting.GetPersonNameByPersonID

--Begin procedure reporting.GetInvoiceList
EXEC utility.DropObject 'reporting.GetInvoiceList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the invoice.Invoice table
-- =============================================================================
CREATE PROCEDURE reporting.GetInvoiceList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CAST(E.InvoiceAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(E.InvoiceVAT AS NUMERIC(18,2)) AS InvoiceExpenseVAT,
		person.FormatPersonNameByPersonID(ISNULL((SELECT EL.PersonID FROM eventlog.EventLog EL WHERE EL.EntityTypeCode = 'Project' AND EL.EventCode = 'create' AND EL.EntityID = I.ProjectID), 0), 'LastFirst') AS ProjectCreatorNameFormatted,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		CASE WHEN I.InvoiceStatus NOT IN ('Rejected', 'Approved') THEN DATEDIFF(d, I.InvoiceDateTime, getDate()) ELSE NULL END AS InvoiceAge,
		I.InvoiceAmount,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		person.FormatPersonNameByPersonID(I.PersonID, 'LastFirst') AS PersonNameFormatted,
		I.PersonInvoiceNumber,
		I.TaxRate,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectName,
		CAST(T.InvoiceDPAAmount AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		CAST(T.InvoiceDPAVAT AS NUMERIC(18,2)) AS InvoiceDPAVAT,
		CAST(T.InvoiceDSAAmount AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		CAST(T.InvoiceDSAVAT AS NUMERIC(18,2)) AS InvoiceDSAVAT,
		CAST(T.InvoiceFeeAmount AS NUMERIC(18,2)) AS InvoiceFeeAmount,
		CAST(T.InvoiceFeeVAT AS NUMERIC(18,2)) AS InvoiceFeeVAT
	FROM invoice.Invoice I
		JOIN Project.Project P ON P.ProjectID = I.ProjectID 
		LEFT JOIN
			(
			SELECT
				PPE.InvoiceID,
				CAST(SUM(PPE.ExpenseAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
				CAST(SUM(PPE.TaxAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceVAT
			FROM person.PersonProjectExpense PPE
			GROUP BY PPE.InvoiceID
			) E ON E.InvoiceID = I.InvoiceID
		LEFT JOIN
			(
			SELECT
				PPT.InvoiceID,
				SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) AS InvoiceFeeAmount,
				SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceFeeVAT,
				SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceDPAAmount,
				SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceDPAVAT,
				SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceDSAAmount,
				SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceDSAVAT
			FROM person.PersonProjectTime PPT
				JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN project.Project P ON P.ProjectID = PP.ProjectID
				JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			GROUP BY PPT.InvoiceID
			) T ON T.InvoiceID = I.InvoiceID  
		JOIN reporting.SearchResult SR ON SR.EntityID = I.InvoiceID
			AND SR.PersonID = @PersonID
			AND SR.EntityTypeCode = 'Invoice'
	ORDER BY I.InvoiceStatus, I.InvoiceDateTime

END
GO
--End procedure reporting.GetInvoiceList

--Begin procedure reporting.GetInvoiceTimeSummaryWithLineManagertReview
EXEC utility.DropObject 'reporting.GetInvoiceTimeSummaryWithLineManagertReview'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.04.12
-- Description:	A procedure to return invoice time data
-- ====================================================
CREATE PROCEDURE reporting.GetInvoiceTimeSummaryWithLineManagertReview

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.InvoiceID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS ConsultantName,
		I.PersonInvoiceNumber,
		core.FormatDate(PPT.DateWorked) AS [Date], 
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		PP.PersonProjectName,
		TT.TimeTypeName AS TimeType,
		PL.ProjectLocationName AS [Location],
		(SELECT person.GetPersonProjectDays(PPT.PersonProjectID) * P.HoursPerDay) AS Alloted,
		PPT.HoursWorked AS ForApprovalInThisTimeSheet,
		PPT.ProjectManagerNotes AS FootNotes,
		P.ProjectCode,
		P.ProjectName
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN person.PersonProjectTime PPT ON PPT.InvoiceID = I.InvoiceID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND I.InvoiceID = @InvoiceID

END
GO
--Begin procedure reporting.GetInvoiceTimeSummaryWithLineManagertReview

--Begin procedure reporting.GetLessonList
EXEC utility.DropObject 'reporting.GetLessonList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.03.25
-- Description:	A procedure to return Lessons for reporting
-- ========================================================
CREATE PROCEDURE reporting.GetLessonList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SqlQuery1 AS  NVARCHAR(4000)
	DECLARE @SqlQuery2 AS  NVARCHAR(4000)
	DECLARE @SqlQuery AS NVARCHAR(MAX)

	DECLARE @ParamDefinition AS NVARCHAR(MAX)

	DECLARE @ColumnListWithConversion VARCHAR(4000) = (SELECT STUFF((SELECT ', CONVERT(NVARCHAR(MAX), ' + ColumnCode + ' ) AS ' + ColumnCode  FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson'
																							ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, ',') );

	DECLARE @ColumnList VARCHAR(4000) = (SELECT STUFF((SELECT ', '  + ColumnCode FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson' AND ColumnCode != 'ID'
																					ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, ',') );
	IF @ColumnList = ' ' OR @ColumnList IS NULL
			BEGIN																												
							SET  @SqlQuery1 =     N'SELECT ID, ColumnName, Valuess
																   FROM (SELECT 	
																									L.LessonID AS ID,
																									CONVERT(NVARCHAR(MAX), ''L-'' + RIGHT(''00000'' + CAST(L.LessonID AS VARCHAR(5)), 5)) AS [Log Number],
																									CONVERT(NVARCHAR(MAX),  person.FormatPersonNameByPersonID((CAST(L.CreatePersonID AS VARCHAR(100))), ''LastFirst'')) AS [User],		
																									CONVERT(NVARCHAR(MAX), CASE WHEN L.IsActive = 1 THEN ''Yes'' ELSE ''No'' END) AS [Lesson Status],		
																									CONVERT(NVARCHAR(MAX), ISNULL(LO.LessonOwnerName, '''')) AS [Lesson Owner],
																									CONVERT(NVARCHAR(MAX),(SELECT reporting.GetWorkflowStatus(''Lesson'', (SELECT L1.LessonID  FROM lesson.Lesson L1 WHERE  L1.LessonID = L.LessonID)))) AS [Workflow Status],
																									CONVERT(NVARCHAR(MAX), P.ProjectName) AS [Activity Stream],
																									CONVERT(NVARCHAR(MAX), PS.ProjectSponsorName) AS Client,	   
																									CONVERT(NVARCHAR(MAX), ISNULL(L.ClientCode, '''')) AS [Client Code],
																									CONVERT(NVARCHAR(MAX), P.ProjectName) AS Project,
																									CONVERT(NVARCHAR(MAX), dropdown.GetCountryNameByISOCountryCode(L.ISOCountryCode2))  AS Country,		
																									CONVERT(NVARCHAR(MAX), CAST(ISNULL(L.ScaleOfResponse, 0) AS nvarchar(100)) + '' '' + ISNULL(L.ISOCurrencyCode, '''')) AS [Scale Of Response],
																									CONVERT(NVARCHAR(MAX),L.LessonName) AS [Lesson Name],
																									CONVERT(NVARCHAR(MAX),core.FormatDate(L.LessonDate)) AS [Lesson Date],
																									CONVERT(NVARCHAR(MAX),LT.LessonTypeName) AS [Lesson Type],
																									CONVERT(NVARCHAR(MAX),ISNULL(LC.LessonCategoryName, '''')) AS [Leson Category],
																									CONVERT(NVARCHAR(MAX),ISNULL(Impact.ImplementationImpactName, '''')) AS [Lesson Implementation Impact],
																									CONVERT(NVARCHAR(MAX),ISNULL(ID.ImplementationDifficultyName, '''')) AS [Difficulty of Implementation],
																									CONVERT(NVARCHAR(MAX), ISNULL(IT.IncidentTypeName, '''')) AS [Event Subcategory]
																					FROM   lesson.Lesson L
																								   JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
																								   JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
																								   JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
																								   JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID  
																								   JOIN dropdown.ImplementationImpact Impact ON Impact.ImplementationImpactID = L.ImplementationImpactID
																								   JOIN dropdown.ImplementationDifficulty ID ON ID.ImplementationDifficultyID = L.ImplementationDifficultyID
																								   JOIN dropdown.LessonOwner LO ON LO.LessonOwnerID = L.LessonOwnerID
																								   JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = L.IncidentTypeID
																								   JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
																								   JOIN Reporting.SearchResult SR ON SR.EntityID = L.LessonID
																								   AND SR.EntityTypeCode = ''Lesson''
																								  AND SR.PersonID = ' + CAST(@PersonID AS NVARCHAR(10)) + ')p'

						SET @SqlQuery2 =        N' UNPIVOT
																   (Valuess FOR ColumnName IN([Log Number],  [User], [Lesson Status], [Lesson Owner], [Workflow Status], [Activity Stream], Client, [Client Code] , Project, Country, [Scale Of Response], [Lesson Name], [Lesson Date], [Lesson Type], [Leson Category],  [Lesson Implementation Impact], [Difficulty of Implementation], [Event Subcategory])
																   ) AS UnPvt;'
			END
	ELSE
				BEGIN																												
							SET  @SqlQuery1 =     N'SELECT ID, ColumnName, Valuess
																   FROM (SELECT 	
																								L.LessonID AS ID,
																								CONVERT(NVARCHAR(MAX), ''L-'' + RIGHT(''00000'' + CAST(L.LessonID AS VARCHAR(5)), 5)) AS [Log Number],
																								CONVERT(NVARCHAR(MAX),  person.FormatPersonNameByPersonID((CAST(L.CreatePersonID AS VARCHAR(100))), ''LastFirst'')) AS [User],		
																								CONVERT(NVARCHAR(MAX), CASE WHEN L.IsActive = 1 THEN ''Yes'' ELSE ''No'' END) AS [Lesson Status],		
																								CONVERT(NVARCHAR(MAX), ISNULL(LO.LessonOwnerName, '''')) AS [Lesson Owner],
																								CONVERT(NVARCHAR(MAX),(SELECT reporting.GetWorkflowStatus(''Lesson'', (SELECT L1.LessonID  FROM lesson.Lesson L1 WHERE  L1.LessonID = L.LessonID)))) AS [Workflow Status],
																								CONVERT(NVARCHAR(MAX), P.ProjectName) AS [Activity Stream],
																								CONVERT(NVARCHAR(MAX), PS.ProjectSponsorName) AS Client,	   
																								CONVERT(NVARCHAR(MAX), ISNULL(L.ClientCode, '''')) AS [Client Code],
																								CONVERT(NVARCHAR(MAX), P.ProjectName) AS Project,
																								CONVERT(NVARCHAR(MAX), dropdown.GetCountryNameByISOCountryCode(L.ISOCountryCode2))  AS Country,		
																								CONVERT(NVARCHAR(MAX), CAST(ISNULL(L.ScaleOfResponse, 0) AS nvarchar(100)) + '' '' + ISNULL(L.ISOCurrencyCode, '''')) AS [Scale Of Response],
																								CONVERT(NVARCHAR(MAX),L.LessonName) AS [Lesson Name],
																								CONVERT(NVARCHAR(MAX),core.FormatDate(L.LessonDate)) AS [Lesson Date],
																								CONVERT(NVARCHAR(MAX),LT.LessonTypeName) AS [Lesson Type],
																								CONVERT(NVARCHAR(MAX),ISNULL(LC.LessonCategoryName, '''')) AS [Leson Category],
																								CONVERT(NVARCHAR(MAX),ISNULL(Impact.ImplementationImpactName, '''')) AS [Lesson Implementation Impact],
																								CONVERT(NVARCHAR(MAX),ISNULL(ID.ImplementationDifficultyName, '''')) AS [Difficulty of Implementation],
																								CONVERT(NVARCHAR(MAX), ISNULL(IT.IncidentTypeName, '''')) AS [Event Subcategory]
																								' + @ColumnListWithConversion +' 				   
																				 FROM   lesson.Lesson L
																							   JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
																							   JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
																								JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
																								JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID  
																								JOIN dropdown.ImplementationImpact Impact ON Impact.ImplementationImpactID = L.ImplementationImpactID
																								JOIN dropdown.ImplementationDifficulty ID ON ID.ImplementationDifficultyID = L.ImplementationDifficultyID
																								JOIN dropdown.LessonOwner LO ON LO.LessonOwnerID = L.LessonOwnerID
																								JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = L.IncidentTypeID
																								JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
																								JOIN Reporting.SearchResult SR ON SR.EntityID = L.LessonID
																								AND SR.EntityTypeCode = ''Lesson''
																								AND SR.PersonID = ' + CAST(@PersonID AS NVARCHAR(10)) + ')p'

					SET @SqlQuery2 =  N' UNPIVOT
																   (Valuess FOR ColumnName IN([Log Number],  [User], [Lesson Status], [Lesson Owner], [Workflow Status], [Activity Stream], Client, [Client Code] , Project, Country, [Scale Of Response], [Lesson Name], [Lesson Date], [Lesson Type], [Leson Category],  [Lesson Implementation Impact], [Difficulty of Implementation], [Event Subcategory]  ' + @ColumnList +' )
																   ) AS UnPvt;'
			END
	
	
											
		
		  Set @ParamDefinition =      ' @PersonID INT'
		
		SET @SqlQuery = CAST( @SqlQuery1 AS NVARCHAR(MAX)) + CAST(@SqlQuery2 AS NVARCHAR(MAX))
		 PRINT @SQLQuery

         Execute sp_Executesql    @SqlQuery, @ParamDefinition, @PersonID

		RETURN

END
GO
--End procedure reporting.GetLessonList

--Begin procedure reporting.GetPersonProjectList
EXEC utility.DropObject 'reporting.GetPersonProjectList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the person.PersonProject table
-- ==================================================================================
CREATE PROCEDURE reporting.GetPersonProjectList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientName,
		CF.ClientFunctionName,
		CPPR.ClientPersonProjectRoleName AS ProjectRoleName,
		CT.ContractingTypeName,
		CC.ClientCustomerName AS CustomerName,
		core.FormatDate(PJ.EndDate) AS ProjectEndDateFormatted,
		CASE WHEN PJ.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS ProjectStatus,
		dropdown.GetCountryNameByISOCountryCode(PJ.ISOCountryCode2) AS CountryName,
		PJ.ProjectCode,
		PJ.ProjectName,
		core.FormatDate(PJ.StartDate) AS ProjectStartDateFormatted,
		CAST(PL.DPAAmount AS NUMERIC(18,2)) AS LocationDPAAmount,
		PL.DPAISOCurrencyCode,
		CAST(PL.DSACeiling AS NUMERIC(18,2)) AS DSACellingValue,
		PL.DSAISOCurrencyCode,
		PL.ProjectLocationName,
		PLC.ProjectLaborCode,
		PLC.ProjectLaborCodeName,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		core.FormatDate(PP.EndDate) AS DeploymentEndDateFormatted,
		CAST(PP.FeeRate AS NUMERIC(18,2)) AS DeployemntFeeRate,
		CASE WHEN PP.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS DeployemntStatus, --fix the typo here and in the rdl
		PP.ISOCurrencyCode,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted,
		core.FormatDate(PP.StartDate) AS DeploymentStartDateFormatted,
		PPL.Days,
		TOR.ProjectTermOfReferenceCode,
		TOR.ProjectTermOfReferenceName
	FROM person.PersonProject PP 
		JOIN person.Person PN ON PN.PersonID = PP.PersonID 
		JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID  
		JOIN client.ClientCustomer CC ON CC.ClientCustomerID = PJ.ClientCustomerID
		JOIN client.Client C ON C.ClientID = PJ.ClientID 
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID 
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID 
		JOIN client.ClientPersonProjectRole CPPR ON CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = PN.ContractingTypeID 
		JOIN person.PersonProjectLocation PPL on PPL.PersonProjectID = PP.PersonProjectID 
		JOIN project.ProjectLocation PL ON PP.ProjectID = PL.ProjectID 
		JOIN project.ProjectTermOfReference TOR ON TOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
		JOIN reporting.SearchResult SR ON SR.EntityID = PP.PersonProjectID
			AND PPL.ProjectLocationID = PL.ProjectLocationID 
			AND SR.EntityTypeCode = 'PersonProject'
			AND SR.PersonID = @PersonID
	ORDER BY PJ.ProjectName, C.ClientName, 25

END
GO
--End procedure reporting.GetPersonProjectList