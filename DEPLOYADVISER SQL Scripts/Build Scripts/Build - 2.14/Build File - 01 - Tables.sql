USE DeployAdviserCloud
GO

--Begin table document.DocumentEntity
EXEC utility.DropIndex 'document.DocumentEntity', 'IX_DocumentEntity'
GO

EXEC utility.SetIndexClustered 'document.DocumentEntity', 'IX_DocumentEntity', 'EntityTypeCode,EntityID'
GO
--End table document.DocumentEntity

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'HasExpenseRecords', 'BIT', '0'
GO

--Begin trigger invoice.TR_Invoice
EXEC utility.DropObject 'invoice.TR_Invoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A trigger to populate the LineManagerPin column
-- ============================================================
CREATE TRIGGER invoice.TR_Invoice ON invoice.Invoice AFTER INSERT
AS
BEGIN
	SET ARITHABORT ON;

	UPDATE INV
	SET 
		INV.HasExpenseRecords = 
			CASE
				WHEN EXISTS 
					(
					SELECT 1 
					FROM document.DocumentEntity DE 
						JOIN person.PersonProjectExpense PPE ON PPE.PersonProjectExpenseID = DE.EntityID 
							AND DE.EntityTypeCode = 'PersonProjectExpense' 
							AND PPE.InvoiceID = INV.InvoiceID 
					)
				THEN 1
				ELSE 0
			END,

		INV.LineManagerPin = CAST(FLOOR(RAND() * (9999 - 1000) + 1000) AS VARCHAR(4)),
		INV.LineManagerToken = newID()
	FROM invoice.Invoice INV
		JOIN INSERTED INS ON INS.InvoiceID = INV.InvoiceID

END
GO
--End trigger invoice.TR_Invoice

ALTER TABLE invoice.Invoice ENABLE TRIGGER TR_Invoice
GO
--End table invoice.Invoice
