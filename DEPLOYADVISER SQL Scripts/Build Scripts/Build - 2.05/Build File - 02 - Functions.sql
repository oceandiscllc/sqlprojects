USE DeployAdviser
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113) + ' (' + LEFT(DATENAME(dw, @DateTimeData), 3) + ')'

END
GO
--End function core.FormatDate

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO
--End function person.CheckFileAccess

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO
--End function person.ValidatePassword

--Begin function project.IsValidWorkDay
EXEC utility.DropObject 'project.IsValidWorkDay'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.15
-- Description:	A function to determine a date is a valid workday for a project location
-- =====================================================================================
CREATE FUNCTION project.IsValidWorkDay
(
@DateWorked DATE,
@ProjectLocationID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsValidWorkDay BIT
	DECLARE @nDayOfWeek INT = DATEPART(dw, @DateWorked)

		SELECT @bIsValidWorkDay = 
			CASE 
				WHEN @nDayOfWeek = 1 AND PL.CanWorkDay1 = 1
				THEN 1
				WHEN @nDayOfWeek = 2 AND PL.CanWorkDay2 = 1
				THEN 1
				WHEN @nDayOfWeek = 3 AND PL.CanWorkDay3 = 1
				THEN 1
				WHEN @nDayOfWeek = 4 AND PL.CanWorkDay4 = 1
				THEN 1
				WHEN @nDayOfWeek = 5 AND PL.CanWorkDay5 = 1
				THEN 1
				WHEN @nDayOfWeek = 6 AND PL.CanWorkDay6 = 1
				THEN 1
				WHEN @nDayOfWeek = 7 AND PL.CanWorkDay7 = 1
				THEN 1
				ELSE 0
			END

		FROM project.ProjectLocation PL
		WHERE PL.ProjectLocationID = @ProjectLocationID
	
	RETURN @bIsValidWorkDay
	
END
GO
--End function project.IsValidWorkDay
