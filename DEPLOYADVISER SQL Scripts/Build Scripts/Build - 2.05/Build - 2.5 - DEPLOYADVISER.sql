-- File Name:	Build - 2.5 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.5 - 2018.04.09 17.43.31

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Triggers:
--		document.TR_Document ON document.Document
--		person.TR_PersonAccount ON person.PersonAccount
--		person.TR_PersonProjectTime ON person.PersonProjectTime
--
-- Functions:
--		core.FormatDate
--		project.IsValidWorkDay
--
-- Procedures:
--		client.GetClientByClientID
--		document.GetDocumentByDocumentData
--		document.GetDocumentByDocumentEntityCode
--		document.GetDocumentByDocumentGUID
--		document.GetEntityDocuments
--		document.PurgeEntityDocuments
--		eventlog.LogPersonAction
--		invoice.GetInvoiceExpenseLogByInvoiceID
--		invoice.GetInvoiceWorkflowDataByInvoiceID
--		invoice.ProcessLineManagerInvoiceWorkflowAction
--		person.CheckAccess
--		person.GetPersonByPersonID
--		person.GetPersonJSONDataByPersonID
--		person.GetPersonProjectByPersonProjectID
--		person.GetPersonProjectExpenseByPersonProjectExpenseID
--		person.GetPersonUsernameByEmailAddress
--		person.SavePersonMerge
--		project.ValidateDateWorked
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'ClientLegalEntities', 'VARCHAR(MAX)'
GO
--End table client.Client

--Begin table document.Document
IF utility.HasColumn('document.Document', 'DocumentName') = 1
	EXEC sp_RENAME 'document.Document.DocumentName', 'DocumentTitle', 'COLUMN'
--ENDIF	
GO

IF utility.HasColumn('document.Document', 'DocumentSize') = 1
	BEGIN

	EXEC sp_RENAME 'document.Document.DocumentSize', 'PhysicalFileSize', 'COLUMN';
	EXEC sp_rename 'document.DF_Document_DocumentSize', 'DF_Document_PhysicalFileSize', 'OBJECT'

	END
--ENDIF	
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'LineManagerActionDateTime', 'DATETIME'
EXEC utility.AddColumn @TableName, 'LineManagerEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'LineManagerName', 'VARCHAR(200)'
GO
--End table invoice.Invoice

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

ALTER TABLE person.Person ALTER COLUMN SummaryBiography VARCHAR(MAX)

EXEC utility.DropColumn @TableName, 'GenderCode'
GO
--End table person.Person

--Begin table person.PersonAccount
EXEC utility.DropObject 'person.TR_PersonAccount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A trigger to ensure there is only 1 default account per person id
-- ==============================================================================
CREATE TRIGGER person.TR_PersonAccount ON person.PersonAccount AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	DECLARE @nPersonAccountID INT = ISNULL((SELECT TOP 1 INS.PersonAccountID FROM INSERTED INS WHERE INS.IsDefault = 1), 0)

	IF @nPersonAccountID > 0
		BEGIN

		UPDATE PA
		SET 
			PA.IsDefault = 0
		FROM person.PersonAccount PA
			JOIN INSERTED INS ON INS.PersonID = PA.PersonID
				AND PA.PersonAccountID <> @nPersonAccountID

		END
	--ENDIF

END
GO
--End table person.PersonAccount

--Begin table person.PersonProjectTime
EXEC utility.DropObject 'person.TR_PersonProjectTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER person.TR_PersonProjectTime ON person.PersonProjectTime AFTER UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	IF UPDATE(HoursWorked) OR UPDATE(ApplyVAT) OR UPDATE(ProjectManagerNotes)
		BEGIN

		UPDATE PPT
		SET PPT.IsLineManagerApproved = 0
		FROM person.PersonProjectTime PPT
			JOIN INSERTED I ON I.PersonProjectTimeID = PPT.PersonProjectTimeID

		UPDATE INV
		SET 
			INV.LineManagerActionDateTime = NULL,
			INV.LineManagerEmailAddress = NULL,
			INV.LineManagerName = NULL
		FROM invoice.Invoice INV
		WHERE EXISTS
			(
			SELECT 1
			FROM person.PersonProjectTime PPT
				JOIN INSERTED INS ON INS.PersonProjectTimeID = PPT.PersonProjectTimeID
					AND PPT.InvoiceID = INV.InvoiceID
			)

		END
	--ENDIF

END
GO
--End table person.PersonProjectTime
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113) + ' (' + LEFT(DATENAME(dw, @DateTimeData), 3) + ')'

END
GO
--End function core.FormatDate

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO
--End function person.CheckFileAccess

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO
--End function person.ValidatePassword

--Begin function project.IsValidWorkDay
EXEC utility.DropObject 'project.IsValidWorkDay'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.15
-- Description:	A function to determine a date is a valid workday for a project location
-- =====================================================================================
CREATE FUNCTION project.IsValidWorkDay
(
@DateWorked DATE,
@ProjectLocationID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsValidWorkDay BIT
	DECLARE @nDayOfWeek INT = DATEPART(dw, @DateWorked)

		SELECT @bIsValidWorkDay = 
			CASE 
				WHEN @nDayOfWeek = 1 AND PL.CanWorkDay1 = 1
				THEN 1
				WHEN @nDayOfWeek = 2 AND PL.CanWorkDay2 = 1
				THEN 1
				WHEN @nDayOfWeek = 3 AND PL.CanWorkDay3 = 1
				THEN 1
				WHEN @nDayOfWeek = 4 AND PL.CanWorkDay4 = 1
				THEN 1
				WHEN @nDayOfWeek = 5 AND PL.CanWorkDay5 = 1
				THEN 1
				WHEN @nDayOfWeek = 6 AND PL.CanWorkDay6 = 1
				THEN 1
				WHEN @nDayOfWeek = 7 AND PL.CanWorkDay7 = 1
				THEN 1
				ELSE 0
			END

		FROM project.ProjectLocation PL
		WHERE PL.ProjectLocationID = @ProjectLocationID
	
	RETURN @bIsValidWorkDay
	
END
GO
--End function project.IsValidWorkDay

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientLegalEntities,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure document.*
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
EXEC utility.DropObject 'document.GetEntityDocuments'
EXEC utility.DropObject 'document.SaveEntityDocuments'
GO
--End procedure document.*

--Begin procedure invoice.GetInvoiceExpenseLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectExpense table
-- ======================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLog
	SELECT
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate,
		CAST(PPE.ExchangeRate * PPE.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(PPE.ExchangeRate * PPE.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID) IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' 
				+ (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID)
				+ '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY CCC.ClientCostCodeName, PPE.ExpenseDate

END
GO
--End procedure invoice.GetInvoiceExpenseLogByInvoiceID

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentTitle, 
		D.PhysicalFileSize,
		D.ThumbnailData, 
		D.ThumbnailSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentTitle, 
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure document.GetEntityDocuments
EXEC Utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get records from the document.DocumentEntity table
-- =====================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@ProjectID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentGUID,
		D.DocumentID,
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.ThumbnailData,
		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentTitle, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentGUID VARCHAR(50) = NULL,
@DocumentID INT = 0,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @DocumentGUID IS NOT NULL
		BEGIN

		SELECT @DocumentID = D.DocumentID
		FROM document.Document D
		WHERE D.DocumentGUID = @DocumentGUID

		END
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE	DE.EntityTypeCode = @EntityTypeCode
		AND (DE.EntityTypeSubCode = @EntityTypeSubCode OR (@EntityTypeSubCode IS NULL AND DE.EntityTypeSubCode IS NULL))
		AND DE.EntityID = @EntityID 
		AND (@DocumentID = 0 OR DE.DocumentID = @DocumentID)

	IF NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.DocumentID = @DocumentID)
		BEGIN

		DELETE D
		FROM document.Document D
		WHERE D.DocumentID = @DocumentID

		END
	--ENDIF

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonAccounts VARCHAR(MAX) = ''
		
		SELECT @cPersonAccounts = COALESCE(@cPersonAccounts, '') + D.PersonAccount
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonAccount'), ELEMENTS) AS PersonAccount
			FROM person.PersonAccount T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonLanguages VARCHAR(MAX) = ''
		
		SELECT @cPersonLanguages = COALESCE(@cPersonLanguages, '') + D.PersonLanguage
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonLanguage'), ELEMENTS) AS PersonLanguage
			FROM person.PersonLanguage T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonNextOfKin VARCHAR(MAX) = ''
		
		SELECT @cPersonNextOfKin = COALESCE(@cPersonNextOfKin, '') + D.PersonNextOfKin
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonNextOfKin'), ELEMENTS) AS PersonNextOfKin
			FROM person.PersonNextOfKin T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPassport VARCHAR(MAX) = ''
		
		SELECT @cPersonPassport = COALESCE(@cPersonPassport, '') + D.PersonPassport
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPassport'), ELEMENTS) AS PersonPassport
			FROM person.PersonPassport T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPasswordSecurity VARCHAR(MAX) = ''
		
		SELECT @cPersonPasswordSecurity = COALESCE(@cPersonPasswordSecurity, '') + D.PersonPasswordSecurity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPasswordSecurity'), ELEMENTS) AS PersonPasswordSecurity
			FROM person.PersonPasswordSecurity T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonProofOfLife VARCHAR(MAX) = ''
		
		SELECT @cPersonProofOfLife = COALESCE(@cPersonProofOfLife, '') + D.PersonProofOfLife
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonProofOfLife'), ELEMENTS) AS PersonProofOfLife
			FROM person.PersonProofOfLife T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonQualificationAcademic VARCHAR(MAX) = ''
		
		SELECT @cPersonQualificationAcademic = COALESCE(@cPersonQualificationAcademic, '') + D.PersonQualificationAcademic
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonQualificationAcademic'), ELEMENTS) AS PersonQualificationAcademic
			FROM person.PersonQualificationAcademic T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonQualificationCertification VARCHAR(MAX) = ''
		
		SELECT @cPersonQualificationCertification = COALESCE(@cPersonQualificationCertification, '') + D.PersonQualificationCertification
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonQualificationCertification'), ELEMENTS) AS PersonQualificationCertification
			FROM person.PersonQualificationCertification T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<Accounts>' + ISNULL(@cPersonAccounts, '') + '</Accounts>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<Languages>' + ISNULL(@cPersonLanguages, '') + '</Languages>' AS XML),
			CAST('<NextOfKin>' + ISNULL(@cPersonNextOfKin, '') + '</NextOfKin>' AS XML),
			CAST('<PersonPassport>' + ISNULL(@cPersonPassport, '') + '</PersonPassport>' AS XML),
			CAST('<PasswordSecurity>' + ISNULL(@cPersonPasswordSecurity, '') + '</PasswordSecurity>' AS XML),
			CAST('<Permissionables>' + ISNULL(@cPersonPermissionables, '') + '</Permissionables>' AS XML),
			CAST('<ProofOfLife>' + ISNULL(@cPersonProofOfLife, '') + '</ProofOfLife>' AS XML),
			CAST('<QualificationAcademic>' + ISNULL(@cPersonQualificationAcademic, '') + '</QualificationAcademic>' AS XML),
			CAST('<QualificationCertification>' + ISNULL(@cPersonQualificationCertification, '') + '</QualificationCertification>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure invoice.GetInvoiceWorkflowDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get workflow data for an invoice
-- ===================================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsInWorkflow BIT = 1

	--InvoiceWorkflowData
	IF EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Invoice' AND EWSGP.EntityID = @InvoiceID)
		EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SET @bIsInWorkflow = 0

		UPDATE I
		SET I.InvoiceStatus = CASE WHEN I.InvoiceStatus = 'Submitted' THEN 'Line Manager Review' ELSE I.InvoiceStatus END
		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		SELECT

			CASE
				WHEN I.InvoiceStatus = 'Line Manager Review'
				THEN 'Line Manager Approval' 
				ELSE NULL
			END AS WorkflowStepName,

			1 AS WorkflowStepNumber,

			CASE
				WHEN I.InvoiceStatus = 'Rejected'
				THEN 0
				ELSE 1
			END AS WorkflowStepCount

		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		END
	--ENDIF

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode LIKE 'linemanagerapprove%'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN EL.EventCode LIKE 'linemanagerreject%'
			THEN 'Line Manager Rejected Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND 
				(
				EL.EventCode IN ('approve','create')
					OR EL.EventCode LIKE '%workflow'
					OR EL.EventCode LIKE 'linemanager%'
				)
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowPeopleByInvoiceID @InvoiceID, @bIsInWorkflow

END
GO
--End procedure invoice.GetInvoiceWorkflowDataByInvoiceID

--Begin procedure invoice.ProcessLineManagerInvoiceWorkflowAction
EXEC utility.DropObject 'invoice.ProcessLineManagerInvoiceWorkflowAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.29
-- Description:	A stored procedure to reject or move an invoice into the regular workflow based on a line manager action
-- =====================================================================================================================
CREATE PROCEDURE invoice.ProcessLineManagerInvoiceWorkflowAction

@InvoiceID INT = 0,
@WorkflowAction VARCHAR(50) = NULL,
@WorkflowComments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientID INT = 0

	UPDATE I
	SET
		I.InvalidLineManagerLoginAttempts = 0,
		I.LineManagerPin = NULL,
		I.LineManagerToken = NULL,
		I.InvoiceStatus = CASE WHEN @WorkflowAction = 'LineManagerRejected' THEN 'Rejected' ELSE I.InvoiceStatus END
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	EXEC eventlog.LogInvoiceAction @InvoiceID, @WorkflowAction, 0, @WorkflowComments

	IF @WorkflowAction = 'LineManagerApproved'
		BEGIN

		UPDATE PPT
		SET PPT.IsLineManagerApproved = 1
		FROM person.PersonProjectTime PPT
		WHERE PPT.InvoiceID = @InvoiceID

		SELECT @nClientID = P.ClientID
		FROM invoice.Invoice I
			JOIN project.Project P ON P.ProjectID = I.ProjectID
				AND I.InvoiceID = @InvoiceID

		EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

		SELECT workflow.GetWorkflowStepName('Invoice', @InvoiceID) AS WorkflowStepName

		END
	ELSE IF @WorkflowAction = 'LineManagerRejected'
		EXEC invoice.RejectInvoiceByInvoiceID @InvoiceID
	--ENDIF

END
GO
--End procedure invoice.ProcessLineManagerInvoiceWorkflowAction

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Administrator')) OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonEngagement
	SELECT
		C.ClientID,
		C.ClientLegalEntities,
		C.ClientName,

		CASE
			WHEN CP.AcceptedDate IS NULL
			THEN '<button type="button" class="btn btn-md btn-success" onClick="acceptClientPerson(' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Accept</button>'
			ELSE core.FormatDate(CP.AcceptedDate)
		END AS AcceptButton,

		'<button type="button" class="btn btn-md btn-danger" onClick="rejectClientPerson('+ CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">'
			+ CASE
					WHEN CP.AcceptedDate IS NULL
					THEN 'Decline'
					ELSE 'Resign'
				END + '</button>' AS RejectButton,

		R.RoleName
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
	ORDER BY 1, 4

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonJSONDataByPersonID
EXEC utility.DropObject 'person.GetPersonJSONDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.10
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonJSONDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cJSONData VARCHAR(MAX) = '{}'
	DECLARE @nIsForExport BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM client.ClientPerson CP 
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN person.Person P ON P.PersonID = CP.PersonID
				AND CP.PersonID = @PersonID 
				AND CP.ClientPersonRoleCode = 'Consultant'
				AND C.ClientCode = 'H3'
				AND P.HasAcceptedTerms = 1
		)
		BEGIN

		SET @nIsForExport = 1
		SET @cJSONData = '['

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				G.GenderCode,
				P.BirthDate,
				P.CellPhone,
				P.ChestSize,
				P.Citizenship1ISOCountryCode2,
				P.Citizenship2ISOCountryCode2,
				P.CollarSize,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.CountryCallingCodeID) AS CountryCallingCode,
				P.EmailAddress,
				P.FirstName,
				P.HeadSize,
				P.Height,
				P.HomePhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.HomePhoneCountryCallingCodeID) AS HomePhoneCountryCallingCode,
				CAST(P.IsPhoneVerified AS INT) AS IsPhoneVerified,
				CAST(P.IsUKEUNational AS INT) AS IsUKEUNational,
				P.LastName,
				P.MailAddress1,
				P.MailAddress2,
				P.MailAddress3,
				P.MailISOCountryCode2,
				P.MailMunicipality,
				P.MailPostalCode,
				P.MailRegion,
				P.MiddleName,
				P.Password,
				P.PasswordExpirationDateTime,
				P.PasswordSalt,
				P.PlaceOfBirthISOCountryCode2,
				P.PlaceOfBirthMunicipality,
				P.PreferredName,
				P.Suffix,
				P.SummaryBiography,
				P.Title,
				P.UserName,
				P.WorkPhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.WorkPhoneCountryCallingCodeID) AS WorkPhoneCountryCallingCode
			FROM person.Person P
				JOIN dropdown.Gender G ON G.GenderID = P.GenderID
					AND P.PersonID = @PersonID
			FOR JSON PATH, ROOT('Person'), Include_Null_Values
			),'{"Person":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				DE.EntityID AS DAPersonID,
				DE.EntityTypeCode,
				DE.EntityTypeSubCode,
				D.ContentSubtype, 
				D.ContentType, 
				D.CreateDateTime,
				CAST('' AS XML).value('xs:base64Binary(sql:column("D.DocumentData"))', 'VARCHAR(MAX)') AS DocumentData,
				D.DocumentDate, 
				D.DocumentDescription, 
				D.DocumentGUID, 
				D.DocumentTitle, 
				D.Extension, 
				D.PhysicalFileSize, 
				D.ThumbnailData, 
				D.ThumbnailSize
			FROM document.Document D
				JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
					AND DE.EntityTypeCode = 'Person'
					AND DE.EntityTypeSubCode = 'CV'
					AND DE.EntityID = @PersonID
			FOR JSON PATH, ROOT('PersonDocument'), Include_Null_Values
			),'{"PersonDocument":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				PL.ISOLanguageCode2,
				LP1.LanguageProficiencyCode AS SpeakingLanguageProficiencyCode,
				LP2.LanguageProficiencyCode AS ReadingLanguageProficiencyCode,
				LP3.LanguageProficiencyCode AS WritingLanguageProficiencyCode
			FROM person.PersonLanguage PL
				JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
				JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
					AND PL.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonLanguage'), Include_Null_Values
			),'{"PersonLanguage":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PP.PassportExpirationDate,
				PP.PassportIssuer,
				PP.PassportNumber,
				PT.PassportTypeCode
			FROM person.PersonPassport PP
				JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
					AND PP.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPassport'), Include_Null_Values
			),'{"PersonPassport":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PPS.PasswordSecurityQuestionAnswer,
				PSQ.PasswordSecurityQuestionCode
			FROM person.PersonPasswordSecurity PPS
				JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
					AND PPS.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPasswordSecurity'), Include_Null_Values
			),'{"PersonPasswordSecurity":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PQA.Degree,
				PQA.GraduationDate,
				PQA.Institution,
				PQA.StartDate,
				PQA.SubjectArea
			FROM person.PersonQualificationAcademic PQA
			WHERE PQA.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationAcademic'), Include_Null_Values
			),'{"PersonQualificationAcademic":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				PQC.CompletionDate,
				PQC.Course,
				PQC.ExpirationDate,
				PQC.Provider
			FROM person.PersonQualificationCertification PQC
			WHERE PQC.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationCertification'), Include_Null_Values
			),'{"PersonQualificationCertification":[]}')

		SET @cJSONData += ']'

		END
	--ENDIF

	SELECT 
		@cJSONData AS JSONData,
		@nIsForExport AS IsForExport

	SELECT
		SS.SystemSetupKey,
		SS.SystemSetupValue
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupKey IN ('HermisAPIKey', 'HermisAPISecretKey', 'HermisAPIURL', 'IsHermisIntegrationEnabled')

END
GO
--End procedure person.GetPersonJSONDataByPersonID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			NULL AS AcceptedDateFormatted,
			NULL AS AlternateManagerEmailAddress,
			NULL AS AlternateManagerName,
			NULL AS CurrencyName,
			1 AS IsActive,
			0 AS IsAmendment,
			NULL AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			NULL AS ClientFunctionName,
			0 AS InsuranceTypeID,
			NULL AS InsuranceTypeName,
			NULL AS EndDate,
			NULL AS EndDateFormatted,
			0 AS FeeRate,
			NULL AS ManagerEmailAddress,
			NULL AS ManagerName,
			NULL AS Notes,
			NULL AS ProjectCode,
			NULL AS ProjectLaborCode,
			0 AS ProjectLaborCodeID,
			NULL AS ProjectLaborCodeName,
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectRoleID,
			NULL AS ProjectRoleName,
			NULL AS ProjectTermOfReferenceCode,
			0 AS ProjectTermOfReferenceID,
			NULL AS ProjectTermOfReferenceName,
			NULL AS Status,
			NULL AS StartDate,
			NULL AS StartDateFormatted,
			NULL AS UserName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			PJ.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = PJ.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			PJ.HoursPerDay,
			PJ.ProjectCode,
			PJ.ProjectID,
			PJ.ProjectName,
			person.FormatPersonNameByPersonID(PN.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PN.UserName,
			PLC.ProjectLaborCode,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			CASE WHEN PP.AcceptedDate IS NOT NULL THEN 1 ELSE 0 END AS IsAmendment,
			PP.AlternateManagerEmailAddress,
			PP.AlternateManagerName,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.IsActive,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.Notes,
			PP.PersonID,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceCode,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = PJ.ClientID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PersonProject' 
			AND DE.EntityID = @PersonProjectID
	ORDER BY D.DocumentTitle, D.DocumentID

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.GetPersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return data from the person.PersonProjectExpense table
-- =========================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectID INT = ISNULL((SELECT PPE.PersonProjectID FROM person.PersonProjectExpense PPE WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID), 0)

	--PersonProjectExpense
	SELECT
		C.CurrencyName,
		C.ISOCurrencyCode,
		CF.ClientFunctionName,
		P.ProjectName,
		PM.PaymentMethodID, 
		PM.PaymentMethodName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPE.ClientCostCodeID,
		project.GetProjectCostCodeDescriptionByClientCostCodeID(PPE.ClientCostCodeID) AS ProjectCostCodeDescription,
		PPE.ExpenseAmount,
		PPE.ExpenseDate,
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		PPE.InvoiceID,
		PPE.IsProjectExpense,
		PPE.OwnNotes,
		PPE.PersonProjectExpenseID,
		PPE.PersonProjectID,
		PPE.ProjectManagerNotes,
		PPE.TaxAmount,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PPE.ISOCurrencyCode
		JOIN dropdown.PaymentMethod PM ON PM.PaymentMethodID = PPE.PaymentMethodID
			AND PPE.PersonProjectExpenseID = @PersonProjectExpenseID

	--PersonProjectExpenseClientCostCode & PersonProjectExpenseProjectCurrency
	EXEC person.GetPersonProjectExpenseDataByPersonProjectID @nPersonProjectID

	--PersonProjectExpenseDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PersonProjectExpense' 
			AND DE.EntityID = @PersonProjectExpenseID
	ORDER BY D.DocumentTitle, D.DocumentID

END
GO
--End procedure person.GetPersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.GetPersonUsernameByEmailAddress
EXEC utility.DropObject 'person.GetPersonUsernameByEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.04.07
-- Description:	A stored procedure to return a username from the person.Person table based on an email address
-- ===========================================================================================================
CREATE PROCEDURE person.GetPersonUsernameByEmailAddress

@EmailAddress VARCHAR(320)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.UserName,
		P.EmailAddress
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

END
GO
--End procedure person.GetPersonUsernameByEmailAddress

--Begin procedure person.SavePersonMerge
EXEC utility.DropObject 'person.SavePersonMerge'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.person table
-- ============================================================================
CREATE PROCEDURE person.SavePersonMerge

@MergeFromPersonID INT,
@MergeToPersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO client.ClientPerson
		(ClientID, PersonID, ClientPersonRoleCode, AcceptedDate)
	SELECT
		CP1.ClientID,
		@MergeToPersonID,
		CP1.ClientPersonRoleCode,
		CP1.AcceptedDate
	FROM client.ClientPerson CP1
	WHERE CP1.PersonID = @MergeFromPersonID
		AND NOT EXISTS
			(
			SELECT 1
			FROM client.ClientPerson CP2
			WHERE CP2.ClientID = CP1.ClientID
				AND CP2.PersonID = @MergeToPersonID
				AND CP2.ClientPersonRoleCode = CP1.ClientPersonRoleCode
			)

	UPDATE DE
	SET DE.EntityID = @MergeToPersonID
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = 'Person'
		AND DE.EntityID = @MergeFromPersonID

	UPDATE I
	SET I.PersonID = @MergeToPersonID
	FROM invoice.Invoice I
	WHERE I.PersonID = @MergeFromPersonID

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@MergeToPersonID,
		PP1.PermissionableLineage
	FROM person.PersonPermissionable PP1
	WHERE PP1.PersonID = @MergeFromPersonID
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP2
			WHERE PP2.PersonID = @MergeToPersonID
				AND PP2.PermissionableLineage = PP1.PermissionableLineage
			)

	UPDATE PP
	SET PP.PersonID = @MergeToPersonID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @MergeFromPersonID

	UPDATE SS
	SET SS.PersonID = @MergeToPersonID
	FROM person.SavedSearch SS
	WHERE SS.PersonID = @MergeFromPersonID

	UPDATE PU
	SET PU.PersonID = @MergeToPersonID
	FROM person.PersonUnavailability PU
	WHERE PU.PersonID = @MergeFromPersonID

	UPDATE PP
	SET PP.PersonID = @MergeToPersonID
	FROM project.ProjectPerson PP
	WHERE PP.PersonID = @MergeFromPersonID

	UPDATE EWSGP
	SET EWSGP.PersonID = @MergeToPersonID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.PersonID = @MergeFromPersonID

	UPDATE WSGP
	SET WSGP.PersonID = @MergeToPersonID
	FROM workflow.WorkflowStepGroupPerson WSGP
	WHERE WSGP.PersonID = @MergeFromPersonID

	DELETE P FROM person.Person P WHERE P.PersonID = @MergeFromPersonID

	DELETE T FROM client.ClientPerson T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM invoice.Invoice T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonAccount T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonLanguage T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonNextOfKin T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonPassport T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonPasswordSecurity T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonPermissionable T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonProject T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonProofOfLife T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonQualificationAcademic T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonQualificationCertification T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.PersonUnavailability T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)
	DELETE T FROM person.SavedSearch T WHERE NOT EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = T.PersonID)

END
GO
--End procedure person.SavePersonMerge

--Begin procedure project.ValidateDateWorked
EXEC utility.DropObject 'project.ValidateDateWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.15
-- Description:	A stored procedure to validate a DateWorked from a ProjectLocationID
-- =================================================================================
CREATE PROCEDURE project.ValidateDateWorked

@DateWorkedList VARCHAR(MAX),
@PersonProjectID INT,
@ProjectLocationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EndDate DATE
	DECLARE @StartDate DATE

	SELECT
		@StartDate = PP.StartDate,
		@EndDate = PP.EndDate
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	;
	WITH DW AS 
		(
		SELECT
			CAST(LTT.ListItem AS DATE) AS DateWorked,
			project.IsValidWorkDay(CAST(LTT.ListItem AS DATE), @ProjectLocationID) AS IsValidWorkDay
		FROM core.ListToTable(@DateWorkedList, ',') LTT
		)

	SELECT
		CASE 
			WHEN EXISTS (SELECT 1 FROM DW WHERE DW.DateWorked < @StartDate OR DW.DateWorked > @EndDate OR DW.IsValidWorkDay = 0)
			THEN 0
			ELSE 1
		END AS IsValid

END
GO
--End procedure project.ValidateDateWorked
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO

--Begin table core.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Person' AND ET.EmailTemplateCode = 'ForgotUsernameRequest')
 BEGIN

 INSERT INTO core.EmailTemplate
  (EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
 VALUES
  ('Person', 'ForgotUsernameRequest', 'Sent when a user requests his forgotten username', 'DeployAdviser Forgot Username Request', '<p>A forgot username request has been received for your DeployAdviser account.</p><p>If you did not request your username please report this message to your administrator.</p><p>Your username for your DeployAdvisor account is: <strong>[[ForgottenUsername]]</strong>.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')

 END
--ENDIF
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Person' AND ETF.PlaceHolderText = '[[ForgottenUsername]]')
 BEGIN

 INSERT INTO core.EmailTemplateField
  (EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
 VALUES
  ('Person', '[[ForgottenUsername]]', 'Forgotten Username')

 END
--ENDIF
GO
--End table core.EmailTemplateField

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonImport',
	@NewMenuItemCode = 'PersonMerge',
	@NewMenuItemLink = '/person/personmerge',
	@NewMenuItemText = 'Merge Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.PersonMerge'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Merge user accounts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonMerge', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonMerge', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit the contents of the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Delete an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Next of Kin data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Proof of Life data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Merge user accounts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonMerge', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonMerge', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export person data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Layout', @DESCRIPTION='Allow access to the Feedback icon in the site header', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Layout.Default.CanHaveFeedback', @PERMISSIONCODE='CanHaveFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Allow change access to selected fields on a deployment record AFTER the deployment has been accpted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='See the list of invoices on a project view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View.InvoiceList', @PERMISSIONCODE='InvoiceList';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Export invoice data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Allow the user to regenerate an invoice .pdf and add it to the document library for an existing invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.RegenerateInvoice', @PERMISSIONCODE='RegenerateInvoice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.5 - 2018.04.09 17.43.31')
GO
--End build tracking

