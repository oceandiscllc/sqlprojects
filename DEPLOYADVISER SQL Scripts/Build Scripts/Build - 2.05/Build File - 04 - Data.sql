USE DeployAdviser
GO

--Begin table core.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Person' AND ET.EmailTemplateCode = 'ForgotUsernameRequest')
 BEGIN

 INSERT INTO core.EmailTemplate
  (EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
 VALUES
  ('Person', 'ForgotUsernameRequest', 'Sent when a user requests his forgotten username', 'DeployAdviser Forgot Username Request', '<p>A forgot username request has been received for your DeployAdviser account.</p><p>If you did not request your username please report this message to your administrator.</p><p>Your username for your DeployAdvisor account is: <strong>[[ForgottenUsername]]</strong>.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')

 END
--ENDIF
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Person' AND ETF.PlaceHolderText = '[[ForgottenUsername]]')
 BEGIN

 INSERT INTO core.EmailTemplateField
  (EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
 VALUES
  ('Person', '[[ForgottenUsername]]', 'Forgotten Username')

 END
--ENDIF
GO
--End table core.EmailTemplateField

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonImport',
	@NewMenuItemCode = 'PersonMerge',
	@NewMenuItemLink = '/person/personmerge',
	@NewMenuItemText = 'Merge Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.PersonMerge'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Merge user accounts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonMerge', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonMerge', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable