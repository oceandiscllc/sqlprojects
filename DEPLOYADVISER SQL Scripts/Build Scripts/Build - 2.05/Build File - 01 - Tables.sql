USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'ClientLegalEntities', 'VARCHAR(MAX)'
GO
--End table client.Client

--Begin table document.Document
IF utility.HasColumn('document.Document', 'DocumentName') = 1
	EXEC sp_RENAME 'document.Document.DocumentName', 'DocumentTitle', 'COLUMN'
--ENDIF	
GO

IF utility.HasColumn('document.Document', 'DocumentSize') = 1
	BEGIN

	EXEC sp_RENAME 'document.Document.DocumentSize', 'PhysicalFileSize', 'COLUMN';
	EXEC sp_rename 'document.DF_Document_DocumentSize', 'DF_Document_PhysicalFileSize', 'OBJECT'

	END
--ENDIF	
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'LineManagerActionDateTime', 'DATETIME'
EXEC utility.AddColumn @TableName, 'LineManagerEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'LineManagerName', 'VARCHAR(200)'
GO
--End table invoice.Invoice

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

ALTER TABLE person.Person ALTER COLUMN SummaryBiography VARCHAR(MAX)

EXEC utility.DropColumn @TableName, 'GenderCode'
GO
--End table person.Person

--Begin table person.PersonAccount
EXEC utility.DropObject 'person.TR_PersonAccount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A trigger to ensure there is only 1 default account per person id
-- ==============================================================================
CREATE TRIGGER person.TR_PersonAccount ON person.PersonAccount AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	DECLARE @nPersonAccountID INT = ISNULL((SELECT TOP 1 INS.PersonAccountID FROM INSERTED INS WHERE INS.IsDefault = 1), 0)

	IF @nPersonAccountID > 0
		BEGIN

		UPDATE PA
		SET 
			PA.IsDefault = 0
		FROM person.PersonAccount PA
			JOIN INSERTED INS ON INS.PersonID = PA.PersonID
				AND PA.PersonAccountID <> @nPersonAccountID

		END
	--ENDIF

END
GO
--End table person.PersonAccount

--Begin table person.PersonProjectTime
EXEC utility.DropObject 'person.TR_PersonProjectTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER person.TR_PersonProjectTime ON person.PersonProjectTime AFTER UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	IF UPDATE(HoursWorked) OR UPDATE(ApplyVAT) OR UPDATE(ProjectManagerNotes)
		BEGIN

		UPDATE PPT
		SET PPT.IsLineManagerApproved = 0
		FROM person.PersonProjectTime PPT
			JOIN INSERTED I ON I.PersonProjectTimeID = PPT.PersonProjectTimeID

		UPDATE INV
		SET 
			INV.LineManagerActionDateTime = NULL,
			INV.LineManagerEmailAddress = NULL,
			INV.LineManagerName = NULL
		FROM invoice.Invoice INV
		WHERE EXISTS
			(
			SELECT 1
			FROM person.PersonProjectTime PPT
				JOIN INSERTED INS ON INS.PersonProjectTimeID = PPT.PersonProjectTimeID
					AND PPT.InvoiceID = INV.InvoiceID
			)

		END
	--ENDIF

END
GO
--End table person.PersonProjectTime