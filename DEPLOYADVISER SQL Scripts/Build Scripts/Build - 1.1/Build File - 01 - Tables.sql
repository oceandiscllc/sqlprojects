/* Build File - 01 - Tables */
USE DeployAdviser
GO

--Begin table core.MenuItem
DECLARE @TableName VARCHAR(250) = 'core.MenuItem'

EXEC utility.DropColumn @TableName, 'HasMenuItemAccessViaWorkflow'
GO
--End table core.MenuItem

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.AddColumn @TableName, 'AcceptedDate', 'DATE'
GO
--End table client.ClientPerson

--Begin table dropdown.InvoiceRejectionReason
DECLARE @TableName VARCHAR(250) = 'dropdown.InvoiceRejectionReason'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InvoiceRejectionReason
	(
	InvoiceRejectionReasonID INT IDENTITY(0,1) NOT NULL,
	InvoiceRejectionReasonCode VARCHAR(50),
	InvoiceRejectionReasonName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoiceRejectionReasonID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoiceRejectionReason', 'DisplayOrder,InvoiceRejectionReasonName'
GO
--End table dropdown.InvoiceRejectionReason

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.Invoice
	(
	InvoiceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT,
	StartDate DATE,
	EndDate DATE,
	InvoiceDateTime DATETIME,
	InvoiceAmount NUMERIC(18,2),
	ISOCurrencyCode CHAR(3),
	PersonInvoiceNumber VARCHAR(50),
	TaxRate NUMERIC(18,4),
	ExchangeRateXML VARCHAR(MAX),
	InvoiceRejectionReasonID INT,
	InvoiceStatus VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvoiceAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceRejectionReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceStatus', 'VARCHAR(50)', 'Submitted'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaxRate', 'NUMERIC(18,4)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InvoiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Invoice', 'PersonID,ProjectID', 'InvoiceID'
GO
--End table invoice.Invoice

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username
-- ==================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nCount INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') AS LastName
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		IF LEN(@cLastName) < 3
			SET @cLastName += 'XXX'
		--ENDIF

		SET @cUserName = UPPER(LEFT(@cLastName, 3))

		SELECT @nCount = COUNT(P.UserName) + 1
		FROM person.Person P
		WHERE LEFT(P.UserName, 3) = @cUserName

		SET @cUserName += RIGHT('000' + CAST(@nCount AS VARCHAR(10)), 3)

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '1'
GO
--Begin table person.PersonAccount

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'FeeRate', 'NUMERIC(18,2)', '0'
GO
--End table person.PersonProject

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.DropColumn @TableName, 'IsInvoiced'
EXEC utility.AddColumn @TableName, 'InvoiceID', 'INT', '0'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropColumn @TableName, 'Rate'
GO
--End table person.PersonProjectLocation

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.DropColumn @TableName, 'IsInvoiced'
EXEC utility.AddColumn @TableName, 'InvoiceID', 'INT', '0'
GO
--End table person.PersonProjectTime

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'IsForecastRequired', 'BIT', '0'
GO
--End table project.Project

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchResult
	(
	SearchResultID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeGroupCode VARCHAR(50),
	EntityData VARCHAR(MAX),
	EntityID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchResultID'
EXEC utility.SetIndexClustered @TableName, 'IX_SearchResult', 'EntityTypeCode,EntityTypeGroupCode,EntityID,PersonID'
GO
--End table reporting.SearchResult