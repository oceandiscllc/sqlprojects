/* Build File - 00 - Prerequisites */
USE DeployAdviser
GO

--Begin schema invoice
EXEC utility.AddSchema 'invoice'
EXEC utility.AddSchema 'reporting'
EXEC utility.AddSchema 'workflow'
GO
--End schema invoice
