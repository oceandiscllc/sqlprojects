/* Build File - 02 - Functions - Workflow */
USE DeployAdviser
GO

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT

	SELECT @nWorkflowStepCount = MAX(EWSGP.WorkflowStepNumber) 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT TOP 1 @nWorkflowStepNumber = EWSGP.WorkflowStepNumber 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID 
		AND EWSGP.IsComplete = 0 
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.28
-- Description:	A function to determine if a PeronID is in the current workflow step of a a specific EntityTypeCode and EntityID
-- =============================================================================================================================
CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID
			)
			SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep
	
END
GO
--End function workflow.IsPersonInCurrentWorkflowStep
