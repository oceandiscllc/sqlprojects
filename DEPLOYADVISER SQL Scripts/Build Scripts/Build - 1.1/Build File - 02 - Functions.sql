/* Build File - 02 - Functions */
USE DeployAdviser
GO

--Begin function core.DecimalFormatForJS
EXEC utility.DropObject 'core.DecimalFormatForJS'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a numeric value for javascript
-- ================================================================

CREATE FUNCTION core.DecimalFormatForJS
(
@Value NUMERIC(18,2)
)

RETURNS VARCHAR(50)

AS
BEGIN

	RETURN ' ' + CAST(@Value AS VARCHAR(50))

END
GO
--End function core.DecimalFormatForJS

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function dropdown.GetCurrencyNameFromISOCurrencyCode
EXEC utility.DropObject 'dropdown.GetCurrencyNameFromISOCurrencyCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a Currency name from an ISOCurrencyCode
-- =========================================================================

CREATE FUNCTION dropdown.GetCurrencyNameFromISOCurrencyCode
(
@ISOCurrencyCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCurrencyName VARCHAR(50)

	SELECT @cCurrencyName = C.CurrencyName
	FROM dropdown.Currency C
	WHERE C.ISOCurrencyCode = @ISOCurrencyCode

	RETURN ISNULL(@cCurrencyName, '')

END
GO
--End function dropdown.GetCurrencyNameFromISOCurrencyCode

--Begin function person.GetClientsByPersonID
EXEC utility.DropObject 'person.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all clients accessible to a PersonID
-- ====================================================================================================

CREATE FUNCTION person.GetClientsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ClientID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ClientID)
	SELECT 
		CP.ClientID
	FROM client.ClientPerson CP
	WHERE CP.PersonID = @PersonID

	UNION

	SELECT 
		C.ClientID
	FROM client.Client C
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person P
		WHERE P.PersonID = @PersonID
			AND P.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetClientsByPersonID

--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID)
	SELECT 
		PP.ProjectID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID
	FROM project.ProjectPerson PP
	WHERE PP.ProjectPersonRoleCode = 'ProjectManager'
		AND PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID
	FROM project.ProjectPerson PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientPerson CP ON CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.PersonID = PP.PersonID
			AND PP.PersonID = @PersonID

	UNION

	SELECT 
		PJ.ProjectID
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function project.GetProjectCostCodeDescriptionByClientCostCodeID
EXEC utility.DropObject 'project.GetProjectCostCodeDescriptionByClientCostCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a per diem amount for a PersonProject record
-- ==============================================================================

CREATE FUNCTION project.GetProjectCostCodeDescriptionByClientCostCodeID
(
@ClientCostCodeID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cProjectCostCodeDescription VARCHAR(250)

	SELECT 
		@cProjectCostCodeDescription = ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription)
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ClientCostCodeID = @ClientCostCodeID

	RETURN ISNULL(@cProjectCostCodeDescription, '')

END
GO
--End function project.GetProjectCostCodeDescriptionByClientCostCodeID

--Begin function utility.StripCharacters
EXEC utility.DropObject 'utility.StripCharacters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION utility.StripCharacters
(
@String NVARCHAR(MAX), 
@Pattern VARCHAR(255)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	SET @Pattern =  '%[' + @Pattern + ']%'

	WHILE PatIndex(@Pattern, @String) > 0
		SET @String = Stuff(@String, PatIndex(@Pattern, @String), 1, '')
	--END WHILE

	RETURN @String

END
GO
--End function utility.StripCharacters
