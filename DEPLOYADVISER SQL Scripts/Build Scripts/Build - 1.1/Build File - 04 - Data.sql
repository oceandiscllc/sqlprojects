/* Build File - 04 - Data */
USE DeployAdviser
GO

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Invoice', 'Approve', 'Sent when an invoice is approved', 'An Invoice You Have Submitted Has Been Approved', '<p>The following invoice has been approved:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'DecrementWorkflow', 'Sent when an invoice is rejected', 'An Invoice You Have Submitted Has Been Rejected', '<p>The following invoice has been rejected:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Rejection Reason: &nbsp;[[InvoiceRejectionReasonName]]<br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'IncrementWorkflow', 'Sent when an invoice is forwarded in the workflow', 'A Consultant Invoice Has Been Forwarded For Your Review', '<p>[[PersonNameFormatted]] has reviewed the following invoice and is forwarding it for your approval:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'Submit', 'Sent when an invoice is submitted', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonExisting', 'Sent when an existing person is invited to DeployAdviser', 'You Have Been Invited To Affiliate With A Client In DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonNew', 'Sent when a new person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'InvitePersonProject', 'Sent when a project assignment offer is made', 'A Project Assignment Invitation Has Been Made', '<p>[[ClientName]] invites you to accept the following project assignment:</p><p>Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ClientTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Any', '[[SiteLink]]', 'Embedded Site Link'),
	('Any', '[[SiteURL]]', 'Site URL'),
	('Invoice', '[[ClientName]]', 'Client Name'),
	('Invoice', '[[InvoicePersonNameFormatted]]', 'Consultant Name'),
	('Invoice', '[[PersonNameFormatted]]', 'Person Name'),
	('Invoice', '[[ProjectName]]', 'Project Name'),
	('Invoice', '[[EndDateFormatted]]', 'Invoice End Date'),
	('Invoice', '[[InvoiceAmount]]', 'Invoice Amount'),
	('Invoice', '[[InvoiceDateTimeFormatted]]', 'Invoice Date'),
	('Invoice', '[[InvoiceRejectionReasonName]]', 'Rejection Reason'),
	('Invoice', '[[ISOCurrencyCode]]', 'Invoice Currency'),
	('Invoice', '[[PersonInvoiceNumber]]', 'Invoice Number'),
	('Invoice', '[[StartDateFormatted]]', 'Invoice Start Date'),
	('Invoice', '[[WorkflowComments]]', 'Comments'),
	('Person', '[[ClientName]]', 'Client Name'),
	('Person', '[[EmailAddress]]', 'Sender Email'),
	('Person', '[[PasswordTokenLink]]', 'Embedded Reset Password Link'),
	('Person', '[[PasswordTokenURL]]', 'Site Reset Password URL'),
	('Person', '[[PersonNameFormattedLong]]', 'Sender Name (Long)'),
	('Person', '[[PersonNameFormattedShort]]', 'Sender Name (Short)'),
	('Person', '[[RoleName]]', 'Role Name'),
	('Person', '[[UserName]]', 'User Name'),
	('PersonProject', '[[ClientFunctionName]]', 'Function'),
	('PersonProject', '[[ClientName]]', 'Client Name'),
	('PersonProject', '[[ClientTermOfReferenceName]]', 'Term Of Reference'),
	('PersonProject', '[[PersonNameFormatted]]', 'Person Name'),
	('PersonProject', '[[ProjectName]]', 'Project Name')
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Workflow', 
	@EntityTypeName = 'Workflow', 
	@EntityTypeNamePlural = 'Workflows',
	@SchemaName = 'workflow', 
	@TableName = 'Workflow', 
	@PrimaryKeyFieldName = 'WorkflowID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectList',
	@Icon = 'fa fa-fw fa-user',
	@NewMenuItemCode = 'PersonInvite',
	@NewMenuItemLink = '/person/invite',
	@NewMenuItemText = 'Invite',
	@PermissionableLineageList = 'Person.Invite'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SystemSetupList',
	@NewMenuItemCode = 'WorkflowList',
	@NewMenuItemLink = '/workflow/list',
	@NewMenuItemText = 'Workflows',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Workflow.List'
GO

--Begin Time & Expenses Submenu --
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PersonProjectExpenseList',
	@NewMenuItemLink = '/personprojectexpense/list'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonProjectExpenseList',
	@NewMenuItemCode = 'InvoiceSetup',
	@NewMenuItemLink = '/invoice/setup',
	@NewMenuItemText = 'Submit Invoice',
	@ParentMenuItemCode = 'TimeExpense',
	@PermissionableLineageList = 'Invoice.Setup'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'InvoiceSetup',
	@NewMenuItemCode = 'InvoiceList',
	@NewMenuItemLink = '/invoice/list',
	@NewMenuItemText = 'Review Invoices',
	@ParentMenuItemCode = 'TimeExpense'
GO
--End Time & Expenses Submenu --

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'TimeExpense'
GO
--End table core.MenuItem

--Begin table dropdown.InvoiceRejectionReason
TRUNCATE TABLE dropdown.InvoiceRejectionReason
GO

EXEC utility.InsertIdentityValue 'dropdown.InvoiceRejectionReason', 'InvoiceRejectionReasonID', 0
GO

INSERT INTO dropdown.InvoiceRejectionReason
	(InvoiceRejectionReasonName, DisplayOrder)
VALUES
	('Missing Receipts', 0),
	('Ineligible Expense', 0),
	('Ineligible Time Claim', 0),
	('Incorrect Payment Rate', 0),
	('Duplicate Claim', 0),
	('Insufficient Information', 0),
	('Work on non-authorised day', 0),
	('Missing Engagement Forecast', 0),
	('Other', 99)
GO
--End table dropdown.InvoiceRejectionReason

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleCode, RoleName, DisplayOrder) 
VALUES
	('Consultant', 'Consultant', 1),
	('ProjectManager', 'Project Manager', 2),
	('Administrator', 'Consultancy Administrator', 3)
GO
--End table dropdown.Role

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
