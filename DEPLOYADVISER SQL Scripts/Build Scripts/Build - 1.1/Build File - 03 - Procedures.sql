/* Build File - 03 - Procedures */
USE DeployAdviser
GO

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplate
	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	--EmailTemplateField
	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplateData
	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	--EmailTemplateFieldData
	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

	--EmailTemplateGlobalData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN

			UPDATE MI
			SET 
				MI.Icon = CASE WHEN @NewMenuItemText IS NOT NULL THEN CASE WHEN LEN(RTRIM(@Icon)) = 0 THEN NULL ELSE @Icon END ELSE MI.Icon END,
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab,
				MI.MenuItemLink = CASE WHEN LEN(RTRIM(@NewMenuItemLink)) = 0 THEN NULL ELSE CASE WHEN @NewMenuItemLink IS NOT NULL THEN @NewMenuItemLink ELSE MI.MenuItemLink END END,
				MI.MenuItemText = CASE WHEN @NewMenuItemText IS NOT NULL THEN @NewMenuItemText ELSE MI.MenuItemText END,
				MI.ParentMenuItemID = CASE WHEN @nOldParentMenuItemID <> @nParentMenuItemID THEN @nParentMenuItemID ELSE MI.ParentMenuItemID END
			FROM core.MenuItem MI 
			WHERE MI.MenuItemID = @nNewMenuItemID

			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentID INT,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = @DocumentID
		AND DE.EntityTypeCode = @EntityTypeCode
		AND (DE.EntityTypeSubCode = @EntityTypeSubCode OR (@EntityTypeSubCode IS NULL AND DE.EntityTypeSubCode IS NULL))
		AND DE.EntityID = @EntityID 

END
GO
--End procedure document.PurgeEntityDocuments

--Begin table dropdown.GetInvoiceRejectionReasonData
EXEC Utility.DropObject 'dropdown.GetInvoiceRejectionReasonData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.InvoiceRejectionReason table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetInvoiceRejectionReasonData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InvoiceRejectionReasonID,
		T.InvoiceRejectionReasonCode,
		T.InvoiceRejectionReasonName
	FROM dropdown.InvoiceRejectionReason T
	WHERE (T.InvoiceRejectionReasonID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InvoiceRejectionReasonName, T.InvoiceRejectionReasonID

END
GO
--End procedure dropdown.GetInvoiceRejectionReasonData

--Begin procedure eventlog.LogInvoiceAction
EXEC utility.DropObject 'eventlog.LogInvoiceAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogInvoiceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Invoice'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	ELSE 
		BEGIN

		DECLARE @cPersonProjectExpenses VARCHAR(MAX) = ''
		
		SELECT @cPersonProjectExpenses = COALESCE(@cPersonProjectExpenses, '') + D.PersonProjectExpense
		FROM
			(
			SELECT
				(SELECT T.PersonProjectExpenseID FOR XML RAW(''), ELEMENTS) AS PersonProjectExpense
			FROM person.PersonProjectExpense T 
			WHERE T.InvoiceID = @EntityID
			) D

		DECLARE @cPersonProjectTimes VARCHAR(MAX) = ''
		
		SELECT @cPersonProjectTimes = COALESCE(@cPersonProjectTimes, '') + D.PersonProjectTime
		FROM
			(
			SELECT
				(SELECT T.PersonProjectTimeID FOR XML RAW(''), ELEMENTS) AS PersonProjectTime
			FROM person.PersonProjectTime T 
			WHERE T.InvoiceID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML),
			CAST('<PersonProjectExpenses>' + ISNULL(@cPersonProjectExpenses, '') + '</PersonProjectExpenses>' AS XML),
			CAST('<PersonProjectTimes>' + ISNULL(@cPersonProjectTimes, '') + '</PersonProjectTimes>' AS XML)
			FOR XML RAW('Invoice'), ELEMENTS
			)
		FROM invoice.Invoice T
		WHERE T.InvoiceID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogInvoiceAction

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@ExchangeRateXML VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)
	DECLARE @tExchangeRates TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ISOCurrencyCode CHAR(3), ExchangeRate NUMERIC(18,5))
	DECLARE @xExchangeRates XML = CAST(@ExchangeRateXML AS XML)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ISOCurrencyCode CHAR(3), 
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DSAAmount NUMERIC(18,2),
		DSAISOCurrencyCode CHAR(3), 
		DPAAmount NUMERIC(18,2),
		DPAISOCurrencyCode CHAR(3) 
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		FeeRate NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		HoursWorked INT,
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityData, PersonID)
	VALUES
		('Invoice', 'ExchangeRateXML', @ExchangeRateXML, @PersonID)

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	INSERT INTO @tExchangeRates
		(ISOCurrencyCode, ExchangeRate)
	SELECT 
		T.x.value('ISOCurrencyCode[1]','CHAR(3)') AS ISOCurrencyCode,
		T.x.value('ExchangeRate[1]','NUMERIC(18,5)') AS ExchangeRate
	FROM @xExchangeRates.nodes('/ExchangeRates') T(x)

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ISOCurrencyCode, ExpenseAmount, TaxAmount, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ISOCurrencyCode, 
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DSAAmount, DSAISOCurrencyCode, DPAAmount, DPAISOCurrencyCode)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PL.DPAISOCurrencyCode
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, FeeRate, ISOCurrencyCode, HoursWorked, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		C.ClientName,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate
	FROM person.Person PN, project.Project PJ
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
	WHERE PN.PersonID = @PersonID
		AND PJ.ProjectID = @ProjectID

	--InvoiceExchangeRate
	SELECT
		TER.ExchangeRate,
		TER.ISOCurrencyCode
	FROM @tExchangeRates TER
	ORDER BY TER.ISOCurrencyCode

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		CAST(TER.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TER.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TEL.ISOCurrencyCode
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT 
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(TPL.DSAAmount * TER1.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		TPL.DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(TPL.DPAAmount * TER2.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount
	FROM @tPerDiemLog TPL
		JOIN @tExchangeRates TER1 ON TER1.ISOCurrencyCode = TPL.DSAISOCurrencyCode
		JOIN @tExchangeRates TER2 ON TER2.ISOCurrencyCode = TPL.DPAISOCurrencyCode
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TER1.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		SUM(TPL.DPAAmount) AS DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(SUM(TPL.DPAAmount * TER2.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount
	FROM @tPerDiemLog TPL
		JOIN @tExchangeRates TER1 ON TER1.ISOCurrencyCode = TPL.DSAISOCurrencyCode
		JOIN @tExchangeRates TER2 ON TER2.ISOCurrencyCode = TPL.DPAISOCurrencyCode
	GROUP BY TPL.ProjectLocationName, TPL.DSAAmount, TPL.DSAISOCurrencyCode, TPL.DPAISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TPL.DSAISOCurrencyCode

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TPL.DSAISOCurrencyCode

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TER.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TER.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TEL.ISOCurrencyCode
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	UNION

	SELECT
		6,
		'Total Claim' AS DisplayText,
		SUM(INS.InvoiceAmount) + SUM(INS.InvoiceVAT) AS InvoiceAmount,
		NULL AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceReviewData
EXEC utility.DropObject 'invoice.GetInvoiceReviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoiceReviewData

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nTaxRate NUMERIC(18,4)
	DECLARE @tExchangeRates TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ISOCurrencyCode CHAR(3), ExchangeRate NUMERIC(18,5))
	DECLARE @xExchangeRates XML

	SELECT
		@nTaxRate = I.TaxRate,
		@xExchangeRates = CAST(I.ExchangeRateXML AS XML)
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ISOCurrencyCode CHAR(3), 
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DSAAmount NUMERIC(18,2),
		DSAISOCurrencyCode CHAR(3), 
		DPAAmount NUMERIC(18,2),
		DPAISOCurrencyCode CHAR(3) 
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		FeeRate NUMERIC(18,2),
		ISOCurrencyCode CHAR(3),
		HoursWorked INT,
		ProjectManagerNotes VARCHAR(250)
		)

	INSERT INTO @tExchangeRates
		(ISOCurrencyCode, ExchangeRate)
	SELECT 
		T.x.value('ISOCurrencyCode[1]','CHAR(3)') AS ISOCurrencyCode,
		T.x.value('ExchangeRate[1]','NUMERIC(18,5)') AS ExchangeRate
	FROM @xExchangeRates.nodes('/ExchangeRates') T(x)

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ISOCurrencyCode, ExpenseAmount, TaxAmount, DocumentGUID, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ISOCurrencyCode, 
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DSAAmount, DSAISOCurrencyCode, DPAAmount, DPAISOCurrencyCode)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PL.DPAISOCurrencyCode
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, FeeRate, ISOCurrencyCode, HoursWorked, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.HoursWorked,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

	--InvoiceExchangeRate
	SELECT
		TER.ExchangeRate,
		TER.ISOCurrencyCode
	FROM @tExchangeRates TER
	ORDER BY TER.ISOCurrencyCode

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		CAST(TER.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TER.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TEL.ISOCurrencyCode
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT 
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(TPL.DSAAmount * TER1.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		TPL.DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(TPL.DPAAmount * TER2.ExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount
	FROM @tPerDiemLog TPL
		JOIN @tExchangeRates TER1 ON TER1.ISOCurrencyCode = TPL.DSAISOCurrencyCode
		JOIN @tExchangeRates TER2 ON TER2.ISOCurrencyCode = TPL.DPAISOCurrencyCode
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TER1.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		SUM(TPL.DPAAmount) AS DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(SUM(TPL.DPAAmount * TER2.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount
	FROM @tPerDiemLog TPL
		JOIN @tExchangeRates TER1 ON TER1.ISOCurrencyCode = TPL.DSAISOCurrencyCode
		JOIN @tExchangeRates TER2 ON TER2.ISOCurrencyCode = TPL.DPAISOCurrencyCode
	GROUP BY TPL.ProjectLocationName, TPL.DSAAmount, TPL.DSAISOCurrencyCode, TPL.DPAISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
		JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * TTL.FeeRate * TER.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TTL.ISOCurrencyCode

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TPL.DSAISOCurrencyCode

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TER.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TPL.DSAISOCurrencyCode

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TER.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TER.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
			JOIN @tExchangeRates TER ON TER.ISOCurrencyCode = TEL.ISOCurrencyCode
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	UNION

	SELECT
		6,
		'Total Claim' AS DisplayText,
		SUM(INS.InvoiceAmount) + SUM(INS.InvoiceVAT) AS InvoiceAmount,
		NULL AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--InvoiceWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID

END
GO
--End procedure invoice.GetInvoiceReviewData

--Begin procedure invoice.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to data from the invoice.Invoice Table
-- ======================================================================
CREATE PROCEDURE invoice.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceSummaryData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		@PersonID AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.InvoiceAmount,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		I.PersonID AS InvoicePersonID,
		(SELECT PN.EmailAddress FROM person.Person PN WHERE PN.PersonID = I.PersonID) AS InvoicePersonEmailAddress,
		person.FormatPersonNameByPersonID(I.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		IRR.InvoiceRejectionReasonName,
		P.ProjectName
	FROM invoice.Invoice I
		JOIN dropdown.InvoiceRejectionReason IRR ON IRR.InvoiceRejectionReasonID = I.InvoiceRejectionReasonID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND I.InvoiceID = @InvoiceID

	--InvoiceWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('approve','create','decrementworkflow','incrementworkflow')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID

END
GO
--End procedure invoice.GetInvoiceSummaryDataByInvoiceID

--Begin procedure invoice.GetProjectISOCurrencyCodes
EXEC utility.DropObject 'invoice.GetProjectISOCurrencyCodes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return the ISOCurrencyCodes used by a project
-- ================================================================================
CREATE PROCEDURE invoice.GetProjectISOCurrencyCodes

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PPE.ISOCurrencyCode
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			AND PPE.IsProjectExpense = 1
			AND PPE.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)

	UNION

	SELECT
		PL.DPAISOCurrencyCode
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)

	UNION

	SELECT
		PL.DSAISOCurrencyCode
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)

	UNION

	SELECT
		PP.ISOCurrencyCode
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)

END
GO
--End procedure invoice.GetProjectISOCurrencyCodes

--Begin procedure invoice.RejectInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.RejectInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to reset the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ====================================================================================================================================
CREATE PROCEDURE invoice.RejectInvoiceByInvoiceID

@InvoiceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PPE
	SET PPE.InvoiceID = 0
	FROM person.PersonProjectExpense PPE
	WHERE PPE.InvoiceID = @InvoiceID

	UPDATE PPT
	SET PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
	WHERE PPT.InvoiceID = @InvoiceID

	EXEC workflow.decrementWorkflow 'Invoice', @InvoiceID

END
GO
--End procedure invoice.RejectInvoiceByInvoiceID

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientID INT = (SELECT P.ClientID FROM project.Project P JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID AND I.InvoiceID = @InvoiceID)

	UPDATE PPE
	SET PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID

	;
	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE T.ClientID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND EXISTS 
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 

					UNION 

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1
					)
				AND (@AccessCode <> 'AddUpdate' OR (T.EndDate >= getDate() AND T.AcceptedDate IS NULL))

		END
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF workflow.IsPersonInCurrentWorkflowStep(@EntityTypeCode, @EntityID, @PersonID) = 1 OR EXISTS (SELECT 1 FROM invoice.Invoice I WHERE I.InvoiceID = @EntityID AND I.PersonID = @PersonID)
			SET @bHasAccess = 1
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM project.Project T WHERE T.ProjectID = @EntityID AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID UNION SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GeneratePassword
EXEC Utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250))

	--ClientPerson
	IF EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.PersonID = @PersonID
				AND CP.AcceptedDate IS NULL
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Accept client affilliation invitation')
	--ENDIF

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter bank details for invoicing')
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Add next of kin details')
	--ENDIF

	--PersonProject
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND PP.AcceptedDate IS NULL
				AND PP.EndDate <= getDate()
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Accept project engagement')
	--ENDIF

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Add three proof of life questions')
	--ENDIF

	SELECT T.PendingAction
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonAccountByPersonAccountID
EXEC utility.DropObject 'person.GetPersonAccountByPersonAccountID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE person.GetPersonAccountByPersonAccountID

@PersonAccountID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonAccountID = @PersonAccountID

END
GO
--End procedure person.GetPersonAccountByPersonAccountID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.NickName,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		IsActive BIT,
		ProjectLocationID INT,
		ProjectLocationName VARCHAR(100),
		Days INT NOT NULL DEFAULT 0,
		HoursPerDay INT NOT NULL DEFAULT 8
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS ClientTermOfReferenceID,
			'' AS ClientTermOfReferenceName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			0 AS FeeRate,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName
		FROM project.Project P
		WHERE P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CTOR.ClientTermOfReferenceID,
			CTOR.ClientTermOfReferenceName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			P.ProjectID,
			P.ProjectName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ClientTermOfReference CTOR ON CTOR.ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(IsActive, ProjectLocationID, ProjectLocationName)
		SELECT
			PL.IsActive,
			PL.ProjectLocationID,
			PL.ProjectLocationName
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET 
			TPPL.Days = PPL.Days,
			TPPL.HoursPerDay = PPL.HoursPerDay
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		T.IsActive,
		T.ProjectLocationID,
		T.ProjectLocationName,
		T.Days,
		T.HoursPerDay
	FROM @tPersonProjectLocation T
	ORDER BY T.ProjectLocationName, T.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--End procedure person.GetPersonProjectCurrency
EXEC utility.DropObject 'person.GetPersonProjectCurrency'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return the ISOCurrencyCodes used by a PersonProject combination
-- ==================================================================================================
CREATE PROCEDURE person.GetPersonProjectCurrency

@PersonID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM person.PersonProject PP
		JOIN dropdown.CUrrency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
	ORDER BY C.CurrencyName, C.ISOCurrencyCode

END
GO
--End procedure person.GetPersonProjectCurrency

--Begin procedure person.GetPersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.GetPersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return data from the person.PersonProjectExpense table
-- =========================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectID INT = ISNULL((SELECT PPE.PersonProjectID FROM person.PersonProjectExpense PPE WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID), 0)

	--PersonProjectExpense
	SELECT
		C.CurrencyName,
		C.ISOCurrencyCode,
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		PM.PaymentMethodID, 
		PM.PaymentMethodName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPE.ClientCostCodeID,
		project.GetProjectCostCodeDescriptionByClientCostCodeID(PPE.ClientCostCodeID) AS ProjectCostCodeDescription,
		PPE.ExpenseAmount,
		PPE.ExpenseDate,
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		PPE.IsProjectExpense,
		PPE.OwnNotes,
		PPE.PersonProjectExpenseID,
		PPE.PersonProjectID,
		PPE.ProjectManagerNotes,
		PPE.TaxAmount
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PPE.ISOCurrencyCode
		JOIN dropdown.PaymentMethod PM ON PM.PaymentMethodID = PPE.PaymentMethodID
			AND PPE.PersonProjectExpenseID = @PersonProjectExpenseID

	--PersonProjectExpenseClientCostCode & PersonProjectExpenseProjectCurrency
	EXEC person.GetPersonProjectExpenseDataByPersonProjectID @nPersonProjectID

	--PersonProjectExpenseDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'PersonProjectExpense' 
			AND DE.EntityID = @PersonProjectExpenseID

END
GO
--End procedure person.GetPersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		CTOR.ClientTermOfReferenceName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN client.ClientTermOfReference CTOR ON CTOR .ClientTermOfReferenceID = PP.ClientTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
--
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	Modified to return a recordset
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT P.PersonID
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		UserName VARCHAR(250),
		CountryCallingCodeID INT,
		CellPhone VARCHAR(64),
		IsPhoneVerified BIT,
		ClientAdministratorCount INT,
		ClientProjectManagerCount INT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR (@bIsTwoFactorEnabled = 1 AND (P.CellPhone IS NULL OR LEN(LTRIM(P.CellPhone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cCellPhone = P.CellPhone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,UserName,CountryCallingCodeID,CellPhone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cUserName,
			@nCountryCallingCodeID,
			@cCellPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure person.ValidateUserName
EXEC Utility.DropObject 'person.ValidateUserName'
GO
--End procedure person.ValidateUserName

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		C.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.ValidateHoursWorked
EXEC utility.DropObject 'project.ValidateHoursWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.25
-- Description:	A stored procedure to validate HoursWorked from a PersonProjectID
-- ==============================================================================
CREATE PROCEDURE project.ValidateHoursWorked

@HoursWorked INT,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectTimeAllotted INT
	DECLARE @nPersonProjectTimeExpended INT

	SELECT
		@nPersonProjectTimeAllotted = SUM((PPL.Days * PPL.HoursPerDay))
	FROM person.PersonProjectLocation PPL
	WHERE PPL.PersonProjectID = @PersonProjectID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID

	SELECT
		CASE
			WHEN ISNULL(@nPersonProjectTimeAllotted, 0) >= ISNULL(@nPersonProjectTimeExpended, 0) + @HoursWorked
			THEN 1
			ELSE 0
		END AS IsValid

END
GO
--End procedure project.ValidateHoursWorked
