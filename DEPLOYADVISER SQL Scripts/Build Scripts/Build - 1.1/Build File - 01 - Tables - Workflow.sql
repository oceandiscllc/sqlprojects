/* Build File - 01 - Tables - Workflow */
USE DeployAdviser
GO

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @cTableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.EntityWorkflowStepGroupPerson
	(
	EntityWorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50), 
	EntityID INT, 
	WorkflowID INT, 
	WorkflowStepID INT, 
	WorkflowStepGroupID INT, 
	WorkflowName VARCHAR(250), 
	WorkflowStepNumber INT, 
	WorkflowStepName VARCHAR(250), 
	WorkflowStepGroupName VARCHAR(250), 
	PersonID INT, 
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @cTableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowID', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepID', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepNumber', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @cTableName, 'IsComplete', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'EntityWorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @cTableName, 'IX_EntityWorkflowStepGroupPerson', 'EntityID,WorkflowStepID'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.Workflow
DECLARE @cTableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.Workflow
	(
	WorkflowID INT IDENTITY(1,1) NOT NULL,
	WorkflowName VARCHAR(250), 
	EntityTypeCode VARCHAR(50),
	IsActive BIT,
	ClientID INT
	)

EXEC utility.SetDefaultConstraint @cTableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @cTableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowID'
EXEC utility.SetIndexClustered @cTableName, 'IX_Workflow', 'EntityTypeCode,ClientID,WorkflowID'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @cTableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.WorkflowStep
	(
	WorkflowStepID INT IDENTITY(1,1) NOT NULL,
	WorkflowID INT,
	WorkflowStepNumber INT,
	WorkflowStepName VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowStepID'
EXEC utility.SetIndexClustered @cTableName, 'IX_WorkflowStep', 'WorkflowID,WorkflowStepNumber,WorkflowStepName'
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepGroup
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroup'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroup
	(
	WorkflowStepGroupID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepID INT,
	WorkflowStepGroupName VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroup', 'WorkflowStepID,WorkflowStepGroupName'
GO
--End table workflow.WorkflowStepGroup

--Begin table workflow.WorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroupPerson
	(
	WorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepGroupID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroupPerson', 'WorkflowStepGroupID,PersonID'
GO
--End table workflow.WorkflowStepGroupPerson
