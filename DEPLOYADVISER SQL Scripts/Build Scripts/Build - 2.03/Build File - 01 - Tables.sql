USE DeployAdviser
GO

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table document.Document

--Begin table dropdown.Gender
DECLARE @TableName VARCHAR(250) = 'dropdown.Gender'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Gender
	(
	GenderID INT IDENTITY(0,1) NOT NULL,
	GenderCode VARCHAR(50),
	GenderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'GenderID'
EXEC utility.SetIndexClustered @TableName, 'IX_Gender', 'DisplayOrder,GenderName'
GO
--End table dropdown.Gender

--Begin table dropdown.LanguageProficiency
DECLARE @TableName VARCHAR(250) = 'dropdown.LanguageProficiency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LanguageProficiency
	(
	LanguageProficiencyID INT IDENTITY(0,1) NOT NULL,
	LanguageProficiencyCode VARCHAR(50),
	LanguageProficiencyName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LanguageProficiencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_LanguageProficiency', 'DisplayOrder,LanguageProficiencyName'
GO
--End table dropdown.LanguageProficiency

--Begin table dropdown.PassportType
DECLARE @TableName VARCHAR(250) = 'dropdown.PassportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PassportType
	(
	PassportTypeID INT IDENTITY(0,1) NOT NULL,
	PassportTypeCode VARCHAR(50),
	PassportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PassportTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PassportType', 'DisplayOrder,PassportTypeName'
GO
--End table dropdown.PassportType

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

EXEC utility.AddColumn @TableName, 'PasswordSecurityQuestionCode', 'VARCHAR(50)'
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table person.Person
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.Person') AND SC.Name = 'CitizenshipISOCountryCode2')
	EXEC sp_RENAME 'person.Person.CitizenshipISOCountryCode2', 'Citizenship1ISOCountryCode2', 'COLUMN';
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'Citizenship1ISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'Citizenship2ISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'GenderID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'HomePhone', 'VARCHAR(15)'
EXEC utility.AddColumn @TableName, 'HomePhoneCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'MiddleName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthMunicipality', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PreferredName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'SummaryBiography', 'VARCHAR(2000)'
EXEC utility.AddColumn @TableName, 'WorkPhone', 'VARCHAR(15)'
EXEC utility.AddColumn @TableName, 'WorkPhoneCountryCallingCodeID', 'INT', '0'

EXEC utility.DropColumn @TableName, 'PassportExpirationDate'
EXEC utility.DropColumn @TableName, 'PassportIssuer'
EXEC utility.DropColumn @TableName, 'PassportNumber'
EXEC utility.DropColumn @TableName, 'PassportType'
GO
--End table person.Person

--Begin table person.PersonLanguage
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'OralLevel')
	EXEC sp_RENAME 'person.PersonLanguage.OralLevel', 'SpeakingLanguageProficiencyID', 'COLUMN';
--ENDIF
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'ReadLevel')
	EXEC sp_RENAME 'person.PersonLanguage.ReadLevel', 'ReadingLanguageProficiencyID', 'COLUMN';
--ENDIF
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'WriteLevel')
	EXEC sp_RENAME 'person.PersonLanguage.WriteLevel', 'WritingLanguageProficiencyID', 'COLUMN';
--ENDIF
GO
--End table person.PersonLanguage

--Begin table person.PersonPassport
DECLARE @TableName VARCHAR(250) = 'person.PersonPassport'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonPassport
	(
	PersonPassportID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PassportExpirationDate DATE,
	PassportIssuer VARCHAR(50),
	PassportNumber VARCHAR(50),
	PassportTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PassportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPassportID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPassport', 'PersonID'
GO
--End table person.PersonPassport

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table person.PersonProject

--Begin table person.PersonQualification
DECLARE @TableName VARCHAR(250) = 'person.PersonQualification'

EXEC utility.DropObject @TableName
GO
--End table person.PersonQualification

--Begin table person.PersonQualificationAcademic
DECLARE @TableName VARCHAR(250) = 'person.PersonQualificationAcademic'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonQualificationAcademic
	(
	PersonQualificationAcademicID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Institution VARCHAR(50),
	SubjectArea VARCHAR(50),
	GraduationDate DATE,
	StartDate DATE,
	Degree VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonQualificationAcademicID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonQualificationAcademic', 'PersonID'
GO
--End table person.PersonQualificationAcademic

--Begin table person.PersonQualificationCertification
DECLARE @TableName VARCHAR(250) = 'person.PersonQualificationCertification'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonQualificationCertification
	(
	PersonQualificationCertificationID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Provider VARCHAR(50),
	Course VARCHAR(50),
	CompletionDate DATE,
	ExpirationDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonQualificationCertificationID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonQualificationCertification', 'PersonID'
GO
--End table person.PersonQualificationCertification

--Begin table person.SavedSearch
DECLARE @TableName VARCHAR(250) = 'person.SavedSearch'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.SavedSearch
	(
	SavedSearchID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EntityTypeCode VARCHAR(50),
	SavedSearchName VARCHAR(50),
	SavedSearchData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SavedSearchID'
EXEC utility.SetIndexClustered @TableName, 'IX_SavedSearch', 'PersonID,EntityTypeCode'
GO
--End table person.SavedSearch

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table project.Project
