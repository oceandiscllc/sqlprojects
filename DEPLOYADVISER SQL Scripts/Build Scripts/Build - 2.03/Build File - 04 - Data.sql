USE DeployAdviser
GO

--Begin table core.SystemSetup
DECLARE @cGUUID VARCHAR(100) = (SELECT CAST(NEWID() AS VARCHAR(100)))
EXEC core.SystemSetupAddUpdate 'HermisAPIKey', NULL, @cGUUID

SELECT @cGUUID = CAST(NEWID() AS VARCHAR(100))
EXEC core.SystemSetupAddUpdate 'HermisAPISecretKey', NULL, @cGUUID

IF (SELECT core.GetSystemSetupValueBySystemSetupKey('Environment', '')) = 'Dev'
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'DevURL'
ELSE IF	(SELECT core.GetSystemSetupValueBySystemSetupKey('Environment', '')) = 'UAT'
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'UATURL'
ELSE
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'ProdURL'
--ENDIF

EXEC core.SystemSetupAddUpdate 'IsHermisIntegrationEnabled', 'Indicates if the system should call the Hermis person export API', '1'
EXEC core.SystemSetupAddUpdate 'PersonLanguageReferenceText', 'The text string displayed above the person language table on the person profile add update page', 'If you have a language skill, please use the <strong>Common European Framework of Reference for Languages and Self-Assessment Tool (PDF, 127KB, 4 pages)</strong> found <a href="https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/615703/Common_European_Framework_of_Reference_for_Languages_and_Self_Assessment_Tool.pdf" style="font-color:blue;" target="_blank">here</a> as a guide to indicate the level of proficiency you have attained in relation to each language. If you have listed your proficiency for a language as above "basic", you may be asked to complete a language skill assessment.'
GO
--End table core.SystemSetup

--Begin	table dropdown.Country
UPDATE C
SET C.CountryName = 'Ivory Coast'
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'CI'
GO

UPDATE C
SET C.CountryName = 'Curacao'
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'CW'
GO
--End	table dropdown.Country

--Begin table dropdown.Gender
TRUNCATE TABLE dropdown.Gender
GO

EXEC utility.InsertIdentityValue 'dropdown.Gender', 'GenderID', 0
GO

INSERT INTO dropdown.Gender 
	(GenderCode, GenderName, DisplayOrder) 
VALUES
	('M', 'Male', 1),
	('F', 'Female', 2),
	('NB', 'Non-Binary', 3),
	('ND', 'Prefer not to say', 4)
GO

UPDATE P
SET P.GenderID = ISNULL((SELECT G.GenderID FROM dropdown.Gender G WHERE G.GenderCode = P.GenderCode), 0)
FROM person.Person P
GO
--End table dropdown.Gender

--Begin table dropdown.LanguageProficiency
TRUNCATE TABLE dropdown.LanguageProficiency
GO

EXEC utility.InsertIdentityValue 'dropdown.LanguageProficiency', 'LanguageProficiencyID', 0
GO

INSERT INTO dropdown.LanguageProficiency 
	(LanguageProficiencyCode, LanguageProficiencyName, DisplayOrder) 
VALUES
	('A1', 'Basic User A1', 1),
	('A2', 'Basic User A2', 2),
	('B1', 'Independent User B1', 3),
	('B2', 'Independent User B2', 4),
	('C1', 'Proficient User C1', 5),
	('C2', 'Proficient User C2', 6)
GO
--End table dropdown.LanguageProficiency

--Begin table dropdown.PassportType
TRUNCATE TABLE dropdown.PassportType
GO

EXEC utility.InsertIdentityValue 'dropdown.PassportType', 'PassportTypeID', 0
GO

INSERT INTO dropdown.PassportType 
	(PassportTypeCode, PassportTypeName, DisplayOrder) 
VALUES
	('Tourist', 'Regular', 1),
	('Official', 'Official / Service', 2),
	('Diplomatic', 'Diplomatic', 3)
GO
--End table dropdown.PassportType

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

UPDATE PSQ
SET PSQ.PasswordSecurityQuestionName = 'In what city was your father born? (Enter full name of city only)'
FROM dropdown.PasswordSecurityQuestion PSQ
WHERE PSQ.PasswordSecurityQuestionName = 'In what city was your father born?  (Enter full name of city only)'
GO

UPDATE PSQ
SET PSQ.PasswordSecurityQuestionCode = 
	CASE PSQ.PasswordSecurityQuestionName
		WHEN 'In what city was your father born? (Enter full name of city only)'
		THEN 'CityFatherBorn'
		WHEN 'In what city was your high school? (Enter only "Charlotte" for Charlotte High School)'
		THEN 'CityHighSchool'
		WHEN 'In what city was your mother born? (Enter full name of city only)'
		THEN 'CityMotherBorn'
		WHEN 'In what city were you born? (Enter full name of city only)'
		THEN 'CityPersonBorn'
		WHEN 'In what city were you married?'
		THEN 'CityFatherMarried'
		WHEN 'What is the first name of your first child?'
		THEN 'FirstNameFirstChild'
		WHEN 'What is the name of your first niece/nephew?'
		THEN 'FirstNameNieceNephew'
		WHEN 'What is your father''s middle name?'
		THEN 'MiddleNameFather'
		WHEN 'What is your favourite colour?'
		THEN 'FavoriteColor'
		WHEN 'What is your maternal grandfather''s first name?'
		THEN 'FirstNameMaternalGrandfather'
		WHEN 'What is your maternal grandmother''s first name?'
		THEN 'FirstNameMaternalGrandmother'
		WHEN 'What is your mother''s middle name?'
		THEN 'MiddleNameMother'
		WHEN 'What was the name of your first friend?'
		THEN 'FirstNameFirstFriend'
		WHEN 'What was the name of your first pet?'
		THEN 'FirstNameFirstPet'
		WHEN 'What was the name of your first school teacher?'
		THEN 'FirstNameFirstTeacher'
		WHEN 'What was the name or number of your first house?'
		THEN 'FirstHouseNameNumber'
		WHEN 'What was your first job'
		THEN 'FirstJobName'
		WHEN 'Where did you go to university'
		THEN 'UniversityName'
		ELSE NULL
	END
FROM dropdown.PasswordSecurityQuestion PSQ

GO
--End table dropdown.PasswordSecurityQuestion