-- File Name:	Build - 2.3 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.3 - 2018.02.18 15.43.32

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Tables:
--		dropdown.Gender
--		dropdown.LanguageProficiency
--		dropdown.PassportType
--		person.PersonPassport
--		person.PersonQualificationAcademic
--		person.PersonQualificationCertification
--		person.SavedSearch
--
-- Functions:
--		core.GetCountryCallingCodeByCountryCallingCodeID
--		person.GetPersonProjectDaysInvoiced
--		person.GetPersonProjectDaysLogged
--		person.GetPersonProjectDaysPaid
--
-- Procedures:
--		document.GetDocumentByDocumentGUID
--		dropdown.GetGenderData
--		dropdown.GetLanguageProficiencyData
--		dropdown.GetPassportTypeData
--		invoice.GetInvoiceTimeLogByInvoiceID
--		invoice.GetInvoiceTimeLogSummaryByInvoiceID
--		person.GetPersonByPersonID
--		person.GetPersonJSONDataByPersonID
--		person.GetPersonProjectByPersonProjectID
--		project.GetProjectByProjectID
--		reporting.GetInvoicePerDiemLogSummaryByInvoiceID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table document.Document

--Begin table dropdown.Gender
DECLARE @TableName VARCHAR(250) = 'dropdown.Gender'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Gender
	(
	GenderID INT IDENTITY(0,1) NOT NULL,
	GenderCode VARCHAR(50),
	GenderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'GenderID'
EXEC utility.SetIndexClustered @TableName, 'IX_Gender', 'DisplayOrder,GenderName'
GO
--End table dropdown.Gender

--Begin table dropdown.LanguageProficiency
DECLARE @TableName VARCHAR(250) = 'dropdown.LanguageProficiency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LanguageProficiency
	(
	LanguageProficiencyID INT IDENTITY(0,1) NOT NULL,
	LanguageProficiencyCode VARCHAR(50),
	LanguageProficiencyName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LanguageProficiencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_LanguageProficiency', 'DisplayOrder,LanguageProficiencyName'
GO
--End table dropdown.LanguageProficiency

--Begin table dropdown.PassportType
DECLARE @TableName VARCHAR(250) = 'dropdown.PassportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PassportType
	(
	PassportTypeID INT IDENTITY(0,1) NOT NULL,
	PassportTypeCode VARCHAR(50),
	PassportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PassportTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PassportType', 'DisplayOrder,PassportTypeName'
GO
--End table dropdown.PassportType

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

EXEC utility.AddColumn @TableName, 'PasswordSecurityQuestionCode', 'VARCHAR(50)'
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table person.Person
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.Person') AND SC.Name = 'CitizenshipISOCountryCode2')
	EXEC sp_RENAME 'person.Person.CitizenshipISOCountryCode2', 'Citizenship1ISOCountryCode2', 'COLUMN';
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'Citizenship1ISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'Citizenship2ISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'GenderID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'HomePhone', 'VARCHAR(15)'
EXEC utility.AddColumn @TableName, 'HomePhoneCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'MiddleName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthISOCountryCode2', 'CHAR(2)'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthMunicipality', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PreferredName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'SummaryBiography', 'VARCHAR(2000)'
EXEC utility.AddColumn @TableName, 'WorkPhone', 'VARCHAR(15)'
EXEC utility.AddColumn @TableName, 'WorkPhoneCountryCallingCodeID', 'INT', '0'

EXEC utility.DropColumn @TableName, 'PassportExpirationDate'
EXEC utility.DropColumn @TableName, 'PassportIssuer'
EXEC utility.DropColumn @TableName, 'PassportNumber'
EXEC utility.DropColumn @TableName, 'PassportType'
GO
--End table person.Person

--Begin table person.PersonLanguage
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'OralLevel')
	EXEC sp_RENAME 'person.PersonLanguage.OralLevel', 'SpeakingLanguageProficiencyID', 'COLUMN';
--ENDIF
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'ReadLevel')
	EXEC sp_RENAME 'person.PersonLanguage.ReadLevel', 'ReadingLanguageProficiencyID', 'COLUMN';
--ENDIF
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('person.PersonLanguage') AND SC.Name = 'WriteLevel')
	EXEC sp_RENAME 'person.PersonLanguage.WriteLevel', 'WritingLanguageProficiencyID', 'COLUMN';
--ENDIF
GO
--End table person.PersonLanguage

--Begin table person.PersonPassport
DECLARE @TableName VARCHAR(250) = 'person.PersonPassport'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonPassport
	(
	PersonPassportID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PassportExpirationDate DATE,
	PassportIssuer VARCHAR(50),
	PassportNumber VARCHAR(50),
	PassportTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PassportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPassportID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPassport', 'PersonID'
GO
--End table person.PersonPassport

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table person.PersonProject

--Begin table person.PersonQualification
DECLARE @TableName VARCHAR(250) = 'person.PersonQualification'

EXEC utility.DropObject @TableName
GO
--End table person.PersonQualification

--Begin table person.PersonQualificationAcademic
DECLARE @TableName VARCHAR(250) = 'person.PersonQualificationAcademic'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonQualificationAcademic
	(
	PersonQualificationAcademicID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Institution VARCHAR(50),
	SubjectArea VARCHAR(50),
	GraduationDate DATE,
	StartDate DATE,
	Degree VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonQualificationAcademicID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonQualificationAcademic', 'PersonID'
GO
--End table person.PersonQualificationAcademic

--Begin table person.PersonQualificationCertification
DECLARE @TableName VARCHAR(250) = 'person.PersonQualificationCertification'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonQualificationCertification
	(
	PersonQualificationCertificationID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Provider VARCHAR(50),
	Course VARCHAR(50),
	CompletionDate DATE,
	ExpirationDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonQualificationCertificationID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonQualificationCertification', 'PersonID'
GO
--End table person.PersonQualificationCertification

--Begin table person.SavedSearch
DECLARE @TableName VARCHAR(250) = 'person.SavedSearch'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.SavedSearch
	(
	SavedSearchID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EntityTypeCode VARCHAR(50),
	SavedSearchName VARCHAR(50),
	SavedSearchData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SavedSearchID'
EXEC utility.SetIndexClustered @TableName, 'IX_SavedSearch', 'PersonID,EntityTypeCode'
GO
--End table person.SavedSearch

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table project.Project

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO

--Begin function core.GetCountryCallingCodeByCountryCallingCodeID
EXEC utility.DropObject 'core.GetCountryCallingCodeByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.10
-- Description:	A function to get a data from the dropdown.CountryCallingCode table
-- ================================================================================

CREATE FUNCTION core.GetCountryCallingCodeByCountryCallingCodeID
(
@CountryCallingCodeID INT
)

RETURNS INT

AS
BEGIN

	RETURN ISNULL((SELECT CCC.CountryCallingCode FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCodeID = @CountryCallingCodeID), 0)

END
GO
--End function core.GetCountryCallingCodeByCountryCallingCodeID

--Begin function person.GetPersonProjectDaysBilled / person.GetPersonProjectDaysNotBilled
EXEC utility.DropObject 'person.GetPersonProjectDaysBilled'
EXEC utility.DropObject 'person.GetPersonProjectDaysNotBilled'
GO
--Begin function person.GetPersonProjectDaysBilled / person.GetPersonProjectDaysNotBilled

--Begin function person.GetPersonProjectDaysLogged
EXEC utility.DropObject 'person.GetPersonProjectDaysLogged'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days logged on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysLogged
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysLogged NUMERIC(18,2)

	SELECT @nDaysLogged = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID

	RETURN ISNULL(@nDaysLogged, 0)
	
END
GO
--End function person.GetPersonProjectDaysLogged

--Begin function person.GetPersonProjectDaysInvoiced
EXEC utility.DropObject 'person.GetPersonProjectDaysInvoiced'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Invoiced on a PersonProject record
-- ========================================================================================

CREATE FUNCTION person.GetPersonProjectDaysInvoiced
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysInvoiced NUMERIC(18,2)

	SELECT @nDaysInvoiced = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND I.InvoiceStatus <> 'Approved'

	RETURN ISNULL(@nDaysInvoiced, 0)
	
END
GO
--End function person.GetPersonProjectDaysInvoiced

--Begin function person.GetPersonProjectDaysPaid
EXEC utility.DropObject 'person.GetPersonProjectDaysPaid'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Paid on a PersonProject record
-- ====================================================================================

CREATE FUNCTION person.GetPersonProjectDaysPaid
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysPaid NUMERIC(18,2)

	SELECT @nDaysPaid = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND I.InvoiceStatus = 'Approved'

	RETURN ISNULL(@nDaysPaid, 0)
	
END
GO
--End function person.GetPersonProjectDaysPaid

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		REPLACE(REPLACE(D.DocumentName, '-', '_'), ' ', '_') AS DocumentName,
		D.DocumentSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin table dropdown.GetGenderData
EXEC Utility.DropObject 'dropdown.GetGenderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.15
-- Description:	A stored procedure to return data from the dropdown.Gender table
-- =============================================================================
CREATE PROCEDURE dropdown.GetGenderData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GenderID,
		T.GenderCode,
		T.GenderName
	FROM dropdown.Gender T
	WHERE (T.GenderID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GenderName, T.GenderID

END
GO
--End procedure dropdown.GetGenderData

--Begin procedure dropdown.GetLanguageProficiencyData
EXEC Utility.DropObject 'dropdown.GetLanguageProficiencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.16
-- Description:	A stored procedure to return data from the dropdown.LanguageProficiency table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetLanguageProficiencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LanguageProficiencyID,
		T.LanguageProficiencyCode,
		T.LanguageProficiencyName
	FROM dropdown.LanguageProficiency T
	WHERE (T.LanguageProficiencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LanguageProficiencyName, T.LanguageProficiencyID

END
GO
--End procedure dropdown.GetLanguageProficiencyData

--Begin table dropdown.GetPassportTypeData
EXEC Utility.DropObject 'dropdown.GetPassportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.15
-- Description:	A stored procedure to return data from the dropdown.PassportType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetPassportTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PassportTypeID,
		T.PassportTypeCode,
		T.PassportTypeName
	FROM dropdown.PassportType T
	WHERE (T.PassportTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PassportTypeName, T.PassportTypeID

END
GO
--End procedure dropdown.GetPassportTypeData

--Begin procedure invoice.GetInvoiceTimeLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLog
	SELECT 
		C.FinanceCode1 AS LaborCode,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		PP.FeeRate,
		PPT.HoursWorked,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		PP.ISOCurrencyCode,
		PPT.ExchangeRate,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
	ORDER BY PPT.DateWorked

END
GO
--End procedure invoice.GetInvoiceTimeLogByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH ITL AS 
		(
		SELECT 
			PL.ProjectLocationName,
			C.FinanceCode1 AS LaborCode,
			PP.FeeRate,
			PPT.HoursWorked,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS Amount,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS VAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	--InvoiceTimeLogSummary
	SELECT 
		ITL.ProjectLocationName,
		ITL.LaborCode,
		ITL.FeeRate,
		SUM(ITL.HoursWorked) AS HoursWorked,
		CAST(SUM(ITL.Amount) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(ITL.VAT) AS NUMERIC(18,2)) AS VAT
	FROM ITL
	GROUP BY ITL.ProjectLocationName, ITL.LaborCode, ITL.FeeRate
	ORDER BY ITL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDateTime(D.CreateDateTime) AS CreateDateTimeDateTimeFormatted,
		D.DocumentID,
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton

	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonJSONDataByPersonID
EXEC utility.DropObject 'person.GetPersonJSONDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.10
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonJSONDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cJSONData VARCHAR(MAX) = '{}'
	DECLARE @nIsForExport BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM client.ClientPerson CP 
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN person.Person P ON P.PersonID = CP.PersonID
				AND CP.PersonID = @PersonID 
				AND CP.ClientPersonRoleCode = 'Consultant'
				AND C.ClientCode = 'HSOT'
				AND P.HasAcceptedTerms = 1
		)
		BEGIN

		SET @nIsForExport = 1
		SET @cJSONData = '['

		SET @cJSONData += 
			(
			SELECT
				@PersonID AS DAPersonID,
				G.GenderCode,
				P.BirthDate,
				P.CellPhone,
				P.ChestSize,
				P.Citizenship1ISOCountryCode2,
				P.Citizenship2ISOCountryCode2,
				P.CollarSize,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.CountryCallingCodeID) AS CountryCallingCode,
				P.EmailAddress,
				P.FirstName,
				P.HeadSize,
				P.Height,
				P.HomePhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.HomePhoneCountryCallingCodeID) AS HomePhoneCountryCallingCode,
				P.IsPhoneVerified,
				P.IsUKEUNational,
				P.LastName,
				P.MailAddress1,
				P.MailAddress2,
				P.MailAddress3,
				P.MailISOCountryCode2,
				P.MailMunicipality,
				P.MailPostalCode,
				P.MailRegion,
				P.MiddleName,
				P.Password,
				P.PasswordExpirationDateTime,
				P.PasswordSalt,
				P.PlaceOfBirthISOCountryCode2,
				P.PlaceOfBirthMunicipality,
				P.PreferredName,
				P.Suffix,
				P.SummaryBiography,
				P.Title,
				P.UserName,
				P.WorkPhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.WorkPhoneCountryCallingCodeID) AS WorkPhoneCountryCallingCode
			FROM person.Person P
				JOIN dropdown.Gender G ON G.GenderID = P.GenderID
					AND P.PersonID = @PersonID
			FOR JSON PATH, ROOT('Person'), Include_Null_Values
			)

		SET @cJSONData += ','

		SET @cJSONData += 
			(
			SELECT
				@PersonID AS DAPersonID,
				PL.ISOLanguageCode2,
				LP1.LanguageProficiencyCode AS SpeakingLanguageProficiencyCode,
				LP2.LanguageProficiencyCode AS ReadingLanguageProficiencyCode,
				LP3.LanguageProficiencyCode AS WritingLanguageProficiencyCode
			FROM person.PersonLanguage PL
				JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
				JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
					AND PL.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonLanguage'), Include_Null_Values
			)

		SET @cJSONData += ','

		SET @cJSONData += 
			(
			SELECT 
				@PersonID AS DAPersonID,
				PP.PassportExpirationDate,
				PP.PassportIssuer,
				PP.PassportNumber,
				PT.PassportTypeCode
			FROM person.PersonPassport PP
				JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
					AND PP.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPassport'), Include_Null_Values
			)

		SET @cJSONData += ','

		SET @cJSONData += 
			(
			SELECT 
				@PersonID AS DAPersonID,
				PPS.PasswordSecurityQuestionAnswer,
				PSQ.PasswordSecurityQuestionCode
			FROM person.PersonPasswordSecurity PPS
				JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
					AND PPS.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPasswordSecurity'), Include_Null_Values
			)

		SET @cJSONData += ','

		SET @cJSONData += 
			(
			SELECT 
				@PersonID AS DAPersonID,
				PQA.Degree,
				PQA.GraduationDate,
				PQA.Institution,
				PQA.StartDate,
				PQA.SubjectArea
			FROM person.PersonQualificationAcademic PQA
			WHERE PQA.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationAcademic'), Include_Null_Values
			)

		SET @cJSONData += ','

		SET @cJSONData += 
			(
			SELECT
				@PersonID AS DAPersonID,
				PQC.CompletionDate,
				PQC.Course,
				PQC.ExpirationDate,
				PQC.Provider
			FROM person.PersonQualificationCertification PQC
			WHERE PQC.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationCertification'), Include_Null_Values
			)

		SET @cJSONData += ']'

		END
	--ENDIF

	SELECT 
		@cJSONData AS JSONData,
		@nIsForExport AS IsForExport

	SELECT
		SS.SystemSetupKey,
		SS.SystemSetupValue
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupKey IN ('HermisAPIKey', 'HermisAPISecretKey', 'HermisAPIURL', 'IsHermisIntegrationEnabled')

END
GO
--End procedure person.GetPersonJSONDataByPersonID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			NULL AS AcceptedDateFormatted,
			NULL AS AlternateManagerEmailAddress,
			NULL AS AlternateManagerName,
			NULL AS CurrencyName,
			1 AS IsActive,
			0 AS IsAmendment,
			NULL AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			NULL AS ClientFunctionName,
			0 AS InsuranceTypeID,
			NULL AS InsuranceTypeName,
			NULL AS EndDate,
			NULL AS EndDateFormatted,
			0 AS FeeRate,
			NULL AS ManagerEmailAddress,
			NULL AS ManagerName,
			NULL AS Notes,
			NULL AS ProjectCode,
			NULL AS ProjectLaborCode,
			0 AS ProjectLaborCodeID,
			NULL AS ProjectLaborCodeName,
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectRoleID,
			NULL AS ProjectRoleName,
			NULL AS ProjectTermOfReferenceCode,
			0 AS ProjectTermOfReferenceID,
			NULL AS ProjectTermOfReferenceName,
			NULL AS Status,
			NULL AS StartDate,
			NULL AS StartDateFormatted,
			NULL AS UserName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			PJ.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = PJ.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			PJ.HoursPerDay,
			PJ.ProjectCode,
			PJ.ProjectID,
			PJ.ProjectName,
			person.FormatPersonNameByPersonID(PN.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PN.UserName,
			PLC.ProjectLaborCode,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			CASE WHEN PP.AcceptedDate IS NOT NULL THEN 1 ELSE 0 END AS IsAmendment,
			PP.AlternateManagerEmailAddress,
			PP.AlternateManagerName,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.IsActive,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.Notes,
			PP.PersonID,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceCode,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = PJ.ClientID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.IsOvertimeAllowed,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.Notes,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Inderjeet Kaur
-- Create date:	2017.12.07
-- Description:	A stored procedure to return invoice per diem data
-- ===============================================================
CREATE PROCEDURE reporting.GetInvoicePerDiemLogSummaryByInvoiceID 

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH IPL AS 
		(
		SELECT
			C.ClientCode,
			C.ClientCode + PN.UserName AS VendorID,
			P.ProjectCode AS ProjectChargeCode,
			C.FinanceCode2 AS DPACode,
			CAST((CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END) * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDPAVAT,
			C.FinanceCode3 AS DSACode,
			PPT.DSAAmount,
			CAST(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDSAVAT
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
	    JOIN person.Person PN ON PN.PersonID = I.PersonID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	SELECT
		D.ClientCode,
		D.VendorID,
		D.ProjectChargeCode,
		D.PerdiemType,
		D.Code,
		CAST(SUM(D.Amount) AS NUMERIC(18,2)) AS Amount ,
		CAST(SUM(D.VAT) AS NUMERIC(18,2)) AS VAT
	FROM
		(
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DPA' AS PerdiemType,
			IPL.DPACode AS Code,
			IPL.InvoiceDPAAmount AS Amount,
			IPL.InvoiceDPAVAT AS VAT
		FROM IPL

		UNION ALL
	
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DSA' AS PerdiemType,
			IPL.DSACode AS Code,
			IPL.InvoiceDSAAmount AS Amount,
			IPL.InvoiceDSAVAT AS VAT
		FROM IPL
		) D 
	GROUP BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode, D.Code
	ORDER BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode

END
GO
--End procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO

--Begin table core.SystemSetup
DECLARE @cGUUID VARCHAR(100) = (SELECT CAST(NEWID() AS VARCHAR(100)))
EXEC core.SystemSetupAddUpdate 'HermisAPIKey', NULL, @cGUUID

SELECT @cGUUID = CAST(NEWID() AS VARCHAR(100))
EXEC core.SystemSetupAddUpdate 'HermisAPISecretKey', NULL, @cGUUID

IF (SELECT core.GetSystemSetupValueBySystemSetupKey('Environment', '')) = 'Dev'
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'DevURL'
ELSE IF	(SELECT core.GetSystemSetupValueBySystemSetupKey('Environment', '')) = 'UAT'
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'UATURL'
ELSE
	EXEC core.SystemSetupAddUpdate 'HermisAPIURL', NULL, 'ProdURL'
--ENDIF

EXEC core.SystemSetupAddUpdate 'IsHermisIntegrationEnabled', 'Indicates if the system should call the Hermis person export API', '1'
EXEC core.SystemSetupAddUpdate 'PersonLanguageReferenceText', 'The text string displayed above the person language table on the person profile add update page', 'If you have a language skill, please use the <strong>Common European Framework of Reference for Languages and Self-Assessment Tool (PDF, 127KB, 4 pages)</strong> found <a href="https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/615703/Common_European_Framework_of_Reference_for_Languages_and_Self_Assessment_Tool.pdf" style="font-color:blue;" target="_blank">here</a> as a guide to indicate the level of proficiency you have attained in relation to each language. If you have listed your proficiency for a language as above "basic", you may be asked to complete a language skill assessment.'
GO
--End table core.SystemSetup

--Begin	table dropdown.Country
UPDATE C
SET C.CountryName = 'Ivory Coast'
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'CI'
GO

UPDATE C
SET C.CountryName = 'Curacao'
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'CW'
GO
--End	table dropdown.Country

--Begin table dropdown.Gender
TRUNCATE TABLE dropdown.Gender
GO

EXEC utility.InsertIdentityValue 'dropdown.Gender', 'GenderID', 0
GO

INSERT INTO dropdown.Gender 
	(GenderCode, GenderName, DisplayOrder) 
VALUES
	('M', 'Male', 1),
	('F', 'Female', 2),
	('NB', 'Non-Binary', 3),
	('ND', 'Prefer not to say', 4)
GO

UPDATE P
SET P.GenderID = ISNULL((SELECT G.GenderID FROM dropdown.Gender G WHERE G.GenderCode = P.GenderCode), 0)
FROM person.Person P
GO
--End table dropdown.Gender

--Begin table dropdown.LanguageProficiency
TRUNCATE TABLE dropdown.LanguageProficiency
GO

EXEC utility.InsertIdentityValue 'dropdown.LanguageProficiency', 'LanguageProficiencyID', 0
GO

INSERT INTO dropdown.LanguageProficiency 
	(LanguageProficiencyCode, LanguageProficiencyName, DisplayOrder) 
VALUES
	('A1', 'Basic User A1', 1),
	('A2', 'Basic User A2', 2),
	('B1', 'Independent User B1', 3),
	('B2', 'Independent User B2', 4),
	('C1', 'Proficient User C1', 5),
	('C2', 'Proficient User C2', 6)
GO
--End table dropdown.LanguageProficiency

--Begin table dropdown.PassportType
TRUNCATE TABLE dropdown.PassportType
GO

EXEC utility.InsertIdentityValue 'dropdown.PassportType', 'PassportTypeID', 0
GO

INSERT INTO dropdown.PassportType 
	(PassportTypeCode, PassportTypeName, DisplayOrder) 
VALUES
	('Tourist', 'Regular', 1),
	('Official', 'Official / Service', 2),
	('Diplomatic', 'Diplomatic', 3)
GO
--End table dropdown.PassportType

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

UPDATE PSQ
SET PSQ.PasswordSecurityQuestionName = 'In what city was your father born? (Enter full name of city only)'
FROM dropdown.PasswordSecurityQuestion PSQ
WHERE PSQ.PasswordSecurityQuestionName = 'In what city was your father born?  (Enter full name of city only)'
GO

UPDATE PSQ
SET PSQ.PasswordSecurityQuestionCode = 
	CASE PSQ.PasswordSecurityQuestionName
		WHEN 'In what city was your father born? (Enter full name of city only)'
		THEN 'CityFatherBorn'
		WHEN 'In what city was your high school? (Enter only "Charlotte" for Charlotte High School)'
		THEN 'CityHighSchool'
		WHEN 'In what city was your mother born? (Enter full name of city only)'
		THEN 'CityMotherBorn'
		WHEN 'In what city were you born? (Enter full name of city only)'
		THEN 'CityPersonBorn'
		WHEN 'In what city were you married?'
		THEN 'CityFatherMarried'
		WHEN 'What is the first name of your first child?'
		THEN 'FirstNameFirstChild'
		WHEN 'What is the name of your first niece/nephew?'
		THEN 'FirstNameNieceNephew'
		WHEN 'What is your father''s middle name?'
		THEN 'MiddleNameFather'
		WHEN 'What is your favourite colour?'
		THEN 'FavoriteColor'
		WHEN 'What is your maternal grandfather''s first name?'
		THEN 'FirstNameMaternalGrandfather'
		WHEN 'What is your maternal grandmother''s first name?'
		THEN 'FirstNameMaternalGrandmother'
		WHEN 'What is your mother''s middle name?'
		THEN 'MiddleNameMother'
		WHEN 'What was the name of your first friend?'
		THEN 'FirstNameFirstFriend'
		WHEN 'What was the name of your first pet?'
		THEN 'FirstNameFirstPet'
		WHEN 'What was the name of your first school teacher?'
		THEN 'FirstNameFirstTeacher'
		WHEN 'What was the name or number of your first house?'
		THEN 'FirstHouseNameNumber'
		WHEN 'What was your first job'
		THEN 'FirstJobName'
		WHEN 'Where did you go to university'
		THEN 'UniversityName'
		ELSE NULL
	END
FROM dropdown.PasswordSecurityQuestion PSQ

GO
--End table dropdown.PasswordSecurityQuestion
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit the contents of the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Next of Kin data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Proof of Life data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export person data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Layout', @DESCRIPTION='Allow access to the Feedback icon in the site header', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Layout.Default.CanHaveFeedback', @PERMISSIONCODE='CanHaveFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Allow change access to selected fields on a deployment record AFTER the deployment has been accpted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='See the list of invoices on a project view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View.InvoiceList', @PERMISSIONCODE='InvoiceList';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Export invoice data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Allow the user to regenerate an invoice .pdf and add it to the document library for an existing invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List.RegenerateInvoice', @PERMISSIONCODE='RegenerateInvoice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.3 - 2018.02.18 15.43.32')
GO
--End build tracking

