USE DeployAdviser
GO

--Begin function core.GetCountryCallingCodeByCountryCallingCodeID
EXEC utility.DropObject 'core.GetCountryCallingCodeByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.10
-- Description:	A function to get a data from the dropdown.CountryCallingCode table
-- ================================================================================

CREATE FUNCTION core.GetCountryCallingCodeByCountryCallingCodeID
(
@CountryCallingCodeID INT
)

RETURNS INT

AS
BEGIN

	RETURN ISNULL((SELECT CCC.CountryCallingCode FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCodeID = @CountryCallingCodeID), 0)

END
GO
--End function core.GetCountryCallingCodeByCountryCallingCodeID

--Begin function person.GetPersonProjectDaysBilled / person.GetPersonProjectDaysNotBilled
EXEC utility.DropObject 'person.GetPersonProjectDaysBilled'
EXEC utility.DropObject 'person.GetPersonProjectDaysNotBilled'
GO
--Begin function person.GetPersonProjectDaysBilled / person.GetPersonProjectDaysNotBilled

--Begin function person.GetPersonProjectDaysLogged
EXEC utility.DropObject 'person.GetPersonProjectDaysLogged'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days logged on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysLogged
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysLogged NUMERIC(18,2)

	SELECT @nDaysLogged = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID

	RETURN ISNULL(@nDaysLogged, 0)
	
END
GO
--End function person.GetPersonProjectDaysLogged

--Begin function person.GetPersonProjectDaysInvoiced
EXEC utility.DropObject 'person.GetPersonProjectDaysInvoiced'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Invoiced on a PersonProject record
-- ========================================================================================

CREATE FUNCTION person.GetPersonProjectDaysInvoiced
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysInvoiced NUMERIC(18,2)

	SELECT @nDaysInvoiced = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND I.InvoiceStatus <> 'Approved'

	RETURN ISNULL(@nDaysInvoiced, 0)
	
END
GO
--End function person.GetPersonProjectDaysInvoiced

--Begin function person.GetPersonProjectDaysPaid
EXEC utility.DropObject 'person.GetPersonProjectDaysPaid'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Paid on a PersonProject record
-- ====================================================================================

CREATE FUNCTION person.GetPersonProjectDaysPaid
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysPaid NUMERIC(18,2)

	SELECT @nDaysPaid = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND I.InvoiceStatus = 'Approved'

	RETURN ISNULL(@nDaysPaid, 0)
	
END
GO
--End function person.GetPersonProjectDaysPaid
