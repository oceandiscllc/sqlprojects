-- File Name:	Build - 2.12 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.12 - 2019.03.04 18.03.26

--USE DeployAdviserCloud
GO

-- ==============================================================================================================================
-- Tables:
--		core.MenuItemClient
--
-- Procedures:
--		core.GetMenuItemsByPersonID
--		invoice.GetInvoicePreviewData
--		person.GetFeedbackByFeedbackID
--		person.GetFeedbackMetadata
--		person.GetPendingActionsByPersonID
--		person.GetPersonByPersonID
--		person.GetPersonDashboardDataByPersonID
--		person.GetPersonProjectLeave
--		person.ValidateLogin
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviserCloud
GO

--Begin synonym person.HermisCloud_person_Feedback
DECLARE @TableName VARCHAR(250) = 'person.HermisCloud_person_Feedback'

EXEC utility.DropObject @TableName
GO
--End synonym person.HermisCloud_person_Feedback

--Begin table core.MenuItemClient
DECLARE @TableName VARCHAR(250) = 'core.MenuItemClient'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemClient
	(
	MenuItemClientID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	MenuItemID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemClientID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemClient', 'ClientID,MenuItemID'
GO
--End table core.MenuItemClient

--Begin table invoice.Invoice
ALTER TABLE invoice.Invoice ALTER COLUMN Notes NVARCHAR(MAX)
GO
--End table invoice.Invoice

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviserCloud
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviserCloud
GO

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- ===============================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'

						UNION

						SELECT 1
						FROM permissionable.Permissionable P
							JOIN core.MenuItemPermissionableLineage MIPL ON MIPL.PermissionableLineage = P.PermissionableLineage
								AND MIPL.MenuItemID = MI.MenuItemID
								AND P.IsGlobal = 1
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)
			AND
				(
				NOT EXISTS (SELECT 1 FROM core.MenuItemClient MIC WHERE MIC.MenuItemID = MI.MenuItemID)
					OR EXISTS (SELECT 1 FROM core.MenuItemClient MIC JOIN client.ClientPerson CP ON CP.ClientID = MIC.ClientID AND MIC.MenuItemID = MI.MenuItemID AND CP.PersonID = @PersonID)
				)
		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
				AND
					(
					NOT EXISTS (SELECT 1 FROM core.MenuItemClient MIC WHERE MIC.MenuItemID = MI.MenuItemID)
						OR EXISTS (SELECT 1 FROM core.MenuItemClient MIC JOIN client.ClientPerson CP ON CP.ClientID = MIC.ClientID AND MIC.MenuItemID = MI.MenuItemID AND CP.PersonID = @PersonID)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		MI.IsForNewTab,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX) = NULL,
@PersonProjectTimeIDExclusionList VARCHAR(MAX) = NULL,
@InvoiceISOCurrencyCode CHAR(3),
@PayoutLeaveBalance BIT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @cFixedAllowanceLabel VARCHAR(250)
	DECLARE @cVariableAllowanceLabel VARCHAR(250)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(350),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		TimeTypeName VARCHAR(50),
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tPersonProjectLeave TABLE 
		(
		DisplayOrder INT,
		PersonProjectID INT,
		PersonProjectName VARCHAR(250), 
		TimeTypeID INT,
		TimeTypeName VARCHAR(50), 
		IsBalancePayableUponTermination BIT, 
		IsBalancePayableUponTerminationFormatted VARCHAR(5), 
		PersonProjectLeaveHours NUMERIC(18,2), 
		TimeTaken NUMERIC(18,2), 
		TimeRemaining NUMERIC(18,2)
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		TimeTypeName VARCHAR(50),
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		AccountCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	IF @PayoutLeaveBalance = 1
		BEGIN
	
		--PersonProjectLeave
		INSERT INTO @tPersonProjectLeave
			(DisplayOrder, IsBalancePayableUponTermination, IsBalancePayableUponTerminationFormatted, PersonProjectID, PersonProjectName, TimeTaken, PersonProjectLeaveHours, TimeRemaining, TimeTypeID, TimeTypeName)
		EXEC person.GetPersonProjectLeave 'Project', @ProjectID, @PersonID

		INSERT INTO person.PersonProjectTime
			(PersonProjectID, DateWorked, HoursWorked, OwnNotes, ProjectManagerNotes, TimeTypeID, IsForLeaveBalance)
		SELECT
			T.PersonProjectID,
			getDate(),
			T.TimeRemaining,
			LEFT(T.TimeTypeName + ' Balance Payout For ' + T.PersonProjectName, 250),
			LEFT(T.TimeTypeName + ' Balance Payout For ' + T.PersonProjectName, 250),
			T.TimeTypeID,
			1
		FROM @tPersonProjectLeave T
		WHERE IsBalancePayableUponTermination = 1
		ORDER BY T.PersonProjectName, T.PersonProjectID, T.TimeTypeName, T.TimeTypeID

		END
	--ENDIF

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate OR PPT.IsForLeaveBalance = 1)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate OR PPT.IsForLeaveBalance = 1)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3,
		@cFixedAllowanceLabel = ISNULL(C.FixedAllowanceLabel, 'DPA'),
		@cVariableAllowanceLabel = ISNULL(C.VariableAllowanceLabel, 'DSA')
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription AS ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, TimeTypeName, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate, ApplyVAT)
	SELECT 
		PPT.DateWorked,
		TT.TimeTypeName,
		PL.ProjectLocationName,
		PL.DPAAmount * PPT.HasDPA AS DPAAmount,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.ApplyVAT
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, TimeTypeName, ProjectLocationName, HoursPerDay, HoursWorked, ApplyVAT, FeeRate, ISOCurrencyCode, ExchangeRate, AccountCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		TT.TimeTypeName,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PPT.ApplyVAT,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),

		CASE
			WHEN TT.TimeTypeCode = 'PROD'
			THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
			ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
		END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), ''),

		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, TT.TimeTypeName, PL.ProjectLocationName, PPT.PersonProjectTimeID

	IF @EndDate IS NULL
		BEGIN

		SELECT TOP 1 @EndDate = D.EndDate
		FROM
			(
			SELECT PPE.ExpenseDate AS EndDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS EndDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.EndDate DESC

		END
	--ENDIF

	IF @StartDate IS NULL
		BEGIN

		SELECT TOP 1 @StartDate = D.StartDate
		FROM
			(
			SELECT PPE.ExpenseDate AS StartDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS StartDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.StartDate

		END
	--ENDIF

	--InvoiceData
	SELECT
		NULL AS InvoiceStatus,

		CASE
			WHEN PN.BillAddress1 IS NULL 
				OR PN.BillISOCountryCode2 IS NULL 
				OR PN.BillMunicipality IS NULL 
				OR PN.ContractingTypeID = 0 
				OR 
					(
					(SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company' 
						AND PN.SendInvoicesFrom IS NULL 
						AND PN.OwnCompanyName IS NULL
					)
			THEN 1
			ELSE 0
		END AS IsAddressUpdateRequired,

		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PINT.PersonInvoiceNumberTypeCode,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.CellPhone,
		PN.ContractingTypeID,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		PN.OwnCompanyName,
		PN.SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		PN.PersonInvoiceNumberIncrement,
		PN.PersonInvoiceNumberPrefix,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID,
		PN.TaxRate
	FROM project.Project PJ
	CROSS JOIN person.Person PN
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND PJ.ProjectID = @ProjectID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = PN.PersonInvoiceNumberTypeID
			AND PN.PersonID = @PersonID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDateWithDay(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoiceExpenseLogSummary
	;
	WITH IEL AS 
		(
		SELECT
			TEL.ClientCostCodeName,
			SUM(TEL.ExchangeRate * TEL.ExpenseAmount) AS ExpenseAmount,
			SUM(TEL.ExchangeRate * TEL.TaxAmount) AS TaxAmount,
			COUNT(DISTINCT TEL.ExpenseDate) AS DaysCount
		FROM @tExpenseLog TEL
		GROUP BY TEL.ClientCostCodeName, TEL.ExpenseDate
		)

	SELECT
		IEL.ClientCostCodeName,
		CAST(SUM(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS ExpenseAmount,
		CAST(SUM(IEL.TaxAmount) AS NUMERIC(18,2)) AS TaxAmount,
		SUM(IEL.DaysCount) AS DaysCount,
		CAST(MIN(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MinExpenseAmount,
		CAST(MAX(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MaxExpenseAmount,
		CAST(SUM(IEL.ExpenseAmount) / SUM(IEL.DaysCount) AS NUMERIC(18,2)) AS AvgExpenseAmount
	FROM IEL
	GROUP BY IEL.ClientCostCodeName, IEL.DaysCount
	ORDER BY 1

	--InvoicePerDiemLog
	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		TPL.DPAAmount AS Amount,
		TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DPAExchangeRate AS ExchangeRate,
		TPL.DPAAmount * TPL.DPAExchangeRate AS InvoiceAmount,
		TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DPAAmount * TPL.DPAExchangeRate) + (TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	UNION

	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		TPL.DSAAmount AS Amount,
		TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DSAExchangeRate AS ExchangeRate,
		TPL.DSAAmount * TPL.DSAExchangeRate AS InvoiceAmount,
		TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DSAAmount * TPL.DSAExchangeRate) + (TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	ORDER BY 1, 3, 2, 8

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DPAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DPAAmount) AS Amount,
		SUM(TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) + SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.TimeTypeName, TPL.ProjectLocationName, TPL.DPAISOCurrencyCode

	UNION

	SELECT 
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DSAAmount) AS Amount,
		SUM(TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) + SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.TimeTypeName, TPL.ProjectLocationName, TPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 3

	--InvoiceTimeLog
	SELECT 
		TTL.AccountCode,
		TTL.TimeLogID,
		core.FormatDateWithDay(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.TimeTypeName,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.TimeTypeName,
		TTL.AccountCode,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.TimeTypeName, TTL.ProjectLocationName, TTL.AccountCode, TTL.FeeRate
	ORDER BY TTL.TimeTypeName, TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS InvoiceAmount,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			@cFixedAllowanceLabel AS DisplayText,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			@cVariableAllowanceLabel AS DisplayText,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(TEL.ExpenseAmount * TEL.ExchangeRate) AS InvoiceAmount,
			SUM(TEL.TaxAmount * TEL.ExchangeRate) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		CAST(INS.InvoiceAmount AS NUMERIC(18,2)) AS InvoiceAmount,
		CAST(INS.InvoiceVAT AS NUMERIC(18,2)) AS InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		CAST(SUM(INS.InvoiceAmount) AS NUMERIC(18,2)) AS InvoiceAmount,
		CAST(SUM(INS.InvoiceVAT) AS NUMERIC(18,2)) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.IsDefault
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure person.GetFeedbackByFeedbackID
EXEC Utility.DropObject 'person.GetFeedbackByFeedbackID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the HermisCloud.person.Feedback table
-- =========================================================================================
CREATE PROCEDURE person.GetFeedbackByFeedbackID

@FeedbackID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Feedback
	SELECT
		CASE WHEN DATEDIFF(d, F.CreateDate, getDate()) <= 30 AND F.CSGMemberComment IS NULL THEN 1 ELSE 0 END AS CanHaveEdit,
		HermisCloud.person.FormatPersonNameByPersonID(F.CreatePersonID, 'LastFirst') AS CreatePersonNameFormated,
		F.CSGMemberComment,
		core.FormatDate(F.EndDate) AS EndDateFormatted,
		F.FeedbackID,
		F.FeedbackName,
		F.Question7,
		core.FormatDate(F.StartDate) AS StartDateFormatted,
		MPR.CSGMemberPerformanceRatingID
	FROM HermisCloud.person.Feedback F
		JOIN HermisCloud.dropdown.CSGMemberPerformanceRating MPR ON MPR.CSGMemberPerformanceRatingID = F.CSGMemberPerformanceRatingID
			AND F.FeedbackID = @FeedbackID

END
GO
--End procedure person.GetFeedbackByFeedbackID

--Begin procedure person.GetFeedbackMetadata
EXEC Utility.DropObject 'person.GetFeedbackMetadata'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.27
-- Description:	A stored procedure to return data from the HermisCloud.dropdown.CSGMemberPerformanceRating and dropdown.FeedbackQuestion tables
-- ============================================================================================================================================
CREATE PROCEDURE person.GetFeedbackMetadata

AS
BEGIN
	SET NOCOUNT ON;

	--CSGMemberPerformanceRating
	SELECT
		MPR.CSGMemberPerformanceRatingCode,
		MPR.CSGMemberPerformanceRatingID,
		MPR.CSGMemberPerformanceRatingName
	FROM HermisCloud.dropdown.CSGMemberPerformanceRating MPR
	WHERE MPR.IsActive = 1
		AND MPR.CSGMemberPerformanceRatingID > 0
	ORDER BY MPR.DisplayOrder, MPR.CSGMemberPerformanceRatingID, MPR.CSGMemberPerformanceRatingName

	--FeedbackQuestion
	SELECT
		FQ.FeedbackQuestionCode,
		FQ.FeedbackQuestionID,
		FQ.FeedbackQuestionName
	FROM HermisCloud.dropdown.FeedbackQuestion FQ
	WHERE FQ.IsInternal = 1
		AND FQ.IsActive = 1
	ORDER BY FQ.DisplayOrder, FQ.FeedbackQuestionName

END
GO
--End procedure person.GetFeedbackMetadata

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (PendingActionID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>',
		'<button class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonFeedback
	IF EXISTS 
		(
		SELECT 1 
		FROM HermisCloud.person.Feedback F 
			JOIN HermisCloud.person.Person P ON P.PersonID = F.CSGMemberPersonID
				AND P.DAPersonID = @PersonID
				AND F.CSGMemberComment IS NULL
				AND 
					(
					(F.IsExternal = 1 AND HermisCloud.workflow.IsWorkflowComplete('ExternalFeedback', F.FeedbackID) = 1)
						OR HermisCloud.workflow.IsWorkflowComplete('InternalFeedback', F.FeedbackID) = 1
					)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Review CSG Member Feedback',
			'<a class="btn btn-sm btn-info" href="/feedback/list">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two emergency contact records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	INSERT INTO @tTable 
		(PendingAction, Link1) 
	SELECT
		'Accept deployment ' + PP.PersonProjectName + ' on project ' + P.ProjectName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptPersonProject'', ' + CAST(PP.PersonProjectID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>'
	FROM person.PersonProject PP 
		JOIN project.Project P ON P.ProjectID = PP.ProjectID 
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NULL
			AND PP.EndDate >= getDate()

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT
		T.PendingActionID,
		T.PendingAction, 
		REPLACE(T.Link1, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link1,
		REPLACE(T.Link2, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.NationalInsuranceNumber,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		CASE WHEN NOT EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS AcceptClientPerson,
		CASE WHEN EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = P.PersonID AND PP.AcceptedDate IS NULL) THEN 1 ELSE 0 END AS AcceptPersonProject,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') THEN 1 ELSE 0 END AS IsConsultant,
		CASE WHEN EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = P.PersonID) THEN 1 ELSE 0 END AS HasDeployment,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProfileClientConfiguration
	SELECT DISTINCT
		CC.ClientConfigurationKey
	FROM client.ClientConfiguration CC
		JOIN client.ClientPerson CP ON CP.ClientID = CC.ClientID
			AND CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = 'Consultant'
			AND CC.ClientConfigurationCategoryCode = 'PersonProfile'
			AND CAST(CC.ClientConfigurationValue AS BIT) = 1

	--PersonProjectLeave
	EXEC person.GetPersonProjectLeave 'Person', @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonDashboardDataByPersonID
EXEC utility.DropObject 'person.GetPersonDashboardDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return dashboard data based on a PersonID
-- ============================================================================
CREATE PROCEDURE person.GetPersonDashboardDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @nDaysLogged NUMERIC(18,2)
	DECLARE @nPersonProjectDays NUMERIC(18,2)

	SELECT @nPersonProjectDays = SUM(PPL.Days)
	FROM person.PersonProjectLocation PPL
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPL.PersonProjectID
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NOT NULL 
			AND PP.EndDate >= getDate()

	SELECT @nDaysLogged = ROUND(SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2))), 1)
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NOT NULL 
			AND PP.EndDate >= getDate()

	;
	WITH ID AS
		(
		SELECT
			COUNT(D.InvoiceStatus) AS ItemCount,
			D.InvoiceStatus
		FROM
			(
			SELECT
				CASE
					WHEN I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected')
					THEN I.InvoiceStatus
					ELSE 'Client Review'
				END AS InvoiceStatus
			FROM invoice.Invoice I
			WHERE I.PersonID = @PersonID
				AND DATEDIFF(d, I.InvoiceDateTime, getDate()) < 90
			) D
		GROUP BY D.InvoiceStatus
		)

	SELECT
		CAST(@nPersonProjectDays - @nDaysLogged AS NUMERIC(18,1)) AS DaysAvailable,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Approved'), 0) AS ApprovedInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Client Review'), 0) AS ClientReviewInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Line Manager Review'), 0) AS LineManagerReviewInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Rejected'), 0) AS RejectedInvoiceCount,
		ISNULL((SELECT COUNT(I.InvoiceID) FROM invoice.Invoice I WHERE I.PersonID = @PersonID AND DATEDIFF(d, I.InvoiceDateTime, getDate()) < 90), 0) AS InvoiceCount

END
GO
--End procedure person.GetPersonDashboardDataByPersonID

--Begin procedure person.GetPersonProjectLeave
EXEC utility.DropObject 'person.GetPersonProjectLeave'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.08
-- Description:	A stored procedure to return leave data based on an EntityTypeCode and an EntityID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonProjectLeave

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT = 0,
@IncludeProductiveTime BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--WARNING!!! invoice.GetInvoicePreviewData relies on this specific schema and column order.  Changes here must be reflected there.
	SELECT
		1 AS DisplayOrder,
		CTT.IsBalancePayableUponTermination,
		core.YesNoFormat(CTT.IsBalancePayableUponTermination) AS IsBalancePayableUponTerminationFormatted,
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PPT1.HoursWorked, 0) AS TimeTaken,
		PPTT.PersonProjectLeaveHours,
		PPTT.PersonProjectLeaveHours - ISNULL(PPT2.HoursWorked, 0) AS TimeRemaining,
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM person.PersonProjectTimeType PPTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = PPTT.ClientTimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPTT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
			AND 
				(
					(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID)
						OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
						OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
						OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
				)
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PPT.InvoiceID = @EntityID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
					AND PPT.TimeTypeID = TT.TimeTypeID
					AND PPTT.PersonProjectID = PPT.PersonProjectID
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PPT1
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
					AND PPT.TimeTypeID = TT.TimeTypeID
					AND PPTT.PersonProjectID = PPT.PersonProjectID
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PPT2

	UNION

	SELECT
		0 AS DisplayOrder,
		0,
		'',
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PTT3.HoursWorked, 0) AS TimeTaken,
		CAST(ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PTT4.PersonProjectID), 0) * P.HoursPerDay AS NUMERIC(18,2)),
		CAST((ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PTT5.PersonProjectID), 0) * P.HoursPerDay) - ISNULL(PTT5.HoursWorked, 0) AS NUMERIC(18,2)),
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM
		(
		SELECT
			PPT.PersonProjectID,
			PPT.TimeTypeID,
			SUM(PPT.HoursWorked) AS HoursWorked
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
				AND TT.TimeTypeCode = 'PROD'
				AND @IncludeProductiveTime = 1
				AND 
					(
						(@EntityTypeCode = 'Invoice' AND PPT.InvoiceID = @EntityID)
							OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
							OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
							OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
					)
		GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
		) PTT3
		JOIN person.PersonProject PP ON PP.PersonProjectID = PTT3.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PTT3.TimeTypeID
			AND @IncludeProductiveTime = 1
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
					AND TT.TimeTypeCode = 'PROD'
					AND @IncludeProductiveTime = 1
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID AND PPT.InvoiceID = @EntityID )
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PTT4
		OUTER APPLY
			(
				SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
					AND TT.TimeTypeCode = 'PROD'
					AND @IncludeProductiveTime = 1
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID AND (SELECT I.ProjectID FROM Invoice.Invoice I WHERE I.InvoiceID = @EntityID) = PP.ProjectID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PTT5
	ORDER BY 1, CTT.IsBalancePayableUponTermination, TT.TimeTypeName, TT.TimeTypeID, PP.PersonProjectName, PP.PersonProjectID
	
END
GO
--End procedure person.GetPersonProjectLeave

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,
		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE 
					WHEN EXISTS 
						(
						SELECT 1 
						FROM client.ClientPerson CP 
						WHERE CP.PersonID = P.PersonID 
							AND CP.ClientPersonRoleCode = 'Consultant'
							AND EXISTS
								(
								SELECT 1
								FROM client.ClientConfiguration CC
								WHERE CC.ClientID = CP.ClientID
									AND CC.ClientConfigurationKey = 'ShowPersonProfilePersonalInformation'
									AND CAST(CC.ClientConfigurationValue AS INT) = 1
									)
						) 
						AND core.NullIfEmpty(P.SummaryBiography) IS NULL 
					THEN ',SummaryBiography' 
					ELSE '' 
				END 
			+ CASE 
					WHEN EXISTS 
						(
						SELECT 1 
						FROM client.ClientPerson CP 
						WHERE CP.PersonID = P.PersonID 
							AND CP.ClientPersonRoleCode = 'Consultant'
							AND EXISTS
								(
								SELECT 1
								FROM client.ClientConfiguration CC
								WHERE CC.ClientID = CP.ClientID
									AND CC.ClientConfigurationKey = 'ShowPersonProfilePersonalInformation'
									AND CAST(CC.ClientConfigurationValue AS INT) = 1
									)
						) 
						 AND NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.EntityTypeCode = 'Person' AND DE.EntityTypeSubCode = 'CV' AND DE.EntityID = P.PersonID)
					 THEN ',CV'
					ELSE '' 
				END 
			+	CASE 
					WHEN EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = P.PersonID) 
						AND 
							(
							core.NullIfEmpty(P.BillAddress1) IS NULL
								OR core.NullIfEmpty(P.BillISOCountryCode2) IS NULL
								OR core.NullIfEmpty(P.BillMunicipality) IS NULL
								OR core.NullIfEmpty(P.BillRegion) IS NULL
								OR P.ContractingTypeID = 0
							)
					THEN ',InvoicingDetails'
					ELSE '' 
				END 
			+ CASE 
					WHEN EXISTS 
						(
						SELECT 1 
						FROM person.PersonProject PP 
						WHERE PP.PersonID = P.PersonID 
							AND NOT EXISTS (SELECT 1 FROM person.PersonAccount PA WHERE PA.PersonID = P.PersonID)
						)
					 THEN ',PersonAccountCount'
					ELSE '' 
				END 
			+ CASE 
					WHEN EXISTS 
						(
						SELECT 1 
						FROM person.PersonProject PP 
						WHERE PP.PersonID = P.PersonID 
							AND (SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = P.PersonID) < 2
						)
					 THEN ',PersonNextOfKinCount'
					ELSE '' 
				END 
			+ CASE 
					WHEN EXISTS 
						(
						SELECT 1 
						FROM person.PersonProject PP 
						WHERE PP.PersonID = P.PersonID 
							AND (SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = P.PersonID) < 3
						)
					 THEN ',PersonProofOfLifeCount'
					ELSE '' 
				END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END
	SET @cRequiredProfileUpdate = CASE WHEN LEFT(@cRequiredProfileUpdate, 1) = ',' THEN STUFF(@cRequiredProfileUpdate, 1, 1, '') ELSE @cRequiredProfileUpdate END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			CASE WHEN person.IsProfileUpdateRequired(@nPersonID, @bIsTwoFactorEnabled) = 1 OR LEN(@cRequiredProfileUpdate) > 0 THEN 1 ELSE 0 END,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		TP.*
	FROM @tPerson TP

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviserCloud
GO

--Begin table core.EntityTypeAddUpdate
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Feedback', 
	@EntityTypeName = 'Feedback', 
	@EntityTypeNamePlural = 'Feedback',
	@PrimaryKeyFieldName = 'FeedbackID',
	@SchemaName = 'person', 
	@TableName = 'Feedback'
GO
--End table core.EntityTypeAddUpdate

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ClientList',
	@Icon = 'fa fa-fw fa-users',
	@NewMenuItemCode = 'RosterTools',
	@NewMenuItemText = 'Roster Tools'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'Feedback',
	@NewMenuItemLink = '/feedback/list',
	@NewMenuItemText = 'Feedback',
	@ParentMenuItemCode = 'RosterTools',
	@PermissionableLineageList = 'Fedback.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'RosterTools'
GO
--End table core.MenuItem

--Begin table core.MenuItemClient
INSERT INTO core.MenuItemClient
	(ClientID,	MenuItemID)
SELECT
	C.ClientID,
	MI.MenuItemID
FROM client.Client C
	CROSS APPLY core.MenuItem MI
WHERE C.IntegrationCode = 'Hermis'
	AND MI.MenuItemCode IN ('Feedback', 'RosterTools')
GO
--End table core.MenuItemClient

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View the feedback list', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=1, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--End file Build File - 04 - Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.12 - 2019.03.04 18.03.26')
GO
--End build tracking

