USE DeployAdviserCloud
GO

--Begin synonym person.HermisCloud_person_Feedback
DECLARE @TableName VARCHAR(250) = 'person.HermisCloud_person_Feedback'

EXEC utility.DropObject @TableName
GO
--End synonym person.HermisCloud_person_Feedback

--Begin table core.MenuItemClient
DECLARE @TableName VARCHAR(250) = 'core.MenuItemClient'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemClient
	(
	MenuItemClientID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	MenuItemID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemClientID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemClient', 'ClientID,MenuItemID'
GO
--End table core.MenuItemClient

--Begin table invoice.Invoice
ALTER TABLE invoice.Invoice ALTER COLUMN Notes NVARCHAR(MAX)
GO
--End table invoice.Invoice
