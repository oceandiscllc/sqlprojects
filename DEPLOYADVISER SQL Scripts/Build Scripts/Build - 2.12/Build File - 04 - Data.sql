USE DeployAdviserCloud
GO

--Begin table core.EntityTypeAddUpdate
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Feedback', 
	@EntityTypeName = 'Feedback', 
	@EntityTypeNamePlural = 'Feedback',
	@PrimaryKeyFieldName = 'FeedbackID',
	@SchemaName = 'person', 
	@TableName = 'Feedback'
GO
--End table core.EntityTypeAddUpdate

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ClientList',
	@Icon = 'fa fa-fw fa-users',
	@NewMenuItemCode = 'RosterTools',
	@NewMenuItemText = 'Roster Tools'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'Feedback',
	@NewMenuItemLink = '/feedback/list',
	@NewMenuItemText = 'Feedback',
	@ParentMenuItemCode = 'RosterTools',
	@PermissionableLineageList = 'Fedback.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'RosterTools'
GO
--End table core.MenuItem

--Begin table core.MenuItemClient
INSERT INTO core.MenuItemClient
	(ClientID,	MenuItemID)
SELECT
	C.ClientID,
	MI.MenuItemID
FROM client.Client C
	CROSS APPLY core.MenuItem MI
WHERE C.IntegrationCode = 'Hermis'
	AND MI.MenuItemCode IN ('Feedback', 'RosterTools')
GO
--End table core.MenuItemClient

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View the feedback list', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=1, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
