USE DeployAdviser
GO

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.DropColumn @TableName, 'ExchangeRateXML'

EXEC utility.AddColumn @TableName, 'InvalidLineManagerLoginAttempts', 'INT', '0'
EXEC utility.AddColumn @TableName, 'LineManagerPin', 'CHAR(4)'
EXEC utility.AddColumn @TableName, 'LineManagerToken', 'VARCHAR(36)'
GO

EXEC utility.DropObject 'invoice.TR_Invoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A trigger to populate the LineManagerPin column
-- ============================================================
CREATE TRIGGER invoice.TR_Invoice ON invoice.Invoice AFTER INSERT
AS
BEGIN
	SET ARITHABORT ON;

	UPDATE INV
	SET 
		INV.LineManagerPin = CAST(FLOOR(RAND() * (9999 - 1000) + 1000) AS VARCHAR(4)),
		INV.LineManagerToken = newID()
	FROM invoice.Invoice INV
		JOIN INSERTED INS ON INS.InvoiceID = INV.InvoiceID

END
GO
--End table invoice.Invoice

--Begin table syslog.APILog
DECLARE @TableName VARCHAR(250) = 'syslog.APILog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.APILog
	(
	APILogID INT IDENTITY(1,1) NOT NULL,
	EventCode VARCHAR(50) NULL,
	APIData VARCHAR(MAX) NULL,
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'APILogID DESC'
GO
--End table syslog.APILog
