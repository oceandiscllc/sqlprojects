USE DeployAdviser
GO

--Begin procedure invoice.GetMissingExchangeRateData
EXEC utility.DropObject 'invoice.GetMissingExchangeRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return any exchange rate data needed by an invoice but not present in the invoice.ExchangeRate table
-- =======================================================================================================================================
CREATE PROCEDURE invoice.GetMissingExchangeRateData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	--ExchangeRateVendorAPIKey
	SELECT core.GetSystemSetupValueBySystemSetupKey('OANDAAPIKey', '') AS OANDAAPIKey

	--MissingExchangeRateData
	SELECT
		PP.ISOCurrencyCode AS ISOCurrencyCodeTo,
		PPE.ISOCurrencyCode	AS ISOCurrencyCodeFrom,
		invoice.GetExchangeRateDate(PPE.ExpenseDate) AS ExchangeRateDate
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			AND PPE.IsProjectExpense = 1
			AND PPE.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.ISOCurrencyCode <> PP.ISOCurrencyCode
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
						WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
						)
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = PP.ISOCurrencyCode
					AND ER.ISOCurrencyCodeFrom = PPE.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		PP.ISOCurrencyCode AS ISOCurrencyCodeTo,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PP.ISOCurrencyCode <> PP.ISOCurrencyCode
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = PP.ISOCurrencyCode
					AND ER.ISOCurrencyCodeFrom = PP.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		PP.ISOCurrencyCode AS ISOCurrencyCodeTo,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PL.DPAISOCurrencyCode <> PP.ISOCurrencyCode
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = PP.ISOCurrencyCode
					AND ER.ISOCurrencyCodeFrom = PL.DPAISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		PP.ISOCurrencyCode AS ISOCurrencyCodeTo,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PL.DSAISOCurrencyCode <> PP.ISOCurrencyCode
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = PP.ISOCurrencyCode
					AND ER.ISOCurrencyCodeFrom = PL.DSAISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	ORDER BY 3, 2

END
GO
--End procedure invoice.GetMissingExchangeRateData

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.LineManagerPIN,
		I.Notes,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoiceReviewData
EXEC utility.DropObject 'invoice.GetInvoiceReviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoiceReviewData

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	EXEC invoice.InitializeInvoiceDataByInvoiceID @InvoiceID

	--InvoiceData
	EXEC invoice.GetInvoiceByInvoiceID @InvoiceID

	--InvoiceExpenseLog
	EXEC invoice.GetInvoiceExpenseLogByInvoiceID @InvoiceID

	--InvoicePerDiemLog
	EXEC invoice.GetInvoicePerDiemLogByInvoiceID @InvoiceID

	--InvoicePerDiemLogSummary
	EXEC invoice.GetInvoicePerDiemLogSummaryByInvoiceID @InvoiceID

	--InvoiceTimeLog
	EXEC invoice.GetInvoiceTimeLogByInvoiceID @InvoiceID

	--InvoiceTimeLogSummary
	EXEC invoice.GetInvoiceTimeLogSummaryByInvoiceID @InvoiceID

	--InvoiceTotals
	EXEC invoice.GetInvoiceTotalsByInvoiceID @InvoiceID

	--InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowDataByInvoiceID @InvoiceID

END
GO
--End procedure invoice.GetInvoiceReviewData

--Begin procedure invoice.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get data from the invoice.Invoice Table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDocumentGUID VARCHAR(50) = (SELECT D.DocumentGUID FROM document.DocumentEntity DE JOIN document.Document D ON D.DocumentID = DE.DocumentID AND DE.EntityTypeCode = 'Invoice' AND DE.EntityID = @InvoiceID)

	--InvoiceSummaryData
	SELECT

		CASE
			WHEN @cDocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + @cDocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		@PersonID AS PersonID,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		person.FormatPersonNameByPersonID(@PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.InvoiceAmount,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		I.LineManagerPIN,
		I.LineManagerToken,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		IRR.InvoiceRejectionReasonName,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		PN.PersonID AS InvoicePersonID,
		person.HasPermission('Main.Error.ViewCFErrors', PN.PersonID) AS HasViewCFErrorsPermission,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted,
		PN.UserName AS InvoicePersonUserName
	FROM invoice.Invoice I
		JOIN dropdown.InvoiceRejectionReason IRR ON IRR.InvoiceRejectionReasonID = I.InvoiceRejectionReasonID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

	--InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowDataByInvoiceID @InvoiceID

END
GO
--End procedure invoice.GetInvoiceSummaryDataByInvoiceID

--Begin procedure invoice.GetInvoiceWorkflowDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get workflow data for an invoice
-- ===================================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsInWorkflow BIT = 1

	--InvoiceWorkflowData
	IF EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Invoice' AND EWSGP.EntityID = @InvoiceID)
		EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SET @bIsInWorkflow = 0

		SELECT
			'Line Manager Approval' AS WorkflowStepName,
			1 AS WorkflowStepNumber,
			1 AS WorkflowStepCount

		END
	--ENDIF

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'linemanagerapprove'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'linemanagerreject'
			THEN 'Line Manager Rejected Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('approve','create','decrementworkflow','incrementworkflow','linemanagerapprove','linemanagerreject')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	IF @bIsInWorkflow = 1
		EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			PP.ManagerName AS FullName,
			PP.ManagerEmailAddress AS EmailAddress,
			0 AS IsComplete
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.ManagerEmailAddress IS NOT NULL

		UNION

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			PP.AlternateManagerName AS FullName,
			PP.AlternateManagerEmailAddress AS EmailAddress,
			0 AS IsComplete
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.AlternateManagerEmailAddress IS NOT NULL

		END
	--ENDIF

END
GO
--End procedure invoice.GetInvoiceWorkflowDataByInvoiceID

--Begin procedure invoice.ProcessLineManagerInvoiceWorkflowAction
EXEC utility.DropObject 'invoice.ProcessLineManagerInvoiceWorkflowAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.29
-- Description:	A stored procedure to reject or move an invoice into the regular workflow based on a line manager action
-- =====================================================================================================================
CREATE PROCEDURE invoice.ProcessLineManagerInvoiceWorkflowAction

@InvoiceID INT = 0,
@WorkflowAction VARCHAR(50) = NULL,
@WorkflowComments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientID INT = 0

	UPDATE I
	SET
		I.InvalidLineManagerLoginAttempts = 0,
		I.LineManagerPin = NULL,
		I.LineManagerToken = NULL
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	EXEC eventlog.LogInvoiceAction @InvoiceID, @WorkflowAction, 0, @WorkflowComments

	IF @WorkflowAction = 'LineManagerApprove'
		BEGIN

		SELECT @nClientID = P.ClientID
		FROM invoice.Invoice I
			JOIN project.Project P ON P.ProjectID = I.ProjectID

		EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

		END
	ELSE IF @WorkflowAction = 'LineManagerReject'
		EXEC invoice.RejectInvoiceByInvoiceID @InvoiceID
	--ENDIF

END
GO
--End procedure invoice.ProcessLineManagerInvoiceWorkflowAction

--Begin procedure invoice.RejectInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.RejectInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to reset the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ====================================================================================================================================
CREATE PROCEDURE invoice.RejectInvoiceByInvoiceID

@InvoiceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PPE
	SET PPE.InvoiceID = 0
	FROM person.PersonProjectExpense PPE
	WHERE PPE.InvoiceID = @InvoiceID

	UPDATE PPT
	SET PPT.InvoiceID = 0
	FROM person.PersonProjectTime PPT
	WHERE PPT.InvoiceID = @InvoiceID

	EXEC workflow.decrementWorkflow 'Invoice', @InvoiceID

END
GO
--End procedure invoice.RejectInvoiceByInvoiceID

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0,
@PersonUnavailabilityDatesToToggle VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cInvoiceISOCurrencyCode CHAR(3)
	DECLARE @nClientID INT

	SELECT 
		@cInvoiceISOCurrencyCode = I.ISOCurrencyCode,
		@nClientID = P.ClientID 
	FROM project.Project P 
		JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID 
			AND I.InvoiceID = @InvoiceID

	UPDATE PPE
	SET 
		PPE.ExchangeRate = 
			CASE
				WHEN PPE.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PPE.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPE.ExpenseDate)
			END,

		PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET 
		PPT.ExchangeRate = 
			CASE
				WHEN PP.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PP.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPT.DateWorked)
			END,

		PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	;

	EXEC person.SavePersonUnavailabilityByPersonID @PersonID, @PersonUnavailabilityDatesToToggle

	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	EXEC invoice.InitializeInvoiceDataByInvoiceID @InvoiceID

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure invoice.validateLineManager
EXEC utility.DropObject 'invoice.validateLineManager'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A stored procedure to return data from the invoice.Invoice table based on a Token
-- ==============================================================================================
CREATE PROCEDURE invoice.validateLineManager

@Token VARCHAR(36),
@LineManagerPIN CHAR(4) = ''

AS
BEGIN
	SET NOCOUNT ON;

	IF LEN(@LineManagerPIN) > 0
		BEGIN

		UPDATE I
		SET I.InvalidLineManagerLoginAttempts = I.InvalidLineManagerLoginAttempts + 1
		FROM invoice.Invoice I
		WHERE I.LineManagerToken = @Token
			AND I.LineManagerPIN <> @LineManagerPIN

		END
	--ENDIF

	SELECT 
		I.InvalidLineManagerLoginAttempts,
		I.InvoiceID,
		I.LineManagerPIN,
		I.LineManagerToken,
		person.FormatPersonNameByPersonID(I.PersonID, 'TitleFirstLast') AS PersonNameFormatted
	FROM invoice.Invoice I
	WHERE I.LineManagerToken = @Token

END
GO
--End procedure invoice.validateLineManager

--Begin procedure person.GetPersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.GetPersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A stored procedure to return data from the person.PersonProjectExpense table
-- =========================================================================================
CREATE PROCEDURE person.GetPersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectID INT = ISNULL((SELECT PPE.PersonProjectID FROM person.PersonProjectExpense PPE WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID), 0)

	--PersonProjectExpense
	SELECT
		C.CurrencyName,
		C.ISOCurrencyCode,
		CF.ClientFunctionName,
		P.ProjectName,
		PM.PaymentMethodID, 
		PM.PaymentMethodName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPE.ClientCostCodeID,
		project.GetProjectCostCodeDescriptionByClientCostCodeID(PPE.ClientCostCodeID) AS ProjectCostCodeDescription,
		PPE.ExpenseAmount,
		PPE.ExpenseDate,
		core.FormatDate(PPE.ExpenseDate) AS ExpenseDateFormatted,
		PPE.InvoiceID,
		PPE.IsProjectExpense,
		PPE.OwnNotes,
		PPE.PersonProjectExpenseID,
		PPE.PersonProjectID,
		PPE.ProjectManagerNotes,
		PPE.TaxAmount,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PPE.ISOCurrencyCode
		JOIN dropdown.PaymentMethod PM ON PM.PaymentMethodID = PPE.PaymentMethodID
			AND PPE.PersonProjectExpenseID = @PersonProjectExpenseID

	--PersonProjectExpenseClientCostCode & PersonProjectExpenseProjectCurrency
	EXEC person.GetPersonProjectExpenseDataByPersonProjectID @nPersonProjectID

	--PersonProjectExpenseDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'PersonProjectExpense' 
			AND DE.EntityID = @PersonProjectExpenseID

END
GO
--End procedure person.GetPersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectTime
	SELECT
		CF.ClientFunctionName,
		P.ProjectName,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.InvoiceID,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		PTOR.ProjectTermOfReferenceName
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID