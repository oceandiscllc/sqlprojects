USE DeployAdviser
GO

--Begin table core.EmailTemplate
UPDATE ET
SET ET.EmailText = '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]</p><p>In order for this invoice to be processed, you must review the invoice time log and approve or reject the entires. &nbsp; You may review this invoice [[LineManagerLink]].</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[LineManagerURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode = 'Invoice'
	AND ET.EmailTemplateCode = 'Submit'

GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Invoice', '[[LineManagerLink]]', 'Embedded Line Manager Link'),
	('Invoice', '[[LineManagerURL]]', 'Line Manager URL')

GO
--End table core.EmailTemplateField