USE DeployAdviser
GO


--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID)
	SELECT 
		PP.ProjectID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID
	FROM project.ProjectPerson PP
	WHERE PP.PersonID = @PersonID	

	UNION

	SELECT 
		P.ProjectID
	FROM project.Project P
	WHERE EXISTS	
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NOT NULL
		)

	UNION

	SELECT 
		PJ.ProjectID
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID