-- File Name:	Build - 1.7 - DEPLOYADVISER.sql
-- Build Key:	Build - 1.7 - 2017.11.27 16.59.42

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Procedures:
--		invoice.GetInvoiceByInvoiceID
--		person.CheckAccess
--		person.DeletePersonProjectExpenseByPersonProjectExpenseID
--		person.DeletePersonProjectTimeByPersonProjectTimeID
--		person.GetPersonInviteRolesByClientID
--		person.GetPersonProjectByPersonProjectID
--		person.GetPersonProjectLocationsByPersonProjectID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(1000)'
GO
--End table invoice.Invoice
	
--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'AlternateManagerName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'AlternateManagerEmailAddress', 'VARCHAR(320)'
GO
--End table person.PersonProject


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.Notes,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Administrator')) OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
								workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL)
				AND (@AccessCode <> 'AddUpdate' OR T.EndDate >= getDate())
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.DeletePersonProjectExpenseByPersonProjectExpenseID
EXEC utility.DropObject 'person.DeletePersonProjectExpenseByPersonProjectExpenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.26
-- Description:	A stored procedure to delete data from the person.PersonProjectExpense table based on a PersonProjectExpenseID
-- ===========================================================================================================================
CREATE PROCEDURE person.DeletePersonProjectExpenseByPersonProjectExpenseID

@PersonProjectExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PPE
	FROM person.PersonProjectExpense PPE
	WHERE PPE.PersonProjectExpenseID = @PersonProjectExpenseID

END
GO
--End procedure person.DeletePersonProjectExpenseByPersonProjectExpenseID

--Begin procedure person.DeletePersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.DeletePersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.26
-- Description:	A stored procedure to delete data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.DeletePersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PPT
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectTimeID = @PersonProjectTimeID

END
GO
--End procedure person.DeletePersonProjectTimeByPersonProjectTimeID

--Begin procedure person.GetPersonInviteRolesByClientID
EXEC utility.DropObject 'person.GetPersonInviteRolesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return invitable roles for a person / client combination
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonInviteRolesByClientID

@ClientID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH CPD AS
		(
		SELECT CP.ClientPersonRoleCode
		FROM client.ClientPerson CP
		WHERE CP.ClientID = @ClientID
			AND CP.PersonID = @PersonID
		)

	SELECT
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'Administrator'
		AND EXISTS
			(
			SELECT 1
			FROM person.Person P
			WHERE P.PersonID = @PersonID
				AND P.IsSuperAdministrator = 1
			)

	UNION

	SELECT 
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'ProjectManager'
		AND EXISTS
			(
			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'Administrator'
			)

	UNION

	SELECT 
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'Consultant'
		AND EXISTS
			(
			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'Administrator'

			UNION

			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'ProjectManager'
			)

	ORDER BY 1, 3, 2			

END
GO
--End procedure person.GetPersonInviteRolesByClientID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days INT NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS AlternateManagerEmailAddress,
			'' AS AlternateManagerName,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			0 AS FeeRate,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectTermOfReferenceID,
			'' AS ProjectTermOfReferenceName,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectLaborCodeID,
			'' AS ProjectLaborCodeName,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.AlternateManagerEmailAddress,
			PP.AlternateManagerName,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = P.ClientID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectLocationsByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectLocationsByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProjectLocation table based on a PersonID
-- ==============================================================================================================
CREATE PROCEDURE person.GetPersonProjectLocationsByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		
		CASE WHEN CanWorkDay1 = 1 THEN '' ELSE '0,' END
			+ CASE WHEN CanWorkDay2 = 1 THEN '' ELSE '1,' END
			+ CASE WHEN CanWorkDay3 = 1 THEN '' ELSE '2,' END
			+ CASE WHEN CanWorkDay4 = 1 THEN '' ELSE '3,' END
			+ CASE WHEN CanWorkDay5 = 1 THEN '' ELSE '4,' END
			+ CASE WHEN CanWorkDay6 = 1 THEN '' ELSE '5,' END
			+ CASE WHEN CanWorkDay7 = 1 THEN '' ELSE '6,' END
		AS DaysOfWeekDisabled

	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			AND PPL.PersonProjectID = @PersonProjectID
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectLocationsByPersonProjectID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data - Client.sql
--USE DeployAdviser
GO

--Begin table client.ProjectLaborCode
TRUNCATE TABLE client.ProjectLaborCode
GO

IF NOT EXISTS (SELECT 1 FROM client.Client C WHERE C.ClientName = 'HSOT')
	INSERT INTO client.Client (C.ClientName) VALUES ('HSOT')
--ENDIF

EXEC utility.InsertIdentityValue 'client.ProjectLaborCode', 'ProjectLaborCodeID', 0
GO

DECLARE @nClientID INT = (SELECT C.ClientID from client.Client C WHERE C.ClientName = 'HSOT')

INSERT INTO client.ProjectLaborCode
	(ClientID,ProjectLaborCode,ProjectLaborCodeName)
VALUES
	(@nClientID, '9TMLA0', 'Team Leader'),
	(@nClientID, '9DTMA0', 'Deputy Team Leader'),
	(@nClientID, '9DTLA0', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMA0', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMA0', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMA0', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUA0', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIA0', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUA0', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIA0', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUA0', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUA0', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIA0', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUA0', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIA0', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIA0', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUA0', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUA0', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIA0', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIA0', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUA0', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIA0', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA0', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA0', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUA0', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUA0', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIA0', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUA0', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIA0', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIA0', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIA0', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUA0', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIA0', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIA0', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIA0', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIA0', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIA0', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUA0', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIA0', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIA0', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUA0', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUA0', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUA0', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUA0', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIA0', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIA0', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUA0', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUA0', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIA0', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIA0', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUA0', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIA0', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUA0', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIA0', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIA0', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUA0', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLA5', 'Team Leader'),
	(@nClientID, '9DTMA5', 'Deputy Team Leader'),
	(@nClientID, '9DTLA5', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMA5', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMA5', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMA5', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUA5', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIA5', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUA5', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIA5', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUA5', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUA5', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIA5', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUA5', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIA5', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIA5', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUA5', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUA5', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIA5', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIA5', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUA5', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIA5', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA5', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA5', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUA5', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUA5', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIA5', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUA5', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIA5', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIA5', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIA5', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUA5', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIA5', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIA5', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIA5', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIA5', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIA5', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUA5', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIA5', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIA5', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUA5', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUA5', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUA5', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUA5', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIA5', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIA5', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUA5', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUA5', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIA5', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIA5', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUA5', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIA5', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUA5', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIA5', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIA5', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUA5', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLA7', 'Team Leader'),
	(@nClientID, '9DTMA7', 'Deputy Team Leader'),
	(@nClientID, '9DTLA7', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMA7', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMA7', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMA7', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUA7', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIA7', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUA7', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIA7', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUA7', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUA7', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIA7', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUA7', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIA7', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIA7', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUA7', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUA7', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIA7', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIA7', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUA7', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIA7', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA7', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIA7', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUA7', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUA7', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIA7', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUA7', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIA7', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIA7', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIA7', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUA7', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIA7', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIA7', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIA7', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIA7', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIA7', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUA7', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIA7', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIA7', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUA7', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUA7', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUA7', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUA7', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIA7', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIA7', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUA7', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUA7', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIA7', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIA7', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUA7', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIA7', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUA7', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIA7', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIA7', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUA7', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLAX', 'Team Leader'),
	(@nClientID, '9DTMAX', 'Deputy Team Leader'),
	(@nClientID, '9DTLAX', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMAX', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMAX', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMAX', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUAX', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIAX', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUAX', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIAX', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUAX', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUAX', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIAX', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUAX', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIAX', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIAX', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUAX', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUAX', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIAX', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIAX', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUAX', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIAX', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIAX', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIAX', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUAX', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUAX', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIAX', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUAX', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIAX', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIAX', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIAX', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUAX', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIAX', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIAX', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIAX', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIAX', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIAX', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUAX', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIAX', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIAX', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUAX', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUAX', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUAX', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUAX', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIAX', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIAX', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUAX', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUAX', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIAX', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIAX', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUAX', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIAX', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUAX', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIAX', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIAX', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUAX', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLB0', 'Team Leader'),
	(@nClientID, '9DTMB0', 'Deputy Team Leader'),
	(@nClientID, '9DTLB0', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMB0', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMB0', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMB0', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUB0', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIB0', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUB0', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIB0', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUB0', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUB0', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIB0', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUB0', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIB0', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIB0', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUB0', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUB0', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIB0', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIB0', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUB0', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIB0', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB0', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB0', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUB0', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUB0', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIB0', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUB0', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIB0', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIB0', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIB0', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUB0', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIB0', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIB0', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIB0', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIB0', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIB0', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUB0', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIB0', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIB0', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUB0', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUB0', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUB0', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUB0', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIB0', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIB0', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUB0', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUB0', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIB0', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIB0', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUB0', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIB0', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUB0', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIB0', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIB0', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUB0', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLB5', 'Team Leader'),
	(@nClientID, '9DTMB5', 'Deputy Team Leader'),
	(@nClientID, '9DTLB5', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMB5', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMB5', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMB5', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUB5', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIB5', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUB5', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIB5', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUB5', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUB5', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIB5', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUB5', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIB5', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIB5', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUB5', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUB5', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIB5', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIB5', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUB5', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIB5', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB5', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB5', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUB5', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUB5', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIB5', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUB5', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIB5', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIB5', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIB5', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUB5', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIB5', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIB5', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIB5', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIB5', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIB5', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUB5', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIB5', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIB5', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUB5', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUB5', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUB5', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUB5', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIB5', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIB5', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUB5', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUB5', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIB5', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIB5', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUB5', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIB5', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUB5', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIB5', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIB5', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUB5', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLB7', 'Team Leader'),
	(@nClientID, '9DTMB7', 'Deputy Team Leader'),
	(@nClientID, '9DTLB7', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMB7', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMB7', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMB7', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUB7', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIB7', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUB7', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIB7', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUB7', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUB7', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIB7', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUB7', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIB7', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIB7', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUB7', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUB7', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIB7', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIB7', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUB7', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIB7', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB7', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIB7', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUB7', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUB7', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIB7', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUB7', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIB7', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIB7', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIB7', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUB7', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIB7', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIB7', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIB7', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIB7', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIB7', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUB7', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIB7', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIB7', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUB7', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUB7', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUB7', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUB7', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIB7', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIB7', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUB7', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUB7', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIB7', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIB7', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUB7', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIB7', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUB7', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIB7', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIB7', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUB7', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLBX', 'Team Leader'),
	(@nClientID, '9DTMBX', 'Deputy Team Leader'),
	(@nClientID, '9DTLBX', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMBX', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMBX', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMBX', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUBX', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIBX', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUBX', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIBX', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUBX', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUBX', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIBX', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUBX', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIBX', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIBX', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUBX', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUBX', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIBX', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIBX', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUBX', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIBX', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIBX', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIBX', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUBX', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUBX', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIBX', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUBX', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIBX', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIBX', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIBX', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUBX', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIBX', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIBX', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIBX', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIBX', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIBX', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUBX', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIBX', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIBX', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUBX', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUBX', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUBX', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUBX', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIBX', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIBX', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUBX', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUBX', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIBX', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIBX', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUBX', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIBX', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUBX', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIBX', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIBX', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUBX', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLC0', 'Team Leader'),
	(@nClientID, '9DTMC0', 'Deputy Team Leader'),
	(@nClientID, '9DTLC0', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMC0', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMC0', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMC0', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUC0', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIC0', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUC0', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIC0', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUC0', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUC0', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIC0', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUC0', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIC0', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIC0', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUC0', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUC0', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIC0', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIC0', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUC0', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIC0', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC0', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC0', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUC0', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUC0', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIC0', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUC0', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIC0', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIC0', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIC0', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUC0', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIC0', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIC0', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIC0', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIC0', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIC0', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUC0', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIC0', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIC0', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUC0', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUC0', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUC0', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUC0', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIC0', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIC0', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUC0', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUC0', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIC0', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIC0', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUC0', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIC0', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUC0', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIC0', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIC0', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUC0', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLC5', 'Team Leader'),
	(@nClientID, '9DTMC5', 'Deputy Team Leader'),
	(@nClientID, '9DTLC5', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMC5', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMC5', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMC5', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUC5', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIC5', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUC5', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIC5', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUC5', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUC5', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIC5', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUC5', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIC5', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIC5', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUC5', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUC5', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIC5', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIC5', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUC5', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIC5', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC5', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC5', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUC5', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUC5', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIC5', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUC5', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIC5', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIC5', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIC5', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUC5', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIC5', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIC5', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIC5', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIC5', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIC5', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUC5', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIC5', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIC5', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUC5', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUC5', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUC5', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUC5', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIC5', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIC5', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUC5', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUC5', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIC5', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIC5', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUC5', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIC5', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUC5', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIC5', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIC5', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUC5', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLC7', 'Team Leader'),
	(@nClientID, '9DTMC7', 'Deputy Team Leader'),
	(@nClientID, '9DTLC7', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMC7', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMC7', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMC7', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUC7', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIC7', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUC7', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIC7', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUC7', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUC7', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIC7', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUC7', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIC7', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIC7', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUC7', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUC7', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIC7', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIC7', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUC7', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIC7', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC7', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIC7', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUC7', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUC7', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIC7', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUC7', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIC7', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIC7', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIC7', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUC7', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIC7', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIC7', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIC7', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIC7', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIC7', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUC7', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIC7', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIC7', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUC7', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUC7', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUC7', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUC7', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIC7', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIC7', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUC7', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUC7', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIC7', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIC7', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUC7', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIC7', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUC7', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIC7', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIC7', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUC7', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLCX', 'Team Leader'),
	(@nClientID, '9DTMCX', 'Deputy Team Leader'),
	(@nClientID, '9DTLCX', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMCX', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMCX', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMCX', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUCX', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMICX', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUCX', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMICX', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUCX', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUCX', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTICX', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUCX', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTICX', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAICX', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUCX', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUCX', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOICX', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMICX', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUCX', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLICX', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAICX', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAICX', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUCX', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUCX', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSICX', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUCX', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEICX', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHICX', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STICX', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUCX', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSICX', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAICX', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAICX', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSICX', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRICX', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUCX', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASICX', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASICX', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUCX', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUCX', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUCX', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUCX', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSICX', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSICX', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUCX', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUCX', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSICX', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSICX', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUCX', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAICX', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUCX', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSICX', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSICX', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUCX', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLD0', 'Team Leader'),
	(@nClientID, '9DTMD0', 'Deputy Team Leader'),
	(@nClientID, '9DTLD0', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMD0', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMD0', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMD0', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUD0', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMID0', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUD0', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMID0', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUD0', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUD0', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTID0', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUD0', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTID0', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAID0', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUD0', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUD0', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOID0', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMID0', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUD0', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLID0', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID0', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID0', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUD0', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUD0', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSID0', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUD0', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEID0', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHID0', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STID0', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUD0', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSID0', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAID0', 'Health Operations Specilaist, CHASE International Entry Level')
GO

DECLARE @nClientID INT = (SELECT C.ClientID from client.Client C WHERE C.ClientName = 'HSOT')

INSERT INTO client.ProjectLaborCode
	(ClientID,ProjectLaborCode,ProjectLaborCodeName)
VALUES
	(@nClientID, '9CAID0', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSID0', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRID0', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUD0', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASID0', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASID0', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUD0', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUD0', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUD0', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUD0', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSID0', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSID0', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUD0', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUD0', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSID0', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSID0', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUD0', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAID0', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUD0', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSID0', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSID0', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUD0', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLD5', 'Team Leader'),
	(@nClientID, '9DTMD5', 'Deputy Team Leader'),
	(@nClientID, '9DTLD5', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMD5', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMD5', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMD5', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUD5', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMID5', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUD5', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMID5', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUD5', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUD5', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTID5', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUD5', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTID5', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAID5', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUD5', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUD5', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOID5', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMID5', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUD5', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLID5', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID5', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID5', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUD5', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUD5', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSID5', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUD5', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEID5', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHID5', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STID5', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUD5', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSID5', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAID5', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAID5', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSID5', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRID5', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUD5', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASID5', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASID5', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUD5', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUD5', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUD5', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUD5', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSID5', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSID5', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUD5', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUD5', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSID5', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSID5', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUD5', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAID5', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUD5', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSID5', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSID5', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUD5', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLD7', 'Team Leader'),
	(@nClientID, '9DTMD7', 'Deputy Team Leader'),
	(@nClientID, '9DTLD7', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMD7', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMD7', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMD7', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUD7', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMID7', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUD7', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMID7', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUD7', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUD7', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTID7', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUD7', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTID7', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAID7', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUD7', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUD7', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOID7', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMID7', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUD7', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLID7', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID7', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAID7', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUD7', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUD7', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSID7', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUD7', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEID7', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHID7', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STID7', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUD7', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSID7', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAID7', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAID7', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSID7', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRID7', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUD7', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASID7', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASID7', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUD7', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUD7', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUD7', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUD7', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSID7', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSID7', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUD7', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUD7', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSID7', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSID7', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUD7', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAID7', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUD7', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSID7', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSID7', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUD7', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLDX', 'Team Leader'),
	(@nClientID, '9DTMDX', 'Deputy Team Leader'),
	(@nClientID, '9DTLDX', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMDX', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMDX', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMDX', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUDX', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIDX', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUDX', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIDX', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUDX', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUDX', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIDX', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUDX', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIDX', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIDX', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUDX', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUDX', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIDX', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIDX', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUDX', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIDX', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIDX', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIDX', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUDX', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUDX', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIDX', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUDX', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIDX', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIDX', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIDX', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUDX', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIDX', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIDX', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIDX', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIDX', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIDX', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUDX', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIDX', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIDX', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUDX', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUDX', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUDX', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUDX', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIDX', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIDX', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUDX', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUDX', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIDX', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIDX', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUDX', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIDX', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUDX', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIDX', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIDX', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUDX', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLE0', 'Team Leader'),
	(@nClientID, '9DTME0', 'Deputy Team Leader'),
	(@nClientID, '9DTLE0', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLME0', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPME0', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPME0', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUE0', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIE0', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUE0', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIE0', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUE0', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUE0', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIE0', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUE0', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIE0', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIE0', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUE0', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUE0', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIE0', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIE0', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUE0', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIE0', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE0', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE0', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUE0', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUE0', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIE0', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUE0', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIE0', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIE0', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIE0', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUE0', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIE0', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIE0', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIE0', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIE0', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIE0', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUE0', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIE0', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIE0', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUE0', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUE0', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUE0', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUE0', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIE0', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIE0', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUE0', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUE0', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIE0', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIE0', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUE0', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIE0', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUE0', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIE0', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIE0', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUE0', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLE5', 'Team Leader'),
	(@nClientID, '9DTME5', 'Deputy Team Leader'),
	(@nClientID, '9DTLE5', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLME5', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPME5', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPME5', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUE5', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIE5', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUE5', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIE5', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUE5', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUE5', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIE5', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUE5', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIE5', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIE5', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUE5', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUE5', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIE5', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIE5', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUE5', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIE5', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE5', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE5', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUE5', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUE5', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIE5', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUE5', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIE5', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIE5', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIE5', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUE5', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIE5', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIE5', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIE5', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIE5', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIE5', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUE5', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIE5', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIE5', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUE5', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUE5', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUE5', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUE5', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIE5', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIE5', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUE5', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUE5', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIE5', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIE5', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUE5', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIE5', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUE5', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIE5', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIE5', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUE5', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLE7', 'Team Leader'),
	(@nClientID, '9DTME7', 'Deputy Team Leader'),
	(@nClientID, '9DTLE7', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLME7', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPME7', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPME7', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUE7', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIE7', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUE7', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIE7', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUE7', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUE7', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIE7', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUE7', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIE7', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIE7', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUE7', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUE7', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIE7', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIE7', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUE7', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIE7', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE7', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIE7', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUE7', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUE7', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIE7', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUE7', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIE7', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIE7', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIE7', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUE7', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIE7', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIE7', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIE7', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIE7', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIE7', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUE7', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIE7', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIE7', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUE7', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUE7', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUE7', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUE7', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIE7', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIE7', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUE7', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUE7', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIE7', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIE7', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUE7', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIE7', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUE7', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIE7', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIE7', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUE7', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9TMLEX', 'Team Leader'),
	(@nClientID, '9DTMEX', 'Deputy Team Leader'),
	(@nClientID, '9DTLEX', 'Logistics Deputy Team Leader'),
	(@nClientID, '9FLMEX', 'Fleet Manager, SU International, Senior Level'),
	(@nClientID, '9OPMEX', 'Operations Manager , CHASE UK, Mid Level'),
	(@nClientID, '9OPMEX', 'Operations Manager, SU UK, Mid Level'),
	(@nClientID, '9PMUEX', 'Programme Manager, CHASE UK, Mid Level'),
	(@nClientID, '9PMIEX', 'Programme Manager, CHASE International, Mid Level'),
	(@nClientID, '9PMUEX', 'Programme Manager, SU UK, Mid Level'),
	(@nClientID, '9PMIEX', 'Programme Manager, SU International, Mid Level'),
	(@nClientID, '9PLUEX', 'Procurement and Logistics Manager, SU UK, Mid-Lvel'),
	(@nClientID, '9LTUEX', 'Lead Trainer / Facilitator, CHASE UK, Senior Level'),
	(@nClientID, '9LTIEX', 'Lead Trainer / Facilitator , CHASE International, Senior Level'),
	(@nClientID, '9LTUEX', 'Lead Trainer / Facilitator, SU UK, Senior Level'),
	(@nClientID, '9LTIEX', 'Lead Trainer / Facilitator, SU International, Senior Level'),
	(@nClientID, '9SAIEX', 'Advisor, SU International, Mid Level'),
	(@nClientID, '9AOUEX', 'Humanitarian Affairs Officer, CHASE UK, Mid Level'),
	(@nClientID, '9ADUEX', 'Humanitarian Advisor, CHASE UK, Senior Level'),
	(@nClientID, '9AOIEX', 'Humanitarian Affairs Officer, CHASE International, Mid Level'),
	(@nClientID, '9IMIEX', 'Information Management Officer, CHASE International, Mid Level'),
	(@nClientID, '9LLUEX', 'Lessons Learning Officer, CHASE UK, Mid Level'),
	(@nClientID, '9LLIEX', 'Lesson Learning Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIEX', 'Ops / Logs Advisor, CHASE International, Mid Level'),
	(@nClientID, '9OAIEX', 'Ops / Logs Advisor, SU International, Mid Level'),
	(@nClientID, '9REUEX', 'Researcher, SU UK, Mid Level'),
	(@nClientID, '9SSUEX', 'Senior Advisor / Specialist, SU UK, Senior Level'),
	(@nClientID, '9SSIEX', 'Senior Advisor / Specialist, SU International, Senior Level'),
	(@nClientID, '9SEUEX', 'Senior Security Advisor, SU UK, Senior Level'),
	(@nClientID, '9SEIEX', 'Senior Security Advisor, SU International, Senior Level'),
	(@nClientID, '9SHIEX', 'Senior Humanitarian Advisors, CHASE International, Senior Level'),
	(@nClientID, '9STIEX', 'Senior Specialist, CHASE International, Senior Level'),
	(@nClientID, '9TRUEX', 'Trainer / Facilitator, CHASE UK, Mid Level'),
	(@nClientID, '9HSIEX', 'Health Operations Specilaist, CHASE International Mid Level'),
	(@nClientID, '9HAIEX', 'Health Operations Specilaist, CHASE International Entry Level'),
	(@nClientID, '9CAIEX', 'Clinical Specialist, CHASE International Entry Level'),
	(@nClientID, '9CSIEX', 'Clinical Specialist, CHASE International Mid Level'),
	(@nClientID, '9PRIEX', 'Procurement and Logistics Specialist, SU International Mid-Level'),
	(@nClientID, '9PRUEX', 'Procurement and Logistics Specialist, SU UK Mid-Level'),
	(@nClientID, '9ASIEX', 'Advisor / Specialist, SU International, Mid Level'),
	(@nClientID, '9ASIEX', 'Advisor / Specialist, CHASE International, Mid Level'),
	(@nClientID, '9DSUEX', 'Deployments Officer, CHASE UK Entry Level'),
	(@nClientID, '9DSUEX', 'Deployments Officer, SU UK, Entry Level'),
	(@nClientID, '9LSUEX', 'Logistics Officer, CHASE UK, Entry Level'),
	(@nClientID, '9LSUEX', 'Logistics Officer, SU UK, Entry Level'),
	(@nClientID, '9LSIEX', 'Logistics Officer ,CHASE International, Entry Level'),
	(@nClientID, '9LSIEX', 'Logistics Officer, SU International, Entry Level'),
	(@nClientID, '9RSUEX', 'Reporting Officer, CHASE UK, Entry Level'),
	(@nClientID, '9RSUEX', 'Reporting Officer, SU UK, Entry Level'),
	(@nClientID, '9RSIEX', 'Reporting Officer, SU International, Entry Level'),
	(@nClientID, '9RSIEX', 'Reporting Officer, CHASE International, Entry Level'),
	(@nClientID, '9LAUEX', 'Logistics Administrator, SU UK, Entry Level'),
	(@nClientID, '9LAIEX', 'Logistics Administrator, SU International, Entry Level'),
	(@nClientID, '9FSUEX', 'Finance and Operations Officer, SU UK, Entry Level'),
	(@nClientID, '9FSIEX', 'Finance and Operations Officer, SU International, Entry Level'),
	(@nClientID, '9FSIEX', 'Finance and Operations Officer, CHASE International, Entry Level'),
	(@nClientID, '9FSUEX', 'Finance and Operations Officer, CHASE UK, Entry Level'),
	(@nClientID, '9NV001', 'Humanitarian Adviser'),
	(@nClientID, '9NV002', 'Humanitarian Adviser'),
	(@nClientID, '9NV003', 'Humanitarian Adviser'),
	(@nClientID, '9NV004', 'Humanitarian Reporting Officer'),
	(@nClientID, '9NV005', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV006', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV007', 'Humanitarian Adviser'),
	(@nClientID, '9NV003', 'Humanitarian Adviser'),
	(@nClientID, '9NV008', 'Cash Adviser'),
	(@nClientID, '9NV009', 'Humanitarian Adviser'),
	(@nClientID, '9NV003', 'Humanitarian Adviser'),
	(@nClientID, '9NV010', 'Humanitarian Adviser'),
	(@nClientID, '9NV011', 'Pakistan Surge Support'),
	(@nClientID, '9NV012', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV013', 'Humanitarian Adviser'),
	(@nClientID, '9NV014', 'Humanitarian Adviser'),
	(@nClientID, '9NV015', 'Humanitarian Adviser'),
	(@nClientID, '9NV016', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV017', 'Humanitarian Adviser'),
	(@nClientID, '9NV018', 'Humanitarian Adviser'),
	(@nClientID, '9NV019', 'Humanitarian Adviser'),
	(@nClientID, '9NV020', 'Regional Humanitarian Adviser'),
	(@nClientID, '9NV021', 'Humanitarian / Civil Military Adviser'),
	(@nClientID, '9NV010', 'Humanitarian Adviser'),
	(@nClientID, '9NV022', 'Humanitarian Adviser'),
	(@nClientID, '9NV023', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV024', 'Humanitarian Adviser'),
	(@nClientID, '9NV013', 'Humanitarian Adviser'),
	(@nClientID, '9NV025', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV026', 'Humanitarian Adviser'),
	(@nClientID, '9NV017', 'Humanitarian Adviser'),
	(@nClientID, '9NV027', 'Humanitarian Adviser'),
	(@nClientID, '9NV013', 'Humanitarian Adviser'),
	(@nClientID, '9NV028', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV029', 'Humanitarian Affairs Officer'),
	(@nClientID, '9NV030', 'Humanitarian Adviser'),
	(@nClientID, '9NV031', 'Support Officer'),
	(@nClientID, '9NV032', 'Migration Advisor'),
	(@nClientID, '9NV033', 'Advisor'),
	(@nClientID, '9NV034', 'Defense Reform Advisor'),
	(@nClientID, '9NV035', 'Strategic Communications Lead'),
	(@nClientID, '9NV036', 'Police Adviser'),
	(@nClientID, '9NV037', 'Parliamentary Affairs Adviser'),
	(@nClientID, '9NV038', 'Senior Adviser (Policing Adviser)'),
	(@nClientID, '9NV039', 'Trainer/Facilitator'),
	(@nClientID, '9NV040', 'Training Course Project Manager and Lead Designer '),
	(@nClientID, '9NV041', 'Public Sector Reform Adviser'),
	(@nClientID, '9NV042', 'Institutional Reform Expert'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV044', 'Conflict Analyst'),
	(@nClientID, '9NV045', 'Essential Services and Civil Contingencies Advisor'),
	(@nClientID, '9NV046', 'Senior Advisor'),
	(@nClientID, '9NV047', 'Yemen STABAD'),
	(@nClientID, '9NV048', 'Advisor'),
	(@nClientID, '9NV049', 'Stabilisation and Gender Advisers'),
	(@nClientID, '9NV050', 'Stabilisation Unit Lessons Team'),
	(@nClientID, '9NV051', 'Senior Advisor'),
	(@nClientID, '9NV052', 'Senior Advisor'),
	(@nClientID, '9NV053', 'Early Recovery and Stabilisation Advisor'),
	(@nClientID, '9NV054', 'Conflict Advisor'),
	(@nClientID, '9NV055', 'Stabilisation Advisor'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV048', 'Advisor'),
	(@nClientID, '9NV056', 'Prosecution Reform Adviser'),
	(@nClientID, '9NV057', 'Senior Governance and Conflict Adviser'),
	(@nClientID, '9NV058', 'Pol/Mil Advisr'),
	(@nClientID, '9NV059', 'Reporting Officer'),
	(@nClientID, '9NV060', 'Advisor'),
	(@nClientID, '9NV061', 'Camp Manager'),
	(@nClientID, '9NV062', 'Civil-Military Lessons and Exercise Advisers'),
	(@nClientID, '9NV054', 'Conflict Advisor'),
	(@nClientID, '9NV063', 'Senior Advisor'),
	(@nClientID, '9NV064', 'Stabilisation Adviser'),
	(@nClientID, '9NV065', 'Advisor'),
	(@nClientID, '9NV051', 'Senior Advisor'),
	(@nClientID, '9NV067', 'Monitoring and Evaluation Adviser'),
	(@nClientID, '9NV068', 'Trainer/Facilitator'),
	(@nClientID, '9NV069', 'Investigations Expert'),
	(@nClientID, '9NV070', 'Cultural Advisor'),
	(@nClientID, '9NV071', 'Trainer/Facilitator'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV072', 'SU Business Support'),
	(@nClientID, '9NV073', 'Advisor'),
	(@nClientID, '9NV074', 'STRATCOM to GNA'),
	(@nClientID, '9NV075', 'Gender, Conflict and Stabilisation Advisor'),
	(@nClientID, '9NV065', 'Advisor'),
	(@nClientID, '9NV076', 'Gender, Conflict and Stability Course Facilitator'),
	(@nClientID, '9NV077', 'Senior Advisor'),
	(@nClientID, '9NV078', 'Senior Adviser'),
	(@nClientID, '9NV079', 'Researcher'),
	(@nClientID, '9NV080', 'Communications Support Officer'),
	(@nClientID, '9NV081', 'Stabilisation Adviser '),
	(@nClientID, '9NV082', 'Centre for Professional Development Mentor'),
	(@nClientID, '9NV083', 'Transitional Justice Advisor'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV084', 'Liaison planners'),
	(@nClientID, '9NV085', 'Advisor'),
	(@nClientID, '9NV073', 'Advisor'),
	(@nClientID, '9NV086', 'Stabilisation Advisor'),
	(@nClientID, '9NV087', 'Deployments Officer'),
	(@nClientID, '9NV088', 'Senior Government Adviser'),
	(@nClientID, '9NV089', 'Senior Government Adviser'),
	(@nClientID, '9NV090', 'Joint Analysis of Conflict and Stability (JACS) Lead'),
	(@nClientID, '9NV091', 'Advisor'),
	(@nClientID, '9NV079', 'Researcher'),
	(@nClientID, '9NV092', 'UNDP STABAD'),
	(@nClientID, '9NV093', 'Military Adviser '),
	(@nClientID, '9NV094', 'Small Arms and Light Weapons Advisor'),
	(@nClientID, '9NV095', 'Military Planning Adviser'),
	(@nClientID, '9NV096', 'Session facilitator/trainer '),
	(@nClientID, '9NV097', 'Senior Advisor'),
	(@nClientID, '9NV098', 'M&E Adviser'),
	(@nClientID, '9NV099', 'Security Planning Officer'),
	(@nClientID, '9NV098', 'M&E Adviser'),
	(@nClientID, '9NV100', 'Monitoring and Evaluation Workshop Facilitator'),
	(@nClientID, '9NV101', 'IHL Trainer'),
	(@nClientID, '9NV102', 'Programme Manager'),
	(@nClientID, '9NV103', 'M&E Adviser'),
	(@nClientID, '9NV104', 'Building Integrity/Counter and Anti- Corruption(BI/CAC) Adviser'),
	(@nClientID, '9NV105', 'Conflict Adviser'),
	(@nClientID, '9NV049', 'Stabilisation and Gender Advisers'),
	(@nClientID, '9NV106', 'Governance Adviser'),
	(@nClientID, '9NV107', 'Senior Strategy Expert'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV108', 'HR Business Support'),
	(@nClientID, '9NV109', 'Advisor'),
	(@nClientID, '9NV106', 'Governance Adviser'),
	(@nClientID, '9NV110', 'Learning and Event Facilitator'),
	(@nClientID, '9NV043', 'Trainer/Facilitator'),
	(@nClientID, '9NV051', 'Senior Advisor'),
	(@nClientID, '9NV111', 'Surge Support Deputy Regional Coordinator MENA'),
	(@nClientID, '9NV112', 'Lead Course Facilitator'),
	(@nClientID, '9NV113', 'Civil-Military Lessons and Exercise Adviser'),
	(@nClientID, '9NV114', 'Camp Liaison Manager'),
	(@nClientID, '9NV115', 'Strat Comms Adviser'),
	(@nClientID, '9NV062', 'Civil-Military Lessons and Exercise Advisers'),
	(@nClientID, '9NV116', 'Multilats Team Support '),
	(@nClientID, '9NV117', 'Case Study Author'),
	(@nClientID, '9NV048', 'Advisor')
GO
--End table client.ProjectLaborCode

--End file Build File - 04 - Data - Client.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO

UPDATE C
SET C.CountryName = 'Aland Islands'
FROM dropdown.Country C
WHERE C.ISOCountryCode3 = 'ALA'
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit the contents of the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Next of Kin data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit Proof of Life data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export person data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a users'' permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View the about page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Export invoice data to excel', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.7 - 2017.11.27 16.59.42')
GO
--End build tracking

