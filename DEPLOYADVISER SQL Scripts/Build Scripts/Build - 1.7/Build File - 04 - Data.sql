USE DeployAdviser
GO

UPDATE C
SET C.CountryName = 'Aland Islands'
FROM dropdown.Country C
WHERE C.ISOCountryCode3 = 'ALA'
GO
