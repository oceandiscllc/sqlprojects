USE DeployAdviser
GO

--Begin table invoice.Invoice
DECLARE @TableName VARCHAR(250) = 'invoice.Invoice'

EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(1000)'
GO
--End table invoice.Invoice
	
--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'AlternateManagerName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'AlternateManagerEmailAddress', 'VARCHAR(320)'
GO
--End table person.PersonProject

