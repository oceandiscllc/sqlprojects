USE DeployAdviser
GO

--Begin table core.CalendarMetadata
TRUNCATE TABLE core.CalendarMetadata
GO

INSERT INTO core.CalendarMetadata
	(EntityTypeCode,BackgroundColor,TextColor,Icon)
VALUES
	('Availability', '#DCDCDC', '#000000', 'fa-circle-o'),
	('DPA', '#AAD7FF', '#000000', 'fa-exclamation-triangle'),
	('DSA', '#7AC0FF', '#000000', 'fa-cutlery'),
	('Expense', '#004A8C', '#FFFFFF', 'fa-money'),
	('Project', '#128fff', '#000000', 'fa-tasks'),
	('Time', '#00396C', '#FFFFFF', 'fa-clock-o'),
	('Unavailability', '#000000', '#FFFFFF', 'fa-ban')
GO
--End table core.CalendarMetadata

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Invoice', 'Approve', 'Sent when an invoice is approved', 'An Invoice You Have Submitted Has Been Approved', '<p>The following invoice has been approved:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'DecrementWorkflow', 'Sent when an invoice is rejected', 'An Invoice You Have Submitted Has Been Rejected', '<p>The following invoice has been rejected:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Rejection Reason: &nbsp;[[InvoiceRejectionReasonName]]<br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'IncrementWorkflow', 'Sent when an invoice is forwarded in the workflow', 'A Consultant Invoice Has Been Forwarded For Your Review', '<p>[[PersonNameFormatted]] has reviewed the following invoice and is forwarding it for your approval:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'Submit', 'Sent when an invoice is submitted', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonExisting', 'Sent when an existing person is invited to DeployAdviser', 'You Have Been Invited To Affiliate With A Client In DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonImport', 'Sent when an imported person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonNew', 'Sent when a new person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'InvitePersonProject', 'Sent when a project assignment offer is made', 'A Project Assignment Invitation Has Been Made', '<p>[[ClientName]] invites you to accept the following project assignment:</p><p>Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Any', '[[SiteLink]]', 'Embedded Site Link'),
	('Any', '[[SiteURL]]', 'Site URL'),
	('Invoice', '[[ClientName]]', 'Client Name'),
	('Invoice', '[[InvoicePersonNameFormatted]]', 'Consultant Name'),
	('Invoice', '[[PersonNameFormatted]]', 'Person Name'),
	('Invoice', '[[ProjectName]]', 'Project Name'),
	('Invoice', '[[EndDateFormatted]]', 'Invoice End Date'),
	('Invoice', '[[InvoiceAmount]]', 'Invoice Amount'),
	('Invoice', '[[InvoiceDateTimeFormatted]]', 'Invoice Date'),
	('Invoice', '[[InvoiceRejectionReasonName]]', 'Rejection Reason'),
	('Invoice', '[[ISOCurrencyCode]]', 'Invoice Currency'),
	('Invoice', '[[PersonInvoiceNumber]]', 'Invoice Number'),
	('Invoice', '[[StartDateFormatted]]', 'Invoice Start Date'),
	('Invoice', '[[WorkflowComments]]', 'Comments'),
	('Person', '[[ClientName]]', 'Client Name'),
	('Person', '[[EmailAddress]]', 'Sender Email'),
	('Person', '[[PasswordTokenLink]]', 'Embedded Reset Password Link'),
	('Person', '[[PasswordTokenURL]]', 'Site Reset Password URL'),
	('Person', '[[PersonNameFormattedLong]]', 'Sender Name (Long)'),
	('Person', '[[PersonNameFormattedShort]]', 'Sender Name (Short)'),
	('Person', '[[RoleName]]', 'Role Name'),
	('Person', '[[UserName]]', 'User Name'),
	('PersonProject', '[[ClientFunctionName]]', 'Function'),
	('PersonProject', '[[ClientName]]', 'Client Name'),
	('PersonProject', '[[ProjectTermOfReferenceName]]', 'Term Of Reference'),
	('PersonProject', '[[PersonNameFormatted]]', 'Person Name'),
	('PersonProject', '[[ProjectName]]', 'Project Name')
GO
--End table core.EmailTemplateField

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'DatabaseServerDomainName', NULL, 'gamma.pridedata.com'
EXEC core.SystemSetupAddUpdate 'DatabaseServerIPV4Address', NULL, '10.10.1.102'
EXEC core.SystemSetupAddUpdate 'SSRSDomain', NULL, 'gamma.pridedata.com'
EXEC core.SystemSetupAddUpdate 'SSRSPassword', NULL, 'Sch00n3rs'
EXEC core.SystemSetupAddUpdate 'SSRSReportPath', NULL, '/DeployAdviserReporting/'
EXEC core.SystemSetupAddUpdate 'SSRSReportServerPath', NULL, 'ReportServer'
EXEC core.SystemSetupAddUpdate 'SSRSUserName', NULL, 'ssrsuser'
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

