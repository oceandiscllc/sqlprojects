USE DeployAdviser
GO

--Begin table import.Person
DECLARE @TableName VARCHAR(250) = 'import.Person'

EXEC utility.DropObject @TableName

CREATE TABLE import.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	Organization VARCHAR(250),
	HRCompassID VARCHAR(50),
	PhoenixID VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ClientID INT,
	ClientPersonRoleCodeList VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table import.Person

--Begin table core.CalendarMetadata
DECLARE @TableName VARCHAR(250) = 'core.CalendarMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE core.CalendarMetadata
	(
	CalendarMetadataID INT IDENTITY(1, 1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	BackgroundColor CHAR(7),
	TextColor CHAR(7),
	Icon VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'CalendarMetadataID'
GO
--End table core.CalendarMetadata

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropColumn @TableName, 'HasAccptedTerms'

EXEC utility.AddColumn @TableName, 'HasAcceptedTerms', 'BIT', '0'

EXEC utility.AddColumn @TableName, 'Organization', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'HRCompassID', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PhoenixID', 'VARCHAR(50)'
GO
--End table person.Person
