-- File Name:	Build - 1.4 - DEPLOYADVISER.sql
-- Build Key:	Build - 1.4 - 2017.11.13 08.16.53

--USE DeployAdviser
GO

-- ==============================================================================================================================
-- Tables:
--		core.CalendarMetadata
--		import.Person
--
-- Procedures:
--		core.GetCalendarMetadata
--		document.GetDocumentByDocumentGUID
--		invoice.GetInvoicePreviewData
--		invoice.GetInvoiceSummaryDataByInvoiceID
--		invoice.GetMissingExchangeRateData
--		invoice.InitializeInvoiceDataByInvoiceID
--		invoice.SubmitInvoice
--		person.GetCalendarEntriesByPersonID
--		person.GetPersonByPersonID
--		person.GetPersonProjectByPersonProjectID
--		person.SavePersonUnavailabilityByPersonID
--		person.ValidateLogin
--		project.GetCalendarEntriesByProjectID
--		project.GetProjectMetaDataByProjectID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE DeployAdviser
GO

--Begin table import.Person
DECLARE @TableName VARCHAR(250) = 'import.Person'

EXEC utility.DropObject @TableName

CREATE TABLE import.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	Organization VARCHAR(250),
	HRCompassID VARCHAR(50),
	PhoenixID VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ClientID INT,
	ClientPersonRoleCodeList VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table import.Person

--Begin table core.CalendarMetadata
DECLARE @TableName VARCHAR(250) = 'core.CalendarMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE core.CalendarMetadata
	(
	CalendarMetadataID INT IDENTITY(1, 1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	BackgroundColor CHAR(7),
	TextColor CHAR(7),
	Icon VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'CalendarMetadataID'
GO
--End table core.CalendarMetadata

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropColumn @TableName, 'HasAccptedTerms'

EXEC utility.AddColumn @TableName, 'HasAcceptedTerms', 'BIT', '0'

EXEC utility.AddColumn @TableName, 'Organization', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'HRCompassID', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PhoenixID', 'VARCHAR(50)'
GO
--End table person.Person

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE DeployAdviser
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE DeployAdviser
GO

--Begin procedure core.GetCalendarMetadata
EXEC utility.DropObject 'core.GetCalendarMetadata'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A stored procedure to return data from the core.CalendarMetadata table
-- ===================================================================================
CREATE PROCEDURE core.GetCalendarMetadata

@EntityTypeCode VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CMD.EntityTypeCode,
		CMD.BackgroundColor,
		CMD.TextColor,
		CMD.Icon
	FROM core.CalendarMetadata CMD
	WHERE 
		(
		@EntityTypeCode IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM core.ListToTable(@EntityTypeCode, ',') LTT
				WHERE LTT.ListItem = CMD.EntityTypeCode
				)
		)

END
GO
--End procedure core.GetCalendarMetadata

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate
	FROM person.Person PN, project.Project PJ
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
	WHERE PN.PersonID = @PersonID
		AND PJ.ProjectID = @ProjectID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		TPL.DPAAmount,
		TPL.DPAISOCurrencyCode,
		TPL.DPAExchangeRate,
		CAST(TPL.DPAAmount * TPL.DPAExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		TPL.DSAExchangeRate,
		CAST(TPL.DSAAmount * TPL.DSAExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount
	FROM @tPerDiemLog TPL
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		SUM(TPL.DPAAmount) AS DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DSAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS DPACount,
		@cFinanceCode3 AS DSACode,
		SUM(TPL.DPAAmount) AS DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS DSACount
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DPAISOCurrencyCode, TPL.DSAISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TEL.ExpenseAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TEL.TaxAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to data from the invoice.Invoice Table
-- ======================================================================
CREATE PROCEDURE invoice.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDocumentGUID VARCHAR(50) = (SELECT D.DocumentGUID FROM document.DocumentEntity DE JOIN document.Document D ON D.DocumentID = DE.DocumentID AND DE.EntityTypeCode = 'Invoice' AND DE.EntityID = @InvoiceID)

	--InvoiceSummaryData
	SELECT

		CASE
			WHEN @cDocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + @cDocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		@PersonID AS PersonID,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		person.FormatPersonNameByPersonID(@PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.InvoiceAmount,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		IRR.InvoiceRejectionReasonName,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		PN.PersonID AS InvoicePersonID,
		person.HasPermission('Main.Error.ViewCFErrors', PN.PersonID) AS HasViewCFErrorsPermission,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted,
		PN.UserName AS InvoicePersonUserName
	FROM invoice.Invoice I
		JOIN dropdown.InvoiceRejectionReason IRR ON IRR.InvoiceRejectionReasonID = I.InvoiceRejectionReasonID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

	--InvoiceWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('approve','create','decrementworkflow','incrementworkflow')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID

END
GO
--End procedure invoice.GetInvoiceSummaryDataByInvoiceID

--Begin procedure invoice.GetMissingExchangeRateData
EXEC utility.DropObject 'invoice.GetProjectISOCurrencyCodes'
EXEC utility.DropObject 'invoice.GetMissingExchangeRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to return any exchange rate data needed by an invoice but not present in the invoice.ExchangeRate table
-- =======================================================================================================================================
CREATE PROCEDURE invoice.GetMissingExchangeRateData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cISOCurrencyCodeTo CHAR(3)

	SELECT
		@cISOCurrencyCodeTo = PP.ISOCurrencyCode
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID
		AND PP.ProjectID = @ProjectID

	--MissingExchangeRateData
	SELECT
		@cISOCurrencyCodeTo AS ISOCurrencyCodeTo,
		PPE.ISOCurrencyCode	AS ISOCurrencyCodeFrom,
		invoice.GetExchangeRateDate(PPE.ExpenseDate) AS ExchangeRateDate
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			AND PPE.IsProjectExpense = 1
			AND PPE.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.ISOCurrencyCode <> @cISOCurrencyCodeTo
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
						WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
						)
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = @cISOCurrencyCodeTo
					AND ER.ISOCurrencyCodeFrom = PPE.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		@cISOCurrencyCodeTo AS ISOCurrencyCodeTo,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PP.ISOCurrencyCode <> @cISOCurrencyCodeTo
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = @cISOCurrencyCodeTo
					AND ER.ISOCurrencyCodeFrom = PP.ISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		@cISOCurrencyCodeTo AS ISOCurrencyCodeTo,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PL.DPAISOCurrencyCode <> @cISOCurrencyCodeTo
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = @cISOCurrencyCodeTo
					AND ER.ISOCurrencyCodeFrom = PL.DPAISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	UNION

	SELECT 
		@cISOCurrencyCodeTo AS ISOCurrencyCodeTo,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRateDate(PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND PP.ProjectID = @ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PL.DSAISOCurrencyCode <> @cISOCurrencyCodeTo
			AND NOT EXISTS
				(
				SELECT 1
				FROM invoice.ExchangeRate ER
				WHERE ER.ISOCurrencyCodeTo = @cISOCurrencyCodeTo
					AND ER.ISOCurrencyCodeFrom = PL.DSAISOCurrencyCode
					AND ER.IsFinalRate = 1
				)

	ORDER BY 3, 2

	--ProjectExchangeRateData
	SELECT 
		@cISOCurrencyCodeTo AS ISOCurrencyCodeTo,
		core.GetSystemSetupValueBySystemSetupKey('OANDAAPIKey', '') AS OANDAAPIKey

END
GO
--End procedure invoice.GetMissingExchangeRateData

--Begin procedure invoice.InitializeInvoiceDataByInvoiceID
EXEC utility.DropObject 'invoice.InitializeInvoiceDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to initialize invoice data for review or printing
-- =================================================================================
CREATE PROCEDURE invoice.InitializeInvoiceDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Initialize the invoice data
	DELETE IEL FROM invoice.InvoiceExpenseLog IEL WHERE IEL.InvoiceID = @InvoiceID
	DELETE IPL FROM invoice.InvoicePerDiemLog IPL WHERE IPL.InvoiceID = @InvoiceID
	DELETE ITL FROM invoice.InvoiceTimeLog ITL WHERE ITL.InvoiceID = @InvoiceID

	INSERT INTO invoice.InvoiceExpenseLog
		(InvoiceID, ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, ProjectManagerNotes)
	SELECT
		@InvoiceID,
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate, 
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO invoice.InvoicePerDiemLog
		(InvoiceID, DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO invoice.InvoiceTimeLog
		(InvoiceID, DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectManagerNotes)
	SELECT 
		@InvoiceID,
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.ExchangeRate,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

END
GO
--End procedure invoice.InitializeInvoiceDataByInvoiceID

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0,
@PersonUnavailabilityDatesToToggle VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cInvoiceISOCurrencyCode CHAR(3)
	DECLARE @nClientID INT

	SELECT 
		@cInvoiceISOCurrencyCode = I.ISOCurrencyCode,
		@nClientID = P.ClientID 
	FROM project.Project P 
		JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID 
			AND I.InvoiceID = @InvoiceID

	UPDATE PPE
	SET 
		PPE.ExchangeRate = 
			CASE
				WHEN PPE.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PPE.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPE.ExpenseDate)
			END,

		PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET 
		PPT.ExchangeRate = 
			CASE
				WHEN PP.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PP.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPT.DateWorked)
			END,

		PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	;

	EXEC person.SavePersonUnavailabilityByPersonID @PersonID, @PersonUnavailabilityDatesToToggle

	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure person.GetCalendarEntriesByPersonID
EXEC utility.DropObject 'person.GetCalendarEntriesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A stored procedure to get data for the desktop calendar
-- ====================================================================
CREATE PROCEDURE person.GetCalendarEntriesByPersonID

@PersonID INT,
@StartDate DATE,
@EndDate DATE,
@EntityTypeCode VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDates TABLE (DateValue DATE) 
	DECLARE @tEntityTypeCodes TABLE (EntityTypeCode VARCHAR(50)) 

	INSERT INTO @tDates (DateValue) SELECT D.DateValue FROM core.GetDatesFromRange(@StartDate, @EndDate) D

	SET @EntityTypeCode = core.NullIfEmpty(@EntityTypeCode)
	IF @EntityTypeCode IS NOT NULL
		INSERT INTO @tEntityTypeCodes (EntityTypeCode) SELECT LTT.ListItem FROM core.ListToTable(@EntityTypeCode, ',') LTT
	--ENDIF

	SELECT
		CMD.BackgroundColor,
		CMD.EntityTypeCode,
		CMD.Icon,
		CMD.TextColor,
		D.EndDate,
		D.StartDate,
		D.Title,
		D.URL
	FROM
		(
		SELECT
			'Availability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(T.DateValue, 'yyyy-MM-dd') AS StartDate,
			'Available' AS Title,
			NULL AS URL
		FROM @tDates T
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Availability'))
			AND NOT EXISTS
				(
				SELECT 1
				FROM person.PersonUnavailability PU
				WHERE PU.PersonID = @PersonID
					AND PU.PersonUnavailabilityDate >= @StartDate
					AND PU.PersonUnavailabilityDate <= @EndDate
					AND PU.PersonUnavailabilityDate = T.DateValue
				)

		UNION

		SELECT
			'DPA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'DPA Invoiced'
				ELSE 'DPA Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DPA'))
				AND PPT.HasDPA = 1
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'DSA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'DSA Invoiced'
				ELSE 'DSA Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DSA'))
				AND PPT.DSAAmount > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Expense' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPE.InvoiceID > 0
				THEN 'Expense Invoiced'
				ELSE 'Expense Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPEC.PersonProjectExpenseID) FROM person.PersonProjectExpense PPEC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPEC.PersonProjectID AND PPC.PersonID = @PersonID AND PPEC.ExpenseDate = PPE.ExpenseDate) > 1
				THEN '/personprojectexpense/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd')
				ELSE '/personprojectexpense/view/id/' + CAST(PPE.PersonProjectExpenseID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Expense'))
				AND PP.PersonID = @PersonID
				AND PPE.ExpenseDate >= @StartDate
				AND PPE.ExpenseDate <= @EndDate

		UNION

		SELECT
			'Project' AS EntityTypeCode,
			FORMAT(PP.EndDate, 'yyyy-MM-dd') AS EndDate,
			FORMAT(PP.StartDate, 'yyyy-MM-dd') AS StartDate,
			P.ProjectName + ' / ' + PTOR.ProjectTermOfReferenceName AS Title,
			'/personproject/view/id/' + CAST(PP.PersonProjectID AS VARCHAR(10)) AS URL
		FROM person.PersonProject PP
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Project'))
				AND PP.PersonID = @PersonID
				AND PP.StartDate <= @EndDate
				AND PP.EndDate >= @StartDate
				AND PP.AcceptedDate IS NOT NULL

		UNION

		SELECT
			'Time' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'Time Invoiced'
				ELSE 'Time Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Time'))
				AND PPT.HoursWorked > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Unavailability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS StartDate,
			'Unavailable' AS Title,
			NULL AS URL
		FROM person.PersonUnavailability PU
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Unavailability'))
			AND PU.PersonID = @PersonID
			AND PU.PersonUnavailabilityDate >= @StartDate
			AND PU.PersonUnavailabilityDate <= @EndDate

		) D 
		JOIN core.CalendarMetadata CMD ON CMD.EntityTypeCode = D.EntityTypeCode

END
GO
--End procedure person.GetCalendarEntriesByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		PPOL.ProofOfLifeAnswer,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days INT NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			'' AS AcceptedDateFormatted,
			'' AS CurrencyName,
			'' AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			'' AS ClientFunctionName,
			0 AS InsuranceTypeID,
			'' AS InsuranceTypeName,
			NULL AS EndDate,
			'' AS EndDateFormatted,
			0 AS FeeRate,
			'' AS ManagerEmailAddress,
			'' AS ManagerName,
			0 AS PersonID,
			'' AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectTermOfReferenceID,
			'' AS ProjectTermOfReferenceName,
			'' AS Status,
			NULL AS StartDate,
			'' AS StartDateFormatted,
			0 AS ProjectLaborCodeID,
			'' AS ProjectLaborCodeName,
			0 AS ProjectRoleID,
			'' AS ProjectRoleName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			P.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = P.ClientID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.GetPersonUnavailabilityByPersonID'
GO
--End procedure person.GetPersonUnavailabilityByPersonID

--Begin procedure person.SavePersonUnavailabilityByPersonID
EXEC utility.DropObject 'person.SavePersonUnavailabilityByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.30
-- Description:	A stored procedure to save data to the person.PersonUnavailability table
-- =====================================================================================
CREATE PROCEDURE person.SavePersonUnavailabilityByPersonID

@PersonID INT = 0,
@PersonUnavailabilityDatesToToggle VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonUnavailabilityDatesToToggle TABLE (DateValue VARCHAR(50), IsForDelete BIT NOT NULL DEFAULT 0) 

	SET @PersonUnavailabilityDatesToToggle = core.NullIfEmpty(@PersonUnavailabilityDatesToToggle)

	IF @PersonUnavailabilityDatesToToggle IS NOT NULL
		BEGIN

		INSERT INTO @tPersonUnavailabilityDatesToToggle 
			(DateValue, IsForDelete) 
		SELECT 
			CAST(LTT.ListItem AS DATE),
			
			CASE
				WHEN EXISTS (SELECT 1 FROM person.PersonUnavailability PU WHERE PU.PersonID = @PersonID AND PU.PersonUnavailabilityDate = CAST(LTT.ListItem AS DATE))
				THEN 1
				ELSE 0
			END

		FROM core.ListToTable(@PersonUnavailabilityDatesToToggle, ',') LTT

		DELETE PU
		FROM person.PersonUnavailability PU
			JOIN @tPersonUnavailabilityDatesToToggle PUD ON PUD.DateValue = PU.PersonUnavailabilityDate
				AND PU.PersonID = @PersonID
				AND PUD.IsForDelete = 1

		INSERT INTO person.PersonUnavailability
			(PersonID, PersonUnavailabilityDate)
		SELECT
			@PersonID,
			PUD.DateValue
		FROM @tPersonUnavailabilityDatesToToggle PUD
		WHERE PUD.IsForDelete = 0

		END
	--ENDIF

END
GO
--End procedure person.SavePersonUnavailabilityByPersonID

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure project.GetCalendarEntriesByProjectID
EXEC utility.DropObject 'project.GetCalendarEntriesByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data for project forecasts
-- =================================================================
CREATE PROCEDURE project.GetCalendarEntriesByProjectID

@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@EntityTypeCode VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityTypeCodes TABLE (EntityTypeCode VARCHAR(50)) 

	SET @EntityTypeCode = core.NullIfEmpty(@EntityTypeCode)
	IF @EntityTypeCode IS NOT NULL
		INSERT INTO @tEntityTypeCodes (EntityTypeCode) SELECT LTT.ListItem FROM core.ListToTable(@EntityTypeCode, ',') LTT
	--ENDIF

	SELECT
		CMD.BackgroundColor,
		CMD.EntityTypeCode,
		CMD.Icon,
		CMD.TextColor,
		D.EndDate,
		D.StartDate,
		D.Title,
		D.URL
	FROM
		(
		SELECT
			'Project' AS EntityTypeCode,
			FORMAT(P.EndDate, 'yyyy-MM-dd') AS EndDate,
			FORMAT(P.StartDate, 'yyyy-MM-dd') AS StartDate,
			P.ProjectName AS Title,
			'/project/view/id/' + CAST(P.ProjectID AS VARCHAR(10)) AS URL
		FROM project.Project P
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Project'))
			AND P.ProjectID = @ProjectID
			AND P.StartDate <= @EndDate
			AND P.EndDate >= @StartDate

		UNION

		SELECT
			'Unavailability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS StartDate,
			person.FormatPersonNameByPersonID(PU.PersonID, 'LastFirst') AS Title,
			NULL AS URL
		FROM person.PersonUnavailability PU
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Unavailability'))
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.ProjectID = @ProjectID
					AND PP.PersonID = PU.PersonID
				)
			AND PU.PersonUnavailabilityDate >= @StartDate
			AND PU.PersonUnavailabilityDate <= @EndDate

		) D 
		JOIN core.CalendarMetadata CMD ON CMD.EntityTypeCode = D.EntityTypeCode
	ORDER BY 2, 7, 6, 5

END
GO
--End procedure person.GetCalendarEntriesByProjectID

--Begin procedure project.GetProjectMetaDataByProjectID
EXEC utility.DropObject 'project.GetProjectMetaDataByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the project.Project table
-- ==========================================================================
CREATE PROCEDURE project.GetProjectMetaDataByProjectID

@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientName,
		P.ProjectID,
		P.ProjectCode,
		P.ProjectName
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

END
GO
--End procedure project.GetProjectMetaDataByProjectID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE DeployAdviser
GO

--Begin table core.CalendarMetadata
TRUNCATE TABLE core.CalendarMetadata
GO

INSERT INTO core.CalendarMetadata
	(EntityTypeCode,BackgroundColor,TextColor,Icon)
VALUES
	('Availability', '#DCDCDC', '#000000', 'fa-circle-o'),
	('DPA', '#AAD7FF', '#000000', 'fa-exclamation-triangle'),
	('DSA', '#7AC0FF', '#000000', 'fa-cutlery'),
	('Expense', '#004A8C', '#FFFFFF', 'fa-money'),
	('Project', '#128fff', '#000000', 'fa-tasks'),
	('Time', '#00396C', '#FFFFFF', 'fa-clock-o'),
	('Unavailability', '#000000', '#FFFFFF', 'fa-ban')
GO
--End table core.CalendarMetadata

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Invoice', 'Approve', 'Sent when an invoice is approved', 'An Invoice You Have Submitted Has Been Approved', '<p>The following invoice has been approved:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'DecrementWorkflow', 'Sent when an invoice is rejected', 'An Invoice You Have Submitted Has Been Rejected', '<p>The following invoice has been rejected:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Rejection Reason: &nbsp;[[InvoiceRejectionReasonName]]<br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'IncrementWorkflow', 'Sent when an invoice is forwarded in the workflow', 'A Consultant Invoice Has Been Forwarded For Your Review', '<p>[[PersonNameFormatted]] has reviewed the following invoice and is forwarding it for your approval:</p><p>Claimant: &nbsp;[[InvoicePersonNameFormatted]]<br />Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]<br /><br />Comments: &nbsp;[[WorkflowComments]]</p><br /><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Invoice', 'Submit', 'Sent when an invoice is submitted', 'A Consultant Invoice Has Been Submitted For Your Review', '<p>[[InvoicePersonNameFormatted]] has submitted the following invoice:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br />Invoice Amount: &nbsp;[[InvoiceAmount]] [[ISOCurrencyCode]]</p><p>You may log in to the [[SiteLink]] system to review this invoice.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonExisting', 'Sent when an existing person is invited to DeployAdviser', 'You Have Been Invited To Affiliate With A Client In DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonImport', 'Sent when an imported person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('Person', 'InvitePersonNew', 'Sent when a new person is invited to DeployAdviser', 'You Have Been Invited To Join DeployAdviser', '<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]].&nbsp; Accepting this offer will allow [[ClientName]] to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'AcceptPersonProject', 'Sent when a project assignment is accepted', 'A Project Assignment Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted the following project assignment:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'ExpirePersonProject', 'Sent when a project assignment is expired', 'A Project Assignment Has Expired', '<p>The following project assignment for [[PersonNameFormatted]] has expired:</p><br /><p>Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]<br /></p><br /><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><br /><p>Thank you,<br />The DeployAdviser Team</p>'),
	('PersonProject', 'InvitePersonProject', 'Sent when a project assignment offer is made', 'A Project Assignment Invitation Has Been Made', '<p>[[ClientName]] invites you to accept the following project assignment:</p><p>Project Name: &nbsp;[[ProjectName]]<br />Term Of Reference: &nbsp;[[ProjectTermOfReferenceName]]<br />Function: &nbsp;[[ClientFunctionName]]</p><p>You may log in to the [[SiteLink]] system to accept or reject this invitation.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[SiteURL]]/</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Any', '[[SiteLink]]', 'Embedded Site Link'),
	('Any', '[[SiteURL]]', 'Site URL'),
	('Invoice', '[[ClientName]]', 'Client Name'),
	('Invoice', '[[InvoicePersonNameFormatted]]', 'Consultant Name'),
	('Invoice', '[[PersonNameFormatted]]', 'Person Name'),
	('Invoice', '[[ProjectName]]', 'Project Name'),
	('Invoice', '[[EndDateFormatted]]', 'Invoice End Date'),
	('Invoice', '[[InvoiceAmount]]', 'Invoice Amount'),
	('Invoice', '[[InvoiceDateTimeFormatted]]', 'Invoice Date'),
	('Invoice', '[[InvoiceRejectionReasonName]]', 'Rejection Reason'),
	('Invoice', '[[ISOCurrencyCode]]', 'Invoice Currency'),
	('Invoice', '[[PersonInvoiceNumber]]', 'Invoice Number'),
	('Invoice', '[[StartDateFormatted]]', 'Invoice Start Date'),
	('Invoice', '[[WorkflowComments]]', 'Comments'),
	('Person', '[[ClientName]]', 'Client Name'),
	('Person', '[[EmailAddress]]', 'Sender Email'),
	('Person', '[[PasswordTokenLink]]', 'Embedded Reset Password Link'),
	('Person', '[[PasswordTokenURL]]', 'Site Reset Password URL'),
	('Person', '[[PersonNameFormattedLong]]', 'Sender Name (Long)'),
	('Person', '[[PersonNameFormattedShort]]', 'Sender Name (Short)'),
	('Person', '[[RoleName]]', 'Role Name'),
	('Person', '[[UserName]]', 'User Name'),
	('PersonProject', '[[ClientFunctionName]]', 'Function'),
	('PersonProject', '[[ClientName]]', 'Client Name'),
	('PersonProject', '[[ProjectTermOfReferenceName]]', 'Term Of Reference'),
	('PersonProject', '[[PersonNameFormatted]]', 'Person Name'),
	('PersonProject', '[[ProjectName]]', 'Project Name')
GO
--End table core.EmailTemplateField

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'DatabaseServerDomainName', NULL, 'gamma.pridedata.com'
EXEC core.SystemSetupAddUpdate 'DatabaseServerIPV4Address', NULL, '10.10.1.102'
EXEC core.SystemSetupAddUpdate 'SSRSDomain', NULL, 'gamma.pridedata.com'
EXEC core.SystemSetupAddUpdate 'SSRSPassword', NULL, 'Sch00n3rs'
EXEC core.SystemSetupAddUpdate 'SSRSReportPath', NULL, '/DeployAdviserReporting/'
EXEC core.SystemSetupAddUpdate 'SSRSReportServerPath', NULL, 'ReportServer'
EXEC core.SystemSetupAddUpdate 'SSRSUserName', NULL, 'ssrsuser'
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'General', 'General', 0;
EXEC permissionable.SavePermissionableGroup 'TimeExpense', 'Time &amp; Expenses', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the list of permissionabless on the user edit page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Import users from the import.Person table', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='PersonImport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.PersonImport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the availability forecast', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='UnAvailabilityUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.UnAvailabilityUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Next of Kin tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NextOfKin', @PERMISSIONCODE='NextOfKin';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the Proof of Life tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ProofOfLife', @PERMISSIONCODE='ProofOfLife';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Edit a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View the list of clients', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a client', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Access the invite a user menu item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Add / edit a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View the list of deployments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Expire a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.List.CanExpirePersonProject', @PERMISSIONCODE='CanExpirePersonProject';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='View a deployment', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='Add / edit an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View the expense log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectExpense', @DESCRIPTION='View an expense entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectExpense.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='Add / edit a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View the time log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProjectTime', @DESCRIPTION='View a time entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProjectTime.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View availability forecasts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Forecast', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.Forecast', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of projects', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View the list of invoices under review', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Show the time &amp; expense candidate records for an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCandidateRecords', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ListCandidateRecords', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Preview an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='PreviewInvoice', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.PreviewInvoice', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Setup an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Setup', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Setup', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='View an invoice summary', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='ShowSummary', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.ShowSummary', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Submit an invoice', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Submit', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.Submit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an you have submitted', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Invoice', @DESCRIPTION='Review an invoice for approval', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='TimeExpense', @PERMISSIONABLELINEAGE='Invoice.View.Review', @PERMISSIONCODE='Review';
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.4 - 2017.11.13 08.16.53')
GO
--End build tracking

