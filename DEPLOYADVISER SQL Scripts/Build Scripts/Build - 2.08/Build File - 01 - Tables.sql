USE DeployAdviser
GO

--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.AddColumn @TableName, 'HasFixedAllowance', 'BIT', '1'
EXEC utility.AddColumn @TableName, 'HasVariableAllowance', 'BIT', '1'
EXEC utility.AddColumn @TableName, 'FixedAllowanceLabel', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'VariableAllowanceLabel', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'MaximumProductiveDays', 'INT', '0'
GO
--End table client.Client

--Begin table client.ClientCustomer
DECLARE @TableName VARCHAR(250) = 'client.ClientCustomer'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientCustomer
	(
	ClientCustomerID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientCustomerName VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientCustomerID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientCustomer', 'ClientID,ClientCustomerName'
GO
--End table client.ClientCustomer

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.AddColumn @TableName, 'PayrollNumber', 'VARCHAR(50)'
GO
--End table client.ClientPerson

--Begin table client.ClientPersonProjectRole
DECLARE @TableName VARCHAR(250) = 'client.ClientPersonProjectRole'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPersonProjectRole
	(
	ClientPersonProjectRoleID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	ClientPersonProjectRoleName VARCHAR(250),
	FinanceCode VARCHAR(50),
	InvoiceRDLCode VARCHAR(50),
	HasFees BIT,
	HasFixedAllowance BIT,
	HasVariableAllowance BIT,
	HasVAT BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasFees', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasFixedAllowance', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasVariableAllowance', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasVAT', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonProjectRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPersonProjectRole', 'ClientID,ClientPersonProjectRoleName'
GO
--End table client.ClientPersonProjectRole

--Begin table client.ClientPersonProjectRoleClientTimeType
DECLARE @TableName VARCHAR(250) = 'client.ClientPersonProjectRoleClientTimeType'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPersonProjectRoleClientTimeType
	(
	ClientPersonProjectRoleClientTimeTypeID INT IDENTITY(1,1) NOT NULL,
	ClientPersonProjectRoleID INT,
	ClientTimeTypeID INT,
	LeaveDaysPerAnnum INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientPersonProjectRoleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientTimeTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LeaveDaysPerAnnum', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonProjectRoleClientTimeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPersonProjectRoleClientTimeType', 'ClientPersonProjectRoleID,ClientTimeTypeID'
GO
--End table client.ClientPersonProjectRoleClientTimeType

--Begin table client.ClientPersonProjectRoleProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'client.ClientPersonProjectRoleProjectLaborCode'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPersonProjectRoleProjectLaborCode
	(
	ClientPersonProjectRoleProjectLaborCodeID INT IDENTITY(1,1) NOT NULL,
	ClientPersonProjectRoleID INT,
	ProjectLaborCodeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientPersonProjectRoleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLaborCodeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonProjectRoleProjectLaborCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPersonProjectRoleProjectLaborCode', 'ClientPersonProjectRoleID,ProjectLaborCodeID'
GO
--End table client.ClientPersonProjectRoleClientTimeType

--Begin table client.ClientTimeType
DECLARE @TableName VARCHAR(250) = 'client.ClientTimeType'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientTimeType
	(
	ClientTimeTypeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	TimeTypeID INT,
	AccountCode VARCHAR(50),
	HasFixedAllowance BIT,
	HasLocation BIT,
	HasVariableAllowance BIT,
	IsAccruable BIT,
	IsActive BIT,
	IsBalancePayableUponTermination BIT,
	IsValidated BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasFixedAllowance', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'HasLocation', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'HasVariableAllowance', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccruable', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsBalancePayableUponTermination', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsValidated', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'TimeTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientTimeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientTimeType', 'ClientID,TimeTypeID'
GO
--End table client.ClientTimeType

--Begin table client.ProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'client.ProjectLaborCode'

EXEC utility.AddColumn @TableName, 'ProjectLaborCodeCategoryName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'AlternateProjectLaborCodeName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'BillRate', 'NUMERIC(18,2)', '0'
EXEC utility.AddColumn @TableName, 'PayRate1', 'NUMERIC(18,2)', '0'
EXEC utility.AddColumn @TableName, 'PayRate2', 'NUMERIC(18,2)', '0'
GO
--End table client.ProjectLaborCode

--Begin table dropdown.TimeType
DECLARE @TableName VARCHAR(250) = 'dropdown.TimeType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TimeType
	(
	TimeTypeID INT IDENTITY(0,1) NOT NULL,
	TimeTypeCategoryCode VARCHAR(50),
	TimeTypeCode VARCHAR(50),
	TimeTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TimeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TimeType', 'DisplayOrder,TimeTypeName'
GO

EXEC utility.DropObject 'dropdown.TR_TimeType'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.24
-- Description:	A trigger to populate the client.ClientTimeType table
-- ==================================================================
CREATE TRIGGER dropdown.TR_TimeType ON dropdown.TimeType AFTER INSERT
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO client.ClientTimeType
		(ClientID, TimeTypeID)
	SELECT
		C.ClientID,
		TT.TimeTypeID
	FROM client.Client C, dropdown.TimeType TT
	WHERE TT.TimeTypeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM client.ClientTimeType CTT
			WHERE CTT.ClientID = C.ClientID
				AND CTT.TimeTypeID = TT.TimeTypeID
			)

END
GO

ALTER TABLE dropdown.TimeType ENABLE TRIGGER TR_TimeType
GO
--End table dropdown.TimeType

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropColumn @TableName, 'DAPersonID'
EXEC utility.DropColumn @TableName, 'PayrollNumber'
GO
--End table person.Person

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.AddColumn @TableName, 'ClientPersonProjectRoleID', 'INT', '0'
EXEC utility.DropColumn @TableName, 'MaximumProductiveDays'
GO
--End table person.PersonProject

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.AddColumn @TableName, 'IsForLeaveBalance', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'TimeTypeID', 'INT', '0'
GO

EXEC utility.DropObject 'person.TR_PersonProjectTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a value for the IsLineManagerApproved field
-- =========================================================================
CREATE TRIGGER person.TR_PersonProjectTime ON person.PersonProjectTime AFTER UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	UPDATE PPT
	SET 
		PPT.IsLineManagerApproved = 0
	FROM person.PersonProjectTime PPT
		JOIN DELETED D ON D.PersonProjectTimeID = PPT.PersonProjectTimeID
			AND
				(
				D.DateWorked <> PPT.DateWorked
					OR D.HoursWorked <> PPT.HoursWorked
					OR D.ProjectManagerNotes <> PPT.ProjectManagerNotes
					OR D.ProjectLocationID <> PPT.ProjectLocationID
					OR D.TimeTypeID <> PPT.TimeTypeID
				)

END
GO

ALTER TABLE person.PersonProjectTime ENABLE TRIGGER TR_PersonProjectTime
GO
--End table person.PersonProjectTime

--Begin table person.PersonProjectTimeType
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTimeType'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonProjectTimeType
	(
	PersonProjectTimeTypeID INT IDENTITY(1,1) NOT NULL,
	PersonProjectID INT,
	ClientTimeTypeID INT,
	PersonProjectLeaveHours NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientTimeTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonProjectLeaveHours', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectTimeTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProjectTimeType', 'PersonProjectID,ClientTimeTypeID'
GO
--End table person.PersonProjectTimeType

--Begin table project.Project
EXEC utility.AddColumn 'project.Project', 'ClientCustomerID', 'INT', '0'
GO
--End table project.Project

