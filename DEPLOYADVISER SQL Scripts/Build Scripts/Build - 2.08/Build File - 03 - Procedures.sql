USE DeployAdviser
GO

--Begin cleanup
EXEC utility.DropObject 'dropdown.GetCountryNameFromISOCountryCode'
EXEC utility.DropObject 'invoice.SetInvoiceID'
EXEC utility.DropObject 'person.GetDeploymentsByPersonID'
EXEC utility.DropObject 'person.GetPersonProjects'
EXEC utility.DropObject 'project.ValidateDateWorked'
EXEC utility.DropObject 'project.ValidateExpenseDate'
EXEC utility.DropObject 'project.ValidateHoursWorked'
EXEC utility.DropObject 'reporting.GetDeploymentList'
EXEC utility.DropObject 'reporting.GetInvoiceDetailList'
GO
--End cleanup

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tClientPersonProjectRole TABLE (ClientPersonProjectRoleGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID(), ClientPersonProjectRoleID INT)

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientLegalEntities,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.FixedAllowanceLabel,
		C.HasFixedAllowance,
		C.HasVariableAllowance,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.MaximumProductiveDays,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.VariableAllowanceLabel,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientCustomer
	SELECT
		newID() AS ClientCustomerGUID,
		CC.ClientCustomerID,
		CC.ClientCustomerName,
		CC.IsActive
	FROM client.ClientCustomer CC
	WHERE CC.ClientID = @ClientID
	ORDER BY CC.ClientCustomerName, CC.ClientCustomerID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	INSERT INTO @tClientPersonProjectRole 
		(ClientPersonProjectRoleID)
	SELECT
		CPPR.ClientPersonProjectRoleID
	FROM client.ClientPersonProjectRole CPPR
	WHERE CPPR.ClientID = @ClientID
		AND CPPR.ClientPersonProjectRoleID > 0

	--ClientLeave
	EXEC client.GetClientLeave @ClientID

	--ClientPerson
	SELECT DISTINCT
		CP.PayrollNumber,
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Consultant'
	ORDER BY 3, 2

	--ClientPersonProjectRole
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPR.ClientPersonProjectRoleID,
		CPPR.ClientPersonProjectRoleName,
		CPPR.FinanceCode,
		CPPR.HasFees,
		CPPR.HasFixedAllowance,
		CPPR.HasVariableAllowance,
		CPPR.HasVAT,
		CPPR.InvoiceRDLCode,
		CPPR.IsActive
	FROM client.ClientPersonProjectRole CPPR
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPR.ClientPersonProjectRoleID
	ORDER BY CPPR.ClientPersonProjectRoleName, CPPR.ClientPersonProjectRoleID

	--ClientPersonProjectRoleClientTimeType
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPRCTT.ClientTimeTypeID,
		CPPRCTT.LeaveDaysPerAnnum
	FROM client.ClientPersonProjectRoleClientTimeType CPPRCTT
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPRCTT.ClientPersonProjectRoleID
	ORDER BY CPPRCTT.ClientPersonProjectRoleID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	IF @ClientID > 0
		BEGIN

		--ClientTimeType
		SELECT
			CTT.AccountCode,
			CTT.ClientTimeTypeID,
			CTT.HasFixedAllowance,
			CTT.HasLocation,
			CTT.HasVariableAllowance,
			CTT.IsAccruable,
			CTT.IsActive,
			CTT.IsBalancePayableUponTermination,
			CTT.IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM client.ClientTimeType CTT
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
				AND CTT.ClientID = @ClientID
				AND TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	ELSE
		BEGIN

		--ClientTimeType
		SELECT
			NULL AS AccountCode,
			TT.TimeTypeID AS ClientTimeTypeID,
			0 AS HasFixedAllowance,
			0 AS HasLocation,
			0 AS HasVariableAllowance,
			0 AS IsAccruable,
			0 AS IsActive,
			0 AS IsBalancePayableUponTermination,
			0 AS IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM dropdown.TimeType TT
		WHERE TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	--ENDIF

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetClientByPersonProjectID
EXEC utility.DropObject 'client.GetClientByPersonProjectID'
GO
--End procedure client.GetClientByPersonProjectID

--Begin procedure client.getClientDataByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'client.getClientDataByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.16
-- Description:	A stored procedure to return data from the client.Client table
-- ===========================================================================
CREATE PROCEDURE client.getClientDataByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.FixedAllowanceLabel,
		C.HasFixedAllowance,
		C.HasVariableAllowance,
		C.VariableAllowanceLabel
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.CLientID
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND 
				(
				(@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
					OR (@EntityTypeCode = 'Project' AND P.ProjectID = @EntityID)
				)

END
GO
--End procedure client.getClientDataByEntityTypeCodeAndEntityID

--Begin procedure client.GetClientLeave
EXEC utility.DropObject 'client.GetClientLeave'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.08
-- Description:	A stored procedure to return leave data based on an EntityTypeCode and an EntityID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientLeave

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectLeave
	WITH PPT AS
		(
		SELECT
			PPT.PersonProjectID,
			PPT.TimeTypeID,
			SUM(PPT.HoursWorked) AS HoursWorked
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
				AND P.ClientID = @ClientID
		GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
		)

	SELECT
		P.IsActive AS IsProjectActive,
		core.YesNoFormat(P.IsActive, NULL) AS IsProjectActiveFormatted,
		P.ProjectCode,
		P.ProjectName,
		PP.IsActive AS IsPersonProjectActive,
		core.YesNoFormat(PP.IsActive, NULL) AS IsPersonProjectActiveFormatted,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted,
		PP.PersonProjectName,
		ISNULL(PPT.HoursWorked, 0) AS TimeTaken,
		PPTT.PersonProjectLeaveHours,
		PPTT.PersonProjectLeaveHours - ISNULL(PPT.HoursWorked, 0) AS TimeRemaining,
		TT.TimeTypeName
	FROM person.PersonProjectTimeType PPTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = PPTT.ClientTimeTypeID
		JOIN client.Client C ON C.ClientID = CTT.ClientID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPTT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
		LEFT JOIN PPT ON PPT.TimeTypeID = TT.TimeTypeID
			AND PPTT.PersonProjectID = PPT.PersonProjectID
	WHERE P.ClientID = @ClientID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectName, PP.PersonProjectID, TT.TimeTypeName, TT.TimeTypeID

END
GO
--End procedure client.GetClientLeave

--Begin procedure dropdown.GetTimeTypeData
EXEC Utility.DropObject 'dropdown.GetTimeTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the dropdown.TimeType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetTimeTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TimeTypeID,
		T.TimeTypeCategoryCode,
		T.TimeTypeCode,
		T.TimeTypeName
	FROM dropdown.TimeType T
	WHERE (T.TimeTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TimeTypeName, T.TimeTypeID

END
GO
--End procedure dropdown.GetTimeTypeData

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.23
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cClientCostCodes VARCHAR(MAX) = ''
		
		SELECT @cClientCostCodes = COALESCE(@cClientCostCodes, '') + D.ClientCostCode
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientCostCode'), ELEMENTS) AS ClientCostCode
			FROM client.ClientCostCode T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientFunctions VARCHAR(MAX) = ''
		
		SELECT @cClientFunctions = COALESCE(@cClientFunctions, '') + D.ClientFunction
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientFunction'), ELEMENTS) AS ClientFunction
			FROM client.ClientFunction T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientPersonProjectRoles VARCHAR(MAX) = ''
		
		SELECT @cClientPersonProjectRoles = COALESCE(@cClientPersonProjectRoles, '') + D.ClientPersonProjectRole
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientPersonProjectRole'), ELEMENTS) AS ClientPersonProjectRole
			FROM client.ClientPersonProjectRole T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cClientPersonProjectRoleClientTimeTypes VARCHAR(MAX) = ''
		
		SELECT @cClientPersonProjectRoleClientTimeTypes = COALESCE(@cClientPersonProjectRoleClientTimeTypes, '') + D.ClientPersonProjectRoleClientTimeType
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientPersonProjectRoleClientTimeType'), ELEMENTS) AS ClientPersonProjectRoleClientTimeType
			FROM client.ClientPersonProjectRoleClientTimeType T
				JOIN client.ClientPersonProjectRole CPPR ON CPPR.ClientPersonProjectRoleID = T.ClientPersonProjectRoleID
					AND CPPR.ClientID = @EntityID
			) D

		DECLARE @cClientTimeTypes VARCHAR(MAX) = ''
		
		SELECT @cClientTimeTypes = COALESCE(@cClientTimeTypes, '') + D.ClientTimeType
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientTimeType'), ELEMENTS) AS ClientTimeType
			FROM client.ClientTimeType T 
			WHERE T.ClientID = @EntityID
			) D

		DECLARE @cProjectInvoiceTos VARCHAR(MAX) = ''
		
		SELECT @cProjectInvoiceTos = COALESCE(@cProjectInvoiceTos, '') + D.ProjectInvoiceTo
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectInvoiceTo'), ELEMENTS) AS ProjectInvoiceTo
			FROM client.ProjectInvoiceTo T 
			WHERE T.ClientID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<CostCodes>' + ISNULL(@cClientCostCodes, '') + '</CostCodes>' AS XML),
			CAST('<Functions>' + ISNULL(@cClientFunctions, '') + '</Functions>' AS XML),
			CAST('<PersonProjectRoles>' + ISNULL(@cClientPersonProjectRoles, '') + '</PersonProjectRoles>' AS XML),
			CAST('<PersonProjectRoleTimeTypes>' + ISNULL(@cClientPersonProjectRoleClientTimeTypes, '') + '</PersonProjectRoleTimeTypes>' AS XML),
			CAST('<TimeTypes>' + ISNULL(@cClientTimeTypes, '') + '</TimeTypes>' AS XML),
			CAST('<ProjectInvoiceTos>' + ISNULL(@cProjectInvoiceTos, '') + '</ProjectInvoiceTos>' AS XML)
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		(SELECT TOP 1 CP.PayrollNumber FROM client.ClientPerson CP WHERE CP.ClientID = PJ.ClientID AND CP.PersonID = I.PersonID ORDER BY CP.ClientPersonID) AS PayrollNumber,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.LineManagerPIN,
		I.Notes,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode2 AS PerDiemCode,
		C.FixedAllowanceLabel AS PerdiemType,
		PL.DPAAmount * PPT.HasDPA AS Amount,
		PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DPAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	UNION

	SELECT 
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode3 AS PerDiemCode,
		C.VariableAllowanceLabel AS PerdiemType,
		PPT.DSAAmount AS Amount,
		PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DSAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	ORDER BY 1, 3, 2, 9

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLogSummary
	WITH IPL AS 
		(
		SELECT
			PL.ProjectLocationName,
			C.FinanceCode2 AS DPACode,
			C.FixedAllowanceLabel AS DPALabel,
			PL.DPAAmount * PPT.HasDPA AS DPAAmount,
			PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS DPAVAT,
			PL.DPAISOCurrencyCode,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceDPAAmount,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDPAVAT,
			CAST(PPT.HasDPA AS INT) AS HasDPA,
			C.FinanceCode3 AS DSACode,
			C.VariableAllowanceLabel AS DSALabel,
			PPT.DSAAmount,
			PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS DSAVAT,
			PL.DSAISOCurrencyCode,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceDSAAmount,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDSAVAT,
			CASE WHEN PPT.DSAAmount > 0 THEN 1 ELSE 0 END AS HasDSA,
			TT.TimeTypeName
		FROM person.PersonProjectTime PPT
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
	    JOIN person.Person PN ON PN.PersonID = I.PersonID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	SELECT
		IPL.ProjectLocationName,
		IPL.TimeTypeName,
		IPL.DPACode AS PerDiemCode,
		IPL.DPALabel AS PerdiemType,
		SUM(IPL.HasDPA) AS Count,
		SUM(IPL.DPAAmount) AS Amount,
		SUM(IPL.DPAVAT) AS VAT,
		IPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(IPL.InvoiceDPAAmount) AS InvoiceAmount,
		SUM(IPL.InvoiceDPAVAT) AS InvoiceVAT,
		SUM(IPL.InvoiceDPAAmount) + SUM(IPL.InvoiceDPAVAT) AS InvoiceTotal
	FROM IPL
	GROUP BY IPL.TimeTypeName, IPL.ProjectLocationName, IPL.DPACode, IPL.DPALabel, IPL.DPAISOCurrencyCode

	UNION

	SELECT
		IPL.ProjectLocationName,
		IPL.TimeTypeName,
		IPL.DSACode AS PerDiemCode,
		IPL.DSALabel AS PerdiemType,
		SUM(IPL.HasDSA) AS Count,
		SUM(IPL.DSAAmount) AS Amount,
		SUM(IPL.DSAVAT) AS VAT,
		IPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(IPL.InvoiceDSAAmount) AS InvoiceAmount,
		SUM(IPL.InvoiceDSAVAT) AS InvoiceVAT,
		SUM(IPL.InvoiceDSAAmount) + SUM(IPL.InvoiceDSAVAT) AS InvoiceTotal
	FROM IPL
	GROUP BY IPL.TimeTypeName, IPL.ProjectLocationName, IPL.DSACode, IPL.DSALabel, IPL.DSAISOCurrencyCode

	ORDER BY 2, 1, 3, 4

END
GO
--End procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX) = NULL,
@PersonProjectTimeIDExclusionList VARCHAR(MAX) = NULL,
@InvoiceISOCurrencyCode CHAR(3),
@PayoutLeaveBalance BIT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @cFixedAllowanceLabel VARCHAR(250)
	DECLARE @cVariableAllowanceLabel VARCHAR(250)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(350),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		TimeTypeName VARCHAR(50),
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tPersonProjectLeave TABLE 
		(
		DisplayOrder INT,
		PersonProjectID INT,
		PersonProjectName VARCHAR(250), 
		TimeTypeID INT,
		TimeTypeName VARCHAR(50), 
		IsBalancePayableUponTermination BIT, 
		IsBalancePayableUponTerminationFormatted VARCHAR(5), 
		PersonProjectLeaveHours NUMERIC(18,2), 
		TimeTaken NUMERIC(18,2), 
		TimeRemaining NUMERIC(18,2)
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		TimeTypeName VARCHAR(50),
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		AccountCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	IF @PayoutLeaveBalance = 1
		BEGIN
	
		--PersonProjectLeave
		INSERT INTO @tPersonProjectLeave
			(DisplayOrder, IsBalancePayableUponTermination, IsBalancePayableUponTerminationFormatted, PersonProjectID, PersonProjectName, TimeTaken, PersonProjectLeaveHours, TimeRemaining, TimeTypeID, TimeTypeName)
		EXEC person.GetPersonProjectLeave 'Project', @ProjectID, @PersonID

		INSERT INTO person.PersonProjectTime
			(PersonProjectID, DateWorked, HoursWorked, OwnNotes, ProjectManagerNotes, TimeTypeID, IsForLeaveBalance)
		SELECT
			T.PersonProjectID,
			getDate(),
			T.TimeRemaining,
			LEFT(T.TimeTypeName + ' Balance Payout For ' + T.PersonProjectName, 250),
			LEFT(T.TimeTypeName + ' Balance Payout For ' + T.PersonProjectName, 250),
			T.TimeTypeID,
			1
		FROM @tPersonProjectLeave T
		WHERE IsBalancePayableUponTermination = 1
		ORDER BY T.PersonProjectName, T.PersonProjectID, T.TimeTypeName, T.TimeTypeID

		END
	--ENDIF

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate OR PPT.IsForLeaveBalance = 1)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate OR PPT.IsForLeaveBalance = 1)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3,
		@cFixedAllowanceLabel = ISNULL(C.FixedAllowanceLabel, 'DPA'),
		@cVariableAllowanceLabel = ISNULL(C.VariableAllowanceLabel, 'DSA')
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription AS ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, TimeTypeName, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate, ApplyVAT)
	SELECT 
		PPT.DateWorked,
		TT.TimeTypeName,
		PL.ProjectLocationName,
		PL.DPAAmount * PPT.HasDPA AS DPAAmount,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.ApplyVAT
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, TimeTypeName, ProjectLocationName, HoursPerDay, HoursWorked, ApplyVAT, FeeRate, ISOCurrencyCode, ExchangeRate, AccountCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		TT.TimeTypeName,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PPT.ApplyVAT,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),

		CASE
			WHEN TT.TimeTypeCode = 'PROD'
			THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
			ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
		END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), ''),

		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, TT.TimeTypeName, PL.ProjectLocationName, PPT.PersonProjectTimeID

	IF @EndDate IS NULL
		BEGIN

		SELECT TOP 1 @EndDate = D.EndDate
		FROM
			(
			SELECT PPE.ExpenseDate AS EndDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS EndDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.EndDate DESC

		END
	--ENDIF

	IF @StartDate IS NULL
		BEGIN

		SELECT TOP 1 @StartDate = D.StartDate
		FROM
			(
			SELECT PPE.ExpenseDate AS StartDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS StartDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.StartDate

		END
	--ENDIF

	--InvoiceData
	SELECT
		NULL AS InvoiceStatus,

		CASE
			WHEN PN.BillAddress1 IS NULL 
				OR PN.BillISOCountryCode2 IS NULL 
				OR PN.BillMunicipality IS NULL 
				OR PN.ContractingTypeID = 0 
				OR 
					(
					(SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company' 
						AND PN.SendInvoicesFrom IS NULL 
						AND PN.OwnCompanyName IS NULL
					)
			THEN 1
			ELSE 0
		END AS IsAddressUpdateRequired,

		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PINT.PersonInvoiceNumberTypeCode,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.CellPhone,
		PN.ContractingTypeID,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		PN.OwnCompanyName,
		PN.SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		PN.PersonInvoiceNumberIncrement,
		PN.PersonInvoiceNumberPrefix,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID,
		PN.TaxRate
	FROM project.Project PJ
	CROSS JOIN person.Person PN
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND PJ.ProjectID = @ProjectID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = PN.PersonInvoiceNumberTypeID
			AND PN.PersonID = @PersonID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDateWithDay(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoiceExpenseLogSummary
	;
	WITH IEL AS 
		(
		SELECT
			TEL.ClientCostCodeName,
			SUM(TEL.ExchangeRate * TEL.ExpenseAmount) AS ExpenseAmount,
			SUM(TEL.ExchangeRate * TEL.TaxAmount) AS TaxAmount,
			COUNT(DISTINCT TEL.ExpenseDate) AS DaysCount
		FROM @tExpenseLog TEL
		GROUP BY TEL.ClientCostCodeName, TEL.ExpenseDate
		)

	SELECT
		IEL.ClientCostCodeName,
		CAST(SUM(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS ExpenseAmount,
		CAST(SUM(IEL.TaxAmount) AS NUMERIC(18,2)) AS TaxAmount,
		SUM(IEL.DaysCount) AS DaysCount,
		CAST(MIN(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MinExpenseAmount,
		CAST(MAX(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MaxExpenseAmount,
		CAST(SUM(IEL.ExpenseAmount) / SUM(IEL.DaysCount) AS NUMERIC(18,2)) AS AvgExpenseAmount
	FROM IEL
	GROUP BY IEL.ClientCostCodeName, IEL.DaysCount
	ORDER BY 1

	--InvoicePerDiemLog
	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		TPL.DPAAmount AS Amount,
		TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DPAExchangeRate AS ExchangeRate,
		TPL.DPAAmount * TPL.DPAExchangeRate AS InvoiceAmount,
		TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DPAAmount * TPL.DPAExchangeRate) + (TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	UNION

	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		TPL.DSAAmount AS Amount,
		TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DSAExchangeRate AS ExchangeRate,
		TPL.DSAAmount * TPL.DSAExchangeRate AS InvoiceAmount,
		TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DSAAmount * TPL.DSAExchangeRate) + (TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	ORDER BY 1, 3, 2, 8

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DPAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DPAAmount) AS Amount,
		SUM(TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) + SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.TimeTypeName, TPL.ProjectLocationName, TPL.DPAISOCurrencyCode

	UNION

	SELECT 
		TPL.TimeTypeName,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DSAAmount) AS Amount,
		SUM(TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) + SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.TimeTypeName, TPL.ProjectLocationName, TPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 3

	--InvoiceTimeLog
	SELECT 
		TTL.AccountCode,
		TTL.TimeLogID,
		core.FormatDateWithDay(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.TimeTypeName,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.TimeTypeName,
		TTL.AccountCode,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.TimeTypeName, TTL.ProjectLocationName, TTL.AccountCode, TTL.FeeRate
	ORDER BY TTL.TimeTypeName, TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS InvoiceAmount,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			@cFixedAllowanceLabel AS DisplayText,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			@cVariableAllowanceLabel AS DisplayText,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(TEL.ExpenseAmount * TEL.ExchangeRate) AS InvoiceAmount,
			SUM(TEL.TaxAmount * TEL.ExchangeRate) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.IsDefault
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get data from the invoice.Invoice Table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDocumentGUID VARCHAR(50) = (SELECT TOP 1 D.DocumentGUID FROM document.DocumentEntity DE JOIN document.Document D ON D.DocumentID = DE.DocumentID AND DE.EntityTypeCode = 'Invoice' AND DE.EntityID = @InvoiceID ORDER BY DE.DocumentEntityID DESC)

	--InvoiceSummaryData
	SELECT

		CASE
			WHEN @cDocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + @cDocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		@PersonID AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'TitleFirstLast') AS PersonNameFormatted,
		C.ClientName,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.InvoiceAmount,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		I.LineManagerPIN,
		I.LineManagerToken,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		IRR.InvoiceRejectionReasonName,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		PN.PersonID AS InvoicePersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted,
		person.HasPermission('Main.Error.ViewCFErrors', PN.PersonID) AS HasViewCFErrorsPermission,
		PN.UserName AS InvoicePersonUserName,

		CASE
			WHEN EXISTS 
				(
				SELECT 1 
				FROM client.ClientPersonProjectRole CPPR 
					JOIN person.PersonProject PP ON PP.ClientPersonProjectRoleID = CPPR.ClientPersonProjectRoleID 
						AND PP.PersonID = I.PersonID 
						AND PP.ProjectID = I.ProjectID 
						AND CPPR.InvoiceRDLCode = 'Internal'
				)
			THEN 1
			ELSE 0
		END AS IsInternalInvoice

	FROM invoice.Invoice I
		JOIN dropdown.InvoiceRejectionReason IRR ON IRR.InvoiceRejectionReasonID = I.InvoiceRejectionReasonID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceSummaryDataByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLog
	SELECT
		ROW_NUMBER() OVER (ORDER BY PPT.DateWorked, PPT.PersonProjectTimeID) AS NoteIndex,

		CASE
			WHEN TT.TimeTypeCode = 'PROD'
			THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
			ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
		END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), '') AS AccountCode,

		I.PersonID,
		P.ProjectID,
		PL.ProjectLocationName,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.DateWorked,
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.ExchangeRate,
		PPT.HoursWorked,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND PPT.ProjectManagerNotes IS NOT NULL

	UNION

	SELECT
		NULL AS NoteIndex,

		CASE
			WHEN TT.TimeTypeCode = 'PROD'
			THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
			ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
		END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), '') AS AccountCode,

		I.PersonID,
		P.ProjectID,
		PL.ProjectLocationName,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		PPT.DateWorked,
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.ExchangeRate,
		PPT.HoursWorked,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND PPT.ProjectManagerNotes IS NULL

	ORDER BY PPT.DateWorked, PPT.PersonProjectTimeID

END
GO
--End procedure invoice.GetInvoiceTimeLogByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogNotesByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogNotesByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.13
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogNotesByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTimeLogNotes
	SELECT
		ROW_NUMBER() OVER (ORDER BY PPT.DateWorked, PPT.PersonProjectTimeID) AS NoteIndex,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
	WHERE PPT.ProjectManagerNotes IS NOT NULL
		AND PPT.InvoiceID = @InvoiceID
	ORDER BY PPT.DateWorked, PPT.PersonProjectTimeID

END
GO
--End procedure invoice.GetInvoiceTimeLogNotesByInvoiceID

--Begin procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTimeLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoiceTimeLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH ITL AS 
		(
		SELECT 
			TT.TimeTypeName,
			PL.ProjectLocationName,

			CASE
				WHEN TT.TimeTypeCode = 'PROD'
				THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
				ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
			END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), '') AS AccountCode,

			PP.FeeRate,
			PPT.HoursWorked,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS Amount,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS VAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	--InvoiceTimeLogSummary
	SELECT 
		ITL.TimeTypeName,
		ITL.ProjectLocationName,
		ITL.AccountCode,
		ITL.FeeRate,
		SUM(ITL.HoursWorked) AS HoursWorked,
		CAST(SUM(ITL.Amount) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(ITL.VAT) AS NUMERIC(18,2)) AS VAT
	FROM ITL
	GROUP BY ITL.TimeTypeName, ITL.ProjectLocationName, ITL.AccountCode, ITL.FeeRate
	ORDER BY ITL.TimeTypeName, ITL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoiceTimeLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoiceTotalsByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTotalsByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from varions invoice.Invoice% tables
-- ================================================================================
CREATE PROCEDURE invoice.GetInvoiceTotalsByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			ISNULL(SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate), 0) AS InvoiceAmount,
			ISNULL(SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * (I.TaxRate / 100) * PPT.ApplyVAT), 0) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID

		UNION

		SELECT 
			2 AS DisplayOrder,
			ISNULL(SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)), 0) AS InvoiceAmount,
			ISNULL(SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT), 0) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID

		UNION

		SELECT 
			3 AS DisplayOrder,
			ISNULL(SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)), 0) AS InvoiceAmount,
			ISNULL(SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT), 0) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID

		UNION

		SELECT 
			4 AS DisplayOrder,
			ISNULL(CAST(SUM(PPE.ExpenseAmount * PPE.ExchangeRate) AS NUMERIC(18,2)), 0) AS InvoiceAmount,
			ISNULL(CAST(SUM(PPE.TaxAmount * PPE.ExchangeRate) AS NUMERIC(18,2)), 0) AS InvoiceVAT
		FROM person.PersonProjectExpense PPE
		WHERE PPE.InvoiceID = @InvoiceID
		)

	SELECT
		INS.DisplayOrder,
		
		CASE INS.DisplayOrder
			WHEN 1
			THEN 'Fees'
			WHEN 2
			THEN (SELECT C.FixedAllowanceLabel FROM invoice.Invoice I JOIN project.Project P ON P.ProjectID = I.ProjectID JOIN client.Client C ON C.ClientID = P.ClientID AND I.InvoiceID = @InvoiceID)
			WHEN 3
			THEN (SELECT C.VariableAllowanceLabel FROM invoice.Invoice I JOIN project.Project P ON P.ProjectID = I.ProjectID JOIN client.Client C ON C.ClientID = P.ClientID AND I.InvoiceID = @InvoiceID)
			WHEN 4
			THEN 'Expenses'
		END AS DisplayText,

		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

END
GO
--End procedure invoice.GetInvoiceTotalsByInvoiceID


--Begin procedure person.CreatePersonProjectLeaveHours
EXEC utility.DropObject 'person.CreatePersonProjectLeaveHours'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.10
-- Description:	A stored procedure to create the leave balances for a person.PersonProject record
-- ==============================================================================================
CREATE PROCEDURE person.CreatePersonProjectLeaveHours

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (PersonProjectID INT, ClientTimeTypeID INT, PersonProjectLeaveTime NUMERIC(18,2))

	INSERT INTO @tTable
		(PersonProjectID, ClientTimeTypeID, PersonProjectLeaveTime)
	SELECT
		PP.PersonProjectID,
		CPPRCTT.ClientTimeTypeID,
		CAST(CPPRCTT.LeaveDaysPerAnnum AS NUMERIC(18,2)) / CAST(C.MaximumProductiveDays AS NUMERIC(18,2)) * CAST((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PP.PersonProjectID) AS NUMERIC(18,2)) * CAST(P.HoursPerDay AS NUMERIC(18,2))
	FROM client.ClientPersonProjectRoleClientTimeType CPPRCTT
		JOIN person.PersonProject PP ON PP.ClientPersonProjectRoleID = CPPRCTT.ClientPersonProjectRoleID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND PP.PersonProjectID = @PersonProjectID

	INSERT INTO person.PersonProjectTimeType
		(PersonProjectID, ClientTimeTypeID, PersonProjectLeaveHours)
	SELECT 
		T.PersonProjectID,
		T.ClientTimeTypeID,
		FLOOR(T.PersonProjectLeaveTime)
			+ CASE 
					WHEN ROUND(T.PersonProjectLeaveTime, 2) % 1 < .25
					THEN 0
					WHEN ROUND(T.PersonProjectLeaveTime, 2) % 1 < .5
					THEN .25
					WHEN ROUND(T.PersonProjectLeaveTime, 2) % 1 < .75
					THEN .5
					ELSE .75
				END
	FROM @tTable T

END
GO
--End procedure person.CreatePersonProjectLeaveHours

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant') THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.NationalInsuranceNumber,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProjectLeave
	EXEC person.GetPersonProjectLeave 'Person', @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--ClientPersonProjectRole
		SELECT
			CPPR.ClientPersonProjectRoleID,
			CPPR.ClientPersonProjectRoleName,
			CPPR.HasFees
		FROM client.ClientPersonProjectRole CPPR
			JOIN project.Project P ON P.ClientID = CPPR.ClientID
				AND P.ProjectID = @ProjectID
				AND CPPR.IsActive = 1
		ORDER BY CPPR.ClientPersonProjectRoleName, CPPR.ClientPersonProjectRoleID

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			NULL AS AcceptedDateFormatted,
			NULL AS AlternateManagerEmailAddress,
			NULL AS AlternateManagerName,
			0 AS ClientFunctionID,
			NULL AS ClientFunctionName,
			0 AS ClientPersonProjectRoleID,
			NULL AS CurrencyName,
			1 AS HasFees,
			1 AS IsActive,
			0 AS IsAmendment,
			NULL AS ISOCurrencyCode,
			0 AS InsuranceTypeID,
			NULL AS InsuranceTypeName,
			NULL AS EndDate,
			NULL AS EndDateFormatted,
			0 AS FeeRate,
			NULL AS ManagerEmailAddress,
			NULL AS ManagerName,
			NULL AS Notes,
			NULL AS ProjectCode,
			NULL AS ProjectLaborCode,
			0 AS ProjectLaborCodeID,
			NULL AS ProjectLaborCodeName,
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			0 AS PersonProjectID,
			NULL AS ProjectRoleName,
			NULL AS ProjectTermOfReferenceCode,
			0 AS ProjectTermOfReferenceID,
			NULL AS ProjectTermOfReferenceName,
			NULL AS Status,
			NULL AS StartDate,
			NULL AS StartDateFormatted,
			NULL AS UserName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--ClientPersonProjectRole
		SELECT
			CPPR.ClientPersonProjectRoleID,
			CPPR.ClientPersonProjectRoleName,
			CPPR.HasFees
		FROM client.ClientPersonProjectRole CPPR
			JOIN project.Project P ON P.ClientID = CPPR.ClientID
			JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
				AND PP.PersonProjectID = @PersonProjectID
				AND CPPR.IsActive = 1
		ORDER BY CPPR.ClientPersonProjectRoleName, CPPR.ClientPersonProjectRoleID

		--PersonProject
		SELECT
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			CPPR.ClientPersonProjectRoleID,
			CPPR.ClientPersonProjectRoleName,
			CPPR.HasFees,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			PJ.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = PJ.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			PJ.HoursPerDay,
			PJ.ProjectCode,
			PJ.ProjectID,
			PJ.ProjectName,
			person.FormatPersonNameByPersonID(PN.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PN.UserName,
			PLC.ProjectLaborCode,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			CASE WHEN PP.AcceptedDate IS NOT NULL THEN 1 ELSE 0 END AS IsAmendment,
			PP.AlternateManagerEmailAddress,
			PP.AlternateManagerName,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.IsActive,
			PP.ISOCurrencyCode,
			dropdown.GetCurrencyNameFromISOCurrencyCode(PP.ISOCurrencyCode) AS CurrencyName,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.Notes,
			PP.PersonID,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PTOR.ProjectTermOfReferenceCode,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = PJ.ClientID
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ClientPersonProjectRole CPPR ON CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PersonProject' 
			AND DE.EntityID = @PersonProjectID
	ORDER BY D.DocumentTitle, D.DocumentID

	--PersonProjectLeave
	EXEC person.GetPersonProjectLeave 'PersonProject', @PersonProjectID

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.GetPersonProjectLeave
EXEC utility.DropObject 'person.GetPersonProjectLeave'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.08
-- Description:	A stored procedure to return leave data based on an EntityTypeCode and an EntityID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonProjectLeave

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT = 0,
@IncludeProductiveTime BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectLeave
	WITH PPT AS
		(
		SELECT
			PPT.PersonProjectID,
			PPT.TimeTypeID,
			SUM(PPT.HoursWorked) AS HoursWorked
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND 
					(
						(@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
							OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
							OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
					)
		GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
		)

	--WARNING!!! invoice.GetInvoicePreviewData relies on this specific schema and column order.  Changes here must be reflected there.
	SELECT
		1 AS DisplayOrder,
		CTT.IsBalancePayableUponTermination,
		core.YesNoFormat(CTT.IsBalancePayableUponTermination, NULL) AS IsBalancePayableUponTerminationFormatted,
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PPT.HoursWorked, 0) AS TimeTaken,
		PPTT.PersonProjectLeaveHours,
		PPTT.PersonProjectLeaveHours - ISNULL(PPT.HoursWorked, 0) AS TimeRemaining,
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM person.PersonProjectTimeType PPTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = PPTT.ClientTimeTypeID
		JOIN client.Client C ON C.ClientID = CTT.ClientID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPTT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
		LEFT JOIN PPT ON PPT.TimeTypeID = TT.TimeTypeID
			AND PPTT.PersonProjectID = PPT.PersonProjectID
	WHERE
		(
			(@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID AND PP.IsActive = 1)
				OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
				OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
		)

	UNION

	SELECT
		0 AS DisplayOrder,
		0,
		'',
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PPT.HoursWorked, 0) AS TimeTaken,
		CAST(ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PPT.PersonProjectID), 0) * P.HoursPerDay AS NUMERIC(18,2)),
		CAST((ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PPT.PersonProjectID), 0) * P.HoursPerDay) - ISNULL(PPT.HoursWorked, 0) AS NUMERIC(18,2)),
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
			AND TT.TimeTypeCode = 'PROD'
			AND @IncludeProductiveTime = 1

	ORDER BY 1, CTT.IsBalancePayableUponTermination, TT.TimeTypeName, TT.TimeTypeID, PP.PersonProjectName, PP.PersonProjectID

END
GO
--End procedure person.GetPersonProjectLeave

--Begin procedure person.GetPersonProjectTimeByPersonProjectTimeID
EXEC utility.DropObject 'person.GetPersonProjectTimeByPersonProjectTimeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.18
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonProjectTimeID
-- =====================================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeByPersonProjectTimeID

@PersonProjectTimeID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonID INT
	DECLARE @nPersonProjectID INT

	SELECT
		@nPersonID = PP.PersonID,
		@nPersonProjectID = PP.PersonProjectID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

	SET @nPersonID = ISNULL(@nPersonID, @PersonID)

	--ClientData
	EXEC client.getClientDataByEntityTypeCodeAndEntityID 'PersonProject', @nPersonProjectID

	--PersonProject
	EXEC person.GetPersonProjectsByPersonID @nPersonID

	--PersonProjectLocation
	EXEC person.GetPersonProjectLocationsByPersonProjectID @nPersonProjectID

	--PersonProjectTime
	SELECT
		C.FixedAllowanceLabel,
		C.VariableAllowanceLabel,
		CF.ClientFunctionName,
		P.HoursPerDay,
		P.IsOvertimeAllowed,
		P.ProjectName,
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
		PPT.ApplyVat,
		PPT.DateWorked,
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PPT.DSAAmount,
		PPT.HasDPA,
		PPT.HoursWorked,
		PPT.InvoiceID,
		PPT.OwnNotes,
		PPT.PersonProjectID,
		PPT.ProjectLocationID,
		(SELECT PL.DSACeiling FROM project.ProjectLocation PL WHERE PL.ProjectLocationID = PPT.ProjectLocationID) AS DSACeiling,
		(SELECT PL.ProjectLocationName FROM project.ProjectLocation PL WHERE PL.ProjectLocationID = PPT.ProjectLocationID) AS ProjectLocationName,
		PPT.PersonProjectTimeID,
		PPT.ProjectManagerNotes,
		PTOR.ProjectTermOfReferenceName,
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.PersonProjectTimeID = @PersonProjectTimeID

	--PersonProjectTimeType
	EXEC person.GetPersonProjectTimeTypesByPersonProjectID @nPersonProjectID	

END
GO
--End procedure person.GetPersonProjectTimeByPersonProjectTimeID

--Begin procedure person.GetPersonProjectTimeTypesByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectTimeTypesByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.01
-- Description:	A stored procedure to return time type data based on a PersonProjectID
-- ===================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeTypesByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CTT.HasLocation,
		CTT.HasFixedAllowance,
		CTT.HasVariableAllowance,
		CTT.IsValidated,
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM client.ClientPersonProjectRoleClientTimeType CPPRCTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = CPPRCTT.ClientTimeTypeID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
		JOIN person.PersonProject PP ON PP.ClientPersonProjectRoleID = CPPRCTT.ClientPersonProjectRoleID
			AND PP.PersonProjectID = @PersonProjectID

	UNION

	SELECT 
		1 AS HasLocation,
		1 AS HasFixedAllowance,
		1 AS HasVariableAllowance,
		1 AS IsValidated,
		TT.TimeTypeID, 
		TT.TimeTypeName
	FROM dropdown.TimeType TT
	WHERE TT.TimeTypeCategoryCode = 'Productive'

	ORDER BY 2, 1

END
GO
--End procedure person.GetPersonProjectTimeTypesByPersonProjectID

--Begin procedure person.GetPersonUsernameByEmailAddress
EXEC utility.DropObject 'person.GetPersonUsernameByEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.04.07
-- Description:	A stored procedure to return a username from the person.Person table based on an email address
-- ===========================================================================================================
CREATE PROCEDURE person.GetPersonUsernameByEmailAddress

@EmailAddress VARCHAR(320)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.UserName,
		P.EmailAddress
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

END
GO
--End procedure person.GetPersonUsernameByEmailAddress

--Begin procedure person.GetTimeEntryDataByPersonProjectID
EXEC utility.DropObject 'person.GetTimeEntryDataByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.01
-- Description:	A stored procedure to return time entry data based on a PersonProjectID
-- ====================================================================================
CREATE PROCEDURE person.GetTimeEntryDataByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	EXEC client.getClientDataByEntityTypeCodeAndEntityID 'PersonProject', @PersonProjectID
	EXEC person.GetPersonProjectLocationsByPersonProjectID @PersonProjectID
	EXEC person.GetPersonProjectTimeTypesByPersonProjectID @PersonProjectID	

END
GO
--End procedure person.GetTimeEntryDataByPersonProjectID	

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND core.NullIfEmpty(P.SummaryBiography) IS NULL THEN ',SummaryBiography' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.EntityTypeCode = 'Person' AND DE.EntityTypeSubCode = 'CV' AND DE.EntityID = P.PersonID) THEN ',CV' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END
	SET @cRequiredProfileUpdate = CASE WHEN LEFT(@cRequiredProfileUpdate, 1) = ',' THEN STUFF(@cRequiredProfileUpdate, 1, 1, '') ELSE @cRequiredProfileUpdate END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		C.FixedAllowanceLabel,
		C.VariableAllowanceLabel,
		P.ClientCustomerID,
		(SELECT CC.ClientCustomerName FROM client.ClientCustomer CC WHERE CC.ClientCustomerID = P.ClientCustomerID) AS ClientCustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.IsOvertimeAllowed,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.Notes,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure project.ValidateProjectCode
EXEC Utility.DropObject 'project.ValidateProjectCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the project.Project table
-- ==========================================================================
CREATE PROCEDURE project.ValidateProjectCode

@ProjectCode VARCHAR(50),
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT P.ProjectID
	FROM project.Project P
	WHERE P.ProjectCode = @ProjectCode
		AND P.ProjectID <> @ProjectID

END
GO
--End procedure project.ValidateProjectCode

--Begin procedure project.validateProjectDateAndTimeData
EXEC utility.DropObject 'project.validateProjectDateAndTimeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.29
-- Description:	A stored procedure to validate a dates and hours worked on a deployment
-- ====================================================================================
CREATE PROCEDURE project.validateProjectDateAndTimeData

@DateWorkedList VARCHAR(MAX) = NULL,
@HoursWorked INT = 0,
@PersonProjectID INT,
@ProjectLocationID INT,
@TimeTypeID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsValidated BIT

	DECLARE @cTimeTypeCode VARCHAR(50)

	DECLARE @dEndDate DATE
	DECLARE @dStartDate DATE

	DECLARE @nDateWorkedCount INT
	DECLARE @nHoursPerDay INT
	DECLARE @nPersonProjectLeaveHours NUMERIC(18,2)
	DECLARE @nPersonProjectTimeAllotted INT
	DECLARE @nPersonProjectTimeExpended INT

	DECLARE @tDateWorked TABLE (DateID INT NOT NULL PRIMARY KEY, DateWorked DATE, IsValidWorkDay BIT)
	DECLARE @tResult TABLE (MessageID INT NOT NULL PRIMARY KEY IDENTITY(1,1), Message VARCHAR(MAX))

	SELECT
		@bIsValidated = CTT.IsValidated,
		@nHoursPerDay = P.HoursPerDay,
		@dEndDate = PP.EndDate,
		@dStartDate = PP.StartDate,
		@nPersonProjectLeaveHours = ISNULL((SELECT PPTT.PersonProjectLeaveHours FROM person.PersonProjectTimeType PPTT WHERE PPTT.PersonProjectID = PP.PersonProjectID AND PPTT.ClientTimeTypeID = CTT.ClientTimeTypeID), 0),
		@cTimeTypeCode = TT.TimeTypeCode
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientTimeType CTT ON CTT.ClientID = P.ClientID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
			AND PP.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeID = @TimeTypeID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID
		AND PPT.TimeTypeID = @TimeTypeID

	IF @DateWorkedList IS NOT NULL
		BEGIN

		INSERT INTO @tDateWorked
			(DateID, DateWorked, IsValidWorkDay)
		SELECT
			LTT.ListItemID,
			CAST(LTT.ListItem AS DATE),
			project.IsValidWorkDay(CAST(LTT.ListItem AS DATE), @ProjectLocationID)
		FROM core.ListToTable(@DateWorkedList, ',') LTT

		END
	--ENDIF

	SELECT
		@nDateWorkedCount = ISNULL(COUNT(DW.DateID), 0)
	FROM @tDateWorked DW

	INSERT INTO @tResult
		(Message)
	SELECT
		core.FormatDate(DW.DateWorked) + ' is outside the allowed date range for this deployment.'
	FROM @tDateWorked DW
	WHERE DW.DateWorked < @dStartDate OR DW.DateWorked > @dEndDate
	ORDER BY DW.DateWorked

	IF @cTimeTypeCode = 'PROD'
		BEGIN

		INSERT INTO @tResult
			(Message)
		SELECT
			core.FormatDate(DW.DateWorked) + ' is not a valid work day for this deployment.'
		FROM @tDateWorked DW
		WHERE DW.IsValidWorkDay = 0
		ORDER BY DW.DateWorked

		SELECT
			@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
		FROM person.PersonProjectLocation PPL
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = PL.ProjectID
				AND PPL.PersonProjectID = @PersonProjectID

		IF ISNULL(@nPersonProjectTimeAllotted, 0) < ISNULL(@nPersonProjectTimeExpended, 0) + (@nDateWorkedCount * @HoursWorked)
			INSERT INTO @tResult (Message) VALUES ('The total hours worked exceeds your hours allocation for this deployment.')
		--ENDIF

		END
	ELSE IF @bIsValidated = 1
		BEGIN

		IF ISNULL(@nPersonProjectLeaveHours, 0) < ISNULL(@nPersonProjectTimeExpended, 0) + (@nDateWorkedCount * @HoursWorked)
			INSERT INTO @tResult (Message) VALUES ('The total hours claimed exceeds your allowance for this deployment.')
		--ENDIF

		END
	--ENDIF

	SELECT R.Message 
	FROM @tResult R
	ORDER BY R.MessageID

END
GO
--End procedure project.validateProjectDateAndTimeData

--Begin procedure reporting.GetInvoiceList
EXEC utility.DropObject 'reporting.GetInvoiceDetailList'
EXEC utility.DropObject 'reporting.GetInvoiceList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the invoice.Invoice table
-- =============================================================================
CREATE PROCEDURE reporting.GetInvoiceList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CAST(E.InvoiceAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(E.InvoiceVAT AS NUMERIC(18,2)) AS InvoiceExpenseVAT,
		person.FormatPersonNameByPersonID(ISNULL((SELECT EL.PersonID FROM eventlog.EventLog EL WHERE EL.EntityTypeCode = 'Project' AND EL.EventCode = 'create' AND EL.EntityID = I.ProjectID), 0), 'LastFirst') AS ProjectCreatorNameFormatted,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		CASE WHEN I.InvoiceStatus NOT IN ('Rejected', 'Approved') THEN DATEDIFF(d, I.InvoiceDateTime, getDate()) ELSE NULL END AS InvoiceAge,
		I.InvoiceAmount,
		I.InvoiceID,
		I.InvoiceStatus,
		I.ISOCurrencyCode,
		person.FormatPersonNameByPersonID(I.PersonID, 'LastFirst') AS PersonNameFormatted,
		I.PersonInvoiceNumber,
		I.TaxRate,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectName,
		CAST(T.InvoiceDPAAmount AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		CAST(T.InvoiceDPAVAT AS NUMERIC(18,2)) AS InvoiceDPAVAT,
		CAST(T.InvoiceDSAAmount AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		CAST(T.InvoiceDSAVAT AS NUMERIC(18,2)) AS InvoiceDSAVAT,
		CAST(T.InvoiceFeeAmount AS NUMERIC(18,2)) AS InvoiceFeeAmount,
		CAST(T.InvoiceFeeVAT AS NUMERIC(18,2)) AS InvoiceFeeVAT
	FROM invoice.Invoice I
		JOIN Project.Project P ON P.ProjectID = I.ProjectID 
		JOIN
			(
			SELECT
				PPE.InvoiceID,
				CAST(SUM(PPE.ExpenseAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
				CAST(SUM(PPE.TaxAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceVAT
			FROM person.PersonProjectExpense PPE
			GROUP BY PPE.InvoiceID
			) E ON E.InvoiceID = I.InvoiceID
		JOIN
			(
			SELECT
				PPT.InvoiceID,
				SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) AS InvoiceFeeAmount,
				SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceFeeVAT,
				SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceDPAAmount,
				SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceDPAVAT,
				SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceDSAAmount,
				SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceDSAVAT
			FROM person.PersonProjectTime PPT
				JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN project.Project P ON P.ProjectID = PP.ProjectID
				JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			GROUP BY PPT.InvoiceID
			) T ON T.InvoiceID = I.InvoiceID  
		JOIN reporting.SearchResult SR ON SR.EntityID = I.InvoiceID
			AND SR.PersonID = @PersonID
			AND SR.EntityTypeCode = 'Invoice'
	ORDER BY I.InvoiceStatus, I.InvoiceDateTime

END
GO
--End procedure reporting.GetInvoiceList

--Begin procedure reporting.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice summary data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientCode + PN.UserName AS VendorID,
		CT.ContractingTypeCode,		
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDate,
		I.InvoiceID,
		I.ISOCurrencyCode,
		LEFT(person.FormatPersonNameByPersonID(I.PersonID, 'FirstLast'), 40) AS VendorLongName,
		PA.IntermediateSWIFTCode,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalSWIFTCode,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		R.RoleName,

		CASE
			WHEN EXISTS 
				(
				SELECT 1 
				FROM client.ClientPersonProjectRole CPPR 
					JOIN person.PersonProject PP ON PP.ClientPersonProjectRoleID = CPPR.ClientPersonProjectRoleID 
						AND PP.PersonID = I.PersonID 
						AND PP.ProjectID = I.ProjectID 
						AND CPPR.InvoiceRDLCode = 'Internal'
				)
			THEN 1
			ELSE 0
		END AS IsInternalInvoice

	FROM invoice.Invoice I
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN client.ClientPerson CP ON C.ClientID = CP.ClientID AND CP.PersonID = I.PersonID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
		JOIN person.PersonAccount PA ON PA.PersonAccountID = I.PersonAccountID
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = PN.ContractingTypeID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure reporting.GetInvoiceSummaryDataByInvoiceID

--Begin procedure reporting.GetInvoiceTimeSummaryDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceTimeSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice time data
-- ===========================================================
CREATE PROCEDURE reporting.GetInvoiceTimeSummaryDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE AS
	(
			SELECT 
							I.InvoiceID,
							core.FormatDate(I.InvoiceDateTime) AS InvoiceDate,
							CASE
			    					WHEN TT.TimeTypeCode = 'PROD'
			    					THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
									ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
		         			END + ':' + ISNULL((SELECT PLC.ProjectLaborCode FROM client.ProjectLaborCode PLC WHERE PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID), '') AS AccountCode,

							PLC.ProjectLaborCode,
							PLC.ProjectLaborCodeName,
							C.ClientCode,
							C.ClientCode + PN.UserName AS VendorID,
							'21.HRO.23.00.1000'  AS ORG,
							P.ProjectCode + '.' + PTR.ProjectTermOfReferenceCode AS ProjectChargeCode,
							PPT.HoursWorked,
							CASE WHEN PP.FeeRate = 0 THEN   (P.HoursPerDay * PLC.PayRate1) * PPT.ExchangeRate 
							           ELSE PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate
							END AS Amount,
							PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * ((I.TaxRate / 100) * PPT.ApplyVAT) AS VAT
			FROM invoice.Invoice I
							JOIN person.Person PN ON PN.PersonID = I.PersonID
							JOIN person.PersonProjectTime PPT ON PPT.InvoiceID = I.InvoiceID
							JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
							JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
							JOIN project.Project P ON P.ProjectID = PP.ProjectID
							JOIN project.ProjectTermOfReference PTR ON PTR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
							JOIN client.Client C ON C.ClientID = P.ClientID
							JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
							AND I.InvoiceID = @InvoiceID
	)

		 SELECT 
						CTE.ProjectLaborCode,
						CTE.InvoiceID,
						CTE.InvoiceDate,
						CTE.ClientCode,
						CTE.AccountCode,
						CTE.VendorID,
						CTE.ProjectChargeCode,
						CTE.ORG,
						SUM(CTE.HoursWorked) AS HoursWorked,
						CAST(SUM(CTE.Amount) AS NUMERIC(18,2)) AS Amount,
						CAST(SUM(CTE.VAT) AS NUMERIC(18,2)) AS VAT
			FROM	CTE
			GROUP BY CTE.ProjectLaborCode, CTE.ClientCode, CTE.AccountCode,CTE.InvoiceID,CTE.InvoiceDate, CTE.VendorID, CTE.ProjectChargeCode, CTE.ORG
			ORDER BY CTE.ProjectLaborCode

END
GO
--End procedure reporting.GetInvoiceTimeSummaryDataByInvoiceID

--Begin procedure reporting.GetPersonProjectList
EXEC utility.DropObject 'reporting.GetDeploymentList'
EXEC utility.DropObject 'reporting.GetPersonProjectList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.24
-- Description:	A stored procedure to return data from the person.PersonProject table
-- ==================================================================================
CREATE PROCEDURE reporting.GetPersonProjectList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

		SELECT
			CF.ClientFunctionName,
			C.ClientName,
			CT.ContractingTypeName,
			PJ.CustomerName,
			core.FormatDate(PJ.EndDate) AS ProjectEndDateFormatted,
			CASE WHEN PJ.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS ProjectStatus,
			dropdown.GetCountryNameByISOCountryCode(PJ.ISOCountryCode2) AS CountryName,
			PJ.ProjectCode,
			PJ.ProjectName,
			core.FormatDate(PJ.StartDate) AS ProjectStartDateFormatted,
			CAST(PL.DPAAmount AS NUMERIC(18,2)) AS LocationDPAAmount,
			PL.DPAISOCurrencyCode,
			CAST(PL.DSACeiling AS NUMERIC(18,2)) AS DSACellingValue,
			PL.DSAISOCurrencyCode,
			PL.ProjectLocationName,
			PLC.ProjectLaborCode,
			PLC.ProjectLaborCodeName,
			PN.EmailAddress,
			PN.IsRegisteredForUKTax,
			core.FormatDate(PP.EndDate) AS DeploymentEndDateFormatted,
			CAST(PP.FeeRate AS NUMERIC(18,2)) AS DeployemntFeeRate,
			CASE WHEN PP.IsActive = 1 THEN 'Active' ELSE 'Inactive' END AS DeployemntStatus,
			PP.ISOCurrencyCode,
			person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted,
			core.FormatDate(PP.StartDate) AS DeploymentStartDateFormatted,
			PPL.Days,
			PR.ProjectRoleName,
			TOR.ProjectTermOfReferenceCode,
			TOR.ProjectTermOfReferenceName		
	FROM person.PersonProject PP 
		JOIN person.Person PN ON PN.PersonID = PP.PersonID 
		JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID  
		JOIN client.Client C ON C.ClientID = PJ.ClientID 
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID 
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID 
		JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID 
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = PN.ContractingTypeID 
		JOIN person.PersonProjectLocation PPL on PPL.PersonProjectID = PP.PersonProjectID 
		JOIN project.ProjectLocation PL ON PP.ProjectID = PL.ProjectID 
		JOIN project.ProjectTermOfReference TOR ON TOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
		JOIN reporting.SearchResult SR ON SR.EntityID = PP.PersonProjectID
			AND PPL.ProjectLocationID = PL.ProjectLocationID 
			AND SR.EntityTypeCode = 'PersonProject'
			AND SR.PersonID = @PersonID

	ORDER BY PJ.ProjectName, C.ClientName, 24

END
GO
--End procedure reporting.GetPersonProjectList

