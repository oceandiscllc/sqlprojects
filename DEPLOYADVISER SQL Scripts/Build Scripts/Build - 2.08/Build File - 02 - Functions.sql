USE DeployAdviser
GO

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT,
@YesClassName VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN
	
	DECLARE @cClassName VARCHAR(50) = CASE WHEN @Value = 1 THEN @YesClassName WHEN @YesClassName = 'btn-success' THEN 'btn-danger' ELSE 'btn-success' END
	DECLARE @cValue VARCHAR(3) = CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END
	DECLARE @cHTML VARCHAR(200) = CASE WHEN @YesClassName IS NULL THEN '' ELSE '<a class="btn btn-xs btn-icon btn-circle ' + @cClassName + ' m-r-10" title="' + RTRIM(@cValue) + '"></a>' END

	RETURN @cHTML + @cValue

END
GO
--End function core.YesNoFormat

--Begin function person.GetClientsByPersonID
EXEC utility.DropObject 'person.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all clients accessible to a PersonID
-- ====================================================================================================

CREATE FUNCTION person.GetClientsByPersonID
(
@PersonID INT,
@ClientPersonRoleCode VARCHAR(250)
)

RETURNS @tTable TABLE (ClientID INT NOT NULL PRIMARY KEY, ClientName VARCHAR(250), IntegrationCode VARCHAR(50))  

AS
BEGIN

	SET @ClientPersonRoleCode = core.NullIfEmpty(@ClientPersonRoleCode)

	INSERT INTO @tTable
		(ClientID, ClientName, IntegrationCode)
	SELECT 
		CP.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
			AND 
				(
				@ClientPersonRoleCode IS NULL
					OR EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@ClientPersonRoleCode, ',') LTT
						WHERE LTT.ListItem = CP.ClientPersonRoleCode
						)
				)

	UNION

	SELECT 
		C.ClientID,
		C.ClientName,
		C.IntegrationCode
	FROM client.Client C
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person P
		WHERE P.PersonID = @PersonID
			AND P.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetClientsByPersonID

--Begin function person.GetPersonProjectDaysLogged
EXEC utility.DropObject 'person.GetPersonProjectDaysLogged'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days logged on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysLogged
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysLogged NUMERIC(18,2)

	SELECT @nDaysLogged = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'

	RETURN ISNULL(@nDaysLogged, 0)
	
END
GO
--End function person.GetPersonProjectDaysLogged

--Begin function person.GetPersonProjectDaysInvoiced
EXEC utility.DropObject 'person.GetPersonProjectDaysInvoiced'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Invoiced on a PersonProject record
-- ========================================================================================

CREATE FUNCTION person.GetPersonProjectDaysInvoiced
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysInvoiced NUMERIC(18,2)

	SELECT @nDaysInvoiced = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND I.InvoiceStatus <> 'Approved'

	RETURN ISNULL(@nDaysInvoiced, 0)
	
END
GO
--End function person.GetPersonProjectDaysInvoiced

--Begin function person.GetPersonProjectDaysPaid
EXEC utility.DropObject 'person.GetPersonProjectDaysPaid'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Paid on a PersonProject record
-- ====================================================================================

CREATE FUNCTION person.GetPersonProjectDaysPaid
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysPaid NUMERIC(18,2)

	SELECT @nDaysPaid = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND I.InvoiceStatus = 'Approved'

	RETURN ISNULL(@nDaysPaid, 0)
	
END
GO
--End function person.GetPersonProjectDaysPaid