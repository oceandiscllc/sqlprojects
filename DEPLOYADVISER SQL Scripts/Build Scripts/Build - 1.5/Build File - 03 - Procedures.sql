USE DeployAdviser
GO

--Begin procedure core.GetClientActiveUserCountsByMonthAndYear
EXEC utility.DropObject 'core.GetClientActiveUserCountsByMonthAndYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE core.GetClientActiveUserCountsByMonthAndYear

@Month INT = 0,
@Year INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nYear INT = 0

	--Swap month and year if they come in in the wrong order
	IF LEN(CAST(@Year AS VARCHAR(4))) <> 4
		BEGIN

		SET @nYear = @Year
		SET @Year = @Month
		SET @Month = @nYear

		END
	--ENDIF

	;
	WITH CPD AS
		(
		SELECT DISTINCT
			EL.PersonID,
			CP.ClientID
		FROM eventlog.EventLog EL
			JOIN client.ClientPerson CP ON CP.PersonID = EL.PersonID
				AND EL.EntityTypeCode IN ('Invoice','PersonProjectTime','PersonProjectExpense')
				AND EL.EventCode IN ('Create','Update')
				AND YEAR(EL.CreateDateTime) = @Year
				AND MONTH(EL.CreateDateTime) = @Month
		)

	SELECT
		COUNT(CPD.PersonID) AS ActiveUserCount,
		C.ClientName,
		DATENAME(MONTH, DATEADD(MONTH, @Month, -1)) + ', ' + CAST(@Year AS CHAR(4)) AS MonthYear
	FROM CPD
		JOIN client.Client C ON C.ClientID = CPD.ClientID
	GROUP BY CPD.ClientID, C.ClientName
	ORDER BY C.ClientName

END
GO
--End procedure core.GetClientActiveUserCountsByMonthAndYear

--Begin procedure core.GetSystemSetupDataBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.13
-- Description:	A stored procedure to return data from the core.SystemSetup table
-- ==============================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM core.SystemSetup SS WHERE SS.SystemSetupKey = @SystemSetupKey)
		INSERT INTO core.SystemSetup (SystemSetupKey) VALUES (@SystemSetupKey)
	--ENDIF

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupKey = @SystemSetupKey

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupKey

--Begin procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.InvoicePerDiemLog table
-- ====================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLogSummary
	SELECT 
		IPL.ProjectLocationName,
		C.FinanceCode2 AS DPACode,
		SUM(IPL.DPAAmount) AS DPAAmount,
		IPL.DPAISOCurrencyCode,
		CAST(SUM(IPL.DPAAmount * IPL.DPAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(IPL1.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL1 WHERE IPL1.InvoiceID = IPL.InvoiceID AND IPL1.DPAAmount > 0 AND IPL1.ProjectLocationName = IPL.ProjectLocationName) AS DPACount,
		C.FinanceCode3 AS DSACode,
		SUM(IPL.DSAAmount) AS DSAAmount,
		IPL.DSAISOCurrencyCode,
		CAST(SUM(IPL.DSAAmount * IPL.DSAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(IPL2.InvoicePerDiemLogID) FROM invoice.InvoicePerDiemLog IPL2 WHERE IPL2.InvoiceID = IPL.InvoiceID AND IPL2.DSAAmount > 0 AND IPL2.ProjectLocationName = IPL.ProjectLocationName) AS DSACount
	FROM invoice.InvoicePerDiemLog IPL
		JOIN invoice.Invoice I ON I.InvoiceID = IPL.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND IPL.InvoiceID = @InvoiceID
	GROUP BY IPL.ProjectLocationName, IPL.DPAISOCurrencyCode, C.FinanceCode2, IPL.DSAISOCurrencyCode, C.FinanceCode3, IPL.InvoiceID
	ORDER BY IPL.ProjectLocationName

END
GO
--End procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked)
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.ProjectLocationID = PPT.ProjectLocationID
			AND PPL.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
				AND SR.EntityTypeGroupCode = 'PersonProjectTime'
				AND SR.EntityID = PPT.PersonProjectTimeID
				AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate
	FROM person.Person PN, project.Project PJ
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
	WHERE PN.PersonID = @PersonID
		AND PJ.ProjectID = @ProjectID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		TPL.DPAAmount,
		TPL.DPAISOCurrencyCode,
		TPL.DPAExchangeRate,
		CAST(TPL.DPAAmount * TPL.DPAExchangeRate AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		@cFinanceCode3 AS DSACode,
		TPL.DSAAmount,
		TPL.DSAISOCurrencyCode,
		TPL.DSAExchangeRate,
		CAST(TPL.DSAAmount * TPL.DSAExchangeRate AS NUMERIC(18,2)) AS InvoiceDSAAmount
	FROM @tPerDiemLog TPL
	ORDER BY TPL.PerDiemLogID

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS DPACode,
		SUM(TPL.DPAAmount) AS DPAAmount,
		TPL.DPAISOCurrencyCode,
		CAST(SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DSAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS DPACount,
		@cFinanceCode3 AS DSACode,
		SUM(TPL.DSAAmount) AS DSAAmount,
		TPL.DSAISOCurrencyCode,
		CAST(SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS DSACount
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DPAISOCurrencyCode, TPL.DSAISOCurrencyCode
	ORDER BY TPL.ProjectLocationName

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100 AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * @nTaxRate / 100) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			CAST(SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			CAST(SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			0 AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(CAST(TEL.ExpenseAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceAmount,
			SUM(CAST(TEL.TaxAmount * TEL.ExchangeRate AS NUMERIC(18,2))) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure person.GetPersonProjectDataByPersonID
EXEC utility.DropObject 'person.GetPersonProjectDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.17
-- Description:	A stored procedure to return person project data based on a PersonID
-- =================================================================================
CREATE PROCEDURE person.GetPersonProjectDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (PersonProjectID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tTable
		(PersonProjectID)
	SELECT
		PP.PersonProjectID
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND P.IsActive = 1
			AND PP.AcceptedDate IS NOT NULL
			AND PP.PersonID = @PersonID

	SELECT
		PP.PersonProjectID,
		P.ProjectID,
		P.ProjectName,
		CF.ClientFunctionName,
		PTOR.ProjectTermOfReferenceName
	FROM @tTable T
		JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
	ORDER BY 1

	SELECT
		PP.PersonProjectID,
		PL.DSACeiling,
		PL.ProjectLocationID,
		PL.ProjectLocationName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7
	FROM @tTable T
		JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID
		JOIN person.PersonProjectLocation PPL ON PPL.PersonProjectID = PP.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
	ORDER BY 1

	SELECT 
		PP.PersonProjectID,
		PCC.ClientCostCodeID,

		CASE
			WHEN PCC.ProjectCostCodeDescription IS NULL
			THEN CCC.ClientCostCodeDescription
			ELSE PCC.ProjectCostCodeDescription
		END AS ProjectCostCodeDescription

	FROM @tTable T
		JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID
		JOIN project.ProjectCostCode PCC ON PCC.ProjectID = PP.ProjectID
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
	ORDER BY 1

	SELECT 
		PP.PersonProjectID,
		C.CurrencyName,
		C.ISOCurrencyCode
	FROM @tTable T
		JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID
		JOIN project.ProjectCurrency PC ON PC.ProjectID = PP.ProjectID
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
	ORDER BY 1

END
GO
--End procedure person.GetPersonProjectDataByPersonID

--Begin procedure person.ValidateMobileLogin
EXEC utility.DropObject 'person.ValidateMobileLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to validate user logins from a mobile device
-- ============================================================================
CREATE PROCEDURE person.ValidateMobileLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsValidLogin BIT = 0
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cAPIKey VARCHAR(32)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 1 AND @bIsAccountLockedOut = 0 AND @bIsActive = 1 AND @bIsPasswordExpired = 0
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE P
			SET 
				P.APIKey = NULL,
				P.APIKeyExpirationDateTime = NULL,
				P.InvalidLoginAttempts = @nInvalidLoginAttempts,
				P.IsAccountLockedOut = @bIsAccountLockedOut
			FROM person.Person P
			WHERE P.PersonID = @nPersonID

			END
		ELSE 
			BEGIN

			SET @bIsValidLogin = 1

			UPDATE P
			SET 
				P.APIKey = REPLACE(newID(), '-', ''),
				P.APIKeyExpirationDateTime = SYSUTCDateTime(),
				P.InvalidLoginAttempts = 0,
				P.LastLoginDateTime = getDate()
			FROM person.Person P
			WHERE P.PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF	

	SELECT
		P.APIKey,
		@bIsValidLogin AS IsValidLogin
	FROM person.Person P
	WHERE P.PersonID = @nPersonID

END
GO
--End procedure person.ValidateMobileLogin

--Begin procedure project.GetProjectByProjectID
EXEC utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.02
-- Description:	A stored procedure to return data from the project.Project table based on a ProjectID
-- ==================================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectInvoiceToID INT = (SELECT P.ProjectInvoiceToID FROM project.Project P WHERE P.ProjectID = @ProjectID)
	SET @nProjectInvoiceToID = ISNULL(@nProjectInvoiceToID, 0)

	--Project
	SELECT
		C.ClientID,
		C.ClientName,
		P.CustomerName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.HoursPerDay,
		P.InvoiceDueDayOfMonth,
		P.IsActive,
		P.IsForecastRequired,
		P.IsNextOfKinInformationRequired,
		P.ISOCountryCode2,
		(SELECT C.CountryName FROM dropdown.Country C WHERE C.ISOCountryCode2 = P.ISOCountryCode2) AS CountryName,
		P.ProjectCode,
		P.ProjectID,
		P.ProjectInvoiceToID,
		P.ProjectName,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted
	FROM project.Project P
		JOIN client.Client C ON C.ClientID = P.ClientID
			AND P.ProjectID = @ProjectID

	--ProjectCostCode
	SELECT
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeName,
		PCC.IsActive,
		ISNULL(PCC.ProjectCostCodeDescription, CCC.ClientCostCodeDescription) AS ProjectCostCodeDescription,
		PCC.ProjectCostCodeID
	FROM project.ProjectCostCode PCC
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PCC.ClientCostCodeID
			AND PCC.ProjectID = @ProjectID
	ORDER BY 3, 4, 2

	--ProjectCurrency
	SELECT
		C.ISOCurrencyCode,
		C.CurrencyID,
		C.CurrencyName,
		PC.IsActive
	FROM project.ProjectCurrency PC
		JOIN dropdown.Currency C ON C.ISOCurrencyCode = PC.ISOCurrencyCode
			AND PC.ProjectID = @ProjectID
	ORDER BY 3, 1

	--ProjectInvoiceTo
	EXEC client.GetProjectInvoiceToByProjectInvoiceToID @nProjectInvoiceToID

	--ProjectLocation
	SELECT
		newID() AS ProjectLocationGUID,
		C.CountryName,
		PL.CanWorkDay1,
		PL.CanWorkDay2,
		PL.CanWorkDay3,
		PL.CanWorkDay4,
		PL.CanWorkDay5,
		PL.CanWorkDay6,
		PL.CanWorkDay7,
		PL.DPAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DPAISOCurrencyCode) AS DPAISOCurrencyName,
		PL.DPAAmount,
		PL.DSAISOCurrencyCode,
		dropdown.GetCurrencyNameFromISOCurrencyCode(PL.DSAISOCurrencyCode) AS DSAISOCurrencyName,
		PL.DSACeiling,
		PL.IsActive,
		PL.ISOCountryCode2,
		PL.ProjectID,
		PL.ProjectLocationID,
		PL.ProjectLocationName
	FROM project.ProjectLocation PL
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
			AND PL.ProjectID = @ProjectID
	ORDER BY PL.ProjectLocationName, PL.ISOCountryCode2, PL.ProjectLocationID

	--ProjectPerson
	SELECT
		PP.PersonID,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM project.ProjectPerson PP
	WHERE PP.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProjectTermOfReference
	SELECT
		newID() AS ProjectTermOfReferenceGUID,
		PTOR.ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID,
		PTOR.ProjectTermOfReferenceName,
		PTOR.ProjectTermOfReferenceDescription,
		PTOR.IsActive
	FROM project.ProjectTermOfReference PTOR
	WHERE PTOR.ProjectID = @ProjectID
	ORDER BY PTOR.ProjectTermOfReferenceName, PTOR.ProjectTermOfReferenceID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure reporting.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice summary data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientCode + PN.UserName AS VendorID,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDate,
		I.InvoiceID,
		I.ISOCurrencyCode,
		LEFT(person.FormatPersonNameByPersonID(I.PersonID, 'FirstLast'), 40) AS VendorLongName,
		PA.IntermediateSWIFTCode,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalSWIFTCode,
		PN.EmailAddress
	FROM invoice.Invoice I
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN person.PersonAccount PA ON PA.PersonAccountID = I.PersonAccountID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure reporting.GetInvoiceSummaryDataByInvoiceID

--Begin procedure reporting.GetInvoiceExpenseDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceExpenseDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice expense data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceExpenseDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CC.ClientCostCodeName,
		P.ProjectCode,
		CAST(SUM(PPE.ExchangeRate * PPE.ExpenseAmount) AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(SUM(PPE.ExchangeRate * PPE.TaxAmount) AS NUMERIC(18,2)) AS InvoiceTaxAmount
	FROM invoice.Invoice I
		JOIN person.PersonProjectExpense PPE ON PPE.InvoiceID = I.InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientCostCode CC ON CC.ClientCostCodeID = PPE.ClientCostCodeID
			AND I.InvoiceID = @InvoiceID
	GROUP BY CC.ClientCostCodeName, P.ProjectCode

END
GO
--End procedure reporting.GetInvoiceExpenseDataByInvoiceID

--Begin procedure reporting.GetInvoiceTimeDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceTimeDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice time data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceTimeDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.FinanceCode1,
		PLC.ProjectLaborCode,
		C.ClientCode + PN.UserName AS VendorID,
		P.ProjectCode,
		SUM(PPT.HoursWorked) AS HoursWorked,
		CAST(SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * I.TaxRate / 100) AS NUMERIC(18,2)) AS VAT
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN person.PersonProjectTime PPT ON PPT.InvoiceID = I.InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			AND I.InvoiceID = @InvoiceID
	GROUP BY C.ClientCode, C.FinanceCode1, P.ProjectCode, PLC.ProjectLaborCode, PN.UserName

END
GO
--End procedure reporting.GetInvoiceTimeDataByInvoiceID
