USE DeployAdviser
GO

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'APIKey', 'VARCHAR(32)'
EXEC utility.AddColumn @TableName, 'APIKeyExpirationDateTime', 'DATETIME'
GO
--End table person.Person

--Begin table person.PersonProjectExpense
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectExpense'

EXEC utility.AddColumn @TableName, 'PersonProjectExpenseGUID', 'VARCHAR(50)'
GO
--End table person.PersonProjectExpense

--Begin table person.PersonProjectTime
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectTime'

EXEC utility.AddColumn @TableName, 'PersonProjectTimeGUID', 'VARCHAR(50)'
GO
--End table person.PersonProjectTime

--Begin table project.ProjectTermOfReference
DECLARE @TableName VARCHAR(250) = 'project.ProjectTermOfReference'

EXEC utility.AddColumn @TableName, 'ProjectTermOfReferenceCode', 'CHAR(3)'
GO

EXEC utility.DropObject 'project.TR_ProjectTermOfReference'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.14
-- Description:	A trigger to populate the ProjectTermOfReferenceCode in the project.ProjectTermOfReference table
-- =============================================================================================================
CREATE TRIGGER project.TR_ProjectTermOfReference ON project.ProjectTermOfReference AFTER INSERT
AS
SET ARITHABORT ON

;
WITH D AS
	(
	SELECT
		RIGHT('000' + CAST(ROW_NUMBER() OVER (PARTITION BY PTOR.ProjectID ORDER BY PTOR.ProjectTermOfReferenceID) AS VARCHAR(3)), 3) AS ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID
	FROM project.ProjectTermOfReference PTOR
	)

UPDATE PTOR
SET PTOR.ProjectTermOfReferenceCode = D.ProjectTermOfReferenceCode
FROM project.ProjectTermOfReference PTOR
	JOIN D ON D.ProjectTermOfReferenceID = PTOR.ProjectTermOfReferenceID
		AND PTOR.ProjectTermOfReferenceCode IS NULL
GO

ALTER TABLE project.ProjectTermOfReference ENABLE TRIGGER TR_ProjectTermOfReference
GO
--End table project.ProjectTermOfReference