USE DeployAdviser
GO

--Begin function core.GetDatesFromRange
EXEC utility.DropObject 'core.GetDatesFromRange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A function to return a table with dates based on a start end dates
-- ===============================================================================

CREATE FUNCTION core.GetDatesFromRange
(
@StartDate DATE,
@EndDate DATE
)

RETURNS @tTable TABLE (ID INT IDENTITY(1,1) PRIMARY KEY, DateValue DATE)  

AS
BEGIN

	DECLARE @dDate1 DATE
	DECLARE @dDate2 DATE

	SELECT
		@dDate1 = CASE WHEN @StartDate <= @EndDate THEN @StartDate ELSE @EndDate END,
		@dDate2 = CASE WHEN @StartDate <= @EndDate THEN @EndDate ELSE @StartDate END

	;
	WITH D AS
		(
		SELECT @dDate1 AS DateValue

	  UNION ALL

	  SELECT DATEADD(d, 1, D.DateValue)
		FROM D
		WHERE DATEADD(d, 1, D.DateValue) <= @dDate2
		)

	INSERT INTO @tTable 
		(DateValue) 
	SELECT D.DateValue 
	FROM D
	ORDER BY D.DateValue
	
	RETURN

END
GO
--End function core.GetDatesFromRange

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode IN ('add', 'create')
			THEN 'Add'
			WHEN @EventCode = 'addupdate'
			THEN 'Update'
			WHEN @EventCode = 'forecast'
			THEN 'View Forecast'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode IN ('read', 'view')
			THEN 'View'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE @EventCodeName
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode