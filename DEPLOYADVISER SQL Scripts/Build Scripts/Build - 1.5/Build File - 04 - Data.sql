USE DeployAdviser
GO

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TimeExpense',
	@Icon = 'fa fa-fw fa-question-circle',
	@NewMenuItemCode = 'About',
	@NewMenuItemLink = '/about',
	@NewMenuItemText = 'About &amp; Support'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'AboutContent', 'Content that appears on the About & Support page', '<p style="text-align: center;"><span style="font-size:14px;"><strong>Hello World</strong></span></p>'
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit the contents of the about page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View the about page', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table project.ProjectTermOfReference
ALTER TABLE project.ProjectTermOfReference DISABLE TRIGGER TR_ProjectTermOfReference
GO

;
WITH D AS
	(
	SELECT
		RIGHT('000' + CAST(ROW_NUMBER() OVER (PARTITION BY PTOR.ProjectID ORDER BY PTOR.ProjectTermOfReferenceID) AS VARCHAR(3)), 3) AS ProjectTermOfReferenceCode,
		PTOR.ProjectTermOfReferenceID
	FROM project.ProjectTermOfReference PTOR
	)

UPDATE PTOR
SET PTOR.ProjectTermOfReferenceCode = D.ProjectTermOfReferenceCode
FROM project.ProjectTermOfReference PTOR
	JOIN D ON D.ProjectTermOfReferenceID = PTOR.ProjectTermOfReferenceID
GO

ALTER TABLE project.ProjectTermOfReference ENABLE TRIGGER TR_ProjectTermOfReference
GO
--End table project.ProjectTermOfReference

