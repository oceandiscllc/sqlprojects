USE DeployAdviser
GO

--Begin table person.PersonGroup
DECLARE @TableName VARCHAR(250) = 'person.PersonGroup'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroup
	(
	PersonGroupID INT IDENTITY(1,1) NOT NULL,
	ClientID INT,
	PersonGroupName VARCHAR(250),
	PersonGroupDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonGroupID'
GO
--End table person.PersonGroup

--Begin table person.PersonGroupPerson
DECLARE @TableName VARCHAR(250) = 'person.PersonGroupPerson'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroupPerson
	(
	PersonGroupPersonID INT IDENTITY(1,1) NOT NULL,
	PersonGroupID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonGroupPerson', 'PersonGroupID,PersonID'
GO
--End table person.PersonGroupPerson
