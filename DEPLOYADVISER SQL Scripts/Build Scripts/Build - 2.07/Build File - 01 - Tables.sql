USE DeployAdviser
GO

--Begin table client.Client
EXEC utility.AddColumn 'client.Client', 'IntegrationCode', 'VARCHAR(50)'
GO
--End table client.Client

--Begin table client.ClientPerson
EXEC utility.AddColumn 'client.ClientPerson', 'CreatePersonID', 'INT', '0'
GO

EXEC utility.DropObject 'client.TR_ClientPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER client.TR_ClientPerson ON client.ClientPerson AFTER DELETE, INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 0
			AND PP.PermissionableLineage IN ('Person.Invite', 'Project.Forecast')
			AND NOT EXISTS
				(
				SELECT 1
				FROM client.ClientPerson CP
				WHERE CP.PersonID = P.PersonID
					AND CP.ClientPersonRoleCode IN ('Administrator', 'ProjectManager')
					AND CP.AcceptedDate IS NOT NULL
				)

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		CP.PersonID,
		P.PermissionableLineage
	FROM client.ClientPerson CP, permissionable.Permissionable P
	WHERE CP.ClientPersonRoleCode IN ('Administrator', 'ProjectManager')
		AND CP.AcceptedDate IS NOT NULL
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = CP.PersonID
				AND PP.PermissionableLineage = 'Person.Invite'
			)
		AND P.PermissionableLineage IN ('Person.Invite', 'Project.Forecast')

END
GO
--End table person.ClientPerson

--Begin table dropdown.ClientRoster
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientRoster
	(
	ClientRosterID INT IDENTITY(0,1) NOT NULL,
	ClientRosterCode VARCHAR(50),
	ClientRosterName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'DisplayOrder,ClientRosterName'
GO
--End table dropdown.ClientRoster

--Begin table person.Person
EXEC utility.DropDefaultConstraint 'person.Person', 'IsUKEUNational'
GO

ALTER TABLE person.Person ALTER COLUMN IsUKEUNational BIT NULL
GO

EXEC utility.AddColumn 'person.Person', 'NationalInsuranceNumber', 'VARCHAR(25)'
GO
--End table person.Person

--Begin table person.PersonClientRoster
DECLARE @TableName VARCHAR(250) = 'person.PersonClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonClientRoster
	(
	PersonClientRosterID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ClientRosterID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientRosterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'PersonID,ClientRosterID'
GO
--End table person.PersonClientRoster

--Begin table person.PersonProjectTime
EXEC utility.DropObject 'person.TR_PersonProjectTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.12
-- Description:	A trigger to set a default value for the PersonProjectName field
-- =============================================================================
CREATE TRIGGER person.TR_PersonProjectTime ON person.PersonProjectTime AFTER UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	UPDATE PPT
	SET 
		PPT.IsLineManagerApproved = 0
	FROM person.PersonProjectTime PPT
		JOIN DELETED D ON D.PersonProjectTimeID = PPT.PersonProjectTimeID
			AND
				(
				D.DateWorked <> PPT.DateWorked
					OR D.HoursWorked <> PPT.HoursWorked
					OR D.ProjectManagerNotes <> PPT.ProjectManagerNotes
					OR D.ProjectLocationID <> PPT.ProjectLocationID
				)

END
GO
--End table person.PersonProjectTime