USE DeployAdviser
GO

--Begin table client.Client
UPDATE C
SET C.IntegrationCode = 'Hermis'
FROM client.Client C
WHERE C.ClientCode IN ('H3', 'HSOT')
GO
--End table client.Client

--Begin table core.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Person' AND ET.EmailTemplateCode = 'AcceptClientPerson')
 BEGIN

 INSERT INTO core.EmailTemplate
  (EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
 VALUES
  ('Person', 'AcceptClientPerson', 'Sent when a user accepts an invitation to affiliate with a client', 'A DeployAdviser Client Affiliation Has Been Accepted', '<p>[[PersonNameFormatted]] has accepted an invitation to affiliate with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]]. This action will allow [[ClientName]] to make future offers of assignment to [[PersonNameFormatted]].</p><p>&nbsp;</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>'),
  ('Person', 'RejectClientPerson', 'Sent when a user declines an invitation to affiliate with a client', 'A DeployAdviser Client Affiliation Has Been Declined', '<p>[[PersonNameFormatted]] has declined an invitation to affiliate with [[ClientName]] in the DeployAdviser system with a role of [[RoleName]]. This action will not allow [[ClientName]] to make future offers of assignment to [[PersonNameFormatted]].</p><p>&nbsp;</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')

 END
--ENDIF
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Person' AND ETF.PlaceHolderText = '[[PersonNameFormatted]]')
 BEGIN

 INSERT INTO core.EmailTemplateField
  (EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
 VALUES
  ('Person', '[[PersonNameFormatted]]', 'Person Name')

 END
--ENDIF
GO
--End table core.EmailTemplateField

--Begin table dropdown.ClientRoster
TRUNCATE TABLE dropdown.ClientRoster
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientRoster', 'ClientRosterID', 0
GO

INSERT INTO dropdown.ClientRoster 
	(ClientRosterCode, ClientRosterName) 
VALUES
	('Humanitarian', 'Humanitarian Roster'),
	('SU', 'SU Roster')
GO
--End table dropdown.ClientRoster

--Begin table dropdown.ProjectRole
TRUNCATE TABLE dropdown.ProjectRole
GO

EXEC utility.InsertIdentityValue 'dropdown.ProjectRole', 'ProjectRoleID', 0
GO

INSERT INTO dropdown.ProjectRole
	(ProjectRoleCode, ProjectRoleName, DisplayOrder)
VALUES
	('Consultant', 'Consultant', 1),
	('Employee', 'Employee', 2),
	('Fixed Term Employee', 'FixedTermEmployee', 3)
GO

UPDATE PP
SET PP.ProjectRoleID = (SELECT PR.ProjectRoleID FROM dropdown.ProjectRole PR WHERE PR.ProjectRoleCode = 'Consultant')
FROM person.PersonProject PP
GO
--End table dropdown.ProjectRole

--Begin table permissionable.PermissionableTemplatePermissionable
DELETE PTP
FROM permissionable.PermissionableTemplatePermissionable PTP
WHERE PTP.PermissionableLineage IN ('Person.Invite', 'Project.Forecast')
GO
--End table permissionable.PermissionableTemplatePermissionable
