USE DeployAdviser
GO

--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure document.GetFileTypeData
EXEC utility.DropObject 'document.GetFileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.08
-- Description:	A stored procedure to get data from the document.FileType table
-- ============================================================================
CREATE PROCEDURE document.GetFileTypeData

@MimeType VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		FT.Extension,
		LEFT(FT.MimeType, CHARINDEX('/', FT.MimeType) - 1) AS ContentType,
		REVERSE(LEFT(REVERSE(FT.MimeType), CHARINDEX('/', REVERSE(FT.MimeType)) - 1)) AS ContentSubType
	FROM document.FileType FT
	WHERE FT.MimeType = @MimeType

END
GO
--End procedure document.GetFileTypeData

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentGUID VARCHAR(50) = NULL,
@DocumentID INT = 0,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @DocumentGUID IS NOT NULL
		BEGIN

		SELECT @DocumentID = D.DocumentID
		FROM document.Document D
		WHERE D.DocumentGUID = @DocumentGUID

		END
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE	DE.EntityTypeCode = @EntityTypeCode
		AND (DE.EntityTypeSubCode = @EntityTypeSubCode OR (@EntityTypeSubCode IS NULL AND DE.EntityTypeSubCode IS NULL))
		AND DE.EntityID = @EntityID 
		AND ((@DocumentGUID IS NULL AND @DocumentID = 0) OR DE.DocumentID = @DocumentID)

	IF NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.DocumentID = @DocumentID)
		BEGIN

		DELETE D
		FROM document.Document D
		WHERE D.DocumentID = @DocumentID

		END
	--ENDIF

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure dropdown.GetClientRosterData
EXEC Utility.DropObject 'dropdown.GetClientRosterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.ClientRoster table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetClientRosterData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientRosterID,
		T.ClientRosterCode,
		T.ClientRosterName
	FROM dropdown.ClientRoster T
	WHERE (T.ClientRosterID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientRosterName, T.ClientRosterID

END
GO
--End procedure dropdown.GetClientRosterData

--Begin procedure invoice.GetInvoiceExpenseLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectExpense table
-- ======================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLog
	SELECT
		core.FormatDateWithDay(PPE.ExpenseDate) AS ExpenseDateFormatted,
		CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription AS ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		PPE.ExchangeRate,
		CAST(PPE.ExchangeRate * PPE.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(PPE.ExchangeRate * PPE.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID) IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' 
				+ (SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID)
				+ '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			AND PPE.InvoiceID = @InvoiceID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName 

END
GO
--End procedure invoice.GetInvoiceExpenseLogByInvoiceID

--Begin procedure invoice.GetInvoiceExpenseLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceExpenseLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectExpense table
-- ======================================================================================
CREATE PROCEDURE invoice.GetInvoiceExpenseLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceExpenseLogSummary
	WITH IEL AS 
		(
		SELECT
			PPE.ClientCostCodeID,
			SUM(PPE.ExchangeRate * PPE.ExpenseAmount) AS ExpenseAmount,
			SUM(PPE.ExchangeRate * PPE.TaxAmount) AS TaxAmount,
			COUNT(DISTINCT PPE.ExpenseDate) AS DaysCount
		FROM person.PersonProjectExpense PPE
		WHERE PPE.InvoiceID = @InvoiceID
		GROUP BY PPE.ClientCostCodeID, PPE.ExpenseDate
		)

	SELECT
		CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription AS ClientCostCodeName,
		CAST(SUM(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS ExpenseAmount,
		CAST(SUM(IEL.TaxAmount) AS NUMERIC(18,2)) AS TaxAmount,
		SUM(IEL.DaysCount) AS DaysCount,
		CAST(MIN(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MinExpenseAmount,
		CAST(MAX(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MaxExpenseAmount,
		CAST(SUM(IEL.ExpenseAmount) / SUM(IEL.DaysCount) AS NUMERIC(18,2)) AS AvgExpenseAmount
	FROM IEL
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = IEL.ClientCostCodeID
	GROUP BY CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription, IEL.DaysCount
	ORDER BY 1

END
GO
--End procedure invoice.GetInvoiceExpenseLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(350),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName + ' - ' + CCC.ClientCostCodeDescription AS ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate, ApplyVAT)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PL.DPAAmount * PPT.HasDPA AS DPAAmount,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.ApplyVAT
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, ApplyVAT, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PPT.ApplyVAT,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	IF @EndDate IS NULL
		BEGIN

		SELECT TOP 1 @EndDate = D.EndDate
		FROM
			(
			SELECT PPE.ExpenseDate AS EndDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS EndDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.EndDate DESC

		END
	--ENDIF

	IF @StartDate IS NULL
		BEGIN

		SELECT TOP 1 @StartDate = D.StartDate
		FROM
			(
			SELECT PPE.ExpenseDate AS StartDate 
			FROM person.PersonProjectExpense PPE 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
					AND SR.EntityID = PPE.PersonProjectExpenseID
					AND SR.PersonID = @PersonID

			UNION

			SELECT PPT.DateWorked AS StartDate 
			FROM person.PersonProjectTime PPT 
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
					AND SR.EntityTypeGroupCode = 'PersonProjectTime'
					AND SR.EntityID = PPT.PersonProjectTimeID
					AND SR.PersonID = @PersonID
			) AS D
		ORDER BY D.StartDate

		END
	--ENDIF

	--InvoiceData
	SELECT
		NULL AS InvoiceStatus,

		CASE
			WHEN PN.BillAddress1 IS NULL 
				OR PN.BillISOCountryCode2 IS NULL 
				OR PN.BillMunicipality IS NULL 
				OR PN.ContractingTypeID = 0 
				OR 
					(
					(SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company' 
						AND PN.SendInvoicesFrom IS NULL 
						AND PN.OwnCompanyName IS NULL
					)
			THEN 1
			ELSE 0
		END AS IsAddressUpdateRequired,

		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PINT.PersonInvoiceNumberTypeCode,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.CellPhone,
		PN.ContractingTypeID,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		PN.OwnCompanyName,
		PN.SendInvoicesFrom,
		PN.TaxID,
		PN.TaxRate,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		PN.PersonInvoiceNumberIncrement,
		PN.PersonInvoiceNumberPrefix,

		CASE
			WHEN (SELECT CT.ContractingTypeCode FROM dropdown.ContractingType CT WHERE CT.ContractingTypeID = PN.ContractingTypeID) = 'Company'
			THEN ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) 
			ELSE person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast')
		END AS SendInvoicesFrom,

		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID,
		PN.TaxRate
	FROM project.Project PJ
	CROSS JOIN person.Person PN
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND PJ.ProjectID = @ProjectID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = PN.PersonInvoiceNumberTypeID
			AND PN.PersonID = @PersonID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDateWithDay(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoiceExpenseLogSummary
	;
	WITH IEL AS 
		(
		SELECT
			TEL.ClientCostCodeName,
			SUM(TEL.ExchangeRate * TEL.ExpenseAmount) AS ExpenseAmount,
			SUM(TEL.ExchangeRate * TEL.TaxAmount) AS TaxAmount,
			COUNT(DISTINCT TEL.ExpenseDate) AS DaysCount
		FROM @tExpenseLog TEL
		GROUP BY TEL.ClientCostCodeName, TEL.ExpenseDate
		)

	SELECT
		IEL.ClientCostCodeName,
		CAST(SUM(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS ExpenseAmount,
		CAST(SUM(IEL.TaxAmount) AS NUMERIC(18,2)) AS TaxAmount,
		SUM(IEL.DaysCount) AS DaysCount,
		CAST(MIN(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MinExpenseAmount,
		CAST(MAX(IEL.ExpenseAmount) AS NUMERIC(18,2)) AS MaxExpenseAmount,
		CAST(SUM(IEL.ExpenseAmount) / SUM(IEL.DaysCount) AS NUMERIC(18,2)) AS AvgExpenseAmount
	FROM IEL
	GROUP BY IEL.ClientCostCodeName, IEL.DaysCount
	ORDER BY 1

	--InvoicePerDiemLog
	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		TPL.DPAAmount AS Amount,
		TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DPAExchangeRate AS ExchangeRate,
		TPL.DPAAmount * TPL.DPAExchangeRate AS InvoiceAmount,
		TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DPAAmount * TPL.DPAExchangeRate) + (TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	UNION

	SELECT
		core.FormatDateWithDay(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		TPL.DSAAmount AS Amount,
		TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DSAExchangeRate AS ExchangeRate,
		TPL.DSAAmount * TPL.DSAExchangeRate AS InvoiceAmount,
		TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DSAAmount * TPL.DSAExchangeRate) + (TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	ORDER BY 1, 3, 2, 8

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DPAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DPAAmount) AS Amount,
		SUM(TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) + SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DPAISOCurrencyCode

	UNION

	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DSAAmount) AS Amount,
		SUM(TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) + SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 3

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDateWithDay(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		NULL AS LaborCode,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS InvoiceAmount,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(TEL.ExpenseAmount * TEL.ExchangeRate) AS InvoiceAmount,
			SUM(TEL.TaxAmount * TEL.ExchangeRate) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.IsDefault
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceReviewData
EXEC utility.DropObject 'invoice.GetInvoiceReviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoiceReviewData

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	EXEC invoice.GetInvoiceByInvoiceID @InvoiceID

	--InvoiceExpenseLog
	EXEC invoice.GetInvoiceExpenseLogByInvoiceID @InvoiceID

	--InvoiceExpenseLogSummary
	EXEC invoice.GetInvoiceExpenseLogSummaryByInvoiceID @InvoiceID

	--InvoicePerDiemLog
	EXEC invoice.GetInvoicePerDiemLogByInvoiceID @InvoiceID

	--InvoicePerDiemLogSummary
	EXEC invoice.GetInvoicePerDiemLogSummaryByInvoiceID @InvoiceID

	--InvoiceTimeLog
	EXEC invoice.GetInvoiceTimeLogByInvoiceID @InvoiceID

	--InvoiceTimeLogSummary
	EXEC invoice.GetInvoiceTimeLogSummaryByInvoiceID @InvoiceID

	--InvoiceTotals
	EXEC invoice.GetInvoiceTotalsByInvoiceID @InvoiceID

	--InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowDataByInvoiceID @InvoiceID

END
GO
--End procedure invoice.GetInvoiceReviewData

--Begin procedure invoice.GetInvoiceWorkflowPeopleByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowPeopleByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get invoice workflow data
-- ============================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowPeopleByInvoiceID

@InvoiceID INT,
@IsInWorkflow BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceWorkflowPeople
	IF @IsInWorkflow = 1
		EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			ISNULL(core.NullIfEmpty(PP.ManagerName), 'Name Not Provided') AS FullName,
			PP.ManagerEmailAddress AS EmailAddress,
			0 AS IsComplete,
			1 AS CanGetWorkflowEmail
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.ManagerEmailAddress IS NOT NULL
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonProjectTime PPT
					WHERE PPT.InvoiceID = I.InvoiceID
						AND PPT.PersonProjectID = PP.PersonProjectID
					)

		UNION

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			ISNULL(core.NullIfEmpty(PP.AlternateManagerName), 'Name Not Provided') AS FullName,
			PP.AlternateManagerEmailAddress AS EmailAddress,
			0 AS IsComplete,
			1 AS CanGetWorkflowEmail
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.AlternateManagerEmailAddress IS NOT NULL
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonProjectTime PPT
					WHERE PPT.InvoiceID = I.InvoiceID
						AND PPT.PersonProjectID = PP.PersonProjectID
					)

		ORDER BY 1, 3

		END
	--ENDIF

END
GO
--End procedure invoice.GetInvoiceWorkflowPeopleByInvoiceID

--Begin procedure person.AcceptPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.AcceptPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.15
-- Description:	A stored procedure to set the AcceptedDate in the person.PersonProject table
-- ==========================================================================================
CREATE PROCEDURE person.AcceptPersonProjectByPersonProjectID

@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nPersonID INT = (SELECT PP.PersonID FROM person.PersonProject PP WHERE PP.PersonProjectID = @PersonProjectID)

	UPDATE PP
	SET PP.AcceptedDate = getDate()
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID

	EXEC eventlog.LogPersonProjectAction @PersonProjectID, 'update', @nPersonID, 'Assignment Accepted'

	EXEC person.GetPersonProjectEmailTemplateDataByPersonProjectID @PersonProjectID

/*
	SELECT
		PP.ManagerEmailAddress AS EmailAddress
	FROM person.PersonProject PP
	WHERE PP.PersonProjectID = @PersonProjectID
		AND PP.ManagerEmailAddress IS NOT NULL

	UNION
*/

	SELECT
		P.EmailAddress
	FROM person.Person P
		JOIN project.ProjectPerson PP1 ON PP1.PersonID = P.PersonID
		JOIN person.PersonProject PP2 ON PP2.ProjectID = PP1.ProjectID
			AND PP1.ProjectPersonRoleCode = 'ProjectManager'
			AND PP2.PersonProjectID = @PersonProjectID
			AND P.EmailAddress IS NOT NULL

	ORDER BY 1

END
GO
--End procedure person.AcceptPersonProjectByPersonProjectID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.NationalInsuranceNumber,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonJSONDataByPersonID
EXEC utility.DropObject 'person.GetPersonJSONDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.10
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonJSONDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cJSONData VARCHAR(MAX) = '{}'
	DECLARE @nIsForExport BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM client.ClientPerson CP 
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN person.Person P ON P.PersonID = CP.PersonID
				AND CP.PersonID = @PersonID 
				AND CP.ClientPersonRoleCode = 'Consultant'
				AND C.IntegrationCode = 'Hermis'
				AND P.HasAcceptedTerms = 1
		)
		BEGIN

		SET @nIsForExport = 1
		SET @cJSONData = '['

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				G.GenderCode,
				P.BirthDate,
				P.CellPhone,
				P.ChestSize,
				P.Citizenship1ISOCountryCode2,
				P.Citizenship2ISOCountryCode2,
				P.CollarSize,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.CountryCallingCodeID) AS CountryCallingCode,
				P.EmailAddress,
				P.FirstName,
				P.HeadSize,
				P.Height,
				P.HomePhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.HomePhoneCountryCallingCodeID) AS HomePhoneCountryCallingCode,
				CAST(P.IsPhoneVerified AS INT) AS IsPhoneVerified,
				CAST(P.IsUKEUNational AS INT) AS IsUKEUNational,
				P.LastName,
				P.MailAddress1,
				P.MailAddress2,
				P.MailAddress3,
				P.MailISOCountryCode2,
				P.MailMunicipality,
				P.MailPostalCode,
				P.MailRegion,
				P.MiddleName,
				P.Password,
				P.PasswordExpirationDateTime,
				P.PasswordSalt,
				P.PlaceOfBirthISOCountryCode2,
				P.PlaceOfBirthMunicipality,
				P.PreferredName,
				P.Suffix,
				P.SummaryBiography,
				P.Title,
				P.UserName,
				P.WorkPhone,
				core.GetCountryCallingCodeByCountryCallingCodeID(P.WorkPhoneCountryCallingCodeID) AS WorkPhoneCountryCallingCode
			FROM person.Person P
				JOIN dropdown.Gender G ON G.GenderID = P.GenderID
					AND P.PersonID = @PersonID
			FOR JSON PATH, ROOT('Person'), Include_Null_Values
			),'{"Person":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				CR.ClientRosterCode
			FROM person.PersonClientRoster PCR
				JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
					AND PCR.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonClientRoster'), Include_Null_Values
			),'{"PersonClientRoster":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				DE.EntityID AS DAPersonID,
				DE.EntityTypeCode,
				DE.EntityTypeSubCode,
				D.ContentSubtype, 
				D.ContentType, 
				D.CreateDateTime,
				CAST('' AS XML).value('xs:base64Binary(sql:column("D.DocumentData"))', 'VARCHAR(MAX)') AS DocumentData,
				D.DocumentDate, 
				D.DocumentDescription, 
				D.DocumentGUID, 
				D.DocumentTitle, 
				D.Extension, 
				D.PhysicalFileSize, 
				D.ThumbnailData, 
				D.ThumbnailSize
			FROM document.Document D
				JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
					AND DE.EntityTypeCode = 'Person'
					AND DE.EntityTypeSubCode = 'CV'
					AND DE.EntityID = @PersonID
			FOR JSON PATH, ROOT('PersonDocument'), Include_Null_Values
			),'{"PersonDocument":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				PL.ISOLanguageCode2,
				LP1.LanguageProficiencyCode AS SpeakingLanguageProficiencyCode,
				LP2.LanguageProficiencyCode AS ReadingLanguageProficiencyCode,
				LP3.LanguageProficiencyCode AS WritingLanguageProficiencyCode
			FROM person.PersonLanguage PL
				JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
				JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
				JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
					AND PL.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonLanguage'), Include_Null_Values
			),'{"PersonLanguage":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PP.PassportExpirationDate,
				PP.PassportIssuer,
				PP.PassportNumber,
				PT.PassportTypeCode
			FROM person.PersonPassport PP
				JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
					AND PP.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPassport'), Include_Null_Values
			),'{"PersonPassport":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PPS.PasswordSecurityQuestionAnswer,
				PSQ.PasswordSecurityQuestionCode
			FROM person.PersonPasswordSecurity PPS
				JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
					AND PPS.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonPasswordSecurity'), Include_Null_Values
			),'{"PersonPasswordSecurity":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT 
				@PersonID AS DAPersonID,
				PQA.Degree,
				PQA.GraduationDate,
				PQA.Institution,
				PQA.StartDate,
				PQA.SubjectArea
			FROM person.PersonQualificationAcademic PQA
			WHERE PQA.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationAcademic'), Include_Null_Values
			),'{"PersonQualificationAcademic":[]}')

		SET @cJSONData += ','

		SET @cJSONData += ISNULL(
			(
			SELECT
				@PersonID AS DAPersonID,
				PQC.CompletionDate,
				PQC.Course,
				PQC.ExpirationDate,
				PQC.Provider
			FROM person.PersonQualificationCertification PQC
			WHERE PQC.PersonID = @PersonID
			FOR JSON PATH, ROOT('PersonQualificationCertification'), Include_Null_Values
			),'{"PersonQualificationCertification":[]}')

		SET @cJSONData += ']'

		END
	--ENDIF

	SELECT 
		@cJSONData AS JSONData,
		@nIsForExport AS IsForExport

	SELECT
		SS.SystemSetupKey,
		SS.SystemSetupValue
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupKey IN ('HermisAPIKey', 'HermisAPISecretKey', 'HermisAPIURL', 'IsHermisIntegrationEnabled')

END
GO
--End procedure person.GetPersonJSONDataByPersonID

--Begin procedure person.GetPersonInviteRolesByClientID
EXEC utility.DropObject 'person.GetPersonInviteRolesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return invitable roles for a person / client combination
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonInviteRolesByClientID

@ClientID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH CPD AS
		(
		SELECT CP.ClientPersonRoleCode
		FROM client.ClientPerson CP
		WHERE CP.ClientID = @ClientID
			AND CP.PersonID = @PersonID
		)

	SELECT
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'Administrator'
		AND EXISTS
			(
			SELECT 1
			FROM person.Person P
			WHERE P.PersonID = @PersonID
				AND P.IsSuperAdministrator = 1
			)

	UNION

	SELECT 
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'ProjectManager'
		AND EXISTS
			(
			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'Administrator'

			UNION

			SELECT 1
			FROM person.Person P
			WHERE P.PersonID = @PersonID
				AND P.IsSuperAdministrator = 1
			)

	UNION

	SELECT 
		R.DisplayOrder, 
		R.RoleID,
		R.RoleName
	FROM dropdown.Role R
	WHERE R.RoleID > 0
		AND	R.RoleCode = 'Consultant'
		AND EXISTS
			(
			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'Administrator'

			UNION

			SELECT 1
			FROM CPD
			WHERE CPD.ClientPersonRoleCode = 'ProjectManager'

			UNION

			SELECT 1
			FROM person.Person P
			WHERE P.PersonID = @PersonID
				AND P.IsSuperAdministrator = 1
			)

	ORDER BY 1, 3, 2			

END
GO
--End procedure person.GetPersonInviteRolesByClientID

--Begin procedure person.ResolvePendingAction
EXEC utility.DropObject 'person.ResolvePendingAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.31
-- Description:	A stored procedure to manage data in the client.ClientPerson table
-- ===============================================================================
CREATE PROCEDURE person.ResolvePendingAction

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cClientName VARCHAR(500)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cPersonNameFormatted VARCHAR(500)
	DECLARE @cRoleName VARCHAR(50)

	SELECT 
		@cClientName = C.ClientName,
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(CP.PersonID, 'FirstLast'),
		@cRoleName = R.RoleName
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN person.Person P ON P.PersonID = CP.CreatePersonID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.ClientPersonID = @EntityID

	IF @EntityTypeCode = 'AcceptClientPerson'
		UPDATE CP SET CP.AcceptedDate = getDate() FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	ELSE
		DELETE CP FROM client.ClientPerson CP WHERE CP.ClientPersonID = @EntityID
	--ENDIF

	SELECT 
		@cClientName AS ClientName,
		@cEmailAddress AS EmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		@cPersonNameFormatted AS PersonNameFormatted,
		@cRoleName AS RoleName

END
GO
--End procedure person.ResolvePendingAction

--Begin procedure reporting.GetInvoiceExpenseSummaryDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceExpenseSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice expense data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceExpenseSummaryDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CC.ClientCostCodeName,
		CC.ClientCostCodeDescription 
			+ ' ' 
			+ CASE 
					WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (1,2,3) THEN 'P3'
					WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (4,5,6) THEN 'P4'
					WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (7,8,9) THEN 'P1'
					WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (10,11,12) THEN 'P2' 
				END AS ClientCostCodeDescription,
		P.ProjectCode + '.' + PTR.ProjectTermOfReferenceCode AS ProjectChargeCode,
		C.ClientCode,
		C.ClientCode + PN.UserName AS VendorID,
		CAST(SUM(PPE.ExchangeRate * PPE.ExpenseAmount) AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(SUM(PPE.ExchangeRate * PPE.TaxAmount) AS NUMERIC(18,2)) AS InvoiceTaxAmount
	FROM invoice.Invoice I
		JOIN person.PersonProjectExpense PPE ON PPE.InvoiceID = I.InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTR ON PTR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
		JOIN client.ClientCostCode CC ON CC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN client.Client C ON C.ClientID = CC.ClientID 
		JOIN person.Person PN ON PN.PersonID = PP.PersonID
			AND I.InvoiceID = @InvoiceID AND CC.ClientCostCodeName != '532-001-006'
	GROUP BY C.ClientCode, CC.ClientCostCodeName, CC.ClientCostCodeDescription, P.ProjectCode, PTR.ProjectTermOfReferenceCode, PN.UserName, I.InvoiceDateTime
	ORDER BY CC.ClientCostCodeName, P.ProjectCode, PTR.ProjectTermOfReferenceCode

END
GO
--End procedure reporting.GetInvoiceExpenseSummaryDataByInvoiceID

--Begin procedure reporting.GetInvoiceLivingAllowanceDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceLivingAllowanceDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.06.05
-- Description:	A stored procedure to return invoice living allowance data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceLivingAllowanceDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CC.ClientCostCodeName,
		CC.ClientCostCodeDescription + ' ' + CASE WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (1,2,3) THEN 'P3'
		     WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (4,5,6) THEN 'P4'
			 WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (7,8,9) THEN 'P1'
			 WHEN MONTH(core.FormatDate(I.InvoiceDateTime))  IN (10,11,12) THEN 'P2' END AS ClientCostCodeDescription,
		P.ProjectCode + '.' + PTR.ProjectTermOfReferenceCode AS ProjectChargeCode,
		C.ClientCode,
		C.ClientCode + PN.UserName AS VendorID,
		CAST(SUM(PPE.ExchangeRate * PPE.ExpenseAmount) AS NUMERIC(18,2)) AS LivingAllowanceAmount,
		CAST(SUM(PPE.ExchangeRate * PPE.TaxAmount) AS NUMERIC(18,2)) AS LivingAllowanceTaxAmount
	FROM invoice.Invoice I
		JOIN person.PersonProjectExpense PPE ON PPE.InvoiceID = I.InvoiceID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN project.ProjectTermOfReference PTR ON PTR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID 
		JOIN client.ClientCostCode CC ON CC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN client.Client C ON C.ClientID = CC.ClientID 
		JOIN person.Person PN ON PN.PersonID = PP.PersonID
		AND I.InvoiceID = @InvoiceID AND CC.ClientCostCodeName = '532-001-006'
	GROUP BY C.ClientCode, CC.ClientCostCodeName, CC.ClientCostCodeDescription, P.ProjectCode, PTR.ProjectTermOfReferenceCode, PN.UserName, I.InvoiceDateTime
	ORDER BY CC.ClientCostCodeName, P.ProjectCode, PTR.ProjectTermOfReferenceCode

END
GO
--End procedure reporting.GetInvoiceLivingAllowanceDataByInvoiceID

--Begin procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Inderjeet Kaur
-- Create date:	2017.12.07
-- Description:	A stored procedure to return invoice per diem data
-- ===============================================================
CREATE PROCEDURE reporting.GetInvoicePerDiemLogSummaryByInvoiceID 

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH IPL AS 
		(
		SELECT
			C.ClientCode,
			C.ClientCode + PN.UserName AS VendorID,
			P.ProjectCode+ '.' + PTOR.ProjectTermOfReferenceCode AS ProjectChargeCode, 
			C.FinanceCode2 AS DPACode,
			CAST((CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END) * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDPAVAT,
			C.FinanceCode3 AS DSACode,
			PPT.DSAAmount,
			CAST(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDSAVAT
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
	    JOIN person.Person PN ON PN.PersonID = I.PersonID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	SELECT
		D.ClientCode,
		D.VendorID,
		D.ProjectChargeCode,
		D.PerdiemType,
		D.Code,
		CAST(SUM(D.Amount) AS NUMERIC(18,2)) AS Amount ,
		CAST(SUM(D.VAT) AS NUMERIC(18,2)) AS VAT
	FROM
		(
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DPA' AS PerdiemType,
			IPL.DPACode AS Code,
			IPL.InvoiceDPAAmount AS Amount,
			IPL.InvoiceDPAVAT AS VAT
		FROM IPL

		UNION ALL
	
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DSA' AS PerdiemType,
			IPL.DSACode AS Code,
			IPL.InvoiceDSAAmount AS Amount,
			IPL.InvoiceDSAVAT AS VAT
		FROM IPL
		) D 
	GROUP BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode, D.Code
	ORDER BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode

END
GO
--End procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure reporting.GetInvoiceSummaryDataByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoiceSummaryDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.18
-- Description:	A stored procedure to return invoice summary data
-- ==============================================================
CREATE PROCEDURE reporting.GetInvoiceSummaryDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.ClientCode + PN.UserName AS VendorID,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDate,
		I.InvoiceID,
		I.ISOCurrencyCode,
		LEFT(person.FormatPersonNameByPersonID(I.PersonID, 'FirstLast'), 40) AS VendorLongName,
		PA.IntermediateSWIFTCode,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalSWIFTCode,
		PN.EmailAddress,
		PN.IsRegisteredForUKTax,
		CT.ContractingTypeCode,		
		R.RoleName
	FROM invoice.Invoice I
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN client.ClientPerson CP ON C.ClientID = CP.ClientID AND CP.PersonID = I.PersonID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
		JOIN person.PersonAccount PA ON PA.PersonAccountID = I.PersonAccountID
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = PN.ContractingTypeID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure reporting.GetInvoiceSummaryDataByInvoiceID