USE DeployAdviser
GO

--Begin table invoice.InvoicePerDiemLog
DECLARE @TableName VARCHAR(250) = 'invoice.InvoicePerDiemLog'

EXEC utility.DropObject @TableName

CREATE TABLE invoice.InvoicePerDiemLog
	(
	InvoicePerDiemLogID INT NOT NULL IDENTITY(1,1),
	InvoiceID INT,
	DateWorked DATE,
	ProjectLocationName VARCHAR(100),
	DPAAmount NUMERIC(18,2),
	DPAISOCurrencyCode CHAR(3),
	DPAExchangeRate NUMERIC(18,5),
	DSAAmount NUMERIC(18,2),
	DSAISOCurrencyCode CHAR(3),
	DSAExchangeRate NUMERIC(18,5)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DPAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DPAExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DSAExchangeRate', 'NUMERIC(18,5)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'InvoicePerDiemLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_InvoicePerDiemLog', 'InvoiceID,DateWorked'
GO
--End table invoice.InvoicePerDiemLog

