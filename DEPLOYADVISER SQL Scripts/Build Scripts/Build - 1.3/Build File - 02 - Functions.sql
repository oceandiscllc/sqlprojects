USE DeployAdviser
GO

--Begin function core.GetDatesFromRange
EXEC utility.DropObject 'core.GetDatesFromRange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A function to return a table with dates based on a start end dates
-- ===============================================================================

CREATE FUNCTION core.GetDatesFromRange
(
@StartDate DATE,
@EndDate DATE
)

RETURNS @tTable TABLE (ID INT IDENTITY(1,1) PRIMARY KEY, DateValue DATE)  

AS
BEGIN

	DECLARE @dDate1 DATE
	DECLARE @dDate2 DATE

	SELECT
		@dDate1 = CASE WHEN @StartDate <= @EndDate THEN @StartDate ELSE @EndDate END,
		@dDate2 = CASE WHEN @StartDate <= @EndDate THEN @EndDate ELSE @StartDate END

	;
	WITH D AS
		(
		SELECT @dDate1 AS DateValue

	  UNION ALL

	  SELECT DATEADD(d, 1, D.DateValue)
		FROM D
		WHERE DATEADD(d, 1, D.DateValue) <= @dDate2
		)

	INSERT INTO @tTable 
		(DateValue) 
	SELECT D.DateValue 
	FROM D
	ORDER BY D.DateValue
	
	RETURN

END
GO
--End function core.GetDatesFromRange

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	RETURN IIF(@String IS NULL OR LEN(RTRIM(@String)) = 0, NULL, @String)

END
GO
--End function core.NullIfEmpty

--Begin function person.GetClientsByPersonID
EXEC utility.DropObject 'person.GetClientsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all clients accessible to a PersonID
-- ====================================================================================================

CREATE FUNCTION person.GetClientsByPersonID
(
@PersonID INT,
@ClientPersonRoleCode VARCHAR(250)
)

RETURNS @tTable TABLE (ClientID INT NOT NULL PRIMARY KEY, ClientName VARCHAR(250))  

AS
BEGIN

	SET @ClientPersonRoleCode = core.NullIfEmpty(@ClientPersonRoleCode)

	INSERT INTO @tTable
		(ClientID, ClientName)
	SELECT 
		CP.ClientID,
		C.ClientName
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
			AND 
				(
				@ClientPersonRoleCode IS NULL
					OR EXISTS
						(
						SELECT 1
						FROM core.ListToTable(@ClientPersonRoleCode, ',') LTT
						WHERE LTT.ListItem = CP.ClientPersonRoleCode
						)
				)

	UNION

	SELECT 
		C.ClientID,
		C.ClientName
	FROM client.Client C
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person P
		WHERE P.PersonID = @PersonID
			AND P.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetClientsByPersonID

--Begin function person.GetPersonProjectDaysBilled
EXEC utility.DropObject 'person.GetPersonProjectDaysBilled'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.06
-- Description:	A function to return a count of the days billed on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysBilled
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,5)

AS
BEGIN

	DECLARE @nDaysBilled NUMERIC(18,2)

	SELECT @nDaysBilled = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND PPT.InvoiceID = 1

	RETURN ISNULL(@nDaysBilled, 0)
	
END
GO
--End function person.GetPersonProjectDaysBilled

--Begin function person.GetPersonProjectDaysNotBilled
EXEC utility.DropObject 'person.GetPersonProjectDaysNotBilled'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.06
-- Description:	A function to return a count of the days billed on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysNotBilled
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,5)

AS
BEGIN

	DECLARE @nDaysNotBilled NUMERIC(18,2)

	SELECT @nDaysNotBilled = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND PPT.InvoiceID = 0

	RETURN ISNULL(@nDaysNotBilled, 0)
	
END
GO
--End function person.GetPersonProjectDaysNotBilled

--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL PRIMARY KEY)  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID)
	SELECT 
		PP.ProjectID
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		P.ProjectID
	FROM project.Project P
	WHERE EXISTS	
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode IN ('Administrator', 'ProjectManager')
			AND CP.PersonID = @PersonID
		)

	UNION

	SELECT 
		PJ.ProjectID
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID