USE DeployAdviser
GO

--Begin function invoice.GetInvoiceRecordsByPersonID
EXEC utility.DropObject 'invoice.GetInvoiceRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.26
-- Description:	A function to return a table with the InvoiceID of all invoices accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION invoice.GetInvoiceRecordsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (InvoiceID INT NOT NULL, CanHaveList BIT, CanHavePDF BIT, CanHaveView BIT, CanHaveVoucher BIT)

AS
BEGIN

	WITH CP AS 
		(
		SELECT
			CP.ClientID,
			0 AS ProjectID,
			CP.ClientPersonRoleCode
		FROM client.ClientPerson CP
		WHERE CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.AcceptedDate IS NOT NULL

		UNION

		SELECT
			P.ClientID,
			P.ProjectID,
			'ProjectManager' AS ClientPersonRoleCode
		FROM project.Project P
			JOIN project.ProjectPerson PP ON PP.ProjectID = P.ProjectID
				AND PP.PersonID = @PersonID
				AND EXISTS
					(
					SELECT 1
					FROM client.ClientPerson CP
					WHERE CP.PersonID = PP.PersonID
						AND CP.ClientPersonRoleCode = 'ProjectManager'
						AND CP.AcceptedDate IS NOT NULL
					)
		)

	INSERT INTO @tTable
		(InvoiceID, CanHaveList, CanHavePDF, CanHaveView, CanHaveVoucher)
	SELECT
		I.InvoiceID,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHaveList,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHavePDF,

		CASE
			WHEN I.PersonID = @PersonID
				OR ((I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected') AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1)
				OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
				OR person.IsSuperAdministrator(@PersonID) = 1 
			THEN 1
			ELSE 0
		END AS CanHaveView,

		CASE
			WHEN I.InvoiceStatus NOT IN ('Line Manager Review','Rejected') 
				AND
					(
					(I.InvoiceStatus = 'Approved' AND workflow.IsPersonInCurrentWorkflow('Invoice', I.InvoiceID, @PersonID) = 1) 
						OR workflow.IsPersonInCurrentWorkflowStep('Invoice', I.InvoiceID, @PersonID) = 1
						OR EXISTS (SELECT 1 FROM CP WHERE CP.ClientID = P.ClientID AND CP.ClientPersonRoleCode = 'Administrator')
						OR person.IsSuperAdministrator(@PersonID) = 1 
					)
			THEN 1
			ELSE 0
		END AS CanHaveVoucher

	FROM invoice.Invoice I
		JOIN project.Project P ON P.ProjectID = I.ProjectID
			AND 
				(
				I.PersonID = @PersonID
					OR person.IsSuperAdministrator(@PersonID) = 1
					OR EXISTS
						(
						SELECT 1
						FROM CP
						WHERE CP.ClientID = P.ClientID
							AND 
								(
								CP.ClientPersonRoleCode = 'Administrator'
									OR CP.ProjectID = P.ProjectID
								)
						)
				)
	
	RETURN

END
GO
--End function invoice.GetInvoiceRecordsByPersonID

--Begin function person.GetProjectsByPersonID
EXEC utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A function to return a table with the ProjectID of all projects accessible to a PersonID
-- =====================================================================================================

CREATE FUNCTION person.GetProjectsByPersonID
(
@PersonID INT
)

RETURNS @tTable TABLE (ProjectID INT NOT NULL, RoleCode VARCHAR(50))  

AS
BEGIN

	INSERT INTO @tTable
		(ProjectID, RoleCode)
	SELECT 
		PP.ProjectID, 
		'Consultant'
	FROM person.PersonProject PP
	WHERE PP.PersonID = @PersonID

	UNION

	SELECT 
		PP.ProjectID, 
		'ProjectManager'
	FROM project.ProjectPerson PP
	WHERE PP.PersonID = @PersonID	

	UNION

	SELECT 
		P.ProjectID, 
		'Administrator'
	FROM project.Project P
	WHERE EXISTS	
		(
		SELECT 1
		FROM client.ClientPerson CP 
		WHERE CP.ClientID = P.ClientID
			AND CP.ClientPersonRoleCode = 'Administrator'
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NOT NULL
		)

	UNION

	SELECT 
		PJ.ProjectID, 
		'SuperAdministrator'
	FROM project.Project PJ
	WHERE EXISTS
		(
		SELECT 1
		FROM person.Person PN
		WHERE PN.PersonID = @PersonID
			AND PN.IsSuperAdministrator = 1
		)
	
	RETURN

END
GO
--End function person.GetProjectsByPersonID

--Begin function workflow.IsPersonInCurrentWorkflow
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.26
-- Description:	A function to determine if a PeronID is in any step of the workflow for a specific EntityTypeCode and EntityID
-- ===========================================================================================================================
CREATE FUNCTION workflow.IsPersonInCurrentWorkflow
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflow BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
		)
		SET @bIsPersonInCurrentWorkflow = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflow
	
END
GO
--End function workflow.IsPersonInCurrentWorkflow