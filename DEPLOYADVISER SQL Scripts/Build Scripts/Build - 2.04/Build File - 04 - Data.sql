USE DeployAdviser
GO

--Begin table document.DocumentEntity
UPDATE DE
SET DE.EntityTypeSubCode = 'CV'
FROM document.DocumentEntity DE
WHERE DE.EntityTypeCode = 'Person'
	AND DE.EntityTypeSubCode IS NULL
GO
--End table document.DocumentEntity

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'SessionTimeOut', 'The duration of the session timeout in seconds', '7200'
GO
--End table core.SystemSetup
