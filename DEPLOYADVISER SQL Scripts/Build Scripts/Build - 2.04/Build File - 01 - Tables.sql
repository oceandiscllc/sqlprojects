USE DeployAdviser
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.AddColumn @TableName, 'StartDateTime', 'DATETIME'
EXEC utility.AddColumn @TableName, 'EndDateTime', 'DATETIME'

EXEC utility.DropIndex 'core.Announcement', 'IX_Announcement'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDateTime,EndDateTime'
GO

DECLARE @TableName VARCHAR(250) = 'core.Announcement'

IF utility.HasColumn(@TableName, 'EndDate') = 1 AND utility.HasColumn(@TableName, 'StartDate') = 1
	BEGIN

	UPDATE A
	SET 
		A.EndDateTime = A.EndDate,
		A.StartDateTime = A.StartDate
	FROM core.Announcement A

	EXEC utility.DropColumn @TableName, 'EndDate'
	EXEC utility.DropColumn @TableName, 'StartDate'

	END
--ENDIF
GO
--End table core.Announcement

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nDocumentSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentName), CHARINDEX('.', REVERSE(I.DocumentName)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS DocumentSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nDocumentSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.DocumentSize = CASE WHEN D.DocumentSize IS NULL OR D.DocumentSize = 0 THEN @nDocumentSize ELSE D.DocumentSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.DocumentSize = CASE WHEN D.DocumentSize IS NULL OR D.DocumentSize = 0 THEN @nDocumentSize ELSE D.DocumentSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.DocumentSize = CASE WHEN D.DocumentSize IS NULL OR D.DocumentSize = 0 THEN @nDocumentSize ELSE D.DocumentSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nDocumentSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.AddColumn @TableName, 'DocumentEntityCode', 'VARCHAR(50)'
GO
--End table document.DocumentEntity

--Begin table person.SavedSearch
DECLARE @TableName VARCHAR(250) = 'person.SavedSearch'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.SavedSearch
	(
	SavedSearchID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EntityTypeCode VARCHAR(50),
	SavedSearchName VARCHAR(50),
	SavedSearchData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SavedSearchID'
EXEC utility.SetIndexClustered @TableName, 'IX_SavedSearch', 'PersonID,EntityTypeCode'
GO
--End table person.SavedSearch

