USE DeployAdviserCloud
GO

--Begin procedure reporting.GetInvoiceTimeSummaryWithLineManagertReview
EXEC utility.DropObject 'reporting.GetInvoiceTimeSummaryWithLineManagertReview'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.04.12
-- Description:	A procedure to return invoice time data
-- ====================================================
CREATE PROCEDURE reporting.GetInvoiceTimeSummaryWithLineManagertReview

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.InvoiceID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS ConsultantName,
		I.PersonInvoiceNumber,
		core.FormatDate(PPT.DateWorked) AS [Date], 
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		PP.PersonProjectName,
		TT.TimeTypeName AS TimeType,
		PL.ProjectLocationName AS [Location],
		(SELECT person.GetPersonProjectDays(PPT.PersonProjectID) * P.HoursPerDay) AS Alloted,
		PPT.HoursWorked AS ForApprovalInThisTimeSheet,
		PPT.ProjectManagerNotes AS FootNotes,
		P.ProjectCode,
		P.ProjectName
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN person.PersonProjectTime PPT ON PPT.InvoiceID = I.InvoiceID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND I.InvoiceID = @InvoiceID
		ORDER BY PPT.DateWorked


END
GO
--Begin procedure reporting.GetInvoiceTimeSummaryWithLineManagertReview

--Begin procedure reporting.GetLineManagerApprovalDetails
EXEC Utility.DropObject 'reporting.GetLineManagerApprovalDetails'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.04.12
-- Description:	A procedure to return invoice time data
-- ====================================================
CREATE PROCEDURE reporting.GetLineManagerApprovalDetails

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.InvoiceID,					
		I.LineManagerName,
		core.FormatDate(I.LineManagerActionDateTime)  AS LineManagerActionDateTime,
		EL.Comments
	FROM invoice.Invoice I
		JOIN person.PersonProjectTime PPT ON PPT.InvoiceID = I.InvoiceID
		JOIN eventlog.EventLog EL ON EL.EntityID = I.InvoiceID 
			AND EL.EntityTypeCode = 'Invoice' 
			AND EL.EventCode = 'LineManagerApproved'
			AND I.InvoiceID = @InvoiceID 
			AND PPT.IsLineManagerApproved = 1
	GROUP BY I.InvoiceID, I.LineManagerName, I.LineManagerActionDateTime, EL.Comments 

END
GO
--End procedure reporting.GetLineManagerApprovalDetails

