
--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function core.YesNoFormatWithIcon
EXEC utility.DropObject 'core.YesNoFormatWithIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string with a colored icon
-- ==============================================================================

CREATE FUNCTION core.YesNoFormatWithIcon
(
@Value BIT,
@YesClassName VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN
	
	DECLARE @cClassName VARCHAR(50) = CASE WHEN @Value = 1 THEN @YesClassName WHEN @YesClassName = 'btn-success' THEN 'btn-danger' ELSE 'btn-success' END
	DECLARE @cValue VARCHAR(3) = CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END
	DECLARE @cHTML VARCHAR(200) = '<a class="btn btn-xs btn-icon btn-circle ' + @cClassName + ' m-r-10" title="' + RTRIM(@cValue) + '"></a>'

	RETURN @cHTML + @cValue

END
GO
--End function core.YesNoFormatWithIcon

--Begin function person.GetPersonProjectDaysInvoiced
EXEC utility.DropObject 'person.GetPersonProjectDaysInvoiced'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Invoiced on a PersonProject record
-- ========================================================================================

CREATE FUNCTION person.GetPersonProjectDaysInvoiced
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysInvoiced NUMERIC(18,2)

	SELECT @nDaysInvoiced = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND I.InvoiceStatus <> 'Approved'
			AND PPT.InvoiceID > 0

	RETURN ISNULL(@nDaysInvoiced, 0)
	
END
GO
--End function person.GetPersonProjectDaysInvoiced

--Begin function person.GetPersonProjectDaysLogged
EXEC utility.DropObject 'person.GetPersonProjectDaysLogged'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days logged on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysLogged
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysLogged NUMERIC(18,2)

	SELECT @nDaysLogged = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND PPT.InvoiceID = 0

	RETURN ISNULL(@nDaysLogged, 0)
	
END
GO
--End function person.GetPersonProjectDaysLogged
