--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropColumn @TableName, 'HasClientFinanceExport'
EXEC utility.DropColumn @TableName, 'ClientFinanceExportReportFileName'
GO
--End table client.Client

--Begin table client.ClientConfiguration
DECLARE @TableName VARCHAR(250) = 'client.ClientConfiguration'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientConfiguration
	(
	ClientConfigurationID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	ClientConfigurationCategoryCode VARCHAR(50),
	ClientConfigurationKey VARCHAR(50),
	ClientConfigurationDescription VARCHAR(250),
	ClientConfigurationValue VARCHAR(MAX), 
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientConfigurationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientConfiguration', 'ClientID'
GO

EXEC utility.DropObject 'client.TR_ClientConfiguration'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.25
-- Description:	A trigger to add records to the client.ClientConfiguration table
-- =============================================================================
CREATE TRIGGER client.TR_ClientConfiguration ON client.ClientConfiguration AFTER INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED I WHERE I.ClientID = 0)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	SELECT
		C.ClientID,
		I.ClientConfigurationCategoryCode,
		I.ClientConfigurationKey,
		I.ClientConfigurationDescription,
		I.ClientConfigurationValue,
		I.DisplayOrder
	FROM client.Client C, INSERTED I
	ORDER BY 1, 2

	END
--ENDIF
GO
--End table client.ClientConfiguration

--Begin table client.Client
--Put the trigger here so that the client.ClientConfiguration is created before this opbejct is to avoid timeing issues at script run time
EXEC utility.DropObject 'client.TR_Client'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.25
-- Description:	A trigger to add records to the client.ClientConfiguration table
-- =============================================================================
CREATE TRIGGER client.TR_Client ON client.Client AFTER INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	SELECT
		I.ClientID,
		CC.ClientConfigurationCategoryCode,
		CC.ClientConfigurationKey,
		CC.ClientConfigurationDescription,
		CC.ClientConfigurationValue,
		CC.DisplayOrder
	FROM INSERTED I
		CROSS APPLY client.ClientConfiguration CC
	WHERE CC.ClientID = 0

	END
--ENDIF
GO
--End table client.Client

--Begin table core.FAQ
DECLARE @TableName VARCHAR(250) = 'core.FAQ'

EXEC utility.DropObject @TableName

CREATE TABLE core.FAQ
	(
	FAQID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	FAQKey VARCHAR(MAX),
	FAQValue VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FAQID'
EXEC utility.SetIndexClustered @TableName, 'IX_FAQ', 'ClientID'
GO
--End table core.FAQ

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cDocumentGUID VARCHAR(50)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			CAST(ISNULL(I.DocumentGUID, newID()) AS VARCHAR(50)) AS DocumentGUID,
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document