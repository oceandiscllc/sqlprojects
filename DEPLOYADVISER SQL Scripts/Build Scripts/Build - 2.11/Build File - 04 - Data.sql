--Begin table client.ClientConfiguration
IF NOT EXISTS (SELECT 1 FROM client.ClientConfiguration CC WHERE CC.ClientID = 0)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	VALUES
		(0, 'ClientFinanceExport', 'ClientFinanceExportReportFileName', 'The name of the SSRS .rdl file to use for the client finance export', NULL, 0),
		(0, 'ClientFinanceExport', 'HasClientFinanceExport', 'Does the client have a client finance export', 0, 0),
		(0, 'PersonProfile', 'ShowPersonProfileContactDetails', 'Show the contact details tab on the user profile', 1, 1),
		(0, 'PersonProfile', 'ShowPersonProfilePersonalInformation', 'Show the personal information tab on the user profile', 1, 2),
		(0, 'PersonProfile', 'ShowPersonProfileLanguageInformation', 'Show the language information tab on the user profile', 1, 3),
		(0, 'PersonProfile', 'ShowPersonProfileQualificationInformation', 'Show the qualification information tab on the user profile', 1, 4)

	END
--ENDIF
GO

UPDATE CC
SET 
	CC.ClientConfigurationValue = 'ClientFinanceExport_Hermis'
FROM client.ClientConfiguration CC
	JOIN client.Client C ON C.CLientID = CC.ClientID
		AND C.IntegrationCode = 'Hermis'
		AND CC.ClientConfigurationKey = 'ClientFinanceExportReportFileName'
GO

UPDATE CC
SET 
	CC.ClientConfigurationValue = '1'
FROM client.ClientConfiguration CC
	JOIN client.Client C ON C.CLientID = CC.ClientID
		AND C.IntegrationCode = 'Hermis'
		AND CC.ClientConfigurationKey = 'HasClientFinanceExport'
GO
--End table client.ClientConfiguration

--Begin table core.CalendarMetadata
UPDATE CM SET CM.Icon = 'fas fa-fw fa-check-circle' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Availability'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-exclamation-triangle' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'DPA'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-utensils' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'DSA'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-clock' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Time'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-tasks' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Project'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-credit-card' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Expense'
GO
--End table core.CalendarMetadata

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-tachometer-alt',
	@NewMenuItemCode = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'far fa-fw fa-clock',
	@NewMenuItemCode = 'PersonProjectTimeList'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dollar-sign',
	@NewMenuItemCode = 'PersonProjectExpenseList'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-edit',
	@NewMenuItemCode = 'InvoiceSetup'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'InvoiceList',
	@Icon = 'fas fa-fw fa-calendar-alt',
	@NewMenuItemCode = 'PersonCalendar',
	@NewMenuItemLink = '/person/calendar',
	@NewMenuItemText = 'Calendar'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'FAQList',
	@NewMenuItemLink = '/faq/list',
	@NewMenuItemText = 'FAQs',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'FAQ.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'DashboardChartHexColors', NULL, '#4dc9f6,#f67019,#f53794,#537bc4,#acc236,#166a8f,#00a950,#58595b,#8549ba,#d2a4d8'
EXEC core.SystemSetupAddUpdate 'DashboardDayCount', 'The number of days displayed in the dashboard time logged chart', '35'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', 'The logo used for printing', '/assets/img/DA_Logo_White_Big.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', 'The regular sized sit logo', '/assets/img/DA_Logo_White_Med.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', 'The small sized sit logo', '/assets/img/DA_Logo_White_Small.png'
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 'FAQ', 'AddUpdate', NULL, 'FAQ.AddUpdate', 'Administration', 'Add / edit a FAQ', 0, 1, 0, 0
EXEC permissionable.SavePermissionable 'FAQ', 'List', NULL, 'FAQ.List', 'Administration', 'View the list of FAQs', 0, 1, 0, 0
EXEC permissionable.SavePermissionable 'FAQ', 'View', NULL, 'FAQ.View', 'Administration', 'View a FAQ', 0, 1, 0, 0
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Invoice', 
	@DESCRIPTION='Export the client finance integration data', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Invoice.List.ClientFinanceExport', 
	@PERMISSIONCODE='ClientFinanceExport';
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the projects, time, expenses & availability calendar for a user', 
	@ISGLOBAL=1,
	@METHODNAME='Calendar', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.Calendar', 
	@PERMISSIONCODE=NULL;
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Assign permissionable templates to individuals on the person list page', 
	@ISGLOBAL=1,
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.List.PermissionableTemplate', 
	@PERMISSIONCODE='PermissionableTemplate';
GO

EXEC permissionable.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
