-- File Name:	Build - 2.11 - DEPLOYADVISER.sql
-- Build Key:	Build - 2.11 - 2019.01.20 17.15.45

--USE DeployAdviserCloud
GO

-- ==============================================================================================================================
-- Tables:
--		client.ClientConfiguration
--		core.FAQ
--
-- Triggers:
--		client.TR_Client ON client.Client
--		client.TR_ClientConfiguration ON client.ClientConfiguration
--		document.TR_Document ON document.Document
--
-- Functions:
--		core.YesNoFormat
--		core.YesNoFormatWithIcon
--		person.GetPersonProjectDaysInvoiced
--		person.GetPersonProjectDaysLogged
--
-- Procedures:
--		client.GetClientByClientID
--		client.GetClientLeave
--		core.GetFAQByFAQID
--		eventlog.LogFAQAction
--		invoice.GetInvoicePerDiemLogByInvoiceID
--		invoice.GetInvoiceWorkflowDataByInvoiceID
--		person.CheckAccess
--		person.GetCalendarEntriesByPersonID
--		person.GetFAQByPersonID
--		person.GetPendingActionsByPersonID
--		person.GetPersonByPersonID
--		person.GetPersonDashboardDataByPersonID
--		person.GetPersonProjectLeave
--		person.GetPersonProjectTimeJSONDataByPersonID
--		person.TogglePersonUnavailabilityDate
--		person.ValidateLogin
--		project.validateProjectDateAndTimeData
--		reporting.GetClientFinanceExportData_Hermis
--		reporting.GetClientFinanceExportData_Hermis_D
--		reporting.GetClientFinanceExportData_Hermis_H
--		reporting.GetClientFinanceExportData_Hermis_V
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropColumn @TableName, 'HasClientFinanceExport'
EXEC utility.DropColumn @TableName, 'ClientFinanceExportReportFileName'
GO
--End table client.Client

--Begin table client.ClientConfiguration
DECLARE @TableName VARCHAR(250) = 'client.ClientConfiguration'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientConfiguration
	(
	ClientConfigurationID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	ClientConfigurationCategoryCode VARCHAR(50),
	ClientConfigurationKey VARCHAR(50),
	ClientConfigurationDescription VARCHAR(250),
	ClientConfigurationValue VARCHAR(MAX), 
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientConfigurationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientConfiguration', 'ClientID'
GO

EXEC utility.DropObject 'client.TR_ClientConfiguration'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.25
-- Description:	A trigger to add records to the client.ClientConfiguration table
-- =============================================================================
CREATE TRIGGER client.TR_ClientConfiguration ON client.ClientConfiguration AFTER INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED I WHERE I.ClientID = 0)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	SELECT
		C.ClientID,
		I.ClientConfigurationCategoryCode,
		I.ClientConfigurationKey,
		I.ClientConfigurationDescription,
		I.ClientConfigurationValue,
		I.DisplayOrder
	FROM client.Client C, INSERTED I
	ORDER BY 1, 2

	END
--ENDIF
GO
--End table client.ClientConfiguration

--Begin table client.Client
--Put the trigger here so that the client.ClientConfiguration is created before this opbejct is to avoid timeing issues at script run time
EXEC utility.DropObject 'client.TR_Client'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.25
-- Description:	A trigger to add records to the client.ClientConfiguration table
-- =============================================================================
CREATE TRIGGER client.TR_Client ON client.Client AFTER INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	SELECT
		I.ClientID,
		CC.ClientConfigurationCategoryCode,
		CC.ClientConfigurationKey,
		CC.ClientConfigurationDescription,
		CC.ClientConfigurationValue,
		CC.DisplayOrder
	FROM INSERTED I
		CROSS APPLY client.ClientConfiguration CC
	WHERE CC.ClientID = 0

	END
--ENDIF
GO
--End table client.Client

--Begin table core.FAQ
DECLARE @TableName VARCHAR(250) = 'core.FAQ'

EXEC utility.DropObject @TableName

CREATE TABLE core.FAQ
	(
	FAQID INT NOT NULL IDENTITY(1,1),
	ClientID INT,
	FAQKey VARCHAR(MAX),
	FAQValue VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FAQID'
EXEC utility.SetIndexClustered @TableName, 'IX_FAQ', 'ClientID'
GO
--End table core.FAQ

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cDocumentGUID VARCHAR(50)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			CAST(ISNULL(I.DocumentGUID, newID()) AS VARCHAR(50)) AS DocumentGUID,
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function core.YesNoFormatWithIcon
EXEC utility.DropObject 'core.YesNoFormatWithIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string with a colored icon
-- ==============================================================================

CREATE FUNCTION core.YesNoFormatWithIcon
(
@Value BIT,
@YesClassName VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN
	
	DECLARE @cClassName VARCHAR(50) = CASE WHEN @Value = 1 THEN @YesClassName WHEN @YesClassName = 'btn-success' THEN 'btn-danger' ELSE 'btn-success' END
	DECLARE @cValue VARCHAR(3) = CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END
	DECLARE @cHTML VARCHAR(200) = '<a class="btn btn-xs btn-icon btn-circle ' + @cClassName + ' m-r-10" title="' + RTRIM(@cValue) + '"></a>'

	RETURN @cHTML + @cValue

END
GO
--End function core.YesNoFormatWithIcon

--Begin function person.GetPersonProjectDaysInvoiced
EXEC utility.DropObject 'person.GetPersonProjectDaysInvoiced'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days Invoiced on a PersonProject record
-- ========================================================================================

CREATE FUNCTION person.GetPersonProjectDaysInvoiced
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysInvoiced NUMERIC(18,2)

	SELECT @nDaysInvoiced = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND I.InvoiceStatus <> 'Approved'
			AND PPT.InvoiceID > 0

	RETURN ISNULL(@nDaysInvoiced, 0)
	
END
GO
--End function person.GetPersonProjectDaysInvoiced

--Begin function person.GetPersonProjectDaysLogged
EXEC utility.DropObject 'person.GetPersonProjectDaysLogged'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.17
-- Description:	A function to return a count of the days logged on a PersonProject record
-- ======================================================================================

CREATE FUNCTION person.GetPersonProjectDaysLogged
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nDaysLogged NUMERIC(18,2)

	SELECT @nDaysLogged = SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2)))
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND PPT.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND PPT.InvoiceID = 0

	RETURN ISNULL(@nDaysLogged, 0)
	
END
GO
--End function person.GetPersonProjectDaysLogged

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.22
-- Description:	A stored procedure to return data from the client.Client table based on a ClientID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tClientPersonProjectRole TABLE (ClientPersonProjectRoleGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID(), ClientPersonProjectRoleID INT)

	--Client
	SELECT
		C.BillAddress1,
		C.BillAddress2,
		C.BillAddress3,
		C.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.BillISOCountryCode2) AS BillCountryName,
		C.BillMunicipality,
		C.BillPostalCode,
		C.BillRegion,
		C.CanProjectAlterCostCodeDescription,
		C.ClientCode,
		C.ClientID,
		C.ClientLegalEntities,
		C.ClientName,
		C.EmailAddress,
		C.FAX,
		C.FinanceCode1,
		C.FinanceCode2,
		C.FinanceCode3,
		C.FixedAllowanceLabel,
		C.HasFixedAllowance,
		C.HasVariableAllowance,
		C.HREmailAddress,
		C.InvoiceDueReminderInterval,
		C.IsActive,
		C.MailAddress1,
		C.MailAddress2,
		C.MailAddress3,
		C.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(C.MailISOCountryCode2) AS MailCountryName,
		C.MailMunicipality,
		C.MailPostalCode,
		C.MailRegion,
		C.MaximumProductiveDays,
		C.PersonAccountEmailAddress,
		C.Phone,
		C.SendOverdueInvoiceEmails,
		C.TaxID,
		C.TaxRate,
		C.VariableAllowanceLabel,
		C.Website
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientAdministrator
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Administrator'
	ORDER BY 2, 1

	--ClientConfiguration
	SELECT
		CC.ClientConfigurationID,
		CC.ClientConfigurationCategoryCode,
		CC.ClientConfigurationKey,
		CC.ClientConfigurationDescription,
		CC.ClientConfigurationValue
	FROM client.ClientConfiguration CC
	WHERE CC.ClientID = @ClientID
	ORDER BY CC.ClientConfigurationCategoryCode, CC.DisplayOrder, CC.ClientConfigurationDescription, CC.ClientConfigurationID

	--ClientCostCode
	SELECT
		newID() AS ClientCostCodeGUID,
		CCC.ClientCostCodeID,
		CCC.ClientCostCodeDescription,
		CCC.ClientCostCodeName,
		CCC.IsActive
	FROM client.ClientCostCode CCC
	WHERE CCC.ClientID = @ClientID
	ORDER BY CCC.ClientCostCodeName, CCC.ClientCostCodeID

	--ClientCustomer
	SELECT
		newID() AS ClientCustomerGUID,
		CC.ClientCustomerID,
		CC.ClientCustomerName,
		CC.IsActive
	FROM client.ClientCustomer CC
	WHERE CC.ClientID = @ClientID
	ORDER BY CC.ClientCustomerName, CC.ClientCustomerID

	--ClientFunction
	SELECT
		newID() AS ClientFunctionGUID,
		CF.ClientFunctionID,
		CF.ClientFunctionDescription,
		CF.ClientFunctionName,
		CF.IsActive
	FROM client.ClientFunction CF
	WHERE CF.ClientID = @ClientID
	ORDER BY CF.ClientFunctionName, CF.ClientFunctionID

	INSERT INTO @tClientPersonProjectRole 
		(ClientPersonProjectRoleID)
	SELECT
		CPPR.ClientPersonProjectRoleID
	FROM client.ClientPersonProjectRole CPPR
	WHERE CPPR.ClientID = @ClientID
		AND CPPR.ClientPersonProjectRoleID > 0

	--ClientLeave
	EXEC client.GetClientLeave @ClientID

	--ClientPerson
	SELECT DISTINCT
		CP.PayrollNumber,
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'Consultant'
	ORDER BY 3, 2

	--ClientPersonProjectRole
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPR.ClientPersonProjectRoleID,
		CPPR.ClientPersonProjectRoleName,
		CPPR.FinanceCode,
		CPPR.HasFees,
		CPPR.HasFixedAllowance,
		CPPR.HasVariableAllowance,
		CPPR.HasVAT,
		CPPR.InvoiceRDLCode,
		CPPR.IsActive
	FROM client.ClientPersonProjectRole CPPR
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPR.ClientPersonProjectRoleID
	ORDER BY CPPR.ClientPersonProjectRoleName, CPPR.ClientPersonProjectRoleID

	--ClientPersonProjectRoleClientTimeType
	SELECT
		TCPPR.ClientPersonProjectRoleGUID,
		CPPRCTT.ClientTimeTypeID,
		CPPRCTT.LeaveDaysPerAnnum
	FROM client.ClientPersonProjectRoleClientTimeType CPPRCTT
		JOIN @tClientPersonProjectRole TCPPR ON TCPPR.ClientPersonProjectRoleID = CPPRCTT.ClientPersonProjectRoleID
	ORDER BY CPPRCTT.ClientPersonProjectRoleID

	--ClientProjectManager
	SELECT
		CP.PersonID,
		person.FormatPersonNameByPersonID(CP.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.ClientPersonRoleCode = 'ProjectManager'
	ORDER BY 2, 1

	IF @ClientID > 0
		BEGIN

		--ClientTimeType
		SELECT
			CTT.AccountCode,
			CTT.ClientTimeTypeID,
			CTT.HasFixedAllowance,
			CTT.HasLocation,
			CTT.HasVariableAllowance,
			CTT.IsAccruable,
			CTT.IsActive,
			CTT.IsBalancePayableUponTermination,
			CTT.IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM client.ClientTimeType CTT
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
				AND CTT.ClientID = @ClientID
				AND TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	ELSE
		BEGIN

		--ClientTimeType
		SELECT
			NULL AS AccountCode,
			TT.TimeTypeID AS ClientTimeTypeID,
			0 AS HasFixedAllowance,
			0 AS HasLocation,
			0 AS HasVariableAllowance,
			0 AS IsAccruable,
			0 AS IsActive,
			0 AS IsBalancePayableUponTermination,
			0 AS IsValidated,
			TT.TimeTypeID,
			TT.TimeTypeName
		FROM dropdown.TimeType TT
		WHERE TT.TimeTypeCategoryCode = 'LEAVE'
		ORDER BY TT.DisplayOrder, TT.TimeTypeName, TT.TimeTypeID

		END
	--ENDIF

	--ProjectInvoiceTo
	SELECT
		newID() AS ProjectInvoiceToGUID,
		PIT.ClientID,
		PIT.IsActive,
		PIT.ProjectInvoiceToAddress1,
		PIT.ProjectInvoiceToAddress2,
		PIT.ProjectInvoiceToAddress3,
		PIT.ProjectInvoiceToAddressee,
		PIT.ProjectInvoiceToEmailAddress,
		PIT.ProjectInvoiceToID,
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality,
		PIT.ProjectInvoiceToName,
		PIT.ProjectInvoiceToPhone,
		PIT.ProjectInvoiceToPostalCode,
		PIT.ProjectInvoiceToRegion
	FROM client.ProjectInvoiceTo PIT
	WHERE PIT.ClientID = @ClientID
	ORDER BY PIT.ProjectInvoiceToName, PIT.ProjectInvoiceToID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetClientLeave
EXEC utility.DropObject 'client.GetClientLeave'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.08
-- Description:	A stored procedure to return leave data based on an EntityTypeCode and an EntityID
-- ===============================================================================================
CREATE PROCEDURE client.GetClientLeave

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonProjectLeave
	WITH PPT AS
		(
		SELECT
			PPT.PersonProjectID,
			PPT.TimeTypeID,
			SUM(PPT.HoursWorked) AS HoursWorked
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
				AND P.ClientID = @ClientID
		GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
		)

	SELECT
		P.IsActive AS IsProjectActive,
		core.YesNoFormat(P.IsActive) AS IsProjectActiveFormatted,
		P.ProjectCode,
		P.ProjectName,
		PP.IsActive AS IsPersonProjectActive,
		core.YesNoFormat(PP.IsActive) AS IsPersonProjectActiveFormatted,
		person.FormatPersonNameByPersonID(PP.PersonID, 'LastFirst') AS PersonNameFormatted,
		PP.PersonProjectName,
		ISNULL(PPT.HoursWorked, 0) AS TimeTaken,
		PPTT.PersonProjectLeaveHours,
		PPTT.PersonProjectLeaveHours - ISNULL(PPT.HoursWorked, 0) AS TimeRemaining,
		TT.TimeTypeName
	FROM person.PersonProjectTimeType PPTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = PPTT.ClientTimeTypeID
		JOIN client.Client C ON C.ClientID = CTT.ClientID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPTT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
		LEFT JOIN PPT ON PPT.TimeTypeID = TT.TimeTypeID
			AND PPTT.PersonProjectID = PPT.PersonProjectID
	WHERE P.ClientID = @ClientID
	ORDER BY P.ProjectName, P.ProjectID, PP.PersonProjectName, PP.PersonProjectID, TT.TimeTypeName, TT.TimeTypeID

END
GO
--End procedure client.GetClientLeave

--Begin procedure core.GetFAQByFAQID
EXEC utility.DropObject 'core.GetFAQByFAQID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.03
-- Description:	A stored procedure to return data from the core.FAQ table based on a FAQID
-- =======================================================================================
CREATE PROCEDURE core.GetFAQByFAQID

@FAQID INT

AS
BEGIN
	SET NOCOUNT ON;

	--FAQ
	SELECT
		F.FAQID, 
		F.ClientID, 
		F.FAQKey, 
		F.FAQValue
	FROM core.FAQ F
	WHERE F.FAQID = @FAQID

END
GO
--End procedure core.GetFAQByFAQID

--Begin procedure eventlog.LogFAQAction
EXEC utility.DropObject 'eventlog.LogFAQAction'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE eventlog.LogFAQAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'FAQ'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('FAQ'), ELEMENTS
			)
		FROM core.FAQ T
		WHERE T.FAQID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFAQAction

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		PPT.DateWorked,
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode2 AS PerDiemCode,
		C.FixedAllowanceLabel AS PerdiemType,
		PL.DPAAmount * PPT.HasDPA AS Amount,
		PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DPAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	UNION

	SELECT 
		PPT.DateWorked,
		core.FormatDateWithDay(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode3 AS PerDiemCode,
		C.VariableAllowanceLabel AS PerdiemType,
		PPT.DSAAmount AS Amount,
		PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DSAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal,
		TT.TimeTypeName
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	ORDER BY 1, 4, 3, 10

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoiceWorkflowDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get workflow data for an invoice
-- ===================================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsInWorkflow BIT = 1

	--InvoiceWorkflowData
	IF EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Invoice' AND EWSGP.EntityID = @InvoiceID)
		EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SET @bIsInWorkflow = 0

		UPDATE I
		SET I.InvoiceStatus = CASE WHEN I.InvoiceStatus = 'Submitted' THEN 'Line Manager Review' ELSE I.InvoiceStatus END
		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		SELECT

			CASE
				WHEN I.InvoiceStatus = 'Line Manager Review'
				THEN 'Line Manager Approval' 
				ELSE NULL
			END AS WorkflowStepName,

			1 AS WorkflowStepNumber,

			CASE
				WHEN I.InvoiceStatus = 'Rejected'
				THEN 0
				ELSE 1
			END AS WorkflowStepCount

		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		END
	--ENDIF

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		(SELECT TOP 1 EWSGP.WorkflowStepName FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Invoice' AND EWSGP.EntityID = @InvoiceID AND EWSGP.PersonID = EL.PersonID ORDER BY EWSGP.WorkflowStepID DESC) AS WorkflowStepName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode LIKE 'linemanagerapprove%'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN EL.EventCode LIKE 'linemanagerreject%'
			THEN 'Line Manager Rejected Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND 
				(
				EL.EventCode IN ('approve','create')
					OR EL.EventCode LIKE '%workflow'
					OR EL.EventCode LIKE 'linemanager%'
				)
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowPeopleByInvoiceID @InvoiceID, @bIsInWorkflow

END
GO
--End procedure invoice.GetInvoiceWorkflowDataByInvoiceID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, 'Administrator') CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'ClientNotice'
		SELECT @bHasAccess = 1 FROM client.ClientNotice T WHERE (T.ClientNoticeID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM person.GetClientsByPersonID(@PersonID, NULL) CP WHERE CP.ClientID = T.ClientID))
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'List.ClientFinanceExport'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM person.GetClientsByPersonID(@PersonID, 'Administrator') CBP
				JOIN client.Client C ON C.ClientID = CBP.ClientID
					AND EXISTS
						(
						SELECT 1
						FROM client.ClientConfiguration CC
						WHERE CC.ClientID = CBP.ClientID
							AND CC.ClientConfigurationKey = 'HasClientFinanceExport'
							AND CAST(CC.ClientConfigurationValue AS BIT) = 1
						)
					AND EXISTS
						(
						SELECT 1
						FROM invoice.Invoice T
							JOIN project.Project P ON P.ProjectID = T.ProjectID
								AND P.ClientID = CBP.ClientID
								AND T.InvoiceStatus = 'Approved'
								AND T.ClientFinanceExportID = 0
							)

			END
		ELSE IF @AccessCode = 'ShowSummary'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.PersonID = @PersonID
				OR EXISTS
					(
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = 'Invoice'
						AND EWSGP.EntityID = T.InvoiceID
						AND EWSGP.PersonID = @PersonID
					)

			END
		ELSE IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE 
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR PBP.RoleCode = 'Administrator'
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
									(
									workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
										OR T.InvoiceStatus = 'Rejected'
									)
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR NOT EXISTS
								(
								SELECT 1
								FROM workflow.EntityWorkflowStepGroupPerson EWSGP
								WHERE EWSGP.EntityTypeCode = 'Invoice'
									AND EWSGP.EntityID = T.InvoiceID
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID AND T.RoleCode IN ('Administrator', 'ProjectManager') OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetCalendarEntriesByPersonID
EXEC utility.DropObject 'person.GetCalendarEntriesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A stored procedure to get data for the desktop calendar
-- ====================================================================
CREATE PROCEDURE person.GetCalendarEntriesByPersonID

@PersonID INT,
@StartDate DATE,
@EndDate DATE,
@EntityTypeCode VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDates TABLE (DateValue DATE) 
	DECLARE @tEntityTypeCodes TABLE (EntityTypeCode VARCHAR(50)) 

	INSERT INTO @tDates (DateValue) SELECT D.DateValue FROM core.GetDatesFromRange(@StartDate, @EndDate) D

	SET @EntityTypeCode = core.NullIfEmpty(@EntityTypeCode)
	IF @EntityTypeCode IS NOT NULL
		INSERT INTO @tEntityTypeCodes (EntityTypeCode) SELECT LTT.ListItem FROM core.ListToTable(@EntityTypeCode, ',') LTT
	--ENDIF

	SELECT
		CMD.BackgroundColor,
		CMD.EntityTypeCode,
		CMD.Icon,
		CMD.TextColor,
		D.EndDate,
		D.StartDate,
		D.Title,
		D.URL
	FROM
		(
		SELECT
			'Availability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(T.DateValue, 'yyyy-MM-dd') AS StartDate,
			'Available' AS Title,
			NULL AS URL
		FROM @tDates T
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Availability'))
			AND NOT EXISTS
				(
				SELECT 1
				FROM person.PersonUnavailability PU
				WHERE PU.PersonID = @PersonID
					AND PU.PersonUnavailabilityDate >= @StartDate
					AND PU.PersonUnavailabilityDate <= @EndDate
					AND PU.PersonUnavailabilityDate = T.DateValue
				)

		UNION

		SELECT
			'DPA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'Fixed Allowance Invoiced'
				ELSE 'Fixed Allowance Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DPA'))
				AND PPT.HasDPA = 1
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'DSA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'Variable Allowance Invoiced'
				ELSE 'Variable Allowance Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DSA'))
				AND PPT.DSAAmount > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Expense' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPE.InvoiceID > 0
				THEN 'Expense Invoiced'
				ELSE 'Expense Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPEC.PersonProjectExpenseID) FROM person.PersonProjectExpense PPEC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPEC.PersonProjectID AND PPC.PersonID = @PersonID AND PPEC.ExpenseDate = PPE.ExpenseDate) > 1
				THEN '/personprojectexpense/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd')
				ELSE '/personprojectexpense/view/id/' + CAST(PPE.PersonProjectExpenseID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Expense'))
				AND PP.PersonID = @PersonID
				AND PPE.ExpenseDate >= @StartDate
				AND PPE.ExpenseDate <= @EndDate

		UNION

		SELECT
			'Project' AS EntityTypeCode,
			FORMAT(DATEADD(d, 1, PP.EndDate), 'yyyy-MM-dd') AS EndDate,
			FORMAT(PP.StartDate, 'yyyy-MM-dd') AS StartDate,
			P.ProjectName + ' / ' + PTOR.ProjectTermOfReferenceName AS Title,
			'/personproject/view/id/' + CAST(PP.PersonProjectID AS VARCHAR(10)) AS URL
		FROM person.PersonProject PP
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Project'))
				AND PP.PersonID = @PersonID
				AND PP.StartDate <= @EndDate
				AND PP.EndDate >= @StartDate
				AND PP.AcceptedDate IS NOT NULL

		UNION

		SELECT
			'Time' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'Time Invoiced'
				ELSE 'Time Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Time'))
				AND PPT.HoursWorked > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Unavailability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS StartDate,
			'Unavailable' AS Title,
			NULL AS URL
		FROM person.PersonUnavailability PU
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Unavailability'))
			AND PU.PersonID = @PersonID
			AND PU.PersonUnavailabilityDate >= @StartDate
			AND PU.PersonUnavailabilityDate <= @EndDate

		) D 
		JOIN core.CalendarMetadata CMD ON CMD.EntityTypeCode = D.EntityTypeCode

END
GO
--End procedure person.GetCalendarEntriesByPersonID

--Begin procedure person.GetFAQByPersonID
EXEC Utility.DropObject 'person.GetFAQByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data for the responsedashboard
-- ========================================================================
CREATE PROCEDURE person.GetFAQByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		F.FAQID,
		F.FAQKey,
		F.FAQValue
	FROM core.FAQ F
	WHERE F.ClientID = 0
		OR EXISTS
			(
			SELECT 1
			FROM person.GetClientsByPersonID(@PersonID, NULL) CBP
			WHERE CBP.ClientID = F.ClientID
			)
	ORDER BY F.FAQKey, F.FAQID

END
GO
--End procedure person.GetFAQByPersonID

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (PendingActionID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>',
		'<button class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two emergency contact records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	INSERT INTO @tTable 
		(PendingAction, Link1) 
	SELECT
		'Accept deployment ' + PP.PersonProjectName + ' on project ' + P.ProjectName,
		'<button class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptPersonProject'', ' + CAST(PP.PersonProjectID AS VARCHAR(10)) + ', [PendingActionID])" type="button">Accept</button>'
	FROM person.PersonProject PP 
		JOIN project.Project P ON P.ProjectID = PP.ProjectID 
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NULL
			AND PP.EndDate >= getDate()

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT
		T.PendingActionID,
		T.PendingAction, 
		REPLACE(T.Link1, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link1,
		REPLACE(T.Link2, '[PendingActionID]', CAST(T.PendingActionID AS VARCHAR(5))) AS Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		G.GenderID,
		G.GenderName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant') THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.OwnCompanyName,
		P.NationalInsuranceNumber,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.SummaryBiography,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		P.WorkPhone,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProfileClientConfiguration
	SELECT DISTINCT
		CC.ClientConfigurationKey
	FROM client.ClientConfiguration CC
		JOIN client.ClientPerson CP ON CP.ClientID = CC.ClientID
			AND CP.PersonID = @PersonID
			AND CP.ClientPersonRoleCode = 'Consultant'
			AND CC.ClientConfigurationCategoryCode = 'PersonProfile'
			AND CAST(CC.ClientConfigurationValue AS BIT) = 1

	--PersonProjectLeave
	EXEC person.GetPersonProjectLeave 'Person', @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationAcademic'
				AND DE.EntityID = PQA.PersonQualificationAcademicID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'QualificationCertification'
				AND DE.EntityID = PQC.PersonQualificationCertificationID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonDashboardDataByPersonID
EXEC utility.DropObject 'person.GetPersonDashboardDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return dashboard data based on a PersonID
-- ============================================================================
CREATE PROCEDURE person.GetPersonDashboardDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @nDaysLogged NUMERIC(18,2)
	DECLARE @nPersonProjectDays NUMERIC(18,2)

	SELECT @nPersonProjectDays = SUM(PPL.Days)
	FROM person.PersonProjectLocation PPL
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPL.PersonProjectID
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NOT NULL 
			AND PP.EndDate >= getDate()

	SELECT @nDaysLogged = ROUND(SUM(CAST(PPT.HoursWorked / P.HoursPerDay AS NUMERIC(18,2))), 1)
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
			AND TT.TimeTypeCode = 'PROD'
			AND PP.PersonID = @PersonID
			AND PP.AcceptedDate IS NOT NULL 
			AND PP.EndDate >= getDate()

	;
	WITH ID AS
		(
		SELECT
			COUNT(I.InvoiceStatus) AS ItemCount,
			CASE
				WHEN I.InvoiceStatus IN ('Approved','Line Manager Review','Rejected')
				THEN I.InvoiceStatus
				ELSE 'Client Review'
			END AS InvoiceStatus
		FROM invoice.Invoice I
		WHERE I.PersonID = @PersonID
			AND DATEDIFF(d, I.InvoiceDateTime, getDate()) < 90
		GROUP BY I.InvoiceStatus
		)

	SELECT
		CAST(@nPersonProjectDays - @nDaysLogged AS NUMERIC(18,1)) AS DaysAvailable,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Approved'), 0) AS ApprovedInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Client Review'), 0) AS ClientReviewInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Line Manager Review'), 0) AS LineManagerReviewInvoiceCount,
		ISNULL((SELECT ID.ItemCount FROM ID WHERE ID.InvoiceStatus = 'Rejected'), 0) AS RejectedInvoiceCount,
		ISNULL((SELECT COUNT(I.InvoiceID) FROM invoice.Invoice I WHERE I.PersonID = @PersonID AND DATEDIFF(d, I.InvoiceDateTime, getDate()) < 90), 0) AS InvoiceCount

END
GO
--End procedure person.GetPersonDashboardDataByPersonID

--Begin procedure person.GetPersonProjectLeave
EXEC utility.DropObject 'person.GetPersonProjectLeave'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.08
-- Description:	A stored procedure to return leave data based on an EntityTypeCode and an EntityID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonProjectLeave

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT = 0,
@IncludeProductiveTime BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--WARNING!!! invoice.GetInvoicePreviewData relies on this specific schema and column order.  Changes here must be reflected there.
	SELECT
		1 AS DisplayOrder,
		CTT.IsBalancePayableUponTermination,
		core.YesNoFormat(CTT.IsBalancePayableUponTermination) AS IsBalancePayableUponTerminationFormatted,
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PPT1.HoursWorked, 0) AS TimeTaken,
		PPTT.PersonProjectLeaveHours,
		PPTT.PersonProjectLeaveHours - ISNULL(PPT2.HoursWorked, 0) AS TimeRemaining,
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM person.PersonProjectTimeType PPTT
		JOIN client.ClientTimeType CTT ON CTT.ClientTimeTypeID = PPTT.ClientTimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPTT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
			AND 
				(
					(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID)
						OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
						OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
						OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
				)
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PPT.InvoiceID = @EntityID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
					AND PPT.TimeTypeID = TT.TimeTypeID
					AND PPTT.PersonProjectID = PPT.PersonProjectID
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PPT1
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
					AND PPT.TimeTypeID = TT.TimeTypeID
					AND PPTT.PersonProjectID = PPT.PersonProjectID
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PPT2

	UNION

	SELECT
		0 AS DisplayOrder,
		0,
		'',
		PP.PersonProjectID,
		PP.PersonProjectName,
		ISNULL(PTT3.HoursWorked, 0) AS TimeTaken,
		CAST(ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PTT4.PersonProjectID), 0) * P.HoursPerDay AS NUMERIC(18,2)),
		CAST((ISNULL((SELECT SUM(PPL.Days) FROM person.PersonProjectLocation PPL WHERE PPL.PersonProjectID = PTT4.PersonProjectID), 0) * P.HoursPerDay) - ISNULL(PTT4.HoursWorked, 0) AS NUMERIC(18,2)),
		TT.TimeTypeID,
		TT.TimeTypeName
	FROM
		(
		SELECT
			PPT.PersonProjectID,
			PPT.TimeTypeID,
			SUM(PPT.HoursWorked) AS HoursWorked
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
				AND TT.TimeTypeCode = 'PROD'
				AND @IncludeProductiveTime = 1
				AND 
					(
						(@EntityTypeCode = 'Invoice' AND PPT.InvoiceID = @EntityID)
							OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
							OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
							OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
					)
		GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
		) PTT3
		JOIN person.PersonProject PP ON PP.PersonProjectID = PTT3.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PTT3.TimeTypeID
			AND @IncludeProductiveTime = 1
		OUTER APPLY
			(
			SELECT
				PPT.PersonProjectID,
				PPT.TimeTypeID,
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
					AND TT.TimeTypeCode = 'PROD'
					AND @IncludeProductiveTime = 1
					AND 
						(
							(@EntityTypeCode = 'Invoice' AND PP.PersonID = @PersonID)
								OR (@EntityTypeCode = 'Person' AND PP.PersonID = @EntityID)
								OR (@EntityTypeCode = 'PersonProject' AND PP.PersonProjectID = @EntityID)
								OR (@EntityTypeCode = 'Project' AND PP.ProjectID = @EntityID AND PP.PersonID = @PersonID)
						)
			GROUP BY PPT.PersonProjectID, PPT.TimeTypeID
			) PTT4
	
	ORDER BY 1, CTT.IsBalancePayableUponTermination, TT.TimeTypeName, TT.TimeTypeID, PP.PersonProjectName, PP.PersonProjectID
	
END
GO
--End procedure person.GetPersonProjectLeave

--Begin procedure person.GetPersonProjectTimeJSONDataByPersonID
EXEC utility.DropObject 'person.GetPersonProjectTimeJSONDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.21
-- Description:	A stored procedure to return data from the person.PersonProjectTime table based on a PersonID
-- ==========================================================================================================
CREATE PROCEDURE person.GetPersonProjectTimeJSONDataByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nDashboardDayCount INT = CAST(core.GetSystemSetupValueBySystemSetupKey('DashboardDayCount', '30') AS INT)
	DECLARE @tTable1 TABLE (DateIndex DATE NOT NULL PRIMARY KEY)
	DECLARE @tTable2 TABLE (RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PersonProjectID INT NOT NULL DEFAULT 0)
	DECLARE @tTable3 TABLE (DateIndex DATE NOT NULL, PersonProjectID INT NOT NULL DEFAULT 0, HoursWorked NUMERIC(18,2) DEFAULT 0)
	DECLARE @tTable4 TABLE (RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, JSONData VARCHAR(MAX))

	;
	WITH RD AS 
		(
		SELECT 
			1 AS RowIndex, 
			CAST(DATEADD(d, -1 * @nDashboardDayCount, getDate()) AS DATE) AS DateIndex
		
		UNION ALL 
		
		SELECT 
			RD.RowIndex + 1, 
			DATEADD(d, 1, RD.DateIndex) AS DateIndex 
		FROM RD 
		WHERE RD.RowIndex <= @nDashboardDayCount
		) 

	INSERT INTO @tTable1 
		(DateIndex) 
	SELECT 
		RD.DateIndex 
	FROM RD

	INSERT INTO @tTable2
		(PersonProjectID)
	SELECT DISTINCT
		PPT.PersonProjectID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM @tTable1 T1
				WHERE T1.DateIndex = PPT.DateWorked
				)

	INSERT INTO @tTable3
		(DateIndex, PersonProjectID, HoursWorked)
	SELECT 
		T1.DateIndex,
		T2.PersonProjectID,
		ISNULL(OAPPT.HoursWorked, 0) AS HoursWorked
	FROM @tTable1 T1
		CROSS APPLY @tTable2 T2
		OUTER APPLY
			(
			SELECT 
				SUM(PPT.HoursWorked) AS HoursWorked
			FROM person.PersonProjectTime PPT
				JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				JOIN person.Person P ON P.PersonID = PP.PersonID
					AND P.PersonID = @PersonID
					AND PPT.DateWorked = T1.DateIndex
					AND PPT.PersonProjectID = T2.PersonProjectID
			GROUP BY PPT.PersonProjectID, PPT.DateWorked
			) OAPPT
	ORDER BY T2.RowIndex, T1.DateIndex

	INSERT INTO @tTable4
		(JSONData)
	VALUES
		(
		'{"labels": [' 
			+ STUFF(
				(
				SELECT
					',"' + D.DateIndexFormatted + '"' AS [text()]
				FROM
					(
					SELECT
						T1.DateIndex,
						CONVERT(CHAR(6), T1.DateIndex, 113) AS DateIndexFormatted 
					FROM @tTable1 T1
					) D
				ORDER BY D.DateIndex
				FOR XML PATH('')
				), 1, 1, '') 
			+ ']'
		)

	IF EXISTS (SELECT 1 FROM @tTable2 T2)
		BEGIN

		DECLARE @cJSONData VARCHAR(MAX)
		DECLARE @nPersonProjectID INT
		DECLARE @nRowIndex INT

		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				T2.RowIndex,
				T2.PersonProjectID
			FROM @tTable2 T2
			ORDER BY T2.PersonProjectID
			
		OPEN oCursor
		FETCH oCursor INTO @nRowIndex, @nPersonProjectID
		WHILE @@fetch_status = 0
			BEGIN
			
			SET @cJSONData = ''
			IF @nRowIndex = 1
				SET @cJSONData += '"datasets": ['
			--ENDIF

			SELECT @cJSONData += 
				'{"label": "' + ISNULL(PP.PersonProjectName, 'No Deployment Name Provided') + '",' 
			FROM person.PersonProject PP 
			WHERE PP.PersonProjectID = @nPersonProjectID

			SELECT @cJSONData += 
				'"backgroundColor": "' + LTT.ListItem + '",' 
			FROM core.ListToTable(core.GetSystemSetupValueBySystemSetupKey('DashboardChartHexColors', ''), ',') LTT 
			WHERE LTT.ListItemID = @nRowIndex

			SELECT @cJSONData += 
				'"data": ['
					+ STUFF(
						(
						SELECT
							',' + D.HoursWorked + '' AS [text()]
						FROM
							(
							SELECT TOP 100 PERCENT
								CAST(T3.HoursWorked AS VARCHAR(10)) AS HoursWorked
							FROM @tTable3 T3
							WHERE T3.PersonProjectID = @nPersonProjectID
							ORDER BY T3.DateIndex
							) D
						FOR XML PATH('')
						), 1, 1, '') 
					+ ']}'

			INSERT INTO @tTable4 (JSONData) VALUES (@cJSONData)
					
			FETCH oCursor INTO @nRowIndex, @nPersonProjectID
			
			END
		--END WHILE
				
		CLOSE oCursor
		DEALLOCATE oCursor	

		INSERT INTO @tTable4 (JSONData) VALUES (']}')

		END
	ELSE
		INSERT INTO @tTable4 (JSONData) VALUES ('"datasets": []}')
	--ENDIF

	SELECT
		REPLACE(STUFF(
			(
			SELECT
				',' + D.JSONData AS [text()]
			FROM
				(
				SELECT TOP 100 PERCENT
					T4.JSONData
				FROM @tTable4 T4
				ORDER BY T4.RowIndex
				) D
			FOR XML PATH('')
			), 1, 1, ''), ']},]}', ']}]}') AS JSONData

END
GO
--End procedure person.GetPersonProjectTimeJSONDataByPersonID

--Begin procedure person.TogglePersonUnavailabilityDate
EXEC utility.DropObject 'person.TogglePersonUnavailabilityDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.19
-- Description:	A stored procedure to update the person.PersonUnavailability table
-- ===============================================================================
CREATE PROCEDURE person.TogglePersonUnavailabilityDate

@PersonID INT,
@PersonUnavailabilityDate DATE,
@IsAvailable BIT

AS
BEGIN
	SET NOCOUNT ON;

	IF @IsAvailable = 0 AND NOT EXISTS (SELECT 1 FROM person.PersonUnavailability PU WHERE PU.PersonID = @PersonID AND PU.PersonUnavailabilityDate = @PersonUnavailabilityDate)
		INSERT INTO person.PersonUnavailability (PersonID, PersonUnavailabilityDate) VALUES (@PersonID, @PersonUnavailabilityDate)
	ELSE IF @IsAvailable = 1
		DELETE PU FROM person.PersonUnavailability PU WHERE PU.PersonID = @PersonID AND PU.PersonUnavailabilityDate = @PersonUnavailabilityDate
	--ENDIF

END
GO
--End procedure person.TogglePersonUnavailabilityDate

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,
		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND core.NullIfEmpty(P.SummaryBiography) IS NULL THEN ',SummaryBiography' ELSE '' END 
			+ CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant') AND NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.EntityTypeCode = 'Person' AND DE.EntityTypeSubCode = 'CV' AND DE.EntityID = P.PersonID) THEN ',CV' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END
	SET @cRequiredProfileUpdate = CASE WHEN LEFT(@cRequiredProfileUpdate, 1) = ',' THEN STUFF(@cRequiredProfileUpdate, 1, 1, '') ELSE @cRequiredProfileUpdate END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			CASE WHEN person.IsProfileUpdateRequired(@nPersonID, @bIsTwoFactorEnabled) = 1 OR LEN(@cRequiredProfileUpdate) > 0 THEN 1 ELSE 0 END,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure project.validateProjectDateAndTimeData
EXEC utility.DropObject 'project.validateProjectDateAndTimeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.29
-- Description:	A stored procedure to validate a dates and hours worked on a deployment
-- ====================================================================================
CREATE PROCEDURE project.validateProjectDateAndTimeData

@DateWorkedList VARCHAR(MAX) = NULL,
@HoursWorked NUMERIC(18,2) = 0,
@PersonProjectID INT,
@ProjectLocationID INT,
@TimeTypeID INT,
@PersonProjectTimeID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsValidated BIT

	DECLARE @cTimeTypeCode VARCHAR(50)

	DECLARE @dEndDate DATE
	DECLARE @dStartDate DATE

	DECLARE @nDateWorkedCount INT
	DECLARE @nHoursPerDay NUMERIC(18,2)
	DECLARE @nPersonProjectLeaveHours NUMERIC(18,2)
	DECLARE @nPersonProjectTimeAllotted NUMERIC(18,2)
	DECLARE @nPersonProjectTimeExpended NUMERIC(18,2)

	DECLARE @tDateWorked TABLE (DateID INT NOT NULL PRIMARY KEY, DateWorked DATE, IsValidWorkDay BIT)
	DECLARE @tResult TABLE (MessageID INT NOT NULL PRIMARY KEY IDENTITY(1,1), Message VARCHAR(MAX))

	SELECT
		@bIsValidated = CTT.IsValidated,
		@nHoursPerDay = P.HoursPerDay,
		@dEndDate = PP.EndDate,
		@dStartDate = PP.StartDate,
		@nPersonProjectLeaveHours = ISNULL((SELECT PPTT.PersonProjectLeaveHours FROM person.PersonProjectTimeType PPTT WHERE PPTT.PersonProjectID = PP.PersonProjectID AND PPTT.ClientTimeTypeID = CTT.ClientTimeTypeID), 0),
		@cTimeTypeCode = TT.TimeTypeCode
	FROM person.PersonProject PP
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ClientTimeType CTT ON CTT.ClientID = P.ClientID
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = CTT.TimeTypeID
			AND PP.PersonProjectID = @PersonProjectID
			AND TT.TimeTypeID = @TimeTypeID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID
		AND PPT.TimeTypeID = @TimeTypeID

	IF @DateWorkedList IS NOT NULL
		BEGIN

		INSERT INTO @tDateWorked
			(DateID, DateWorked, IsValidWorkDay)
		SELECT
			LTT.ListItemID,
			CAST(LTT.ListItem AS DATE),
			project.IsValidWorkDay(CAST(LTT.ListItem AS DATE), @ProjectLocationID)
		FROM core.ListToTable(@DateWorkedList, ',') LTT

		END
	--ENDIF

	SELECT
		@nDateWorkedCount = ISNULL(COUNT(DW.DateID), 0)
	FROM @tDateWorked DW

	INSERT INTO @tResult
		(Message)
	SELECT
		core.FormatDate(DW.DateWorked) + ' is outside the allowed date range for this deployment.'
	FROM @tDateWorked DW
	WHERE DW.DateWorked < @dStartDate OR DW.DateWorked > @dEndDate
	ORDER BY DW.DateWorked

	IF @cTimeTypeCode = 'PROD'
		BEGIN

		INSERT INTO @tResult
			(Message)
		SELECT
			core.FormatDate(DW.DateWorked) + ' is not a valid work day for this deployment.'
		FROM @tDateWorked DW
		WHERE DW.IsValidWorkDay = 0
		ORDER BY DW.DateWorked

		SELECT
			@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
		FROM person.PersonProjectLocation PPL
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
			JOIN project.Project P ON P.ProjectID = PL.ProjectID
				AND PPL.PersonProjectID = @PersonProjectID

		IF ISNULL(@nPersonProjectTimeAllotted, 0) < 
			ISNULL(@nPersonProjectTimeExpended, 0) 
				+ (@nDateWorkedCount * @HoursWorked)
				- CASE 
						WHEN @PersonProjectTimeID = 0 
						THEN 0 
						ELSE ISNULL((SELECT PPT.HoursWorked FROM person.PersonProjectTime PPT WHERE PPT.PersonProjectTimeID = @PersonProjectTimeID), 0) 
					END
			INSERT INTO @tResult (Message) VALUES ('The total hours worked exceeds your hours allocation for this deployment.')
		--ENDIF

		END
	ELSE IF @bIsValidated = 1
		BEGIN

		IF ISNULL(@nPersonProjectLeaveHours, 0) < 
			ISNULL(@nPersonProjectTimeExpended, 0) 
				+ (@nDateWorkedCount * @HoursWorked) 
				- CASE 
						WHEN @PersonProjectTimeID = 0 
						THEN 0 
						ELSE ISNULL((SELECT PPT.HoursWorked FROM person.PersonProjectTime PPT WHERE PPT.PersonProjectTimeID = @PersonProjectTimeID), 0) 
					END
			INSERT INTO @tResult (Message) VALUES ('The total hours claimed exceeds your allowance for this deployment.')
		--ENDIF

		END
	--ENDIF

	SELECT R.Message 
	FROM @tResult R
	ORDER BY R.MessageID

END
GO
--End procedure project.validateProjectDateAndTimeData

--Begin procedure reporting.GetClientFinanceExportData_Hermis
EXEC utility.DropObject 'client.GetClientFinanceExportData_Hermis'
EXEC utility.DropObject 'reporting.GetClientFinanceExportData_Hermis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.16
-- Description:	A stored procedure to return data from the invoice.Invoice table based on a ClientFinanceExportID
-- ==============================================================================================================
CREATE PROCEDURE reporting.GetClientFinanceExportData_Hermis

@ClientFinanceExportID INT,
@ClientPersonProjectRoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE (RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, InvoiceID INT)

	INSERT INTO @tTable1 
		(InvoiceID) 
	SELECT 
		I.InvoiceID
	FROM invoice.Invoice I
	WHERE I.ClientFinanceExportID = @ClientFinanceExportID
	ORDER BY I.InvoiceID

	SELECT
		ROW_NUMBER() OVER (PARTITION BY D.InvoiceID ORDER BY D.InvoiceID, D.Account, D.LineDescription, D.ProjectTermOfReferenceCode),
		D.InvoiceID,
		D.Account,
		D.LineDescription,
		D.ProjectTermOfReferenceCode,
		SUM(D.LineAmount) AS LineAmount,
		SUM(D.TaxAmount) AS TaxAmount
	FROM
		(
		SELECT
			I.InvoiceID,
			CCC.ClientCostCodeName AS Account,
			CCC.ClientCostCodeDescription AS LineDescription,
			PTOR.ProjectTermOfReferenceCode,
			PPE.ExpenseAmount * PPE.ExchangeRate AS LineAmount,
			PPE.TaxAmount * PPE.ExchangeRate AS TaxAmount
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
			JOIN invoice.Invoice I ON I.ProjectID = PP.ProjectID
			JOIN @tTable1 T1 ON T1.InvoiceID = I.InvoiceID
				AND PPE.InvoiceID = I.InvoiceID
				AND PP.ClientPersonProjectRoleID = @ClientPersonProjectRoleID

		UNION ALL

		SELECT
			I.InvoiceID,
			C.FinanceCode2 AS Account,
			C.FixedAllowanceLabel AS LineDescription,
			PTOR.ProjectTermOfReferenceCode,
			PL.DPAAmount * CAST(PPT.HasDPA AS INT) * PPT.ExchangeRate AS LineAmount,
			PL.DPAAmount * CAST(PPT.HasDPA AS INT) * (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100) * PPT.ExchangeRate AS TaxAmount
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			JOIN @tTable1 T1 ON T1.InvoiceID = I.InvoiceID
				AND PPT.InvoiceID = I.InvoiceID
				AND PP.ClientPersonProjectRoleID = @ClientPersonProjectRoleID

		UNION ALL

		SELECT
			I.InvoiceID,
			C.FinanceCode3 AS Account,
			C.VariableAllowanceLabel AS LineDescription,
			PTOR.ProjectTermOfReferenceCode,
			PPT.DSAAmount * PPT.ExchangeRate AS LineAmount,
			PPT.DSAAmount * (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100) * PPT.ExchangeRate AS TaxAmount
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			JOIN @tTable1 T1 ON T1.InvoiceID = I.InvoiceID
				AND PPT.InvoiceID = I.InvoiceID
				AND PP.ClientPersonProjectRoleID = @ClientPersonProjectRoleID

		UNION ALL

		SELECT
			I.InvoiceID,

			CASE
				WHEN TT.TimeTypeCode = 'PROD'
				THEN ISNULL((SELECT CPPR.FinanceCode FROM client.ClientPersonProjectRole CPPR WHERE CPPR.ClientPersonProjectRoleID = PP.ClientPersonProjectRoleID), '')
				ELSE ISNULL((SELECT CTT.AccountCode FROM client.ClientTimeType CTT WHERE CTT.TimeTypeID = PPT.TimeTypeID AND CTT.ClientID = P.ClientID), '')
			END AS Account,

			'Labour' AS LineDescription,
			PTOR.ProjectTermOfReferenceCode,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate AS LineAmount,
			PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100) * PPT.ExchangeRate AS TaxAmount
		FROM person.PersonProjectTime PPT
			JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			JOIN @tTable1 T1 ON T1.InvoiceID = I.InvoiceID
				AND PPT.InvoiceID = I.InvoiceID
				AND PP.ClientPersonProjectRoleID = @ClientPersonProjectRoleID
		) D
	WHERE D.LineAmount > 0 OR D.TaxAmount > 0
	GROUP BY D.InvoiceID, D.Account, D.LineDescription, D.ProjectTermOfReferenceCode

END
GO
--End procedure reporting.GetClientFinanceExportData_Hermis

--Begin procedure reporting.GetClientFinanceExportData_Hermis_H
EXEC utility.DropObject 'client.GetClientFinanceExportData_Hermis_H'
EXEC utility.DropObject 'reporting.GetClientFinanceExportData_Hermis_H'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.16
-- Description:	A stored procedure to return data from the invoice.Invoice table based on a ClientFinanceExportID
-- ==============================================================================================================
CREATE PROCEDURE reporting.GetClientFinanceExportData_Hermis_H

@ClientFinanceExportID INT,
@ClientPersonProjectRoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable2 TABLE 
		(
		RowIndex INT, 
		InvoiceID INT,
		Account VARCHAR(100),
		LineDescription VARCHAR(300),
		ProjectTermOfReferenceCode VARCHAR(5),
		LineAmount NUMERIC(18,2),
		TaxAmount NUMERIC(18,2)
		)

	INSERT INTO @tTable2 
		(RowIndex, InvoiceID, Account, LineDescription, ProjectTermOfReferenceCode, LineAmount, TaxAmount) 
	EXEC reporting.GetClientFinanceExportData_Hermis @ClientFinanceExportID, @ClientPersonProjectRoleID

	SELECT
		I.InvoiceID,
		'H' AS RecordType,
		ROW_NUMBER() OVER (ORDER BY I.InvoiceID) AS RowIndex1,
		YEAR(I.InvoiceDateTime) + CASE WHEN MONTH(I.InvoiceDateTime) < 7 THEN 0 ELSE 1 END AS FiscalYear,
		MONTH(I.InvoiceDateTime) + (6 * CASE WHEN MONTH(I.InvoiceDateTime) < 7 THEN 1 ELSE -1 END) AS PeriodNumber,
		1 AS SubPeriodNumber,
		C.ClientCode + PN.UserName AS VendorID,
		'NET 30' AS Terms,
		I.PersonInvoiceNumber,
		CONVERT(VARCHAR(10), I.InvoiceDateTime, 126) AS InvoiceDateFormatted,
		CAST((SELECT SUM(T2.LineAmount + T2.TaxAmount) FROM @tTable2 T2 WHERE T2.InvoiceID = I.InvoiceID) AS NUMERIC(18,2)) AS InvoiceAmount,
		CONVERT(VARCHAR(10), DATEADD(d, 30, I.InvoiceDateTime), 126) AS InvoiceDueDateFormatted,
		'N' AS HoldVoucher,
		'N' AS PayWhenPaid
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND EXISTS
				(
				SELECT 1
				FROM @tTable2 T2
				WHERE T2.InvoiceID = I.InvoiceID
				)

END
GO
--End procedure reporting.GetClientFinanceExportData_Hermis_H

--Begin procedure reporting.GetClientFinanceExportData_Hermis_D
EXEC utility.DropObject 'client.GetClientFinanceExportData_Hermis_D'
EXEC utility.DropObject 'reporting.GetClientFinanceExportData_Hermis_D'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.16
-- Description:	A stored procedure to return data from the invoice.Invoice table based on a ClientFinanceExportID
-- ==============================================================================================================
CREATE PROCEDURE reporting.GetClientFinanceExportData_Hermis_D

@ClientFinanceExportID INT,
@ClientPersonProjectRoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE (RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, InvoiceID INT)

	DECLARE @tTable2 TABLE 
		(
		RowIndex INT, 
		InvoiceID INT,
		Account VARCHAR(100),
		LineDescription VARCHAR(300),
		ProjectTermOfReferenceCode VARCHAR(5),
		LineAmount NUMERIC(18,2),
		TaxAmount NUMERIC(18,2)
		)

	INSERT INTO @tTable2 
		(RowIndex, InvoiceID, Account, LineDescription, ProjectTermOfReferenceCode, LineAmount, TaxAmount) 
	EXEC reporting.GetClientFinanceExportData_Hermis @ClientFinanceExportID, @ClientPersonProjectRoleID

	INSERT INTO @tTable1 
		(InvoiceID) 
	SELECT DISTINCT
		T2.InvoiceID
	FROM @tTable2 T2
	ORDER BY T2.InvoiceID

	SELECT
		T2.InvoiceID, 
		'D' AS RecordType,
		T1.RowIndex AS RowIndex1,
		YEAR(I.InvoiceDateTime) + CASE WHEN MONTH(I.InvoiceDateTime) < 7 THEN 0 ELSE 1 END AS FiscalYear,
		T2.RowIndex AS RowIndex2,
		T2.Account,
		'21.HRO.23.00.1000' AS OrganizationCode,
		P.ProjectCode + '.' + T2.ProjectTermOfReferenceCode AS ProjectCode,
		CAST(T2.LineAmount AS NUMERIC(18,2)) AS LineAmount,
		'S' AS TaxableCode,
		'PIL20S' AS TaxCode,
		CAST(TaxAmount AS NUMERIC(18,2)) AS TaxAmount,
		0 AS DiscountAmount,
		0 AS UseTaxAmount,
		'N' AS APTaxCode,
		T2.LineDescription + ' ' + CONVERT(VARCHAR(10), I.StartDate, 101) + '-' + CONVERT(VARCHAR(10), I.EndDate, 101) AS LineDescription
	FROM @tTable2 T2
		JOIN @tTable1 T1 ON T1.InvoiceID = T2.InvoiceID
		JOIN invoice.Invoice I ON I.InvoiceID = T2.InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
	ORDER BY T2.InvoiceID, T2.RowIndex

END
GO
--End procedure reporting.GetClientFinanceExportData_Hermis_D

--Begin procedure reporting.GetClientFinanceExportData_Hermis_V
EXEC utility.DropObject 'client.GetClientFinanceExportData_Hermis_V'
EXEC utility.DropObject 'reporting.GetClientFinanceExportData_Hermis_V'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.16
-- Description:	A stored procedure to return data from the invoice.Invoice table based on a ClientFinanceExportID
-- ==============================================================================================================
CREATE PROCEDURE reporting.GetClientFinanceExportData_Hermis_V

@ClientFinanceExportID INT,
@ClientPersonProjectRoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE (RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, InvoiceID INT)

	DECLARE @tTable2 TABLE 
		(
		RowIndex INT, 
		InvoiceID INT,
		Account VARCHAR(100),
		LineDescription VARCHAR(300),
		ProjectTermOfReferenceCode VARCHAR(5),
		LineAmount NUMERIC(18,2),
		TaxAmount NUMERIC(18,2)
		)

	INSERT INTO @tTable2 
		(RowIndex, InvoiceID, Account, LineDescription, ProjectTermOfReferenceCode, LineAmount, TaxAmount) 
	EXEC reporting.GetClientFinanceExportData_Hermis @ClientFinanceExportID, @ClientPersonProjectRoleID

	INSERT INTO @tTable1 
		(InvoiceID) 
	SELECT DISTINCT
		T2.InvoiceID
	FROM @tTable2 T2
	ORDER BY T2.InvoiceID

	SELECT
		I.InvoiceID,
		'V' AS RecordType,
		T1.RowIndex AS RowIndex1,
		YEAR(I.InvoiceDateTime) + CASE WHEN MONTH(I.InvoiceDateTime) < 7 THEN 0 ELSE 1 END AS FiscalYear,
		(SELECT T2.RowIndex FROM @tTable2 T2 WHERE T2.InvoiceID = I.InvoiceID AND T2.LineAmount + T2.TaxAmount = SUM(CAST((PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) * (1 + (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100)) AS NUMERIC(18,2)))) AS RowIndex2,
		1 AS SubPeriod,
		C.ClientCode + PN.UserName AS VendorID,
		'SUBC' AS GeneralLaborCategory,
		PLC.ProjectLaborCode,
		SUM(CAST(PPT.HoursWorked AS NUMERIC(18,2))) AS HoursWorked,
		SUM(CAST((PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) * (1 + (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100))	AS NUMERIC(18,2))) AS LineAmount
	FROM person.PersonProjectTime PPT
		JOIN dropdown.TimeType TT ON TT.TimeTypeID = PPT.TimeTypeID
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN person.Person PN ON PN.PersonID = PP.PersonID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
		JOIN @tTable1 T1 ON T1.InvoiceID = PPT.InvoiceID
			AND PP.ClientPersonProjectRoleID = @ClientPersonProjectRoleID
			AND EXISTS
				(
				SELECT 1
				FROM @tTable2 T2
				WHERE T2.InvoiceID = I.InvoiceID
				)
	GROUP BY I.InvoiceID, T1.InvoiceID, T1.RowIndex, PPT.TimeTypeID, TT.TimeTypeCode, P.ClientID, C.ClientCode, PN.UserName, PP.PersonProjectID, PP.ClientPersonProjectRoleID, PLC.ProjectLaborCode, I.InvoiceDateTime
	HAVING SUM(CAST((PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) * (1 + (PN.TaxRate * CAST(PPT.ApplyVAT AS INT) / 100))	AS NUMERIC(18,2))) > 0

END
GO
--End procedure reporting.GetClientFinanceExportData_Hermis_V

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--Begin table client.ClientConfiguration
IF NOT EXISTS (SELECT 1 FROM client.ClientConfiguration CC WHERE CC.ClientID = 0)
	BEGIN

	INSERT INTO client.ClientConfiguration
		(ClientID, ClientConfigurationCategoryCode, ClientConfigurationKey, ClientConfigurationDescription, ClientConfigurationValue, DisplayOrder)
	VALUES
		(0, 'ClientFinanceExport', 'ClientFinanceExportReportFileName', 'The name of the SSRS .rdl file to use for the client finance export', NULL, 0),
		(0, 'ClientFinanceExport', 'HasClientFinanceExport', 'Does the client have a client finance export', 0, 0),
		(0, 'PersonProfile', 'ShowPersonProfileContactDetails', 'Show the contact details tab on the user profile', 1, 1),
		(0, 'PersonProfile', 'ShowPersonProfilePersonalInformation', 'Show the personal information tab on the user profile', 1, 2),
		(0, 'PersonProfile', 'ShowPersonProfileLanguageInformation', 'Show the language information tab on the user profile', 1, 3),
		(0, 'PersonProfile', 'ShowPersonProfileQualificationInformation', 'Show the qualification information tab on the user profile', 1, 4)

	END
--ENDIF
GO

UPDATE CC
SET 
	CC.ClientConfigurationValue = 'ClientFinanceExport_Hermis'
FROM client.ClientConfiguration CC
	JOIN client.Client C ON C.CLientID = CC.ClientID
		AND C.IntegrationCode = 'Hermis'
		AND CC.ClientConfigurationKey = 'ClientFinanceExportReportFileName'
GO

UPDATE CC
SET 
	CC.ClientConfigurationValue = '1'
FROM client.ClientConfiguration CC
	JOIN client.Client C ON C.CLientID = CC.ClientID
		AND C.IntegrationCode = 'Hermis'
		AND CC.ClientConfigurationKey = 'HasClientFinanceExport'
GO
--End table client.ClientConfiguration

--Begin table core.CalendarMetadata
UPDATE CM SET CM.Icon = 'fas fa-fw fa-check-circle' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Availability'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-exclamation-triangle' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'DPA'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-utensils' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'DSA'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-clock' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Time'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-tasks' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Project'
UPDATE CM SET CM.Icon = 'fas fa-fw fa-credit-card' FROM core.CalendarMetadata CM WHERE CM.EntityTypeCode = 'Expense'
GO
--End table core.CalendarMetadata

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-tachometer-alt',
	@NewMenuItemCode = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'far fa-fw fa-clock',
	@NewMenuItemCode = 'PersonProjectTimeList'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dollar-sign',
	@NewMenuItemCode = 'PersonProjectExpenseList'
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-edit',
	@NewMenuItemCode = 'InvoiceSetup'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'InvoiceList',
	@Icon = 'fas fa-fw fa-calendar-alt',
	@NewMenuItemCode = 'PersonCalendar',
	@NewMenuItemLink = '/person/calendar',
	@NewMenuItemText = 'Calendar'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'FAQList',
	@NewMenuItemLink = '/faq/list',
	@NewMenuItemText = 'FAQs',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'FAQ.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'DashboardChartHexColors', NULL, '#4dc9f6,#f67019,#f53794,#537bc4,#acc236,#166a8f,#00a950,#58595b,#8549ba,#d2a4d8'
EXEC core.SystemSetupAddUpdate 'DashboardDayCount', 'The number of days displayed in the dashboard time logged chart', '35'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', 'The logo used for printing', '/assets/img/DA_Logo_White_Big.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', 'The regular sized sit logo', '/assets/img/DA_Logo_White_Med.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', 'The small sized sit logo', '/assets/img/DA_Logo_White_Small.png'
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 'FAQ', 'AddUpdate', NULL, 'FAQ.AddUpdate', 'Administration', 'Add / edit a FAQ', 0, 1, 0, 0
EXEC permissionable.SavePermissionable 'FAQ', 'List', NULL, 'FAQ.List', 'Administration', 'View the list of FAQs', 0, 1, 0, 0
EXEC permissionable.SavePermissionable 'FAQ', 'View', NULL, 'FAQ.View', 'Administration', 'View a FAQ', 0, 1, 0, 0
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Invoice', 
	@DESCRIPTION='Export the client finance integration data', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Invoice.List.ClientFinanceExport', 
	@PERMISSIONCODE='ClientFinanceExport';
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the projects, time, expenses & availability calendar for a user', 
	@ISGLOBAL=1,
	@METHODNAME='Calendar', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.Calendar', 
	@PERMISSIONCODE=NULL;
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Assign permissionable templates to individuals on the person list page', 
	@ISGLOBAL=1,
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.List.PermissionableTemplate', 
	@PERMISSIONCODE='PermissionableTemplate';
GO

EXEC permissionable.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--End file Build File - 04 - Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 2.11 - 2019.01.20 17.15.45')
GO
--End build tracking

