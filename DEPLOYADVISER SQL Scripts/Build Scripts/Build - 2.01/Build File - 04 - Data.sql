USE DeployAdviser
GO

--Begin table core.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Invoice' AND ET.EmailTemplateCode = 'SubmitToClaimant')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
	VALUES
		('Invoice', 'SubmitToClaimant', 'Sent when an invoice is submitted for line manager review', 'An Invoice You Have Submitted Has Been Sent To A Line Manager For Review', '<p>You have submitted an invoice that requires a line manager review.</p><p>Invoice Details:</p><p>Invoice Date: &nbsp;[[InvoiceDateTimeFormatted]]<br />Client Name: &nbsp;[[ClientName]]<br />Project Name: &nbsp;[[ProjectName]]<br />Invoice Number: &nbsp;[[PersonInvoiceNumber]]<br />Invoice From: &nbsp;[[StartDateFormatted]]<br />Invoice To: &nbsp;[[EndDateFormatted]]<br /><br />The following individual(s) have recieved an email with a link to enable them to review your timesheet:</p><p>[[LineManagerArray]]</p><p>Access to your time sheet will require the PIN number that was provided to you on the screen after the submit and is shown below:</p><p>[[LineManagerPIN]]</p><p>You are responsiible for providing this number to the relevant individual(s).&nbsp; If the name(s) shown above are incorrect or unavailalbe please advise the deployments team responsible for your contract as they will be able to send the line manager approval email to an alternate individual</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')

	END
--ENDIF
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
DELETE D
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY ETF.EntityTypeCode, ETF.PlaceHolderText ORDER BY ETF.EntityTypeCode, ETF.PlaceHolderText, ETF.EmailTemplateFieldID) AS RowIndex,
		ETF.EmailTemplateFieldID
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = 'Invoice'
		AND ETF.PlaceHolderText IN ('[[LineManagerLink]]','[[LineManagerURL]]')
	) D
WHERE D.RowIndex > 1
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Invoice' AND ETF.PlaceHolderText = '[[LineManagerArray]]')
	BEGIN

	INSERT INTO core.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
	VALUES
		('Invoice', '[[LineManagerArray]]', 'The list of Line Managers')
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Invoice' AND ETF.PlaceHolderText = '[[LineManagerLink]]')
	BEGIN

	INSERT INTO core.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
	VALUES
		('Invoice', '[[LineManagerLink]]', 'Embedded Line Manager Link')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Invoice' AND ETF.PlaceHolderText = '[[LineManagerPIN]]')
	BEGIN

	INSERT INTO core.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
	VALUES
		('Invoice', '[[LineManagerPIN]]', 'Line Manager Invoice Review PIN')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Invoice' AND ETF.PlaceHolderText = '[[LineManagerURL]]')
	BEGIN

	INSERT INTO core.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
	VALUES
		('Invoice', '[[LineManagerURL]]', 'Line Manager URL')

	END
--ENDIF
GO
--End table core.EmailTemplateField

--Begin table dropdown.PasswordSecurityQuestion
IF NOT EXISTS (SELECT 1 FROM dropdown.PasswordSecurityQuestion PSQ WHERE PSQ.PasswordSecurityQuestionName = 'What is your favourite colour?')
	BEGIN

	INSERT INTO dropdown.PasswordSecurityQuestion 
		(GroupID, PasswordSecurityQuestionName, DisplayOrder) 
	VALUES 
		(1, 'What is your favourite colour?', 5),
		(1, 'What was the name of your first friend?', 6),
		(2, 'What was the name of your first school teacher?', 5),
		(2, 'What was the name or number of your first house?', 6),
		(3, 'What was your first job', 5),
		(3, 'Where did you go to university', 6)

	END
--ENDIF
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.PersonInvoiceNumberType
TRUNCATE TABLE dropdown.PersonInvoiceNumberType
GO

EXEC utility.InsertIdentityValue 'dropdown.PersonInvoiceNumberType', 'PersonInvoiceNumberTypeID', 0
GO

INSERT INTO dropdown.PersonInvoiceNumberType 
	(PersonInvoiceNumberTypeCode, PersonInvoiceNumberTypeName) 
VALUES
	('Text', 'Free Text'),
	('Increment', 'Incremental Numbers'),
	('Prefix', 'Incremental Numbers With Prefix')
GO
--End table dropdown.PersonInvoiceNumberType

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Layout', @DESCRIPTION='Allow access to the Feedback icon in the site header', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Layout.Default.CanHaveFeedback', @PERMISSIONCODE='CanHaveFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonProject', @DESCRIPTION='Allow change access to selected fields on a deployment record AFTER the deployment has been accpted', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='PersonProject.AddUpdate.Amend', @PERMISSIONCODE='Amend';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableTemplatePermissionable
INSERT INTO permissionable.PermissionableTemplatePermissionable
	(PermissionableTemplateID, PermissionableLineage)
SELECT
	PT.PermissionableTemplateID,
	'Layout.Default.CanHaveFeedback'
FROM permissionable.PermissionableTemplate PT
WHERE PT.PermissionableTemplateCode IN ('Administrator','ProjectManager')
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PermissionableTemplatePermissionable PTP
		WHERE PTP.PermissionableTemplateID = PT.PermissionableTemplateID
			AND PTP.PermissionableLineage = 'Layout.Default.CanHaveFeedback'
		)
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table person.PersonPermissionable
INSERT INTO person.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	P.PersonID,
	'Layout.Default.CanHaveFeedback'
FROM person.Person P
WHERE EXISTS
	(
	SELECT 1
	FROM client.ClientPerson CP
	WHERE CP.PersonID = P.PersonID
		AND CP.ClientPersonRoleCode IN ('Administrator','ProjectManager')
	)
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = P.PersonID
			AND PP.PermissionableLineage = 'Layout.Default.CanHaveFeedback'
		)
GO
--End table person.PersonPermissionable
