USE DeployAdviser
GO

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject

--Begin procedure utility.DropDefaultConstraint
EXEC utility.DropObject 'utility.DropDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
	FROM sys.default_constraints DC 
		JOIN sys.objects O ON O.object_id = DC.parent_object_id 
			AND O.type = 'U' 
		JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
		JOIN sys.columns C ON C.object_id = O.object_id 
			AND C.column_id = DC.parent_column_id 
			AND S.Name + '.' + O.Name = @TableName 
			AND C.Name = @ColumnName

	IF @cSQL IS NOT NULL
		EXECUTE (@cSQL)
	--ENDIF
		
END
GO
--End procedure utility.DropDefaultConstraint

