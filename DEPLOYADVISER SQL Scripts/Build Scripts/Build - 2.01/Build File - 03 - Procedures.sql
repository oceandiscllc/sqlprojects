USE DeployAdviser
GO

--Begin table dropdown.GetPersonInvoiceNumberTypeData
EXEC Utility.DropObject 'dropdown.GetPersonInvoiceNumberTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.PersonInvoiceNumberType table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetPersonInvoiceNumberTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PersonInvoiceNumberTypeID,
		T.PersonInvoiceNumberTypeCode,
		T.PersonInvoiceNumberTypeName
	FROM dropdown.PersonInvoiceNumberType T
	WHERE (T.PersonInvoiceNumberTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PersonInvoiceNumberTypeName, T.PersonInvoiceNumberTypeID

END
GO
--End procedure dropdown.GetPersonInvoiceNumberTypeData

--Begin procedure invoice.GetInvoiceByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceData
	SELECT
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		I.ISOCurrencyCode,
		core.FormatDate(I.InvoiceDateTime) AS InvoiceDateFormatted,
		I.InvoiceID,
		I.InvoiceStatus,
		I.LineManagerPIN,
		I.Notes,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND I.InvoiceID = @InvoiceID

END
GO
--End procedure invoice.GetInvoiceByInvoiceID

--Begin procedure invoice.GetInvoiceLinemanagerEmailDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceLinemanagerEmailDataByInvoiceID'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.12.05
-- Description:	A stored procedure to get data from the invoice.Invoice table
-- ==========================================================================
CREATE PROCEDURE invoice.GetInvoiceLinemanagerEmailDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE I
	SET I.InvalidLineManagerLoginAttempts = 0
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	--InvoiceSummaryData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		C.ClientName,
		core.FormatDate(I.EndDate) AS EndDateFormatted,
		core.FormatDateTime(I.InvoiceDateTime) AS InvoiceDateTimeFormatted,
		I.LineManagerPIN,
		I.LineManagerToken,
		I.PersonInvoiceNumber,
		core.FormatDate(I.StartDate) AS StartDateFormatted,
		PJ.ProjectName,
		PN.EmailAddress AS InvoicePersonEmailAddress,
		person.FormatPersonNameByPersonID(PN.PersonID, 'TitleFirstLast') AS InvoicePersonNameFormatted
	FROM invoice.Invoice I
		JOIN person.Person PN ON PN.PersonID = I.PersonID
		JOIN project.Project PJ ON PJ.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = PJ.ClientID
			AND I.InvoiceID = @InvoiceID

	--InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowPeopleByInvoiceID @InvoiceID

END
GO
--End procedure invoice.GetInvoiceLinemanagerEmailDataByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLog
	SELECT 
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode2 AS PerDiemCode,
		'DPA' AS PerdiemType,
		PL.DPAAmount * PPT.HasDPA AS Amount,
		PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DPAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	UNION

	SELECT 
		core.FormatDate(PPT.DateWorked) AS DateWorkedFormatted,
		PL.ProjectLocationName,
		C.FinanceCode3 AS PerDiemCode,
		'DSA' AS PerdiemType,
		PPT.DSAAmount AS Amount,
		PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS VAT,
		PL.DSAISOCurrencyCode AS ISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS ExchangeRate,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceAmount,
		PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceVAT,
		(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) + (PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceTotal
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
			AND PPT.InvoiceID = @InvoiceID
		JOIN project.Project P ON P.ProjectID = I.ProjectID
		JOIN client.Client C ON C.ClientID = P.ClientID

	ORDER BY 1, 3, 2, 9

END
GO
--End procedure invoice.GetInvoicePerDiemLogByInvoiceID

--Begin procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from the person.PersonProjectTime table
-- ===================================================================================
CREATE PROCEDURE invoice.GetInvoicePerDiemLogSummaryByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoicePerDiemLogSummary
	WITH IPL AS 
		(
		SELECT
			PL.ProjectLocationName,
			C.FinanceCode2 AS DPACode,
			PL.DPAAmount * PPT.HasDPA AS DPAAmount,
			PL.DPAAmount * PPT.HasDPA * (I.TaxRate / 100) * PPT.ApplyVAT AS DPAVAT,
			PL.DPAISOCurrencyCode,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceDPAAmount,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDPAVAT,
			CAST(PPT.HasDPA AS INT) AS HasDPA,
			C.FinanceCode3 AS DSACode,
			PPT.DSAAmount,
			PPT.DSAAmount * (I.TaxRate / 100) * PPT.ApplyVAT AS DSAVAT,
			PL.DSAISOCurrencyCode,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS InvoiceDSAAmount,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDSAVAT,
			CASE WHEN PPT.DSAAmount > 0 THEN 1 ELSE 0 END AS HasDSA
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
	    JOIN person.Person PN ON PN.PersonID = I.PersonID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	SELECT
		IPL.ProjectLocationName,
		IPL.DPACode AS PerDiemCode,
		'DPA' AS PerdiemType,
		SUM(IPL.HasDPA) AS Count,
		SUM(IPL.DPAAmount) AS Amount,
		SUM(IPL.DPAVAT) AS VAT,
		IPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(IPL.InvoiceDPAAmount) AS InvoiceAmount,
		SUM(IPL.InvoiceDPAVAT) AS InvoiceVAT,
		SUM(IPL.InvoiceDPAAmount) + SUM(IPL.InvoiceDPAVAT) AS InvoiceTotal
	FROM IPL
	GROUP BY IPL.ProjectLocationName, IPL.DPACode, IPL.DPAISOCurrencyCode

	UNION

	SELECT
		IPL.ProjectLocationName,
		IPL.DSACode AS PerDiemCode,
		'DSA' AS PerdiemType,
		SUM(IPL.HasDSA) AS Count,
		SUM(IPL.DSAAmount) AS Amount,
		SUM(IPL.DSAVAT) AS VAT,
		IPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(IPL.InvoiceDSAAmount) AS InvoiceAmount,
		SUM(IPL.InvoiceDSAVAT) AS InvoiceVAT,
		SUM(IPL.InvoiceDSAAmount) + SUM(IPL.InvoiceDSAVAT) AS InvoiceTotal
	FROM IPL
	GROUP BY IPL.ProjectLocationName, IPL.DSACode, IPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 4

END
GO
--End procedure invoice.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure invoice.GetInvoicePreviewData
EXEC utility.DropObject 'invoice.GetInvoicePreviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.22
-- Description:	A stored procedure to get data for an invoice
-- ==========================================================
CREATE PROCEDURE invoice.GetInvoicePreviewData

@PersonID INT,
@ProjectID INT,
@StartDate DATE,
@EndDate DATE,
@PersonProjectExpenseIDExclusionList VARCHAR(MAX),
@PersonProjectTimeIDExclusionList VARCHAR(MAX),
@InvoiceISOCurrencyCode CHAR(3)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cFinanceCode1 VARCHAR(50)
	DECLARE @cFinanceCode2 VARCHAR(50)
	DECLARE @cFinanceCode3 VARCHAR(50)
	DECLARE @nTaxRate NUMERIC(18,4) = (SELECT P.TaxRate FROM person.Person P WHERE P.PersonID = @PersonID)

	DECLARE @tExpenseLog TABLE
		(
		ExpenseLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		ExpenseDate DATE,
		ClientCostCodeName VARCHAR(50),
		ExpenseAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		TaxAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3), 
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DocumentGUID VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DECLARE @tPerDiemLog TABLE
		(
		PerDiemLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		DPAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		DSAAmount NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		DPAISOCurrencyCode CHAR(3),
		DSAISOCurrencyCode CHAR(3),
		DPAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		DSAExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0
		)

	DECLARE @tTimeLog TABLE
		(
		TimeLogID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		DateWorked DATE,
		ProjectLocationName VARCHAR(100),
		HoursPerDay NUMERIC(18,2) NOT NULL DEFAULT 0,
		HoursWorked NUMERIC(18,2) NOT NULL DEFAULT 0,
		ApplyVAT BIT,
		FeeRate NUMERIC(18,2) NOT NULL DEFAULT 0,
		ISOCurrencyCode CHAR(3),
		ExchangeRate NUMERIC(18,5) NOT NULL DEFAULT 0,
		ProjectLaborCode VARCHAR(50),
		OwnNotes VARCHAR(250),
		ProjectManagerNotes VARCHAR(250)
		)

	DELETE SR
	FROM reporting.SearchResult SR
	WHERE SR.PersonID = @PersonID

	INSERT INTO reporting.SearchResult
		(EntityTypeCode, EntityTypeGroupCode, EntityID, PersonID)
	SELECT
		'Invoice', 
		'PersonProjectExpense',
		PPE.PersonProjectExpenseID,
		@PersonID
	FROM person.PersonProjectExpense PPE
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPE.ExpenseDate >= @StartDate)
			AND (@EndDate IS NULL OR PPE.ExpenseDate <= @EndDate)
			AND PPE.InvoiceID = 0
			AND PPE.IsProjectExpense = 1
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectExpenseIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectExpenseIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPE.PersonProjectExpenseID
					)
				)

	UNION

	SELECT
		'Invoice', 
		'PersonProjectTime',
		PPT.PersonProjectTimeID,
		@PersonID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = PP.ProjectID
			AND (@StartDate IS NULL OR PPT.DateWorked >= @StartDate)
			AND (@EndDate IS NULL OR PPT.DateWorked <= @EndDate)
			AND PPT.InvoiceID = 0
			AND PP.PersonID = @PersonID
			AND P.ProjectID = @ProjectID
			AND 
				(
				@PersonProjectTimeIDExclusionList IS NULL
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@PersonProjectTimeIDExclusionList, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = PPT.PersonProjectTimeID
					)
				)

	SELECT 
		@cFinanceCode1 = C.FinanceCode1,
		@cFinanceCode2 = C.FinanceCode2,
		@cFinanceCode3 = C.FinanceCode3
	FROM client.Client C
		JOIN project.Project P ON P.ClientID = C.ClientID
			AND P.ProjectID = @ProjectID

	INSERT INTO @tExpenseLog
		(ExpenseDate, ClientCostCodeName, ExpenseAmount, TaxAmount, ISOCurrencyCode, ExchangeRate, DocumentGUID, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPE.ExpenseDate,
		CCC.ClientCostCodeName,
		PPE.ExpenseAmount,
		PPE.TaxAmount,
		PPE.ISOCurrencyCode, 
		invoice.GetExchangeRate(PPE.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPE.ExpenseDate),
		(SELECT D.DocumentGUID FROM document.Document D JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID AND DE.EntityTypeCode = 'PersonProjectExpense' AND DE.EntityID = PPE.PersonProjectExpenseID),
		PPE.OwnNotes,
		PPE.ProjectManagerNotes
	FROM person.PersonProjectExpense PPE
		JOIN client.ClientCostCode CCC ON CCC.ClientCostCodeID = PPE.ClientCostCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID
	ORDER BY PPE.ExpenseDate, CCC.ClientCostCodeName, PPE.PersonProjectExpenseID

	INSERT INTO @tPerDiemLog
		(DateWorked, ProjectLocationName, DPAAmount, DPAISOCurrencyCode, DPAExchangeRate, DSAAmount, DSAISOCurrencyCode, DSAExchangeRate, ApplyVAT)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		PL.DPAAmount * PPT.HasDPA AS DPAAmount,
		PL.DPAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DPAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.DSAAmount,
		PL.DSAISOCurrencyCode,
		invoice.GetExchangeRate(PL.DSAISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PPT.ApplyVAT
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	INSERT INTO @tTimeLog
		(DateWorked, ProjectLocationName, HoursPerDay, HoursWorked, ApplyVAT, FeeRate, ISOCurrencyCode, ExchangeRate, ProjectLaborCode, OwnNotes, ProjectManagerNotes)
	SELECT 
		PPT.DateWorked,
		PL.ProjectLocationName,
		P.HoursPerDay,
		PPT.HoursWorked,
		PPT.ApplyVAT,
		PP.FeeRate,
		PP.ISOCurrencyCode,
		invoice.GetExchangeRate(PP.ISOCurrencyCode, @InvoiceISOCurrencyCode, PPT.DateWorked),
		PLC.ProjectLaborCode,
		PPT.OwnNotes,
		PPT.ProjectManagerNotes
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PP.ProjectID
		JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	ORDER BY PPT.DateWorked, PL.ProjectLocationName, PPT.PersonProjectTimeID

	--InvoiceData
	SELECT
		NULL AS InvoiceStatus,
		core.FormatDate(getDate()) AS InvoiceDateFormatted,
		@StartDate AS StartDate,
		core.FormatDate(@StartDate) AS StartDateFormatted,
		@EndDate AS EndDate,
		core.FormatDate(@EndDate) AS EndDateFormatted,
		ISNULL(C.ClientCode, '') + PN.UserName AS PersonCode,
		C.ClientName,
		PINT.PersonInvoiceNumberTypeCode,
		PIT.ProjectInvoiceToAddress1, 
		PIT.ProjectInvoiceToAddress2, 
		PIT.ProjectInvoiceToAddress3, 
		PIT.ProjectInvoiceToAddressee, 
		PIT.ProjectInvoiceToEmailAddress, 
		PIT.ProjectInvoiceToISOCountryCode2,
		PIT.ProjectInvoiceToMunicipality, 
		PIT.ProjectInvoiceToName, 
		PIT.ProjectInvoiceToPhone, 
		PIT.ProjectInvoiceToPostalCode, 
		PIT.ProjectInvoiceToRegion, 
		PJ.IsForecastRequired,
		PJ.ProjectCode,
		PJ.ProjectID,
		PJ.ProjectName,
		PN.BillAddress1,
		PN.BillAddress2,
		PN.BillAddress3,
		PN.BillISOCountryCode2,
		PN.BillMunicipality,
		PN.BillPostalCode,
		PN.BillRegion,
		PN.EmailAddress,
		PN.CellPhone,
		PN.PersonID,
		person.FormatPersonNameByPersonID(PN.PersonID, 'FirstLast') AS PersonNameFormatted,
		PN.PersonInvoiceNumberIncrement,
		PN.PersonInvoiceNumberPrefix,
		ISNULL(PN.SendInvoicesFrom, PN.OwnCompanyName) AS SendInvoicesFrom,
		ISNULL(core.NullIfEmpty(PN.TaxID), 'None Provided') AS TaxID,
		PN.TaxRate
	FROM project.Project PJ
	CROSS JOIN person.Person PN
		JOIN client.Client C ON C.ClientID = PJ.ClientID
		JOIN client.ProjectInvoiceTo PIT ON PIT.ProjectInvoiceToID = PJ.ProjectInvoiceToID
			AND PJ.ProjectID = @ProjectID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = PN.PersonInvoiceNumberTypeID
			AND PN.PersonID = @PersonID

	--InvoiceExpenseLog
	SELECT
		TEL.ExpenseLogID,
		core.FormatDate(TEL.ExpenseDate) AS ExpenseDateFormatted,
		TEL.ClientCostCodeName,
		TEL.ISOCurrencyCode, 
		TEL.ExpenseAmount,
		TEL.TaxAmount,
		TEL.ExchangeRate,
		CAST(TEL.ExchangeRate * TEL.ExpenseAmount AS NUMERIC(18,2)) AS InvoiceExpenseAmount,
		CAST(TEL.ExchangeRate * TEL.TaxAmount AS NUMERIC(18,2)) AS InvoiceTaxAmount,

		CASE
			WHEN TEL.DocumentGUID IS NOT NULL
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + TEL.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS Document,

		TEL.OwnNotes,
		TEL.ProjectManagerNotes
	FROM @tExpenseLog TEL
	ORDER BY TEL.ExpenseLogID

	--InvoicePerDiemLog
	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		TPL.DPAAmount AS Amount,
		TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DPAExchangeRate AS ExchangeRate,
		TPL.DPAAmount * TPL.DPAExchangeRate AS InvoiceAmount,
		TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DPAAmount * TPL.DPAExchangeRate) + (TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	UNION

	SELECT
		core.FormatDate(TPL.DateWorked) AS DateWorkedFormatted,
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		TPL.DSAAmount AS Amount,
		TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		TPL.DSAExchangeRate AS ExchangeRate,
		TPL.DSAAmount * TPL.DSAExchangeRate AS InvoiceAmount,
		TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT AS InvoiceVAT,
		(TPL.DSAAmount * TPL.DSAExchangeRate) + (TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL

	ORDER BY 1, 3, 2, 8

	--InvoicePerDiemLogSummary
	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode2 AS PerDiemCode,
		(SELECT COUNT(TPL1.PerDiemLogID) FROM @tPerDiemLog TPL1 WHERE TPL1.DPAAmount > 0 AND TPL1.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DPAAmount) AS Amount,
		SUM(TPL.DPAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DPAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DPAAmount * TPL.DPAExchangeRate) + SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DPAISOCurrencyCode

	UNION

	SELECT 
		TPL.ProjectLocationName,
		@cFinanceCode3 AS PerDiemCode,
		(SELECT COUNT(TPL2.PerDiemLogID) FROM @tPerDiemLog TPL2 WHERE TPL2.DSAAmount > 0 AND TPL2.ProjectLocationName = TPL.ProjectLocationName) AS Count,
		SUM(TPL.DSAAmount) AS Amount,
		SUM(TPL.DSAAmount * (@nTaxRate / 100) * TPL.ApplyVAT) AS VAT,
		TPL.DSAISOCurrencyCode AS ISOCurrencyCode,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT,
		SUM(TPL.DSAAmount * TPL.DSAExchangeRate) + SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceTotal
	FROM @tPerDiemLog TPL
	GROUP BY TPL.ProjectLocationName, TPL.DSAISOCurrencyCode

	ORDER BY 1, 2, 3

	--InvoiceTimeLog
	SELECT 
		@cFinanceCode1 + CASE WHEN TTL.ProjectLaborCode IS NULL THEN '' ELSE '.' + TTL.ProjectLaborCode END AS LaborCode,
		TTL.TimeLogID,
		core.FormatDate(TTL.DateWorked) AS DateWorkedFormatted,
		TTL.ProjectLocationName,
		TTL.FeeRate,
		TTL.HoursWorked,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate AS NUMERIC(18,2)) AS Amount,
		CAST(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT) AS NUMERIC(18,2)) AS VAT,
		@InvoiceISOCurrencyCode AS ISOCurrencyCode,
		TTL.ExchangeRate,
		TTL.OwnNotes,
		TTL.ProjectManagerNotes
	FROM @tTimeLog TTL
	ORDER BY TTL.TimeLogID

	--InvoiceTimeLogSummary
	SELECT 
		TTL.ProjectLocationName,
		NULL AS LaborCode,
		TTL.FeeRate AS FeeRate,
		SUM(TTL.HoursWorked) AS HoursWorked,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS NUMERIC(18,2)) AS Amount,
		CAST(SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS NUMERIC(18,2)) AS VAT
	FROM @tTimeLog TTL
	GROUP BY TTL.ProjectLocationName, TTL.FeeRate
	ORDER BY TTL.ProjectLocationName

	;
	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate) AS InvoiceAmount,
			SUM(TTL.HoursWorked * (TTL.FeeRate / TTL.HoursPerDay) * TTL.ExchangeRate * ((@nTaxRate / 100) * TTL.ApplyVAT)) AS InvoiceVAT
		FROM @tTimeLog TTL

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DPAAmount * TPL.DPAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate) AS InvoiceAmount,
			SUM(TPL.DSAAmount * TPL.DSAExchangeRate * (@nTaxRate / 100) * TPL.ApplyVAT) AS InvoiceVAT
		FROM @tPerDiemLog TPL

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			SUM(TEL.ExpenseAmount * TEL.ExchangeRate) AS InvoiceAmount,
			SUM(TEL.TaxAmount * TEL.ExchangeRate) AS InvoiceVAT
		FROM @tExpenseLog TEL
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

	--PersonAccount
	SELECT 
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.IsDefault
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
		AND PA.IsActive = 1
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

END
GO
--End procedure invoice.GetInvoicePreviewData

--Begin procedure invoice.GetInvoiceTotalsByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceTotalsByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get data from varions invoice.Invoice% tables
-- ================================================================================
CREATE PROCEDURE invoice.GetInvoiceTotalsByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceTotals
	WITH INS AS
		(
		SELECT 
			1 AS DisplayOrder,
			'Fees' AS DisplayText,
			SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate) AS InvoiceAmount,
			SUM(PPT.HoursWorked * (PP.FeeRate / P.HoursPerDay) * PPT.ExchangeRate * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.Project P ON P.ProjectID = PP.ProjectID

		UNION

		SELECT 
			2 AS DisplayOrder,
			'DPA' AS DisplayText,
			SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceAmount,
			SUM(PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID

		UNION

		SELECT 
			3 AS DisplayOrder,
			'DSA' AS DisplayText,
			SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked)) AS InvoiceAmount,
			SUM(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT) AS InvoiceVAT
		FROM person.PersonProjectTime PPT
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID

		UNION

		SELECT 
			4 AS DisplayOrder,
			'Expenses' AS DisplayText,
			CAST(SUM(PPE.ExpenseAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceAmount,
			CAST(SUM(PPE.TaxAmount * PPE.ExchangeRate) AS NUMERIC(18,2)) AS InvoiceVAT
		FROM person.PersonProjectExpense PPE
		WHERE PPE.InvoiceID = @InvoiceID
		)

	SELECT
		INS.DisplayOrder,
		INS.DisplayText,
		INS.InvoiceAmount,
		INS.InvoiceVAT
	FROM INS

	UNION

	SELECT
		5,
		'Sub Totals' AS DisplayText,
		SUM(INS.InvoiceAmount) AS InvoiceAmount,
		SUM(INS.InvoiceVAT) AS InvoiceVAT
	FROM INS

	ORDER BY 1

END
GO
--End procedure invoice.GetInvoiceTotalsByInvoiceID

--Begin procedure invoice.GetInvoiceWorkflowDataByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowDataByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.29
-- Description:	A stored procedure to get workflow data for an invoice
-- ===================================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowDataByInvoiceID

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsInWorkflow BIT = 1

	--InvoiceWorkflowData
	IF EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Invoice' AND EWSGP.EntityID = @InvoiceID)
		EXEC workflow.GetEntityWorkflowData 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SET @bIsInWorkflow = 0

		UPDATE I
		SET I.InvoiceStatus = 'Line Manager Review'
		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		SELECT
			'Line Manager Approval' AS WorkflowStepName,
			1 AS WorkflowStepNumber,
			1 AS WorkflowStepCount

		END
	--ENDIF

	--qInvoiceWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'approve'
			THEN 'Approved Invoice'
			WHEN EL.EventCode = 'create'
			THEN 'Submitted Invoice'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Invoice'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'linemanagerapprove'
			THEN 'Line Manager Forwarded Invoice For Approval'
			WHEN EL.EventCode = 'linemanagerreject'
			THEN 'Line Manager Rejected Invoice'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN Invoice.Invoice I ON I.InvoiceID = EL.EntityID
			AND I.InvoiceID = @InvoiceID
			AND EL.EntityTypeCode = 'Invoice'
			AND EL.EventCode IN ('approve','create','decrementworkflow','incrementworkflow','linemanagerapprove','linemanagerreject')
	ORDER BY EL.CreateDateTime

	--InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceWorkflowPeopleByInvoiceID @InvoiceID, @bIsInWorkflow

END
GO
--End procedure invoice.GetInvoiceWorkflowDataByInvoiceID

--Begin procedure invoice.GetInvoiceWorkflowPeopleByInvoiceID
EXEC utility.DropObject 'invoice.GetInvoiceWorkflowPeopleByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.11.01
-- Description:	A stored procedure to get invoice workflow data
-- ============================================================
CREATE PROCEDURE invoice.GetInvoiceWorkflowPeopleByInvoiceID

@InvoiceID INT,
@IsInWorkflow BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--InvoiceWorkflowPeople
	IF @IsInWorkflow = 1
		EXEC workflow.GetEntityWorkflowPeople 'Invoice', @InvoiceID
	ELSE
		BEGIN

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			ISNULL(core.NullIfEmpty(PP.ManagerName), 'Name Not Provided') AS FullName,
			PP.ManagerEmailAddress AS EmailAddress,
			0 AS IsComplete,
			1 AS CanGetWorkflowEmail
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.ManagerEmailAddress IS NOT NULL

		UNION

		SELECT
			'Line Manager Group' AS WorkflowStepGroupName, 
			0 AS PersonID,
			ISNULL(core.NullIfEmpty(PP.AlternateManagerName), 'Name Not Provided') AS FullName,
			PP.AlternateManagerEmailAddress AS EmailAddress,
			0 AS IsComplete,
			1 AS CanGetWorkflowEmail
		FROM person.PersonProject PP
			JOIN invoice.Invoice I ON I.PersonID = PP.PersonID
				AND I.ProjectID = PP.ProjectID
				AND I.InvoiceID = @InvoiceID
				AND PP.AlternateManagerEmailAddress IS NOT NULL

		ORDER BY 1, 3

		END
	--ENDIF

END
GO
--End procedure invoice.GetInvoiceWorkflowPeopleByInvoiceID

--Begin procedure invoice.GetProjectTermOfReferencebyInvocieID
EXEC utility.DropObject 'invoice.GetProjectTermOfReferencebyInvocieID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2017.12.18
-- Description:	A stored procedure to get data from the project.ProjectTermOfReference
-- ===================================================================================
CREATE PROCEDURE invoice.GetProjectTermOfReferencebyInvocieID 

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TORCode VARCHAR(MAX)

	SELECT @TORCode = COALESCE(@TORCode + ', ', '') + PTOR.ProjectTermOfReferenceCode 
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			AND PPT.InvoiceID = @InvoiceID
	GROUP BY PTOR.ProjectTermOfReferenceCode 
	ORDER BY PTOR.ProjectTermOfReferenceCode 

	SELECT  @TORCode AS TORCode

END
GO
--End procedure invoice.GetProjectTermOfReferencebyInvocieID

--Begin procedure invoice.ProcessLineManagerInvoiceWorkflowAction
EXEC utility.DropObject 'invoice.ProcessLineManagerInvoiceWorkflowAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.29
-- Description:	A stored procedure to reject or move an invoice into the regular workflow based on a line manager action
-- =====================================================================================================================
CREATE PROCEDURE invoice.ProcessLineManagerInvoiceWorkflowAction

@InvoiceID INT = 0,
@WorkflowAction VARCHAR(50) = NULL,
@WorkflowComments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientID INT = 0

	UPDATE I
	SET
		I.InvalidLineManagerLoginAttempts = 0,
		I.LineManagerPin = NULL,
		I.LineManagerToken = NULL
	FROM invoice.Invoice I
	WHERE I.InvoiceID = @InvoiceID

	EXEC eventlog.LogInvoiceAction @InvoiceID, @WorkflowAction, 0, @WorkflowComments

	IF @WorkflowAction = 'LineManagerApprove'
		BEGIN

		SELECT @nClientID = P.ClientID
		FROM invoice.Invoice I
			JOIN project.Project P ON P.ProjectID = I.ProjectID

		EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID

		SELECT workflow.GetWorkflowStepName('Invoice', @InvoiceID) AS WorkflowStepName

		END
	ELSE IF @WorkflowAction = 'LineManagerReject'
		EXEC invoice.RejectInvoiceByInvoiceID @InvoiceID
	--ENDIF


END
GO
--End procedure invoice.ProcessLineManagerInvoiceWorkflowAction

--Begin procedure invoice.SubmitInvoice
EXEC utility.DropObject 'invoice.SubmitInvoice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to set the InvoiceID column in the person.PersonProjectExpense and person.PersonProjectTime tables
-- ==================================================================================================================================
CREATE PROCEDURE invoice.SubmitInvoice

@InvoiceID INT = 0,
@PersonID INT = 0,
@PersonUnavailabilityDatesToToggle VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsInWorkflow BIT = 0
	DECLARE @cInvoiceISOCurrencyCode CHAR(3)
	DECLARE @nClientID INT
	DECLARE @nHoursWorked NUMERIC(18,2)

	SELECT 
		@cInvoiceISOCurrencyCode = I.ISOCurrencyCode,
		@nClientID = P.ClientID 
	FROM project.Project P 
		JOIN invoice.Invoice I ON I.ProjectID = P.ProjectID 
			AND I.InvoiceID = @InvoiceID

	UPDATE P
	SET P.PersonInvoiceNumberIncrement = P.PersonInvoiceNumberIncrement + 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	UPDATE PPE
	SET 
		PPE.ExchangeRate = 
			CASE
				WHEN PPE.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PPE.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPE.ExpenseDate)
			END,

		PPE.InvoiceID = @InvoiceID
	FROM person.PersonProjectExpense PPE
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectExpense'
			AND SR.EntityID = PPE.PersonProjectExpenseID
			AND SR.PersonID = @PersonID

	UPDATE PPT
	SET 
		PPT.ExchangeRate = 
			CASE
				WHEN PP.ISOCurrencyCode = @cInvoiceISOCurrencyCode
				THEN 1
				ELSE invoice.GetExchangeRate(PP.ISOCurrencyCode, @cInvoiceISOCurrencyCode, PPT.DateWorked)
			END,

		PPT.InvoiceID = @InvoiceID
	FROM person.PersonProjectTime PPT
		JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Invoice'
			AND SR.EntityTypeGroupCode = 'PersonProjectTime'
			AND SR.EntityID = PPT.PersonProjectTimeID
			AND SR.PersonID = @PersonID
	;

	EXEC person.SavePersonUnavailabilityByPersonID @PersonID, @PersonUnavailabilityDatesToToggle

	DELETE SR
	FROM reporting.SearchResult SR 
	WHERE SR.EntityTypeCode = 'Invoice'
		AND SR.PersonID = @PersonID

	SELECT @nHoursWorked = ISNULL(SUM(PPT.HoursWorked), 0)
	FROM person.PersonProjectTime PPT
	WHERE PPT.InvoiceID = @InvoiceID

	IF @nHoursWorked = 0 OR (SELECT C.HasLineManagerReview FROM client.Client C WHERE C.ClientID = @nClientID) = 0
		BEGIN

		UPDATE I
		SET 
			I.LineManagerPin = NULL,
			I.LineManagerToken = NULL
		FROM invoice.Invoice I
		WHERE I.InvoiceID = @InvoiceID

		EXEC workflow.InitializeEntityWorkflow 'Invoice', @InvoiceID, @nClientID
		SET @bIsInWorkflow = 1

		END
	--ENDIF

	--InvoiceSummaryData / --InvoiceWorkflowData / --InvoiceWorkflowEventLog / --InvoiceWorkflowPeople
	EXEC invoice.GetInvoiceSummaryDataByInvoiceID @InvoiceID, @PersonID

	--InvoiceWorkflowTarget
	SELECT CASE WHEN @bIsInWorkflow = 0 THEN 'SubmitToLineManager' ELSE 'SubmitToWorkflow' END AS InvoiceWorkflowTarget

END
GO
--End procedure invoice.SubmitInvoice

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@AccessCode VARCHAR(500),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Client'
		SELECT @bHasAccess = 1 FROM client.Client T WHERE (T.ClientID = @EntityID AND @EntityID > 0 AND EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.ClientID = T.ClientID AND CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Administrator')) OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1)
	ELSE IF @EntityTypeCode = 'Invoice'
		BEGIN

		IF @AccessCode = 'View.Review'
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
			WHERE T.InvoiceID = @EntityID
				AND workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1

			END
		ELSE
			BEGIN

			SELECT @bHasAccess = 1 
			FROM invoice.Invoice T
				JOIN person.GetProjectsByPersonID(@PersonID) PBP ON PBP.ProjectID = T.ProjectID
					AND T.InvoiceID = @EntityID
					AND 
						(
						person.IsSuperAdministrator(@PersonID) = 1
							OR workflow.IsPersonInCurrentWorkflowStep('Invoice', T.InvoiceID, @PersonID) = 1
							OR 
								(
								workflow.GetWorkflowStepNumber('Invoice', T.InvoiceID) > workflow.GetWorkflowStepCount('Invoice', T.InvoiceID) 
									AND EXISTS
										(
										SELECT 1
										FROM workflow.EntityWorkflowStepGroupPerson EWSGP
										WHERE EWSGP.EntityTypeCode = 'Invoice'
											AND EWSGP.EntityID = T.InvoiceID
											AND EWSGP.PersonID = @PersonID
										)
								)
							OR T.PersonID = @PersonID
						)

			END
		--ENDIF

		END
	ELSE IF @EntityTypeCode = 'PersonProject'
		BEGIN

		SELECT @bHasAccess = 1 
		FROM person.PersonProject T
			JOIN project.Project P ON P.ProjectID = T.ProjectID
				AND T.PersonProjectID = @EntityID 
				AND (@AccessCode <> 'AddUpdate' OR T.AcceptedDate IS NULL OR person.HasPermission('PersonProject.AddUpdate.Amend', @PersonID) = 1)
				AND EXISTS
					(
					SELECT 1 
					FROM client.ClientPerson CP 
					WHERE CP.ClientID = P.ClientID 
						AND CP.PersonID = @PersonID 
						AND CP.ClientPersonRoleCode = 'Administrator'

					UNION

					SELECT 1
					FROM project.ProjectPerson PP
					WHERE PP.ProjectID = T.ProjectID
						AND PP.PersonID = @PersonID 

					UNION

					SELECT 1 
					FROM person.Person P 
					WHERE P.PersonID = @PersonID 
						AND P.IsSuperAdministrator = 1

					UNION

					SELECT 1
					FROM person.Person P 
					WHERE P.PersonID = T.PersonID
						AND T.PersonID = @PersonID 
						AND @AccessCode <> 'AddUpdate'
					)

		END
	ELSE IF @EntityTypeCode = 'PersonProjectExpense'
		SELECT @bHasAccess = 1 FROM person.PersonProjectExpense T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectExpenseID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'PersonProjectTime'
		SELECT @bHasAccess = 1 FROM person.PersonProjectTime T JOIN person.PersonProject PP ON PP.PersonProjectID = T.PersonProjectID AND T.PersonProjectTimeID = @EntityID AND PP.PersonID = @PersonID AND (@AccessCode <> 'AddUpdate' OR T.InvoiceID = 0)
	ELSE IF @EntityTypeCode = 'Project'
		SELECT @bHasAccess = 1 FROM person.GetProjectsByPersonID(@PersonID) T WHERE T.ProjectID = @EntityID
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetCalendarEntriesByPersonID
EXEC utility.DropObject 'person.GetCalendarEntriesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.10
-- Description:	A stored procedure to get data for the desktop calendar
-- ====================================================================
CREATE PROCEDURE person.GetCalendarEntriesByPersonID

@PersonID INT,
@StartDate DATE,
@EndDate DATE,
@EntityTypeCode VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDates TABLE (DateValue DATE) 
	DECLARE @tEntityTypeCodes TABLE (EntityTypeCode VARCHAR(50)) 

	INSERT INTO @tDates (DateValue) SELECT D.DateValue FROM core.GetDatesFromRange(@StartDate, @EndDate) D

	SET @EntityTypeCode = core.NullIfEmpty(@EntityTypeCode)
	IF @EntityTypeCode IS NOT NULL
		INSERT INTO @tEntityTypeCodes (EntityTypeCode) SELECT LTT.ListItem FROM core.ListToTable(@EntityTypeCode, ',') LTT
	--ENDIF

	SELECT
		CMD.BackgroundColor,
		CMD.EntityTypeCode,
		CMD.Icon,
		CMD.TextColor,
		D.EndDate,
		D.StartDate,
		D.Title,
		D.URL
	FROM
		(
		SELECT
			'Availability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(T.DateValue, 'yyyy-MM-dd') AS StartDate,
			'Available' AS Title,
			NULL AS URL
		FROM @tDates T
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Availability'))
			AND NOT EXISTS
				(
				SELECT 1
				FROM person.PersonUnavailability PU
				WHERE PU.PersonID = @PersonID
					AND PU.PersonUnavailabilityDate >= @StartDate
					AND PU.PersonUnavailabilityDate <= @EndDate
					AND PU.PersonUnavailabilityDate = T.DateValue
				)

		UNION

		SELECT
			'DPA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'DPA Invoiced'
				ELSE 'DPA Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DPA'))
				AND PPT.HasDPA = 1
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'DSA' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'DSA Invoiced'
				ELSE 'DSA Claimed'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'DSA'))
				AND PPT.DSAAmount > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Expense' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPE.InvoiceID > 0
				THEN 'Expense Invoiced'
				ELSE 'Expense Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPEC.PersonProjectExpenseID) FROM person.PersonProjectExpense PPEC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPEC.PersonProjectID AND PPC.PersonID = @PersonID AND PPEC.ExpenseDate = PPE.ExpenseDate) > 1
				THEN '/personprojectexpense/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPE.ExpenseDate, 'yyyy-MM-dd')
				ELSE '/personprojectexpense/view/id/' + CAST(PPE.PersonProjectExpenseID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Expense'))
				AND PP.PersonID = @PersonID
				AND PPE.ExpenseDate >= @StartDate
				AND PPE.ExpenseDate <= @EndDate

		UNION

		SELECT
			'Project' AS EntityTypeCode,
			FORMAT(DATEADD(d, 1, PP.EndDate), 'yyyy-MM-dd') AS EndDate,
			FORMAT(PP.StartDate, 'yyyy-MM-dd') AS StartDate,
			P.ProjectName + ' / ' + PTOR.ProjectTermOfReferenceName AS Title,
			'/personproject/view/id/' + CAST(PP.PersonProjectID AS VARCHAR(10)) AS URL
		FROM person.PersonProject PP
			JOIN project.Project P ON P.ProjectID = PP.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Project'))
				AND PP.PersonID = @PersonID
				AND PP.StartDate <= @EndDate
				AND PP.EndDate >= @StartDate
				AND PP.AcceptedDate IS NOT NULL

		UNION

		SELECT
			'Time' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PPT.DateWorked, 'yyyy-MM-dd') AS StartDate,

			CASE
				WHEN PPT.InvoiceID > 0
				THEN 'Time Invoiced'
				ELSE 'Time Logged'
			END AS Title,

			CASE
				WHEN (SELECT COUNT(PPTC.PersonProjectTimeID) FROM person.PersonProjectTime PPTC JOIN person.PersonProject PPC ON PPC.PersonProjectID = PPTC.PersonProjectID AND PPC.PersonID = @PersonID AND PPTC.DateWorked = PPT.DateWorked) > 1
				THEN '/personprojecttime/list/personid/' + CAST(@PersonID AS VARCHAR(10)) + '/startdate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd') + '/enddate/' + FORMAT(PPT.DateWorked, 'yyyy-MM-dd')
				ELSE '/personprojecttime/view/id/' + CAST(PPT.PersonProjectTimeID AS VARCHAR(10))
			END AS URL

		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Time'))
				AND PPT.HoursWorked > 0
				AND PP.PersonID = @PersonID
				AND PPT.DateWorked >= @StartDate
				AND PPT.DateWorked <= @EndDate

		UNION

		SELECT
			'Unavailability' AS EntityTypeCode,
			NULL AS EndDate,
			FORMAT(PU.PersonUnavailabilityDate, 'yyyy-MM-dd') AS StartDate,
			'Unavailable' AS Title,
			NULL AS URL
		FROM person.PersonUnavailability PU
		WHERE (@EntityTypeCode IS NULL OR EXISTS (SELECT 1 FROM @tEntityTypeCodes TEC WHERE TEC.EntityTypeCode = 'Unavailability'))
			AND PU.PersonID = @PersonID
			AND PU.PersonUnavailabilityDate >= @StartDate
			AND PU.PersonUnavailabilityDate <= @EndDate

		) D 
		JOIN core.CalendarMetadata CMD ON CMD.EntityTypeCode = D.EntityTypeCode

END
GO
--End procedure person.GetCalendarEntriesByPersonID

--Begin procedure person.GetPendingActionsByPersonID
EXEC utility.DropObject 'person.GetPendingActionsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return various data elemets regarding user pending actions
-- =============================================================================================
CREATE PROCEDURE person.GetPendingActionsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMonth INT =  MONTH(getDate())
	DECLARE @nYear INT = YEAR(getDate())
	DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))
	DECLARE @tTable TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, PendingAction VARCHAR(250), Link1 VARCHAR(MAX), Link2 VARCHAR(MAX))

	--ClientPerson
	INSERT INTO @tTable 
		(PendingAction, Link1, Link2) 
	SELECT
		'Accept an invitation to be a ' + LOWER(R.RoleName) + ' with ' + C.ClientName,
		'<button type="button" class="btn btn-sm btn-success" onClick="resolvePendingAction(''AcceptClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Accept</button>',
		'<button type="button" class="btn btn-sm btn-danger" onClick="resolvePendingAction(''RejectClientPerson'', ' + CAST(CP.ClientPersonID AS VARCHAR(10)) + ')">Reject</button>'
	FROM client.ClientPerson CP 
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN dropdown.Role R ON R.RoleCode = CP.ClientPersonRoleCode
			AND CP.PersonID = @PersonID
			AND CP.AcceptedDate IS NULL

	--PersonAccount
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND ((SELECT COUNT(PA.PersonAccountID) FROM person.PersonAccount PA WHERE PA.IsActive = 1 AND PA.PersonID = PP.PersonID) = 0)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Enter banking details for invoicing',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/banking">Go</a>'
			)

		END
	--ENDIF

	--PersonNextOfKin
	IF EXISTS 
		(
		SELECT 1 
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND P.IsNextOfKinInformationRequired = 1 
				AND ((SELECT COUNT(PNOK.PersonNextOfKinID) FROM person.PersonNextOfKin PNOK WHERE PNOK.PersonID = PP.PersonID) < 2)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add two emergency contact records',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/nextofkin">Go</a>'
			)

		END
	--ENDIF

	--PersonProject
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID
				AND PP.AcceptedDate IS NULL
				AND PP.EndDate >= getDate()
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Accept a project engagement',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/projects">Go</a>'
			)

		END
	--ENDIF

	--PersonProjectExpense / PersonProjectTime
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProjectExpense PPE
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPE.PersonProjectID
				AND PPE.ExpenseDate < @dStartDate
				AND PPE.IsProjectExpense = 1
				AND PPE.InvoiceID = 0
				AND PP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
				AND PPT.DateWOrked < @dStartDate
				AND PPT.InvoiceID = 0
				AND PP.PersonID = @PersonID
		)

		INSERT INTO @tTable (PendingAction) VALUES ('Enter invoicing data')
	--ENDIF

	--PersonProofOfLife
	IF EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP 
			JOIN project.Project P ON P.ProjectID = PP.ProjectID 
				AND PP.PersonID = @PersonID 
				AND ((SELECT COUNT(PPOL.PersonProofOfLifeID) FROM person.PersonProofOfLife PPOL WHERE PPOL.PersonID = PP.PersonID) < 3)
		)
		BEGIN

		INSERT INTO @tTable 
			(PendingAction, Link1) 
		VALUES 
			(
			'Add three proof of life questions',
			'<a class="btn btn-sm btn-warning" href="/person/addupdate/id/' + CAST(@PersonID AS VARCHAR(10)) + '/activetab/proofoflife">Go</a>'
			)

		END
	--ENDIF

	SELECT 
		T.PendingAction, 
		T.Link1, 
		T.Link2
	FROM @tTable T
	ORDER BY T.PendingAction

END
GO
--End procedure person.GetPendingActionsByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CT.ContractingTypeID,
		CT.ContractingTypeName,
		P.BillAddress1,
		P.BillAddress2,
		P.BillAddress3,
		P.BillISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.BillISOCountryCode2) AS BillCountryName,
		P.BillMunicipality,
		P.BillPostalCode,
		P.BillRegion,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.CollarSize,
		P.EmailAddress,
		P.FirstName,
		P.GenderCode,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = @PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) THEN 1 ELSE 0 END AS IsConsultant,
		P.IsPhoneVerified,
		P.IsRegisteredForUKTax,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MobilePIN,
		P.OwnCompanyName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PersonInvoiceNumberIncrement,
		P.PersonInvoiceNumberPrefix,
		P.RegistrationCode,
		P.SendInvoicesFrom,
		P.Suffix,
		P.TaxID,
		P.TaxRate,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		PINT.PersonInvoiceNumberTypeID,
		PINT.PersonInvoiceNumberTypeCode,
		PINT.PersonInvoiceNumberTypeName
	FROM person.Person P
		JOIN dropdown.ContractingType CT ON CT.ContractingTypeID = P.ContractingTypeID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.PersonInvoiceNumberType PINT ON PINT.PersonInvoiceNumberTypeID = P.PersonInvoiceNumberTypeID
			AND P.PersonID = @PersonID

	--PersonAccount
	SELECT
		newID() AS PersonAccountGUID,
		PA.IntermediateAccountNumber,
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch,
		PA.IntermediateBankName,
		PA.IntermediateBankRoutingNumber,
		PA.IntermediateIBAN,
		PA.IntermediateISOCurrencyCode,
		PA.IntermediateSWIFTCode,
		PA.IsActive,
		PA.IsDefault,
		PA.PersonAccountID,
		PA.PersonAccountName,
		PA.TerminalAccountNumber,
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch,
		PA.TerminalBankName,
		PA.TerminalBankRoutingNumber,
		PA.TerminalIBAN,
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonID = @PersonID
	ORDER BY PA.PersonAccountName, PA.PersonAccountID

	--PersonDocument
	SELECT
		D.DocumentName,

		CASE
			WHEN D.DocumentGUID IS NOT NULL 
			THEN '<a class="btn btn-md btn-primary" href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">Download</a>'
			ELSE '&nbsp;'
		END AS DownloadButton,

		D.DocumentID
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID 
			AND DE.EntityTypeCode = 'Person' 
			AND DE.EntityID = @PersonID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		PL.OralLevel,
		PL.ReadLevel,
		PL.WriteLevel
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonNextOfKin
	SELECT
		newID() AS PersonNextOfKinGUID,
		PNOK.Address1,
		PNOK.Address2,
		PNOK.Address3,
		PNOK.CellPhone,
		PNOK.ISOCountryCode2,
		PNOK.Municipality,
		PNOK.PersonNextOfKinName,
		PNOK.Phone,
		PNOK.PostalCode,
		PNOK.Region,
		PNOK.Relationship,
		PNOK.WorkPhone
	FROM person.PersonNextOfKin PNOK
	WHERE PNOK.PersonID = @PersonID
	ORDER BY PNOK.Relationship, PNOK.PersonNextOfKinName, PNOK.PersonNextOfKinID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	--PersonProofOfLife
	SELECT
		newID() AS PersonProofOfLifeGUID,
		core.FormatDateTime(PPOL.CreateDateTime) AS CreateDateTimeFormatted,
		PPOL.ProofOfLifeAnswer,
		PPOL.PersonProofOfLifeID,
		PPOL.ProofOfLifeQuestion
	FROM person.PersonProofOfLife PPOL
	WHERE PPOL.PersonID = @PersonID
	ORDER BY 2, PPOL.ProofOfLifeQuestion, PPOL.PersonProofOfLifeID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonAccountByPersonAccountID
EXEC utility.DropObject 'person.GetPersonAccountByPersonAccountID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.24
-- Description:	A stored procedure to return data from the person.PersonAccount
-- ============================================================================
CREATE PROCEDURE person.GetPersonAccountByPersonAccountID

@PersonAccountID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PA.IntermediateAccountNumber, 
		PA.IntermediateAccountPayee, 
		PA.IntermediateBankBranch, 
		PA.IntermediateBankName, 
		PA.IntermediateBankRoutingNumber, 
		PA.IntermediateIBAN, 
		PA.IntermediateISOCurrencyCode, 
		PA.IntermediateSWIFTCode, 
		PA.TerminalAccountNumber, 
		PA.TerminalAccountPayee, 
		PA.TerminalBankBranch, 
		PA.TerminalBankName, 
		PA.TerminalBankRoutingNumber, 
		PA.TerminalIBAN, 
		PA.TerminalISOCurrencyCode,
		PA.TerminalSWIFTCode
	FROM person.PersonAccount PA
	WHERE PA.PersonAccountID = @PersonAccountID

END
GO
--End procedure person.GetPersonAccountByPersonAccountID

--Begin procedure person.GetPersonProjectByPersonProjectID
EXEC utility.DropObject 'person.GetPersonProjectByPersonProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.14
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonProjectID
-- =============================================================================================================
CREATE PROCEDURE person.GetPersonProjectByPersonProjectID

@PersonProjectID INT,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPersonProjectLocation TABLE
		(
		ProjectLocationID INT,
		Days NUMERIC(18,2) NOT NULL DEFAULT 0
		)

	IF @PersonProjectID = 0
		BEGIN

		--PersonProject
		SELECT
			NULL AS AcceptedDate,
			NULL AS AcceptedDateFormatted,
			NULL AS AlternateManagerEmailAddress,
			NULL AS AlternateManagerName,
			NULL AS CurrencyName,
			0 AS IsAmendment,
			NULL AS ISOCurrencyCode,
			0 AS ClientFunctionID,
			NULL AS ClientFunctionName,
			0 AS InsuranceTypeID,
			NULL AS InsuranceTypeName,
			NULL AS EndDate,
			NULL AS EndDateFormatted,
			0 AS FeeRate,
			NULL AS ManagerEmailAddress,
			NULL AS ManagerName,
			NULL AS ProjectCode,
			NULL AS ProjectLaborCode,
			0 AS ProjectLaborCodeID,
			NULL AS ProjectLaborCodeName,
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			0 AS PersonProjectID,
			0 AS ProjectRoleID,
			NULL AS ProjectRoleName,
			NULL AS ProjectTermOfReferenceCode,
			0 AS ProjectTermOfReferenceID,
			NULL AS ProjectTermOfReferenceName,
			NULL AS Status,
			NULL AS StartDate,
			NULL AS StartDateFormatted,
			NULL AS UserName,
			C.ClientID,
			C.ClientName,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = P.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			P.HoursPerDay,
			P.ProjectID,
			P.ProjectName
		FROM project.Project P
			JOIN client.Client C ON C.ClientID = P.ClientID
				AND P.ProjectID = @ProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
		WHERE PL.ProjectID = @ProjectID

		END
	ELSE
		BEGIN

		--PersonProject
		SELECT
			C.CurrencyName,
			C.ISOCurrencyCode,
			CF.ClientFunctionID,
			CF.ClientFunctionName,
			CL.ClientID,
			CL.ClientName,
			IT.InsuranceTypeID,
			IT.InsuranceTypeName,
			PJ.ClientID,
			CASE WHEN EXISTS (SELECT 1 FROM client.ProjectLaborCode PLC WHERE PLC.ClientID = PJ.ClientID) THEN 1 ELSE 0 END AS HasProjectLaborCode,
			PJ.HoursPerDay,
			PJ.ProjectCode,
			PJ.ProjectID,
			PJ.ProjectName,
			person.FormatPersonNameByPersonID(PN.PersonID, 'LastFirstTitle') AS PersonNameFormatted,
			PN.UserName,
			PLC.ProjectLaborCode,
			PLC.ProjectLaborCodeID,
			PLC.ProjectLaborCodeName,
			PP.AcceptedDate,
			core.FormatDate(PP.AcceptedDate) AS AcceptedDateFormatted,
			CASE WHEN PP.AcceptedDate IS NOT NULL THEN 1 ELSE 0 END AS IsAmendment,
			PP.AlternateManagerEmailAddress,
			PP.AlternateManagerName,
			PP.EndDate,
			core.FormatDate(PP.EndDate) AS EndDateFormatted,
			PP.FeeRate,
			PP.ManagerEmailAddress,
			PP.ManagerName,
			PP.PersonID,
			PP.PersonProjectID,
			person.GetPersonProjectStatus(PP.PersonProjectID, 'Both') AS Status,
			PP.StartDate,
			core.FormatDate(PP.StartDate) AS StartDateFormatted,
			PR.ProjectRoleID,
			PR.ProjectRoleName,
			PTOR.ProjectTermOfReferenceCode,
			PTOR.ProjectTermOfReferenceID,
			PTOR.ProjectTermOfReferenceName
		FROM person.PersonProject PP
			JOIN client.ClientFunction CF ON CF.ClientFunctionID = PP.ClientFunctionID
			JOIN client.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = PP.ProjectLaborCodeID
			JOIN dropdown.Currency C ON C.ISOCurrencyCode = PP.ISOCurrencyCode
			JOIN dropdown.InsuranceType IT ON IT.InsuranceTypeID = PP.InsuranceTypeID
			JOIN dropdown.ProjectRole PR ON PR.ProjectRoleID = PP.ProjectRoleID
			JOIN person.Person PN ON PN.PersonID = PP.PersonID
			JOIN project.Project PJ ON PJ.ProjectID = PP.ProjectID
			JOIN client.Client CL ON CL.ClientID = PJ.ClientID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
				AND PP.PersonProjectID = @PersonProjectID

		INSERT INTO @tPersonProjectLocation
			(ProjectLocationID)
		SELECT
			PL.ProjectLocationID
		FROM project.ProjectLocation PL
			JOIN person.PersonProject PP ON PP.ProjectID = PL.ProjectID
				AND PP.PersonProjectID = @PersonProjectID

		UPDATE TPPL
		SET TPPL.Days = PPL.Days
		FROM @tPersonProjectLocation TPPL
			JOIN person.PersonProjectLocation PPL
				ON PPL.ProjectLocationID = TPPL.ProjectLocationID
					AND PPL.PersonProjectID = @PersonProjectID

		END
	--ENDIF

	--PersonProjectLocation
	SELECT
		newID() AS PersonProjectLocationGUID,
		C.CountryName, 
		C.ISOCountryCode2, 
		PL.CanWorkDay1, 
		PL.CanWorkDay2, 
		PL.CanWorkDay3, 
		PL.CanWorkDay4, 
		PL.CanWorkDay5, 
		PL.CanWorkDay6, 
		PL.CanWorkDay7,
		PL.DPAAmount, 
		PL.DPAISOCurrencyCode, 
		PL.DSACeiling, 
		PL.DSAISOCurrencyCode, 
		PL.IsActive,
		PL.ProjectLocationID,
		PL.ProjectLocationName, 
		TPL.Days
	FROM @tPersonProjectLocation TPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = TPL.ProjectLocationID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = PL.ISOCountryCode2
	ORDER BY PL.ProjectLocationName, PL.ProjectLocationID

END
GO
--End procedure person.GetPersonProjectByPersonProjectID

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsConsultant BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsConsultant BIT,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,

		@bIsConsultant = 
			CASE 
				WHEN EXISTS (SELECT 1 FROM client.ClientPerson CP WHERE CP.PersonID = P.PersonID AND CP.ClientPersonRoleCode = 'Consultant' AND CP.AcceptedDate IS NOT NULL) 
				THEN 1 
				ELSE 0 
			END,

		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
	WHERE P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END
	SET @cRequiredProfileUpdate = CASE WHEN LEFT(@cRequiredProfileUpdate, 1) = ',' THEN STUFF(@cRequiredProfileUpdate, 1, 1, '') ELSE @cRequiredProfileUpdate END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(CellPhone,CountryCallingCodeID,EmailAddress,FullName,IsAccountLockedOut,IsActive,IsConsultant,IsPasswordExpired,IsPhoneVerified,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,PersonID,RequiredProfileUpdate,UserName) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsConsultant,
			@bIsPasswordExpired,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure project.ValidateHoursWorked
EXEC utility.DropObject 'project.ValidateHoursWorked'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.25
-- Description:	A stored procedure to validate HoursWorked from a PersonProjectID
-- ==============================================================================
CREATE PROCEDURE project.ValidateHoursWorked

@DateWorkedCount INT,
@HoursWorked INT,
@PersonProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPersonProjectTimeAllotted INT
	DECLARE @nPersonProjectTimeExpended INT

	SELECT
		@nPersonProjectTimeAllotted = SUM((PPL.Days * P.HoursPerDay))
	FROM person.PersonProjectLocation PPL
		JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPL.ProjectLocationID
		JOIN project.Project P ON P.ProjectID = PL.ProjectID
			AND PPL.PersonProjectID = @PersonProjectID

	SELECT
		@nPersonProjectTimeExpended = SUM(PPT.HoursWorked)
	FROM person.PersonProjectTime PPT
	WHERE PPT.PersonProjectID = @PersonProjectID

	SELECT
		CASE
			WHEN ISNULL(@nPersonProjectTimeAllotted, 0) >= ISNULL(@nPersonProjectTimeExpended, 0) + (@DateWorkedCount * @HoursWorked)
			THEN 1
			ELSE 0
		END AS IsValid

END
GO
--End procedure project.ValidateHoursWorked

--Begin procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID
EXEC utility.DropObject 'reporting.GetInvoicePerDiemLogSummaryByInvoiceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Inderjeet Kaur
-- Create date:	2017.12.07
-- Description:	A stored procedure to return invoice per diem data
-- ===============================================================
CREATE PROCEDURE reporting.GetInvoicePerDiemLogSummaryByInvoiceID 

@InvoiceID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH IPL AS 
		(
		SELECT
			C.ClientCode,
			C.ClientCode + PN.UserName AS VendorID,
			P.ProjectCode + '.' + PTOR.ProjectTermOfReferenceCode AS ProjectChargeCode,
			C.FinanceCode2 AS DPACode,
			CAST((CASE WHEN PPT.HasDPA = 1 THEN PL.DPAAmount ELSE 0 END) * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDPAAmount,
			PL.DPAAmount * PPT.HasDPA * invoice.GetExchangeRate(PL.DPAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDPAVAT,
			C.FinanceCode3 AS DSACode,
			PPT.DSAAmount,
			CAST(PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) AS NUMERIC(18,2)) AS InvoiceDSAAmount,
			PPT.DSAAmount * invoice.GetExchangeRate(PL.DSAISOCurrencyCode, I.ISOCurrencyCode, PPT.DateWorked) * (I.TaxRate / 100) * PPT.ApplyVAT AS InvoiceDSAVAT
		FROM person.PersonProjectTime PPT
			JOIN person.PersonProject PP ON PP.PersonProjectID = PPT.PersonProjectID
			JOIN project.ProjectLocation PL ON PL.ProjectLocationID = PPT.ProjectLocationID
			JOIN invoice.Invoice I ON I.InvoiceID = PPT.InvoiceID
				AND PPT.InvoiceID = @InvoiceID
	    JOIN person.Person PN ON PN.PersonID = I.PersonID
			JOIN project.Project P ON P.ProjectID = I.ProjectID
			JOIN project.ProjectTermOfReference PTOR ON PTOR.ProjectTermOfReferenceID = PP.ProjectTermOfReferenceID
			JOIN client.Client C ON C.ClientID = P.ClientID
		)

	SELECT
		D.ClientCode,
		D.VendorID,
		D.ProjectChargeCode,
		D.PerdiemType,
		D.Code,
		CAST(SUM(D.Amount) AS NUMERIC(18,2)) AS Amount ,
		CAST(SUM(D.VAT) AS NUMERIC(18,2)) AS VAT
	FROM
		(
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DPA' AS PerdiemType,
			IPL.DPACode AS Code,
			IPL.InvoiceDPAAmount AS Amount,
			IPL.InvoiceDPAVAT AS VAT
		FROM IPL

		UNION ALL
	
		SELECT
			IPL.ClientCode,
			IPL.VendorID,
			IPL.ProjectChargeCode,
			'DSA' AS PerdiemType,
			IPL.DSACode AS Code,
			IPL.InvoiceDSAAmount AS Amount,
			IPL.InvoiceDSAVAT AS VAT
		FROM IPL
		) D 
	GROUP BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode, D.Code
	ORDER BY D.ClientCode, D.VendorID, D.PerdiemType, D.ProjectChargeCode

END
GO
--End procedure reporting.GetInvoicePerDiemLogSummaryByInvoiceID

--Begin procedure workflow.DecrementWorkflow
EXEC utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
		workflow.GetWorkflowStepName(@EntityTypeCode, @EntityID) AS WorkflowStepName

END
GO
--End procedure workflow.DecrementWorkflow

--Begin procedure workflow.IncrementWorkflow
EXEC utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
			workflow.GetWorkflowStepName(@EntityTypeCode, @EntityID) AS WorkflowStepName
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
			NULL AS WorkflowStepName

		END
	--ENDIF

END
GO
--End procedure workflow.IncrementWorkflow
