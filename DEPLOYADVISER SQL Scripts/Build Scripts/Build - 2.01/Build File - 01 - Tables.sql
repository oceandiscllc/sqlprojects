USE DeployAdviser
GO

--Begin table dropdown.PersonInvoiceNumberType
DECLARE @TableName VARCHAR(250) = 'dropdown.PersonInvoiceNumberType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PersonInvoiceNumberType
	(
	PersonInvoiceNumberTypeID INT IDENTITY(0,1) NOT NULL,
	PersonInvoiceNumberTypeCode VARCHAR(50),
	PersonInvoiceNumberTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonInvoiceNumberTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonInvoiceNumberType', 'DisplayOrder,PersonInvoiceNumberTypeName'
GO
--End table dropdown.PersonInvoiceNumberType

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'PersonInvoiceNumberTypeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PersonInvoiceNumberIncrement', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PersonInvoiceNumberPrefix', 'VARCHAR(10)'
GO
--End table person.Person

--Begin table person.PersonAccount
DECLARE @TableName VARCHAR(250) = 'person.PersonAccount'

EXEC utility.AddColumn @TableName, 'IsDefault', 'BIT', '0'
GO

EXEC utility.DropObject 'person.TR_PersonAccount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.28
-- Description:	A trigger to ensure there is only 1 default account per person id
-- ==============================================================================
CREATE TRIGGER person.TR_PersonAccount ON person.PersonAccount AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	DECLARE @nIsDefault BIT = (SELECT INS.IsDefault FROM INSERTED INS)

	IF @nIsDefault = 1
		BEGIN

		UPDATE PA
		SET 
			PA.IsDefault = 0
		FROM person.PersonAccount PA
			JOIN INSERTED INS ON INS.PersonID = PA.PersonID
				AND INS.PersonAccountID <> PA.PersonAccountID

		END
	--ENDIF

END
GO
--End table person.PersonAccount

--Begin table person.PersonProjectLocation
DECLARE @TableName VARCHAR(250) = 'person.PersonProjectLocation'

EXEC utility.DropDefaultConstraint @TableName, 'Days'

ALTER TABLE person.PersonProjectLocation ALTER COLUMN Days NUMERIC(18,2)

EXEC utility.SetDefaultConstraint @TableName, 'Days', 'NUMERIC(18,2)', '0'
GO
--End table person.PersonProjectLocation
