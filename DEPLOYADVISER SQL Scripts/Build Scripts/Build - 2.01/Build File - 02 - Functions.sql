USE DeployAdviser
GO

--Begin function person.GetPersonProjectDays
EXEC utility.DropObject 'person.GetPersonProjectDays'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to get a data from a person.PersonProject record
-- ========================================================================

CREATE FUNCTION person.GetPersonProjectDays
(
@PersonProjectID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nPersonProjectDays NUMERIC(18,2)

	SELECT @nPersonProjectDays = SUM(PPL.Days)
	FROM person.PersonProjectLocation PPL
	WHERE PPL.PersonProjectID = @PersonProjectID

	RETURN ISNULL(@nPersonProjectDays, 0)

END
GO
--End function person.GetPersonProjectDays

--Begin function workflow.GetWorkflowStepName
EXEC utility.DropObject 'workflow.GetWorkflowStepName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to get a workflow step name for a specific EntityTypeCode and EntityID
-- ==============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepName
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStepName VARCHAR(250)

	SELECT TOP 1 @cWorkflowStepName = EWSGP.WorkflowStepName 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID 
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	RETURN @cWorkflowStepName

END
GO
--End function workflow.GetWorkflowStepName

