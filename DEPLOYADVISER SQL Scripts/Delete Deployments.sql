SELECT * FROM [client].[Client] WHERE ClientID = 1
SELECT * FROM [client].[ClientPerson] WHERE PersonID = 6 AND  ClientID = 1
SELECT * FROM project.Project WHERE ClientID = 1

DELETE PP FROM person.PersonProject PP WHERE PP.PersonID = 6
DELETE PPE FROM person.PersonProjectExpense PPE WHERE NOT EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonProjectID = PPE.PersonProjectID)
DELETE PPE FROM person.PersonProjectTime PPE WHERE NOT EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonProjectID = PPE.PersonProjectID)
DELETE I FROM invoice.Invoice I WHERE I.PersonID = 6
DELETE DE From [document].[DocumentEntity] DE WHERE DE.EntityTypeCode = 'Invoice' AND NOT EXISTS (SELECT 1 FROM invoice.Invoice I WHERE I.InvoiceID = DE.EntityID)
DELETE D From [document].[Document] D WHERE NOT EXISTS (SELECT 1 FROM [document].[DocumentEntity] DE WHERE DE.DocumentID = D.DocumentID)

