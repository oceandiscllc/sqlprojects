SELECT 
	S.Name AS SchemaName,
	T.Name AS TableName, 
	PS.Row_Count AS RecordCount
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.DM_DB_Partition_Stats PS ON PS.Object_ID = T.Object_ID
		AND T.Type = 'U'
		AND PS.Index_ID IN (0,1)
ORDER BY 1, 2