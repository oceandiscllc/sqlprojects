--DELETE D
SELECT D.*
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY ETF.EntityTypeCode, ETF.PlaceHolderText ORDER BY ETF.EntityTypeCode, ETF.PlaceHolderText, ETF.EmailTemplateFieldID) AS RowIndex,
		ETF.EmailTemplateFieldID,
		ETF.EntityTypeCode,
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = 'Invoice'
		AND ETF.PlaceHolderText IN ('[[LineManagerLink]]','[[LineManagerURL]]')
	) D
WHERE D.RowIndex > 1
