USE SBRS_DHHS
GO

--Begin procedure person.ActivateAccountByPersonID
EXEC utility.DropObject 'person.ActivateAccountByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:   Todd Pires
-- Create Date: 2017.07.14
-- Description: A stored procedure to update data in the person.Person table
-- =========================================================================
CREATE PROCEDURE person.ActivateAccountByPersonID

@PersonID INT

AS
BEGIN
SET NOCOUNT ON;
	
	UPDATE P
	SET P.IsActive = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.ActivateAccountByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC Utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName,
		P.IsActive,
		P.JobTitle,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLogin,
		P.LastName,
		P.PersonID,
		P.PhoneNumber,
		O.OrganizationID,
		O.OrganizationName
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonDataBySearchCriteria
EXEC Utility.DropObject 'person.GetPersonDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table based on search criteria
-- =================================================================================================
CREATE PROCEDURE person.GetPersonDataBySearchCriteria

@EmailAddress VARCHAR(320) = NULL,
@FirstLetter CHAR(1) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = NULL,
@JobTitle VARCHAR(250) = NULL,
@Keyword VARCHAR(250) = NULL,
@LastName VARCHAR(50) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'LastName',
@OrganizationID INT = 0,
@OrganizationRoleOrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PersonID INT = 0,
@PhoneNumber VARCHAR(50) = NULL,
@RoleCode VARCHAR(50) = NULL,
@RoleID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	IF @Keyword IS NOT NULL
		SET @Keyword = '%' + @Keyword + '%'
	--ENDIF
	IF @RoleCode IS NOT NULL
		SET @RoleID = ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = @RoleCode), 0)
	--ENDIF

	;
	WITH PD AS
		(
		SELECT
			P.PersonID
		FROM person.Person P
			JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
				AND (@IsActive IS NULL OR P.IsActive = @IsActive)
				AND (@EmailAddress IS NULL OR P.EmailAddress LIKE '%' + @EmailAddress + '%')
				AND (@FirstLetter IS NULL OR LEFT(P.LastName, 1) = @FirstLetter)
				AND (@FirstName IS NULL OR P.FirstName LIKE '%' + @FirstName + '%')			
				AND (@JobTitle IS NULL OR P.JobTitle LIKE '%' + @JobTitle + '%')
				AND 
					(
					@Keyword IS NULL 
						OR P.EmailAddress LIKE @Keyword
						OR P.FirstName LIKE @Keyword
						OR P.JobTitle LIKE @Keyword
						OR P.LastName LIKE @Keyword
						OR O.OrganizationName LIKE @Keyword
						OR P.PhoneNumber LIKE @Keyword
					)
				AND (@LastName IS NULL OR P.LastName LIKE '%' + @LastName + '%')
				AND (@OrganizationID = 0 OR P.OrganizationID = @OrganizationID)
				AND (@PhoneNumber IS NULL OR P.PhoneNumber LIKE '%' + @PhoneNumber + '%')
				AND 
					(
					@RoleID = 0 
						OR EXISTS
							(
							SELECT 1
							FROM person.PersonOrganizationRole POR
							WHERE POR.PersonID = P.PersonID
								AND (@OrganizationRoleOrganizationID = 0 OR POR.OrganizationID = @OrganizationRoleOrganizationID)
								AND POR.RoleID = @RoleID
							)
					)
		)

	SELECT
		O.OrganizationName,
		P.IsActive,
		P.JobTitle,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PersonID, 
		P.EmailAddress,
		P.PhoneNumber,
		(SELECT COUNT(RT.PersonID) FROM person.Person RT) AS RecordsTotal,
		(SELECT COUNT(PD.PersonID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN person.Person P ON P.PersonID = PD.PersonID
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
	ORDER BY 
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
		CASE WHEN @OrderByField = 'JobTitle' AND @OrderByDirection = 'ASC' THEN P.JobTitle END ASC,
		CASE WHEN @OrderByField = 'JobTitle' AND @OrderByDirection = 'DESC' THEN P.JobTitle END DESC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'PhoneNumber' AND @OrderByDirection = 'ASC' THEN P.PhoneNumber END ASC,
		CASE WHEN @OrderByField = 'PhoneNumber' AND @OrderByDirection = 'DESC' THEN P.PhoneNumber END DESC,
		CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
		CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
		PD.PersonID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure person.GetPersonDataBySearchCriteria

--Begin procedure person.GetPersonOrganizationRolesByPersonID
EXEC Utility.DropObject 'person.GetPersonRolesByPersonID'
EXEC Utility.DropObject 'person.GetPersonOrganizationRolesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.PersonOrganizationRole table
-- ========================================================================================
CREATE PROCEDURE person.GetPersonOrganizationRolesByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		POR.PersonOrganizationRoleID,
		R.RoleID,
		R.RoleName,
		O.OrganizationID,
		O.OrganizationName
	FROM person.PersonOrganizationRole POR
		JOIN dropdown.Role R ON R.RoleID = POR.RoleID
			AND POR.PersonID = @PersonID
		JOIN dropdown.Organization O ON O.OrganizationID = POR.OrganizationID
	ORDER BY O.OrganizationName, O.OrganizationID, R.RoleName, R.RoleID

END
GO
--End procedure person.GetPersonOrganizationRolesByPersonID

--Begin procedure person.SavePerson
EXEC utility.DropObject 'person.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to save data to the person.Person table
-- =======================================================================
CREATE PROCEDURE person.SavePerson

@EmailAddress VARCHAR(320),
@FirstName VARCHAR(50),
@IsActive BIT,
@JobTitle VARCHAR(250),
@LastName VARCHAR(50),
@OrganizationID INT,
@Password VARCHAR(64),
@PersonID INT,
@PhoneNumber VARCHAR(50)

AS
BEGIN
SET NOCOUNT ON;
	
	DECLARE @cEventCode VARCHAR(50)
	DECLARE @cPasswordSalt VARCHAR(50) = newID()

	IF @PersonID = 0
		BEGIN

		SET @cEventCode = 'Create'

		DECLARE @tOutput TABLE (PersonID INT)

		INSERT INTO person.Person
			(
			EmailAddress,
			FirstName,
			IsActive,
			JobTitle,
			LastName,
			OrganizationID,
			Password,
			PasswordSalt,
			PhoneNumber
			)
		OUTPUT INSERTED.PersonID INTO @tOutput
		VALUES
			(
			@EmailAddress,
			@FirstName,
			@IsActive,
			@JobTitle,
			@LastName,
			@OrganizationID,
			person.HashPassword(@Password, @cPasswordSalt),
			@cPasswordSalt,
			@PhoneNumber
			)
		
		SELECT @PersonID = O.PersonID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @cEventCode = 'Update'

		UPDATE P
		SET
			P.EmailAddress = @EmailAddress,
			P.FirstName = @FirstName,
			P.IsActive = @IsActive,
			P.JobTitle = @JobTitle,
			P.LastName = @LastName,
			P.OrganizationID = @OrganizationID,
			P.Password = CASE WHEN @Password IS NULL OR LEN(RTRIM(@Password)) = 0 THEN P.Password ELSE person.HashPassword(@Password, P.PasswordSalt) END,
			P.PhoneNumber = @PhoneNumber
		FROM person.Person P
		WHERE P.PersonID = @PersonID

		DELETE T FROM person.PersonOrganizationRole T WHERE T.PersonID = @PersonID

		END
	--ENDIF

	SELECT
		@cEventCode AS EventCode,
		P.PersonID
	FROM person.Person P
	WHERE PersonID = @PersonID

END
GO
--End procedure person.SavePerson

--Begin procedure person.SavePersonOrganizationRole
EXEC utility.DropObject 'person.SavePersonOrganizationRole'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to save data to the person.PersonOrganizationRole table
-- =======================================================================================
CREATE PROCEDURE person.SavePersonOrganizationRole

@PersonID INT,
@OrganizationID INT,
@RoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cNotificationRoleCode VARCHAR(50)

	IF NOT EXISTS (SELECT 1 FROM person.PersonOrganizationRole POR WHERE POR.PersonID = @PersonID AND POR.OrganizationID = @OrganizationID AND POR.RoleID = @RoleID)
		BEGIN

		INSERT INTO person.PersonOrganizationRole
			(
			PersonID,
			OrganizationID,
			RoleID
			)
		VALUES
			(
			@PersonID,
			@OrganizationID,
			@RoleID
			)

		END
	--ENDIF

	SELECT R.NotificationRoleCode
	FROM dropdown.Role R
	WHERE R.RoleID = @RoleID

END
GO
--End procedure person.SavePersonOrganizationRole

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@EmailAddress VARCHAR(320),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsActive BIT = 0
	DECLARE @bIsValidPassword BIT = 0
	DECLARE @cOrganizationDisplayCode VARCHAR(50)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhoneNumber VARCHAR(50)
	DECLARE @dPasswordExpirationDateTime DATETIME
	DECLARE @nPersonID INT = ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.EmailAddress = @EmailAddress), 0)

	DECLARE @tPerson TABLE (PersonID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	IF @nPersonID = 0
		SET @nPersonID = ISNULL((SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.EmailAddress = @EmailAddress), 0)
	--ENDIF

	SELECT
		@bIsActive = P.IsActive,
		@cOrganizationDisplayCode = (SELECT O.OrganizationDisplayCode FROM dropdown.Organization O WHERE O.OrganizationID = P.OrganizationID),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cPhoneNumber = P.PhoneNumber,
		@dPasswordExpirationDateTime = P.PasswordExpirationDateTime,
		@nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.PersonID = @nPersonID

	INSERT INTO @tPerson (PersonID) VALUES (@nPersonID)

	IF @nPersonID > 0
		BEGIN

		IF @cPasswordDB IS NULL
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA1', @Password), 2)

			IF EXISTS (SELECT 1 FROM migration.UserMigration UM WHERE UM.EmailAddress = @EmailAddress AND UM.Password = @cPasswordHash)
				BEGIN
	
				SET @bIsValidPassword = 1
				SET @cPasswordSalt = newID()
				SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

				UPDATE P
				SET 
					P.PasswordSalt = @cPasswordSalt,
					P.Password = @cPasswordHash
				FROM person.Person P
				WHERE P.PersonID = @nPersonID

				DELETE UM
				FROM migration.UserMigration UM
				WHERE UM.PersonID = @nPersonID

				END
			--ENDIF

			END
		ELSE
			BEGIN

			SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsValidPassword = 0 AND @IncrementInvalidLoginAttempts = 1
			BEGIN
		
			UPDATE P
			SET P.InvalidLoginAttempts = P.InvalidLoginAttempts + 1
			FROM person.Person P
			WHERE P.PersonID = @nPersonID

			END
		--ENDIF

		IF @bIsValidPassword = 1
			BEGIN

			UPDATE P
			SET 
				P.InvalidLoginAttempts = 0,
				P.LastLoginDateTime = getDate()
			FROM person.Person P
			WHERE PersonID = @nPersonID

			END
		--ENDIF
	
		END
	--ENDIF	
	
	SELECT 
		@EmailAddress AS EmailAddress,
		ISNULL(@bIsActive, 0) AS IsActive,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM person.PersonOrganizationRole POR JOIN dropdown.Role R ON R.RoleID = POR.RoleID AND POR.PersonID = TP.PersonID AND R.RoleCode = 'SysAdmin')
			THEN 1 
			ELSE 0 
		END AS IsSystemAdministrator,

		CASE 
			WHEN @dPasswordExpirationDateTime IS NULL OR @dPasswordExpirationDateTime < SYSUTCDateTime()
			THEN 1 
			ELSE 0 
		END AS IsPasswordExpired,

		CASE
			WHEN TP.PersonID > 0
			THEN 1
			ELSE 0
		END AS IsValidEmailAddress,

		@bIsValidPassword AS IsValidPassword,
		@cOrganizationDisplayCode AS OrganizationDisplayCode,
		TP.PersonID,
		person.FormatPersonNameByPersonID(TP.PersonID, 'FirstLast') AS PersonNameFormatted,
		@cPhoneNumber AS PhoneNumber
	FROM @tPerson TP

END
GO
--End procedure person.ValidateLogin
