USE SBRS_DHHS
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(10)

AS
BEGIN

	RETURN CONVERT(VARCHAR, @DateTimeData, 101)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN CONVERT(VARCHAR, @DateTimeData, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTimeData, 9), 13, 5) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTimeData, 9), 25, 2)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.11.13
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(VARCHAR, @TimeData, 101), '')

END
GO
--End function core.FormatTime

--Begin function core.FormatMoney
EXEC utility.DropObject 'core.FormatMoney'
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to format numeric data as money
-- =======================================================

CREATE FUNCTION core.FormatMoney
(
@NumericData NUMERIC(18,2)
)

RETURNS VARCHAR(25)

AS
BEGIN

	RETURN '$' + CONVERT(VARCHAR, CAST(@NumericData AS MONEY), 1)

END
GO
--End function core.FormatMoney

--Begin function core.GetFiscalYearFromDateTime
EXEC utility.DropObject 'core.GetFiscalYearFromDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2014.06.24
-- Description:	A function to return a fiscal year from a from a DATE or DATETIME
-- ==============================================================================

CREATE FUNCTION core.GetFiscalYearFromDateTime
(
@DateTime DATETIME
)

RETURNS INT

AS
BEGIN

	DECLARE @nFiscalYear INT = YEAR(@DateTime)

	IF MONTH(@DateTime) > 9
		SET @nFiscalYear = YEAR(@DateTime) + 1
	--ENDIF

	RETURN @nFiscalYear

END
GO
--End function core.GetFiscalYearFromDateTime

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to return a table from a delimted list of values
-- ========================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cString VARCHAR(MAX) = @String

	IF @String IS NULL OR LEN(RTRIM(@String)) = 0
		SET @cString = NULL
	--ENDIF

	RETURN @cString

END
GO
--End function core.NullIfEmpty

--Begin function migration.ConvertCharToInt
EXEC utility.DropObject 'migration.ConvertCharToInt'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION migration.ConvertCharToInt
(
@CharData VARCHAR(50)
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @cCharData VARCHAR(50) = LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@CharData, '$', ''), '-', ''), '@', ''), '+', ''), ',', ''), ' ', ''), 'S', ''))
	DECLARE @nReturn NUMERIC = NULL

	SET @cCharData = RTRIM(LTRIM(REPLACE(REPLACE(@cCharData, 'n/a', ''), 'megaw', '')))

	IF ISNUMERIC(@cCharData) = 1
		SET @nReturn = CAST(@cCharData AS NUMERIC(18,2))
	ELSE IF RIGHT(@cCharData, 1) = 'k' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000
	ELSE IF RIGHT(@cCharData, 2) = 'mm' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 2)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 2) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 7) = 'milliom'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 1) = 'm' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 3) = 'mil'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 3)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 3) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 4) = 'mill'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 4)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 4) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 7) = 'million'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 8) = 'millions'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 8)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 8) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 8) = 'miullion'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 8)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 8) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 9) = 'milliondo'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 2)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 9) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 1) = 'b'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000000000
	ELSE IF RIGHT(@cCharData, 7) = 'billion' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000000
	--ENDIF

	RETURN @nReturn

END
GO
--End function migration.ConvertCharToInt

--Begin function migration.GetAssigneeWorkflowStepNumberByReviewFormID
EXEC utility.DropObject 'migration.GetAssigneeWorkflowStepNumberByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.10
-- Description:	A function to the workflowstepnumber based on the assignee role for a given reviewformid
-- =====================================================================================================

CREATE FUNCTION migration.GetAssigneeWorkflowStepNumberByReviewFormID
(
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT
		@nWorkflowStepNumber = WS.DisplayOrder
	FROM workflow.WOrkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN
			(
			SELECT TOP 1 D.RoleCode
			FROM
				(
				SELECT 
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.OriginatingOrganizationID = POR.OrganizationID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
						AND R.HasOrganization = 1

				UNION

				SELECT
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				) D
			ORDER BY D.DisplayOrder DESC
			) E ON E.RoleCode = WS.RoleCode

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function migration.GetAssigneeWorkflowStepNumberByReviewFormID

--Begin function migration.TitleCaseName
EXEC utility.DropObject 'migration.TitleCaseName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format name data
-- ===========================================

CREATE FUNCTION migration.TitleCaseName
(
@String VARCHAR(500)
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cLastChar CHAR(1) = ''
	DECLARE @cReturn VARCHAR(500) = ''
	DECLARE @cString VARCHAR(500) = LTRIM(RTRIM(@String))
	DECLARE @cThisChar CHAR(1) = ''
	DECLARE @nI INT = 1
	DECLARE @nIStop INT = LEN(@cString)

	IF @nIStop = 0
		SET @cReturn = ''
	ELSE
		BEGIN

		WHILE @nI <= @nIStop
			BEGIN

			SET @cThisChar = SUBSTRING(@cString, @nI, 1)

			IF @cLastChar IN ('', ' ', '-', '(', '''')
				SET @cReturn += UPPER(@cThisChar)
			ELSE
				SET @cReturn += LOWER(@cThisChar)
			--ENDIF

			SET @cLastChar = @cThisChar
			SET @nI += 1

			END
		--END WHILE

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function migration.TitleCaseName

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2014.04.22
-- Description:	A function to return the name of a person in a specified format from a PersonID
-- ============================================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @FirstName VARCHAR(25)
	DECLARE @LastName VARCHAR(25)
	DECLARE @RetVal VARCHAR(250)

	SET @RetVal = ''

	IF @PersonID IS NOT NULL AND @PersonID > 0
		BEGIN
		
		SELECT
			@FirstName = ISNULL(P.FirstName, ''),
			@LastName = ISNULL(P.LastName, '')
		FROM person.Person P
		WHERE P.PersonID = @PersonID

		IF @Format = 'FirstLast'
			SET @RetVal = @FirstName + ' ' + @LastName
		--ENDIF
			
		IF @Format = 'LastFirst'
			BEGIN
			
			IF LEN(RTRIM(@LastName)) > 0
				SET @RetVal = @LastName + ', '
			--ENDIF
				
			SET @RetVal = @RetVal + @FirstName + ' '

			END
		--ENDIF
		END
	--ENDIF

	RETURN RTRIM(LTRIM(@RetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.GetOriginatingWorkflowStepNumberByPersonID
EXEC utility.DropObject 'person.GetOriginatingWorkflowStepNumberByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.GetOriginatingWorkflowStepNumberByPersonID
(
@PersonID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	
	SELECT TOP 1 @nWorkflowStepNumber = R.DisplayOrder
	FROM person.PersonOrganizationRole POR
		JOIN person.Person P ON P.PersonID = POR.PersonID
		JOIN dropdown.Role R ON R.RoleID = POR.RoleID
			AND POR.PersonID = @PersonID
			AND (R.HasOrganization = 0 OR POR.OrganizationID = P.OrganizationID)
	ORDER BY R.DisplayOrder DESC

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function person.GetOriginatingWorkflowStepNumberByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function reviewform.CreateControlCode
EXEC utility.DropObject 'reviewform.CreateControlCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to create a control code for a review form
-- ==================================================================

CREATE FUNCTION reviewform.CreateControlCode
(
@PersonID INT
)

RETURNS VARCHAR(15)

AS
BEGIN

	DECLARE @cControlCode VARCHAR(15)

	SELECT 
		@cControlCode = 
			CAST(core.GetFiscalYearFromDateTime(getDate()) AS CHAR(4)) + '-' 
				+ O.OrganizationCode + '-'
				+ RIGHT('0000' + CAST((SELECT (COUNT(RF.OriginatingOrganizationID) + 1) FROM reviewform.ReviewForm RF WHERE RF.OriginatingOrganizationID = O.OrganizationID AND core.GetFiscalYearFromDateTime(RF.CreateDateTime) = core.GetFiscalYearFromDateTime(getDate())) AS VARCHAR(10)), 4)
	FROM dropdown.Organization O
		JOIN person.Person P ON P.OrganizationID = O.OrganizationID
			AND P.PersonID = @PersonID

	RETURN @cControlCode

END
GO
--End function reviewform.CreateControlCode

--Begin function reviewform.GetAssigneeRolesListByReviewFormID
EXEC utility.DropObject 'reviewform.GetAssigneeRolesListByReviewFormID'
GO
--End function reviewform.GetAssigneeRolesListByReviewFormID

--Begin function reviewform.GetAuthorWorkflowStepNumberByReviewFormID
EXEC utility.DropObject 'reviewform.GetAuthorWorkflowStepNumberByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.10
-- Description:	A function to the workflowstepnumber of the "BackToAuthor" status for a given reviewformid
-- =======================================================================================================

CREATE FUNCTION reviewform.GetAuthorWorkflowStepNumberByReviewFormID
(
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT
		@nWorkflowStepNumber = WS.DisplayOrder
	FROM workflow.WOrkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN
			(
			SELECT TOP 1 D.RoleCode
			FROM
				(
				SELECT 
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.OriginatingOrganizationID = POR.OrganizationID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
						AND R.HasOrganization = 1

				UNION

				SELECT
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				) D
			ORDER BY D.DisplayOrder DESC
			) E ON E.RoleCode = WS.RoleCode

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function reviewform.GetAuthorWorkflowStepNumberByReviewFormID

--Begin function reviewform.GetTotalEstimatedValue
EXEC utility.DropObject 'reviewform.GetTotalEstimatedValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A function to return the total estimated value of a review form
-- ============================================================================

CREATE FUNCTION reviewform.GetTotalEstimatedValue
(
@ReviewFormID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN
	
	DECLARE @nReturn NUMERIC(18,2)

	SELECT @nReturn = ISNULL(RF.BasePrice, 0)
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	SELECT @nReturn += ISNULL(SUM(RFOY.OptionYearValue), 0)
	FROM reviewform.ReviewFormOptionYear RFOY
	WHERE RFOY.ReviewFormID = @ReviewFormID

	RETURN @nReturn

END
GO
--End function reviewform.GetTotalEstimatedValue

--Begin function utility.CreateParameterClassDefinition
EXEC utility.DropObject 'utility.CreateParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION utility.CreateParameterClassDefinition
(
@ParameterName VARCHAR(50),
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cReturn VARCHAR(MAX) = 'public '

	IF @DataType <> 'varchar'
		SET @cReturn += 'Nullable'
	--ENDIF
	
	IF @DataType = 'bit'
		SET @cReturn += '<bool>'
	ELSE IF @DataType IN ('date','datetime')
		SET @cReturn += '<System.DateTime>'
	ELSE IF @DataType = 'varchar'
		SET @cReturn += 'string'
	ELSE
		SET @cReturn += '<' + LOWER(@DataType) + '>'
	--ENDIF
	
	SET @cReturn += ' ' + @ParameterName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@cReturn))

END
GO
--End function utility.CreateParameterClassDefinition

--Begin function utility.CreateTableColumnClassDefinition
EXEC utility.DropObject 'utility.CreateTableColumnClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION utility.CreateTableColumnClassDefinition
(
@ColumnName VARCHAR(50),
@IsNullable BIT,
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cReturn VARCHAR(MAX) = 'public '

	IF @DataType = 'bit'
		SET @cReturn += 'bool'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 0
		SET @cReturn += 'System.DateTime'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 1
		SET @cReturn += 'Nullable<System.DateTime>'
	ELSE IF @DataType = 'integer'
		SET @cReturn += 'int'
	ELSE IF @DataType = 'varchar'
		SET @cReturn += 'string'
	ELSE
		SET @cReturn += LOWER(@DataType)
	--ENDIF
	
	SET @cReturn += ' ' + @ColumnName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@cReturn))

END
GO
--End function utility.CreateTableColumnClassDefinition

--Begin function utility.GetStoredProcedureObjectArgumentList
EXEC utility.DropObject 'utility.GetStoredProcedureObjectArgumentList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper function for class creation
-- =================================================
CREATE FUNCTION utility.GetStoredProcedureObjectArgumentList
(
@StoredProcedureName VARCHAR(250),
@StoredProcedureObjectName VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @ParameterName VARCHAR(50)
	DECLARE @Return VARCHAR(MAX) = ''

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SPP.ParameterName
		FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP
	
	OPEN oCursor
	FETCH oCursor INTO @ParameterName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @Return <> ''
			SET @Return += ', '
		--ENDIF
		
		SET @Return += @ParameterName + ' = ' + @StoredProcedureObjectName + '.' + @ParameterName
		
		FETCH oCursor INTO @ParameterName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor
	
	RETURN @Return

END
GO
--End function utility.GetStoredProcedureObjectArgumentList

--Begin function utility.GetStoredProcedureParameters
EXEC utility.DropObject 'utility.GetStoredProcedureParameters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================
-- Author:			Todd Pires
-- Create date: 2014.05.01
-- Description:	A helper function
-- ==============================
CREATE FUNCTION utility.GetStoredProcedureParameters
(
@StoredProcedureID INT,
@StoredProcedureName VARCHAR(250)
)

RETURNS @tReturn table 
	(
	ParameterName VARCHAR(50),
	DataType VARCHAR(50)
	) 

AS
BEGIN

	IF @StoredProcedureID = 0
		BEGIN

		IF CHARINDEX('.', @StoredProcedureName) = 0
			SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
		--ENDIF
		
		SELECT @StoredProcedureID = O.Object_ID
		FROM sys.Objects O
			JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
				AND S.Name + '.' + O.Name = @StoredProcedureName
				
		END
	--ENDIF

	INSERT INTO @tReturn
		(ParameterName,DataType)
	SELECT 
		REPLACE(P.Parameter_Name, '@', ''),
		P.Data_Type
	FROM INFORMATION_SCHEMA.PARAMETERS P
		JOIN sys.Objects O ON O.Name = P.Specific_Name
			AND O.Object_ID = @StoredProcedureID
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
			AND S.Name = P.Specific_Schema
	ORDER BY P.Parameter_Name
					
	RETURN

END
GO
--End function utility.GetStoredProcedureParameters

--Begin function workflow.GetPersonReviewformAccessLevel
EXEC utility.DropObject 'workflow.GetPersonReviewformAccessLevel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the current access level of a person on a review form
-- ====================================================================================
CREATE FUNCTION workflow.GetPersonReviewformAccessLevel
(
@PersonID INT,
@ReviewFormID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cAccessLevel VARCHAR(50) = 'None'
	DECLARE @cStatusCode VARCHAR(50)
	DECLARE @nAssignedPersonID INT
	DECLARE @nCreatePersonID INT

	SELECT
		@cStatusCode = S.StatusCode,
		@nAssignedPersonID = RF.AssignedPersonID,
		@nCreatePersonID = RF.CreatePersonID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND RF.ReviewFormID = @ReviewFormID

	IF @nAssignedPersonID = @PersonID
		BEGIN

		IF @cStatusCode IN ('BackToAuthor', 'InProgress') AND @nAssignedPersonID = @nCreatePersonID
			SET @cAccessLevel = 'Full'
		ELSE IF @cStatusCode IN ('InProgress', 'UnderReview')
			SET @cAccessLevel = 'DocumentsAndWorkflow'
		--ENDIF

		END
	--ENDIF

	RETURN @cAccessLevel

END
GO
--End function workflow.GetPersonReviewformAccessLevel

--Begin function workflow.GetStatusIDByEventCode
EXEC utility.DropObject 'workflow.GetStatusIDByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get a status for a review form based on an event code
-- ================================================================================
CREATE FUNCTION workflow.GetStatusIDByEventCode
(
@EventCode VARCHAR(50),
@WorkflowStepNumber INT,
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nStatusID INT

	SELECT
		@nStatusID = 
			CASE
				WHEN @EventCode = 'IncrementWorkflow'
				THEN 
					CASE
						WHEN workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) = @WorkflowStepNumber
						THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Complete'), 0)
						ELSE ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
					END
				WHEN @EventCode = 'DecrementWorkflow'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor'), 0)
				WHEN @EventCode = 'Update'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0)
				WHEN @EventCode = 'Withdraw'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Withdrawn'), 0)
				ELSE RF.StatusID
			END
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	RETURN ISNULL(@nStatusID, 0)

END
GO
--End function workflow.GetStatusIDByEventCode

--Begin function workflow.GetWorkflowStepCountByWorkflowID
EXEC utility.DropObject 'workflow.GetWorkflowStepCountByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the number of active steps in a workflow
-- =======================================================================
CREATE FUNCTION workflow.GetWorkflowStepCountByWorkflowID
(
@WorkflowID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT = 0

	SELECT @nWorkflowStepCount = COUNT(WS.WorkflowStepID)
	FROM workflow.WorkflowStep WS
	WHERE WS.WorkflowID = @WorkflowID
		AND WS.IsActive = 1
					
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCountByWorkflowID

--Begin function workflow.GetWorkflowStepNameByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowStepNameByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the current workflow step for a review form
-- ==========================================================================
CREATE FUNCTION workflow.GetWorkflowStepNameByReviewFormID
(
@ReviewFormID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStepName VARCHAR(50)

	SELECT @cWorkflowStepName = R.RoleName
	FROM workflow.WorkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
		JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode
			AND RF.ReviewFormID = @ReviewFormID
			AND WS.DisplayOrder = RF.WorkflowStepNumber

	RETURN @cWorkflowStepName

END
GO
--End function workflow.GetWorkflowStepNameByReviewFormID

--Begin function workflow.GetWorkflowStepPeopleByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowStepPeopleByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the people for the next workflow step
-- ====================================================================
CREATE FUNCTION workflow.GetWorkflowStepPeopleByReviewFormID
(
@ReviewFormID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cWorkflowStepPeople VARCHAR(MAX) = ''

	;
	WITH WSD AS 
		(
		SELECT 
			WS.RoleCode, 
			R.HasOrganization,
			RF.OriginatingOrganizationID
		FROM workflow.WorkflowStep WS
			JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode
			JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
				AND RF.WorkflowStepNumber + 1 = WS.DisplayOrder
				AND RF.ReviewFormID = @ReviewFormID
		)

	SELECT @cWorkflowStepPeople = 
		COALESCE(@cWorkflowStepPeople 
			+ ',{"PersonID":' 
			+ CAST(P.PersonID AS VARCHAR(10)) 
			+ ', "PersonNameFormatted":"' 
			+ person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') + '"}', ',')
	FROM person.Person P
	WHERE P.IsActive = 1
		AND EXISTS
			(
			SELECT 1
			FROM person.PersonOrganizationRole POR 
				JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				JOIN WSD ON WSD.RoleCode = R.RoleCode
					AND (WSD.HasOrganization = 0 OR POR.OrganizationID = WSD.OriginatingOrganizationID)
					AND P.PersonID = POR.PersonID
			)
	ORDER BY P.LastName, P.FirstName, P.PersonID

	IF @cWorkflowStepPeople = ''
		SET @cWorkflowStepPeople = '[]'
	ELSE
		SET @cWorkflowStepPeople = '[' + STUFF(@cWorkflowStepPeople, 1, 1, '') + ']'
	--ENDIF

	RETURN @cWorkflowStepPeople

END
GO
--End function workflow.GetWorkflowStepPeopleByReviewFormID
