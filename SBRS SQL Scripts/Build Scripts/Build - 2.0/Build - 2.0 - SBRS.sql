-- File Name:	Build - 2.0 - SBRS.sql
-- Build Key:	Build - 2.0 - 2017.08.04 09.13.02

USE SBRS_DHHS
GO

-- ==============================================================================================================================
-- Functions:
--		core.FormatDate
--		core.FormatDateTime
--		core.FormatMoney
--		core.FormatTime
--		core.GetFiscalYearFromDateTime
--		core.ListToTable
--		core.NullIfEmpty
--		migration.ConvertCharToInt
--		migration.GetAssigneeWorkflowStepNumberByReviewFormID
--		migration.TitleCaseName
--		person.FormatPersonNameByPersonID
--		person.GetOriginatingWorkflowStepNumberByPersonID
--		person.HashPassword
--		reviewform.CreateControlCode
--		reviewform.GetAuthorWorkflowStepNumberByReviewFormID
--		reviewform.GetTotalEstimatedValue
--		utility.CreateParameterClassDefinition
--		utility.CreateTableColumnClassDefinition
--		utility.GetStoredProcedureObjectArgumentList
--		utility.GetStoredProcedureParameters
--		workflow.GetPersonReviewformAccessLevel
--		workflow.GetStatusIDByEventCode
--		workflow.GetWorkflowStepCountByWorkflowID
--		workflow.GetWorkflowStepNameByReviewFormID
--		workflow.GetWorkflowStepPeopleByReviewFormID
--
-- Procedures:
--		dropdown.GetBundleTypeData
--		dropdown.GetCOACSData
--		dropdown.GetContractTerminationData
--		dropdown.GetDocumentTypeData
--		dropdown.GetFARJustificationData
--		dropdown.GetFundingOrganizationData
--		dropdown.GetGSAJustificationData
--		dropdown.GetNAICSData
--		dropdown.GetOrganizationData
--		dropdown.GetPriorBundlerData
--		dropdown.GetProcurementHistoryData
--		dropdown.GetProcurementMethodData
--		dropdown.GetResearchTypeData
--		dropdown.GetRoleData
--		dropdown.GetSizeTypeData
--		dropdown.GetStatusData
--		dropdown.GetSubContractingPlanData
--		dropdown.GetSynopsisData
--		dropdown.GetSynopsisExceptionReasonData
--		dropdown.GetUnrestrictedProcurementReasonData
--		emailtemplate.GetNewUserRegistrationEmailData
--		emailtemplate.GetReviewFormAssignmentEmailData
--		emailtemplate.GetReviewFormCloseOutEmailData
--		emailtemplate.GetUserActivationEmailData
--		eventlog.LogPersonAction
--		eventlog.LogReviewFormAction
--		person.ActivateAccountByPersonID
--		person.GetPersonByPersonID
--		person.GetPersonDataBySearchCriteria
--		person.GetPersonOrganizationRolesByPersonID
--		person.SavePerson
--		person.SavePersonOrganizationRole
--		person.ValidateEmailAddress
--		person.ValidateLogin
--		reviewform.GetReviewFormByReviewFormID
--		reviewform.GetReviewFormContractByReviewFormID
--		reviewform.GetReviewFormDataByPersonID
--		reviewform.GetReviewFormDataBySearchCriteria
--		reviewform.GetReviewFormDocumentByDocumentID
--		reviewform.GetReviewFormDocumentByReviewFormID
--		reviewform.GetReviewFormOptionYearByReviewFormID
--		reviewform.GetReviewFormPointOfContactByReviewFormID
--		reviewform.GetReviewFormProcurementHistoryByReviewFormID
--		reviewform.GetReviewFormProcurementMethodByReviewFormID
--		reviewform.GetReviewFormResearchTypeByReviewFormID
--		reviewform.SaveReviewForm
--		reviewform.SaveReviewFormContract
--		reviewform.SaveReviewFormDocument
--		reviewform.SaveReviewFormOptionYear
--		reviewform.SaveReviewFormPointOfContact
--		reviewform.SaveReviewFormProcurementHistory
--		reviewform.SaveReviewFormProcurementMethod
--		reviewform.SaveReviewFormResearchType
--		reviewform.SaveReviewFormWorkflowData
--		utility.AddColumn
--		utility.AddSchema
--		utility.CreateGetDataMethodDefinition
--		utility.CreateSaveDataMethodDefinition
--		utility.CreateStoredProcedureClassDefinition
--		utility.CreateStoredProcedureObjectArgumentList
--		utility.CreateStoredProcedureParameterClassDefinition
--		utility.CreateTableClassDefinition
--		utility.CreateTableInsertUpdateDefinition
--		utility.CreateTableSaveProcedureDefinition
--		utility.DropColumn
--		utility.DropIndex
--		utility.DropObject
--		utility.SetDefaultConstraint
--		utility.SetIndexClustered
--		utility.SetIndexNonClustered
--		utility.SetPrimaryKeyClustered
--		utility.SetPrimaryKeyNonClustered
--		workflow.GetWorkflowByReviewFormID
--		workflow.GetWorkflowHistoryByReviewFormID
--		workflow.SetWorkflowByReviewFormID
--
-- Schemas:
--		core
--		dropdown
--		emailtemplate
--		eventlog
--		migration
--		person
--		reviewform
--		workflow
--
-- Tables:
--		dropdown.BundleType
--		dropdown.COACS
--		dropdown.ContractTermination
--		dropdown.DocumentType
--		dropdown.FARJustification
--		dropdown.GSAJustification
--		dropdown.NAICS
--		dropdown.Organization
--		dropdown.PriorBundler
--		dropdown.ProcurementHistory
--		dropdown.ProcurementMethod
--		dropdown.ResearchType
--		dropdown.Role
--		dropdown.SizeType
--		dropdown.Status
--		dropdown.SubContractingPlan
--		dropdown.Synopsis
--		dropdown.SynopsisExceptionReason
--		dropdown.UnrestrictedProcurementReason
--		emailtemplate.EmailTemplate
--		eventlog.EventLog
--		migration.UserMigration
--		person.Person
--		person.PersonOrganizationRole
--		reviewform.ReviewForm
--		reviewform.ReviewFormContract
--		reviewform.ReviewFormDocument
--		reviewform.ReviewFormOptionYear
--		reviewform.ReviewFormPointOfContact
--		reviewform.ReviewFormProcurementHistory
--		reviewform.ReviewFormProcurementMethod
--		reviewform.ReviewFormResearchType
--		workflow.Workflow
--		workflow.WorkflowStep
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE SBRS_DHHS
GO

--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN [' + @ColumnName + ']'
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex

@TableName VARCHAR(250),
@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX),
@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered

EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'dropdown'
EXEC utility.AddSchema 'emailtemplate'
EXEC utility.AddSchema 'eventlog'
EXEC utility.AddSchema 'migration'
EXEC utility.AddSchema 'person'
EXEC utility.AddSchema 'reviewform'
EXEC utility.AddSchema 'workflow'
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables - Dropdown.sql
USE SBRS_DHHS
GO

--Begin table dropdown.BundleType
DECLARE @TableName VARCHAR(250) = 'dropdown.BundleType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.BundleType
	(
	BundleTypeID INT IDENTITY(0,1) NOT NULL,
	BundleTypeCode VARCHAR(50),
	BundleTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'BundleTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_BundleType', 'DisplayOrder, BundleTypeName'
GO
--End table dropdown.BundleType

--Begin table dropdown.COACS
DECLARE @TableName VARCHAR(250) = 'dropdown.COACS'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.COACS
	(
	COACSID INT IDENTITY(0,1) NOT NULL,
	COACSCode VARCHAR(50),
	COACSName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'COACSID'
EXEC Utility.SetIndexClustered @TableName, 'IX_COACS', 'DisplayOrder,COACSName,COACSID'
GO
--End table dropdown.COACS

--Begin table dropdown.ContractTermination
DECLARE @TableName VARCHAR(250) = 'dropdown.ContractTermination'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ContractTermination
	(
	ContractTerminationID INT IDENTITY(0,1) NOT NULL,
	ContractTerminationCode VARCHAR(50),
	ContractTerminationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractTerminationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ContractTermination', 'DisplayOrder,ContractTerminationName,ContractTerminationID'
GO
--End table dropdown.ContractTermination

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT IDENTITY(0,1) NOT NULL,
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'DocumentTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_DocumentType', 'DisplayOrder,DocumentTypeName,DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.FARJustification
DECLARE @TableName VARCHAR(250) = 'dropdown.FARJustification'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.FARJustification
	(
	FARJustificationID INT IDENTITY(0,1) NOT NULL,
	FARJustificationCode VARCHAR(50),
	FARJustificationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'FARJustificationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_FARJustification', 'DisplayOrder,FARJustificationName,FARJustificationID'
GO
--End table dropdown.FARJustification

--Begin table dropdown.GSAJustification
DECLARE @TableName VARCHAR(250) = 'dropdown.GSAJustification'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.GSAJustification
	(
	GSAJustificationID INT IDENTITY(0,1) NOT NULL,
	GSAJustificationCode VARCHAR(50),
	GSAJustificationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'GSAJustificationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_GSAJustification', 'DisplayOrder,GSAJustificationName,GSAJustificationID'
GO
--End table dropdown.GSAJustification

--Begin table dropdown.NAICS
DECLARE @TableName VARCHAR(250) = 'dropdown.NAICS'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.NAICS
	(
	NAICSID INT IDENTITY(0,1) NOT NULL,
	NAICSCode VARCHAR(50),
	NAICSName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'NAICSID'
EXEC Utility.SetIndexClustered @TableName, 'IX_NAICS', 'DisplayOrder,NAICSCode,NAICSName,NAICSID'
GO
--End table dropdown.NAICS

--Begin table dropdown.Organization
DECLARE @TableName VARCHAR(250) = 'dropdown.Organization'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Organization
	(
	OrganizationID INT IDENTITY(0,1) NOT NULL,
	OrganizationCode VARCHAR(50),
	OrganizationDisplayCode VARCHAR(50),
	OrganizationFilePath VARCHAR(50),
	OrganizationName VARCHAR(250),
	DisplayOrder INT,
	IsForFundingOffice BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsForFundingOffice', 'BIT', '1'

EXEC Utility.SetPrimaryKeyClustered @TableName, 'OrganizationID'
GO
--End table dropdown.Organization

--Begin table dropdown.PriorBundler
DECLARE @TableName VARCHAR(250) = 'dropdown.PriorBundler'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.PriorBundler
	(
	PriorBundlerID INT IDENTITY(0,1) NOT NULL,
	PriorBundlerCode VARCHAR(50),
	PriorBundlerName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PriorBundlerID'
EXEC Utility.SetIndexClustered @TableName, 'IX_PriorBundler', 'DisplayOrder, PriorBundlerName'
GO
--End table dropdown.PriorBundler

--Begin table dropdown.ProcurementHistory
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementHistory'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ProcurementHistory
	(
	ProcurementHistoryID INT IDENTITY(0,1) NOT NULL,
	ProcurementHistoryCode VARCHAR(50),
	ProcurementHistoryName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ProcurementHistoryID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ProcurementHistory', 'DisplayOrder, ProcurementHistoryName'
GO
--End table dropdown.ProcurementHistory

--Begin table dropdown.ProcurementType
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementMethod'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ProcurementMethod
	(
	ProcurementMethodID INT IDENTITY(0,1) NOT NULL,
	ProcurementMethodCode VARCHAR(50),
	ProcurementMethodName VARCHAR(250),
	DisplayOrder INT,
	IsSetAside BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsSetAside', 'BIT', '0'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ProcurementMethodID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ProcurementMethod', 'DisplayOrder, ProcurementMethodName'
GO
--End table dropdown.ProcurementMethod

--Begin table dropdown.ResearchType
DECLARE @TableName VARCHAR(250) = 'dropdown.ResearchType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ResearchType
	(
	ResearchTypeID INT IDENTITY(0,1) NOT NULL,
	ResearchTypeCode VARCHAR(50),
	ResearchTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ResearchTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ResearchType', 'DisplayOrder, ResearchTypeName'
GO
--End table dropdown.ResearchType

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleCode VARCHAR(50),
	RoleName VARCHAR(250),
	DisplayOrder INT,
	IsForDisplay BIT,
	HasOrganization BIT,
	NotificationRoleCode VARCHAR(50),
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'HasOrganization', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsForDisplay', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'RoleID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Role', 'DisplayOrder, RoleName'
GO
--End table dropdown.Role

--Begin table dropdown.SizeType
DECLARE @TableName VARCHAR(250) = 'dropdown.SizeType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SizeType
	(
	SizeTypeID INT IDENTITY(0,1) NOT NULL,
	SizeTypeCode VARCHAR(50),
	SizeTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SizeTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SizeType', 'DisplayOrder, SizeTypeName'
GO
--End table dropdown.SizeType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'StatusID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Status', 'DisplayOrder, StatusName'
GO
--End table dropdown.Status

--Begin table dropdown.SubContractingPlan
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractingPlan'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SubContractingPlan
	(
	SubContractingPlanID INT IDENTITY(0,1) NOT NULL,
	SubContractingPlanCode VARCHAR(50),
	SubContractingPlanName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SubContractingPlanID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SubContractingPlan', 'DisplayOrder, SubContractingPlanName'
GO
--End table dropdown.SubContractingPlan

--Begin table dropdown.Synopsis
DECLARE @TableName VARCHAR(250) = 'dropdown.Synopsis'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Synopsis
	(
	SynopsisID INT IDENTITY(0,1) NOT NULL,
	SynopsisCode VARCHAR(50),
	SynopsisName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SynopsisID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Synopsis', 'DisplayOrder, SynopsisName'
GO
--End table dropdown.Synopsis

--Begin table dropdown.SynopsisExceptionReason
DECLARE @TableName VARCHAR(250) = 'dropdown.SynopsisExceptionReason'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SynopsisExceptionReason
	(
	SynopsisExceptionReasonID INT IDENTITY(0,1) NOT NULL,
	SynopsisExceptionReasonCode VARCHAR(50),
	SynopsisExceptionReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SynopsisExceptionReasonID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SynopsisExceptionReason', 'DisplayOrder, SynopsisExceptionReasonName'
GO
--End table dropdown.SynopsisExceptionReason

--Begin table dropdown.UnrestrictedProcurementReason
DECLARE @TableName VARCHAR(250) = 'dropdown.UnrestrictedProcurementReason'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.UnrestrictedProcurementReason
	(
	UnrestrictedProcurementReasonID INT IDENTITY(0,1) NOT NULL,
	UnrestrictedProcurementReasonCode VARCHAR(50),
	UnrestrictedProcurementReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'UnrestrictedProcurementReasonID'
EXEC Utility.SetIndexClustered @TableName, 'IX_UnrestrictedProcurementReason', 'DisplayOrder, UnrestrictedProcurementReasonName'
GO
--End table dropdown.UnrestrictedProcurementReason
--End file Build File - 01 - Tables - Dropdown.sql

--Begin file Build File - 01 - Tables - Other.sql
USE SBRS_DHHS
GO

--Begin table emailtemplate.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'emailtemplate.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE emailtemplate.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EmailTemplateCode VARCHAR(50),
	EmailTemplateName VARCHAR(250),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EmailTemplateCode'
GO
--End table emailtemplate.EmailTemplate

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Action VARCHAR(100),
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table migration.UserMigration
DECLARE @TableName VARCHAR(250) = 'migration.UserMigration'

EXEC Utility.DropObject 'migration.Person'
EXEC Utility.DropObject @TableName

CREATE TABLE migration.UserMigration
	(
	UserMigrationID INT IDENTITY(1,1) NOT NULL,
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	Password VARCHAR(64),
	PersonID INT,
	UserID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UserID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'UserMigrationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_UserMigration', 'UserID,PersonID'
GO
--End table migration.Person

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC Utility.DropObject @TableName

CREATE TABLE person.Person
	(
	PersonID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	PhoneNumber VARCHAR(50),
	JobTitle VARCHAR(250),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', '0'

EXEC Utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonOrganizationRole
DECLARE @TableName VARCHAR(250) = 'person.PersonOrganizationRole'

EXEC Utility.DropObject @TableName

CREATE TABLE person.PersonOrganizationRole
	(
	PersonOrganizationRoleID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	OrganizationID INT,
	RoleID INT,
	CreateDateTime DATETIME
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PersonOrganizationRoleID'
EXEC Utility.SetIndexClustered @TableName, 'IX_PersonOrganizationRole', 'PersonID, OrganizationID, RoleID'
GO
--End table person.PersonOrganizationRole

--Begin table reviewform.ReviewForm
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewForm'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewForm
	(
	ReviewFormID INT NOT NULL IDENTITY(1,1),
	ControlCode VARCHAR(50),
	RequisitionNumber1 VARCHAR(100),
	RequisitionNumber2 VARCHAR(100),
	COACSID INT,
	FundingOrganizationID INT,
	FundingOrganizationName VARCHAR(250),
	BasePrice	NUMERIC(18,2),
	PeriodOfPerformanceStartDate DATE,
	PeriodOfPerformanceEndDate DATE,
	NAICSID INT,
	SizeTypeID INT,
	Size BIGINT,
	SynopsisID INT,
	SynopsisExceptionReasonID	INT,	
	Description VARCHAR(MAX),
	ProcurementHistoryID INT,
	ProcurementHistoryContractAwardAmount	NUMERIC(18,2),
	ProcurementHistoryContractAwardDate	DATE,
	ProcurementHistoryContractEndDate	DATE,
	ProcurementHistoryContractNumber VARCHAR(50),
	ProcurementHistoryContractTerminationID INT,
	ProcurementHistoryContractTerminationReason	VARCHAR(MAX),
	OtherResearch VARCHAR(500),
	BundleTypeID INT,
	PriorBundlerID INT,
	SubContractingPlanID INT,
	AssignedPersonID INT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	IsActive BIT,
	OriginatingOrganizationID INT,
	StatusID INT,
	WorkflowID INT,
	WorkflowStepNumber INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'AssignedPersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'BasePrice', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'BundleTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'COACSID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'PriorBundlerID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryContractAwardAmount', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryContractTerminationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'Size', 'BIGINT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SizeTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SubContractingPlanID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SynopsisExceptionReasonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SynopsisID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyClustered @TableName, 'ReviewFormID'
GO
--End table reviewform.ReviewForm

--Begin table reviewform.ReviewFormContract
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormContract'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormContract
	(
	ReviewFormContractID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ContractNumber VARCHAR(100),
	)
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormContractID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormContract', 'ReviewFormID'
GO
--End table reviewform.ReviewFormContract

--Begin table reviewform.ReviewFormDocument
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormDocument'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormDocument
	(
	ReviewFormDocumentID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	DocumentPath VARCHAR(50),
	DocumentName VARCHAR(250),
	DocumentTypeID INT,
	FileGUID VARCHAR(50),
	FileExtension VARCHAR(10),
	IsFileGUIDFileName BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'FileGUID', 'VARCHAR(50)', 'newID()'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsFileGUIDFileName', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormDocumentID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormDocument', 'ReviewFormID'
GO
--End table reviewform.ReviewFormDocument

--Begin table reviewform.ReviewFormOptionYear
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormOptionYear'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormOptionYear
	(
	ReviewFormOptionYearID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	OptionYear INT,
	OptionYearValue	NUMERIC(18,2)
	)

EXEC Utility.SetDefaultConstraint @TableName, 'OptionYear', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'OptionYearValue', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormOptionYearID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormOptionYear', 'ReviewFormID, OptionYear'
GO
--End table reviewform.ReviewFormOptionYear

--Begin table reviewform.ReviewFormPointOfContact
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormPointOfContact'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormPointOfContact
	(
	ReviewFormPointOfContactID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	PointOfContactName VARCHAR(50),
	PointOfContactPhone VARCHAR(50),
	PointOfContactEmailAddress VARCHAR(320)
	)

EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormPointOfContactID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormPointOfContact', 'ReviewFormID, PointOfContactName'
GO
--End table reviewform.ReviewFormPointOfContact

--Begin table reviewform.ReviewFormProcurementHistory
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormProcurementHistory'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormProcurementHistory
	(
	ReviewFormProcurementHistoryID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ProcurementMethodID INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	Percentage INT,
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'FARJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'GSAJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'Percentage', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementMethodID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UnrestrictedProcurementReasonID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormProcurementHistoryID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormProcurementHistory', 'ReviewFormID'
GO
--End table reviewform.ReviewFormProcurementHistory

--Begin table reviewform.ReviewFormProcurementMethod
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormProcurementMethod'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormProcurementMethod
	(
	ReviewFormProcurementMethodID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ProcurementMethodID INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	Percentage INT,
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'FARJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'GSAJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'Percentage', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementMethodID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UnrestrictedProcurementReasonID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormProcurementMethodID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormProcurementMethod', 'ReviewFormID'
GO
--End table reviewform.ReviewFormProcurementMethod

--Begin table reviewform.ReviewFormResearchType
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormResearchType'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormResearchType
	(
	ReviewFormResearchTypeID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ResearchTypeID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'ResearchTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormResearchTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormResearchType', 'ReviewFormID, ResearchTypeID'
GO
--End table reviewform.ReviewFormResearchType

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC Utility.DropObject @TableName

CREATE TABLE workflow.Workflow
	(
	WorkflowID INT NOT NULL IDENTITY(1,1),
	WorkflowCode VARCHAR(50),
	OrganizationCode VARCHAR(50),
	LevelCode VARCHAR(50)
	)
	
EXEC Utility.SetPrimaryKeyClustered @TableName, 'WorkflowID'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC Utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStep
	(
	WorkflowStepID INT NOT NULL IDENTITY(1,1),
	WorkflowID INT,
	RoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT,
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'WorkflowStepID'
EXEC Utility.SetIndexClustered @TableName, 'IX_WorkflowStep', 'WorkflowID,DisplayOrder'
GO
--End table workflow.WorkflowStep

--End file Build File - 01 - Tables - Other.sql

--Begin file Build File - 02 - Functions.sql
USE SBRS_DHHS
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(10)

AS
BEGIN

	RETURN CONVERT(VARCHAR, @DateTimeData, 101)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN CONVERT(VARCHAR, @DateTimeData, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTimeData, 9), 13, 5) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTimeData, 9), 25, 2)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.11.13
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(VARCHAR, @TimeData, 101), '')

END
GO
--End function core.FormatTime

--Begin function core.FormatMoney
EXEC utility.DropObject 'core.FormatMoney'
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to format numeric data as money
-- =======================================================

CREATE FUNCTION core.FormatMoney
(
@NumericData NUMERIC(18,2)
)

RETURNS VARCHAR(25)

AS
BEGIN

	RETURN '$' + CONVERT(VARCHAR, CAST(@NumericData AS MONEY), 1)

END
GO
--End function core.FormatMoney

--Begin function core.GetFiscalYearFromDateTime
EXEC utility.DropObject 'core.GetFiscalYearFromDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2014.06.24
-- Description:	A function to return a fiscal year from a from a DATE or DATETIME
-- ==============================================================================

CREATE FUNCTION core.GetFiscalYearFromDateTime
(
@DateTime DATETIME
)

RETURNS INT

AS
BEGIN

	DECLARE @nFiscalYear INT = YEAR(@DateTime)

	IF MONTH(@DateTime) > 9
		SET @nFiscalYear = YEAR(@DateTime) + 1
	--ENDIF

	RETURN @nFiscalYear

END
GO
--End function core.GetFiscalYearFromDateTime

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to return a table from a delimted list of values
-- ========================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cString VARCHAR(MAX) = @String

	IF @String IS NULL OR LEN(RTRIM(@String)) = 0
		SET @cString = NULL
	--ENDIF

	RETURN @cString

END
GO
--End function core.NullIfEmpty

--Begin function migration.ConvertCharToInt
EXEC utility.DropObject 'migration.ConvertCharToInt'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION migration.ConvertCharToInt
(
@CharData VARCHAR(50)
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @cCharData VARCHAR(50) = LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@CharData, '$', ''), '-', ''), '@', ''), '+', ''), ',', ''), ' ', ''), 'S', ''))
	DECLARE @nReturn NUMERIC = NULL

	SET @cCharData = RTRIM(LTRIM(REPLACE(REPLACE(@cCharData, 'n/a', ''), 'megaw', '')))

	IF ISNUMERIC(@cCharData) = 1
		SET @nReturn = CAST(@cCharData AS NUMERIC(18,2))
	ELSE IF RIGHT(@cCharData, 1) = 'k' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000
	ELSE IF RIGHT(@cCharData, 2) = 'mm' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 2)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 2) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 7) = 'milliom'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 1) = 'm' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 3) = 'mil'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 3)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 3) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 4) = 'mill'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 4)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 4) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 7) = 'million'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 8) = 'millions'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 8)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 8) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 8) = 'miullion'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 8)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 8) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 9) = 'milliondo'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 2)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 9) AS NUMERIC(18,2)) * 1000000
	ELSE IF RIGHT(@cCharData, 1) = 'b'AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 1)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 1) AS NUMERIC(18,2)) * 1000000000
	ELSE IF RIGHT(@cCharData, 7) = 'billion' AND ISNUMERIC(LEFT(@cCharData, LEN(@cCharData) - 7)) = 1
		SET @nReturn = CAST(LEFT(@cCharData, LEN(@cCharData) - 7) AS NUMERIC(18,2)) * 1000000000
	--ENDIF

	RETURN @nReturn

END
GO
--End function migration.ConvertCharToInt

--Begin function migration.GetAssigneeWorkflowStepNumberByReviewFormID
EXEC utility.DropObject 'migration.GetAssigneeWorkflowStepNumberByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.10
-- Description:	A function to the workflowstepnumber based on the assignee role for a given reviewformid
-- =====================================================================================================

CREATE FUNCTION migration.GetAssigneeWorkflowStepNumberByReviewFormID
(
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT
		@nWorkflowStepNumber = WS.DisplayOrder
	FROM workflow.WOrkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN
			(
			SELECT TOP 1 D.RoleCode
			FROM
				(
				SELECT 
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.OriginatingOrganizationID = POR.OrganizationID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
						AND R.HasOrganization = 1

				UNION

				SELECT
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				) D
			ORDER BY D.DisplayOrder DESC
			) E ON E.RoleCode = WS.RoleCode

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function migration.GetAssigneeWorkflowStepNumberByReviewFormID

--Begin function migration.TitleCaseName
EXEC utility.DropObject 'migration.TitleCaseName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format name data
-- ===========================================

CREATE FUNCTION migration.TitleCaseName
(
@String VARCHAR(500)
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cLastChar CHAR(1) = ''
	DECLARE @cReturn VARCHAR(500) = ''
	DECLARE @cString VARCHAR(500) = LTRIM(RTRIM(@String))
	DECLARE @cThisChar CHAR(1) = ''
	DECLARE @nI INT = 1
	DECLARE @nIStop INT = LEN(@cString)

	IF @nIStop = 0
		SET @cReturn = ''
	ELSE
		BEGIN

		WHILE @nI <= @nIStop
			BEGIN

			SET @cThisChar = SUBSTRING(@cString, @nI, 1)

			IF @cLastChar IN ('', ' ', '-', '(', '''')
				SET @cReturn += UPPER(@cThisChar)
			ELSE
				SET @cReturn += LOWER(@cThisChar)
			--ENDIF

			SET @cLastChar = @cThisChar
			SET @nI += 1

			END
		--END WHILE

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function migration.TitleCaseName

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2014.04.22
-- Description:	A function to return the name of a person in a specified format from a PersonID
-- ============================================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @FirstName VARCHAR(25)
	DECLARE @LastName VARCHAR(25)
	DECLARE @RetVal VARCHAR(250)

	SET @RetVal = ''

	IF @PersonID IS NOT NULL AND @PersonID > 0
		BEGIN
		
		SELECT
			@FirstName = ISNULL(P.FirstName, ''),
			@LastName = ISNULL(P.LastName, '')
		FROM person.Person P
		WHERE P.PersonID = @PersonID

		IF @Format = 'FirstLast'
			SET @RetVal = @FirstName + ' ' + @LastName
		--ENDIF
			
		IF @Format = 'LastFirst'
			BEGIN
			
			IF LEN(RTRIM(@LastName)) > 0
				SET @RetVal = @LastName + ', '
			--ENDIF
				
			SET @RetVal = @RetVal + @FirstName + ' '

			END
		--ENDIF
		END
	--ENDIF

	RETURN RTRIM(LTRIM(@RetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.GetOriginatingWorkflowStepNumberByPersonID
EXEC utility.DropObject 'person.GetOriginatingWorkflowStepNumberByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.GetOriginatingWorkflowStepNumberByPersonID
(
@PersonID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	
	SELECT TOP 1 @nWorkflowStepNumber = R.DisplayOrder
	FROM person.PersonOrganizationRole POR
		JOIN person.Person P ON P.PersonID = POR.PersonID
		JOIN dropdown.Role R ON R.RoleID = POR.RoleID
			AND POR.PersonID = @PersonID
			AND (R.HasOrganization = 0 OR POR.OrganizationID = P.OrganizationID)
	ORDER BY R.DisplayOrder DESC

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function person.GetOriginatingWorkflowStepNumberByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function reviewform.CreateControlCode
EXEC utility.DropObject 'reviewform.CreateControlCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A function to create a control code for a review form
-- ==================================================================

CREATE FUNCTION reviewform.CreateControlCode
(
@PersonID INT
)

RETURNS VARCHAR(15)

AS
BEGIN

	DECLARE @cControlCode VARCHAR(15)

	SELECT 
		@cControlCode = 
			CAST(core.GetFiscalYearFromDateTime(getDate()) AS CHAR(4)) + '-' 
				+ O.OrganizationCode + '-'
				+ RIGHT('0000' + CAST((SELECT (COUNT(RF.OriginatingOrganizationID) + 1) FROM reviewform.ReviewForm RF WHERE RF.OriginatingOrganizationID = O.OrganizationID AND core.GetFiscalYearFromDateTime(RF.CreateDateTime) = core.GetFiscalYearFromDateTime(getDate())) AS VARCHAR(10)), 4)
	FROM dropdown.Organization O
		JOIN person.Person P ON P.OrganizationID = O.OrganizationID
			AND P.PersonID = @PersonID

	RETURN @cControlCode

END
GO
--End function reviewform.CreateControlCode

--Begin function reviewform.GetAssigneeRolesListByReviewFormID
EXEC utility.DropObject 'reviewform.GetAssigneeRolesListByReviewFormID'
GO
--End function reviewform.GetAssigneeRolesListByReviewFormID

--Begin function reviewform.GetAuthorWorkflowStepNumberByReviewFormID
EXEC utility.DropObject 'reviewform.GetAuthorWorkflowStepNumberByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.10
-- Description:	A function to the workflowstepnumber of the "BackToAuthor" status for a given reviewformid
-- =======================================================================================================

CREATE FUNCTION reviewform.GetAuthorWorkflowStepNumberByReviewFormID
(
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT
		@nWorkflowStepNumber = WS.DisplayOrder
	FROM workflow.WOrkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN
			(
			SELECT TOP 1 D.RoleCode
			FROM
				(
				SELECT 
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.OriginatingOrganizationID = POR.OrganizationID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
						AND R.HasOrganization = 1

				UNION

				SELECT
					R.DisplayOrder, 
					R.RoleCode 
				FROM person.PersonOrganizationRole POR 	
					JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = POR.PersonID
						AND RF.ReviewFormID = @ReviewFormID
					JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				) D
			ORDER BY D.DisplayOrder DESC
			) E ON E.RoleCode = WS.RoleCode

	RETURN ISNULL(@nWorkflowStepNumber, 1)

END
GO
--End function reviewform.GetAuthorWorkflowStepNumberByReviewFormID

--Begin function reviewform.GetTotalEstimatedValue
EXEC utility.DropObject 'reviewform.GetTotalEstimatedValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A function to return the total estimated value of a review form
-- ============================================================================

CREATE FUNCTION reviewform.GetTotalEstimatedValue
(
@ReviewFormID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN
	
	DECLARE @nReturn NUMERIC(18,2)

	SELECT @nReturn = ISNULL(RF.BasePrice, 0)
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	SELECT @nReturn += ISNULL(SUM(RFOY.OptionYearValue), 0)
	FROM reviewform.ReviewFormOptionYear RFOY
	WHERE RFOY.ReviewFormID = @ReviewFormID

	RETURN @nReturn

END
GO
--End function reviewform.GetTotalEstimatedValue

--Begin function utility.CreateParameterClassDefinition
EXEC utility.DropObject 'utility.CreateParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION utility.CreateParameterClassDefinition
(
@ParameterName VARCHAR(50),
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cReturn VARCHAR(MAX) = 'public '

	IF @DataType <> 'varchar'
		SET @cReturn += 'Nullable'
	--ENDIF
	
	IF @DataType = 'bit'
		SET @cReturn += '<bool>'
	ELSE IF @DataType IN ('date','datetime')
		SET @cReturn += '<System.DateTime>'
	ELSE IF @DataType = 'varchar'
		SET @cReturn += 'string'
	ELSE
		SET @cReturn += '<' + LOWER(@DataType) + '>'
	--ENDIF
	
	SET @cReturn += ' ' + @ParameterName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@cReturn))

END
GO
--End function utility.CreateParameterClassDefinition

--Begin function utility.CreateTableColumnClassDefinition
EXEC utility.DropObject 'utility.CreateTableColumnClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION utility.CreateTableColumnClassDefinition
(
@ColumnName VARCHAR(50),
@IsNullable BIT,
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cReturn VARCHAR(MAX) = 'public '

	IF @DataType = 'bit'
		SET @cReturn += 'bool'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 0
		SET @cReturn += 'System.DateTime'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 1
		SET @cReturn += 'Nullable<System.DateTime>'
	ELSE IF @DataType = 'integer'
		SET @cReturn += 'int'
	ELSE IF @DataType = 'varchar'
		SET @cReturn += 'string'
	ELSE
		SET @cReturn += LOWER(@DataType)
	--ENDIF
	
	SET @cReturn += ' ' + @ColumnName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@cReturn))

END
GO
--End function utility.CreateTableColumnClassDefinition

--Begin function utility.GetStoredProcedureObjectArgumentList
EXEC utility.DropObject 'utility.GetStoredProcedureObjectArgumentList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper function for class creation
-- =================================================
CREATE FUNCTION utility.GetStoredProcedureObjectArgumentList
(
@StoredProcedureName VARCHAR(250),
@StoredProcedureObjectName VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @ParameterName VARCHAR(50)
	DECLARE @Return VARCHAR(MAX) = ''

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SPP.ParameterName
		FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP
	
	OPEN oCursor
	FETCH oCursor INTO @ParameterName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @Return <> ''
			SET @Return += ', '
		--ENDIF
		
		SET @Return += @ParameterName + ' = ' + @StoredProcedureObjectName + '.' + @ParameterName
		
		FETCH oCursor INTO @ParameterName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor
	
	RETURN @Return

END
GO
--End function utility.GetStoredProcedureObjectArgumentList

--Begin function utility.GetStoredProcedureParameters
EXEC utility.DropObject 'utility.GetStoredProcedureParameters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================
-- Author:			Todd Pires
-- Create date: 2014.05.01
-- Description:	A helper function
-- ==============================
CREATE FUNCTION utility.GetStoredProcedureParameters
(
@StoredProcedureID INT,
@StoredProcedureName VARCHAR(250)
)

RETURNS @tReturn table 
	(
	ParameterName VARCHAR(50),
	DataType VARCHAR(50)
	) 

AS
BEGIN

	IF @StoredProcedureID = 0
		BEGIN

		IF CHARINDEX('.', @StoredProcedureName) = 0
			SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
		--ENDIF
		
		SELECT @StoredProcedureID = O.Object_ID
		FROM sys.Objects O
			JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
				AND S.Name + '.' + O.Name = @StoredProcedureName
				
		END
	--ENDIF

	INSERT INTO @tReturn
		(ParameterName,DataType)
	SELECT 
		REPLACE(P.Parameter_Name, '@', ''),
		P.Data_Type
	FROM INFORMATION_SCHEMA.PARAMETERS P
		JOIN sys.Objects O ON O.Name = P.Specific_Name
			AND O.Object_ID = @StoredProcedureID
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
			AND S.Name = P.Specific_Schema
	ORDER BY P.Parameter_Name
					
	RETURN

END
GO
--End function utility.GetStoredProcedureParameters

--Begin function workflow.GetPersonReviewformAccessLevel
EXEC utility.DropObject 'workflow.GetPersonReviewformAccessLevel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the current access level of a person on a review form
-- ====================================================================================
CREATE FUNCTION workflow.GetPersonReviewformAccessLevel
(
@PersonID INT,
@ReviewFormID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cAccessLevel VARCHAR(50) = 'None'
	DECLARE @cStatusCode VARCHAR(50)
	DECLARE @nAssignedPersonID INT
	DECLARE @nCreatePersonID INT

	SELECT
		@cStatusCode = S.StatusCode,
		@nAssignedPersonID = RF.AssignedPersonID,
		@nCreatePersonID = RF.CreatePersonID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND RF.ReviewFormID = @ReviewFormID

	IF @nAssignedPersonID = @PersonID
		BEGIN

		IF @cStatusCode IN ('BackToAuthor', 'InProgress') AND @nAssignedPersonID = @nCreatePersonID
			SET @cAccessLevel = 'Full'
		ELSE IF @cStatusCode IN ('InProgress', 'UnderReview')
			SET @cAccessLevel = 'DocumentsAndWorkflow'
		--ENDIF

		END
	--ENDIF

	RETURN @cAccessLevel

END
GO
--End function workflow.GetPersonReviewformAccessLevel

--Begin function workflow.GetStatusIDByEventCode
EXEC utility.DropObject 'workflow.GetStatusIDByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get a status for a review form based on an event code
-- ================================================================================
CREATE FUNCTION workflow.GetStatusIDByEventCode
(
@EventCode VARCHAR(50),
@WorkflowStepNumber INT,
@ReviewFormID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nStatusID INT

	SELECT
		@nStatusID = 
			CASE
				WHEN @EventCode = 'IncrementWorkflow'
				THEN 
					CASE
						WHEN workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) = @WorkflowStepNumber
						THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Complete'), 0)
						ELSE ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
					END
				WHEN @EventCode = 'DecrementWorkflow'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor'), 0)
				WHEN @EventCode = 'Update'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0)
				WHEN @EventCode = 'Withdraw'
				THEN ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Withdrawn'), 0)
				ELSE RF.StatusID
			END
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	RETURN ISNULL(@nStatusID, 0)

END
GO
--End function workflow.GetStatusIDByEventCode

--Begin function workflow.GetWorkflowStepCountByWorkflowID
EXEC utility.DropObject 'workflow.GetWorkflowStepCountByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the number of active steps in a workflow
-- =======================================================================
CREATE FUNCTION workflow.GetWorkflowStepCountByWorkflowID
(
@WorkflowID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT = 0

	SELECT @nWorkflowStepCount = COUNT(WS.WorkflowStepID)
	FROM workflow.WorkflowStep WS
	WHERE WS.WorkflowID = @WorkflowID
		AND WS.IsActive = 1
					
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCountByWorkflowID

--Begin function workflow.GetWorkflowStepNameByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowStepNameByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the current workflow step for a review form
-- ==========================================================================
CREATE FUNCTION workflow.GetWorkflowStepNameByReviewFormID
(
@ReviewFormID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStepName VARCHAR(50)

	SELECT @cWorkflowStepName = R.RoleName
	FROM workflow.WorkflowStep WS
		JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
		JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode
			AND RF.ReviewFormID = @ReviewFormID
			AND WS.DisplayOrder = RF.WorkflowStepNumber

	RETURN @cWorkflowStepName

END
GO
--End function workflow.GetWorkflowStepNameByReviewFormID

--Begin function workflow.GetWorkflowStepPeopleByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowStepPeopleByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.01
-- Description:	A function to get the people for the next workflow step
-- ====================================================================
CREATE FUNCTION workflow.GetWorkflowStepPeopleByReviewFormID
(
@ReviewFormID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cWorkflowStepPeople VARCHAR(MAX) = ''

	;
	WITH WSD AS 
		(
		SELECT 
			WS.RoleCode, 
			R.HasOrganization,
			RF.OriginatingOrganizationID
		FROM workflow.WorkflowStep WS
			JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode
			JOIN reviewform.ReviewForm RF ON RF.WorkflowID = WS.WorkflowID
				AND RF.WorkflowStepNumber + 1 = WS.DisplayOrder
				AND RF.ReviewFormID = @ReviewFormID
		)

	SELECT @cWorkflowStepPeople = 
		COALESCE(@cWorkflowStepPeople 
			+ ',{"PersonID":' 
			+ CAST(P.PersonID AS VARCHAR(10)) 
			+ ', "PersonNameFormatted":"' 
			+ person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') + '"}', ',')
	FROM person.Person P
	WHERE P.IsActive = 1
		AND EXISTS
			(
			SELECT 1
			FROM person.PersonOrganizationRole POR 
				JOIN dropdown.Role R ON R.RoleID = POR.RoleID
				JOIN WSD ON WSD.RoleCode = R.RoleCode
					AND (WSD.HasOrganization = 0 OR POR.OrganizationID = WSD.OriginatingOrganizationID)
					AND P.PersonID = POR.PersonID
			)
	ORDER BY P.LastName, P.FirstName, P.PersonID

	IF @cWorkflowStepPeople = ''
		SET @cWorkflowStepPeople = '[]'
	ELSE
		SET @cWorkflowStepPeople = '[' + STUFF(@cWorkflowStepPeople, 1, 1, '') + ']'
	--ENDIF

	RETURN @cWorkflowStepPeople

END
GO
--End function workflow.GetWorkflowStepPeopleByReviewFormID

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures - Dropdown.sql
USE SBRS_DHHS
GO

--Begin procedure dropdown.GetBundleTypeData
EXEC utility.DropObject 'dropdown.GetBundleTypeData'
GO

CREATE PROCEDURE dropdown.GetBundleTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.BundleTypeID,
		T.BundleTypeCode,
		T.BundleTypeName
	FROM dropdown.BundleType T
	WHERE T.IsActive = 1
		AND T.BundleTypeID > 0
	ORDER BY T.DisplayOrder, T.BundleTypeName, T.BundleTypeID

END
GO
--End procedure dropdown.GetBundleTypeData

--Begin procedure dropdown.GetCOACSData
EXEC utility.DropObject 'dropdown.GetCOACSData'
GO

CREATE PROCEDURE dropdown.GetCOACSData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.COACSID,
		T.COACSCode,
		T.COACSName
	FROM dropdown.COACS T
	WHERE T.IsActive = 1
		AND T.COACSID > 0
	ORDER BY T.DisplayOrder, T.COACSName, T.COACSID

END
GO
--End procedure dropdown.GetCOACSData

--Begin procedure dropdown.GetContractTerminationData
EXEC utility.DropObject 'dropdown.GetContractTerminationData'
GO

CREATE PROCEDURE dropdown.GetContractTerminationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractTerminationID,
		T.ContractTerminationCode,
		T.ContractTerminationName
	FROM dropdown.ContractTermination T
	WHERE T.IsActive = 1
		AND T.ContractTerminationID > 0
	ORDER BY T.DisplayOrder, T.ContractTerminationName, T.ContractTerminationID

END
GO
--End procedure dropdown.GetContractTerminationData

--Begin procedure dropdown.GetDocumentTypeData
EXEC utility.DropObject 'dropdown.GetDocumentTypeData'
GO

CREATE PROCEDURE dropdown.GetDocumentTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
	WHERE T.IsActive = 1
		AND T.DocumentTypeID > 0
	ORDER BY T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetFARJustificationData
EXEC utility.DropObject 'dropdown.GetFARJustificationData'
GO

CREATE PROCEDURE dropdown.GetFARJustificationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FARJustificationID,
		T.FARJustificationCode,
		T.FARJustificationName
	FROM dropdown.FARJustification T
	WHERE T.IsActive = 1
		AND T.FARJustificationID > 0
	ORDER BY T.DisplayOrder, T.FARJustificationName, T.FARJustificationID

END
GO
--End procedure dropdown.GetFARJustificationData

--Begin procedure dropdown.GetFundingOrganizationData
EXEC utility.DropObject 'dropdown.GetFundingOrganizationData'
GO

CREATE PROCEDURE dropdown.GetFundingOrganizationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.OrganizationID,
		T.OrganizationCode,
		T.OrganizationName
	FROM dropdown.Organization T
	WHERE T.IsActive = 1
		AND T.OrganizationID > 0
		AND T.IsForFundingOffice = 1
	ORDER BY T.DisplayOrder, T.OrganizationName, T.OrganizationID

END
GO
--End procedure dropdown.GetFundingOrganizationData

--Begin procedure dropdown.GetGSAJustificationData
EXEC utility.DropObject 'dropdown.GetGSAJustificationData'
GO

CREATE PROCEDURE dropdown.GetGSAJustificationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GSAJustificationID,
		T.GSAJustificationCode,
		T.GSAJustificationName
	FROM dropdown.GSAJustification T
	WHERE T.IsActive = 1
		AND T.GSAJustificationID > 0
	ORDER BY T.DisplayOrder, T.GSAJustificationName, T.GSAJustificationID

END
GO
--End procedure dropdown.GetGSAJustificationData

--Begin procedure dropdown.GetNAICSData
EXEC utility.DropObject 'dropdown.GetNAICSData'
GO

CREATE PROCEDURE dropdown.GetNAICSData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.NAICSID,
		T.NAICSCode,
		T.NAICSName
	FROM dropdown.NAICS T
	WHERE T.IsActive = 1
		AND T.NAICSID > 0
	ORDER BY T.DisplayOrder, T.NAICSName, T.NAICSID

END
GO
--End procedure dropdown.GetNAICSData

--Begin procedure dropdown.GetOrganizationData
EXEC utility.DropObject 'dropdown.GetOrganizationData'
GO

CREATE PROCEDURE dropdown.GetOrganizationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.OrganizationID,
		T.OrganizationCode,
		T.OrganizationName
	FROM dropdown.Organization T
	WHERE T.IsActive = 1
		AND T.OrganizationID > 0
		AND T.OrganizationCode <> 'NON'
	ORDER BY T.DisplayOrder, T.OrganizationName, T.OrganizationID

END
GO
--End procedure dropdown.GetOrganizationData

--Begin procedure dropdown.GetPriorBundlerData
EXEC utility.DropObject 'dropdown.GetPriorBundlerData'
GO

CREATE PROCEDURE dropdown.GetPriorBundlerData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PriorBundlerID,
		T.PriorBundlerCode,
		T.PriorBundlerName
	FROM dropdown.PriorBundler T
	WHERE T.IsActive = 1
		AND T.PriorBundlerID > 0
	ORDER BY T.DisplayOrder, T.PriorBundlerName, T.PriorBundlerID

END
GO
--End procedure dropdown.GetPriorBundlerData

--Begin procedure dropdown.GetProcurementHistoryData
EXEC utility.DropObject 'dropdown.GetProcurementHistoryData'
GO

CREATE PROCEDURE dropdown.GetProcurementHistoryData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProcurementHistoryID,
		T.ProcurementHistoryCode,
		T.ProcurementHistoryName
	FROM dropdown.ProcurementHistory T
	WHERE T.IsActive = 1
		AND T.ProcurementHistoryID > 0
	ORDER BY T.DisplayOrder, T.ProcurementHistoryName, T.ProcurementHistoryID

END
GO
--End procedure dropdown.GetProcurementHistoryData

--Begin procedure dropdown.GetProcurementMethodData
EXEC utility.DropObject 'dropdown.GetProcurementMethodData'
GO

CREATE PROCEDURE dropdown.GetProcurementMethodData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProcurementMethodID,
		T.ProcurementMethodCode,
		T.ProcurementMethodName
	FROM dropdown.ProcurementMethod T
	WHERE T.IsActive = 1
		AND T.ProcurementMethodID > 0
	ORDER BY T.DisplayOrder, T.ProcurementMethodName, T.ProcurementMethodID

END
GO
--End procedure dropdown.GetProcurementMethodData

--Begin procedure dropdown.GetResearchTypeData
EXEC utility.DropObject 'dropdown.GetResearchTypeData'
GO

CREATE PROCEDURE dropdown.GetResearchTypeData

@ReviewFormID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResearchTypeID,
		T.ResearchTypeCode,
		T.ResearchTypeName,
		
		CASE
			WHEN @ReviewFormID > 0 AND EXISTS (SELECT 1 FROM reviewform.ReviewFormResearchType RFRT WHERE RFRT.ReviewFormID = @ReviewFormID AND RFRT.ResearchTypeID = T.ResearchTypeID)
			THEN 1
			ELSE 0
		END AS IsSelected

	FROM dropdown.ResearchType T
	WHERE T.IsActive = 1
		AND T.ResearchTypeID > 0
	ORDER BY T.DisplayOrder, T.ResearchTypeName, T.ResearchTypeID

END
GO
--End procedure dropdown.GetResearchTypeData

--Begin procedure dropdown.GetRoleData
EXEC utility.DropObject 'dropdown.GetRoleData'
GO

CREATE PROCEDURE dropdown.GetRoleData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE T.IsActive = 1
		AND T.RoleID > 0
		AND T.IsForDisplay = 1 
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSizeTypeData
EXEC utility.DropObject 'dropdown.GetSizeTypeData'
GO

CREATE PROCEDURE dropdown.GetSizeTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SizeTypeID,
		T.SizeTypeCode,
		T.SizeTypeName
	FROM dropdown.SizeType T
	WHERE T.IsActive = 1
		AND T.SizeTypeID > 0
	ORDER BY T.DisplayOrder, T.SizeTypeName, T.SizeTypeID

END
GO
--End procedure dropdown.GetSizeTypeData

--Begin procedure dropdown.GetStatusData
EXEC utility.DropObject 'dropdown.GetStatusData'
GO

CREATE PROCEDURE dropdown.GetStatusData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE T.IsActive = 1
		AND T.StatusID > 0
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure dropdown.GetSubContractingOpportunityData
EXEC utility.DropObject 'dropdown.GetSubContractingOpportunityData'
GO
--End procedure dropdown.GetSubContractingOpportunityData

--Begin procedure dropdown.GetSubContractingPlanData
EXEC utility.DropObject 'dropdown.GetSubContractingPlanData'
GO

CREATE PROCEDURE dropdown.GetSubContractingPlanData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractingPlanID,
		T.SubContractingPlanCode,
		T.SubContractingPlanName
	FROM dropdown.SubContractingPlan T
	WHERE T.IsActive = 1
		AND T.SubContractingPlanID > 0
	ORDER BY T.DisplayOrder, T.SubContractingPlanName, T.SubContractingPlanID

END
GO
--End procedure dropdown.GetSubContractingPlanData

--Begin procedure dropdown.GetSynopsisData
EXEC utility.DropObject 'dropdown.GetSynopsisData'
GO

CREATE PROCEDURE dropdown.GetSynopsisData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SynopsisID,
		T.SynopsisCode,
		T.SynopsisName
	FROM dropdown.Synopsis T
	WHERE T.IsActive = 1
		AND T.SynopsisID > 0
	ORDER BY T.DisplayOrder, T.SynopsisName, T.SynopsisID

END
GO
--End procedure dropdown.GetSynopsisData

--Begin procedure dropdown.GetSynopsisExceptionReasonData
EXEC utility.DropObject 'dropdown.GetSynopsisExceptionReasonData'
GO

CREATE PROCEDURE dropdown.GetSynopsisExceptionReasonData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SynopsisExceptionReasonID,
		T.SynopsisExceptionReasonCode,
		T.SynopsisExceptionReasonName
	FROM dropdown.SynopsisExceptionReason T
	WHERE T.IsActive = 1
		AND T.SynopsisExceptionReasonID > 0
	ORDER BY T.DisplayOrder, T.SynopsisExceptionReasonName, T.SynopsisExceptionReasonID

END
GO
--End procedure dropdown.GetSynopsisExceptionReasonData

--Begin procedure dropdown.GetUnrestrictedProcurementReasonData
EXEC utility.DropObject 'dropdown.GetUnrestrictedProcurementReasonData'
GO

CREATE PROCEDURE dropdown.GetUnrestrictedProcurementReasonData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnrestrictedProcurementReasonID,
		T.UnrestrictedProcurementReasonCode,
		T.UnrestrictedProcurementReasonName
	FROM dropdown.UnrestrictedProcurementReason T
	WHERE T.IsActive = 1
		AND T.UnrestrictedProcurementReasonID > 0
	ORDER BY T.DisplayOrder, T.UnrestrictedProcurementReasonName, T.UnrestrictedProcurementReasonID

END
GO
--End procedure dropdown.GetUnrestrictedProcurementReasonData

--End file Build File - 03 - Procedures - Dropdown.sql

--Begin file Build File - 03 - Procedures - Other.sql
USE SBRS_DHHS
GO
	
--Begin procedure emailtemplate.GetEmailData
EXEC Utility.DropObject 'emailtemplate.GetEmailData'
GO
--End procedure emailtemplate.GetEmailData

--Begin procedure emailtemplate.GetNewUserRegistrationEmailData
EXEC Utility.DropObject 'emailtemplate.GetNewUserRegistrationEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetNewUserRegistrationEmailData

@PersonID INT,
@HRef VARCHAR(500)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cOrganizationName VARCHAR(250)
	DECLARE @cPersonNameFormatted VARCHAR(250)
	DECLARE @cRoleNameList VARCHAR(MAX)
	DECLARE @nOrganizationID INT

	SELECT 
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst'),
		@nOrganizationID = O.OrganizationID,
		@cOrganizationName = O.OrganizationName
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

	SELECT @cRoleNameList = COALESCE(@cRoleNameList + ', ', '') + R.RoleName
	FROM dropdown.Role R
		JOIN person.PersonOrganizationRole POR ON POR.RoleID = R.RoleID
			AND POR.PersonID = @PersonID
	ORDER BY R.RoleName

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'NewUserRegistration'

	SET @cEmailText = REPLACE(@cEmailText, '[[EmailAddress]]', ISNULL(@cEmailAddress, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[HRef]]', ISNULL(@HRef, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[OrganizationName]]', ISNULL(@cOrganizationName, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[RoleNameList]]', ISNULL(@cRoleNameList, ''))

	SELECT
		@nOrganizationID AS OrganizationID,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetNewUserRegistrationEmailData

--Begin procedure emailtemplate.GetReviewFormAssignmentEmailData
EXEC Utility.DropObject 'emailtemplate.GetReviewFormAssignmentEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetReviewFormAssignmentEmailData

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cControlCode VARCHAR(50)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cPersonNameFormatted VARCHAR(250)
	DECLARE @cWorkflowStepName VARCHAR(50)

	SELECT
		@cControlCode = RF.ControlCode,
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cWorkflowStepName = R.RoleName
	FROM person.Person P
		JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = P.PersonID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = RF.WorkflowID
			AND WS.DisplayOrder = RF.WorkflowStepNumber
		JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'ReviewFormAssignment'

	SET @cEmailText = REPLACE(@cEmailText, '[[ControlCode]]', ISNULL(@cControlCode, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[WorkflowStepName]]', ISNULL(@cWorkflowStepName, ''))

	SELECT
		@cEmailAddress AS EmailAddress,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetReviewFormAssignmentEmailData

--Begin procedure emailtemplate.GetReviewFormCloseOutEmailData
EXEC Utility.DropObject 'emailtemplate.GetReviewFormCloseOutEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetReviewFormCloseOutEmailData

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cControlCode VARCHAR(50)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cPersonNameFormatted VARCHAR(250)

	SELECT
		@cControlCode = RF.ControlCode,
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast')
	FROM person.Person P
		JOIN reviewform.ReviewForm RF ON RF.CreatePersonID = P.PersonID
			AND RF.ReviewFormID = @ReviewFormID

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'ReviewFormComplete'

	SET @cEmailText = REPLACE(@cEmailText, '[[ControlCode]]', ISNULL(@cControlCode, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))

	SELECT
		@cEmailAddress AS EmailAddress,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetReviewFormCloseOutEmailData

--Begin procedure emailtemplate.GetUserActivationEmailData
EXEC Utility.DropObject 'emailtemplate.GetUserActivationEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetUserActivationEmailData

@PersonID INT,
@HRef VARCHAR(500)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cFirstName VARCHAR(250)
	DECLARE @nOrganizationID INT

	SELECT 
		@cFirstName = P.FirstName,
		@nOrganizationID = O.OrganizationID
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'UserActivation'

	SET @cEmailText = REPLACE(@cEmailText, '[[HRef]]', ISNULL(@HRef, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[FirstName]]', ISNULL(@cFirstName, ''))

	SELECT
		@nOrganizationID AS OrganizationID,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetUserActivationEmailData

--Begin procedure eventlog.LogPersonAction
EXEC Utility.DropObject 'eventlog.LogPersonAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@Comments VARCHAR(MAX) = NULL,
@EntityID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0,
@LogDateTime DATETIME = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PersonOrganizationRole VARCHAR(MAX)
	
	IF @LogDateTime IS NULL
		SET @LogDateTime = getDate()
	--ENDIF

	SELECT @PersonOrganizationRole = COALESCE(@PersonOrganizationRole, '') + T2.PersonOrganizationRole FROM (SELECT (SELECT T1.* FOR XML RAW('PersonOrganizationRole'), ELEMENTS) AS PersonOrganizationRole FROM person.PersonOrganizationRole T1 WHERE T1.PersonID = @EntityID) T2
		
	INSERT INTO eventlog.EventLog
		(Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID,CreateDateTime)
	SELECT
		@Comments,
		@EntityID,
		'Person',
		@EventCode,
		(
		SELECT T.*, 
		CAST(('<PersonOrganizationRoleRecords>' + @PersonOrganizationRole + '</PersonOrganizationRoleRecords>') AS XML)
		FOR XML RAW('Person'), ELEMENTS
		),
		@PersonID,
		@LogDateTime
	FROM person.Person T
	WHERE T.personID = @EntityID

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogReviewFormAction
EXEC Utility.DropObject 'eventlog.LogReviewFormAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewFormAction

@Action VARCHAR(100) = NULL,
@Comments VARCHAR(MAX) = NULL,
@CreateDateTime DATETIME = NULL,
@EntityID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ReviewFormContract VARCHAR(MAX)
	DECLARE @ReviewFormDocument VARCHAR(MAX)
	DECLARE @ReviewFormOptionYear VARCHAR(MAX)
	DECLARE @ReviewFormPointOfContact VARCHAR(MAX)
	DECLARE @ReviewFormProcurementHistory VARCHAR(MAX)
	DECLARE @ReviewFormProcurementMethod VARCHAR(MAX)
	DECLARE @ReviewFormResearchType VARCHAR(MAX)

	IF @CreateDateTime IS NULL
		SET @CreateDateTime = getDate()
	--ENDIF

	SELECT @ReviewFormContract = COALESCE(@ReviewFormContract, '') + T2.ReviewFormContract FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormContract'), ELEMENTS) AS ReviewFormContract FROM reviewform.ReviewFormContract T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormDocument = COALESCE(@ReviewFormDocument, '') + T2.ReviewFormDocument FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormDocument'), ELEMENTS) AS ReviewFormDocument FROM reviewform.ReviewFormDocument T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormOptionYear = COALESCE(@ReviewFormOptionYear, '') + T2.ReviewFormOptionYear FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormOptionYear'), ELEMENTS) AS ReviewFormOptionYear FROM reviewform.ReviewFormOptionYear T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormPointOfContact = COALESCE(@ReviewFormPointOfContact, '') + T2.ReviewFormPointOfContact FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormPointOfContact'), ELEMENTS) AS ReviewFormPointOfContact FROM reviewform.ReviewFormPointOfContact T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormProcurementHistory = COALESCE(@ReviewFormProcurementHistory, '') + T2.ReviewFormProcurementHistory FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormProcurementHistory'), ELEMENTS) AS ReviewFormProcurementHistory FROM reviewform.ReviewFormProcurementHistory T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormProcurementMethod = COALESCE(@ReviewFormProcurementMethod, '') + T2.ReviewFormProcurementMethod FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormProcurementMethod'), ELEMENTS) AS ReviewFormProcurementMethod FROM reviewform.ReviewFormProcurementMethod T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormResearchType = COALESCE(@ReviewFormResearchType, '') + T2.ReviewFormResearchType FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormResearchType'), ELEMENTS) AS ReviewFormResearchType FROM reviewform.ReviewFormResearchType T1 WHERE T1.ReviewFormID = @EntityID) T2
		
	INSERT INTO eventlog.EventLog
		(Action,Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID,CreateDateTime)
	SELECT
		@Action,
		@Comments,
		@EntityID,
		'ReviewForm',
		@EventCode,
		(
		SELECT T.*, 
		CAST(('<ReviewFormContractRecords>' + @ReviewFormContract + '</ReviewFormContractRecords>') AS XML), 
		CAST(('<ReviewFormDocumentRecords>' + @ReviewFormDocument + '</ReviewFormDocumentRecords>') AS XML), 
		CAST(('<ReviewFormOptionYearRecords>' + @ReviewFormOptionYear + '</ReviewFormOptionYearRecords>') AS XML), 
		CAST(('<ReviewFormPointOfContactRecords>' + @ReviewFormPointOfContact + '</ReviewFormPointOfContactRecords>') AS XML), 
		CAST(('<ReviewFormProcurementHistoryRecords>' + @ReviewFormProcurementHistory + '</ReviewFormProcurementHistoryRecords>') AS XML), 
		CAST(('<ReviewFormProcurementMethodRecords>' + @ReviewFormProcurementMethod + '</ReviewFormProcurementMethodRecords>') AS XML), 
		CAST(('<ReviewFormResearchTypeRecords>' + @ReviewFormResearchType + '</ReviewFormResearchTypeRecords>') AS XML)
		FOR XML RAW('ReviewForm'), ELEMENTS
		),
		@PersonID,
		@CreateDateTime
	FROM reviewform.ReviewForm T
	WHERE T.ReviewFormID = @EntityID

END
GO
--End procedure eventlog.LogReviewFormAction

--Begin procedure utility.CreateGetDataMethodDefinition
EXEC utility.DropObject 'utility.CreateGetDataMethodDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:      Todd Pires
-- Create date: 2017.02.03
-- Description: A helper stored procedure for method creation
-- ==========================================================
CREATE PROCEDURE utility.CreateGetDataMethodDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(250),
@ConnectionString VARCHAR(250) = 'hhs-sbrs-connection'

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @cCRLF VARCHAR(2) = CHAR(13) + CHAR(10)
	DECLARE @cDataType VARCHAR(50)
	DECLARE @cParamaterName VARCHAR(250)
	DECLARE @cParamaterString1 VARCHAR(MAX) = ''
	DECLARE @cParamaterString2 VARCHAR(MAX) = ''
	DECLARE @cSQL VARCHAR(MAX) = ''
	DECLARE @cStoredProcedureName VARCHAR(250) = ''
	DECLARE @cTab1 VARCHAR(1) = CHAR(9)
	DECLARE @cTab2 VARCHAR(2) = REPLICATE(CHAR(9), 2)
	DECLARE @cTab3 VARCHAR(3) = REPLICATE(CHAR(9), 3)
	DECLARE @nPosition INT = 0

	SET @nPosition = CHARINDEX('.', @StoredProcedureName)
	SET @cStoredProcedureName = STUFF(@StoredProcedureName, 1, @nPosition, '')

	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			REPLACE(P.Name, '@', ''),

			CASE
				WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
				THEN 'string'
				ELSE Type_Name(P.User_Type_ID)
			END AS DataType

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

	OPEN oCursor
	FETCH oCursor INTO @cParamaterName, @cDataType
	WHILE @@fetch_status = 0
		BEGIN

		IF @cParamaterString1 <> ''
			SET @cParamaterString1 += ', '
		--ENDIF

		SET @cParamaterString1 += @cDataType + ' ' + @cParamaterName

		IF @cParamaterString2 <> ''
			SET @cParamaterString2 += ', '
		--ENDIF
		
		SET @cParamaterString2 += @cParamaterName + ' = ' + @cParamaterName

		FETCH oCursor INTO @cParamaterName, @cDataType

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	IF @cParamaterString2 <> ''
		SET @cParamaterString2 = 'new { ' + @cParamaterString2 + ' }, '
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('//' + @StoredProcedureName),
		('public IEnumerable<' + @ModelName + '>' + @cStoredProcedureName + '(' + @cParamaterString1 + ')'),
		(@cTab1 + '{'),
		(@cTab1 + 'IEnumerable<' + @ModelName + '> recordSet = null;'),
		(@cTab1 + 'try'),
		(@cTab2 + '{'),
		(@cTab2 + 'using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["' + @ConnectionString + '"].ConnectionString))'),
		(@cTab3 + '{'),
		(@cTab3 + 'recordSet = connection.Query<' + @ModelName + '>("' + @StoredProcedureName + '", ' + @cParamaterString2 + 'commandType: CommandType.StoredProcedure);'),
		(@cTab3 + '}'),
		(@cTab2 + '}'),
		(@cTab1 + 'catch (Exception error)'),
		(@cTab2 + '{')

	IF @cParamaterString1 <> ''
		BEGIN

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			(@cTab2 + 'Hashtable ExtraData = new Hashtable();')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			@cTab2 + 'ExtraData["' + REPLACE(P.Name, '@', '') + '"] = ' + REPLACE(P.Name, '@', '') + 
				CASE
					WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
					THEN ';'
					ELSE '.ToString();'
				END

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

		END
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES
		(''),
		(@cTab2 + 'SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, error);'),
		(@cTab2 + '}'),
		(@cTab1 + 'return recordSet;'),
		(@cTab1 + '}')

	SELECT TextString
	FROM @tTable
	ORDER BY RowIndex

END
GO
--End procedure utility.CreateGetDataMethodDefinition

--Begin procedure utility.CreateSaveDataMethodDefinition
EXEC utility.DropObject 'utility.CreateSaveDataMethodDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:      Todd Pires
-- Create date: 2017.02.03
-- Description: A helper stored procedure for method creation
-- ==========================================================
CREATE PROCEDURE utility.CreateSaveDataMethodDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(250),
@ConnectionString VARCHAR(250) = 'hhs-sbrs-connection'

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @cArgumentsList VARCHAR(MAX) = ''
	DECLARE @cCRLF VARCHAR(2) = CHAR(13) + CHAR(10)
	DECLARE @cDataType VARCHAR(50)
	DECLARE @cObjectName VARCHAR(250) = STUFF(@ModelName, 1, 1, LOWER(LEFT(@ModelName, 1)))
	DECLARE @cParamaterName VARCHAR(250)
	DECLARE @cParamaterString1 VARCHAR(MAX) = ''
	DECLARE @cSQL VARCHAR(MAX) = ''
	DECLARE @cStoredProcedureName VARCHAR(250) = ''
	DECLARE @cTab1 VARCHAR(1) = CHAR(9)
	DECLARE @cTab2 VARCHAR(2) = REPLICATE(CHAR(9), 2)
	DECLARE @cTab3 VARCHAR(3) = REPLICATE(CHAR(9), 3)
	DECLARE @cTab4 VARCHAR(4) = REPLICATE(CHAR(9), 4)
	DECLARE @nPosition INT = 0

	SET @nPosition = CHARINDEX('.', @StoredProcedureName)
	SET @cStoredProcedureName = STUFF(@StoredProcedureName, 1, @nPosition, '')

	SET @cArgumentsList = utility.GetStoredProcedureObjectArgumentList(@StoredProcedureName, @cObjectName)

	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			REPLACE(P.Name, '@', ''),

			CASE
				WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
				THEN 'string'
				ELSE Type_Name(P.User_Type_ID)
			END AS DataType

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

	OPEN oCursor
	FETCH oCursor INTO @cParamaterName, @cDataType
	WHILE @@fetch_status = 0
		BEGIN

		IF @cParamaterString1 <> ''
			SET @cParamaterString1 += ', '
		--ENDIF

		SET @cParamaterString1 += @cDataType + ' ' + @cParamaterName

		FETCH oCursor INTO @cParamaterName, @cDataType

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('//' + @StoredProcedureName),
		('public bool ' + @cStoredProcedureName + '(' + @ModelName + ' ' + @cObjectName + ')'),
		(@cTab1 + '{'),
		(@cTab1 + 'try'),
		(@cTab2 + '{'),
		(@cTab2 + 'using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["' + @ConnectionString + '"].ConnectionString))'),
		(@cTab3 + '{'),
		(@cTab3 + 'connection.Query<' + @ModelName + '>("' + @StoredProcedureName + '", new'),
		(@cTab4 + '{' + @cArgumentsList + '}, commandType: CommandType.StoredProcedure).SingleOrDefault();'),
		(@cTab3 + '}'),
		(@cTab2 + '}'),
		(@cTab1 + 'catch (Exception error)'),
		(@cTab2 + '{')

	IF @cParamaterString1 <> ''
		BEGIN

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			(@cTab2 + 'Hashtable ExtraData = new Hashtable();')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			@cTab2 + 'ExtraData["' + REPLACE(P.Name, '@', '') + '"] = ' + @cObjectName + '.' + REPLACE(P.Name, '@', '') + 
				CASE
					WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
					THEN ';'
					ELSE '.ToString();'
				END

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

		END
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES
		(''),
		(@cTab2 + 'SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, error);'),
		(@cTab2 + '}'),
		(@cTab1 + 'return true;'),
		(@cTab1 + '}')

	SELECT TextString
	FROM @tTable
	ORDER BY RowIndex

END
GO
--End procedure utility.CreateSaveDataMethodDefinition

--Begin procedure utility.CreateStoredProcedureClassDefinition
EXEC utility.DropObject 'utility.CreateStoredProcedureClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureClassDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nSQLVersion INT = CAST(LEFT(REPLACE(@@version, 'Microsoft SQL Server ', ''), 4) AS INT)
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF @nSQLVersion < 2012
		print 'This procedure only works on SQL Server 2012 or higher'
	ELSE
		BEGIN

		IF CHARINDEX('.', @StoredProcedureName) = 0
			SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
		--ENDIF

		INSERT INTO @tTable 
			(TextString) 
		VALUES
			('public class ' + @ModelName)

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			('{')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			CASE
				WHEN CHARINDEX('(', system_type_name, 0) = 0
				THEN utility.CreateTableColumnClassDefinition(name, 1, system_type_name)
				ELSE utility.CreateTableColumnClassDefinition(name, 1, LEFT(system_type_name, CHARINDEX('(', system_type_name, 0) - 1))
			END 
		FROM sys.dm_exec_describe_first_result_set_for_object
			(
			OBJECT_ID(@StoredProcedureName), 
			0
			)
		ORDER BY name

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			('}')
		
		SELECT TextString
		FROM @tTable	

		END
	--ENDIF

END
GO
--End procedure utility.CreateStoredProcedureClassDefinition

--Begin procedure utility.CreateStoredProcedureObjectArgumentList
EXEC utility.DropObject 'utility.CreateStoredProcedureObjectArgumentList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureObjectArgumentList

@StoredProcedureName VARCHAR(250),
@StoredProcedureObjectName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ParameterName VARCHAR(50)
	DECLARE @Return VARCHAR(MAX) = ''

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SPP.ParameterName
		FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP
	
	OPEN oCursor
	FETCH oCursor INTO @ParameterName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @Return <> ''
			SET @Return += ', '
		--ENDIF
		
		SET @Return += @ParameterName + ' = ' + @StoredProcedureObjectName + '.' + @ParameterName
		
		FETCH oCursor INTO @ParameterName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT @Return

END
GO
--End procedure utility.CreateStoredProcedureObjectArgumentList

--Begin procedure utility.CreateStoredProcedureParameterClassDefinition
EXEC utility.DropObject 'utility.CreateStoredProcedureParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureParameterClassDefinition
	@StoredProcedureName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('public class ' + REPLACE(PARSENAME(@StoredProcedureName, 1), 'Get', '')),
		('{')
		
	INSERT INTO @tTable
		(TextString) 
	SELECT 
		utility.CreateParameterClassDefinition(SPP.ParameterName, SPP.DataType)
	FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure utility.CreateStoredProcedureParameterClassDefinition

--Begin procedure utility.CreateTableClassDefinition
EXEC utility.DropObject 'utility.CreateTableClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableClassDefinition
	@TableName VARCHAR(250),
	@IncludeAnnotations BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColumnName VARCHAR(50)
	DECLARE @DataType VARCHAR(50)
	DECLARE @IsNullable BIT
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	SELECT
		'public class ' + T.Name
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
			AND S.Name + '.' + T.Name = @TableName

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('{')

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			C1.Name AS ColumnName, 
			C1.Is_Nullable AS IsNullable, 
			T1.Name AS DataType
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.schema_ID = O1.schema_ID
			JOIN sys.Columns C1 ON O1.Object_ID = C1.Object_ID
			JOIN sys.Types T1 ON C1.User_Type_ID = T1.User_Type_ID
				AND S1.Name + '.' + O1.Name = @TableName
				AND O1.Type = 'U'
		ORDER BY C1.Name

	
	OPEN oCursor
	FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @IncludeAnnotations = 1
			BEGIN

			IF @DataType IN ('date','datetime')
				BEGIN

				INSERT INTO @tTable 
					(TextString)
				VALUES
					('[DataType(DataType.Date)]'),
        	('[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]')
				
				END
			--ENDIF

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('[Display(Name = "' + @ColumnName + '")]')

			END
		--ENDIF
		
		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			utility.CreateTableColumnClassDefinition(@ColumnName, @IsNullable, @DataType)

		IF @IncludeAnnotations = 1
			BEGIN

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('')

			END
		--ENDIF
		
		FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure utility.CreateTableClassDefinition

--Begin procedure utility.CreateTableInsertUpdateDefinition
EXEC utility.DropObject 'utility.CreateTableInsertUpdateDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.05.05
-- Description:	A helper stored procedure for table creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableInsertUpdateDefinition
	@TableName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tTable1 TABLE
		(
		ColumnName VARCHAR(50), 
		IsIdentity BIT
		)

	DECLARE @tTable2 TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable1
		(ColumnName,IsIdentity)
	SELECT
		C.Name,
		C.Is_Identity
	FROM sys.objects O
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.Columns C ON O.Object_ID = C.Object_ID
		JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
			AND S.Name + '.' + O.Name = @TableName
			AND O.Type = 'U'
	ORDER BY C.Name

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('INSERT INTO ' + @TableName),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'OUTPUT INSERTED.' + T1.ColumnName + ' INTO @tOutput'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('VALUES'),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('UPDATE ' + @TableName),
		('SET')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ' = @' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'WHERE ' + T1.ColumnName + ' = @' + T1.ColumnName 
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1
		
	SELECT TextString
	FROM @tTable2

END
GO
--End procedure utility.CreateTableInsertUpdateDefinition

--Begin procedure utility.CreateTableSaveProcedureDefinition
EXEC utility.DropObject 'utility.CreateTableSaveProcedureDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.05.05
-- Description:	A helper stored procedure for table creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableSaveProcedureDefinition
	@TableName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cProcedureName VARCHAR(250)
	DECLARE @nCharIndex INT

	DECLARE @tTable1 TABLE
		(
		ColumnName VARCHAR(50), 
		DataType VARCHAR(50),
		Length INT,
		IsIdentity BIT
		)

	DECLARE @tTable2 TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nCharIndex = CHARINDEX('.', @TableName)
	SET @cProcedureName = STUFF(@TableName, @nCharIndex + 1, 0, 'Save')

	INSERT INTO @tTable1
		(ColumnName,DataType,Length,IsIdentity)
	SELECT
		C.Name AS ColumnName,
		UPPER(T.Name) AS DataType,
		C.Max_Length AS Length,
		C.Is_Identity
	FROM sys.objects O
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.Columns C ON O.Object_ID = C.Object_ID
		JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
			AND S.Name + '.' + O.Name = @TableName
			AND O.Type = 'U'
	ORDER BY C.Name

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('--Begin procedure ' + @cProcedureName),
		('EXEC utility.DropObject ''' + @cProcedureName + ''''),
		('GO'),
		(''),
		('SET ANSI_NULLS ON'),
		('GO'),
		('SET QUOTED_IDENTIFIER ON'),
		('GO'),
		(''),
		('-- ============================================================================================='),
		('-- Author:			Todd Pires'),
		('-- Create Date:	' + REPLACE(CONVERT(CHAR(10), getDate(), 126), '-', '.')),
		('-- Description:	A stored procedure to save data to the ' + @TableName + ' table'),
		('-- ============================================================================================='),
		('CREATE PROCEDURE ' + @cProcedureName),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ' ' + T1.DataType
		+ CASE
				WHEN T1.DataType = 'VARCHAR'
				THEN
					CASE 
						WHEN T1.Length = -1
						THEN '(MAX)'
						ELSE '(' + CAST(T1.Length AS VARCHAR(10)) + ')'
					END
				ELSE ''
			END
		+ ','
	FROM @tTable1 T1

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('AS'),
		('BEGIN'),
		('SET NOCOUNT ON;'),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'IF @' + T1.ColumnName + ' = 0'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('BEGIN'),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'DECLARE @tOutput TABLE (' + T1.ColumnName + ' ' + T1.DataType + ')'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('INSERT INTO ' + @TableName),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'OUTPUT INSERTED.' + T1.ColumnName + ' INTO @tOutput'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('VALUES'),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')'),
		(''),
		('END'),
		('ELSE'),
		('BEGIN'),
		(''),
		('UPDATE ' + @TableName),
		('SET')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ' = @' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'WHERE ' + T1.ColumnName + ' = @' + T1.ColumnName 
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('END'),
		('--ENDIF'),
		(''),
		('END'),
		('GO'),
		('--End procedure ' + @cProcedureName)
		
	SELECT TextString
	FROM @tTable2

END
GO
--End procedure utility.CreateTableSaveProcedureDefinition

--Begin procedure workflow.GetWorkflowByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to get workflow data for a reviewform.ReviewForm record
-- =======================================================================================
CREATE PROCEDURE workflow.GetWorkflowByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

SELECT
	person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
	person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
	person.GetOriginatingWorkflowStepNumberByPersonID(RF.CreatePersonID) AS OriginatingWorkflowStepNumber,
	S.StatusName,
	RF.WorkflowStepNumber,
	workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS WorkflowStepCount,
	workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
	workflow.GetWorkflowStepPeopleByReviewFormID(RF.ReviewFormID) AS WorkflowStepPeople
FROM reviewform.ReviewForm RF
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure workflow.GetWorkflowByReviewFormID

--Begin procedure workflow.GetWorkflowHistoryByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowHistoryByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to get workflow history for a reviewform.ReviewForm record
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowHistoryByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EL.Action,
		EL.Comments,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ReviewForm'
		AND EL.EntityID = @ReviewFormID
	ORDER BY EL.EventLogID

END
GO
--End procedure workflow.GetWorkflowHistoryByReviewFormID

--Begin procedure workflow.SetWorkflowByReviewFormID
EXEC utility.DropObject 'workflow.SetWorkflowByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to set a workflow for a reviewform.ReviewForm record
-- ====================================================================================
CREATE PROCEDURE workflow.SetWorkflowByReviewFormID

@ReviewFormID INT,
@TotalEstimatedValue INT = NULL,
@ProcurementMethodIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @TotalEstimatedValue IS NULL
		SET @TotalEstimatedValue = reviewform.GetTotalEstimatedValue(@ReviewFormID)
	--ENDIF

	UPDATE RF
	SET RF.WorkflowID = W.WorkflowID
	FROM reviewform.ReviewForm RF
		JOIN 
			(
			SELECT
				RF.ReviewFormID,

				CASE
					WHEN O.OrganizationCode = 'IHS'
					THEN O.OrganizationCode 
					ELSE 'HHS'
				END AS OrganizationCode,

				CASE
					WHEN @TotalEstimatedValue < 150000
						OR (@ProcurementMethodIDList IS NULL AND EXISTS (SELECT 1 FROM reviewform.ReviewFormProcurementMethod RFPM JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID AND PM.IsSetAside = 1 AND RFPM.ReviewFormID = RF.ReviewFormID))
						OR (@ProcurementMethodIDList IS NOT NULL AND EXISTS (SELECT 1 FROM dropdown.ProcurementMethod PM JOIN core.ListToTable(@ProcurementMethodIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PM.ProcurementMethodID AND PM.IsSetAside = 1))
					THEN 'Level1'
					WHEN @TotalEstimatedValue >= 5000000
					THEN 'Level3'
					ELSE 'Level2'
				END AS LevelCode

			FROM reviewform.ReviewForm RF
				JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
					AND RF.ReviewFormID = @ReviewFormID
			) WFD ON WFD.ReviewFormID = RF.ReviewFormID
		JOIN workflow.Workflow W ON W.OrganizationCode = WFD.OrganizationCode
			AND W.LevelCode = WFD.LevelCode

	EXEC workflow.GetWorkflowByReviewFormID @ReviewFormID = @ReviewFormID

END
GO
--End procedure workflow.SetWorkflowByReviewFormID
--End file Build File - 03 - Procedures - Other.sql

--Begin file Build File - 03 - Procedures - Person.sql
USE SBRS_DHHS
GO

--Begin procedure person.ActivateAccountByPersonID
EXEC utility.DropObject 'person.ActivateAccountByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:   Todd Pires
-- Create Date: 2017.07.14
-- Description: A stored procedure to update data in the person.Person table
-- =========================================================================
CREATE PROCEDURE person.ActivateAccountByPersonID

@PersonID INT

AS
BEGIN
SET NOCOUNT ON;
	
	UPDATE P
	SET P.IsActive = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.ActivateAccountByPersonID

--Begin procedure person.GetPersonByPersonID
EXEC Utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName,
		P.IsActive,
		P.JobTitle,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLogin,
		P.LastName,
		P.PersonID,
		P.PhoneNumber,
		O.OrganizationID,
		O.OrganizationName
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonDataBySearchCriteria
EXEC Utility.DropObject 'person.GetPersonDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table based on search criteria
-- =================================================================================================
CREATE PROCEDURE person.GetPersonDataBySearchCriteria

@EmailAddress VARCHAR(320) = NULL,
@FirstLetter CHAR(1) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = NULL,
@JobTitle VARCHAR(250) = NULL,
@Keyword VARCHAR(250) = NULL,
@LastName VARCHAR(50) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'LastName',
@OrganizationID INT = 0,
@OrganizationRoleOrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PersonID INT = 0,
@PhoneNumber VARCHAR(50) = NULL,
@RoleCode VARCHAR(50) = NULL,
@RoleID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	IF @Keyword IS NOT NULL
		SET @Keyword = '%' + @Keyword + '%'
	--ENDIF
	IF @RoleCode IS NOT NULL
		SET @RoleID = ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = @RoleCode), 0)
	--ENDIF

	;
	WITH PD AS
		(
		SELECT
			P.PersonID
		FROM person.Person P
			JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
				AND (@IsActive IS NULL OR P.IsActive = @IsActive)
				AND (@EmailAddress IS NULL OR P.EmailAddress LIKE '%' + @EmailAddress + '%')
				AND (@FirstLetter IS NULL OR LEFT(P.LastName, 1) = @FirstLetter)
				AND (@FirstName IS NULL OR P.FirstName LIKE '%' + @FirstName + '%')			
				AND (@JobTitle IS NULL OR P.JobTitle LIKE '%' + @JobTitle + '%')
				AND 
					(
					@Keyword IS NULL 
						OR P.EmailAddress LIKE @Keyword
						OR P.FirstName LIKE @Keyword
						OR P.JobTitle LIKE @Keyword
						OR P.LastName LIKE @Keyword
						OR O.OrganizationName LIKE @Keyword
						OR P.PhoneNumber LIKE @Keyword
					)
				AND (@LastName IS NULL OR P.LastName LIKE '%' + @LastName + '%')
				AND (@OrganizationID = 0 OR P.OrganizationID = @OrganizationID)
				AND (@PhoneNumber IS NULL OR P.PhoneNumber LIKE '%' + @PhoneNumber + '%')
				AND 
					(
					@RoleID = 0 
						OR EXISTS
							(
							SELECT 1
							FROM person.PersonOrganizationRole POR
							WHERE POR.PersonID = P.PersonID
								AND (@OrganizationRoleOrganizationID = 0 OR POR.OrganizationID = @OrganizationRoleOrganizationID)
								AND POR.RoleID = @RoleID
							)
					)
		)

	SELECT
		O.OrganizationName,
		P.IsActive,
		P.JobTitle,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PersonID, 
		P.EmailAddress,
		P.PhoneNumber,
		(SELECT COUNT(RT.PersonID) FROM person.Person RT) AS RecordsTotal,
		(SELECT COUNT(PD.PersonID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN person.Person P ON P.PersonID = PD.PersonID
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
	ORDER BY 
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
		CASE WHEN @OrderByField = 'JobTitle' AND @OrderByDirection = 'ASC' THEN P.JobTitle END ASC,
		CASE WHEN @OrderByField = 'JobTitle' AND @OrderByDirection = 'DESC' THEN P.JobTitle END DESC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'PhoneNumber' AND @OrderByDirection = 'ASC' THEN P.PhoneNumber END ASC,
		CASE WHEN @OrderByField = 'PhoneNumber' AND @OrderByDirection = 'DESC' THEN P.PhoneNumber END DESC,
		CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
		CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
		PD.PersonID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure person.GetPersonDataBySearchCriteria

--Begin procedure person.GetPersonOrganizationRolesByPersonID
EXEC Utility.DropObject 'person.GetPersonRolesByPersonID'
EXEC Utility.DropObject 'person.GetPersonOrganizationRolesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the person.PersonOrganizationRole table
-- ========================================================================================
CREATE PROCEDURE person.GetPersonOrganizationRolesByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		POR.PersonOrganizationRoleID,
		R.RoleID,
		R.RoleName,
		O.OrganizationID,
		O.OrganizationName
	FROM person.PersonOrganizationRole POR
		JOIN dropdown.Role R ON R.RoleID = POR.RoleID
			AND POR.PersonID = @PersonID
		JOIN dropdown.Organization O ON O.OrganizationID = POR.OrganizationID
	ORDER BY O.OrganizationName, O.OrganizationID, R.RoleName, R.RoleID

END
GO
--End procedure person.GetPersonOrganizationRolesByPersonID

--Begin procedure person.SavePerson
EXEC utility.DropObject 'person.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to save data to the person.Person table
-- =======================================================================
CREATE PROCEDURE person.SavePerson

@EmailAddress VARCHAR(320),
@FirstName VARCHAR(50),
@IsActive BIT,
@JobTitle VARCHAR(250),
@LastName VARCHAR(50),
@OrganizationID INT,
@Password VARCHAR(64),
@PersonID INT,
@PhoneNumber VARCHAR(50)

AS
BEGIN
SET NOCOUNT ON;
	
	DECLARE @cEventCode VARCHAR(50)
	DECLARE @cPasswordSalt VARCHAR(50) = newID()

	IF @PersonID = 0
		BEGIN

		SET @cEventCode = 'Create'

		DECLARE @tOutput TABLE (PersonID INT)

		INSERT INTO person.Person
			(
			EmailAddress,
			FirstName,
			IsActive,
			JobTitle,
			LastName,
			OrganizationID,
			Password,
			PasswordSalt,
			PhoneNumber
			)
		OUTPUT INSERTED.PersonID INTO @tOutput
		VALUES
			(
			@EmailAddress,
			@FirstName,
			@IsActive,
			@JobTitle,
			@LastName,
			@OrganizationID,
			person.HashPassword(@Password, @cPasswordSalt),
			@cPasswordSalt,
			@PhoneNumber
			)
		
		SELECT @PersonID = O.PersonID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @cEventCode = 'Update'

		UPDATE P
		SET
			P.EmailAddress = @EmailAddress,
			P.FirstName = @FirstName,
			P.IsActive = @IsActive,
			P.JobTitle = @JobTitle,
			P.LastName = @LastName,
			P.OrganizationID = @OrganizationID,
			P.Password = CASE WHEN @Password IS NULL OR LEN(RTRIM(@Password)) = 0 THEN P.Password ELSE person.HashPassword(@Password, P.PasswordSalt) END,
			P.PhoneNumber = @PhoneNumber
		FROM person.Person P
		WHERE P.PersonID = @PersonID

		DELETE T FROM person.PersonOrganizationRole T WHERE T.PersonID = @PersonID

		END
	--ENDIF

	SELECT
		@cEventCode AS EventCode,
		P.PersonID
	FROM person.Person P
	WHERE PersonID = @PersonID

END
GO
--End procedure person.SavePerson

--Begin procedure person.SavePersonOrganizationRole
EXEC utility.DropObject 'person.SavePersonOrganizationRole'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to save data to the person.PersonOrganizationRole table
-- =======================================================================================
CREATE PROCEDURE person.SavePersonOrganizationRole

@PersonID INT,
@OrganizationID INT,
@RoleID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cNotificationRoleCode VARCHAR(50)

	IF NOT EXISTS (SELECT 1 FROM person.PersonOrganizationRole POR WHERE POR.PersonID = @PersonID AND POR.OrganizationID = @OrganizationID AND POR.RoleID = @RoleID)
		BEGIN

		INSERT INTO person.PersonOrganizationRole
			(
			PersonID,
			OrganizationID,
			RoleID
			)
		VALUES
			(
			@PersonID,
			@OrganizationID,
			@RoleID
			)

		END
	--ENDIF

	SELECT R.NotificationRoleCode
	FROM dropdown.Role R
	WHERE R.RoleID = @RoleID

END
GO
--End procedure person.SavePersonOrganizationRole

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@EmailAddress VARCHAR(320),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsActive BIT = 0
	DECLARE @bIsValidPassword BIT = 0
	DECLARE @cOrganizationDisplayCode VARCHAR(50)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhoneNumber VARCHAR(50)
	DECLARE @dPasswordExpirationDateTime DATETIME
	DECLARE @nPersonID INT = ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.EmailAddress = @EmailAddress), 0)

	DECLARE @tPerson TABLE (PersonID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	IF @nPersonID = 0
		SET @nPersonID = ISNULL((SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.EmailAddress = @EmailAddress), 0)
	--ENDIF

	SELECT
		@bIsActive = P.IsActive,
		@cOrganizationDisplayCode = (SELECT O.OrganizationDisplayCode FROM dropdown.Organization O WHERE O.OrganizationID = P.OrganizationID),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cPhoneNumber = P.PhoneNumber,
		@dPasswordExpirationDateTime = P.PasswordExpirationDateTime,
		@nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.PersonID = @nPersonID

	INSERT INTO @tPerson (PersonID) VALUES (@nPersonID)

	IF @nPersonID > 0
		BEGIN

		IF @cPasswordDB IS NULL
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA1', @Password), 2)

			IF EXISTS (SELECT 1 FROM migration.UserMigration UM WHERE UM.EmailAddress = @EmailAddress AND UM.Password = @cPasswordHash)
				BEGIN
	
				SET @bIsValidPassword = 1
				SET @cPasswordSalt = newID()
				SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

				UPDATE P
				SET 
					P.PasswordSalt = @cPasswordSalt,
					P.Password = @cPasswordHash
				FROM person.Person P
				WHERE P.PersonID = @nPersonID

				DELETE UM
				FROM migration.UserMigration UM
				WHERE UM.PersonID = @nPersonID

				END
			--ENDIF

			END
		ELSE
			BEGIN

			SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsValidPassword = 0 AND @IncrementInvalidLoginAttempts = 1
			BEGIN
		
			UPDATE P
			SET P.InvalidLoginAttempts = P.InvalidLoginAttempts + 1
			FROM person.Person P
			WHERE P.PersonID = @nPersonID

			END
		--ENDIF

		IF @bIsValidPassword = 1
			BEGIN

			UPDATE P
			SET 
				P.InvalidLoginAttempts = 0,
				P.LastLoginDateTime = getDate()
			FROM person.Person P
			WHERE PersonID = @nPersonID

			END
		--ENDIF
	
		END
	--ENDIF	
	
	SELECT 
		@EmailAddress AS EmailAddress,
		ISNULL(@bIsActive, 0) AS IsActive,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM person.PersonOrganizationRole POR JOIN dropdown.Role R ON R.RoleID = POR.RoleID AND POR.PersonID = TP.PersonID AND R.RoleCode = 'SysAdmin')
			THEN 1 
			ELSE 0 
		END AS IsSystemAdministrator,

		CASE 
			WHEN @dPasswordExpirationDateTime IS NULL OR @dPasswordExpirationDateTime < SYSUTCDateTime()
			THEN 1 
			ELSE 0 
		END AS IsPasswordExpired,

		CASE
			WHEN TP.PersonID > 0
			THEN 1
			ELSE 0
		END AS IsValidEmailAddress,

		@bIsValidPassword AS IsValidPassword,
		@cOrganizationDisplayCode AS OrganizationDisplayCode,
		TP.PersonID,
		person.FormatPersonNameByPersonID(TP.PersonID, 'FirstLast') AS PersonNameFormatted,
		@cPhoneNumber AS PhoneNumber
	FROM @tPerson TP

END
GO
--End procedure person.ValidateLogin

--End file Build File - 03 - Procedures - Person.sql

--Begin file Build File - 03 - Procedures - ReviewForm.sql
USE SBRS_DHHS
GO

--Begin procedure reviewform.GetReviewFormByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table
-- ================================================================================
CREATE PROCEDURE reviewform.GetReviewFormByReviewFormID

@ReviewFormID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RF.AssignedPersonID,
		RF.BasePrice,
		core.FormatMoney(RF.BasePrice) AS BasePriceFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		RF.CreatePersonID,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		person.GetOriginatingWorkflowStepNumberByPersonID(RF.CreatePersonID) AS OriginatingWorkflowStepNumber,
		RF.Description,
		RF.FundingOrganizationID,
		RF.IsActive,

		CASE
			WHEN LEN(RTRIM(LTRIM(RF.FundingOrganizationName))) > 0
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationName
		END AS FundingOrganizationName,

		RF.OtherResearch,
		core.FormatDate(RF.PeriodOfPerformanceEndDate) AS PeriodOfPerformanceEndDate,
		core.FormatDate(RF.PeriodOfPerformanceStartDate) AS PeriodOfPerformanceStartDate,
		RF.ProcurementHistoryContractAwardAmount,
		core.FormatMoney(RF.ProcurementHistoryContractAwardAmount) AS ProcurementHistoryContractAwardAmountFormatted,
		core.FormatDate(RF.ProcurementHistoryContractAwardDate) AS ProcurementHistoryContractAwardDate,
		core.FormatDate(RF.ProcurementHistoryContractEndDate) AS ProcurementHistoryContractEndDate,
		RF.ProcurementHistoryContractNumber,
		RF.ProcurementHistoryContractTerminationID,
		RF.ProcurementHistoryContractTerminationReason,
		RF.RequisitionNumber1,
		RF.RequisitionNumber2,
		RF.ReviewFormID,
		workflow.GetPersonReviewformAccessLevel(@PersonID, RF.ReviewFormID) AS AccessLevel,
		reviewform.GetTotalEstimatedValue(RF.ReviewFormID) AS TotalEstimatedValue,
		core.FormatMoney(reviewform.GetTotalEstimatedValue(RF.ReviewFormID)) AS TotalEstimatedValueFormatted,
		RF.Size,
		RF.WorkflowStepNumber,
		BT.BundleTypeID,
		BT.BundleTypeName,
		C.COACSID,
		C.COACSName,
		CT.ContractTerminationID AS ProcurementHistoryContractTerminationID,
		CT.ContractTerminationName AS ProcurementHistoryContractTerminationName,
		N.NAICSID,
		N.NAICSCode,
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted,
		O2.OrganizationFilePath,
		PB.PriorBundlerID,
		PB.PriorBundlerName,
		PH.ProcurementHistoryID,
		PH.ProcurementHistoryName,
		S.StatusName,
		SYN.SynopsisID,
		SYN.SynopsisName,
		SCP.SubContractingPlanID,
		SCP.SubContractingPlanName,
		SER.SynopsisExceptionReasonID,
		SER.SynopsisExceptionReasonName,
		ST.SizeTypeID,
		ST.SizeTypeName,

		CASE
			WHEN @PersonID = RF.CreatePersonID AND S.StatusCode NOT IN ('Complete', 'Withdrawn') AND NOT EXISTS (SELECT 1 FROM eventLog.EventLog EL WHERE EL.EntityTypeCode = 'ReviewForm' AND EL.EntityID = RF.ReviewFormID AND EL.EventCode = 'IncrementWorkflow')
			THEN 1
			ELSE 0
		END AS CanHaveDelete,

		CASE
			WHEN @PersonID = RF.CreatePersonID AND S.StatusCode NOT IN ('Complete', 'Withdrawn') AND EXISTS (SELECT 1 FROM eventLog.EventLog EL WHERE EL.EntityTypeCode = 'ReviewForm' AND EL.EntityID = RF.ReviewFormID AND EL.EventCode = 'IncrementWorkflow')
			THEN 1
			ELSE 0
		END AS CanHaveWithdraw

	FROM reviewform.ReviewForm RF
		JOIN dropdown.BundleType BT ON BT.BundleTypeID = RF.BundleTypeID
		JOIN dropdown.COACS C ON C.COACSID = RF.COACSID
		JOIN dropdown.ContractTermination CT ON CT.ContractTerminationID = RF.ProcurementHistoryContractTerminationID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID		
		JOIN dropdown.PriorBundler PB ON PB.PriorBundlerID = RF.PriorBundlerID
		JOIN dropdown.ProcurementHistory PH ON PH.ProcurementHistoryID = RF.ProcurementHistoryID
		JOIN dropdown.SizeType ST ON ST.SizeTypeID = RF.SizeTypeID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		JOIN dropdown.SubContractingPlan SCP ON SCP.SubContractingPlanID = RF.SubContractingPlanID
		JOIN dropdown.Synopsis SYN ON SYN.SynopsisID = RF.SynopsisID
		JOIN dropdown.SynopsisExceptionReason SER ON SER.SynopsisExceptionReasonID = RF.SynopsisExceptionReasonID
			AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure reviewform.GetReviewFormByReviewFormID

--Begin procedure reviewform.GetReviewFormContractByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormContractByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormContract table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormContractByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFC.ContractNumber,
		RFC.ReviewFormContractID
	FROM reviewform.ReviewFormContract RFC
	WHERE RFC.ReviewFormID = @ReviewFormID
	ORDER BY RFC.ContractNumber, RFC.ReviewFormContractID

END
GO
--End procedure reviewform.GetReviewFormContractByReviewFormID

--Begin procedure reviewform.GetReviewFormDataByPersonID
EXEC Utility.DropObject 'reviewform.GetReviewFormDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.04.07
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on a personid
-- ====================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataByPersonID

@PersonID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT RF.ReviewFormID
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
				AND (RF.CreatePersonID = @PersonID OR RF.AssignedPersonID = @PersonID)

		UNION

		SELECT EL.EntityID AS ReviewFormID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode = 'ReviewForm'
			AND EL.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM reviewform.ReviewForm RF
					JOIN dropdown.Status S ON S.StatusID = RF.StatusID
						AND S.StatusCode NOT IN ('Complete','Withdrawn')
						AND RF.IsActive = 1
						AND RF.ReviewFormID = EL.EntityID
				)
		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataByPersonID

--Begin procedure reviewform.GetReviewFormDataBySearchCriteria
EXEC Utility.DropObject 'reviewform.GetReviewFormDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.03.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on search criteria
-- =========================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataBySearchCriteria

@AssignedPersonID INT = 0,
@AssignedPersonName VARCHAR(100) = NULL,
@COACSID INT = 0,
@ControlCode VARCHAR(50) = NULL,
@CreateDateFrom DATE = NULL,
@CreateDateTo DATE = NULL,
@CreatePersonID INT = 0,
@CreatePersonName VARCHAR(100) = NULL,
@FiscalYear INT = 0,
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@OriginatingOrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@RequisitionNumber VARCHAR(100) = NULL,
@RoleID INT = 0,
@StatusCodeList VARCHAR(MAX) = NULL,
@StatusID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	SET @AssignedPersonName = core.NullIfEmpty(@AssignedPersonName)
	SET @ControlCode = core.NullIfEmpty(@ControlCode)
	SET @CreateDateFrom = core.NullIfEmpty(@CreateDateFrom)
	SET @CreateDateTo = core.NullIfEmpty(@CreateDateTo)
	SET @CreatePersonName = core.NullIfEmpty(@CreatePersonName)
	SET @FundingOrganizationName = core.NullIfEmpty(@FundingOrganizationName)
	SET @RequisitionNumber = core.NullIfEmpty(@RequisitionNumber)
	SET @StatusCodeList = core.NullIfEmpty(@StatusCodeList)

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RF.ReviewFormID
		FROM reviewform.ReviewForm RF
		WHERE RF.IsActive = 1
			AND (@AssignedPersonName IS NULL OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = RF.AssignedPersonID AND (P.FirstName LIKE '%' + @AssignedPersonName + '%' OR P.LastName LIKE '%' + @AssignedPersonName + '%' OR P.FirstName + ' ' + P.LastName LIKE '%' + @AssignedPersonName + '%')))
			AND (@AssignedPersonID = 0 OR (@AssignedPersonID > 0 AND RF.AssignedPersonID = @AssignedPersonID) OR (@AssignedPersonID < 0 AND RF.AssignedPersonID <> ABS(@AssignedPersonID)))
			AND (@COACSID = 0 OR RF.COACSID = @COACSID)
			AND (@ControlCode IS NULL OR RF.ControlCode LIKE '%' + @ControlCode + '%')
			AND (@CreateDateFrom IS NULL OR RF.CreateDateTime >= @CreateDateFrom)
			AND (@CreateDateTo IS NULL OR RF.CreateDateTime <= @CreateDateTo)
			AND (@CreatePersonID = 0 OR RF.CreatePersonID = @CreatePersonID)
			AND (@CreatePersonName IS NULL OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = RF.CreatePersonID AND (P.FirstName LIKE '%' + @CreatePersonName + '%' OR P.LastName LIKE '%' + @CreatePersonName + '%' OR P.FirstName + ' ' + P.LastName LIKE '%' + @CreatePersonName + '%')))
			AND (@FiscalYear = 0 OR core.GetFiscalYearFromDateTime(RF.CreateDateTime) = @FiscalYear)
			AND (@FundingOrganizationID = 0 OR RF.FundingOrganizationID = @FundingOrganizationID)
			AND (@FundingOrganizationName IS NULL OR RF.FundingOrganizationName = @FundingOrganizationName)
			AND (@OriginatingOrganizationID = 0 OR RF.FundingOrganizationID = @OriginatingOrganizationID)				
			AND (@RequisitionNumber IS NULL OR RF.RequisitionNumber1 = @RequisitionNumber OR RF.RequisitionNumber2 = @RequisitionNumber)
			AND (@RoleID = 0 OR EXISTS (SELECT 1 FROM workflow.WorkflowStep WS JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode AND R.RoleID = @RoleID AND WS.WorkflowID = RF.WorkflowID AND RF.WorkflowStepNumber = WS.DisplayOrder))
			AND (@StatusCodeList IS NULL OR EXISTS (SELECT 1 FROM dropdown.Status S JOIN core.ListToTable(@StatusCodeList, ',') LTT  ON LTT.ListItem = S.StatusCode AND S.StatusID = RF.StatusID))
			AND (@StatusID = 0 OR RF.StatusID = @StatusID)
		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataBySearchCriteria

--Begin procedure reviewform.GetReviewFormDocumentByDocumentID
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentByDocumentID

@ReviewFormDocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE 
			WHEN RFD.IsFileGUIDFileName = 1
			THEN RFD.DocumentPath + RFD.FileGUID
			ELSE RFD.DocumentPath + RFD.DocumentName
		END AS DocumentPath,

		RFD.DocumentName
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

END
GO
--End procedure reviewform.GetReviewFormDocumentByDocumentID
	
--Begin procedure reviewform.GetReviewFormDocumentByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDateTime(RFD.CreateDateTime) AS CreateDateTimeFormatted,
		RFD.FileGUID,
		RFD.DocumentName,

		CASE
			WHEN CHARINDEX('.', RFD.DocumentName) > 0
			THEN REVERSE(LEFT(REVERSE(RFD.DocumentName), CHARINDEX('.', REVERSE(RFD.DocumentName)) - 1))
			ELSE NULL
		END AS FileExtension,

		RFD.IsFileGUIDFileName,
		RFD.ReviewFormDocumentID,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM reviewform.ReviewFormDocument RFD
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = RFD.DocumentTypeID
			AND RFD.ReviewFormID = @ReviewFormID
			AND RFD.IsActive = 1

	UNION

	SELECT
		'',
		'',
		'',
		'',
		'',
		0,
		0,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
	WHERE DT.DocumentTypeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM reviewform.ReviewFormDocument RFD
			WHERE RFD.ReviewFormID = @ReviewFormID
				AND RFD.IsActive = 1
				AND RFD.DocumentTypeID = DT.DocumentTypeID
			)

	ORDER BY DT.DocumentTypeName, DT.DocumentTypeID

END
GO
--End procedure reviewform.GetReviewFormDocumentByReviewFormID
	
--Begin procedure reviewform.GetReviewFormOptionYearByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormOptionYearByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormOptionYear table
-- ==========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormOptionYearByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFOY.OptionYear,
		RFOY.OptionYearValue,
		core.FormatMoney(RFOY.OptionYearValue) AS OptionYearValueFormatted,
		RFOY.ReviewFormID,
		RFOY.ReviewFormOptionYearID
	FROM reviewform.ReviewFormOptionYear RFOY
	WHERE RFOY.ReviewFormID = @ReviewFormID
	ORDER BY RFOY.OptionYear

END
GO
--End procedure reviewform.GetReviewFormOptionYearByReviewFormID
	
--Begin procedure reviewform.GetReviewFormPointOfContactByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormPointOfContactByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormPointOfContact table
-- ==============================================================================================
CREATE PROCEDURE reviewform.GetReviewFormPointOfContactByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPOC.PointOfContactEmailAddress,
		RFPOC.PointOfContactName,
		RFPOC.PointOfContactPhone,
		RFPOC.ReviewFormPointOfContactID
	FROM reviewform.ReviewFormPointOfContact RFPOC
	WHERE RFPOC.ReviewFormID = @ReviewFormID
	ORDER BY RFPOC.PointOfContactName, RFPOC.ReviewFormPointOfContactID

END
GO
--End procedure reviewform.GetReviewFormPointOfContactByReviewFormID

--Begin procedure reviewform.GetReviewFormProcurementHistoryByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormProcurementHistoryByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormProcurementHistory table
-- ==================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormProcurementHistoryByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPH.GSAScheduleNumber,
		RFPH.Percentage,
		RFPH.ReviewFormID,
		RFPH.ReviewFormProcurementHistoryID,
		RFPH.TaskOrderDescription,
		RFPH.TaskOrderNumber,
		RFPH.UnrestrictedProcurementReason,
		FJ.FARJustificationID,
		FJ.FARJustificationName,
		GJ.GSAJustificationID,
		GJ.GSAJustificationName,
		PM.ProcurementMethodID,
		PM.ProcurementMethodName,
		UPR.UnrestrictedProcurementReasonID,
		UPR.UnrestrictedProcurementReasonName
	FROM reviewform.ReviewFormProcurementHistory RFPH
		JOIN dropdown.FARJustification FJ ON FJ.FARJustificationID = RFPH.FARJustificationID
		JOIN dropdown.GSAJustification GJ ON GJ.GSAJustificationID = RFPH.GSAJustificationID
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPH.ProcurementMethodID
		JOIN dropdown.UnrestrictedProcurementReason UPR ON UPR.UnrestrictedProcurementReasonID = RFPH.UnrestrictedProcurementReasonID
			AND RFPH.ReviewFormID = @ReviewFormID
	ORDER BY RFPH.Percentage, PM.ProcurementMethodName, RFPH.ReviewFormProcurementHistoryID

END
GO
--End procedure reviewform.GetReviewFormProcurementHistoryByReviewFormID

--Begin procedure reviewform.GetReviewFormProcurementMethodByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormProcurementMethodByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormProcurementMethod table
-- =================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormProcurementMethodByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPM.GSAScheduleNumber,
		RFPM.Percentage,
		RFPM.ReviewFormID,
		RFPM.ReviewFormProcurementMethodID,
		RFPM.TaskOrderDescription,
		RFPM.TaskOrderNumber,
		RFPM.UnrestrictedProcurementReason,
		FJ.FARJustificationID,
		FJ.FARJustificationName,
		GJ.GSAJustificationID,
		GJ.GSAJustificationName,
		PM.ProcurementMethodID,
		PM.ProcurementMethodName,
		UPR.UnrestrictedProcurementReasonID,
		UPR.UnrestrictedProcurementReasonName
	FROM reviewform.ReviewFormProcurementMethod RFPM
		JOIN dropdown.FARJustification FJ ON FJ.FARJustificationID = RFPM.FARJustificationID
		JOIN dropdown.GSAJustification GJ ON GJ.GSAJustificationID = RFPM.GSAJustificationID
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID
		JOIN dropdown.UnrestrictedProcurementReason UPR ON UPR.UnrestrictedProcurementReasonID = RFPM.UnrestrictedProcurementReasonID
			AND RFPM.ReviewFormID = @ReviewFormID
	ORDER BY RFPM.Percentage, PM.ProcurementMethodName, RFPM.ReviewFormProcurementMethodID

END
GO
--End procedure reviewform.GetReviewFormProcurementMethodByReviewFormID

--Begin procedure reviewform.GetReviewFormResearchTypeByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormResearchTypeByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormResearchType table
-- ============================================================================================
CREATE PROCEDURE reviewform.GetReviewFormResearchTypeByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RT.ResearchTypeID,
		RT.ResearchTypeName
	FROM reviewform.ReviewFormResearchType RFRT
		JOIN dropdown.ResearchType RT ON RT.ResearchTypeID = RFRT.ResearchTypeID
			AND RFRT.ReviewFormID = @ReviewFormID
	ORDER BY RT.ResearchTypeName, RT.ResearchTypeID

END
GO
--End procedure reviewform.GetReviewFormResearchTypeByReviewFormID

--Begin procedure reviewform.SaveReviewForm
EXEC utility.DropObject 'reviewform.SaveReviewForm'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewForm

@AssignedPersonID INT = 0,
@BasePrice NUMERIC(18,2) = 0,
@BundleTypeID INT = 0,
@COACSID INT = 0,
@CreatePersonID INT = 0,
@Description VARCHAR(MAX) = NULL,
@EventCode VARCHAR(50) = 'Update',
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@InactiveDocumentTypeIDList VARCHAR(MAX) = NULL,
@IsActive BIT = 1,
@NAICSID INT = 0,
@OtherResearch VARCHAR(500) = NULL,
@PeriodOfPerformanceEndDate DATE = NULL,
@PeriodOfPerformanceStartDate DATE = NULL,
@PriorBundlerID INT = 0,
@ProcurementHistoryContractAwardAmount NUMERIC(18,2) = 0,
@ProcurementHistoryContractAwardDate DATE = NULL,
@ProcurementHistoryContractEndDate DATE = NULL,
@ProcurementHistoryContractNumber VARCHAR(50) = 0,
@ProcurementHistoryContractTerminationID INT = 0,
@ProcurementHistoryContractTerminationReason VARCHAR(MAX) = NULL,
@ProcurementHistoryID INT = 0,
@RequisitionNumber1 VARCHAR(100) = NULL,
@RequisitionNumber2 VARCHAR(100) = NULL,
@ReviewFormID INT = 0,
@Size BIGINT = 0,
@SizeTypeID INT = 0,
@SubContractingPlanID INT = 0,
@SynopsisExceptionReasonID INT = 0,	
@SynopsisID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ReviewFormID = 0
		BEGIN

		DECLARE @tOutput TABLE (ReviewFormID INT)

		INSERT INTO reviewform.ReviewForm
			(
			AssignedPersonID, 
			BasePrice,
			BundleTypeID,
			COACSID,
			ControlCode,
			CreatePersonID,
			Description,
			FundingOrganizationID,
			FundingOrganizationName,
			IsActive,
			NAICSID,
			OriginatingOrganizationID,
			OtherResearch,
			PeriodOfPerformanceEndDate,
			PeriodOfPerformanceStartDate,
			PriorBundlerID,
			ProcurementHistoryContractAwardAmount,
			ProcurementHistoryContractAwardDate,
			ProcurementHistoryContractEndDate,
			ProcurementHistoryContractNumber,
			ProcurementHistoryContractTerminationID,
			ProcurementHistoryContractTerminationReason,
			ProcurementHistoryID,
			RequisitionNumber1,
			RequisitionNumber2,
			Size,
			SizeTypeID,
			StatusID,
			SubContractingPlanID,
			SynopsisExceptionReasonID,
			SynopsisID,
			WorkflowStepNumber
			)
		OUTPUT INSERTED.ReviewFormID INTO @tOutput
		SELECT
			@CreatePersonID,
			@BasePrice,
			@BundleTypeID,
			@COACSID,
			reviewform.CreateControlCode(P.PersonID),
			@CreatePersonID,
			@Description,
			@FundingOrganizationID,
			@FundingOrganizationName,
			@IsActive,
			@NAICSID,
			P.OrganizationID,
			@OtherResearch,
			@PeriodOfPerformanceEndDate,
			@PeriodOfPerformanceStartDate,
			@PriorBundlerID,
			@ProcurementHistoryContractAwardAmount,
			@ProcurementHistoryContractAwardDate,
			@ProcurementHistoryContractEndDate,
			@ProcurementHistoryContractNumber,
			@ProcurementHistoryContractTerminationID,
			@ProcurementHistoryContractTerminationReason,
			@ProcurementHistoryID,
			@RequisitionNumber1,
			@RequisitionNumber2,
			@Size,
			@SizeTypeID,
			ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0),
			@SubContractingPlanID,
			@SynopsisExceptionReasonID,
			@SynopsisID,
			person.GetOriginatingWorkflowStepNumberByPersonID(@CreatePersonID)
		FROM person.Person P
		WHERE P.PersonID = @CreatePersonID
		
		SELECT @ReviewFormID = O.ReviewFormID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		UPDATE RF
		SET
			RF.AssignedPersonID = @AssignedPersonID,
			RF.BasePrice = @BasePrice,
			RF.BundleTypeID = @BundleTypeID,
			RF.COACSID = @COACSID,
			RF.Description = @Description,
			RF.FundingOrganizationID = @FundingOrganizationID,
			RF.FundingOrganizationName = @FundingOrganizationName,
			RF.IsActive = @IsActive,
			RF.NAICSID = @NAICSID,
			RF.OtherResearch = @OtherResearch,
			RF.PeriodOfPerformanceEndDate = @PeriodOfPerformanceEndDate,
			RF.PeriodOfPerformanceStartDate = @PeriodOfPerformanceStartDate,
			RF.PriorBundlerID = @PriorBundlerID,
			RF.ProcurementHistoryContractAwardAmount = @ProcurementHistoryContractAwardAmount,
			RF.ProcurementHistoryContractAwardDate = @ProcurementHistoryContractAwardDate,
			RF.ProcurementHistoryContractEndDate = @ProcurementHistoryContractEndDate,
			RF.ProcurementHistoryContractNumber = @ProcurementHistoryContractNumber,
			RF.ProcurementHistoryContractTerminationID = @ProcurementHistoryContractTerminationID,
			RF.ProcurementHistoryContractTerminationReason = @ProcurementHistoryContractTerminationReason,
			RF.ProcurementHistoryID = @ProcurementHistoryID,
			RF.RequisitionNumber1 = @RequisitionNumber1,
			RF.RequisitionNumber2 = @RequisitionNumber2,
			RF.Size = @Size,
			RF.SizeTypeID = @SizeTypeID,
			RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
			RF.SubContractingPlanID = @SubContractingPlanID,
			RF.SynopsisExceptionReasonID = @SynopsisExceptionReasonID,
			RF.SynopsisID = @SynopsisID,
			RF.WorkflowStepNumber = @WorkflowStepNumber
		FROM reviewform.ReviewForm RF
		WHERE RF.ReviewFormID = @ReviewFormID

		DELETE T FROM reviewform.ReviewFormContract T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormOptionYear T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormPointOfContact T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementHistory T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementMethod T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormResearchType T WHERE T.ReviewFormID = @ReviewFormID
		
		END
	--ENDIF

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

	IF @InactiveDocumentTypeIDList IS NOT NULL
		BEGIN

		UPDATE RFD
		SET RFD.IsActive = 0
		FROM reviewform.ReviewFormDocument RFD
			JOIN core.ListToTable(@InactiveDocumentTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RFD.DocumentTypeID
				AND RFD.ReviewFormID = @ReviewFormID

		END
	--ENDIF

END
GO
--End procedure reviewform.SaveReviewForm

--Begin procedure reviewform.SaveReviewFormWorkflowData
EXEC utility.DropObject 'reviewform.SaveReviewFormWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewFormWorkflowData

@AssignedPersonID INT = 0,
@EventCode VARCHAR(50) = 'Update',
@InactiveDocumentTypeIDList VARCHAR(MAX) = NULL,
@ReviewFormID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE RF
	SET
		RF.AssignedPersonID = @AssignedPersonID,
		RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
		RF.WorkflowStepNumber = @WorkflowStepNumber
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

	IF @InactiveDocumentTypeIDList IS NOT NULL
		BEGIN

		UPDATE RFD
		SET RFD.IsActive = 0
		FROM reviewform.ReviewFormDocument RFD
			JOIN core.ListToTable(@InactiveDocumentTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RFD.DocumentTypeID
				AND RFD.ReviewFormID = @ReviewFormID

		END
	--ENDIF

END
GO
--End procedure reviewform.SaveReviewFormWorkflowData

--Begin procedure reviewform.SaveReviewFormContract
EXEC utility.DropObject 'reviewform.SaveReviewFormContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormContract

@ContractNumber VARCHAR(100),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormContract
		(
		ContractNumber,
		ReviewFormID
		)
	VALUES
		(
		@ContractNumber,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormContract

--Begin procedure reviewform.SaveReviewFormDocument
EXEC utility.DropObject 'reviewform.SaveReviewFormDocument'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormDocument

@CreatePersonID INT,
@DocumentName VARCHAR(250),
@DocumentPath VARCHAR(50),
@DocumentTypeID INT,
@FileGUID VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormDocument
		(
		CreatePersonID,
		DocumentName,
		DocumentPath,
		DocumentTypeID,
		FileExtension,
		FileGUID,
		ReviewFormID
		)
	VALUES
		(
		@CreatePersonID,
		@DocumentName,
		@DocumentPath,
		@DocumentTypeID,

		CASE
			WHEN CHARINDEX('.', @DocumentName) > 0
			THEN REVERSE(LEFT(REVERSE(@DocumentName), CHARINDEX('.', REVERSE(@DocumentName))))
			ELSE NULL
		END,

		@FileGUID,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormDocument

--Begin procedure reviewform.SaveReviewFormOptionYear
EXEC utility.DropObject 'reviewform.SaveReviewFormOptionYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormOptionYear

@OptionYear INT,
@OptionYearValue NUMERIC(18,2),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormOptionYear
		(
		OptionYear,
		OptionYearValue,
		ReviewFormID
		)
	VALUES
		(
		@OptionYear,
		@OptionYearValue,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormOptionYear

--Begin procedure reviewform.SaveReviewFormPointOfContact
EXEC utility.DropObject 'reviewform.SaveReviewFormPointOfContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormPointOfContact

@PointOfContactEmailAddress VARCHAR(320),
@PointOfContactName VARCHAR(50),
@PointOfContactPhone VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormPointOfContact
		(
		PointOfContactEmailAddress,
		PointOfContactName,
		PointOfContactPhone,
		ReviewFormID
		)
	VALUES
		(
		@PointOfContactEmailAddress,
		@PointOfContactName,
		@PointOfContactPhone,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormPointOfContact

--Begin procedure reviewform.SaveReviewFormProcurementHistory
EXEC utility.DropObject 'reviewform.SaveReviewFormProcurementHistory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormProcurementHistory

@FARJustificationID INT,
@GSAJustificationID INT,
@GSAScheduleNumber VARCHAR(50),
@Percentage INT,
@ProcurementMethodID INT,
@ReviewFormID INT,
@TaskOrderDescription VARCHAR(MAX),
@TaskOrderNumber VARCHAR(50),
@UnrestrictedProcurementReason VARCHAR(MAX),
@UnrestrictedProcurementReasonID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormProcurementHistory
		(
		FARJustificationID,
		GSAJustificationID,
		GSAScheduleNumber,
		Percentage,
		ProcurementMethodID,
		ReviewFormID,
		TaskOrderDescription,
		TaskOrderNumber,
		UnrestrictedProcurementReason,
		UnrestrictedProcurementReasonID
		)
	VALUES
		(
		@FARJustificationID,
		@GSAJustificationID,
		@GSAScheduleNumber,
		@Percentage,
		@ProcurementMethodID,
		@ReviewFormID,
		@TaskOrderDescription,
		@TaskOrderNumber,
		@UnrestrictedProcurementReason,
		@UnrestrictedProcurementReasonID
		)

END
GO
--End procedure reviewform.SaveReviewFormProcurementHistory

--Begin procedure reviewform.SaveReviewFormProcurementMethod
EXEC utility.DropObject 'reviewform.SaveReviewFormProcurementMethod'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormProcurementMethod

@FARJustificationID INT,
@GSAJustificationID INT,
@GSAScheduleNumber VARCHAR(50),
@Percentage INT,
@ProcurementMethodID INT,
@ReviewFormID INT,
@TaskOrderDescription VARCHAR(MAX),
@TaskOrderNumber VARCHAR(50),
@UnrestrictedProcurementReason VARCHAR(MAX),
@UnrestrictedProcurementReasonID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormProcurementMethod
		(
		FARJustificationID,
		GSAJustificationID,
		GSAScheduleNumber,
		Percentage,
		ProcurementMethodID,
		ReviewFormID,
		TaskOrderDescription,
		TaskOrderNumber,
		UnrestrictedProcurementReason,
		UnrestrictedProcurementReasonID
		)
	VALUES
		(
		@FARJustificationID,
		@GSAJustificationID,
		@GSAScheduleNumber,
		@Percentage,
		@ProcurementMethodID,
		@ReviewFormID,
		@TaskOrderDescription,
		@TaskOrderNumber,
		@UnrestrictedProcurementReason,
		@UnrestrictedProcurementReasonID
		)

END
GO
--End procedure reviewform.SaveReviewFormProcurementMethod

--Begin procedure reviewform.SaveReviewFormResearchType
EXEC utility.DropObject 'reviewform.SaveReviewFormResearchType'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormResearchType

@ResearchTypeID INT,
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormResearchType
		(
		ResearchTypeID,
		ReviewFormID
		)
	VALUES
		(
		@ResearchTypeID,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormResearchType

--End file Build File - 03 - Procedures - ReviewForm.sql

--Begin file Build File - 04 - Data - NAICS.sql
USE SBRS_DHHS
GO

--Begin table dropdown.NAICS
TRUNCATE TABLE dropdown.NAICS
GO

SET IDENTITY_INSERT dropdown.NAICS ON;
INSERT INTO dropdown.NAICS (NAICSID) VALUES (0);
SET IDENTITY_INSERT dropdown.NAICS OFF;

INSERT INTO dropdown.NAICS 
	(NAICSCode,NAICSName)
VALUES
	('111110','Soybean Farming'),
	('111120','Oilseed (except Soybean) Farming'),
	('111130','Dry Pea and Bean Farming'),
	('111140','Wheat Farming'),
	('111150','Corn Farming'),
	('111160','Rice Farming'),
	('111191','Oilseed and Grain Combination Farming'),
	('111199','All Other Grain Farming'),
	('111211','Potato Farming'),
	('111219','Other Vegetable (except Potato) and Melon Farming'),
	('111310','Orange Groves'),
	('111320','Citrus (except Orange) Groves'),
	('111331','Apple Orchards'),
	('111332','Grape Vineyards'),
	('111333','Strawberry Farming'),
	('111334','Berry (except Strawberry) Farming'),
	('111335','Tree Nut Farming'),
	('111336','Fruit and Tree Nut Combination Farming'),
	('111339','Other Noncitrus Fruit Farming'),
	('111411','Mushroom Production'),
	('111419','Other Food Crops Grown Under Cover'),
	('111421','Nursery and Tree Production'),
	('111422','Floriculture Production'),
	('111910','Tobacco Farming'),
	('111920','Cotton Farming'),
	('111930','Sugarcane Farming'),
	('111940','Hay Farming'),
	('111991','Sugar Beet Farming'),
	('111992','Peanut Farming'),
	('111998','All Other Miscellaneous Crop Farming'),
	('112111','Beef Cattle Ranching and Farming'),
	('112112','Cattle Feedlots'),
	('112120','Dairy Cattle and Milk Production'),
	('112130','Dual-Purpose Cattle Ranching and Farming'),
	('112210','Hog and Pig Farming'),
	('112310','Chicken Egg Production'),
	('112320','Broilers and Other Meat Type Chicken Production'),
	('112330','Turkey Production'),
	('112340','Poultry Hatcheries'),
	('112390','Other Poultry Production'),
	('112410','Sheep Farming'),
	('112420','Goat Farming'),
	('112511','Finfish Farming and Fish Hatcheries'),
	('112512','Shellfish Farming'),
	('112519','Other Aquaculture'),
	('112910','Apiculture'),
	('112920','Horses and Other Equine Production'),
	('112930','Fur-Bearing Animal and Rabbit Production'),
	('112990','All Other Animal Production'),
	('113110','Timber Tract Operations'),
	('113210','Forest Nurseries and Gathering of Forest Products'),
	('113310','Logging'),
	('114111','Finfish Fishing'),
	('114112','Shellfish Fishing'),
	('114119','Other Marine Fishing'),
	('114210','Hunting and Trapping'),
	('115111','Cotton Ginning'),
	('115112','Soil Preparation, Planting, and Cultivating'),
	('115113','Crop Harvesting, Primarily by Machine'),
	('115114','Postharvest Crop Activities (except Cotton Ginning)'),
	('115115','Farm Labor Contractors and Crew Leaders'),
	('115116','Farm Management Services'),
	('115210','Support Activities for Animal Production'),
	('115310','Support Activities for Forestry'),
	('211111','Crude Petroleum and Natural Gas Extraction'),
	('211112','Natural Gas Liquid Extraction'),
	('212111','Bituminous Coal and Lignite Surface Mining'),
	('212112','Bituminous Coal Underground Mining'),
	('212113','Anthracite Mining'),
	('212210','Iron Ore Mining'),
	('212221','Gold Ore Mining'),
	('212222','Silver Ore Mining'),
	('212231','Lead Ore and Zinc Ore Mining'),
	('212234','Copper Ore and Nickel Ore Mining'),
	('212291','Uranium-Radium-Vanadium Ore Mining'),
	('212299','All Other Metal Ore Mining'),
	('212311','Dimension Stone Mining and Quarrying'),
	('212312','Crushed and Broken Limestone Mining and Quarrying'),
	('212313','Crushed and Broken Granite Mining and Quarrying'),
	('212319','Other Crushed and Broken Stone Mining and Quarrying'),
	('212321','Construction Sand and Gravel Mining'),
	('212322','Industrial Sand Mining'),
	('212324','Kaolin and Ball Clay Mining'),
	('212325','Clay and Ceramic and Refractory Minerals Mining'),
	('212391','Potash, Soda, and Borate Mineral Mining'),
	('212392','Phosphate Rock Mining'),
	('212393','Other Chemical and Fertilizer Mineral Mining'),
	('212399','All Other Nonmetallic Mineral Mining'),
	('213111','Drilling Oil and Gas Wells'),
	('213112','Support Activities for Oil and Gas Operations'),
	('213113','Support Activities for Coal Mining'),
	('213114','Support Activities for Metal Mining'),
	('213115','Support Activities for Nonmetallic Minerals (except Fuels) Mining'),
	('221111','Hydroelectric Power Generation'),
	('221112','Fossil Fuel Electric Power Generation'),
	('221113','Nuclear Electric Power Generation'),
	('221114','Solar Electric Power Generation'),
	('221115','Wind Electric Power Generation'),
	('221116','Geothermal Electric Power Generation'),
	('221117','Biomass Electric Power Generation'),
	('221118','Other Electric Power Generation'),
	('221121','Electric Bulk Power Transmission and Control'),
	('221122','Electric Power Distribution'),
	('221210','Natural Gas Distribution'),
	('221310','Water Supply and Irrigation Systems'),
	('221320','Sewage Treatment Facilities'),
	('221330','Steam and Air-Conditioning Supply'),
	('236115','New Single-Family Housing Construction (except For-Sale Builders)'),
	('236116','New Multifamily Housing Construction (except For-Sale Builders)'),
	('236117','New Housing For-Sale Builders'),
	('236118','Residential Remodelers'),
	('236210','Industrial Building Construction'),
	('236220','Commercial and Institutional Building Construction'),
	('237110','Water and Sewer Line and Related Structures Construction'),
	('237120','Oil and Gas Pipeline and Related Structures Construction'),
	('237130','Power and Communication Line and Related Structures Construction'),
	('237210','Land Subdivision'),
	('237310','Highway, Street, and Bridge Construction'),
	('237990','Other Heavy and Civil Engineering Construction'),
	('238110','Poured Concrete Foundation and Structure Contractors'),
	('238120','Structural Steel and Precast Concrete Contractors'),
	('238130','Framing Contractors'),
	('238140','Masonry Contractors'),
	('238150','Glass and Glazing Contractors'),
	('238160','Roofing Contractors'),
	('238170','Siding Contractors'),
	('238190','Other Foundation, Structure, and Building Exterior Contractors'),
	('238210','Electrical Contractors and Other Wiring Installation Contractors'),
	('238220','Plumbing, Heating, and Air-Conditioning Contractors'),
	('238290','Other Building Equipment Contractors'),
	('238310','Drywall and Insulation Contractors'),
	('238320','Painting and Wall Covering Contractors'),
	('238330','Flooring Contractors'),
	('238340','Tile and Terrazzo Contractors'),
	('238350','Finish Carpentry Contractors'),
	('238390','Other Building Finishing Contractors'),
	('238910','Site Preparation Contractors'),
	('238990','All Other Specialty Trade Contractors'),
	('311111','Dog and Cat Food Manufacturing'),
	('311119','Other Animal Food Manufacturing'),
	('311211','Flour Milling'),
	('311212','Rice Milling'),
	('311213','Malt Manufacturing'),
	('311221','Wet Corn Milling'),
	('311224','Soybean and Other Oilseed Processing'),
	('311225','Fats and Oils Refining and Blending'),
	('311230','Breakfast Cereal Manufacturing'),
	('311313','Beet Sugar Manufacturing'),
	('311314','Cane Sugar Manufacturing'),
	('311340','Nonchocolate Confectionery Manufacturing'),
	('311351','Chocolate and Confectionery Manufacturing from Cacao Beans'),
	('311352','Confectionery Manufacturing from Purchased Chocolate'),
	('311411','Frozen Fruit, Juice, and Vegetable Manufacturing'),
	('311412','Frozen Specialty Food Manufacturing'),
	('311421','Fruit and Vegetable Canning'),
	('311422','Specialty Canning'),
	('311423','Dried and Dehydrated Food Manufacturing'),
	('311511','Fluid Milk Manufacturing'),
	('311512','Creamery Butter Manufacturing'),
	('311513','Cheese Manufacturing'),
	('311514','Dry, Condensed, and Evaporated Dairy Product Manufacturing'),
	('311520','Ice Cream and Frozen Dessert Manufacturing'),
	('311611','Animal (except Poultry) Slaughtering'),
	('311612','Meat Processed from Carcasses'),
	('311613','Rendering and Meat Byproduct Processing'),
	('311615','Poultry Processing'),
	('311710','Seafood Product Preparation and Packaging'),
	('311811','Retail Bakeries'),
	('311812','Commercial Bakeries'),
	('311813','Frozen Cakes, Pies, and Other Pastries Manufacturing'),
	('311821','Cookie and Cracker Manufacturing'),
	('311824','Dry Pasta, Dough, and Flour Mixes Manufacturing from Purchased Flour'),
	('311830','Tortilla Manufacturing'),
	('311911','Roasted Nuts and Peanut Butter Manufacturing'),
	('311919','Other Snack Food Manufacturing'),
	('311920','Coffee and Tea Manufacturing'),
	('311930','Flavoring Syrup and Concentrate Manufacturing'),
	('311941','Mayonnaise, Dressing, and Other Prepared Sauce Manufacturing'),
	('311942','Spice and Extract Manufacturing'),
	('311991','Perishable Prepared Food Manufacturing'),
	('311999','All Other Miscellaneous Food Manufacturing'),
	('312111','Soft Drink Manufacturing'),
	('312112','Bottled Water Manufacturing'),
	('312113','Ice Manufacturing'),
	('312120','Breweries'),
	('312130','Wineries'),
	('312140','Distilleries'),
	('312230','Tobacco Manufacturing'),
	('313110','Fiber, Yarn, and Thread Mills'),
	('313210','Broadwoven Fabric Mills'),
	('313220','Narrow Fabric Mills and Schiffli Machine Embroidery'),
	('313230','Nonwoven Fabric Mills'),
	('313240','Knit Fabric Mills'),
	('313310','Textile and Fabric Finishing Mills'),
	('313320','Fabric Coating Mills'),
	('314110','Carpet and Rug Mills'),
	('314120','Curtain and Linen Mills'),
	('314910','Textile Bag and Canvas Mills'),
	('314994','Rope, Cordage, Twine, Tire Cord, and Tire Fabric Mills'),
	('314999','All Other Miscellaneous Textile Product Mills'),
	('315110','Hosiery and Sock Mills'),
	('315190','Other Apparel Knitting Mills'),
	('315210','Cut and Sew Apparel Contractors'),
	('315220','Menâs and Boysâ Cut and Sew Apparel Manufacturing'),
	('315240','Womenâs, Girlsâ, and Infantsâ Cut and Sew Apparel Manufacturing'),
	('315280','Other Cut and Sew Apparel Manufacturing'),
	('315990','Apparel Accessories and Other Apparel Manufacturing'),
	('316110','Leather and Hide Tanning and Finishing'),
	('316210','Footwear Manufacturing'),
	('316992','Women''s Handbag and Purse Manufacturing'),
	('316998','All Other Leather Good and Allied Product Manufacturing'),
	('321113','Sawmills'),
	('321114','Wood Preservation'),
	('321211','Hardwood Veneer and Plywood Manufacturing'),
	('321212','Softwood Veneer and Plywood Manufacturing'),
	('321213','Engineered Wood Member (except Truss) Manufacturing'),
	('321214','Truss Manufacturing'),
	('321219','Reconstituted Wood Product Manufacturing'),
	('321911','Wood Window and Door Manufacturing'),
	('321912','Cut Stock, Resawing Lumber, and Planing'),
	('321918','Other Millwork (including Flooring)'),
	('321920','Wood Container and Pallet Manufacturing'),
	('321991','Manufactured Home (Mobile Home) Manufacturing'),
	('321992','Prefabricated Wood Building Manufacturing'),
	('321999','All Other Miscellaneous Wood Product Manufacturing'),
	('322110','Pulp Mills'),
	('322121','Paper (except Newsprint) Mills'),
	('322122','Newsprint Mills'),
	('322130','Paperboard Mills'),
	('322211','Corrugated and Solid Fiber Box Manufacturing'),
	('322212','Folding Paperboard Box Manufacturing'),
	('322219','Other Paperboard Container Manufacturing'),
	('322220','Paper Bag and Coated and Treated Paper Manufacturing'),
	('322230','Stationery Product Manufacturing'),
	('322291','Sanitary Paper Product Manufacturing'),
	('322299','All Other Converted Paper Product Manufacturing'),
	('323111','Commercial Printing (except Screen and Books)'),
	('323113','Commercial Screen Printing'),
	('323117','Books Printing'),
	('323120','Support Activities for Printing'),
	('324110','Petroleum Refineries'),
	('324121','Asphalt Paving Mixture and Block Manufacturing'),
	('324122','Asphalt Shingle and Coating Materials Manufacturing'),
	('324191','Petroleum Lubricating Oil and Grease Manufacturing'),
	('324199','All Other Petroleum and Coal Products Manufacturing'),
	('325110','Petrochemical Manufacturing'),
	('325120','Industrial Gas Manufacturing'),
	('325130','Synthetic Dye and Pigment Manufacturing'),
	('325180','Other Basic Inorganic Chemical Manufacturing'),
	('325193','Ethyl Alcohol Manufacturing'),
	('325194','Cyclic Crude, Intermediate, and Gum and Wood Chemical Manufacturing'),
	('325199','All Other Basic Organic Chemical Manufacturing'),
	('325211','Plastics Material and Resin Manufacturing'),
	('325212','Synthetic Rubber Manufacturing'),
	('325220','Artificial and Synthetic Fibers and Filaments Manufacturing'),
	('325311','Nitrogenous Fertilizer Manufacturing'),
	('325312','Phosphatic Fertilizer Manufacturing'),
	('325314','Fertilizer (Mixing Only) Manufacturing'),
	('325320','Pesticide and Other Agricultural Chemical Manufacturing'),
	('325411','Medicinal and Botanical Manufacturing'),
	('325412','Pharmaceutical Preparation Manufacturing'),
	('325413','In-Vitro Diagnostic Substance Manufacturing'),
	('325414','Biological Product (except Diagnostic) Manufacturing'),
	('325510','Paint and Coating Manufacturing'),
	('325520','Adhesive Manufacturing'),
	('325611','Soap and Other Detergent Manufacturing'),
	('325612','Polish and Other Sanitation Good Manufacturing'),
	('325613','Surface Active Agent Manufacturing'),
	('325620','Toilet Preparation Manufacturing'),
	('325910','Printing Ink Manufacturing'),
	('325920','Explosives Manufacturing'),
	('325991','Custom Compounding of Purchased Resins'),
	('325992','Photographic Film, Paper, Plate, and Chemical Manufacturing'),
	('325998','All Other Miscellaneous Chemical Product and Preparation Manufacturing'),
	('326111','Plastics Bag and Pouch Manufacturing'),
	('326112','Plastics Packaging Film and Sheet (including Laminated) Manufacturing'),
	('326113','Unlaminated Plastics Film and Sheet (except Packaging) Manufacturing'),
	('326121','Unlaminated Plastics Profile Shape Manufacturing'),
	('326122','Plastics Pipe and Pipe Fitting Manufacturing'),
	('326130','Laminated Plastics Plate, Sheet (except Packaging), and Shape Manufacturing'),
	('326140','Polystyrene Foam Product Manufacturing'),
	('326150','Urethane and Other Foam Product (except Polystyrene) Manufacturing'),
	('326160','Plastics Bottle Manufacturing'),
	('326191','Plastics Plumbing Fixture Manufacturing'),
	('326199','All Other Plastics Product Manufacturing'),
	('326211','Tire Manufacturing (except Retreading)'),
	('326212','Tire Retreading'),
	('326220','Rubber and Plastics Hoses and Belting Manufacturing'),
	('326291','Rubber Product Manufacturing for Mechanical Use'),
	('326299','All Other Rubber Product Manufacturing'),
	('327110','Pottery, Ceramics, and Plumbing Fixture Manufacturing'),
	('327120','Clay Building Material and Refractories Manufacturing'),
	('327211','Flat Glass Manufacturing'),
	('327212','Other Pressed and Blown Glass and Glassware Manufacturing'),
	('327213','Glass Container Manufacturing'),
	('327215','Glass Product Manufacturing Made of Purchased Glass'),
	('327310','Cement Manufacturing'),
	('327320','Ready-Mix Concrete Manufacturing'),
	('327331','Concrete Block and Brick Manufacturing'),
	('327332','Concrete Pipe Manufacturing'),
	('327390','Other Concrete Product Manufacturing'),
	('327410','Lime Manufacturing'),
	('327420','Gypsum Product Manufacturing'),
	('327910','Abrasive Product Manufacturing'),
	('327991','Cut Stone and Stone Product Manufacturing'),
	('327992','Ground or Treated Mineral and Earth Manufacturing'),
	('327993','Mineral Wool Manufacturing'),
	('327999','All Other Miscellaneous Nonmetallic Mineral Product Manufacturing'),
	('331110','Iron and Steel Mills and Ferroalloy Manufacturing'),
	('331210','Iron and Steel Pipe and Tube Manufacturing from Purchased Steel'),
	('331221','Rolled Steel Shape Manufacturing'),
	('331222','Steel Wire Drawing'),
	('331313','Alumina Refining and Primary Aluminum Production'),
	('331314','Secondary Smelting and Alloying of Aluminum'),
	('331315','Aluminum Sheet, Plate, and Foil Manufacturing'),
	('331318','Other Aluminum Rolling, Drawing, and Extruding'),
	('331410','Nonferrous Metal (except Aluminum) Smelting and Refining'),
	('331420','Copper Rolling, Drawing, Extruding, and Alloying'),
	('331491','Nonferrous Metal (except Copper and Aluminum) Rolling, Drawing, and Extruding'),
	('331492','Secondary Smelting, Refining, and Alloying of Nonferrous Metal (except Copper and Aluminum)'),
	('331511','Iron Foundries'),
	('331512','Steel Investment Foundries'),
	('331513','Steel Foundries (except Investment)'),
	('331523','Nonferrous Metal Die-Casting Foundries'),
	('331524','Aluminum Foundries (except Die-Casting)'),
	('331529','Other Nonferrous Metal Foundries (except Die-Casting)'),
	('332111','Iron and Steel Forging'),
	('332112','Nonferrous Forging'),
	('332114','Custom Roll Forming'),
	('332117','Powder Metallurgy Part Manufacturing'),
	('332119','Metal Crown, Closure, and Other Metal Stamping (except Automotive)'),
	('332215','Metal Kitchen Cookware, Utensil, Cutlery, and Flatware (except Precious) Manufacturing'),
	('332216','Saw Blade and Handtool Manufacturing'),
	('332311','Prefabricated Metal Building and Component Manufacturing'),
	('332312','Fabricated Structural Metal Manufacturing'),
	('332313','Plate Work Manufacturing'),
	('332321','Metal Window and Door Manufacturing'),
	('332322','Sheet Metal Work Manufacturing'),
	('332323','Ornamental and Architectural Metal Work Manufacturing'),
	('332410','Power Boiler and Heat Exchanger Manufacturing'),
	('332420','Metal Tank (Heavy Gauge) Manufacturing'),
	('332431','Metal Can Manufacturing'),
	('332439','Other Metal Container Manufacturing'),
	('332510','Hardware Manufacturing'),
	('332613','Spring Manufacturing'),
	('332618','Other Fabricated Wire Product Manufacturing'),
	('332710','Machine Shops'),
	('332721','Precision Turned Product Manufacturing'),
	('332722','Bolt, Nut, Screw, Rivet, and Washer Manufacturing'),
	('332811','Metal Heat Treating'),
	('332812','Metal Coating, Engraving (except Jewelry and Silverware), and Allied Services to Manufacturers'),
	('332813','Electroplating, Plating, Polishing, Anodizing, and Coloring'),
	('332911','Industrial Valve Manufacturing'),
	('332912','Fluid Power Valve and Hose Fitting Manufacturing'),
	('332913','Plumbing Fixture Fitting and Trim Manufacturing'),
	('332919','Other Metal Valve and Pipe Fitting Manufacturing'),
	('332991','Ball and Roller Bearing Manufacturing'),
	('332992','Small Arms Ammunition Manufacturing'),
	('332993','Ammunition (except Small Arms) Manufacturing'),
	('332994','Small Arms, Ordnance, and Ordnance Accessories Manufacturing'),
	('332996','Fabricated Pipe and Pipe Fitting Manufacturing'),
	('332999','All Other Miscellaneous Fabricated Metal Product Manufacturing'),
	('333111','Farm Machinery and Equipment Manufacturing'),
	('333112','Lawn and Garden Tractor and Home Lawn and Garden Equipment Manufacturing'),
	('333120','Construction Machinery Manufacturing'),
	('333131','Mining Machinery and Equipment Manufacturing'),
	('333132','Oil and Gas Field Machinery and Equipment Manufacturing'),
	('333241','Food Product Machinery Manufacturing'),
	('333242','Semiconductor Machinery Manufacturing'),
	('333243','Sawmill, Woodworking, and Paper Machinery Manufacturing'),
	('333244','Printing Machinery and Equipment Manufacturing'),
	('333249','Other Industrial Machinery Manufacturing'),
	('333314','Optical Instrument and Lens Manufacturing'),
	('333316','Photographic and Photocopying Equipment Manufacturing'),
	('333318','Other Commercial and Service Industry Machinery Manufacturing'),
	('333413','Industrial and Commercial Fan and Blower and Air Purification Equipment Manufacturing'),
	('333414','Heating Equipment (except Warm Air Furnaces) Manufacturing'),
	('333415','Air-Conditioning and Warm Air Heating Equipment and Commercial and Industrial Refrigeration Equipment Manufacturing'),
	('333511','Industrial Mold Manufacturing'),
	('333514','Special Die and Tool, Die Set, Jig, and Fixture Manufacturing'),
	('333515','Cutting Tool and Machine Tool Accessory Manufacturing'),
	('333517','Machine Tool Manufacturing'),
	('333519','Rolling Mill and Other Metalworking Machinery Manufacturing'),
	('333611','Turbine and Turbine Generator Set Units Manufacturing'),
	('333612','Speed Changer, Industrial High-Speed Drive, and Gear Manufacturing'),
	('333613','Mechanical Power Transmission Equipment Manufacturing'),
	('333618','Other Engine Equipment Manufacturing'),
	('333911','Pump and Pumping Equipment Manufacturing'),
	('333912','Air and Gas Compressor Manufacturing'),
	('333913','Measuring and Dispensing Pump Manufacturing'),
	('333921','Elevator and Moving Stairway Manufacturing'),
	('333922','Conveyor and Conveying Equipment Manufacturing'),
	('333923','Overhead Traveling Crane, Hoist, and Monorail System Manufacturing'),
	('333924','Industrial Truck, Tractor, Trailer, and Stacker Machinery Manufacturing'),
	('333991','Power-Driven Handtool Manufacturing'),
	('333992','Welding and Soldering Equipment Manufacturing'),
	('333993','Packaging Machinery Manufacturing'),
	('333994','Industrial Process Furnace and Oven Manufacturing'),
	('333995','Fluid Power Cylinder and Actuator Manufacturing'),
	('333996','Fluid Power Pump and Motor Manufacturing'),
	('333997','Scale and Balance Manufacturing'),
	('333999','All Other Miscellaneous General Purpose Machinery Manufacturing'),
	('334111','Electronic Computer Manufacturing'),
	('334112','Computer Storage Device Manufacturing'),
	('334118','Computer Terminal and Other Computer Peripheral Equipment Manufacturing'),
	('334210','Telephone Apparatus Manufacturing'),
	('334220','Radio and Television Broadcasting and Wireless Communications Equipment Manufacturing'),
	('334290','Other Communications Equipment Manufacturing'),
	('334310','Audio and Video Equipment Manufacturing'),
	('334412','Bare Printed Circuit Board Manufacturing'),
	('334413','Semiconductor and Related Device Manufacturing'),
	('334416','Capacitor, Resistor, Coil, Transformer, and Other Inductor Manufacturing'),
	('334417','Electronic Connector Manufacturing'),
	('334418','Printed Circuit Assembly (Electronic Assembly) Manufacturing'),
	('334419','Other Electronic Component Manufacturing'),
	('334510','Electromedical and Electrotherapeutic Apparatus Manufacturing'),
	('334511','Search, Detection, Navigation, Guidance, Aeronautical, and Nautical System and Instrument Manufacturing'),
	('334512','Automatic Environmental Control Manufacturing for Residential, Commercial, and Appliance Use'),
	('334513','Instruments and Related Products Manufacturing for Measuring, Displaying, and Controlling Industrial Process Variables'),
	('334514','Totalizing Fluid Meter and Counting Device Manufacturing'),
	('334515','Instrument Manufacturing for Measuring and Testing Electricity and Electrical Signals'),
	('334516','Analytical Laboratory Instrument Manufacturing'),
	('334517','Irradiation Apparatus Manufacturing'),
	('334519','Other Measuring and Controlling Device Manufacturing'),
	('334613','Blank Magnetic and Optical Recording Media Manufacturing'),
	('334614','Software and Other Prerecorded Compact Disc, Tape, and Record Reproducing'),
	('335110','Electric Lamp Bulb and Part Manufacturing'),
	('335121','Residential Electric Lighting Fixture Manufacturing'),
	('335122','Commercial, Industrial, and Institutional Electric Lighting Fixture Manufacturing'),
	('335129','Other Lighting Equipment Manufacturing'),
	('335210','Small Electrical Appliance Manufacturing'),
	('335221','Household Cooking Appliance Manufacturing'),
	('335222','Household Refrigerator and Home Freezer Manufacturing'),
	('335224','Household Laundry Equipment Manufacturing'),
	('335228','Other Major Household Appliance Manufacturing'),
	('335311','Power, Distribution, and Specialty Transformer Manufacturing'),
	('335312','Motor and Generator Manufacturing'),
	('335313','Switchgear and Switchboard Apparatus Manufacturing'),
	('335314','Relay and Industrial Control Manufacturing'),
	('335911','Storage Battery Manufacturing'),
	('335912','Primary Battery Manufacturing'),
	('335921','Fiber Optic Cable Manufacturing'),
	('335929','Other Communication and Energy Wire Manufacturing'),
	('335931','Current-Carrying Wiring Device Manufacturing'),
	('335932','Noncurrent-Carrying Wiring Device Manufacturing'),
	('335991','Carbon and Graphite Product Manufacturing'),
	('335999','All Other Miscellaneous Electrical Equipment and Component Manufacturing'),
	('336111','Automobile Manufacturing'),
	('336112','Light Truck and Utility Vehicle Manufacturing'),
	('336120','Heavy Duty Truck Manufacturing'),
	('336211','Motor Vehicle Body Manufacturing'),
	('336212','Truck Trailer Manufacturing'),
	('336213','Motor Home Manufacturing'),
	('336214','Travel Trailer and Camper Manufacturing'),
	('336310','Motor Vehicle Gasoline Engine and Engine Parts Manufacturing'),
	('336320','Motor Vehicle Electrical and Electronic Equipment Manufacturing'),
	('336330','Motor Vehicle Steering and Suspension Components (except Spring) Manufacturing'),
	('336340','Motor Vehicle Brake System Manufacturing'),
	('336350','Motor Vehicle Transmission and Power Train Parts Manufacturing'),
	('336360','Motor Vehicle Seating and Interior Trim Manufacturing'),
	('336370','Motor Vehicle Metal Stamping'),
	('336390','Other Motor Vehicle Parts Manufacturing'),
	('336411','Aircraft Manufacturing'),
	('336412','Aircraft Engine and Engine Parts Manufacturing'),
	('336413','Other Aircraft Parts and Auxiliary Equipment Manufacturing'),
	('336414','Guided Missile and Space Vehicle Manufacturing'),
	('336415','Guided Missile and Space Vehicle Propulsion Unit and Propulsion Unit Parts Manufacturing'),
	('336419','Other Guided Missile and Space Vehicle Parts and Auxiliary Equipment Manufacturing'),
	('336510','Railroad Rolling Stock Manufacturing'),
	('336611','Ship Building and Repairing'),
	('336612','Boat Building'),
	('336991','Motorcycle, Bicycle, and Parts Manufacturing'),
	('336992','Military Armored Vehicle, Tank, and Tank Component Manufacturing'),
	('336999','All Other Transportation Equipment Manufacturing'),
	('337110','Wood Kitchen Cabinet and Countertop Manufacturing'),
	('337121','Upholstered Household Furniture Manufacturing'),
	('337122','Nonupholstered Wood Household Furniture Manufacturing'),
	('337124','Metal Household Furniture Manufacturing'),
	('337125','Household Furniture (except Wood and Metal) Manufacturing'),
	('337127','Institutional Furniture Manufacturing'),
	('337211','Wood Office Furniture Manufacturing'),
	('337212','Custom Architectural Woodwork and Millwork Manufacturing'),
	('337214','Office Furniture (except Wood) Manufacturing'),
	('337215','Showcase, Partition, Shelving, and Locker Manufacturing'),
	('337910','Mattress Manufacturing'),
	('337920','Blind and Shade Manufacturing'),
	('339112','Surgical and Medical Instrument Manufacturing'),
	('339113','Surgical Appliance and Supplies Manufacturing'),
	('339114','Dental Equipment and Supplies Manufacturing'),
	('339115','Ophthalmic Goods Manufacturing'),
	('339116','Dental Laboratories'),
	('339910','Jewelry and Silverware Manufacturing'),
	('339920','Sporting and Athletic Goods Manufacturing'),
	('339930','Doll, Toy, and Game Manufacturing'),
	('339940','Office Supplies (except Paper) Manufacturing'),
	('339950','Sign Manufacturing'),
	('339991','Gasket, Packing, and Sealing Device Manufacturing'),
	('339992','Musical Instrument Manufacturing'),
	('339993','Fastener, Button, Needle, and Pin Manufacturing'),
	('339994','Broom, Brush, and Mop Manufacturing')
GO

INSERT INTO dropdown.NAICS 
	(NAICSCode,NAICSName) 
VALUES
	('339995','Burial Casket Manufacturing'),
	('339999','All Other Miscellaneous Manufacturing'),
	('423110','Automobile and Other Motor Vehicle Merchant Wholesalers'),
	('423120','Motor Vehicle Supplies and New Parts Merchant Wholesalers'),
	('423130','Tire and Tube Merchant Wholesalers'),
	('423140','Motor Vehicle Parts (Used) Merchant Wholesalers'),
	('423210','Furniture Merchant Wholesalers'),
	('423220','Home Furnishing Merchant Wholesalers'),
	('423310','Lumber, Plywood, Millwork, and Wood Panel Merchant Wholesalers'),
	('423320','Brick, Stone, and Related Construction Material Merchant Wholesalers'),
	('423330','Roofing, Siding, and Insulation Material Merchant Wholesalers'),
	('423390','Other Construction Material Merchant Wholesalers'),
	('423410','Photographic Equipment and Supplies Merchant Wholesalers'),
	('423420','Office Equipment Merchant Wholesalers'),
	('423430','Computer and Computer Peripheral Equipment and Software Merchant Wholesalers'),
	('423440','Other Commercial Equipment Merchant Wholesalers'),
	('423450','Medical, Dental, and Hospital Equipment and Supplies Merchant Wholesalers'),
	('423460','Ophthalmic Goods Merchant Wholesalers'),
	('423490','Other Professional Equipment and Supplies Merchant Wholesalers'),
	('423510','Metal Service Centers and Other Metal Merchant Wholesalers'),
	('423520','Coal and Other Mineral and Ore Merchant Wholesalers'),
	('423610','Electrical Apparatus and Equipment, Wiring Supplies, and Related Equipment Merchant Wholesalers'),
	('423620','Household Appliances, Electric Housewares, and Consumer Electronics Merchant Wholesalers'),
	('423690','Other Electronic Parts and Equipment Merchant Wholesalers'),
	('423710','Hardware Merchant Wholesalers'),
	('423720','Plumbing and Heating Equipment and Supplies (Hydronics) Merchant Wholesalers'),
	('423730','Warm Air Heating and Air-Conditioning Equipment and Supplies Merchant Wholesalers'),
	('423740','Refrigeration Equipment and Supplies Merchant Wholesalers'),
	('423810','Construction and Mining (except Oil Well) Machinery and Equipment Merchant Wholesalers'),
	('423820','Farm and Garden Machinery and Equipment Merchant Wholesalers'),
	('423830','Industrial Machinery and Equipment Merchant Wholesalers'),
	('423840','Industrial Supplies Merchant Wholesalers'),
	('423850','Service Establishment Equipment and Supplies Merchant Wholesalers'),
	('423860','Transportation Equipment and Supplies (except Motor Vehicle) Merchant Wholesalers'),
	('423910','Sporting and Recreational Goods and Supplies Merchant Wholesalers'),
	('423920','Toy and Hobby Goods and Supplies Merchant Wholesalers'),
	('423930','Recyclable Material Merchant Wholesalers'),
	('423940','Jewelry, Watch, Precious Stone, and Precious Metal Merchant Wholesalers'),
	('423990','Other Miscellaneous Durable Goods Merchant Wholesalers'),
	('424110','Printing and Writing Paper Merchant Wholesalers'),
	('424120','Stationery and Office Supplies Merchant Wholesalers'),
	('424130','Industrial and Personal Service Paper Merchant Wholesalers'),
	('424210','Drugs and Druggists'' Sundries Merchant Wholesalers'),
	('424310','Piece Goods, Notions, and Other Dry Goods Merchant Wholesalers'),
	('424320','Men''s and Boy''s Clothing and Furnishings Merchant Wholesalers'),
	('424330','Women''s, Children''s, and Infant''s Clothing and Accessories Merchant Wholesalers'),
	('424340','Footwear Merchant Wholesalers'),
	('424410','General Line Grocery Merchant Wholesalers'),
	('424420','Packaged Frozen Food Merchant Wholesalers'),
	('424430','Dairy Product (except Dried or Canned) Merchant Wholesalers'),
	('424440','Poultry and Poultry Product Merchant Wholesalers'),
	('424450','Confectionery Merchant Wholesalers'),
	('424460','Fish and Seafood Merchant Wholesalers'),
	('424470','Meat and Meat Product Merchant Wholesalers'),
	('424480','Fresh Fruit and Vegetable Merchant Wholesalers'),
	('424490','Other Grocery and Related Products Merchant Wholesalers'),
	('424510','Grain and Field Bean Merchant Wholesalers'),
	('424520','Livestock Merchant Wholesalers'),
	('424590','Other Farm Product Raw Material Merchant Wholesalers'),
	('424610','Plastics Materials and Basic Forms and Shapes Merchant Wholesalers'),
	('424690','Other Chemical and Allied Products Merchant Wholesalers'),
	('424710','Petroleum Bulk Stations and Terminals'),
	('424720','Petroleum and Petroleum Products Merchant Wholesalers (except Bulk Stations and Terminals)'),
	('424810','Beer and Ale Merchant Wholesalers'),
	('424820','Wine and Distilled Alcoholic Beverage Merchant Wholesalers'),
	('424910','Farm Supplies Merchant Wholesalers'),
	('424920','Book, Periodical, and Newspaper Merchant Wholesalers'),
	('424930','Flower, Nursery Stock, and Florists'' Supplies Merchant Wholesalers'),
	('424940','Tobacco and Tobacco Product Merchant Wholesalers'),
	('424950','Paint, Varnish, and Supplies Merchant Wholesalers'),
	('424990','Other Miscellaneous Nondurable Goods Merchant Wholesalers'),
	('425110','Business to Business Electronic Markets'),
	('425120','Wholesale Trade Agents and Brokers'),
	('441110','New Car Dealers'),
	('441120','Used Car Dealers'),
	('441210','Recreational Vehicle Dealers'),
	('441222','Boat Dealers'),
	('441228','Motorcycle, ATV, and All Other Motor Vehicle Dealers'),
	('441310','Automotive Parts and Accessories Stores'),
	('441320','Tire Dealers'),
	('442110','Furniture Stores'),
	('442210','Floor Covering Stores'),
	('442291','Window Treatment Stores'),
	('442299','All Other Home Furnishings Stores'),
	('443141','Household Appliance Stores'),
	('443142','Electronics Stores'),
	('444110','Home Centers'),
	('444120','Paint and Wallpaper Stores'),
	('444130','Hardware Stores'),
	('444190','Other Building Material Dealers'),
	('444210','Outdoor Power Equipment Stores'),
	('444220','Nursery, Garden Center, and Farm Supply Stores'),
	('445110','Supermarkets and Other Grocery (except Convenience) Stores'),
	('445120','Convenience Stores'),
	('445210','Meat Markets'),
	('445220','Fish and Seafood Markets'),
	('445230','Fruit and Vegetable Markets'),
	('445291','Baked Goods Stores'),
	('445292','Confectionery and Nut Stores'),
	('445299','All Other Specialty Food Stores'),
	('445310','Beer, Wine, and Liquor Stores'),
	('446110','Pharmacies and Drug Stores'),
	('446120','Cosmetics, Beauty Supplies, and Perfume Stores'),
	('446130','Optical Goods Stores'),
	('446191','Food (Health) Supplement Stores'),
	('446199','All Other Health and Personal Care Stores'),
	('447110','Gasoline Stations with Convenience Stores'),
	('447190','Other Gasoline Stations'),
	('448110','Men''s Clothing Stores'),
	('448120','Women''s Clothing Stores'),
	('448130','Children''s and Infant''s Clothing Stores'),
	('448140','Family Clothing Stores'),
	('448150','Clothing Accessories Stores'),
	('448190','Other Clothing Stores'),
	('448210','Shoe Stores'),
	('448310','Jewelry Stores'),
	('448320','Luggage and Leather Goods Stores'),
	('451110','Sporting Goods Stores'),
	('451120','Hobby, Toy, and Game Stores'),
	('451130','Sewing, Needlework, and Piece Goods Stores'),
	('451140','Musical Instrument and Supplies Stores'),
	('451211','Book Stores'),
	('451212','News Dealers and Newsstands'),
	('452111','Department Stores (except Discount Department Stores)'),
	('452112','Discount Department Stores'),
	('452910','Warehouse Clubs and Supercenters'),
	('452990','All Other General Merchandise Stores'),
	('453110','Florists'),
	('453210','Office Supplies and Stationery Stores'),
	('453220','Gift, Novelty, and Souvenir Stores'),
	('453310','Used Merchandise Stores'),
	('453910','Pet and Pet Supplies Stores'),
	('453920','Art Dealers'),
	('453930','Manufactured (Mobile) Home Dealers'),
	('453991','Tobacco Stores'),
	('453998','All Other Miscellaneous Store Retailers (except Tobacco Stores)'),
	('454111','Electronic Shopping'),
	('454112','Electronic Auctions'),
	('454113','Mail-Order Houses'),
	('454210','Vending Machine Operators'),
	('454310','Fuel Dealers'),
	('454390','Other Direct Selling Establishments'),
	('481111','Scheduled Passenger Air Transportation'),
	('481112','Scheduled Freight Air Transportation'),
	('481211','Nonscheduled Chartered Passenger Air Transportation'),
	('481212','Nonscheduled Chartered Freight Air Transportation'),
	('481219','Other Nonscheduled Air Transportation'),
	('482111','Line-Haul Railroads'),
	('482112','Short Line Railroads'),
	('483111','Deep Sea Freight Transportation'),
	('483112','Deep Sea Passenger Transportation'),
	('483113','Coastal and Great Lakes Freight Transportation'),
	('483114','Coastal and Great Lakes Passenger Transportation'),
	('483211','Inland Water Freight Transportation'),
	('483212','Inland Water Passenger Transportation'),
	('484110','General Freight Trucking, Local'),
	('484121','General Freight Trucking, Long-Distance, Truckload'),
	('484122','General Freight Trucking, Long-Distance, Less Than Truckload'),
	('484210','Used Household and Office Goods Moving'),
	('484220','Specialized Freight (except Used Goods) Trucking, Local'),
	('484230','Specialized Freight (except Used Goods) Trucking, Long-Distance'),
	('485111','Mixed Mode Transit Systems'),
	('485112','Commuter Rail Systems'),
	('485113','Bus and Other Motor Vehicle Transit Systems'),
	('485119','Other Urban Transit Systems'),
	('485210','Interurban and Rural Bus Transportation'),
	('485310','Taxi Service'),
	('485320','Limousine Service'),
	('485410','School and Employee Bus Transportation'),
	('485510','Charter Bus Industry'),
	('485991','Special Needs Transportation'),
	('485999','All Other Transit and Ground Passenger Transportation'),
	('486110','Pipeline Transportation of Crude Oil'),
	('486210','Pipeline Transportation of Natural Gas'),
	('486910','Pipeline Transportation of Refined Petroleum Products'),
	('486990','All Other Pipeline Transportation'),
	('487110','Scenic and Sightseeing Transportation, Land'),
	('487210','Scenic and Sightseeing Transportation, Water'),
	('487990','Scenic and Sightseeing Transportation, Other'),
	('488111','Air Traffic Control'),
	('488119','Other Airport Operations'),
	('488190','Other Support Activities for Air Transportation'),
	('488210','Support Activities for Rail Transportation'),
	('488310','Port and Harbor Operations'),
	('488320','Marine Cargo Handling'),
	('488330','Navigational Services to Shipping'),
	('488390','Other Support Activities for Water Transportation'),
	('488410','Motor Vehicle Towing'),
	('488490','Other Support Activities for Road Transportation'),
	('488510','Freight Transportation Arrangement'),
	('488991','Packing and Crating'),
	('488999','All Other Support Activities for Transportation'),
	('491110','Postal Service'),
	('492110','Couriers and Express Delivery Services'),
	('492210','Local Messengers and Local Delivery'),
	('493110','General Warehousing and Storage'),
	('493120','Refrigerated Warehousing and Storage'),
	('493130','Farm Product Warehousing and Storage'),
	('493190','Other Warehousing and Storage'),
	('511110','Newspaper Publishers'),
	('511120','Periodical Publishers'),
	('511130','Book Publishers'),
	('511140','Directory and Mailing List Publishers'),
	('511191','Greeting Card Publishers'),
	('511199','All Other Publishers'),
	('511210','Software Publishers'),
	('512110','Motion Picture and Video Production'),
	('512120','Motion Picture and Video Distribution'),
	('512131','Motion Picture Theaters (except Drive-Ins)'),
	('512132','Drive-In Motion Picture Theaters'),
	('512191','Teleproduction and Other Postproduction Services'),
	('512199','Other Motion Picture and Video Industries'),
	('512210','Record Production'),
	('512220','Integrated Record Production/Distribution'),
	('512230','Music Publishers'),
	('512240','Sound Recording Studios'),
	('512290','Other Sound Recording Industries'),
	('515111','Radio Networks'),
	('515112','Radio Stations'),
	('515120','Television Broadcasting'),
	('515210','Cable and Other Subscription Programming'),
	('517110','Wired Telecommunications Carriers'),
	('517210','Wireless Telecommunications Carriers (except Satellite)'),
	('517410','Satellite Telecommunications'),
	('517911','Telecommunications Resellers'),
	('517919','All Other Telecommunications'),
	('518210','Data Processing, Hosting, and Related Services'),
	('519110','News Syndicates'),
	('519120','Libraries and Archives'),
	('519130','Internet Publishing and Broadcasting and Web Search Portals'),
	('519190','All Other Information Services'),
	('521110','Monetary Authorities-Central Bank'),
	('522110','Commercial Banking'),
	('522120','Savings Institutions'),
	('522130','Credit Unions'),
	('522190','Other Depository Credit Intermediation'),
	('522210','Credit Card Issuing'),
	('522220','Sales Financing'),
	('522291','Consumer Lending'),
	('522292','Real Estate Credit'),
	('522293','International Trade Financing'),
	('522294','Secondary Market Financing'),
	('522298','All Other Nondepository Credit Intermediation'),
	('522310','Mortgage and Nonmortgage Loan Brokers'),
	('522320','Financial Transactions Processing, Reserve, and Clearinghouse Activities'),
	('522390','Other Activities Related to Credit Intermediation'),
	('523110','Investment Banking and Securities Dealing'),
	('523120','Securities Brokerage'),
	('523130','Commodity Contracts Dealing'),
	('523140','Commodity Contracts Brokerage'),
	('523210','Securities and Commodity Exchanges'),
	('523910','Miscellaneous Intermediation'),
	('523920','Portfolio Management'),
	('523930','Investment Advice'),
	('523991','Trust, Fiduciary, and Custody Activities'),
	('523999','Miscellaneous Financial Investment Activities'),
	('524113','Direct Life Insurance Carriers'),
	('524114','Direct Health and Medical Insurance Carriers'),
	('524126','Direct Property and Casualty Insurance Carriers'),
	('524127','Direct Title Insurance Carriers'),
	('524128','Other Direct Insurance (except Life, Health, and Medical) Carriers'),
	('524130','Reinsurance Carriers'),
	('524210','Insurance Agencies and Brokerages'),
	('524291','Claims Adjusting'),
	('524292','Third Party Administration of Insurance and Pension Funds'),
	('524298','All Other Insurance Related Activities'),
	('525110','Pension Funds'),
	('525120','Health and Welfare Funds'),
	('525190','Other Insurance Funds'),
	('525910','Open-End Investment Funds'),
	('525920','Trusts, Estates, and Agency Accounts'),
	('525990','Other Financial Vehicles'),
	('531110','Lessors of Residential Buildings and Dwellings'),
	('531120','Lessors of Nonresidential Buildings (except Miniwarehouses)'),
	('531130','Lessors of Miniwarehouses and Self-Storage Units'),
	('531190','Lessors of Other Real Estate Property'),
	('531210','Offices of Real Estate Agents and Brokers'),
	('531311','Residential Property Managers'),
	('531312','Nonresidential Property Managers'),
	('531320','Offices of Real Estate Appraisers'),
	('531390','Other Activities Related to Real Estate'),
	('532111','Passenger Car Rental'),
	('532112','Passenger Car Leasing'),
	('532120','Truck, Utility Trailer, and RV (Recreational Vehicle) Rental and Leasing'),
	('532210','Consumer Electronics and Appliances Rental'),
	('532220','Formal Wear and Costume Rental'),
	('532230','Video Tape and Disc Rental'),
	('532291','Home Health Equipment Rental'),
	('532292','Recreational Goods Rental'),
	('532299','All Other Consumer Goods Rental'),
	('532310','General Rental Centers'),
	('532411','Commercial Air, Rail, and Water Transportation Equipment Rental and Leasing'),
	('532412','Construction, Mining, and Forestry Machinery and Equipment Rental and Leasing'),
	('532420','Office Machinery and Equipment Rental and Leasing'),
	('532490','Other Commercial and Industrial Machinery and Equipment Rental and Leasing'),
	('533110','Lessors of Nonfinancial Intangible Assets (except Copyrighted Works)'),
	('541110','Offices of Lawyers'),
	('541120','Offices of Notaries'),
	('541191','Title Abstract and Settlement Offices'),
	('541199','All Other Legal Services'),
	('541211','Offices of Certified Public Accountants'),
	('541213','Tax Preparation Services'),
	('541214','Payroll Services'),
	('541219','Other Accounting Services'),
	('541310','Architectural Services'),
	('541320','Landscape Architectural Services'),
	('541330','Engineering Services'),
	('541340','Drafting Services'),
	('541350','Building Inspection Services'),
	('541360','Geophysical Surveying and Mapping Services'),
	('541370','Surveying and Mapping (except Geophysical) Services'),
	('541380','Testing Laboratories'),
	('541410','Interior Design Services'),
	('541420','Industrial Design Services'),
	('541430','Graphic Design Services'),
	('541490','Other Specialized Design Services'),
	('541511','Custom Computer Programming Services'),
	('541512','Computer Systems Design Services'),
	('541513','Computer Facilities Management Services'),
	('541519','Other Computer Related Services'),
	('541611','Administrative Management and General Management Consulting Services'),
	('541612','Human Resources Consulting Services'),
	('541613','Marketing Consulting Services'),
	('541614','Process, Physical Distribution, and Logistics Consulting Services'),
	('541618','Other Management Consulting Services'),
	('541620','Environmental Consulting Services'),
	('541690','Other Scientific and Technical Consulting Services'),
	('541711','Research and Development in Biotechnology'),
	('541712','Research and Development in the Physical, Engineering, and Life Sciences (except Biotechnology)'),
	('541720','Research and Development in the Social Sciences and Humanities'),
	('541810','Advertising Agencies'),
	('541820','Public Relations Agencies'),
	('541830','Media Buying Agencies'),
	('541840','Media Representatives'),
	('541850','Outdoor Advertising'),
	('541860','Direct Mail Advertising'),
	('541870','Advertising Material Distribution Services'),
	('541890','Other Services Related to Advertising'),
	('541910','Marketing Research and Public Opinion Polling'),
	('541921','Photography Studios, Portrait'),
	('541922','Commercial Photography'),
	('541930','Translation and Interpretation Services'),
	('541940','Veterinary Services'),
	('541990','All Other Professional, Scientific, and Technical Services'),
	('551111','Offices of Bank Holding Companies'),
	('551112','Offices of Other Holding Companies'),
	('551114','Corporate, Subsidiary, and Regional Managing Offices'),
	('561110','Office Administrative Services'),
	('561210','Facilities Support Services'),
	('561311','Employment Placement Agencies'),
	('561312','Executive Search Services'),
	('561320','Temporary Help Services'),
	('561330','Professional Employer Organizations'),
	('561410','Document Preparation Services'),
	('561421','Telephone Answering Services'),
	('561422','Telemarketing Bureaus and Other Contact Centers'),
	('561431','Private Mail Centers'),
	('561439','Other Business Service Centers (including Copy Shops)'),
	('561440','Collection Agencies'),
	('561450','Credit Bureaus'),
	('561491','Repossession Services'),
	('561492','Court Reporting and Stenotype Services'),
	('561499','All Other Business Support Services'),
	('561510','Travel Agencies'),
	('561520','Tour Operators'),
	('561591','Convention and Visitors Bureaus'),
	('561599','All Other Travel Arrangement and Reservation Services'),
	('561611','Investigation Services'),
	('561612','Security Guards and Patrol Services'),
	('561613','Armored Car Services'),
	('561621','Security Systems Services (except Locksmiths)'),
	('561622','Locksmiths'),
	('561710','Exterminating and Pest Control Services'),
	('561720','Janitorial Services'),
	('561730','Landscaping Services'),
	('561740','Carpet and Upholstery Cleaning Services'),
	('561790','Other Services to Buildings and Dwellings'),
	('561910','Packaging and Labeling Services'),
	('561920','Convention and Trade Show Organizers'),
	('561990','All Other Support Services'),
	('562111','Solid Waste Collection'),
	('562112','Hazardous Waste Collection'),
	('562119','Other Waste Collection'),
	('562211','Hazardous Waste Treatment and Disposal'),
	('562212','Solid Waste Landfill'),
	('562213','Solid Waste Combustors and Incinerators'),
	('562219','Other Nonhazardous Waste Treatment and Disposal'),
	('562910','Remediation Services'),
	('562920','Materials Recovery Facilities'),
	('562991','Septic Tank and Related Services'),
	('562998','All Other Miscellaneous Waste Management Services'),
	('611110','Elementary and Secondary Schools'),
	('611210','Junior Colleges'),
	('611310','Colleges, Universities, and Professional Schools'),
	('611410','Business and Secretarial Schools'),
	('611420','Computer Training'),
	('611430','Professional and Management Development Training'),
	('611511','Cosmetology and Barber Schools'),
	('611512','Flight Training'),
	('611513','Apprenticeship Training'),
	('611519','Other Technical and Trade Schools'),
	('611610','Fine Arts Schools'),
	('611620','Sports and Recreation Instruction'),
	('611630','Language Schools'),
	('611691','Exam Preparation and Tutoring'),
	('611692','Automobile Driving Schools'),
	('611699','All Other Miscellaneous Schools and Instruction'),
	('611710','Educational Support Services'),
	('621111','Offices of Physicians (except Mental Health Specialists)'),
	('621112','Offices of Physicians, Mental Health Specialists'),
	('621210','Offices of Dentists'),
	('621310','Offices of Chiropractors'),
	('621320','Offices of Optometrists'),
	('621330','Offices of Mental Health Practitioners (except Physicians)'),
	('621340','Offices of Physical, Occupational and Speech Therapists, and Audiologists'),
	('621391','Offices of Podiatrists'),
	('621399','Offices of All Other Miscellaneous Health Practitioners'),
	('621410','Family Planning Centers'),
	('621420','Outpatient Mental Health and Substance Abuse Centers'),
	('621491','HMO Medical Centers'),
	('621492','Kidney Dialysis Centers'),
	('621493','Freestanding Ambulatory Surgical and Emergency Centers'),
	('621498','All Other Outpatient Care Centers'),
	('621511','Medical Laboratories'),
	('621512','Diagnostic Imaging Centers'),
	('621610','Home Health Care Services'),
	('621910','Ambulance Services'),
	('621991','Blood and Organ Banks'),
	('621999','All Other Miscellaneous Ambulatory Health Care Services'),
	('622110','General Medical and Surgical Hospitals'),
	('622210','Psychiatric and Substance Abuse Hospitals'),
	('622310','Specialty (except Psychiatric and Substance Abuse) Hospitals'),
	('623110','Nursing Care Facilities (Skilled Nursing Facilities)'),
	('623210','Residential Intellectual and Developmental Disability Facilities'),
	('623220','Residential Mental Health and Substance Abuse Facilities'),
	('623311','Continuing Care Retirement Communities'),
	('623312','Assisted Living Facilities for the Elderly'),
	('623990','Other Residential Care Facilities'),
	('624110','Child and Youth Services'),
	('624120','Services for the Elderly and Persons with Disabilities'),
	('624190','Other Individual and Family Services'),
	('624210','Community Food Services'),
	('624221','Temporary Shelters'),
	('624229','Other Community Housing Services'),
	('624230','Emergency and Other Relief Services'),
	('624310','Vocational Rehabilitation Services'),
	('624410','Child Day Care Services'),
	('711110','Theater Companies and Dinner Theaters'),
	('711120','Dance Companies'),
	('711130','Musical Groups and Artists'),
	('711190','Other Performing Arts Companies'),
	('711211','Sports Teams and Clubs'),
	('711212','Racetracks'),
	('711219','Other Spectator Sports'),
	('711310','Promoters of Performing Arts, Sports, and Similar Events with Facilities'),
	('711320','Promoters of Performing Arts, Sports, and Similar Events without Facilities'),
	('711410','Agents and Managers for Artists, Athletes, Entertainers, and Other Public Figures'),
	('711510','Independent Artists, Writers, and Performers'),
	('712110','Museums'),
	('712120','Historical Sites'),
	('712130','Zoos and Botanical Gardens'),
	('712190','Nature Parks and Other Similar Institutions'),
	('713110','Amusement and Theme Parks'),
	('713120','Amusement Arcades'),
	('713210','Casinos (except Casino Hotels)'),
	('713290','Other Gambling Industries'),
	('713910','Golf Courses and Country Clubs'),
	('713920','Skiing Facilities'),
	('713930','Marinas'),
	('713940','Fitness and Recreational Sports Centers'),
	('713950','Bowling Centers'),
	('713990','All Other Amusement and Recreation Industries'),
	('721110','Hotels (except Casino Hotels) and Motels'),
	('721120','Casino Hotels'),
	('721191','Bed-and-Breakfast Inns'),
	('721199','All Other Traveler Accommodation'),
	('721211','RV (Recreational Vehicle) Parks and Campgrounds'),
	('721214','Recreational and Vacation Camps (except Campgrounds)'),
	('721310','Rooming and Boarding Houses'),
	('722310','Food Service Contractors'),
	('722320','Caterers'),
	('722330','Mobile Food Services'),
	('722410','Drinking Places (Alcoholic Beverages)'),
	('722511','Full-Service Restaurants'),
	('722513','Limited-Service Restaurants'),
	('722514','Cafeterias, Grill Buffets, and Buffets'),
	('722515','Snack and Nonalcoholic Beverage Bars'),
	('811111','General Automotive Repair'),
	('811112','Automotive Exhaust System Repair'),
	('811113','Automotive Transmission Repair'),
	('811118','Other Automotive Mechanical and Electrical Repair and Maintenance'),
	('811121','Automotive Body, Paint, and Interior Repair and Maintenance'),
	('811122','Automotive Glass Replacement Shops'),
	('811191','Automotive Oil Change and Lubrication Shops'),
	('811192','Car Washes'),
	('811198','All Other Automotive Repair and Maintenance'),
	('811211','Consumer Electronics Repair and Maintenance'),
	('811212','Computer and Office Machine Repair and Maintenance'),
	('811213','Communication Equipment Repair and Maintenance'),
	('811219','Other Electronic and Precision Equipment Repair and Maintenance')
GO

INSERT INTO dropdown.NAICS 
	(NAICSCode,NAICSName) 
VALUES
	('811310','Commercial and Industrial Machinery and Equipment (except Automotive and Electronic) Repair and Maintenance'),
	('811411','Home and Garden Equipment Repair and Maintenance'),
	('811412','Appliance Repair and Maintenance'),
	('811420','Reupholstery and Furniture Repair'),
	('811430','Footwear and Leather Goods Repair'),
	('811490','Other Personal and Household Goods Repair and Maintenance'),
	('812111','Barber Shops'),
	('812112','Beauty Salons'),
	('812113','Nail Salons'),
	('812191','Diet and Weight Reducing Centers'),
	('812199','Other Personal Care Services'),
	('812210','Funeral Homes and Funeral Services'),
	('812220','Cemeteries and Crematories'),
	('812310','Coin-Operated Laundries and Drycleaners'),
	('812320','Drycleaning and Laundry Services (except Coin-Operated)'),
	('812331','Linen Supply'),
	('812332','Industrial Launderers'),
	('812910','Pet Care (except Veterinary) Services'),
	('812921','Photofinishing Laboratories (except One-Hour)'),
	('812922','One-Hour Photofinishing'),
	('812930','Parking Lots and Garages'),
	('812990','All Other Personal Services'),
	('813110','Religious Organizations'),
	('813211','Grantmaking Foundations'),
	('813212','Voluntary Health Organizations'),
	('813219','Other Grantmaking and Giving Services'),
	('813311','Human Rights Organizations'),
	('813312','Environment, Conservation and Wildlife Organizations'),
	('813319','Other Social Advocacy Organizations'),
	('813410','Civic and Social Organizations'),
	('813910','Business Associations'),
	('813920','Professional Organizations'),
	('813930','Labor Unions and Similar Labor Organizations'),
	('813940','Political Organizations'),
	('813990','Other Similar Organizations (except Business, Professional, Labor, and Political Organizations)'),
	('814110','Private Households'),
	('921110','Executive Offices'),
	('921120','Legislative Bodies'),
	('921130','Public Finance Activities'),
	('921140','Executive and Legislative Offices, Combined'),
	('921150','American Indian and Alaska Native Tribal Governments'),
	('921190','Other General Government Support'),
	('922110','Courts'),
	('922120','Police Protection'),
	('922130','Legal Counsel and Prosecution'),
	('922140','Correctional Institutions'),
	('922150','Parole Offices and Probation Offices'),
	('922160','Fire Protection'),
	('922190','Other Justice, Public Order, and Safety Activities'),
	('923110','Administration of Education Programs'),
	('923120','Administration of Public Health Programs'),
	('923130','Administration of Human Resource Programs (except Education, Public Health, and Veterans'' Affairs Programs)'),
	('923140','Administration of Veterans'' Affairs'),
	('924110','Administration of Air and Water Resource and Solid Waste Management Programs'),
	('924120','Administration of Conservation Programs'),
	('925110','Administration of Housing Programs'),
	('925120','Administration of Urban Planning and Community and Rural Development'),
	('926110','Administration of General Economic Programs'),
	('926120','Regulation and Administration of Transportation Programs'),
	('926130','Regulation and Administration of Communications, Electric, Gas, and Other Utilities'),
	('926140','Regulation of Agricultural Marketing and Commodities'),
	('926150','Regulation, Licensing, and Inspection of Miscellaneous Commercial Sectors'),
	('927110','Space Research and Technology'),
	('928110','National Security'),
	('928120','International Affairs')
GO
--End table dropdown.NAICS

--End file Build File - 04 - Data - NAICS.sql

--Begin file Build File - 04 - Data - Other.sql
USE SBRS_DHHS
GO

--Begin table dropdown.BundleType
TRUNCATE TABLE dropdown.BundleType
GO

SET IDENTITY_INSERT dropdown.BundleType ON;
INSERT INTO dropdown.BundleType (BundleTypeID) VALUES (0);

INSERT INTO dropdown.BundleType 
	(BundleTypeID, BundleTypeCode, BundleTypeName)
VALUES
	(1, 'No', 'No'),
	(2, 'NA', 'N/A (Below estimated threshold)'),
	(3, 'Yes', 'Yes (Please list contract numbers)')

SET IDENTITY_INSERT dropdown.BundleType OFF;
GO
--End table dropdown.BundleType

--Begin table dropdown.COACS
TRUNCATE TABLE dropdown.COACS
GO

SET IDENTITY_INSERT dropdown.COACS ON;
INSERT INTO dropdown.COACS (COACSID) VALUES (0);

INSERT INTO dropdown.COACS 
	(COACSID, COACSCode, COACSName)
VALUES
	(1, 'NCI', 'National Cancer Institute (NCI,NCCAM)'),
	(2, 'CC', 'Clinical Center (CC)'),
	(3, 'NIDA', 'National Institute on Drug Abuse (NIDA, NINDS, NIMH, NIA, NCATS (partial yr.))'),
	(4, 'NHLBI', 'National Heart, Lung, & Blood Institute (NHLBI, NIAMS, CSR, NIDCR, NIBIB, NCATS (partial yr.), NHGRI)'),
	(5, 'NIAID', 'National Institute Of Allergy & Infectious Diseases (NIAID, HHS BIODEFENSE)'),
	(6, 'OLAO', 'Office of Logistics & Acquisition Operations (OLAO, NINR, NIMHD, NEI, ORS, NIDCD, NIGMS, OD, NITAAC)'),
	(7, 'ORF', 'Office of Research Facilities (ORF)'),
	(8, 'NIEHS', 'National Institute of Environmental Health Sciences (NIEHS)'),
	(9, 'NLM', 'National Library of Medicine (NLM, NIDDK, CIT, OD)'),
	(10, 'NICHD', 'National Institute of Child Health (NICHD, NIAAA, FIC)')

SET IDENTITY_INSERT dropdown.COACS OFF;
GO
--End table dropdown.COACS

--Begin table dropdown.ContractTermination
TRUNCATE TABLE dropdown.ContractTermination
GO

SET IDENTITY_INSERT dropdown.ContractTermination ON;
INSERT INTO dropdown.ContractTermination (ContractTerminationID) VALUES (0);

INSERT INTO dropdown.ContractTermination
	(ContractTerminationID, ContractTerminationCode, ContractTerminationName)
VALUES
	(1, 'No', 'No, then Completion Date'),
	(2, 'Yes', 'Yes, then Specify Reason')

SET IDENTITY_INSERT dropdown.ContractTermination OFF;
GO
--End table dropdown.ContractTermination

--Begin table dropdown.DocumentType
TRUNCATE TABLE dropdown.DocumentType
GO

SET IDENTITY_INSERT dropdown.DocumentType ON;
INSERT INTO dropdown.DocumentType (DocumentTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.DocumentType OFF;

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCode, DocumentTypeName)
VALUES
	('8aAL', '8(a) Acceptance Letter'),
	('8aOL', '8(a) Offer Letter'),
	('AP', 'Acquisition Plan'),
	('IGCE', 'IGCE'),
	('J&A', 'J&A'),
	('JEFO', 'JEFO-(Far 16.505(b)(2)(C))'),
	('LSJ', 'Limited Source Justification'),
	('MR', 'Market Research'),
	('OTR', 'Others'),
	('REQ', 'Requisition'),
	('SOW', 'Requirements Document (SOW, PWS, SOO)')
GO
--End table dropdown.DocumentType

--Begin table dropdown.FARJustification
TRUNCATE TABLE dropdown.FARJustification
GO

SET IDENTITY_INSERT dropdown.FARJustification ON;
INSERT INTO dropdown.FARJustification (FARJustificationID) VALUES (0);

INSERT INTO dropdown.FARJustification
	(FARJustificationID, FARJustificationName)
VALUES
	(4, 'Authorized or required by statute (6.302-5)'),
	(3, 'International agreement (6.302-4)'),
	(5, 'National security (6.302-6)'),
	(1, 'Only one responsible source and no other supplies or services will satisfy agency requirements (6.302-1)'),
	(6, 'Public interest (6.302-7)'),
	(2, 'Unusual and compelling urgency (6.302-2)')

SET IDENTITY_INSERT dropdown.FARJustification OFF;
GO
--End table dropdown.FARJustification

--Begin table dropdown.GSAJustification
TRUNCATE TABLE dropdown.GSAJustification
GO

SET IDENTITY_INSERT dropdown.GSAJustification ON;
INSERT INTO dropdown.GSAJustification (GSAJustificationID) VALUES (0);

INSERT INTO dropdown.GSAJustification
	(GSAJustificationID, GSAJustificationCode, GSAJustificationName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.GSAJustification OFF;
GO
--End table dropdown.GSAJustification

--Begin table dropdown.Organization
TRUNCATE TABLE dropdown.Organization
GO

SET IDENTITY_INSERT dropdown.Organization ON
GO

INSERT INTO dropdown.Organization (OrganizationID) VALUES (0)

INSERT INTO dropdown.Organization 
	(OrganizationID, OrganizationCode, OrganizationDisplayCode, OrganizationName)
VALUES
	(1, 'OS', 'OS', 'Office of Secretary (OS)'),
	(2, 'NIH', 'NIH', 'National Institute of Health (NIH)'),
	(3, 'AHQ', 'AHRQ', 'Agency for Healthcare Research and Quality (AHRQ)'),
	(4, 'CDC', 'CDC', 'Centers for Disease Control and Prevention (CDC)'),
	(5, 'CMS', 'CMS', 'Centers for Medicare & Medicaid Services (CMS)'),
	(6, 'FDA', 'FDA', 'Food and Drug Administration (FDA)'),
	(7, 'HRS', 'HRSA', 'Health Resources and Services Administration (HRSA)'),
	(8, 'IHS', 'IHS', 'Indian Health Service (IHS)'),
	(9, 'SAM', 'SAMHSA', 'Substance Abuse and Mental Health Services Administration (SAMHSA)'),
	(11, 'ACF', 'ACF', 'Administration for Children and Families (ACF)'),
	(13, 'ACL', 'ACL', 'Administration for Community Living (ACL)'),
	(15, 'APR', 'ASPR', 'Assistant Secretary for Preparedness and Response (ASPR)'),
	(16, 'PSC', 'PSC', 'Program Support Center (PSC)'),
	(17, 'SBA', 'SBA', 'Small Business Administration (SBA)')

INSERT INTO dropdown.Organization 
	(OrganizationID, OrganizationCode, OrganizationDisplayCode, OrganizationName, DisplayOrder)
SELECT 
	99,
	'NON',
	'NON',
	'NON-HHS',
	99
GO

SET IDENTITY_INSERT dropdown.Organization OFF
GO

UPDATE O
SET O.IsForFundingOffice = 0
FROM dropdown.Organization O
WHERE O.OrganizationDisplayCode IN ('NON', 'SBA')
GO

UPDATE O
SET OrganizationFilePath = '/Docs/' + O.OrganizationDisplayCode + '/'
FROM dropdown.Organization O
GO
--End table dropdown.Organization

--Begin table dropdown.PriorBundler
TRUNCATE TABLE dropdown.PriorBundler
GO

SET IDENTITY_INSERT dropdown.PriorBundler ON;
INSERT INTO dropdown.PriorBundler (PriorBundlerID) VALUES (0);

INSERT INTO dropdown.PriorBundler 
	(PriorBundlerID, PriorBundlerCode, PriorBundlerName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.PriorBundler OFF;
GO
--End table dropdown.PriorBundler

--Begin table dropdown.ProcurementHistory
TRUNCATE TABLE dropdown.ProcurementHistory
GO

SET IDENTITY_INSERT dropdown.ProcurementHistory ON;
INSERT INTO dropdown.ProcurementHistory (ProcurementHistoryID) VALUES (0);

INSERT INTO dropdown.ProcurementHistory 
	(ProcurementHistoryID, ProcurementHistoryCode, ProcurementHistoryName)
VALUES
	(1, 'NewRequirement', 'New Requirement (No prior procurement history)'),
	(2, 'Recompetition', 'Prior Procurement History - Recompetition')

SET IDENTITY_INSERT dropdown.ProcurementHistory OFF;
GO
--End table dropdown.ProcurementHistory

--Begin table dropdown.ProcurementMethod
TRUNCATE TABLE dropdown.ProcurementMethod
GO

SET IDENTITY_INSERT dropdown.ProcurementMethod ON;
GO

INSERT INTO dropdown.ProcurementMethod (ProcurementMethodID) VALUES (0);

INSERT INTO dropdown.ProcurementMethod 
	(ProcurementMethodID, ProcurementMethodCode, ProcurementMethodName)
VALUES
	(1, 'AONE', 'Ability One'),
	(2, 'GSA', 'GSA Federal Supply Schedule'),
	(3, 'DTO', 'Delivery / Task Order'),
	(4, '8aSS', '8(a) Sole Source'),
	(5, '8aC', '8(a) Competitive'),
	(6, 'HUBSS', 'HUBZone Sole Source'),
	(7, 'HUBC', 'HUBZone Competitive'),
	(8, 'SDVOSBSS', 'SDVOSB Sole Source'),
	(9, 'SDVOSBC', 'SDVOSB Competitive'),
	(10, 'SBSA', 'Small Business Set-Aside'),
	(11, 'SBSAP', 'Partial Small Business Set-Aside'),
	(12, 'FOU', 'Full & Open/Unrestricted'),
	(13, 'OFOU', 'Other than Full and Open'),
	(14, 'UNICOR', 'UNICOR'),
	(15, 'NASASEWP', 'NASA SEWP'),
	(16, 'WOSBC', 'Women-Owned Small Business (WOSB) Competitive'),
	(17, 'WOSBSS', 'Women-Owned Small Business (WOSB) Sole Source'),
	(18, 'EDWOSB', 'Economically Disadvantaged Women-Owned Small Business (EDWOSB) Set-Aside'),
	(19, 'Indian', 'Urban Indian Organization (P.L. 94-437) & Buy Indian Act (25 USC 47) - IHS HCA Authorization required')
GO

SET IDENTITY_INSERT dropdown.ProcurementMethod OFF;
GO

UPDATE PM
SET PM.IsSetAside = 1
FROM dropdown.ProcurementMethod PM
WHERE PM.ProcurementMethodName LIKE '%Competitive%'
	OR PM.ProcurementMethodName LIKE '%Set-Aside%'
	OR PM.ProcurementMethodName LIKE '%Sole Source%'
	OR PM.ProcurementMethodName LIKE '%Women-Ow%'
GO
--End table dropdown.ProcurementMethod

--Begin table dropdown.ResearchType
TRUNCATE TABLE dropdown.ResearchType
GO

SET IDENTITY_INSERT dropdown.ResearchType ON;
INSERT INTO dropdown.ResearchType (ResearchTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.ResearchType OFF;

INSERT INTO dropdown.ResearchType 
	(ResearchTypeCode,ResearchTypeName,DisplayOrder)
VALUES
	('SAM', 'System for Award Management (SAM)', 1), -- SAMType
	('DSBS', 'Dynamic Small Business Search (DSBS)', 2), --DBBSType
	('VOS', 'Vendor Outreach Session', 3), --TVOSType
	('CS', 'Capability Statements', 4), --CSType
	('SS', 'Sources Sought Notice', 5), --SourceType
	('OTR', 'Other', 6) --OtherType
GO
--End table dropdown.ResearchType

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

SET IDENTITY_INSERT dropdown.Role ON;
INSERT INTO dropdown.Role (RoleID) VALUES (0);
SET IDENTITY_INSERT dropdown.Role OFF;

INSERT INTO dropdown.Role 
	(RoleCode, RoleName, NotificationRoleCode, IsForDisplay, HasOrganization, DisplayOrder)
VALUES
	('CS', 'Contract Specialist', 'SBS', 1, 1, 1),
	('CO', 'Contracting Officer', 'SBS', 1, 1, 2),
	('DHCA', 'Deputy Head of Contracting Activities', 'OSDBU', 1, 1, 6),
	('HCA', 'Head of Contracting Activities', 'OSDBU', 1, 1, 7),
	('OSDBU', 'OSDBU Director / Deputy Director', 'OSDBU', 1, 0, 8),
	('SBAPCR', 'SBA Procurement Center Representative', 'OSDBU', 1, 0, 5),
	('SBS', 'Small Business Specialist', 'OSDBU', 1, 1, 4),
	('SBTA', 'Small Business Technical Advisor', 'OSDBU', 1, 1, 3),
	('CloseOut', 'Close Out', 'OSDBU', 0, 0, 0),
	('SysAdmin', 'System Administrator', 'OSDBU', 0, 0, 0)
GO
--End table dropdown.Role

--Begin table dropdown.SizeType
TRUNCATE TABLE dropdown.SizeType
GO

SET IDENTITY_INSERT dropdown.SizeType ON;
INSERT INTO dropdown.SizeType (SizeTypeID) VALUES (0);

INSERT INTO dropdown.SizeType 
	(SizeTypeID, SizeTypeCode, SizeTypeName)
VALUES
	(1, 'EmployeeCount', 'Number of Employees'),
	(2, 'Revenue', 'Dollar Amount')

SET IDENTITY_INSERT dropdown.SizeType OFF;
GO
--End table dropdown.SizeType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

SET IDENTITY_INSERT dropdown.Status ON;
INSERT INTO dropdown.Status (StatusID) VALUES (0);
SET IDENTITY_INSERT dropdown.Status OFF;

INSERT INTO dropdown.Status 
	(StatusCode, StatusName, DisplayOrder)
VALUES
	('InProgress', 'In Progress', 1),
	('UnderReview', 'Under Review', 2),
	('Complete', 'Complete', 3),
	('BackToAuthor', 'Back to Author', 4),
	('Withdrawn', 'Withdrawn', 5)
GO
--End table dropdown.Status

--Begin table dropdown.SubContractingPlan
TRUNCATE TABLE dropdown.SubContractingPlan
GO

SET IDENTITY_INSERT dropdown.SubContractingPlan ON;
INSERT INTO dropdown.SubContractingPlan (SubContractingPlanID) VALUES (0);

INSERT INTO dropdown.SubContractingPlan 
	(SubContractingPlanID, SubContractingPlanCode, SubContractingPlanName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.SubContractingPlan OFF;
GO
--End table dropdown.SubContractingPlan

--Begin table dropdown.Synopsis
TRUNCATE TABLE dropdown.Synopsis
GO

SET IDENTITY_INSERT dropdown.Synopsis ON;
INSERT INTO dropdown.Synopsis (SynopsisID) VALUES (0);

INSERT INTO dropdown.Synopsis 
	(SynopsisID, SynopsisCode, SynopsisName)
VALUES
	(1, 'Yes', 'Yes'),
	(2, 'No', 'No')

SET IDENTITY_INSERT dropdown.Synopsis OFF;
GO
--End table dropdown.Synopsis

--Begin table dropdown.SynopsisExceptionReason
TRUNCATE TABLE dropdown.SynopsisExceptionReason
GO

SET IDENTITY_INSERT dropdown.SynopsisExceptionReason ON;
GO

INSERT INTO dropdown.SynopsisExceptionReason (SynopsisExceptionReasonID) VALUES (0);
GO

INSERT INTO dropdown.SynopsisExceptionReason 
	(SynopsisExceptionReasonID, SynopsisExceptionReasonName)
VALUES
	(8, 'Acceptance of an unsolicited research proposal'),
	(4, 'Authorized or required by statute'),
	(13, 'Below SAT; available through GPE; and public responds electronically'),
	(12, 'Defense agency contract to be made and performed outside the US'),
	(15, 'Determination by agency head; as approved by OFPP and SBA'),
	(14, 'Expert services under 6.302-3'),
	(3, 'International agreement'),
	(10, 'Meets conditions under 6.302-3, or 6.302-5 (brand name commercial items), or 6.302-7'),
	(1, 'National security'),
	(6, 'Order placed under Subpart 16.5'),
	(9, 'Perishable subsistence supplies, advance notice is not appropriate or reasonable'),
	(11, 'Previously synopsized contract in compliance with 5.207'),
	(7, 'Proposal under the SBIR Program'),
	(2, 'Unusual and compelling urgency (6.302-2)'),
	(5, 'Utility services and only one source is available')
GO

SET IDENTITY_INSERT dropdown.SynopsisExceptionReason OFF;
GO
--End table dropdown.SynopsisExceptionReason

--Begin table dropdown.UnrestrictedProcurementReason
TRUNCATE TABLE dropdown.UnrestrictedProcurementReason
GO

SET IDENTITY_INSERT dropdown.UnrestrictedProcurementReason ON;
INSERT INTO dropdown.UnrestrictedProcurementReason (UnrestrictedProcurementReasonID) VALUES (0);

INSERT INTO dropdown.UnrestrictedProcurementReason 
	(UnrestrictedProcurementReasonID, UnrestrictedProcurementReasonCode, UnrestrictedProcurementReasonName, DisplayOrder)
VALUES
	(2,	'NoMarket', 'No reasonable expectation that award will be made at fair market price', 1),
	(1,	'NoFirms', 'No reasonable expectation that offers will be obtained from at least two small business concerns', 2),
	(3,	'NoProduct', 'Small business can not provide the products of another small business concerns', 3),
	(4,	'FAR', 'Sole Source/Proprietary Item Justified in accordance with FAR 6.3, please specify', 4),
	(5, 'UPROther', 'Others, please specify', 5)

SET IDENTITY_INSERT dropdown.UnrestrictedProcurementReason OFF;
GO
--End table dropdown.UnrestrictedProcurementReason

--Begin table emailtemplate.EmailTemplate
TRUNCATE TABLE emailtemplate.EmailTemplate
GO

INSERT INTO emailtemplate.EmailTemplate
	(EmailTemplateCode, EmailTemplateName, EmailSubject, EmailText)
VALUES
	('NewUserRegistration', 'New SBRS User Registration', 'A New SBRS User Has Registered', '<p>A new user has registered in the SBRS and is requesting access to your OpDiv.</p><p>Request details:<br />User:&nbsp;&nbsp;[[PersonNameFormatted]]<br />Email:&nbsp;&nbsp;[[EmailAddress]]<br />OpDiv:&nbsp;&nbsp;[[OrganizationName]]<br />Roles(s):&nbsp;&nbsp;[[RoleNameList]]</p><p>To approve this user&#39;s request, <a href="[[HRef]]">click here to Approve Their Account</a> or disregard this email to leave their account disabled.</p><p>Thanks,</p><p>The SBRS Team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('ReviewFormAssignment', 'SBRS Review Form Assignment', 'A Small Business Review Form Has Been Assigned To You', '[[PersonNameFormatted]],<p>An SBRS Review Form has been assigned to you.</p><p>Form:&nbsp;&nbsp;[[ControlCode]] is in the [[WorkflowStepName]] step of its workflow and is awaiting your review.  You may access this form by logging in to the SBRS <a href="https://sbrs.hhs.gov/">here</a> and clicking the "Open Items" tab.</p><p>Thanks,</p><p>The SBRS team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('ReviewFormComplete', 'SBRS Review Form Closeout', 'A Small Business Review Form You Originated Has Been Completed', '[[PersonNameFormatted]],<p>An SBRS Review Form that you originated has been completed.</p><p>Form:&nbsp;&nbsp;[[ControlCode]] has been closed out.  You may access this form by logging in to the SBRS <a href="https://sbrs.hhs.gov/">here</a> and clicking the "Closed Items" tab.</p><p>Thanks,</p><p>The SBRS team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('UserActivation', 'SBRS User Account Activation', 'Your SBRS User Account Has Been Activated', '<p>Dear [[FirstName]],</p><p>Your account in the Small Business Review System has been activated.</p><p>You may now login to your account with your email address and password at:</p><p><a href="[[HRef]]">Small Business Review System</a></p><p>&nbsp;</p><p>Thanks,</p><p>The SBRS Team</p><p>This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>')
GO
--End table emailtemplate.EmailTemplate

--Begin table workflow.Workflow
TRUNCATE TABLE workflow.Workflow
GO

INSERT INTO workflow.Workflow
	(WorkflowCode, OrganizationCode, LevelCode)
VALUES
	('IHS1', 'IHS', 'Level1'),
	('IHS2', 'IHS', 'Level2'),
	('IHS3', 'IHS', 'Level3'),
	('HHS1', 'HHS', 'Level1'),
	('HHS2', 'HHS', 'Level2'),
	('HHS3', 'HHS', 'Level3')
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
TRUNCATE TABLE workflow.WorkflowStep
GO

INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS1';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,SBAPCR,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS2';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,SBS,SBAPCR,OSDBU,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS3';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS1';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,SBAPCR,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS2';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,SBAPCR,OSDBU,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS3';
GO
--End table workflow.WorkflowStep

--End file Build File - 04 - Data - Other.sql

--Begin file Build File - 05 - Conversion.sql
USE SBRS_DHHS
GO

--Begin table person.Person
IF (SELECT OBJECT_ID('tempdb.person.#tPerson', 'U')) IS NOT NULL
	DROP TABLE person.#tPerson
--ENDIF

CREATE TABLE person.#tPerson 
	(
	PersonID INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(50),
	IsActive BIT,
	JobTitle VARCHAR(250),
	LastLoginDateTime DATETIME,
	LastName VARCHAR(50),
	OrganizationID INT,
	PhoneNumber VARCHAR(50)
	)

INSERT INTO person.#tPerson
	(EmailAddress, FirstName, IsActive, JobTitle, LastLoginDateTime, LastName, OrganizationID, PhoneNumber)
SELECT 
	D.EmailAddress, 
	D.FirstName, 
	D.IsActive, 
	D.JobTitle, 
	D.LastLoginDateTime, 
	D.LastName, 
	D.OrganizationID, 
	D.PhoneNumber
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY LOWER(U.Email) ORDER BY U.LastName, U.FirstName, U.Email, CASE WHEN AU.Status = 'A' OR U.Status = 'A' THEN 1 ELSE 0 END DESC, CASE WHEN AU.DateStamp >= U.UpdateDate THEN AU.DateStamp ELSE U.UpdateDate END DESC) AS RowIndex,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'vns7@cdc.gov' ELSE LOWER(U.Email) END AS EmailAddress,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'Corina' ELSE core.NullIfEmpty(migration.TitleCaseName(U.FirstName)) END AS FirstName,
		CASE WHEN AU.Status = 'A' OR U.Status = 'A' THEN 1 ELSE 0 END AS IsActive,
		core.NullIfEmpty(RTRIM(LTRIM(U.JobFunction))) AS JobTitle,
		CASE WHEN AU.DateStamp >= U.UpdateDate THEN AU.DateStamp ELSE U.UpdateDate END AS LastLoginDateTime,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'Genson' ELSE core.NullIfEmpty(migration.TitleCaseName(U.LastName)) END AS LastName,
		U.OrgID AS OrganizationID, 
		core.NullIfEmpty(RTRIM(LTRIM(U.PhoneNumber))) AS PhoneNumber
	FROM HQITApps.dbo.Users U
		JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = U.UserID
			AND core.NullIfEmpty(AU.AccessLevel) IS NOT NULL
			AND core.NullIfEmpty(U.Email) IS NOT NULL
	) D
WHERE D.RowIndex = 1
	AND EmailAddress NOT LIKE '%nsitellc%'
	AND EmailAddress NOT LIKE '%stsllc%'
	AND EmailAddress NOT LIKE '%stech%'
	AND EmailAddress NOT IN ('christopher.crouch@nih.gov', 'guimondmi@mail.nih.gov')
ORDER BY D.LastName, D.FirstName, IsActive DESC, LastLoginDateTime DESC

UPDATE P
SET 
	P.FirstName = 'Berta',
	P.LastName = 'Biltz'
FROM person.#tPerson P
WHERE P.EmailAddress IN ('bbiltz@cdc.gov', 'bbiltz@cdc.gov')
GO

UPDATE P
SET 
	P.FirstName = 'Maxine',
	P.LastName = 'Dineyazhe'
FROM person.#tPerson P
WHERE P.EmailAddress = 'maxine.dineyazhe@ihs.gov'
GO

UPDATE P
SET 
	P.FirstName = 'Korriise',
	P.LastName = 'Laroche'
FROM person.#tPerson P
WHERE P.EmailAddress IN ('korriise.laroche@psc.hhs.gov', 'korriise.laroche@nih.gov')
GO

UPDATE P
SET 
	P.FirstName = 'Brian',
	P.LastName = 'Numkena'
FROM person.#tPerson P
WHERE P.EmailAddress = 'brian.numkena@ihs.gov'
GO

TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person
	(EmailAddress, OrganizationID, FirstName, LastName, PhoneNumber, JobTitle, Password, PasswordSalt)
VALUES
	('christopher.crouch@hhs.gov', 99, 'Chris', 'Crouch', '999-888-7777', 'Contractor', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@hhs.gov', 99, 'Todd', 'Pires', '555-2121', 'Contractor', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('christopher.crouch@cs.gov', 2, 'Chris', 'Crouch (CS)', '999-888-7777', 'Contract Specialist', '3D022F59444D1847400006E0D041C59157E04868584BEF5487', '407C678A-086D-4D94-9DEF-E29F4C4C9D24'),
	('christopher.crouch@co.gov', 2, 'Chris', 'Crouch (CO)', '999-888-7777', 'Contracting Officer', 'F5BC792D95BCE2F0C55784314713E83B0940CC9655DF983AB7', 'FDBE9682-586D-4CEA-8157-D0916028416F'),
	('christopher.crouch@sbs.gov', 2, 'Chris', 'Crouch (SBS)', '999-888-7777', 'Small Business Specialist', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('christopher.crouch@sbapcr.gov', 2, 'Chris', 'Crouch (SBAPCR)', '999-888-7777', 'SBA Procurement Center Representative', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('christopher.crouch@osdbu.gov', 2, 'Chris', 'Crouch (OSDBU)', '999-888-7777', 'OSDBU Director / Deputy Director', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('christopher.crouch@sbta.gov', 8, 'Chris', 'Crouch (SBTA)', '999-888-7777', 'Small Business Technical Advisor', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('todd.pires@cs.gov', 8, 'Todd', 'Pires', '555-2121', 'CS', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@co.gov', 8, 'Todd', 'Pires', '555-2121', 'CO', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbta.gov', 8, 'Todd', 'Pires', '555-2121', 'SBTA', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbs.gov', 8, 'Todd', 'Pires', '555-2121', 'SBS', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbapcr.gov', 8, 'Todd', 'Pires', '555-2121', 'SBAPCR', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@osdbu.gov', 8, 'Todd', 'Pires', '555-2121', 'OSDBU', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2')
GO

TRUNCATE TABLE person.PersonOrganizationRole
GO

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 1, 99, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SysAdmin';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 2, 99, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SysAdmin';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 3, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 4, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CO';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 5, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 6, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 7, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 8, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBTA';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 9, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 10, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CO';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 11, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBTA';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 12, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 13, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 14, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 5, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 6, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 7, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';
GO

INSERT INTO person.Person
	(EmailAddress, FirstName, IsActive, JobTitle, LastLoginDateTime, LastName, OrganizationID, PhoneNumber)
SELECT 
	D.EmailAddress, 
	D.FirstName, 
	D.IsActive, 
	D.JobTitle, 
	D.LastLoginDateTime, 
	D.LastName, 
	D.OrganizationID, 
	D.PhoneNumber
FROM
	(		
	SELECT 
		ROW_NUMBER() OVER (PARTITION BY P.LastName, P.FirstName ORDER BY P.LastName, P.FirstName, P.IsActive DESC, P.LastLoginDateTime DESC) AS RowIndex,
		P.EmailAddress, 
		P.FirstName, 
		P.IsActive, 
		P.JobTitle, 
		P.LastLoginDateTime, 
		P.LastName, 
		P.OrganizationID, 
		P.PhoneNumber
	FROM person.#tPerson P
	) D
WHERE D.RowIndex = 1
GO

DROP TABLE person.#tPerson
GO
--End table person.Person

--Begin table migration.UserMigration
TRUNCATE TABLE migration.UserMigration
GO

INSERT INTO migration.UserMigration
	(EmailAddress, FirstName, LastName, Password, UserID)
SELECT 
	LOWER(U.Email) AS EmailAddress, 
	core.NullIfEmpty(migration.TitleCaseName(U.FirstName)) AS FirstName,
	core.NullIfEmpty(migration.TitleCaseName(U.LastName)) AS LastName,
	U.Password, 
	U.UserID
FROM HQITApps.dbo.Users U
	JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = U.UserID
		AND core.NullIfEmpty(AU.AccessLevel) IS NOT NULL
		AND core.NullIfEmpty(U.Email) IS NOT NULL
		AND LOWER(U.Email) NOT LIKE '%nsitellc%'
		AND LOWER(U.Email) NOT LIKE '%stsllc%'
		AND LOWER(U.Email) NOT LIKE '%stech%'
		AND LOWER(U.Email) NOT IN ('christopher.crouch@nih.gov', 'guimondmi@mail.nih.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Berta',
	UM.LastName = 'Biltz'
FROM migration.UserMigration UM
WHERE UM.EmailAddress IN ('bbiltz@cdc.gov', 'bbiltz@cdc.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Maxine',
	UM.LastName = 'Dineyazhe'
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'maxine.dineyazhe@ihs.gov'
GO

UPDATE UM
SET 
	UM.FirstName = 'Korriise',
	UM.LastName = 'Laroche'
FROM migration.UserMigration UM
WHERE UM.EmailAddress IN ('korriise.laroche@psc.hhs.gov', 'korriise.laroche@nih.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Brian',
	UM.LastName = 'Numkena'
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'brian.numkena@ihs.gov'
GO

UPDATE UM
SET UM.PersonID = P.PersonID
FROM migration.UserMigration UM
	JOIN person.Person P ON P.EmailAddress = UM.EmailAddress
GO

UPDATE UM
SET UM.PersonID = P.PersonID
FROM migration.UserMigration UM
	JOIN person.Person P ON P.LastName + ', ' + P.FirstName = UM.LastName + ', ' + UM.FirstName
		AND UM.PersonID = 0
GO

UPDATE UM
SET UM.PersonID = ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.EmailAddress = 'vns7@cdc.gov'), 0)
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'corina.couch@fda.hhs.gov'
	AND UM.PersonID = 0
GO
--End table migration.UserMigration

--Begin table person.PersonOrganizationRole
;
WITH PROD AS
	(
	SELECT DISTINCT
		UM.PersonID,
		P.OrganizationID,

		CASE
			WHEN AU.AccessLevel LIKE 'OSDBU%'
			THEN ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode LIKE 'OSDBU%'), 0)
			WHEN CHARINDEX(',', AU.AccessLevel) = 0
			THEN ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleName = AU.AccessLevel), 0)
			ELSE ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleName = LEFT(AU.AccessLevel, CHARINDEX(',', AU.AccessLevel) - 1)), 0)
		END AS RoleID

	FROM migration.UserMigration UM
		JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = UM.UserID
		JOIN HQITApps.dbo.Users U ON U.UserID = UM.UserID
		JOIN person.Person P ON P.PersonID = UM.PersonID
	)

INSERT INTO person.PersonOrganizationRole
	(PersonID, OrganizationID, RoleID)
SELECT
	PROD.PersonID,
	PROD.OrganizationID,
	PROD.RoleID
FROM PROD

UNION

SELECT
	UM.PersonID,
	MOU.OrgId,
	PROD.RoleID
FROM HQITApps.dbo.MultipleOrgUsers MOU
	JOIN migration.UserMigration UM ON MOU.UserID = UM.UserID
	JOIN PROD ON PROD.PersonID = UM.PersonID
ORDER BY 1, 2, 3
--End table person.PersonOrganizationRole

--Begin table reviewform.ReviewForm
TRUNCATE TABLE reviewform.ReviewForm
GO

SET IDENTITY_INSERT reviewform.ReviewForm ON
GO

INSERT INTO reviewform.ReviewForm
	(
	ReviewFormID,
	ControlCode,
	RequisitionNumber1,
	RequisitionNumber2,
	COACSID,
	FundingOrganizationID,
	FundingOrganizationName,
	BasePrice, 
	PeriodOfPerformanceStartDate,
	PeriodOfPerformanceEndDate,
	NAICSID,
	SizeTypeID,
	Size,
	SynopsisID,
	SynopsisExceptionReasonID,
	Description,
	ProcurementHistoryID,
	ProcurementHistoryContractAwardAmount,
	ProcurementHistoryContractAwardDate, 
	ProcurementHistoryContractEndDate, 
	ProcurementHistoryContractNumber, 
	ProcurementHistoryContractTerminationID, 
	ProcurementHistoryContractTerminationReason, 
	OtherResearch,
	BundleTypeID,
	PriorBundlerID,
	SubContractingPlanID,
	CreatePersonID, 
	CreateDateTime,
	OriginatingOrganizationID,
	IsActive
	)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	SBRSF.ReviewNo AS ControlCode,
	core.NullIfEmpty(SBRSF.RequistionNo) AS RequisitionNumber1,
	core.NullIfEmpty(SBRSF.OtherReqNo) AS RequisitionNumber2,
	ISNULL((SELECT C.COACSID FROM dropdown.COACS C WHERE C.COACSCode = SBRSF.CoacsCode), 0) AS COACSID,
	ISNULL((SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationDisplayCode = SBRSF.BuyerOffice), 0) AS FundingOrganizationID,
	core.NullIfEmpty(SBRSF.NonHHSFundingOffice) AS FundingOrganizationName,
	BasePrice,
	CASE WHEN ISDATE(SBRSF.ProjAwardDate) = 1 THEN SBRSF.ProjAwardDate ELSE NULL END AS PeriodOfPerformanceStartDate,
	CASE WHEN ISDATE(SBRSF.ProjAwardToDate) = 1 THEN SBRSF.ProjAwardToDate ELSE NULL END AS PeriodOfPerformanceEndDate,
	ISNULL((SELECT N.NAICSID FROM dropdown.NAICS N WHERE N.NAICSCode = SBRSF.NAICS), 0) AS NAICSID,
	SBRSF.SizeType AS SizeTypeID,
	ISNULL(migration.ConvertCharToInt(SBRSF.Size), 0) AS Size,
	SBRSF.SynopsisType AS SynopsisID,
	SBRSF.Exception AS SynopsisExceptionReasonID,
	core.NullIfEmpty(SBRSF.Description) AS Description,
	ISNULL(SBRSF.History, 0) AS ProcurementHistoryID,
	ISNULL(SBRSF.ContractAwardAmount, 0) AS ProcurementHistoryContractAwardAmount,
	CASE WHEN ISDATE(SBRSF.ContractDate) = 1 THEN SBRSF.ContractDate ELSE NULL END AS ProcurementHistoryContractAwardDate,
	CASE WHEN ISDATE(SBRSF.Completion) = 1 THEN SBRSF.Completion ELSE NULL END AS ProcurementHistoryContractEndDate,
	core.NullIfEmpty(SBRSF.ContractNo) AS ProcurementHistoryContractNumber,
	ISNULL(SBRSF.Terminate, 0) AS ProcurementHistoryContractTerminationID,
	core.NullIfEmpty(SBRSF.Terminated) AS ProcurementHistoryContractTerminationReason,
	core.NullIfEmpty(SBRSF.OtherResearch) AS OtherResearch,
	ISNULL(SBRSF.Bundle, 0) AS BundleTypeID,
	ISNULL(SBRSF.SBconcern, 0) AS PriorBundlerID,
	ISNULL(SBRSF.Subcontract, 0) AS SubContractingPlanID,
	ISNULL((SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.UserID = SBRSF.UserID), 0) AS CreatePersonID,
	SBRSF.EntryDate AS CreateDateTime,
	SBRSF.OrgID AS OriginatingOrganizationID,
	CASE WHEN SBRSF.Displayed = 'Y' THEN 1 ELSE 0 END AS IsActive
FROM SBReview.dbo.SBRSForms SBRSF
GO

SET IDENTITY_INSERT reviewform.ReviewForm OFF
GO

UPDATE RF
SET 
	RF.PeriodOfPerformanceEndDate = CASE WHEN RF.PeriodOfPerformanceEndDate = '1900-01-01' THEN NULL ELSE RF.PeriodOfPerformanceEndDate END,
	RF.PeriodOfPerformanceStartDate = CASE WHEN RF.PeriodOfPerformanceStartDate = '1900-01-01' THEN NULL ELSE RF.PeriodOfPerformanceStartDate END,
	RF.ProcurementHistoryContractAwardDate = CASE WHEN RF.ProcurementHistoryContractAwardDate = '1900-01-01' THEN NULL ELSE RF.ProcurementHistoryContractAwardDate END,
	RF.ProcurementHistoryContractEndDate = CASE WHEN RF.ProcurementHistoryContractEndDate = '1900-01-01' THEN NULL ELSE RF.ProcurementHistoryContractEndDate END,
	RF.Size = CASE WHEN RIGHT(CAST(RF.Size AS VARCHAR), 3) = '.00' THEN CAST(REPLACE(CAST(RF.Size AS VARCHAR), '.00', '') AS BIGINT) ELSE RF.Size END
FROM reviewform.ReviewForm RF
GO

--Begin set WorkflowID
WITH RFD AS 
	(
	SELECT
		RF.ReviewFormID,

		CASE
			WHEN O.OrganizationCode NOT IN ('IHS', 'NON')
			THEN 'HHS'
			ELSE O.OrganizationCode 
		END AS OrganizationCode,

		CASE
			WHEN reviewform.GetTotalEstimatedValue(RF.ReviewFormID) < 150000
				OR EXISTS (SELECT 1 FROM reviewform.ReviewFormProcurementMethod RFPM JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID AND PM.IsSetAside = 1 AND RFPM.ReviewFormID = RF.ReviewFormID)
			THEN 'Level1'
			WHEN reviewform.GetTotalEstimatedValue(RF.ReviewFormID) >= 5000000
			THEN 'Level3'
			ELSE 'Level2'
		END AS LevelCode

	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
	)

UPDATE RF
SET RF.WorkflowID = W.WorkflowID
FROM reviewform.ReviewForm RF
	JOIN RFD ON RFD.ReviewFormID = RF.ReviewFormID
	JOIN workflow.Workflow W ON W.OrganizationCode = RFD.OrganizationCode
		AND W.LevelCode = RFD.LevelCode
GO
--End set WorkflowID

--Begin set StatusID
WITH SD AS
	(
	SELECT 
		D.ReviewNo,
		S.StatusID
	FROM
		(
		SELECT 
			SF.ReviewNo,
		
			CASE
				WHEN SF.Status IN ('Exist', 'New', 'Re-submit')
				THEN 'In Progress'
				WHEN SF.Status LIKE '%Review' OR SF.Status = 'Back to SBS'
				THEN 'Under Review'
				WHEN SF.Status = 'Withdraw'
				THEN 'Withdrawn'
				ELSE core.NullIfEmpty(SF.Status)
			END AS DerivedStatus

		FROM SBReview.dbo.SBRSForms SF
		) D 
			JOIN dropdown.Status S ON S.StatusName = D.DerivedStatus
	)

UPDATE RF
SET RF.StatusID = SD.StatusID
FROM reviewform.ReviewForm RF
	JOIN SD ON SD.ReviewNo = RF.ControlCode
GO
--End set StatusID

--Begin set AssignedPersonID
UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = RTRIM(LTRIM(REPLACE(SF.CurrentQ, '  ', ' ')))), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND RF.AssignedPersonID = 0
		AND SF.CurrentQ <> 'Closeout'
		AND S.StatusCode <> 'Complete'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'Joyce Thurmond'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Joyce Thurman'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'STEPHEN LESTER'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Lester Stephen'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'margaret shirley-damon'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'margaret shirley'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'Louis Anderson'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Anderson Louis'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = SF.CurrentQ), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'James Josey'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = 'Ralph Vallone'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Ralp Vallone'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = 'Berta Biltz'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode = '2014-CDC-0581'
GO
--End set AssignedPersonID

--Begin set AssignedPersonID & StatusID
UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.StatusID = (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor')
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ IN ('Annette Owens-Scarboro', 'crouch SBS', 'Nishchala Kodali', 'Test CO')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-NIH-0938','2015-PSC-0071','2015-PSC-0077','2016-CDC-0297','2016-CDC-0469','2016-FDA-0154','2016-IHS-0376','2016-IHS-0529','2016-NIH-0710')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Complete'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2014-CDC-0213','2014-FDA-0256','2014-IHS-0001','2014-PSC-0116','2015-CDC-0653','2015-CDC-0959','2015-IHS-0041','2015-IHS-0045','2015-IHS-0100','2015-IHS-0171','2015-IHS-0188','2015-IHS-0226','2015-IHS-0234','2015-IHS-0266','2015-IHS-0283','2015-IHS-0302','2015-IHS-0344','2015-NIH-0027','2015-NIH-0122','2015-NIH-0200','2015-NIH-0286','2015-NIH-1315','2015-NIH-1568','2015-NIH-2016','2015-SAM-0003','2015-SAM-0008','2016-APR-0017','2016-APR-0035','2016-APR-0037','2016-APR-0072','2016-CDC-0203','2016-CDC-0538','2016-CDC-0560','2016-CDC-0635','2016-CDC-0745','2016-CDC-0953','2016-CDC-1104','2016-CMS-0087','2016-FDA-0045','2016-FDA-0121','2016-FDA-0138','2016-FDA-0145','2016-FDA-0147','2016-FDA-0259','2016-HRS-0020','2016-HRS-0025','2016-HRS-0072','2016-HRS-0091','2016-IHS-0071','2016-IHS-0151','2016-IHS-0172','2016-IHS-0292','2016-IHS-0298','2016-IHS-0300','2016-IHS-0318','2016-IHS-0324','2016-IHS-0326','2016-IHS-0334','2016-IHS-0342','2016-IHS-0350','2016-IHS-0369','2016-IHS-0414','2016-IHS-0419','2016-IHS-0505','2016-IHS-0539','2016-IHS-0550','2016-IHS-0572','2016-IHS-0583','2016-IHS-0597','2016-IHS-0598','2016-IHS-0629','2016-IHS-0647','2016-IHS-0690','2016-IHS-0716','2016-IHS-0726','2016-IHS-0729','2016-IHS-0730','2016-IHS-0756','2016-IHS-0774','2016-IHS-0800','2016-IHS-0861','2016-IHS-0870','2016-IHS-0905','2016-IHS-0917','2016-IHS-0939','2016-IHS-0941','2016-IHS-0952','2016-IHS-0955','2016-IHS-0957','2016-IHS-0985','2016-IHS-0987','2016-IHS-1000','2016-IHS-1014','2016-IHS-1025','2016-IHS-1049','2016-IHS-1065','2016-IHS-1077','2016-IHS-1078','2016-IHS-1080','2016-IHS-1082','2016-IHS-1083','2016-IHS-1088','2016-IHS-1119','2016-NIH-0077','2016-NIH-0561','2016-NIH-0641','2016-NIH-0712','2016-NIH-0763','2016-NIH-0782','2016-NIH-0967','2016-NIH-0977','2016-NIH-1035','2016-NIH-1222','2016-NIH-1242','2016-NIH-1452','2016-NIH-1705','2016-NIH-1777','2016-NIH-2076','2016-NIH-2095','2016-PSC-0020','2016-PSC-0059','2017-CDC-0044','2017-FDA-0044','2017-IHS-0001','2017-IHS-0002','2017-IHS-0003','2017-IHS-0004','2017-IHS-0029','2017-IHS-0030','2017-IHS-0031','2017-IHS-0033','2017-IHS-0041','2017-IHS-0042','2017-IHS-0061','2017-IHS-0066','2017-IHS-0134','2017-IHS-0158','2017-IHS-0292','2017-IHS-0513','2017-NIH-0022','2017-NIH-0158','2017-NIH-0187','2017-NIH-0240','2017-NIH-0309','2017-NIH-0388','2017-NIH-0516','2017-NIH-0518','2017-PSC-0092','2017-SAM-0002')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Rosanna Browning'), 0),
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2014-PSC-0010')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Barbara Taylor'), 0),
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2017-NIH-0104')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Dwight Deneal'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-APR-0027')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Gina Jackson'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0068')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Jerry Black'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-IHS-0122')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.EmailAddress = 'john.stout@ihs.gov'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2017-IHS-0325')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Kathy Harrigan'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0086')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Maxine Dineyazhe'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-IHS-0063')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicola Carmichael'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0070')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicola Carmichael'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0116')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicole Stevenson'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0063')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Theresa Routh-Murphy'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-CDC-0311')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Withdrawn'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-NIH-0871')
GO
--End set AssignedPersonID & StatusID

--Begin set AssignedPersonID & WorkflowStepNumber
UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.WorkflowStepNumber = ISNULL((SELECT COUNT(WS.WorkflowStepID) FROM workflow.WorkflowStep WS WHERE WS.WorkflowID = RF.WorkflowID), 0)
FROM reviewform.ReviewForm RF 
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND S.StatusCode = 'Complete'
GO

UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.WorkflowStepNumber = reviewform.GetAuthorWorkflowStepNumberByReviewFormID(RF.ReviewFormID)
FROM reviewform.ReviewForm RF 
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND S.StatusCode = 'BackToAuthor'
GO
--End set AssignedPersonID & WorkflowStepNumber

--Begin set WorkflowStepNumber
WITH WSN AS
	(
	SELECT
		RF.ReviewFormID,
		migration.GetAssigneeWorkflowStepNumberByReviewFormID(RF.ReviewFormID) AS WorkflowStepNumber
	FROM reviewform.ReviewForm RF 
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND S.StatusCode IN ('BackToAuthor', 'InProgress', 'UnderReview')
	)

UPDATE RF
SET RF.WorkflowStepNumber = WSN.WorkflowStepNumber
FROM reviewform.ReviewForm RF 
	JOIN WSN ON WSN.ReviewFormID = RF.ReviewFormID
GO
--End set WorkflowStepNumber
--End table reviewform.ReviewForm

--Begin final pass on person.Person for name swapping
UPDATE P
SET 
	P.FirstName = P.LastName,
	P.LastName = P.FirstName
FROM person.Person P
WHERE P.EmailAddress IN 
	(
	'allison.lempka@psc.hhs.gov',
	'alphonso.johnson@psc.hhs.gov',
	'alphonso.simmons@fda.hhs.gov',
	'anne.mastrincola@sba.gov',
	'annette.merrion@dm.usdo.gov',
	'anthony.terrell@psc.hhs.gov',
	'arlene.belindo@ihs.gov',
	'ashley.flynn@ihs.gov',
	'azeb.mengistu@psc.hhs.gov',
	'bernard.durham@sba.gov',
	'billy.castleberry@psc.hhs.gov',
	'brad.engel@nih.gov',
	'brendan.miller2@nih.gov',
	'brent.owens@hill.af.mil',
	'bryan.daines@psc.hhs.gov',
	'caleb.owen@hhs.gov',
	'carla.mosley@psc.hhs.gov',
	'carol.diaz@ihs.gov',
	'carol.thompson@sba.gov',
	'chad.martin@ihs.gov',
	'christine.russman@fda.hhs.gov',
	'colleen.burns@sba.gov',
	'dan.finley@ihs.gov',
	'daniel.federline@nih.gov',
	'daniel.hall@nih.gov',
	'darin.deschiney@ihs.gov',
	'david.spicer@nih.gov',
	'debbie.hope@psc.hhs.gov',
	'debra.hoffman@cms.hhs.gov',
	'diane.frasier@nih.hhs.gov',
	'dina.inhulsen@sba.gov',
	'douglas.gerard@sba.gov',
	'elliott.sloan@cms.hhs.gov',
	'gary.heard@sba.gov',
	'gary.klaff@fda.hhs.gov',
	'george.dupin@sba.gov',
	'gregory.benedict@nih.gov',
	'haig.altunian@psc.hhs.gov',
	'hing.wong@nih.gov',
	'howard.parker@doe.gov',
	'howard.yablon@fda.hhs.gov',
	'isaiah.hugs@ihs.gov',
	'ivette.bascumbe.mesa@sba.gov',
	'james.gambardella@sba.gov',
	'janette.fasano@sba.gov',
	'jasmine.ruffin@nih.gov',
	'jeremy.steel@ihs.gov',
	'john.bagaason@sba.gov',
	'john.stout@ihs.gov',
	'jose.martinez@sba.gov',
	'joyce.spears@sba.gov',
	'karen.zeiter@fda.hhs.gov',
	'katherine.ragland@sba.gov',
	'katherine.renville@ihs.gov',
	'keith.harding@psc.hhs.gov',
	'keith.waye@sba.gov',
	'kelley.williams-vollmer@cms.hhs.gov',
	'ken.parrish@ihs.gov',
	'kevin.michael@sba.gov',
	'kimesha.leake@nih.gov',
	'laurence.orr@navy.mil',
	'leann.delaney@sba.gov ',
	'licinda.peters@cms.hhs.gov',
	'lisa.bielen@nih.gov',
	'lydina.battle@psc.hhs.gov',
	'm.manzanares@sba.gov',
	'maria.galloway@sba.gov',
	'marichu.relativo@sba.gov',
	'martina.williams@do.treas.gov',
	'michael.cecere@us.army.mil',
	'michael.gemmill@nih.gov',
	'michelle.creenan@fda.hhs.gov',
	'michelle.street@hhs.gov',
	'nathan.amador@cdc.hhs.gov',
	'nicole.craig@fda.hhs.gov',
	'pamela.beavers@sba.gov',
	'pamela.daugherty@nih.gov',
	'pamela.m.thompson24.civ@mail.nih',
	'paul.chann@sba.gov',
	'paul.p.stone@usace.army.mil',
	'randall.johnston@sba.gov',
	'randy.marchiafava@usace.army.mil',
	'rhonda.duncan@psc.hhs.gov',
	'ricardo.corderocruz@nih.gov',
	'robert.c.taylor@sba.gov',
	'robert.sim@ihs.gov',
	'ronald.robinson@psc.hhs.gov',
	'rose.weston@ihs.gov',
	'sally.walton@doeal.gov',
	'shalisha.harris@psc.hhs.gov',
	'shannon.sartin@iha.gov',
	'sharmaine.fagan-kerr@nih.gov',
	'shaun.miles@nih.gov',
	'sheena.little@sba.gov',
	'skye.duffner@nih.gov',
	'sonya.owens-cobblah@nih.gov',
	'tanika.pierce@sba.gov',
	'teresa.shook@sba.gov',
	'thomas.krusemark@sba.gov',
	'thomas.vanhorne@sba.gov',
	'tiffany.thornton@psc.hhs.gov',
	'todd.cole@nih.hhs.gov',
	'tracy.scott@nih.gov',
	'valerie.j.coleman@nasa.gov'
	)
GO
--End final pass on person.Person for name swapping

--Begin table reviewform.ReviewFormContract
TRUNCATE TABLE reviewform.ReviewFormContract
GO

DECLARE	@cContract1No VARCHAR(100)
DECLARE	@cContract2No VARCHAR(100)
DECLARE	@cContract3No VARCHAR(100)
DECLARE	@cContract4No VARCHAR(100)
DECLARE	@cContract5No VARCHAR(100)
DECLARE	@cContract6No VARCHAR(100)
DECLARE	@nReviewFormID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		SBRSF.ReviewID AS ReviewFormID, 
		core.NullIfEmpty(SBRSF.Contract1No), 
		core.NullIfEmpty(SBRSF.Contract2No), 
		core.NullIfEmpty(SBRSF.Contract3No), 
		core.NullIfEmpty(SBRSF.Contract4No), 
		core.NullIfEmpty(SBRSF.Contract5No), 
		core.NullIfEmpty(SBRSF.Contract6No)
	FROM SBReview.dbo.SBRSForms SBRSF

OPEN oCursor
FETCH oCursor INTO @nReviewFormID, @cContract1No, @cContract2No, @cContract3No, @cContract4No, @cContract5No, @cContract6No
WHILE @@fetch_status = 0
	BEGIN

	IF @cContract1No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract1No) 
	--ENDIF
	IF @cContract2No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract2No) 
	--ENDIF
	IF @cContract3No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract3No) 
	--ENDIF
	IF @cContract4No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract4No) 
	--ENDIF
	IF @cContract5No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract5No) 
	--ENDIF
	IF @cContract6No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract6No) 
	--ENDIF

	FETCH oCursor INTO @nReviewFormID, @cContract1No, @cContract2No, @cContract3No, @cContract4No, @cContract5No, @cContract6No
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End table reviewform.ReviewFormContract

--Begin table reviewform.ReviewFormDocument
TRUNCATE TABLE reviewform.ReviewFormDocument
GO

INSERT INTO reviewform.ReviewFormDocument
	(ReviewFormID, CreateDateTime, CreatePersonID, DocumentPath, DocumentName, DocumentTypeID, FileGUID, IsActive, IsFileGUIDFileName)
SELECT
	RF.ReviewFormID,
	A.EntryDate AS CreateDateTime,
	ISNULL((SELECT P.PersonID FROM person.Person P JOIN HQITAPPS.dbo.Users U ON LOWER(U.Email) = P.EmailAddress AND U.UserID = A.UserID), 0) AS CreatePersonID,
	'/' + A.FilePath AS DocumentPath,
	A.FileContent AS DocumentName,
	ISNULL((SELECT DT.DocumentTypeID FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = A.DocType), 0) AS DocumentTypeID,
	newID() AS FileGUID,

	CASE
		WHEN A.Status = 'A'
		THEN 1
		ELSE 0
	END AS IsActive,

	0 AS IsFileGUIDFileName
FROM SBReview.dbo.Attachments A
	JOIN reviewform.ReviewForm RF ON RF.ControlCode = A.ReviewNo
ORDER BY A.ReviewNo, A.EntryDate
GO
--End table reviewform.ReviewFormDocument

--Begin table reviewform.ReviewFormOptionYear
TRUNCATE TABLE reviewform.ReviewFormOptionYear
GO

INSERT INTO reviewform.ReviewFormOptionYear
	(ReviewFormID, OptionYearValue)

SELECT
	ReviewFormID,
	OptionYearValue
FROM
	(
	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		CASE WHEN SBRSF.OptionPrice = 0 THEN ISNULL(SBRSF.Option1Price, 0) ELSE SBRSF.OptionPrice END AS OptionYearValue,
		1 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.OptionPrice > 0 
		OR ISNULL(SBRSF.Option1Price, 0) > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option2Price AS OptionYearValue,
		2 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option2Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option3Price AS OptionYearValue,
		3 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option3Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option4Price AS OptionYearValue,
		4 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option4Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option5Price AS OptionYearValue,
		5 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option5Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option6Price AS OptionYearValue,
		6 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option6Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option7Price AS OptionYearValue,
		7 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option7Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option8Price AS OptionYearValue,
		8 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option8Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option9Price AS OptionYearValue,
		9 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option9Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option10Price AS OptionYearValue,
		10 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option10Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option11Price AS OptionYearValue,
		11 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option11Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option12Price AS OptionYearValue,
		12 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option12Price > 0
	) D
ORDER BY D.ReviewFormID, D.SequenceNumber
GO

;
WITH PD AS 
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY RFOY.ReviewFormID ORDER BY RFOY.ReviewFormOptionYearID) AS RowIndex,
		RFOY.ReviewFormOptionYearID
	FROM reviewform.ReviewFormOptionYear RFOY
	)

UPDATE RFOY
SET RFOY.OptionYear = PD.RowIndex
FROM reviewform.ReviewFormOptionYear RFOY
	JOIN PD ON PD.ReviewFormOptionYearID = RFOY.ReviewFormOptionYearID
GO
--End table reviewform.ReviewFormOptionYear

--Begin table reviewform.ReviewFormPointOfContact
TRUNCATE TABLE reviewform.ReviewFormPointOfContact
GO

INSERT INTO reviewform.ReviewFormPointOfContact 
	(ReviewFormID, PointOfContactName)
SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	core.NullIfEmpty(SBRSF.BuyerName)
FROM SBReview.dbo.SBRSForms SBRSF
WHERE CHARINDEX(';', SBRSF.BuyerName) = 0
ORDER BY 1

DECLARE	@cPointOfContactName VARCHAR(50)
DECLARE	@nReviewFormID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		core.NullIfEmpty(SBRSF.BuyerName)
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE CHARINDEX(';', SBRSF.BuyerName) > 0

OPEN oCursor
FETCH oCursor INTO @nReviewFormID, @cPointOfContactName
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO reviewform.ReviewFormPointOfContact 
		(ReviewFormID, PointOfContactName)
	SELECT DISTINCT
		@nReviewFormID,
		LTRIM(RTRIM(LTT.ListItem)) AS PointOfContactName
	FROM core.ListToTable(@cPointOfContactName, ';') LTT
	ORDER BY 1

	FETCH oCursor INTO @nReviewFormID, @cPointOfContactName
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE RFPOC
SET RFPOC.PointOfContactPhone = (SELECT TOP 1 core.NullIfEmpty(U.PhoneNumber) FROM HQITApps.dbo.Users U WHERE LTRIM(RTRIM(LTRIM(RTRIM(U.FirstName)) + ' ' + LTRIM(RTRIM(U.LastName)))) = RFPOC.PointOfContactName AND U.PhoneNumber IS NOT NULL)
FROM reviewform.ReviewFormPointOfContact RFPOC
GO

UPDATE RFPOC
SET RFPOC.PointOfContactEmailAddress = (SELECT TOP 1 core.NullIfEmpty(LOWER(U.Email)) FROM HQITApps.dbo.Users U WHERE LTRIM(RTRIM(LTRIM(RTRIM(U.FirstName)) + ' ' + LTRIM(RTRIM(U.LastName)))) = RFPOC.PointOfContactName AND U.Email IS NOT NULL)
FROM reviewform.ReviewFormPointOfContact RFPOC
GO
--End table reviewform.ReviewFormPointOfContact

--Begin table reviewform.ReviewFormProcurementHistory
TRUNCATE TABLE reviewform.ReviewFormProcurementHistory
GO

DECLARE @tTable TABLE
	(
	ReviewFormID INT,
	ProcurementMethodID1 INT,
	Percentage1 INT,
	ProcurementMethodID2 INT,
	Percentage2 INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

INSERT INTO @tTable
	(ReviewFormID, ProcurementMethodID1, Percentage1, ProcurementMethodID2, Percentage2, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.PreMethodType1), 0) AS ProcurementMethodID,
	ISNULL(SBRSF.PreMType1pcnt, 0) AS Percentage,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.PreMethodType2), 0) AS ProcurementMethodID2,
	ISNULL(SBRSF.PreMType2pcnt, 0) AS Percentage2,
	ISNULL(SBRSF.PreJustify, 0) AS FARJustificationID,
	ISNULL(SBRSF.PreLimitRsc, 0) AS GSAJustificationID,
	core.NullIfEmpty(SBRSF.PreScheduleNo) AS GSAScheduleNumber,
	core.NullIfEmpty(SBRSF.PreTOdescr) AS TaskOrderDescription,
	core.NullIfEmpty(SBRSF.PreTaksOrderNo) AS TaskOrderNumber,
	core.NullIfEmpty(SBRSF.PreUPother) AS UnrestrictedProcurementReason,
	ISNULL(SBRSF.PreUPreason, 0) AS UnrestrictedProcurementReasonID
FROM SBReview.dbo.SBRSForms SBRSF
ORDER BY 1

UPDATE T
SET T.ProcurementMethodID1 = 0
FROM @tTable T
WHERE T.Percentage1 = 0

UPDATE T
SET T.ProcurementMethodID2 = 0
FROM @tTable T
WHERE T.Percentage2 = 0

INSERT INTO reviewform.ReviewFormProcurementHistory
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID1,
	T.Percentage1,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID1 > 0
	AND T.Percentage1 > 0

INSERT INTO reviewform.ReviewFormProcurementHistory
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID2,
	T.Percentage2,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID2 > 0
	AND T.Percentage2 > 0
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID NOT IN (2,3,12,13)
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 2
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 3
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 12
GO

UPDATE RFPH
SET
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 13
GO
--End table reviewform.ReviewFormProcurementHistory

--Begin table reviewform.ReviewFormProcurementMethod
TRUNCATE TABLE reviewform.ReviewFormProcurementMethod
GO

DECLARE @tTable TABLE
	(
	ReviewFormID INT,
	ProcurementMethodID1 INT,
	Percentage1 INT,
	ProcurementMethodID2 INT,
	Percentage2 INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

INSERT INTO @tTable
	(ReviewFormID, ProcurementMethodID1, Percentage1, ProcurementMethodID2, Percentage2, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.MethodType1), 0) AS ProcurementMethodID,
	ISNULL(SBRSF.MType1pcnt, 0) AS Percentage,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.MethodType2), 0) AS ProcurementMethodID2,
	ISNULL(SBRSF.MType2pcnt, 0) AS Percentage2,
	ISNULL(SBRSF.Justify, 0) AS FARJustificationID,
	ISNULL(SBRSF.LimitRsc, 0) AS GSAJustificationID,
	core.NullIfEmpty(SBRSF.ScheduleNo) AS GSAScheduleNumber,
	core.NullIfEmpty(SBRSF.TOdescr) AS TaskOrderDescription,
	core.NullIfEmpty(SBRSF.TaksOrderNo) AS TaskOrderNumber,
	core.NullIfEmpty(SBRSF.UPother) AS UnrestrictedProcurementReason,
	ISNULL(SBRSF.UPreason, 0) AS UnrestrictedProcurementReasonID
FROM SBReview.dbo.SBRSForms SBRSF
ORDER BY 1

UPDATE T
SET T.ProcurementMethodID1 = 0
FROM @tTable T
WHERE T.Percentage1 = 0

UPDATE T
SET T.ProcurementMethodID2 = 0
FROM @tTable T
WHERE T.Percentage2 = 0

INSERT INTO reviewform.ReviewFormProcurementMethod
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID1,
	T.Percentage1,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID1 > 0
	AND T.Percentage1 > 0

INSERT INTO reviewform.ReviewFormProcurementMethod
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID2,
	T.Percentage2,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID2 > 0
	AND T.Percentage2 > 0
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID NOT IN (2,3,12,13)
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 2
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 3
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 12
GO

UPDATE RFPM
SET
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 13
GO

DELETE RFPM1
FROM reviewform.ReviewFormProcurementMethod RFPM1
	JOIN
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY RFPM2.ReviewFormID, RFPM2.ProcurementMethodID ORDER BY  RFPM2.ReviewFormID, RFPM2.ProcurementMethodID) AS RowIndex,
			RFPM2.ReviewFormProcurementMethodID
		FROM reviewform.ReviewFormProcurementMethod RFPM2
		) D ON D.ReviewFormProcurementMethodID = RFPM1.ReviewFormProcurementMethodID
			AND D.RowIndex > 1
GO
--End table reviewform.ReviewFormProcurementMethod

--Begin table reviewform.ReviewFormResearchType
TRUNCATE TABLE reviewform.ReviewFormResearchType
GO

INSERT INTO reviewform.ReviewFormResearchType
	(ReviewFormID, ResearchTypeID)

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'CS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.CSType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'DSBS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.DBBSType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'OTR') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.OtherType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'SAM') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.SAMType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'SS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.SourceType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'VOS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.TVOSType = 1
GO
--End table reviewform.ReviewFormResearchType

--Begin table eventlog.EventLog
TRUNCATE TABLE eventlog.EventLog
GO

INSERT INTO eventlog.EventLog
	(PersonID, Action, EntityTypeCode, EntityID, Comments, CreateDateTime)
SELECT
	(SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.UserID = NRH.UserID),
	NRH.Action,
	'ReviewForm',
	RF.ReviewFormID,
	NRH.Reason,
	NRH.ActionDate
FROM SBReview.dbo.NotifyReviewHistory NRH
	JOIN reviewform.ReviewForm RF ON RF.ControlCode = NRH.ReviewNo
		AND EXISTS
			(
			SELECT 1
			FROM migration.UserMigration UM 
			WHERE UM.UserID = NRH.UserID
			)
ORDER BY NRH.ActionDate
GO
--End table eventlog.EventLog

--End file Build File - 05 - Conversion.sql

