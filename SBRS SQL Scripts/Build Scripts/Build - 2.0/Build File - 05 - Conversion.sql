USE SBRS_DHHS
GO

--Begin table person.Person
IF (SELECT OBJECT_ID('tempdb.person.#tPerson', 'U')) IS NOT NULL
	DROP TABLE person.#tPerson
--ENDIF

CREATE TABLE person.#tPerson 
	(
	PersonID INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(50),
	IsActive BIT,
	JobTitle VARCHAR(250),
	LastLoginDateTime DATETIME,
	LastName VARCHAR(50),
	OrganizationID INT,
	PhoneNumber VARCHAR(50)
	)

INSERT INTO person.#tPerson
	(EmailAddress, FirstName, IsActive, JobTitle, LastLoginDateTime, LastName, OrganizationID, PhoneNumber)
SELECT 
	D.EmailAddress, 
	D.FirstName, 
	D.IsActive, 
	D.JobTitle, 
	D.LastLoginDateTime, 
	D.LastName, 
	D.OrganizationID, 
	D.PhoneNumber
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY LOWER(U.Email) ORDER BY U.LastName, U.FirstName, U.Email, CASE WHEN AU.Status = 'A' OR U.Status = 'A' THEN 1 ELSE 0 END DESC, CASE WHEN AU.DateStamp >= U.UpdateDate THEN AU.DateStamp ELSE U.UpdateDate END DESC) AS RowIndex,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'vns7@cdc.gov' ELSE LOWER(U.Email) END AS EmailAddress,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'Corina' ELSE core.NullIfEmpty(migration.TitleCaseName(U.FirstName)) END AS FirstName,
		CASE WHEN AU.Status = 'A' OR U.Status = 'A' THEN 1 ELSE 0 END AS IsActive,
		core.NullIfEmpty(RTRIM(LTRIM(U.JobFunction))) AS JobTitle,
		CASE WHEN AU.DateStamp >= U.UpdateDate THEN AU.DateStamp ELSE U.UpdateDate END AS LastLoginDateTime,
		CASE WHEN LOWER(U.Email) IN ('corina.couch@fda.hhs.gov', 'vns7@cdc.gov') THEN 'Genson' ELSE core.NullIfEmpty(migration.TitleCaseName(U.LastName)) END AS LastName,
		U.OrgID AS OrganizationID, 
		core.NullIfEmpty(RTRIM(LTRIM(U.PhoneNumber))) AS PhoneNumber
	FROM HQITApps.dbo.Users U
		JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = U.UserID
			AND core.NullIfEmpty(AU.AccessLevel) IS NOT NULL
			AND core.NullIfEmpty(U.Email) IS NOT NULL
	) D
WHERE D.RowIndex = 1
	AND EmailAddress NOT LIKE '%nsitellc%'
	AND EmailAddress NOT LIKE '%stsllc%'
	AND EmailAddress NOT LIKE '%stech%'
	AND EmailAddress NOT IN ('christopher.crouch@nih.gov', 'guimondmi@mail.nih.gov')
ORDER BY D.LastName, D.FirstName, IsActive DESC, LastLoginDateTime DESC

UPDATE P
SET 
	P.FirstName = 'Berta',
	P.LastName = 'Biltz'
FROM person.#tPerson P
WHERE P.EmailAddress IN ('bbiltz@cdc.gov', 'bbiltz@cdc.gov')
GO

UPDATE P
SET 
	P.FirstName = 'Maxine',
	P.LastName = 'Dineyazhe'
FROM person.#tPerson P
WHERE P.EmailAddress = 'maxine.dineyazhe@ihs.gov'
GO

UPDATE P
SET 
	P.FirstName = 'Korriise',
	P.LastName = 'Laroche'
FROM person.#tPerson P
WHERE P.EmailAddress IN ('korriise.laroche@psc.hhs.gov', 'korriise.laroche@nih.gov')
GO

UPDATE P
SET 
	P.FirstName = 'Brian',
	P.LastName = 'Numkena'
FROM person.#tPerson P
WHERE P.EmailAddress = 'brian.numkena@ihs.gov'
GO

TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person
	(EmailAddress, OrganizationID, FirstName, LastName, PhoneNumber, JobTitle, Password, PasswordSalt)
VALUES
	('christopher.crouch@hhs.gov', 99, 'Chris', 'Crouch', '999-888-7777', 'Contractor', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@hhs.gov', 99, 'Todd', 'Pires', '555-2121', 'Contractor', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('christopher.crouch@cs.gov', 2, 'Chris', 'Crouch (CS)', '999-888-7777', 'Contract Specialist', '3D022F59444D1847400006E0D041C59157E04868584BEF5487', '407C678A-086D-4D94-9DEF-E29F4C4C9D24'),
	('christopher.crouch@co.gov', 2, 'Chris', 'Crouch (CO)', '999-888-7777', 'Contracting Officer', 'F5BC792D95BCE2F0C55784314713E83B0940CC9655DF983AB7', 'FDBE9682-586D-4CEA-8157-D0916028416F'),
	('christopher.crouch@sbs.gov', 2, 'Chris', 'Crouch (SBS)', '999-888-7777', 'Small Business Specialist', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('christopher.crouch@sbapcr.gov', 2, 'Chris', 'Crouch (SBAPCR)', '999-888-7777', 'SBA Procurement Center Representative', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('christopher.crouch@osdbu.gov', 2, 'Chris', 'Crouch (OSDBU)', '999-888-7777', 'OSDBU Director / Deputy Director', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('christopher.crouch@sbta.gov', 8, 'Chris', 'Crouch (SBTA)', '999-888-7777', 'Small Business Technical Advisor', 'B67FFBDFF31D66C473F18E228B69185B76CCA1211A36CE5257', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),

	('todd.pires@cs.gov', 8, 'Todd', 'Pires', '555-2121', 'CS', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@co.gov', 8, 'Todd', 'Pires', '555-2121', 'CO', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbta.gov', 8, 'Todd', 'Pires', '555-2121', 'SBTA', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbs.gov', 8, 'Todd', 'Pires', '555-2121', 'SBS', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@sbapcr.gov', 8, 'Todd', 'Pires', '555-2121', 'SBAPCR', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2'),
	('todd.pires@osdbu.gov', 8, 'Todd', 'Pires', '555-2121', 'OSDBU', 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF86', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2')
GO

TRUNCATE TABLE person.PersonOrganizationRole
GO

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 1, 99, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SysAdmin';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 2, 99, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SysAdmin';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 3, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 4, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CO';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 5, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 6, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 7, 2, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 8, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBTA';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 9, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 10, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'CO';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 11, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBTA';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 12, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 13, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 14, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';

INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 5, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBS';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 6, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SBAPCR';
INSERT INTO person.PersonOrganizationRole (PersonID, OrganizationID, RoleID) SELECT 7, 8, R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'OSDBU';
GO

INSERT INTO person.Person
	(EmailAddress, FirstName, IsActive, JobTitle, LastLoginDateTime, LastName, OrganizationID, PhoneNumber)
SELECT 
	D.EmailAddress, 
	D.FirstName, 
	D.IsActive, 
	D.JobTitle, 
	D.LastLoginDateTime, 
	D.LastName, 
	D.OrganizationID, 
	D.PhoneNumber
FROM
	(		
	SELECT 
		ROW_NUMBER() OVER (PARTITION BY P.LastName, P.FirstName ORDER BY P.LastName, P.FirstName, P.IsActive DESC, P.LastLoginDateTime DESC) AS RowIndex,
		P.EmailAddress, 
		P.FirstName, 
		P.IsActive, 
		P.JobTitle, 
		P.LastLoginDateTime, 
		P.LastName, 
		P.OrganizationID, 
		P.PhoneNumber
	FROM person.#tPerson P
	) D
WHERE D.RowIndex = 1
GO

DROP TABLE person.#tPerson
GO
--End table person.Person

--Begin table migration.UserMigration
TRUNCATE TABLE migration.UserMigration
GO

INSERT INTO migration.UserMigration
	(EmailAddress, FirstName, LastName, Password, UserID)
SELECT 
	LOWER(U.Email) AS EmailAddress, 
	core.NullIfEmpty(migration.TitleCaseName(U.FirstName)) AS FirstName,
	core.NullIfEmpty(migration.TitleCaseName(U.LastName)) AS LastName,
	U.Password, 
	U.UserID
FROM HQITApps.dbo.Users U
	JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = U.UserID
		AND core.NullIfEmpty(AU.AccessLevel) IS NOT NULL
		AND core.NullIfEmpty(U.Email) IS NOT NULL
		AND LOWER(U.Email) NOT LIKE '%nsitellc%'
		AND LOWER(U.Email) NOT LIKE '%stsllc%'
		AND LOWER(U.Email) NOT LIKE '%stech%'
		AND LOWER(U.Email) NOT IN ('christopher.crouch@nih.gov', 'guimondmi@mail.nih.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Berta',
	UM.LastName = 'Biltz'
FROM migration.UserMigration UM
WHERE UM.EmailAddress IN ('bbiltz@cdc.gov', 'bbiltz@cdc.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Maxine',
	UM.LastName = 'Dineyazhe'
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'maxine.dineyazhe@ihs.gov'
GO

UPDATE UM
SET 
	UM.FirstName = 'Korriise',
	UM.LastName = 'Laroche'
FROM migration.UserMigration UM
WHERE UM.EmailAddress IN ('korriise.laroche@psc.hhs.gov', 'korriise.laroche@nih.gov')
GO

UPDATE UM
SET 
	UM.FirstName = 'Brian',
	UM.LastName = 'Numkena'
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'brian.numkena@ihs.gov'
GO

UPDATE UM
SET UM.PersonID = P.PersonID
FROM migration.UserMigration UM
	JOIN person.Person P ON P.EmailAddress = UM.EmailAddress
GO

UPDATE UM
SET UM.PersonID = P.PersonID
FROM migration.UserMigration UM
	JOIN person.Person P ON P.LastName + ', ' + P.FirstName = UM.LastName + ', ' + UM.FirstName
		AND UM.PersonID = 0
GO

UPDATE UM
SET UM.PersonID = ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.EmailAddress = 'vns7@cdc.gov'), 0)
FROM migration.UserMigration UM
WHERE UM.EmailAddress = 'corina.couch@fda.hhs.gov'
	AND UM.PersonID = 0
GO
--End table migration.UserMigration

--Begin table person.PersonOrganizationRole
;
WITH PROD AS
	(
	SELECT DISTINCT
		UM.PersonID,
		P.OrganizationID,

		CASE
			WHEN AU.AccessLevel LIKE 'OSDBU%'
			THEN ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode LIKE 'OSDBU%'), 0)
			WHEN CHARINDEX(',', AU.AccessLevel) = 0
			THEN ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleName = AU.AccessLevel), 0)
			ELSE ISNULL((SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleName = LEFT(AU.AccessLevel, CHARINDEX(',', AU.AccessLevel) - 1)), 0)
		END AS RoleID

	FROM migration.UserMigration UM
		JOIN HQITApps.dbo.ApplicationUsers AU ON AU.UserID = UM.UserID
		JOIN HQITApps.dbo.Users U ON U.UserID = UM.UserID
		JOIN person.Person P ON P.PersonID = UM.PersonID
	)

INSERT INTO person.PersonOrganizationRole
	(PersonID, OrganizationID, RoleID)
SELECT
	PROD.PersonID,
	PROD.OrganizationID,
	PROD.RoleID
FROM PROD

UNION

SELECT
	UM.PersonID,
	MOU.OrgId,
	PROD.RoleID
FROM HQITApps.dbo.MultipleOrgUsers MOU
	JOIN migration.UserMigration UM ON MOU.UserID = UM.UserID
	JOIN PROD ON PROD.PersonID = UM.PersonID
ORDER BY 1, 2, 3
		
UPDATE POR
SET POR.RoleID = (SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'SYSAdmin')
FROM person.PersonOrganizationRole POR
	JOIN person.Person P ON P.PersonID = POR.PersonID
		AND 
			(
				(P.LastName = 'Underwood' AND P.FirstName = 'Ronald')
				OR (P.LastName = 'Horne' AND P.FirstName = 'Karisma')
			)
--End table person.PersonOrganizationRole

--Begin table reviewform.ReviewForm
TRUNCATE TABLE reviewform.ReviewForm
GO

SET IDENTITY_INSERT reviewform.ReviewForm ON
GO

INSERT INTO reviewform.ReviewForm
	(
	ReviewFormID,
	ControlCode,
	RequisitionNumber1,
	RequisitionNumber2,
	COACSID,
	FundingOrganizationID,
	FundingOrganizationName,
	BasePrice, 
	PeriodOfPerformanceStartDate,
	PeriodOfPerformanceEndDate,
	NAICSID,
	SizeTypeID,
	Size,
	SynopsisID,
	SynopsisExceptionReasonID,
	Description,
	ProcurementHistoryID,
	ProcurementHistoryContractAwardAmount,
	ProcurementHistoryContractAwardDate, 
	ProcurementHistoryContractEndDate, 
	ProcurementHistoryContractNumber, 
	ProcurementHistoryContractTerminationID, 
	ProcurementHistoryContractTerminationReason, 
	OtherResearch,
	BundleTypeID,
	PriorBundlerID,
	SubContractingPlanID,
	CreatePersonID, 
	CreateDateTime,
	OriginatingOrganizationID,
	IsActive
	)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	SBRSF.ReviewNo AS ControlCode,
	core.NullIfEmpty(SBRSF.RequistionNo) AS RequisitionNumber1,
	core.NullIfEmpty(SBRSF.OtherReqNo) AS RequisitionNumber2,
	ISNULL((SELECT C.COACSID FROM dropdown.COACS C WHERE C.COACSCode = SBRSF.CoacsCode), 0) AS COACSID,
	ISNULL((SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationDisplayCode = SBRSF.BuyerOffice), 0) AS FundingOrganizationID,
	core.NullIfEmpty(SBRSF.NonHHSFundingOffice) AS FundingOrganizationName,
	BasePrice,
	CASE WHEN ISDATE(SBRSF.ProjAwardDate) = 1 THEN SBRSF.ProjAwardDate ELSE NULL END AS PeriodOfPerformanceStartDate,
	CASE WHEN ISDATE(SBRSF.ProjAwardToDate) = 1 THEN SBRSF.ProjAwardToDate ELSE NULL END AS PeriodOfPerformanceEndDate,
	ISNULL((SELECT N.NAICSID FROM dropdown.NAICS N WHERE N.NAICSCode = SBRSF.NAICS), 0) AS NAICSID,
	SBRSF.SizeType AS SizeTypeID,
	CAST(ISNULL(migration.ConvertCharToInt(SBRSF.Size), 0) AS VARCHAR(50)) AS Size,
	SBRSF.SynopsisType AS SynopsisID,
	SBRSF.Exception AS SynopsisExceptionReasonID,
	core.NullIfEmpty(SBRSF.Description) AS Description,
	ISNULL(SBRSF.History, 0) AS ProcurementHistoryID,
	ISNULL(SBRSF.ContractAwardAmount, 0) AS ProcurementHistoryContractAwardAmount,
	CASE WHEN ISDATE(SBRSF.ContractDate) = 1 THEN SBRSF.ContractDate ELSE NULL END AS ProcurementHistoryContractAwardDate,
	CASE WHEN ISDATE(SBRSF.Completion) = 1 THEN SBRSF.Completion ELSE NULL END AS ProcurementHistoryContractEndDate,
	core.NullIfEmpty(SBRSF.ContractNo) AS ProcurementHistoryContractNumber,
	ISNULL(SBRSF.Terminate, 0) AS ProcurementHistoryContractTerminationID,
	core.NullIfEmpty(SBRSF.Terminated) AS ProcurementHistoryContractTerminationReason,
	core.NullIfEmpty(SBRSF.OtherResearch) AS OtherResearch,
	ISNULL(SBRSF.Bundle, 0) AS BundleTypeID,
	ISNULL(SBRSF.SBconcern, 0) AS PriorBundlerID,
	ISNULL(SBRSF.Subcontract, 0) AS SubContractingPlanID,
	ISNULL((SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.UserID = SBRSF.UserID), 0) AS CreatePersonID,
	SBRSF.EntryDate AS CreateDateTime,
	SBRSF.OrgID AS OriginatingOrganizationID,
	CASE WHEN SBRSF.Displayed = 'Y' THEN 1 ELSE 0 END AS IsActive
FROM SBReview.dbo.SBRSForms SBRSF
GO

SET IDENTITY_INSERT reviewform.ReviewForm OFF
GO

UPDATE RF
SET 
	RF.PeriodOfPerformanceEndDate = CASE WHEN RF.PeriodOfPerformanceEndDate = '1900-01-01' THEN NULL ELSE RF.PeriodOfPerformanceEndDate END,
	RF.PeriodOfPerformanceStartDate = CASE WHEN RF.PeriodOfPerformanceStartDate = '1900-01-01' THEN NULL ELSE RF.PeriodOfPerformanceStartDate END,
	RF.ProcurementHistoryContractAwardDate = CASE WHEN RF.ProcurementHistoryContractAwardDate = '1900-01-01' THEN NULL ELSE RF.ProcurementHistoryContractAwardDate END,
	RF.ProcurementHistoryContractEndDate = CASE WHEN RF.ProcurementHistoryContractEndDate = '1900-01-01' THEN NULL ELSE RF.ProcurementHistoryContractEndDate END,
	RF.Size = CASE WHEN RIGHT(RF.Size, 3) = '.00' THEN REPLACE(RF.Size, '.00', '') ELSE RF.Size END
FROM reviewform.ReviewForm RF
GO

--Begin set WorkflowID
WITH RFD AS 
	(
	SELECT
		RF.ReviewFormID,

		CASE
			WHEN O.OrganizationCode NOT IN ('IHS', 'NON')
			THEN 'HHS'
			ELSE O.OrganizationCode 
		END AS OrganizationCode,

		CASE
			WHEN reviewform.GetTotalEstimatedValue(RF.ReviewFormID) < 150000
				OR EXISTS (SELECT 1 FROM reviewform.ReviewFormProcurementMethod RFPM JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID AND PM.IsSetAside = 1 AND RFPM.ReviewFormID = RF.ReviewFormID)
			THEN 'Level1'
			WHEN reviewform.GetTotalEstimatedValue(RF.ReviewFormID) >= 5000000
			THEN 'Level3'
			ELSE 'Level2'
		END AS LevelCode

	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
	)

UPDATE RF
SET RF.WorkflowID = W.WorkflowID
FROM reviewform.ReviewForm RF
	JOIN RFD ON RFD.ReviewFormID = RF.ReviewFormID
	JOIN workflow.Workflow W ON W.OrganizationCode = RFD.OrganizationCode
		AND W.LevelCode = RFD.LevelCode
GO
--End set WorkflowID

--Begin set StatusID
WITH SD AS
	(
	SELECT 
		D.ReviewNo,
		S.StatusID
	FROM
		(
		SELECT 
			SF.ReviewNo,
		
			CASE
				WHEN SF.Status IN ('Exist', 'New', 'Re-submit')
				THEN 'In Progress'
				WHEN SF.Status LIKE '%Review' OR SF.Status = 'Back to SBS'
				THEN 'Under Review'
				WHEN SF.Status = 'Withdraw'
				THEN 'Withdrawn'
				ELSE core.NullIfEmpty(SF.Status)
			END AS DerivedStatus

		FROM SBReview.dbo.SBRSForms SF
		) D 
			JOIN dropdown.Status S ON S.StatusName = D.DerivedStatus
	)

UPDATE RF
SET RF.StatusID = SD.StatusID
FROM reviewform.ReviewForm RF
	JOIN SD ON SD.ReviewNo = RF.ControlCode
GO
--End set StatusID

--Begin set AssignedPersonID
UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = RTRIM(LTRIM(REPLACE(SF.CurrentQ, '  ', ' ')))), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND RF.AssignedPersonID = 0
		AND SF.CurrentQ <> 'Closeout'
		AND S.StatusCode <> 'Complete'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'Joyce Thurmond'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Joyce Thurman'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'STEPHEN LESTER'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Lester Stephen'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'margaret shirley-damon'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'margaret shirley'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.IsActive = 1 AND P.FirstName + ' ' + P.LastName = 'Louis Anderson'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Anderson Louis'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = SF.CurrentQ), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'James Josey'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = 'Ralph Vallone'), 0)
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ = 'Ralp Vallone'
GO

UPDATE RF
SET RF.AssignedPersonID = ISNULL((SELECT TOP 1 UM.PersonID FROM migration.UserMigration UM WHERE UM.FirstName + ' ' + UM.LastName = 'Berta Biltz'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode = '2014-CDC-0581'
GO
--End set AssignedPersonID

--Begin set AssignedPersonID & StatusID
UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.StatusID = (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor')
FROM reviewform.ReviewForm RF
	JOIN SBReview.dbo.SBRSForms SF ON SF.ReviewNo = RF.ControlCode
		AND SF.CurrentQ IN ('Annette Owens-Scarboro', 'crouch SBS', 'Nishchala Kodali', 'Test CO')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'BackToAuthor'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-NIH-0938','2015-PSC-0071','2015-PSC-0077','2016-CDC-0297','2016-CDC-0469','2016-FDA-0154','2016-IHS-0376','2016-IHS-0529','2016-NIH-0710')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Complete'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2014-CDC-0213','2014-FDA-0256','2014-IHS-0001','2014-PSC-0116','2015-CDC-0653','2015-CDC-0959','2015-IHS-0041','2015-IHS-0045','2015-IHS-0100','2015-IHS-0171','2015-IHS-0188','2015-IHS-0226','2015-IHS-0234','2015-IHS-0266','2015-IHS-0283','2015-IHS-0302','2015-IHS-0344','2015-NIH-0027','2015-NIH-0122','2015-NIH-0200','2015-NIH-0286','2015-NIH-1315','2015-NIH-1568','2015-NIH-2016','2015-SAM-0003','2015-SAM-0008','2016-APR-0017','2016-APR-0035','2016-APR-0037','2016-APR-0072','2016-CDC-0203','2016-CDC-0538','2016-CDC-0560','2016-CDC-0635','2016-CDC-0745','2016-CDC-0953','2016-CDC-1104','2016-CMS-0087','2016-FDA-0045','2016-FDA-0121','2016-FDA-0138','2016-FDA-0145','2016-FDA-0147','2016-FDA-0259','2016-HRS-0020','2016-HRS-0025','2016-HRS-0072','2016-HRS-0091','2016-IHS-0071','2016-IHS-0151','2016-IHS-0172','2016-IHS-0292','2016-IHS-0298','2016-IHS-0300','2016-IHS-0318','2016-IHS-0324','2016-IHS-0326','2016-IHS-0334','2016-IHS-0342','2016-IHS-0350','2016-IHS-0369','2016-IHS-0414','2016-IHS-0419','2016-IHS-0505','2016-IHS-0539','2016-IHS-0550','2016-IHS-0572','2016-IHS-0583','2016-IHS-0597','2016-IHS-0598','2016-IHS-0629','2016-IHS-0647','2016-IHS-0690','2016-IHS-0716','2016-IHS-0726','2016-IHS-0729','2016-IHS-0730','2016-IHS-0756','2016-IHS-0774','2016-IHS-0800','2016-IHS-0861','2016-IHS-0870','2016-IHS-0905','2016-IHS-0917','2016-IHS-0939','2016-IHS-0941','2016-IHS-0952','2016-IHS-0955','2016-IHS-0957','2016-IHS-0985','2016-IHS-0987','2016-IHS-1000','2016-IHS-1014','2016-IHS-1025','2016-IHS-1049','2016-IHS-1065','2016-IHS-1077','2016-IHS-1078','2016-IHS-1080','2016-IHS-1082','2016-IHS-1083','2016-IHS-1088','2016-IHS-1119','2016-NIH-0077','2016-NIH-0561','2016-NIH-0641','2016-NIH-0712','2016-NIH-0763','2016-NIH-0782','2016-NIH-0967','2016-NIH-0977','2016-NIH-1035','2016-NIH-1222','2016-NIH-1242','2016-NIH-1452','2016-NIH-1705','2016-NIH-1777','2016-NIH-2076','2016-NIH-2095','2016-PSC-0020','2016-PSC-0059','2017-CDC-0044','2017-FDA-0044','2017-IHS-0001','2017-IHS-0002','2017-IHS-0003','2017-IHS-0004','2017-IHS-0029','2017-IHS-0030','2017-IHS-0031','2017-IHS-0033','2017-IHS-0041','2017-IHS-0042','2017-IHS-0061','2017-IHS-0066','2017-IHS-0134','2017-IHS-0158','2017-IHS-0292','2017-IHS-0513','2017-NIH-0022','2017-NIH-0158','2017-NIH-0187','2017-NIH-0240','2017-NIH-0309','2017-NIH-0388','2017-NIH-0516','2017-NIH-0518','2017-PSC-0092','2017-SAM-0002')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Rosanna Browning'), 0),
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2014-PSC-0010')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Barbara Taylor'), 0),
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2017-NIH-0104')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Dwight Deneal'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-APR-0027')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Gina Jackson'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0068')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Jerry Black'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2015-IHS-0122')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.EmailAddress = 'john.stout@ihs.gov'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2017-IHS-0325')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Kathy Harrigan'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0086')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Maxine Dineyazhe'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-IHS-0063')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicola Carmichael'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0070')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicola Carmichael'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0116')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Nicole Stevenson'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-FDA-0063')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = ISNULL((SELECT TOP 1 P.PersonID FROM person.Person P WHERE P.FirstName + ' ' + P.Lastname = 'Theresa Routh-Murphy'), 0),	
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'UnderReview'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-CDC-0311')
GO

UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.StatusID = ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Withdrawn'), 0)
FROM reviewform.ReviewForm RF
WHERE RF.ControlCode IN ('2016-NIH-0871')
GO
--End set AssignedPersonID & StatusID

--Begin set AssignedPersonID & WorkflowStepNumber
UPDATE RF
SET 
	RF.AssignedPersonID = 0,
	RF.WorkflowStepNumber = ISNULL((SELECT COUNT(WS.WorkflowStepID) FROM workflow.WorkflowStep WS WHERE WS.WorkflowID = RF.WorkflowID), 0)
FROM reviewform.ReviewForm RF 
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND S.StatusCode = 'Complete'
GO

UPDATE RF
SET 
	RF.AssignedPersonID = RF.CreatePersonID,
	RF.WorkflowStepNumber = reviewform.GetAuthorWorkflowStepNumberByReviewFormID(RF.ReviewFormID)
FROM reviewform.ReviewForm RF 
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND S.StatusCode = 'BackToAuthor'
GO
--End set AssignedPersonID & WorkflowStepNumber

--Begin set WorkflowStepNumber
WITH WSN AS
	(
	SELECT
		RF.ReviewFormID,
		migration.GetAssigneeWorkflowStepNumberByReviewFormID(RF.ReviewFormID) AS WorkflowStepNumber
	FROM reviewform.ReviewForm RF 
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND S.StatusCode IN ('BackToAuthor', 'InProgress', 'UnderReview')
	)

UPDATE RF
SET RF.WorkflowStepNumber = WSN.WorkflowStepNumber
FROM reviewform.ReviewForm RF 
	JOIN WSN ON WSN.ReviewFormID = RF.ReviewFormID
GO
--End set WorkflowStepNumber
--End table reviewform.ReviewForm

--Begin final pass on person.Person for name swapping
UPDATE P
SET 
	P.FirstName = P.LastName,
	P.LastName = P.FirstName
FROM person.Person P
WHERE P.EmailAddress IN 
	(
	'allison.lempka@psc.hhs.gov',
	'alphonso.johnson@psc.hhs.gov',
	'alphonso.simmons@fda.hhs.gov',
	'anne.mastrincola@sba.gov',
	'annette.merrion@dm.usdo.gov',
	'anthony.terrell@psc.hhs.gov',
	'arlene.belindo@ihs.gov',
	'ashley.flynn@ihs.gov',
	'azeb.mengistu@psc.hhs.gov',
	'bernard.durham@sba.gov',
	'billy.castleberry@psc.hhs.gov',
	'brad.engel@nih.gov',
	'brendan.miller2@nih.gov',
	'brent.owens@hill.af.mil',
	'bryan.daines@psc.hhs.gov',
	'caleb.owen@hhs.gov',
	'carla.mosley@psc.hhs.gov',
	'carol.diaz@ihs.gov',
	'carol.thompson@sba.gov',
	'chad.martin@ihs.gov',
	'christine.russman@fda.hhs.gov',
	'colleen.burns@sba.gov',
	'dan.finley@ihs.gov',
	'daniel.federline@nih.gov',
	'daniel.hall@nih.gov',
	'darin.deschiney@ihs.gov',
	'david.spicer@nih.gov',
	'debbie.hope@psc.hhs.gov',
	'debra.hoffman@cms.hhs.gov',
	'diane.frasier@nih.hhs.gov',
	'dina.inhulsen@sba.gov',
	'douglas.gerard@sba.gov',
	'elliott.sloan@cms.hhs.gov',
	'gary.heard@sba.gov',
	'gary.klaff@fda.hhs.gov',
	'george.dupin@sba.gov',
	'gregory.benedict@nih.gov',
	'haig.altunian@psc.hhs.gov',
	'hing.wong@nih.gov',
	'howard.parker@doe.gov',
	'howard.yablon@fda.hhs.gov',
	'isaiah.hugs@ihs.gov',
	'ivette.bascumbe.mesa@sba.gov',
	'james.gambardella@sba.gov',
	'janette.fasano@sba.gov',
	'jasmine.ruffin@nih.gov',
	'jeremy.steel@ihs.gov',
	'john.bagaason@sba.gov',
	'john.stout@ihs.gov',
	'jose.martinez@sba.gov',
	'joyce.spears@sba.gov',
	'karen.zeiter@fda.hhs.gov',
	'katherine.ragland@sba.gov',
	'katherine.renville@ihs.gov',
	'keith.harding@psc.hhs.gov',
	'keith.waye@sba.gov',
	'kelley.williams-vollmer@cms.hhs.gov',
	'ken.parrish@ihs.gov',
	'kevin.michael@sba.gov',
	'kimesha.leake@nih.gov',
	'laurence.orr@navy.mil',
	'leann.delaney@sba.gov ',
	'licinda.peters@cms.hhs.gov',
	'lisa.bielen@nih.gov',
	'lydina.battle@psc.hhs.gov',
	'm.manzanares@sba.gov',
	'maria.galloway@sba.gov',
	'marichu.relativo@sba.gov',
	'martina.williams@do.treas.gov',
	'michael.cecere@us.army.mil',
	'michael.gemmill@nih.gov',
	'michelle.creenan@fda.hhs.gov',
	'michelle.street@hhs.gov',
	'nathan.amador@cdc.hhs.gov',
	'nicole.craig@fda.hhs.gov',
	'pamela.beavers@sba.gov',
	'pamela.daugherty@nih.gov',
	'pamela.m.thompson24.civ@mail.nih',
	'paul.chann@sba.gov',
	'paul.p.stone@usace.army.mil',
	'randall.johnston@sba.gov',
	'randy.marchiafava@usace.army.mil',
	'rhonda.duncan@psc.hhs.gov',
	'ricardo.corderocruz@nih.gov',
	'robert.c.taylor@sba.gov',
	'robert.sim@ihs.gov',
	'ronald.robinson@psc.hhs.gov',
	'rose.weston@ihs.gov',
	'sally.walton@doeal.gov',
	'shalisha.harris@psc.hhs.gov',
	'shannon.sartin@iha.gov',
	'sharmaine.fagan-kerr@nih.gov',
	'shaun.miles@nih.gov',
	'sheena.little@sba.gov',
	'skye.duffner@nih.gov',
	'sonya.owens-cobblah@nih.gov',
	'tanika.pierce@sba.gov',
	'teresa.shook@sba.gov',
	'thomas.krusemark@sba.gov',
	'thomas.vanhorne@sba.gov',
	'tiffany.thornton@psc.hhs.gov',
	'todd.cole@nih.hhs.gov',
	'tracy.scott@nih.gov',
	'valerie.j.coleman@nasa.gov'
	)
GO
--End final pass on person.Person for name swapping

--Begin table reviewform.ReviewFormContract
TRUNCATE TABLE reviewform.ReviewFormContract
GO

DECLARE	@cContract1No VARCHAR(100)
DECLARE	@cContract2No VARCHAR(100)
DECLARE	@cContract3No VARCHAR(100)
DECLARE	@cContract4No VARCHAR(100)
DECLARE	@cContract5No VARCHAR(100)
DECLARE	@cContract6No VARCHAR(100)
DECLARE	@nReviewFormID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		SBRSF.ReviewID AS ReviewFormID, 
		core.NullIfEmpty(SBRSF.Contract1No), 
		core.NullIfEmpty(SBRSF.Contract2No), 
		core.NullIfEmpty(SBRSF.Contract3No), 
		core.NullIfEmpty(SBRSF.Contract4No), 
		core.NullIfEmpty(SBRSF.Contract5No), 
		core.NullIfEmpty(SBRSF.Contract6No)
	FROM SBReview.dbo.SBRSForms SBRSF

OPEN oCursor
FETCH oCursor INTO @nReviewFormID, @cContract1No, @cContract2No, @cContract3No, @cContract4No, @cContract5No, @cContract6No
WHILE @@fetch_status = 0
	BEGIN

	IF @cContract1No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract1No) 
	--ENDIF
	IF @cContract2No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract2No) 
	--ENDIF
	IF @cContract3No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract3No) 
	--ENDIF
	IF @cContract4No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract4No) 
	--ENDIF
	IF @cContract5No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract5No) 
	--ENDIF
	IF @cContract6No IS NOT NULL
		INSERT INTO reviewform.ReviewFormContract (ReviewFormID, ContractNumber) VALUES (@nReviewFormID, @cContract6No) 
	--ENDIF

	FETCH oCursor INTO @nReviewFormID, @cContract1No, @cContract2No, @cContract3No, @cContract4No, @cContract5No, @cContract6No
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End table reviewform.ReviewFormContract

--Begin table reviewform.ReviewFormDocument
TRUNCATE TABLE reviewform.ReviewFormDocument
GO

INSERT INTO reviewform.ReviewFormDocument
	(ReviewFormID, CreateDateTime, CreatePersonID, DocumentPath, DocumentName, DocumentTypeID, FileGUID, IsActive, IsFileGUIDFileName)
SELECT
	RF.ReviewFormID,
	A.EntryDate AS CreateDateTime,
	ISNULL((SELECT P.PersonID FROM person.Person P JOIN HQITAPPS.dbo.Users U ON LOWER(U.Email) = P.EmailAddress AND U.UserID = A.UserID), 0) AS CreatePersonID,
	'/' + A.FilePath AS DocumentPath,
	A.FileContent AS DocumentName,
	ISNULL((SELECT DT.DocumentTypeID FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = A.DocType), 0) AS DocumentTypeID,
	newID() AS FileGUID,

	CASE
		WHEN A.Status = 'A'
		THEN 1
		ELSE 0
	END AS IsActive,

	0 AS IsFileGUIDFileName
FROM SBReview.dbo.Attachments A
	JOIN reviewform.ReviewForm RF ON RF.ControlCode = A.ReviewNo
ORDER BY A.ReviewNo, A.EntryDate
GO
--End table reviewform.ReviewFormDocument

--Begin table reviewform.ReviewFormOptionYear
TRUNCATE TABLE reviewform.ReviewFormOptionYear
GO

INSERT INTO reviewform.ReviewFormOptionYear
	(ReviewFormID, OptionYearValue)

SELECT
	ReviewFormID,
	OptionYearValue
FROM
	(
	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		CASE WHEN SBRSF.OptionPrice = 0 THEN ISNULL(SBRSF.Option1Price, 0) ELSE SBRSF.OptionPrice END AS OptionYearValue,
		1 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.OptionPrice > 0 
		OR ISNULL(SBRSF.Option1Price, 0) > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option2Price AS OptionYearValue,
		2 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option2Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option3Price AS OptionYearValue,
		3 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option3Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option4Price AS OptionYearValue,
		4 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option4Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option5Price AS OptionYearValue,
		5 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option5Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option6Price AS OptionYearValue,
		6 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option6Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option7Price AS OptionYearValue,
		7 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option7Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option8Price AS OptionYearValue,
		8 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option8Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option9Price AS OptionYearValue,
		9 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option9Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option10Price AS OptionYearValue,
		10 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option10Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option11Price AS OptionYearValue,
		11 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option11Price > 0

	UNION ALL

	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		SBRSF.Option12Price AS OptionYearValue,
		12 AS SequenceNumber
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE SBRSF.Option12Price > 0
	) D
ORDER BY D.ReviewFormID, D.SequenceNumber
GO

;
WITH PD AS 
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY RFOY.ReviewFormID ORDER BY RFOY.ReviewFormOptionYearID) AS RowIndex,
		RFOY.ReviewFormOptionYearID
	FROM reviewform.ReviewFormOptionYear RFOY
	)

UPDATE RFOY
SET RFOY.OptionYear = PD.RowIndex
FROM reviewform.ReviewFormOptionYear RFOY
	JOIN PD ON PD.ReviewFormOptionYearID = RFOY.ReviewFormOptionYearID
GO
--End table reviewform.ReviewFormOptionYear

--Begin table reviewform.ReviewFormPointOfContact
TRUNCATE TABLE reviewform.ReviewFormPointOfContact
GO

INSERT INTO reviewform.ReviewFormPointOfContact 
	(ReviewFormID, PointOfContactName)
SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	core.NullIfEmpty(SBRSF.BuyerName)
FROM SBReview.dbo.SBRSForms SBRSF
WHERE CHARINDEX(';', SBRSF.BuyerName) = 0
ORDER BY 1

DECLARE	@cPointOfContactName VARCHAR(50)
DECLARE	@nReviewFormID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		SBRSF.ReviewID AS ReviewFormID,
		core.NullIfEmpty(SBRSF.BuyerName)
	FROM SBReview.dbo.SBRSForms SBRSF
	WHERE CHARINDEX(';', SBRSF.BuyerName) > 0

OPEN oCursor
FETCH oCursor INTO @nReviewFormID, @cPointOfContactName
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO reviewform.ReviewFormPointOfContact 
		(ReviewFormID, PointOfContactName)
	SELECT DISTINCT
		@nReviewFormID,
		LTRIM(RTRIM(LTT.ListItem)) AS PointOfContactName
	FROM core.ListToTable(@cPointOfContactName, ';') LTT
	ORDER BY 1

	FETCH oCursor INTO @nReviewFormID, @cPointOfContactName
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE RFPOC
SET RFPOC.PointOfContactPhone = (SELECT TOP 1 core.NullIfEmpty(U.PhoneNumber) FROM HQITApps.dbo.Users U WHERE LTRIM(RTRIM(LTRIM(RTRIM(U.FirstName)) + ' ' + LTRIM(RTRIM(U.LastName)))) = RFPOC.PointOfContactName AND U.PhoneNumber IS NOT NULL)
FROM reviewform.ReviewFormPointOfContact RFPOC
GO

UPDATE RFPOC
SET RFPOC.PointOfContactEmailAddress = (SELECT TOP 1 core.NullIfEmpty(LOWER(U.Email)) FROM HQITApps.dbo.Users U WHERE LTRIM(RTRIM(LTRIM(RTRIM(U.FirstName)) + ' ' + LTRIM(RTRIM(U.LastName)))) = RFPOC.PointOfContactName AND U.Email IS NOT NULL)
FROM reviewform.ReviewFormPointOfContact RFPOC
GO
--End table reviewform.ReviewFormPointOfContact

--Begin table reviewform.ReviewFormProcurementHistory
TRUNCATE TABLE reviewform.ReviewFormProcurementHistory
GO

DECLARE @tTable TABLE
	(
	ReviewFormID INT,
	ProcurementMethodID1 INT,
	Percentage1 INT,
	ProcurementMethodID2 INT,
	Percentage2 INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

INSERT INTO @tTable
	(ReviewFormID, ProcurementMethodID1, Percentage1, ProcurementMethodID2, Percentage2, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.PreMethodType1), 0) AS ProcurementMethodID,
	ISNULL(SBRSF.PreMType1pcnt, 0) AS Percentage,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.PreMethodType2), 0) AS ProcurementMethodID2,
	ISNULL(SBRSF.PreMType2pcnt, 0) AS Percentage2,
	ISNULL(SBRSF.PreJustify, 0) AS FARJustificationID,
	ISNULL(SBRSF.PreLimitRsc, 0) AS GSAJustificationID,
	core.NullIfEmpty(SBRSF.PreScheduleNo) AS GSAScheduleNumber,
	core.NullIfEmpty(SBRSF.PreTOdescr) AS TaskOrderDescription,
	core.NullIfEmpty(SBRSF.PreTaksOrderNo) AS TaskOrderNumber,
	core.NullIfEmpty(SBRSF.PreUPother) AS UnrestrictedProcurementReason,
	ISNULL(SBRSF.PreUPreason, 0) AS UnrestrictedProcurementReasonID
FROM SBReview.dbo.SBRSForms SBRSF
ORDER BY 1

UPDATE T
SET T.ProcurementMethodID1 = 0
FROM @tTable T
WHERE T.Percentage1 = 0

UPDATE T
SET T.ProcurementMethodID2 = 0
FROM @tTable T
WHERE T.Percentage2 = 0

INSERT INTO reviewform.ReviewFormProcurementHistory
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID1,
	T.Percentage1,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID1 > 0
	AND T.Percentage1 > 0

INSERT INTO reviewform.ReviewFormProcurementHistory
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID2,
	T.Percentage2,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID2 > 0
	AND T.Percentage2 > 0
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID NOT IN (2,3,12,13)
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 2
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 3
GO

UPDATE RFPH
SET
	RFPH.FARJustificationID = 0,
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 12
GO

UPDATE RFPH
SET
	RFPH.GSAJustificationID = 0,
	RFPH.GSAScheduleNumber = NULL,
	RFPH.TaskOrderDescription = NULL,
	RFPH.TaskOrderNumber = NULL,
	RFPH.UnrestrictedProcurementReason = NULL,
	RFPH.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementHistory RFPH
WHERE RFPH.ProcurementMethodID = 13
GO
--End table reviewform.ReviewFormProcurementHistory

--Begin table reviewform.ReviewFormProcurementMethod
TRUNCATE TABLE reviewform.ReviewFormProcurementMethod
GO

DECLARE @tTable TABLE
	(
	ReviewFormID INT,
	ProcurementMethodID1 INT,
	Percentage1 INT,
	ProcurementMethodID2 INT,
	Percentage2 INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

INSERT INTO @tTable
	(ReviewFormID, ProcurementMethodID1, Percentage1, ProcurementMethodID2, Percentage2, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	SBRSF.ReviewID AS ReviewFormID,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.MethodType1), 0) AS ProcurementMethodID,
	ISNULL(SBRSF.MType1pcnt, 0) AS Percentage,
	ISNULL((SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodName = SBRSF.MethodType2), 0) AS ProcurementMethodID2,
	ISNULL(SBRSF.MType2pcnt, 0) AS Percentage2,
	ISNULL(SBRSF.Justify, 0) AS FARJustificationID,
	ISNULL(SBRSF.LimitRsc, 0) AS GSAJustificationID,
	core.NullIfEmpty(SBRSF.ScheduleNo) AS GSAScheduleNumber,
	core.NullIfEmpty(SBRSF.TOdescr) AS TaskOrderDescription,
	core.NullIfEmpty(SBRSF.TaksOrderNo) AS TaskOrderNumber,
	core.NullIfEmpty(SBRSF.UPother) AS UnrestrictedProcurementReason,
	ISNULL(SBRSF.UPreason, 0) AS UnrestrictedProcurementReasonID
FROM SBReview.dbo.SBRSForms SBRSF
ORDER BY 1

UPDATE T
SET T.ProcurementMethodID1 = 0
FROM @tTable T
WHERE T.Percentage1 = 0

UPDATE T
SET T.ProcurementMethodID2 = 0
FROM @tTable T
WHERE T.Percentage2 = 0

INSERT INTO reviewform.ReviewFormProcurementMethod
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID1,
	T.Percentage1,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID1 > 0
	AND T.Percentage1 > 0

INSERT INTO reviewform.ReviewFormProcurementMethod
	(ReviewFormID, ProcurementMethodID, Percentage, FARJustificationID, GSAJustificationID, GSAScheduleNumber, TaskOrderDescription, TaskOrderNumber, UnrestrictedProcurementReason, UnrestrictedProcurementReasonID)
SELECT
	T.ReviewFormID,
	T.ProcurementMethodID2,
	T.Percentage2,
	T.FARJustificationID,
	T.GSAJustificationID,
	T.GSAScheduleNumber,
	T.TaskOrderDescription,
	T.TaskOrderNumber,
	T.UnrestrictedProcurementReason,
	T.UnrestrictedProcurementReasonID
FROM @tTable T
WHERE T.ProcurementMethodID2 > 0
	AND T.Percentage2 > 0
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID NOT IN (2,3,12,13)
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 2
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 3
GO

UPDATE RFPM
SET
	RFPM.FARJustificationID = 0,
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 12
GO

UPDATE RFPM
SET
	RFPM.GSAJustificationID = 0,
	RFPM.GSAScheduleNumber = NULL,
	RFPM.TaskOrderDescription = NULL,
	RFPM.TaskOrderNumber = NULL,
	RFPM.UnrestrictedProcurementReason = NULL,
	RFPM.UnrestrictedProcurementReasonID = 0
FROM reviewform.ReviewFormProcurementMethod RFPM
WHERE RFPM.ProcurementMethodID = 13
GO

DELETE RFPM1
FROM reviewform.ReviewFormProcurementMethod RFPM1
	JOIN
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY RFPM2.ReviewFormID, RFPM2.ProcurementMethodID ORDER BY  RFPM2.ReviewFormID, RFPM2.ProcurementMethodID) AS RowIndex,
			RFPM2.ReviewFormProcurementMethodID
		FROM reviewform.ReviewFormProcurementMethod RFPM2
		) D ON D.ReviewFormProcurementMethodID = RFPM1.ReviewFormProcurementMethodID
			AND D.RowIndex > 1
GO
--End table reviewform.ReviewFormProcurementMethod

--Begin table reviewform.ReviewFormResearchType
TRUNCATE TABLE reviewform.ReviewFormResearchType
GO

INSERT INTO reviewform.ReviewFormResearchType
	(ReviewFormID, ResearchTypeID)

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'CS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.CSType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'DSBS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.DBBSType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'OTR') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.OtherType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'SAM') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.SAMType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'SS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.SourceType = 1

UNION

SELECT 
	SBRSF.ReviewID AS ReviewFormID,
	(SELECT RT.ResearchTypeID FROM SBRS_DHHS.dropdown.ResearchType RT WHERE RT.ResearchTypeCode = 'VOS') AS ResearchTypeID
FROM SBReview.dbo.SBRSForms SBRSF
WHERE SBRSF.TVOSType = 1
GO
--End table reviewform.ReviewFormResearchType

--Begin table eventlog.EventLog
TRUNCATE TABLE eventlog.EventLog
GO

INSERT INTO eventlog.EventLog
	(PersonID, Action, EntityTypeCode, EntityID, Comments, CreateDateTime)
SELECT
	(SELECT UM.PersonID FROM migration.UserMigration UM WHERE UM.UserID = NRH.UserID),
	NRH.Action,
	'ReviewForm',
	RF.ReviewFormID,
	NRH.Reason,
	NRH.ActionDate
FROM SBReview.dbo.NotifyReviewHistory NRH
	JOIN reviewform.ReviewForm RF ON RF.ControlCode = NRH.ReviewNo
		AND EXISTS
			(
			SELECT 1
			FROM migration.UserMigration UM 
			WHERE UM.UserID = NRH.UserID
			)
ORDER BY NRH.ActionDate
GO
--End table eventlog.EventLog
