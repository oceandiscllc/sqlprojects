USE SBRS_DHHS
GO
	
--Begin procedure emailtemplate.GetEmailData
EXEC Utility.DropObject 'emailtemplate.GetEmailData'
GO
--End procedure emailtemplate.GetEmailData

--Begin procedure emailtemplate.GetNewUserRegistrationEmailData
EXEC Utility.DropObject 'emailtemplate.GetNewUserRegistrationEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetNewUserRegistrationEmailData

@PersonID INT,
@HRef VARCHAR(500)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cOrganizationName VARCHAR(250)
	DECLARE @cPersonNameFormatted VARCHAR(250)
	DECLARE @cRoleNameList VARCHAR(MAX)
	DECLARE @nOrganizationID INT

	SELECT 
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst'),
		@nOrganizationID = O.OrganizationID,
		@cOrganizationName = O.OrganizationName
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

	SELECT @cRoleNameList = COALESCE(@cRoleNameList + ', ', '') + R.RoleName
	FROM dropdown.Role R
		JOIN person.PersonOrganizationRole POR ON POR.RoleID = R.RoleID
			AND POR.PersonID = @PersonID
	ORDER BY R.RoleName

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'NewUserRegistration'

	SET @cEmailText = REPLACE(@cEmailText, '[[EmailAddress]]', ISNULL(@cEmailAddress, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[HRef]]', ISNULL(@HRef, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[OrganizationName]]', ISNULL(@cOrganizationName, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[RoleNameList]]', ISNULL(@cRoleNameList, ''))

	SELECT
		@nOrganizationID AS OrganizationID,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetNewUserRegistrationEmailData

--Begin procedure emailtemplate.GetReviewFormAssignmentEmailData
EXEC Utility.DropObject 'emailtemplate.GetReviewFormAssignmentEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetReviewFormAssignmentEmailData

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cControlCode VARCHAR(50)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cPersonNameFormatted VARCHAR(250)
	DECLARE @cWorkflowStepName VARCHAR(50)

	SELECT
		@cControlCode = RF.ControlCode,
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cWorkflowStepName = R.RoleName
	FROM person.Person P
		JOIN reviewform.ReviewForm RF ON RF.AssignedPersonID = P.PersonID
			AND RF.ReviewFormID = @ReviewFormID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = RF.WorkflowID
			AND WS.DisplayOrder = RF.WorkflowStepNumber
		JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'ReviewFormAssignment'

	SET @cEmailText = REPLACE(@cEmailText, '[[ControlCode]]', ISNULL(@cControlCode, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[WorkflowStepName]]', ISNULL(@cWorkflowStepName, ''))

	SELECT
		@cEmailAddress AS EmailAddress,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetReviewFormAssignmentEmailData

--Begin procedure emailtemplate.GetReviewFormCloseOutEmailData
EXEC Utility.DropObject 'emailtemplate.GetReviewFormCloseOutEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetReviewFormCloseOutEmailData

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cControlCode VARCHAR(50)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cPersonNameFormatted VARCHAR(250)

	SELECT
		@cControlCode = RF.ControlCode,
		@cEmailAddress = P.EmailAddress,
		@cPersonNameFormatted = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast')
	FROM person.Person P
		JOIN reviewform.ReviewForm RF ON RF.CreatePersonID = P.PersonID
			AND RF.ReviewFormID = @ReviewFormID

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'ReviewFormComplete'

	SET @cEmailText = REPLACE(@cEmailText, '[[ControlCode]]', ISNULL(@cControlCode, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[PersonNameFormatted]]', ISNULL(@cPersonNameFormatted, ''))

	SELECT
		@cEmailAddress AS EmailAddress,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetReviewFormCloseOutEmailData

--Begin procedure emailtemplate.GetUserActivationEmailData
EXEC Utility.DropObject 'emailtemplate.GetUserActivationEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to return a new user registration email for a person id
-- =======================================================================================
CREATE PROCEDURE emailtemplate.GetUserActivationEmailData

@PersonID INT,
@HRef VARCHAR(500)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEmailSubject VARCHAR(500)
	DECLARE @cEmailText VARCHAR(MAX)
	DECLARE @cFirstName VARCHAR(250)
	DECLARE @nOrganizationID INT

	SELECT 
		@cFirstName = P.FirstName,
		@nOrganizationID = O.OrganizationID
	FROM person.Person P
		JOIN dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND P.PersonID = @PersonID

	SELECT
		@cEmailSubject = ET.EmailSubject,
		@cEmailText = ET.EmailText
	FROM emailtemplate.EmailTemplate ET
	WHERE ET.EmailTemplateCode = 'UserActivation'

	SET @cEmailText = REPLACE(@cEmailText, '[[HRef]]', ISNULL(@HRef, ''))
	SET @cEmailText = REPLACE(@cEmailText, '[[FirstName]]', ISNULL(@cFirstName, ''))

	SELECT
		@nOrganizationID AS OrganizationID,
		@cEmailSubject AS EmailSubject,
		@cEmailText AS EmailText

END
GO
--End procedure emailtemplate.GetUserActivationEmailData

--Begin procedure eventlog.LogPersonAction
EXEC Utility.DropObject 'eventlog.LogPersonAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@Comments VARCHAR(MAX) = NULL,
@EntityID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0,
@LogDateTime DATETIME = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PersonOrganizationRole VARCHAR(MAX)
	
	IF @LogDateTime IS NULL
		SET @LogDateTime = getDate()
	--ENDIF

	SELECT @PersonOrganizationRole = COALESCE(@PersonOrganizationRole, '') + T2.PersonOrganizationRole FROM (SELECT (SELECT T1.* FOR XML RAW('PersonOrganizationRole'), ELEMENTS) AS PersonOrganizationRole FROM person.PersonOrganizationRole T1 WHERE T1.PersonID = @EntityID) T2
		
	INSERT INTO eventlog.EventLog
		(Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID,CreateDateTime)
	SELECT
		@Comments,
		@EntityID,
		'Person',
		@EventCode,
		(
		SELECT T.*, 
		CAST(('<PersonOrganizationRoleRecords>' + @PersonOrganizationRole + '</PersonOrganizationRoleRecords>') AS XML)
		FOR XML RAW('Person'), ELEMENTS
		),
		@PersonID,
		@LogDateTime
	FROM person.Person T
	WHERE T.personID = @EntityID

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogReviewFormAction
EXEC Utility.DropObject 'eventlog.LogReviewFormAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewFormAction

@Action VARCHAR(100) = NULL,
@Comments VARCHAR(MAX) = NULL,
@CreateDateTime DATETIME = NULL,
@EntityID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ReviewFormContract VARCHAR(MAX)
	DECLARE @ReviewFormDocument VARCHAR(MAX)
	DECLARE @ReviewFormOptionYear VARCHAR(MAX)
	DECLARE @ReviewFormPointOfContact VARCHAR(MAX)
	DECLARE @ReviewFormProcurementHistory VARCHAR(MAX)
	DECLARE @ReviewFormProcurementMethod VARCHAR(MAX)
	DECLARE @ReviewFormResearchType VARCHAR(MAX)

	IF @CreateDateTime IS NULL
		SET @CreateDateTime = getDate()
	--ENDIF

	SELECT @ReviewFormContract = COALESCE(@ReviewFormContract, '') + T2.ReviewFormContract FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormContract'), ELEMENTS) AS ReviewFormContract FROM reviewform.ReviewFormContract T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormDocument = COALESCE(@ReviewFormDocument, '') + T2.ReviewFormDocument FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormDocument'), ELEMENTS) AS ReviewFormDocument FROM reviewform.ReviewFormDocument T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormOptionYear = COALESCE(@ReviewFormOptionYear, '') + T2.ReviewFormOptionYear FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormOptionYear'), ELEMENTS) AS ReviewFormOptionYear FROM reviewform.ReviewFormOptionYear T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormPointOfContact = COALESCE(@ReviewFormPointOfContact, '') + T2.ReviewFormPointOfContact FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormPointOfContact'), ELEMENTS) AS ReviewFormPointOfContact FROM reviewform.ReviewFormPointOfContact T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormProcurementHistory = COALESCE(@ReviewFormProcurementHistory, '') + T2.ReviewFormProcurementHistory FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormProcurementHistory'), ELEMENTS) AS ReviewFormProcurementHistory FROM reviewform.ReviewFormProcurementHistory T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormProcurementMethod = COALESCE(@ReviewFormProcurementMethod, '') + T2.ReviewFormProcurementMethod FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormProcurementMethod'), ELEMENTS) AS ReviewFormProcurementMethod FROM reviewform.ReviewFormProcurementMethod T1 WHERE T1.ReviewFormID = @EntityID) T2
	SELECT @ReviewFormResearchType = COALESCE(@ReviewFormResearchType, '') + T2.ReviewFormResearchType FROM (SELECT (SELECT T1.* FOR XML RAW('ReviewFormResearchType'), ELEMENTS) AS ReviewFormResearchType FROM reviewform.ReviewFormResearchType T1 WHERE T1.ReviewFormID = @EntityID) T2
		
	INSERT INTO eventlog.EventLog
		(Action,Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID,CreateDateTime)
	SELECT
		@Action,
		@Comments,
		@EntityID,
		'ReviewForm',
		@EventCode,
		(
		SELECT T.*, 
		CAST(('<ReviewFormContractRecords>' + @ReviewFormContract + '</ReviewFormContractRecords>') AS XML), 
		CAST(('<ReviewFormDocumentRecords>' + @ReviewFormDocument + '</ReviewFormDocumentRecords>') AS XML), 
		CAST(('<ReviewFormOptionYearRecords>' + @ReviewFormOptionYear + '</ReviewFormOptionYearRecords>') AS XML), 
		CAST(('<ReviewFormPointOfContactRecords>' + @ReviewFormPointOfContact + '</ReviewFormPointOfContactRecords>') AS XML), 
		CAST(('<ReviewFormProcurementHistoryRecords>' + @ReviewFormProcurementHistory + '</ReviewFormProcurementHistoryRecords>') AS XML), 
		CAST(('<ReviewFormProcurementMethodRecords>' + @ReviewFormProcurementMethod + '</ReviewFormProcurementMethodRecords>') AS XML), 
		CAST(('<ReviewFormResearchTypeRecords>' + @ReviewFormResearchType + '</ReviewFormResearchTypeRecords>') AS XML)
		FOR XML RAW('ReviewForm'), ELEMENTS
		),
		@PersonID,
		@CreateDateTime
	FROM reviewform.ReviewForm T
	WHERE T.ReviewFormID = @EntityID

END
GO
--End procedure eventlog.LogReviewFormAction

--Begin procedure utility.CreateGetDataMethodDefinition
EXEC utility.DropObject 'utility.CreateGetDataMethodDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:      Todd Pires
-- Create date: 2017.02.03
-- Description: A helper stored procedure for method creation
-- ==========================================================
CREATE PROCEDURE utility.CreateGetDataMethodDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(250),
@ConnectionString VARCHAR(250) = 'hhs-sbrs-connection'

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @cCRLF VARCHAR(2) = CHAR(13) + CHAR(10)
	DECLARE @cDataType VARCHAR(50)
	DECLARE @cParamaterName VARCHAR(250)
	DECLARE @cParamaterString1 VARCHAR(MAX) = ''
	DECLARE @cParamaterString2 VARCHAR(MAX) = ''
	DECLARE @cSQL VARCHAR(MAX) = ''
	DECLARE @cStoredProcedureName VARCHAR(250) = ''
	DECLARE @cTab1 VARCHAR(1) = CHAR(9)
	DECLARE @cTab2 VARCHAR(2) = REPLICATE(CHAR(9), 2)
	DECLARE @cTab3 VARCHAR(3) = REPLICATE(CHAR(9), 3)
	DECLARE @nPosition INT = 0

	SET @nPosition = CHARINDEX('.', @StoredProcedureName)
	SET @cStoredProcedureName = STUFF(@StoredProcedureName, 1, @nPosition, '')

	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			REPLACE(P.Name, '@', ''),

			CASE
				WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
				THEN 'string'
				ELSE Type_Name(P.User_Type_ID)
			END AS DataType

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

	OPEN oCursor
	FETCH oCursor INTO @cParamaterName, @cDataType
	WHILE @@fetch_status = 0
		BEGIN

		IF @cParamaterString1 <> ''
			SET @cParamaterString1 += ', '
		--ENDIF

		SET @cParamaterString1 += @cDataType + ' ' + @cParamaterName

		IF @cParamaterString2 <> ''
			SET @cParamaterString2 += ', '
		--ENDIF
		
		SET @cParamaterString2 += @cParamaterName + ' = ' + @cParamaterName

		FETCH oCursor INTO @cParamaterName, @cDataType

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	IF @cParamaterString2 <> ''
		SET @cParamaterString2 = 'new { ' + @cParamaterString2 + ' }, '
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('//' + @StoredProcedureName),
		('public IEnumerable<' + @ModelName + '>' + @cStoredProcedureName + '(' + @cParamaterString1 + ')'),
		(@cTab1 + '{'),
		(@cTab1 + 'IEnumerable<' + @ModelName + '> recordSet = null;'),
		(@cTab1 + 'try'),
		(@cTab2 + '{'),
		(@cTab2 + 'using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["' + @ConnectionString + '"].ConnectionString))'),
		(@cTab3 + '{'),
		(@cTab3 + 'recordSet = connection.Query<' + @ModelName + '>("' + @StoredProcedureName + '", ' + @cParamaterString2 + 'commandType: CommandType.StoredProcedure);'),
		(@cTab3 + '}'),
		(@cTab2 + '}'),
		(@cTab1 + 'catch (Exception error)'),
		(@cTab2 + '{')

	IF @cParamaterString1 <> ''
		BEGIN

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			(@cTab2 + 'Hashtable ExtraData = new Hashtable();')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			@cTab2 + 'ExtraData["' + REPLACE(P.Name, '@', '') + '"] = ' + REPLACE(P.Name, '@', '') + 
				CASE
					WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
					THEN ';'
					ELSE '.ToString();'
				END

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

		END
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES
		(''),
		(@cTab2 + 'SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, error);'),
		(@cTab2 + '}'),
		(@cTab1 + 'return recordSet;'),
		(@cTab1 + '}')

	SELECT TextString
	FROM @tTable
	ORDER BY RowIndex

END
GO
--End procedure utility.CreateGetDataMethodDefinition

--Begin procedure utility.CreateSaveDataMethodDefinition
EXEC utility.DropObject 'utility.CreateSaveDataMethodDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:      Todd Pires
-- Create date: 2017.02.03
-- Description: A helper stored procedure for method creation
-- ==========================================================
CREATE PROCEDURE utility.CreateSaveDataMethodDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(250),
@ConnectionString VARCHAR(250) = 'hhs-sbrs-connection'

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @cArgumentsList VARCHAR(MAX) = ''
	DECLARE @cCRLF VARCHAR(2) = CHAR(13) + CHAR(10)
	DECLARE @cDataType VARCHAR(50)
	DECLARE @cObjectName VARCHAR(250) = STUFF(@ModelName, 1, 1, LOWER(LEFT(@ModelName, 1)))
	DECLARE @cParamaterName VARCHAR(250)
	DECLARE @cParamaterString1 VARCHAR(MAX) = ''
	DECLARE @cSQL VARCHAR(MAX) = ''
	DECLARE @cStoredProcedureName VARCHAR(250) = ''
	DECLARE @cTab1 VARCHAR(1) = CHAR(9)
	DECLARE @cTab2 VARCHAR(2) = REPLICATE(CHAR(9), 2)
	DECLARE @cTab3 VARCHAR(3) = REPLICATE(CHAR(9), 3)
	DECLARE @cTab4 VARCHAR(4) = REPLICATE(CHAR(9), 4)
	DECLARE @nPosition INT = 0

	SET @nPosition = CHARINDEX('.', @StoredProcedureName)
	SET @cStoredProcedureName = STUFF(@StoredProcedureName, 1, @nPosition, '')

	SET @cArgumentsList = utility.GetStoredProcedureObjectArgumentList(@StoredProcedureName, @cObjectName)

	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			REPLACE(P.Name, '@', ''),

			CASE
				WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
				THEN 'string'
				ELSE Type_Name(P.User_Type_ID)
			END AS DataType

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

	OPEN oCursor
	FETCH oCursor INTO @cParamaterName, @cDataType
	WHILE @@fetch_status = 0
		BEGIN

		IF @cParamaterString1 <> ''
			SET @cParamaterString1 += ', '
		--ENDIF

		SET @cParamaterString1 += @cDataType + ' ' + @cParamaterName

		FETCH oCursor INTO @cParamaterName, @cDataType

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('//' + @StoredProcedureName),
		('public bool ' + @cStoredProcedureName + '(' + @ModelName + ' ' + @cObjectName + ')'),
		(@cTab1 + '{'),
		(@cTab1 + 'try'),
		(@cTab2 + '{'),
		(@cTab2 + 'using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["' + @ConnectionString + '"].ConnectionString))'),
		(@cTab3 + '{'),
		(@cTab3 + 'connection.Query<' + @ModelName + '>("' + @StoredProcedureName + '", new'),
		(@cTab4 + '{' + @cArgumentsList + '}, commandType: CommandType.StoredProcedure).SingleOrDefault();'),
		(@cTab3 + '}'),
		(@cTab2 + '}'),
		(@cTab1 + 'catch (Exception error)'),
		(@cTab2 + '{')

	IF @cParamaterString1 <> ''
		BEGIN

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			(@cTab2 + 'Hashtable ExtraData = new Hashtable();')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			@cTab2 + 'ExtraData["' + REPLACE(P.Name, '@', '') + '"] = ' + @cObjectName + '.' + REPLACE(P.Name, '@', '') + 
				CASE
					WHEN Type_Name(P.User_Type_ID) IN ('char','nvarchar','varchar')
					THEN ';'
					ELSE '.ToString();'
				END

		FROM sys.Parameters P
		WHERE P.Object_ID = Object_ID(@StoredProcedureName)
		ORDER BY P.Parameter_ID

		END
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	VALUES
		(''),
		(@cTab2 + 'SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, error);'),
		(@cTab2 + '}'),
		(@cTab1 + 'return true;'),
		(@cTab1 + '}')

	SELECT TextString
	FROM @tTable
	ORDER BY RowIndex

END
GO
--End procedure utility.CreateSaveDataMethodDefinition

--Begin procedure utility.CreateStoredProcedureClassDefinition
EXEC utility.DropObject 'utility.CreateStoredProcedureClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureClassDefinition

@StoredProcedureName VARCHAR(250),
@ModelName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nSQLVersion INT = CAST(LEFT(REPLACE(@@version, 'Microsoft SQL Server ', ''), 4) AS INT)
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF @nSQLVersion < 2012
		print 'This procedure only works on SQL Server 2012 or higher'
	ELSE
		BEGIN

		IF CHARINDEX('.', @StoredProcedureName) = 0
			SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
		--ENDIF

		INSERT INTO @tTable 
			(TextString) 
		VALUES
			('public class ' + @ModelName)

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			('{')

		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			CASE
				WHEN CHARINDEX('(', system_type_name, 0) = 0
				THEN utility.CreateTableColumnClassDefinition(name, 1, system_type_name)
				ELSE utility.CreateTableColumnClassDefinition(name, 1, LEFT(system_type_name, CHARINDEX('(', system_type_name, 0) - 1))
			END 
		FROM sys.dm_exec_describe_first_result_set_for_object
			(
			OBJECT_ID(@StoredProcedureName), 
			0
			)
		ORDER BY name

		INSERT INTO @tTable 
			(TextString) 
		VALUES 
			('}')
		
		SELECT TextString
		FROM @tTable	

		END
	--ENDIF

END
GO
--End procedure utility.CreateStoredProcedureClassDefinition

--Begin procedure utility.CreateStoredProcedureObjectArgumentList
EXEC utility.DropObject 'utility.CreateStoredProcedureObjectArgumentList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureObjectArgumentList

@StoredProcedureName VARCHAR(250),
@StoredProcedureObjectName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ParameterName VARCHAR(50)
	DECLARE @Return VARCHAR(MAX) = ''

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SPP.ParameterName
		FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP
	
	OPEN oCursor
	FETCH oCursor INTO @ParameterName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @Return <> ''
			SET @Return += ', '
		--ENDIF
		
		SET @Return += @ParameterName + ' = ' + @StoredProcedureObjectName + '.' + @ParameterName
		
		FETCH oCursor INTO @ParameterName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT @Return

END
GO
--End procedure utility.CreateStoredProcedureObjectArgumentList

--Begin procedure utility.CreateStoredProcedureParameterClassDefinition
EXEC utility.DropObject 'utility.CreateStoredProcedureParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateStoredProcedureParameterClassDefinition
	@StoredProcedureName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('public class ' + REPLACE(PARSENAME(@StoredProcedureName, 1), 'Get', '')),
		('{')
		
	INSERT INTO @tTable
		(TextString) 
	SELECT 
		utility.CreateParameterClassDefinition(SPP.ParameterName, SPP.DataType)
	FROM utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure utility.CreateStoredProcedureParameterClassDefinition

--Begin procedure utility.CreateTableClassDefinition
EXEC utility.DropObject 'utility.CreateTableClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableClassDefinition
	@TableName VARCHAR(250),
	@IncludeAnnotations BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColumnName VARCHAR(50)
	DECLARE @DataType VARCHAR(50)
	DECLARE @IsNullable BIT
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	SELECT
		'public class ' + T.Name
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
			AND S.Name + '.' + T.Name = @TableName

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('{')

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			C1.Name AS ColumnName, 
			C1.Is_Nullable AS IsNullable, 
			T1.Name AS DataType
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.schema_ID = O1.schema_ID
			JOIN sys.Columns C1 ON O1.Object_ID = C1.Object_ID
			JOIN sys.Types T1 ON C1.User_Type_ID = T1.User_Type_ID
				AND S1.Name + '.' + O1.Name = @TableName
				AND O1.Type = 'U'
		ORDER BY C1.Name

	
	OPEN oCursor
	FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @IncludeAnnotations = 1
			BEGIN

			IF @DataType IN ('date','datetime')
				BEGIN

				INSERT INTO @tTable 
					(TextString)
				VALUES
					('[DataType(DataType.Date)]'),
        	('[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]')
				
				END
			--ENDIF

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('[Display(Name = "' + @ColumnName + '")]')

			END
		--ENDIF
		
		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			utility.CreateTableColumnClassDefinition(@ColumnName, @IsNullable, @DataType)

		IF @IncludeAnnotations = 1
			BEGIN

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('')

			END
		--ENDIF
		
		FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure utility.CreateTableClassDefinition

--Begin procedure utility.CreateTableInsertUpdateDefinition
EXEC utility.DropObject 'utility.CreateTableInsertUpdateDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.05.05
-- Description:	A helper stored procedure for table creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableInsertUpdateDefinition
	@TableName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tTable1 TABLE
		(
		ColumnName VARCHAR(50), 
		IsIdentity BIT
		)

	DECLARE @tTable2 TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable1
		(ColumnName,IsIdentity)
	SELECT
		C.Name,
		C.Is_Identity
	FROM sys.objects O
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.Columns C ON O.Object_ID = C.Object_ID
		JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
			AND S.Name + '.' + O.Name = @TableName
			AND O.Type = 'U'
	ORDER BY C.Name

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('INSERT INTO ' + @TableName),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'OUTPUT INSERTED.' + T1.ColumnName + ' INTO @tOutput'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('VALUES'),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('UPDATE ' + @TableName),
		('SET')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ' = @' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'WHERE ' + T1.ColumnName + ' = @' + T1.ColumnName 
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1
		
	SELECT TextString
	FROM @tTable2

END
GO
--End procedure utility.CreateTableInsertUpdateDefinition

--Begin procedure utility.CreateTableSaveProcedureDefinition
EXEC utility.DropObject 'utility.CreateTableSaveProcedureDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.05.05
-- Description:	A helper stored procedure for table creation
-- =========================================================
CREATE PROCEDURE utility.CreateTableSaveProcedureDefinition
	@TableName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cProcedureName VARCHAR(250)
	DECLARE @nCharIndex INT

	DECLARE @tTable1 TABLE
		(
		ColumnName VARCHAR(50), 
		DataType VARCHAR(50),
		Length INT,
		IsIdentity BIT
		)

	DECLARE @tTable2 TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nCharIndex = CHARINDEX('.', @TableName)
	SET @cProcedureName = STUFF(@TableName, @nCharIndex + 1, 0, 'Save')

	INSERT INTO @tTable1
		(ColumnName,DataType,Length,IsIdentity)
	SELECT
		C.Name AS ColumnName,
		UPPER(T.Name) AS DataType,
		C.Max_Length AS Length,
		C.Is_Identity
	FROM sys.objects O
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.Columns C ON O.Object_ID = C.Object_ID
		JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
			AND S.Name + '.' + O.Name = @TableName
			AND O.Type = 'U'
	ORDER BY C.Name

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('--Begin procedure ' + @cProcedureName),
		('EXEC utility.DropObject ''' + @cProcedureName + ''''),
		('GO'),
		(''),
		('SET ANSI_NULLS ON'),
		('GO'),
		('SET QUOTED_IDENTIFIER ON'),
		('GO'),
		(''),
		('-- ============================================================================================='),
		('-- Author:			Todd Pires'),
		('-- Create Date:	' + REPLACE(CONVERT(CHAR(10), getDate(), 126), '-', '.')),
		('-- Description:	A stored procedure to save data to the ' + @TableName + ' table'),
		('-- ============================================================================================='),
		('CREATE PROCEDURE ' + @cProcedureName),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ' ' + T1.DataType
		+ CASE
				WHEN T1.DataType = 'VARCHAR'
				THEN
					CASE 
						WHEN T1.Length = -1
						THEN '(MAX)'
						ELSE '(' + CAST(T1.Length AS VARCHAR(10)) + ')'
					END
				ELSE ''
			END
		+ ','
	FROM @tTable1 T1

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('AS'),
		('BEGIN'),
		('SET NOCOUNT ON;'),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'IF @' + T1.ColumnName + ' = 0'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('BEGIN'),
		('')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'DECLARE @tOutput TABLE (' + T1.ColumnName + ' ' + T1.DataType + ')'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('INSERT INTO ' + @TableName),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'OUTPUT INSERTED.' + T1.ColumnName + ' INTO @tOutput'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('VALUES'),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')'),
		(''),
		('END'),
		('ELSE'),
		('BEGIN'),
		(''),
		('UPDATE ' + @TableName),
		('SET')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ' = @' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'WHERE ' + T1.ColumnName + ' = @' + T1.ColumnName 
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('END'),
		('--ENDIF'),
		(''),
		('END'),
		('GO'),
		('--End procedure ' + @cProcedureName)
		
	SELECT TextString
	FROM @tTable2

END
GO
--End procedure utility.CreateTableSaveProcedureDefinition

--Begin procedure workflow.GetWorkflowByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to get workflow data for a reviewform.ReviewForm record
-- =======================================================================================
CREATE PROCEDURE workflow.GetWorkflowByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

SELECT
	person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
	person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
	person.GetOriginatingWorkflowStepNumberByPersonID(RF.CreatePersonID) AS OriginatingWorkflowStepNumber,
	S.StatusName,
	RF.WorkflowStepNumber,
	workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS WorkflowStepCount,
	workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
	workflow.GetWorkflowStepPeopleByReviewFormID(RF.ReviewFormID) AS WorkflowStepPeople
FROM reviewform.ReviewForm RF
	JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure workflow.GetWorkflowByReviewFormID

--Begin procedure workflow.GetWorkflowHistoryByReviewFormID
EXEC utility.DropObject 'workflow.GetWorkflowHistoryByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to get workflow history for a reviewform.ReviewForm record
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowHistoryByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EL.Action,
		EL.Comments,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ReviewForm'
		AND EL.EntityID = @ReviewFormID
	ORDER BY EL.EventLogID

END
GO
--End procedure workflow.GetWorkflowHistoryByReviewFormID

--Begin procedure workflow.SetWorkflowByReviewFormID
EXEC utility.DropObject 'workflow.SetWorkflowByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.03.01
-- Description: A stored procedure to set a workflow for a reviewform.ReviewForm record
-- ====================================================================================
CREATE PROCEDURE workflow.SetWorkflowByReviewFormID

@ReviewFormID INT,
@TotalEstimatedValue INT = NULL,
@ProcurementMethodIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @TotalEstimatedValue IS NULL
		SET @TotalEstimatedValue = reviewform.GetTotalEstimatedValue(@ReviewFormID)
	--ENDIF

	UPDATE RF
	SET RF.WorkflowID = W.WorkflowID
	FROM reviewform.ReviewForm RF
		JOIN 
			(
			SELECT
				RF.ReviewFormID,

				CASE
					WHEN O.OrganizationCode = 'IHS'
					THEN O.OrganizationCode 
					ELSE 'HHS'
				END AS OrganizationCode,

				CASE
					WHEN @TotalEstimatedValue < 150000
						OR (@ProcurementMethodIDList IS NULL AND EXISTS (SELECT 1 FROM reviewform.ReviewFormProcurementMethod RFPM JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID AND PM.IsSetAside = 1 AND RFPM.ReviewFormID = RF.ReviewFormID))
						OR (@ProcurementMethodIDList IS NOT NULL AND EXISTS (SELECT 1 FROM dropdown.ProcurementMethod PM JOIN core.ListToTable(@ProcurementMethodIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PM.ProcurementMethodID AND PM.IsSetAside = 1))
					THEN 'Level1'
					WHEN @TotalEstimatedValue >= 5000000
					THEN 'Level3'
					ELSE 'Level2'
				END AS LevelCode

			FROM reviewform.ReviewForm RF
				JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
					AND RF.ReviewFormID = @ReviewFormID
			) WFD ON WFD.ReviewFormID = RF.ReviewFormID
		JOIN workflow.Workflow W ON W.OrganizationCode = WFD.OrganizationCode
			AND W.LevelCode = WFD.LevelCode

	EXEC workflow.GetWorkflowByReviewFormID @ReviewFormID = @ReviewFormID

END
GO
--End procedure workflow.SetWorkflowByReviewFormID