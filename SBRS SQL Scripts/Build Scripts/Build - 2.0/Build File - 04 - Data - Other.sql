USE SBRS_DHHS
GO

--Begin table dropdown.BundleType
TRUNCATE TABLE dropdown.BundleType
GO

SET IDENTITY_INSERT dropdown.BundleType ON;
INSERT INTO dropdown.BundleType (BundleTypeID) VALUES (0);

INSERT INTO dropdown.BundleType 
	(BundleTypeID, BundleTypeCode, BundleTypeName, IsActive)
VALUES
	(1, 'No', 'No', 1),
	(2, 'NA', 'N/A (Below estimated threshold)', 0),
	(3, 'Yes', 'Yes (Please list contract numbers)', 1)

SET IDENTITY_INSERT dropdown.BundleType OFF;
GO
--End table dropdown.BundleType

--Begin table dropdown.COACS
TRUNCATE TABLE dropdown.COACS
GO

SET IDENTITY_INSERT dropdown.COACS ON;
INSERT INTO dropdown.COACS (COACSID) VALUES (0);

INSERT INTO dropdown.COACS 
	(COACSID, COACSCode, COACSName)
VALUES
	(1, 'NCI', 'National Cancer Institute (NCI,NCCAM)'),
	(2, 'CC', 'Clinical Center (CC)'),
	(3, 'NIDA', 'National Institute on Drug Abuse (NIDA, NINDS, NIMH, NIA, NCATS (partial yr.))'),
	(4, 'NHLBI', 'National Heart, Lung, & Blood Institute (NHLBI, NIAMS, CSR, NIDCR, NIBIB, NCATS (partial yr.), NHGRI)'),
	(5, 'NIAID', 'National Institute Of Allergy & Infectious Diseases (NIAID, HHS BIODEFENSE)'),
	(6, 'OLAO', 'Office of Logistics & Acquisition Operations (OLAO, NINR, NIMHD, NEI, ORS, NIDCD, NIGMS, OD, NITAAC)'),
	(7, 'ORF', 'Office of Research Facilities (ORF)'),
	(8, 'NIEHS', 'National Institute of Environmental Health Sciences (NIEHS)'),
	(9, 'NLM', 'National Library of Medicine (NLM, NIDDK, CIT, OD)'),
	(10, 'NICHD', 'National Institute of Child Health (NICHD, NIAAA, FIC)')

SET IDENTITY_INSERT dropdown.COACS OFF;
GO
--End table dropdown.COACS

--Begin table dropdown.ContractTermination
TRUNCATE TABLE dropdown.ContractTermination
GO

SET IDENTITY_INSERT dropdown.ContractTermination ON;
INSERT INTO dropdown.ContractTermination (ContractTerminationID) VALUES (0);

INSERT INTO dropdown.ContractTermination
	(ContractTerminationID, ContractTerminationCode, ContractTerminationName)
VALUES
	(1, 'No', 'No, then Completion Date'),
	(2, 'Yes', 'Yes, then Specify Reason')

SET IDENTITY_INSERT dropdown.ContractTermination OFF;
GO
--End table dropdown.ContractTermination

--Begin table dropdown.DocumentType
TRUNCATE TABLE dropdown.DocumentType
GO

SET IDENTITY_INSERT dropdown.DocumentType ON;
INSERT INTO dropdown.DocumentType (DocumentTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.DocumentType OFF;

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCode, DocumentTypeName)
VALUES
	('8aAL', '8(a) Acceptance Letter'),
	('8aOL', '8(a) Offer Letter'),
	('AP', 'Acquisition Plan'),
	('IGCE', 'IGCE'),
	('J&A', 'J&A'),
	('JEFO', 'JEFO-(Far 16.505(b)(2)(C))'),
	('LSJ', 'Limited Source Justification'),
	('MR', 'Market Research'),
	('OTR', 'Others'),
	('REQ', 'Requisition'),
	('SOW', 'Requirements Document (SOW, PWS, SOO)')
GO
--End table dropdown.DocumentType

--Begin table dropdown.FARJustification
TRUNCATE TABLE dropdown.FARJustification
GO

SET IDENTITY_INSERT dropdown.FARJustification ON;
INSERT INTO dropdown.FARJustification (FARJustificationID) VALUES (0);

INSERT INTO dropdown.FARJustification
	(FARJustificationID, FARJustificationName)
VALUES
	(4, 'Authorized or required by statute (6.302-5)'),
	(3, 'International agreement (6.302-4)'),
	(5, 'National security (6.302-6)'),
	(1, 'Only one responsible source and no other supplies or services will satisfy agency requirements (6.302-1)'),
	(6, 'Public interest (6.302-7)'),
	(2, 'Unusual and compelling urgency (6.302-2)')

SET IDENTITY_INSERT dropdown.FARJustification OFF;
GO
--End table dropdown.FARJustification

--Begin table dropdown.GSAJustification
TRUNCATE TABLE dropdown.GSAJustification
GO

SET IDENTITY_INSERT dropdown.GSAJustification ON;
INSERT INTO dropdown.GSAJustification (GSAJustificationID) VALUES (0);

INSERT INTO dropdown.GSAJustification
	(GSAJustificationID, GSAJustificationCode, GSAJustificationName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.GSAJustification OFF;
GO
--End table dropdown.GSAJustification

--Begin table dropdown.Organization
TRUNCATE TABLE dropdown.Organization
GO

SET IDENTITY_INSERT dropdown.Organization ON
GO

INSERT INTO dropdown.Organization (OrganizationID) VALUES (0)

INSERT INTO dropdown.Organization 
	(OrganizationID, OrganizationCode, OrganizationDisplayCode, OrganizationName)
VALUES
	(1, 'OS', 'OS', 'Office of Secretary (OS)'),
	(2, 'NIH', 'NIH', 'National Institute of Health (NIH)'),
	(3, 'AHQ', 'AHRQ', 'Agency for Healthcare Research and Quality (AHRQ)'),
	(4, 'CDC', 'CDC', 'Centers for Disease Control and Prevention (CDC)'),
	(5, 'CMS', 'CMS', 'Centers for Medicare & Medicaid Services (CMS)'),
	(6, 'FDA', 'FDA', 'Food and Drug Administration (FDA)'),
	(7, 'HRS', 'HRSA', 'Health Resources and Services Administration (HRSA)'),
	(8, 'IHS', 'IHS', 'Indian Health Service (IHS)'),
	(9, 'SAM', 'SAMHSA', 'Substance Abuse and Mental Health Services Administration (SAMHSA)'),
	(11, 'ACF', 'ACF', 'Administration for Children and Families (ACF)'),
	(13, 'ACL', 'ACL', 'Administration for Community Living (ACL)'),
	(15, 'APR', 'ASPR', 'Assistant Secretary for Preparedness and Response (ASPR)'),
	(16, 'PSC', 'PSC', 'Program Support Center (PSC)'),
	(17, 'SBA', 'SBA', 'Small Business Administration (SBA)')

INSERT INTO dropdown.Organization 
	(OrganizationID, OrganizationCode, OrganizationDisplayCode, OrganizationName, DisplayOrder)
SELECT 
	99,
	'NON',
	'NON',
	'NON-HHS',
	99
GO

SET IDENTITY_INSERT dropdown.Organization OFF
GO

UPDATE O
SET O.IsForFundingOffice = 0
FROM dropdown.Organization O
WHERE O.OrganizationDisplayCode IN ('NON', 'SBA')
GO

UPDATE O
SET OrganizationFilePath = '/Docs/' + O.OrganizationDisplayCode + '/'
FROM dropdown.Organization O
GO
--End table dropdown.Organization

--Begin table dropdown.PriorBundler
TRUNCATE TABLE dropdown.PriorBundler
GO

SET IDENTITY_INSERT dropdown.PriorBundler ON;
INSERT INTO dropdown.PriorBundler (PriorBundlerID) VALUES (0);

INSERT INTO dropdown.PriorBundler 
	(PriorBundlerID, PriorBundlerCode, PriorBundlerName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.PriorBundler OFF;
GO
--End table dropdown.PriorBundler

--Begin table dropdown.ProcurementHistory
TRUNCATE TABLE dropdown.ProcurementHistory
GO

SET IDENTITY_INSERT dropdown.ProcurementHistory ON;
INSERT INTO dropdown.ProcurementHistory (ProcurementHistoryID) VALUES (0);

INSERT INTO dropdown.ProcurementHistory 
	(ProcurementHistoryID, ProcurementHistoryCode, ProcurementHistoryName)
VALUES
	(1, 'NewRequirement', 'New Requirement (No prior procurement history)'),
	(2, 'Recompetition', 'Prior Procurement History - Recompetition')

SET IDENTITY_INSERT dropdown.ProcurementHistory OFF;
GO
--End table dropdown.ProcurementHistory

--Begin table dropdown.ProcurementMethod
TRUNCATE TABLE dropdown.ProcurementMethod
GO

SET IDENTITY_INSERT dropdown.ProcurementMethod ON;
GO

INSERT INTO dropdown.ProcurementMethod (ProcurementMethodID) VALUES (0);

INSERT INTO dropdown.ProcurementMethod 
	(ProcurementMethodID, ProcurementMethodCode, ProcurementMethodName)
VALUES
	(1, 'AONE', 'Ability One'),
	(2, 'GSA', 'GSA Federal Supply Schedule'),
	(3, 'DTO', 'Delivery / Task Order'),
	(4, '8aSS', '8(a) Sole Source'),
	(5, '8aC', '8(a) Competitive'),
	(6, 'HUBSS', 'HUBZone Sole Source'),
	(7, 'HUBC', 'HUBZone Competitive'),
	(8, 'SDVOSBSS', 'SDVOSB Sole Source'),
	(9, 'SDVOSBC', 'SDVOSB Competitive'),
	(10, 'SBSA', 'Small Business Set-Aside'),
	(11, 'SBSAP', 'Partial Small Business Set-Aside'),
	(12, 'FOU', 'Full & Open/Unrestricted'),
	(13, 'OFOU', 'Other than Full and Open'),
	(14, 'UNICOR', 'UNICOR'),
	(15, 'NASASEWP', 'NASA SEWP'),
	(16, 'WOSBC', 'Women-Owned Small Business (WOSB) Competitive'),
	(17, 'WOSBSS', 'Women-Owned Small Business (WOSB) Sole Source'),
	(18, 'EDWOSB', 'Economically Disadvantaged Women-Owned Small Business (EDWOSB) Set-Aside'),
	(19, 'Indian', 'Urban Indian Organization (P.L. 94-437) & Buy Indian Act (25 USC 47) - IHS HCA Authorization required')
GO

SET IDENTITY_INSERT dropdown.ProcurementMethod OFF;
GO

UPDATE PM
SET PM.IsSetAside = 1
FROM dropdown.ProcurementMethod PM
WHERE PM.ProcurementMethodName LIKE '%Competitive%'
	OR PM.ProcurementMethodName LIKE '%Set-Aside%'
	OR PM.ProcurementMethodName LIKE '%Sole Source%'
	OR PM.ProcurementMethodName LIKE '%Women-Ow%'
GO
--End table dropdown.ProcurementMethod

--Begin table dropdown.ResearchType
TRUNCATE TABLE dropdown.ResearchType
GO

SET IDENTITY_INSERT dropdown.ResearchType ON;
INSERT INTO dropdown.ResearchType (ResearchTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.ResearchType OFF;

INSERT INTO dropdown.ResearchType 
	(ResearchTypeCode,ResearchTypeName,DisplayOrder)
VALUES
	('SAM', 'System for Award Management (SAM)', 1), -- SAMType
	('DSBS', 'Dynamic Small Business Search (DSBS)', 2), --DBBSType
	('VOS', 'Vendor Outreach Session', 3), --TVOSType
	('CS', 'Capability Statements', 4), --CSType
	('SS', 'Sources Sought Notice', 5), --SourceType
	('OTR', 'Other', 6) --OtherType
GO
--End table dropdown.ResearchType

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

SET IDENTITY_INSERT dropdown.Role ON;
INSERT INTO dropdown.Role (RoleID) VALUES (0);
SET IDENTITY_INSERT dropdown.Role OFF;

INSERT INTO dropdown.Role 
	(RoleCode, RoleName, NotificationRoleCode, IsForDisplay, HasOrganization, DisplayOrder)
VALUES
	('CS', 'Contract Specialist', 'SBS', 1, 1, 1),
	('CO', 'Contracting Officer', 'SBS', 1, 1, 2),
	('DHCA', 'Deputy Head of Contracting Activities', 'OSDBU', 1, 1, 6),
	('HCA', 'Head of Contracting Activities', 'OSDBU', 1, 1, 7),
	('OSDBU', 'OSDBU Director / Deputy Director', 'OSDBU', 1, 0, 8),
	('SBAPCR', 'SBA Procurement Center Representative', 'OSDBU', 1, 0, 5),
	('SBS', 'Small Business Specialist', 'OSDBU', 1, 1, 4),
	('SBTA', 'Small Business Technical Advisor', 'OSDBU', 1, 1, 3),
	('CloseOut', 'Close Out', 'OSDBU', 0, 0, 0),
	('SysAdmin', 'System Administrator', 'OSDBU', 0, 0, 0)
GO
--End table dropdown.Role

--Begin table dropdown.SizeType
TRUNCATE TABLE dropdown.SizeType
GO

SET IDENTITY_INSERT dropdown.SizeType ON;
INSERT INTO dropdown.SizeType (SizeTypeID) VALUES (0);

INSERT INTO dropdown.SizeType 
	(SizeTypeID, SizeTypeCode, SizeTypeName)
VALUES
	(1, 'EmployeeCount', 'Number of Employees'),
	(2, 'Revenue', 'Dollar Amount')

SET IDENTITY_INSERT dropdown.SizeType OFF;
GO
--End table dropdown.SizeType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

SET IDENTITY_INSERT dropdown.Status ON;
INSERT INTO dropdown.Status (StatusID) VALUES (0);
SET IDENTITY_INSERT dropdown.Status OFF;

INSERT INTO dropdown.Status 
	(StatusCode, StatusName, DisplayOrder)
VALUES
	('InProgress', 'In Progress', 1),
	('UnderReview', 'Under Review', 2),
	('Complete', 'Complete', 3),
	('BackToAuthor', 'Back to Author', 4),
	('Withdrawn', 'Withdrawn', 5)
GO
--End table dropdown.Status

--Begin table dropdown.SubContractingPlan
TRUNCATE TABLE dropdown.SubContractingPlan
GO

SET IDENTITY_INSERT dropdown.SubContractingPlan ON;
INSERT INTO dropdown.SubContractingPlan (SubContractingPlanID) VALUES (0);

INSERT INTO dropdown.SubContractingPlan 
	(SubContractingPlanID, SubContractingPlanCode, SubContractingPlanName)
VALUES
	(1, 'No', 'No'),
	(2, 'Yes', 'Yes')

SET IDENTITY_INSERT dropdown.SubContractingPlan OFF;
GO
--End table dropdown.SubContractingPlan

--Begin table dropdown.Synopsis
TRUNCATE TABLE dropdown.Synopsis
GO

SET IDENTITY_INSERT dropdown.Synopsis ON;
INSERT INTO dropdown.Synopsis (SynopsisID) VALUES (0);

INSERT INTO dropdown.Synopsis 
	(SynopsisID, SynopsisCode, SynopsisName)
VALUES
	(1, 'Yes', 'Yes'),
	(2, 'No', 'No')

SET IDENTITY_INSERT dropdown.Synopsis OFF;
GO
--End table dropdown.Synopsis

--Begin table dropdown.SynopsisExceptionReason
TRUNCATE TABLE dropdown.SynopsisExceptionReason
GO

SET IDENTITY_INSERT dropdown.SynopsisExceptionReason ON;
GO

INSERT INTO dropdown.SynopsisExceptionReason (SynopsisExceptionReasonID) VALUES (0);
GO

INSERT INTO dropdown.SynopsisExceptionReason 
	(SynopsisExceptionReasonID, SynopsisExceptionReasonName)
VALUES
	(8, 'Acceptance of an unsolicited research proposal'),
	(4, 'Authorized or required by statute'),
	(13, 'Below SAT; available through GPE; and public responds electronically'),
	(12, 'Defense agency contract to be made and performed outside the US'),
	(15, 'Determination by agency head; as approved by OFPP and SBA'),
	(14, 'Expert services under 6.302-3'),
	(3, 'International agreement'),
	(10, 'Meets conditions under 6.302-3, or 6.302-5 (brand name commercial items), or 6.302-7'),
	(1, 'National security'),
	(6, 'Order placed under Subpart 16.5'),
	(9, 'Perishable subsistence supplies, advance notice is not appropriate or reasonable'),
	(11, 'Previously synopsized contract in compliance with 5.207'),
	(7, 'Proposal under the SBIR Program'),
	(2, 'Unusual and compelling urgency (6.302-2)'),
	(5, 'Utility services and only one source is available')
GO

SET IDENTITY_INSERT dropdown.SynopsisExceptionReason OFF;
GO
--End table dropdown.SynopsisExceptionReason

--Begin table dropdown.UnrestrictedProcurementReason
TRUNCATE TABLE dropdown.UnrestrictedProcurementReason
GO

SET IDENTITY_INSERT dropdown.UnrestrictedProcurementReason ON;
INSERT INTO dropdown.UnrestrictedProcurementReason (UnrestrictedProcurementReasonID) VALUES (0);

INSERT INTO dropdown.UnrestrictedProcurementReason 
	(UnrestrictedProcurementReasonID, UnrestrictedProcurementReasonCode, UnrestrictedProcurementReasonName, DisplayOrder)
VALUES
	(2,	'NoMarket', 'No reasonable expectation that award will be made at fair market price', 1),
	(1,	'NoFirms', 'No reasonable expectation that offers will be obtained from at least two small business concerns', 2),
	(3,	'NoProduct', 'Small business can not provide the products of another small business concerns', 3),
	(4,	'FAR', 'Sole Source/Proprietary Item Justified in accordance with FAR 6.3, please specify', 4),
	(5, 'UPROther', 'Others, please specify', 5)

SET IDENTITY_INSERT dropdown.UnrestrictedProcurementReason OFF;
GO
--End table dropdown.UnrestrictedProcurementReason

--Begin table emailtemplate.EmailTemplate
TRUNCATE TABLE emailtemplate.EmailTemplate
GO

INSERT INTO emailtemplate.EmailTemplate
	(EmailTemplateCode, EmailTemplateName, EmailSubject, EmailText)
VALUES
	('NewUserRegistration', 'New SBRS User Registration', 'A New SBRS User Has Registered', '<p>A new user has registered in the SBRS and is requesting access to your OpDiv.</p><p>Request details:<br />User:&nbsp;&nbsp;[[PersonNameFormatted]]<br />Email:&nbsp;&nbsp;[[EmailAddress]]<br />OpDiv:&nbsp;&nbsp;[[OrganizationName]]<br />Roles(s):&nbsp;&nbsp;[[RoleNameList]]</p><p>To approve this user&#39;s request, <a href="[[HRef]]">click here to Approve Their Account</a> or disregard this email to leave their account disabled.</p><p>Thanks,</p><p>The SBRS Team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('ReviewFormAssignment', 'SBRS Review Form Assignment', 'A Small Business Review Form Has Been Assigned To You', '[[PersonNameFormatted]],<p>An SBRS Review Form has been assigned to you.</p><p>Form:&nbsp;&nbsp;[[ControlCode]] is in the [[WorkflowStepName]] step of its workflow and is awaiting your review.  You may access this form by logging in to the SBRS <a href="https://sbrs.hhs.gov/">here</a> and clicking the "Open Items" tab.</p><p>Thanks,</p><p>The SBRS team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('ReviewFormComplete', 'SBRS Review Form Closeout', 'A Small Business Review Form You Originated Has Been Completed', '[[PersonNameFormatted]],<p>An SBRS Review Form that you originated has been completed.</p><p>Form:&nbsp;&nbsp;[[ControlCode]] has been closed out.  You may access this form by logging in to the SBRS <a href="https://sbrs.hhs.gov/">here</a> and clicking the "Closed Items" tab.</p><p>Thanks,</p><p>The SBRS team<br />This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>'),
	('UserActivation', 'SBRS User Account Activation', 'Your SBRS User Account Has Been Activated', '<p>Dear [[FirstName]],</p><p>Your account in the Small Business Review System has been activated.</p><p>You may now login to your account with your email address and password at:</p><p><a href="[[HRef]]">Small Business Review System</a></p><p>&nbsp;</p><p>Thanks,</p><p>The SBRS Team</p><p>This is an unmonitored mail box.&nbsp; Please DO NOT reply to this e-mail.</p>')
GO
--End table emailtemplate.EmailTemplate

--Begin table workflow.Workflow
TRUNCATE TABLE workflow.Workflow
GO

INSERT INTO workflow.Workflow
	(WorkflowCode, OrganizationCode, LevelCode)
VALUES
	('IHS1', 'IHS', 'Level1'),
	('IHS2', 'IHS', 'Level2'),
	('IHS3', 'IHS', 'Level3'),
	('HHS1', 'HHS', 'Level1'),
	('HHS2', 'HHS', 'Level2'),
	('HHS3', 'HHS', 'Level3')
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
TRUNCATE TABLE workflow.WorkflowStep
GO

INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS1';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,SBAPCR,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS2';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBTA,SBS,SBAPCR,OSDBU,CloseOut', ',') LTT WHERE W.WorkflowCode = 'IHS3';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS1';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,SBAPCR,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS2';
INSERT INTO workflow.WorkflowStep (WorkflowID, RoleCode, DisplayOrder) SELECT W.WorkflowID, LTT.ListItem, LTT.ListItemID FROM workflow.Workflow W CROSS APPLY core.ListToTable('CS,CO,SBS,SBAPCR,OSDBU,CloseOut', ',') LTT WHERE W.WorkflowCode = 'HHS3';
GO
--End table workflow.WorkflowStep
