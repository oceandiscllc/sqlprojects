USE SBRS_DHHS
GO

--Begin procedure reviewform.GetReviewFormByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table
-- ================================================================================
CREATE PROCEDURE reviewform.GetReviewFormByReviewFormID

@ReviewFormID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RF.AssignedPersonID,
		RF.BasePrice,
		core.FormatMoney(RF.BasePrice) AS BasePriceFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		RF.CreatePersonID,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		person.GetOriginatingWorkflowStepNumberByPersonID(RF.CreatePersonID) AS OriginatingWorkflowStepNumber,
		RF.CurrentTab,
		RF.Description,
		RF.FundingOrganizationID,
		RF.IsActive,

		CASE
			WHEN LEN(RTRIM(LTRIM(RF.FundingOrganizationName))) > 0
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationName
		END AS FundingOrganizationName,

		RF.OtherResearch,
		core.FormatDate(RF.PeriodOfPerformanceEndDate) AS PeriodOfPerformanceEndDate,
		core.FormatDate(RF.PeriodOfPerformanceStartDate) AS PeriodOfPerformanceStartDate,
		RF.ProcurementHistoryContractAwardAmount,
		core.FormatMoney(RF.ProcurementHistoryContractAwardAmount) AS ProcurementHistoryContractAwardAmountFormatted,
		core.FormatDate(RF.ProcurementHistoryContractAwardDate) AS ProcurementHistoryContractAwardDate,
		core.FormatDate(RF.ProcurementHistoryContractEndDate) AS ProcurementHistoryContractEndDate,
		RF.ProcurementHistoryContractNumber,
		RF.ProcurementHistoryContractTerminationID,
		RF.ProcurementHistoryContractTerminationReason,
		RF.RequisitionNumber1,
		RF.RequisitionNumber2,
		RF.ReviewFormID,
		workflow.GetPersonReviewformAccessLevel(@PersonID, RF.ReviewFormID) AS AccessLevel,
		reviewform.GetTotalEstimatedValue(RF.ReviewFormID) AS TotalEstimatedValue,
		core.FormatMoney(reviewform.GetTotalEstimatedValue(RF.ReviewFormID)) AS TotalEstimatedValueFormatted,
		RF.Size,
		RF.SubContractingPlanReason,
		RF.WorkflowStepNumber,
		BT.BundleTypeID,
		BT.BundleTypeName,
		C.COACSID,
		C.COACSName,
		CT.ContractTerminationID AS ProcurementHistoryContractTerminationID,
		CT.ContractTerminationName AS ProcurementHistoryContractTerminationName,
		N.NAICSID,
		N.NAICSCode,
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted,
		O2.OrganizationFilePath,
		PB.PriorBundlerID,
		PB.PriorBundlerName,
		PH.ProcurementHistoryID,
		PH.ProcurementHistoryName,
		S.StatusName,
		SYN.SynopsisID,
		SYN.SynopsisName,
		SCP.SubContractingPlanID,
		SCP.SubContractingPlanName,
		SER.SynopsisExceptionReasonID,
		SER.SynopsisExceptionReasonName,
		ST.SizeTypeID,
		ST.SizeTypeName,

		CASE
			WHEN @PersonID = RF.CreatePersonID AND S.StatusCode NOT IN ('Complete', 'Withdrawn') AND NOT EXISTS (SELECT 1 FROM eventLog.EventLog EL WHERE EL.EntityTypeCode = 'ReviewForm' AND EL.EntityID = RF.ReviewFormID AND EL.EventCode = 'IncrementWorkflow')
			THEN 1
			ELSE 0
		END AS CanHaveDelete,

		CASE
			WHEN @PersonID = RF.CreatePersonID AND S.StatusCode NOT IN ('Complete', 'Withdrawn') AND EXISTS (SELECT 1 FROM eventLog.EventLog EL WHERE EL.EntityTypeCode = 'ReviewForm' AND EL.EntityID = RF.ReviewFormID AND EL.EventCode = 'IncrementWorkflow')
			THEN 1
			ELSE 0
		END AS CanHaveWithdraw

	FROM reviewform.ReviewForm RF
		JOIN dropdown.BundleType BT ON BT.BundleTypeID = RF.BundleTypeID
		JOIN dropdown.COACS C ON C.COACSID = RF.COACSID
		JOIN dropdown.ContractTermination CT ON CT.ContractTerminationID = RF.ProcurementHistoryContractTerminationID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID		
		JOIN dropdown.PriorBundler PB ON PB.PriorBundlerID = RF.PriorBundlerID
		JOIN dropdown.ProcurementHistory PH ON PH.ProcurementHistoryID = RF.ProcurementHistoryID
		JOIN dropdown.SizeType ST ON ST.SizeTypeID = RF.SizeTypeID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
		JOIN dropdown.SubContractingPlan SCP ON SCP.SubContractingPlanID = RF.SubContractingPlanID
		JOIN dropdown.Synopsis SYN ON SYN.SynopsisID = RF.SynopsisID
		JOIN dropdown.SynopsisExceptionReason SER ON SER.SynopsisExceptionReasonID = RF.SynopsisExceptionReasonID
			AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure reviewform.GetReviewFormByReviewFormID

--Begin procedure reviewform.GetReviewFormContractByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormContractByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormContract table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormContractByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFC.ContractNumber,
		RFC.ReviewFormContractID
	FROM reviewform.ReviewFormContract RFC
	WHERE RFC.ReviewFormID = @ReviewFormID
	ORDER BY RFC.ContractNumber, RFC.ReviewFormContractID

END
GO
--End procedure reviewform.GetReviewFormContractByReviewFormID

--Begin procedure reviewform.GetReviewFormDataByPersonID
EXEC Utility.DropObject 'reviewform.GetReviewFormDataByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.04.07
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on a personid
-- ====================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataByPersonID

@PersonID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT RF.ReviewFormID
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
				AND (RF.CreatePersonID = @PersonID OR RF.AssignedPersonID = @PersonID)

		UNION

		SELECT EL.EntityID AS ReviewFormID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode = 'ReviewForm'
			AND EL.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM reviewform.ReviewForm RF
					JOIN dropdown.Status S ON S.StatusID = RF.StatusID
						AND S.StatusCode NOT IN ('Complete','Withdrawn')
						AND RF.IsActive = 1
						AND RF.ReviewFormID = EL.EntityID
				)
		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataByPersonID

--Begin procedure reviewform.GetReviewFormDataBySearchCriteria
EXEC Utility.DropObject 'reviewform.GetReviewFormDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.03.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on search criteria
-- =========================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataBySearchCriteria

@AssignedPersonID INT = 0,
@AssignedPersonName VARCHAR(100) = NULL,
@COACSID INT = 0,
@ControlCode VARCHAR(50) = NULL,
@CreateDateFrom DATE = NULL,
@CreateDateTo DATE = NULL,
@CreatePersonID INT = 0,
@CreatePersonName VARCHAR(100) = NULL,
@FiscalYear INT = 0,
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@OriginatingOrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@RequisitionNumber VARCHAR(100) = NULL,
@RoleID INT = 0,
@StatusCodeList VARCHAR(MAX) = NULL,
@StatusID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	SET @AssignedPersonName = core.NullIfEmpty(@AssignedPersonName)
	SET @ControlCode = core.NullIfEmpty(@ControlCode)
	SET @CreateDateFrom = core.NullIfEmpty(@CreateDateFrom)
	SET @CreateDateTo = core.NullIfEmpty(@CreateDateTo)
	SET @CreatePersonName = core.NullIfEmpty(@CreatePersonName)
	SET @FundingOrganizationName = core.NullIfEmpty(@FundingOrganizationName)
	SET @RequisitionNumber = core.NullIfEmpty(@RequisitionNumber)
	SET @StatusCodeList = core.NullIfEmpty(@StatusCodeList)

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RF.ReviewFormID
		FROM reviewform.ReviewForm RF
		WHERE RF.IsActive = 1
			AND (@AssignedPersonName IS NULL OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = RF.AssignedPersonID AND (P.FirstName LIKE '%' + @AssignedPersonName + '%' OR P.LastName LIKE '%' + @AssignedPersonName + '%' OR P.FirstName + ' ' + P.LastName LIKE '%' + @AssignedPersonName + '%')))
			AND (@AssignedPersonID = 0 OR (@AssignedPersonID > 0 AND RF.AssignedPersonID = @AssignedPersonID) OR (@AssignedPersonID < 0 AND RF.AssignedPersonID <> ABS(@AssignedPersonID)))
			AND (@COACSID = 0 OR RF.COACSID = @COACSID)
			AND (@ControlCode IS NULL OR RF.ControlCode LIKE '%' + @ControlCode + '%')
			AND (@CreateDateFrom IS NULL OR RF.CreateDateTime >= @CreateDateFrom)
			AND (@CreateDateTo IS NULL OR RF.CreateDateTime <= @CreateDateTo)
			AND (@CreatePersonID = 0 OR RF.CreatePersonID = @CreatePersonID)
			AND (@CreatePersonName IS NULL OR EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = RF.CreatePersonID AND (P.FirstName LIKE '%' + @CreatePersonName + '%' OR P.LastName LIKE '%' + @CreatePersonName + '%' OR P.FirstName + ' ' + P.LastName LIKE '%' + @CreatePersonName + '%')))
			AND (@FiscalYear = 0 OR core.GetFiscalYearFromDateTime(RF.CreateDateTime) = @FiscalYear)
			AND (@FundingOrganizationID = 0 OR RF.FundingOrganizationID = @FundingOrganizationID)
			AND (@FundingOrganizationName IS NULL OR RF.FundingOrganizationName = @FundingOrganizationName)
			AND (@OriginatingOrganizationID = 0 OR RF.FundingOrganizationID = @OriginatingOrganizationID)				
			AND (@RequisitionNumber IS NULL OR RF.RequisitionNumber1 = @RequisitionNumber OR RF.RequisitionNumber2 = @RequisitionNumber)
			AND (@RoleID = 0 OR EXISTS (SELECT 1 FROM workflow.WorkflowStep WS JOIN dropdown.Role R ON R.RoleCode = WS.RoleCode AND R.RoleID = @RoleID AND WS.WorkflowID = RF.WorkflowID AND RF.WorkflowStepNumber = WS.DisplayOrder))
			AND (@StatusCodeList IS NULL OR EXISTS (SELECT 1 FROM dropdown.Status S JOIN core.ListToTable(@StatusCodeList, ',') LTT  ON LTT.ListItem = S.StatusCode AND S.StatusID = RF.StatusID))
			AND (@StatusID = 0 OR RF.StatusID = @StatusID)

		UNION

		SELECT EL.EntityID AS ReviewFormID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode = 'ReviewForm'
			AND EL.PersonID = @CreatePersonID
			AND (CHARINDEX('Complete', 'Complete,Withdrawn') > 0 OR CHARINDEX('Withdrawn', 'Complete,Withdrawn') > 0)
			AND EXISTS
				(
				SELECT 1
				FROM reviewform.ReviewForm RF
					JOIN dropdown.Status S ON S.StatusID = RF.StatusID
						AND S.StatusCode IN ('Complete','Withdrawn')
						AND RF.IsActive = 1
						AND RF.ReviewFormID = EL.EntityID
				)

		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataBySearchCriteria

--Begin procedure reviewform.GetReviewFormDocumentByDocumentID
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentByDocumentID

@ReviewFormDocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE 
			WHEN RFD.IsFileGUIDFileName = 1
			THEN RFD.DocumentPath + RFD.FileGUID
			ELSE RFD.DocumentPath + RFD.DocumentName
		END AS DocumentPath,

		RFD.DocumentName
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

END
GO
--End procedure reviewform.GetReviewFormDocumentByDocumentID
	
--Begin procedure reviewform.GetReviewFormDocumentByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDateTime(RFD.CreateDateTime) AS CreateDateTimeFormatted,
		RFD.FileGUID,
		RFD.DocumentName,

		CASE
			WHEN CHARINDEX('.', RFD.DocumentName) > 0
			THEN REVERSE(LEFT(REVERSE(RFD.DocumentName), CHARINDEX('.', REVERSE(RFD.DocumentName)) - 1))
			ELSE NULL
		END AS FileExtension,

		RFD.IsFileGUIDFileName,
		RFD.ReviewFormDocumentID,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM reviewform.ReviewFormDocument RFD
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = RFD.DocumentTypeID
			AND RFD.ReviewFormID = @ReviewFormID
			AND RFD.IsActive = 1

	UNION

	SELECT
		'',
		'',
		'',
		'',
		'',
		0,
		0,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
	WHERE DT.DocumentTypeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM reviewform.ReviewFormDocument RFD
			WHERE RFD.ReviewFormID = @ReviewFormID
				AND RFD.IsActive = 1
				AND RFD.DocumentTypeID = DT.DocumentTypeID
			)

	ORDER BY DT.DocumentTypeName, DT.DocumentTypeID

END
GO
--End procedure reviewform.GetReviewFormDocumentByReviewFormID
	
--Begin procedure reviewform.GetReviewFormOptionYearByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormOptionYearByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormOptionYear table
-- ==========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormOptionYearByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFOY.OptionYear,
		RFOY.OptionYearValue,
		core.FormatMoney(RFOY.OptionYearValue) AS OptionYearValueFormatted,
		RFOY.ReviewFormID,
		RFOY.ReviewFormOptionYearID
	FROM reviewform.ReviewFormOptionYear RFOY
	WHERE RFOY.ReviewFormID = @ReviewFormID
	ORDER BY RFOY.OptionYear

END
GO
--End procedure reviewform.GetReviewFormOptionYearByReviewFormID
	
--Begin procedure reviewform.GetReviewFormPointOfContactByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormPointOfContactByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormPointOfContact table
-- ==============================================================================================
CREATE PROCEDURE reviewform.GetReviewFormPointOfContactByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPOC.PointOfContactEmailAddress,
		RFPOC.PointOfContactName,
		RFPOC.PointOfContactPhone,
		RFPOC.ReviewFormPointOfContactID
	FROM reviewform.ReviewFormPointOfContact RFPOC
	WHERE RFPOC.ReviewFormID = @ReviewFormID
	ORDER BY RFPOC.PointOfContactName, RFPOC.ReviewFormPointOfContactID

END
GO
--End procedure reviewform.GetReviewFormPointOfContactByReviewFormID

--Begin procedure reviewform.GetReviewFormProcurementHistoryByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormProcurementHistoryByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormProcurementHistory table
-- ==================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormProcurementHistoryByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPH.GSAScheduleNumber,
		RFPH.Percentage,
		RFPH.ReviewFormID,
		RFPH.ReviewFormProcurementHistoryID,
		RFPH.TaskOrderDescription,
		RFPH.TaskOrderNumber,
		RFPH.UnrestrictedProcurementReason,
		FJ.FARJustificationID,
		FJ.FARJustificationName,
		GJ.GSAJustificationID,
		GJ.GSAJustificationName,
		PM.ProcurementMethodID,
		PM.ProcurementMethodName,
		UPR.UnrestrictedProcurementReasonID,
		UPR.UnrestrictedProcurementReasonName
	FROM reviewform.ReviewFormProcurementHistory RFPH
		JOIN dropdown.FARJustification FJ ON FJ.FARJustificationID = RFPH.FARJustificationID
		JOIN dropdown.GSAJustification GJ ON GJ.GSAJustificationID = RFPH.GSAJustificationID
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPH.ProcurementMethodID
		JOIN dropdown.UnrestrictedProcurementReason UPR ON UPR.UnrestrictedProcurementReasonID = RFPH.UnrestrictedProcurementReasonID
			AND RFPH.ReviewFormID = @ReviewFormID
	ORDER BY RFPH.Percentage, PM.ProcurementMethodName, RFPH.ReviewFormProcurementHistoryID

END
GO
--End procedure reviewform.GetReviewFormProcurementHistoryByReviewFormID

--Begin procedure reviewform.GetReviewFormProcurementMethodByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormProcurementMethodByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormProcurementMethod table
-- =================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormProcurementMethodByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFPM.GSAScheduleNumber,
		RFPM.Percentage,
		RFPM.ReviewFormID,
		RFPM.ReviewFormProcurementMethodID,
		RFPM.TaskOrderDescription,
		RFPM.TaskOrderNumber,
		RFPM.UnrestrictedProcurementReason,
		FJ.FARJustificationID,
		FJ.FARJustificationName,
		GJ.GSAJustificationID,
		GJ.GSAJustificationName,
		PM.ProcurementMethodID,
		PM.ProcurementMethodName,
		UPR.UnrestrictedProcurementReasonID,
		UPR.UnrestrictedProcurementReasonName
	FROM reviewform.ReviewFormProcurementMethod RFPM
		JOIN dropdown.FARJustification FJ ON FJ.FARJustificationID = RFPM.FARJustificationID
		JOIN dropdown.GSAJustification GJ ON GJ.GSAJustificationID = RFPM.GSAJustificationID
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = RFPM.ProcurementMethodID
		JOIN dropdown.UnrestrictedProcurementReason UPR ON UPR.UnrestrictedProcurementReasonID = RFPM.UnrestrictedProcurementReasonID
			AND RFPM.ReviewFormID = @ReviewFormID
	ORDER BY RFPM.Percentage, PM.ProcurementMethodName, RFPM.ReviewFormProcurementMethodID

END
GO
--End procedure reviewform.GetReviewFormProcurementMethodByReviewFormID

--Begin procedure reviewform.GetReviewFormResearchTypeByReviewFormID
EXEC utility.DropObject 'reviewform.GetReviewFormResearchTypeByReviewFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to get data from the reviewform.ReviewFormResearchType table
-- ============================================================================================
CREATE PROCEDURE reviewform.GetReviewFormResearchTypeByReviewFormID

@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RT.ResearchTypeID,
		RT.ResearchTypeName
	FROM reviewform.ReviewFormResearchType RFRT
		JOIN dropdown.ResearchType RT ON RT.ResearchTypeID = RFRT.ResearchTypeID
			AND RFRT.ReviewFormID = @ReviewFormID
	ORDER BY RT.ResearchTypeName, RT.ResearchTypeID

END
GO
--End procedure reviewform.GetReviewFormResearchTypeByReviewFormID

--Begin procedure reviewform.SaveReviewForm
EXEC utility.DropObject 'reviewform.SaveReviewForm'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewForm

@AssignedPersonID INT = 0,
@BasePrice NUMERIC(18,2) = 0,
@BundleTypeID INT = 0,
@COACSID INT = 0,
@CreatePersonID INT = 0,
@Description VARCHAR(MAX) = NULL,
@EventCode VARCHAR(50) = 'Update',
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@InactiveDocumentTypeIDList VARCHAR(MAX) = NULL,
@IsActive BIT = 1,
@NAICSID INT = 0,
@OtherResearch VARCHAR(500) = NULL,
@PeriodOfPerformanceEndDate DATE = NULL,
@PeriodOfPerformanceStartDate DATE = NULL,
@PriorBundlerID INT = 0,
@ProcurementHistoryContractAwardAmount NUMERIC(18,2) = 0,
@ProcurementHistoryContractAwardDate DATE = NULL,
@ProcurementHistoryContractEndDate DATE = NULL,
@ProcurementHistoryContractNumber VARCHAR(50) = 0,
@ProcurementHistoryContractTerminationID INT = 0,
@ProcurementHistoryContractTerminationReason VARCHAR(MAX) = NULL,
@ProcurementHistoryID INT = 0,
@RequisitionNumber1 VARCHAR(100) = NULL,
@RequisitionNumber2 VARCHAR(100) = NULL,
@ReviewFormID INT = 0,
@Size VARCHAR(50) = NULL,
@SizeTypeID INT = 0,
@SubContractingPlanID INT = 0,
@SynopsisExceptionReasonID INT = 0,	
@SynopsisID INT = 0,
@WorkflowStepNumber INT = 0,
@CurrentTab INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ReviewFormID = 0
		BEGIN

		DECLARE @tOutput TABLE (ReviewFormID INT)

		INSERT INTO reviewform.ReviewForm
			(
			AssignedPersonID, 
			BasePrice,
			BundleTypeID,
			COACSID,
			ControlCode,
			CreatePersonID,
			CurrentTab,
			Description,
			FundingOrganizationID,
			FundingOrganizationName,
			IsActive,
			NAICSID,
			OriginatingOrganizationID,
			OtherResearch,
			PeriodOfPerformanceEndDate,
			PeriodOfPerformanceStartDate,
			PriorBundlerID,
			ProcurementHistoryContractAwardAmount,
			ProcurementHistoryContractAwardDate,
			ProcurementHistoryContractEndDate,
			ProcurementHistoryContractNumber,
			ProcurementHistoryContractTerminationID,
			ProcurementHistoryContractTerminationReason,
			ProcurementHistoryID,
			RequisitionNumber1,
			RequisitionNumber2,
			Size,
			SizeTypeID,
			StatusID,
			SubContractingPlanID,
			SynopsisExceptionReasonID,
			SynopsisID,
			WorkflowStepNumber
			)
		OUTPUT INSERTED.ReviewFormID INTO @tOutput
		SELECT
			@CreatePersonID,
			@BasePrice,
			@BundleTypeID,
			@COACSID,
			reviewform.CreateControlCode(P.PersonID),
			@CreatePersonID,
			@CurrentTab,
			@Description,
			@FundingOrganizationID,
			@FundingOrganizationName,
			@IsActive,
			@NAICSID,
			P.OrganizationID,
			@OtherResearch,
			@PeriodOfPerformanceEndDate,
			@PeriodOfPerformanceStartDate,
			@PriorBundlerID,
			@ProcurementHistoryContractAwardAmount,
			@ProcurementHistoryContractAwardDate,
			@ProcurementHistoryContractEndDate,
			@ProcurementHistoryContractNumber,
			@ProcurementHistoryContractTerminationID,
			@ProcurementHistoryContractTerminationReason,
			@ProcurementHistoryID,
			@RequisitionNumber1,
			@RequisitionNumber2,
			@Size,
			@SizeTypeID,
			ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0),
			@SubContractingPlanID,
			@SynopsisExceptionReasonID,
			@SynopsisID,
			person.GetOriginatingWorkflowStepNumberByPersonID(@CreatePersonID)
		FROM person.Person P
		WHERE P.PersonID = @CreatePersonID
		
		SELECT @ReviewFormID = O.ReviewFormID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		UPDATE RF
		SET
			RF.AssignedPersonID = @AssignedPersonID,
			RF.BasePrice = @BasePrice,
			RF.BundleTypeID = @BundleTypeID,
			RF.COACSID = @COACSID,
			RF.CurrentTab = @CurrentTab,
			RF.Description = @Description,
			RF.FundingOrganizationID = @FundingOrganizationID,
			RF.FundingOrganizationName = @FundingOrganizationName,
			RF.IsActive = @IsActive,
			RF.NAICSID = @NAICSID,
			RF.OtherResearch = @OtherResearch,
			RF.PeriodOfPerformanceEndDate = @PeriodOfPerformanceEndDate,
			RF.PeriodOfPerformanceStartDate = @PeriodOfPerformanceStartDate,
			RF.PriorBundlerID = @PriorBundlerID,
			RF.ProcurementHistoryContractAwardAmount = @ProcurementHistoryContractAwardAmount,
			RF.ProcurementHistoryContractAwardDate = @ProcurementHistoryContractAwardDate,
			RF.ProcurementHistoryContractEndDate = @ProcurementHistoryContractEndDate,
			RF.ProcurementHistoryContractNumber = @ProcurementHistoryContractNumber,
			RF.ProcurementHistoryContractTerminationID = @ProcurementHistoryContractTerminationID,
			RF.ProcurementHistoryContractTerminationReason = @ProcurementHistoryContractTerminationReason,
			RF.ProcurementHistoryID = @ProcurementHistoryID,
			RF.RequisitionNumber1 = @RequisitionNumber1,
			RF.RequisitionNumber2 = @RequisitionNumber2,
			RF.Size = @Size,
			RF.SizeTypeID = @SizeTypeID,
			RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
			RF.SubContractingPlanID = @SubContractingPlanID,
			RF.SynopsisExceptionReasonID = @SynopsisExceptionReasonID,
			RF.SynopsisID = @SynopsisID,
			RF.WorkflowStepNumber = @WorkflowStepNumber
		FROM reviewform.ReviewForm RF
		WHERE RF.ReviewFormID = @ReviewFormID

		DELETE T FROM reviewform.ReviewFormContract T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormOptionYear T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormPointOfContact T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementHistory T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementMethod T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormResearchType T WHERE T.ReviewFormID = @ReviewFormID
		
		END
	--ENDIF

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

	IF @InactiveDocumentTypeIDList IS NOT NULL
		BEGIN

		UPDATE RFD
		SET RFD.IsActive = 0
		FROM reviewform.ReviewFormDocument RFD
			JOIN core.ListToTable(@InactiveDocumentTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RFD.DocumentTypeID
				AND RFD.ReviewFormID = @ReviewFormID

		END
	--ENDIF

END
GO
--End procedure reviewform.SaveReviewForm

--Begin procedure reviewform.SaveReviewFormWorkflowData
EXEC utility.DropObject 'reviewform.SaveReviewFormWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewFormWorkflowData

@AssignedPersonID INT = 0,
@EventCode VARCHAR(50) = 'Update',
@InactiveDocumentTypeIDList VARCHAR(MAX) = NULL,
@ReviewFormID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE RF
	SET
		RF.AssignedPersonID = @AssignedPersonID,
		RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
		RF.WorkflowStepNumber = @WorkflowStepNumber
	FROM reviewform.ReviewForm RF
	WHERE RF.ReviewFormID = @ReviewFormID

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

	IF @InactiveDocumentTypeIDList IS NOT NULL
		BEGIN

		UPDATE RFD
		SET RFD.IsActive = 0
		FROM reviewform.ReviewFormDocument RFD
			JOIN core.ListToTable(@InactiveDocumentTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RFD.DocumentTypeID
				AND RFD.ReviewFormID = @ReviewFormID

		END
	--ENDIF

END
GO
--End procedure reviewform.SaveReviewFormWorkflowData

--Begin procedure reviewform.SaveReviewFormContract
EXEC utility.DropObject 'reviewform.SaveReviewFormContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormContract

@ContractNumber VARCHAR(100),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormContract
		(
		ContractNumber,
		ReviewFormID
		)
	VALUES
		(
		@ContractNumber,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormContract

--Begin procedure reviewform.SaveReviewFormDocument
EXEC utility.DropObject 'reviewform.SaveReviewFormDocument'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormDocument

@CreatePersonID INT,
@DocumentName VARCHAR(250),
@DocumentPath VARCHAR(50),
@DocumentTypeID INT,
@FileGUID VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormDocument
		(
		CreatePersonID,
		DocumentName,
		DocumentPath,
		DocumentTypeID,
		FileExtension,
		FileGUID,
		ReviewFormID
		)
	VALUES
		(
		@CreatePersonID,
		@DocumentName,
		@DocumentPath,
		@DocumentTypeID,

		CASE
			WHEN CHARINDEX('.', @DocumentName) > 0
			THEN REVERSE(LEFT(REVERSE(@DocumentName), CHARINDEX('.', REVERSE(@DocumentName))))
			ELSE NULL
		END,

		@FileGUID,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormDocument

--Begin procedure reviewform.SaveReviewFormOptionYear
EXEC utility.DropObject 'reviewform.SaveReviewFormOptionYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormOptionYear

@OptionYear INT,
@OptionYearValue NUMERIC(18,2),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormOptionYear
		(
		OptionYear,
		OptionYearValue,
		ReviewFormID
		)
	VALUES
		(
		@OptionYear,
		@OptionYearValue,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormOptionYear

--Begin procedure reviewform.SaveReviewFormPointOfContact
EXEC utility.DropObject 'reviewform.SaveReviewFormPointOfContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormPointOfContact

@PointOfContactEmailAddress VARCHAR(320),
@PointOfContactName VARCHAR(50),
@PointOfContactPhone VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormPointOfContact
		(
		PointOfContactEmailAddress,
		PointOfContactName,
		PointOfContactPhone,
		ReviewFormID
		)
	VALUES
		(
		@PointOfContactEmailAddress,
		@PointOfContactName,
		@PointOfContactPhone,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormPointOfContact

--Begin procedure reviewform.SaveReviewFormProcurementHistory
EXEC utility.DropObject 'reviewform.SaveReviewFormProcurementHistory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormProcurementHistory

@FARJustificationID INT,
@GSAJustificationID INT,
@GSAScheduleNumber VARCHAR(50),
@Percentage INT,
@ProcurementMethodID INT,
@ReviewFormID INT,
@TaskOrderDescription VARCHAR(MAX),
@TaskOrderNumber VARCHAR(50),
@UnrestrictedProcurementReason VARCHAR(MAX),
@UnrestrictedProcurementReasonID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormProcurementHistory
		(
		FARJustificationID,
		GSAJustificationID,
		GSAScheduleNumber,
		Percentage,
		ProcurementMethodID,
		ReviewFormID,
		TaskOrderDescription,
		TaskOrderNumber,
		UnrestrictedProcurementReason,
		UnrestrictedProcurementReasonID
		)
	VALUES
		(
		@FARJustificationID,
		@GSAJustificationID,
		@GSAScheduleNumber,
		@Percentage,
		@ProcurementMethodID,
		@ReviewFormID,
		@TaskOrderDescription,
		@TaskOrderNumber,
		@UnrestrictedProcurementReason,
		@UnrestrictedProcurementReasonID
		)

END
GO
--End procedure reviewform.SaveReviewFormProcurementHistory

--Begin procedure reviewform.SaveReviewFormProcurementMethod
EXEC utility.DropObject 'reviewform.SaveReviewFormProcurementMethod'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormProcurementMethod

@FARJustificationID INT,
@GSAJustificationID INT,
@GSAScheduleNumber VARCHAR(50),
@Percentage INT,
@ProcurementMethodID INT,
@ReviewFormID INT,
@TaskOrderDescription VARCHAR(MAX),
@TaskOrderNumber VARCHAR(50),
@UnrestrictedProcurementReason VARCHAR(MAX),
@UnrestrictedProcurementReasonID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormProcurementMethod
		(
		FARJustificationID,
		GSAJustificationID,
		GSAScheduleNumber,
		Percentage,
		ProcurementMethodID,
		ReviewFormID,
		TaskOrderDescription,
		TaskOrderNumber,
		UnrestrictedProcurementReason,
		UnrestrictedProcurementReasonID
		)
	VALUES
		(
		@FARJustificationID,
		@GSAJustificationID,
		@GSAScheduleNumber,
		@Percentage,
		@ProcurementMethodID,
		@ReviewFormID,
		@TaskOrderDescription,
		@TaskOrderNumber,
		@UnrestrictedProcurementReason,
		@UnrestrictedProcurementReasonID
		)

END
GO
--End procedure reviewform.SaveReviewFormProcurementMethod

--Begin procedure reviewform.SaveReviewFormResearchType
EXEC utility.DropObject 'reviewform.SaveReviewFormResearchType'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormResearchType

@ResearchTypeID INT,
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reviewform.ReviewFormResearchType
		(
		ResearchTypeID,
		ReviewFormID
		)
	VALUES
		(
		@ResearchTypeID,
		@ReviewFormID
		)

END
GO
--End procedure reviewform.SaveReviewFormResearchType
