USE SBRS_DHHS
GO

--Begin procedure dropdown.GetBundleTypeData
EXEC utility.DropObject 'dropdown.GetBundleTypeData'
GO

CREATE PROCEDURE dropdown.GetBundleTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.BundleTypeID,
		T.BundleTypeCode,
		T.BundleTypeName
	FROM dropdown.BundleType T
	WHERE T.IsActive = 1
		AND T.BundleTypeID > 0
	ORDER BY T.DisplayOrder, T.BundleTypeName, T.BundleTypeID

END
GO
--End procedure dropdown.GetBundleTypeData

--Begin procedure dropdown.GetCOACSData
EXEC utility.DropObject 'dropdown.GetCOACSData'
GO

CREATE PROCEDURE dropdown.GetCOACSData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.COACSID,
		T.COACSCode,
		T.COACSName
	FROM dropdown.COACS T
	WHERE T.IsActive = 1
		AND T.COACSID > 0
	ORDER BY T.DisplayOrder, T.COACSName, T.COACSID

END
GO
--End procedure dropdown.GetCOACSData

--Begin procedure dropdown.GetContractTerminationData
EXEC utility.DropObject 'dropdown.GetContractTerminationData'
GO

CREATE PROCEDURE dropdown.GetContractTerminationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractTerminationID,
		T.ContractTerminationCode,
		T.ContractTerminationName
	FROM dropdown.ContractTermination T
	WHERE T.IsActive = 1
		AND T.ContractTerminationID > 0
	ORDER BY T.DisplayOrder, T.ContractTerminationName, T.ContractTerminationID

END
GO
--End procedure dropdown.GetContractTerminationData

--Begin procedure dropdown.GetDocumentTypeData
EXEC utility.DropObject 'dropdown.GetDocumentTypeData'
GO

CREATE PROCEDURE dropdown.GetDocumentTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
	WHERE T.IsActive = 1
		AND T.DocumentTypeID > 0
	ORDER BY T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetFARJustificationData
EXEC utility.DropObject 'dropdown.GetFARJustificationData'
GO

CREATE PROCEDURE dropdown.GetFARJustificationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FARJustificationID,
		T.FARJustificationCode,
		T.FARJustificationName
	FROM dropdown.FARJustification T
	WHERE T.IsActive = 1
		AND T.FARJustificationID > 0
	ORDER BY T.DisplayOrder, T.FARJustificationName, T.FARJustificationID

END
GO
--End procedure dropdown.GetFARJustificationData

--Begin procedure dropdown.GetFundingOrganizationData
EXEC utility.DropObject 'dropdown.GetFundingOrganizationData'
GO

CREATE PROCEDURE dropdown.GetFundingOrganizationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.OrganizationID,
		T.OrganizationCode,
		T.OrganizationName
	FROM dropdown.Organization T
	WHERE T.IsActive = 1
		AND T.OrganizationID > 0
		AND T.IsForFundingOffice = 1
	ORDER BY T.DisplayOrder, T.OrganizationName, T.OrganizationID

END
GO
--End procedure dropdown.GetFundingOrganizationData

--Begin procedure dropdown.GetGSAJustificationData
EXEC utility.DropObject 'dropdown.GetGSAJustificationData'
GO

CREATE PROCEDURE dropdown.GetGSAJustificationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GSAJustificationID,
		T.GSAJustificationCode,
		T.GSAJustificationName
	FROM dropdown.GSAJustification T
	WHERE T.IsActive = 1
		AND T.GSAJustificationID > 0
	ORDER BY T.DisplayOrder, T.GSAJustificationName, T.GSAJustificationID

END
GO
--End procedure dropdown.GetGSAJustificationData

--Begin procedure dropdown.GetNAICSData
EXEC utility.DropObject 'dropdown.GetNAICSData'
GO

CREATE PROCEDURE dropdown.GetNAICSData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.NAICSID,
		T.NAICSCode,
		T.NAICSName,
		T.Size,
		ST.SizeTypeID,
		ST.SizeTypeName
	FROM dropdown.NAICS T
		JOIN dropdown.SizeType ST ON ST.SizeTypeCode = T.SizeTypeCode
			AND T.IsActive = 1
			AND T.NAICSID > 0
	ORDER BY T.DisplayOrder, T.NAICSName, T.NAICSID

END
GO
--End procedure dropdown.GetNAICSData

--Begin procedure dropdown.GetOrganizationData
EXEC utility.DropObject 'dropdown.GetOrganizationData'
GO

CREATE PROCEDURE dropdown.GetOrganizationData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.OrganizationID,
		T.OrganizationCode,
		T.OrganizationName
	FROM dropdown.Organization T
	WHERE T.IsActive = 1
		AND T.OrganizationID > 0
		AND T.OrganizationCode <> 'NON'
	ORDER BY T.DisplayOrder, T.OrganizationName, T.OrganizationID

END
GO
--End procedure dropdown.GetOrganizationData

--Begin procedure dropdown.GetPriorBundlerData
EXEC utility.DropObject 'dropdown.GetPriorBundlerData'
GO

CREATE PROCEDURE dropdown.GetPriorBundlerData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PriorBundlerID,
		T.PriorBundlerCode,
		T.PriorBundlerName
	FROM dropdown.PriorBundler T
	WHERE T.IsActive = 1
		AND T.PriorBundlerID > 0
	ORDER BY T.DisplayOrder, T.PriorBundlerName, T.PriorBundlerID

END
GO
--End procedure dropdown.GetPriorBundlerData

--Begin procedure dropdown.GetProcurementHistoryData
EXEC utility.DropObject 'dropdown.GetProcurementHistoryData'
GO

CREATE PROCEDURE dropdown.GetProcurementHistoryData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProcurementHistoryID,
		T.ProcurementHistoryCode,
		T.ProcurementHistoryName
	FROM dropdown.ProcurementHistory T
	WHERE T.IsActive = 1
		AND T.ProcurementHistoryID > 0
	ORDER BY T.DisplayOrder, T.ProcurementHistoryName, T.ProcurementHistoryID

END
GO
--End procedure dropdown.GetProcurementHistoryData

--Begin procedure dropdown.GetProcurementMethodData
EXEC utility.DropObject 'dropdown.GetProcurementMethodData'
GO

CREATE PROCEDURE dropdown.GetProcurementMethodData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProcurementMethodID,
		T.ProcurementMethodCode,
		T.ProcurementMethodName
	FROM dropdown.ProcurementMethod T
	WHERE T.IsActive = 1
		AND T.ProcurementMethodID > 0
	ORDER BY T.DisplayOrder, T.ProcurementMethodName, T.ProcurementMethodID

END
GO
--End procedure dropdown.GetProcurementMethodData

--Begin procedure dropdown.GetResearchTypeData
EXEC utility.DropObject 'dropdown.GetResearchTypeData'
GO

CREATE PROCEDURE dropdown.GetResearchTypeData

@ReviewFormID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResearchTypeID,
		T.ResearchTypeCode,
		T.ResearchTypeName,
		
		CASE
			WHEN @ReviewFormID > 0 AND EXISTS (SELECT 1 FROM reviewform.ReviewFormResearchType RFRT WHERE RFRT.ReviewFormID = @ReviewFormID AND RFRT.ResearchTypeID = T.ResearchTypeID)
			THEN 1
			ELSE 0
		END AS IsSelected

	FROM dropdown.ResearchType T
	WHERE T.IsActive = 1
		AND T.ResearchTypeID > 0
	ORDER BY T.DisplayOrder, T.ResearchTypeName, T.ResearchTypeID

END
GO
--End procedure dropdown.GetResearchTypeData

--Begin procedure dropdown.GetRoleData
EXEC utility.DropObject 'dropdown.GetRoleData'
GO

CREATE PROCEDURE dropdown.GetRoleData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE T.IsActive = 1
		AND T.RoleID > 0
		AND T.IsForDisplay = 1 
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSizeTypeData
EXEC utility.DropObject 'dropdown.GetSizeTypeData'
GO

CREATE PROCEDURE dropdown.GetSizeTypeData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SizeTypeID,
		T.SizeTypeCode,
		T.SizeTypeName
	FROM dropdown.SizeType T
	WHERE T.IsActive = 1
		AND T.SizeTypeID > 0
	ORDER BY T.DisplayOrder, T.SizeTypeName, T.SizeTypeID

END
GO
--End procedure dropdown.GetSizeTypeData

--Begin procedure dropdown.GetStatusData
EXEC utility.DropObject 'dropdown.GetStatusData'
GO

CREATE PROCEDURE dropdown.GetStatusData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE T.IsActive = 1
		AND T.StatusID > 0
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure dropdown.GetSubContractingOpportunityData
EXEC utility.DropObject 'dropdown.GetSubContractingOpportunityData'
GO
--End procedure dropdown.GetSubContractingOpportunityData

--Begin procedure dropdown.GetSubContractingPlanData
EXEC utility.DropObject 'dropdown.GetSubContractingPlanData'
GO

CREATE PROCEDURE dropdown.GetSubContractingPlanData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractingPlanID,
		T.SubContractingPlanCode,
		T.SubContractingPlanName
	FROM dropdown.SubContractingPlan T
	WHERE T.IsActive = 1
		AND T.SubContractingPlanID > 0
	ORDER BY T.DisplayOrder, T.SubContractingPlanName, T.SubContractingPlanID

END
GO
--End procedure dropdown.GetSubContractingPlanData

--Begin procedure dropdown.GetSynopsisData
EXEC utility.DropObject 'dropdown.GetSynopsisData'
GO

CREATE PROCEDURE dropdown.GetSynopsisData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SynopsisID,
		T.SynopsisCode,
		T.SynopsisName
	FROM dropdown.Synopsis T
	WHERE T.IsActive = 1
		AND T.SynopsisID > 0
	ORDER BY T.DisplayOrder, T.SynopsisName, T.SynopsisID

END
GO
--End procedure dropdown.GetSynopsisData

--Begin procedure dropdown.GetSynopsisExceptionReasonData
EXEC utility.DropObject 'dropdown.GetSynopsisExceptionReasonData'
GO

CREATE PROCEDURE dropdown.GetSynopsisExceptionReasonData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SynopsisExceptionReasonID,
		T.SynopsisExceptionReasonCode,
		T.SynopsisExceptionReasonName
	FROM dropdown.SynopsisExceptionReason T
	WHERE T.IsActive = 1
		AND T.SynopsisExceptionReasonID > 0
	ORDER BY T.DisplayOrder, T.SynopsisExceptionReasonName, T.SynopsisExceptionReasonID

END
GO
--End procedure dropdown.GetSynopsisExceptionReasonData

--Begin procedure dropdown.GetUnrestrictedProcurementReasonData
EXEC utility.DropObject 'dropdown.GetUnrestrictedProcurementReasonData'
GO

CREATE PROCEDURE dropdown.GetUnrestrictedProcurementReasonData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnrestrictedProcurementReasonID,
		T.UnrestrictedProcurementReasonCode,
		T.UnrestrictedProcurementReasonName
	FROM dropdown.UnrestrictedProcurementReason T
	WHERE T.IsActive = 1
		AND T.UnrestrictedProcurementReasonID > 0
	ORDER BY T.DisplayOrder, T.UnrestrictedProcurementReasonName, T.UnrestrictedProcurementReasonID

END
GO
--End procedure dropdown.GetUnrestrictedProcurementReasonData
