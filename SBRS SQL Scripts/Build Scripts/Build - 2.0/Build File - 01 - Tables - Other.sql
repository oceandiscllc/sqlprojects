USE SBRS_DHHS
GO

--Begin table emailtemplate.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'emailtemplate.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE emailtemplate.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EmailTemplateCode VARCHAR(50),
	EmailTemplateName VARCHAR(250),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EmailTemplateCode'
GO
--End table emailtemplate.EmailTemplate

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	Action VARCHAR(100),
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table migration.UserMigration
DECLARE @TableName VARCHAR(250) = 'migration.UserMigration'

EXEC Utility.DropObject 'migration.Person'
EXEC Utility.DropObject @TableName

CREATE TABLE migration.UserMigration
	(
	UserMigrationID INT IDENTITY(1,1) NOT NULL,
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	Password VARCHAR(64),
	PersonID INT,
	UserID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UserID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'UserMigrationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_UserMigration', 'UserID,PersonID'
GO
--End table migration.Person

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC Utility.DropObject @TableName

CREATE TABLE person.Person
	(
	PersonID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	PhoneNumber VARCHAR(50),
	JobTitle VARCHAR(250),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', '0'

EXEC Utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonOrganizationRole
DECLARE @TableName VARCHAR(250) = 'person.PersonOrganizationRole'

EXEC Utility.DropObject @TableName

CREATE TABLE person.PersonOrganizationRole
	(
	PersonOrganizationRoleID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	OrganizationID INT,
	RoleID INT,
	CreateDateTime DATETIME
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PersonOrganizationRoleID'
EXEC Utility.SetIndexClustered @TableName, 'IX_PersonOrganizationRole', 'PersonID, OrganizationID, RoleID'
GO
--End table person.PersonOrganizationRole

--Begin table reviewform.ReviewForm
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewForm'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewForm
	(
	ReviewFormID INT NOT NULL IDENTITY(1,1),
	ControlCode VARCHAR(50),
	RequisitionNumber1 VARCHAR(100),
	RequisitionNumber2 VARCHAR(100),
	COACSID INT,
	FundingOrganizationID INT,
	FundingOrganizationName VARCHAR(250),
	BasePrice	NUMERIC(18,2),
	PeriodOfPerformanceStartDate DATE,
	PeriodOfPerformanceEndDate DATE,
	NAICSID INT,
	SizeTypeID INT,
	Size VARCHAR(50),
	SynopsisID INT,
	SynopsisExceptionReasonID	INT,	
	Description VARCHAR(MAX),
	ProcurementHistoryID INT,
	ProcurementHistoryContractAwardAmount	NUMERIC(18,2),
	ProcurementHistoryContractAwardDate	DATE,
	ProcurementHistoryContractEndDate	DATE,
	ProcurementHistoryContractNumber VARCHAR(50),
	ProcurementHistoryContractTerminationID INT,
	ProcurementHistoryContractTerminationReason	VARCHAR(MAX),
	OtherResearch VARCHAR(500),
	BundleTypeID INT,
	PriorBundlerID INT,
	SubContractingPlanID INT,
	SubContractingPlanReason VARCHAR(MAX),
	AssignedPersonID INT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	IsActive BIT,
	OriginatingOrganizationID INT,
	StatusID INT,
	WorkflowID INT,
	WorkflowStepNumber INT,
	CurrentTab INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'AssignedPersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'BasePrice', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'BundleTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'COACSID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'CurrentTab', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'PriorBundlerID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryContractAwardAmount', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryContractTerminationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementHistoryID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SizeTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SubContractingPlanID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SynopsisExceptionReasonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'SynopsisID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyClustered @TableName, 'ReviewFormID'
GO
--End table reviewform.ReviewForm

--Begin table reviewform.ReviewFormContract
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormContract'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormContract
	(
	ReviewFormContractID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ContractNumber VARCHAR(100),
	)
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormContractID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormContract', 'ReviewFormID'
GO
--End table reviewform.ReviewFormContract

--Begin table reviewform.ReviewFormDocument
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormDocument'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormDocument
	(
	ReviewFormDocumentID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	DocumentPath VARCHAR(50),
	DocumentName VARCHAR(250),
	DocumentTypeID INT,
	FileGUID VARCHAR(50),
	FileExtension VARCHAR(10),
	IsFileGUIDFileName BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'FileGUID', 'VARCHAR(50)', 'newID()'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsFileGUIDFileName', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'

EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormDocumentID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormDocument', 'ReviewFormID'
GO
--End table reviewform.ReviewFormDocument

--Begin table reviewform.ReviewFormOptionYear
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormOptionYear'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormOptionYear
	(
	ReviewFormOptionYearID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	OptionYear INT,
	OptionYearValue	NUMERIC(18,2)
	)

EXEC Utility.SetDefaultConstraint @TableName, 'OptionYear', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'OptionYearValue', 'NUMERIC(18,2)', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormOptionYearID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormOptionYear', 'ReviewFormID, OptionYear'
GO
--End table reviewform.ReviewFormOptionYear

--Begin table reviewform.ReviewFormPointOfContact
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormPointOfContact'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormPointOfContact
	(
	ReviewFormPointOfContactID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	PointOfContactName VARCHAR(50),
	PointOfContactPhone VARCHAR(50),
	PointOfContactEmailAddress VARCHAR(320)
	)

EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormPointOfContactID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormPointOfContact', 'ReviewFormID, PointOfContactName'
GO
--End table reviewform.ReviewFormPointOfContact

--Begin table reviewform.ReviewFormProcurementHistory
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormProcurementHistory'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormProcurementHistory
	(
	ReviewFormProcurementHistoryID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ProcurementMethodID INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	Percentage INT,
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'FARJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'GSAJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'Percentage', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementMethodID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UnrestrictedProcurementReasonID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormProcurementHistoryID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormProcurementHistory', 'ReviewFormID'
GO
--End table reviewform.ReviewFormProcurementHistory

--Begin table reviewform.ReviewFormProcurementMethod
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormProcurementMethod'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormProcurementMethod
	(
	ReviewFormProcurementMethodID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ProcurementMethodID INT,
	FARJustificationID INT,
	GSAJustificationID INT,
	GSAScheduleNumber	VARCHAR(50),
	Percentage INT,
	TaskOrderDescription VARCHAR(MAX),
	TaskOrderNumber VARCHAR(50),
	UnrestrictedProcurementReason VARCHAR(MAX),
	UnrestrictedProcurementReasonID	INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'FARJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'GSAJustificationID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'Percentage', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ProcurementMethodID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'UnrestrictedProcurementReasonID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormProcurementMethodID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormProcurementMethod', 'ReviewFormID'
GO
--End table reviewform.ReviewFormProcurementMethod

--Begin table reviewform.ReviewFormResearchType
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewFormResearchType'

EXEC Utility.DropObject @TableName

CREATE TABLE reviewform.ReviewFormResearchType
	(
	ReviewFormResearchTypeID INT NOT NULL IDENTITY(1,1),
	ReviewFormID INT,
	ResearchTypeID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'ResearchTypeID', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'ReviewFormID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'ReviewFormResearchTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ReviewFormResearchType', 'ReviewFormID, ResearchTypeID'
GO
--End table reviewform.ReviewFormResearchType

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC Utility.DropObject @TableName

CREATE TABLE workflow.Workflow
	(
	WorkflowID INT NOT NULL IDENTITY(1,1),
	WorkflowCode VARCHAR(50),
	OrganizationCode VARCHAR(50),
	LevelCode VARCHAR(50)
	)
	
EXEC Utility.SetPrimaryKeyClustered @TableName, 'WorkflowID'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC Utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStep
	(
	WorkflowStepID INT NOT NULL IDENTITY(1,1),
	WorkflowID INT,
	RoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT,
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', '0'
	
EXEC Utility.SetPrimaryKeyNonClustered @TableName, 'WorkflowStepID'
EXEC Utility.SetIndexClustered @TableName, 'IX_WorkflowStep', 'WorkflowID,DisplayOrder'
GO
--End table workflow.WorkflowStep
