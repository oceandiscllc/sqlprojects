USE SBRS_DHHS
GO

--Begin table dropdown.BundleType
DECLARE @TableName VARCHAR(250) = 'dropdown.BundleType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.BundleType
	(
	BundleTypeID INT IDENTITY(0,1) NOT NULL,
	BundleTypeCode VARCHAR(50),
	BundleTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'BundleTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_BundleType', 'DisplayOrder, BundleTypeName'
GO
--End table dropdown.BundleType

--Begin table dropdown.COACS
DECLARE @TableName VARCHAR(250) = 'dropdown.COACS'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.COACS
	(
	COACSID INT IDENTITY(0,1) NOT NULL,
	COACSCode VARCHAR(50),
	COACSName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'COACSID'
EXEC Utility.SetIndexClustered @TableName, 'IX_COACS', 'DisplayOrder,COACSName,COACSID'
GO
--End table dropdown.COACS

--Begin table dropdown.ContractTermination
DECLARE @TableName VARCHAR(250) = 'dropdown.ContractTermination'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ContractTermination
	(
	ContractTerminationID INT IDENTITY(0,1) NOT NULL,
	ContractTerminationCode VARCHAR(50),
	ContractTerminationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractTerminationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ContractTermination', 'DisplayOrder,ContractTerminationName,ContractTerminationID'
GO
--End table dropdown.ContractTermination

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT IDENTITY(0,1) NOT NULL,
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'DocumentTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_DocumentType', 'DisplayOrder,DocumentTypeName,DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.FARJustification
DECLARE @TableName VARCHAR(250) = 'dropdown.FARJustification'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.FARJustification
	(
	FARJustificationID INT IDENTITY(0,1) NOT NULL,
	FARJustificationCode VARCHAR(50),
	FARJustificationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'FARJustificationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_FARJustification', 'DisplayOrder,FARJustificationName,FARJustificationID'
GO
--End table dropdown.FARJustification

--Begin table dropdown.GSAJustification
DECLARE @TableName VARCHAR(250) = 'dropdown.GSAJustification'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.GSAJustification
	(
	GSAJustificationID INT IDENTITY(0,1) NOT NULL,
	GSAJustificationCode VARCHAR(50),
	GSAJustificationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'GSAJustificationID'
EXEC Utility.SetIndexClustered @TableName, 'IX_GSAJustification', 'DisplayOrder,GSAJustificationName,GSAJustificationID'
GO
--End table dropdown.GSAJustification

--Begin table dropdown.NAICS
DECLARE @TableName VARCHAR(250) = 'dropdown.NAICS'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.NAICS
	(
	NAICSID INT IDENTITY(0,1) NOT NULL,
	NAICSCode VARCHAR(50),
	NAICSName VARCHAR(250),
	Size VARCHAR(50),
	SizeTypeCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'NAICSID'
EXEC Utility.SetIndexClustered @TableName, 'IX_NAICS', 'DisplayOrder,NAICSCode,NAICSName,NAICSID'
GO
--End table dropdown.NAICS

--Begin table dropdown.Organization
DECLARE @TableName VARCHAR(250) = 'dropdown.Organization'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Organization
	(
	OrganizationID INT IDENTITY(0,1) NOT NULL,
	OrganizationCode VARCHAR(50),
	OrganizationDisplayCode VARCHAR(50),
	OrganizationFilePath VARCHAR(50),
	OrganizationName VARCHAR(250),
	DisplayOrder INT,
	IsForFundingOffice BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsForFundingOffice', 'BIT', '1'

EXEC Utility.SetPrimaryKeyClustered @TableName, 'OrganizationID'
GO
--End table dropdown.Organization

--Begin table dropdown.PriorBundler
DECLARE @TableName VARCHAR(250) = 'dropdown.PriorBundler'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.PriorBundler
	(
	PriorBundlerID INT IDENTITY(0,1) NOT NULL,
	PriorBundlerCode VARCHAR(50),
	PriorBundlerName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PriorBundlerID'
EXEC Utility.SetIndexClustered @TableName, 'IX_PriorBundler', 'DisplayOrder, PriorBundlerName'
GO
--End table dropdown.PriorBundler

--Begin table dropdown.ProcurementHistory
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementHistory'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ProcurementHistory
	(
	ProcurementHistoryID INT IDENTITY(0,1) NOT NULL,
	ProcurementHistoryCode VARCHAR(50),
	ProcurementHistoryName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ProcurementHistoryID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ProcurementHistory', 'DisplayOrder, ProcurementHistoryName'
GO
--End table dropdown.ProcurementHistory

--Begin table dropdown.ProcurementType
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementMethod'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ProcurementMethod
	(
	ProcurementMethodID INT IDENTITY(0,1) NOT NULL,
	ProcurementMethodCode VARCHAR(50),
	ProcurementMethodName VARCHAR(250),
	DisplayOrder INT,
	IsSetAside BIT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsSetAside', 'BIT', '0'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ProcurementMethodID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ProcurementMethod', 'DisplayOrder, ProcurementMethodName'
GO
--End table dropdown.ProcurementMethod

--Begin table dropdown.ResearchType
DECLARE @TableName VARCHAR(250) = 'dropdown.ResearchType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.ResearchType
	(
	ResearchTypeID INT IDENTITY(0,1) NOT NULL,
	ResearchTypeCode VARCHAR(50),
	ResearchTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ResearchTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_ResearchType', 'DisplayOrder, ResearchTypeName'
GO
--End table dropdown.ResearchType

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleCode VARCHAR(50),
	RoleName VARCHAR(250),
	DisplayOrder INT,
	IsForDisplay BIT,
	HasOrganization BIT,
	NotificationRoleCode VARCHAR(50),
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'HasOrganization', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC Utility.SetDefaultConstraint @TableName, 'IsForDisplay', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'RoleID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Role', 'DisplayOrder, RoleName'
GO
--End table dropdown.Role

--Begin table dropdown.SizeType
DECLARE @TableName VARCHAR(250) = 'dropdown.SizeType'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SizeType
	(
	SizeTypeID INT IDENTITY(0,1) NOT NULL,
	SizeTypeCode VARCHAR(50),
	SizeTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SizeTypeID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SizeType', 'DisplayOrder, SizeTypeName'
GO
--End table dropdown.SizeType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'StatusID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Status', 'DisplayOrder, StatusName'
GO
--End table dropdown.Status

--Begin table dropdown.SubContractingPlan
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractingPlan'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SubContractingPlan
	(
	SubContractingPlanID INT IDENTITY(0,1) NOT NULL,
	SubContractingPlanCode VARCHAR(50),
	SubContractingPlanName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SubContractingPlanID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SubContractingPlan', 'DisplayOrder, SubContractingPlanName'
GO
--End table dropdown.SubContractingPlan

--Begin table dropdown.Synopsis
DECLARE @TableName VARCHAR(250) = 'dropdown.Synopsis'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.Synopsis
	(
	SynopsisID INT IDENTITY(0,1) NOT NULL,
	SynopsisCode VARCHAR(50),
	SynopsisName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SynopsisID'
EXEC Utility.SetIndexClustered @TableName, 'IX_Synopsis', 'DisplayOrder, SynopsisName'
GO
--End table dropdown.Synopsis

--Begin table dropdown.SynopsisExceptionReason
DECLARE @TableName VARCHAR(250) = 'dropdown.SynopsisExceptionReason'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.SynopsisExceptionReason
	(
	SynopsisExceptionReasonID INT IDENTITY(0,1) NOT NULL,
	SynopsisExceptionReasonCode VARCHAR(50),
	SynopsisExceptionReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'SynopsisExceptionReasonID'
EXEC Utility.SetIndexClustered @TableName, 'IX_SynopsisExceptionReason', 'DisplayOrder, SynopsisExceptionReasonName'
GO
--End table dropdown.SynopsisExceptionReason

--Begin table dropdown.UnrestrictedProcurementReason
DECLARE @TableName VARCHAR(250) = 'dropdown.UnrestrictedProcurementReason'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.UnrestrictedProcurementReason
	(
	UnrestrictedProcurementReasonID INT IDENTITY(0,1) NOT NULL,
	UnrestrictedProcurementReasonCode VARCHAR(50),
	UnrestrictedProcurementReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'UnrestrictedProcurementReasonID'
EXEC Utility.SetIndexClustered @TableName, 'IX_UnrestrictedProcurementReason', 'DisplayOrder, UnrestrictedProcurementReasonName'
GO
--End table dropdown.UnrestrictedProcurementReason