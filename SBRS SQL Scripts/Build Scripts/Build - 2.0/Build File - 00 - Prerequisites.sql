USE SBRS_DHHS
GO

--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN [' + @ColumnName + ']'
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex

@TableName VARCHAR(250),
@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX),
@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered

EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'dropdown'
EXEC utility.AddSchema 'emailtemplate'
EXEC utility.AddSchema 'eventlog'
EXEC utility.AddSchema 'migration'
EXEC utility.AddSchema 'person'
EXEC utility.AddSchema 'reviewform'
EXEC utility.AddSchema 'workflow'
GO
