USE SBRS_DHHS
GO

--Begin table dropdown.ProcurementMethod
UPDATE PM
SET PM.ProcurementMethodShortName =
	CASE
		WHEN PM.ProcurementMethodCode = 'EDWOSB'
		THEN 'EDWOSB Set-Aside'
		WHEN PM.ProcurementMethodCode = 'GSA'
		THEN 'GSA Schedule'
		WHEN PM.ProcurementMethodCode = 'Indian'
		THEN 'Buy Indian Act'
		WHEN PM.ProcurementMethodCode = 'WOSBC'
		THEN 'WOSB Competitive'
		WHEN PM.ProcurementMethodCode = 'WOSBSS'
		THEN 'WOSB Sole Source'
		ELSE PM.ProcurementMethodName
	END
FROM dropdown.ProcurementMethod PM
GO
--End table dropdown.ProcurementMethod

--Begin table metrics.ChartMetadata
TRUNCATE TABLE metrics.ChartMetadata
GO

INSERT INTO metrics.ChartMetadata
	(ChartCode, ChartTitle, ChartType, HAxisTitle, Orientation, VAxisTitle)
VALUES
	('CompletedByOperatingDivision', 'Completed Reviews By Operating Division', 'ColumnChart', '# of Reviews', 'vertical', 'Operating Division'),
	('CompletedByProcurementMethod', 'Completed Reviews By Procurement Method', 'PieChart', NULL, NULL, NULL),
	('CreatedByMonth', 'Reviews Created By Month', 'ColumnChart', 'Month', NULL, '# of Reviews'),
	('OpenByOperatingDivision', 'Open Reviews By Operating Division', 'ColumnChart', '# of Reviews', 'vertical', 'Operating Division'),
	('OpenByProcurementMethod', 'Open Reviews By Procurement Method', 'PieChart', NULL, NULL, NULL),
	('WorkflowAgeing', 'Workflow Step Aging', 'ColumnChart', 'Days', NULL, '# of Reviews'),
	('WorkflowPercentComplete', 'Workflow % Complete', 'ColumnChart', '% Complete', NULL, '# of Reviews')
GO
--End table metrics.ChartMetadata

--Begin table metrics.ChartItemMetadata
TRUNCATE TABLE metrics.ChartItemMetadata
GO

INSERT INTO metrics.ChartItemMetadata
	(ChartCode, ItemCode, ItemLegend, ItemTitle)
VALUES
	('WorkflowAgeing', 1, '<= 7', 'Reviews that have been in their current workflow step for 7 days or less'),
	('WorkflowAgeing', 2, '<= 14', 'Reviews that have been in their current workflow step between 8 and 14 days'),
	('WorkflowAgeing', 3, '<= 30', 'Reviews that have been in their current workflow step between 15 and 30 days'),
	('WorkflowAgeing', 4, '<= 60', 'Reviews that have been in their current workflow step between 31 and 60 days'),
	('WorkflowAgeing', 5, '<= 90', 'Reviews that have been in their current workflow step between 61 and 90 days'),
	('WorkflowAgeing', 6, '> 90', 'Reviews that have been in their current workflow step more than 90 days'),
	('WorkflowPercentComplete', 1, '<= 25', 'Reviews that are 25% or less through their workflow'),
	('WorkflowPercentComplete', 2, '<= 50', 'Reviews that are 50% or less through their workflow'),
	('WorkflowPercentComplete', 3, '<= 75', 'Reviews that are 75% or less through their workflow'),
	('WorkflowPercentComplete', 4, '> 75', 'Reviews that are more than 75% through their workflow')
GO
--End table metrics.ChartItemMetadata
