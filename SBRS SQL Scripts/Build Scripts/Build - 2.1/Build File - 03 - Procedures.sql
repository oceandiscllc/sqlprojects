USE SBRS_DHHS
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteByAnnouncementID'
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime),
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonName,
		core.FormatDate(A.StartDate) AS StartDate,
		core.FormatDate(A.EndDate) AS EndDate
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTime,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonName,
		core.FormatDate(A.StartDate) AS StartDate,
		core.FormatDate(A.EndDate) AS EndDate
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.SaveAnnouncement
EXEC Utility.DropObject 'core.SaveAnnouncement'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.SaveAnnouncement

@AnnouncementID INT = 0,
@AnnouncementText VARCHAR(MAX), 
@CreatePersonID INT = 0,
@EndDate DATE, 
@StartDate DATE

AS
BEGIN
	SET NOCOUNT ON;

	IF @AnnouncementID = 0
		BEGIN

		INSERT INTO core.Announcement
			(StartDate, EndDate, AnnouncementText, CreatePersonID)
		VALUES
			(
			@StartDate, 
			@EndDate, 
			@AnnouncementText, 
			@CreatePersonID
			)

		END
	ELSE
		BEGIN

		UPDATE A
		SET 
			A.AnnouncementText = @AnnouncementText, 
			A.EndDate = @EndDate, 
			A.StartDate = @StartDate
		FROM core.Announcement A
		WHERE A.AnnouncementID = @AnnouncementID

		END
	--ENDIF

END
GO
--End procedure core.SaveAnnouncement

--Begin procedure dropdown.GetFiscalYearData
EXEC Utility.DropObject 'dropdown.GetFiscalYearData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.10
-- Description:	A stored procedure to get a distinct list of fiscal years from the reviewform.ReviewForm table
-- ===========================================================================================================
CREATE PROCEDURE dropdown.GetFiscalYearData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT core.GetFiscalYearFromDateTime(RF.CreateDateTime) AS Year
	FROM reviewform.ReviewForm RF
	ORDER BY 1 DESC
	
END
GO
--End procedure dropdown.GetFiscalYearData

--Begin procedure metrics.GetChartMetaData
EXEC Utility.DropObject 'metrics.GetChartMetaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get the metrics chart metadata
-- =================================================================
CREATE PROCEDURE metrics.GetChartMetaData

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	SELECT 
		CMD.ChartCode, 
		'FY ' + CAST(@FiscalYear AS CHAR(4)) + ' ' + CMD.ChartTitle AS ChartTitle, 
		CMD.ChartType, 
		CMD.HAxisTitle, 
		CMD.Orientation, 
		CMD.VAxisTitle
	FROM metrics.ChartMetaData CMD
	ORDER BY 1
	
END
GO
--End procedure metrics.GetChartMetaData

--Begin procedure metrics.GetCompletedOpDivCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedOpDivCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCompletedOpDivCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (OrganizationID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (OrganizationID) SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationID > 0 AND O.IsActive = 1 AND O.IsForFundingOffice = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CODD.ItemCode) AS ItemCount,
				CODD.ItemCode
			FROM metrics.GetCompletedOpDivDataByFiscalYear(@FiscalYear) CODD
			GROUP BY CODD.ItemCode
			) D ON D.ItemCode = T.OrganizationID

	SELECT
		T.ItemCount,
		O.OrganizationID AS ItemCode,
		O.OrganizationDisplayCode AS ItemName
	FROM @tTable T
		JOIN dropdown.Organization O ON O.OrganizationID = T.OrganizationID
	ORDER BY 3

END
GO
--End procedure metrics.GetCompletedOpDivCountsByFiscalYear

--Begin procedure metrics.GetCompletedProcurementMethodCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedProcurementMethodCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCompletedProcurementMethodCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ProcurementMethodID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ProcurementMethodID) SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID > 0 AND PM.IsActive = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CPMD.ItemCode) AS ItemCount,
				CPMD.ItemCode
			FROM metrics.GetCompletedProcurementMethodDataByFiscalYear(@FiscalYear) CPMD
			GROUP BY CPMD.ItemCode
			) D ON D.ItemCode = T.ProcurementMethodID

	SELECT
		T.ItemCount,
		PM.ProcurementMethodID AS ItemCode,
		PM.ProcurementMethodShortName AS ItemName
	FROM @tTable T
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = T.ProcurementMethodID
	ORDER BY 3

END
GO
--End procedure metrics.GetCompletedProcurementMethodCountsByFiscalYear

--Begin procedure metrics.GetCreatedMonthCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCreatedMonthCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCreatedMonthCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ItemCode INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ItemCode) SELECT NR.Number FROM core.CreateNumericRange(1, 12) NR

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CMD.ReviewFormID) AS ItemCount,
				CMD.ItemCode
			FROM metrics.GetCreateMonthDataByFiscalYear(@FiscalYear) CMD
			GROUP BY CMD.ItemCode
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount,
		T.ItemCode,
		LEFT(DATENAME(month, DATEADD(month, T.ItemCode, -1)), 3) AS ItemName
	FROM @tTable T
	ORDER BY 
		CASE
			WHEN T.ItemCode > 9
			THEN T.ItemCode - 9
			ELSE T.ItemCode + 3
		END


END
GO
--End procedure metrics.GetCreatedMonthCountsByFiscalYear

--Begin procedure metrics.GetMetricsMetaData
EXEC utility.DropObject 'metrics.GetMetricsMetaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetMetricsMetaData

@ChartCode VARCHAR(50) = NULL,
@ItemCode INT = 0,
@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	SELECT 
		'FY ' + CAST(@FiscalYear AS CHAR(4)) + ' ' + CMD.ChartTitle + ' - ' + 
			CASE
				WHEN @ChartCode IN ('CompletedByOperatingDivision', 'OpenByOperatingDivision')
				THEN (SELECT O.OrganizationDisplayCode FROM dropdown.Organization O WHERE O.OrganizationID = @ItemCode)
				WHEN @ChartCode = 'CreatedByMonth'
				THEN LEFT(DATENAME(month, DATEADD(month, @ItemCode, -1)), 3)
				WHEN @ChartCode IN ('CompletedByProcurementMethod', 'OpenByProcurementMethod')
				THEN (SELECT PM.ProcurementMethodShortName FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID = @ItemCode)
				WHEN @ChartCode IN ('WorkflowAgeing', 'WorkflowPercentComplete')
				THEN (SELECT CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = CMD.ChartCode AND CIMD.ItemCode = @ItemCode)
			END AS MetricsDataTitle
	FROM metrics.ChartMetaData CMD
	WHERE CMD.ChartCode = @ChartCode

END
GO
--End procedure metrics.GetMetricsMetaData

--Begin procedure metrics.GetOpenOpDivCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenOpDivCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetOpenOpDivCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (OrganizationID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (OrganizationID) SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationID > 0 AND O.IsActive = 1 AND O.IsForFundingOffice = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(OODD.ItemCode) AS ItemCount,
				OODD.ItemCode
			FROM metrics.GetOpenOpDivDataByFiscalYear(@FiscalYear) OODD
			GROUP BY OODD.ItemCode
			) D ON D.ItemCode = T.OrganizationID

	SELECT
		T.ItemCount,
		O.OrganizationID AS ItemCode,
		O.OrganizationDisplayCode AS ItemName
	FROM @tTable T
		JOIN dropdown.Organization O ON O.OrganizationID = T.OrganizationID
	ORDER BY 3

END
GO
--End procedure metrics.GetOpenOpDivCountsByFiscalYear

--Begin procedure metrics.GetOpenProcurementMethodCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenProcurementMethodCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetOpenProcurementMethodCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ProcurementMethodID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ProcurementMethodID) SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID > 0 AND PM.IsActive = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(OPMD.ItemCode) AS ItemCount,
				OPMD.ItemCode
			FROM metrics.GetOpenProcurementMethodDataByFiscalYear(@FiscalYear) OPMD
			GROUP BY OPMD.ItemCode
			) D ON D.ItemCode = T.ProcurementMethodID

	SELECT
		T.ItemCount,
		PM.ProcurementMethodID AS ItemCode,
		PM.ProcurementMethodShortName AS ItemName
	FROM @tTable T
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = T.ProcurementMethodID
	ORDER BY 3

END
GO
--End procedure metrics.GetOpenProcurementMethodCountsByFiscalYear

--Begin procedure metrics.GetWorkflowStepAgeCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepAgeCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetWorkflowStepAgeCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ItemCount INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))

	INSERT INTO @tTable (ItemCode, ItemName) SELECT CIMD.ItemCode, CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = 'WorkflowAgeing'

	UPDATE T
	SET 
		T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(WSAD.ItemCode) AS ItemCount,
				WSAD.ItemCode
			FROM metrics.GetWorkflowStepAgeDataByFiscalYear(@FiscalYear) WSAD
			GROUP BY WSAD.ItemCode
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount, 
		T.ItemCode, 
		T.ItemName
	FROM @tTable T
	ORDER BY T.ItemCode

END
GO
--End procedure metrics.GetWorkflowStepAgeCountsByFiscalYear

--Begin procedure metrics.GetWorkflowStepCompletionCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepCompletionCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetWorkflowStepCompletionCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ItemCount INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))

	INSERT INTO @tTable (ItemCode, ItemName) SELECT CIMD.ItemCode, CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = 'WorkflowPercentComplete'

	UPDATE T
	SET 
		T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(WSCD.ItemCode) AS ItemCount,
				WSCD.ItemCode
			FROM metrics.GetWorkflowStepCompletionDataByFiscalYear(@FiscalYear) WSCD
			GROUP BY WSCD.ItemCode, WSCD.ItemName
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount, 
		T.ItemCode, 
		T.ItemName
	FROM @tTable T
	ORDER BY T.ItemCode

END
GO
--End procedure metrics.GetWorkflowStepCompletionCountsByFiscalYear

--Begin procedure reviewform.DeleteReviewFormDocumentByeviewFormDocumentID
EXEC Utility.DropObject 'reviewform.DeleteReviewFormDocumentByeviewFormDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.07
-- Description:	A stored procedure to delete data from the reviewform.ReviewFormDocument table
-- ===========================================================================================
CREATE PROCEDURE reviewform.DeleteReviewFormDocumentByeviewFormDocumentID

@ReviewFormDocumentID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cComments VARCHAR(MAX)
	DECLARE @cDocumentName VARCHAR(250)
	DECLARE @nReviewFormID INT
	
	SELECT 
		@cDocumentName = RFD.DocumentName,
		@nReviewFormID = RFD.ReviewFormID
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

	DELETE RFD
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

	SET @cComments = 'Deleted file ' + @cDocumentName

	EXEC eventlog.LogReviewFormAction 
		@Action = 'Delete File', 
		@Comments = @cComments,
		@EntityID = @nReviewFormID,
		@EventCode = 'Update',
		@PersonID = @PersonID

END
GO

--End procedure reviewform.DeleteReviewFormDocumentByeviewFormDocumentID

--Begin procedure reviewform.GetReviewFormDataByMetricsSearchCriteria
EXEC Utility.DropObject 'reviewform.GetReviewFormDataByMetricsSearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.03.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on search criteria
-- =========================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataByMetricsSearchCriteria

@ChartCode VARCHAR(50) = NULL,
@FiscalYear INT = 0,
@ItemCode INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RF.ReviewFormID
		FROM reviewform.ReviewForm RF
		WHERE 1 = 1
			AND (@ChartCode <> 'CompletedByOperatingDivision' OR EXISTS (SELECT 1 FROM metrics.GetCompletedOpDivDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'CompletedByProcurementMethod' OR EXISTS (SELECT 1 FROM metrics.GetCompletedProcurementMethodDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'CreatedByMonth' OR EXISTS (SELECT 1 FROM metrics.GetCreateMonthDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'OpenByOperatingDivision' OR EXISTS (SELECT 1 FROM metrics.GetOpenOpDivDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'OpenByProcurementMethod' OR EXISTS (SELECT 1 FROM metrics.GetOpenProcurementMethodDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'WorkflowAgeing' OR EXISTS (SELECT 1 FROM metrics.GetWorkflowStepAgeDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'WorkflowPercentComplete' OR EXISTS (SELECT 1 FROM metrics.GetWorkflowStepCompletionDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataByMetricsSearchCriteria
	
--Begin procedure reviewform.GetReviewFormDocumentDataBySearchCriteria
--EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByReviewFormID'
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.07
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentDataBySearchCriteria

@ReviewFormID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RFD.ReviewFormDocumentID
		FROM reviewform.ReviewFormDocument RFD
		WHERE RFD.ReviewFormID = @ReviewFormID
			AND RFD.IsActive = 1
		)

	SELECT 
		person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDateTime(RFD.CreateDateTime) AS CreateDateTimeFormatted,
		'<img src="/assets/images/file-icons/' 
			+ ISNULL(core.GetFileExtension(RFD.DocumentName), 'txt') 
			+ '.png" class="fileIcon pull-left">&nbsp;<a href="javascript:deleteDocument(' 
			+ CAST(RFD.ReviewFormDocumentID AS VARCHAR(10)) 
			+ ');" class="btn btn-xs btn-danger pull-right"><i class="hhsicon icon-x"></i></a>&nbsp;&nbsp;'
			+ '<a href="/Review/GetFile/' 
			+ CAST(RFD.ReviewFormDocumentID AS VARCHAR(10)) 
			+ '" title="' 
			+ RFD.DocumentName 
			+ '">' 
			+ RFD.DocumentName 
			+ '</a>' AS DocumentName,
		RFD.ReviewFormDocumentID,
		DT.DocumentTypeName,
		(SELECT COUNT(PD.ReviewFormDocumentID) FROM PD) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormDocumentID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewFormDocument RFD ON RFD.ReviewFormDocumentID = PD.ReviewFormDocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = RFD.DocumentTypeID
	ORDER BY
		CASE WHEN @OrderByField = 'CreateDateTimeFormatted' AND @OrderByDirection = 'ASC' THEN RFD.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDateTimeFormatted' AND @OrderByDirection = 'DESC' THEN RFD.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'DocumentName' AND @OrderByDirection = 'ASC' THEN RFD.DocumentName END ASC,
		CASE WHEN @OrderByField = 'DocumentName' AND @OrderByDirection = 'DESC' THEN RFD.DocumentName END DESC,
		CASE WHEN @OrderByField = 'DocumentTypeName' AND @OrderByDirection = 'ASC' THEN DT.DocumentTypeName END ASC,
		CASE WHEN @OrderByField = 'DocumentTypeName' AND @OrderByDirection = 'DESC' THEN DT.DocumentTypeName END DESC,
		RFD.ReviewFormDocumentID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDocumentDataBySearchCriteria

--Begin procedure reviewform.SaveReviewForm
EXEC utility.DropObject 'reviewform.SaveReviewForm'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewForm

@AssignedPersonID INT = 0,
@BasePrice NUMERIC(18,2) = 0,
@BundleTypeID INT = 0,
@COACSID INT = 0,
@CreatePersonID INT = 0,
@Description VARCHAR(MAX) = NULL,
@EventCode VARCHAR(50) = 'Update',
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@IsActive BIT = 1,
@NAICSID INT = 0,
@OtherResearch VARCHAR(500) = NULL,
@PeriodOfPerformanceEndDate DATE = NULL,
@PeriodOfPerformanceStartDate DATE = NULL,
@PriorBundlerID INT = 0,
@ProcurementHistoryContractAwardAmount NUMERIC(18,2) = 0,
@ProcurementHistoryContractAwardDate DATE = NULL,
@ProcurementHistoryContractEndDate DATE = NULL,
@ProcurementHistoryContractNumber VARCHAR(50) = 0,
@ProcurementHistoryContractTerminationID INT = 0,
@ProcurementHistoryContractTerminationReason VARCHAR(MAX) = NULL,
@ProcurementHistoryID INT = 0,
@RequisitionNumber1 VARCHAR(100) = NULL,
@RequisitionNumber2 VARCHAR(100) = NULL,
@ReviewFormID INT = 0,
@Size VARCHAR(50) = NULL,
@SizeTypeID INT = 0,
@SubContractingPlanID INT = 0,
@SynopsisExceptionReasonID INT = 0,	
@SynopsisID INT = 0,
@WorkflowStepNumber INT = 0,
@CurrentTab INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ReviewFormID = 0
		BEGIN

		DECLARE @tOutput TABLE (ReviewFormID INT)

		INSERT INTO reviewform.ReviewForm
			(
			AssignedPersonID, 
			BasePrice,
			BundleTypeID,
			COACSID,
			ControlCode,
			CreatePersonID,
			CurrentTab,
			Description,
			FundingOrganizationID,
			FundingOrganizationName,
			IsActive,
			NAICSID,
			OriginatingOrganizationID,
			OtherResearch,
			PeriodOfPerformanceEndDate,
			PeriodOfPerformanceStartDate,
			PriorBundlerID,
			ProcurementHistoryContractAwardAmount,
			ProcurementHistoryContractAwardDate,
			ProcurementHistoryContractEndDate,
			ProcurementHistoryContractNumber,
			ProcurementHistoryContractTerminationID,
			ProcurementHistoryContractTerminationReason,
			ProcurementHistoryID,
			RequisitionNumber1,
			RequisitionNumber2,
			Size,
			SizeTypeID,
			StatusID,
			SubContractingPlanID,
			SynopsisExceptionReasonID,
			SynopsisID,
			WorkflowStepNumber
			)
		OUTPUT INSERTED.ReviewFormID INTO @tOutput
		SELECT
			@CreatePersonID,
			@BasePrice,
			@BundleTypeID,
			@COACSID,
			reviewform.CreateControlCode(P.PersonID),
			@CreatePersonID,
			@CurrentTab,
			@Description,
			@FundingOrganizationID,
			@FundingOrganizationName,
			@IsActive,
			@NAICSID,
			P.OrganizationID,
			@OtherResearch,
			@PeriodOfPerformanceEndDate,
			@PeriodOfPerformanceStartDate,
			@PriorBundlerID,
			@ProcurementHistoryContractAwardAmount,
			@ProcurementHistoryContractAwardDate,
			@ProcurementHistoryContractEndDate,
			@ProcurementHistoryContractNumber,
			@ProcurementHistoryContractTerminationID,
			@ProcurementHistoryContractTerminationReason,
			@ProcurementHistoryID,
			@RequisitionNumber1,
			@RequisitionNumber2,
			@Size,
			@SizeTypeID,
			ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0),
			@SubContractingPlanID,
			@SynopsisExceptionReasonID,
			@SynopsisID,
			person.GetOriginatingWorkflowStepNumberByPersonID(@CreatePersonID)
		FROM person.Person P
		WHERE P.PersonID = @CreatePersonID
		
		SELECT @ReviewFormID = O.ReviewFormID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		UPDATE RF
		SET
			RF.AssignedPersonID = @AssignedPersonID,
			RF.BasePrice = @BasePrice,
			RF.BundleTypeID = @BundleTypeID,
			RF.COACSID = @COACSID,
			RF.CurrentTab = @CurrentTab,
			RF.Description = @Description,
			RF.FundingOrganizationID = @FundingOrganizationID,
			RF.FundingOrganizationName = @FundingOrganizationName,
			RF.IsActive = @IsActive,
			RF.NAICSID = @NAICSID,
			RF.OtherResearch = @OtherResearch,
			RF.PeriodOfPerformanceEndDate = @PeriodOfPerformanceEndDate,
			RF.PeriodOfPerformanceStartDate = @PeriodOfPerformanceStartDate,
			RF.PriorBundlerID = @PriorBundlerID,
			RF.ProcurementHistoryContractAwardAmount = @ProcurementHistoryContractAwardAmount,
			RF.ProcurementHistoryContractAwardDate = @ProcurementHistoryContractAwardDate,
			RF.ProcurementHistoryContractEndDate = @ProcurementHistoryContractEndDate,
			RF.ProcurementHistoryContractNumber = @ProcurementHistoryContractNumber,
			RF.ProcurementHistoryContractTerminationID = @ProcurementHistoryContractTerminationID,
			RF.ProcurementHistoryContractTerminationReason = @ProcurementHistoryContractTerminationReason,
			RF.ProcurementHistoryID = @ProcurementHistoryID,
			RF.RequisitionNumber1 = @RequisitionNumber1,
			RF.RequisitionNumber2 = @RequisitionNumber2,
			RF.Size = @Size,
			RF.SizeTypeID = @SizeTypeID,
			RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
			RF.SubContractingPlanID = @SubContractingPlanID,
			RF.SynopsisExceptionReasonID = @SynopsisExceptionReasonID,
			RF.SynopsisID = @SynopsisID,
			RF.WorkflowStepNumber = @WorkflowStepNumber
		FROM reviewform.ReviewForm RF
		WHERE RF.ReviewFormID = @ReviewFormID

		DELETE T FROM reviewform.ReviewFormContract T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormOptionYear T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormPointOfContact T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementHistory T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementMethod T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormResearchType T WHERE T.ReviewFormID = @ReviewFormID
		
		END
	--ENDIF

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure reviewform.SaveReviewForm

--Begin procedure reviewform.SaveReviewFormDocument
EXEC utility.DropObject 'reviewform.SaveReviewFormDocument'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormDocument

@CreatePersonID INT,
@DocumentName VARCHAR(250),
@DocumentPath VARCHAR(50),
@DocumentTypeID INT,
@FileGUID VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cComments VARCHAR(MAX) = 'Added file ' + @DocumentName

	INSERT INTO reviewform.ReviewFormDocument
		(
		CreatePersonID,
		DocumentName,
		DocumentPath,
		DocumentTypeID,
		FileExtension,
		FileGUID,
		ReviewFormID
		)
	VALUES
		(
		@CreatePersonID,
		@DocumentName,
		@DocumentPath,
		@DocumentTypeID,
		core.GetFileExtension(@DocumentName),
		@FileGUID,
		@ReviewFormID
		)

	EXEC eventlog.LogReviewFormAction 
		@Action = 'Add File', 
		@Comments = @cComments,
		@EntityID = @ReviewFormID,
		@EventCode = 'Update',
		@PersonID = @CreatePersonID

END
GO
--End procedure reviewform.SaveReviewFormDocument