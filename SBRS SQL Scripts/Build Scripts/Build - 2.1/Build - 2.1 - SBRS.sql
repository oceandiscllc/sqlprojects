-- File Name:	Build - 2.1 - SBRS.sql
-- Build Key:	Build - 2.1 - 2017.09.07 17.17.03

USE SBRS_DHHS
GO

-- ==============================================================================================================================
-- Functions:
--		core.CreateNumericRange
--		core.GetFileExtension
--		metrics.GetCompletedOpDivDataByFiscalYear
--		metrics.GetCompletedProcurementMethodDataByFiscalYear
--		metrics.GetCreateMonthDataByFiscalYear
--		metrics.GetOpenOpDivDataByFiscalYear
--		metrics.GetOpenProcurementMethodDataByFiscalYear
--		metrics.GetWorkflowStepAgeDataByFiscalYear
--		metrics.GetWorkflowStepCompletionDataByFiscalYear
--
-- Procedures:
--		core.DeleteAnnouncementByAnnouncementID
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		core.GetAnnouncementsByDate
--		core.SaveAnnouncement
--		dropdown.GetFiscalYearData
--		metrics.GetChartMetaData
--		metrics.GetCompletedOpDivCountsByFiscalYear
--		metrics.GetCompletedProcurementMethodCountsByFiscalYear
--		metrics.GetCreatedMonthCountsByFiscalYear
--		metrics.GetMetricsMetaData
--		metrics.GetOpenOpDivCountsByFiscalYear
--		metrics.GetOpenProcurementMethodCountsByFiscalYear
--		metrics.GetWorkflowStepAgeCountsByFiscalYear
--		metrics.GetWorkflowStepCompletionCountsByFiscalYear
--		reviewform.DeleteReviewFormDocumentByeviewFormDocumentID
--		reviewform.GetReviewFormDataByMetricsSearchCriteria
--		reviewform.GetReviewFormDocumentDataBySearchCriteria
--		reviewform.SaveReviewForm
--		reviewform.SaveReviewFormDocument
--
-- Schemas:
--		core
--		metrics
--
-- Tables:
--		core.Announcement
--		metrics.ChartItemMetadata
--		metrics.ChartMetadata
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE SBRS_DHHS
GO

EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'metrics'
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE SBRS_DHHS
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table dropdown.ProcurementMethod
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementMethod'

EXEC utility.AddColumn @TableName, 'ProcurementMethodShortName', 'VARCHAR(50)'
GO
--End table dropdown.ProcurementMethod

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropIndex @TableName, 'IX_EventLogAction'

EXEC utility.SetIndexNonClustered @TableName, 'IX_EventLogAction', 'EntityTypeCode, EntityID, Action', 'CreateDateTime'
GO
--End table eventlog.EventLog

--Begin table metrics.ChartMetadata
DECLARE @TableName VARCHAR(250) = 'metrics.ChartMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE metrics.ChartMetadata
	(
	ChartMetadataID INT IDENTITY(1,1) NOT NULL,
	ChartCode VARCHAR(50),
	ChartTitle VARCHAR(250),
	ChartType VARCHAR(50),
	HAxisTitle VARCHAR(250),
	Orientation VARCHAR(50),
	VAxisTitle VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ChartMetadataID'
EXEC utility.SetIndexClustered @TableName, 'IX_ChartMetadata', 'ChartCode'
GO
--End table metrics.ChartMetadata

--Begin table metrics.ChartItemMetadata
DECLARE @TableName VARCHAR(250) = 'metrics.ChartItemMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE metrics.ChartItemMetadata
	(
	ChartItemMetadataID INT IDENTITY(1,1) NOT NULL,
	ChartCode VARCHAR(50),
	ItemCode INT,
	ItemLegend VARCHAR(10),
	ItemTitle VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ChartItemMetadataID'
EXEC utility.SetIndexClustered @TableName, 'IX_ChartMetadata', 'ChartCode, ItemCode'
GO
--End table metrics.ChartItemMetadata

--Begin table reviewform.ReviewForm
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewForm'

EXEC utility.DropIndex @TableName, 'IX_ReviewForm'

EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewForm', 'IsActive, ReviewFormID, ControlCode', 'StatusID'
GO
--End table reviewform.ReviewForm
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE SBRS_DHHS
GO

--Begin function core.CreateNumericRange
EXEC utility.DropObject 'core.CreateNumericRange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2017.04.05
-- Description:	A function to return a table of numbers in a range
-- ===============================================================

CREATE FUNCTION core.CreateNumericRange
(
@Start INT,
@Stop INT
)

RETURNS @tTable TABLE (Number INT NOT NULL PRIMARY KEY)

AS
BEGIN

	WITH NR AS
		(
		SELECT @Start AS Number

		UNION ALL

		SELECT Number + 1
		FROM NR
		WHERE Number < @Stop
		)

	INSERT INTO @tTable (Number) SELECT Number FROM NR

	RETURN

END
GO
--End function core.CreateNumericRange

--Begin function core.GetFileExtension
EXEC utility.DropObject 'core.GetFileExtension'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.07
-- Description:	A function to extrat a file extension from a file name
-- ===================================================================

CREATE FUNCTION core.GetFileExtension
(
@FileName VARCHAR(MAX)
)

RETURNS VARCHAR(10)

AS
BEGIN

	DECLARE @cReturn VARCHAR(10)

	IF CHARINDEX('.', @FileName) > 0
		SET @cReturn = REVERSE(LEFT(REVERSE(@FileName), CHARINDEX('.', REVERSE(@FileName)) - 1))
	--ENDIF

	RETURN @cReturn

END
GO
--End function core.GetFileExtension

--Begin function metrics.GetCompletedOpDivDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedOpDivDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and OriginatingOrganizationID data
-- ============================================================================================

CREATE FUNCTION metrics.GetCompletedOpDivDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		RF.OriginatingOrganizationID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode = 'Complete'
			AND RF.IsActive = 1

	RETURN

END
GO
--End function metrics.GetCompletedOpDivDataByFiscalYear

--Begin function metrics.GetCompletedProcurementMethodDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedProcurementMethodDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and ProcurementMethodID data
-- ======================================================================================

CREATE FUNCTION metrics.GetCompletedProcurementMethodDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RFPM.ReviewFormID,
		RFPM.ProcurementMethodID
	FROM reviewform.ReviewFormProcurementMethod RFPM
	WHERE EXISTS
		(
		SELECT 1
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND RF.ReviewFormID = RFPM.ReviewFormID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode = 'Complete'
				AND RF.IsActive = 1
		)

	RETURN

END
GO
--End function metrics.GetCompletedProcurementMethodDataByFiscalYear

--Begin function metrics.GetCreateMonthDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCreateMonthDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.05
-- Description:	A function to return a table of ReviewFormID and CreateMonth data
-- ==============================================================================

CREATE FUNCTION metrics.GetCreateMonthDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		MONTH(RF.CreateDateTime)
	FROM reviewform.ReviewForm RF
	WHERE CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear

	RETURN

END
GO
--End function metrics.GetCreateMonthDataByFiscalYear

--Begin function metrics.GetOpenOpDivDataByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenOpDivDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and OriginatingOrganizationID data
-- ============================================================================================

CREATE FUNCTION metrics.GetOpenOpDivDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		RF.OriginatingOrganizationID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode NOT IN ('Complete','Withdrawn')
			AND RF.IsActive = 1

	RETURN

END
GO
--End function metrics.GetOpenOpDivDataByFiscalYear

--Begin function metrics.GetOpenProcurementMethodDataByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenProcurementMethodDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and ProcurementMethodID data
-- ======================================================================================

CREATE FUNCTION metrics.GetOpenProcurementMethodDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RFPM.ReviewFormID,
		RFPM.ProcurementMethodID
	FROM reviewform.ReviewFormProcurementMethod RFPM
	WHERE EXISTS
		(
		SELECT 1
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND RF.ReviewFormID = RFPM.ReviewFormID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
		)

	RETURN

END
GO
--End function metrics.GetOpenProcurementMethodDataByFiscalYear

--Begin function metrics.GetWorkflowStepAgeDataByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepAgeDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and AgeGroup data
-- ===========================================================================

CREATE FUNCTION metrics.GetWorkflowStepAgeDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @cItemLegend VARCHAR(10)
	DECLARE @nItemCode INT

	SELECT TOP 1
		@cItemLegend = CIMD.ItemLegend,
		@nItemCode = CIMD.ItemCode
	FROM metrics.ChartItemMetadata CIMD
	WHERE CIMD.ChartCode = 'WorkflowAgeing'
	ORDER BY CIMD.ItemCode DESC

	INSERT INTO @tTable
		(ReviewFormID, ItemCode, ItemName)
	SELECT 
		RF.ReviewFormID,
		@nItemCode,
		@cItemLegend
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode NOT IN ('Complete','Withdrawn')
			AND RF.IsActive = 1

	UPDATE T
	SET 
		T.ItemCode = E.ItemCode,
		T.ItemName = CIMD.ItemLegend
	FROM @tTable T
		JOIN 
			(
			SELECT
				D.EntityID,

				CASE
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 7
					THEN 1
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 14
					THEN 2
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 30
					THEN 3
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 60
					THEN 4
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 90
					THEN 5
					ELSE 6
				END AS ItemCode

			FROM
				(
				SELECT 
					ROW_NUMBER() OVER (PARTITION BY EL.EntityTypeCode, EL.EntityID ORDER BY EL.CreateDateTime DESC) AS RowIndex,
					EL.EntityID,
					EL.CreateDateTime
				FROM eventlog.EventLog EL
				WHERE EL.EntityTypeCode = 'ReviewForm'
					AND EL.Action NOT IN ('Comment','Update')
					AND core.NullIfEmpty(EL.Action) IS NOT NULL
					AND EXISTS
						(
						SELECT 1
						FROM reviewform.ReviewForm RF
							JOIN dropdown.Status S ON S.StatusID = RF.StatusID
							AND RF.ReviewFormID = EL.EntityID
							AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
							AND S.StatusCode NOT IN ('Complete','Withdrawn')
							AND RF.IsActive = 1
						)
				) D
			WHERE D.RowIndex = 1
			) E ON E.EntityID = T.ReviewFormID
		JOIN metrics.ChartItemMetadata CIMD ON CIMD.ChartCode = 'WorkflowAgeing'
			AND CIMD.ItemCode = E.ItemCode

	RETURN

END
GO
--End function metrics.GetWorkflowStepAgeDataByFiscalYear

--Begin function metrics.GetWorkflowStepCompletionDataByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepCompletionDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and CompletionGroup data
-- ==================================================================================

CREATE FUNCTION metrics.GetWorkflowStepCompletionDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode, ItemName)
	SELECT
		D.ReviewFormID,
		D.CompletionGroup,
		CIMD.ItemLegend
	FROM
		(
		SELECT
			RF.ReviewFormID,
			CASE 
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 25
				THEN 1
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 50
				THEN 2
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 75
				THEN 3
				ELSE 4
			END AS CompletionGroup
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
		) D	
		JOIN metrics.ChartItemMetadata CIMD ON CIMD.ChartCode = 'WorkflowPercentComplete'
			AND CIMD.ItemCode = D.CompletionGroup

	RETURN

END
GO
--End function metrics.GetWorkflowStepCompletionDataByFiscalYear

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE SBRS_DHHS
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteByAnnouncementID'
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime),
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonName,
		core.FormatDate(A.StartDate) AS StartDate,
		core.FormatDate(A.EndDate) AS EndDate
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTime,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonName,
		core.FormatDate(A.StartDate) AS StartDate,
		core.FormatDate(A.EndDate) AS EndDate
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.SaveAnnouncement
EXEC Utility.DropObject 'core.SaveAnnouncement'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.SaveAnnouncement

@AnnouncementID INT = 0,
@AnnouncementText VARCHAR(MAX), 
@CreatePersonID INT = 0,
@EndDate DATE, 
@StartDate DATE

AS
BEGIN
	SET NOCOUNT ON;

	IF @AnnouncementID = 0
		BEGIN

		INSERT INTO core.Announcement
			(StartDate, EndDate, AnnouncementText, CreatePersonID)
		VALUES
			(
			@StartDate, 
			@EndDate, 
			@AnnouncementText, 
			@CreatePersonID
			)

		END
	ELSE
		BEGIN

		UPDATE A
		SET 
			A.AnnouncementText = @AnnouncementText, 
			A.EndDate = @EndDate, 
			A.StartDate = @StartDate
		FROM core.Announcement A
		WHERE A.AnnouncementID = @AnnouncementID

		END
	--ENDIF

END
GO
--End procedure core.SaveAnnouncement

--Begin procedure dropdown.GetFiscalYearData
EXEC Utility.DropObject 'dropdown.GetFiscalYearData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.10
-- Description:	A stored procedure to get a distinct list of fiscal years from the reviewform.ReviewForm table
-- ===========================================================================================================
CREATE PROCEDURE dropdown.GetFiscalYearData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT core.GetFiscalYearFromDateTime(RF.CreateDateTime) AS Year
	FROM reviewform.ReviewForm RF
	ORDER BY 1 DESC
	
END
GO
--End procedure dropdown.GetFiscalYearData

--Begin procedure metrics.GetChartMetaData
EXEC Utility.DropObject 'metrics.GetChartMetaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get the metrics chart metadata
-- =================================================================
CREATE PROCEDURE metrics.GetChartMetaData

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	SELECT 
		CMD.ChartCode, 
		'FY ' + CAST(@FiscalYear AS CHAR(4)) + ' ' + CMD.ChartTitle AS ChartTitle, 
		CMD.ChartType, 
		CMD.HAxisTitle, 
		CMD.Orientation, 
		CMD.VAxisTitle
	FROM metrics.ChartMetaData CMD
	ORDER BY 1
	
END
GO
--End procedure metrics.GetChartMetaData

--Begin procedure metrics.GetCompletedOpDivCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedOpDivCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCompletedOpDivCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (OrganizationID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (OrganizationID) SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationID > 0 AND O.IsActive = 1 AND O.IsForFundingOffice = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CODD.ItemCode) AS ItemCount,
				CODD.ItemCode
			FROM metrics.GetCompletedOpDivDataByFiscalYear(@FiscalYear) CODD
			GROUP BY CODD.ItemCode
			) D ON D.ItemCode = T.OrganizationID

	SELECT
		T.ItemCount,
		O.OrganizationID AS ItemCode,
		O.OrganizationDisplayCode AS ItemName
	FROM @tTable T
		JOIN dropdown.Organization O ON O.OrganizationID = T.OrganizationID
	ORDER BY 3

END
GO
--End procedure metrics.GetCompletedOpDivCountsByFiscalYear

--Begin procedure metrics.GetCompletedProcurementMethodCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedProcurementMethodCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCompletedProcurementMethodCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ProcurementMethodID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ProcurementMethodID) SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID > 0 AND PM.IsActive = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CPMD.ItemCode) AS ItemCount,
				CPMD.ItemCode
			FROM metrics.GetCompletedProcurementMethodDataByFiscalYear(@FiscalYear) CPMD
			GROUP BY CPMD.ItemCode
			) D ON D.ItemCode = T.ProcurementMethodID

	SELECT
		T.ItemCount,
		PM.ProcurementMethodID AS ItemCode,
		PM.ProcurementMethodShortName AS ItemName
	FROM @tTable T
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = T.ProcurementMethodID
	ORDER BY 3

END
GO
--End procedure metrics.GetCompletedProcurementMethodCountsByFiscalYear

--Begin procedure metrics.GetCreatedMonthCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetCreatedMonthCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetCreatedMonthCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ItemCode INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ItemCode) SELECT NR.Number FROM core.CreateNumericRange(1, 12) NR

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(CMD.ReviewFormID) AS ItemCount,
				CMD.ItemCode
			FROM metrics.GetCreateMonthDataByFiscalYear(@FiscalYear) CMD
			GROUP BY CMD.ItemCode
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount,
		T.ItemCode,
		LEFT(DATENAME(month, DATEADD(month, T.ItemCode, -1)), 3) AS ItemName
	FROM @tTable T
	ORDER BY 
		CASE
			WHEN T.ItemCode > 9
			THEN T.ItemCode - 9
			ELSE T.ItemCode + 3
		END


END
GO
--End procedure metrics.GetCreatedMonthCountsByFiscalYear

--Begin procedure metrics.GetMetricsMetaData
EXEC utility.DropObject 'metrics.GetMetricsMetaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetMetricsMetaData

@ChartCode VARCHAR(50) = NULL,
@ItemCode INT = 0,
@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	SELECT 
		'FY ' + CAST(@FiscalYear AS CHAR(4)) + ' ' + CMD.ChartTitle + ' - ' + 
			CASE
				WHEN @ChartCode IN ('CompletedByOperatingDivision', 'OpenByOperatingDivision')
				THEN (SELECT O.OrganizationDisplayCode FROM dropdown.Organization O WHERE O.OrganizationID = @ItemCode)
				WHEN @ChartCode = 'CreatedByMonth'
				THEN LEFT(DATENAME(month, DATEADD(month, @ItemCode, -1)), 3)
				WHEN @ChartCode IN ('CompletedByProcurementMethod', 'OpenByProcurementMethod')
				THEN (SELECT PM.ProcurementMethodShortName FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID = @ItemCode)
				WHEN @ChartCode IN ('WorkflowAgeing', 'WorkflowPercentComplete')
				THEN (SELECT CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = CMD.ChartCode AND CIMD.ItemCode = @ItemCode)
			END AS MetricsDataTitle
	FROM metrics.ChartMetaData CMD
	WHERE CMD.ChartCode = @ChartCode

END
GO
--End procedure metrics.GetMetricsMetaData

--Begin procedure metrics.GetOpenOpDivCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenOpDivCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetOpenOpDivCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (OrganizationID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (OrganizationID) SELECT O.OrganizationID FROM dropdown.Organization O WHERE O.OrganizationID > 0 AND O.IsActive = 1 AND O.IsForFundingOffice = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(OODD.ItemCode) AS ItemCount,
				OODD.ItemCode
			FROM metrics.GetOpenOpDivDataByFiscalYear(@FiscalYear) OODD
			GROUP BY OODD.ItemCode
			) D ON D.ItemCode = T.OrganizationID

	SELECT
		T.ItemCount,
		O.OrganizationID AS ItemCode,
		O.OrganizationDisplayCode AS ItemName
	FROM @tTable T
		JOIN dropdown.Organization O ON O.OrganizationID = T.OrganizationID
	ORDER BY 3

END
GO
--End procedure metrics.GetOpenOpDivCountsByFiscalYear

--Begin procedure metrics.GetOpenProcurementMethodCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenProcurementMethodCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetOpenProcurementMethodCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @tTable TABLE (ProcurementMethodID INT NOT NULL DEFAULT 0 PRIMARY KEY, ItemCount INT NOT NULL DEFAULT 0)

	INSERT INTO @tTable (ProcurementMethodID) SELECT PM.ProcurementMethodID FROM dropdown.ProcurementMethod PM WHERE PM.ProcurementMethodID > 0 AND PM.IsActive = 1

	UPDATE T
	SET T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(OPMD.ItemCode) AS ItemCount,
				OPMD.ItemCode
			FROM metrics.GetOpenProcurementMethodDataByFiscalYear(@FiscalYear) OPMD
			GROUP BY OPMD.ItemCode
			) D ON D.ItemCode = T.ProcurementMethodID

	SELECT
		T.ItemCount,
		PM.ProcurementMethodID AS ItemCode,
		PM.ProcurementMethodShortName AS ItemName
	FROM @tTable T
		JOIN dropdown.ProcurementMethod PM ON PM.ProcurementMethodID = T.ProcurementMethodID
	ORDER BY 3

END
GO
--End procedure metrics.GetOpenProcurementMethodCountsByFiscalYear

--Begin procedure metrics.GetWorkflowStepAgeCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepAgeCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetWorkflowStepAgeCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ItemCount INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))

	INSERT INTO @tTable (ItemCode, ItemName) SELECT CIMD.ItemCode, CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = 'WorkflowAgeing'

	UPDATE T
	SET 
		T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(WSAD.ItemCode) AS ItemCount,
				WSAD.ItemCode
			FROM metrics.GetWorkflowStepAgeDataByFiscalYear(@FiscalYear) WSAD
			GROUP BY WSAD.ItemCode
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount, 
		T.ItemCode, 
		T.ItemName
	FROM @tTable T
	ORDER BY T.ItemCode

END
GO
--End procedure metrics.GetWorkflowStepAgeCountsByFiscalYear

--Begin procedure metrics.GetWorkflowStepCompletionCountsByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepCompletionCountsByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.04
-- Description:	A stored procedure to get metrics data
-- ===================================================
CREATE PROCEDURE metrics.GetWorkflowStepCompletionCountsByFiscalYear

@FiscalYear INT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (ItemCount INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))

	INSERT INTO @tTable (ItemCode, ItemName) SELECT CIMD.ItemCode, CIMD.ItemLegend FROM metrics.ChartItemMetadata CIMD WHERE CIMD.ChartCode = 'WorkflowPercentComplete'

	UPDATE T
	SET 
		T.ItemCount = D.ItemCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				COUNT(WSCD.ItemCode) AS ItemCount,
				WSCD.ItemCode
			FROM metrics.GetWorkflowStepCompletionDataByFiscalYear(@FiscalYear) WSCD
			GROUP BY WSCD.ItemCode, WSCD.ItemName
			) D ON D.ItemCode = T.ItemCode

	SELECT
		T.ItemCount, 
		T.ItemCode, 
		T.ItemName
	FROM @tTable T
	ORDER BY T.ItemCode

END
GO
--End procedure metrics.GetWorkflowStepCompletionCountsByFiscalYear

--Begin procedure reviewform.DeleteReviewFormDocumentByeviewFormDocumentID
EXEC Utility.DropObject 'reviewform.DeleteReviewFormDocumentByeviewFormDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.07
-- Description:	A stored procedure to delete data from the reviewform.ReviewFormDocument table
-- ===========================================================================================
CREATE PROCEDURE reviewform.DeleteReviewFormDocumentByeviewFormDocumentID

@ReviewFormDocumentID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cComments VARCHAR(MAX)
	DECLARE @cDocumentName VARCHAR(250)
	DECLARE @nReviewFormID INT
	
	SELECT 
		@cDocumentName = RFD.DocumentName,
		@nReviewFormID = RFD.ReviewFormID
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

	DELETE RFD
	FROM reviewform.ReviewFormDocument RFD
	WHERE RFD.ReviewFormDocumentID = @ReviewFormDocumentID

	SET @cComments = 'Deleted file ' + @cDocumentName

	EXEC eventlog.LogReviewFormAction 
		@Action = 'Delete File', 
		@Comments = @cComments,
		@EntityID = @nReviewFormID,
		@EventCode = 'Update',
		@PersonID = @PersonID

END
GO

--End procedure reviewform.DeleteReviewFormDocumentByeviewFormDocumentID

--Begin procedure reviewform.GetReviewFormDataByMetricsSearchCriteria
EXEC Utility.DropObject 'reviewform.GetReviewFormDataByMetricsSearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.03.01
-- Description:	A stored procedure to get data from the reviewform.ReviewForm table based on search criteria
-- =========================================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDataByMetricsSearchCriteria

@ChartCode VARCHAR(50) = NULL,
@FiscalYear INT = 0,
@ItemCode INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RF.ReviewFormID
		FROM reviewform.ReviewForm RF
		WHERE 1 = 1
			AND (@ChartCode <> 'CompletedByOperatingDivision' OR EXISTS (SELECT 1 FROM metrics.GetCompletedOpDivDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'CompletedByProcurementMethod' OR EXISTS (SELECT 1 FROM metrics.GetCompletedProcurementMethodDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'CreatedByMonth' OR EXISTS (SELECT 1 FROM metrics.GetCreateMonthDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'OpenByOperatingDivision' OR EXISTS (SELECT 1 FROM metrics.GetOpenOpDivDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'OpenByProcurementMethod' OR EXISTS (SELECT 1 FROM metrics.GetOpenProcurementMethodDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'WorkflowAgeing' OR EXISTS (SELECT 1 FROM metrics.GetWorkflowStepAgeDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
			AND (@ChartCode <> 'WorkflowPercentComplete' OR EXISTS (SELECT 1 FROM metrics.GetWorkflowStepCompletionDataByFiscalYear(@FiscalYear) D WHERE D.ItemCode = @ItemCode AND D.ReviewFormID = RF.ReviewFormID))
		)

	SELECT
		person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') AS AssignedPersonNameFormatted,
		RF.ControlCode,
		core.FormatDate(RF.CreateDateTime) AS CreateDate,
		person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,

		CASE
			WHEN RF.FundingOrganizationName IS NOT NULL
			THEN RF.FundingOrganizationName
			ELSE O1.OrganizationDisplayCode
		END AS FundingOrganizationDisplayCode,

		O2.OrganizationDisplayCode AS OriginatingOrganizationDisplayCode,
		S.StatusName,
		workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) AS WorkflowStepName,
		RF.ReviewFormID, --This column is not displayed in the data table, it is for the javascript
		N.NAICSCode + CASE WHEN RF.NAICSID = 0  THEN '' ELSE ' - ' + N.NAICSName END AS NAICSCodeFormatted, --This column is not displayed in the data table, it is for the excel export
		RF.Description, --This column is not displayed in the data table, it is for the excel export
		(SELECT COUNT(RT.ReviewFormID) FROM reviewform.ReviewForm RT WHERE IsActive = 1) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewForm RF ON RF.ReviewFormID = PD.ReviewFormID
		JOIN dropdown.NAICS N ON N.NAICSID = RF.NAICSID
		JOIN dropdown.Organization O1 ON O1.OrganizationID = RF.FundingOrganizationID
		JOIN dropdown.Organization O2 ON O2.OrganizationID = RF.OriginatingOrganizationID
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
	ORDER BY
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'AssignedPersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.AssignedPersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'ASC' THEN RF.ControlCode END ASC,
		CASE WHEN @OrderByField = 'ControlCode' AND @OrderByDirection = 'DESC' THEN RF.ControlCode END DESC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'ASC' THEN RF.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDate' AND @OrderByDirection = 'DESC' THEN RF.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RF.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END ASC,
		CASE WHEN @OrderByField = 'FundingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN CASE WHEN RF.FundingOrganizationName IS NOT NULL THEN RF.FundingOrganizationName ELSE O1.OrganizationDisplayCode END END DESC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'ASC' THEN O2.OrganizationDisplayCode END ASC,
		CASE WHEN @OrderByField = 'OriginatingOrganizationDisplayCode' AND @OrderByDirection = 'DESC' THEN O2.OrganizationDisplayCode END DESC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'ASC' THEN S.StatusName END ASC,
		CASE WHEN @OrderByField = 'StatusName' AND @OrderByDirection = 'DESC' THEN S.StatusName END DESC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'ASC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END ASC,
		CASE WHEN @OrderByField = 'WorkflowStepName' AND @OrderByDirection = 'DESC' THEN workflow.GetWorkflowStepNameByReviewFormID(RF.ReviewFormID) END DESC,
		RF.ReviewFormID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDataByMetricsSearchCriteria
	
--Begin procedure reviewform.GetReviewFormDocumentDataBySearchCriteria
--EXEC utility.DropObject 'reviewform.GetReviewFormDocumentByReviewFormID'
EXEC utility.DropObject 'reviewform.GetReviewFormDocumentDataBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.07
-- Description:	A stored procedure to get data from the reviewform.ReviewFormDocument table
-- ========================================================================================
CREATE PROCEDURE reviewform.GetReviewFormDocumentDataBySearchCriteria

@ReviewFormID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	DECLARE @nOffSet INT = (@PageIndex * @PageSize) - @PageSize

	;
	WITH PD AS
		(
		SELECT
			RFD.ReviewFormDocumentID
		FROM reviewform.ReviewFormDocument RFD
		WHERE RFD.ReviewFormID = @ReviewFormID
			AND RFD.IsActive = 1
		)

	SELECT 
		person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDateTime(RFD.CreateDateTime) AS CreateDateTimeFormatted,
		'<img src="/assets/images/file-icons/' 
			+ ISNULL(core.GetFileExtension(RFD.DocumentName), 'txt') 
			+ '.png" class="fileIcon pull-left">&nbsp;<a href="javascript:deleteDocument(' 
			+ CAST(RFD.ReviewFormDocumentID AS VARCHAR(10)) 
			+ ');" class="btn btn-xs btn-danger pull-right"><i class="hhsicon icon-x"></i></a>&nbsp;'
			+ '<a href="/Review/GetFile/' 
			+ CAST(RFD.ReviewFormDocumentID AS VARCHAR(10)) 
			+ '" title="' 
			+ RFD.DocumentName 
			+ '">' 
			+ RFD.DocumentName 
			+ '</a>' AS DocumentName,
		RFD.ReviewFormDocumentID,
		DT.DocumentTypeName,
		(SELECT COUNT(PD.ReviewFormDocumentID) FROM PD) AS RecordsTotal,
		(SELECT COUNT(PD.ReviewFormDocumentID) FROM PD) AS RecordsFiltered
	FROM PD
		JOIN reviewform.ReviewFormDocument RFD ON RFD.ReviewFormDocumentID = PD.ReviewFormDocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = RFD.DocumentTypeID
	ORDER BY
		CASE WHEN @OrderByField = 'CreateDateTimeFormatted' AND @OrderByDirection = 'ASC' THEN RFD.CreateDateTime END ASC,
		CASE WHEN @OrderByField = 'CreateDateTimeFormatted' AND @OrderByDirection = 'DESC' THEN RFD.CreateDateTime END DESC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'ASC' THEN person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'CreatePersonNameFormatted' AND @OrderByDirection = 'DESC' THEN person.FormatPersonNameByPersonID(RFD.CreatePersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'DocumentName' AND @OrderByDirection = 'ASC' THEN RFD.DocumentName END ASC,
		CASE WHEN @OrderByField = 'DocumentName' AND @OrderByDirection = 'DESC' THEN RFD.DocumentName END DESC,
		CASE WHEN @OrderByField = 'DocumentTypeName' AND @OrderByDirection = 'ASC' THEN DT.DocumentTypeName END ASC,
		CASE WHEN @OrderByField = 'DocumentTypeName' AND @OrderByDirection = 'DESC' THEN DT.DocumentTypeName END DESC,
		RFD.ReviewFormDocumentID ASC
	OFFSET CASE WHEN @PageSize > 0 THEN @nOffSet ELSE 0 END ROWS
	FETCH NEXT CASE WHEN @PageSize > 0 THEN @PageSize ELSE 1000000 END ROWS ONLY

END
GO
--End procedure reviewform.GetReviewFormDocumentDataBySearchCriteria

--Begin procedure reviewform.SaveReviewForm
EXEC utility.DropObject 'reviewform.SaveReviewForm'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2017.02.01
-- Description:	A stored procedure to save data to the reviewform.ReviewForm table
-- ===============================================================================
CREATE PROCEDURE reviewform.SaveReviewForm

@AssignedPersonID INT = 0,
@BasePrice NUMERIC(18,2) = 0,
@BundleTypeID INT = 0,
@COACSID INT = 0,
@CreatePersonID INT = 0,
@Description VARCHAR(MAX) = NULL,
@EventCode VARCHAR(50) = 'Update',
@FundingOrganizationID INT = 0,
@FundingOrganizationName VARCHAR(250) = NULL,
@IsActive BIT = 1,
@NAICSID INT = 0,
@OtherResearch VARCHAR(500) = NULL,
@PeriodOfPerformanceEndDate DATE = NULL,
@PeriodOfPerformanceStartDate DATE = NULL,
@PriorBundlerID INT = 0,
@ProcurementHistoryContractAwardAmount NUMERIC(18,2) = 0,
@ProcurementHistoryContractAwardDate DATE = NULL,
@ProcurementHistoryContractEndDate DATE = NULL,
@ProcurementHistoryContractNumber VARCHAR(50) = 0,
@ProcurementHistoryContractTerminationID INT = 0,
@ProcurementHistoryContractTerminationReason VARCHAR(MAX) = NULL,
@ProcurementHistoryID INT = 0,
@RequisitionNumber1 VARCHAR(100) = NULL,
@RequisitionNumber2 VARCHAR(100) = NULL,
@ReviewFormID INT = 0,
@Size VARCHAR(50) = NULL,
@SizeTypeID INT = 0,
@SubContractingPlanID INT = 0,
@SynopsisExceptionReasonID INT = 0,	
@SynopsisID INT = 0,
@WorkflowStepNumber INT = 0,
@CurrentTab INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ReviewFormID = 0
		BEGIN

		DECLARE @tOutput TABLE (ReviewFormID INT)

		INSERT INTO reviewform.ReviewForm
			(
			AssignedPersonID, 
			BasePrice,
			BundleTypeID,
			COACSID,
			ControlCode,
			CreatePersonID,
			CurrentTab,
			Description,
			FundingOrganizationID,
			FundingOrganizationName,
			IsActive,
			NAICSID,
			OriginatingOrganizationID,
			OtherResearch,
			PeriodOfPerformanceEndDate,
			PeriodOfPerformanceStartDate,
			PriorBundlerID,
			ProcurementHistoryContractAwardAmount,
			ProcurementHistoryContractAwardDate,
			ProcurementHistoryContractEndDate,
			ProcurementHistoryContractNumber,
			ProcurementHistoryContractTerminationID,
			ProcurementHistoryContractTerminationReason,
			ProcurementHistoryID,
			RequisitionNumber1,
			RequisitionNumber2,
			Size,
			SizeTypeID,
			StatusID,
			SubContractingPlanID,
			SynopsisExceptionReasonID,
			SynopsisID,
			WorkflowStepNumber
			)
		OUTPUT INSERTED.ReviewFormID INTO @tOutput
		SELECT
			@CreatePersonID,
			@BasePrice,
			@BundleTypeID,
			@COACSID,
			reviewform.CreateControlCode(P.PersonID),
			@CreatePersonID,
			@CurrentTab,
			@Description,
			@FundingOrganizationID,
			@FundingOrganizationName,
			@IsActive,
			@NAICSID,
			P.OrganizationID,
			@OtherResearch,
			@PeriodOfPerformanceEndDate,
			@PeriodOfPerformanceStartDate,
			@PriorBundlerID,
			@ProcurementHistoryContractAwardAmount,
			@ProcurementHistoryContractAwardDate,
			@ProcurementHistoryContractEndDate,
			@ProcurementHistoryContractNumber,
			@ProcurementHistoryContractTerminationID,
			@ProcurementHistoryContractTerminationReason,
			@ProcurementHistoryID,
			@RequisitionNumber1,
			@RequisitionNumber2,
			@Size,
			@SizeTypeID,
			ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'InProgress'), 0),
			@SubContractingPlanID,
			@SynopsisExceptionReasonID,
			@SynopsisID,
			person.GetOriginatingWorkflowStepNumberByPersonID(@CreatePersonID)
		FROM person.Person P
		WHERE P.PersonID = @CreatePersonID
		
		SELECT @ReviewFormID = O.ReviewFormID
		FROM @tOutput O

		END
	ELSE
		BEGIN

		UPDATE RF
		SET
			RF.AssignedPersonID = @AssignedPersonID,
			RF.BasePrice = @BasePrice,
			RF.BundleTypeID = @BundleTypeID,
			RF.COACSID = @COACSID,
			RF.CurrentTab = @CurrentTab,
			RF.Description = @Description,
			RF.FundingOrganizationID = @FundingOrganizationID,
			RF.FundingOrganizationName = @FundingOrganizationName,
			RF.IsActive = @IsActive,
			RF.NAICSID = @NAICSID,
			RF.OtherResearch = @OtherResearch,
			RF.PeriodOfPerformanceEndDate = @PeriodOfPerformanceEndDate,
			RF.PeriodOfPerformanceStartDate = @PeriodOfPerformanceStartDate,
			RF.PriorBundlerID = @PriorBundlerID,
			RF.ProcurementHistoryContractAwardAmount = @ProcurementHistoryContractAwardAmount,
			RF.ProcurementHistoryContractAwardDate = @ProcurementHistoryContractAwardDate,
			RF.ProcurementHistoryContractEndDate = @ProcurementHistoryContractEndDate,
			RF.ProcurementHistoryContractNumber = @ProcurementHistoryContractNumber,
			RF.ProcurementHistoryContractTerminationID = @ProcurementHistoryContractTerminationID,
			RF.ProcurementHistoryContractTerminationReason = @ProcurementHistoryContractTerminationReason,
			RF.ProcurementHistoryID = @ProcurementHistoryID,
			RF.RequisitionNumber1 = @RequisitionNumber1,
			RF.RequisitionNumber2 = @RequisitionNumber2,
			RF.Size = @Size,
			RF.SizeTypeID = @SizeTypeID,
			RF.StatusID = workflow.GetStatusIDByEventCode(@EventCode, @WorkflowStepNumber, RF.ReviewFormID),
			RF.SubContractingPlanID = @SubContractingPlanID,
			RF.SynopsisExceptionReasonID = @SynopsisExceptionReasonID,
			RF.SynopsisID = @SynopsisID,
			RF.WorkflowStepNumber = @WorkflowStepNumber
		FROM reviewform.ReviewForm RF
		WHERE RF.ReviewFormID = @ReviewFormID

		DELETE T FROM reviewform.ReviewFormContract T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormOptionYear T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormPointOfContact T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementHistory T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormProcurementMethod T WHERE T.ReviewFormID = @ReviewFormID
		DELETE T FROM reviewform.ReviewFormResearchType T WHERE T.ReviewFormID = @ReviewFormID
		
		END
	--ENDIF

	SELECT
		O.OrganizationFilePath,
		RF.ControlCode,
		RF.ReviewFormID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Organization O ON O.OrganizationID = RF.OriginatingOrganizationID
			AND RF.ReviewFormID = @ReviewFormID

END
GO
--End procedure reviewform.SaveReviewForm

--Begin procedure reviewform.SaveReviewFormDocument
EXEC utility.DropObject 'reviewform.SaveReviewFormDocument'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:   Todd Pires
-- Create Date: 2017.02.17
-- Description: A stored procedure to save data to the reviewform.ReviewFormPointOfContact table
-- =============================================================================================
CREATE PROCEDURE reviewform.SaveReviewFormDocument

@CreatePersonID INT,
@DocumentName VARCHAR(250),
@DocumentPath VARCHAR(50),
@DocumentTypeID INT,
@FileGUID VARCHAR(50),
@ReviewFormID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cComments VARCHAR(MAX) = 'Added file ' + @DocumentName

	INSERT INTO reviewform.ReviewFormDocument
		(
		CreatePersonID,
		DocumentName,
		DocumentPath,
		DocumentTypeID,
		FileExtension,
		FileGUID,
		ReviewFormID
		)
	VALUES
		(
		@CreatePersonID,
		@DocumentName,
		@DocumentPath,
		@DocumentTypeID,
		core.GetFileExtension(@DocumentName),
		@FileGUID,
		@ReviewFormID
		)

	EXEC eventlog.LogReviewFormAction 
		@Action = 'Add File', 
		@Comments = @cComments,
		@EntityID = @ReviewFormID,
		@EventCode = 'Update',
		@PersonID = @CreatePersonID

END
GO
--End procedure reviewform.SaveReviewFormDocument
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE SBRS_DHHS
GO

--Begin table dropdown.ProcurementMethod
UPDATE PM
SET PM.ProcurementMethodShortName =
	CASE
		WHEN PM.ProcurementMethodCode = 'EDWOSB'
		THEN 'EDWOSB Set-Aside'
		WHEN PM.ProcurementMethodCode = 'GSA'
		THEN 'GSA Schedule'
		WHEN PM.ProcurementMethodCode = 'Indian'
		THEN 'Buy Indian Act'
		WHEN PM.ProcurementMethodCode = 'WOSBC'
		THEN 'WOSB Competitive'
		WHEN PM.ProcurementMethodCode = 'WOSBSS'
		THEN 'WOSB Sole Source'
		ELSE PM.ProcurementMethodName
	END
FROM dropdown.ProcurementMethod PM
GO
--End table dropdown.ProcurementMethod

--Begin table metrics.ChartMetadata
TRUNCATE TABLE metrics.ChartMetadata
GO

INSERT INTO metrics.ChartMetadata
	(ChartCode, ChartTitle, ChartType, HAxisTitle, Orientation, VAxisTitle)
VALUES
	('CompletedByOperatingDivision', 'Completed Reviews By Operating Division', 'ColumnChart', '# of Reviews', 'vertical', 'Operating Division'),
	('CompletedByProcurementMethod', 'Completed Reviews By Procurement Method', 'PieChart', NULL, NULL, NULL),
	('CreatedByMonth', 'Reviews Created By Month', 'ColumnChart', 'Month', NULL, '# of Reviews'),
	('OpenByOperatingDivision', 'Open Reviews By Operating Division', 'ColumnChart', '# of Reviews', 'vertical', 'Operating Division'),
	('OpenByProcurementMethod', 'Open Reviews By Procurement Method', 'PieChart', NULL, NULL, NULL),
	('WorkflowAgeing', 'Workflow Step Aging', 'ColumnChart', 'Days', NULL, '# of Reviews'),
	('WorkflowPercentComplete', 'Workflow % Complete', 'ColumnChart', '% Complete', NULL, '# of Reviews')
GO
--End table metrics.ChartMetadata

--Begin table metrics.ChartItemMetadata
TRUNCATE TABLE metrics.ChartItemMetadata
GO

INSERT INTO metrics.ChartItemMetadata
	(ChartCode, ItemCode, ItemLegend, ItemTitle)
VALUES
	('WorkflowAgeing', 1, '<= 7', 'Reviews that have been in their current workflow step for 7 days or less'),
	('WorkflowAgeing', 2, '<= 14', 'Reviews that have been in their current workflow step between 8 and 14 days'),
	('WorkflowAgeing', 3, '<= 30', 'Reviews that have been in their current workflow step between 15 and 30 days'),
	('WorkflowAgeing', 4, '<= 60', 'Reviews that have been in their current workflow step between 31 and 60 days'),
	('WorkflowAgeing', 5, '<= 90', 'Reviews that have been in their current workflow step between 61 and 90 days'),
	('WorkflowAgeing', 6, '> 90', 'Reviews that have been in their current workflow step more than 90 days'),
	('WorkflowPercentComplete', 1, '<= 25', 'Reviews that are 25% or less through their workflow'),
	('WorkflowPercentComplete', 2, '<= 50', 'Reviews that are 50% or less through their workflow'),
	('WorkflowPercentComplete', 3, '<= 75', 'Reviews that are 75% or less through their workflow'),
	('WorkflowPercentComplete', 4, '> 75', 'Reviews that are more than 75% through their workflow')
GO
--End table metrics.ChartItemMetadata

--End file Build File - 04 - Data.sql

