USE SBRS_DHHS
GO

--Begin function core.CreateNumericRange
EXEC utility.DropObject 'core.CreateNumericRange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2017.04.05
-- Description:	A function to return a table of numbers in a range
-- ===============================================================

CREATE FUNCTION core.CreateNumericRange
(
@Start INT,
@Stop INT
)

RETURNS @tTable TABLE (Number INT NOT NULL PRIMARY KEY)

AS
BEGIN

	WITH NR AS
		(
		SELECT @Start AS Number

		UNION ALL

		SELECT Number + 1
		FROM NR
		WHERE Number < @Stop
		)

	INSERT INTO @tTable (Number) SELECT Number FROM NR

	RETURN

END
GO
--End function core.CreateNumericRange

--Begin function core.GetFileExtension
EXEC utility.DropObject 'core.GetFileExtension'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.07
-- Description:	A function to extrat a file extension from a file name
-- ===================================================================

CREATE FUNCTION core.GetFileExtension
(
@FileName VARCHAR(MAX)
)

RETURNS VARCHAR(10)

AS
BEGIN

	DECLARE @cReturn VARCHAR(10)

	IF CHARINDEX('.', @FileName) > 0
		SET @cReturn = REVERSE(LEFT(REVERSE(@FileName), CHARINDEX('.', REVERSE(@FileName)) - 1))
	--ENDIF

	RETURN @cReturn

END
GO
--End function core.GetFileExtension

--Begin function metrics.GetCompletedOpDivDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedOpDivDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and OriginatingOrganizationID data
-- ============================================================================================

CREATE FUNCTION metrics.GetCompletedOpDivDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		RF.OriginatingOrganizationID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode = 'Complete'
			AND RF.IsActive = 1

	RETURN

END
GO
--End function metrics.GetCompletedOpDivDataByFiscalYear

--Begin function metrics.GetCompletedProcurementMethodDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCompletedProcurementMethodDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and ProcurementMethodID data
-- ======================================================================================

CREATE FUNCTION metrics.GetCompletedProcurementMethodDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RFPM.ReviewFormID,
		RFPM.ProcurementMethodID
	FROM reviewform.ReviewFormProcurementMethod RFPM
	WHERE EXISTS
		(
		SELECT 1
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND RF.ReviewFormID = RFPM.ReviewFormID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode = 'Complete'
				AND RF.IsActive = 1
		)

	RETURN

END
GO
--End function metrics.GetCompletedProcurementMethodDataByFiscalYear

--Begin function metrics.GetCreateMonthDataByFiscalYear
EXEC utility.DropObject 'metrics.GetCreateMonthDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.05
-- Description:	A function to return a table of ReviewFormID and CreateMonth data
-- ==============================================================================

CREATE FUNCTION metrics.GetCreateMonthDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		MONTH(RF.CreateDateTime)
	FROM reviewform.ReviewForm RF
	WHERE CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear

	RETURN

END
GO
--End function metrics.GetCreateMonthDataByFiscalYear

--Begin function metrics.GetOpenOpDivDataByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenOpDivDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and OriginatingOrganizationID data
-- ============================================================================================

CREATE FUNCTION metrics.GetOpenOpDivDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RF.ReviewFormID,
		RF.OriginatingOrganizationID
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode NOT IN ('Complete','Withdrawn')
			AND RF.IsActive = 1

	RETURN

END
GO
--End function metrics.GetOpenOpDivDataByFiscalYear

--Begin function metrics.GetOpenProcurementMethodDataByFiscalYear
EXEC utility.DropObject 'metrics.GetOpenProcurementMethodDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and ProcurementMethodID data
-- ======================================================================================

CREATE FUNCTION metrics.GetOpenProcurementMethodDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL DEFAULT 0, ItemCode INT NOT NULL DEFAULT 0)  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode)
	SELECT
		RFPM.ReviewFormID,
		RFPM.ProcurementMethodID
	FROM reviewform.ReviewFormProcurementMethod RFPM
	WHERE EXISTS
		(
		SELECT 1
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND RF.ReviewFormID = RFPM.ReviewFormID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
		)

	RETURN

END
GO
--End function metrics.GetOpenProcurementMethodDataByFiscalYear

--Begin function metrics.GetWorkflowStepAgeDataByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepAgeDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and AgeGroup data
-- ===========================================================================

CREATE FUNCTION metrics.GetWorkflowStepAgeDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	DECLARE @cItemLegend VARCHAR(10)
	DECLARE @nItemCode INT

	SELECT TOP 1
		@cItemLegend = CIMD.ItemLegend,
		@nItemCode = CIMD.ItemCode
	FROM metrics.ChartItemMetadata CIMD
	WHERE CIMD.ChartCode = 'WorkflowAgeing'
	ORDER BY CIMD.ItemCode DESC

	INSERT INTO @tTable
		(ReviewFormID, ItemCode, ItemName)
	SELECT 
		RF.ReviewFormID,
		@nItemCode,
		@cItemLegend
	FROM reviewform.ReviewForm RF
		JOIN dropdown.Status S ON S.StatusID = RF.StatusID
			AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
			AND S.StatusCode NOT IN ('Complete','Withdrawn')
			AND RF.IsActive = 1

	UPDATE T
	SET 
		T.ItemCode = E.ItemCode,
		T.ItemName = CIMD.ItemLegend
	FROM @tTable T
		JOIN 
			(
			SELECT
				D.EntityID,

				CASE
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 7
					THEN 1
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 14
					THEN 2
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 30
					THEN 3
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 60
					THEN 4
					WHEN DATEDIFF(d, D.CreateDateTime, getDate()) <= 90
					THEN 5
					ELSE 6
				END AS ItemCode

			FROM
				(
				SELECT 
					ROW_NUMBER() OVER (PARTITION BY EL.EntityTypeCode, EL.EntityID ORDER BY EL.CreateDateTime DESC) AS RowIndex,
					EL.EntityID,
					EL.CreateDateTime
				FROM eventlog.EventLog EL
				WHERE EL.EntityTypeCode = 'ReviewForm'
					AND EL.Action NOT IN ('Comment','Update')
					AND core.NullIfEmpty(EL.Action) IS NOT NULL
					AND EXISTS
						(
						SELECT 1
						FROM reviewform.ReviewForm RF
							JOIN dropdown.Status S ON S.StatusID = RF.StatusID
							AND RF.ReviewFormID = EL.EntityID
							AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
							AND S.StatusCode NOT IN ('Complete','Withdrawn')
							AND RF.IsActive = 1
						)
				) D
			WHERE D.RowIndex = 1
			) E ON E.EntityID = T.ReviewFormID
		JOIN metrics.ChartItemMetadata CIMD ON CIMD.ChartCode = 'WorkflowAgeing'
			AND CIMD.ItemCode = E.ItemCode

	RETURN

END
GO
--End function metrics.GetWorkflowStepAgeDataByFiscalYear

--Begin function metrics.GetWorkflowStepCompletionDataByFiscalYear
EXEC utility.DropObject 'metrics.GetWorkflowStepCompletionDataByFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.31
-- Description:	A function to return a table of ReviewFormID and CompletionGroup data
-- ==================================================================================

CREATE FUNCTION metrics.GetWorkflowStepCompletionDataByFiscalYear
(
@FiscalYear INT
)

RETURNS @tTable TABLE (ReviewFormID INT NOT NULL PRIMARY KEY, ItemCode INT NOT NULL DEFAULT 0, ItemName VARCHAR(50))  

AS
BEGIN

	IF @FiscalYear = 0 OR @FiscalYear IS NULL
		SET @FiscalYear = core.GetFiscalYearFromDateTime(getDate())
	--ENDIF

	INSERT INTO @tTable
		(ReviewFormID, ItemCode, ItemName)
	SELECT
		D.ReviewFormID,
		D.CompletionGroup,
		CIMD.ItemLegend
	FROM
		(
		SELECT
			RF.ReviewFormID,
			CASE 
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 25
				THEN 1
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 50
				THEN 2
				WHEN CAST(RF.WorkflowStepNumber AS NUMERIC(18,2)) / CAST(workflow.GetWorkflowStepCountByWorkflowID(RF.WorkflowID) AS NUMERIC(18,2)) * 100 <= 75
				THEN 3
				ELSE 4
			END AS CompletionGroup
		FROM reviewform.ReviewForm RF
			JOIN dropdown.Status S ON S.StatusID = RF.StatusID
				AND CAST(LEFT(RF.ControlCode, 4) AS INT) = @FiscalYear
				AND S.StatusCode NOT IN ('Complete','Withdrawn')
				AND RF.IsActive = 1
		) D	
		JOIN metrics.ChartItemMetadata CIMD ON CIMD.ChartCode = 'WorkflowPercentComplete'
			AND CIMD.ItemCode = D.CompletionGroup

	RETURN

END
GO
--End function metrics.GetWorkflowStepCompletionDataByFiscalYear
