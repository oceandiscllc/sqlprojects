USE SBRS_DHHS
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table dropdown.ProcurementMethod
DECLARE @TableName VARCHAR(250) = 'dropdown.ProcurementMethod'

EXEC utility.AddColumn @TableName, 'ProcurementMethodShortName', 'VARCHAR(50)'
GO
--End table dropdown.ProcurementMethod

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropIndex @TableName, 'IX_EventLogAction'

EXEC utility.SetIndexNonClustered @TableName, 'IX_EventLogAction', 'EntityTypeCode, EntityID, Action', 'CreateDateTime'
GO
--End table eventlog.EventLog

--Begin table metrics.ChartMetadata
DECLARE @TableName VARCHAR(250) = 'metrics.ChartMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE metrics.ChartMetadata
	(
	ChartMetadataID INT IDENTITY(1,1) NOT NULL,
	ChartCode VARCHAR(50),
	ChartTitle VARCHAR(250),
	ChartType VARCHAR(50),
	HAxisTitle VARCHAR(250),
	Orientation VARCHAR(50),
	VAxisTitle VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ChartMetadataID'
EXEC utility.SetIndexClustered @TableName, 'IX_ChartMetadata', 'ChartCode'
GO
--End table metrics.ChartMetadata

--Begin table metrics.ChartItemMetadata
DECLARE @TableName VARCHAR(250) = 'metrics.ChartItemMetadata'

EXEC utility.DropObject @TableName

CREATE TABLE metrics.ChartItemMetadata
	(
	ChartItemMetadataID INT IDENTITY(1,1) NOT NULL,
	ChartCode VARCHAR(50),
	ItemCode INT,
	ItemLegend VARCHAR(10),
	ItemTitle VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ChartItemMetadataID'
EXEC utility.SetIndexClustered @TableName, 'IX_ChartMetadata', 'ChartCode, ItemCode'
GO
--End table metrics.ChartItemMetadata

--Begin table reviewform.ReviewForm
DECLARE @TableName VARCHAR(250) = 'reviewform.ReviewForm'

EXEC utility.DropIndex @TableName, 'IX_ReviewForm'

EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewForm', 'IsActive, ReviewFormID, ControlCode', 'StatusID'
GO
--End table reviewform.ReviewForm