DECLARE @cBakFileName VARCHAR(250)
DECLARE @cDatabaseName VARCHAR(250)
DECLARE @cLogFileName VARCHAR(250)
DECLARE @cSQL VARCHAR(MAX)
 
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		D.Name AS DatabaseName,
		'''C:\Backup\' + D.Name + '_' + CONVERT(VARCHAR(20), GETDATE(),112) + '.bak''' AS BakFileName,
		F.Name AS LogFileName
	FROM master.dbo.sysdatabases D
		JOIN sys.master_files F ON F.Database_ID = D.DBID
			AND F.Type = 1
			AND D.Name NOT IN ('AJACSUtility','master','model','msdb','ReportServer','ReportServerTempDB','tempdb')
	ORDER BY D.Name

OPEN oCursor   
FETCH NEXT FROM oCursor INTO @cDatabaseName, @cBakFileName, @cLogFileName
WHILE @@FETCH_STATUS = 0   

	BEGIN

	SET @cSQL = 'ALTER DATABASE ' + @cDatabaseName + ' SET RECOVERY SIMPLE;'
	--EXEC (@cSQL)
	print @cSQL

	SET @cSQL = 'DBCC SHRINKFILE(' + @cLogFileName + ', 0, TRUNCATEONLY);'
	--EXEC (@cSQL)
	print @cSQL

	SET @cSQL = 'ALTER DATABASE ' + @cDatabaseName + ' SET RECOVERY FULL;'
	--EXEC (@cSQL)
	print @cSQL

	SET @cSQL = 'BACKUP DATABASE ' + @cDatabaseName + ' TO DISK = ' + @cBakFileName + ';'
	--EXEC (@cSQL)
	print @cSQL

	FETCH NEXT FROM oCursor INTO @cDatabaseName, @cBakFileName, @cLogFileName

	END   
--NEXT
 
CLOSE oCursor   
DEALLOCATE oCursor
