USE LEO0004
GO

DECLARE @cColumnName2 VARCHAR(50) = 'activityreport' + 'ID'
DECLARE @cIDList VARCHAR(MAX) = '1,2,3,4'

SELECT 
	NULL AS SchemaName, 
	NULL AS TableName,
	NULL AS ColumnName,
	'DECLARE @cIDList VARCHAR(MAX) = ''' + @cIDList + '''' AS SelectData,
	'DECLARE @cIDList VARCHAR(MAX) = ''' + @cIDList + '''' AS DeleteData,
	'' AS TruncateData

UNION

SELECT
	S.Name AS SchemaName,
	T.Name AS TableName,
	C.Name AS ColumnName,
	'SELECT * FROM ' + S.Name + '.' + T.Name + ' T JOIN core.ListToTable(@cIDList, '','') LTT ON CAST(LTT.ListItem AS INT) = T.' + C.Name,
	'DELETE T FROM ' + S.Name + '.' + T.Name + ' T JOIN core.ListToTable(@cIDList, '','') LTT ON CAST(LTT.ListItem AS INT) = T.' + C.Name,
	'TRUNCATE TABLE ' + S.Name + '.' + T.Name
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND 
			(
				C.Name LIKE @cColumnName2
			)
		--AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')
ORDER BY 2, 3
