-- File Name:	Build - 1.2 - LEO0000.sql
-- Build Key:	Build - 1.2 - 2017.01.27 17.02.08

USE [LEO0000]
GO

-- ==============================================================================================================================
-- Functions:
--		core.GetEntityTypeNameByEntityTypeCode
--
-- Procedures:
--		dropdown.GetActivityReportTypeData
--		dropdown.GetActivityTypeData
--		dropdown.GetAreaOfOperationTypeData
--		dropdown.GetAssetTypeData
--		dropdown.GetAtmosphericTypeData
--		dropdown.GetCommunicationThemeData
--		dropdown.GetConfidenceLevelData
--		dropdown.GetContactStatusData
--		dropdown.GetContactTypeData
--		dropdown.GetCountryCallingCodeData
--		dropdown.GetCountryData
--		dropdown.GetCurrencyData
--		dropdown.GetDateFilterData
--		dropdown.GetDistributorTypeData
--		dropdown.GetDocumentTypeData
--		dropdown.GetEngagementStatusData
--		dropdown.GetFindingStatusData
--		dropdown.GetFindingTypeData
--		dropdown.GetFundingSourceData
--		dropdown.GetImpactDecisionData
--		dropdown.GetIncidentTypeData
--		dropdown.GetInformationValidityData
--		dropdown.GetLearnerProfileTypeData
--		dropdown.GetLogicalFrameworkStatusData
--		dropdown.GetMediaReportSourceTypeData
--		dropdown.GetMediaReportTypeData
--		dropdown.GetModuleTypeData
--		dropdown.GetObjectiveTypeData
--		dropdown.GetProductOriginData
--		dropdown.GetProductStatusData
--		dropdown.GetProductTypeData
--		dropdown.GetProgramTypeData
--		dropdown.GetProjectData
--		dropdown.GetRequestForInformationResultTypeData
--		dropdown.GetRequestForInformationStatusData
--		dropdown.GetResourceProviderData
--		dropdown.GetRoleData
--		dropdown.GetSourceCategoryData
--		dropdown.GetSourceTypeData
--		dropdown.GetStatusChangeData
--		dropdown.GetSubContractorBusinessTypeData
--		dropdown.GetSubContractorRelationshipTypeData
--		dropdown.GetTargetAudienceData
--		dropdown.GetUnitTypeData
--		dropdown.GetVettingOutcomeData
--		implementer.ImplementerAddUpdate
--		implementer.ImplementerDropdownDataAddUpdate
--		implementer.ImplementerSynonymAddUpdate
--		utility.AddColumn
--
-- Tables:
--		dropdown.ActivityReportType
-- ==============================================================================================================================

--Begin file LEO0000 - 00 - Prerequisites.sql
USE LEO0000
GO

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--End file LEO0000 - 00 - Prerequisites.sql

--Begin file LEO0000 - 01 - Tables.sql
USE LEO0000
GO

--Begin table dropdown.ActivityReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityReportType
	(
	ActivityReportTypeID INT NOT NULL IDENTITY(0,1),
	ActivityReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportType', 'DisplayOrder,ActivityReportTypeName'
GO

SET IDENTITY_INSERT dropdown.ActivityReportType ON
GO  

INSERT INTO dropdown.ActivityReportType 
	(ActivityReportTypeID, ActivityReportTypeName, DisplayOrder)
VALUES 
	(0, NULL, 0),
	(1, 'Monthly', 1),
	(2, 'Quarterly', 2),
	(3, 'Additional', 3)
GO

SET IDENTITY_INSERT dropdown.ActivityReportType OFF
GO  
--End table dropdown.ActivityReportType

--Begin table implementer.Implementer
ALTER TABLE implementer.Implementer ALTER COLUMN ImplementerURL VARCHAR(50)
GO
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdown
EXEC utility.DropObject 'implementer.ImplementerDropdown'
GO
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdownData
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdownData'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '1'
GO
--End table implementer.ImplementerDropdownData

--End file LEO0000 - 01 - Tables.sql

--Begin file LEO0000 - 02 - Functions.sql
USE LEO0000
GO


--End file LEO0000 - 02 - Functions.sql

--Begin file LEO0000 - 03 - Procedures.sql
USE LEO0000
GO

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin procedure dropdown.GetActivityReportTypeData
EXEC Utility.DropObject 'dropdown.GetActivityReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityReportType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetActivityReportTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityReportTypeID, 
		T.ActivityReportTypeName
	FROM dropdown.ActivityReportType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ActivityReportTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ActivityReportType'
			AND (T.ActivityReportTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityReportTypeName, T.ActivityReportTypeID

END
GO
--End procedure dropdown.GetActivityReportTypeData

--Begin procedure dropdown.GetActivityTypeData
EXEC Utility.DropObject 'dropdown.GetActivityTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetActivityTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityTypeID, 
		T.ActivityTypeName
	FROM dropdown.ActivityType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ActivityTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ActivityType'
			AND (T.ActivityTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityTypeName, T.ActivityTypeID

END
GO
--End procedure dropdown.GetActivityTypeData

--Begin procedure dropdown.GetAreaOfOperationTypeData
EXEC Utility.DropObject 'dropdown.GetAreaOfOperationTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AreaOfOperationType table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetAreaOfOperationTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AreaOfOperationTypeID, 
		T.AreaOfOperationTypeName,
		T.HexColor
	FROM dropdown.AreaOfOperationType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AreaOfOperationTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AreaOfOperationType'
			AND (T.AreaOfOperationTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AreaOfOperationTypeName, T.AreaOfOperationTypeID

END
GO
--End procedure dropdown.GetAreaOfOperationTypeData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AssetTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AssetType'
			AND (T.AssetTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetAtmosphericTypeData
EXEC Utility.DropObject 'dropdown.GetAtmosphericTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AtmosphericType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetAtmosphericTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AtmosphericTypeID, 
		T.AtmosphericTypeName
	FROM dropdown.AtmosphericType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AtmosphericTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AtmosphericType'
			AND (T.AtmosphericTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AtmosphericTypeName, T.AtmosphericTypeID

END
GO
--End procedure dropdown.GetAtmosphericTypeData

--Begin procedure dropdown.GetCommunicationThemeData
EXEC Utility.DropObject 'dropdown.GetCommunicationThemeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.CommunicationTheme table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCommunicationThemeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunicationThemeID, 
		T.CommunicationThemeName
	FROM dropdown.CommunicationTheme T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CommunicationThemeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CommunicationTheme'
			AND (T.CommunicationThemeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunicationThemeName, T.CommunicationThemeID

END
GO
--End procedure dropdown.GetCommunicationThemeData

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConfidenceLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConfidenceLevel'
			AND (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetContactStatusData
EXEC Utility.DropObject 'dropdown.GetContactStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetContactStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactStatusID,
		T.ContactStatusName
	FROM dropdown.ContactStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactStatus'
			AND (T.ContactStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactStatusName, T.ContactStatusID

END
GO
--End procedure dropdown.GetContactStatusData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID,
		T.ContactTypeName
	FROM dropdown.ContactType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactType'
			AND (T.ContactTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin procedure dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CurrencyID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Currency'
			AND (T.CurrencyID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetDateFilterData
EXEC Utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DateFilterID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DateFilter'
			AND (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetDistributorTypeData
EXEC Utility.DropObject 'dropdown.GetDistributorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DistributorType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetDistributorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DistributorTypeID, 
		T.DistributorTypeName,
		T.DistributorTypeCode
	FROM dropdown.DistributorType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DistributorTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DistributorType'
			AND (T.DistributorTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.DistributorTypeName, T.DistributorTypeID

END
GO
--End procedure dropdown.GetDistributorTypeData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID,
		DC.DocumentCategoryID,
		DC.DocumentCategoryName,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
		JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = T.DocumentCategoryID
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DocumentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DocumentType'
			AND (T.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY DC.DisplayOrder, DC.DocumentCategoryName, T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetEngagementStatusData
EXEC Utility.DropObject 'dropdown.GetEngagementStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.EngagementStatus table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetEngagementStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementStatusID,
		T.EngagementStatusName
	FROM dropdown.EngagementStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EngagementStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EngagementStatus'
			AND (T.EngagementStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementStatusName, T.EngagementStatusID

END
GO
--End procedure dropdown.GetEngagementStatusData

--Begin procedure dropdown.GetFindingStatusData
EXEC Utility.DropObject 'dropdown.GetFindingStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFindingStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingStatusID,
		T.FindingStatusName
	FROM dropdown.FindingStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingStatus'
			AND (T.FindingStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingStatusName, T.FindingStatusID

END
GO
--End procedure dropdown.GetFindingStatusData

--Begin procedure dropdown.GetFindingTypeData
EXEC Utility.DropObject 'dropdown.GetFindingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetFindingTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingTypeID,
		T.FindingTypeName
	FROM dropdown.FindingType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingType'
			AND (T.FindingTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingTypeName, T.FindingTypeID

END
GO
--End procedure dropdown.GetFindingTypeData

--Begin procedure dropdown.GetFundingSourceData
EXEC Utility.DropObject 'dropdown.GetFundingSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FundingSource table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFundingSourceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FundingSourceID, 
		T.FundingSourceName
	FROM dropdown.FundingSource T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FundingSourceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FundingSource'
			AND (T.FundingSourceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FundingSourceName, T.FundingSourceID

END
GO
--End procedure dropdown.GetFundingSourceData

--Begin procedure dropdown.GetImpactDecisionData
EXEC Utility.DropObject 'dropdown.GetImpactDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ImpactDecision table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetImpactDecisionData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImpactDecisionID,
		T.ImpactDecisionName,
		T.HexColor
	FROM dropdown.ImpactDecision T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ImpactDecisionID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ImpactDecision'
			AND (T.ImpactDecisionID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImpactDecisionName, T.ImpactDecisionID

END
GO
--End procedure dropdown.GetImpactDecisionData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID, 
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentType'
			AND (T.IncidentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.IncidentTypeCategory,T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure dropdown.GetInformationValidityData
EXEC Utility.DropObject 'dropdown.GetInformationValidityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.InformationValidity table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetInformationValidityData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InformationValidityID, 
		T.InformationValidityName
	FROM dropdown.InformationValidity T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.InformationValidityID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'InformationValidity'
			AND (T.InformationValidityID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.InformationValidityName, T.InformationValidityID

END
GO
--End procedure dropdown.GetInformationValidityData

--Begin procedure dropdown.GetLearnerProfileTypeData
EXEC Utility.DropObject 'dropdown.GetLearnerProfileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LearnerProfileType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetLearnerProfileTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LearnerProfileTypeID,
		T.LearnerProfileTypeName
	FROM dropdown.LearnerProfileType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LearnerProfileTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LearnerProfileType'
			AND (T.LearnerProfileTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.LearnerProfileTypeName, T.LearnerProfileTypeID

END
GO
--End procedure dropdown.GetLearnerProfileTypeData

--Begin procedure dropdown.GetLogicalFrameworkStatusData
EXEC Utility.DropObject 'dropdown.GetLogicalFrameworkStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LogicalFrameworkStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetLogicalFrameworkStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LogicalFrameworkStatusID,
		T.LogicalFrameworkStatusName
	FROM dropdown.LogicalFrameworkStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LogicalFrameworkStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LogicalFrameworkStatus'
			AND (T.LogicalFrameworkStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.LogicalFrameworkStatusName, T.LogicalFrameworkStatusID

END
GO
--End procedure dropdown.GetLogicalFrameworkStatusData

--Begin procedure dropdown.GetMediaReportSourceTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportSourceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetMediaReportSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportSourceTypeID,
		T.MediaReportSourceTypeName,
		T.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		T.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM dropdown.MediaReportSourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportSourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportSourceType'
			AND (T.MediaReportSourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportSourceTypeName, T.MediaReportSourceTypeID

END
GO
--End procedure dropdown.GetMediaReportSourceTypeData

--Begin procedure dropdown.GetMediaReportTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetMediaReportTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportTypeID,
		T.MediaReportTypeName
	FROM dropdown.MediaReportType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportType'
			AND (T.MediaReportTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportTypeName, T.MediaReportTypeID

END
GO
--End procedure dropdown.GetMediaReportTypeData

--Begin procedure dropdown.GetModuleTypeData
EXEC Utility.DropObject 'dropdown.GetModuleTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ModuleType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetModuleTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ModuleTypeID,
		T.ModuleTypeName
	FROM dropdown.ModuleType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ModuleTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ModuleType'
			AND (T.ModuleTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ModuleTypeName, T.ModuleTypeID

END
GO
--End procedure dropdown.GetModuleTypeData

--Begin procedure dropdown.GetObjectiveTypeData
EXEC Utility.DropObject 'dropdown.GetObjectiveTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ObjectiveType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetObjectiveTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ObjectiveTypeID, 
		T.ObjectiveTypeCode,
		T.ObjectiveTypeName
	FROM dropdown.ObjectiveType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ObjectiveTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ObjectiveType'
			AND (T.ObjectiveTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ObjectiveTypeName, T.ObjectiveTypeID

END
GO
--End procedure dropdown.GetObjectiveTypeData

--Begin procedure dropdown.GetProductOriginData
EXEC Utility.DropObject 'dropdown.GetProductOriginData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductOrigin table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductOriginData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductOriginID,
		T.ProductOriginName
	FROM dropdown.ProductOrigin T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductOriginID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductOrigin'
			AND (T.ProductOriginID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductOriginName, T.ProductOriginID

END
GO
--End procedure dropdown.GetProductOriginData

--Begin procedure dropdown.GetProductStatusData
EXEC Utility.DropObject 'dropdown.GetProductStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductStatusID, 
		T.ProductStatusName
	FROM dropdown.ProductStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductStatus'
			AND (T.ProductStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductStatusName, T.ProductStatusID

END
GO
--End procedure dropdown.GetProductStatusData

--Begin procedure dropdown.GetProductTypeData
EXEC Utility.DropObject 'dropdown.GetProductTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProductTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductTypeID, 
		T.ProductTypeName
	FROM dropdown.ProductType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductType'
			AND (T.ProductTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductTypeName, T.ProductTypeID

END
GO
--End procedure dropdown.GetProductTypeData

--Begin procedure dropdown.GetProgramTypeData
EXEC Utility.DropObject 'dropdown.GetProgramTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProgramType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProgramTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProgramTypeID, 
		T.ProgramTypeName
	FROM dropdown.ProgramType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProgramTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProgramType'
			AND (T.ProgramTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProgramTypeName, T.ProgramTypeID

END
GO
--End procedure dropdown.GetProgramTypeData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName
	FROM dropdown.Project T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProjectID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Project'
			AND (T.ProjectID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetRequestForInformationResultTypeData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationResultTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationResultType table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationResultTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationResultTypeID, 
		T.RequestForInformationResultTypeCode,
		T.RequestForInformationResultTypeName
	FROM dropdown.RequestForInformationResultType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationResultTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationResultType'
			AND (T.RequestForInformationResultTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationResultTypeName, T.RequestForInformationResultTypeID

END
GO
--End procedure dropdown.GetRequestForInformationResultTypeData

--Begin procedure dropdown.GetRequestForInformationStatusData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationStatusID, 
		T.RequestForInformationStatusCode,
		T.RequestForInformationStatusName
	FROM dropdown.RequestForInformationStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationStatus'
			AND (T.RequestForInformationStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationStatusName, T.RequestForInformationStatusID

END
GO
--End procedure dropdown.GetRequestForInformationStatusData

--Begin procedure dropdown.GetResourceProviderData
EXEC Utility.DropObject 'dropdown.GetResourceProviderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ResourceProvider table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetResourceProviderData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResourceProviderID, 
		T.ResourceProviderName
	FROM dropdown.ResourceProvider T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ResourceProviderID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ResourceProvider'
			AND (T.ResourceProviderID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ResourceProviderName, T.ResourceProviderID

END
GO
--End procedure dropdown.GetResourceProviderData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleName
	FROM dropdown.Role T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RoleID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Role'
			AND (T.RoleID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSourceCategoryData
EXEC Utility.DropObject 'dropdown.GetSourceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetSourceCategoryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceCategoryID,
		T.SourceCategoryName
	FROM dropdown.SourceCategory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceCategoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceCategory'
			AND (T.SourceCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceCategoryName, T.SourceCategoryID

END
GO
--End procedure dropdown.GetSourceCategoryData

--Begin procedure dropdown.GetSourceTypeData
EXEC Utility.DropObject 'dropdown.GetSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceTypeID, 
		T.SourceTypeName
	FROM dropdown.SourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceType'
			AND (T.SourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceTypeName, T.SourceTypeID

END
GO
--End procedure dropdown.GetSourceTypeData

--Begin procedure dropdown.GetStatusChangeData
EXEC Utility.DropObject 'dropdown.GetStatusChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.StatusChange table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetStatusChangeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusChangeID,
		T.StatusChangeName
	FROM dropdown.StatusChange T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.StatusChangeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'StatusChange'
			AND (T.StatusChangeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusChangeName, T.StatusChangeID

END
GO
--End procedure dropdown.GetStatusChangeData

--Begin procedure dropdown.GetSubContractorBusinessTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorBusinessTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorBusinessType table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorBusinessTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorBusinessTypeID, 
		T.SubContractorBusinessTypeName
	FROM dropdown.SubContractorBusinessType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorBusinessTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorBusinessType'
			AND (T.SubContractorBusinessTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorBusinessTypeName, T.SubContractorBusinessTypeID

END
GO
--End procedure dropdown.GetSubContractorBusinessTypeData

--Begin procedure dropdown.GetSubContractorRelationshipTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorRelationshipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorRelationshipType table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorRelationshipTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorRelationshipTypeID, 
		T.SubContractorRelationshipTypeName
	FROM dropdown.SubContractorRelationshipType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorRelationshipTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorRelationshipType'
			AND (T.SubContractorRelationshipTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorRelationshipTypeName, T.SubContractorRelationshipTypeID

END
GO
--End procedure dropdown.GetSubContractorRelationshipTypeData

--Begin procedure dropdown.GetTargetAudienceData
EXEC Utility.DropObject 'dropdown.GetTargetAudienceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.TargetAudience table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetTargetAudienceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TargetAudienceID, 
		T.TargetAudienceName
	FROM dropdown.TargetAudience T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TargetAudienceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TargetAudience'
			AND (T.TargetAudienceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.TargetAudienceName, T.TargetAudienceID

END
GO
--End procedure dropdown.GetTargetAudienceData

--Begin procedure dropdown.GetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.UnitType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetUnitTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnitTypeID, 
		T.UnitTypeName
	FROM dropdown.UnitType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.UnitTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'UnitType'
			AND (T.UnitTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.UnitTypeName, T.UnitTypeID

END
GO
--End procedure dropdown.GetUnitTypeData

--Begin procedure dropdown.GetVettingOutcomeData
EXEC Utility.DropObject 'dropdown.GetVettingOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.VettingOutcome table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetVettingOutcomeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VettingOutcomeID,
		T.VettingOutcomeName
	FROM dropdown.VettingOutcome T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.VettingOutcomeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'VettingOutcome'
			AND (T.VettingOutcomeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.VettingOutcomeName, T.VettingOutcomeID

END
GO
--End procedure dropdown.GetVettingOutcomeData

--Begin procedure implementer.ImplementerAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new implementer to the LEO system and initialize its dropdown data
-- ===========================================================================================================
CREATE PROCEDURE implementer.ImplementerAddUpdate

@ImplementerCode VARCHAR(25),
@ImplementerName VARCHAR(250),
@ImplementerURL VARCHAR(50),
@ProjectNameList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCode VARCHAR(250)
	DECLARE @cSQL VARCHAR(MAX)

	IF NOT EXISTS (SELECT 1 FROM implementer.Implementer I WHERE I.ImplementerCode = @ImplementerCode)
		INSERT INTO implementer.Implementer (ImplementerCode, ImplementerName, ImplementerURL) VALUES (@ImplementerCode, @ImplementerName, @ImplementerURL)
	--ENDIF

	INSERT INTO dropdown.Project
		(ProjectName)
	SELECT
		LTT.ListItem
	FROM core.ListToTable(@ProjectNameList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dropdown.Project P
		WHERE P.ProjectName = LTT.ListItem
		)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, DropdownID)
	SELECT
		@ImplementerCode,
		'Project',
		P.ProjectID
	FROM dropdown.Project P
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItem = P.ProjectName

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = 'Dropdown'
				AND T.Name <> 'Project'
		ORDER BY T.Name

	OPEN oCursor
	FETCH oCursor INTO @cDropdownCode
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC implementer.ImplementerDropdownDataAddUpdate @cDropdownCode

		FETCH oCursor INTO @cDropdownCode
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure implementer.ImplementerAddUpdate

--Begin procedure implementer.ImplementerDropdownDataAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerDropdownDataAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new dropdown / dropdown data to the LEO system and grant implemeters access to the new data
-- ====================================================================================================================================
CREATE PROCEDURE implementer.ImplementerDropdownDataAddUpdate

@DropdownCode VARCHAR(50),
@DropdownCodePrimaryKey VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCodePrimaryKey VARCHAR(60) = ISNULL(@DropdownCodePrimaryKey, @DropdownCode + 'ID')
	DECLARE @cSQL VARCHAR(MAX)

	SET @cSQL = 'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT I.ImplementerCode, '''
	SET @cSQL += @DropdownCode + ''', T.' + @cDropdownCodePrimaryKey + ' FROM dropdown.' + @DropdownCode
	SET @cSQL += ' T CROSS APPLY implementer.Implementer I WHERE NOT EXISTS'
	SET @cSQL += ' (SELECT 1 FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = I.ImplementerCode AND IDD.DropdownCode = '''
	SET @cSQL += @DropdownCode + ''' AND IDD.DropdownID = T.' + @cDropdownCodePrimaryKey + ')'

	EXEC (@cSQL)

END
GO
--End procedure implementer.ImplementerDropdownDataAddUpdate

--Begin procedure implementer.ImplementerSynonymAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerSynonymAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer synonym records
-- ===========================================================================
CREATE PROCEDURE implementer.ImplementerSynonymAddUpdate

@BaseObjectSchema VARCHAR(50),
@BaseObjectName VARCHAR(250),
@Mode VARCHAR(50) = ''

AS
BEGIN
	SET NOCOUNT ON;

	IF @Mode = 'Delete'
		DELETE I FROM implementer.Synonym I WHERE I.BaseObjectSchema = @BaseObjectSchema AND I.BaseObjectName = @BaseObjectName
	ELSE IF NOT EXISTS (SELECT 1 FROM implementer.Synonym I WHERE I.BaseObjectSchema = @BaseObjectSchema AND I.BaseObjectName = @BaseObjectName)
		INSERT INTO implementer.Synonym (BaseObjectSchema, BaseObjectName) VALUES (@BaseObjectSchema, @BaseObjectName)
	--ENDIF

END
GO
--End procedure implementer.ImplementerSynonymAddUpdate
--End file LEO0000 - 03 - Procedures.sql

--Begin file LEO0000 - 04 - Data.sql
USE LEO0000
GO

EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
EXEC Utility.DropObject 'dropdown.GetRiskStatusData'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'core', 'GetEntityTypeNameByEntityTypeCode'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ActivityReportType'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetActivityReportTypeData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEntityTypeNameData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEventCodeNameData', 'Delete'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetRiskStatusData', 'Delete'
GO

UPDATE DT 
SET 
	DT.DocumentTypeName = 'Monthly Activity'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'MonthlyActivity'
GO

UPDATE I SET I.ImplementerName = 'HMG', I.ImplementerURL = 'ABBY' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0001'
UPDATE I SET I.ImplementerName = 'Strategm', I.ImplementerURL = 'NIKO' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0002'
UPDATE I SET I.ImplementerName = 'TSN', I.ImplementerURL = 'ODEE' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0003'
UPDATE I SET I.ImplementerName = 'ICS', I.ImplementerURL = 'PETRA' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0004'
GO

UPDATE P SET P.ProjectName = 'Leo' FROM dropdown.Project P WHERE P.ProjectName = 'Project One'
UPDATE P SET P.ProjectName = 'Disrupt' FROM dropdown.Project P WHERE P.ProjectName = 'Project Two'
UPDATE P SET P.ProjectName = 'Undermine' FROM dropdown.Project P WHERE P.ProjectName = 'Project Three'
UPDATE P SET P.ProjectName = 'MAO' FROM dropdown.Project P WHERE P.ProjectName = 'Project Four'
GO

UPDATE P SET P.ProjectCode = NULL FROM dropdown.Project P
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ActivityReportType'
GO

EXEC implementer.ImplementerAddUpdate 'LEO0005', 'ARK', 'Tamba', 'Grassroots'
GO

--End file LEO0000 - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Budgets', 'Budgets', 0;
EXEC permissionable.SavePermissionableGroup 'Contacts', 'Contacts', 0;
EXEC permissionable.SavePermissionableGroup 'Documents', 'Documents', 0;
EXEC permissionable.SavePermissionableGroup 'Equipment', 'Equipment', 0;
EXEC permissionable.SavePermissionableGroup 'ForceAssets', 'Forces & Assets', 0;
EXEC permissionable.SavePermissionableGroup 'Research', 'Insight & Understanding', 0;
EXEC permissionable.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0;
EXEC permissionable.SavePermissionableGroup 'ProductDistributor', 'Product Distribution', 0;
EXEC permissionable.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0;
EXEC permissionable.SavePermissionableGroup 'Subcontractor', 'Sub Contractors', 0;
EXEC permissionable.SavePermissionableGroup 'Territories', 'Territories', 0;
EXEC permissionable.SavePermissionableGroup 'Training', 'Training', 0;
EXEC permissionable.SavePermissionableGroup 'Utility', 'Utility', 0;
EXEC permissionable.SavePermissionableGroup 'Workflows', 'Workflows', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the event log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImplementerSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ImplementerSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImplementerSetup', @DESCRIPTION='View the list of server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ImplementerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a system user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of system users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a system user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='addupdate social media info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addupdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.addupdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='social media list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='list', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.list', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='refresh social media data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='refresh', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.refresh', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='renew token', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='renew', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.renew', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View a budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts in the vetting process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type additional activity in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.AdditionalActivity', @PERMISSIONCODE='AdditionalActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type general in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.General', @PERMISSIONCODE='General';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type legal & policy in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Legal&Policy', @PERMISSIONCODE='Legal&Policy';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type leonardo administration in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LeonardoAdministration', @PERMISSIONCODE='LeonardoAdministration';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type leonardo user guides in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LeonardoUserGuides', @PERMISSIONCODE='LeonardoUserGuides';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type log frame in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LogFrame', @PERMISSIONCODE='LogFrame';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type m & e plan in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.M&EPlan', @PERMISSIONCODE='M&EPlan';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type media in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type meeting in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Meeting', @PERMISSIONCODE='Meeting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type monthly activty in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.MonthlyActivity', @PERMISSIONCODE='MonthlyActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type other in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type plan in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Plan', @PERMISSIONCODE='Plan';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type presentations in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Presentations', @PERMISSIONCODE='Presentations';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type procurement in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Procurement', @PERMISSIONCODE='Procurement';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type program in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Program', @PERMISSIONCODE='Program';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type quality assurance feedback document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.QualityAssuranceFeedbackDocument', @PERMISSIONCODE='QualityAssuranceFeedbackDocument';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type quarterly activity in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.QuarterlyActivity', @PERMISSIONCODE='QuarterlyActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type reporting in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Reporting', @PERMISSIONCODE='Reporting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type rfi response in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.RFIResponse', @PERMISSIONCODE='RFIResponse';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type situational in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Situational', @PERMISSIONCODE='Situational';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type spot in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Spot', @PERMISSIONCODE='Spot';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type trainer 1 document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Trainer1Document', @PERMISSIONCODE='Trainer1Document';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type trainer 2 document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Trainer2Document', @PERMISSIONCODE='Trainer2Document';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type training in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Training', @PERMISSIONCODE='Training';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of forces', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit an activity report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of activity reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View an activity report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View the list of atmospheric reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='Add / edit a field report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='View the list of field reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='View a field report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View the list of findings', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View the list of recommendations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View the list of risks', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export the list of risks', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a trend report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a trend report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View the list of indicators', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicator type', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View the list of indicator types', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View the list of milestones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View the objectives overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View the list of objectives', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage objectives', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View objective overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='Add / edit a campaign', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='View the list of campaigns', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='View a campaign', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='Add / edit a distributor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='View the list of distributors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='View a distributor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='Add / edit an Impact Story', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='View the list of Impact Stories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='View an Impact Story', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='Add / edit a product', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View the list of products', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View the product overview', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View a product', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProductReport', @DESCRIPTION='List Product Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ProductReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View the list of program reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='Add / edit a sub-contractor capability', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateCapability', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.AddUpdateCapability', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View the list of sub-contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View the list of Sub-contractor Capabilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCapabilities', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.ListCapabilities', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='Add / edit an activity', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='View the list of activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='View an activity', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.2 - 2017.01.27 17.02.08')
GO
--End build tracking

