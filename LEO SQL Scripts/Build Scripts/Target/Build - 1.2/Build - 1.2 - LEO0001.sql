-- File Name:	Build - 1.2 - LEO0001.sql
-- Build Key:	Build - 1.2 - 2017.01.27 17.02.08

USE [LEO0001]
GO

-- ==============================================================================================================================
-- Procedures:
--		activityreport.GetActivityReportByActivityReportID
--		core.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode
--		document.GetDocumentsByEntityTypeCodeAndEntityID
--		dropdown.GetEventCodeNameData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogActivityReportAction
--		eventlog.LogAssetAction
--		eventlog.LogFindingAction
--		eventlog.LogRecommendationAction
--		productdistributor.GetImpactStoryByImpactStoryID
--		training.GetModuleByModuleID
--		utility.AddColumn
--
-- Schemas:
--		activityreport
--
-- Tables:
--		activityreport.ActivityReport
--		activityreport.ActivityReportAsset
--		activityreport.ActivityReportAtmospheric
--		activityreport.ActivityReportCampaign
--		activityreport.ActivityReportForce
--		activityreport.ActivityReportImpactStory
--		activityreport.ActivityReportIncident
--		activityreport.ActivityReportMediaReport
--		activityreport.ActivityReportProduct
--		activityreport.ActivityReportTerritory
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0001
GO

EXEC utility.AddSchema 'activityreport'
GO

--Begin table activityreport.ActivityReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReport
	(
	ActivityReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportTypeID INT,
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX),
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportID'
GO
--End table activityreport.ActivityReport

--Begin table activityreport.ActivityReportAsset
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAsset'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAsset
	(
	ActivityReportAssetID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAsset', 'ActivityReportID,AssetID'
GO
--End table activityreport.ActivityReportAsset

--Begin table activityreport.ActivityReportAtmospheric
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAtmospheric'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAtmospheric
	(
	ActivityReportAtmosphericID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AtmosphericID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAtmosphericID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAtmospheric', 'ActivityReportID,AtmosphericID'
GO
--End table activityreport.ActivityReportAtmospheric

--Begin table activityreport.ActivityReportCampaign
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportCampaign'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportCampaign
	(
	ActivityReportCampaignID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	CampaignID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CampaignID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportCampaignID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportCampaign', 'ActivityReportID,CampaignID'
GO
--End table activityreport.ActivityReportCampaign

--Begin table activityreport.ActivityReportForce
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportForce'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportForce
	(
	ActivityReportForceID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportForce', 'ActivityReportID,ForceID'
GO
--End table activityreport.ActivityReportForce

--Begin table activityreport.ActivityReportImpactStory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportImpactStory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportImpactStory
	(
	ActivityReportImpactStoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ImpactStoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImpactStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportImpactStoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportImpactStory', 'ActivityReportID,ImpactStoryID'
GO
--End table activityreport.ActivityReportImpactStory

--Begin table activityreport.ActivityReportIncident
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportIncident
	(
	ActivityReportIncidentID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportIncident', 'ActivityReportID,IncidentID'
GO
--End table activityreport.ActivityReportIncident

--Begin table activityreport.ActivityReportProduct
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportProduct'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportProduct
	(
	ActivityReportProductID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ProductID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportProductID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportProduct', 'ActivityReportID,ProductID'
GO
--End table activityreport.ActivityReportProduct

--Begin table activityreport.ActivityReportMediaReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportMediaReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportMediaReport
	(
	ActivityReportMediaReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	MediaReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportMediaReportID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportMediaReport', 'ActivityReportID,MediaReportID'
GO
--End table activityreport.ActivityReportMediaReport

--Begin table activityreport.ActivityReportTerritory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportTerritory
	(
	ActivityReportTerritoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportTerritory', 'ActivityReportID,TerritoryID'
GO
--End table activityreport.ActivityReportTerritory

--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0001
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0001
GO

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:		Todd Pires
-- Create date: 2017.01.24
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID)
	
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail
	FROM activityreport.ActivityReport AR
	WHERE AR.ActivityReportID = @ActivityReportID

	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
	ORDER BY A.AssetName, A.AssetID

	SELECT
		A.AtmosphericID,
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted,
		ATT.AtmosphericTypeName
	FROM activityreport.ActivityReportAtmospheric ARA
		JOIN atmospheric.Atmospheric A ON A.AtmosphericID = ARA.AtmosphericID
		JOIN dropdown.AtmosphericType ATT ON ATT.AtmosphericTypeID = A.AtmosphericTypeID
			AND ARA.ActivityReportID = @ActivityReportID
	ORDER BY ATT.AtmosphericTypeName, A.AtmosphericDate

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM activityreport.ActivityReportCampaign ARC
		JOIN productdistributor.Campaign C ON C.CampaignID = ARC.CampaignID
			AND ARC.ActivityReportID = @ActivityReportID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
	ORDER BY F.ForceName, F.ForceID

	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM activityreport.ActivityReportImpactStory ARI
		JOIN productdistributor.ImpactStory I ON I.ImpactStoryID = ARI.ImpactStoryID
			AND ARI.ActivityReportID = @ActivityReportID
	ORDER BY I.ImpactStoryName, I.ImpactStoryID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN dbo.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM activityreport.ActivityReportMediaReport ARMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = ARMR.MediaReportID
			AND ARMR.ActivityReportID = @ActivityReportID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	SELECT
		P.ProductID,
		P.ProductName
	FROM activityreport.ActivityReportProduct ARP
		JOIN productdistributor.Product P ON P.ProductID = ARP.ProductID
			AND ARP.ActivityReportID = @ActivityReportID
	ORDER BY P.ProductName, P.ProductID

	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
	WHERE ART.ActivityReportID = @ActivityReportID
	ORDER BY 2, 1

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ActivityReport', @ActivityReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ActivityReport', @ActivityReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ActivityReport'
		AND EL.EntityID = @ActivityReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure core.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode
EXEC utility.DropObject 'core.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode

@EntityTypeCode VARCHAR(50),
@WorkflowActionCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.WorkflowActionCode = @WorkflowActionCode

END
GO
--End procedure core.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode

--Begin procedure document.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'document.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.GetEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogActivityReportAction
EXEC utility.DropObject 'eventlog.LogActivityReportAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2017.01.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogActivityReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ActivityReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cActivityReportAssets VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportAssets = COALESCE(@cActivityReportAssets, '') + D.ActivityReportAsset 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportAsset'), ELEMENTS) AS ActivityReportAsset
			FROM activityreport.ActivityReportAsset T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportAtmospherics VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportAtmospherics = COALESCE(@cActivityReportAtmospherics, '') + D.ActivityReportAtmospheric
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportAtmospheric'), ELEMENTS) AS ActivityReportAtmospheric
			FROM activityreport.ActivityReportAtmospheric T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportCampaigns VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportCampaigns = COALESCE(@cActivityReportCampaigns, '') + D.ActivityReportCampaign 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportCampaign'), ELEMENTS) AS ActivityReportCampaign
			FROM activityreport.ActivityReportCampaign T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportForces VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportForces = COALESCE(@cActivityReportForces, '') + D.ActivityReportForce 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportForce'), ELEMENTS) AS ActivityReportForce
			FROM activityreport.ActivityReportForce T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportImpactStories VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportImpactStories = COALESCE(@cActivityReportImpactStories, '') + D.ActivityReportImpactStory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportImpactStory'), ELEMENTS) AS ActivityReportImpactStory
			FROM activityreport.ActivityReportImpactStory T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportIncidents = COALESCE(@cActivityReportIncidents, '') + D.ActivityReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportIncident'), ELEMENTS) AS ActivityReportIncident
			FROM activityreport.ActivityReportIncident T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportMediaReports VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportMediaReports = COALESCE(@cActivityReportMediaReports, '') + D.ActivityReportMediaReport 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportMediaReport'), ELEMENTS) AS ActivityReportMediaReport
			FROM activityreport.ActivityReportMediaReport T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportProducts VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportProducts = COALESCE(@cActivityReportProducts, '') + D.ActivityReportProduct 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportProduct'), ELEMENTS) AS ActivityReportProduct
			FROM activityreport.ActivityReportProduct T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportTerritories = COALESCE(@cActivityReportTerritories, '') + D.ActivityReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportTerritory'), ELEMENTS) AS ActivityReportTerritory
			FROM activityreport.ActivityReportTerritory T 
			WHERE T.ActivityReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ActivityReportAssets>' + ISNULL(@cActivityReportAssets, '') + '</ActivityReportAssets>') AS XML),
			CAST(('<ActivityReportAtmospherics>' + ISNULL(@cActivityReportAtmospherics, '') + '</ActivityReportAtmospherics>') AS XML),
			CAST(('<ActivityReportCampaigns>' + ISNULL(@cActivityReportCampaigns, '') + '</ActivityReportCampaigns>') AS XML),
			CAST(('<ActivityReportForces>' + ISNULL(@cActivityReportForces, '') + '</ActivityReportForces>') AS XML),
			CAST(('<ActivityReportImpactStories>' + ISNULL(@cActivityReportImpactStories, '') + '</ActivityReportImpactStories>') AS XML),
			CAST(('<ActivityReportIncidents>' + ISNULL(@cActivityReportIncidents, '') + '</ActivityReportIncidents>') AS XML),
			CAST(('<ActivityReportMediaReports>' + ISNULL(@cActivityReportMediaReports, '') + '</ActivityReportMediaReports>') AS XML),
			CAST(('<ActivityReportProducts>' + ISNULL(@cActivityReportProducts, '') + '</ActivityReportProducts>') AS XML),
			CAST(('<ActivityReportTerritories>' + ISNULL(@cActivityReportTerritories, '') + '</ActivityReportTerritories>') AS XML)
			FOR XML RAW('ActivityReport'), ELEMENTS
			)
		FROM activityreport.ActivityReport T
		WHERE T.ActivityReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogActivityReportAction

--Begin procedure eventlog.LogAssetAction
EXEC utility.DropObject 'eventlog.LogAssetAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			T.AssetID,
			@Comments
		FROM asset.Asset T
		WHERE T.AssetID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cAssetEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetEquipmentResourceProviders = COALESCE(@cAssetEquipmentResourceProviders, '') + D.AssetEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetEquipmentResourceProvider'), ELEMENTS) AS AssetEquipmentResourceProvider
			FROM asset.AssetEquipmentResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		DECLARE @cAssetFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetFinancialResourceProviders = COALESCE(@cAssetFinancialResourceProviders, '') + D.AssetFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetFinancialResourceProvider'), ELEMENTS) AS AssetFinancialResourceProvider
			FROM asset.AssetFinancialResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogAssetActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogAssetActionTable
		FROM asset.Asset T
		WHERE T.AssetID = @EntityID
		
		ALTER TABLE #LogAssetActionTable DROP COLUMN Location
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(A.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<AssetEquipmentResourceProviders>' + ISNULL(@cAssetEquipmentResourceProviders, '') + '</AssetEquipmentResourceProviders>') AS XML),
			CAST(('<AssetFinancialResourceProviders>' + ISNULL(@cAssetFinancialResourceProviders, '') + '</AssetFinancialResourceProviders>') AS XML)
			FOR XML RAW('Asset'), ELEMENTS
			)
		FROM #LogAssetActionTable T
			JOIN asset.Asset A ON A.AssetID = T.AssetID

		DROP TABLE #LogAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAssetAction

--Begin procedure eventlog.LogFindingAction
EXEC utility.DropObject 'eventlog.LogFindingAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFindingAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cFindingIndicators VARCHAR(MAX) 
	
		SELECT 
			@cFindingIndicators = COALESCE(@cFindingIndicators, '') + D.FindingIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingIndicators'), ELEMENTS) AS FindingIndicators
			FROM finding.FindingIndicator T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cFindingRecommendations = COALESCE(@cFindingRecommendations, '') + D.FindingRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRecommendations'), ELEMENTS) AS FindingRecommendations
			FROM finding.FindingRecommendation T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingTerritories VARCHAR(MAX) 
	
		SELECT 
			@cFindingTerritories = COALESCE(@cFindingTerritories, '') + D.FindingTerritories 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingTerritories'), ELEMENTS) AS FindingTerritories
			FROM finding.FindingTerritory T 
			WHERE T.FindingID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<FindingIndicators>' + ISNULL(@cFindingIndicators, '') + '</FindingIndicators>') AS XML),
			CAST(('<FindingRecommendations>' + ISNULL(@cFindingRecommendations, '') + '</FindingRecommendations>') AS XML),
			CAST(('<FindingTerritories>' + ISNULL(@cFindingTerritories, '') + '</FindingTerritories>') AS XML)
			FOR XML RAW('Finding'), ELEMENTS
			)
		FROM finding.Finding T
		WHERE T.FindingID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFindingAction

--Begin procedure eventlog.LogRecommendationAction
EXEC utility.DropObject 'eventlog.LogRecommendationAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationIndicators VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationIndicators = COALESCE(@cRecommendationIndicators, '') + D.RecommendationIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationIndicators'), ELEMENTS) AS RecommendationIndicators
			FROM recommendation.RecommendationIndicator T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationTerritories VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationTerritories = COALESCE(@cRecommendationTerritories, '') + D.RecommendationTerritories 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationTerritories'), ELEMENTS) AS RecommendationTerritories
			FROM recommendation.RecommendationTerritory T 
			WHERE T.RecommendationID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationIndicators>' + ISNULL(@cRecommendationIndicators, '') + '</RecommendationIndicators>') AS XML),
			CAST(('<RecommendationTerritories>' + ISNULL(@cRecommendationTerritories, '') + '</RecommendationTerritories>') AS XML)
			FOR XML RAW('Recommendation'), ELEMENTS
			)
		FROM recommendation.Recommendation T
		WHERE T.RecommendationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationAction

--Begin procedure productdistributor.GetImpactStoryByImpactStoryID
EXEC utility.DropObject 'productdistributor.GetImpactStoryByImpactStoryID'
GO

-- =======================================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.05
-- Description:	A stored procedure to add data to the productdistributor.ImpactStory table
-- =======================================================================================
CREATE PROCEDURE productdistributor.GetImpactStoryByImpactStoryID

@ImpactStoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		I.ImpactStoryID,
		I.ImpactStoryName,
		I.ImpactStoryDescription,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName
	FROM productdistributor.ImpactStory I
		WHERE I.ImpactStoryID = @ImpactStoryID

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM productdistributor.ImpactStoryCampaign IC
		JOIN productdistributor.Campaign C ON C.CampaignID = IC.CampaignID
			AND IC.ImpactStoryID = @ImpactStoryID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		P.ProductID,
		P.ProductName
	FROM productdistributor.ImpactStoryProduct IP
		JOIN productdistributor.Product P ON P.ProductID = IP.ProductID
			AND IP.ImpactStoryID = @ImpactStoryID
	ORDER BY P.ProductName, P.ProductID

	SELECT
		IT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(IT.TerritoryID) AS TerritoryNameFormatted
	FROM productdistributor.ImpactStoryTerritory IT
	WHERE IT.ImpactStoryID = @ImpactStoryID
	ORDER BY 2, 1
	
END
GO
--End procedure productdistributor.GetImpactStoryByImpactStoryID

--Begin procedure training.GetModuleByModuleID
EXEC utility.DropObject 'training.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the training.Module table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added call for Module Document information
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE training.GetModuleByModuleID

@ModuleID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.ModuleID,
		C.ModuleName,
		C.ModuleTypeID,
		C.Summary,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.SponsorName,
		C.Notes,
		CT.ModuleTypeID,
		CT.ModuleTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		core.GetEntityTypeNameByEntityTypeCode('Module') AS EntityTypeName
	FROM training.Module C
		JOIN dropdown.ModuleType CT ON CT.ModuleTypeID = C.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ModuleID = @ModuleID
		
END
GO
--End procedure training.GetModuleByModuleID

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0001
GO

DELETE S
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'ConceptNoteBackgroundText'
GO

UPDATE S
SET S.SetupValue = 'jcole@skotkonung.com,rabaat@skotkonung.com'
WHERE S.SetupKey IN ('FeedBackMailTo', 'RFIMailTo', 'VettingMailTo')
GO

DECLARE @cURLPrefix VARCHAR(50)

SELECT @cURLPrefix = I.ImplementerURL
FROM LEO0000.implementer.Implementer I
WHERE I.ImplementerCode = 'LEO0001'

UPDATE S
SET S.SetupValue = 'NoReply@' + @cURLPrefix + '.projectkms.com'
WHERE S.SetupKey = 'NoReply'

UPDATE S
SET S.SetupValue = 'https://' + @cURLPrefix + '.projectkms.com'
WHERE S.SetupKey = 'SiteURL'
GO
--End file LEO000X - 04 - Data.sql

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.2 - 2017.01.27 17.02.08')
GO
--End build tracking

