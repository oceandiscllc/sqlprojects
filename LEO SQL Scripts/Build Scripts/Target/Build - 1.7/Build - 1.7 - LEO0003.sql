-- File Name:	LEO0003.sql
-- Build Key:	Build - 1.7 - 2017.05.29 20.27.33

USE LEO0003
GO

-- ==============================================================================================================================
-- Functions:
--		reporting.IsDraftReport
--
-- Procedures:
--		activity.GetActivityByActivityID
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		eventlog.LogAnnouncementAction
--		eventlog.LogCommunityAction
--		force.GetForceByForceID
--		force.GetForceUnitByForceUnitID
--		logicalframework.GetIntermediateOutcomeChartData
--		procurement.GetEquipmentCatalogByEquipmentCatalogID
--		product.GetProductByProductID
--		reporting.GetProductListByPersonID
--		reporting.GetSpotReportBySpotReportID
--		territory.AddCommunityProject
--		territory.GetCommunityByTerritoryID
--		territory.GetTerritoryByTerritoryID
--
-- Tables:
--		territory.TerritoryAnnex
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0003
GO

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE FU
SET 
	FU.CommanderFullName = contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle'),
	FU.DeputyCommanderFullName = contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.ForceUnit FU
GO	
--End table force.ForceUnit

--Begin table territory.TerritoryAnnex
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryAnnex'

EXEC utility.DropObject @TableName

CREATE TABLE territory.TerritoryAnnex
	(
	TerritoryAnnexID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryID INT,
	TerritoryStatusID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryAnnexID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryAnnex', 'ProjectID,TerritoryID'
GO
--End table territory.TerritoryAnnex

--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0003
GO

--Begin procedure reporting.IsDraftReport
EXEC utility.DropObject 'reporting.IsDraftReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.08
-- Description:	A function to determine if a report is in draft status
-- ===================================================================

CREATE FUNCTION reporting.IsDraftReport
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nIsDraft BIT = 1
	
	IF @EntityTypeCode = 'SpotReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)) = 1 THEN 1 ELSE 0 END 
	
		END
	ELSE IF @EntityTypeCode = 'WeeklyReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)) = 1 THEN 1 ELSE 0 END
	
		END
	--ENDIF
	
	RETURN @nIsDraft

END
GO
--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0003
GO

--Begin procedure activity.GetActivityByActivityID
EXEC Utility.DropObject 'activity.GetActivityByActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the activity.Activity table
-- ========================================================================
CREATE PROCEDURE activity.GetActivityByActivityID

@ActivityID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Activity', @ActivityID, @ProjectID)

	--Activity
	SELECT 
		A.ActivityID,
		A.ActivityName,
		A.ActivityTypeID,
		A.AwardeeSubContractorID1,
		A.AwardeeSubContractorID2,
		A.Background,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.FundingSourceID,
		A.Objectives,
		A.PointOfContactPersonID,
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		A.TaskCode,
		A.AwardeeSubContractorID1,
		(SELECT SC1.SubContractorName FROM dbo.SubContractor SC1 WHERE SC1.SubContractorID = A.AwardeeSubContractorID1) AS AwardeeSubContractorName1,
		A.AwardeeSubContractorID2,
		(SELECT SC2.SubContractorName FROM dbo.SubContractor SC2 WHERE SC2.SubContractorID = A.AwardeeSubContractorID2) AS AwardeeSubContractorName2,
		AT.ActivityTypeName,
		FS.FundingSourceName
	FROM activity.Activity A
		JOIN dropdown.ActivityType AT ON AT.ActivityTypeID = A.ActivityTypeID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = A.FundingSourceID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID

	--ActivityContact
	SELECT
		AC.VettingDate,
		core.FormatDate(AC.VettingDate) AS VettingDateFormatted,
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryNameFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM activity.ActivityContact AC
		JOIN contact.Contact C ON C.ContactID = AC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC.VettingOutcomeID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID

	--ActivityCourse
	SELECT
		AC.CourseID,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		M.ModuleName
	FROM activity.ActivityCourse AC
		JOIN course.Course C ON C.CourseID = AC.CourseID
		JOIN dbo.Module M on M.ModuleID = C.ModuleID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID
	ORDER BY M.ModuleName

	--ActivityTerritory
	SELECT
		AT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AT.TerritoryID) AS TerritoryName
	FROM activity.ActivityTerritory AT
	WHERE AT.ActivityID = @ActivityID
		AND AT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ActivityWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Activity', @ActivityID, @ProjectID

	--ActivityWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activity.Activity A ON A.ActivityID = EL.EntityID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'Activity'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--ActivityWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Activity', @ActivityID, @ProjectID, @nWorkflowStepNumber

END
GO
--End procedure activity.GetActivityByActivityID

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Announcement',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Announcement',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT
			T.ParentTerritoryID, T.ISOCountryCode2, T.TerritoryTypeCode, T.TerritoryName, T.Population, T.PopulationSource, T.TerritoryNameLocal, TA.*
		INTO #LogCommuniytActionTable
		FROM territory.Territory T
			JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
				AND T.TerritoryID = @EntityID
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Territory',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			C.*, 
			CAST(('<Territory>' + CAST(T.Location AS VARCHAR(MAX)) + '</Territory>') AS XML)
			FOR XML RAW('Territory'), ELEMENTS
			)
		FROM #LogCommuniytActionTable C
			JOIN territory.Territory T ON T.TerritoryID = C.TerritoryID
				AND T.TerritoryID = @EntityID

		DROP TABLE #LogCommuniytActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderContactID,
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		FU.ForceUnitID,
		FU.ProjectID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
			AND FU.ProjectID = @ProjectID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure logicalframework.GetIntermediateOutcomeChartData
EXEC utility.DropObject 'logicalframework.GetIntermediateOutcomeChartData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	A stored procedure to return intermediate outcome objective data
-- =============================================================================
CREATE PROCEDURE logicalframework.GetIntermediateOutcomeChartData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH Total AS
		(
		SELECT
			I.AchievedValue,
			I.IndicatorID,
			I.IndicatorName,
			I.InprogressValue,
			I.PlannedValue,
			I.TargetValue,
			I.PlannedValue + I.InprogressValue + I.AchievedValue AS TotalValue,
			LFS.LogicalFrameworkStatusName,
			O2.ObjectiveID,
			O2.ObjectiveName,
			O2.ProjectID,
			O2.StatusUpdateDescription
		FROM logicalframework.Indicator I
			JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
			JOIN person.PersonProject PP1 ON PP1.ProjectID = O1.ProjectID
				AND PP1.PersonID = @PersonID
			JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
			JOIN person.PersonProject PP2 ON PP2.ProjectID = O2.ProjectID
				AND PP2.PersonID = @PersonID
				AND O2.IsActive = 1
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O2.LogicalFrameworkStatusID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
				AND OT.ObjectiveTypeCode = 'Output'
				AND I.IsActive = 1
		)
	
	SELECT
		T.IndicatorID,
		T.IndicatorName,
		T.LogicalFrameworkStatusName,
		T.ObjectiveID,
		T.ObjectiveName,
		T.ProjectID,
		T.StatusUpdateDescription,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS AchievedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS InprogressValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST((CAST(T.TargetValue AS NUMERIC(18,2)) - CAST(T.AchievedValue AS NUMERIC(18,2)) - CAST(T.InprogressValue AS NUMERIC(18,2)) - CAST(T.PlannedValue AS NUMERIC(18,2))) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 0
		END AS IntendedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS PlannedValuePercent

	FROM Total T
	ORDER BY T.ObjectiveName, T.ObjectiveID, T.IndicatorName, T.IndicatorID

END
GO
--End procedure logicalframework.GetIntermediateOutcomeChartData

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date: 2017.01.01
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.CurrencyID,
		EC.EquipmentCatalogID, 
		EC.IsActive, 
		IIF(EC.IsActive = 1, 'Yes ', 'No ') AS IsActiveFormatted,
		EC.ItemName, 
		EC.Notes, 
		EC.ProjectID,
		dropdown.GetProjectNameByProjectID(EC.ProjectID) AS ProjectName,
		EC.UnitCost,
		EC.UnitOfIssue,
		CONCAT(C.ISOCurrencyCode, ' ', EC.UnitCost) AS UnitCostFormatted
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.Currency C ON C.CurrencyID = EC.CurrencyID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
--
-- Author:		Todd Pires
-- Create date:	2017.05.06
-- Description:	Added facebook url support
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFBPageID INT
	DECLARE @nFBPostID INT
	DECLARE @cFBPostName VARCHAR(MAX)

	SELECT
		@cFBPostName = FBPO.Name,
		@nFBPageID = FBPO.PageID,
		@nFBPostID = PD.FacebookPostID
	FROM integration.FacebookPost FBPO
		JOIN product.ProductDistribution PD ON PD.FacebookPostID = FBPO.ID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID

	--Product
	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.Quantity,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	--ProductCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN campaign.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ProductCommunicationTheme
	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProductDistribution
	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.FacebookPostID,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		@cFBPostName AS FacebookPostName,
		'https://www.facebook.com/' + CAST(ISNULL(@nFBPageID, 0) AS VARCHAR(20)) + '/posts/' + CAST(ISNULL(@nFBPostID, 0) AS VARCHAR(20)) AS FacebookPostURL
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	--ProductDistributionCount
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID
		AND PD.ProjectID = @ProjectID

	--ProductRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM product.ProductRelevantTheme PRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = PRT.RelevantThemeID
			AND PRT.ProductID = @ProductID
			AND PRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ProductTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
		AND PT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure reporting.GetProductListByPersonID
EXEC utility.DropObject 'reporting.GetProductListByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the product.Product table matching the reporting.SearchResults items
-- =====================================================================================================================
CREATE PROCEDURE reporting.GetProductListByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ProductID INT NOT NULL PRIMARY KEY,
		CommentCount INT NOT NULL DEFAULT 0,
		LikeCount INT NOT NULL DEFAULT 0,
		OrganicReachTotal INT NOT NULL DEFAULT 0,
		ShareCount INT NOT NULL DEFAULT 0,
		TotalReach INT NOT NULL DEFAULT 0,
		ViewCount INT NOT NULL DEFAULT 0
		)

	INSERT INTO @tTable (ProductID) SELECT SR.EntityID FROM reporting.SearchResult SR WHERE SR.EntityTypeCode = 'Product' AND SR.PersonID = @PersonID

	UPDATE T
	SET
		T.CommentCount = D.CommentCount,
		T.LikeCount = D.LikeCount,
		T.OrganicReachTotal = D.OrganicReach,
		T.ShareCount = D.ShareCount,
		T.TotalReach = D.TotalReach,
		T.ViewCount = D.ViewCount
	FROM @tTable T
		JOIN 
			(
			SELECT
				PD.ProductID,
				ISNULL(SUM(PD.CommentCount), 0)AS CommentCount,
				ISNULL(SUM(PD.LikeCount), 0) AS LikeCount,
				ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReach,
				ISNULL(SUM(PD.ShareCount), 0) AS ShareCount,
				ISNULL(SUM(PD.TotalReach), 0) AS TotalReach,
				ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
			FROM product.ProductDistribution PD
				JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Product'
					AND SR.EntityID = PD.ProductID
					AND SR.PersonID = @PersonID
			GROUP BY PD.ProductID
			) D ON D.ProductID = T.ProductID

	SELECT
		P.ProductCode,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PRJ.ProjectName,
		PS.ProductStatusName,
		PT.ProductTypeName,
		TA.TargetAudienceName,
		T.CommentCount,
		T.LikeCount,
		T.OrganicReachTotal,
		T.ShareCount,
		T.TotalReach,
		T.ViewCount
	FROM product.Product P
		JOIN @tTable T ON T.ProductID = P.ProductID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.Project PRJ ON PRJ.ProjectID = P.ProjectID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
	ORDER BY P.ProductName, P.ProductID

END
GO
--End procedure reporting.GetProductListByPersonID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID, @ProjectID ) AS WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID, @ProjectID) AS IsDraft,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		SR.SummaryMap AS GMap,
		dbo.GetSpotReportTerritoriesList(SR.SpotReportID) AS TerritoryList	
	FROM spotReport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure reporting.GetSpotReportBySpotReportID

--Begin procedure territory.AddCommunityProject
EXEC utility.DropObject 'territory.AddCommunityProject'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to associate a community to a project
-- =====================================================================
CREATE PROCEDURE territory.AddCommunityProject

@ImplemnterCode VARCHAR(50),
@TerritoryID INT,
@ProjectID INT,
@TerritoryStatusID INT,
@Notes VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE implementer.ImplementerDropdownData IDD
	USING (SELECT @ImplemnterCode AS ImplementerCode, 'Territory' AS DropdownCode, @ProjectID AS ProjectID, @TerritoryID AS DropdownID) T2
		ON T2.ImplementerCode = IDD.ImplementerCode
			AND T2.DropdownCode = IDD.DropdownCode
			AND T2.ProjectID = IDD.ProjectID
			AND T2.DropdownID = IDD.DropdownID
	WHEN MATCHED THEN UPDATE 
	SET 
		IDD.IsActive = 1
	WHEN NOT MATCHED THEN
	INSERT 
		(ImplementerCode, DropdownCode, ProjectID, DropdownID)
	VALUES
		(
		T2.ImplementerCode,
		T2.DropdownCode,
		T2.ProjectID,
		T2.DropdownID
		);

	MERGE territory.TerritoryAnnex TA
	USING (SELECT @TerritoryID AS TerritoryID, @ProjectID AS ProjectID, @TerritoryStatusID AS TerritoryStatusID, @Notes AS Notes) T2
		ON T2.TerritoryID = TA.TerritoryID
			AND T2.ProjectID = TA.ProjectID
	WHEN MATCHED THEN UPDATE 
	SET 
		TA.Notes = T2.Notes,
		TA.TerritoryStatusID = T2.TerritoryStatusID
	WHEN NOT MATCHED THEN
	INSERT 
		(TerritoryID, ProjectID, TerritoryStatusID, Notes) 
	VALUES 
		(
		T2.TerritoryID, 
		T2.ProjectID, 
		T2.TerritoryStatusID, 
		T2.Notes
		);

END
GO
--End procedure territory.AddCommunityProject

--Begin procedure territory.GetCommunityByTerritoryID
EXEC utility.DropObject 'territory.GetCommunityByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to get data from the LEO0000.territory.Territory and LEO000X.territory.TerritoryAnnex tables
-- ============================================================================================================================
CREATE PROCEDURE territory.GetCommunityByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT 
		T.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryNameFormatted,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Location.STAsText() AS Location,
		T.TerritoryName,
		T.TerritoryNameLocal,
		T.Population, 
		T.PopulationSource
	FROM territory.Territory T
	WHERE T.TerritoryID = @TerritoryID

	--TerritoryAnnex
	SELECT 
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.Notes,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName
	FROM territory.TerritoryAnnex TA
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
			AND TA.TerritoryID = @TerritoryID
			AND TA.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.TerritoryID = @TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
			AND C.TerritoryID = @TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		AOT.AreaOfOperationTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.TerritoryID = @TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
		WHERE IT.ImpactStoryID = I.ImpactStoryID
			AND IT.TerritoryID = @TerritoryID
			AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND I.TerritoryID = @TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.TerritoryID = @TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
				WHERE T.TerritoryID = @TerritoryID
					AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetCommunityByTerritoryID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
-- =====================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		AOT.AreaOfOperationTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0003
GO

UPDATE T SET T.TerritoryID = 0 FROM activityreport.ActivityReportTerritory T
UPDATE T SET T.TerritoryID = 0 FROM activity.ActivityTerritory T
UPDATE T SET T.TerritoryID = 0 FROM asset.Asset T
UPDATE T SET T.TerritoryID = 0 FROM atmospheric.AtmosphericTerritory T
UPDATE T SET T.TerritoryID = 0 FROM contact.Contact T
UPDATE T SET T.TerritoryID = 0 FROM course.Course T
UPDATE T SET T.TerritoryID = 0 FROM distributor.DistributorTerritory T
UPDATE T SET T.TerritoryID = 0 FROM finding.FindingTerritory T
UPDATE T SET T.TerritoryID = 0 FROM force.Force T
UPDATE T SET T.TerritoryID = 0 FROM force.ForceUnit T
UPDATE T SET T.TerritoryID = 0 FROM impactstory.ImpactStoryTerritory T
UPDATE T SET T.TerritoryID = 0 FROM dbo.Incident T
UPDATE T SET T.TerritoryID = 0 FROM mediareport.MediaReport T
UPDATE T SET T.TerritoryID = 0 FROM product.ProductTerritory T
UPDATE T SET T.TerritoryID = 0 FROM recommendation.RecommendationTerritory T
UPDATE T SET T.TerritoryID = 0 FROM dbo.RequestForInformation T
UPDATE T SET T.TerritoryID = 0 FROM spotreport.SpotReportTerritory T
UPDATE T SET T.TerritoryID = 0 FROM trendreport.TrendReportTerritory T
--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.7 - 2017.05.29 20.27.33'
GO
--End build tracking

