-- File Name:	LEO0000.sql
-- Build Key:	Build - 1.9 - 2017.10.18 18.07.38

USE LEO0000
GO

-- ==============================================================================================================================
-- ==============================================================================================================================

--Begin file LEO0000 - 01 - Tables.sql
USE LEO0000
GO


--End file LEO0000 - 01 - Tables.sql

--Begin file LEO0000 - 02 - Functions.sql
USE LEO0000
GO


--End file LEO0000 - 02 - Functions.sql

--Begin file LEO0000 - 03 - Procedures.sql
USE LEO0000
GO


--End file LEO0000 - 03 - Procedures.sql

--Begin file LEO0000 - 04 - Data.sql
USE LEO0000
GO


--End file LEO0000 - 04 - Data.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.9 - 2017.10.18 18.07.38')
GO
--End build tracking

