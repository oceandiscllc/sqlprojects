-- File Name:	LEO0003.sql
-- Build Key:	Build - 1.9 - 2017.10.18 18.07.38

USE LEO0003
GO

-- ==============================================================================================================================
-- Functions:
--		person.HashPassword
--
-- Procedures:
--		person.GeneratePassword
--		person.ValidateLogin
--		workflow.InitializeEntityWorkflow
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0003
GO


--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0003
GO

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0003
GO

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(core.GetSystemSetupValueBySetupKey('PasswordDuration', 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@ImplementerCode VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		DefaultProjectID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDefaultProjectID = P.DefaultProjectID,
		@cPhone = P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,DefaultProjectID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@nDefaultProjectID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN

			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET IsPasswordExpired = 1
			WHERE PersonID = @nPersonID

			END
			ELSE
				BEGIN

				UPDATE person.Person
				SET InvalidLoginAttempts = 0
				WHERE PersonID = @nPersonID
	
		END
	--ENDIF	
		
			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure workflow.InitializeEntityWorkflow
EXEC utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0003
GO


--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.9 - 2017.10.18 18.07.38')
GO
--End build tracking

