-- File Name:	LEO0002.sql
-- Build Key:	Build - 1.6 - 2017.03.26 15.40.09

USE LEO0002
GO

-- ==============================================================================================================================
-- Procedures:
--		activityreport.GetActivityReportByActivityReportID
--		campaign.GetCampaignByCampaignID
--		eventlog.LogCampaignAction
--		eventlog.LogSpotReportAction
--		force.GetForceByForceID
--		impactstory.GetImpactStoryByImpactStoryID
--		person.GetNewsFeed
--		portalupdate.UpdateDataBySchemaName
--		portalupdate.UpdateDataBySchemaNameAndTableName
--		product.GetProductByProductID
--		spotreport.GetSpotReportBySpotReportID
--		trendreport.GetTrendReportByTrendReportID
--		workflow.ResetEntityWorkflowStepGroupPerson
--
-- Schemas:
--		campaign
--		deprecated
--
-- Tables:
--		activityreport.ActivityReportRelevantTheme
--		campaign.CampaignRelevantTheme
--		impactstory.ImpactStoryRelevantTheme
--		product.ProductRelevantTheme
--		trendreport.TrendReportRelevantTheme
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0002
GO

--Begin schema campaign
EXEC utility.AddSchema 'campaign'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'dbo' AND T.Name = 'Campaign')
	ALTER SCHEMA campaign TRANSFER dbo.Campaign
--ENDIF
GO
--End schema campaign

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName
GO
--End table dropdown.IndicatorType

--Begin table mediareport.MediaReport
ALTER TABLE mediareport.MediaReport ALTER COLUMN MediaReportTitle NVARCHAR(250)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Summary NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Comments NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Implications NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN RiskMitigation NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN CommunicationOpportunities NVARCHAR(MAX)
GO
--End table mediareport.MediaReport

--Begin table activityreport.ActivityReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportRelevantTheme
	(
	ActivityReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportRelevantTheme', 'ActivityReportID,RelevantThemeID'
GO
--End table activityreport.ActivityReportRelevantTheme

--Begin table campaign.CampaignRelevantTheme
DECLARE @TableName VARCHAR(250) = 'campaign.CampaignRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE campaign.CampaignRelevantTheme
	(
	CampaignRelevantThemeID INT NOT NULL IDENTITY(1,1),
	CampaignID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CampaignID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CampaignRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_CampaignRelevantTheme', 'CampaignID,RelevantThemeID'
GO
--End table campaign.CampaignRelevantTheme

--Begin table impactstory.ImpactStoryRelevantTheme
DECLARE @TableName VARCHAR(250) = 'impactstory.ImpactStoryRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE impactstory.ImpactStoryRelevantTheme
	(
	ImpactStoryRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ImpactStoryID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ImpactStoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ImpactStoryRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ImpactStoryRelevantTheme', 'ImpactStoryID,RelevantThemeID'
GO
--End table impactstory.ImpactStoryRelevantTheme

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.AddColumn @TableName, 'Quantity', 'INT'
GO
--End table product.Product

--Begin table product.ProductRelevantTheme
DECLARE @TableName VARCHAR(250) = 'product.ProductRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE product.ProductRelevantTheme
	(
	ProductRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ProductID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProductRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProductRelevantTheme', 'ProductID,RelevantThemeID'
GO
--End table product.ProductRelevantTheme

--Begin table spotreport.SpotReport
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReport'

EXEC utility.AddColumn @TableName, 'SummaryMap', 'VARBINARY(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryMapZoom', 'INT', '0'
GO
--End table spotreport.SpotReport

--Begin table trendreport.TrendReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportRelevantTheme
	(
	TrendReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	TrendReportID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportRelevantTheme', 'TrendReportID,RelevantThemeID'
GO
--End table trendreport.TrendReportRelevantTheme
--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0002
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0002
GO

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID, @ProjectID)

	--ActivityReport
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail,
		ART.ActivityReportTypeID,
		ART.ActivityReportTypeName
	FROM activityreport.ActivityReport AR
		JOIN dropdown.ActivityReportType ART ON ART.ActivityReportTypeID = AR.ActivityReportTypeID
			AND AR.ActivityReportID = @ActivityReportID
		AND AR.ProjectID = @ProjectID

	--ActivityReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY A.AssetName, A.AssetID

	--ActivityReportAtmospheric
	SELECT
		A.AtmosphericID,
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted,
		ATT.AtmosphericTypeName
	FROM activityreport.ActivityReportAtmospheric ARA
		JOIN atmospheric.Atmospheric A ON A.AtmosphericID = ARA.AtmosphericID
		JOIN dropdown.AtmosphericType ATT ON ATT.AtmosphericTypeID = A.AtmosphericTypeID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY ATT.AtmosphericTypeName, A.AtmosphericDate

	--ActivityReportCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM activityreport.ActivityReportCampaign ARC
		JOIN campaign.Campaign C ON C.CampaignID = ARC.CampaignID
			AND ARC.ActivityReportID = @ActivityReportID
			AND ARC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ActivityReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
			AND ARF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

	--ActivityReportImpactStory
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM activityreport.ActivityReportImpactStory ARI
		JOIN impactstory.ImpactStory I ON I.ImpactStoryID = ARI.ImpactStoryID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.ImpactStoryName, I.ImpactStoryID

	--ActivityReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN dbo.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	--ActivityReportMediaReport
	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM activityreport.ActivityReportMediaReport ARMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = ARMR.MediaReportID
			AND ARMR.ActivityReportID = @ActivityReportID
			AND ARMR.ProjectID = @ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	--ActivityReportProduct
	SELECT
		P.ProductID,
		P.ProductName
	FROM activityreport.ActivityReportProduct ARP
		JOIN product.Product P ON P.ProductID = ARP.ProductID
			AND ARP.ActivityReportID = @ActivityReportID
			AND ARP.ProjectID = @ProjectID
	ORDER BY P.ProductName, P.ProductID

	--ActivityReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM activityreport.ActivityReportRelevantTheme ARRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = ARRT.RelevantThemeID
			AND ARRT.ActivityReportID = @ActivityReportID
			AND ARRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ActivityReportTerritory
	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
			AND ART.ActivityReportID = @ActivityReportID
			AND ART.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ActivityReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'ActivityReport', @ActivityReportID, @ProjectID

	--ActivityReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activityreport.ActivityReport AR ON AR.ActivityReportID = EL.EntityID
			AND AR.ActivityReportID = @ActivityReportID
			AND AR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'ActivityReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--ActivityReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ActivityReport', @ActivityReportID, @ProjectID, @nWorkflowStepNumber

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure campaign.GetCampaignByCampaignID
EXEC utility.DropObject 'dbo.GetCampaignByCampaignID'
EXEC utility.DropObject 'campaign.GetCampaignByCampaignID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the campaign.Campaign table
-- ========================================================================
CREATE PROCEDURE campaign.GetCampaignByCampaignID

@CampaignID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Campaign
	SELECT 
		C.CampaignDescription,
		C.CampaignID,
		C.CampaignName,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.IsActive,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted
	FROM campaign.Campaign C
	WHERE C.CampaignID = @CampaignID
		AND C.ProjectID = @ProjectID

	--CampaignRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM campaign.CampaignRelevantTheme CRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = CRT.RelevantThemeID
			AND CRT.CampaignID = @CampaignID
			AND CRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

END
GO
--End procedure campaign.GetCampaignByCampaignID

--Begin procedure document.DeleteDocumentByDocumentID
EXEC Utility.DropObject 'document.DeleteDocumentByDocumentID'
GO
--End procedure document.DeleteDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure dropdown.GetIndicatorTypeData
EXEC utility.DropObject 'dropdown.GetIndicatorTypeData'
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure eventlog.LogCampaignAction
EXEC utility.DropObject 'eventlog.LogCampaignAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCampaignAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			T.CampaignID,
			@Comments,
			@ProjectID
		FROM campaign.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Campaign'), ELEMENTS
			)
		FROM campaign.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCampaignAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM spotreport.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportTerritories = COALESCE(@cSpotReportTerritories, '') + D.SpotReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportTerritory'), ELEMENTS) AS SpotReportTerritory
			FROM spotreport.SpotReportTerritory T 
			WHERE T.SpotReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogSpotReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogSpotReportActionTable
		--ENDIF

		SELECT T.*
		INTO #LogSpotReportActionTable
		FROM spotreport.SpotReport T
		WHERE T.SpotReportID = @EntityID
		
		ALTER TABLE #LogSpotReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<SummaryMap>' + CAST(SR.SpotReportID AS VARCHAR(MAX)) + '</SummaryMap>') AS XML), 
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportTerritories>' + ISNULL(@cSpotReportTerritories, '') + '</SpotReportTerritories>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM #LogSpotReportActionTable T
			JOIN spotreport.SpotReport SR ON SR.SpotReportID = T.SpotReportID
				AND T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		FU.CommanderContactID, 
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderContactNameFormatted,
		FU.DeputyCommanderContactID,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderContactNameFormatted,
		FU.ForceUnitID,
		FU.ProjectID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure impactstory.GetImpactStoryByImpactStoryID
EXEC utility.DropObject 'impactstory.GetImpactStoryByImpactStoryID'
GO

-- =======================================================================================
-- Author:			Kevin Ross
-- Create date: 2017.01.01
-- Description:	A stored procedure to add data to the impactstory.ImpactStory table
-- =======================================================================================
CREATE PROCEDURE impactstory.GetImpactStoryByImpactStoryID

@ImpactStoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ImpactStory
	SELECT 
		I.ImpactStoryID,
		I.ImpactStoryName,
		I.ImpactStoryDescription,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName
	FROM impactstory.ImpactStory I
		WHERE I.ImpactStoryID = @ImpactStoryID
			AND I.ProjectID = @ProjectID

	--ImpactStoryCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM impactstory.ImpactStoryCampaign IC
		JOIN campaign.Campaign C ON C.CampaignID = IC.CampaignID
			AND IC.ImpactStoryID = @ImpactStoryID
			AND IC.ProjectID = @ProjectID
			AND C.ProjectID = IC.ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ImpactStoryProduct
	SELECT
		P.ProductID,
		P.ProductName
	FROM impactstory.ImpactStoryProduct IP
		JOIN product.Product P ON P.ProductID = IP.ProductID
			AND IP.ImpactStoryID = @ImpactStoryID
			AND IP.ProjectID = @ProjectID
			AND P.ProjectID = IP.ProjectID
	ORDER BY P.ProductName, P.ProductID

	--ImpactStoryRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM impactstory.ImpactStoryRelevantTheme ISRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = ISRT.RelevantThemeID
			AND ISRT.ImpactStoryID = @ImpactStoryID
			AND ISRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ImpactStoryTerritory
	SELECT
		IT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(IT.TerritoryID) AS TerritoryNameFormatted
	FROM impactstory.ImpactStoryTerritory IT
	WHERE IT.ImpactStoryID = @ImpactStoryID
		AND IT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure impactstory.GetImpactStoryByImpactStoryID

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		'fa fa-fw fa-tasks' AS Icon,
		AR.ActivityReportID AS EntityID,
		AR.ActivityReportTitle AS Title,
		AR.UpdateDateTime,
		core.FormatDate(AR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM activityreport.ActivityReport AR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ActivityReport'
		JOIN dropdown.Project P ON P.ProjectID = AR.ProjectID
			AND AR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = AR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-exclamation-circle' AS Icon,
		I.ImpactStoryID AS EntityID,
		I.ImpactStoryName AS Title,
		I.UpdateDateTime,
		core.FormatDate(I.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM impactstory.ImpactStory I
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ImpactStory'
		JOIN dropdown.Project P ON P.ProjectID = I.ProjectID
			AND I.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = I.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM dbo.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed

--Begin procedure portalupdate.UpdateDataBySchemaName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data for tables in a specific schema
-- =====================================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaName

@SchemaName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTableName VARCHAR(50)

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

	IF @PrintSQL = 0
		CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)
	ELSE
		print 'CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)'
	--ENDIF

	SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @SchemaName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @SchemaName + ' T WHERE 1 = 1'
	IF @PrimaryKey > 0
		SET @cSQL += ' AND T.' + @SchemaName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
	--ENDIF

	IF EXISTS (SELECT 1 FROM core.EntityType ET WHERE ET.EntityTypeCode = @SchemaName AND ET.HasWorkflow = 1)
		SET @cSQL += ' AND workflow.GetWorkflowStepNumber(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID) > workflow.GetWorkflowStepCount(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID)'
	--ENDIF

	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = @SchemaName
		ORDER BY 1

	OPEN oCursor
	FETCH oCursor INTO @cTableName
	WHILE @@fetch_status = 0
		BEGIN	

		SET @cSQL = 'DELETE T1 FROM LEO0001.' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', ')
		SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', T1.')

		SET @cSQL = 'INSERT INTO LEO0001.' + @SchemaName + '.' + @cTableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		FETCH oCursor INTO @cTableName

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

END
GO
--End procedure portalupdate.UpdateDataBySchemaName

--Begin procedure portalupdate.UpdateDataBySchemaNameAndTableName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaNameAndTableName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data in a specific table
-- =========================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaNameAndTableName

@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cSQL VARCHAR(MAX)

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

	IF @PrintSQL = 0
		CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)
	ELSE
		print 'CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)'
	--ENDIF

	SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @TableName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @TableName + ' T WHERE 1 = 1'
	IF @PrimaryKey > 0
		SET @cSQL += ' AND T.' + @TableName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
	--ENDIF
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	SET @cSQL = 'DELETE T1 FROM LEO0001.' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @TableName, ', ')
	SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @TableName, ', T1.')

	SET @cSQL = 'INSERT INTO LEO0001.' + @SchemaName + '.' + @TableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

END
GO
--End procedure portalupdate.UpdateDataBySchemaNameAndTableName

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Product
	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.Quantity,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	--ProductCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN campaign.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ProductCommunicationTheme
	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProductDistribution
	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	--ProductDistributionCount
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID
		AND PD.ProjectID = @ProjectID

	--ProductRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM product.ProductRelevantTheme PRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = PRT.RelevantThemeID
			AND PRT.ProductID = @ProductID
			AND PRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ProductTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
		AND PT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID, @ProjectID)
	
	SELECT
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunicationOpportunities,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SourceDetails,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom
	FROM spotreport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
			AND SRI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
		AND SRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure trendreport.GetTrendReportByTrendReportID
EXEC utility.DropObject 'trendreport.GetTrendReportByTrendReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the trendreport.TrendReport table
-- ==================================================================================
CREATE PROCEDURE trendreport.GetTrendReportByTrendReportID

@TrendReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReport', @TrendReportID, @ProjectID)

	--TrendReport
	SELECT
		TR.ProjectID,
		dropdown.GetProjectNameByProjectID(TR.ProjectID) AS ProjectName,
		TR.TrendReportID,
		TR.TrendReportTitle,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Summary,
		TR.ReportDetail,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("TR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		TR.SummaryMapZoom
	FROM trendreport.TrendReport TR
	WHERE TR.TrendReportID = @TrendReportID
		AND TR.ProjectID = @ProjectID

	--TrendReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM trendreport.TrendReportAsset TRA
		JOIN asset.Asset A ON A.AssetID = TRA.AssetID
			AND A.ProjectID = @ProjectID
			AND TRA.TrendReportID = @TrendReportID
			AND TRA.ProjectID = A.ProjectID
	ORDER BY A.AssetName, A.AssetID

	--TrendReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM trendreport.TrendReportForce TRF
		JOIN force.Force F ON F.ForceID = TRF.ForceID
			AND F.ProjectID = @ProjectID
			AND TRF.TrendReportID = @TrendReportID
			AND TRF.ProjectID = F.ProjectID
	ORDER BY F.ForceName, F.ForceID

	--TrendReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM trendreport.TrendReportIncident TRI
		JOIN dbo.Incident I ON I.IncidentID = TRI.IncidentID
			AND I.ProjectID = @ProjectID
			AND TRI.TrendReportID = @TrendReportID
			AND TRI.ProjectID = I.ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	--TrendReportMediaReport
	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM trendreport.TrendReportMediaReport TRMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = TRMR.MediaReportID
			AND MR.ProjectID = @ProjectID
			AND TRMR.TrendReportID = @TrendReportID
			AND TRMR.ProjectID = MR.ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	--TrendReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM trendreport.TrendReportRelevantTheme TRRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = TRRT.RelevantThemeID
			AND TRRT.TrendReportID = @TrendReportID
			AND TRRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--TrendReportTerritory
	SELECT
		TRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(TRT.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM trendreport.TrendReportTerritory TRT
		JOIN territory.Territory T ON T.TerritoryID = TRT.TerritoryID
			AND TRT.TrendReportID = @TrendReportID
			AND TRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--TrendReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'TrendReport', @TrendReportID, @ProjectID

	--TrendReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN trendreport.TrendReport TR ON TR.TrendReportID = EL.EntityID
			AND TR.TrendReportID = @TrendReportID
			AND TR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'TrendReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--TrendReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'TrendReport', @TrendReportID, @ProjectID, @nWorkflowStepNumber

END
GO
--End procedure trendreport.GetTrendReportByTrendReportID

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC Utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		PersonID INT,
		ProjectID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,
			W.ProjectID,
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, ProjectID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		E.PersonID,
		E.ProjectID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.ProjectID, 
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.ProjectID, 
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID,
				WSGP.ProjectID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
				AND WSGP.ProjectID = D.ProjectID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, ProjectID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.PersonID, 
		T.ProjectID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0002
GO

--Begin table document.Document
IF 'LEO0002' <> 'LEO0001'
	BEGIN

	INSERT INTO LEO0000.document.Document
		(ImplementerID, DocumentDate, DocumentDescription, DocumentName, DocumentTitle, DocumentTypeID, ProjectID, Extension, CreatePersonID, ContentType, ContentSubtype, PhysicalFileSize, DocumentData, Thumbnail)
	SELECT
		D.DocumentID, 
		D.DocumentDate, 
		D.DocumentDescription, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.ProjectID, 
		D.Extension, 
		D.CreatePersonID, 
		D.ContentType, 
		D.ContentSubtype, 
		D.PhysicalFileSize, 
		D.DocumentData, 
		D.Thumbnail
	FROM document.Document D

	UPDATE DE
	SET DE.DocumentID = LEOD.DocumentID
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN LEO0000.document.Document LEOD ON LEOD.ImplementerID = D.DocumentID
			AND D.ProjectID = LEOD.ProjectID

	END
--ENDIF
GO

EXEC utility.AddSchema 'deprecated'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'document' AND T.Name = 'Document')
	BEGIN

	EXEC utility.DropFullTextIndex 'document.Document';
	ALTER SCHEMA deprecated TRANSFER document.Document;

	END
--ENDIF
GO

--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.6 - 2017.03.26 15.40.09'
GO
--End build tracking

