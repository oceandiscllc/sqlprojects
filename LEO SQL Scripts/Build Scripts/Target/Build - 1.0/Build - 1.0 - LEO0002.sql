-- File Name:	Build - 1.0 - LEO0002.sql
-- Build Key:	Build - 1.0 - 2017.01.22 21.35.47

USE [LEO0002]
GO

-- ==============================================================================================================================
-- Functions:
--		core.GetImplementerSetupValueBySetupKey
--		document.FormatFullTextSearchString
--		person.CanAccessProject
--		spotreport.FormatStaticGoogleMapForSpotReport
--		workflow.CanHaveAddUpdate
--		workflow.GetEntityWorkflowData
--
-- Procedures:
--		activity.GetActivityByActivityID
--		asset.GetAssetByAssetID
--		atmospheric.GetAtmosphericByAtmosphericID
--		contact.GetContactByContactID
--		contact.GetSubContractorBySubContractorID
--		contact.GetSubContractors
--		core.EntityTypeAddUpdate
--		core.GetEmailTemplateByEmailTemplateID
--		core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode
--		core.GetEmailTemplateFieldsByEntityTypeCode
--		core.GetImplementerSetupByImplementerSetupID
--		core.GetImplementerSetupValuesBySetupKey
--		core.GetMenuItemsByPersonID
--		core.ImplementerSetupAddUpdate
--		dbo.GetDocumentByDocumentID
--		dbo.GetFieldReportByFieldReportID
--		dbo.GetIncidentByIncidentID
--		dbo.GetRequestForInformationByRequestForInformationID
--		document.GetDocumentByDocumentData
--		document.GetDocumentByDocumentID
--		document.GetDocumentByDocumentName
--		document.GetEntityDocuments
--		document.PurgeEntityDocuments
--		document.SaveEntityDocuments
--		dropdown.GetIndicatorTypeData
--		dropdown.GetSubContractorCapabilityData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogImplementerSetupAction
--		eventlog.LogSubContractorAction
--		finding.GetFindingByFindingID
--		force.GetForceByForceID
--		force.GetForceUnitByForceUnitID
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetMilestoneByMilestoneID
--		logicalframework.GetMilestoneDataByIndicatorID
--		logicalframework.GetObjectiveByObjectiveID
--		mediareport.GetMediaReportByMediaReportID
--		permissionable.DeletePermissionable
--		permissionable.GetPermissionables
--		person.GetMenuItemsByPersonID
--		person.GetProjectsByPersonID
--		person.ValidateLogin
--		procurement.GetEquipmentInventoryByEquipmentInventoryID
--		productdistributor.GetCampaignByCampaignID
--		productdistributor.GetDistributorByDistributorID
--		productdistributor.GetProductByProductID
--		programreport.GetProgramReportByProgramReportID
--		recommendation.GetRecommendationByRecommendationID
--		reporting.GetEquipmentInventoryListByPersonID
--		reporting.GetMediaReports
--		reporting.GetProductListByPersonID
--		reporting.GetSpotReportBySpotReportID
--		spotreport.GetSpotReportBySpotReportID
--		territory.GetTerritoryByTerritoryID
--		training.GetCourseByCourseID
--		training.GetModuleByModuleID
--		training.GetModules
--		utility.DropFullTextIndex
--		utility.UpdateSuperAdministratorPersonPermissionables
--		workflow.GetWorkflowByWorkflowID
--
-- Schemas:
--		core
--		document
--		implementer
--
-- Tables:
--		document.Document
--		document.DocumentEntity
--		dropdown.IndicatorType
--		dropdown.SubContractorCapability
-- ==============================================================================================================================

--Begin file Client - 00 - Prerequisites.sql
--Begin schemas
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'document'
EXEC utility.AddSchema 'implementer'
GO
--End schemas

--Begin create synonyms
DECLARE @cSQLText1 VARCHAR(MAX)
DECLARE @cSQLText2 VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		S.BaseObjectSchema + '.' + S.BaseObjectName  AS SQLText1,
		'CREATE SYNONYM ' + S.BaseObjectSchema + '.' + S.BaseObjectName + ' FOR LEO0000.' + S.BaseObjectSchema + '.' + S.BaseObjectName AS SQLText2
	FROM LEO0000.implementer.Synonym S
	ORDER BY S.BaseObjectSchema, S.BaseObjectName

OPEN oCursor
FETCH oCursor INTO @cSQLText1, @cSQLText2
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC utility.DropObject @cSQLText1
	EXEC (@cSQLText2)

	FETCH oCursor INTO @cSQLText1, @cSQLText2
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor

GO
--End create synonyms

--Begin procedure core.ImplementerSetupAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.ImplementerSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer setup key records
-- =============================================================================
CREATE PROCEDURE core.ImplementerSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM core.ImplementerSetup S WHERE S.SetupKey = @SetupKey)
		BEGIN

		INSERT INTO core.ImplementerSetup
			(SetupKey,SetupValue)
		VALUES
			(@SetupKey,@SetupValue)

		END
	ELSE
		BEGIN

		UPDATE core.ImplementerSetup
		SET SetupValue = @SetupValue
		WHERE SetupKey = @SetupKey

		END
	--ENDIF
	
END
GO
--End procedure core.ImplementerSetupAddUpdate

--Begin procedure utility.DropFullTextIndex
EXEC utility.DropObject 'utility.DropFullTextIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropFullTextIndex

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT * FROM sys.fulltext_indexes FTI WHERE FTI.object_id = OBJECT_ID(@TableName))
		BEGIN

		SET @cSQL = 'ALTER FULLTEXT INDEX ON ' + @TableName + ' DISABLE'
		EXEC (@cSQL)

		SET @cSQL = 'DROP FULLTEXT INDEX ON ' + @TableName
		EXEC (@cSQL)

		END
	--ENDIF
	
END
GO
--End procedure utility.DropFullTextIndex

IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'DocumentSearchCatalog')
	CREATE FULLTEXT CATALOG DocumentSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO

--End file Client - 00 - Prerequisites.sql

--Begin file Client - 01 - Tables.sql

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentName VARCHAR(50),
	DocumentTitle NVARCHAR(250),
	DocumentTypeID INT,
	ProjectID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	PhysicalFileSize BIGINT,
	DocumentData VARBINARY(MAX),
	Thumbnail VARBINARY(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET D.Extension = ISNULL(@cFileExtenstion, @cExtenstion)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		

utility.DropFullTextIndex 'document.Document'

CREATE FULLTEXT INDEX ON document.Document 
	(
	DocumentData TYPE COLUMN Extension LANGUAGE ENGLISH,
	DocumentDescription LANGUAGE ENGLISH,
	DocumentName LANGUAGE ENGLISH,
	DocumentTitle LANGUAGE ENGLISH
	) KEY INDEX PK_Document ON DocumentSearchCatalog
GO
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentEntity
	(
	DocumentEntityID INT IDENTITY(1,1) NOT NULL,
	DocumentID INT,
	EntityTypeCode VARCHAR(50),
	EntityTypeSubCode VARCHAR(50),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'
GO
--End table document.DocumentEntity

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorType
	(
	IndicatorTypeID INT IDENTITY(0,1) NOT NULL,
	IndicatorTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IndicatorTypeName', 'DisplayOrder,IndicatorTypeName', 'IndicatorTypeID'
GO

SET IDENTITY_INSERT dropdown.IndicatorType ON
GO

INSERT INTO dropdown.IndicatorType (IndicatorTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.IndicatorType OFF
GO
--End table dropdown.IndicatorType

--Begin table dropdown.SubContractorCapability
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractorCapability'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubContractorCapability
	(
	SubContractorCapabilityID INT IDENTITY(0,1) NOT NULL,
	SubContractorCapabilityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SubContractorCapabilityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SubContractorCapabilityName', 'DisplayOrder,SubContractorCapabilityName', 'SubContractorCapabilityID'
GO

SET IDENTITY_INSERT dropdown.SubContractorCapability ON
GO

INSERT INTO dropdown.SubContractorCapability (SubContractorCapabilityID) VALUES (0)

SET IDENTITY_INSERT dropdown.SubContractorCapability OFF
GO
--End table dropdown.SubContractorCapability

--End file Client - 01 - Tables.sql

--Begin file Client - 02 - Functions.sql
--Begin function core.GetImplementerSetupValueBySetupKey
EXEC utility.DropObject 'core.GetImplementerSetupValueBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a SetupValue for a SetupKey from the core.ImplementerSetup table
-- ==================================================================================================
CREATE FUNCTION core.GetImplementerSetupValueBySetupKey
(
@SetupKey VARCHAR(250),
@DefaultSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSetupValue VARCHAR(MAX)
	
	SELECT @cSetupValue = S.SetupValue 
	FROM core.ImplementerSetup S
	WHERE S.SetupKey = @SetupKey
	
	RETURN ISNULL(@cSetupValue, @DefaultSetupValue)

END
GO
--End function core.GetImplementerSetupValueBySetupKey

--Begin function document.FormatFullTextSearchString
EXEC utility.DropObject 'document.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION document.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10)
)

RETURNS VARCHAR(2000)

AS
BEGIN

	SET @SearchString = LTRIM(RTRIM(REPLACE(@SearchString, '"', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE IF CHARINDEX(' ', @SearchString) > 0
		SET @SearchString = CASE WHEN @MatchTypeCode = 'All' THEN REPLACE(@SearchString, ' ', ' AND ') ELSE REPLACE(@SearchString, ' ', ' OR ') END
	--ENDIF

	RETURN @SearchString

END
GO
--End function document.FormatFullTextSearchString

--Begin function person.CanAccessProject
EXEC utility.DropObject 'person.CanAccessProject'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.20
-- Description:	A function to return a bit indicating weather or not a person id can access a projectid
-- ====================================================================================================
CREATE FUNCTION person.CanAccessProject
(
@PersonID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanAccessProject BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = @ProjectID)
		SET @nCanAccessProject = 1
	--ENDIF

	RETURN @nCanAccessProject

END
GO
--End function person.CanAccessProject

--Begin function spotreport.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'spotreport.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION spotreport.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE 
	  @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@IncidentMarker varchar(max)=''
	
	SELECT
		@IncidentMarker += '&markers=icon:' 
			+ replace(core.GetSystemSetupValueBySetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Location.STY,'') AS VARCHAR(MAX)) 
			+ ','
			 + CAST(ISNULL(I.Location.STX,'') AS VARCHAR(MAX))
	 FROM spotreport.SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult +=  @IncidentMarker 

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function spotreport.FormatStaticGoogleMapForSpotReport

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Greg Yingling
-- Create date:	2016.03.02
-- Description:	Refactored to support multiple projects
-- =================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS @tCanHaveAddUpdate TABLE
	(
	ProjectID	INT NOT NULL PRIMARY KEY,
	DefaultProjectID INT NOT NULL DEFAULT 0
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tCanHaveAddUpdate
			(ProjectID, DefaultProjectID)
		SELECT DISTINCT 
			PP.ProjectID, 

			CASE
				WHEN P.DefaultProjectID = PP.ProjectID
				THEN 1
				ELSE 0
			END AS DefaultProjectID

		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
				AND WSGP.PersonID = @PersonID
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
			JOIN person.PersonProject PP ON PP.PersonID = P.PersonID
				AND PP.ProjectID = W.ProjectID
			
		END
	ELSE
		BEGIN
	
		DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		INSERT @tCanHaveAddUpdate
			(ProjectID, DefaultProjectID)
		SELECT DISTINCT
			PP.ProjectID, 
			0
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.PersonProject PP ON PP.PersonID = EWSGP.PersonID
				AND EWSGP.PersonID = @PersonID
			JOIN core.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = 
					CASE
						WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
						THEN @nWorkflowStepCount
						ELSE @nWorkflowStepNumber
					END
	
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN core.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN core.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM core.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData
--End file Client - 02 - Functions.sql

--Begin file Client - 03 - Procedures.sql
--Begin procedure activity.GetActivityByActivityID
EXEC Utility.DropObject 'activity.GetActivityByActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2015.11.07
-- Description:	A stored procedure to data from the activity.Activity table
-- ========================================================================
CREATE PROCEDURE activity.GetActivityByActivityID

@ActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Activity', @ActivityID)

	SELECT 
		A.ActivityID,
		A.ActivityName,
		A.ActivityTypeID,
		A.AwardeeSubContractorID1,
		A.AwardeeSubContractorID2,
		A.Background,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.FundingSourceID,
		A.Objectives,
		A.PointOfContactPersonID,
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		A.TaskCode,
		A.AwardeeSubContractorID1,
		(SELECT SC1.SubContractorName FROM contact.SubContractor SC1 WHERE SC1.SubContractorID = A.AwardeeSubContractorID1) AS AwardeeSubContractorName1,
		A.AwardeeSubContractorID2,
		(SELECT SC2.SubContractorName FROM contact.SubContractor SC2 WHERE SC2.SubContractorID = A.AwardeeSubContractorID2) AS AwardeeSubContractorName2,
		AT.ActivityTypeName,
		FS.FundingSourceName
	FROM activity.Activity A
		JOIN dropdown.ActivityType AT ON AT.ActivityTypeID = A.ActivityTypeID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = A.FundingSourceID
			AND A.ActivityID = @ActivityID

	SELECT
		AC.CourseID,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		M.ModuleName
	FROM activity.ActivityCourse AC
		JOIN training.Course C ON C.CourseID = AC.CourseID
		JOIN training.Module M on M.ModuleID = C.ModuleID
			AND AC.ActivityID = @ActivityID
	ORDER BY M.ModuleName

	SELECT
		AC.VettingDate,
		core.FormatDate(AC.VettingDate) AS VettingDateFormatted,
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryNameFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM activity.ActivityContact AC
		JOIN contact.Contact C ON C.ContactID = AC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC.VettingOutcomeID
			AND AC.ActivityID = @ActivityID

	SELECT
		AT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AT.TerritoryID) AS TerritoryName
	FROM activity.ActivityTerritory AT
	WHERE AT.ActivityID = @ActivityID
	ORDER BY 2, 1

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('Activity', @ActivityID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('Activity', @ActivityID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Activity'
		AND EL.EntityID = @ActivityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure activity.GetActivityByActivityID

--Begin procedure asset.GetAssetByAssetID
EXEC utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.18
-- Description:	A stored procedure to data from the asset.Asset table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
--
-- Author:			Todd Pires
-- Create date:	2016.04.06
-- Description:	Added the LastUpdateDate field
-- ==================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AssetDescription, 		
		A.AssetID, 		
		A.AssetName, 		
		A.Comments, 		
		A.DeputyManagerContactID, 		
		contact.FormatContactNameByContactID(A.DeputyManagerContactID, 'LastFirstMiddle') AS DeputyManagerFullName,		
		A.History, 		
		A.IsActive,
		A.LastUpdateDate,
		core.FormatDate(A.LastUpdateDate) AS LastUpdateDateFormatted,
		A.Location.STAsText() AS Location,
		A.ManagerContactID,		
		contact.FormatContactNameByContactID(A.ManagerContactID, 'LastFirstMiddle') AS ManagerFullName,		
		A.Notes, 		
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName,		
		A.WebLinks,		
		AT.AssetTypeID, 
		AT.AssetTypeName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetEquipmentResourceProvider AERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AERP.ResourceProviderID
			AND AERP.AssetID = @AssetID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetFinancialResourceProvider AFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AFRP.ResourceProviderID
			AND AFRP.AssetID = @AssetID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID
	
END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure atmospheric.GetAtmosphericByAtmosphericID
EXEC Utility.DropObject 'atmospheric.GetAtmosphericByAtmosphericID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.18
-- Description:	A stored procedure to data from the atmospheric.Atmospheric table
-- ==============================================================================
CREATE PROCEDURE atmospheric.GetAtmosphericByAtmosphericID

@AtmosphericID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AtmosphericDate, 
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID, 
		A.AtmosphericReportedDate, 
		core.FormatDate(A.AtmosphericReportedDate) AS AtmosphericReportedDateFormatted, 
		A.Information, 
		A.IsCritical, 
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.Recommendation, 
		AT.AtmosphericTypeID,
		AT.AtmosphericTypeName,
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
		JOIN dropdown.AtmosphericType AT ON AT.AtmosphericTypeID = A.AtmosphericTypeID
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = A.ConfidenceLevelID
			AND A.AtmosphericID = @AtmosphericID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS AtmosphericSourceGUID,
		ATS.AtmosphericSourceID, 
		ATS.SourceAttribution, 
		ATS.SourceDate, 
		core.FormatDate(ATS.SourceDate) AS SourceDateFormatted, 
		ATS.SourceName, 
		ATS.SourceSummary, 
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName,
		SC.SourceCategoryID, 
		SC.SourceCategoryName,
		ST.SourceTypeID, 
		ST.SourceTypeName
	FROM atmospheric.AtmosphericSource ATS
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = ATS.ConfidenceLevelID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = ATS.SourceCategoryID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = ATS.SourceTypeID
			AND ATS.AtmosphericID = @AtmosphericID
	ORDER BY ATS.SourceName, ATS.AtmosphericSourceID

	SELECT
		AC.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AC.TerritoryID) AS TerritoryName
	FROM atmospheric.AtmosphericTerritory AC
	WHERE AC.AtmosphericID = @AtmosphericID
	ORDER BY 2, 1
	
END
GO
--End procedure atmospheric.GetAtmosphericByAtmosphericID

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Greg Yingling
-- Create date:	2015.10.08
-- Description:	A stored procedure to data from the contact.Contact table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		C7.CountryID AS PlaceOfBirthCountryID,
		C7.CountryName AS PlaceOfBirthCountryName,		
		C8.CountryID AS PassportCountryID,
		C8.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C8 ON C8.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
			AND C1.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT
		CV.VettingDate,
		core.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM contact.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC

	--Courses Attended
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM training.Course C
		JOIN training.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN training.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID

	--Assigned Equipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND AssigneeID = @ContactID 
			AND AssigneeTypeCode = 'Contact'
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure contact.GetSubContractorBySubContractorID
EXEC Utility.DropObject 'contact.GetSubContractorBySubContractorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to data from the contact.SubContractor table
-- ============================================================================
CREATE PROCEDURE contact.GetSubContractorBySubContractorID

@SubContractorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.CountryID AS AddressCountryID,
		C1.CountryName AS AddressCountryName,
		C2.CountryID AS RegistrationCountryID,
		C2.CountryName AS RegistrationCountryName,
		SC.Address,
		SC.PrimaryContactEmailAddress,
		SC.PrimaryContactName,
		SC.PrimaryContactPhone,
		SC.RegistrationNumber,
		SC.SubContractorID,
		SC.SubContractorName,
		SC.TaxNumber,
		SC.AccountNumber,
		SC.BankBranch,
		SC.BankName,
		SC.BankRoutingNumber,
		SC.IBAN,
		SC.SWIFTCode,
		SC.ISOCurrencyCode,
		SC.IsActive,
		SC.ProjectID,
		dropdown.GetProjectNameByProjectID(SC.ProjectID) AS ProjectName,
		SCBT.SubContractorBusinessTypeID,
		SCBT.SubContractorBusinessTypeName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName
	FROM contact.SubContractor SC
		JOIN dropdown.Country C1 ON C1.CountryID = SC.AddressCountryID
		JOIN dropdown.Country C2 ON C2.CountryID = SC.RegistrationCountryID
		JOIN dropdown.SubContractorBusinessType SCBT ON SCBT.SubContractorBusinessTypeID = SC.SubContractorBusinessTypeID
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
			AND SC.SubContractorID = @SubContractorID

	SELECT
		SCC.SubContractorCapabilityID,
		SCC.SubContractorCapabilityName
	FROM dropdown.SubContractorCapability SCC
		JOIN contact.SubContractorSubContractorCapability SCSCC ON SCSCC.SubcontractorCapabilityID = SCC.SubcontractorCapabilityID
			AND SCSCC.SubcontractorID = @SubContractorID

END
GO
--End procedure contact.GetSubContractorBySubContractorID

--Begin procedure contact.GetSubContractors
EXEC Utility.DropObject 'contact.GetSubContractors'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A stored procedure to get data from the contact.SubContractor table
-- ================================================================================
CREATE PROCEDURE contact.GetSubContractors

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		SC.SubContractorID,
		SC.SubContractorName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName
	FROM contact.SubContractor SC 
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
	ORDER BY SCRT.DisplayOrder, SCRT.SubContractorRelationshipTypeName, SC.SubContractorName, SC.SubContractorID

END
GO
--End procedure contact.GetSubContractors

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@HasWorkflow BIT = 0,
@CanRejectAfterFinalApproval BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.HasWorkflow = @HasWorkflow,
		ET.CanRejectAfterFinalApproval = @CanRejectAfterFinalApproval
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,HasWorkflow,CanRejectAfterFinalApproval)
	VALUES
		(
		@EntityTypeCode, 
		@EntityTypeName, 
		@EntityTypeNamePlural,
		@HasWorkflow,
		@CanRejectAfterFinalApproval
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		ET.EmailTemplateCode
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

END
GO
--End procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- =================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetImplementerSetupByImplementerSetupID
EXEC utility.DropObject 'core.GetImplementerSetupByImplementerSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get data from the core.ImplementerSetup table
-- ================================================================================
CREATE PROCEDURE core.GetImplementerSetupByImplementerSetupID

@ImplementerSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		S.ImplementerSetupID, 
		S.SetupKey, 
		S.SetupValue 
	FROM core.ImplementerSetup S
	WHERE S.ImplementerSetupID = @ImplementerSetupID

END
GO
--End procedure core.GetImplementerSetupByImplementerSetupID

--Begin procedure core.GetImplementerSetupValuesBySetupKey
EXEC utility.DropObject 'core.GetImplementerSetupValuesBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get data from the core.ImplementerSetup table
-- ================================================================================
CREATE PROCEDURE core.GetImplementerSetupValuesBySetupKey

@SetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT S.SetupValue 
	FROM core.ImplementerSetup S
	WHERE S.SetupKey = @SetupKey
	ORDER BY S.ImplementerSetupID

END
GO
--End procedure core.GetImplementerSetupValuesBySetupKey

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the core.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the core.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
-- =============================================================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure dbo.GetDocumentByDocumentID
EXEC Utility.DropObject 'dbo.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the dbo.Document table

-- Author:			Greg Yingling
-- Modify Date: 2015.11.07
-- Description: Added territory associations
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.IsForDonorPortal,
		D.PhysicalFileExtension, 
		D.PhysicalFileName, 
		D.PhysicalFilePath, 
		D.PhysicalFileSize, 
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName
	FROM dbo.Document D
	WHERE D.DocumentID = @DocumentID

	SELECT
		DE.EntityID AS TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(DE.EntityID) AS TerritoryName
	FROM dbo.DocumentEntity DE
	WHERE DE.DocumentID = @DocumentID
	ORDER BY 2, 1
	
END
GO
--End procedure dbo.GetDocumentByDocumentID

--Begin procedure dbo.GetFieldReportByFieldReportID
EXEC Utility.DropObject 'dbo.GetFieldReportByFieldReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.15
-- Description:	A stored procedure to data from the dbo.FieldReport table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE dbo.GetFieldReportByFieldReportID

@FieldReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		FR.CommentsArabic,
		FR.CommentsEnglish,
		FR.FieldReportDateTime,
		core.FormatDateTime(FR.FieldReportDateTime) AS FieldReportDateTimeFormatted,
		FR.FieldReportID,
		FR.FieldReportLocationArabic,
		FR.FieldReportLocationEnglish,
		FR.IsCritical,
		FR.Location.STAsText() AS Location,
		FR.ProjectID,
		dropdown.GetProjectNameByProjectID(FR.ProjectID) AS ProjectName,
		FR.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FR.TerritoryID) AS TerritoryName,		
		FR.UserIDArabic,
		FR.UserIDEnglish,
		FR.UserTokenArabic,
		FR.UserTokenEnglish,
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM dbo.FieldReport FR
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = FR.IncidentTypeID
			AND FR.FieldReportID = @FieldReportID

END
GO
--End procedure dbo.GetFieldReportByFieldReportID

--Begin procedure dbo.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.15
-- Description:	A stored procedure to data from the dbo.Incident table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Source,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,		
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		ST.SourceTypeID,
		ST.SourceTypeName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = I.SourceTypeID
			AND I.IncidentID = @IncidentID
	
END
GO
--End procedure dbo.GetIncidentByIncidentID

--Begin procedure dropdown.GetIndicatorTypeData
EXEC utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	A stored procedure to return data from the dropdown.IndicatorType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIndicatorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorTypeID, 
		T.IndicatorTypeName,
		T.IsActive
	FROM dropdown.IndicatorType T
	WHERE (T.IndicatorTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorTypeName, T.IndicatorTypeID

END
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure eventlog.LogImplementerSetupAction
EXEC utility.DropObject 'eventlog.LogImplementerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogImplementerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ImplementerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogImplementerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogImplementerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogImplementerSetupActionTable
		FROM core.ImplementerSetup ISU
		WHERE ISU.ImplementerSetupID = @EntityID
		
		ALTER TABLE #LogImplementerSetupActionTable DROP COLUMN SetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ImplementerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ImplementerSetup'), ELEMENTS
			)
		FROM #LogImplementerSetupActionTable T
			JOIN core.ImplementerSetup ISU ON ISU.ImplementerSetupID = T.ImplementerSetupID

		DROP TABLE #LogImplementerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogImplementerSetupAction

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.03
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFI.CompletedDate,
		core.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.DesiredResponseDate,
		core.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		RFI.IncidentDate,
		core.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		core.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.KnownDetails,
		RFI.PointOfContactPersonID,
		person.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		RFI.ProjectID,
		dropdown.GetProjectNameByProjectID(RFI.ProjectID) AS ProjectName,
		RFI.RequestDate,
		core.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		person.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonNameFormatted,
		RFI.SummaryAnswer,
		RFI.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(RFI.TerritoryID) AS TerritoryNameFormatted,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
			AND DE.DocumentID = @DocumentID
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure document.GetEntityDocuments
EXEC Utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get records from the document.DocumentEntity table
-- =====================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentType, 
		D.ContentSubtype,
		D.DocumentDate, 
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentID INT,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = @DocumentID
		AND DE.EntityTypeCode = @EntityTypeCode
		AND DE.EntityTypeSubCode = @EntityTypeSubCode
		AND DE.EntityID = @EntityID 

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure document.SaveEntityDocuments
EXEC Utility.DropObject 'document.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure document.SaveEntityDocuments

--Begin procedure dropdown.GetSubContractorCapabilityData
EXEC Utility.DropObject 'dropdown.GetSubContractorCapabilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorCapability table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetSubContractorCapabilityData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorCapabilityID, 
		T.SubContractorCapabilityName
	FROM dropdown.SubContractorCapability T
	WHERE (T.SubContractorCapabilityID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorCapabilityName, T.SubContractorCapabilityID

END
GO
--End procedure dropdown.GetSubContractorCapabilityData

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogSubContractorAction
EXEC Utility.DropObject 'eventlog.LogSubContractorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSubContractorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		DECLARE @cSubContractorSubContractorCapabilities VARCHAR(MAX) 
	
		SELECT 
			@cSubContractorSubContractorCapabilities = COALESCE(@cSubContractorSubContractorCapabilities, '') + D.SubContractorSubContractorCapability
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SubContractorCapability'), ELEMENTS) AS SubContractorSubContractorCapability
			FROM contact.SubContractorSubContractorCapability T 
			WHERE T.SubContractorID = @EntityID
			) D	

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('SubContractor', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<SubContractorSubContractorCapabilities>' + ISNULL(@cSubContractorSubContractorCapabilities , '') + '</SubContractorSubContractorCapabilities>') AS XML), 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('SubContractor'), ELEMENTS
			)
		FROM contact.SubContractor T
		WHERE T.SubContractorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSubContractorAction

--Begin procedure finding.GetFindingByFindingID
EXEC Utility.DropObject 'finding.GetFindingByFindingID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to get data from the finding.Finding table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ==========================================================================
CREATE PROCEDURE finding.GetFindingByFindingID

@FindingID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingDescription,
		F.IsResearchElement,
		F.IsActive,
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND F.FindingID = @FindingID

	SELECT
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM finding.FindingIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.FindingID = @FindingID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		R.RecommendationID,
		R.RecommendationName,
		R.IsResearchElement
	FROM finding.FindingRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.FindingID = @FindingID
	ORDER BY R.RecommendationName, R.RecommendationID

	SELECT
		FT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FT.TerritoryID) AS TerritoryName
	FROM finding.FindingTerritory FT
	WHERE FT.FindingID = @FindingID
	ORDER BY 2, 1

END
GO
--End procedure finding.GetFindingByFindingID

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.17
-- Description:	A stored procedure to data from the force.Force table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
--
-- Author:			Todd Pires
-- Create date:	2016.04.06
-- Description:	Added the LastUpdateDate field
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		contact.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		F.Comments, 
		F.DeputyCommanderContactID, 
		contact.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		core.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		core.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		core.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		core.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		core.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		core.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorID, 	
		I.IndicatorName, 	
		M.AchievedValue, 	
		M.MilestoneID, 	
		M.MilestoneName, 	
		M.ProjectID,
		dropdown.GetProjectNameByProjectID(M.ProjectID) AS ProjectName,
		M.TargetDate, 	
		core.FormatDate(M.TargetDate) AS TargetDateFormatted,
		M.TargetValue
	FROM logicalframework.Milestone M
		JOIN logicalframework.Indicator I ON I.IndicatorID = M.IndicatorID
			AND M.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframework.GetMilestoneDataByIndicatorID
EXEC utility.DropObject 'logicalframework.GetMilestoneDataByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2015.09.04
-- Description:	A stored procedure to return Milestone data
-- ============================================================
CREATE PROCEDURE logicalframework.GetMilestoneDataByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.AchievedValue, 
		M.IndicatorID, 
		M.InProgressValue, 
		M.IsActive, 
		M.MilestoneID, 
		M.MilestoneName, 
		M.PlannedValue,
		M.TargetDate, 
		core.FormatDate(M.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, M.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		M.TargetValue
	FROM logicalframework.MileStone M
	WHERE M.IndicatorID = @IndicatorID
	
END
GO
--End procedure logicalframework.GetMilestoneDataByIndicatorID

--Begin procedure logicalframework.GetObjectiveByObjectiveID
EXEC utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.ProjectID,
		dropdown.GetProjectNameByProjectID(O1.ProjectID) AS ProjectName,
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByObjectiveID

--Begin procedure mediareport.GetMediaReportByMediaReportID
EXEC Utility.DropObject 'mediareport.GetMediaReportByMediaReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to data from the mediareport.MediaReport table
-- ==============================================================================
CREATE PROCEDURE mediareport.GetMediaReportByMediaReportID

@MediaReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.Comments,
		MR.CommunicationOpportunities,
		MR.Implications,
		MR.MediaReportDate,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediaReportLocation,
		MR.MediaReportTitle,
		MR.ProjectID,
		dropdown.GetProjectNameByProjectID(MR.ProjectID) AS ProjectName,
		MR.RiskMitigation,
		MR.Summary,
		MR.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.MediaReportID = @MediaReportID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS MediaReportSourceGUID,
		MRS.MediaReportSourceID, 
		MRS.SourceAttribution, 
		MRS.SourceDate, 
		core.FormatDate(MRS.SourceDate) AS SourceDateFormatted, 
		MRS.SourceName, 
		MRS.SourceSummary, 
		MRST.MediaReportSourceTypeID, 
		MRST.MediaReportSourceTypeName,
		MRST.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		MRST.MediaReportSourceTypeName AS SourceTypeName, -- Aliased for the common source cfmodule
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName,
		SC.SourceCategoryID, 
		SC.SourceCategoryName
	FROM mediareport.MediaReportSource MRS
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = MRS.ConfidenceLevelID
		JOIN dropdown.MediaReportSourceType MRST ON MRST.MediaReportSourceTypeID = MRS.MediaReportSourceTypeID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = MRS.SourceCategoryID
			AND MRS.MediaReportID = @MediaReportID
	ORDER BY MRS.SourceName, MRS.MediaReportSourceID

END
GO
--End procedure mediareport.GetMediaReportByMediaReportID

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.3.15
-- Description:	A stored procedure to delete data from the permissionable.Permissionable and person.PersonPermissionable tables
-- ============================================================================================================================
CREATE PROCEDURE permissionable.DeletePermissionable

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPermissionableLineage VARCHAR(MAX)

	SELECT
		@cPermissionableLineage = P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE P
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = MIPL.PermissionableLineage
		)

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PP.PermissionableLineage
		)

	DELETE PTP
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PTP.PermissionableLineage
		)

	SELECT @cPermissionableLineage AS PermissionableLineage
		
END
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.09
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableID = P.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure person.GetMenuItemsByPersonID
EXEC utility.DropObject 'person.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.11
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- ===============================================================================================
CREATE PROCEDURE person.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage = MIPL.PermissionableLineage
						)
						AND PP.PersonID = @PersonID

					UNION
					
					SELECT 1
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = MI.EntityTypeCode
						AND EWSGP.IsComplete = 0
						AND EWSGP.PersonID = @PersonID

					UNION

					SELECT 1
					FROM workflow.Workflow W
						JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
							AND W.EntityTypeCode = MI.EntityTypeCode
						JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
						JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
							AND WSGP.PersonID = @PersonID

					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
						(
						EXISTS
							(
							SELECT 1
							FROM person.PersonPermissionable PP
							WHERE EXISTS
								(
								SELECT 1
								FROM core.MenuItemPermissionableLineage MIPL
								WHERE MIPL.MenuItemID = MI.MenuItemID
									AND PP.PermissionableLineage = MIPL.PermissionableLineage
								)
								AND PP.PersonID = @PersonID
							)
						OR EXISTS
							(
							SELECT 1
							FROM workflow.EntityWorkflowStepGroupPerson EWSGP
							WHERE EWSGP.EntityTypeCode = MI.EntityTypeCode
								AND EWSGP.IsComplete = 0
								AND EWSGP.PersonID = @PersonID
							)	
						OR EXISTS
							(	
							SELECT 1
							FROM workflow.Workflow W
								JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
									AND W.EntityTypeCode = MI.EntityTypeCode
								JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
								JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
									AND WSGP.PersonID = @PersonID
							)
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure person.GetMenuItemsByPersonID

--Begin procedure person.GetProjectsByPersonID
EXEC Utility.DropObject 'person.GetProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.22
-- Description:	A stored procedure to return data from the person.PersonProject table based on a PersonID
-- ======================================================================================================
CREATE PROCEDURE person.GetProjectsByPersonID

@ImplementerCode VARCHAR(50),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PRJ.ProjectID,
		PRJ.ProjectName,
		
		CASE
			WHEN PER.DefaultProjectID = PRJ.ProjectID
			THEN 1
			ELSE 0
		END AS IsDefaultProject

	FROM dropdown.Project PRJ
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = PRJ.ProjectID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Project'
		JOIN person.PersonProject PP ON PP.ProjectID = PRJ.ProjectID
			AND PP.PersonID = @PersonID
		JOIN person.Person PER ON PER.PersonID = PP.PersonID
	ORDER BY PRJ.DisplayOrder, PRJ.ProjectName

END
GO
--End procedure person.GetProjectsByPersonID

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Added password expiration support
--
-- Author:			Todd Pires
-- Create date: 2016.01.23
-- Description:	Added defaultprojectid
--
-- Author:			Todd Pires
-- Create date:	2016.05.08
-- Description:	Added the IsSuperAdministrator bit
-- ================================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		DefaultProjectID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDefaultProjectID = P.DefaultProjectID,
		@cPhone = P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,DefaultProjectID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@nDefaultProjectID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE person.Person
				SET InvalidLoginAttempts = 0
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.21
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
-- =====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EI.AssigneeID, 
		EI.AssigneeTypeCode, 
		
		CASE
			WHEN EI.AssigneeTypeCode = 'Asset'
			THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EI.AssigneeID)
			WHEN EI.AssigneeTypeCode = 'Contact'
			THEN contact.FormatContactNameByContactID(EI.AssigneeID, 'LastFirstMiddle')
			WHEN EI.AssigneeTypeCode = 'Force'
			THEN (SELECT F.ForceName FROM force.Force F WHERE F.ForceID = EI.AssigneeID)
			ELSE ''
		END AS AssigneeName,
		
		EI.AssignmentDate,
		core.FormatDate(EI.AssignmentDate) AS AssignmentDateFormatted,
		EI.EquipmentInventoryID, 
		EI.Notes, 
		EI.ProjectID,
		dropdown.GetProjectNameByProjectID(EI.ProjectID) AS ProjectName,
		EI.Quantity, 
		EI.SerialNumber, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.InvoiceDate,
		EI.InvoiceNumber,
		core.FormatDate(EI.InvoiceDate) AS InvoiceDateFormatted,
		CONCAT(C.ISOCurrencyCode,' ',EC.UnitCost) AS UnitCostFormatted
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.Currency C ON C.CurrencyID = EC.CurrencyID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure productdistributor.GetCampaignByCampaignID
EXEC utility.DropObject 'productdistributor.GetCampaignByCampaignID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.18
-- Description:	A stored procedure to data from the productdistributor.Campaign table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ===================================================================================
CREATE PROCEDURE productdistributor.GetCampaignByCampaignID

@CampaignID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CampaignDescription,
		C.CampaignID,
		C.CampaignName,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.IsActive,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted
	FROM productdistributor.Campaign C
	WHERE C.CampaignID = @CampaignID
	
END
GO
--End procedure productdistributor.GetCampaignByCampaignID

--Begin procedure productdistributor.GetDistributorByDistributorID
EXEC utility.DropObject 'productdistributor.GetDistributorByDistributorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.18
-- Description:	A stored procedure to data from the productdistributor.Distributor table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
--
-- Author:			Todd Pires
-- Create date:	2016.05.17
-- Description:	Added FB Integration support
-- ======================================================================================
CREATE PROCEDURE productdistributor.GetDistributorByDistributorID

@DistributorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.NaturalReach.STAsText() AS NaturalReach,
		D.Notes,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		FBP.ID AS FacebookPageID,
		FBP.Name AS FacebookPageName
	FROM productdistributor.Distributor D
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND D.DistributorID = @DistributorID
		JOIN integration.FacebookPage FBP ON FBP.ID = D.FacebookPageID

	SELECT
		DT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(DT.TerritoryID) AS TerritoryName
	FROM productdistributor.DistributorTerritory DT
	WHERE DT.DistributorID = @DistributorID
	ORDER BY 2, 1

END
GO
--End procedure productdistributor.GetDistributorByDistributorID

--Begin procedure productdistributor.GetProductByProductID
EXEC utility.DropObject 'productdistributor.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date:	2015.10.18
-- Description:	A stored procedure to data from the productdistributor.Product table
-- =================================================================================
CREATE PROCEDURE productdistributor.GetProductByProductID

@ProductID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM productdistributor.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM productdistributor.ProductCampaign PC
		JOIN productdistributor.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM productdistributor.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
	ORDER BY 2, 1

	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName
	FROM productdistributor.ProductDistribution PD
		JOIN productdistributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
	ORDER BY D.DistributorName, D.DistributorID
	
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM productdistributor.ProductDistribution PD
	WHERE PD.ProductID = @ProductID

	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM productdistributor.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
	ORDER BY 2, 1
	
END
GO
--End procedure productdistributor.GetProductByProductID

--Begin procedure programreport.GetProgramReportByProgramReportID
EXEC utility.DropObject 'programreport.GetProgramReportByProgramReportID'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date: 2016.04.17
-- Description:	A stored procedure to get data from the programreport.ProgramReport table
-- ======================================================================================
CREATE PROCEDURE programreport.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ProgramReport', @ProgramReportID)

	SELECT
		P.ProjectID,
		P.ProjectName,
		PR.Broadcast,
		PR.CommentAnalysis,
		PR.CyberSecurity,
		PR.EndDate,
		core.FormatDate(PR.EndDate) AS EndDateFormatted,
		PR.Graphics,
		PR.MediaOutreach,
		PR.OperationalIssues,
		PR.PhysicalSecurity,
		PR.ProgramReportID,
		PR.StartDate,
		core.FormatDate(PR.StartDate) AS StartDateFormatted,
		PR.Summary,
		PR.Training,
		PR.Videos,
		PR.WrittenMaterial
	FROM programreport.ProgramReport PR
		JOIN dropdown.Project P ON P.ProjectID = PR.ProjectID
			AND PR.ProgramReportID = @ProgramReportID

	SELECT
		P.ProductID,
		P.ProductName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName
	FROM productdistributor.Product P
		JOIN programreport.ProgramReportProduct PRP ON PRP.ProductID = P.ProductID
			AND PRP.ProgramReportID = @ProgramReportID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
	ORDER BY P.ProductName, P.ProductID

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ProgramReport', @ProgramReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ProgramReport', @ProgramReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Program Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Program Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Program Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Program Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ProgramReport'
		AND EL.EntityID = @ProgramReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure programreport.GetProgramReportByProgramReportID

--Begin procedure recommendation.GetRecommendationByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to get data from the recommendation.Recommendation table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ========================================================================================
CREATE PROCEDURE recommendation.GetRecommendationByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.IsActive,
		R.IsResearchElement,
		R.ProjectID,
		dropdown.GetProjectNameByProjectID(R.ProjectID) AS ProjectName,
		R.RecommendationDescription,
		R.RecommendationID,
		R.RecommendationName
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendation.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		RT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(RT.TerritoryID) AS TerritoryName
	FROM recommendation.RecommendationTerritory RT
	WHERE RT.RecommendationID = @RecommendationID
	ORDER BY 2, 1

END
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure reporting.GetEquipmentInventoryListByPersonID
EXEC utility.DropObject 'reporting.GetEquipmentInventoryListByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================================================
-- Author:			Justin Branum
-- Create date: 2016.03.04
-- Description: A stored procedure to get data from the procurement.EquipmentInvetory table matching the reporting.SearchResults items
--
-- Author:			Justin Branum
-- Create date: 2016.03.21
-- Description: Adding currency per request for report
-- ===================================================================================================================================
CREATE PROCEDURE reporting.GetEquipmentInventoryListByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EI.Quantity,
		EI.SerialNumber,
		person.FormatPersonNameByPersonID(EI.AssigneeID, 'LastFirstTitle') AS AssigneeName,
		core.FormatDate(EI.AssignmentDate) AS AssignmentDateFormatted,
		EI.InvoiceNumber,
		core.FormatDate(EI.InvoiceDate) AS InvoiceDateFormatted,
		EC.ItemName,
		EC.UnitCost,
		EC.Notes,
		DDP.ProjectName,
		DDC.CurrencyName
	FROM reporting.SearchResult SR
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = SR.EntityID  
		AND SR.PersonID = @PersonID
			AND SR.EntityTypeCode = 'EquipmentInventory'
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.Project DDP ON DDP.ProjectID = EI.ProjectID
	JOIN dropdown.Currency DDC ON DDC.CurrencyID = EC.CurrencyID
END
GO
--End procedure reporting.GetEquipmentInventoryListByPersonID

--Begin procedure reporting.GetMediaReports
EXEC utility.DropObject 'reporting.GetMediaReports'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.29
-- Description:	A stored procedure to data for the MediaReport SSRS
-- ================================================================
CREATE PROCEDURE reporting.GetMediaReports

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.Comments,
		MR.CommunicationOpportunities,
		MR.Implications,
		MR.MediaReportDate,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediareportLocation AS Location,
		reporting.GetMediaReportSourceLinks(MR.MediaReportID) AS MediaReportSourceLinks,
		MR.MediaReportTitle,
		MR.ProjectID,
		dropdown.GetProjectNameByProjectID(MR.ProjectID) AS ProjectName,
		MR.RiskMitigation,
		MR.Summary,
		MR.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName
	FROM reporting.SearchResult SR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = SR.EntityID  
			AND SR.PersonID = @personID  
			AND SR.EntityTypeCode = 'MediaReport'
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID

END
GO
--End procedure reporting.GetMediaReports

--Begin procedure reporting.GetProductListByPersonID
EXEC utility.DropObject 'reporting.GetProductListByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================================
-- Author:			Justin Branum
-- Create date: 2016.03.07
-- Description:	A stored procedure to get data from the productdistributor.Product table matching the reporting.SearchResults items
--
-- Author:			Justin Branum
-- Create date: 2016.05.25
-- Description:	Various updates
-- ================================================================================================================================
CREATE PROCEDURE reporting.GetProductListByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProductName,
		DPS.ProductStatusName,
		DPT.ProductTypeName,
		P.ProductCode,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		DP.ProjectName,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReach,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCount,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCount,
		ISNULL(SUM(PD.CommentCount), 0)AS CommentCount,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount,
		DTA.TargetAudienceName,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal
	FROM reporting.SearchResult SR
		JOIN productdistributor.Product P ON P.ProductID = SR.EntityID  
			AND SR.PersonID = @personID  
			AND SR.EntityTypeCode = 'Product'
		JOIN dropdown.ProductStatus DPS ON DPS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType DPT ON DPT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.Project DP ON DP.ProjectID = P.ProjectID
		JOIN productdistributor.ProductDistribution PD ON PD.ProductID = P.ProductID
		JOIN dropdown.TargetAudience DTA ON DTA.TargetAudienceID = P.TargetAudienceID
	GROUP BY P.ProductName,
		DPS.ProductStatusName,
		DPT.ProductTypeName,
		P.ProductCode,
		P.ReleaseDate,
		DP.ProjectName,
		DTA.TargetAudienceName
END
GO
--End procedure reporting.GetProductListByPersonID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		--spotreport.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID) AS WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.FormatStaticGoogleMapForSpotReport(SR.SpotReportID) AS GMap,
		dbo.GetSpotReportTerritoriesList(SR.SpotReportID) AS TerritoryList	
	FROM spotReport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure reporting.GetSpotReportBySpotReportID

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)
	
	SELECT
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunicationOpportunities,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SourceDetails,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon
	FROM spotreport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
	ORDER BY 2, 1

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM training.Course C
		JOIN training.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		AOT.AreaOfOperationTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM productdistributor.ImpactStory I
	WHERE EXISTS (
		SELECT 1
		FROM productdistributor.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
	)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM productdistributor.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM productdistributor.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1

END
GO
--End procedure territory.GetTerritoryByTerritoryID

--Begin procedure training.GetCourseByCourseID
EXEC utility.DropObject 'training.GetCourseByCourseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the training.Course table
-- ======================================================================
CREATE PROCEDURE training.GetCourseByCourseID

@CourseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CourseID,
		C.CoursePointOfContact,
		C.TerritoryID,
		(territory.FormatTerritoryNameByTerritoryID(C.TerritoryID)) AS TerritoryNameFormatted,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Instructor1,
		C.Instructor1Comments,
		C.Instructor2,
		C.Instructor2Comments,
		C.Location,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Seats,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		C.StudentFeedbackSummary,
		C.QualityAssuranceFeedback,
		M.ModuleID,
		M.ModuleName,
		ISNULL((SELECT AC.ActivityID FROM activity.ActivityCourse AC WHERE AC.CourseID = C.CourseID), 0) AS ActivityID
	FROM training.Course C
		JOIN training.Module M ON M.ModuleID = C.ModuleID
			AND C.CourseID = @CourseID
	
	SELECT
		territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID) AS TerritoryNameFormatted,
		CO.ContactID,
		AC2.VettingIcon,
		AC2.VettingOutcomeName,
		core.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		contact.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS ContactNameFormatted,
		IIF (CO.AssetID > 0,
			(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = CO.AssetID), 
			IIF (CO.ForceID > 0,
				(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = CO.ForceID),
				IIF (CO.TerritoryID > 0,
					territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID),
					'')) ) AS ParentOrganizationName,
		STUFF((
			SELECT ', ' + CT.ContactTypeName
			FROM contact.ContactContactType CCT
			JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = CC.ContactID
			FOR XML PATH('')
		), 1, 2, '') AS ContactTypeNamesList
	FROM training.CourseContact CC
		JOIN training.Course CL ON CL.CourseID = CC.CourseID
		JOIN contact.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.CourseID = @CourseID
		OUTER APPLY 
			(
			SELECT 
				'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
				VO.VettingOutcomeName
			FROM activity.ActivityContact AC1
				JOIN activity.ActivityCourse AC2 ON AC2.CourseID = CL.CourseID
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC1.VettingOutcomeID
					AND AC1.ContactID = CC.ContactID
					AND AC1.ActivityID = AC2.ActivityID
			) AC2

END
GO
--End procedure training.GetCourseByCourseID

--Begin procedure training.GetModuleByModuleID
EXEC utility.DropObject 'training.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the training.Module table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added call for Module Document information
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE training.GetModuleByModuleID

@ModuleID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.ModuleID,
		C.ModuleName,
		C.ModuleTypeID,
		C.Summary,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.SponsorName,
		C.Notes,
		CT.ModuleTypeID,
		CT.ModuleTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Module') AS EntityTypeName
	FROM training.Module C
		JOIN dropdown.ModuleType CT ON CT.ModuleTypeID = C.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ModuleID = @ModuleID
		
END
GO
--End procedure training.GetModuleByModuleID

--Begin procedure training.GetModules
EXEC utility.DropObject 'training.GetModules'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.17
-- Description:	A stored procedure to get data from the training.Module table
-- ==========================================================================
CREATE PROCEDURE training.GetModules

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		M.ModuleID,
		M.ModuleName
	FROM training.Module M
	WHERE M.ProjectID = @ProjectID
	ORDER BY M.ModuleName, M.ModuleID

END
GO
--End procedure training.GetModules

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE utility.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM person.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1

END	
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID
--End file Client - 03 - Procedures.sql

--Begin file Client - 04 - Data.sql
--Begin table core.ImplementerSetup
EXEC core.ImplementerSetupAddUpdate 'ImplementerCode', 'LEO0002'
GO
--End table core.ImplementerSetup

--End file Client - 04 - Data.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2017.01.22 21.35.47')
GO
--End build tracking

