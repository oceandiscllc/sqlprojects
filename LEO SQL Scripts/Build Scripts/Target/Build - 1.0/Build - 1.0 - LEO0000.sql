-- File Name:	Build - 1.0 - LEO0000.sql
-- Build Key:	Build - 1.0 - 2017.01.22 21.35.47

USE [LEO0000]
GO

-- ==============================================================================================================================
-- Functions:
--		core.FormatDate
--		core.FormatDateTime
--		core.FormatTime
--		core.GetEntityTypeNameByEntityTypeCode
--		core.GetSystemSetupValueBySetupKey
--		core.ListToTable
--		document.FormatPhysicalFileSize
--		territory.FormatTerritoryNameByTerritoryID
--		territory.GetDescendantTerritoriesByByTerritoryID
--
-- Procedures:
--		core.GetSystemSetupDataBySystemSetupID
--		core.GetSystemSetupValuesBySetupKey
--		core.MenuItemAddUpdate
--		core.SystemSetupAddUpdate
--		dropdown.GetActivityTypeData
--		dropdown.GetAreaOfOperationTypeData
--		dropdown.GetAssetTypeData
--		dropdown.GetAtmosphericTypeData
--		dropdown.GetCommunicationThemeData
--		dropdown.GetConfidenceLevelData
--		dropdown.GetContactStatusData
--		dropdown.GetContactTypeData
--		dropdown.GetControllerData
--		dropdown.GetCountryCallingCodeData
--		dropdown.GetCountryCallingDataByCountryCallingCodeID
--		dropdown.GetCountryData
--		dropdown.GetCurrencyData
--		dropdown.GetDateFilterData
--		dropdown.GetDistributorTypeData
--		dropdown.GetDocumentTypeData
--		dropdown.GetEngagementStatusData
--		dropdown.GetEntityTypeNameData
--		dropdown.GetEquipmentCatalogData
--		dropdown.GetEventCodeNameData
--		dropdown.GetFacebookPageData
--		dropdown.GetFindingStatusData
--		dropdown.GetFindingTypeData
--		dropdown.GetFundingSourceData
--		dropdown.GetImpactDecisionData
--		dropdown.GetIncidentTypeData
--		dropdown.GetIndicatorTypeData
--		dropdown.GetInformationValidityData
--		dropdown.GetLearnerProfileTypeData
--		dropdown.GetLogicalFrameworkStatusData
--		dropdown.GetMediaReportSourceTypeData
--		dropdown.GetMediaReportTypeData
--		dropdown.GetMethodData
--		dropdown.GetModuleTypeData
--		dropdown.GetObjectiveTypeData
--		dropdown.GetProductOriginData
--		dropdown.GetProductStatusData
--		dropdown.GetProductTypeData
--		dropdown.GetProgramTypeData
--		dropdown.GetProjectData
--		dropdown.GetRequestForInformationResultTypeData
--		dropdown.GetRequestForInformationStatusData
--		dropdown.GetResourceProviderData
--		dropdown.GetRiskCategoryData
--		dropdown.GetRiskStatusData
--		dropdown.GetRiskTypeData
--		dropdown.GetRoleData
--		dropdown.GetSourceCategoryData
--		dropdown.GetSourceTypeData
--		dropdown.GetStatusChangeData
--		dropdown.GetSubContractorBusinessTypeData
--		dropdown.GetSubContractorRelationshipTypeData
--		dropdown.GetTargetAudienceData
--		dropdown.GetTerritoryTypeData
--		dropdown.GetUnitTypeData
--		dropdown.GetVettingOutcomeData
--		eventlog.LogPermissionableAction
--		permissionable.GetPermissionableByPermissionableID
--		permissionable.GetPermissionableGroups
--
-- Schemas:
--		core
--		implementer
--
-- Tables:
--		dropdown.DocumentCategory
--		dropdown.DocumentType
--		dropdown.Project
--		implementer.Implementer
--		implementer.ImplementerDropdown
--		implementer.ImplementerDropdownData
--		implementer.Synonym
-- ==============================================================================================================================

--Begin file LEO0000 - 00 - Prerequisites.sql
USE LEO0000
GO

--Begin schemas
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'implementer'
GO
--End schemas

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM core.SystemSetup S WHERE S.SetupKey = @SetupKey)
		BEGIN

		INSERT INTO core.SystemSetup
			(SetupKey,SetupValue)
		VALUES
			(@SetupKey,@SetupValue)

		END
	ELSE
		BEGIN

		UPDATE core.SystemSetup
		SET SetupValue = @SetupValue
		WHERE SetupKey = @SetupKey

		END
	--ENDIF
	
END
GO
--End procedure core.SystemSetupAddUpdate
--End file LEO0000 - 00 - Prerequisites.sql

--Begin file LEO0000 - 01 - Tables.sql
USE LEO0000
GO

--Begin table dropdown.DocumentCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentCategory
	(
	DocumentCategoryID INT NOT NULL IDENTITY(1,1),
	DocumentCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentCategoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentCategory', 'DisplayOrder,DocumentCategoryName'
GO

INSERT INTO dropdown.DocumentCategory
	(DocumentCategoryName, DisplayOrder)
VALUES
	('Briefings', 1),
	('Reports', 2),
	('M & E', 3),
	('Training', 4),
	('Logistics', 5),
	('Admin', 6)
GO
--End table dropdown.DocumentCategory

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT NOT NULL IDENTITY(1,1),
	DocumentCategoryID INT,
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DocumentCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentType', 'DocumentCategoryID,DisplayOrder,DocumentTypeName'
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.06
-- Description:	A trigger to update the dropdown.DocumentType and permissionable.Permissionable tables
-- ===================================================================================================
CREATE TRIGGER dropdown.TR_DocumentType ON dropdown.DocumentType AFTER INSERT
AS
SET ARITHABORT ON

UPDATE DT
SET DT.DocumentTypeCode = REPLACE(DT.DocumentTypeName, ' ', '')
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NULL

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description)
SELECT
	'Document',
	'View',
	DT.DocumentTypeCode,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = 'Documents'),
	'View documents of type ' + LOWER(DT.DocumentTypeName) + ' in the library'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
		)

GO

ALTER TABLE dropdown.DocumentType ENABLE TRIGGER TR_DocumentType
GO

DECLARE @cDocumentCategoryName VARCHAR(50)
DECLARE @nI INT

SET @cDocumentCategoryName = 'Briefings'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Program', 'Program', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'General', 'General', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Reports'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Situational', 'Situational', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'MonthlyActivity', 'Monthly Activty', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'QuarterlyActivity', 'Quarterly Activity', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'AdditionalActivity', 'Additional Activity', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Spot', 'Spot', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Meeting', 'Meeting', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Training', 'Training', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'RFIResponse', 'RFI Response', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Media', 'Media', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Other', 'Other', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'M & E'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'M&EPlan', 'M & E Plan', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Reporting', 'Reporting', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LogFrame', 'Log Frame', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Training'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Plan', 'Plan', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'QualityAssuranceFeedbackDocument', 'Quality Assurance Feedback Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Trainer1Document', 'Trainer 1 Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Trainer2Document', 'Trainer 2 Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Logistics'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Legal&Policy', 'Legal & Policy', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Presentations', 'Presentations', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Procurement', 'Procurement', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Admin'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LeonardoUserGuides', 'Leonardo User Guides', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LeonardoAdministration', 'Leonardo Administration', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Other', 'Other', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
GO
--End table dropdown.DocumentCategory

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Project
	(
	ProjectID INT NOT NULL IDENTITY(0,1),
	ProjectCode VARCHAR(50),
	ProjectName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Project', 'DisplayOrder,ProjectName', 'ProjectID'
GO

SET IDENTITY_INSERT dropdown.Project ON
GO

INSERT INTO dropdown.Project (ProjectID) VALUES (0)

SET IDENTITY_INSERT dropdown.Project OFF
GO

INSERT INTO dropdown.Project
	(ProjectName, ProjectCode, DisplayOrder)
VALUES
	('Project One', 'P1', 1),
	('Project Two', 'P2', 2),
	('Project Three', 'P3', 3),
	('Project Four', 'P4', 4)
GO
--End table dropdown.Project

--Begin table implementer.Implementer
DECLARE @TableName VARCHAR(250) = 'implementer.Implementer'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.Implementer
	(
	ImplementerID INT IDENTITY(1,1) NOT NULL,
	ImplementerName VARCHAR(250),
	ImplementerURL VARCHAR(2000),
	ImplementerCode VARCHAR(25)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Implementer', 'ImplementerName,ImplementerCode'
GO

INSERT INTO implementer.Implementer
	(ImplementerName, ImplementerURL, ImplementerCode)
VALUES
	('Implementer 1', 'Abby', 'LEO0001'),
	('Implementer 2', 'Niko', 'LEO0002'),
	('Implementer 3', 'Odee', 'LEO0003'),
	('Implementer 4', 'Petra', 'LEO0004')
GO
--End table implementer.Implementer

--Begin table implementer.ImplementerDropdown
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdown'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.ImplementerDropdown
	(
	ImplementerDropdownID INT IDENTITY(1,1) NOT NULL,
	ImplementerCode VARCHAR(25),
	DropdownCode VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ImplementerDropdownID'
EXEC utility.SetIndexClustered @TableName, 'IX_ImplementerDropdown', 'ImplementerCode,DropdownCode'
GO

INSERT INTO implementer.ImplementerDropdown
	(ImplementerCode, DropdownCode)
VALUES
	('LEO0001', 'ActivityType'),
	('LEO0001', 'AreaOfOperationType'),
	('LEO0001', 'AssetType'),
	('LEO0001', 'AtmosphericType'),
	('LEO0001', 'CommunicationTheme'),
	('LEO0001', 'ConfidenceLevel'),
	('LEO0001', 'ContactStatus'),
	('LEO0001', 'ContactType'),
	('LEO0001', 'Country'),
	('LEO0001', 'CountryCallingCode'),
	('LEO0001', 'Currency'),
	('LEO0001', 'DateFilter'),
	('LEO0001', 'DistributorType'),
	('LEO0001', 'DocumentType'),
	('LEO0001', 'EngagementStatus'),
	('LEO0001', 'FindingStatus'),
	('LEO0001', 'FindingType'),
	('LEO0001', 'FundingSource'),
	('LEO0001', 'ImpactDecision'),
	('LEO0001', 'IncidentType'),
	('LEO0001', 'IndicatorType'),
	('LEO0001', 'InformationValidity'),
	('LEO0001', 'LearnerProfileType'),
	('LEO0001', 'LogicalFrameworkStatus'),
	('LEO0001', 'MediaReportSourceType'),
	('LEO0001', 'MediaReportType'),
	('LEO0001', 'ModuleType'),
	('LEO0001', 'ObjectiveType'),
	('LEO0001', 'ProductOrigin'),
	('LEO0001', 'ProductStatus'),
	('LEO0001', 'ProductType'),
	('LEO0001', 'ProgramType'),
	('LEO0001', 'Project'),
	('LEO0001', 'RequestForInformationResultType'),
	('LEO0001', 'RequestForInformationStatus'),
	('LEO0001', 'ResourceProvider'),
	('LEO0001', 'Role'),
	('LEO0001', 'SourceCategory'),
	('LEO0001', 'SourceType'),
	('LEO0001', 'StatusChange'),
	('LEO0001', 'SubContractorBusinessType'),
	('LEO0001', 'SubContractorRelationshipType'),
	('LEO0001', 'TargetAudience'),
	('LEO0001', 'UnitType'),
	('LEO0001', 'VettingOutcome')
GO

INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0002', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0003', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0004', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdownData
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdownData'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.ImplementerDropdownData
	(
	ImplementerDropdownDataID INT IDENTITY(1,1) NOT NULL,
	ImplementerCode VARCHAR(25),
	DropdownCode VARCHAR(50),
	ProjectID INT,
	DropdownID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DropdownID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementerDropdownDataID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementerDropdownData', 'ImplementerCode,DropdownCode,ProjectID,DropdownID'
GO

--Begin table implementer.ImplementerDropdownData
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT ''' + ID.ImplementerCode + ''', '''
			+ T.Name 
			+ ''', D.' 
			+ T.Name 
			+ 'ID FROM ' 
			+ S.Name 
			+ '.' 
			+ T.Name 
			+ ' D' AS SQLText
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
			AND S.Name = 'dropdown'
			AND T.Name NOT IN ('DocumentCategory','Project')
	ORDER BY T.Name

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText)

	FETCH oCursor INTO @cSQLText

	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0001', 'Project', 1)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0002', 'Project', 2)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0003', 'Project', 3)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0004', 'Project', 4)
GO

INSERT INTO implementer.ImplementerDropdownData 
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO000' + CAST(P.ProjectID AS VARCHAR(10)),
	'Territory',
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P
	CROSS APPLY territory.Territory T
		WHERE P.ProjectID > 0
GO
--End table implementer.ImplementerDropdownData

--Begin table implementer.Synonym
DECLARE @TableName VARCHAR(250) = 'implementer.Synonym'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.Synonym
	(
	SynonymID INT IDENTITY(1,1) NOT NULL,
	BaseObjectSchema VARCHAR(50),
	BaseObjectName VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'SynonymID'
GO

INSERT INTO implementer.Synonym
	(BaseObjectSchema, BaseObjectName)
VALUES
	('core', 'EntityType'),
	('core', 'FormatDate'),
	('core', 'FormatDateTime'),
	('core', 'FormatTime'),
	('core', 'GetEntityTypeNameByEntityTypeCode'),
	('core', 'GetSystemSetupValueBySetupKey'),
	('core', 'ListToTable'),
	('core', 'MenuItem'),
	('core', 'MenuItemPermissionableLineage'),
	('document', 'FileType'),
	('document', 'FormatPhysicalFileSize'),
	('dropdown', 'CountryCallingCode'),
	('dropdown', 'GetControllerData'),
	('dropdown', 'GetEntityTypeName'),
	('dropdown', 'GetMethodData'),
	('dropdown', 'GetTerritoryTypeData'),
	('dropdown', 'GetProjectNameByProjectID'),
	('implementer', 'ImplementerDropdownData'),
	('permissionable', 'GetPermissionableByPermissionableID'),
	('permissionable', 'GetPermissionableGroups'),
	('permissionable', 'Permissionable'),
	('permissionable', 'PermissionableGroup'),
	('syslog', 'ApplicationErrorLog'),
	('syslog', 'DuoWebTwoFactorLog'),
	('territory', 'FormatTerritoryNameByTerritoryID'),
	('territory', 'GetDescendantTerritoriesByByTerritoryID'),
	('territory', 'Territory'),
	('territory', 'TerritoryType')
GO

INSERT INTO implementer.Synonym
	(BaseObjectSchema, BaseObjectName)
SELECT 
	S.Name, 
	T.Name
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
		AND S.Name = 'dropdown'
		AND T.Name <> 'DocumentCategory'

UNION

SELECT 
	S.Name,
	'Get' + T.Name + 'Data'
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
		AND S.Name = 'dropdown'
		AND T.Name <> 'DocumentCategory'

ORDER BY 2
--End table implementer.Synonym

--End file LEO0000 - 01 - Tables.sql

--Begin file LEO0000 - 02 - Functions.sql
USE LEO0000
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.11.13
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Author:			Todd Pires
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Author:			Todd Pires
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use XML - based on a script by Kshitij Satpute from SQLServerCentral.com
--
-- Author:			Todd Pires
-- Update date:	2015.08.02
-- Description:	Converted to NVARCHAR
-- ===================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.GetSystemSetupValueBySetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a SetupValue for a SetupKey from the core.SystemSetup table
-- =============================================================================================
CREATE FUNCTION core.GetSystemSetupValueBySetupKey
(
@SetupKey VARCHAR(250),
@DefaultSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSetupValue VARCHAR(MAX)
	
	SELECT @cSetupValue = S.SetupValue 
	FROM core.SystemSetup S
	WHERE S.SetupKey = @SetupKey
	
	RETURN ISNULL(@cSetupValue, @DefaultSetupValue)

END
GO
--End function core.GetSystemSetupValueBySetupKey

--Begin function document.FormatPhysicalFileSize
EXEC utility.DropObject 'document.FormatPhysicalFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatPhysicalFileSize
(
@PhysicalFileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @PhysicalFileSize < 1000
		THEN CAST(@PhysicalFileSize as VARCHAR(50)) + ' b'
		WHEN @PhysicalFileSize < 1000000
		THEN CAST(ROUND((@PhysicalFileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @PhysicalFileSize < 1000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @PhysicalFileSize < 1000000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatPhysicalFileSize

--Begin function territory.FormatTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.FormatTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the formatted name of a territory
-- ===================================================================

CREATE FUNCTION territory.FormatTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cTerritoryNameFormatted VARCHAR(300) = ''
	
	SELECT @cTerritoryNameFormatted = T.TerritoryName + ' (' + TT.TerritoryTypeName + ')' 
	FROM territory.Territory T 
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryNameFormatted, '')
	
END
GO
--End function territory.FormatTerritoryNameByTerritoryID

--Begin function territory.GetDescendantTerritoriesByByTerritoryID
EXEC utility.DropObject 'territory.GetDescendantTerritoriesByByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the descendant territories of a territory
-- ===========================================================================

CREATE FUNCTION territory.GetDescendantTerritoriesByByTerritoryID
(
@TerritoryID INT
)

RETURNS @tTable TABLE (TerritoryID INT NOT NULL PRIMARY KEY)

AS
BEGIN

	WITH HD (ParentTerritoryID,TerritoryID,NodeLevel) AS
		(
		SELECT
			T.ParentTerritoryID,
			T.TerritoryID,
			1
		FROM territory.Territory T
		WHERE T.ParentTerritoryID = @TerritoryID

		UNION ALL

		SELECT
			HD.TerritoryID AS ParentTerritoryID,
			T.TerritoryID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM territory.Territory T
				JOIN HD ON HD.TerritoryID = T.ParentTerritoryID
		)	

	INSERT INTO @tTable
		(TerritoryID)
	SELECT @TerritoryID

	UNION

	SELECT HD.TerritoryID
	FROM HD

	RETURN
	
END
GO
--End function territory.GetDescendantTerritoriesByByTerritoryID
--End file LEO0000 - 02 - Functions.sql

--Begin file LEO0000 - 03 - Procedures - Dropdown.sql
USE LEO0000
GO

--Begin procedure dropdown.GetActivityTypeData
EXEC Utility.DropObject 'dropdown.GetActivityTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetActivityTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityTypeID, 
		T.ActivityTypeName
	FROM dropdown.ActivityType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ActivityTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ActivityType'
			AND (T.ActivityTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityTypeName, T.ActivityTypeID

END
GO
--End procedure dropdown.GetActivityTypeData

--Begin procedure dropdown.GetAreaOfOperationTypeData
EXEC Utility.DropObject 'dropdown.GetAreaOfOperationTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AreaOfOperationType table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetAreaOfOperationTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AreaOfOperationTypeID, 
		T.AreaOfOperationTypeName,
		T.HexColor
	FROM dropdown.AreaOfOperationType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AreaOfOperationTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AreaOfOperationType'
			AND (T.AreaOfOperationTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AreaOfOperationTypeName, T.AreaOfOperationTypeID

END
GO
--End procedure dropdown.GetAreaOfOperationTypeData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AssetTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AssetType'
			AND (T.AssetTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetAtmosphericTypeData
EXEC Utility.DropObject 'dropdown.GetAtmosphericTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AtmosphericType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetAtmosphericTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AtmosphericTypeID, 
		T.AtmosphericTypeName
	FROM dropdown.AtmosphericType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AtmosphericTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AtmosphericType'
			AND (T.AtmosphericTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AtmosphericTypeName, T.AtmosphericTypeID

END
GO
--End procedure dropdown.GetAtmosphericTypeData

--Begin procedure dropdown.GetCommunicationThemeData
EXEC Utility.DropObject 'dropdown.GetCommunicationThemeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.CommunicationTheme table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCommunicationThemeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunicationThemeID, 
		T.CommunicationThemeName
	FROM dropdown.CommunicationTheme T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CommunicationThemeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CommunicationTheme'
			AND (T.CommunicationThemeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunicationThemeName, T.CommunicationThemeID

END
GO
--End procedure dropdown.GetCommunicationThemeData

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConfidenceLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConfidenceLevel'
			AND (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetContactStatusData
EXEC Utility.DropObject 'dropdown.GetContactStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetContactStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactStatusID,
		T.ContactStatusName
	FROM dropdown.ContactStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactStatus'
			AND (T.ContactStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactStatusName, T.ContactStatusID

END
GO
--End procedure dropdown.GetContactStatusData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID,
		T.ContactTypeName
	FROM dropdown.ContactType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactType'
			AND (T.ContactTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin procedure dropdown.GetCountryCallingDataByCountryCallingCodeID
EXEC Utility.DropObject 'dropdown.GetCountryCallingDataByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country and dropdown.CountryCallingCode tables
-- ===============================================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingDataByCountryCallingCodeID

@CountryCallingCodeID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.ISOCountryCode2, 
		CCC.CountryCallingCode
	FROM dropdown.CountryCallingCode CCC
		JOIN dropdown.Country T ON T.CountryID = CCC.CountryID
			AND CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetCountryCallingDataByCountryCallingCodeID

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetCourseTypeData
EXEC Utility.DropObject 'dropdown.GetCourseTypeData'
GO
--End procedure dropdown.GetCourseTypeData

--Begin procedure dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CurrencyID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Currency'
			AND (T.CurrencyID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetDateFilterData
EXEC Utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DateFilterID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DateFilter'
			AND (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetDistributorTypeData
EXEC Utility.DropObject 'dropdown.GetDistributorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DistributorType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetDistributorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DistributorTypeID, 
		T.DistributorTypeName,
		T.DistributorTypeCode
	FROM dropdown.DistributorType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DistributorTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DistributorType'
			AND (T.DistributorTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DistributorTypeName, T.DistributorTypeID

END
GO
--End procedure dropdown.GetDistributorTypeData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID,
		DC.DocumentCategoryID,
		DC.DocumentCategoryName,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
		JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = T.DocumentCategoryID
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DocumentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DocumentType'
			AND (T.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY DC.DisplayOrder, DC.DocumentCategoryName, T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetEngagementStatusData
EXEC Utility.DropObject 'dropdown.GetEngagementStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.EngagementStatus table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetEngagementStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementStatusID,
		T.EngagementStatusName
	FROM dropdown.EngagementStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EngagementStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EngagementStatus'
			AND (T.EngagementStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementStatusName, T.EngagementStatusID

END
GO
--End procedure dropdown.GetEngagementStatusData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the core.EntityType table
-- =============================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	WHERE T.HasWorkflow = 1
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEquipmentCatalogData
EXEC Utility.DropObject 'dropdown.GetEquipmentCatalogData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the procurement.EquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetEquipmentCatalogData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentCatalogID, 
		T.ItemName AS EquipmentCatalogName
	FROM procurement.EquipmentCatalog T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EquipmentCatalogID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EquipmentCatalog'
			AND (T.EquipmentCatalogID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ItemName, T.EquipmentCatalogID

END
GO
--End procedure dropdown.GetEquipmentCatalogData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.GetEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure dropdown.GetFacebookPageData
EXEC Utility.DropObject 'dropdown.GetFacebookPageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FacebookPage table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetFacebookPageData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ID,
		T.Name
	FROM integration.FacebookPage T
	WHERE (T.ID > 0 OR @IncludeZero = 1)
	ORDER BY T.Name, T.ID

END
GO
--End procedure dropdown.GetFacebookPageData

--Begin procedure dropdown.GetFindingStatusData
EXEC Utility.DropObject 'dropdown.GetFindingStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFindingStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingStatusID,
		T.FindingStatusName
	FROM dropdown.FindingStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingStatus'
			AND (T.FindingStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingStatusName, T.FindingStatusID

END
GO
--End procedure dropdown.GetFindingStatusData

--Begin procedure dropdown.GetFindingTypeData
EXEC Utility.DropObject 'dropdown.GetFindingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetFindingTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingTypeID,
		T.FindingTypeName
	FROM dropdown.FindingType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingType'
			AND (T.FindingTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingTypeName, T.FindingTypeID

END
GO
--End procedure dropdown.GetFindingTypeData

--Begin procedure dropdown.GetFundingSourceData
EXEC Utility.DropObject 'dropdown.GetFundingSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FundingSource table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFundingSourceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FundingSourceID, 
		T.FundingSourceName
	FROM dropdown.FundingSource T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FundingSourceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FundingSource'
			AND (T.FundingSourceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FundingSourceName, T.FundingSourceID

END
GO
--End procedure dropdown.GetFundingSourceData

--Begin procedure dropdown.GetImpactDecisionData
EXEC Utility.DropObject 'dropdown.GetImpactDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ImpactDecision table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetImpactDecisionData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImpactDecisionID,
		T.ImpactDecisionName,
		T.HexColor
	FROM dropdown.ImpactDecision T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ImpactDecisionID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ImpactDecision'
			AND (T.ImpactDecisionID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImpactDecisionName, T.ImpactDecisionID

END
GO
--End procedure dropdown.GetImpactDecisionData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID, 
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentType'
			AND (T.IncidentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.IncidentTypeCategory,T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure dropdown.GetIndicatorTypeData
EXEC Utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IndicatorType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIndicatorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorTypeID, 
		T.IndicatorTypeName,
		T.IsActive
	FROM dropdown.IndicatorType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IndicatorTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IndicatorType'
			AND (T.IndicatorTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorTypeName, T.IndicatorTypeID

END
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure dropdown.GetInformationValidityData
EXEC Utility.DropObject 'dropdown.GetInformationValidityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.InformationValidity table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetInformationValidityData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InformationValidityID, 
		T.InformationValidityName
	FROM dropdown.InformationValidity T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.InformationValidityID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'InformationValidity'
			AND (T.InformationValidityID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InformationValidityName, T.InformationValidityID

END
GO
--End procedure dropdown.GetInformationValidityData

--Begin procedure dropdown.GetLearnerProfileTypeData
EXEC Utility.DropObject 'dropdown.GetLearnerProfileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LearnerProfileType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetLearnerProfileTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LearnerProfileTypeID,
		T.LearnerProfileTypeName
	FROM dropdown.LearnerProfileType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LearnerProfileTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LearnerProfileType'
			AND (T.LearnerProfileTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LearnerProfileTypeName, T.LearnerProfileTypeID

END
GO
--End procedure dropdown.GetLearnerProfileTypeData

--Begin procedure dropdown.GetLogicalFrameworkStatusData
EXEC Utility.DropObject 'dropdown.GetLogicalFrameworkStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LogicalFrameworkStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetLogicalFrameworkStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LogicalFrameworkStatusID,
		T.LogicalFrameworkStatusName
	FROM dropdown.LogicalFrameworkStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LogicalFrameworkStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LogicalFrameworkStatus'
			AND (T.LogicalFrameworkStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LogicalFrameworkStatusName, T.LogicalFrameworkStatusID

END
GO
--End procedure dropdown.GetLogicalFrameworkStatusData

--Begin procedure dropdown.GetMediaReportSourceTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportSourceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetMediaReportSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportSourceTypeID,
		T.MediaReportSourceTypeName,
		T.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		T.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM dropdown.MediaReportSourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportSourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportSourceType'
			AND (T.MediaReportSourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportSourceTypeName, T.MediaReportSourceTypeID

END
GO
--End procedure dropdown.GetMediaReportSourceTypeData

--Begin procedure dropdown.GetMediaReportTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetMediaReportTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportTypeID,
		T.MediaReportTypeName
	FROM dropdown.MediaReportType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportType'
			AND (T.MediaReportTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportTypeName, T.MediaReportTypeID

END
GO
--End procedure dropdown.GetMediaReportTypeData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin procedure dropdown.GetModuleTypeData
EXEC Utility.DropObject 'dropdown.GetModuleTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ModuleType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetModuleTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ModuleTypeID,
		T.ModuleTypeName
	FROM dropdown.ModuleType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ModuleTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ModuleType'
			AND (T.ModuleTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ModuleTypeName, T.ModuleTypeID

END
GO
--End procedure dropdown.GetModuleTypeData

--Begin procedure dropdown.GetObjectiveTypeData
EXEC Utility.DropObject 'dropdown.GetObjectiveTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ObjectiveType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetObjectiveTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ObjectiveTypeID, 
		T.ObjectiveTypeCode,
		T.ObjectiveTypeName
	FROM dropdown.ObjectiveType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ObjectiveTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ObjectiveType'
			AND (T.ObjectiveTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ObjectiveTypeName, T.ObjectiveTypeID

END
GO
--End procedure dropdown.GetObjectiveTypeData

--Begin procedure dropdown.GetProductOriginData
EXEC Utility.DropObject 'dropdown.GetProductOriginData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductOrigin table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductOriginData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductOriginID,
		T.ProductOriginName
	FROM dropdown.ProductOrigin T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductOriginID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductOrigin'
			AND (T.ProductOriginID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductOriginName, T.ProductOriginID

END
GO
--End procedure dropdown.GetProductOriginData

--Begin procedure dropdown.GetProductStatusData
EXEC Utility.DropObject 'dropdown.GetProductStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductStatusID, 
		T.ProductStatusName
	FROM dropdown.ProductStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductStatus'
			AND (T.ProductStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductStatusName, T.ProductStatusID

END
GO
--End procedure dropdown.GetProductStatusData

--Begin procedure dropdown.GetProductTypeData
EXEC Utility.DropObject 'dropdown.GetProductTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProductTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductTypeID, 
		T.ProductTypeName
	FROM dropdown.ProductType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductType'
			AND (T.ProductTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductTypeName, T.ProductTypeID

END
GO
--End procedure dropdown.GetProductTypeData

--Begin procedure dropdown.GetProgramTypeData
EXEC Utility.DropObject 'dropdown.GetProgramTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProgramType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProgramTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProgramTypeID, 
		T.ProgramTypeName
	FROM dropdown.ProgramType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProgramTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProgramType'
			AND (T.ProgramTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProgramTypeName, T.ProgramTypeID

END
GO
--End procedure dropdown.GetProgramTypeData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName
	FROM dropdown.Project T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProjectID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Project'
			AND (T.ProjectID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetRequestForInformationResultTypeData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationResultTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationResultType table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationResultTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationResultTypeID, 
		T.RequestForInformationResultTypeCode,
		T.RequestForInformationResultTypeName
	FROM dropdown.RequestForInformationResultType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationResultTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationResultType'
			AND (T.RequestForInformationResultTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationResultTypeName, T.RequestForInformationResultTypeID

END
GO
--End procedure dropdown.GetRequestForInformationResultTypeData

--Begin procedure dropdown.GetRequestForInformationStatusData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationStatusID, 
		T.RequestForInformationStatusCode,
		T.RequestForInformationStatusName
	FROM dropdown.RequestForInformationStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationStatus'
			AND (T.RequestForInformationStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationStatusName, T.RequestForInformationStatusID

END
GO
--End procedure dropdown.GetRequestForInformationStatusData

--Begin procedure dropdown.GetResourceProviderData
EXEC Utility.DropObject 'dropdown.GetResourceProviderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ResourceProvider table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetResourceProviderData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResourceProviderID, 
		T.ResourceProviderName
	FROM dropdown.ResourceProvider T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ResourceProviderID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ResourceProvider'
			AND (T.ResourceProviderID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ResourceProviderName, T.ResourceProviderID

END
GO
--End procedure dropdown.GetResourceProviderData

--Begin procedure dropdown.GetRiskCategoryData
EXEC Utility.DropObject 'dropdown.GetRiskCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RiskCategory table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetRiskCategoryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskCategoryID,
		T.RiskCategoryName
	FROM dropdown.RiskCategory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RiskCategoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RiskCategory'
			AND (T.RiskCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskCategoryName, T.RiskCategoryID

END
GO
--End procedure dropdown.GetRiskCategoryData

--Begin procedure dropdown.GetRiskStatusData
EXEC Utility.DropObject 'dropdown.GetRiskStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RiskStatus table
-- =================================================================================
CREATE PROCEDURE dropdown.GetRiskStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskStatusID,
		T.RiskStatusName
	FROM dropdown.RiskStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RiskStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RiskStatus'
			AND (T.RiskStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskStatusName, T.RiskStatusID

END
GO
--End procedure dropdown.GetRiskStatusData

--Begin procedure dropdown.GetRiskTypeData
EXEC Utility.DropObject 'dropdown.GetRiskTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RiskType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetRiskTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskTypeID,
		T.RiskTypeName
	FROM dropdown.RiskType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RiskTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RiskType'
			AND (T.RiskTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskTypeName, T.RiskTypeID

END
GO
--End procedure dropdown.GetRiskTypeData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleName
	FROM dropdown.Role T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RoleID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Role'
			AND (T.RoleID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSourceCategoryData
EXEC Utility.DropObject 'dropdown.GetSourceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetSourceCategoryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceCategoryID,
		T.SourceCategoryName
	FROM dropdown.SourceCategory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceCategoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceCategory'
			AND (T.SourceCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceCategoryName, T.SourceCategoryID

END
GO
--End procedure dropdown.GetSourceCategoryData

--Begin procedure dropdown.GetSourceTypeData
EXEC Utility.DropObject 'dropdown.GetSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceTypeID, 
		T.SourceTypeName
	FROM dropdown.SourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceType'
			AND (T.SourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceTypeName, T.SourceTypeID

END
GO
--End procedure dropdown.GetSourceTypeData

--Begin procedure dropdown.GetStatusChangeData
EXEC Utility.DropObject 'dropdown.GetStatusChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.StatusChange table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetStatusChangeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusChangeID,
		T.StatusChangeName
	FROM dropdown.StatusChange T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.StatusChangeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'StatusChange'
			AND (T.StatusChangeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusChangeName, T.StatusChangeID

END
GO
--End procedure dropdown.GetStatusChangeData

--Begin procedure dropdown.GetSubContractorBusinessTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorBusinessTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorBusinessType table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorBusinessTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorBusinessTypeID, 
		T.SubContractorBusinessTypeName
	FROM dropdown.SubContractorBusinessType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorBusinessTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorBusinessType'
			AND (T.SubContractorBusinessTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorBusinessTypeName, T.SubContractorBusinessTypeID

END
GO
--End procedure dropdown.GetSubContractorBusinessTypeData

--Begin procedure dropdown.GetSubContractorRelationshipTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorRelationshipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorRelationshipType table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorRelationshipTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorRelationshipTypeID, 
		T.SubContractorRelationshipTypeName
	FROM dropdown.SubContractorRelationshipType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorRelationshipTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorRelationshipType'
			AND (T.SubContractorRelationshipTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorRelationshipTypeName, T.SubContractorRelationshipTypeID

END
GO
--End procedure dropdown.GetSubContractorRelationshipTypeData

--Begin procedure dropdown.GetTargetAudienceData
EXEC Utility.DropObject 'dropdown.GetTargetAudienceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.TargetAudience table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetTargetAudienceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TargetAudienceID, 
		T.TargetAudienceName
	FROM dropdown.TargetAudience T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TargetAudienceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TargetAudience'
			AND (T.TargetAudienceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TargetAudienceName, T.TargetAudienceID

END
GO
--End procedure dropdown.GetTargetAudienceData

--Begin procedure dropdown.GetTerritoryTypeData
EXEC Utility.DropObject 'dropdown.GetTerritoryTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.15
-- Description:	A stored procedure to return data from the dropdown.TerritoryType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetTerritoryTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryTypeID,
		T.TerritoryTypeCode,
		T.TerritoryTypeName
	FROM territory.TerritoryType T
	ORDER BY T.TerritoryTypeName, T.TerritoryTypeID

END
GO
--End procedure dropdown.GetTerritoryTypeData

--Begin procedure dropdown.GetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.UnitType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetUnitTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnitTypeID, 
		T.UnitTypeName
	FROM dropdown.UnitType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.UnitTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'UnitType'
			AND (T.UnitTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.UnitTypeName, T.UnitTypeID

END
GO
--End procedure dropdown.GetUnitTypeData

--Begin procedure dropdown.GetVettingOutcomeData
EXEC Utility.DropObject 'dropdown.GetVettingOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.VettingOutcome table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetVettingOutcomeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VettingOutcomeID,
		T.VettingOutcomeName
	FROM dropdown.VettingOutcome T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.VettingOutcomeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'VettingOutcome'
			AND (T.VettingOutcomeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VettingOutcomeName, T.VettingOutcomeID

END
GO
--End procedure dropdown.GetVettingOutcomeData
--End file LEO0000 - 03 - Procedures - Dropdown.sql

--Begin file LEO0000 - 03 - Procedures - Other.sql
USE LEO0000
GO

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		S.SystemSetupID, 
		S.SetupKey, 
		S.SetupValue 
	FROM core.SystemSetup S
	WHERE S.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySetupKey
EXEC utility.DropObject 'core.GetSystemSetupValuesBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySetupKey

@SetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT S.SetupValue 
	FROM core.SystemSetup S 
	WHERE S.SetupKey = @SetupKey
	ORDER BY S.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySetupKey

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE core.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('Permissionable'), ELEMENTS
			)
		FROM permissionable.Permissionable T 
		WHERE T.PermissionableID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableAction

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Brandon Green
-- Create date:	2015.10.02
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to get data from the permissionable.PermissionableGroup table
-- =============================================================================================
CREATE PROCEDURE permissionable.GetPermissionableGroups

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PG.PermissionableGroupID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		PG.DisplayOrder,
		(SELECT COUNT(P.PermissionableGroupID) FROM permissionable.Permissionable P WHERE P.PermissionableGroupID = PG.PermissionableGroupID) AS ItemCount
	FROM permissionable.PermissionableGroup PG
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID
		
END
GO
--End procedure permissionable.GetPermissionableGroups
--End file LEO0000 - 03 - Procedures - Other.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2017.01.22 21.35.47')
GO
--End build tracking

