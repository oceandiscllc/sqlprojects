-- File Name:	Build - 1.3 - LEO0003.sql
-- Build Key:	Build - 1.3 - 2017.02.05 15.53.53

USE [LEO0003]
GO

-- ==============================================================================================================================
-- Functions:
--		contact.GetContactVettingData
--		portalupdate.GetColumnList
--		workflow.GetWorkflowStepCount
--		workflow.GetWorkflowStepNumber
--
-- Procedures:
--		activity.GetActivityByActivityID
--		activityreport.GetActivityReportByActivityReportID
--		asset.GetAssetByAssetID
--		atmospheric.GetAtmosphericByAtmosphericID
--		contact.GetContactByContactID
--		contact.GetSubContractorBySubContractorID
--		core.EntityTypeAddUpdate
--		core.GetMenuItemsByPersonID
--		course.GetCourseByCourseID
--		dbo.GetCampaignByCampaignID
--		dbo.GetIncidentByIncidentID
--		dbo.GetModuleByModuleID
--		dbo.GetRequestForInformationByRequestForInformationID
--		distributor.GetDistributorByDistributorID
--		document.GetDocumentByDocumentID
--		document.GetDocumentsByEntityTypeCodeAndEntityID
--		document.GetEntityDocuments
--		eventlog.LogCampaignAction
--		eventlog.LogCourseAction
--		eventlog.LogDistributorAction
--		eventlog.LogImpactStoryAction
--		eventlog.LogModuleAction
--		eventlog.LogProductAction
--		finding.GetFindingByFindingID
--		force.GetForceByForceID
--		force.GetForceUnitByForceUnitID
--		impactstory.GetImpactStoryByImpactStoryID
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetIndicatorByObjectiveID
--		logicalframework.GetMilestoneByIndicatorID
--		logicalframework.GetMilestoneByMilestoneID
--		logicalframework.GetMilestoneDataByIndicatorID
--		logicalframework.GetObjectiveByObjectiveID
--		logicalframework.GetObjectiveByParentObjectiveID
--		mediareport.GetMediaReportByMediaReportID
--		person.GetPersonByPersonToken
--		person.ValidateLogin
--		portalupdate.UpdateDataBySchemaName
--		portalupdate.UpdateDataBySchemaNameAndTableName
--		procurement.GetEquipmentCatalogByEquipmentCatalogID
--		product.GetProductByProductID
--		product.GetProductDistributionCounts
--		recommendation.GetRecommendationByRecommendationID
--		reporting.GetProductListByPersonID
--		spotreport.GetSpotReportBySpotReportID
--		territory.GetTerritoryByTerritoryID
--		trendreport.GetTrendReportByTrendReportID
--		utility.AddColumn
--		utility.DropPKConstraint
--		workflow.DecrementWorkflow
--		workflow.GetEntityWorkflowData
--		workflow.GetEntityWorkflowPeople
--		workflow.IncrementWorkflow
--		workflow.IsPersonInCurrentWorkflowStep
--
-- Schemas:
--		budget
--		course
--		distributor
--		impactstory
--		portalupdate
--		product
-- ==============================================================================================================================

--Begin file LEO000X - 00 - Prerequisite.sql
USE LEO0003
GO

EXEC utility.AddSchema 'budget'
EXEC utility.AddSchema 'course'
EXEC utility.AddSchema 'distributor'
EXEC utility.AddSchema 'impactstory'
EXEC utility.AddSchema 'portalupdate'
EXEC utility.AddSchema 'product'
GO

EXEC utility.DropSchema 'api'
EXEC utility.DropSchema 'facebook'
EXEC utility.DropSchema 'programreport'
EXEC utility.DropSchema 'survey'
EXEC utility.DropSchema 'temp'
EXEC utility.DropSchema 'territoryupdate'
GO

--Begin procedure utility.DropPKConstraint
EXEC utility.DropObject 'utility.DropPKConstraint'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.01.28
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropPKConstraint

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX) = NULL

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + KC.Name + ''
	FROM sys.key_constraints KC
	WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	IF @cSQL IS NOT NULL
		EXEC (@cSQL);
	--ENDIF

END
GO
--End procedure utility.DropPKConstraint

--End file LEO000X - 00 - Prerequisite.sql

--Begin file LEO000X - 01 - Tables.sql
USE LEO0003
GO

--Begin table portalupdate.PortalUpdateLog
EXEC utility.DropObject 'portalupdate.PortalUpdateLog'
GO
--End table portalupdate.PortalUpdateLog

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Campaign')
	BEGIN

	ALTER SCHEMA dbo TRANSFER productdistributor.Campaign

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Distributor')
	BEGIN

	ALTER SCHEMA distributor TRANSFER productdistributor.Distributor
	ALTER SCHEMA distributor TRANSFER productdistributor.DistributorTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'ImpactStory')
	BEGIN

	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStory
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryCampaign
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryProduct
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Product')
	BEGIN

	ALTER SCHEMA product TRANSFER productdistributor.Product
	ALTER SCHEMA product TRANSFER productdistributor.ProductCampaign
	ALTER SCHEMA product TRANSFER productdistributor.ProductCommunicationTheme
	ALTER SCHEMA product TRANSFER productdistributor.ProductDistribution
	ALTER SCHEMA product TRANSFER productdistributor.ProductTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'training' AND T.Name = 'Course')
	BEGIN

	ALTER SCHEMA course TRANSFER training.Course
	ALTER SCHEMA course TRANSFER training.CourseContact

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'training' AND T.Name = 'Module')
	BEGIN

	ALTER SCHEMA dbo TRANSFER training.Module

	END
--ENDIF
GO

EXEC utility.DropIndex 'asset.Asset', 'IX_Asset';
EXEC utility.DropPKConstraint 'asset.Asset';
EXEC utility.SetPrimaryKeyClustered 'asset.Asset', 'AssetID, ProjectID';
GO

EXEC utility.DropIndex 'atmospheric.AtmosphericSource', 'IX_AtmosphericSource';
EXEC utility.SetIndexClustered 'atmospheric.AtmosphericSource', 'IX_AtmosphericSource', 'AtmosphericID';
GO

EXEC utility.DropIndex 'force.Force', 'IX_Force';
EXEC utility.SetIndexClustered 'force.Force', 'IX_Force', 'ForceName';
GO

EXEC utility.DropIndex 'dbo.Campaign', 'IX_Campaign';
EXEC utility.DropPKConstraint 'dbo.Campaign';
EXEC utility.SetPrimaryKeyNonClustered 'dbo.Campaign', 'CampaignID, ProjectID';
EXEC utility.SetIndexClustered 'dbo.Campaign', 'IX_Campaign', 'CampaignName';
GO

EXEC utility.DropIndex 'distributor.Distributor', 'IX_Distributor';
EXEC utility.SetIndexClustered 'distributor.Distributor', 'IX_Distributor', 'DistributorName';
GO

EXEC utility.DropIndex 'impactstory.ImpactStory', 'IX_ImpactStory';
EXEC utility.SetIndexClustered 'impactstory.ImpactStory', 'IX_ImpactStory', 'ImpactStoryName';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryCampaign';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryCampaign', 'ImpactStoryCampaignID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryCampaign', 'IX_ImpactStoryCampaign';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryCampaign', 'IX_ImpactStoryCampaign', 'ImpactStoryID,CampaignID';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryProduct';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryProduct', 'ImpactStoryProductID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryProduct', 'IX_ImpactStoryProduct';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryProduct', 'IX_ImpactStoryProduct', 'ImpactStoryID,ProductID';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryTerritory';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryTerritory', 'ImpactStoryTerritoryID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryTerritory', 'IX_ImpactStoryTerritory';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryTerritory', 'IX_ImpactStoryTerritory', 'ImpactStoryID,TerritoryID';
GO

EXEC utility.DropIndex 'mediareport.MediaReportSource', 'IX_MediaReportSource';
EXEC utility.SetIndexClustered 'mediareport.MediaReportSource', 'IX_MediaReportSource', 'MediaReportID';
GO

EXEC utility.DropIndex 'procurement.EquipmentCatalog', 'IX_EquipmentCatalog';
EXEC utility.SetIndexClustered 'procurement.EquipmentCatalog', 'IX_EquipmentCatalog', 'ItemName';
GO

EXEC utility.AddColumn 'activity.ActivityContact', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activity.ActivityCourse', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activity.ActivityTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportAsset', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportAtmospheric', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportForce', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportImpactStory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportMediaReport', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportProduct', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'asset.AssetEquipmentResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'asset.AssetFinancialResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'atmospheric.AtmosphericSource', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'atmospheric.AtmosphericTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.ContactContactType', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.ContactVetting', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.SubContractorSubContractorCapability', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'course.Course', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'course.CourseContact', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Campaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Incident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Module', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.RequestForInformation', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'distributor.Distributor', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'distributor.DistributorTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingIndicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingRecommendation', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceEquipmentResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceFinancialResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceUnit', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryProduct', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Indicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Milestone', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Objective', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'mediareport.MediaReportSource', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentCatalog', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentInventory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.Product', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductCommunicationTheme', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductDistribution', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'recommendation.RecommendationIndicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'recommendation.RecommendationTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'spotreport.SpotReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'spotreport.SpotReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportAsset', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportForce', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportMediaReport', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStep', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStepGroup', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'ProjectID', 'INT', '0'
GO

--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0003
GO

EXEC utility.DropObject 'workflow.CanHaveListView'
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

--Begin function contact.GetContactVettingData
EXEC utility.DropObject 'contact.GetContactVettingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get the most recent set of contact vetting data from contact.ContactVetting table
-- ============================================================================================================
CREATE FUNCTION contact.GetContactVettingData
(
)

RETURNS @tTable TABLE 
	(
	ContactID INT NOT NULL PRIMARY KEY, 
	VettingDate DATE,
	VettingOutcomeID INT,
	VettingOutcomeName VARCHAR(50)
	)

AS
BEGIN

	INSERT INTO @tTable
		(ContactID, VettingDate, VettingOutcomeID, VettingOutcomeName)
	SELECT
		VD.ContactID,
		CV2.VettingDate,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM 
		(
		SELECT 
			MAX(CV1.ContactVettingID) AS ContactVettingID,
			CV1.ContactID
		FROM contact.ContactVetting CV1
		GROUP BY CV1.ContactID
		) VD JOIN contact.ContactVetting CV2 ON CV2.ContactVettingID = VD.ContactVettingID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV2.VettingOutcomeID

	RETURN

END
GO
--End function contact.GetContactVettingData

--Begin function portalupdate.GetColumnList
EXEC utility.DropObject 'portalupdate.GetColumnList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a comma delimited list of column names for a specific table
-- ==========================================================================================

CREATE FUNCTION portalupdate.GetColumnList
(
@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@Prefix VARCHAR(10)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cColumnList VARCHAR(MAX) = ''

	SELECT
		@cColumnList = COALESCE(@cColumnList + @Prefix, '') + C.Name
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN sys.Columns C ON T.Object_ID = C.Object_ID
			AND S.Name = @SchemaName
			AND T.Name = @TableName
	ORDER BY C.Name

	RETURN STUFF(@cColumnList, 1, 2, '')

END
GO
--End function portalupdate.GetColumnList

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT

	SELECT @nWorkflowStepCount = MAX(EWSGP.WorkflowStepNumber) 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.ProjectID = @ProjectID
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT TOP 1 @nWorkflowStepNumber = EWSGP.WorkflowStepNumber 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID 
		AND EWSGP.ProjectID = @ProjectID 
		AND EWSGP.IsComplete = 0 
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber
END
GO
--End function workflow.GetWorkflowStepNumber

--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0003
GO

EXEC utility.DropObject 'contact.GetSubContractors'
EXEC utility.DropObject 'dbo.GetDocumentByDocumentID'
EXEC utility.DropObject 'dbo.GetFieldReportByFieldReportID'
EXEC utility.DropObject 'dropdown.GetCountryCallingDataByCountryCallingCodeID'
EXEC utility.DropObject 'dropdown.GetCourseTypeData'
EXEC utility.DropObject 'dropdown.GetEquipmentCatalogData'
EXEC utility.DropObject 'person.GetProjectsByPersonID'
EXEC Utility.DropObject 'portalupdate.UpdateData'
EXEC utility.DropObject 'productdistributor.GetDistributorByDistributorID'
EXEC utility.DropObject 'productdistributor.GetImpactStoryByImpactStoryID'
EXEC utility.DropObject 'productdistributor.GetProductByProductID'
EXEC utility.DropObject 'productdistributor.GetProductDistributionCounts'
EXEC utility.DropObject 'programreport.GetProgramReportByProgramReportID'
EXEC utility.DropObject 'training.GetCourseByCourseID'
EXEC utility.DropObject 'training.GetModuleByModuleID'
EXEC utility.DropObject 'training.GetModules'
GO

--Begin procedure activity.GetActivityByActivityID
EXEC Utility.DropObject 'activity.GetActivityByActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the activity.Activity table
-- ========================================================================
CREATE PROCEDURE activity.GetActivityByActivityID

@ActivityID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Activity', @ActivityID, @ProjectID)

	SELECT 
		A.ActivityID,
		A.ActivityName,
		A.ActivityTypeID,
		A.AwardeeSubContractorID1,
		A.AwardeeSubContractorID2,
		A.Background,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.FundingSourceID,
		A.Objectives,
		A.PointOfContactPersonID,
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		A.TaskCode,
		A.AwardeeSubContractorID1,
		(SELECT SC1.SubContractorName FROM contact.SubContractor SC1 WHERE SC1.SubContractorID = A.AwardeeSubContractorID1) AS AwardeeSubContractorName1,
		A.AwardeeSubContractorID2,
		(SELECT SC2.SubContractorName FROM contact.SubContractor SC2 WHERE SC2.SubContractorID = A.AwardeeSubContractorID2) AS AwardeeSubContractorName2,
		AT.ActivityTypeName,
		FS.FundingSourceName
	FROM activity.Activity A
		JOIN dropdown.ActivityType AT ON AT.ActivityTypeID = A.ActivityTypeID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = A.FundingSourceID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID

	SELECT
		AC.CourseID,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		M.ModuleName
	FROM activity.ActivityCourse AC
		JOIN course.Course C ON C.CourseID = AC.CourseID
		JOIN dbo.Module M on M.ModuleID = C.ModuleID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID
	ORDER BY M.ModuleName

	SELECT
		AC.VettingDate,
		core.FormatDate(AC.VettingDate) AS VettingDateFormatted,
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryNameFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM activity.ActivityContact AC
		JOIN contact.Contact C ON C.ContactID = AC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC.VettingOutcomeID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID

	SELECT
		AT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AT.TerritoryID) AS TerritoryName
	FROM activity.ActivityTerritory AT
	WHERE AT.ActivityID = @ActivityID
		AND AT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'Activity', @ActivityID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'Activity', @ActivityID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activity.Activity A ON A.ActivityID = EL.EntityID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'Activity'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure activity.GetActivityByActivityID

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID, @ProjectID)
	
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail,
		ART.ActivityReportTypeID,
		ART.ActivityReportTypeName
	FROM activityreport.ActivityReport AR
		JOIN dropdown.ActivityReportType ART ON ART.ActivityReportTypeID = AR.ActivityReportTypeID
			AND AR.ActivityReportID = @ActivityReportID
		AND AR.ProjectID = @ProjectID

	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY A.AssetName, A.AssetID

	SELECT
		A.AtmosphericID,
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted,
		ATT.AtmosphericTypeName
	FROM activityreport.ActivityReportAtmospheric ARA
		JOIN atmospheric.Atmospheric A ON A.AtmosphericID = ARA.AtmosphericID
		JOIN dropdown.AtmosphericType ATT ON ATT.AtmosphericTypeID = A.AtmosphericTypeID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY ATT.AtmosphericTypeName, A.AtmosphericDate

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM activityreport.ActivityReportCampaign ARC
		JOIN dbo.Campaign C ON C.CampaignID = ARC.CampaignID
			AND ARC.ActivityReportID = @ActivityReportID
			AND ARC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
			AND ARF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM activityreport.ActivityReportImpactStory ARI
		JOIN impactstory.ImpactStory I ON I.ImpactStoryID = ARI.ImpactStoryID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.ImpactStoryName, I.ImpactStoryID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN dbo.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM activityreport.ActivityReportMediaReport ARMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = ARMR.MediaReportID
			AND ARMR.ActivityReportID = @ActivityReportID
			AND ARMR.ProjectID = @ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	SELECT
		P.ProductID,
		P.ProductName
	FROM activityreport.ActivityReportProduct ARP
		JOIN product.Product P ON P.ProductID = ARP.ProductID
			AND ARP.ActivityReportID = @ActivityReportID
			AND ARP.ProjectID = @ProjectID
	ORDER BY P.ProductName, P.ProductID

	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
			AND ART.ActivityReportID = @ActivityReportID
			AND ART.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'ActivityReport', @ActivityReportID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'ActivityReport', @ActivityReportID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activityreport.ActivityReport AR ON AR.ActivityReportID = EL.EntityID
			AND AR.ActivityReportID = @ActivityReportID
			AND AR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'ActivityReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure asset.GetAssetByAssetID
EXEC utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the asset.Asset table
-- ==================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AssetDescription, 		
		A.AssetID, 		
		A.AssetName, 		
		A.Comments, 		
		A.DeputyManagerContactID, 		
		contact.FormatContactNameByContactID(A.DeputyManagerContactID, 'LastFirstMiddle') AS DeputyManagerFullName,		
		A.History, 		
		A.IsActive,
		A.LastUpdateDate,
		core.FormatDate(A.LastUpdateDate) AS LastUpdateDateFormatted,
		A.Location.STAsText() AS Location,
		A.ManagerContactID,		
		contact.FormatContactNameByContactID(A.ManagerContactID, 'LastFirstMiddle') AS ManagerFullName,		
		A.Notes, 		
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName,		
		A.WebLinks,		
		AT.AssetTypeID, 
		AT.AssetTypeName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID
			AND A.ProjectID = @ProjectID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetEquipmentResourceProvider AERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AERP.ResourceProviderID
			AND AERP.AssetID = @AssetID
			AND AERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetFinancialResourceProvider AFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AFRP.ResourceProviderID
			AND AFRP.AssetID = @AssetID
			AND AFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID
	
END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure atmospheric.GetAtmosphericByAtmosphericID
EXEC Utility.DropObject 'atmospheric.GetAtmosphericByAtmosphericID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the atmospheric.Atmospheric table
-- ==============================================================================
CREATE PROCEDURE atmospheric.GetAtmosphericByAtmosphericID

@AtmosphericID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AtmosphericDate, 
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID, 
		A.AtmosphericReportedDate, 
		core.FormatDate(A.AtmosphericReportedDate) AS AtmosphericReportedDateFormatted, 
		A.Information, 
		A.IsCritical, 
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.Recommendation, 
		AT.AtmosphericTypeID,
		AT.AtmosphericTypeName,
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
		JOIN dropdown.AtmosphericType AT ON AT.AtmosphericTypeID = A.AtmosphericTypeID
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = A.ConfidenceLevelID
			AND A.AtmosphericID = @AtmosphericID
			AND A.ProjectID = @ProjectID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS AtmosphericSourceGUID,
		ATS.AtmosphericSourceID, 
		ATS.SourceAttribution, 
		ATS.SourceDate, 
		core.FormatDate(ATS.SourceDate) AS SourceDateFormatted, 
		ATS.SourceName, 
		ATS.SourceSummary, 
		CL.ConfidenceLevelID AS SourceConfidenceLevelID, -- Aliased for the common source cfmodule
		CL.ConfidenceLevelName AS SourceConfidenceLevelName, -- Aliased for the common source cfmodule
		SC.SourceCategoryID, 
		SC.SourceCategoryName,
		ST.SourceTypeID, 
		ST.SourceTypeName
	FROM atmospheric.AtmosphericSource ATS
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = ATS.SourceConfidenceLevelID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = ATS.SourceCategoryID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = ATS.SourceTypeID
			AND ATS.AtmosphericID = @AtmosphericID
			AND ATS.ProjectID = @ProjectID
	ORDER BY ATS.SourceName, ATS.AtmosphericSourceID

	SELECT
		AC.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AC.TerritoryID) AS TerritoryName
	FROM atmospheric.AtmosphericTerritory AC
	WHERE AC.AtmosphericID = @AtmosphericID
		AND AC.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure atmospheric.GetAtmosphericByAtmosphericID

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
			AND C1.ContactID = @ContactID
			AND C1.ProjectID = @ProjectID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
			AND CCT.ProjectID = @ProjectID

	SELECT
		CV.VettingDate,
		core.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM contact.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
			AND CV.ProjectID = @ProjectID		
	ORDER BY CV.VettingDate DESC

	--Courses Attended
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM course.Course C
		JOIN course.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID
			AND CC.ProjectID = @ProjectID

	--Assigned Equipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND C.ProjectID = @ProjectID
			AND EI.AssigneeTypeCode = 'Contact'
			AND EI.ProjectID = 	C.ProjectID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure contact.GetSubContractorBySubContractorID
EXEC Utility.DropObject 'contact.GetSubContractorBySubContractorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.SubContractor table
-- ============================================================================
CREATE PROCEDURE contact.GetSubContractorBySubContractorID

@SubContractorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.CountryID AS AddressCountryID,
		C1.CountryName AS AddressCountryName,
		C2.CountryID AS RegistrationCountryID,
		C2.CountryName AS RegistrationCountryName,
		SC.Address,
		SC.PrimaryContactEmailAddress,
		SC.PrimaryContactName,
		SC.PrimaryContactPhone,
		SC.RegistrationNumber,
		SC.SubContractorID,
		SC.SubContractorName,
		SC.TaxNumber,
		SC.AccountNumber,
		SC.BankBranch,
		SC.BankName,
		SC.BankRoutingNumber,
		SC.IBAN,
		SC.SWIFTCode,
		SC.ISOCurrencyCode,
		SC.IsActive,
		SC.ProjectID,
		dropdown.GetProjectNameByProjectID(SC.ProjectID) AS ProjectName,
		SCBT.SubContractorBusinessTypeID,
		SCBT.SubContractorBusinessTypeName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName
	FROM contact.SubContractor SC
		JOIN dropdown.Country C1 ON C1.CountryID = SC.AddressCountryID
		JOIN dropdown.Country C2 ON C2.CountryID = SC.RegistrationCountryID
		JOIN dropdown.SubContractorBusinessType SCBT ON SCBT.SubContractorBusinessTypeID = SC.SubContractorBusinessTypeID
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
			AND SC.SubContractorID = @SubContractorID
			AND SC.ProjectID = @ProjectID

	SELECT
		SCC.SubContractorCapabilityID,
		SCC.SubContractorCapabilityName
	FROM dropdown.SubContractorCapability SCC
		JOIN contact.SubContractorSubContractorCapability SCSCC ON SCSCC.SubcontractorCapabilityID = SCC.SubcontractorCapabilityID
			AND SCSCC.SubcontractorID = @SubContractorID
			AND SCSCC.ProjectID = @ProjectID

END
GO
--End procedure contact.GetSubContractorBySubContractorID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@HasWorkflow BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.HasWorkflow = @HasWorkflow
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,HasWorkflow)
	VALUES
		(
		@EntityTypeCode, 
		@EntityTypeName, 
		@EntityTypeNamePlural,
		@HasWorkflow
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'person.GetMenuItemsByPersonID'
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- ===============================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure dbo.GetCampaignByCampaignID
EXEC utility.DropObject 'productdistributor.GetCampaignByCampaignID'
EXEC utility.DropObject 'dbo.GetCampaignByCampaignID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the dbo.Campaign table
-- ===================================================================
CREATE PROCEDURE dbo.GetCampaignByCampaignID

@CampaignID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CampaignDescription,
		C.CampaignID,
		C.CampaignName,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.IsActive,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Campaign C
	WHERE C.CampaignID = @CampaignID
		AND C.ProjectID = @ProjectID
	
END
GO
--End procedure dbo.GetCampaignByCampaignID

--Begin procedure dbo.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the dbo.Incident table
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentByIncidentID

@IncidentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Source,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,		
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		ST.SourceTypeID,
		ST.SourceTypeName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = I.SourceTypeID
			AND I.IncidentID = @IncidentID
			AND I.ProjectID = @ProjectID
	
END
GO
--End procedure dbo.GetIncidentByIncidentID

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFI.CompletedDate,
		core.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.DesiredResponseDate,
		core.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		RFI.IncidentDate,
		core.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		core.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.KnownDetails,
		RFI.PointOfContactPersonID,
		person.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		RFI.ProjectID,
		dropdown.GetProjectNameByProjectID(RFI.ProjectID) AS ProjectName,
		RFI.RequestDate,
		core.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		person.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonNameFormatted,
		RFI.SummaryAnswer,
		RFI.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(RFI.TerritoryID) AS TerritoryNameFormatted,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
			AND RFI.ProjectID = @ProjectID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'document.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
			AND D.ProjectID = @ProjectID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure document.GetEntityDocuments
EXEC Utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get records from the document.DocumentEntity table
-- =====================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@ProjectID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentType, 
		D.ContentSubtype,
		D.DocumentDate, 
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND D.ProjectID = @ProjectID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure eventlog.LogCampaignAction
EXEC utility.DropObject 'eventlog.LogCampaignAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCampaignAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			T.CampaignID,
			@Comments
		FROM dbo.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Campaign'), ELEMENTS
			)
		FROM dbo.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCampaignAction

--Begin procedure eventlog.LogCourseAction
EXEC utility.DropObject 'eventlog.LogCourseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCourseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cCourseContacts VARCHAR(MAX) 
	
		SELECT 
			@cCourseContacts = COALESCE(@cCourseContacts, '') + D.CourseContact 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CourseContact'), ELEMENTS) AS CourseContact
			FROM course.CourseContact T 
			WHERE T.CourseID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<CourseContacts>' + ISNULL(@cCourseContacts, '') + '</CourseContacts>') AS XML)
			FOR XML RAW('Course'), ELEMENTS
			)
		FROM course.Course T
		WHERE T.CourseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCourseAction

--Begin procedure eventlog.LogDistributorAction
EXEC utility.DropObject 'eventlog.LogDistributorAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDistributorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Distributor',
			T.DistributorID,
			@Comments
		FROM distributor.Distributor T
		WHERE T.DistributorID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cDistributorTerritories VARCHAR(MAX) = ''
		
		SELECT @cDistributorTerritories = COALESCE(@cDistributorTerritories, '') + D.DistributorTerritory
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DistributorTerritory'), ELEMENTS) AS DistributorTerritory
			FROM distributor.DistributorTerritory T 
			WHERE T.DistributorID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDistributorActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDistributorActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogDistributorActionTable
		FROM distributor.Distributor T
		WHERE T.DistributorID = @EntityID
		
		ALTER TABLE #LogDistributorActionTable DROP COLUMN NaturalReach
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Distributor',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			'<NaturalReach>' + CAST(D.NaturalReach AS VARCHAR(MAX)) + '</NaturalReach>',
			CAST(('<DistributorTerritories>' + ISNULL(@cDistributorTerritories, '') + '</DistributorTerritories>') AS XML)
			FOR XML RAW('Distributor'), ELEMENTS
			)
		FROM #LogDistributorActionTable T
			JOIN distributor.Distributor D ON D.DistributorID = T.DistributorID

		DROP TABLE #LogDistributorActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDistributorAction

--Begin procedure eventlog.LogImpactStoryAction
EXEC utility.DropObject 'eventlog.LogImpactStoryAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogImpactStoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'ImpactStory',
			T.ImpactStoryID,
			@Comments
		FROM impactstory.ImpactStory T
		WHERE T.ImpactStoryID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ImpactStory',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ImpactStory'), ELEMENTS
			)
		FROM impactstory.ImpactStory T
		WHERE T.ImpactStoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogImpactStoryAction

--Begin procedure eventlog.LogModuleAction
EXEC utility.DropObject 'eventlog.LogModuleAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogModuleAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Module'

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
	
		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, @EntityID)

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(@cDocuments AS XML)
			FOR XML RAW('Module'), ELEMENTS
			)
		FROM dbo.Module T
		WHERE T.ModuleID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogModuleAction

--Begin procedure eventlog.LogProductAction
EXEC utility.DropObject 'eventlog.LogProductAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProductAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Product',
			T.ProductID,
			@Comments
		FROM product.Product T
		WHERE T.ProductID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cProductCampaigns VARCHAR(MAX) = ''
		
		SELECT @cProductCampaigns = COALESCE(@cProductCampaigns, '') + D.ProductCampaign
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductCampaign'), ELEMENTS) AS ProductCampaign
			FROM product.ProductCampaign T 
			WHERE T.ProductID = @EntityID
			) D

		DECLARE @cProductDistributions VARCHAR(MAX) = ''
		
		SELECT @cProductDistributions = COALESCE(@cProductDistributions, '') + D.ProductDistribution
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductDistribution'), ELEMENTS) AS ProductDistribution
			FROM product.ProductDistribution T 
			WHERE T.ProductID = @EntityID
			) D

		DECLARE @cProductTerritories VARCHAR(MAX) = ''
		
		SELECT @cProductTerritories = COALESCE(@cProductTerritories, '') + D.ProductTerritory
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductTerritory'), ELEMENTS) AS ProductTerritory
			FROM product.ProductTerritory T 
			WHERE T.ProductID = @EntityID
			) D
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Product',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ProductCampaigns>' + ISNULL(@cProductCampaigns, '') + '</ProductCampaigns>') AS XML),
			CAST(('<ProductDistributions>' + ISNULL(@cProductDistributions, '') + '</ProductDistributions>') AS XML),
			CAST(('<ProductTerritories>' + ISNULL(@cProductTerritories, '') + '</ProductTerritories>') AS XML)
			FOR XML RAW('Product'), ELEMENTS
			)
		FROM product.Product T
		WHERE T.ProductID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProductAction

--Begin procedure finding.GetFindingByFindingID
EXEC Utility.DropObject 'finding.GetFindingByFindingID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.01.01
-- Description:	A stored procedure to get data from the finding.Finding table
-- ==========================================================================
CREATE PROCEDURE finding.GetFindingByFindingID

@FindingID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingDescription,
		F.IsResearchElement,
		F.IsActive,
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND F.FindingID = @FindingID
			AND F.ProjectID = @ProjectID

	SELECT
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM finding.FindingIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.FindingID = @FindingID
			AND FI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		R.RecommendationID,
		R.RecommendationName,
		R.IsResearchElement
	FROM finding.FindingRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.FindingID = @FindingID
			AND FR.ProjectID = @ProjectID
	ORDER BY R.RecommendationName, R.RecommendationID

	SELECT
		FT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FT.TerritoryID) AS TerritoryName
	FROM finding.FindingTerritory FT
	WHERE FT.FindingID = @FindingID
		AND FT.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure finding.GetFindingByFindingID

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		contact.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle') AS CommanderContactNameFormatted,
		F.Comments, 
		F.DeputyCommanderContactID, 
		contact.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderContactNameFormatted,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		FU.CommanderContactID, 
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderContactNameFormatted,
		FU.DeputyCommanderContactID,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderContactNameFormatted,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
			AND FU.ProjectID = @ProjectID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Indicator data
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		core.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		core.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		core.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		core.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		core.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		core.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID
			AND I.ProjectID = O.ProjectID
			AND I.ProjectID = @ProjectID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetIndicatorByObjectiveID
EXEC utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date: 2017.01.01
-- Description:	A stored procedure to get data from the logicalframework.Indicator table
-- =====================================================================================
CREATE PROCEDURE logicalframework.GetIndicatorByObjectiveID

@ObjectiveID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		I.AchievedDate,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormatted,
		I.AchievedValue,
		I.ActualDate,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormatted,
		I.BaselineDate,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormatted,
		I.BaselineValue,
		I.IndicatorDescription,
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorNumber,
		I.IndicatorSource,
		I.InProgressDate,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormatted,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormatted,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormatted,
		I.TargetValue,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName
  FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			AND I.ObjectiveID = @ObjectiveID
			AND I.ProjectID = @ProjectID
			AND I.IsActive = 1
	ORDER BY I.IndicatorID
	
END
GO
--End procedure logicalframework.GetIndicatorByObjectiveID

--Begin procedure logicalframework.GetMilestoneByIndicatorID
EXEC utility.DropObject 'logicalframework.GetMilestoneByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByIndicatorID

@IndicatorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH Total AS
		(
		SELECT
			I.AchievedValue,
			3 AS DisplayOrder,
			I.InprogressValue,
			'Total' AS MilestoneName,
			I.PlannedValue,
			I.TargetDate,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue,
			I.PlannedValue + I.InprogressValue + I.AchievedValue AS TotalValue
		FROM logicalframework.Indicator I
		WHERE I.IndicatorID = @IndicatorID
			AND I.ProjectID = @ProjectID
		)
		
	SELECT
		I.BaseLineValue AS AchievedValue,
		1 AS DisplayOrder,
		0 AS InprogressValue,
		'Baseline' AS MilestoneName,
		0 AS PlannedValue,
		I.BaseLineDate AS TargetDate,
		LEFT(DATENAME(month, I.BaseLineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaseLineDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		0 AS TargetValue,
		0 AS AchievedValuePercent,
		0 AS InprogressValuePercent,
		0 AS IntendedValuePercent,
		0 AS PlannedValuePercent
	FROM logicalframework.Indicator I
	WHERE I.IndicatorID = @IndicatorID
		AND I.ProjectID = @ProjectID
	
	UNION
	
	SELECT 
		M.AchievedValue,
		2 AS DisplayOrder,
		M.InProgressValue,
		M.MilestoneName,
		M.PlannedValue,
		M.TargetDate,
		LEFT(DATENAME(month, M.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		M.TargetValue,
		0 AS AchievedValuePercent,
		0 AS InprogressValuePercent,
		0 AS IntendedValuePercent,
		0 AS PlannedValuePercent
	FROM logicalframework.Milestone M
	WHERE M.IndicatorID = @IndicatorID
		AND M.ProjectID = @ProjectID
	
	UNION
	
	SELECT
		T.AchievedValue,
		T.DisplayOrder,
		T.InprogressValue,
		T.MilestoneName,
		T.PlannedValue,
		T.TargetDate,
		T.TargetDateFormattedShort,
		T.TargetValue,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS AchievedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS InprogressValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST((CAST(T.TargetValue AS NUMERIC(18,2)) - CAST(T.AchievedValue AS NUMERIC(18,2)) - CAST(T.InprogressValue AS NUMERIC(18,2)) - CAST(T.PlannedValue AS NUMERIC(18,2))) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS IntendedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS PlannedValuePercent

	FROM Total T
	
	ORDER BY 2, 6
	
END
GO
--End procedure logicalframework.GetMilestoneByIndicatorID

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorID, 	
		I.IndicatorName, 	
		M.AchievedValue, 	
		M.MilestoneID, 	
		M.MilestoneName, 	
		M.ProjectID,
		dropdown.GetProjectNameByProjectID(M.ProjectID) AS ProjectName,
		M.TargetDate, 	
		core.FormatDate(M.TargetDate) AS TargetDateFormatted,
		M.TargetValue
	FROM logicalframework.Milestone M
		JOIN logicalframework.Indicator I ON I.IndicatorID = M.IndicatorID
			AND M.MilestoneID = @MilestoneID
			AND M.ProjectID = I.ProjectID
			AND M.ProjectID = @ProjectID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframework.GetMilestoneDataByIndicatorID
EXEC utility.DropObject 'logicalframework.GetMilestoneDataByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Milestone data
-- ============================================================
CREATE PROCEDURE logicalframework.GetMilestoneDataByIndicatorID

@IndicatorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.AchievedValue, 
		M.IndicatorID, 
		M.InProgressValue, 
		M.IsActive, 
		M.MilestoneID, 
		M.MilestoneName, 
		M.PlannedValue,
		M.TargetDate, 
		core.FormatDate(M.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, M.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		M.TargetValue
	FROM logicalframework.MileStone M
	WHERE M.IndicatorID = @IndicatorID
		AND M.ProjectID = @ProjectID
	
END
GO
--End procedure logicalframework.GetMilestoneDataByIndicatorID

--Begin procedure logicalframework.GetObjectiveByObjectiveID
EXEC utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByObjectiveID

@ObjectiveID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.ProjectID,
		dropdown.GetProjectNameByProjectID(O1.ProjectID) AS ProjectName,
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID
			AND O1.ProjectID = @ProjectID

END
GO
--End procedure logicalframework.GetObjectiveByObjectiveID

--Begin procedure logicalframework.GetObjectiveByParentObjectiveID
EXEC utility.DropObject 'logicalframework.GetObjectiveByParentObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByParentObjectiveID

@ParentObjectiveID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM logicalframework.Objective O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	WHERE O.ParentObjectiveID = @ParentObjectiveID
		AND O.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, O.ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByParentObjectiveID

--Begin procedure mediareport.GetMediaReportByMediaReportID
EXEC Utility.DropObject 'mediareport.GetMediaReportByMediaReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the mediareport.MediaReport table
-- ==============================================================================
CREATE PROCEDURE mediareport.GetMediaReportByMediaReportID

@MediaReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.Comments,
		MR.CommunicationOpportunities,
		MR.Implications,
		MR.MediaReportDate,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediaReportLocation,
		MR.MediaReportTitle,
		MR.ProjectID,
		dropdown.GetProjectNameByProjectID(MR.ProjectID) AS ProjectName,
		MR.RiskMitigation,
		MR.Summary,
		MR.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.MediaReportID = @MediaReportID
			AND MR.ProjectID = @ProjectID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS MediaReportSourceGUID,
		MRS.MediaReportSourceID, 
		MRS.SourceAttribution, 
		MRS.SourceDate, 
		core.FormatDate(MRS.SourceDate) AS SourceDateFormatted, 
		MRS.SourceName, 
		MRS.SourceSummary, 
		MRST.MediaReportSourceTypeID, 
		MRST.MediaReportSourceTypeName,
		MRST.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		MRST.MediaReportSourceTypeName AS SourceTypeName, -- Aliased for the common source cfmodule
		CL.ConfidenceLevelID AS SourceConfidenceLevelID, -- Aliased for the common source cfmodule
		CL.ConfidenceLevelName AS SourceConfidenceLevelName, -- Aliased for the common source cfmodule
		SC.SourceCategoryID, 
		SC.SourceCategoryName
	FROM mediareport.MediaReportSource MRS
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = MRS.SourceConfidenceLevelID
		JOIN dropdown.MediaReportSourceType MRST ON MRST.MediaReportSourceTypeID = MRS.MediaReportSourceTypeID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = MRS.SourceCategoryID
			AND MRS.MediaReportID = @MediaReportID
			AND MRS.ProjectID = @ProjectID
	ORDER BY MRS.SourceName, MRS.MediaReportSourceID

END
GO
--End procedure mediareport.GetMediaReportByMediaReportID

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to return data from the person.Person table
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@PersonToken VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PersonID
	FROM person.Person P
	WHERE P.Token = @PersonToken

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@ImplementerCode VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		DefaultProjectID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDefaultProjectID = P.DefaultProjectID,
		@cPhone = P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,DefaultProjectID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@nDefaultProjectID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE person.Person
				SET InvalidLoginAttempts = 0
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure portalupdate.UpdateDataBySchemaName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data for tables in a specific schema
-- =====================================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaName

@SchemaName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTableName VARCHAR(50)

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

	CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)

	SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @SchemaName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @SchemaName + ' T WHERE T.' + @SchemaName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))

	IF EXISTS (SELECT 1 FROM core.EntityType ET WHERE ET.EntityTypeCode = @SchemaName AND ET.HasWorkflow = 1)
		SET @cSQL += ' AND workflow.GetWorkflowStepNumber(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID) > workflow.GetWorkflowStepCount(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID)'
	--ENDIF

	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = @SchemaName
		ORDER BY 1

	OPEN oCursor
	FETCH oCursor INTO @cTableName
	WHILE @@fetch_status = 0
		BEGIN	

		SET @cSQL = 'DELETE T1 FROM LEO0001.' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', ')
		SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', T1.')

		SET @cSQL = 'INSERT INTO LEO0001.' + @SchemaName + '.' + @cTableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		FETCH oCursor INTO @cTableName

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DROP TABLE portalupdate.#tPortalUpdate

END
GO
--End procedure portalupdate.UpdateDataBySchemaName

--Begin procedure portalupdate.UpdateDataBySchemaNameAndTableName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaNameAndTableName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data in a specific table
-- =========================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaNameAndTableName

@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cSQL VARCHAR(MAX)

	IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
		DROP TABLE portalupdate.#tPortalUpdate
	--ENDIF

	CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)

	SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @TableName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @TableName + ' T WHERE T.' + @TableName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	SET @cSQL = 'DELETE T1 FROM LEO0001.' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @TableName, ', ')
	SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @TableName, ', T1.')

	SET @cSQL = 'INSERT INTO LEO0001.' + @SchemaName + '.' + @TableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
	IF @PrintSQL = 0
		EXEC (@cSQL)
	ELSE
		print @cSQL
	--ENDIF

	DROP TABLE portalupdate.#tPortalUpdate

END
GO
--End procedure portalupdate.UpdateDataBySchemaNameAndTableName

--Begin procedure distributor.GetDistributorByDistributorID
EXEC utility.DropObject 'distributor.GetDistributorByDistributorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the distributor.Distributor table
-- ==============================================================================
CREATE PROCEDURE distributor.GetDistributorByDistributorID

@DistributorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.NaturalReach.STAsText() AS NaturalReach,
		D.Notes,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		FBP.ID AS FacebookPageID,
		FBP.Name AS FacebookPageName
	FROM distributor.Distributor D
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND D.DistributorID = @DistributorID
			AND D.ProjectID = @ProjectID
		LEFT JOIN integration.FacebookPage FBP ON FBP.ID = D.FacebookPageID

	SELECT
		DT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(DT.TerritoryID) AS TerritoryName
	FROM distributor.DistributorTerritory DT
	WHERE DT.DistributorID = @DistributorID
		AND DT.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure distributor.GetDistributorByDistributorID

--Begin procedure impactstory.GetImpactStoryByImpactStoryID
EXEC utility.DropObject 'impactstory.GetImpactStoryByImpactStoryID'
GO

-- =======================================================================================
-- Author:			Kevin Ross
-- Create date: 2017.01.01
-- Description:	A stored procedure to add data to the impactstory.ImpactStory table
-- =======================================================================================
CREATE PROCEDURE impactstory.GetImpactStoryByImpactStoryID

@ImpactStoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		I.ImpactStoryID,
		I.ImpactStoryName,
		I.ImpactStoryDescription,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName
	FROM impactstory.ImpactStory I
		WHERE I.ImpactStoryID = @ImpactStoryID
			AND I.ProjectID = @ProjectID

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM impactstory.ImpactStoryCampaign IC
		JOIN dbo.Campaign C ON C.CampaignID = IC.CampaignID
			AND IC.ImpactStoryID = @ImpactStoryID
			AND IC.ProjectID = @ProjectID
			AND C.ProjectID = IC.ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		P.ProductID,
		P.ProductName
	FROM impactstory.ImpactStoryProduct IP
		JOIN product.Product P ON P.ProductID = IP.ProductID
			AND IP.ImpactStoryID = @ImpactStoryID
			AND IP.ProjectID = @ProjectID
			AND P.ProjectID = IP.ProjectID
	ORDER BY P.ProductName, P.ProductID

	SELECT
		IT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(IT.TerritoryID) AS TerritoryNameFormatted
	FROM impactstory.ImpactStoryTerritory IT
	WHERE IT.ImpactStoryID = @ImpactStoryID
		AND IT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure impactstory.GetImpactStoryByImpactStoryID

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date: 2017.01.01
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.CurrencyID,
		EC.EquipmentCatalogID, 
		EC.IsActive, 
		IIF(EC.IsActive = 1, 'Yes ', 'No ') AS IsActiveFormatted,
		EC.ItemName, 
		EC.Notes, 
		EC.ProjectID, 
		EC.UnitCost,
		EC.UnitOfIssue,
		CONCAT(C.ISOCurrencyCode, ' ', EC.UnitCost) AS UnitCostFormatted
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.Currency C ON C.CurrencyID = EC.CurrencyID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN dbo.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID

	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure product.GetProductDistributionCounts
EXEC Utility.DropObject 'product.GetProductDistributionCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE product.GetProductDistributionCounts

@PersonID INT = 0,
@ProjectID INT = 0,
@StartDate DATE = NULL,
@EndDate DATE = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProjectID,
		Project.ProjectName,
		ISNULL(COUNT(P.ProductID), 0) AS ProductCountTotal,
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.TotalReach), 0) - ISNULL(SUM(PD.OrganicReach), 0) AS PaidReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCountTotal
	FROM product.ProductDistribution PD
		JOIN product.Product P ON P.ProductID = PD.ProductID
		JOIN dropdown.Project ON Project.ProjectID = P.ProjectID
			AND (@ProjectID = 0 OR P.ProjectID = @ProjectID)
			AND (@PersonID = 0 OR EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.ProjectID = P.ProjectID AND PP.PersonID = @PersonID))
			AND (@StartDate IS NULL OR @EndDate IS NULL OR P.ReleaseDate BETWEEN @StartDate AND @EndDate)
	GROUP BY P.ProjectID, Project.ProjectName
	
END
GO
--End procedure product.GetProductDistributionCounts

--Begin procedure recommendation.GetRecommendationByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.01.01
-- Description:	A stored procedure to get data from the recommendation.Recommendation table
-- ========================================================================================
CREATE PROCEDURE recommendation.GetRecommendationByRecommendationID

@RecommendationID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.IsActive,
		R.IsResearchElement,
		R.ProjectID,
		dropdown.GetProjectNameByProjectID(R.ProjectID) AS ProjectName,
		R.RecommendationDescription,
		R.RecommendationID,
		R.RecommendationName
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID
		AND R.ProjectID = @ProjectID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendation.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
			AND RI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		RT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(RT.TerritoryID) AS TerritoryName
	FROM recommendation.RecommendationTerritory RT
	WHERE RT.RecommendationID = @RecommendationID
		AND RT.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure reporting.GetProductListByPersonID
EXEC utility.DropObject 'reporting.GetProductListByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the product.Product table matching the reporting.SearchResults items
-- =====================================================================================================================
CREATE PROCEDURE reporting.GetProductListByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProductName,
		DPS.ProductStatusName,
		DPT.ProductTypeName,
		P.ProductCode,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		DP.ProjectName,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReach,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCount,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCount,
		ISNULL(SUM(PD.CommentCount), 0)AS CommentCount,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount,
		DTA.TargetAudienceName,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal
	FROM reporting.SearchResult SR
		JOIN product.Product P ON P.ProductID = SR.EntityID  
			AND SR.PersonID = @personID  
			AND SR.EntityTypeCode = 'Product'
		JOIN dropdown.ProductStatus DPS ON DPS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType DPT ON DPT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.Project DP ON DP.ProjectID = P.ProjectID
		JOIN product.ProductDistribution PD ON PD.ProductID = P.ProductID
		JOIN dropdown.TargetAudience DTA ON DTA.TargetAudienceID = P.TargetAudienceID
	GROUP BY P.ProductName,
		DPS.ProductStatusName,
		DPT.ProductTypeName,
		P.ProductCode,
		P.ReleaseDate,
		DP.ProjectName,
		DTA.TargetAudienceName
END
GO
--End procedure reporting.GetProductListByPersonID

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID, @ProjectID)
	
	SELECT
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunicationOpportunities,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SourceDetails,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon
	FROM spotreport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
			AND SRI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
		AND SRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		AOT.AreaOfOperationTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1

END
GO
--End procedure territory.GetTerritoryByTerritoryID

--Begin procedure course.GetCourseByCourseID
EXEC utility.DropObject 'course.GetCourseByCourseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the course.Course table
-- ======================================================================
CREATE PROCEDURE course.GetCourseByCourseID

@CourseID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CourseID,
		C.CoursePointOfContact,
		C.TerritoryID,
		(territory.FormatTerritoryNameByTerritoryID(C.TerritoryID)) AS TerritoryNameFormatted,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Instructor1,
		C.Instructor1Comments,
		C.Instructor2,
		C.Instructor2Comments,
		C.Location,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Seats,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		C.StudentFeedbackSummary,
		C.QualityAssuranceFeedback,
		M.ModuleID,
		M.ModuleName,
		ISNULL((SELECT AC.ActivityID FROM activity.ActivityCourse AC WHERE AC.CourseID = C.CourseID), 0) AS ActivityID
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
			AND C.CourseID = @CourseID
			AND C.ProjectID = @ProjectID
	
	SELECT
		territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID) AS TerritoryNameFormatted,
		CO.ContactID,
		AC2.VettingIcon,
		AC2.VettingOutcomeName,
		core.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		contact.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS ContactNameFormatted,
		IIF (CO.AssetID > 0,
			(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = CO.AssetID), 
			IIF (CO.ForceID > 0,
				(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = CO.ForceID),
				IIF (CO.TerritoryID > 0,
					territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID),
					'')) ) AS ParentOrganizationName,
		STUFF((
			SELECT ', ' + CT.ContactTypeName
			FROM contact.ContactContactType CCT
			JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = CC.ContactID
			FOR XML PATH('')
		), 1, 2, '') AS ContactTypeNamesList
	FROM course.CourseContact CC
		JOIN course.Course CL ON CL.CourseID = CC.CourseID
		JOIN contact.Contact CO ON CO.ContactID = CC.ContactID
			AND CC.ProjectID = @ProjectID
			AND CL.CourseID = @CourseID
			AND CL.ProjectID = CC.ProjectID
			AND CO.ProjectID = CC.ProjectID
		OUTER APPLY 
			(
			SELECT 
				'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
				VO.VettingOutcomeName
			FROM activity.ActivityContact AC1
				JOIN activity.ActivityCourse AC2 ON AC2.CourseID = CL.CourseID
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC1.VettingOutcomeID
					AND AC1.ContactID = CC.ContactID
					AND AC1.ActivityID = AC2.ActivityID
					AND AC1.ProjectID = @ProjectID
					AND AC1.ProjectID = AC2.ProjectID
			) AC2

END
GO
--End procedure course.GetCourseByCourseID

--Begin procedure dbo.GetModuleByModuleID
EXEC utility.DropObject 'dbo.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the dbo.Module table
-- ======================================================================
CREATE PROCEDURE dbo.GetModuleByModuleID

@ModuleID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.ModuleID,
		C.ModuleName,
		C.ModuleTypeID,
		C.Summary,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.SponsorName,
		C.Notes,
		CT.ModuleTypeID,
		CT.ModuleTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		core.GetEntityTypeNameByEntityTypeCode('Module') AS EntityTypeName
	FROM dbo.Module C
		JOIN dropdown.ModuleType CT ON CT.ModuleTypeID = C.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ModuleID = @ModuleID
		AND C.ProjectID = @ProjectID
		
END
GO
--End procedure dbo.GetModuleByModuleID

--Begin procedure trendreport.GetTrendReportByTrendReportID
EXEC utility.DropObject 'trendreport.GetTrendReportByTrendReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the trendreport.TrendReport table
-- ==================================================================================
CREATE PROCEDURE trendreport.GetTrendReportByTrendReportID

@TrendReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReport', @TrendReportID, @ProjectID)
	
	SELECT
		TR.ProjectID,
		dropdown.GetProjectNameByProjectID(TR.ProjectID) AS ProjectName,
		TR.TrendReportID,
		TR.TrendReportTitle,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Summary,
		TR.ReportDetail,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("TR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		TR.SummaryMapZoom
	FROM trendreport.TrendReport TR
	WHERE TR.TrendReportID = @TrendReportID
		AND TR.ProjectID = @ProjectID

	SELECT
		A.AssetID,
		A.AssetName
	FROM trendreport.TrendReportAsset TRA
		JOIN asset.Asset A ON A.AssetID = TRA.AssetID
			AND A.ProjectID = @ProjectID
			AND TRA.TrendReportID = @TrendReportID
			AND TRA.ProjectID = A.ProjectID
	ORDER BY A.AssetName, A.AssetID

	SELECT
		F.ForceID,
		F.ForceName
	FROM trendreport.TrendReportForce TRF
		JOIN force.Force F ON F.ForceID = TRF.ForceID
			AND F.ProjectID = @ProjectID
			AND TRF.TrendReportID = @TrendReportID
			AND TRF.ProjectID = F.ProjectID
	ORDER BY F.ForceName, F.ForceID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM trendreport.TrendReportIncident TRI
		JOIN dbo.Incident I ON I.IncidentID = TRI.IncidentID
			AND I.ProjectID = @ProjectID
			AND TRI.TrendReportID = @TrendReportID
			AND TRI.ProjectID = I.ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM trendreport.TrendReportMediaReport TRMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = TRMR.MediaReportID
			AND MR.ProjectID = @ProjectID
			AND TRMR.TrendReportID = @TrendReportID
			AND TRMR.ProjectID = MR.ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	SELECT
		TRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(TRT.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM trendreport.TrendReportTerritory TRT
		JOIN territory.Territory T ON T.TerritoryID = TRT.TerritoryID
			AND TRT.TrendReportID = @TrendReportID
			AND TRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'TrendReport', @TrendReportID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'TrendReport', @TrendReportID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN trendreport.TrendReport TR ON TR.TrendReportID = EL.EntityID
			AND TR.TrendReportID = @TrendReportID
			AND TR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'TrendReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure trendreport.GetTrendReportByTrendReportID

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure workflow.DecrementWorkflow
EXEC Utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.ProjectID = @ProjectID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount

END
GO
--End procedure workflow.DecrementWorkflow

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
					AND W2.ProjectID = W1.ProjectID
					AND WS2.ProjectID = WS1.ProjectID
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND W1.ProjectID = @ProjectID
				AND WS1.ProjectID = @ProjectID
				AND WS1.WorkflowStepNumber = 1
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID)
			BEGIN
			
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.IsComplete = 0
				AND EWSGP.ProjectID = @ProjectID
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
			
			END
		--ENDIF
			
		END
	--ENDIF
	
END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return people assigned to an entityy's current workflow step
-- ========================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowPeople

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT,
@WorkflowStepNumber INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WSG.WorkflowStepGroupName,
			WSGP.PersonID,
			person.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND W.ProjectID = @ProjectID
				AND WS.ProjectID = @ProjectID
				AND WS.WorkflowStepNumber = 1
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
		ORDER BY 1, 3
	
		END
	ELSE
		BEGIN
	
		SELECT
			EWSGP.WorkflowStepGroupName, 
			EWSGP.PersonID,
			person.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.ProjectID = @ProjectID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber
		ORDER BY 1, 3
	
		END
	--ENDIF
	
END
GO
--End procedure workflow.GetEntityWorkflowPeople

--Begin procedure workflow.IncrementWorkflow
EXEC Utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT,
@ProjectID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.ProjectID = @ProjectID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.ProjectID = @ProjectID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure workflow.IncrementWorkflow

--Begin procedure workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to indicate if a personid exists in the current step of an entity's workflow
-- =====================================================================================================

CREATE PROCEDURE workflow.IsPersonInCurrentWorkflowStep

@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@ProjectID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN

		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND W.IsActive = 1

		END
	ELSE
		BEGIN

		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)
			AND EWSGP.PersonID = @PersonID
			AND EWSGP.ProjectID = @ProjectID

		END
	--ENDIF
	
END
GO
--End procedure workflow.IsPersonInCurrentWorkflowStep

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0003
GO

EXEC utility.DropSchema 'productdistributor'
EXEC utility.DropSchema 'training'
GO

DELETE S
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'ConceptNoteBackgroundText'
GO

UPDATE S
SET S.SetupValue = 'jcole@skotkonung.com,rabaat@skotkonung.com'
FROM core.ImplementerSetup S
WHERE S.SetupKey IN ('FeedBackMailTo', 'RFIMailTo', 'VettingMailTo')
GO

DECLARE @cURLPrefix VARCHAR(50)

SELECT @cURLPrefix = LOWER(I.ImplementerURL)
FROM LEO0000.implementer.Implementer I
WHERE I.ImplementerCode = 'LEO0003'

UPDATE S
SET S.SetupValue = 'NoReply@' + @cURLPrefix + CASE WHEN (SELECT LEO0000.core.GetSystemSetupValueBySetupKey('Environment', '')) = 'Prod' THEN '.projectkms.com' ELSE '.oceandisc.com' END
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'NoReply'

UPDATE S
SET S.SetupValue = 'https://' + @cURLPrefix + CASE WHEN (SELECT LEO0000.core.GetSystemSetupValueBySetupKey('Environment', '')) = 'Prod' THEN '.projectkms.com' ELSE '.oceandisc.com' END
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'SiteURL'
GO

UPDATE S
SET S.SetupValue = DB_NAME()
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'ImplementerCode'
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Activity')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('Activity', 'DecrementWorkflow', '<p>A previously submitted activity has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification because you have been assigned as a member of the Activity workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the InCoStrat Knowledge Managment System.<br /><br />Thank you,</p>', 'An Activity Has Been Disapproved'),
		('Activity', 'IncrementWorkflow', '<p>An Activity has been submitted for your review:</p><p><strong>Activity: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the ICS system because you have been assigned as a member of the Activity workflow. Please log in and click the link above to review this activity.</p><p>Please do not reply to this email as it is generated automatically by the ICS system.</p><p>Thank you,</p>', 'An Activity Is Ready For Your Review'),
		('Activity', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The activity is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the ICS system.</p><p>Thank you,<br />The ICS Team</p><p>&nbsp;</p>', 'An Activity Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'ActivityReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('ActivityReport', 'DecrementWorkflow', '<p>A previously submitted activity report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the activity reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Activity Report Has Been Disapproved'),
		('ActivityReport', 'IncrementWorkflow', '<p>A activity report has been submitted for your review:</p><p><strong>Activity Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the activity reports workflow. Please log in and click the link above to review this activity report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Activity Report Is Ready For Your Review'),
		('ActivityReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Activity Report Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'RequestForInformation')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('RequestForInformation', 'Amend', '<p>An Amended response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p><p>&nbsp;</p>', 'A Previously Completed Response To A Request For Information Response Has Been Amended'),
		('RequestForInformation', 'Complete', '<p>A response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,<p>&nbsp;</p>', 'A Response To A Request For Information Has Been Completed'),
		('RequestForInformation', 'PointOfContactUpdate', '<p>Dear [[FullName]] <br /><br />RfI [[Title]] has been assigned to you for action. The deadline for a response is [[IncidentDateFormatted]]. The RfI request is for [[SummaryAnswer]] and can be viewed here [[TitleLink]] <br /><br />Many thanks<br />[[FullName]]</p>', 'RFI Assigned for Action'),
		('RequestForInformation', 'Submit', '<p>A request for information has been recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p>', 'A Request For Information Has Been Submitted')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'SpotReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('SpotReport', 'DecrementWorkflow', '<p>A previously submitted activity report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Spot Report Has Been Disapproved'),
		('SpotReport', 'IncrementWorkflow', '<p>A spot report has been submitted for your review:</p><p><strong>Spot Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow. Please log in and click the link above to review this spot report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Spot Report Is Ready For Your Review'),
		('SpotReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Spot Report Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'TrendReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('TrendReport', 'DecrementWorkflow', '<p>A previously submitted trend report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Trend Report Has Been Disapproved'),
		('TrendReport', 'IncrementWorkflow', '<p>A trend report has been submitted for your review:</p><p><strong>Trend Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow. Please log in and click the link above to review this trend report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Trend Report Is Ready For Your Review'),
		('TrendReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Trend Report Has Been Released')

	END
--ENDIF
GO

TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField	
	(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
VALUES
	('Activity', '[[Comments]]', 'Comments'),
	('Activity', '[[Title]]', 'Title'),
	('Activity', '[[TitleLink]]', 'Title Link'),
	('ActivityReport', '[[Comments]]', 'Comments'),
	('ActivityReport', '[[Title]]', 'Title'),
	('ActivityReport', '[[TitleLink]]', 'Title Link'),
	('RequestForInformation', '[[FullName]]', 'Requested By'),
	('RequestForInformation', '[[IncidentDateFormatted]]', 'Incident Date'),
	('RequestForInformation', '[[InformationRequested]]', 'Information Requested'),
	('RequestForInformation', '[[KnownDetails]]', 'Details'),
	('RequestForInformation', '[[SummaryAnswer]]', 'Response'),
	('RequestForInformation', '[[TerritoryName]]', 'Territory'),
	('RequestForInformation', '[[Title]]', 'Title'),
	('RequestForInformation', '[[TitleLink]]', 'Title Link'),
	('SpotReport', '[[Comments]]', 'Comments'),
	('SpotReport', '[[Title]]', 'Title'),
	('SpotReport', '[[TitleLink]]', 'Title Link'),
	('TrendReport', '[[Comments]]', 'Comments'),
	('TrendReport', '[[Title]]', 'Title'),
	('TrendReport', '[[TitleLink]]', 'Title Link')
GO

DECLARE @cSQL VARCHAR(MAX)
DECLARE @nProjectID INT = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = 'LEO0003' AND IDD.DropdownCode = 'Project' ORDER BY IDD.DropdownID)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'UPDATE T SET T.ProjectID = ' + CAST(@nProjectID AS VARCHAR(10)) + ' FROM ' + S.Name + '.' + T.Name + ' T'
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN sys.Columns C ON C.Object_ID = T.Object_ID
			AND C.Name = 'ProjectID'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN	

	EXEC (@cSQL)

	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

TRUNCATE TABLE person.PersonProject
GO

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P.PersonID,
	IDD.DropdownID
FROM person.Person P
	CROSS APPLY implementer.ImplementerDropdownData IDD
WHERE IDD.DropdownCode = 'Project'
	AND IDD.ImplementerCode = 'LEO0003'
GO

UPDATE C
SET C.ProjectID = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.DropdownCode = 'Project' AND IDD.ImplementerCode = 'LEO0003' ORDER BY IDD.DropdownID DESC)
FROM contact.Contact C
GO

UPDATE P
SET P.DefaultProjectID = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.DropdownCode = 'Project' AND IDD.ImplementerCode = 'LEO0003' ORDER BY IDD.DropdownID DESC)
FROM person.Person P
GO

DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Activity' AND NOT EXISTS (SELECT 1 FROM activity.Activity T WHERE T.ActivityID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'ActivityReport' AND NOT EXISTS (SELECT 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'SpotReport' AND NOT EXISTS (SELECT 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'TrendReport' AND NOT EXISTS (SELECT 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = EWSGP.EntityID)
GO

--End file LEO000X - 04 - Data.sql

--Begin LEO000X post processing
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End LEO000X post processing

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.3 - 2017.02.05 15.53.53')
GO
--End build tracking

