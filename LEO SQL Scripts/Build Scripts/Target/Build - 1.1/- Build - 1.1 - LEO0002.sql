-- File Name:	Build - 1.1 - LEO0002.sql
-- Build Key:	Build - 1.1 - 2017.01.23 16.57.19

USE [LEO0002]
GO

-- ==============================================================================================================================
-- Procedures:
--		core.ImplementerSetupAddUpdate
--		training.GetModuleByModuleID
--		utility.RefreshSynonyms
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0002
GO


--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0002
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0002
GO

EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

--Begin procedure core.ImplementerSetupAddUpdate
EXEC Utility.DropObject 'core.ImplementerSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer setup key records
-- =============================================================================
CREATE PROCEDURE core.ImplementerSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.ImplementerSetup I
	USING (SELECT @SetupKey AS SetupKey) T2
		ON T2.SetupKey = I.SetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		I.SetupValue = @SetupValue
	WHEN NOT MATCHED THEN
	INSERT (SetupKey,SetupValue)
	VALUES
		(
		@SetupKey, 
		@SetupValue
		);

END
GO
--End procedure core.ImplementerSetupAddUpdate

--Begin procedure training.GetModuleByModuleID
EXEC utility.DropObject 'training.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the training.Module table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added call for Module Document information
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE training.GetModuleByModuleID

@ModuleID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.ModuleID,
		C.ModuleName,
		C.ModuleTypeID,
		C.Summary,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.SponsorName,
		C.Notes,
		CT.ModuleTypeID,
		CT.ModuleTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName
	FROM training.Module C
		JOIN dropdown.ModuleType CT ON CT.ModuleTypeID = C.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ModuleID = @ModuleID
		
END
GO
--End procedure training.GetModuleByModuleID

--Begin procedure utility.RefreshSynonyms
EXEC utility.DropObject 'utility.RefreshSynonyms'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.23
-- Description:	A stored procedure to drop and recreate synonyms in a client database
-- ==================================================================================
CREATE PROCEDURE utility.RefreshSynonyms

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQLText1 VARCHAR(MAX)
	DECLARE @cSQLText2 VARCHAR(MAX)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			SCH.Name + '.' + SYN.Name
		FROM sys.Synonyms SYN
			JOIN sys.Schemas SCH ON SCH.Schema_ID = SYN.Schema_ID
		ORDER BY 1

	OPEN oCursor
	FETCH oCursor INTO @cSQLText1
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC utility.DropObject @cSQLText1

		FETCH oCursor INTO @cSQLText1
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT DISTINCT
			S.BaseObjectSchema + '.' + S.BaseObjectName  AS SQLText1,
			'CREATE SYNONYM ' + S.BaseObjectSchema + '.' + S.BaseObjectName + ' FOR LEO0000.' + S.BaseObjectSchema + '.' + S.BaseObjectName AS SQLText2
		FROM LEO0000.implementer.Synonym S
		ORDER BY 1, 2

	OPEN oCursor
	FETCH oCursor INTO @cSQLText1, @cSQLText2
	WHILE @@fetch_status = 0
		BEGIN
		
		IF EXISTS (SELECT 1 FROM LEO0000.sys.Objects O JOIN LEO0000.sys.Schemas S ON S.Schema_ID = O.Schema_ID AND S.Name + '.' + O.Name = @cSQLText1)
			EXEC (@cSQLText2)
		ELSE
			DELETE S FROM LEO0000.implementer.Synonym S WHERE S.BaseObjectSchema + '.' + S.BaseObjectName = @cSQLText1
		--ENDIF

		FETCH oCursor INTO @cSQLText1, @cSQLText2
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure utility.RefreshSynonyms
--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0002
GO

TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, Organization, RoleID, Password, PasswordSalt, PasswordExpirationDateTime, InvalidLoginAttempts, IsAccountLockedOut, Token, TokenCreateDateTime, EmailAddress, IsActive, CountryCallingCodeID, Phone, IsPhoneVerified, IsSuperAdministrator)
VALUES 
	('Nam', N'AH', N'Security Test', N'Nameer', N'SK', 1, N'214676C4D305E151B0FB4FA87637F0C356750B288C6DA3FE7259CDCDFEE2E709', N'500918F4-F98F-4DEC-80FB-1087E993A021', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2015-07-31T05:32:24.590' AS DateTime), N'nalhadithi@skotkonung.com', 1, 0, NULL, 0, 1),
	('Jonathan', N'Cole', N'System Development', N'JCole', N'Skotkonung', 1, N'67589DA2DA1F9EFEB1B78C407E1A66B4CDE8577D259BBAA420FB17C798B6D3C5', N'D2A94546-77A0-448B-B754-E78A6C458CC9', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2016-01-20T05:12:15.393' AS DateTime), N'jcole@skotkonung.com', 1, 0, NULL, 0, 1),
	('John', N'Lyons', N'Development Team', N'jlyons', N'OceanDisc LLC', 1, N'138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', N'71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2015-11-16T22:14:17.917' AS DateTime), N'john.lyons@oceandisc.com', 1, 236, N'7065992970', 1, 1),
	('Usamah', N'Mahmood', NULL, N'usamah', N'SK', 1, NULL, NULL, CAST(N'2016-02-19T11:11:41.000' AS DateTime), 2, 0, NULL, NULL, N'umahmood@skotkonung.com', 1, 0, NULL, 0, 0),
	('Todd', N'Pires', N'Mr.', N'toddpires', N'OceanDisc', 1, N'492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', N'5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2016-08-24T18:23:07.760' AS DateTime), N'todd.pires@oceandisc.com', 1, 236, N'7275791066', 0, 1),
	('Kevin', N'Ross', NULL, N'kevin', N'OceanDisc', 1, N'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', N'B9EF686D-261D-462F-AD81-24157BC4C522', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, N'424F8425-B7D6-98DF-13F0042EA0ED8979', CAST(N'2015-08-26T22:42:36.653' AS DateTime), N'kevin.ross@oceandisc.com', 1, 236, N'4104303492', 0, 1),
	('Rabaa', N'Tarhouni', N'DBM', N'Rabaa', N'Skotkonnung Ltd.', 1, N'35DD22A307D339050F803988C096A93435CF6E54F35E4531BA8568003D439779', N'B7C57F71-0FAB-4999-ACA8-BE0816879294', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2015-03-23T21:07:30.473' AS DateTime), N'rabaat@skotkonung.com', 1, 228, N'05362902172', 1, 1),
	('Ian', N'van Mourik', N'Development Team', N'IanVM', N'Skotkonung', 1, N'DC8013389868E63E539C553780046DB1DF3A80090FC66A2972A5416643E26D23', N'CCB4FD4C-0453-4245-A43C-01F1C620939C', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, N'98F4EAF6-0E06-B0CB-16226829184A876E', CAST(N'2015-03-22T17:19:14.507' AS DateTime), N'ivmourik@skotkonung.com', 1, 0, NULL, 0, 1)
GO

UPDATE P
SET P.DefaultProjectID = (SELECT IDD.DropdownID FROM LEO0000.implementer.ImplementerDropdownData IDD WHERE IDD.DropdownCode = 'Project' AND IDD.ImplementerCode = 'LEO0002')
FROM person.Person P
GO

TRUNCATE TABLE person.PersonPermissionable
GO
--End file LEO000X - 04 - Data.sql

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.1 - 2017.01.23 16.57.19')
GO
--End build tracking

