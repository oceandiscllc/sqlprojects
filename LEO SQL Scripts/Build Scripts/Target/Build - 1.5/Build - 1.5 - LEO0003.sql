-- File Name:	LEO0003.sql
-- Build Key:	Build - 1.5 - 2017.02.20 19.06.45

USE LEO0003
GO

-- ==============================================================================================================================
-- Procedures:
--		aggregator.GetActivityReportAggregatorByActivityReportAggregatorID
--		aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
--		core.GetImplementerSetupValuesBySetupKeyList
--		eventlog.LogActivityReportAggregatorAction
--		eventlog.LogBudgetAction
--		eventlog.LogPermissionableAction
--		eventlog.LogTrendReportAggregatorAction
--		force.GetForceByForceID
--		utility.LogSQLBuild
--
-- Schemas:
--		aggregator
--
-- Tables:
--		aggregator.ActivityReportAggregator
--		aggregator.TrendReportAggregator
--
-- Views:
--		workflow.WorkflowView
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0003
GO

EXEC utility.AddSchema 'aggregator'
GO

EXEC utility.DropObject 'aggregator.ActivityReport'
EXEC utility.DropObject 'aggregator.TrendReport'
GO

EXEC utility.DropColumn 'activity.Activity', 'UpdateDateTime'
EXEC utility.DropColumn 'asset.Asset', 'UpdateDateTime'
EXEC utility.DropColumn 'atmospheric.Atmospheric', 'UpdateDateTime'
EXEC utility.DropColumn 'contact.Contact', 'UpdateDateTime'
EXEC utility.DropColumn 'course.Course', 'UpdateDateTime'
EXEC utility.DropColumn 'course.CourseContact', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Campaign', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Incident', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Module', 'UpdateDateTime'
EXEC utility.DropColumn 'distributor.Distributor', 'UpdateDateTime'
EXEC utility.DropColumn 'distributor.DistributorTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'finding.Finding', 'UpdateDateTime'
EXEC utility.DropColumn 'force.Force', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryCampaign', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryProduct', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Indicator', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Milestone', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Objective', 'UpdateDateTime'
EXEC utility.DropColumn 'mediareport.MediaReport', 'UpdateDateTime'
EXEC utility.DropColumn 'procurement.EquipmentCatalog', 'UpdateDateTime'
EXEC utility.DropColumn 'procurement.EquipmentInventory', 'UpdateDateTime'
EXEC utility.DropColumn 'product.Product', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductCampaign', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductCommunicationTheme', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductDistribution', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'recommendation.Recommendation', 'UpdateDateTime'
GO

EXEC utility.SetDefaultConstraint 'activityreport.ActivityReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'dbo.RequestForInformation', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'impactstory.ImpactStory', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'spotreport.SpotReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'trendreport.TrendReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
GO

--Begin table aggregator.ActivityReport
DECLARE @TableName VARCHAR(250) = 'aggregator.ActivityReportAggregator'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'aggregator.ActivityReport'

CREATE TABLE aggregator.ActivityReportAggregator
	(
	ActivityReportAggregatorID INT NOT NULL IDENTITY(1,1),
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportAggregatorID'
GO
--End table aggregator.ActivityReportAggregator

--Begin table aggregator.TrendReportAggregator
DECLARE @TableName VARCHAR(250) = 'aggregator.TrendReportAggregator'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'aggregator.TrendReport'

CREATE TABLE aggregator.TrendReportAggregator
	(
	TrendReportAggregatorID INT NOT NULL IDENTITY(1,1),
	TrendReportTitle VARCHAR(250),
	TrendReportStartDate DATE,
	TrendReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'TrendReportAggregatorID'
GO
--End table aggregator.TrendReportAggregator

--Begin table force.Force
EXEC utility.AddColumn 'force.Force', 'CommanderFullName', 'NVARCHAR(250)'
GO
--End table force.Force

--Begin view workflow.WorkflowView
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowView'

EXEC utility.DropObject @TableName
GO

CREATE VIEW workflow.WorkflowView AS
SELECT 
	W.WorkflowID,
	W.WorkflowName,
	W.EntityTypeCode,
	W.IsActive,
  W.ProjectID,
	WS.WorkflowStepNumber,
	WS.WorkflowStepName,
	WSG.WorkflowStepGroupName,
	WSGP.PersonID
FROM workflow.Workflow W
	JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		AND WS.ProjectID = W.ProjectID
	JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		AND WSG.ProjectID = WS.ProjectID
	JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		AND WSGP.ProjectID = WSG.ProjectID

GO
--End view workflow.WorkflowView

--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0003
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0003
GO

--Begin procedure aggregator.GetActivityReportAggregatorByActivityReportAggregatorID
EXEC utility.DropObject 'aggregator.GetActivityReportAggregatorByActivityReportAggregatorID'
GO
EXEC utility.DropObject 'aggregator.GetActivityReportByActivityReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.ActivityReportAggregator table
-- ==============================================================================================
CREATE PROCEDURE aggregator.GetActivityReportAggregatorByActivityReportAggregatorID

@ActivityReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReportAggregator', 0, @ActivityReportAggregatorID)
	
	SELECT
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.ActivityReportAggregatorID,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportTitle,
		AR.ReportDetail,
		AR.Summary
	FROM aggregator.ActivityReportAggregator AR
	WHERE AR.ActivityReportAggregatorID = @ActivityReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'ActivityReportAggregator', @ActivityReportAggregatorID, 0

	EXEC workflow.GetEntityWorkflowPeople 'ActivityReportAggregator', @ActivityReportAggregatorID, 0, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.ActivityReportAggregator AR ON AR.ActivityReportAggregatorID = EL.EntityID
			AND AR.ActivityReportAggregatorID = @ActivityReportAggregatorID
			AND EL.EntityTypeCode = 'ActivityReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetActivityReportAggregatorByActivityReportAggregatorID

--Begin procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
EXEC utility.DropObject 'aggregator.GetTrendReportAggregatorByTrendReportAggregatorID'
GO
EXEC utility.DropObject 'aggregator.GetTrendReportByTrendReportID'
GO
EXEC utility.DropObject 'aggregator.GetTrendReportByTrendReportAggregatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.TrendReportAggregator table
-- ===========================================================================================
CREATE PROCEDURE aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

@TrendReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReportAggregator', 0, @TrendReportAggregatorID)
	
	SELECT
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.TrendReportAggregatorID,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportTitle,
		TR.ReportDetail,
		TR.Summary
	FROM aggregator.TrendReportAggregator TR
	WHERE TR.TrendReportAggregatorID = @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'TrendReportAggregator', @TrendReportAggregatorID, 0

	EXEC workflow.GetEntityWorkflowPeople 'TrendReportAggregator', @TrendReportAggregatorID, 0, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Situational Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Situational Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Situational Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Situational Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.TrendReportAggregator TR ON TR.TrendReportAggregatorID = EL.EntityID
			AND TR.TrendReportAggregatorID = @TrendReportAggregatorID
			AND EL.EntityTypeCode = 'TrendReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetImplementerSetupValuesBySetupKeyList
EXEC utility.DropObject 'core.GetImplementerSetupValuesBySetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return SetupValues from a list of SetupKeys from the core.ImplementerSetup table
-- ============================================================================================================

CREATE PROCEDURE core.GetImplementerSetupValuesBySetupKeyList

@SetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SetupKeyList = ''
		BEGIN

		SELECT 
			I.SetupKey,
			I.SetupValue 
		FROM core.ImplementerSetup I
		ORDER BY I.SetupKey

		END
	ELSE
		BEGIN

		SELECT 
			I.SetupKey,
			I.SetupValue 
		FROM core.ImplementerSetup I
			JOIN core.ListToTable(@SetupKeyList, ',') LTT ON LTT.ListItem = I.SetupKey
		ORDER BY I.SetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetImplementerSetupValuesBySetupKeyList

--Begin procedure eventlog.LogActivityReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogActivityReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogActivityReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReportAggregator',
			T.ActivityReportAggregatorID,
			@Comments,
			@ProjectID
		FROM aggregator.ActivityReportAggregator T
		WHERE T.ActivityReportAggregatorID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReportAggregator',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('ActivityReportAggregator'), ELEMENTS
			)
		FROM aggregator.ActivityReportAggregator T
		WHERE T.ActivityReportAggregatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogActivityReportAggregatorAction

--Begin procedure eventlog.LogBudgetAction
EXEC utility.DropObject 'eventlog.LogBudgetAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogBudgetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			T.BudgetID,
			@Comments,
			@ProjectID
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Budget'), ELEMENTS
			)
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogBudgetAction

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			T.*
			FOR XML RAW('Permissionable'), ELEMENTS
			)
		FROM permissionable.Permissionable T 
		WHERE T.PermissionableID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableAction

--Begin procedure eventlog.LogTrendReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogTrendReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'TrendReportAggregator',
			T.TrendReportAggregatorID,
			@Comments,
			@ProjectID
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'TrendReportAggregator',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('TrendReportAggregator'), ELEMENTS
			)
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAggregatorAction

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		FU.CommanderContactID, 
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderContactNameFormatted,
		FU.DeputyCommanderContactID,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderContactNameFormatted,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure utility.LogSQLBuild
EXEC Utility.DropObject 'utility.LogSQLBuild'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to log completion of a SQL build
-- ================================================================
CREATE PROCEDURE utility.LogSQLBuild

@BuildKey VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO syslog.BuildLog (BuildKey) VALUES (@BuildKey)

END
GO
--End procedure utility.LogSQLBuild

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0003
GO

EXEC core.ImplementerSetupAddUpdate 'IsProjectRequired', 1

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.5 - 2017.02.20 19.06.45'
GO
--End build tracking

