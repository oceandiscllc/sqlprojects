-- File Name:	LEO0001.sql
-- Build Key:	Build - 1.8 - 2017.06.19 18.33.31

USE LEO0001
GO

-- ==============================================================================================================================
-- Procedures:
--		document.GetDocumentByDocumentName
--		product.GetProductByProductID
--		workflow.IsWorkflowComplete
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0001
GO


--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0001
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0001
GO

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
--
-- Author:		Todd Pires
-- Create date:	2017.05.06
-- Description:	Added facebook url support
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFBPageID BIGINT
	DECLARE @nFBPostID BIGINT
	DECLARE @cFBPostName VARCHAR(MAX)

	SELECT
		@cFBPostName = FBPO.Name,
		@nFBPageID = FBPO.PageID,
		@nFBPostID = PD.FacebookPostID
	FROM integration.FacebookPost FBPO
		JOIN product.ProductDistribution PD ON PD.FacebookPostID = FBPO.ID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID

	--Product
	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.Quantity,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	--ProductCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN campaign.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ProductCommunicationTheme
	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProductDistribution
	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.FacebookPostID,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		@cFBPostName AS FacebookPostName,
		'https://www.facebook.com/' + CAST(ISNULL(@nFBPageID, 0) AS VARCHAR(20)) + '/posts/' + CAST(ISNULL(@nFBPostID, 0) AS VARCHAR(20)) AS FacebookPostURL
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	--ProductDistributionCount
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID
		AND PD.ProjectID = @ProjectID

	--ProductRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM product.ProductRelevantTheme PRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = PRT.RelevantThemeID
			AND PRT.ProductID = @ProductID
			AND PRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ProductTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
		AND PT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure workflow.IsWorkflowComplete
EXEC Utility.DropObject 'workflow.IsWorkflowComplete'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.06.03
-- Description:	A stored procedure to return a flag indicating if a workflosw is complete
-- ======================================================================================
CREATE PROCEDURE workflow.IsWorkflowComplete

@EntityTypeCode VARCHAR(50), 
@EntityID INT, 
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) > 0
			THEN 1
			ELSE 0
		END AS IsWorkflowComplete

END
GO
--End procedure workflow.IsWorkflowComplete

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0001
GO

EXEC core.ImplementerSetupAddUpdate 'PortalImplementerCode', 'LEO0001'
GO
--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin post process file LEO0001.sql
--Begin strip identity specifications and create composite primary keys
DECLARE @cColumnName VARCHAR(250)
DECLARE @cIndexName VARCHAR(250)
DECLARE @cIndexType VARCHAR(50)
DECLARE @cNewColumnName VARCHAR(250)
DECLARE @cSchemaName VARCHAR(50)
DECLARE @cSchemaNameTableName VARCHAR(300)
DECLARE @cSchemaNameTableNameColumnName VARCHAR(500)
DECLARE @cSQLText VARCHAR(MAX)
DECLARE @cTableName VARCHAR(250)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		S1.Name AS SchemaName,
		T1.Name AS TableName,
		C1.Name AS ColumnName,
		I.IndexName,
		I.IndexType
	FROM sys.Tables T1
		JOIN sys.Schemas S1 ON S1.Schema_ID = T1.Schema_ID
			AND S1.Name NOT IN ('aggregator', 'core', 'document', 'dropdown', 'eventlog', 'integration', 'person', 'reporting', 'syslog', 'workflow')
		JOIN sys.Columns C1 ON C1.Object_ID = T1.Object_ID
			AND C1.Is_Identity = 1
			AND EXISTS
				(
				SELECT 1
				FROM sys.Tables T2
					JOIN sys.Schemas S2 ON S2.Schema_ID = T2.Schema_ID
					JOIN sys.Columns C2 ON C2.Object_ID = T2.Object_ID
						AND S1.Name = S2.Name
						AND T1.Name = T2.Name
						AND C2.Name = 'ProjectID'
				)
		JOIN
			(
			SELECT 
				C3.Name AS ColumnName,
				I.Name AS IndexName,
				I.type_desc AS IndexType
			FROM sys.indexes I
				JOIN sys.index_columns IC ON IC.Object_ID = I.Object_ID 
					AND IC.Index_ID = I.Index_ID
				JOIN sys.Columns C3 ON C3.Object_ID = IC.Object_ID
					AND C3.Column_ID = IC.Column_ID
					AND I.is_primary_key = 1
			) I ON I.ColumnName = C1.Name
	ORDER BY 1, 2, 3

OPEN oCursor
FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
WHILE @@fetch_status = 0
	BEGIN

	SET @cNewColumnName = @cColumnName + '_NoAN'
	SET @cSchemaNameTableName = @cSchemaName + '.' + @cTableName
	SET @cSchemaNameTableNameColumnName = @cSchemaNameTableName + '.' + @cNewColumnName

	EXEC utility.AddColumn @cSchemaNameTableName, @cNewColumnName, 'INT';

	SET @cSQLText = 'UPDATE T SET T.' + @cNewColumnName + ' = T.' + @cColumnName + ' FROM ' + @cSchemaNameTableName + ' T'
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP CONSTRAINT ' + @cIndexName
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP COLUMN ' + @cColumnName
	EXEC (@cSQLText);

	EXEC sp_rename @cSchemaNameTableNameColumnName, @cColumnName, 'COLUMN';
	EXEC utility.SetDefaultConstraint @cSchemaNameTableName, @cColumnName, 'INT', '0', 1;

	IF @cIndexType = 'CLUSTERED'
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	ELSE 
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyNonClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	--ENDIF

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End strip identity specifications and create composite primary keys

--Begin drop triggers
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		'DROP Trigger ' + S.Name + '.' + O.Name AS SQLText
	FROM sysobjects O
		JOIN sys.tables T ON O.Parent_Obj = T.Object_ID
		JOIN sys.schemas S ON T.Schema_ID = S.Schema_ID 
			AND O.Type = 'TR' 

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSQLText
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End drop triggers

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P1.PersonID,
	P2.ProjectID
FROM person.Person P1 
	CROSS APPLY dropdown.Project P2
WHERE P1.IsSuperAdministrator = 1
	AND P2.ProjectID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP
		WHERE PP.PersonID = P1.PersonID
			AND PP.ProjectID = P2.ProjectID
		)
--End post process file LEO0001.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.8 - 2017.06.19 18.33.31'
GO
--End build tracking

