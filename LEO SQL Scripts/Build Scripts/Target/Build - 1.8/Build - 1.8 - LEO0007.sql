-- File Name:	LEO0007.sql
-- Build Key:	Build - 1.8 - 2017.06.19 18.33.31

USE LEO0007
GO

-- ==============================================================================================================================
-- Procedures:
--		document.GetDocumentByDocumentName
--		product.GetProductByProductID
--		workflow.IsWorkflowComplete
-- ==============================================================================================================================

--Begin file LEO000X - 01 - Tables.sql
USE LEO0007
GO


--End file LEO000X - 01 - Tables.sql

--Begin file LEO000X - 02 - Functions.sql
USE LEO0007
GO


--End file LEO000X - 02 - Functions.sql

--Begin file LEO000X - 03 - Procedures.sql
USE LEO0007
GO

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
--
-- Author:		Todd Pires
-- Create date:	2017.05.06
-- Description:	Added facebook url support
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFBPageID BIGINT
	DECLARE @nFBPostID BIGINT
	DECLARE @cFBPostName VARCHAR(MAX)

	SELECT
		@cFBPostName = FBPO.Name,
		@nFBPageID = FBPO.PageID,
		@nFBPostID = PD.FacebookPostID
	FROM integration.FacebookPost FBPO
		JOIN product.ProductDistribution PD ON PD.FacebookPostID = FBPO.ID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID

	--Product
	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.Quantity,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	--ProductCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN campaign.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ProductCommunicationTheme
	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProductDistribution
	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.FacebookPostID,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		@cFBPostName AS FacebookPostName,
		'https://www.facebook.com/' + CAST(ISNULL(@nFBPageID, 0) AS VARCHAR(20)) + '/posts/' + CAST(ISNULL(@nFBPostID, 0) AS VARCHAR(20)) AS FacebookPostURL
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	--ProductDistributionCount
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID
		AND PD.ProjectID = @ProjectID

	--ProductRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM product.ProductRelevantTheme PRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = PRT.RelevantThemeID
			AND PRT.ProductID = @ProductID
			AND PRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ProductTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
		AND PT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure workflow.IsWorkflowComplete
EXEC Utility.DropObject 'workflow.IsWorkflowComplete'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.06.03
-- Description:	A stored procedure to return a flag indicating if a workflosw is complete
-- ======================================================================================
CREATE PROCEDURE workflow.IsWorkflowComplete

@EntityTypeCode VARCHAR(50), 
@EntityID INT, 
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) > 0
			THEN 1
			ELSE 0
		END AS IsWorkflowComplete

END
GO
--End procedure workflow.IsWorkflowComplete

--End file LEO000X - 03 - Procedures.sql

--Begin file LEO000X - 04 - Data.sql
USE LEO0007
GO

EXEC core.ImplementerSetupAddUpdate 'PortalImplementerCode', 'LEO0001'
GO
--End file LEO000X - 04 - Data.sql

--Begin post process file LEO000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM LEO0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN LEO0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file LEO000X.sql

--Begin exception file LEO0007.sql
USE LEO0007
GO

DECLARE @nProjectID INT = (SELECT IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = 'LEO0007' AND IDD.DropdownCode = 'Project')

INSERT INTO territory.Territory
	(TerritoryName, TerritoryNameLocal, Population, PopulationSource, Location, TerritoryTypeCode, ISOCountryCode2, ParentTerritoryID, IntegrationID, IntegrationCode)
SELECT 
	C.CommunityName, 
	C.ArabicCommunityName, 
	C.Population, 
	C.PopulationSource, 
	C.Location,
	'Community',
	'SY',
	ISNULL((SELECT T.TerritoryID FROM territory.Territory T JOIN (SELECT PTT.ParentTerritoryTypeCode FROM territory.ProjectTerritoryType PTT JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID AND PTT.ProjectID = @nProjectID AND TT.TerritoryTypeCode = 'Community') D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode AND T.Location.STIntersects(C.Location) = 1), 0),
	C.CommunityID,
	'AJACS'
FROM AJACS.dbo.Community C
WHERE C.Location IS NOT NULL
	AND C.IsActive = 1
	AND NOT EXISTS 
		(
		SELECT 1
		FROM territory.Territory T
		WHERE T.IntegrationID = C.CommunityID
			AND T.IntegrationCode = 'AJACS'
		)
ORDER BY C.CommunityName

INSERT INTO implementer.ImplementerDropdownData
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO0007',
	'Territory',
	@nProjectID,
	T.TerritoryID
FROM territory.Territory T
WHERE T.IntegrationCode = 'AJACS'
	AND NOT EXISTS
		(
		SELECT 1
		FROM implementer.ImplementerDropdownData IDD
		WHERE IDD.ImplementerCode = 'LEO0007'
			AND IDD.DropdownCode = 'Territory'
			AND IDD.ProjectID = @nProjectID 
			AND IDD.DropdownID = T.TerritoryID
		)

INSERT INTO implementer.ImplementerDropdownData
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO0001',
	'Territory',
	@nProjectID,
	T.TerritoryID
FROM territory.Territory T
WHERE T.IntegrationCode = 'AJACS'
	AND NOT EXISTS
		(
		SELECT 1
		FROM implementer.ImplementerDropdownData IDD
		WHERE IDD.ImplementerCode = 'LEO0001'
			AND IDD.DropdownCode = 'Territory'
			AND IDD.ProjectID = @nProjectID 
			AND IDD.DropdownID = T.TerritoryID
		)

TRUNCATE TABLE territory.TerritoryAnnex;

INSERT INTO territory.TerritoryAnnex
	(ProjectID, TerritoryID, Notes, TerritoryStatusID)
SELECT
	@nProjectID,
	T.TerritoryID,
	C.Summary,

	CASE
		WHEN ID.ImpactDecisionName = 'Yes'
		THEN (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Active')
		WHEN ID.ImpactDecisionName = 'Pending'
		THEN (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Pending')
		ELSE (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Inactive')
	END

FROM territory.Territory T
	JOIN AJACS.dbo.Community C ON C.CommunityID = T.IntegrationID
		AND C.IsActive = 1
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		AND T.IntegrationCode = 'AJACS'
		AND NOT EXISTS
			(
			SELECT 1
			FROM territory.TerritoryAnnex TA
			WHERE TA.ProjectID = @nProjectID
				AND TA.TerritoryID = T.TerritoryID
			)

TRUNCATE TABLE LEO0001.territory.TerritoryAnnex;

INSERT INTO LEO0001.territory.TerritoryAnnex
	(TerritoryAnnexID, ProjectID, TerritoryID, Notes, TerritoryStatusID)
SELECT
	TA.TerritoryAnnexID,
	TA.ProjectID,
	TA.TerritoryID,
	TA.Notes, 
	TA.TerritoryStatusID
FROM territory.TerritoryAnnex TA
GO
--End exception file LEO0007.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.8 - 2017.06.19 18.33.31'
GO
--End build tracking

