﻿USE LEO0000
GO

EXEC utility.DropObject 'dropdown.IndicatorType'
EXEC utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

DELETE S FROM implementer.Synonym S WHERE S.BaseObjectSchema = 'dropdown' AND S.BaseObjectName = 'IndicatorType';
DELETE S FROM implementer.Synonym S WHERE S.BaseObjectSchema = 'dropdown' AND S.BaseObjectName = 'GetIndicatorTypeData';

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEntityTypeNameData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEventCodeNameData'
GO