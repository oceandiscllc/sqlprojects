USE [[INSTANCENAME]]
GO

EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

--Begin procedure core.ImplementerSetupAddUpdate
EXEC Utility.DropObject 'core.ImplementerSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer setup key records
-- =============================================================================
CREATE PROCEDURE core.ImplementerSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.ImplementerSetup I
	USING (SELECT @SetupKey AS SetupKey) T2
		ON T2.SetupKey = I.SetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		I.SetupValue = @SetupValue
	WHEN NOT MATCHED THEN
	INSERT (SetupKey,SetupValue)
	VALUES
		(
		@SetupKey, 
		@SetupValue
		);

END
GO
--End procedure core.ImplementerSetupAddUpdate

--Begin procedure training.GetModuleByModuleID
EXEC utility.DropObject 'training.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the training.Module table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added call for Module Document information
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ======================================================================
CREATE PROCEDURE training.GetModuleByModuleID

@ModuleID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.ModuleID,
		C.ModuleName,
		C.ModuleTypeID,
		C.Summary,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.SponsorName,
		C.Notes,
		CT.ModuleTypeID,
		CT.ModuleTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName
	FROM training.Module C
		JOIN dropdown.ModuleType CT ON CT.ModuleTypeID = C.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ModuleID = @ModuleID
		
END
GO
--End procedure training.GetModuleByModuleID

--Begin procedure utility.RefreshSynonyms
EXEC utility.DropObject 'utility.RefreshSynonyms'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.23
-- Description:	A stored procedure to drop and recreate synonyms in a client database
-- ==================================================================================
CREATE PROCEDURE utility.RefreshSynonyms

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQLText1 VARCHAR(MAX)
	DECLARE @cSQLText2 VARCHAR(MAX)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			SCH.Name + '.' + SYN.Name
		FROM sys.Synonyms SYN
			JOIN sys.Schemas SCH ON SCH.Schema_ID = SYN.Schema_ID
		ORDER BY 1

	OPEN oCursor
	FETCH oCursor INTO @cSQLText1
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC utility.DropObject @cSQLText1

		FETCH oCursor INTO @cSQLText1
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT DISTINCT
			S.BaseObjectSchema + '.' + S.BaseObjectName  AS SQLText1,
			'CREATE SYNONYM ' + S.BaseObjectSchema + '.' + S.BaseObjectName + ' FOR LEO0000.' + S.BaseObjectSchema + '.' + S.BaseObjectName AS SQLText2
		FROM LEO0000.implementer.Synonym S
		ORDER BY 1, 2

	OPEN oCursor
	FETCH oCursor INTO @cSQLText1, @cSQLText2
	WHILE @@fetch_status = 0
		BEGIN
		
		IF EXISTS (SELECT 1 FROM LEO0000.sys.Objects O JOIN LEO0000.sys.Schemas S ON S.Schema_ID = O.Schema_ID AND S.Name + '.' + O.Name = @cSQLText1)
			EXEC (@cSQLText2)
		ELSE
			DELETE S FROM LEO0000.implementer.Synonym S WHERE S.BaseObjectSchema + '.' + S.BaseObjectName = @cSQLText1
		--ENDIF

		FETCH oCursor INTO @cSQLText1, @cSQLText2
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure utility.RefreshSynonyms