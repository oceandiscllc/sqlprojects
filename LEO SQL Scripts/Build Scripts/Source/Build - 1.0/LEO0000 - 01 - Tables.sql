USE LEO0000
GO

--Begin table dropdown.DocumentCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentCategory
	(
	DocumentCategoryID INT NOT NULL IDENTITY(1,1),
	DocumentCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentCategoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentCategory', 'DisplayOrder,DocumentCategoryName'
GO

INSERT INTO dropdown.DocumentCategory
	(DocumentCategoryName, DisplayOrder)
VALUES
	('Briefings', 1),
	('Reports', 2),
	('M & E', 3),
	('Training', 4),
	('Logistics', 5),
	('Admin', 6)
GO
--End table dropdown.DocumentCategory

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT NOT NULL IDENTITY(1,1),
	DocumentCategoryID INT,
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DocumentCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentType', 'DocumentCategoryID,DisplayOrder,DocumentTypeName'
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.06
-- Description:	A trigger to update the dropdown.DocumentType and permissionable.Permissionable tables
-- ===================================================================================================
CREATE TRIGGER dropdown.TR_DocumentType ON dropdown.DocumentType AFTER INSERT
AS
SET ARITHABORT ON

UPDATE DT
SET DT.DocumentTypeCode = REPLACE(DT.DocumentTypeName, ' ', '')
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NULL

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description)
SELECT
	'Document',
	'View',
	DT.DocumentTypeCode,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = 'Documents'),
	'View documents of type ' + LOWER(DT.DocumentTypeName) + ' in the library'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
		)

GO

ALTER TABLE dropdown.DocumentType ENABLE TRIGGER TR_DocumentType
GO

DECLARE @cDocumentCategoryName VARCHAR(50)
DECLARE @nI INT

SET @cDocumentCategoryName = 'Briefings'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Program', 'Program', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'General', 'General', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Reports'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Situational', 'Situational', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'MonthlyActivity', 'Monthly Activty', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'QuarterlyActivity', 'Quarterly Activity', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'AdditionalActivity', 'Additional Activity', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Spot', 'Spot', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Meeting', 'Meeting', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Training', 'Training', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'RFIResponse', 'RFI Response', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Media', 'Media', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Other', 'Other', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'M & E'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'M&EPlan', 'M & E Plan', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Reporting', 'Reporting', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LogFrame', 'Log Frame', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Training'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Plan', 'Plan', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'QualityAssuranceFeedbackDocument', 'Quality Assurance Feedback Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Trainer1Document', 'Trainer 1 Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Trainer2Document', 'Trainer 2 Document', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Logistics'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Legal&Policy', 'Legal & Policy', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Presentations', 'Presentations', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Procurement', 'Procurement', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName

SET @cDocumentCategoryName = 'Admin'
SET @nI = 0
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LeonardoUserGuides', 'Leonardo User Guides', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'LeonardoAdministration', 'Leonardo Administration', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
INSERT INTO dropdown.DocumentType (DocumentCategoryID, DocumentTypeCode, DocumentTypeName, DisplayOrder) SELECT DC.DocumentCategoryID, 'Other', 'Other', utility.IncrementValue(@nI, 1) FROM dropdown.DocumentCategory DC WHERE DC.DocumentCategoryName = @cDocumentCategoryName
GO
--End table dropdown.DocumentCategory

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Project
	(
	ProjectID INT NOT NULL IDENTITY(0,1),
	ProjectCode VARCHAR(50),
	ProjectName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Project', 'DisplayOrder,ProjectName', 'ProjectID'
GO

SET IDENTITY_INSERT dropdown.Project ON
GO

INSERT INTO dropdown.Project (ProjectID) VALUES (0)

SET IDENTITY_INSERT dropdown.Project OFF
GO

INSERT INTO dropdown.Project
	(ProjectName, ProjectCode, DisplayOrder)
VALUES
	('Project One', 'P1', 1),
	('Project Two', 'P2', 2),
	('Project Three', 'P3', 3),
	('Project Four', 'P4', 4)
GO
--End table dropdown.Project

--Begin table implementer.Implementer
DECLARE @TableName VARCHAR(250) = 'implementer.Implementer'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.Implementer
	(
	ImplementerID INT IDENTITY(1,1) NOT NULL,
	ImplementerName VARCHAR(250),
	ImplementerURL VARCHAR(2000),
	ImplementerCode VARCHAR(25)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Implementer', 'ImplementerName,ImplementerCode'
GO

INSERT INTO implementer.Implementer
	(ImplementerName, ImplementerURL, ImplementerCode)
VALUES
	('Implementer 1', 'Abby', 'LEO0001'),
	('Implementer 2', 'Niko', 'LEO0002'),
	('Implementer 3', 'Odee', 'LEO0003'),
	('Implementer 4', 'Petra', 'LEO0004')
GO
--End table implementer.Implementer

--Begin table implementer.ImplementerDropdown
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdown'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.ImplementerDropdown
	(
	ImplementerDropdownID INT IDENTITY(1,1) NOT NULL,
	ImplementerCode VARCHAR(25),
	DropdownCode VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ImplementerDropdownID'
EXEC utility.SetIndexClustered @TableName, 'IX_ImplementerDropdown', 'ImplementerCode,DropdownCode'
GO

INSERT INTO implementer.ImplementerDropdown
	(ImplementerCode, DropdownCode)
VALUES
	('LEO0001', 'ActivityType'),
	('LEO0001', 'AreaOfOperationType'),
	('LEO0001', 'AssetType'),
	('LEO0001', 'AtmosphericType'),
	('LEO0001', 'CommunicationTheme'),
	('LEO0001', 'ConfidenceLevel'),
	('LEO0001', 'ContactStatus'),
	('LEO0001', 'ContactType'),
	('LEO0001', 'Country'),
	('LEO0001', 'CountryCallingCode'),
	('LEO0001', 'Currency'),
	('LEO0001', 'DateFilter'),
	('LEO0001', 'DistributorType'),
	('LEO0001', 'DocumentType'),
	('LEO0001', 'EngagementStatus'),
	('LEO0001', 'FindingStatus'),
	('LEO0001', 'FindingType'),
	('LEO0001', 'FundingSource'),
	('LEO0001', 'ImpactDecision'),
	('LEO0001', 'IncidentType'),
	('LEO0001', 'IndicatorType'),
	('LEO0001', 'InformationValidity'),
	('LEO0001', 'LearnerProfileType'),
	('LEO0001', 'LogicalFrameworkStatus'),
	('LEO0001', 'MediaReportSourceType'),
	('LEO0001', 'MediaReportType'),
	('LEO0001', 'ModuleType'),
	('LEO0001', 'ObjectiveType'),
	('LEO0001', 'ProductOrigin'),
	('LEO0001', 'ProductStatus'),
	('LEO0001', 'ProductType'),
	('LEO0001', 'ProgramType'),
	('LEO0001', 'Project'),
	('LEO0001', 'RequestForInformationResultType'),
	('LEO0001', 'RequestForInformationStatus'),
	('LEO0001', 'ResourceProvider'),
	('LEO0001', 'Role'),
	('LEO0001', 'SourceCategory'),
	('LEO0001', 'SourceType'),
	('LEO0001', 'StatusChange'),
	('LEO0001', 'SubContractorBusinessType'),
	('LEO0001', 'SubContractorRelationshipType'),
	('LEO0001', 'TargetAudience'),
	('LEO0001', 'UnitType'),
	('LEO0001', 'VettingOutcome')
GO

INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0002', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0003', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
INSERT INTO implementer.ImplementerDropdown (ImplementerCode, DropdownCode) SELECT 'LEO0004', ID.DropdownCode FROM implementer.ImplementerDropdown ID WHERE ID.ImplementerCode = 'LEO0001';
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdownData
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdownData'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.ImplementerDropdownData
	(
	ImplementerDropdownDataID INT IDENTITY(1,1) NOT NULL,
	ImplementerCode VARCHAR(25),
	DropdownCode VARCHAR(50),
	ProjectID INT,
	DropdownID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DropdownID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementerDropdownDataID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementerDropdownData', 'ImplementerCode,DropdownCode,ProjectID,DropdownID'
GO

--Begin table implementer.ImplementerDropdownData
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT ''' + ID.ImplementerCode + ''', '''
			+ T.Name 
			+ ''', D.' 
			+ T.Name 
			+ 'ID FROM ' 
			+ S.Name 
			+ '.' 
			+ T.Name 
			+ ' D' AS SQLText
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
			AND S.Name = 'dropdown'
			AND T.Name NOT IN ('DocumentCategory','Project')
	ORDER BY T.Name

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText)

	FETCH oCursor INTO @cSQLText

	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0001', 'Project', 1)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0002', 'Project', 2)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0003', 'Project', 3)
INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) VALUES ('LEO0004', 'Project', 4)
GO

INSERT INTO implementer.ImplementerDropdownData 
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO000' + CAST(P.ProjectID AS VARCHAR(10)),
	'Territory',
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P
	CROSS APPLY territory.Territory T
		WHERE P.ProjectID > 0
GO
--End table implementer.ImplementerDropdownData

--Begin table implementer.Synonym
DECLARE @TableName VARCHAR(250) = 'implementer.Synonym'

EXEC utility.DropObject @TableName

CREATE TABLE implementer.Synonym
	(
	SynonymID INT IDENTITY(1,1) NOT NULL,
	BaseObjectSchema VARCHAR(50),
	BaseObjectName VARCHAR(250)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'SynonymID'
GO

INSERT INTO implementer.Synonym
	(BaseObjectSchema, BaseObjectName)
VALUES
	('core', 'EntityType'),
	('core', 'FormatDate'),
	('core', 'FormatDateTime'),
	('core', 'FormatTime'),
	('core', 'GetEntityTypeNameByEntityTypeCode'),
	('core', 'GetSystemSetupValueBySetupKey'),
	('core', 'ListToTable'),
	('core', 'MenuItem'),
	('core', 'MenuItemPermissionableLineage'),
	('document', 'FileType'),
	('document', 'FormatPhysicalFileSize'),
	('dropdown', 'CountryCallingCode'),
	('dropdown', 'GetControllerData'),
	('dropdown', 'GetEntityTypeName'),
	('dropdown', 'GetMethodData'),
	('dropdown', 'GetTerritoryTypeData'),
	('dropdown', 'GetProjectNameByProjectID'),
	('implementer', 'ImplementerDropdownData'),
	('permissionable', 'GetPermissionableByPermissionableID'),
	('permissionable', 'GetPermissionableGroups'),
	('permissionable', 'Permissionable'),
	('permissionable', 'PermissionableGroup'),
	('syslog', 'ApplicationErrorLog'),
	('syslog', 'DuoWebTwoFactorLog'),
	('territory', 'FormatTerritoryNameByTerritoryID'),
	('territory', 'GetDescendantTerritoriesByByTerritoryID'),
	('territory', 'Territory'),
	('territory', 'TerritoryType')
GO

INSERT INTO implementer.Synonym
	(BaseObjectSchema, BaseObjectName)
SELECT 
	S.Name, 
	T.Name
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
		AND S.Name = 'dropdown'
		AND T.Name <> 'DocumentCategory'

UNION

SELECT 
	S.Name,
	'Get' + T.Name + 'Data'
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN implementer.ImplementerDropdown ID ON ID.DropdownCode = T.Name
		AND S.Name = 'dropdown'
		AND T.Name <> 'DocumentCategory'

ORDER BY 2
--End table implementer.Synonym
