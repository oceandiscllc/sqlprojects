--Begin schemas
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'document'
EXEC utility.AddSchema 'implementer'
GO
--End schemas

--Begin create synonyms
DECLARE @cSQLText1 VARCHAR(MAX)
DECLARE @cSQLText2 VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		S.BaseObjectSchema + '.' + S.BaseObjectName  AS SQLText1,
		'CREATE SYNONYM ' + S.BaseObjectSchema + '.' + S.BaseObjectName + ' FOR LEO0000.' + S.BaseObjectSchema + '.' + S.BaseObjectName AS SQLText2
	FROM LEO0000.implementer.Synonym S
	ORDER BY S.BaseObjectSchema, S.BaseObjectName

OPEN oCursor
FETCH oCursor INTO @cSQLText1, @cSQLText2
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC utility.DropObject @cSQLText1
	EXEC (@cSQLText2)

	FETCH oCursor INTO @cSQLText1, @cSQLText2
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor

GO
--End create synonyms

--Begin procedure core.ImplementerSetupAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.ImplementerSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer setup key records
-- =============================================================================
CREATE PROCEDURE core.ImplementerSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM core.ImplementerSetup S WHERE S.SetupKey = @SetupKey)
		BEGIN

		INSERT INTO core.ImplementerSetup
			(SetupKey,SetupValue)
		VALUES
			(@SetupKey,@SetupValue)

		END
	ELSE
		BEGIN

		UPDATE core.ImplementerSetup
		SET SetupValue = @SetupValue
		WHERE SetupKey = @SetupKey

		END
	--ENDIF
	
END
GO
--End procedure core.ImplementerSetupAddUpdate

--Begin procedure utility.DropFullTextIndex
EXEC utility.DropObject 'utility.DropFullTextIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropFullTextIndex

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT * FROM sys.fulltext_indexes FTI WHERE FTI.object_id = OBJECT_ID(@TableName))
		BEGIN

		SET @cSQL = 'ALTER FULLTEXT INDEX ON ' + @TableName + ' DISABLE'
		EXEC (@cSQL)

		SET @cSQL = 'DROP FULLTEXT INDEX ON ' + @TableName
		EXEC (@cSQL)

		END
	--ENDIF
	
END
GO
--End procedure utility.DropFullTextIndex

IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'DocumentSearchCatalog')
	CREATE FULLTEXT CATALOG DocumentSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO
