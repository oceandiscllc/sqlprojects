USE LEO0000
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.11.13
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Author:			Todd Pires
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Author:			Todd Pires
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use XML - based on a script by Kshitij Satpute from SQLServerCentral.com
--
-- Author:			Todd Pires
-- Update date:	2015.08.02
-- Description:	Converted to NVARCHAR
-- ===================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.GetSystemSetupValueBySetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a SetupValue for a SetupKey from the core.SystemSetup table
-- =============================================================================================
CREATE FUNCTION core.GetSystemSetupValueBySetupKey
(
@SetupKey VARCHAR(250),
@DefaultSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSetupValue VARCHAR(MAX)
	
	SELECT @cSetupValue = S.SetupValue 
	FROM core.SystemSetup S
	WHERE S.SetupKey = @SetupKey
	
	RETURN ISNULL(@cSetupValue, @DefaultSetupValue)

END
GO
--End function core.GetSystemSetupValueBySetupKey

--Begin function document.FormatPhysicalFileSize
EXEC utility.DropObject 'document.FormatPhysicalFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatPhysicalFileSize
(
@PhysicalFileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @PhysicalFileSize < 1000
		THEN CAST(@PhysicalFileSize as VARCHAR(50)) + ' b'
		WHEN @PhysicalFileSize < 1000000
		THEN CAST(ROUND((@PhysicalFileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @PhysicalFileSize < 1000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @PhysicalFileSize < 1000000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatPhysicalFileSize

--Begin function territory.FormatTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.FormatTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the formatted name of a territory
-- ===================================================================

CREATE FUNCTION territory.FormatTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cTerritoryNameFormatted VARCHAR(300) = ''
	
	SELECT @cTerritoryNameFormatted = T.TerritoryName + ' (' + TT.TerritoryTypeName + ')' 
	FROM territory.Territory T 
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryNameFormatted, '')
	
END
GO
--End function territory.FormatTerritoryNameByTerritoryID

--Begin function territory.GetDescendantTerritoriesByByTerritoryID
EXEC utility.DropObject 'territory.GetDescendantTerritoriesByByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the descendant territories of a territory
-- ===========================================================================

CREATE FUNCTION territory.GetDescendantTerritoriesByByTerritoryID
(
@TerritoryID INT
)

RETURNS @tTable TABLE (TerritoryID INT NOT NULL PRIMARY KEY)

AS
BEGIN

	WITH HD (ParentTerritoryID,TerritoryID,NodeLevel) AS
		(
		SELECT
			T.ParentTerritoryID,
			T.TerritoryID,
			1
		FROM territory.Territory T
		WHERE T.ParentTerritoryID = @TerritoryID

		UNION ALL

		SELECT
			HD.TerritoryID AS ParentTerritoryID,
			T.TerritoryID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM territory.Territory T
				JOIN HD ON HD.TerritoryID = T.ParentTerritoryID
		)	

	INSERT INTO @tTable
		(TerritoryID)
	SELECT @TerritoryID

	UNION

	SELECT HD.TerritoryID
	FROM HD

	RETURN
	
END
GO
--End function territory.GetDescendantTerritoriesByByTerritoryID