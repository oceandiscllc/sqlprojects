USE LEO0000
GO

--Begin schemas
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'implementer'
GO
--End schemas

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SetupKey VARCHAR(250),
@SetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM core.SystemSetup S WHERE S.SetupKey = @SetupKey)
		BEGIN

		INSERT INTO core.SystemSetup
			(SetupKey,SetupValue)
		VALUES
			(@SetupKey,@SetupValue)

		END
	ELSE
		BEGIN

		UPDATE core.SystemSetup
		SET SetupValue = @SetupValue
		WHERE SetupKey = @SetupKey

		END
	--ENDIF
	
END
GO
--End procedure core.SystemSetupAddUpdate