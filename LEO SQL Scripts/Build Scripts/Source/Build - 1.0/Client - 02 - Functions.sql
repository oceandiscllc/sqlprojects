--Begin function core.GetImplementerSetupValueBySetupKey
EXEC utility.DropObject 'core.GetImplementerSetupValueBySetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a SetupValue for a SetupKey from the core.ImplementerSetup table
-- ==================================================================================================
CREATE FUNCTION core.GetImplementerSetupValueBySetupKey
(
@SetupKey VARCHAR(250),
@DefaultSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSetupValue VARCHAR(MAX)
	
	SELECT @cSetupValue = S.SetupValue 
	FROM core.ImplementerSetup S
	WHERE S.SetupKey = @SetupKey
	
	RETURN ISNULL(@cSetupValue, @DefaultSetupValue)

END
GO
--End function core.GetImplementerSetupValueBySetupKey

--Begin function document.FormatFullTextSearchString
EXEC utility.DropObject 'document.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION document.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10)
)

RETURNS VARCHAR(2000)

AS
BEGIN

	SET @SearchString = LTRIM(RTRIM(REPLACE(@SearchString, '"', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE IF CHARINDEX(' ', @SearchString) > 0
		SET @SearchString = CASE WHEN @MatchTypeCode = 'All' THEN REPLACE(@SearchString, ' ', ' AND ') ELSE REPLACE(@SearchString, ' ', ' OR ') END
	--ENDIF

	RETURN @SearchString

END
GO
--End function document.FormatFullTextSearchString

--Begin function person.CanAccessProject
EXEC utility.DropObject 'person.CanAccessProject'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.20
-- Description:	A function to return a bit indicating weather or not a person id can access a projectid
-- ====================================================================================================
CREATE FUNCTION person.CanAccessProject
(
@PersonID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanAccessProject BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = @ProjectID)
		SET @nCanAccessProject = 1
	--ENDIF

	RETURN @nCanAccessProject

END
GO
--End function person.CanAccessProject

--Begin function spotreport.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'spotreport.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION spotreport.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE 
	  @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@IncidentMarker varchar(max)=''
	
	SELECT
		@IncidentMarker += '&markers=icon:' 
			+ replace(core.GetSystemSetupValueBySetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Location.STY,'') AS VARCHAR(MAX)) 
			+ ','
			 + CAST(ISNULL(I.Location.STX,'') AS VARCHAR(MAX))
	 FROM spotreport.SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult +=  @IncidentMarker 

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function spotreport.FormatStaticGoogleMapForSpotReport

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Greg Yingling
-- Create date:	2016.03.02
-- Description:	Refactored to support multiple projects
-- =================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS @tCanHaveAddUpdate TABLE
	(
	ProjectID	INT NOT NULL PRIMARY KEY,
	DefaultProjectID INT NOT NULL DEFAULT 0
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tCanHaveAddUpdate
			(ProjectID, DefaultProjectID)
		SELECT DISTINCT 
			PP.ProjectID, 

			CASE
				WHEN P.DefaultProjectID = PP.ProjectID
				THEN 1
				ELSE 0
			END AS DefaultProjectID

		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
				AND WSGP.PersonID = @PersonID
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
			JOIN person.PersonProject PP ON PP.PersonID = P.PersonID
				AND PP.ProjectID = W.ProjectID
			
		END
	ELSE
		BEGIN
	
		DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		INSERT @tCanHaveAddUpdate
			(ProjectID, DefaultProjectID)
		SELECT DISTINCT
			PP.ProjectID, 
			0
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.PersonProject PP ON PP.PersonID = EWSGP.PersonID
				AND EWSGP.PersonID = @PersonID
			JOIN core.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = 
					CASE
						WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
						THEN @nWorkflowStepCount
						ELSE @nWorkflowStepNumber
					END
	
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN core.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN core.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM core.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData