
--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentName VARCHAR(50),
	DocumentTitle NVARCHAR(250),
	DocumentTypeID INT,
	ProjectID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	PhysicalFileSize BIGINT,
	DocumentData VARBINARY(MAX),
	Thumbnail VARBINARY(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET D.Extension = ISNULL(@cFileExtenstion, @cExtenstion)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		

utility.DropFullTextIndex 'document.Document'

CREATE FULLTEXT INDEX ON document.Document 
	(
	DocumentData TYPE COLUMN Extension LANGUAGE ENGLISH,
	DocumentDescription LANGUAGE ENGLISH,
	DocumentName LANGUAGE ENGLISH,
	DocumentTitle LANGUAGE ENGLISH
	) KEY INDEX PK_Document ON DocumentSearchCatalog
GO
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentEntity
	(
	DocumentEntityID INT IDENTITY(1,1) NOT NULL,
	DocumentID INT,
	EntityTypeCode VARCHAR(50),
	EntityTypeSubCode VARCHAR(50),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'
GO
--End table document.DocumentEntity

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorType
	(
	IndicatorTypeID INT IDENTITY(0,1) NOT NULL,
	IndicatorTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IndicatorTypeName', 'DisplayOrder,IndicatorTypeName', 'IndicatorTypeID'
GO

SET IDENTITY_INSERT dropdown.IndicatorType ON
GO

INSERT INTO dropdown.IndicatorType (IndicatorTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.IndicatorType OFF
GO
--End table dropdown.IndicatorType

--Begin table dropdown.SubContractorCapability
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractorCapability'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubContractorCapability
	(
	SubContractorCapabilityID INT IDENTITY(0,1) NOT NULL,
	SubContractorCapabilityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SubContractorCapabilityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SubContractorCapabilityName', 'DisplayOrder,SubContractorCapabilityName', 'SubContractorCapabilityID'
GO

SET IDENTITY_INSERT dropdown.SubContractorCapability ON
GO

INSERT INTO dropdown.SubContractorCapability (SubContractorCapabilityID) VALUES (0)

SET IDENTITY_INSERT dropdown.SubContractorCapability OFF
GO
--End table dropdown.SubContractorCapability
