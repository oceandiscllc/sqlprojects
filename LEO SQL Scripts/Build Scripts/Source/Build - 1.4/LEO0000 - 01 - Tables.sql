USE LEO0000
GO

--Begin table budget.BudgetItem
DECLARE @TableName VARCHAR(250) = 'budget.BudgetItem'

EXEC utility.DropObject @TableName

CREATE TABLE budget.BudgetItem
	(
	BudgetItemID INT IDENTITY(1,1) NOT NULL,
	BudgetID INT,
	BudgetItemName VARCHAR(250),
	BudgetItemDescription VARCHAR(MAX),
	FundingSourceID INT,
	TotalItemAmount NUMERIC(18,2),
	Q1Forecast NUMERIC(18,2),
	Q1Spend NUMERIC(18,2),
	Q2Forecast NUMERIC(18,2),
	Q2Spend NUMERIC(18,2),
	Q3Forecast NUMERIC(18,2),
	Q3Spend NUMERIC(18,2),
	Q4Forecast NUMERIC(18,2),
	Q4Spend NUMERIC(18,2),
	Q5Forecast NUMERIC(18,2),
	Q5Spend NUMERIC(18,2),
	Q6Forecast NUMERIC(18,2),
	Q6Spend NUMERIC(18,2),
	Q7Forecast NUMERIC(18,2),
	Q7Spend NUMERIC(18,2),
	Q8Forecast NUMERIC(18,2),
	Q8Spend NUMERIC(18,2),
	Q9Forecast NUMERIC(18,2),
	Q9Spend NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BudgetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FundingSourceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q1Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q1Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q2Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q2Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q3Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q3Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q4Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q4Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q5Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q5Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q6Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q6Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q7Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q7Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q8Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q8Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q9Spend', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Q9Forecast', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TotalItemAmount', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BudgetItemID'
EXEC utility.SetIndexClustered @TableName, 'IX_BudgetItem', 'BudgetID,BudgetItemName'
GO
--End table budget.BudgetItem

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.AddColumn @TableName, 'SchemaName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TableName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PrimaryKeyFieldName', 'VARCHAR(50)'
GO

DELETE ET
FROM core.EntityType ET
WHERE ET.EntityTypeCode = 'SubContractorCapability'
GO

UPDATE ET
SET 
	ET.SchemaName = S.Name,
	ET.TableName = T.Name,
	ET.PrimaryKeyFieldName = T.Name + 'ID'
FROM core.EntityType ET
	JOIN LEO0002.sys.Tables T ON T.Name = ET.EntityTypeCode
	JOIN LEO0002.sys.Schemas S ON S.Schema_ID = T.Schema_ID
GO
--End table core.EntityType

