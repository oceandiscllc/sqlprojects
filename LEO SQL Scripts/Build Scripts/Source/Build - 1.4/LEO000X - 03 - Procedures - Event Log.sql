USE [[INSTANCENAME]]
GO

--Begin procedure eventlog.LogActivityAction
EXEC utility.DropObject 'eventlog.LogActivityAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogActivityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Activity',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cActivityCourses VARCHAR(MAX) 
	
		SELECT 
			@cActivityCourses = COALESCE(@cActivityCourses, '') + D.ActivityCourse 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityCourse'), ELEMENTS) AS ActivityCourse
			FROM activity.ActivityCourse T
			WHERE T.ActivityID = @EntityID
			) D

		DECLARE @cActivityContacts VARCHAR(MAX) 
	
		SELECT 
			@cActivityContacts = COALESCE(@cActivityContacts, '') + D.ActivityContact 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityContact'), ELEMENTS) AS ActivityContact
			FROM activity.ActivityContact T
			WHERE T.ActivityID = @EntityID
			) D

		DECLARE @cActivityTerritories VARCHAR(MAX) 
	
		SELECT 
			@cActivityTerritories = COALESCE(@cActivityTerritories, '') + D.ActivityTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityTerritory'), ELEMENTS) AS ActivityTerritory
			FROM activity.ActivityTerritory T 
			WHERE T.ActivityID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Activity',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<ActivityCourses>' + ISNULL(@cActivityCourses, '') + '</ActivityCourses>') AS XML),
			CAST(('<ActivityContacts>' + ISNULL(@cActivityContacts, '') + '</ActivityContacts>') AS XML),
			CAST(('<ActivityTerritories>' + ISNULL(@cActivityTerritories, '') + '</ActivityTerritories>') AS XML)
			FOR XML RAW('Activity'), ELEMENTS
			)
		FROM activity.Activity T
		WHERE T.ActivityID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogActivityAction

--Begin procedure eventlog.LogActivityReportAction
EXEC utility.DropObject 'eventlog.LogActivityReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogActivityReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ActivityReport',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cActivityReportAssets VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportAssets = COALESCE(@cActivityReportAssets, '') + D.ActivityReportAsset 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportAsset'), ELEMENTS) AS ActivityReportAsset
			FROM activityreport.ActivityReportAsset T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportAtmospherics VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportAtmospherics = COALESCE(@cActivityReportAtmospherics, '') + D.ActivityReportAtmospheric
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportAtmospheric'), ELEMENTS) AS ActivityReportAtmospheric
			FROM activityreport.ActivityReportAtmospheric T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportCampaigns VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportCampaigns = COALESCE(@cActivityReportCampaigns, '') + D.ActivityReportCampaign 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportCampaign'), ELEMENTS) AS ActivityReportCampaign
			FROM activityreport.ActivityReportCampaign T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportForces VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportForces = COALESCE(@cActivityReportForces, '') + D.ActivityReportForce 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportForce'), ELEMENTS) AS ActivityReportForce
			FROM activityreport.ActivityReportForce T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportImpactStories VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportImpactStories = COALESCE(@cActivityReportImpactStories, '') + D.ActivityReportImpactStory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportImpactStory'), ELEMENTS) AS ActivityReportImpactStory
			FROM activityreport.ActivityReportImpactStory T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportIncidents = COALESCE(@cActivityReportIncidents, '') + D.ActivityReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportIncident'), ELEMENTS) AS ActivityReportIncident
			FROM activityreport.ActivityReportIncident T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportMediaReports VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportMediaReports = COALESCE(@cActivityReportMediaReports, '') + D.ActivityReportMediaReport 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportMediaReport'), ELEMENTS) AS ActivityReportMediaReport
			FROM activityreport.ActivityReportMediaReport T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportProducts VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportProducts = COALESCE(@cActivityReportProducts, '') + D.ActivityReportProduct 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportProduct'), ELEMENTS) AS ActivityReportProduct
			FROM activityreport.ActivityReportProduct T 
			WHERE T.ActivityReportID = @EntityID
			) D

		DECLARE @cActivityReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cActivityReportTerritories = COALESCE(@cActivityReportTerritories, '') + D.ActivityReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ActivityReportTerritory'), ELEMENTS) AS ActivityReportTerritory
			FROM activityreport.ActivityReportTerritory T 
			WHERE T.ActivityReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReport',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<ActivityReportAssets>' + ISNULL(@cActivityReportAssets, '') + '</ActivityReportAssets>') AS XML),
			CAST(('<ActivityReportAtmospherics>' + ISNULL(@cActivityReportAtmospherics, '') + '</ActivityReportAtmospherics>') AS XML),
			CAST(('<ActivityReportCampaigns>' + ISNULL(@cActivityReportCampaigns, '') + '</ActivityReportCampaigns>') AS XML),
			CAST(('<ActivityReportForces>' + ISNULL(@cActivityReportForces, '') + '</ActivityReportForces>') AS XML),
			CAST(('<ActivityReportImpactStories>' + ISNULL(@cActivityReportImpactStories, '') + '</ActivityReportImpactStories>') AS XML),
			CAST(('<ActivityReportIncidents>' + ISNULL(@cActivityReportIncidents, '') + '</ActivityReportIncidents>') AS XML),
			CAST(('<ActivityReportMediaReports>' + ISNULL(@cActivityReportMediaReports, '') + '</ActivityReportMediaReports>') AS XML),
			CAST(('<ActivityReportProducts>' + ISNULL(@cActivityReportProducts, '') + '</ActivityReportProducts>') AS XML),
			CAST(('<ActivityReportTerritories>' + ISNULL(@cActivityReportTerritories, '') + '</ActivityReportTerritories>') AS XML)
			FOR XML RAW('ActivityReport'), ELEMENTS
			)
		FROM activityreport.ActivityReport T
		WHERE T.ActivityReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogActivityReportAction

--Begin procedure eventlog.LogAssetAction
EXEC utility.DropObject 'eventlog.LogAssetAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			T.AssetID,
			@Comments,
			@ProjectID
		FROM asset.Asset T
		WHERE T.AssetID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cAssetEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetEquipmentResourceProviders = COALESCE(@cAssetEquipmentResourceProviders, '') + D.AssetEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetEquipmentResourceProvider'), ELEMENTS) AS AssetEquipmentResourceProvider
			FROM asset.AssetEquipmentResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		DECLARE @cAssetFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetFinancialResourceProviders = COALESCE(@cAssetFinancialResourceProviders, '') + D.AssetFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetFinancialResourceProvider'), ELEMENTS) AS AssetFinancialResourceProvider
			FROM asset.AssetFinancialResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogAssetActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogAssetActionTable
		FROM asset.Asset T
		WHERE T.AssetID = @EntityID
		
		ALTER TABLE #LogAssetActionTable DROP COLUMN Location
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(A.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<AssetEquipmentResourceProviders>' + ISNULL(@cAssetEquipmentResourceProviders, '') + '</AssetEquipmentResourceProviders>') AS XML),
			CAST(('<AssetFinancialResourceProviders>' + ISNULL(@cAssetFinancialResourceProviders, '') + '</AssetFinancialResourceProviders>') AS XML)
			FOR XML RAW('Asset'), ELEMENTS
			)
		FROM #LogAssetActionTable T
			JOIN asset.Asset A ON A.AssetID = T.AssetID

		DROP TABLE #LogAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAssetAction

--Begin procedure eventlog.LogAtmosphericAction
EXEC utility.DropObject 'eventlog.LogAtmosphericAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAtmosphericAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cAtmosphericTerritories VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericTerritories = COALESCE(@cAtmosphericTerritories, '') + D.AtmosphericTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericTerritory'), ELEMENTS) AS AtmosphericTerritory
			FROM atmospheric.AtmosphericTerritory T 
			WHERE T.AtmosphericID = @EntityID
			) D

		DECLARE @cAtmosphericSources VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericSources = COALESCE(@cAtmosphericSources, '') + D.AtmosphericSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericSource'), ELEMENTS) AS AtmosphericSource
			FROM atmospheric.AtmosphericSource T 
			WHERE T.AtmosphericID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<AtmosphericTerritories>' + ISNULL(@cAtmosphericTerritories, '') + '</AtmosphericTerritories>') AS XML),
			CAST(('<AtmosphericSources>' + ISNULL(@cAtmosphericSources, '') + '</AtmosphericSources>') AS XML)
			FOR XML RAW('Atmospheric'), ELEMENTS
			)
		FROM atmospheric.Atmospheric T
		WHERE T.AtmosphericID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAtmosphericAction

--Begin procedure eventlog.LogCampaignAction
EXEC utility.DropObject 'eventlog.LogCampaignAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCampaignAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			T.CampaignID,
			@Comments,
			@ProjectID
		FROM dbo.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Campaign',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Campaign'), ELEMENTS
			)
		FROM dbo.Campaign T
		WHERE T.CampaignID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCampaignAction

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			T.ContactID,
			@Comments,
			@ProjectID
		FROM contact.Contact T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			T.ContactID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetContactContactTypesXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactVettingsXMLByContactID(T.ContactID) AS XML))
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM contact.Contact T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogCourseAction
EXEC utility.DropObject 'eventlog.LogCourseAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCourseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cCourseContacts VARCHAR(MAX) 
	
		SELECT 
			@cCourseContacts = COALESCE(@cCourseContacts, '') + D.CourseContact 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CourseContact'), ELEMENTS) AS CourseContact
			FROM course.CourseContact T 
			WHERE T.CourseID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<CourseContacts>' + ISNULL(@cCourseContacts, '') + '</CourseContacts>') AS XML)
			FOR XML RAW('Course'), ELEMENTS
			)
		FROM course.Course T
		WHERE T.CourseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCourseAction

--Begin procedure eventlog.LogDataExportAction
EXEC utility.DropObject 'eventlog.LogDataExportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDataExportAction

@PersonID INT,
@DataExportActionXML VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EventData)
	VALUES
		(
		@PersonID,
		'create',
		'Export',
		@DataExportActionXML
		)

END
GO
--End procedure eventlog.LogDataExportAction

--Begin procedure eventlog.LogDistributorAction
EXEC utility.DropObject 'eventlog.LogDistributorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDistributorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Distributor',
			T.DistributorID,
			@Comments,
			@ProjectID
		FROM distributor.Distributor T
		WHERE T.DistributorID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cDistributorTerritories VARCHAR(MAX) = ''
		
		SELECT @cDistributorTerritories = COALESCE(@cDistributorTerritories, '') + D.DistributorTerritory
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DistributorTerritory'), ELEMENTS) AS DistributorTerritory
			FROM distributor.DistributorTerritory T 
			WHERE T.DistributorID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDistributorActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDistributorActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogDistributorActionTable
		FROM distributor.Distributor T
		WHERE T.DistributorID = @EntityID
		
		ALTER TABLE #LogDistributorActionTable DROP COLUMN NaturalReach
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Distributor',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			'<NaturalReach>' + CAST(D.NaturalReach AS VARCHAR(MAX)) + '</NaturalReach>',
			CAST(('<DistributorTerritories>' + ISNULL(@cDistributorTerritories, '') + '</DistributorTerritories>') AS XML)
			FOR XML RAW('Distributor'), ELEMENTS
			)
		FROM #LogDistributorActionTable T
			JOIN distributor.Distributor D ON D.DistributorID = T.DistributorID

		DROP TABLE #LogDistributorActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDistributorAction

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocumentEntities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentEntities = COALESCE(@cDocumentEntities, '') + D.DocumentEntity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentEntity'), ELEMENTS) AS DocumentEntity
			FROM document.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDocumentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDocumentActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogDocumentActionTable
		FROM document.Document T
		WHERE T.DocumentID = @EntityID
		
		ALTER TABLE #LogDocumentActionTable DROP COLUMN DocumentData
		ALTER TABLE #LogDocumentActionTable DROP COLUMN Thumbnail

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(('<DocumentEntities>' + ISNULL(@cDocumentEntities, '') + '</DocumentEntities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM #LogDocumentActionTable T
			JOIN document.Document D ON D.DocumentID = T.DocumentID

		DROP TABLE #LogDocumentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EmailTemplate',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EmailTemplate',
			@EntityID,

			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogEquipmentCatalogAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentCatalogAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('EquipmentCatalog'), ELEMENTS
			)
		FROM procurement.EquipmentCatalog T
		WHERE T.EquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentCatalogAction

--Begin procedure eventlog.LogEquipmentInventoryAction
EXEC utility.DropObject 'eventlog.LogEquipmentInventoryAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentInventoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			T.EquipmentInventoryID,
			@Comments,
			@ProjectID
		FROM procurement.EquipmentInventory T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentInventoryID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			T.EquipmentInventoryID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('EquipmentInventory'), ELEMENTS
			)
		FROM procurement.EquipmentInventory T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentInventoryID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentInventoryAction

--Begin procedure eventlog.LogEventLogAction
EXEC utility.DropObject 'eventlog.LogEventLogAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEventLogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL,
	@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EventLog',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEventLogAction

--Begin procedure eventlog.LogFindingAction
EXEC utility.DropObject 'eventlog.LogFindingAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFindingAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cFindingIndicators VARCHAR(MAX) 
	
		SELECT 
			@cFindingIndicators = COALESCE(@cFindingIndicators, '') + D.FindingIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingIndicators'), ELEMENTS) AS FindingIndicators
			FROM finding.FindingIndicator T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cFindingRecommendations = COALESCE(@cFindingRecommendations, '') + D.FindingRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRecommendations'), ELEMENTS) AS FindingRecommendations
			FROM finding.FindingRecommendation T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingTerritories VARCHAR(MAX) 
	
		SELECT 
			@cFindingTerritories = COALESCE(@cFindingTerritories, '') + D.FindingTerritories 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingTerritories'), ELEMENTS) AS FindingTerritories
			FROM finding.FindingTerritory T 
			WHERE T.FindingID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(('<FindingIndicators>' + ISNULL(@cFindingIndicators, '') + '</FindingIndicators>') AS XML),
			CAST(('<FindingRecommendations>' + ISNULL(@cFindingRecommendations, '') + '</FindingRecommendations>') AS XML),
			CAST(('<FindingTerritories>' + ISNULL(@cFindingTerritories, '') + '</FindingTerritories>') AS XML)
			FOR XML RAW('Finding'), ELEMENTS
			)
		FROM finding.Finding T
		WHERE T.FindingID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFindingAction

--Begin procedure eventlog.LogForceAction
EXEC utility.DropObject 'eventlog.LogForceAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogForceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Force',
			T.ForceID,
			@Comments,
			@ProjectID
		FROM force.Force T
		WHERE T.ForceID = @EntityID


		END
	ELSE
		BEGIN

		DECLARE @cForceEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceEquipmentResourceProviders = COALESCE(@cForceEquipmentResourceProviders, '') + D.ForceEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceEquipmentResourceProvider'), ELEMENTS) AS ForceEquipmentResourceProvider
			FROM force.ForceEquipmentResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceFinancialResourceProviders = COALESCE(@cForceFinancialResourceProviders, '') + D.ForceFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceFinancialResourceProvider'), ELEMENTS) AS ForceFinancialResourceProvider
			FROM force.ForceFinancialResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceUnits VARCHAR(MAX) = ''
		
		SELECT @cForceUnits = COALESCE(@cForceUnits, '') + D.ForceUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceUnit'), ELEMENTS) AS ForceUnit
			FROM force.ForceUnit T 
			WHERE T.ForceID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogForceActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogForceActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogForceActionTable
		FROM force.Force T
		WHERE T.ForceID = @EntityID
		
		ALTER TABLE #LogForceActionTable DROP COLUMN Location
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Force',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(F.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<ForceEquipmentResourceProviders>' + ISNULL(@cForceEquipmentResourceProviders, '') + '</ForceEquipmentResourceProviders>') AS XML),
			CAST(('<ForceFinancialResourceProviders>' + ISNULL(@cForceFinancialResourceProviders, '') + '</ForceFinancialResourceProviders>') AS XML),
			CAST(('<ForceUnits>' + ISNULL(@cForceUnits, '') + '</ForceUnits>') AS XML)
			FOR XML RAW('Force'), ELEMENTS
			)
		FROM #LogForceActionTable T
			JOIN force.Force F ON F.ForceID = T.ForceID

		DROP TABLE #LogForceActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogForceAction

--Begin procedure eventlog.LogImpactStoryAction
EXEC utility.DropObject 'eventlog.LogImpactStoryAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogImpactStoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'ImpactStory',
			T.ImpactStoryID,
			@Comments,
			@ProjectID
		FROM impactstory.ImpactStory T
		WHERE T.ImpactStoryID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ImpactStory',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('ImpactStory'), ELEMENTS
			)
		FROM impactstory.ImpactStory T
		WHERE T.ImpactStoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogImpactStoryAction

--Begin procedure eventlog.LogImplementerSetupAction
EXEC utility.DropObject 'eventlog.LogImplementerSetupAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogImplementerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ImplementerSetup',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogImplementerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogImplementerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogImplementerSetupActionTable
		FROM core.ImplementerSetup ISU
		WHERE ISU.ImplementerSetupID = @EntityID
		
		ALTER TABLE #LogImplementerSetupActionTable DROP COLUMN SetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ImplementerSetup',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			T.*
			FOR XML RAW('ImplementerSetup'), ELEMENTS
			)
		FROM #LogImplementerSetupActionTable T
			JOIN core.ImplementerSetup ISU ON ISU.ImplementerSetupID = T.ImplementerSetupID

		DROP TABLE #LogImplementerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogImplementerSetupAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Incident'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogIncidentActionTable
		FROM dbo.Incident I
		WHERE I.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location
	
		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, @EntityID)
				
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT
			T.*, 
			CAST(('<Location>' + CAST(I.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(@cDocuments AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)
		FROM #LogIncidentActionTable T
			JOIN dbo.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure eventlog.LogIndicatorAction
EXEC utility.DropObject 'eventlog.LogIndicatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Indicator'), ELEMENTS
			)
		FROM logicalframework.Indicator T
		WHERE T.IndicatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorAction

--Begin procedure eventlog.LogIndicatorTypeAction
EXEC utility.DropObject 'eventlog.LogIndicatorTypeAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorTypeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'IndicatorType',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'IndicatorType',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('IndicatorType'), ELEMENTS
			)
		FROM dropdown.IndicatorType T
		WHERE T.IndicatorTypeID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorTypeAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogMediaReportAction
EXEC utility.DropObject 'eventlog.LogMediaReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMediaReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Incident'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cMediaReportSources VARCHAR(MAX) 
	
		SELECT 
			@cMediaReportSources = COALESCE(@cMediaReportSources, '') + D.MediaReportSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('MediaReportSource'), ELEMENTS) AS MediaReportSource
			FROM mediareport.MediaReportSource T 
			WHERE T.MediaReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT
			T.*, 
			CAST(('<MediaReportSources>' + ISNULL(@cMediaReportSources, '') + '</MediaReportSources>') AS XML)
			FOR XML RAW('MediaReport'), ELEMENTS
			)
		FROM mediareport.MediaReport T
		WHERE T.MediaReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMediaReportAction

--Begin procedure eventlog.LogMilestoneAction
EXEC utility.DropObject 'eventlog.LogMilestoneAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMilestoneAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Milestone'), ELEMENTS
			)
		FROM logicalframework.Milestone T
		WHERE T.MilestoneID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMilestoneAction

--Begin procedure eventlog.LogModuleAction
EXEC utility.DropObject 'eventlog.LogModuleAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogModuleAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Module'

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN
	
		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, @EntityID)

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(@cDocuments AS XML)
			FOR XML RAW('Module'), ELEMENTS
			)
		FROM dbo.Module T
		WHERE T.ModuleID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogModuleAction

--Begin procedure eventlog.LogObjectiveAction
EXEC utility.DropObject 'eventlog.LogObjectiveAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogObjectiveAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Objective'), ELEMENTS
			)
		FROM logicalframework.Objective T
		WHERE T.ObjectiveID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogObjectiveAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PermissionableTemplate',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PermissionableTemplate',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			T.*
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T 
		WHERE T.PermissionableTemplateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments,
			@ProjectID
		FROM person.Person T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	ELSE
		BEGIN
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetPersonPermissionablesXMLByPersonID(T.PersonID) AS XML))
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogProductAction
EXEC utility.DropObject 'eventlog.LogProductAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProductAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Product',
			T.ProductID,
			@Comments,
			@ProjectID
		FROM product.Product T
		WHERE T.ProductID = @EntityID

		END
	ELSE
		BEGIN

		DECLARE @cProductCampaigns VARCHAR(MAX) = ''
		
		SELECT @cProductCampaigns = COALESCE(@cProductCampaigns, '') + D.ProductCampaign
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductCampaign'), ELEMENTS) AS ProductCampaign
			FROM product.ProductCampaign T 
			WHERE T.ProductID = @EntityID
			) D

		DECLARE @cProductDistributions VARCHAR(MAX) = ''
		
		SELECT @cProductDistributions = COALESCE(@cProductDistributions, '') + D.ProductDistribution
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductDistribution'), ELEMENTS) AS ProductDistribution
			FROM product.ProductDistribution T 
			WHERE T.ProductID = @EntityID
			) D

		DECLARE @cProductTerritories VARCHAR(MAX) = ''
		
		SELECT @cProductTerritories = COALESCE(@cProductTerritories, '') + D.ProductTerritory
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProductTerritory'), ELEMENTS) AS ProductTerritory
			FROM product.ProductTerritory T 
			WHERE T.ProductID = @EntityID
			) D
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Product',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<ProductCampaigns>' + ISNULL(@cProductCampaigns, '') + '</ProductCampaigns>') AS XML),
			CAST(('<ProductDistributions>' + ISNULL(@cProductDistributions, '') + '</ProductDistributions>') AS XML),
			CAST(('<ProductTerritories>' + ISNULL(@cProductTerritories, '') + '</ProductTerritories>') AS XML)
			FOR XML RAW('Product'), ELEMENTS
			)
		FROM product.Product T
		WHERE T.ProductID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProductAction

--Begin procedure eventlog.LogRecommendationAction
EXEC utility.DropObject 'eventlog.LogRecommendationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationIndicators VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationIndicators = COALESCE(@cRecommendationIndicators, '') + D.RecommendationIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationIndicators'), ELEMENTS) AS RecommendationIndicators
			FROM recommendation.RecommendationIndicator T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationTerritories VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationTerritories = COALESCE(@cRecommendationTerritories, '') + D.RecommendationTerritories 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationTerritories'), ELEMENTS) AS RecommendationTerritories
			FROM recommendation.RecommendationTerritory T 
			WHERE T.RecommendationID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(('<RecommendationIndicators>' + ISNULL(@cRecommendationIndicators, '') + '</RecommendationIndicators>') AS XML),
			CAST(('<RecommendationTerritories>' + ISNULL(@cRecommendationTerritories, '') + '</RecommendationTerritories>') AS XML)
			FOR XML RAW('Recommendation'), ELEMENTS
			)
		FROM recommendation.Recommendation T
		WHERE T.RecommendationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationAction

--Begin procedure eventlog.LogRequestForInformationAction
EXEC utility.DropObject 'eventlog.LogRequestForInformationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRequestForInformationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RequestForInformation',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('RequestForInformation', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RequestForInformation',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('RequestForInformation'), ELEMENTS
			)
		FROM dbo.RequestForInformation T
		WHERE T.RequestForInformationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRequestForInformationAction

--Begin procedure eventlog.LogSocialMediaAction
EXEC utility.DropObject 'eventlog.LogSocialMediaAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSocialMediaAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SocialMedia',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE 
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogsocialmediaActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogsocialmediaActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogsocialmediaActionTable
		FROM integration.facebookapplication T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.FaceBookIntegrationID


		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SocialMedia',
			T.FaceBookIntegrationID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('integration.facebookapplication'), ELEMENTS
			)
		FROM #LogsocialmediaActionTable T
			JOIN integration.facebookapplication F ON F.FaceBookIntegrationID = T.FaceBookIntegrationID

		DROP TABLE #LogsocialmediaActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSocialMediaAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM spotreport.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportTerritories = COALESCE(@cSpotReportTerritories, '') + D.SpotReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportTerritory'), ELEMENTS) AS SpotReportTerritory
			FROM spotreport.SpotReportTerritory T 
			WHERE T.SpotReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportTerritories>' + ISNULL(@cSpotReportTerritories, '') + '</SpotReportTerritories>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM spotreport.SpotReport T
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure eventlog.LogSubContractorAction
EXEC utility.DropObject 'eventlog.LogSubContractorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSubContractorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('SubContractor', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('SubContractor'), ELEMENTS
			)
		FROM dbo.SubContractor T
		WHERE T.SubContractorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSubContractorAction

--Begin procedure eventlog.LogTerritoryAction
EXEC utility.DropObject 'eventlog.LogTerritoryAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTerritoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Territory'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTerritoryAction

--Begin procedure eventlog.LogTrendReportAction
EXEC utility.DropObject 'eventlog.LogTrendReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'TrendReport',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cTrendReportAssets VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportAssets = COALESCE(@cTrendReportAssets, '') + D.TrendReportAsset 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportAsset'), ELEMENTS) AS TrendReportAsset
			FROM trendreport.TrendReportAsset T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportForces VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportForces = COALESCE(@cTrendReportForces, '') + D.TrendReportForce 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportForce'), ELEMENTS) AS TrendReportForce
			FROM trendreport.TrendReportForce T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportIncidents = COALESCE(@cTrendReportIncidents, '') + D.TrendReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportIncident'), ELEMENTS) AS TrendReportIncident
			FROM trendreport.TrendReportIncident T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportMediaReports VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportMediaReports = COALESCE(@cTrendReportMediaReports, '') + D.TrendReportMediaReport 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportMediaReport'), ELEMENTS) AS TrendReportMediaReport
			FROM trendreport.TrendReportMediaReport T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportTerritories = COALESCE(@cTrendReportTerritories, '') + D.TrendReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportTerritory'), ELEMENTS) AS TrendReportTerritory
			FROM trendreport.TrendReportTerritory T 
			WHERE T.TrendReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogTrendReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogTrendReportActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogTrendReportActionTable
		FROM trendreport.TrendReport T
		WHERE T.TrendReportID = @EntityID
		
		ALTER TABLE #LogTrendReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'TrendReport',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<TrendReportAssets>' + ISNULL(@cTrendReportAssets, '') + '</TrendReportAssets>') AS XML),
			CAST(('<TrendReportForces>' + ISNULL(@cTrendReportForces, '') + '</TrendReportForces>') AS XML),
			CAST(('<TrendReportIncidents>' + ISNULL(@cTrendReportIncidents, '') + '</TrendReportIncidents>') AS XML),
			CAST(('<TrendReportMediaReports>' + ISNULL(@cTrendReportMediaReports, '') + '</TrendReportMediaReports>') AS XML),
			CAST(('<TrendReportTerritories>' + ISNULL(@cTrendReportTerritories, '') + '</TrendReportTerritories>') AS XML)
			FOR XML RAW('TrendReport'), ELEMENTS
			)
		FROM #LogTrendReportActionTable T
		WHERE T.TrendReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAction

--Begin procedure eventlog.LogWorkflowAction
EXEC utility.DropObject 'eventlog.LogWorkflowAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkflowAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM Workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM Workflow.WorkflowStepGroup T
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM Workflow.WorkflowStepGroupPerson T 
				JOIN Workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<WorkflowSteps>' + ISNULL(@cWorkflowSteps, '') + '</WorkflowSteps>') AS XML),
			CAST(('<WorkflowStepGroups>' + ISNULL(@cWorkflowStepGroups, '') + '</WorkflowStepGroups>') AS XML),
			CAST(('<WorkflowStepGroupPersons>' + ISNULL(@cWorkflowStepGroupPersons, '') + '</WorkflowStepGroupPersons>') AS XML)
			FOR XML RAW('Workflow'), ELEMENTS
			)
		FROM Workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkflowAction