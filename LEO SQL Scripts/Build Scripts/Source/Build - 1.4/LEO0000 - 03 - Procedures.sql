USE LEO0000
GO

--Begin procedure budget.GetBudgetByBudgetID
EXEC Utility.DropObject 'budget.GetBudgetByBudgetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the budget.Budget table
-- ====================================================================
CREATE PROCEDURE budget.GetBudgetByBudgetID

@BudgetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		B.BudgetID,
		B.EndDate,
		core.FormatDate(B.EndDate) AS EndDateFormatted,
		B.IsActive,
		B.Notes,
		B.NumberOfQuarters,
		B.StartDate,
		core.FormatDate(B.StartDate) AS StartDateFormatted,
		P.ProjectID,
		P.ProjectName
	FROM budget.Budget B
		JOIN dropdown.Project P ON P.ProjectID = B.ProjectID
			AND B.BudgetID = @BudgetID

	SELECT
		BI.BudgetItemID,
		BI.BudgetItemName,
		BI.BudgetItemDescription,
		BI.Q1Forecast,
		BI.Q1Spend,
		BI.Q2Forecast,
		BI.Q2Spend,
		BI.Q3Forecast,
		BI.Q3Spend,
		BI.Q4Forecast,
		BI.Q4Spend,
		BI.Q5Forecast,
		BI.Q5Spend,
		BI.Q6Forecast,
		BI.Q6Spend,
		BI.Q7Forecast,
		BI.Q7Spend,
		BI.Q8Forecast,
		BI.Q8Spend,
		BI.Q9Forecast,
		BI.Q9Spend,
		BI.TotalItemAmount,
		FS.FundingSourceID,
		FS.FundingSourceName
	FROM budget.BudgetItem BI
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = BI.FundingSourceID
			AND BI.BudgetID = @BudgetID
	ORDER BY BI.BudgetItemName, BI.BudgetItemID

END
GO
--End procedure budget.GetBudgetByBudgetID

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure eventlog.LogBudgetAction
EXEC utility.DropObject 'eventlog.LogBudgetAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogBudgetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			T.BudgetID,
			@Comments,
			@ProjectID
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Budget'), ELEMENTS
			)
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogBudgetAction

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			T.*
			FOR XML RAW('Permissionable'), ELEMENTS
			)
		FROM permissionable.Permissionable T 
		WHERE T.PermissionableID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableAction