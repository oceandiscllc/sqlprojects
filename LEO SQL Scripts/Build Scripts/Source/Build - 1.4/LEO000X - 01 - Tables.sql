USE [[INSTANCENAME]]
GO

--Begin table contact.SubContractor
EXEC utility.DropObject 'contact.SubContractorSubContractorCapability'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'contact' AND T.Name = 'SubContractor')
	BEGIN

	ALTER SCHEMA dbo TRANSFER contact.SubContractor

	END
--ENDIF
GO
--End schema subcontractor

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.AddColumn @TableName, 'ProjectID', 'INT', '0'
GO
--End table eventlog.EventLog

--Begin table dropdown.SubContractorCapability
EXEC utility.DropObject 'dropdown.SubContractorCapability'
GO
--End table dropdown.SubContractorCapability

