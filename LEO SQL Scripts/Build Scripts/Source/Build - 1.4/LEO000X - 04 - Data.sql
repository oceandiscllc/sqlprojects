USE [[INSTANCENAME]]
GO

DECLARE @nProjectID INT = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = '[[INSTANCENAME]]' AND IDD.DropdownCode = 'Project' ORDER BY IDD.DropdownID)

UPDATE EL
SET EL.ProjectID = @nProjectID
FROM eventlog.EventLog EL
GO
