USE [[INSTANCENAME]]
GO

--Begin procedure activity.GetActivityByActivityID
EXEC Utility.DropObject 'activity.GetActivityByActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the activity.Activity table
-- ========================================================================
CREATE PROCEDURE activity.GetActivityByActivityID

@ActivityID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Activity', @ActivityID, @ProjectID)

	SELECT 
		A.ActivityID,
		A.ActivityName,
		A.ActivityTypeID,
		A.AwardeeSubContractorID1,
		A.AwardeeSubContractorID2,
		A.Background,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.FundingSourceID,
		A.Objectives,
		A.PointOfContactPersonID,
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		A.TaskCode,
		A.AwardeeSubContractorID1,
		(SELECT SC1.SubContractorName FROM dbo.SubContractor SC1 WHERE SC1.SubContractorID = A.AwardeeSubContractorID1) AS AwardeeSubContractorName1,
		A.AwardeeSubContractorID2,
		(SELECT SC2.SubContractorName FROM dbo.SubContractor SC2 WHERE SC2.SubContractorID = A.AwardeeSubContractorID2) AS AwardeeSubContractorName2,
		AT.ActivityTypeName,
		FS.FundingSourceName
	FROM activity.Activity A
		JOIN dropdown.ActivityType AT ON AT.ActivityTypeID = A.ActivityTypeID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = A.FundingSourceID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID

	SELECT
		AC.CourseID,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		M.ModuleName
	FROM activity.ActivityCourse AC
		JOIN course.Course C ON C.CourseID = AC.CourseID
		JOIN dbo.Module M on M.ModuleID = C.ModuleID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID
	ORDER BY M.ModuleName

	SELECT
		AC.VettingDate,
		core.FormatDate(AC.VettingDate) AS VettingDateFormatted,
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryNameFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM activity.ActivityContact AC
		JOIN contact.Contact C ON C.ContactID = AC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = AC.VettingOutcomeID
			AND AC.ActivityID = @ActivityID
			AND AC.ProjectID = @ProjectID

	SELECT
		AT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(AT.TerritoryID) AS TerritoryName
	FROM activity.ActivityTerritory AT
	WHERE AT.ActivityID = @ActivityID
		AND AT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'Activity', @ActivityID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'Activity', @ActivityID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activity.Activity A ON A.ActivityID = EL.EntityID
			AND A.ActivityID = @ActivityID
			AND A.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'Activity'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure activity.GetActivityByActivityID

--Begin procedure dbo.GetSubContractorBySubContractorID
EXEC Utility.DropObject 'dbo.GetSubContractorBySubContractorID'
EXEC Utility.DropObject 'contact.GetSubContractorBySubContractorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the dbo.SubContractor table
-- ========================================================================
CREATE PROCEDURE dbo.GetSubContractorBySubContractorID

@SubContractorID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.CountryID AS AddressCountryID,
		C1.CountryName AS AddressCountryName,
		C2.CountryID AS RegistrationCountryID,
		C2.CountryName AS RegistrationCountryName,
		SC.Address,
		SC.PrimaryContactEmailAddress,
		SC.PrimaryContactName,
		SC.PrimaryContactPhone,
		SC.RegistrationNumber,
		SC.SubContractorID,
		SC.SubContractorName,
		SC.TaxNumber,
		SC.AccountNumber,
		SC.BankBranch,
		SC.BankName,
		SC.BankRoutingNumber,
		SC.IBAN,
		SC.SWIFTCode,
		SC.ISOCurrencyCode,
		SC.IsActive,
		SC.ProjectID,
		dropdown.GetProjectNameByProjectID(SC.ProjectID) AS ProjectName,
		SCBT.SubContractorBusinessTypeID,
		SCBT.SubContractorBusinessTypeName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName
	FROM dbo.SubContractor SC
		JOIN dropdown.Country C1 ON C1.CountryID = SC.AddressCountryID
		JOIN dropdown.Country C2 ON C2.CountryID = SC.RegistrationCountryID
		JOIN dropdown.SubContractorBusinessType SCBT ON SCBT.SubContractorBusinessTypeID = SC.SubContractorBusinessTypeID
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
			AND SC.SubContractorID = @SubContractorID
			AND SC.ProjectID = @ProjectID

END
GO
--End procedure dbo.GetSubContractorBySubContractorID

--Begin procedure document.DeleteDocumentByDocumentID
EXEC Utility.DropObject 'document.DeleteDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to delete from the document.Document table
-- ==========================================================================
CREATE PROCEDURE document.DeleteDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE D
	FROM document.Document D
	WHERE D.DocumentID = @DocumentID 
		AND D.ProjectID = @ProjectID 

END
GO
--End procedure document.DeleteDocumentByDocumentID

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		'fa fa-fw fa-tasks' AS Icon,
		AR.ActivityReportID AS EntityID,
		AR.ActivityReportTitle AS Title,
		AR.UpdateDateTime,
		core.FormatDate(AR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,
		P.ProjectName
	FROM activityreport.ActivityReport AR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ActivityReport'
		JOIN dropdown.Project P ON P.ProjectID = AR.ProjectID
			AND AR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = AR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-exclamation-circle' AS Icon,
		I.ImpactStoryID AS EntityID,
		I.ImpactStoryName AS Title,
		I.UpdateDateTime,
		core.FormatDate(I.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,
		P.ProjectName
	FROM impactstory.ImpactStory I
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ImpactStory'
		JOIN dropdown.Project P ON P.ProjectID = I.ProjectID
			AND I.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = I.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,
		P.ProjectName
	FROM dbo.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,
		P.ProjectName
	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,
		P.ProjectName
	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
					AND W2.ProjectID = W1.ProjectID
					AND WS2.ProjectID = WS1.ProjectID
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND W1.ProjectID = @ProjectID
				AND WS1.ProjectID = @ProjectID
				AND WS1.WorkflowStepNumber = 1
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID)
			BEGIN
			
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.IsComplete = 0
				AND EWSGP.ProjectID = @ProjectID
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			SELECT
				'Approved' AS WorkflowStepName,
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) AS WorkflowStepCount
			
			END
		--ENDIF
			
		END
	--ENDIF
	
END
GO
--End procedure workflow.GetEntityWorkflowData