USE LEO0000
GO

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin procedure dropdown.GetActivityReportTypeData
EXEC Utility.DropObject 'dropdown.GetActivityReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityReportType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetActivityReportTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityReportTypeID, 
		T.ActivityReportTypeName
	FROM dropdown.ActivityReportType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ActivityReportTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ActivityReportType'
			AND (T.ActivityReportTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityReportTypeName, T.ActivityReportTypeID

END
GO
--End procedure dropdown.GetActivityReportTypeData

--Begin procedure dropdown.GetActivityTypeData
EXEC Utility.DropObject 'dropdown.GetActivityTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetActivityTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityTypeID, 
		T.ActivityTypeName
	FROM dropdown.ActivityType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ActivityTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ActivityType'
			AND (T.ActivityTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityTypeName, T.ActivityTypeID

END
GO
--End procedure dropdown.GetActivityTypeData

--Begin procedure dropdown.GetAreaOfOperationTypeData
EXEC Utility.DropObject 'dropdown.GetAreaOfOperationTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AreaOfOperationType table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetAreaOfOperationTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AreaOfOperationTypeID, 
		T.AreaOfOperationTypeName,
		T.HexColor
	FROM dropdown.AreaOfOperationType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AreaOfOperationTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AreaOfOperationType'
			AND (T.AreaOfOperationTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AreaOfOperationTypeName, T.AreaOfOperationTypeID

END
GO
--End procedure dropdown.GetAreaOfOperationTypeData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AssetTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AssetType'
			AND (T.AssetTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetAtmosphericTypeData
EXEC Utility.DropObject 'dropdown.GetAtmosphericTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.AtmosphericType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetAtmosphericTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AtmosphericTypeID, 
		T.AtmosphericTypeName
	FROM dropdown.AtmosphericType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AtmosphericTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AtmosphericType'
			AND (T.AtmosphericTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AtmosphericTypeName, T.AtmosphericTypeID

END
GO
--End procedure dropdown.GetAtmosphericTypeData

--Begin procedure dropdown.GetCommunicationThemeData
EXEC Utility.DropObject 'dropdown.GetCommunicationThemeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.CommunicationTheme table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCommunicationThemeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunicationThemeID, 
		T.CommunicationThemeName
	FROM dropdown.CommunicationTheme T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CommunicationThemeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CommunicationTheme'
			AND (T.CommunicationThemeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunicationThemeName, T.CommunicationThemeID

END
GO
--End procedure dropdown.GetCommunicationThemeData

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConfidenceLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConfidenceLevel'
			AND (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetContactStatusData
EXEC Utility.DropObject 'dropdown.GetContactStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetContactStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactStatusID,
		T.ContactStatusName
	FROM dropdown.ContactStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactStatus'
			AND (T.ContactStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactStatusName, T.ContactStatusID

END
GO
--End procedure dropdown.GetContactStatusData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID,
		T.ContactTypeName
	FROM dropdown.ContactType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactType'
			AND (T.ContactTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.CountryID, 
		T.CountryName,
		T.ISOCountryCode2, 
		T.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country T
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = T.CountryID 
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CountryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'CountryCallingCode'
			AND (T.CountryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin procedure dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.CurrencyID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Currency'
			AND (T.CurrencyID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetDateFilterData
EXEC Utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DateFilterID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DateFilter'
			AND (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetDistributorTypeData
EXEC Utility.DropObject 'dropdown.GetDistributorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DistributorType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetDistributorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DistributorTypeID, 
		T.DistributorTypeName,
		T.DistributorTypeCode
	FROM dropdown.DistributorType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DistributorTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DistributorType'
			AND (T.DistributorTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.DistributorTypeName, T.DistributorTypeID

END
GO
--End procedure dropdown.GetDistributorTypeData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID,
		DC.DocumentCategoryID,
		DC.DocumentCategoryName,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
		JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = T.DocumentCategoryID
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DocumentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DocumentType'
			AND (T.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY DC.DisplayOrder, DC.DocumentCategoryName, T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetEngagementStatusData
EXEC Utility.DropObject 'dropdown.GetEngagementStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.EngagementStatus table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetEngagementStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementStatusID,
		T.EngagementStatusName
	FROM dropdown.EngagementStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EngagementStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EngagementStatus'
			AND (T.EngagementStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementStatusName, T.EngagementStatusID

END
GO
--End procedure dropdown.GetEngagementStatusData

--Begin procedure dropdown.GetFindingStatusData
EXEC Utility.DropObject 'dropdown.GetFindingStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFindingStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingStatusID,
		T.FindingStatusName
	FROM dropdown.FindingStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingStatus'
			AND (T.FindingStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingStatusName, T.FindingStatusID

END
GO
--End procedure dropdown.GetFindingStatusData

--Begin procedure dropdown.GetFindingTypeData
EXEC Utility.DropObject 'dropdown.GetFindingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FindingType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetFindingTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingTypeID,
		T.FindingTypeName
	FROM dropdown.FindingType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FindingTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FindingType'
			AND (T.FindingTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingTypeName, T.FindingTypeID

END
GO
--End procedure dropdown.GetFindingTypeData

--Begin procedure dropdown.GetFundingSourceData
EXEC Utility.DropObject 'dropdown.GetFundingSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FundingSource table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFundingSourceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FundingSourceID, 
		T.FundingSourceName
	FROM dropdown.FundingSource T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.FundingSourceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'FundingSource'
			AND (T.FundingSourceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.FundingSourceName, T.FundingSourceID

END
GO
--End procedure dropdown.GetFundingSourceData

--Begin procedure dropdown.GetImpactDecisionData
EXEC Utility.DropObject 'dropdown.GetImpactDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ImpactDecision table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetImpactDecisionData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImpactDecisionID,
		T.ImpactDecisionName,
		T.HexColor
	FROM dropdown.ImpactDecision T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ImpactDecisionID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ImpactDecision'
			AND (T.ImpactDecisionID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImpactDecisionName, T.ImpactDecisionID

END
GO
--End procedure dropdown.GetImpactDecisionData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID, 
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentType'
			AND (T.IncidentTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.IncidentTypeCategory,T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure dropdown.GetInformationValidityData
EXEC Utility.DropObject 'dropdown.GetInformationValidityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.InformationValidity table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetInformationValidityData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InformationValidityID, 
		T.InformationValidityName
	FROM dropdown.InformationValidity T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.InformationValidityID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'InformationValidity'
			AND (T.InformationValidityID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.InformationValidityName, T.InformationValidityID

END
GO
--End procedure dropdown.GetInformationValidityData

--Begin procedure dropdown.GetLearnerProfileTypeData
EXEC Utility.DropObject 'dropdown.GetLearnerProfileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LearnerProfileType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetLearnerProfileTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LearnerProfileTypeID,
		T.LearnerProfileTypeName
	FROM dropdown.LearnerProfileType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LearnerProfileTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LearnerProfileType'
			AND (T.LearnerProfileTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.LearnerProfileTypeName, T.LearnerProfileTypeID

END
GO
--End procedure dropdown.GetLearnerProfileTypeData

--Begin procedure dropdown.GetLogicalFrameworkStatusData
EXEC Utility.DropObject 'dropdown.GetLogicalFrameworkStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LogicalFrameworkStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetLogicalFrameworkStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LogicalFrameworkStatusID,
		T.LogicalFrameworkStatusName
	FROM dropdown.LogicalFrameworkStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.LogicalFrameworkStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'LogicalFrameworkStatus'
			AND (T.LogicalFrameworkStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.LogicalFrameworkStatusName, T.LogicalFrameworkStatusID

END
GO
--End procedure dropdown.GetLogicalFrameworkStatusData

--Begin procedure dropdown.GetMediaReportSourceTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportSourceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetMediaReportSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportSourceTypeID,
		T.MediaReportSourceTypeName,
		T.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		T.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM dropdown.MediaReportSourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportSourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportSourceType'
			AND (T.MediaReportSourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportSourceTypeName, T.MediaReportSourceTypeID

END
GO
--End procedure dropdown.GetMediaReportSourceTypeData

--Begin procedure dropdown.GetMediaReportTypeData
EXEC Utility.DropObject 'dropdown.GetMediaReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediaReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetMediaReportTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportTypeID,
		T.MediaReportTypeName
	FROM dropdown.MediaReportType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediaReportTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediaReportType'
			AND (T.MediaReportTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportTypeName, T.MediaReportTypeID

END
GO
--End procedure dropdown.GetMediaReportTypeData

--Begin procedure dropdown.GetModuleTypeData
EXEC Utility.DropObject 'dropdown.GetModuleTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ModuleType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetModuleTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ModuleTypeID,
		T.ModuleTypeName
	FROM dropdown.ModuleType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ModuleTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ModuleType'
			AND (T.ModuleTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ModuleTypeName, T.ModuleTypeID

END
GO
--End procedure dropdown.GetModuleTypeData

--Begin procedure dropdown.GetObjectiveTypeData
EXEC Utility.DropObject 'dropdown.GetObjectiveTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ObjectiveType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetObjectiveTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ObjectiveTypeID, 
		T.ObjectiveTypeCode,
		T.ObjectiveTypeName
	FROM dropdown.ObjectiveType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ObjectiveTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ObjectiveType'
			AND (T.ObjectiveTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ObjectiveTypeName, T.ObjectiveTypeID

END
GO
--End procedure dropdown.GetObjectiveTypeData

--Begin procedure dropdown.GetProductOriginData
EXEC Utility.DropObject 'dropdown.GetProductOriginData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:		Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductOrigin table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductOriginData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductOriginID,
		T.ProductOriginName
	FROM dropdown.ProductOrigin T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductOriginID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductOrigin'
			AND (T.ProductOriginID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductOriginName, T.ProductOriginID

END
GO
--End procedure dropdown.GetProductOriginData

--Begin procedure dropdown.GetProductStatusData
EXEC Utility.DropObject 'dropdown.GetProductStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProductStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductStatusID, 
		T.ProductStatusName
	FROM dropdown.ProductStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductStatus'
			AND (T.ProductStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductStatusName, T.ProductStatusID

END
GO
--End procedure dropdown.GetProductStatusData

--Begin procedure dropdown.GetProductTypeData
EXEC Utility.DropObject 'dropdown.GetProductTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProductType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProductTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductTypeID, 
		T.ProductTypeName
	FROM dropdown.ProductType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProductTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProductType'
			AND (T.ProductTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductTypeName, T.ProductTypeID

END
GO
--End procedure dropdown.GetProductTypeData

--Begin procedure dropdown.GetProgramTypeData
EXEC Utility.DropObject 'dropdown.GetProgramTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProgramType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProgramTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProgramTypeID, 
		T.ProgramTypeName
	FROM dropdown.ProgramType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProgramTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ProgramType'
			AND (T.ProgramTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProgramTypeName, T.ProgramTypeID

END
GO
--End procedure dropdown.GetProgramTypeData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName
	FROM dropdown.Project T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ProjectID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Project'
			AND (T.ProjectID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetRequestForInformationResultTypeData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationResultTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationResultType table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationResultTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationResultTypeID, 
		T.RequestForInformationResultTypeCode,
		T.RequestForInformationResultTypeName
	FROM dropdown.RequestForInformationResultType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationResultTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationResultType'
			AND (T.RequestForInformationResultTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationResultTypeName, T.RequestForInformationResultTypeID

END
GO
--End procedure dropdown.GetRequestForInformationResultTypeData

--Begin procedure dropdown.GetRequestForInformationStatusData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationStatusID, 
		T.RequestForInformationStatusCode,
		T.RequestForInformationStatusName
	FROM dropdown.RequestForInformationStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RequestForInformationStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RequestForInformationStatus'
			AND (T.RequestForInformationStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationStatusName, T.RequestForInformationStatusID

END
GO
--End procedure dropdown.GetRequestForInformationStatusData

--Begin procedure dropdown.GetResourceProviderData
EXEC Utility.DropObject 'dropdown.GetResourceProviderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ResourceProvider table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetResourceProviderData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResourceProviderID, 
		T.ResourceProviderName
	FROM dropdown.ResourceProvider T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ResourceProviderID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ResourceProvider'
			AND (T.ResourceProviderID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.ResourceProviderName, T.ResourceProviderID

END
GO
--End procedure dropdown.GetResourceProviderData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleName
	FROM dropdown.Role T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RoleID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Role'
			AND (T.RoleID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSourceCategoryData
EXEC Utility.DropObject 'dropdown.GetSourceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetSourceCategoryData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceCategoryID,
		T.SourceCategoryName
	FROM dropdown.SourceCategory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceCategoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceCategory'
			AND (T.SourceCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceCategoryName, T.SourceCategoryID

END
GO
--End procedure dropdown.GetSourceCategoryData

--Begin procedure dropdown.GetSourceTypeData
EXEC Utility.DropObject 'dropdown.GetSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SourceType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSourceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceTypeID, 
		T.SourceTypeName
	FROM dropdown.SourceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SourceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SourceType'
			AND (T.SourceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceTypeName, T.SourceTypeID

END
GO
--End procedure dropdown.GetSourceTypeData

--Begin procedure dropdown.GetStatusChangeData
EXEC Utility.DropObject 'dropdown.GetStatusChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.StatusChange table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetStatusChangeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusChangeID,
		T.StatusChangeName
	FROM dropdown.StatusChange T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.StatusChangeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'StatusChange'
			AND (T.StatusChangeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusChangeName, T.StatusChangeID

END
GO
--End procedure dropdown.GetStatusChangeData

--Begin procedure dropdown.GetSubContractorBusinessTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorBusinessTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorBusinessType table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorBusinessTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorBusinessTypeID, 
		T.SubContractorBusinessTypeName
	FROM dropdown.SubContractorBusinessType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorBusinessTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorBusinessType'
			AND (T.SubContractorBusinessTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorBusinessTypeName, T.SubContractorBusinessTypeID

END
GO
--End procedure dropdown.GetSubContractorBusinessTypeData

--Begin procedure dropdown.GetSubContractorRelationshipTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorRelationshipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.SubContractorRelationshipType table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorRelationshipTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorRelationshipTypeID, 
		T.SubContractorRelationshipTypeName
	FROM dropdown.SubContractorRelationshipType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.SubContractorRelationshipTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'SubContractorRelationshipType'
			AND (T.SubContractorRelationshipTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorRelationshipTypeName, T.SubContractorRelationshipTypeID

END
GO
--End procedure dropdown.GetSubContractorRelationshipTypeData

--Begin procedure dropdown.GetTargetAudienceData
EXEC Utility.DropObject 'dropdown.GetTargetAudienceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.TargetAudience table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetTargetAudienceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TargetAudienceID, 
		T.TargetAudienceName
	FROM dropdown.TargetAudience T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TargetAudienceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TargetAudience'
			AND (T.TargetAudienceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.TargetAudienceName, T.TargetAudienceID

END
GO
--End procedure dropdown.GetTargetAudienceData

--Begin procedure dropdown.GetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.UnitType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetUnitTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnitTypeID, 
		T.UnitTypeName
	FROM dropdown.UnitType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.UnitTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'UnitType'
			AND (T.UnitTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.UnitTypeName, T.UnitTypeID

END
GO
--End procedure dropdown.GetUnitTypeData

--Begin procedure dropdown.GetVettingOutcomeData
EXEC Utility.DropObject 'dropdown.GetVettingOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.VettingOutcome table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetVettingOutcomeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VettingOutcomeID,
		T.VettingOutcomeName
	FROM dropdown.VettingOutcome T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.VettingOutcomeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'VettingOutcome'
			AND (T.VettingOutcomeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.VettingOutcomeName, T.VettingOutcomeID

END
GO
--End procedure dropdown.GetVettingOutcomeData

--Begin procedure implementer.ImplementerAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new implementer to the LEO system and initialize its dropdown data
-- ===========================================================================================================
CREATE PROCEDURE implementer.ImplementerAddUpdate

@ImplementerCode VARCHAR(25),
@ImplementerName VARCHAR(250),
@ImplementerURL VARCHAR(50),
@ProjectNameList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCode VARCHAR(250)
	DECLARE @cSQL VARCHAR(MAX)

	IF NOT EXISTS (SELECT 1 FROM implementer.Implementer I WHERE I.ImplementerCode = @ImplementerCode)
		INSERT INTO implementer.Implementer (ImplementerCode, ImplementerName, ImplementerURL) VALUES (@ImplementerCode, @ImplementerName, @ImplementerURL)
	--ENDIF

	INSERT INTO dropdown.Project
		(ProjectName)
	SELECT
		LTT.ListItem
	FROM core.ListToTable(@ProjectNameList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dropdown.Project P
		WHERE P.ProjectName = LTT.ListItem
		)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, DropdownID)
	SELECT
		@ImplementerCode,
		'Project',
		P.ProjectID
	FROM dropdown.Project P
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItem = P.ProjectName

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = 'Dropdown'
				AND T.Name <> 'Project'
		ORDER BY T.Name

	OPEN oCursor
	FETCH oCursor INTO @cDropdownCode
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC implementer.ImplementerDropdownDataAddUpdate @cDropdownCode

		FETCH oCursor INTO @cDropdownCode
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure implementer.ImplementerAddUpdate

--Begin procedure implementer.ImplementerDropdownDataAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerDropdownDataAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new dropdown / dropdown data to the LEO system and grant implemeters access to the new data
-- ====================================================================================================================================
CREATE PROCEDURE implementer.ImplementerDropdownDataAddUpdate

@DropdownCode VARCHAR(50),
@DropdownCodePrimaryKey VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCodePrimaryKey VARCHAR(60) = ISNULL(@DropdownCodePrimaryKey, @DropdownCode + 'ID')
	DECLARE @cSQL VARCHAR(MAX)

	SET @cSQL = 'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT I.ImplementerCode, '''
	SET @cSQL += @DropdownCode + ''', T.' + @cDropdownCodePrimaryKey + ' FROM dropdown.' + @DropdownCode
	SET @cSQL += ' T CROSS APPLY implementer.Implementer I WHERE NOT EXISTS'
	SET @cSQL += ' (SELECT 1 FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = I.ImplementerCode AND IDD.DropdownCode = '''
	SET @cSQL += @DropdownCode + ''' AND IDD.DropdownID = T.' + @cDropdownCodePrimaryKey + ')'

	EXEC (@cSQL)

END
GO
--End procedure implementer.ImplementerDropdownDataAddUpdate

--Begin procedure implementer.ImplementerSynonymAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerSynonymAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update implementer synonym records
-- ===========================================================================
CREATE PROCEDURE implementer.ImplementerSynonymAddUpdate

@BaseObjectSchema VARCHAR(50),
@BaseObjectName VARCHAR(250),
@Mode VARCHAR(50) = ''

AS
BEGIN
	SET NOCOUNT ON;

	IF @Mode = 'Delete'
		DELETE I FROM implementer.Synonym I WHERE I.BaseObjectSchema = @BaseObjectSchema AND I.BaseObjectName = @BaseObjectName
	ELSE IF NOT EXISTS (SELECT 1 FROM implementer.Synonym I WHERE I.BaseObjectSchema = @BaseObjectSchema AND I.BaseObjectName = @BaseObjectName)
		INSERT INTO implementer.Synonym (BaseObjectSchema, BaseObjectName) VALUES (@BaseObjectSchema, @BaseObjectName)
	--ENDIF

END
GO
--End procedure implementer.ImplementerSynonymAddUpdate