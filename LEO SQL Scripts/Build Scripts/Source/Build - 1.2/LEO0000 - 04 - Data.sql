USE LEO0000
GO

EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
EXEC Utility.DropObject 'dropdown.GetRiskStatusData'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'core', 'GetEntityTypeNameByEntityTypeCode'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ActivityReportType'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetActivityReportTypeData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEntityTypeNameData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEventCodeNameData', 'Delete'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetRiskStatusData', 'Delete'
GO

UPDATE DT 
SET 
	DT.DocumentTypeName = 'Monthly Activity'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'MonthlyActivity'
GO

UPDATE I SET I.ImplementerName = 'HMG', I.ImplementerURL = 'ABBY' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0001'
UPDATE I SET I.ImplementerName = 'Strategm', I.ImplementerURL = 'NIKO' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0002'
UPDATE I SET I.ImplementerName = 'TSN', I.ImplementerURL = 'ODEE' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0003'
UPDATE I SET I.ImplementerName = 'ICS', I.ImplementerURL = 'PETRA' FROM implementer.Implementer I WHERE I.ImplementerCode = 'LEO0004'
GO

UPDATE P SET P.ProjectName = 'Leo' FROM dropdown.Project P WHERE P.ProjectName = 'Project One'
UPDATE P SET P.ProjectName = 'Disrupt' FROM dropdown.Project P WHERE P.ProjectName = 'Project Two'
UPDATE P SET P.ProjectName = 'Undermine' FROM dropdown.Project P WHERE P.ProjectName = 'Project Three'
UPDATE P SET P.ProjectName = 'MAO' FROM dropdown.Project P WHERE P.ProjectName = 'Project Four'
GO

UPDATE P SET P.ProjectCode = NULL FROM dropdown.Project P
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ActivityReportType'
GO

EXEC implementer.ImplementerAddUpdate 'LEO0005', 'ARK', 'Tamba', 'Grassroots'
GO
