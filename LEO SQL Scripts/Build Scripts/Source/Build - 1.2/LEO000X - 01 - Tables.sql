USE [[INSTANCENAME]]
GO

EXEC utility.AddSchema 'activityreport'
GO

--Begin table activityreport.ActivityReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReport
	(
	ActivityReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportTypeID INT,
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX),
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportID'
GO
--End table activityreport.ActivityReport

--Begin table activityreport.ActivityReportAsset
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAsset'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAsset
	(
	ActivityReportAssetID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAsset', 'ActivityReportID,AssetID'
GO
--End table activityreport.ActivityReportAsset

--Begin table activityreport.ActivityReportAtmospheric
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAtmospheric'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAtmospheric
	(
	ActivityReportAtmosphericID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AtmosphericID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAtmosphericID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAtmospheric', 'ActivityReportID,AtmosphericID'
GO
--End table activityreport.ActivityReportAtmospheric

--Begin table activityreport.ActivityReportCampaign
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportCampaign'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportCampaign
	(
	ActivityReportCampaignID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	CampaignID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CampaignID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportCampaignID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportCampaign', 'ActivityReportID,CampaignID'
GO
--End table activityreport.ActivityReportCampaign

--Begin table activityreport.ActivityReportForce
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportForce'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportForce
	(
	ActivityReportForceID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportForce', 'ActivityReportID,ForceID'
GO
--End table activityreport.ActivityReportForce

--Begin table activityreport.ActivityReportImpactStory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportImpactStory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportImpactStory
	(
	ActivityReportImpactStoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ImpactStoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImpactStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportImpactStoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportImpactStory', 'ActivityReportID,ImpactStoryID'
GO
--End table activityreport.ActivityReportImpactStory

--Begin table activityreport.ActivityReportIncident
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportIncident
	(
	ActivityReportIncidentID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportIncident', 'ActivityReportID,IncidentID'
GO
--End table activityreport.ActivityReportIncident

--Begin table activityreport.ActivityReportProduct
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportProduct'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportProduct
	(
	ActivityReportProductID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ProductID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportProductID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportProduct', 'ActivityReportID,ProductID'
GO
--End table activityreport.ActivityReportProduct

--Begin table activityreport.ActivityReportMediaReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportMediaReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportMediaReport
	(
	ActivityReportMediaReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	MediaReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportMediaReportID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportMediaReport', 'ActivityReportID,MediaReportID'
GO
--End table activityreport.ActivityReportMediaReport

--Begin table activityreport.ActivityReportTerritory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportTerritory
	(
	ActivityReportTerritoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportTerritory', 'ActivityReportID,TerritoryID'
GO
--End table activityreport.ActivityReportTerritory
