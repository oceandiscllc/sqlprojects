USE LEO0000
GO

--Begin table dropdown.ActivityReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityReportType
	(
	ActivityReportTypeID INT NOT NULL IDENTITY(0,1),
	ActivityReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportType', 'DisplayOrder,ActivityReportTypeName'
GO

SET IDENTITY_INSERT dropdown.ActivityReportType ON
GO  

INSERT INTO dropdown.ActivityReportType 
	(ActivityReportTypeID, ActivityReportTypeName, DisplayOrder)
VALUES 
	(0, NULL, 0),
	(1, 'Monthly', 1),
	(2, 'Quarterly', 2),
	(3, 'Additional', 3)
GO

SET IDENTITY_INSERT dropdown.ActivityReportType OFF
GO  
--End table dropdown.ActivityReportType

--Begin table implementer.Implementer
ALTER TABLE implementer.Implementer ALTER COLUMN ImplementerURL VARCHAR(50)
GO
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdown
EXEC utility.DropObject 'implementer.ImplementerDropdown'
GO
--End table implementer.ImplementerDropdown

--Begin table implementer.ImplementerDropdownData
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdownData'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '1'
GO
--End table implementer.ImplementerDropdownData
