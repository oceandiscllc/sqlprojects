USE [[INSTANCENAME]]
GO

--Begin procedure aggregator.GetActivityReportAggregatorByActivityReportAggregatorID
EXEC utility.DropObject 'aggregator.GetActivityReportAggregatorByActivityReportAggregatorID'
GO
EXEC utility.DropObject 'aggregator.GetActivityReportByActivityReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.ActivityReportAggregator table
-- ==============================================================================================
CREATE PROCEDURE aggregator.GetActivityReportAggregatorByActivityReportAggregatorID

@ActivityReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReportAggregator', 0, @ActivityReportAggregatorID)
	
	SELECT
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.ActivityReportAggregatorID,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportTitle,
		AR.ReportDetail,
		AR.Summary
	FROM aggregator.ActivityReportAggregator AR
	WHERE AR.ActivityReportAggregatorID = @ActivityReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'ActivityReportAggregator', @ActivityReportAggregatorID, 0

	EXEC workflow.GetEntityWorkflowPeople 'ActivityReportAggregator', @ActivityReportAggregatorID, 0, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Activity Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Activity Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Activity Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Activity Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.ActivityReportAggregator AR ON AR.ActivityReportAggregatorID = EL.EntityID
			AND AR.ActivityReportAggregatorID = @ActivityReportAggregatorID
			AND EL.EntityTypeCode = 'ActivityReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetActivityReportAggregatorByActivityReportAggregatorID

--Begin procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
EXEC utility.DropObject 'aggregator.GetTrendReportAggregatorByTrendReportAggregatorID'
GO
EXEC utility.DropObject 'aggregator.GetTrendReportByTrendReportID'
GO
EXEC utility.DropObject 'aggregator.GetTrendReportByTrendReportAggregatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.TrendReportAggregator table
-- ===========================================================================================
CREATE PROCEDURE aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

@TrendReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReportAggregator', 0, @TrendReportAggregatorID)
	
	SELECT
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.TrendReportAggregatorID,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportTitle,
		TR.ReportDetail,
		TR.Summary
	FROM aggregator.TrendReportAggregator TR
	WHERE TR.TrendReportAggregatorID = @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'TrendReportAggregator', @TrendReportAggregatorID, 0

	EXEC workflow.GetEntityWorkflowPeople 'TrendReportAggregator', @TrendReportAggregatorID, 0, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Situational Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Situational Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Situational Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Situational Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.TrendReportAggregator TR ON TR.TrendReportAggregatorID = EL.EntityID
			AND TR.TrendReportAggregatorID = @TrendReportAggregatorID
			AND EL.EntityTypeCode = 'TrendReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetImplementerSetupValuesBySetupKeyList
EXEC utility.DropObject 'core.GetImplementerSetupValuesBySetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return SetupValues from a list of SetupKeys from the core.ImplementerSetup table
-- ============================================================================================================

CREATE PROCEDURE core.GetImplementerSetupValuesBySetupKeyList

@SetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SetupKeyList = ''
		BEGIN

		SELECT 
			I.SetupKey,
			I.SetupValue 
		FROM core.ImplementerSetup I
		ORDER BY I.SetupKey

		END
	ELSE
		BEGIN

		SELECT 
			I.SetupKey,
			I.SetupValue 
		FROM core.ImplementerSetup I
			JOIN core.ListToTable(@SetupKeyList, ',') LTT ON LTT.ListItem = I.SetupKey
		ORDER BY I.SetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetImplementerSetupValuesBySetupKeyList

--Begin procedure eventlog.LogActivityReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogActivityReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogActivityReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReportAggregator',
			T.ActivityReportAggregatorID,
			@Comments,
			@ProjectID
		FROM aggregator.ActivityReportAggregator T
		WHERE T.ActivityReportAggregatorID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ActivityReportAggregator',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('ActivityReportAggregator'), ELEMENTS
			)
		FROM aggregator.ActivityReportAggregator T
		WHERE T.ActivityReportAggregatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogActivityReportAggregatorAction

--Begin procedure eventlog.LogBudgetAction
EXEC utility.DropObject 'eventlog.LogBudgetAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogBudgetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			T.BudgetID,
			@Comments,
			@ProjectID
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Budget',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Budget'), ELEMENTS
			)
		FROM budget.Budget T
		WHERE T.BudgetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogBudgetAction

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT 
			T.*
			FOR XML RAW('Permissionable'), ELEMENTS
			)
		FROM permissionable.Permissionable T 
		WHERE T.PermissionableID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableAction

--Begin procedure eventlog.LogTrendReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogTrendReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, ProjectID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'TrendReportAggregator',
			T.TrendReportAggregatorID,
			@Comments,
			@ProjectID
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'TrendReportAggregator',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('TrendReportAggregator'), ELEMENTS
			)
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAggregatorAction

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		FU.CommanderContactID, 
		contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderContactNameFormatted,
		FU.DeputyCommanderContactID,
		contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderContactNameFormatted,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure utility.LogSQLBuild
EXEC Utility.DropObject 'utility.LogSQLBuild'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to log completion of a SQL build
-- ================================================================
CREATE PROCEDURE utility.LogSQLBuild

@BuildKey VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO syslog.BuildLog (BuildKey) VALUES (@BuildKey)

END
GO
--End procedure utility.LogSQLBuild
