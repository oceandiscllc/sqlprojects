USE LEO0000
GO

--Begin procedure budget.GetBudgetByBudgetID
EXEC Utility.DropObject 'budget.GetBudgetByBudgetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the budget.Budget table
-- ====================================================================
CREATE PROCEDURE budget.GetBudgetByBudgetID

@BudgetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		B.BudgetID,
		B.EndDate,
		core.FormatDate(B.EndDate) AS EndDateFormatted,
		B.IsActive,
		B.Notes,
		B.NumberOfQuarters,
		B.StartDate,
		core.FormatDate(B.StartDate) AS StartDateFormatted,
		P.ProjectID,
		P.ProjectName
	FROM budget.Budget B
		JOIN dropdown.Project P ON P.ProjectID = B.ProjectID
			AND B.BudgetID = @BudgetID

	SELECT
		BI.BudgetItemID,
		BI.BudgetItemName,
		BI.BudgetItemDescription,
		BI.Q1Forecast,
		BI.Q1Spend,
		BI.Q2Forecast,
		BI.Q2Spend,
		BI.Q3Forecast,
		BI.Q3Spend,
		BI.Q4Forecast,
		BI.Q4Spend,
		BI.Q5Forecast,
		BI.Q5Spend,
		BI.Q6Forecast,
		BI.Q6Spend,
		BI.Q7Forecast,
		BI.Q7Spend,
		BI.Q8Forecast,
		BI.Q8Spend,
		BI.Q9Forecast,
		BI.Q9Spend,
		BI.TotalItemAmount,
		FS.FundingSourceID,
		FS.FundingSourceName
	FROM budget.BudgetItem BI
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = BI.FundingSourceID
			AND BI.BudgetID = @BudgetID
	ORDER BY BI.BudgetItemName, BI.BudgetItemID

END
GO
--End procedure budget.GetBudgetByBudgetID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@HasWorkflow BIT = 0,
@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@PrimaryKeyFieldName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.HasWorkflow = @HasWorkflow,
		ET.SchemaName = @SchemaName,
		ET.TableName = @TableName,
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName

	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,HasWorkflow,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode, 
		@EntityTypeName, 
		@EntityTypeNamePlural,
		@HasWorkflow,
		@SchemaName,
		@TableName,
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure dropdown.GetFacebookPageData
EXEC Utility.DropObject 'dropdown.GetFacebookPageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.FacebookPage table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetFacebookPageData

@ImplementerCode VARCHAR(50) = '',
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ID,
		T.Name
	FROM integration.FacebookPage T
	WHERE (T.ID > 0 OR @IncludeZero = 1)
	ORDER BY T.Name, T.ID

END
GO
--End procedure dropdown.GetFacebookPageData

--Begin procedure eventlog.LogBudgetAction
EXEC utility.DropObject 'eventlog.LogBudgetAction'
GO
--End procedure eventlog.LogBudgetAction

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO
--End procedure eventlog.LogPermissionableAction

--Begin procedure implementer.ImplementerDropdownDataAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerDropdownDataAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new dropdown / dropdown data to the LEO system and grant implemeters access to the new data
-- ====================================================================================================================================
CREATE PROCEDURE implementer.ImplementerDropdownDataAddUpdate

@DropdownCode VARCHAR(50),
@DropdownCodePrimaryKey VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCodePrimaryKey VARCHAR(60) = ISNULL(@DropdownCodePrimaryKey, @DropdownCode + 'ID')
	DECLARE @cSQL VARCHAR(MAX)

	SET @cSQL = 'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT I.ImplementerCode, '''
	SET @cSQL += @DropdownCode + ''', T.' + @cDropdownCodePrimaryKey + ' FROM dropdown.' + @DropdownCode
	SET @cSQL += ' T CROSS APPLY implementer.Implementer I WHERE NOT EXISTS'
	SET @cSQL += ' (SELECT 1 FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = I.ImplementerCode AND IDD.DropdownCode = '''
	SET @cSQL += @DropdownCode + ''' AND IDD.DropdownID = T.' + @cDropdownCodePrimaryKey + ')'

	EXEC (@cSQL)

	SET @cSQL = 'UPDATE IDD SET IDD.IsActive = T.IsActive FROM implementer.ImplementerDropdownData IDD'
	SET @cSQL += 'JOIN dropdown.' + @DropdownCode + ' T ON T.' + @DropdownCodePrimaryKey + '= IDD.DropdownID'

	EXEC (@cSQL)

END
GO
--End procedure implementer.ImplementerDropdownDataAddUpdate

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE permissionable.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionable

--Begin procedure utility.LogSQLBuild
EXEC Utility.DropObject 'utility.LogSQLBuild'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to log completion of a SQL build
-- ================================================================
CREATE PROCEDURE utility.LogSQLBuild

@BuildKey VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO syslog.BuildLog (BuildKey) VALUES (@BuildKey)

END
GO
--End procedure utility.LogSQLBuild

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables