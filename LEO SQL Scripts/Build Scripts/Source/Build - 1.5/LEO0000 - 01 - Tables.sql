USE LEO0000
GO

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.AddColumn @TableName, 'SchemaName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TableName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PrimaryKeyFieldName', 'VARCHAR(50)'
GO

DELETE ET
FROM core.EntityType ET
WHERE ET.EntityTypeCode = 'SubContractorCapability'
GO

UPDATE ET
SET 
	ET.SchemaName = S.Name,
	ET.TableName = T.Name,
	ET.PrimaryKeyFieldName = T.Name + 'ID'
FROM core.EntityType ET
	JOIN LEO0002.sys.Tables T ON T.Name = ET.EntityTypeCode
	JOIN LEO0002.sys.Schemas S ON S.Schema_ID = T.Schema_ID
GO
--End table core.EntityType
