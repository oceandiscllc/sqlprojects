USE [[INSTANCENAME]]
GO

EXEC utility.AddSchema 'aggregator'
GO

EXEC utility.DropObject 'aggregator.ActivityReport'
EXEC utility.DropObject 'aggregator.TrendReport'
GO

EXEC utility.DropColumn 'activity.Activity', 'UpdateDateTime'
EXEC utility.DropColumn 'asset.Asset', 'UpdateDateTime'
EXEC utility.DropColumn 'atmospheric.Atmospheric', 'UpdateDateTime'
EXEC utility.DropColumn 'contact.Contact', 'UpdateDateTime'
EXEC utility.DropColumn 'course.Course', 'UpdateDateTime'
EXEC utility.DropColumn 'course.CourseContact', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Campaign', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Incident', 'UpdateDateTime'
EXEC utility.DropColumn 'dbo.Module', 'UpdateDateTime'
EXEC utility.DropColumn 'distributor.Distributor', 'UpdateDateTime'
EXEC utility.DropColumn 'distributor.DistributorTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'finding.Finding', 'UpdateDateTime'
EXEC utility.DropColumn 'force.Force', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryCampaign', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryProduct', 'UpdateDateTime'
EXEC utility.DropColumn 'impactstory.ImpactStoryTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Indicator', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Milestone', 'UpdateDateTime'
EXEC utility.DropColumn 'logicalframework.Objective', 'UpdateDateTime'
EXEC utility.DropColumn 'mediareport.MediaReport', 'UpdateDateTime'
EXEC utility.DropColumn 'procurement.EquipmentCatalog', 'UpdateDateTime'
EXEC utility.DropColumn 'procurement.EquipmentInventory', 'UpdateDateTime'
EXEC utility.DropColumn 'product.Product', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductCampaign', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductCommunicationTheme', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductDistribution', 'UpdateDateTime'
EXEC utility.DropColumn 'product.ProductTerritory', 'UpdateDateTime'
EXEC utility.DropColumn 'recommendation.Recommendation', 'UpdateDateTime'
GO

EXEC utility.SetDefaultConstraint 'activityreport.ActivityReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'dbo.RequestForInformation', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'impactstory.ImpactStory', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'spotreport.SpotReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
EXEC utility.SetDefaultConstraint 'trendreport.TrendReport', 'UpdateDateTime', 'DATETIME', 'getDate()', 1
GO

--Begin table aggregator.ActivityReport
DECLARE @TableName VARCHAR(250) = 'aggregator.ActivityReportAggregator'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'aggregator.ActivityReport'

CREATE TABLE aggregator.ActivityReportAggregator
	(
	ActivityReportAggregatorID INT NOT NULL IDENTITY(1,1),
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportAggregatorID'
GO
--End table aggregator.ActivityReportAggregator

--Begin table aggregator.TrendReportAggregator
DECLARE @TableName VARCHAR(250) = 'aggregator.TrendReportAggregator'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'aggregator.TrendReport'

CREATE TABLE aggregator.TrendReportAggregator
	(
	TrendReportAggregatorID INT NOT NULL IDENTITY(1,1),
	TrendReportTitle VARCHAR(250),
	TrendReportStartDate DATE,
	TrendReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'TrendReportAggregatorID'
GO
--End table aggregator.TrendReportAggregator

--Begin table force.Force
EXEC utility.AddColumn 'force.Force', 'CommanderFullName', 'NVARCHAR(250)'
GO
--End table force.Force

--Begin view workflow.WorkflowView
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowView'

EXEC utility.DropObject @TableName
GO

CREATE VIEW workflow.WorkflowView AS
SELECT 
	W.WorkflowID,
	W.WorkflowName,
	W.EntityTypeCode,
	W.IsActive,
  W.ProjectID,
	WS.WorkflowStepNumber,
	WS.WorkflowStepName,
	WSG.WorkflowStepGroupName,
	WSGP.PersonID
FROM workflow.Workflow W
	JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		AND WS.ProjectID = W.ProjectID
	JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		AND WSG.ProjectID = WS.ProjectID
	JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		AND WSGP.ProjectID = WSG.ProjectID

GO
--End view workflow.WorkflowView
