USE LEO0000
GO

EXEC implementer.ImplementerSynonymAddUpdate 'document', 'Document'
GO

--Begin table core.EntityType
UPDATE ET
SET ET.EntityTypeCode = 'ImplementerSetup'
FROM core.EntityType ET
WHERE ET.EntityTypeCode = 'InplementerSetup'
GO

EXEC core.EntityTypeAddUpdate 'ActivityReportAggregator', 'Activity Report Aggregator', 'Activity Report Aggregator', 1, 'aggregator', 'ActivityReportAggregator', 'ActivityReportAggregatorID'
GO

EXEC core.EntityTypeAddUpdate 'TrendReportAggregator', 'Situational Report Aggregator', 'Situational Report Aggregator', 1, 'aggregator', 'TrendReportAggregator', 'TrendReportAggregatorID'
GO
--End table core.EntityType

--Begin table core.MenuItem
UPDATE MI
SET MI.MenuItemText = 'Reference Library'
FROM core.MenuItem MI
WHERE MI.MenuItemCode = 'DocumentList'
GO

UPDATE MI
SET MI.MenuItemText = 'Groups & Assets'
FROM core.MenuItem MI
WHERE MI.MenuItemCode = 'ForceAsset'
GO

UPDATE MI
SET MI.MenuItemText = 'Groups'
FROM core.MenuItem MI
WHERE MI.MenuItemCode = 'ForceList'
GO

EXEC core.MenuItemAddUpdate --Begin Aggregator Submenu
	@ParentMenuItemCode='Insight', 		
	@NewMenuItemCode='Aggregators', 											
	@AfterMenuItemCode='TrendReport',												
	@NewMenuItemText='Report Aggregators', 
	@NewMenuItemLink=NULL
GO

EXEC core.MenuItemAddUpdate --Begin ActivityReport Aggregator
	@ParentMenuItemCode='Aggregators', 
	@NewMenuItemCode='ActivityReportAggregatorList', 											
	@AfterMenuItemCode=NULL,													
	@NewMenuItemText='Activity Report', 
	@NewMenuItemLink='/activityreportaggregator/list',
	@PermissionableLineageList='ActivityReportAggregator.List'
GO

EXEC core.MenuItemAddUpdate --Begin TrendReport Aggregator
	@ParentMenuItemCode='Aggregators', 
	@NewMenuItemCode='TrendReportAggregatorList', 											
	@AfterMenuItemCode='ActivityReportAggregatorList',													
	@NewMenuItemText='Trend Report', 
	@NewMenuItemLink='/trendreportaggregator/list',
	@PermissionableLineageList='TrendReportAggregator.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Aggregators'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

--Begin table dropdown.IncidentType
DECLARE @tTable1 TABLE (IncidentTypeCategory VARCHAR(50), IncidentTypeName VARCHAR(50), Icon VARCHAR(50))

INSERT INTO @tTable1
	(IncidentTypeCategory, IncidentTypeName, Icon)
VALUES
	('Civil-Activity', 'Black Market Activity', 'civilian.png'),
	('Civil-Activity', 'General Calm', 'civilian.png'),
	('Communications', 'Ongoing Negotiations', 'communication.png'),
	('Criminality', 'Confiscation', 'crime.png'),
	('Criminality', 'Smuggling', 'crime.png'),
	('Military Activity - Air', 'Airstrike - Israeli', 'military-air.png'),
	('Military Activity - Air', 'Airstrike - Turkish', 'military-air.png'),
	('Military Activity - Ground', 'Besiegement', 'military-ground.png'),
	('Military Activity - Ground', 'Ceasefire/Truce violation', 'military-ground.png'),
	('Military Activity - Ground', 'Ceasefire/Truce', 'military-ground.png'),
	('Military Activity - Ground', 'Grenade', 'military-ground.png'),
	('Security Sector Activity', 'Arrest', 'police.png'),
	('Security Sector Activity', 'Municipal incapacity/incapability', 'police.png')

MERGE dropdown.IncidentType IT
	USING (SELECT T1.IncidentTypeCategory, T1.IncidentTypeName, T1.Icon FROM @tTable1 T1) T2
		ON T2.IncidentTypeCategory = IT.IncidentTypeCategory
			AND T2.IncidentTypeName = IT.IncidentTypeName
	WHEN MATCHED THEN UPDATE 
	SET 
		IT.Icon = T2.Icon
	WHEN NOT MATCHED THEN
	INSERT (IncidentTypeCategory, IncidentTypeName, Icon)
	VALUES
		(
		T2.IncidentTypeCategory, 
		T2.IncidentTypeName, 
		T2.Icon
		);
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType'
GO
--End table dropdown.IncidentType

--Begin table dropdown.RequestForInformationStatus
UPDATE RFIS
SET
	RFIS.RequestForInformationStatusCode = 'Answered',
	RFIS.RequestForInformationStatusName = 'Answered'
FROM dropdown.RequestForInformationStatus RFIS
WHERE RFIS.RequestForInformationStatusCode = 'Completed'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.RequestForInformationStatus RFIS WHERE RFIS.RequestForInformationStatusCode = 'Rejected')
	BEGIN

	INSERT INTO dropdown.RequestForInformationStatus
		(RequestForInformationStatusCode,RequestForInformationStatusName)
	VALUES
		('Forwarded', 'Forwarded For Response'),
		('Rejected', 'Rejected')

	END
--ENDIF
GO

UPDATE RFIS
SET RFIS.DisplayOrder = 0
FROM dropdown.RequestForInformationStatus RFIS
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'RequestForInformationStatus'
GO
--End table dropdown.RequestForInformationStatus

--Begin table dropdown.RequestForInformationResultType
UPDATE RFIS
SET
	RFIS.RequestForInformationResultTypeCode = 'Answered',
	RFIS.RequestForInformationResultTypeName = 'Answered'
FROM dropdown.RequestForInformationResultType RFIS
WHERE RFIS.RequestForInformationResultTypeCode = 'RFIResponse'
GO

UPDATE RFIS
SET
	RFIS.RequestForInformationResultTypeCode = 'Rejected',
	RFIS.RequestForInformationResultTypeName = 'Rejected'
FROM dropdown.RequestForInformationResultType RFIS
WHERE RFIS.RequestForInformationResultTypeCode = 'SpotReport'
GO

UPDATE RFIS
SET RFIS.IsActive = 0
FROM dropdown.RequestForInformationResultType RFIS
WHERE RFIS.RequestForInformationResultTypeCode NOT IN ('Answered', 'Rejected')
	AND RFIS.RequestForInformationResultTypeID > 0
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'RequestForInformationResultType'
GO
--End table dropdown.RequestForInformationResultType

--Begin table permissionable.PermissionableGroup
EXEC permissionable.SavePermissionableGroup 'Aggregators', 'Reeport Aggregators', 0;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ActivityReportAggregator', 
	@DESCRIPTION='View the list of aggregated activity reports', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Aggregators', 
	@PERMISSIONABLELINEAGE='ActivityReportAggregator.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ActivityReportAggregator', 
	@DESCRIPTION='Add / edit an activity report aggregation', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Aggregators', 
	@PERMISSIONABLELINEAGE='ActivityReportAggregator.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='TrendReportAggregator', 
	@DESCRIPTION='View the list of aggregated situational reports', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Aggregators', 
	@PERMISSIONABLELINEAGE='TrendReportAggregator.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='TrendReportAggregator', 
	@DESCRIPTION='Add / edit a situational report aggregation', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Aggregators', 
	@PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable
