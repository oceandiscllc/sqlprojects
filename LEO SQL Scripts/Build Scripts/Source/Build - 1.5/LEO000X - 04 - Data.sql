USE [[INSTANCENAME]]
GO

EXEC core.ImplementerSetupAddUpdate 'IsProjectRequired', 1

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable
