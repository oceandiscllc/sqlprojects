USE LEO0001
GO

EXEC core.ImplementerSetupAddUpdate 'IsProjectRequired', 0

EXEC utility.SetDefaultConstraint 'contact.Contact', 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'contact.ContactContactType', 'ContactContactTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'contact.ContactVetting', 'ContactVettingID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'dbo.Campaign', 'CampaignID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'dbo.Incident', 'IncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'dbo.Module', 'ModuleID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'dbo.RequestForInformation', 'RequestForInformationID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'dbo.SubContractor', 'SubContractorID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'finding.Finding', 'FindingID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'finding.FindingIndicator', 'FindingIndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'finding.FindingRecommendation', 'FindingRecommendationID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'finding.FindingTerritory', 'FindingTerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'force.ForceEquipmentResourceProvider', 'ForceEquipmentResourceProviderID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'force.ForceFinancialResourceProvider', 'ForceFinancialResourceProviderID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'force.ForceUnit', 'ForceUnitID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'logicalframework.Indicator', 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'logicalframework.Milestone', 'MilestoneID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'logicalframework.Objective', 'ObjectiveID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'mediareport.MediaReport', 'MediaReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'procurement.EquipmentInventory', 'EquipmentInventoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'recommendation.Recommendation', 'RecommendationID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'recommendation.RecommendationIndicator', 'RecommendationIndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'recommendation.RecommendationTerritory', 'RecommendationTerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'spotreport.SpotReport', 'SpotReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'spotreport.SpotReportIncident', 'SpotReportIncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'spotreport.SpotReportTerritory', 'SpotReportTerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReport', 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReportAsset', 'TrendReportAssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReportForce', 'TrendReportForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReportIncident', 'TrendReportIncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReportMediaReport', 'TrendReportMediaReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'trendreport.TrendReportTerritory', 'TrendReportTerritoryID', 'INT', '0'

INSERT INTO core.EmailTemplate
	(EntityTypeCode, EmailTemplateCode, WorkflowActionCode, EmailSubject, EmailText)
SELECT
	ET.EntityTypeCode + 'Aggregator',
	ET.EmailTemplateCode, 
	ET.WorkflowActionCode, 
	ET.EmailSubject, 
	ET.EmailText
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode IN ('ActivityReport', 'TrendReport')
	AND NOT EXISTS
		(
		SELECT 1
		FROM core.EmailTemplate ET
		WHERE ET.EntityTypeCode LIKE '%Aggregator'
		)
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
SELECT
	ETF.EntityTypeCode + 'Aggregator',
	ETF.PlaceHolderText, 
	ETF.PlaceHolderDescription, 
	ETF.DisplayOrder
FROM core.EmailTemplateField ETF
WHERE ETF.EntityTypeCode IN ('ActivityReport', 'TrendReport')
	AND NOT EXISTS
		(
		SELECT 1
		FROM core.EmailTemplateField ETF
		WHERE ETF.EntityTypeCode LIKE '%Aggregator'
		)
GO