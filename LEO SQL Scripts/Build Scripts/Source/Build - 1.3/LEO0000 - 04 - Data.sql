USE LEO0000
GO

EXEC implementer.ImplementerSynonymAddUpdate 'budget', 'Budget'
EXEC implementer.ImplementerSynonymAddUpdate 'budget', 'BudgetItem'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetCountryCallingDataByCountryCallingCodeID'
EXEC implementer.ImplementerSynonymAddUpdate 'eventlog', 'LogBudgetAction'
GO

UPDATE FS SET FS.FundingSourceName = 'DFID' FROM dropdown.FundingSource FS WHERE FS.FundingSourceID = 1
UPDATE FS SET FS.FundingSourceName = 'FCO' FROM dropdown.FundingSource FS WHERE FS.FundingSourceID = 2
UPDATE FS SET FS.FundingSourceName = 'MOD' FROM dropdown.FundingSource FS WHERE FS.FundingSourceID = 3
UPDATE FS SET FS.FundingSourceName = 'Other HMG' FROM dropdown.FundingSource FS WHERE FS.FundingSourceID = 4
UPDATE FS SET FS.FundingSourceName = 'Other' FROM dropdown.FundingSource FS WHERE FS.FundingSourceID = 5
GO

UPDATE FS SET FS.DisplayOrder = FS.FundingSourceID FROM dropdown.FundingSource FS
GO
