USE [[INSTANCENAME]]
GO

--Begin table portalupdate.PortalUpdateLog
EXEC utility.DropObject 'portalupdate.PortalUpdateLog'
GO
--End table portalupdate.PortalUpdateLog

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Campaign')
	BEGIN

	ALTER SCHEMA dbo TRANSFER productdistributor.Campaign

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Distributor')
	BEGIN

	ALTER SCHEMA distributor TRANSFER productdistributor.Distributor
	ALTER SCHEMA distributor TRANSFER productdistributor.DistributorTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'ImpactStory')
	BEGIN

	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStory
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryCampaign
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryProduct
	ALTER SCHEMA impactstory TRANSFER productdistributor.ImpactStoryTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'productdistributor' AND T.Name = 'Product')
	BEGIN

	ALTER SCHEMA product TRANSFER productdistributor.Product
	ALTER SCHEMA product TRANSFER productdistributor.ProductCampaign
	ALTER SCHEMA product TRANSFER productdistributor.ProductCommunicationTheme
	ALTER SCHEMA product TRANSFER productdistributor.ProductDistribution
	ALTER SCHEMA product TRANSFER productdistributor.ProductTerritory

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'training' AND T.Name = 'Course')
	BEGIN

	ALTER SCHEMA course TRANSFER training.Course
	ALTER SCHEMA course TRANSFER training.CourseContact

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'training' AND T.Name = 'Module')
	BEGIN

	ALTER SCHEMA dbo TRANSFER training.Module

	END
--ENDIF
GO

EXEC utility.DropIndex 'asset.Asset', 'IX_Asset';
EXEC utility.DropPKConstraint 'asset.Asset';
EXEC utility.SetPrimaryKeyClustered 'asset.Asset', 'AssetID, ProjectID';
GO

EXEC utility.DropIndex 'atmospheric.AtmosphericSource', 'IX_AtmosphericSource';
EXEC utility.SetIndexClustered 'atmospheric.AtmosphericSource', 'IX_AtmosphericSource', 'AtmosphericID';
GO

EXEC utility.DropIndex 'force.Force', 'IX_Force';
EXEC utility.SetIndexClustered 'force.Force', 'IX_Force', 'ForceName';
GO

EXEC utility.DropIndex 'dbo.Campaign', 'IX_Campaign';
EXEC utility.DropPKConstraint 'dbo.Campaign';
EXEC utility.SetPrimaryKeyNonClustered 'dbo.Campaign', 'CampaignID, ProjectID';
EXEC utility.SetIndexClustered 'dbo.Campaign', 'IX_Campaign', 'CampaignName';
GO

EXEC utility.DropIndex 'distributor.Distributor', 'IX_Distributor';
EXEC utility.SetIndexClustered 'distributor.Distributor', 'IX_Distributor', 'DistributorName';
GO

EXEC utility.DropIndex 'impactstory.ImpactStory', 'IX_ImpactStory';
EXEC utility.SetIndexClustered 'impactstory.ImpactStory', 'IX_ImpactStory', 'ImpactStoryName';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryCampaign';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryCampaign', 'ImpactStoryCampaignID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryCampaign', 'IX_ImpactStoryCampaign';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryCampaign', 'IX_ImpactStoryCampaign', 'ImpactStoryID,CampaignID';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryProduct';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryProduct', 'ImpactStoryProductID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryProduct', 'IX_ImpactStoryProduct';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryProduct', 'IX_ImpactStoryProduct', 'ImpactStoryID,ProductID';
GO

EXEC utility.DropPKConstraint 'impactstory.ImpactStoryTerritory';
EXEC utility.SetPrimaryKeyNonClustered 'impactstory.ImpactStoryTerritory', 'ImpactStoryTerritoryID, ProjectID';
EXEC utility.DropIndex 'impactstory.ImpactStoryTerritory', 'IX_ImpactStoryTerritory';
EXEC utility.SetIndexClustered 'impactstory.ImpactStoryTerritory', 'IX_ImpactStoryTerritory', 'ImpactStoryID,TerritoryID';
GO

EXEC utility.DropIndex 'mediareport.MediaReportSource', 'IX_MediaReportSource';
EXEC utility.SetIndexClustered 'mediareport.MediaReportSource', 'IX_MediaReportSource', 'MediaReportID';
GO

EXEC utility.DropIndex 'procurement.EquipmentCatalog', 'IX_EquipmentCatalog';
EXEC utility.SetIndexClustered 'procurement.EquipmentCatalog', 'IX_EquipmentCatalog', 'ItemName';
GO

EXEC utility.AddColumn 'activity.ActivityContact', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activity.ActivityCourse', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activity.ActivityTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportAsset', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportAtmospheric', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportForce', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportImpactStory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportMediaReport', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportProduct', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'activityreport.ActivityReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'asset.AssetEquipmentResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'asset.AssetFinancialResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'atmospheric.AtmosphericSource', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'atmospheric.AtmosphericTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.ContactContactType', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.ContactVetting', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'contact.SubContractorSubContractorCapability', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'course.Course', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'course.CourseContact', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Campaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Incident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.Module', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'dbo.RequestForInformation', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'distributor.Distributor', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'distributor.DistributorTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingIndicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingRecommendation', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'finding.FindingTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceEquipmentResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceFinancialResourceProvider', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'force.ForceUnit', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryProduct', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'impactstory.ImpactStoryTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Indicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Milestone', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'logicalframework.Objective', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'mediareport.MediaReportSource', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentCatalog', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentInventory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.Product', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductCampaign', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductCommunicationTheme', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductDistribution', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'recommendation.RecommendationIndicator', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'recommendation.RecommendationTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'spotreport.SpotReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'spotreport.SpotReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportAsset', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportForce', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportIncident', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportMediaReport', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'trendreport.TrendReportTerritory', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStep', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStepGroup', 'ProjectID', 'INT', '0'
EXEC utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'ProjectID', 'INT', '0'
GO
