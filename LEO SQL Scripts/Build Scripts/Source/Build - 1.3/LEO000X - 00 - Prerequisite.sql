USE [[INSTANCENAME]]
GO

EXEC utility.AddSchema 'budget'
EXEC utility.AddSchema 'course'
EXEC utility.AddSchema 'distributor'
EXEC utility.AddSchema 'impactstory'
EXEC utility.AddSchema 'portalupdate'
EXEC utility.AddSchema 'product'
GO

EXEC utility.DropSchema 'api'
EXEC utility.DropSchema 'facebook'
EXEC utility.DropSchema 'programreport'
EXEC utility.DropSchema 'survey'
EXEC utility.DropSchema 'temp'
EXEC utility.DropSchema 'territoryupdate'
GO

--Begin procedure utility.DropPKConstraint
EXEC utility.DropObject 'utility.DropPKConstraint'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.01.28
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropPKConstraint

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX) = NULL

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + KC.Name + ''
	FROM sys.key_constraints KC
	WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	IF @cSQL IS NOT NULL
		EXEC (@cSQL);
	--ENDIF

END
GO
--End procedure utility.DropPKConstraint
