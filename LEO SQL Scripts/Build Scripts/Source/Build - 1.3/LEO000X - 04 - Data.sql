USE [[INSTANCENAME]]
GO

EXEC utility.DropSchema 'productdistributor'
EXEC utility.DropSchema 'training'
GO

DELETE S
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'ConceptNoteBackgroundText'
GO

UPDATE S
SET S.SetupValue = 'jcole@skotkonung.com,rabaat@skotkonung.com'
FROM core.ImplementerSetup S
WHERE S.SetupKey IN ('FeedBackMailTo', 'RFIMailTo', 'VettingMailTo')
GO

DECLARE @cURLPrefix VARCHAR(50)

SELECT @cURLPrefix = LOWER(I.ImplementerURL)
FROM LEO0000.implementer.Implementer I
WHERE I.ImplementerCode = '[[INSTANCENAME]]'

UPDATE S
SET S.SetupValue = 'NoReply@' + @cURLPrefix + CASE WHEN (SELECT LEO0000.core.GetSystemSetupValueBySetupKey('Environment', '')) = 'Prod' THEN '.projectkms.com' ELSE '.oceandisc.com' END
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'NoReply'

UPDATE S
SET S.SetupValue = 'https://' + @cURLPrefix + CASE WHEN (SELECT LEO0000.core.GetSystemSetupValueBySetupKey('Environment', '')) = 'Prod' THEN '.projectkms.com' ELSE '.oceandisc.com' END
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'SiteURL'
GO

UPDATE S
SET S.SetupValue = DB_NAME()
FROM core.ImplementerSetup S
WHERE S.SetupKey = 'ImplementerCode'
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'Activity')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('Activity', 'DecrementWorkflow', '<p>A previously submitted activity has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification because you have been assigned as a member of the Activity workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the InCoStrat Knowledge Managment System.<br /><br />Thank you,</p>', 'An Activity Has Been Disapproved'),
		('Activity', 'IncrementWorkflow', '<p>An Activity has been submitted for your review:</p><p><strong>Activity: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the ICS system because you have been assigned as a member of the Activity workflow. Please log in and click the link above to review this activity.</p><p>Please do not reply to this email as it is generated automatically by the ICS system.</p><p>Thank you,</p>', 'An Activity Is Ready For Your Review'),
		('Activity', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The activity is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the ICS system.</p><p>Thank you,<br />The ICS Team</p><p>&nbsp;</p>', 'An Activity Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'ActivityReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('ActivityReport', 'DecrementWorkflow', '<p>A previously submitted activity report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the activity reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Activity Report Has Been Disapproved'),
		('ActivityReport', 'IncrementWorkflow', '<p>A activity report has been submitted for your review:</p><p><strong>Activity Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the activity reports workflow. Please log in and click the link above to review this activity report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Activity Report Is Ready For Your Review'),
		('ActivityReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Activity Report Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'RequestForInformation')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('RequestForInformation', 'Amend', '<p>An Amended response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p><p>&nbsp;</p>', 'A Previously Completed Response To A Request For Information Response Has Been Amended'),
		('RequestForInformation', 'Complete', '<p>A response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,<p>&nbsp;</p>', 'A Response To A Request For Information Has Been Completed'),
		('RequestForInformation', 'PointOfContactUpdate', '<p>Dear [[FullName]] <br /><br />RfI [[Title]] has been assigned to you for action. The deadline for a response is [[IncidentDateFormatted]]. The RfI request is for [[SummaryAnswer]] and can be viewed here [[TitleLink]] <br /><br />Many thanks<br />[[FullName]]</p>', 'RFI Assigned for Action'),
		('RequestForInformation', 'Submit', '<p>A request for information has been recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p>', 'A Request For Information Has Been Submitted')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'SpotReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('SpotReport', 'DecrementWorkflow', '<p>A previously submitted activity report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Spot Report Has Been Disapproved'),
		('SpotReport', 'IncrementWorkflow', '<p>A spot report has been submitted for your review:</p><p><strong>Spot Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow. Please log in and click the link above to review this spot report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Spot Report Is Ready For Your Review'),
		('SpotReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Spot Report Has Been Released')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = 'TrendReport')
	BEGIN

	INSERT INTO core.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText, EmailSubject)
	VALUES
		('TrendReport', 'DecrementWorkflow', '<p>A previously submitted trend report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Trend Report Has Been Disapproved'),
		('TrendReport', 'IncrementWorkflow', '<p>A trend report has been submitted for your review:</p><p><strong>Trend Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow. Please log in and click the link above to review this trend report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>', 'A Trend Report Is Ready For Your Review'),
		('TrendReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>', 'A Trend Report Has Been Released')

	END
--ENDIF
GO

TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField	
	(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
VALUES
	('Activity', '[[Comments]]', 'Comments'),
	('Activity', '[[Title]]', 'Title'),
	('Activity', '[[TitleLink]]', 'Title Link'),
	('ActivityReport', '[[Comments]]', 'Comments'),
	('ActivityReport', '[[Title]]', 'Title'),
	('ActivityReport', '[[TitleLink]]', 'Title Link'),
	('RequestForInformation', '[[FullName]]', 'Requested By'),
	('RequestForInformation', '[[IncidentDateFormatted]]', 'Incident Date'),
	('RequestForInformation', '[[InformationRequested]]', 'Information Requested'),
	('RequestForInformation', '[[KnownDetails]]', 'Details'),
	('RequestForInformation', '[[SummaryAnswer]]', 'Response'),
	('RequestForInformation', '[[TerritoryName]]', 'Territory'),
	('RequestForInformation', '[[Title]]', 'Title'),
	('RequestForInformation', '[[TitleLink]]', 'Title Link'),
	('SpotReport', '[[Comments]]', 'Comments'),
	('SpotReport', '[[Title]]', 'Title'),
	('SpotReport', '[[TitleLink]]', 'Title Link'),
	('TrendReport', '[[Comments]]', 'Comments'),
	('TrendReport', '[[Title]]', 'Title'),
	('TrendReport', '[[TitleLink]]', 'Title Link')
GO

DECLARE @cSQL VARCHAR(MAX)
DECLARE @nProjectID INT = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = '[[INSTANCENAME]]' AND IDD.DropdownCode = 'Project' ORDER BY IDD.DropdownID)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'UPDATE T SET T.ProjectID = ' + CAST(@nProjectID AS VARCHAR(10)) + ' FROM ' + S.Name + '.' + T.Name + ' T'
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN sys.Columns C ON C.Object_ID = T.Object_ID
			AND C.Name = 'ProjectID'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN	

	EXEC (@cSQL)

	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

TRUNCATE TABLE person.PersonProject
GO

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P.PersonID,
	IDD.DropdownID
FROM person.Person P
	CROSS APPLY implementer.ImplementerDropdownData IDD
WHERE IDD.DropdownCode = 'Project'
	AND IDD.ImplementerCode = '[[INSTANCENAME]]'
GO

UPDATE C
SET C.ProjectID = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.DropdownCode = 'Project' AND IDD.ImplementerCode = '[[INSTANCENAME]]' ORDER BY IDD.DropdownID DESC)
FROM contact.Contact C
GO

UPDATE P
SET P.DefaultProjectID = (SELECT TOP 1 IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.DropdownCode = 'Project' AND IDD.ImplementerCode = '[[INSTANCENAME]]' ORDER BY IDD.DropdownID DESC)
FROM person.Person P
GO

DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Activity' AND NOT EXISTS (SELECT 1 FROM activity.Activity T WHERE T.ActivityID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'ActivityReport' AND NOT EXISTS (SELECT 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'SpotReport' AND NOT EXISTS (SELECT 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = EWSGP.EntityID)
GO
DELETE EWSGP FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'TrendReport' AND NOT EXISTS (SELECT 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = EWSGP.EntityID)
GO
