USE [[INSTANCENAME]]
GO

EXEC utility.DropObject 'workflow.CanHaveListView'
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

--Begin function contact.GetContactVettingData
EXEC utility.DropObject 'contact.GetContactVettingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get the most recent set of contact vetting data from contact.ContactVetting table
-- ============================================================================================================
CREATE FUNCTION contact.GetContactVettingData
(
)

RETURNS @tTable TABLE 
	(
	ContactID INT NOT NULL PRIMARY KEY, 
	VettingDate DATE,
	VettingOutcomeID INT,
	VettingOutcomeName VARCHAR(50)
	)

AS
BEGIN

	INSERT INTO @tTable
		(ContactID, VettingDate, VettingOutcomeID, VettingOutcomeName)
	SELECT
		VD.ContactID,
		CV2.VettingDate,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM 
		(
		SELECT 
			MAX(CV1.ContactVettingID) AS ContactVettingID,
			CV1.ContactID
		FROM contact.ContactVetting CV1
		GROUP BY CV1.ContactID
		) VD JOIN contact.ContactVetting CV2 ON CV2.ContactVettingID = VD.ContactVettingID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV2.VettingOutcomeID

	RETURN

END
GO
--End function contact.GetContactVettingData

--Begin function portalupdate.GetColumnList
EXEC utility.DropObject 'portalupdate.GetColumnList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a comma delimited list of column names for a specific table
-- ==========================================================================================

CREATE FUNCTION portalupdate.GetColumnList
(
@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@Prefix VARCHAR(10)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cColumnList VARCHAR(MAX) = ''

	SELECT
		@cColumnList = COALESCE(@cColumnList + @Prefix, '') + C.Name
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		JOIN sys.Columns C ON T.Object_ID = C.Object_ID
			AND S.Name = @SchemaName
			AND T.Name = @TableName
	ORDER BY C.Name

	RETURN STUFF(@cColumnList, 1, 2, '')

END
GO
--End function portalupdate.GetColumnList

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT

	SELECT @nWorkflowStepCount = MAX(EWSGP.WorkflowStepNumber) 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.ProjectID = @ProjectID
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT TOP 1 @nWorkflowStepNumber = EWSGP.WorkflowStepNumber 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID 
		AND EWSGP.ProjectID = @ProjectID 
		AND EWSGP.IsComplete = 0 
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber
END
GO
--End function workflow.GetWorkflowStepNumber
