IF (SELECT core.GetSystemSetupValueBySetupKey('Environment', '')) = 'Dev'
	BEGIN

	DECLARE @nProjectID INT = (SELECT IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = '[[INSTANCENAME]]' AND IDD.DropdownCode = 'Project')
	DECLARE @tOutput TABLE (TerritoryID INT)

	INSERT INTO territory.Territory
		(TerritoryName, TerritoryNameLocal, Population, PopulationSource, Location, TerritoryTypeCode, ISOCountryCode2, ParentTerritoryID, PCode)
	OUTPUT INSERTED.TerritoryID INTO @tOutput
	SELECT 
		C.CommunityName, 
		C.ArabicCommunityName, 
		C.Population, 
		C.PopulationSource, 
		C.Location,
		'Community',
		'SY',
		ISNULL((SELECT T.TerritoryID FROM territory.Territory T JOIN (SELECT PTT.ParentTerritoryTypeCode FROM territory.ProjectTerritoryType PTT JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID AND PTT.ProjectID = @nProjectID AND TT.TerritoryTypeCode = 'Community') D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode AND T.Location.STIntersects(C.Location) = 1), 0),
		CAST(C.CommunityID AS VARCHAR(10))
	FROM AJACS.dbo.Community C
	WHERE C.Location IS NOT NULL
		AND C.CommunityID IN (5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,70,71,72,73,74,75,76,77,78,79,80,81,83,84,85,87,88,89,90,91,92,93,94,95,99,100,101,102,103,104,105,106,108,111,113,114,115,117,118,121,122,123,125)
	ORDER BY CommunityName

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, ProjectID, DropdownID)
	SELECT
		'[[INSTANCENAME]]',
		'Territory',
		@nProjectID,
		O.TerritoryID
	FROM @tOutput O

	INSERT INTO territory.TerritoryAnnex
		(ProjectID, TerritoryID, Notes)
	SELECT
		@nProjectID,
		O.TerritoryID,
		C.Summary
	FROM @tOutput O
		JOIN territory.Territory T ON T.TerritoryID = O.TerritoryID
		JOIN AJACS.dbo.Community C ON C.CommunityID = CAST(T.PCode AS INT)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, ProjectID, DropdownID)
	SELECT
		'LEO0001',
		'Territory',
		@nProjectID,
		O.TerritoryID
	FROM @tOutput O

	INSERT INTO LEO0001.territory.TerritoryAnnex
		(TerritoryAnnexID, ProjectID, TerritoryID, Notes)
	SELECT
		TA.TerritoryAnnexID,
		TA.ProjectID,
		TA.TerritoryID,
		TA.Notes
	FROM territory.TerritoryAnnex TA

	END
--ENDIF

GO