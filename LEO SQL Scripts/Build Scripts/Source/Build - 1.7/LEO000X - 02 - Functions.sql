USE [[INSTANCENAME]]
GO

--Begin procedure reporting.IsDraftReport
EXEC utility.DropObject 'reporting.IsDraftReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.08
-- Description:	A function to determine if a report is in draft status
-- ===================================================================

CREATE FUNCTION reporting.IsDraftReport
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nIsDraft BIT = 1
	
	IF @EntityTypeCode = 'SpotReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)) = 1 THEN 1 ELSE 0 END 
	
		END
	ELSE IF @EntityTypeCode = 'WeeklyReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID)) = 1 THEN 1 ELSE 0 END
	
		END
	--ENDIF
	
	RETURN @nIsDraft

END
GO