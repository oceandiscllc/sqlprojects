USE [[INSTANCENAME]]
GO

UPDATE T SET T.TerritoryID = 0 FROM activityreport.ActivityReportTerritory T
UPDATE T SET T.TerritoryID = 0 FROM activity.ActivityTerritory T
UPDATE T SET T.TerritoryID = 0 FROM asset.Asset T
UPDATE T SET T.TerritoryID = 0 FROM atmospheric.AtmosphericTerritory T
UPDATE T SET T.TerritoryID = 0 FROM contact.Contact T
UPDATE T SET T.TerritoryID = 0 FROM course.Course T
UPDATE T SET T.TerritoryID = 0 FROM distributor.DistributorTerritory T
UPDATE T SET T.TerritoryID = 0 FROM finding.FindingTerritory T
UPDATE T SET T.TerritoryID = 0 FROM force.Force T
UPDATE T SET T.TerritoryID = 0 FROM force.ForceUnit T
UPDATE T SET T.TerritoryID = 0 FROM impactstory.ImpactStoryTerritory T
UPDATE T SET T.TerritoryID = 0 FROM dbo.Incident T
UPDATE T SET T.TerritoryID = 0 FROM mediareport.MediaReport T
UPDATE T SET T.TerritoryID = 0 FROM product.ProductTerritory T
UPDATE T SET T.TerritoryID = 0 FROM recommendation.RecommendationTerritory T
UPDATE T SET T.TerritoryID = 0 FROM dbo.RequestForInformation T
UPDATE T SET T.TerritoryID = 0 FROM spotreport.SpotReportTerritory T
UPDATE T SET T.TerritoryID = 0 FROM trendreport.TrendReportTerritory T