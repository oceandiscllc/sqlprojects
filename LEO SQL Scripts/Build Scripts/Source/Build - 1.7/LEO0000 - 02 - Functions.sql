USE LEO0000
GO

--Begin function territory.FormatParentTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.FormatParentTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.03
-- Description:	A function to return the formatted name of a territory parent
-- ==========================================================================

CREATE FUNCTION territory.FormatParentTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cTerritoryNameFormatted VARCHAR(300) = ''
	
	SELECT @cTerritoryNameFormatted = T2.TerritoryName + ' (' + TT.TerritoryTypeName + ')' 
	FROM territory.Territory T1
		JOIN territory.Territory T2 ON T2.TerritoryID = T1.ParentTerritoryID
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T2.TerritoryTypeCode
			AND T1.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryNameFormatted, '')
	
END
GO
--End function territory.FormatParentTerritoryNameByTerritoryID
