USE LEO0000
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table dropdown.TerritoryStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.TerritoryStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TerritoryStatus
	(
	TerritoryStatusID INT NOT NULL IDENTITY(0,1),
	TerritoryStatusName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryStatus', 'DisplayOrder,TerritoryStatusName'
GO
--End table dropdown.TerritoryStatus

--Begin table implementer.Implementer
DECLARE @TableName VARCHAR(250) = 'implementer.Implementer'

EXEC utility.AddColumn @TableName, 'IsPortal', 'BIT', '0'
GO
--End table implementer.Implementer

--Begin table territory.Territory
DECLARE @TableName VARCHAR(250) = 'territory.Territory'

EXEC utility.AddColumn @TableName, 'Population', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PopulationSource', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'TerritoryNameLocal', 'NVARCHAR(250)'
GO
--End table territory.Territory
