USE LEO0000
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetSystemSetupValuesBySetupKeyList
EXEC utility.DropObject 'core.GetSystemSetupValuesBySetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.30
-- Description:	A procedure to return SetupValues from a list of SetupKeys from the core.SystemSetup table
-- =======================================================================================================

CREATE PROCEDURE core.GetSystemSetupValuesBySetupKeyList

@SetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SetupKeyList = ''
		BEGIN

		SELECT 
			SS.SetupKey,
			SS.SetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SetupKey,
			SS.SetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SetupKeyList, ',') LTT ON LTT.ListItem = SS.SetupKey
		ORDER BY SS.SetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySetupKeyList

--Begin procedure dropdown.GetTerritoryStatusData
EXEC Utility.DropObject 'dropdown.GetTerritoryStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.TerritoryStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetTerritoryStatusData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryStatusID, 
		T.TerritoryStatusName
	FROM dropdown.TerritoryStatus T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryStatusID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TerritoryStatus'
			AND (T.TerritoryStatusID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.TerritoryStatusName, T.TerritoryStatusID

END
GO
--End procedure dropdown.GetTerritoryStatusData

--Begin procedure eventlog.LogSystemSetupAction
EXEC utility.DropObject 'eventlog.LogSystemSetupAction'
GO
--End procedure eventlog.LogSystemSetupAction

--Begin procedure territory.AddCommunityProject
EXEC utility.DropObject 'territory.AddCommunityProject'
GO
--End procedure territory.AddCommunityProject

--Begin procedure territory.GetParentTerritoryByProjectID
EXEC utility.DropObject 'territory.GetParentTerritoryByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to associate a community to a project
-- =====================================================================
CREATE PROCEDURE territory.GetParentTerritoryByProjectID

@ProjectID INT,
@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
		JOIN 
			(
			SELECT PTT.ParentTerritoryTypeCode
			FROM territory.ProjectTerritoryType PTT
				JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
					AND PTT.ProjectID = @ProjectID
					AND TT.TerritoryTypeCode = @TerritoryTypeCode
			) D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1
	
END
GO
--End procedure territory.GetParentTerritoryByProjectID

--Begin procedure territory.ValidateCommunity
EXEC utility.DropObject 'territory.ValidateCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.05.06
-- Description:	A stored procedure to validate a community
-- =======================================================
CREATE PROCEDURE territory.ValidateCommunity

@TerritoryName VARCHAR(250),
@TerritoryNameLocal NVARCHAR(250),
@Latitude VARCHAR(50),
@Longitude VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID,
		T.TerritoryName,
		T.TerritoryNameLocal,
		CAST(ROUND((GEOGRAPHY::STGeomFromText(T.Location.STAsText(), 4326).STDistance(GEOGRAPHY::STGeomFromText('POINT(' + @Longitude + ' ' + @Latitude + ')', 4326)) / 1000), 2) AS VARCHAR(5)) + ' km' AS Distance
	FROM territory.Territory T
	WHERE T.TerritoryTypeCode = 'Community'
		AND 
			(
			T.TerritoryName = @TerritoryName
				OR T.TerritoryNameLocal = @TerritoryNameLocal
				OR GEOGRAPHY::STGeomFromText(T.Location.STAsText(), 4326).STDistance(GEOGRAPHY::STGeomFromText('POINT(' + @Longitude + ' ' + @Latitude + ')', 4326)) <= 5000
			)
	ORDER BY 2

END
GO
--End procedure territory.ValidateCommunity
