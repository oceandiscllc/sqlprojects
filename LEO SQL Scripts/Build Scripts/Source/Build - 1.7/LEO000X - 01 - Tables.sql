USE [[INSTANCENAME]]
GO

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE FU
SET 
	FU.CommanderFullName = contact.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle'),
	FU.DeputyCommanderFullName = contact.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.ForceUnit FU
GO	
--End table force.ForceUnit

--Begin table territory.TerritoryAnnex
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryAnnex'

EXEC utility.DropObject @TableName

CREATE TABLE territory.TerritoryAnnex
	(
	TerritoryAnnexID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryID INT,
	TerritoryStatusID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryAnnexID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryAnnex', 'ProjectID,TerritoryID'
GO
--End table territory.TerritoryAnnex
