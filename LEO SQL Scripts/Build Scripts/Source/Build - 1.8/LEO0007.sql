USE LEO0007
GO

DECLARE @nProjectID INT = (SELECT IDD.DropdownID FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = 'LEO0007' AND IDD.DropdownCode = 'Project')

INSERT INTO territory.Territory
	(TerritoryName, TerritoryNameLocal, Population, PopulationSource, Location, TerritoryTypeCode, ISOCountryCode2, ParentTerritoryID, IntegrationID, IntegrationCode)
SELECT 
	C.CommunityName, 
	C.ArabicCommunityName, 
	C.Population, 
	C.PopulationSource, 
	C.Location,
	'Community',
	'SY',
	ISNULL((SELECT T.TerritoryID FROM territory.Territory T JOIN (SELECT PTT.ParentTerritoryTypeCode FROM territory.ProjectTerritoryType PTT JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID AND PTT.ProjectID = @nProjectID AND TT.TerritoryTypeCode = 'Community') D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode AND T.Location.STIntersects(C.Location) = 1), 0),
	C.CommunityID,
	'AJACS'
FROM AJACS.dbo.Community C
WHERE C.Location IS NOT NULL
	AND C.IsActive = 1
	AND NOT EXISTS 
		(
		SELECT 1
		FROM territory.Territory T
		WHERE T.IntegrationID = C.CommunityID
			AND T.IntegrationCode = 'AJACS'
		)
ORDER BY C.CommunityName

INSERT INTO implementer.ImplementerDropdownData
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO0007',
	'Territory',
	@nProjectID,
	T.TerritoryID
FROM territory.Territory T
WHERE T.IntegrationCode = 'AJACS'
	AND NOT EXISTS
		(
		SELECT 1
		FROM implementer.ImplementerDropdownData IDD
		WHERE IDD.ImplementerCode = 'LEO0007'
			AND IDD.DropdownCode = 'Territory'
			AND IDD.ProjectID = @nProjectID 
			AND IDD.DropdownID = T.TerritoryID
		)

INSERT INTO implementer.ImplementerDropdownData
	(ImplementerCode, DropdownCode, ProjectID, DropdownID)
SELECT
	'LEO0001',
	'Territory',
	@nProjectID,
	T.TerritoryID
FROM territory.Territory T
WHERE T.IntegrationCode = 'AJACS'
	AND NOT EXISTS
		(
		SELECT 1
		FROM implementer.ImplementerDropdownData IDD
		WHERE IDD.ImplementerCode = 'LEO0001'
			AND IDD.DropdownCode = 'Territory'
			AND IDD.ProjectID = @nProjectID 
			AND IDD.DropdownID = T.TerritoryID
		)

TRUNCATE TABLE territory.TerritoryAnnex;

INSERT INTO territory.TerritoryAnnex
	(ProjectID, TerritoryID, Notes, TerritoryStatusID)
SELECT
	@nProjectID,
	T.TerritoryID,
	C.Summary,

	CASE
		WHEN ID.ImpactDecisionName = 'Yes'
		THEN (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Active')
		WHEN ID.ImpactDecisionName = 'Pending'
		THEN (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Pending')
		ELSE (SELECT TS.TerritoryStatusID FROM dropdown.TerritoryStatus TS WHERE TS.TerritoryStatusName = 'Inactive')
	END

FROM territory.Territory T
	JOIN AJACS.dbo.Community C ON C.CommunityID = T.IntegrationID
		AND C.IsActive = 1
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		AND T.IntegrationCode = 'AJACS'
		AND NOT EXISTS
			(
			SELECT 1
			FROM territory.TerritoryAnnex TA
			WHERE TA.ProjectID = @nProjectID
				AND TA.TerritoryID = T.TerritoryID
			)

TRUNCATE TABLE LEO0001.territory.TerritoryAnnex;

INSERT INTO LEO0001.territory.TerritoryAnnex
	(TerritoryAnnexID, ProjectID, TerritoryID, Notes, TerritoryStatusID)
SELECT
	TA.TerritoryAnnexID,
	TA.ProjectID,
	TA.TerritoryID,
	TA.Notes, 
	TA.TerritoryStatusID
FROM territory.TerritoryAnnex TA
GO