USE LEO0000
GO

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'IntegrationID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'
GO
--End table document.Document

--Begin implementer.Implementer
EXEC implementer.ImplementerAddUpdate 'LEO0007', 'AJACS', 'Daisy', 'AJACS', 'Strat-Comms'
GO
--End implementer.Implementer

--Begin table territory.Territory
DECLARE @TableName VARCHAR(250) = 'territory.Territory'

EXEC utility.AddColumn @TableName, 'IntegrationID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'
GO
--End table territory.Territory
