USE LEO0000
GO

--Begin procedure implementer.ImplementerAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new implementer to the LEO system and initialize its dropdown data
-- ===========================================================================================================
CREATE PROCEDURE implementer.ImplementerAddUpdate
@ImplementerCode VARCHAR(25),
@ImplementerName VARCHAR(250),
@ImplementerURL VARCHAR(50),
@ProjectNameList VARCHAR(MAX),
@ProjectAliasList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCode VARCHAR(250)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @tTable TABLE (ItemID INT NOT NULL DEFAULT 0 PRIMARY KEY, ProjectName VARCHAR(50), ProjectAlias VARCHAR(50))

	IF NOT EXISTS (SELECT 1 FROM implementer.Implementer I WHERE I.ImplementerCode = @ImplementerCode)
		INSERT INTO implementer.Implementer (ImplementerCode, ImplementerName, ImplementerURL) VALUES (@ImplementerCode, @ImplementerName, @ImplementerURL)
	--ENDIF

	INSERT INTO @tTable
		(ItemID, ProjectName)
	SELECT
		LTT.ListItemID,
		LTT.ListItem
	FROM core.ListToTable(@ProjectNameList, ',') LTT

	UPDATE T
	SET T.ProjectAlias = LTT.ListItem
	FROM @tTable T 
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItemID = T.ItemID

	INSERT INTO dropdown.Project
		(ProjectName, ProjectAlias)
	SELECT
		T.ProjectName,
		T.ProjectAlias
	FROM @tTable T 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dropdown.Project P
		WHERE P.ProjectName = T.ProjectName
		)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, DropdownID)
	SELECT
		@ImplementerCode,
		'Project',
		P.ProjectID
	FROM dropdown.Project P
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItem = P.ProjectName
			AND NOT EXISTS
				(
				SELECT 1
				FROM implementer.ImplementerDropdownData IDD
				WHERE IDD.ImplementerCode = @ImplementerCode
					AND IDD.DropdownCode = 'Project'
					AND IDD.DropdownID = P.ProjectID
				)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = 'Dropdown'
				AND T.Name <> 'Project'
		ORDER BY T.Name

	OPEN oCursor
	FETCH oCursor INTO @cDropdownCode
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC implementer.ImplementerDropdownDataAddUpdate @cDropdownCode

		FETCH oCursor INTO @cDropdownCode
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE @nProjectID INT = (SELECT TOP 1 PTT.ProjectID FROM territory.ProjectTerritoryType PTT ORDER BY PTT.ProjectID)

	;
	WITH P AS
		(
		SELECT P.ProjectID
		FROM dropdown.Project P
		WHERE P.ProjectID > 0
			AND NOT EXISTS
				(
				SELECT 1
				FROM territory.ProjectTerritoryType PTT
				WHERE PTT.ProjectID = P.ProjectID
				)
		)

	INSERT INTO territory.ProjectTerritoryType
		(ProjectID, TerritoryTypeID, ParentTerritoryTypeCode)
	SELECT 
		P.ProjectID,
		PTT1.TerritoryTypeID,
		PTT1.ParentTerritoryTypeCode
	FROM P
		CROSS APPLY territory.ProjectTerritoryType PTT1
	WHERE PTT1.ProjectID = @nProjectID
		AND NOT EXISTS
			(
			SELECT 1
			FROM territory.ProjectTerritoryType PTT2
			WHERE PTT1.TerritoryTypeID = PTT2.TerritoryTypeID
				AND PTT1.ParentTerritoryTypeCode = PTT1.ParentTerritoryTypeCode
				AND PTT1.ProjectID = P.ProjectID
			)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, DropdownID)
	SELECT
		I.ImplementerCode,
		'Project',
		P.ProjectID
	FROM dropdown.Project P CROSS APPLY implementer.Implementer I
	WHERE I.IsPortal = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM implementer.ImplementerDropdownData IDD
			WHERE IDD.ImplementerCode = I.ImplementerCode
				AND IDD.DropdownCode = 'Project'
				AND IDD.DropdownID = P.ProjectID
			)

END
GO
--End procedure implementer.ImplementerAddUpdate

--Begin procedure territory.GetGovernorates
EXEC Utility.DropObject 'territory.GetGovernorates'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.06.03
-- Description:	A stored procedure to return data from the territory.Territory table
-- =================================================================================
CREATE PROCEDURE territory.GetGovernorates

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID,
		T.TerritoryName
	FROM territory.Territory T
	WHERE T.TerritoryTypeCode = 'Governorate'
	ORDER BY T.TerritoryName

END
GO
--End procedure territory.GetGovernorates
