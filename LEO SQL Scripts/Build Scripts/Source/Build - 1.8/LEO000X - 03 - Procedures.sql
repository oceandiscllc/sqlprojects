USE [[INSTANCENAME]]
GO

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the product.Product table
--
-- Author:		Todd Pires
-- Create date:	2017.05.06
-- Description:	Added facebook url support
-- ======================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFBPageID BIGINT
	DECLARE @nFBPostID BIGINT
	DECLARE @cFBPostName VARCHAR(MAX)

	SELECT
		@cFBPostName = FBPO.Name,
		@nFBPageID = FBPO.PageID,
		@nFBPostID = PD.FacebookPostID
	FROM integration.FacebookPost FBPO
		JOIN product.ProductDistribution PD ON PD.FacebookPostID = FBPO.ID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID

	--Product
	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		core.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.Quantity,
		P.ReleaseDate,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		PO.ProductOriginID,
		PO.ProductOriginName,
		PS.ProductStatusID,
		PS.ProductStatusName,
		PT.ProductTypeID,
		PT.ProductTypeName,
		TA.TargetAudienceID,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductOrigin PO ON PO.ProductOriginID = P.ProductOriginID
		JOIN dropdown.ProductStatus PS ON PS.ProductStatusID = P.ProductStatusID
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND P.ProductID = @ProductID
			AND P.ProjectID = @ProjectID

	--ProductCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM product.ProductCampaign PC
		JOIN campaign.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ProductCommunicationTheme
	SELECT
		CT.CommunicationThemeID,
		CT.CommunicationThemeName
	FROM product.ProductCommunicationTheme PCT
		JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
			AND PCT.ProductID = @ProductID
			AND PCT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ProductDistribution
	SELECT
		PD.Analysis,
		PD.CommentCount,
		PD.DistributionDate,
		core.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.FacebookPostID,
		PD.LikeCount,	 
		PD.OrganicReach,
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		D.DistributorID,
		D.DistributorName,
		DT.DistributorTypeID,
		DT.DistributorTypeName,
		@cFBPostName AS FacebookPostName,
		'https://www.facebook.com/' + CAST(ISNULL(@nFBPageID, 0) AS VARCHAR(20)) + '/posts/' + CAST(ISNULL(@nFBPostID, 0) AS VARCHAR(20)) AS FacebookPostURL
	FROM product.ProductDistribution PD
		JOIN distributor.Distributor D ON D.DistributorID = PD.DistributorID
		JOIN dropdown.DistributorType DT ON DT.DistributorTypeID = D.DistributorTypeID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID
	
	--ProductDistributionCount
	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM product.ProductDistribution PD
	WHERE PD.ProductID = @ProductID
		AND PD.ProjectID = @ProjectID

	--ProductRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM product.ProductRelevantTheme PRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = PRT.RelevantThemeID
			AND PRT.ProductID = @ProductID
			AND PRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ProductTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryNameFormatted
	FROM product.ProductTerritory PT
	WHERE PT.ProductID = @ProductID
		AND PT.ProjectID = @ProjectID
	ORDER BY 2, 1
	
END
GO
--End procedure product.GetProductByProductID

--Begin procedure workflow.IsWorkflowComplete
EXEC Utility.DropObject 'workflow.IsWorkflowComplete'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.06.03
-- Description:	A stored procedure to return a flag indicating if a workflosw is complete
-- ======================================================================================
CREATE PROCEDURE workflow.IsWorkflowComplete

@EntityTypeCode VARCHAR(50), 
@EntityID INT, 
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID, @ProjectID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID, @ProjectID) > 0
			THEN 1
			ELSE 0
		END AS IsWorkflowComplete

END
GO
--End procedure workflow.IsWorkflowComplete
