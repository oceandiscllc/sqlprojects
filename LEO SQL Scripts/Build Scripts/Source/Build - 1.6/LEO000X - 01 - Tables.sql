USE [[INSTANCENAME]]
GO

--Begin schema campaign
EXEC utility.AddSchema 'campaign'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'dbo' AND T.Name = 'Campaign')
	ALTER SCHEMA campaign TRANSFER dbo.Campaign
--ENDIF
GO
--End schema campaign

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName
GO
--End table dropdown.IndicatorType

--Begin table mediareport.MediaReport
ALTER TABLE mediareport.MediaReport ALTER COLUMN MediaReportTitle NVARCHAR(250)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Summary NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Comments NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN Implications NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN RiskMitigation NVARCHAR(MAX)
ALTER TABLE mediareport.MediaReport ALTER COLUMN CommunicationOpportunities NVARCHAR(MAX)
GO
--End table mediareport.MediaReport

--Begin table activityreport.ActivityReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportRelevantTheme
	(
	ActivityReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportRelevantTheme', 'ActivityReportID,RelevantThemeID'
GO
--End table activityreport.ActivityReportRelevantTheme

--Begin table campaign.CampaignRelevantTheme
DECLARE @TableName VARCHAR(250) = 'campaign.CampaignRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE campaign.CampaignRelevantTheme
	(
	CampaignRelevantThemeID INT NOT NULL IDENTITY(1,1),
	CampaignID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CampaignID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CampaignRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_CampaignRelevantTheme', 'CampaignID,RelevantThemeID'
GO
--End table campaign.CampaignRelevantTheme

--Begin table impactstory.ImpactStoryRelevantTheme
DECLARE @TableName VARCHAR(250) = 'impactstory.ImpactStoryRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE impactstory.ImpactStoryRelevantTheme
	(
	ImpactStoryRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ImpactStoryID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ImpactStoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ImpactStoryRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ImpactStoryRelevantTheme', 'ImpactStoryID,RelevantThemeID'
GO
--End table impactstory.ImpactStoryRelevantTheme

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.AddColumn @TableName, 'Quantity', 'INT'
GO
--End table product.Product

--Begin table product.ProductRelevantTheme
DECLARE @TableName VARCHAR(250) = 'product.ProductRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE product.ProductRelevantTheme
	(
	ProductRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ProductID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProductRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProductRelevantTheme', 'ProductID,RelevantThemeID'
GO
--End table product.ProductRelevantTheme

--Begin table spotreport.SpotReport
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReport'

EXEC utility.AddColumn @TableName, 'SummaryMap', 'VARBINARY(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryMapZoom', 'INT', '0'
GO
--End table spotreport.SpotReport

--Begin table trendreport.TrendReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportRelevantTheme
	(
	TrendReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	TrendReportID INT,
	RelevantThemeID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportRelevantTheme', 'TrendReportID,RelevantThemeID'
GO
--End table trendreport.TrendReportRelevantTheme