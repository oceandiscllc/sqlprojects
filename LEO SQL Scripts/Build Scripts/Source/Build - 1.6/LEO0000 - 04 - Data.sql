USE LEO0000
GO

EXEC implementer.ImplementerSynonymAddUpdate 'document', 'Document';
EXEC implementer.ImplementerSynonymAddUpdate 'document', 'GetDocumentByDocumentData';
GO

EXEC core.EntityTypeAddUpdate 'DashboardData', 'Dashboard Data', 'Dashboard Data';
EXEC core.EntityTypeAddUpdate 'Main', 'Main', 'Main';
GO

DELETE MI
FROM core.MenuItem MI
WHERE MI.MenuitemCode = 'IndicatorTypeList'
GO

--Begin table dropdown.CommunicationTheme
DECLARE @tTable TABLE (CommunicationThemeName VARCHAR(50))

INSERT INTO @tTable
	(CommunicationThemeName)
VALUES
	('Bolster Syrian Identity and Values'),
	('Support Moderate Opposition'),
	('Support Syrian Media organizations'),
	('Undermine Regime''s Narrative'),
	('Undermine Violent Extremism')

MERGE dropdown.CommunicationTheme T1
	USING (SELECT T2.CommunicationThemeName FROM @tTable T2) T3
		ON T3.CommunicationThemeName = T1.CommunicationThemeName
	WHEN NOT MATCHED THEN
	INSERT (CommunicationThemeName)
	VALUES
		(
		T3.CommunicationThemeName
		);

EXEC implementer.ImplementerDropdownDataAddUpdate 'CommunicationTheme';

UPDATE IDD
SET IDD.IsActive = 
	CASE
		WHEN IDD.DropdownID = 0
		THEN 1
		WHEN IDD.ImplementerCode = 'LEO0005' AND EXISTS (SELECT 1 FROM @tTable T WHERE T.CommunicationThemeName = CT.CommunicationThemeName)
		THEN 1
		WHEN IDD.ImplementerCode <> 'LEO0005' AND NOT EXISTS (SELECT 1 FROM @tTable T WHERE T.CommunicationThemeName = CT.CommunicationThemeName)
		THEN 1
		ELSE 0
	END
FROM implementer.ImplementerDropdownData IDD
	JOIN dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = IDD.DropdownID
		AND IDD.DropdownCode = 'CommunicationTheme'
GO
--End table dropdown.CommunicationTheme

--Begin table dropdown.IndicatorType
TRUNCATE TABLE dropdown.IndicatorType
GO

SET IDENTITY_INSERT dropdown.IndicatorType ON
GO

INSERT INTO dropdown.IndicatorType (IndicatorTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.IndicatorType OFF
GO

INSERT INTO dropdown.IndicatorType
	(IndicatorTypeName, IndicatorTypeCode, DisplayOrder)
VALUES
	('Indicator Type One', 'IT1', 1),
	('Indicator Type Two', 'IT2', 2),
	('Indicator Type Three', 'IT3', 3),
	('Indicator Type Four', 'IT4', 4)
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'IndicatorType'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIndicatorTypeData'
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IndicatorType'
GO
--End table dropdown.IndicatorType

--Begin table dropdown.Project
UPDATE P
SET P.ProjectAlias = 'Strat-Comms'
FROM dropdown.Project P
WHERE P.ProjectID > 0
GO
--End table dropdown.Project

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

SET IDENTITY_INSERT dropdown.RelevantTheme ON
GO  
INSERT INTO dropdown.RelevantTheme (RelevantThemeID) VALUES (0)
GO
SET IDENTITY_INSERT dropdown.RelevantTheme OFF
GO  

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName) 
VALUES 
	('Military advances/retreats'),
	('Truces and evacuation deals'),
	('Services: e.g. electricity, water'),
	('Livelihoods and economy'),
	('Political track'),
	('International Humanitarian Law: e.g. detainees, humanitarian relief'),
	('Extremism and extremist groups'),
	('Gender and women''s issues'),
	('Opposition fragmentation'),
	('Positive perceptions of Syrian state and army'),
	('Negative perceptions of Syrian state and army'),
	('Internal unrest in regime areas'),
	('Sectarianism'),
	('Religious minorities'),
	('Kurdish issues'),
	('Foreign influence and military intervention'),
	('Fragmentation of regime control/rise of alternate military, economic, political power centers')
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'RelevantTheme'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetRelevantThemeData'
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'RelevantTheme'
GO
--End table dropdown.RelevantTheme

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Main', 
	@DESCRIPTION='Grant user access to dashboard links', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='Default', 
	@PERMISSIONABLEGROUPCODE='Utility', 
	@PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', 
	@PERMISSIONCODE='CanHaveDashboardLinks';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Main', 
	@DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='Default', 
	@PERMISSIONABLEGROUPCODE='Utility', 
	@PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', 
	@PERMISSIONCODE='CanRecieveDashboardInformationRequests';
GO
--End table permissionable.Permissionable
