USE [[INSTANCENAME]]
GO

--Begin table document.Document
IF '[[INSTANCENAME]]' <> 'LEO0001'
	BEGIN

	INSERT INTO LEO0000.document.Document
		(ImplementerID, DocumentDate, DocumentDescription, DocumentName, DocumentTitle, DocumentTypeID, ProjectID, Extension, CreatePersonID, ContentType, ContentSubtype, PhysicalFileSize, DocumentData, Thumbnail)
	SELECT
		D.DocumentID, 
		D.DocumentDate, 
		D.DocumentDescription, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.ProjectID, 
		D.Extension, 
		D.CreatePersonID, 
		D.ContentType, 
		D.ContentSubtype, 
		D.PhysicalFileSize, 
		D.DocumentData, 
		D.Thumbnail
	FROM document.Document D

	UPDATE DE
	SET DE.DocumentID = LEOD.DocumentID
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN LEO0000.document.Document LEOD ON LEOD.ImplementerID = D.DocumentID
			AND D.ProjectID = LEOD.ProjectID

	END
--ENDIF
GO

EXEC utility.AddSchema 'deprecated'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'document' AND T.Name = 'Document')
	BEGIN

	EXEC utility.DropFullTextIndex 'document.Document';
	ALTER SCHEMA deprecated TRANSFER document.Document;

	END
--ENDIF
GO
