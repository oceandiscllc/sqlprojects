USE [[INSTANCENAME]]
GO

--Begin procedure utility.DropFullTextIndex
EXEC utility.DropObject 'utility.DropFullTextIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropFullTextIndex

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT * FROM sys.fulltext_indexes FTI WHERE FTI.object_id = OBJECT_ID(@TableName))
		BEGIN

		SET @cSQL = 'ALTER FULLTEXT INDEX ON ' + @TableName + ' DISABLE'
		EXEC (@cSQL)

		SET @cSQL = 'DROP FULLTEXT INDEX ON ' + @TableName
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropFullTextIndex

--Begin table document.Document
IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'DocumentSearchCatalog')
	CREATE FULLTEXT CATALOG DocumentSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentName VARCHAR(50),
	DocumentTitle NVARCHAR(250),
	DocumentTypeID INT,
	ProjectID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	PhysicalFileSize BIGINT,
	DocumentData VARBINARY(MAX),
	Thumbnail VARBINARY(MAX),
	ImplementerID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImplementerID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET D.Extension = ISNULL(@cFileExtenstion, @cExtenstion)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		

utility.DropFullTextIndex 'document.Document'

CREATE FULLTEXT INDEX ON document.Document 
	(
	DocumentData TYPE COLUMN Extension LANGUAGE ENGLISH,
	DocumentDescription LANGUAGE ENGLISH,
	DocumentName LANGUAGE ENGLISH,
	DocumentTitle LANGUAGE ENGLISH
	) KEY INDEX PK_Document ON DocumentSearchCatalog
GO
--End table document.Document

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorType
	(
	IndicatorTypeID INT NOT NULL IDENTITY(0,1),
	IndicatorTypeCode VARCHAR(50),
	IndicatorTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IndicatorType', 'DisplayOrder,IndicatorTypeName', 'IndicatorTypeID'
GO
--End table dropdown.IndicatorType

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.AddColumn @TableName, 'ProjectAlias', 'VARCHAR(50)'
GO
--End table dropdown.Project

--Begin table dropdown.RelevantTheme
DECLARE @TableName VARCHAR(250) = 'dropdown.RelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RelevantTheme
	(
	RelevantThemeID INT NOT NULL IDENTITY(0,1),
	RelevantThemeName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_RelevantTheme', 'DisplayOrder,RelevantThemeName'
GO
--End table dropdown.RelevantTheme

--Begin table implementer.ImplementerDropdownData
DECLARE @TableName VARCHAR(250) = 'implementer.ImplementerDropdownData'

EXEC utility.AddColumn @TableName, 'DisplayOrder', 'INT'
GO
--End table implementer.ImplementerDropdownData
