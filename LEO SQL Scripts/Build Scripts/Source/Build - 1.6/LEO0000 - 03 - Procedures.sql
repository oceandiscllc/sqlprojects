USE [[INSTANCENAME]]
GO

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@HasWorkflow BIT = 0,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.HasWorkflow = @HasWorkflow,
		ET.SchemaName = @SchemaName,
		ET.TableName = @TableName,
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName

	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,HasWorkflow,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode, 
		@EntityTypeName, 
		@EntityTypeNamePlural,
		@HasWorkflow,
		@SchemaName,
		@TableName,
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure dropdown.GetIndicatorTypeData
EXEC Utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IndicatorType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIndicatorTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorTypeID, 
		T.IndicatorTypeName
	FROM dropdown.IndicatorType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IndicatorTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IndicatorType'
			AND (T.IndicatorTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorTypeName, T.IndicatorTypeID

END
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure dropdown.GetRelevantThemeData
EXEC Utility.DropObject 'dropdown.GetRelevantThemeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.RelevantTheme table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetRelevantThemeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RelevantThemeID, 
		T.RelevantThemeName
	FROM dropdown.RelevantTheme T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.RelevantThemeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'RelevantTheme'
			AND (T.RelevantThemeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.RelevantThemeName, T.RelevantThemeID

END
GO
--End procedure dropdown.GetRelevantThemeData

--Begin procedure implementer.ImplementerAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new implementer to the LEO system and initialize its dropdown data
-- ===========================================================================================================
CREATE PROCEDURE implementer.ImplementerAddUpdate
@ImplementerCode VARCHAR(25),
@ImplementerName VARCHAR(250),
@ImplementerURL VARCHAR(50),
@ProjectNameList VARCHAR(MAX),
@ProjectAliasList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCode VARCHAR(250)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @tTable TABLE (ItemID INT NOT NULL DEFAULT 0 PRIMARY KEY, ProjectName VARCHAR(50), ProjectAlias VARCHAR(50))

	IF NOT EXISTS (SELECT 1 FROM implementer.Implementer I WHERE I.ImplementerCode = @ImplementerCode)
		INSERT INTO implementer.Implementer (ImplementerCode, ImplementerName, ImplementerURL) VALUES (@ImplementerCode, @ImplementerName, @ImplementerURL)
	--ENDIF

	INSERT INTO @tTable
		(ItemID, ProjectName)
	SELECT
		LTT.ListItemID,
		LTT.ListItem
	FROM core.ListToTable(@ProjectNameList, ',') LTT

	UPDATE T
	SET T.ProjectAlias = LTT.ListItem
	FROM @tTable T 
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItemID = T.ItemID

	INSERT INTO dropdown.Project
		(ProjectName, ProjectAlias)
	SELECT
		T.ProjectName,
		T.ProjectAlias
	FROM @tTable T 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dropdown.Project P
		WHERE P.ProjectName = T.ProjectName
		)

	INSERT INTO implementer.ImplementerDropdownData
		(ImplementerCode, DropdownCode, DropdownID)
	SELECT
		@ImplementerCode,
		'Project',
		P.ProjectID
	FROM dropdown.Project P
		JOIN core.ListToTable(@ProjectNameList, ',') LTT ON LTT.ListItem = P.ProjectName

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT T.Name
		FROM sys.Tables T
			JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
				AND S.Name = 'Dropdown'
				AND T.Name <> 'Project'
		ORDER BY T.Name

	OPEN oCursor
	FETCH oCursor INTO @cDropdownCode
	WHILE @@fetch_status = 0
		BEGIN
		
		EXEC implementer.ImplementerDropdownDataAddUpdate @cDropdownCode

		FETCH oCursor INTO @cDropdownCode
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure implementer.ImplementerAddUpdate