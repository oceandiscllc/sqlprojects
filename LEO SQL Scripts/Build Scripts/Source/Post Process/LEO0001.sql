--Begin strip identity specifications and create composite primary keys
DECLARE @cColumnName VARCHAR(250)
DECLARE @cIndexName VARCHAR(250)
DECLARE @cIndexType VARCHAR(50)
DECLARE @cNewColumnName VARCHAR(250)
DECLARE @cSchemaName VARCHAR(50)
DECLARE @cSchemaNameTableName VARCHAR(300)
DECLARE @cSchemaNameTableNameColumnName VARCHAR(500)
DECLARE @cSQLText VARCHAR(MAX)
DECLARE @cTableName VARCHAR(250)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		S1.Name AS SchemaName,
		T1.Name AS TableName,
		C1.Name AS ColumnName,
		I.IndexName,
		I.IndexType
	FROM sys.Tables T1
		JOIN sys.Schemas S1 ON S1.Schema_ID = T1.Schema_ID
			AND S1.Name NOT IN ('aggregator', 'core', 'document', 'dropdown', 'eventlog', 'integration', 'person', 'reporting', 'syslog', 'workflow')
		JOIN sys.Columns C1 ON C1.Object_ID = T1.Object_ID
			AND C1.Is_Identity = 1
			AND EXISTS
				(
				SELECT 1
				FROM sys.Tables T2
					JOIN sys.Schemas S2 ON S2.Schema_ID = T2.Schema_ID
					JOIN sys.Columns C2 ON C2.Object_ID = T2.Object_ID
						AND S1.Name = S2.Name
						AND T1.Name = T2.Name
						AND C2.Name = 'ProjectID'
				)
		JOIN
			(
			SELECT 
				C3.Name AS ColumnName,
				I.Name AS IndexName,
				I.type_desc AS IndexType
			FROM sys.indexes I
				JOIN sys.index_columns IC ON IC.Object_ID = I.Object_ID 
					AND IC.Index_ID = I.Index_ID
				JOIN sys.Columns C3 ON C3.Object_ID = IC.Object_ID
					AND C3.Column_ID = IC.Column_ID
					AND I.is_primary_key = 1
			) I ON I.ColumnName = C1.Name
	ORDER BY 1, 2, 3

OPEN oCursor
FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
WHILE @@fetch_status = 0
	BEGIN

	SET @cNewColumnName = @cColumnName + '_NoAN'
	SET @cSchemaNameTableName = @cSchemaName + '.' + @cTableName
	SET @cSchemaNameTableNameColumnName = @cSchemaNameTableName + '.' + @cNewColumnName

	EXEC utility.AddColumn @cSchemaNameTableName, @cNewColumnName, 'INT';

	SET @cSQLText = 'UPDATE T SET T.' + @cNewColumnName + ' = T.' + @cColumnName + ' FROM ' + @cSchemaNameTableName + ' T'
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP CONSTRAINT ' + @cIndexName
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP COLUMN ' + @cColumnName
	EXEC (@cSQLText);

	EXEC sp_rename @cSchemaNameTableNameColumnName, @cColumnName, 'COLUMN';
	EXEC utility.SetDefaultConstraint @cSchemaNameTableName, @cColumnName, 'INT', '0', 1;

	IF @cIndexType = 'CLUSTERED'
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	ELSE 
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyNonClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	--ENDIF

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End strip identity specifications and create composite primary keys

--Begin drop triggers
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		'DROP Trigger ' + S.Name + '.' + O.Name AS SQLText
	FROM sysobjects O
		JOIN sys.tables T ON O.Parent_Obj = T.Object_ID
		JOIN sys.schemas S ON T.Schema_ID = S.Schema_ID 
			AND O.Type = 'TR' 

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSQLText
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End drop triggers

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P1.PersonID,
	P2.ProjectID
FROM person.Person P1 
	CROSS APPLY dropdown.Project P2
WHERE P1.IsSuperAdministrator = 1
	AND P2.ProjectID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP
		WHERE PP.PersonID = P1.PersonID
			AND PP.ProjectID = P2.ProjectID
		)