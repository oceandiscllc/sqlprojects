USE [LEO0002]
GO
;
WITH ID AS 
	( 
	SELECT
		ic.[index_id] + ic.[object_id] AS [IndexId],
		S.Name AS SchemaName,
		T.Name AS TableName,
		I.Name AS IndexName,
		C.Name AS ColumnName,
		I.Type_Desc AS IndexType,
		I.Is_Primary_Key AS IsPrimaryKey
	FROM sys.indexes I
		JOIN sys.Index_Columns IC ON IC.Index_ID = I.Index_ID
			AND IC.Object_ID = I.Object_ID
		JOIN sys.Columns C ON C.Column_ID = IC.Column_ID
			AND C.Object_ID = I.Object_ID
		JOIN sys.Tables T ON T.Object_ID = I.Object_ID
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID 
	) 

SELECT
	ID.SchemaName,
	ID.TableName,
	ID.IndexName,
	ID.IndexType,
	ID.IsPrimaryKey,
	STUFF((SELECT ', ' + ID2.ColumnName FROM ID ID2 WHERE ID.IndexId = ID2.IndexId FOR XML PATH('')), 1 , 1, '') AS ColumnList
FROM ID
GROUP BY ID.IndexId, ID.SchemaName, ID.TableName, ID.IndexName, ID.IndexType, ID.IsPrimaryKey
ORDER BY ID.SchemaName, ID.TableName, ID.IsPrimaryKey DESC