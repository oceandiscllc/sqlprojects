SELECT
	S.Name AS SchemaName,
	T.Name AS TableName,
	'TRUNCATE TABLE ' + S.Name + '.' + T.Name
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
		AND S.Name NOT IN ('dropdown','logicalframework','permissionable','person','territory','workflow')
		AND T.Name NOT IN ('EmailTemplate','EmailTemplateField','EntityType','MenuItem','MenuItemPermissionableLineage')
ORDER BY S.Name, T.Name

