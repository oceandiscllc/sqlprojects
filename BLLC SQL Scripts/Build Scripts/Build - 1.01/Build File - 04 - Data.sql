USE BLLC
GO

--Begin table dropdown.ContactType
UPDATE CT
SET CT.ContactTypeName = 
	CASE 
		WHEN CT.ContactTypeID = 1
 		THEN 'Beneficiary - CSO'
		WHEN CT.ContactTypeID = 2
		THEN 'Beneficiary - Directorate'
		WHEN CT.ContactTypeID = 3
		THEN 'Beneficiary - Local Council'
		WHEN CT.ContactTypeID = 4
		THEN 'Staff and Partner'
	END
FROM dropdown.ContactType CT
WHERE CT.ContactTypeID IN (1,2,3,4)
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Beneficiary - Local Directorate')
 	BEGIN

 	INSERT INTO dropdown.ContactType
 		(ContactTypeName, DisplayOrder)
 	VALUES
 		('Beneficiary - Local Directorate', 0),
		('Beneficiary - Provincial Council', 0),
		('Other', 99)

	END
--ENDIF
--End table dropdown.ContactType
