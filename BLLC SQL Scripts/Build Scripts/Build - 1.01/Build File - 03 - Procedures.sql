USE BLLC
GO

--Begin procedure dbo.ValidateGovernmentIDNumber
EXEC Utility.DropObject 'dbo.ValidateGovernmentIDNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidateGovernmentIDNumber

@GovernmentIDNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.GovernmentIDNumber = @GovernmentIDNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidateGovernmentIDNumber

--Begin procedure dbo.ValidatePassportNumber
EXEC Utility.DropObject 'dbo.ValidatePassportNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidatePassportNumber

@PassportNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.PassportNumber = @PassportNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidatePassportNumber