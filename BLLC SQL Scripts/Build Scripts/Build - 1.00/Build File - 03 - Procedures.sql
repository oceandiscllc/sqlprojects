USE BLLC
GO

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure eventlog.LogServerSetupAction
EXEC utility.DropObject 'eventlog.LogServerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogServerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogServerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogServerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogServerSetupActionTable
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupID = @EntityID
		
		ALTER TABLE #LogServerSetupActionTable DROP COLUMN ServerSetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ServerSetup'), ELEMENTS
			)
		FROM #LogServerSetupActionTable T
			JOIN dbo.ServerSetup SS ON SS.ServerSetupID = T.ServerSetupID

		DROP TABLE #LogServerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogServerSetupAction

--Begin procedure person.GetTerritoryUpdateProjectsByPersonID
EXEC Utility.DropObject 'person.GetTerritoryUpdateProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.06
-- Description:	A stored procedure to get data from the person.PersonProject table
-- ===============================================================================
CREATE PROCEDURE person.GetTerritoryUpdateProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PRJ.ProjectID,
		PRJ.ProjectName,

		CASE
			WHEN PER.DefaultProjectID = PRJ.ProjectID
			THEN 1
			ELSE 0
		END AS IsDefaultProject

	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		JOIN person.Person PER ON PER.PersonID = WSGP.PersonID
		JOIN dropdown.Project PRJ ON PRJ.ProjectID = W.ProjectID
			AND W.EntityTypeCode = 'TerritoryUpdate'
			AND W.IsActive = 1
			AND WS.WorkflowStepNumber = 1
			AND WSGP.PersonID = @PersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				WHERE EWSGP.WorkflowID = W.WorkflowID
					AND EWSGP.ProjectID = W.ProjectID
					AND W.IsActive = 1
				)

	UNION

	SELECT
		PRJ.ProjectID,
		PRJ.ProjectName,

		CASE
			WHEN PER.DefaultProjectID = PRJ.ProjectID
			THEN 1
			ELSE 0
		END AS IsDefaultProject

	FROM territoryupdate.TerritoryUpdate TU
		JOIN workflow.EntityWorkflowStepGroupPerson EWSGP ON EWSGP.EntityID = TU.TerritoryUpdateID
			AND EWSGP.EntityTypeCode = 'TerritoryUpdate'
			AND EWSGP.ProjectID = TU.ProjectID
			AND EWSGP.IsComplete = 0
		JOIN person.PersonProject PP ON PP.ProjectID = EWSGP.ProjectID
			AND EWSGP.PersonID = @PersonID
			AND PP.PersonID = EWSGP.PersonID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber('TerritoryUpdate', TU.TerritoryUpdateID)
		JOIN person.Person PER ON PER.PersonID = PP.PersonID
		JOIN dropdown.Project PRJ ON PRJ.ProjectID = PP.ProjectID

	ORDER BY 2, 3
		
END
GO
--End procedure person.GetTerritoryUpdateProjectsByPersonID

--Begin procedure procurement.GetLicenseByLicenseID
EXEC Utility.DropObject 'procurement.GetLicenseByLicenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the procurement.License table
--
-- Author:			Todd Pires
-- Create date:	2016.04.07
-- Description:	Added LicenseProject support
-- ==========================================================================
CREATE PROCEDURE procurement.GetLicenseByLicenseID

@LicenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CountryID,
		C.CountryName,
		L.EndDate,
		dbo.FormatDate(L.EndDate) AS EndDateFormatted,
		L.LicenseID,
		L.LicenseNumber,
		L.StartDate,
		dbo.FormatDate(L.StartDate) AS StartDateFormatted
	FROM procurement.License L
		JOIN dropdown.Country C ON C.CountryID = L.CountryID
			AND L.LicenseID = @LicenseID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'License'
			AND DE.EntityID = @LicenseID

	SELECT
		P.ProjectID,
		P.ProjectName
	FROM procurement.LicenseProject LP
		JOIN dropdown.Project P ON P.ProjectID = LP.ProjectID
			AND LP.LicenseID = @LicenseID
	ORDER BY 2

END
GO
--End procedure procurement.GetLicenseByLicenseID

