USE BLLC
GO

--Begin table procurement.LicenseProject
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseProject'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.LicenseProject
	(
	LicenseProjectID INT NOT NULL IDENTITY(1,1),
	LicenseID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LicenseID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LicenseProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_LicenseProject', 'LicenseID,ProjectID'
GO
--End table procurement.LicenseProject