-- File Name:	Build-1.00 File 01 - BLLC.sql
-- Build Key:	Build-1.00 File 01 - BLLC - 2016.04.29 21.00.58

USE BLLC
GO

-- ==============================================================================================================================
-- Functions:
--		territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
--
-- Procedures:
--		dbo.GetMenuItemsByPersonID
--		eventlog.LogServerSetupAction
--		person.GetTerritoryUpdateProjectsByPersonID
--		procurement.GetLicenseByLicenseID
--
-- Tables:
--		procurement.LicenseProject
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE BLLC
GO

--Begin table procurement.LicenseProject
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseProject'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.LicenseProject
	(
	LicenseProjectID INT NOT NULL IDENTITY(1,1),
	LicenseID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LicenseID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LicenseProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_LicenseProject', 'LicenseID,ProjectID'
GO
--End table procurement.LicenseProject
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE BLLC
GO

--Begin function territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2016.03.31
-- Description:	A function to get the name of a parent territory
-- =============================================================

CREATE FUNCTION territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @ParentTerritoryID INT
	DECLARE @ParentTerritoryTypeCode VARCHAR(50)

	IF @TerritoryTypeCode = 'Community'
		BEGIN
	
		SELECT @ParentTerritoryTypeCode = PTT.ParentTerritoryTypeCode 
		FROM territory.ProjectTerritoryType PTT
			JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
				AND PTT.ProjectID = (SELECT T.ProjectID FROM territory.Community T WHERE T.CommunityID = @TerritoryID)
				AND TT.TerritoryTypeCode = 'Community'
	
		IF @ParentTerritoryTypeCode = 'Governorate'
			BEGIN
	
			SELECT @ParentTerritoryID = T.ParentTerritoryID
			FROM territory.Community T 
			WHERE T.CommunityID = @TerritoryID
	
			END
		--ENDIF
	
		END
	--ENDIF
	
	RETURN territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ParentTerritoryTypeCode, @ParentTerritoryID)

END
GO
--End function territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE BLLC
GO

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure eventlog.LogServerSetupAction
EXEC utility.DropObject 'eventlog.LogServerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogServerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogServerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogServerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogServerSetupActionTable
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupID = @EntityID
		
		ALTER TABLE #LogServerSetupActionTable DROP COLUMN ServerSetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ServerSetup'), ELEMENTS
			)
		FROM #LogServerSetupActionTable T
			JOIN dbo.ServerSetup SS ON SS.ServerSetupID = T.ServerSetupID

		DROP TABLE #LogServerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogServerSetupAction

--Begin procedure person.GetTerritoryUpdateProjectsByPersonID
EXEC Utility.DropObject 'person.GetTerritoryUpdateProjectsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.06
-- Description:	A stored procedure to get data from the person.PersonProject table
-- ===============================================================================
CREATE PROCEDURE person.GetTerritoryUpdateProjectsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PRJ.ProjectID,
		PRJ.ProjectName,

		CASE
			WHEN PER.DefaultProjectID = PRJ.ProjectID
			THEN 1
			ELSE 0
		END AS IsDefaultProject

	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		JOIN person.Person PER ON PER.PersonID = WSGP.PersonID
		JOIN dropdown.Project PRJ ON PRJ.ProjectID = W.ProjectID
			AND W.EntityTypeCode = 'TerritoryUpdate'
			AND W.IsActive = 1
			AND WS.WorkflowStepNumber = 1
			AND WSGP.PersonID = @PersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				WHERE EWSGP.WorkflowID = W.WorkflowID
					AND EWSGP.ProjectID = W.ProjectID
					AND W.IsActive = 1
				)

	UNION

	SELECT
		PRJ.ProjectID,
		PRJ.ProjectName,

		CASE
			WHEN PER.DefaultProjectID = PRJ.ProjectID
			THEN 1
			ELSE 0
		END AS IsDefaultProject

	FROM territoryupdate.TerritoryUpdate TU
		JOIN workflow.EntityWorkflowStepGroupPerson EWSGP ON EWSGP.EntityID = TU.TerritoryUpdateID
			AND EWSGP.EntityTypeCode = 'TerritoryUpdate'
			AND EWSGP.ProjectID = TU.ProjectID
			AND EWSGP.IsComplete = 0
		JOIN person.PersonProject PP ON PP.ProjectID = EWSGP.ProjectID
			AND EWSGP.PersonID = @PersonID
			AND PP.PersonID = EWSGP.PersonID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber('TerritoryUpdate', TU.TerritoryUpdateID)
		JOIN person.Person PER ON PER.PersonID = PP.PersonID
		JOIN dropdown.Project PRJ ON PRJ.ProjectID = PP.ProjectID

	ORDER BY 2, 3
		
END
GO
--End procedure person.GetTerritoryUpdateProjectsByPersonID

--Begin procedure procurement.GetLicenseByLicenseID
EXEC Utility.DropObject 'procurement.GetLicenseByLicenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the procurement.License table
--
-- Author:			Todd Pires
-- Create date:	2016.04.07
-- Description:	Added LicenseProject support
-- ==========================================================================
CREATE PROCEDURE procurement.GetLicenseByLicenseID

@LicenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CountryID,
		C.CountryName,
		L.EndDate,
		dbo.FormatDate(L.EndDate) AS EndDateFormatted,
		L.LicenseID,
		L.LicenseNumber,
		L.StartDate,
		dbo.FormatDate(L.StartDate) AS StartDateFormatted
	FROM procurement.License L
		JOIN dropdown.Country C ON C.CountryID = L.CountryID
			AND L.LicenseID = @LicenseID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'License'
			AND DE.EntityID = @LicenseID

	SELECT
		P.ProjectID,
		P.ProjectName
	FROM procurement.LicenseProject LP
		JOIN dropdown.Project P ON P.ProjectID = LP.ProjectID
			AND LP.LicenseID = @LicenseID
	ORDER BY 2

END
GO
--End procedure procurement.GetLicenseByLicenseID


--End file Build File - 03 - Procedures.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Activity', 'Activity Management', 0
GO
EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Contacts', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'ForceAssets', 'Groups & Assets', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'ProductDistributor', 'Product Distribution', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Research', 0
GO
EXEC utility.SavePermissionableGroup 'Surveys', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territories', 'Territories', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'Activity', 'AddUpdate', NULL, 'Activity.AddUpdate', 'Add / edit an activity', 0, 0, 'Activity'
GO
EXEC utility.SavePermissionable 'Activity', 'List', NULL, 'Activity.List', 'View the list of activities', 0, 0, 'Activity'
GO
EXEC utility.SavePermissionable 'Activity', 'View', NULL, 'Activity.View', 'View an activity', 0, 0, 'Activity'
GO
EXEC utility.SavePermissionable 'Activity', 'View', 'ViewBudget', 'Activity.View.ViewBudget', 'View an activity budget', 0, 0, 'Activity'
GO
EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Allow access to the various export modules', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the list of email templates', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log entry', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a permissionable', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'View the list of permissionable templates', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'View the list of system users', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'View the list of risks', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export the list of risks', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View a risk', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the list of server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'PortalItemList', NULL, 'ServerSetup.PortalItemList', 'Enable / disable donor portal access to system modules', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Add / edit a contact', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'UpdateVetting', 'Contact.List.UpdateVetting', 'Add vetting data to a contact record', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'Show the more info button on the vetting history data table', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Subcontractor', 'AddUpdate', NULL, 'Subcontractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Subcontractor', 'AddUpdateCapability', NULL, 'Subcontractor.AddUpdateCapability', 'Add / edit a sub-contractor capability', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Subcontractor', 'List', NULL, 'Subcontractor.List', 'View the list of sub-contractors', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Subcontractor', 'View', NULL, 'Subcontractor.View', 'View a sub-contractor', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', NULL, 'Vetting.List', 'View the list of contacts in the vetting process', 0, 0, 'Contacts'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', 'Add / edit a document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'AdditionalReportsandDeliverables', 'Document.View.AdditionalReportsandDeliverables', 'View documents of type additional reports and deliverables in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'AgreementsandMods', 'Document.View.AgreementsandMods', 'View documents of type agreements and mods in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'AnnualReports', 'Document.View.AnnualReports', 'View documents of type annual reports in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'ContactStipendPaymentReconcilliation', 'Document.View.ContactStipendPaymentReconcilliation', 'View documents of type contact stipend payment reconcilliation in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'CriticalAssessment', 'Document.View.CriticalAssessment', 'View documents of type critical assessment in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'DailyReport', 'Document.View.DailyReport', 'View documents of type daily report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Finance', 'Document.View.Finance', 'View documents of type finance in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'FortnightlyProduct', 'Document.View.FortnightlyProduct', 'View documents of type fortnightly product in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Legal&Policy', 'Document.View.Legal&Policy', 'View documents of type legal & policy in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Log-Frame', 'Document.View.Log-Frame', 'View documents of type log-frame in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'M&EPlan', 'Document.View.M&EPlan', 'View documents of type m&e plan in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'M&EReporting', 'Document.View.M&EReporting', 'View documents of type m&e reporting in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'MeetingNotes', 'Document.View.MeetingNotes', 'View documents of type meeting notes in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'MeetingReport', 'Document.View.MeetingReport', 'View documents of type meeting report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'OtherDocument', 'Document.View.OtherDocument', 'View documents of type other document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Participant', 'Document.View.Participant', 'View documents of type participant in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'ParticipantsDocument', 'Document.View.ParticipantsDocument', 'View documents of type participants document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Plan', 'Document.View.Plan', 'View documents of type plan in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Policy', 'Document.View.Policy', 'View documents of type policy in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Presentations', 'Document.View.Presentations', 'View documents of type presentations in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Procurement', 'Document.View.Procurement', 'View documents of type procurement in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'ProjectDocuments', 'Document.View.ProjectDocuments', 'View documents of type project documents in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'QualityAssuranceFeedback', 'Document.View.QualityAssuranceFeedback', 'View documents of type InCoStrat feedback in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'QualityAssuranceFeedbackDocument', 'Document.View.QualityAssuranceFeedbackDocument', 'View documents of type InCoStrat feedback document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Quarterly', 'Document.View.Quarterly', 'View documents of type quarterly in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'QuarterlyReports', 'Document.View.QuarterlyReports', 'View documents of type quarterly reports in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Reporting', 'Document.View.Reporting', 'View documents of type reporting in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'RFIResponse', 'Document.View.RFIResponse', 'View documents of type rfi response in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'RFPforProject', 'Document.View.RFPforProject', 'View documents of type rfp for project in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'RiskInformation', 'Document.View.RiskInformation', 'View documents of type risk information in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'SOPs', 'Document.View.SOPs', 'View documents of type sops in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Spot', 'Document.View.Spot', 'View documents of type spot in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'SpotReport', 'Document.View.SpotReport', 'View documents of type spot report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Staff�International', 'Document.View.Staff�International', 'View documents of type staff � international  in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Staff�Local', 'Document.View.Staff�Local', 'View documents of type staff � local in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Stakeholders', 'Document.View.Stakeholders', 'View documents of type stakeholders in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Strategy', 'Document.View.Strategy', 'View documents of type strategy in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'TechnicalProposalandBudget', 'Document.View.TechnicalProposalandBudget', 'View documents of type technical proposal and budget in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Trainer', 'Document.View.Trainer', 'View documents of type trainer in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Trainer1Document', 'Document.View.Trainer1Document', 'View documents of type trainer 1 document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'Trainer2Document', 'Document.View.Trainer2Document', 'View documents of type trainer 2 document in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'TrainingReport', 'Document.View.TrainingReport', 'View documents of type training report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'TripReport', 'Document.View.TripReport', 'View documents of type trip report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WeeklyAtmospheric', 'Document.View.WeeklyAtmospheric', 'View documents of type weekly atmospheric in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WeeklyAtmosphericReport', 'Document.View.WeeklyAtmosphericReport', 'View documents of type weekly atmospheric report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WeeklyAtmosphericSummaryReport', 'Document.View.WeeklyAtmosphericSummaryReport', 'View documents of type weekly atmospheric summary report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WeeklyInformationReport', 'Document.View.WeeklyInformationReport', 'View documents of type weekly information report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WeeklyProgramReport', 'Document.View.WeeklyProgramReport', 'View documents of type weekly program report in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', 'WorkPlansandBudgets', 'Document.View.WorkPlansandBudgets', 'View documents of type work plans and budgets in the library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DistributedEquipment', 'List', NULL, 'DistributedEquipment.List', 'View the list of distributed equipment', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'DistributedEquipment', 'List', 'Audit', 'DistributedEquipment.List.Audit', 'Audit distributed equipment', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'DistributedEquipment', 'List', 'SetDeliveryDate', 'DistributedEquipment.List.SetDeliveryDate', 'Update the date delivered to end user of distributed equipment', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'DistributedEquipment', 'List', 'Transfer', 'DistributedEquipment.List.Transfer', 'Transfer distributed equipment between end users', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit an equipment catalog item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'View the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View an equipment catalog item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Add', NULL, 'EquipmentDistribution.Add', 'Add an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'List', NULL, 'EquipmentDistribution.List', 'View the list of equipment distributions', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Update', NULL, 'EquipmentDistribution.Update', 'Update an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit an equipment inventory item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View an equipment inventory item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'View the list of licenses', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View a license', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit a license equipment catalog item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'View the list of licensed equipment catalog items', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View a license equipment catalog item', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'Asset', 'AddUpdate', NULL, 'Asset.AddUpdate', 'Add / edit an asset', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'View the list of forces', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force', 0, 0, 'ForceAssets'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'View the list of findings', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View a finding', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'View the list of indicators', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicator type', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'View the list of indicator types', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'View the list of milestones', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'View the objectives overview', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'View the list of objectives', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage objectives', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'View objective overview', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'View the list of recommendations', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View a recommendation', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'View the list of incident reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'AddUpdate', NULL, 'RequestForInformation.AddUpdate', 'Add / edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Amend', NULL, 'RequestForInformation.Amend', 'Edit a completed request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'View the list of requests for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'View the list of spot reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'View the list of communities', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View a community', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'TerritoryUpdate', 'AddUpdate', NULL, 'TerritoryUpdate.AddUpdate', 'Update Territories', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Module', 'AddUpdate', NULL, 'Module.AddUpdate', 'Add / edit a module', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Module', 'List', NULL, 'Module.List', 'View the list of modules', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Module', 'View', NULL, 'Module.View', 'View a module', 0, 0, 'Training'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.00 File 01 - BLLC - 2016.04.29 21.00.58')
GO
--End build tracking

