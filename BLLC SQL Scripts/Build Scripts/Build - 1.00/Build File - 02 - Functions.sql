USE BLLC
GO

--Begin function territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2016.03.31
-- Description:	A function to get the name of a parent territory
-- =============================================================

CREATE FUNCTION territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @ParentTerritoryID INT
	DECLARE @ParentTerritoryTypeCode VARCHAR(50)

	IF @TerritoryTypeCode = 'Community'
		BEGIN
	
		SELECT @ParentTerritoryTypeCode = PTT.ParentTerritoryTypeCode 
		FROM territory.ProjectTerritoryType PTT
			JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
				AND PTT.ProjectID = (SELECT T.ProjectID FROM territory.Community T WHERE T.CommunityID = @TerritoryID)
				AND TT.TerritoryTypeCode = 'Community'
	
		IF @ParentTerritoryTypeCode = 'Governorate'
			BEGIN
	
			SELECT @ParentTerritoryID = T.ParentTerritoryID
			FROM territory.Community T 
			WHERE T.CommunityID = @TerritoryID
	
			END
		--ENDIF
	
		END
	--ENDIF
	
	RETURN territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ParentTerritoryTypeCode, @ParentTerritoryID)

END
GO
--End function territory.GetParentTerritoryNameByTerritoryTypeCodeAndTerritoryID
