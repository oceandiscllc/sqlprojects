USE MNH
GO

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC Utility.DropObject 'contact.TR_Contact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.13
-- Description:	A trigger to update the contact.Contact table
-- ==========================================================
CREATE TRIGGER contact.TR_Contact ON contact.Contact AFTER INSERT, UPDATE
AS
BEGIN

	SET ARITHABORT ON;

	UPDATE C
	SET C.OtherContactFunction = CASE WHEN CF.ContactFunctionCode = 'OTH' THEN C.OtherContactFunction ELSE NULL END
	FROM contact.Contact C
		JOIN INSERTED I ON I.ContactID = C.ContactID
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID

	UPDATE C
	SET C.OtherContactRole = CASE WHEN CF.ContactRoleCode = 'OTH' THEN C.OtherContactRole ELSE NULL END
	FROM contact.Contact C
		JOIN INSERTED I ON I.ContactID = C.ContactID
		JOIN dropdown.ContactRole CF ON CF.ContactRoleID = C.ContactRoleID

END
GO		
--End table contact.Contact

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.AddColumn @TableName, 'DocumentEntityCode', 'VARCHAR(50)'
GO
--End table document.DocumentEntity

--Begin table dropdown.AssessmentEvidenceType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssessmentEvidenceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssessmentEvidenceType
	(
	AssessmentEvidenceTypeID INT IDENTITY(1,1) NOT NULL,
	AssessmentEvidenceTypeCode VARCHAR(50),
	AssessmentEvidenceTypeDataType VARCHAR(50),
	AssessmentEvidenceTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssessmentEvidenceTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssessmentEvidenceType', 'DisplayOrder,AssessmentEvidenceTypeName', 'AssessmentEvidenceTypeID'
GO
--End table dropdown.AssessmentEvidenceType

--Begin table dropdown.QualityStandardAssessmentEvidenceType
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardAssessmentEvidenceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandardAssessmentEvidenceType
	(
	QualityStandardAssessmentEvidenceTypeID INT IDENTITY(1,1) NOT NULL,
	QualityStandardID INT,
	AssessmentEvidenceTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssessmentEvidenceTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QualityStandardAssessmentEvidenceTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_QualityStandardAssessmentEvidenceType', 'QualityStandardID,AssessmentEvidenceTypeID'
GO
--End table dropdown.AssessmentEvidenceType

--Begin table dropdown.QualityStandardLookup
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardLookup'

EXEC utility.AddColumn @TableName, 'VerificationCriteriaID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'VerificationCriteriaName', 'VARCHAR(500)'
GO
--End table dropdown.QualityStandardLookup

--Begin table dropdown.QualityStandard
EXEC utility.DropObject 'dropdown.TR_QualityStandard'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.09
-- Description:	A trigger to populate the dropdown.QualityStandardLookup table
-- ===========================================================================
CREATE TRIGGER dropdown.TR_QualityStandard ON dropdown.QualityStandard AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	TRUNCATE TABLE dropdown.QualityStandardLookup

	;
	WITH HD (QualityStandardID,ParentQualityStandardID,EntityTypeCode,NodeLevel)
		AS 
		(
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID,
			T.EntityTypeCode, 
			1 
		FROM dropdown.QualityStandard T
		WHERE T.ParentQualityStandardID = 0
			AND T.QualityStandardID > 0
		
		UNION ALL
			
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID, 
			T.EntityTypeCode, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dropdown.QualityStandard T 
			JOIN HD ON HD.QualityStandardID = T.ParentQualityStandardID 
		)
		
	INSERT INTO dropdown.QualityStandardLookup
		(EntityTypeCode, QualityStandardID, ManualID, ChapterID, StandardID, ObjectiveElementID, VerificationCriteriaID)
	SELECT
		A.EntityTypeCode, 
		A.QualityStandardID,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(E.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 1
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END

	FROM HD A
		LEFT JOIN HD B ON B.QualityStandardID = A.ParentQualityStandardID
		LEFT JOIN HD C ON C.QualityStandardID = B.ParentQualityStandardID
		LEFT JOIN HD D ON D.QualityStandardID = C.ParentQualityStandardID
		LEFT JOIN HD E ON E.QualityStandardID = D.ParentQualityStandardID

	UPDATE QSL
	SET
		QSL.ManualName = QSM.EntityName,
		QSL.ChapterName = QSC.EntityName,
		QSL.StandardName = QSS.EntityName,
		QSL.ObjectiveElementName = QSOE.EntityName,
		QSL.VerificationCriteriaName = QSVC.EntityName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QSM ON QSM.QualityStandardID = QSL.ManualID
		JOIN dropdown.QualityStandard QSC ON QSC.QualityStandardID = QSL.ChapterID
		JOIN dropdown.QualityStandard QSS ON QSS.QualityStandardID = QSL.StandardID
		JOIN dropdown.QualityStandard QSOE ON QSOE.QualityStandardID = QSL.ObjectiveElementID
		JOIN dropdown.QualityStandard QSVC ON QSVC.QualityStandardID = QSL.VerificationCriteriaID

	END
--ENDIF
GO

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandard
	(
	VisitQualityStandardID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	QualityStandardID INT,
	AssessmentDate DATE,
	HasDocumentation BIT,
	HasImplementation BIT,
	HasObservation BIT,
	HasPhysicalVerification BIT,
	HasProviderInterview BIT,
	TotalRecordsChecked INT,
	CorrectRecords INT,
	Score INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssessmentDate', 'DATE', 'GetDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CorrectRecords', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasDocumentation', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasImplementation', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasObservation', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasPhysicalVerification', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasProviderInterview', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Score', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TotalRecordsChecked', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardID'
EXEC utility.SetIndexClustered @TableName, 'IX_VisitQualityStandard', 'VisitID,QualityStandardID'
GO
--End table visit.VisitQualityStandard

--Begin table visit.VisitQualityStandardAssessment
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessment'

EXEC utility.DropObject @TableName
GO
--End table visit.VisitQualityStandardAssessment

--Begin table visit.VisitQualityStandardScoreChangeReason
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardScoreChangeReason'

EXEC utility.DropObject 'visit.VisitQualityStandardAssessmentScoreChangeReason'
EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandardScoreChangeReason
	(
	VisitQualityStandardScoreChangeReasonID INT IDENTITY(1,1) NOT NULL,
	VisitQualityStandardID INT,
	ScoreChangeReasonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ScoreChangeReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitQualityStandardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardScoreChangeReasonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitQualityStandardScoreChangeReason', 'VisitQualityStandardID,ScoreChangeReasonID'
GO
--End table visit.VisitQualityStandardScoreChangeReason
