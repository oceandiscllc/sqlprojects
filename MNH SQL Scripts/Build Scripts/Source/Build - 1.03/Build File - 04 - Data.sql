USE MNH
GO

--Begin table dropdown.AssessmentEvidenceType
TRUNCATE TABLE dropdown.AssessmentEvidenceType
GO

EXEC utility.InsertIdentityValue 'dropdown.AssessmentEvidenceType', 'AssessmentEvidenceTypeID', 0
GO

INSERT INTO dropdown.AssessmentEvidenceType 
	(AssessmentEvidenceTypeCode, AssessmentEvidenceTypeName, AssessmentEvidenceTypeDataType, DisplayOrder)
VALUES
	('CorrectRecords', 'Correct Records', 'INT', 3),
	('HasDocumentation', 'Documentation', 'BIT',  6),
	('HasImplementation', 'Implementation', 'BIT', 7),
	('HasProviderInterview', 'Provider Interview', 'BIT', 4),
	('HasObservation', 'Observation', 'BIT', 1),
	('TotalRecordsChecked', 'Total Records Checked', 'INT', 2),
	('HasPhysicalVerification', 'Physical Verification', 'BIT', 5)
GO
--End table dropdown.AssessmentEvidenceType

--Begin table dropdown.ContactFunction
UPDATE CF
SET CF.ContactFunctionName = 'Other - Please Specify'
FROM dropdown.ContactFunction CF
WHERE CF.ContactFunctionName LIKE 'Other%'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
UPDATE CR
SET CR.ContactRoleName = 'Other - Please Specify'
FROM dropdown.ContactRole CR
WHERE CR.ContactRoleName LIKE 'Other%'
GO
--End table dropdown.ContactRole

--Begin table dropdown.QualityStandard
ALTER TABLE dropdown.QualityStandard DISABLE TRIGGER TR_QualityStandard
GO

TRUNCATE TABLE dropdown.QualityStandard
GO

EXEC utility.InsertIdentityValue 'dropdown.QualityStandard', 'QualityStandardID', 0
GO

DECLARE @cEntityTypeCode VARCHAR(50)
DECLARE @nParentQualityStandardID INT

INSERT INTO dropdown.QualityStandard (ParentQualityStandardID, EntityTypeCode, EntityName) VALUES (0, 'Manual', 'FOGSI')

SET @cEntityTypeCode = 'Chapter'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Manual' AND QS.EntityName = 'FOGSI'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, 'Antenatal Care', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, 'At Admission', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, 'At Delivery', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, 'Beyond Delivery', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, 'Post Natal Care Standard', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, 'C-Section Standard', 6)

SET @cEntityTypeCode = 'Standard'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Antenatal Care'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1 Provider screens for key clinical conditions that may lead to complications during pregnancy', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'At Admission'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '2 Provider prepares for safe care during delivery (to be checked every day)', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '3 Provider assesses all pregnant women at admission', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '4 Providers conducts PV examination appropriately', 3 ),
	(@nParentQualityStandardID, @cEntityTypeCode, '5 Provider monitors the progress of labor appropriately', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '6 Provider ensures respectful and supportive care', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'At Delivery'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7 Provider assists the woman to have a safe and clean birth', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8 Provider conducts a rapid initial assessment and performs immediate newborn care (if baby cried immediately)', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '9 Provider performs Active Management of Third Stage of Labor (AMTSL)', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '10 Provider identifies and manages Post-Partum Hemorrhage (PPH)', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '11 Provider identifies and manages severe Pre-eclampsia/Eclampsia (PE/E)', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '12 Provider performs newborn resuscitation if baby does not cry immediately after birth', 6),
	(@nParentQualityStandardID, @cEntityTypeCode, '13 Provider ensures care of newborn with small size at birth', 7)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Beyond Delivery'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14 The facility adheres to universal infection prevention protocols', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Post Natal Care Standard'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15 Provider ensures adequate postpartum care package is offered to the mother and - at discharge', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'C-Section Standard'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '16 Provider reviews clinical practices related to C-section at regular intervals', 1)

SET @cEntityTypeCode = 'Objective Element'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.1 Screens for anaemia', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.2 Screens for hypertensive disorders of pregnancy', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.3 Screens for DM', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.4 Screens for HIV', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.5 Screens for syphilis', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.6 Screens for malaria', 6),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.7 Establishes blood group and Rh type during first ANC visit', 7)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '2.1 Ensures sterile/ HLD delivery tray is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '2.2 Ensures functional items for newborn care and resuscitation', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '3.1 Takes obstetric, medical and surgical history', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.2 Assesses gestational age correctly', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.3 Records fetal heart rate', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.4 Records mother''s BP and temperature', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '4.1 Conducts PV examination as per indication', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2 Conducts PV examination following infection prevention practices and records findings', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '5.1 Undertakes timely assessment of cervical dilatation and descent to monitor the progress of labor', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '5.2 Interprets partograph (condition of mother and fetus and progress of labor) correctly and adjusts care according to findings', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 6
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '6.1 Encourages and welcomes the presence of a birth companion during labor', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '6.2 Treats pregnant woman and her companion cordially and respectfully (RMC), ensures privacy and confidentiality for pregnant woman during her stay', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '6.3 Explains danger signs and important care activities to mother and her companion', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 7
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1 Provider ensures six ''cleans'' while conducting delivery', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.2 Performs episiotomy only when indicated with the use of appropriate local anesthetic', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.3 Provider allows spontaneous delivery of head by flexing it and giving perineal support; manages cord round the neck; assists delivery of shoulders and body', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 8
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.1 Delivers the baby on mother''s abdomen', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.2 Ensures immediate drying, and asses breathing', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.3 Performs delayed cord clamping and cutting', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.4 Ensures early initiation of breastfeeding', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.5 Assesses the newborn for any congenital anomalies', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.6 Weighs the baby and administers Vitamin K', 6)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 9
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1 Performs AMTSL and examines placenta thoroughly', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 10
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.1 Assesses uterine tone and bleeding per vaginum regularly after delivery', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.2 Identifies shock', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3 Manages shock', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4 Manages atonic PPH', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.5 Manages PPH due to retained placenta/ placental bits', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 11
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.1 Identifies mothers with severe PE/E', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.2 Gives correct regimen of Inj. MgSO4 for prevention and management of convulsions', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.3 Facilitates prescription of antihypertensives', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.4 Ensures specialist attention for care of mother and newborn', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.5 Performs nursing care', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 12
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1 Performs steps for resuscitation within first 30 seconds', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.2 Initiates bag and mask ventilation for 30 seconds if baby still not breathing', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3 Takes appropriate action if baby doesn''t respond to ambu bag ventilation after golden minute', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.4 Performs advanced resuscitation in babies not responding to basic resuscitation when chest is rising and heart rate is < 60 per minute', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 13
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '13.1 Facilitate specialist care in newborn weighing <1800 gm', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.2 Facilitates assisted feeding whenever required', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.3 Facilitates thermal management including kangaroo mother care (KMC)', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 14
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14.1 Instruments and re-usable items are adequately and appropriately processed after each use', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.2 Biomedical waste is segregated and disposed of as per the guidelines', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.3 Performs hand hygiene before and after each procedure, and sterile gloves are worn during delivery and internal examination)', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 15
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.1 Conducts proper physical examination of mother and newborn during postpartum visits', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2 Identifies and appropriately manages maternal and neonatal sepsis', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.3', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.4 Counsels on importance of exclusive breast feeding', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.5 Counsels on danger signs, postpartum family planning', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(QS.EntityName, 2) AS INT) = 16
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '16.1 Ensures classification as per Robson''s criteria and reviews indications and complications of C-section at regular intervals', 5)

SET @cEntityTypeCode = 'Verification Criteria'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.1.1 Estimates Hb at each scheduled ANC visit', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.2.1 Functional BP instrument and stethoscope at point of use is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.2.2 Records BP at each ANC visit', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.2.3 Performs proteinuria testing during each scheduled ANC visit', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.3.1 Uses/Refers for standard 75gm OGTT for screening of GDM at first ANC visit and repeats OGTT test at second ANC visit (24-28 weeks) if negative in first screening', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.4.1 Screens/refers for HIV during first ANC visit in all cases, and in fourth ANC visit in high risk cases', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.5.1 Screens/refers for syphilis in first ANC visit in all cases, and in fourth ANC visit in high risk cases', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.6
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.6.1 Screens for malaria (only in endemic areas)', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 1.7
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.7.1 Establishes blood group and Rh type during first ANC visit', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 2.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '2.1.1 Ensure availability of uterotonic agents - IM/IV oxytocin (preferred), misoprostol', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 2.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '2.2.1 Designated newborn corner is present', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '2.2.2 Ensures functional items for newborn care and resuscitation', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '2.2.3 Switches radiant warmer ''on'' 30 min. before child birth', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 3.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '3.1.1 Takes obstetric, medical and surgical history', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 3.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES
	(@nParentQualityStandardID, @cEntityTypeCode, '3.2.1 Assesses gestational age through either LMP or Fundal height or USG (previous or present is available) ', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 3.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES
	(@nParentQualityStandardID, @cEntityTypeCode, '3.3.1 Functional Doppler/fetoscope/stethoscope at point of use is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.3.2 Records FHR', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 3.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES
	(@nParentQualityStandardID, @cEntityTypeCode, '3.4.1 Functional BP instrument and stethoscope and functional thermometer at point of use is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.4.2 Records BP and temperature', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 4.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '4.1.1 Conducts PV examination only as indicated (4 hourly or based on clinical indication)', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 4.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2.1 Soap, running water, antiseptic solution, sterile gauze/ pad is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2.2 Performs hand hygiene (washes hands and wears sterile gloves on both the hands with correct technique)', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2.3 Cleans the perineum appropriately before conducting PV examination', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2.4 Alert specialist/doctor if liquor is meconium stained', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2.5 Records findings of PV examination', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 5.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '5.1.1 Partographs are available in labor room', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '5.1.2 Initiates Partograph plotting once the cervical dilation is >=4 cms', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 5.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '5.2.1 If parameters are not normal, identifies complications, records the diagnosis and makes appropriate adjustments in the birth plan', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 6.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '6.1.1 Encourages and welcomes the presence of birth companion during labor', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 6.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '6.2.1 There are provisions for privacy in LR (curtains / partition between tables and non-see through windows', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '6.2.2 Treats pregnant woman and her companion cordially and respectfully', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 7.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1.1 Sterile gloves are available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1.2 Antiseptic solution (Betadine/ Savlon) is available', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1.3 Sterile cord clamp is available', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1.4 Sterile cutting edge (blade/scissors) is available', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 7.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7.2.1 Performs an episiotomy only if indicated and uses local anesthesia', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 7.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7.3.1 Allows spontaneous delivery of head by maintaining flexion and giving perineal support; manages cord round the neck; assists delivery of shoulders and body', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.1.1 Two towels at normal room temperature or pre warmed to room temperature', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.1.2 Delivers the baby on mother''s abdomen ', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.2.1 If breathing is normal, dries the baby immediately and wraps in second warm towel ', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.3.1 Performs delayed cord clamping and cutting unless medical indication otherwise', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.4.1 Initiates breast feeding within one hour of birth ', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.5.1 Provider immediately assess the newborn for any congenital anomalies', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.5.2 Provider ensures specialist care if required', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 8.6
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.6.1 Baby weighing scale is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.6.2 Vitamin K injection is available', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.6.3 Weighs the baby and administers Vitamin K', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 3) AS NUMERIC(18,1)) = 9.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1.1 Palpates mother''s abdomen to rule out second baby', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1.2 Administers uterotonic. Preferred is Inj. Oxytocin 10 I.U. IM/IV within one minute of delivery of baby (use Misoprostol 600 micrograms if oxytocin is not available)', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1.3 Performs controlled cord traction (CCT) during contraction', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1.4 Performs uterine massage', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1.5 Checks placenta and membranes for completeness before discarding', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 10.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.1.1 Assesses uterine tone and bleeding per vaginum regularly', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 10.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.2.1 Identifies shock by signs and symptoms (pulse > 110 per minute, systolic BP < 90 mmHg, cold clammy skin, respiratory rate > 30 per minute, altered sensorium and scanty urine output < 30 ml per hour)', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 10.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3.1 Ensures availability of wide bore cannulas (No. 14/16), IV infusion sets and fluids and containers for collection of blood for haemoglobin, blood grouping and cross matching', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3.2 Shouts for help, follows ABC approach, monitors vitals, elevates the foot end and keeps the woman warm', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3.3 Starts IV infusions, collects blood for Hb and grouping and cross matching, catheterizes the bladder and monitors I/O, gives oxygen at the rate of 6-8 liters per minute', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3.4 Identifies cause specific PPH', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 10.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4.1 Initiates 20 IU oxytocin drip in 1000 ml of ringer lactate/normal saline at the rate of 40-60 drops per minute', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4.2 Continues uterine massage', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4.3 If uterus is still relaxed, gives other uterotonics as recommended', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4.4 If uterus is still relaxed, performs mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.4.5 If uterus is still relaxed, refers to higher centers while continuing mechanical compression', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 10.5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.5.1 Identifies retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.5.2 Initiates 20 IU oxytocin drip in 1000 ml of ringer lactate/normal saline at the rate of 40-60 drops per minut', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.5.3 Refers to higher center if unable to manage', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.5.4 Performs manual removal of placenta (MRP)', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 11.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.1.1 Dipsticks for proteinuria testing in labor room are available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.1.2 Records BP at admission', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.1.3 Identifies danger signs or presence of convulsions', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 11.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.2.1 MgSO4 in labour room (at least 20 ampoules) is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.2.2 Inj. MgSO4 is appropriately administered', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 11.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.3.1 Antihypertensive are available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.3.2 Facilitates prescription of anti-hypertensives', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 11.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.4.1 Ensures specialist attention for care of mother and newborn', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 11.5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.5.1 Performs nursing care', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 12.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1.1 Suction equipment/mucus extractor, shoulder roll is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1.2 Considers endotracheal intubation and tracheal suctioning if available. If not available, performs oro-pharyngeal suction and proceeds with next steps', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1.3 Performs following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby, immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1.4 Performs following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR)', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 12.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.2.1 Functional ambu bag with mask (size 0 and 1) is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.2.2 Initiates bag and mask ventilation for 30 seconds if baby still not breathing', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 12.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3.1 Functional oxygen cylinder (with wrench) and new born mask is available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3.2 Assesses breathing, if baby still not breathing, continues bag and mask ventilation; starts oxygen', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3.3 Checks heart rate/cord pulsations', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3.4 Calls for advance help/arranges referral', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 12.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.4.1 Performs chest compressions at the rate of 3 compressions to 1 breath till the heart rate is > 60 beats/minute', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.4.2 If heart rate persists to be undetectable or < 60 beats/ minute, administers epinephrine (1:10000), 0.1 - 0.3 ml/kg IV', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 13.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '13.1.1 Facilitates specialist care in newborn <1800 gm (refer to FBNC/seen by pediatrician)', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 13.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '13.2.1 Facilitates assisted feeding whenever required', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 13.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '13.3.1 Facilitates thermal management including KMC', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 14.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14.1.1 Facilities for sterilization of instruments are available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.1.2 Instruments are sterilized after each use', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.1.3 Delivery environment such as labor table, contaminated surfaces and floors are cleaned after each delivery', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 14.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14.2.1 Color coded bags for disposal of biomedical waste are available', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.2.2 Biomedical waste is segregated and disposed of as per the guidelines', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 14.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14.3.1 Performs hand hygiene before and after each procedure, and sterile gloves are worn during delivery and internal examination', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 15.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.1.1 Conducts mother''s examination: breast, perineum for inflammation; status of episiotomy/tear suture; lochia; calf tenderness/redness/swelling; abdomen for involution of uterus, tenderness or distension', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.1.2 Conducts newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 15.2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2.1 Checks mother''s history related to maternal infection', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2.2 Checks mother''s temperature', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2.3 Gives correct regimen of antibiotics', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2.4 Checks baby''s temperature and other looks for other signs of infections', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2.5 Gives correct regime of antibiotics/refers for specialist care', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 15.3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.3.1 Provides emotional support and refers woman to specialist care', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 15.4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.4.1 Provides counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 15.5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.5.1 Counsels on return of fertility and healthy timing and spacing of pregnancy - Counsels on postpartum family planning to mother at discharge', 1)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Objective Element' AND CAST(LEFT(QS.EntityName, 4) AS NUMERIC(18,1)) = 16.1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '16.1.1 Ensures that all C-section cases are classified as per the modified Robson''s criteria and rates of different categories are monitored in facility', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.1.2 Reviews C-section cases through a clinical audit once every quarter in facility', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.1.3 Ensures that rate of complications of C-sections are periodically monitored in facility', 3)

INSERT INTO dropdown.QualityStandard (ParentQualityStandardID, EntityTypeCode, EntityName) VALUES (0, 'Manual', 'NABH')

SET @cEntityTypeCode = 'Chapter'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Manual' AND QS.EntityName = 'NABH'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Chapter','Chapter-1', 1),
	(@nParentQualityStandardID, 'Chapter','Chapter-2', 2),
	(@nParentQualityStandardID, 'Chapter','Chapter-3', 3),
	(@nParentQualityStandardID, 'Chapter','Chapter-4', 4),
	(@nParentQualityStandardID, 'Chapter','Chapter-5', 5),
	(@nParentQualityStandardID, 'Chapter','Chapter-6', 6),
	(@nParentQualityStandardID, 'Chapter','Chapter-7', 7),
	(@nParentQualityStandardID, 'Chapter','Chapter-8', 8),
	(@nParentQualityStandardID, 'Chapter','Chapter-9', 9),
	(@nParentQualityStandardID, 'Chapter','Chapter-10', 10)

SET @cEntityTypeCode = 'Standard'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-1'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '1. The SCHO defines and displays the services that it can provide', 1),
	(@nParentQualityStandardID, 'Standard', '2. The SCHO has a documented registration, admission and transfer process', 2),
	(@nParentQualityStandardID, 'Standard', '3. Patients cared for by the SHCO undergo an established initial assessment', 3),
	(@nParentQualityStandardID, 'Standard', '4. Patient’s care is continuous and all patients cared for by the SHCO undergo a regular assessment.', 4),
	(@nParentQualityStandardID, 'Standard', '5. Laboratory serivces are provided as per the scope of the SCHO''s services and laboratory safety requirements', 4),
	(@nParentQualityStandardID, 'Standard', '6. Managing services are provided as per the scope of the hospital''s services and established radiation safety programme', 5),
	(@nParentQualityStandardID, 'Standard', '7. The SHCO has qa defined discharge process', 6)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-2'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '8. Care of patients is guided by accepted norms and practice', 1),
	(@nParentQualityStandardID, 'Standard', '9. Emergency services including ambulance and guided by documented procedures and applicable laws and regulations', 2),
	(@nParentQualityStandardID, 'Standard', '10. Documented procedures define rational use of blood and blood products', 3),
	(@nParentQualityStandardID, 'Standard', '11. Documented procedures guide the care of patients as per the scope of services provided by the SCHO in intensive care and high dependancy units.', 4),
	(@nParentQualityStandardID, 'Standard', '12. Documented procedures guide the care of obstetrical patients as per the scope of serices provided by the SCHO.', 5),
	(@nParentQualityStandardID, 'Standard', '12. Documented procedures guide the care of obstetrical patients as per the scope of services provided by the SCHO', 6),
	(@nParentQualityStandardID, 'Standard', '13. Documented procedures guide the care of pediatric patients as per the scope of services provided by the SCHO.', 7),
	(@nParentQualityStandardID, 'Standard', '14. Documented procedures guide the administration of anesthesia.', 8),
	(@nParentQualityStandardID, 'Standard', '15. Documented procedured guide the care of patients undergoing surgical procedures.', 9)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-3'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '16. Documented procedures that guide the organization of pharmacy services and usage of medication.', 1),
	(@nParentQualityStandardID, 'Standard', '17. Documented procedures guide the prescription of medications.', 2),
	(@nParentQualityStandardID, 'Standard', '18. Policies and procedure guide the safe dispensing of medicines.', 3),
	(@nParentQualityStandardID, 'Standard', '19. There are defined procedures for medication administration.', 4),
	(@nParentQualityStandardID, 'Standard', '20. Adverse drug events are monitored.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-4'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '21. Patient rights are documented displayed and support individual beliefs, values and involve the patient and family in decision making process', 1),
	(@nParentQualityStandardID, 'Standard', '22. Patient families have a right to information and education about their healthcare needs', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-5'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '23. The SCHO has an infection control manual which it periodically updates; the SHCO conducts surveillance activities', 1),
	(@nParentQualityStandardID, 'Standard', '24. The SCHO rakes actions to prevent or reduce the risks of hospital associates infections (HAI) in patient and employees.', 2),
	(@nParentQualityStandardID, 'Standard', '25. Bio-medical management practices are followed', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-6'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '26. There is a structures quality improvement and contonuous motnitring programme in the organisation', 1),
	(@nParentQualityStandardID, 'Standard', '27. The SCHO identifies key indicators to monitor the structures, processes, and outcomes which are used as tools for continuous improvement', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-7'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '28. The responsibilities of management are defined.', 1),
	(@nParentQualityStandardID, 'Standard', '29. The orgnisation is managed by the leaders by the leaders in an ethical manner', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-8'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '30. The SHCO''s environment and facilities operate to ensure safety of patients, their families, staff and visitors.', 1),
	(@nParentQualityStandardID, 'Standard', '31. The SCHO has a program for clinical and support service equipment management', 2),
	(@nParentQualityStandardID, 'Standard', '32. The SCHO has provisions for safe water, electricity, medical gas, and vacuum systems.', 3),
	(@nParentQualityStandardID, 'Standard', '33. The SCHO has plans for fire and nonfire emergencies within the facilities.', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-9'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '34. The SCHO has an ongoing programme for professional training and development of the staff', 1),
	(@nParentQualityStandardID, 'Standard', '35. The SCHO has a well-documented disciplinary and grievance handling procedure', 2),
	(@nParentQualityStandardID, 'Standard', '36. The SCHO addresses the health needs of employees', 3),
	(@nParentQualityStandardID, 'Standard', '37. There is documented personal record for each staff member', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Chapter' AND QS.EntityName = 'Chapter-10'

INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, 'Standard', '38. The SCHO has a complete and accurate medical record for every patient.', 1),
	(@nParentQualityStandardID, 'Standard', '39. The medical record reflects continuity of care', 2),
	(@nParentQualityStandardID, 'Standard', '40. Documented policies and procedures are in place for maintaining confidentiality, security, and integrity of records, data and information.', 3),
	(@nParentQualityStandardID, 'Standard', '41. Documented procedures exist for retention of the patient''s records, data and information.', 4)

SET @cEntityTypeCode = 'Verification Criteria'
SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 1
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '1.1 The services being provided are clearly defined.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.2 The defined services are prominently displayed.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '1.3 The relevant staff are oriented to these services.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 2
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '2.1 Process addresses registering and admitting outpatients, inpatients, and emergency patients.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '2.2 Process addresses mechanism for transfer or referral of patients who do not match the SHCO''s resources', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 3
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '3.1 The SHCO defines the content of the assessments for inpatients and emergency patients.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.2 The SHCO determines who can perform the assessments.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.3 The initial assessment for inpatients is documented within 24 hours or earlier.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '3.4 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 4
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '4.1 All patients are reassessed at appropriate intervals', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.2 Staff involved in direct clinical care document reassessments', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '4.3 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 5
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '5.1 Scope of the laboratory services are commensurate with the services provided by the SHCO.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '5.2 Procedures guide collection, identification, handling, safe transportation, processing and disposal of specimens.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '5.3 Laboratory results are available within a defined time frame and critical results are intimated immediately to the concerned personnel.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '5.4 Laboratory personnel are trained in safe practices and are provided with appropriate safety equipment or devices.', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 6
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '6.1 Imaging services comply with legal and other requirements', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '6.2 Scope of the imaging services are commensurate to the services provided by the SHCO', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '6.3 Imaging results are available within a defined time frame and critical results are intimated immediately to the concerned personnel', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 7
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '7.1 Process addresses discharge of all patients including medico-legal cases (MLCs) and patients leaving against medical advice.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.2 A discharge summary is given to all the patients leaving the SHCO (including patients leaving against medical advice).', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.3 Discharge summary contains the reasons for admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given, and the patient''s condition at the time of discharge.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.4 Discharge summary contains follow-up advice, medication and other instructions in an understandable manner.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.5 Discharge summary incorporates information about when and how to obtain urgent care', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '7.6 In case of death the summary of the case also includes the cause of death', 6)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 8
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '8.1 The care and treatment order are signed and dated by the concerned doctor', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '8.2 Clinical Practice Guidelines are adopted to guide patient care wherever possible', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 9
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '9.1 Documented procedures address care of patients arriving in the emergency including handling of medico-legal cases.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.2 Staff should be well versed in the care of Emergency patients in consonance with the scope of the services of hospital.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '9.3 Admission or discharge to home or transfer to another organization is also documented.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 10
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '10.1 The transfusion services are governed by the applicable laws and regulations.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.2 Informed consent is obtained for donation and transfusion of blood and blood products.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '10.3 Procedure addresses documenting and reporting of transfusion reactions', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 11
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '11.1 Care of patients is in consonance with the documented procedures.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '11.2 Adequate staff and equipment are available.', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 12
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '12.1 The SHCO defines the scope of obstetric services.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.2 Obstetric patient''s care includes regular antenatal check-ups, maternal nutrition, and postnatal care.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '12.3 The SHCO has the facilities to take care of neonates.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 13
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '13.1 The SHCO defines the scope of its paediatric services.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.2 Provisions are made for special care of children by competent staff.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.3 Patient assessment includes detailed nutritional growth and immunization assessment.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.4 Procedure addresses identification and security measures to prevent child or neonate abduction and abuse.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '13.5 The children''s family members are educated about nutrition, immunization and safe parenting.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 14
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '14.1 There is a documented policy and procedure for the administration of anaesthesia.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.2 All patients for anaesthesia have a pre-anaesthesia assessment by a qualified or trained individual.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.3 The pre-anaesthesia assessment results in formulation of an anaesthesia plan which is documented.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.4 An immediate preoperative revaluation is documented.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.5 Informed consent for administration of anaesthesia is obtained by the anesthetist.', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.6 Anaesthesia monitoring includes regular and periodic recording of heart rate, cardiac rhythm, respiratory rate, blood pressure, oxygen saturation, airway security, and potency and level of anesthesia.', 6),
	(@nParentQualityStandardID, @cEntityTypeCode, '14.7 Each patient''s post anaesthesia status is monitored and documented.', 7)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 15
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '15.1 Surgical patients have a preoperative assessment and a provisional diagnosis documented prior to surgery.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.2 Informed consent is obtained by a surgeon prior to the procedure.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.3 Documented procedures address the prevention of adverse events like wrong site, wrong patient, and wrong surgery.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.4 Qualified persons are permitted to perform the procedures that they are entitled to perform.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.5 The operating surgeon documents the operative notes and postoperative plan of care.', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '15.6 The operation theatre is adequately equipped and monitored for infection control practices.', 6)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 16
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '16.1 Documented procedures incorporate purchase, storage, prescription, and dispensation of medications.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.2 These comply with the applicable laws and regulations.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.3 Sound alike and look alike medications are stored separately.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.4 Medications beyond the expiry date are not stored or used.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '16.5 Documented procedures address procurement and usage of implantable prosthesis.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 17
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '17.1 The SHCO determines who can write orders.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '17.2 Orders are written in a uniform location in the medical records.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '17.3 Medication orders are clear, legible, dated and signed.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '17.4 The SHCO defines a list of high-risk medication and process to prescribe them.', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 18
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '18.1 Medications are checked prior to dispensing including expiry date to ensure that they are fit for use', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '18.2 High risk medication orders are verified prior to dispensing', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 19
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '19.1 Medications ae administered by trained personnel', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '19.2 High risk medication orders are verified prior to administration, medication order including patient, dosage, route and timing are verified', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '19.3 Prepared medication is labelled prior to preparation of second drug', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '19.4 Medication administration is documented', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '19.5 A proper record is kept of the usage administration and disposal of narcotics and psychotropic medication', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 20
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '20.1 Adverse drug event are defined and monitored', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '20.2 Adverse drug events are documented and reported within a specified time frame', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 21
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '21.1 Patient rights include respect for personal dignity and privacy during examination procedures and treatment', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.2 Patient rights include protection from physical abuse or neglect', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.3 Patient rights include treating patient information as confidential', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.4 Patient rights include obtaining informed consent before carrying out procedures', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.5 Patient rights include information on how to voice a complaint', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.6 Patient rights include on the expected cost of the treatment', 6),
	(@nParentQualityStandardID, @cEntityTypeCode, '21.7 Patient has a right to have an access to his / her clinical records', 7)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 22
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '22.1 Patients and families are educated on plan of care, preventive aspects, possible complications, medications, the expected results and cost as applicable', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '22.2 Patients are taught in a language and format that they can understand ', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 23
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '23.1 It focuses on adherence to standard precautions at all times.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '23.2 Cleanliness and general hygiene of facilities will be maintained and monitored.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '23.3 Cleaning and disinfection practices are defined and monitored as appropriate.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '23.4 Equipment cleaning, disinfection and sterilization practices are included.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '23.5 Laundry and linen management processes are also included.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 24
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '24.1 Hand hygiene facilities in all patient care areas are accessible to health care provide', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '24.2 Adequate gloves, masks, soaps, and disinfectants are available and used correctly', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '24.3 Appropriate pre and post exposure prophylaxis is provided to all concerned staff members', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 25
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '25.1 The hospital is authorized by prescribed authority for management and handling of bio-medical waste.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '25.2 Proper segregation and collection of bio-medical waste from all patient care areas of the hospital is implemented and monitored', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '25.3 Bio-medical waste treatment facility is managed as per statutory provisions (if in-house) or outsourced to authorized contractors', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '25.4 Requisite fees, documents and reports are submitted to competent authorities on stipulated dates', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '25.5 Appropriate personal protective measures are used by all categories of staff handling bio-medical waste', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 26
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '26.1 There is a designated individual for coordinating and implementing the quality improvement program', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '26.2 The quality improvement programme is a continuous process and updated at least once in a year', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '26.3 Hospital Management makes available adequate resources required for quality improvement programme', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 27
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '27.1 The SHCO identifies the appropriate key performance indicators in both clinical and managerial areas.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '27.2 These indicators shall be monitored.', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 28
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '28.1 The SHCO has a documented organogram.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '28.2 The SHCO is registered with appropriate authorities as applicable.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '28.3 The SHCO has a designated individual(s) to oversee the hospital-wide safety program.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 29
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '29.1 The management makes public the mission statement of the organization', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '29.2 The leaders/management guide the organization to function in an ethical manner', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '29.3 The organization discloses its ownership', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '29.4 The organization’s billing process is accurate and ethical', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 30
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '30.1 Internal and external signages shall be displayed in a language understood by the patients or families and communities.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '30.2 Maintenance staff is contactable round the clock for emergency repairs.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '30.3 The SHCO has a system to identify the potential safety and security risks including hazardous materials.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '30.4 Facility inspection rounds to ensure safety are conducted periodically.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '30.5 There is a safety education programme for relevant staff.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 31
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '31.1 The SHCO plans for equipment in accordance with its services.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '31.2 There is a documented operational and maintenance (preventive and breakdown) plan.', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 32
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '32.1 Potable water and electricity are available round the clock.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '32.2 Alternate sources are provided for in case of failure and tested regularly.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '32.3 There is a maintenance plan for medical gas and vacuum systems.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 33
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '33.1 The SHCO has plans and provisions for early detection, abatement, and containment of fire and non-fire emergencies.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '33.2 The SHCO has a documented safe exit plan in case of fire and non-fire emergencies.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '33.3 Staff is trained for their role in case of such emergencies.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '33.4 Mock drills are held at least twice in a year.', 4)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 34
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '34.1 All staff is trained on the relevant risks within the hospital environment', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '34.2 Staff members can demonstrate and take actions to report, eliminate/ minimize risks', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '34.3 Training also occurs when job responsibilities change/ new equipment is introduced', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 35
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '35.1 A documented procedure regarding disciplinary and grievance handling is in place.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '35.2 The documented procedure is known to all categories of employees in the SHCO.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '35.3 Actions are taken to redress the grievance.', 3)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 36
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '36.1 Health problems of the employees are taken care of in accordance with the SHCO''s policy.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '36.2 Occupational health hazards are adequately addressed.', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 37
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '37.1 Personal files are maintained in respect of all employees.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '37.2 The personal files contain personal information regarding the employees qualification, disciplinary actions and health status', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 38
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '38.1 Every medical record has a unique identifier.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '38.2 The SHCO identifies those authorized to make entries in medical record.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '38.3 Every medical record entry is dated and timed.', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '38.4 The author of the entry can be identified.', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '38.5 The contents of medical records are identified and documented.', 5)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 39
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '39.1 The records provides an up-to-date and chronological account of patient care.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '39.2 The medical record contains information regarding reasons of admission, diagnosis and plan of care', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '39.3 Operative and other procedures performed are incorporated in the medical record', 3),
	(@nParentQualityStandardID, @cEntityTypeCode, '39.4 The medical record contains a copy of the discharge note duly signed by the appropriate and qualified personnel', 4),
	(@nParentQualityStandardID, @cEntityTypeCode, '39.5 In case of death, the medical records contain a copy of the death certificate indicating the cause, date and time of death', 5),
	(@nParentQualityStandardID, @cEntityTypeCode, '39.6 Care providers have access to current and past medical record', 6)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 40
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '40.1 Documented procedures exist for maintaining confidentiality, security and integrity of information.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '40.2 Privileged health information is used for the purposes identified or as required by law and not disclosed without the patient''s authorization.', 2)

SELECT @nParentQualityStandardID = QS.QualityStandardID FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Standard' AND CAST(LEFT(REPLACE(QS.EntityName, '.', ''), 2) AS INT) = 41
	
INSERT INTO dropdown.QualityStandard 
	(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder) 
VALUES 
	(@nParentQualityStandardID, @cEntityTypeCode, '41.1 Documented procedures exist for retention time of the patient''s clinical records, data and information.', 1),
	(@nParentQualityStandardID, @cEntityTypeCode, '41.2 The retention process provides expected confidentiality and security.', 2),
	(@nParentQualityStandardID, @cEntityTypeCode, '41.3 The destruction of medical records, data, and information is in accordance with the laid down procedure.', 3)
GO

TRUNCATE TABLE dropdown.QualityStandardLookup

;
WITH HD (QualityStandardID,ParentQualityStandardID,EntityTypeCode,NodeLevel)
	AS 
	(
	SELECT
		T.QualityStandardID, 
		T.ParentQualityStandardID,
		T.EntityTypeCode, 
		1 
	FROM dropdown.QualityStandard T
	WHERE T.ParentQualityStandardID = 0
		AND T.QualityStandardID > 0
	
	UNION ALL
		
	SELECT
		T.QualityStandardID, 
		T.ParentQualityStandardID, 
		T.EntityTypeCode, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dropdown.QualityStandard T 
		JOIN HD ON HD.QualityStandardID = T.ParentQualityStandardID 
			--AND T.EntityTypeCode <> 'VerificationCriteria'
	)
	
INSERT INTO dropdown.QualityStandardLookup
	(EntityTypeCode, QualityStandardID, ManualID, ChapterID, StandardID, ObjectiveElementID, VerificationCriteriaID)
SELECT
	A.EntityTypeCode, 
	A.QualityStandardID,

	CASE
		WHEN A.NodeLevel = 5
		THEN ISNULL(E.QualityStandardID, 0)
		WHEN A.NodeLevel = 4
		THEN ISNULL(D.QualityStandardID, 0)
		WHEN A.NodeLevel = 3
		THEN ISNULL(C.QualityStandardID, 0)
		WHEN A.NodeLevel = 2
		THEN ISNULL(B.QualityStandardID, 0)
		WHEN A.NodeLevel = 1
		THEN ISNULL(A.QualityStandardID, 0)
		ELSE 0
	END,

	CASE
		WHEN A.NodeLevel = 5
		THEN ISNULL(D.QualityStandardID, 0)
		WHEN A.NodeLevel = 4
		THEN ISNULL(C.QualityStandardID, 0)
		WHEN A.NodeLevel = 3
		THEN ISNULL(B.QualityStandardID, 0)
		WHEN A.NodeLevel = 2
		THEN ISNULL(A.QualityStandardID, 0)
		ELSE 0
	END,

	CASE
		WHEN A.NodeLevel = 5
		THEN ISNULL(C.QualityStandardID, 0)
		WHEN A.NodeLevel = 4
		THEN ISNULL(B.QualityStandardID, 0)
		WHEN A.NodeLevel = 3
		THEN ISNULL(A.QualityStandardID, 0)
		ELSE 0
	END,

	CASE
		WHEN A.NodeLevel = 5
		THEN ISNULL(B.QualityStandardID, 0)
		WHEN A.NodeLevel = 4
		THEN ISNULL(A.QualityStandardID, 0)
		ELSE 0
	END,

	CASE
		WHEN A.NodeLevel = 5
		THEN ISNULL(A.QualityStandardID, 0)
		ELSE 0
	END

FROM HD A
	LEFT JOIN HD B ON B.QualityStandardID = A.ParentQualityStandardID
	LEFT JOIN HD C ON C.QualityStandardID = B.ParentQualityStandardID
	LEFT JOIN HD D ON D.QualityStandardID = C.ParentQualityStandardID
	LEFT JOIN HD E ON E.QualityStandardID = D.ParentQualityStandardID

UPDATE QSL
SET
	QSL.ManualName = QSM.EntityName,
	QSL.ChapterName = QSC.EntityName,
	QSL.StandardName = QSS.EntityName,
	QSL.ObjectiveElementName = QSOE.EntityName,
	QSL.VerificationCriteriaName = QSVC.EntityName
FROM dropdown.QualityStandardLookup QSL
	JOIN dropdown.QualityStandard QSM ON QSM.QualityStandardID = QSL.ManualID
	JOIN dropdown.QualityStandard QSC ON QSC.QualityStandardID = QSL.ChapterID
	JOIN dropdown.QualityStandard QSS ON QSS.QualityStandardID = QSL.StandardID
	JOIN dropdown.QualityStandard QSOE ON QSOE.QualityStandardID = QSL.ObjectiveElementID
	JOIN dropdown.QualityStandard QSVC ON QSVC.QualityStandardID = QSL.VerificationCriteriaID
GO

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table dropdown.QualityStandardAssessmentEvidenceType
TRUNCATE TABLE dropdown.QualityStandardAssessmentEvidenceType
GO

INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.6.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.7.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.6.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'CorrectRecords') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.6.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.7.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.3.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.5.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasProviderInterview') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.3.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.3.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.4.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '2.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasObservation') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.6.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.7.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.5.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '13.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '15.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '16.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.3.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.4.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.5 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.5.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.6.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'TotalRecordsChecked') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '9.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '1.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '10.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '11.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '12.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '14.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '2.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '2.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '2.2.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.3.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '3.4.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '4.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '5.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '6.2.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.1.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.1.2 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.1.3 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '7.1.4 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.6.1 %'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasPhysicalVerification') FROM dropdown.QualityStandard QS WHERE QS.EntityName LIKE '8.6.2 %'
GO

INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.1 The services being provided are clearly defined.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.2 The defined services are prominently displayed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.3 The relevant staff are oriented to these services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '2.1 Process addresses registering and admitting outpatients, inpatients, and emergency patients.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '2.2 Process addresses mechanism for transfer or referral of patients who do not match the SHCO''s resources'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.1 The SHCO defines the content of the assessments for inpatients and emergency patients.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.2 The SHCO determines who can perform the assessments.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.3 The initial assessment for inpatients is documented within 24 hours or earlier.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.4 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.1 All patients are reassessed at appropriate intervals'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.2 Staff involved in direct clinical care document reassessments'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.3 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.1 Scope of the laboratory services are commensurate with the services provided by the SHCO.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.2 Procedures guide collection, identification, handling, safe transportation, processing and disposal of specimens.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.3 Laboratory results are available within a defined time frame and critical results are intimated immediately to the concerned personnel.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.4 Laboratory personnel are trained in safe practices and are provided with appropriate safety equipment or devices.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.1 Imaging services comply with legal and other requirements'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.2 Scope of the imaging services are commensurate to the services provided by the SHCO'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.3 Imaging results are available within a defined time frame and critical results are intimated immediately to the concerned personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.1 Process addresses discharge of all patients including medico-legal cases (MLCs) and patients leaving against medical advice.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.2 A discharge summary is given to all the patients leaving the SHCO (including patients leaving against medical advice).'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.3 Discharge summary contains the reasons for admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given, and the patient''s condition at the time of discharge.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.4 Discharge summary contains follow-up advice, medication and other instructions in an understandable manner.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.5 Discharge summary incorporates information about when and how to obtain urgent care'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.6 In case of death the summary of the case also includes the cause of death'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '8.1 The care and treatment order are signed and dated by the concerned doctor'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '8.2 Clinical Practice Guidelines are adopted to guide patient care wherever possible'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.1 Documented procedures address care of patients arriving in the emergency including handling of medico-legal cases.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.2 Staff should be well versed in the care of Emergency patients in consonance with the scope of the services of hospital.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.3 Admission or discharge to home or transfer to another organization is also documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.1 The transfusion services are governed by the applicable laws and regulations.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.2 Informed consent is obtained for donation and transfusion of blood and blood products.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.3 Procedure addresses documenting and reporting of transfusion reactions'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '11.1 Care of patients is in consonance with the documented procedures.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '11.2 Adequate staff and equipment are available.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.1 The SHCO defines the scope of obstetric services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.2 Obstetric patient''s care includes regular antenatal check-ups, maternal nutrition, and postnatal care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.3 The SHCO has the facilities to take care of neonates.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.1 The SHCO defines the scope of its paediatric services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.2 Provisions are made for special care of children by competent staff.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.3 Patient assessment includes detailed nutritional growth and immunization assessment.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.4 Procedure addresses identification and security measures to prevent child or neonate abduction and abuse.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.5 The children''s family members are educated about nutrition, immunization and safe parenting.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.1 There is a documented policy and procedure for the administration of anaesthesia.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.2 All patients for anaesthesia have a pre-anaesthesia assessment by a qualified or trained individual.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.3 The pre-anaesthesia assessment results in formulation of an anaesthesia plan which is documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.4 An immediate preoperative revaluation is documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.5 Informed consent for administration of anaesthesia is obtained by the anesthetist.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.6 Anaesthesia monitoring includes regular and periodic recording of heart rate, cardiac rhythm, respiratory rate, blood pressure, oxygen saturation, airway security, and potency and level of anesthesia.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.7 Each patient''s post anaesthesia status is monitored and documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.1 Surgical patients have a preoperative assessment and a provisional diagnosis documented prior to surgery.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.2 Informed consent is obtained by a surgeon prior to the procedure.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.3 Documented procedures address the prevention of adverse events like wrong site, wrong patient, and wrong surgery.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.4 Qualified persons are permitted to perform the procedures that they are entitled to perform.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.5 The operating surgeon documents the operative notes and postoperative plan of care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.6 The operation theatre is adequately equipped and monitored for infection control practices.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.1 Documented procedures incorporate purchase, storage, prescription, and dispensation of medications.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.2 These comply with the applicable laws and regulations.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.3 Sound alike and look alike medications are stored separately.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.4 Medications beyond the expiry date are not stored or used.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.5 Documented procedures address procurement and usage of implantable prosthesis.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.1 The SHCO determines who can write orders.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.2 Orders are written in a uniform location in the medical records.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.3 Medication orders are clear, legible, dated and signed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.4 The SHCO defines a list of high-risk medication and process to prescribe them.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '18.1 Medications are checked prior to dispensing including expiry date to ensure that they are fit for use'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '18.2 High risk medication orders are verified prior to dispensing'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.1 Medications ae administered by trained personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.2 High risk medication orders are verified prior to administration, medication order including patient, dosage, route and timing are verified'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.3 Prepared medication is labelled prior to preparation of second drug'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.4 Medication administration is documented'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.5 A proper record is kept of the usage administration and disposal of narcotics and psychotropic medication'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '20.1 Adverse drug event are defined and monitored'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '20.2 Adverse drug events are documented and reported within a specified time frame'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.1 Patient rights include respect for personal dignity and privacy during examination procedures and treatment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.2 Patient rights include protection from physical abuse or neglect'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.3 Patient rights include treating patient information as confidential'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.4 Patient rights include obtaining informed consent before carrying out procedures'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.5 Patient rights include information on how to voice a complaint'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.6 Patient rights include on the expected cost of the treatment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.7 Patient has a right to have an access to his / her clinical records'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '22.1 Patients and families are educated on plan of care, preventive aspects, possible complications, medications, the expected results and cost as applicable'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '22.2 Patients are taught in a language and format that they can understand '
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.1 It focuses on adherence to standard precautions at all times.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.2 Cleanliness and general hygiene of facilities will be maintained and monitored.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.3 Cleaning and disinfection practices are defined and monitored as appropriate.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.4 Equipment cleaning, disinfection and sterilization practices are included.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.5 Laundry and linen management processes are also included.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.1 Hand hygiene facilities in all patient care areas are accessible to health care provide'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.2 Adequate gloves, masks, soaps, and disinfectants are available and used correctly'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.3 Appropriate pre and post exposure prophylaxis is provided to all concerned staff members'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.1 The hospital is authorized by prescribed authority for management and handling of bio-medical waste.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.2 Proper segregation and collection of bio-medical waste from all patient care areas of the hospital is implemented and monitored'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.3 Bio-medical waste treatment facility is managed as per statutory provisions (if in-house) or outsourced to authorized contractors'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.4 Requisite fees, documents and reports are submitted to competent authorities on stipulated dates'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.5 Appropriate personal protective measures are used by all categories of staff handling bio-medical waste'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.1 There is a designated individual for coordinating and implementing the quality improvement program'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.2 The quality improvement programme is a continuous process and updated at least once in a year'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.3 Hospital Management makes available adequate resources required for quality improvement programme'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '27.1 The SHCO identifies the appropriate key performance indicators in both clinical and managerial areas.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '27.2 These indicators shall be monitored.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.1 The SHCO has a documented organogram.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.2 The SHCO is registered with appropriate authorities as applicable.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.3 The SHCO has a designated individual(s) to oversee the hospital-wide safety program.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.1 The management makes public the mission statement of the organization'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.2 The leaders/management guide the organization to function in an ethical manner'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.3 The organization discloses its ownership'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.4 The organizationâ??s billing process is accurate and ethical'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.1 Internal and external signages shall be displayed in a language understood by the patients or families and communities.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.2 Maintenance staff is contactable round the clock for emergency repairs.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.3 The SHCO has a system to identify the potential safety and security risks including hazardous materials.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.4 Facility inspection rounds to ensure safety are conducted periodically.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.5 There is a safety education programme for relevant staff.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '31.1 The SHCO plans for equipment in accordance with its services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '31.2 There is a documented operational and maintenance (preventive and breakdown) plan.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.1 Potable water and electricity are available round the clock.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.2 Alternate sources are provided for in case of failure and tested regularly.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.3 There is a maintenance plan for medical gas and vacuum systems.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.1 The SHCO has plans and provisions for early detection, abatement, and containment of fire and non-fire emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.2 The SHCO has a documented safe exit plan in case of fire and non-fire emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.3 Staff is trained for their role in case of such emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.4 Mock drills are held at least twice in a year.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.1 All staff is trained on the relevant risks within the hospital environment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.2 Staff members can demonstrate and take actions to report, eliminate/ minimize risks'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.3 Training also occurs when job responsibilities change/ new equipment is introduced'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.1 A documented procedure regarding disciplinary and grievance handling is in place.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.2 The documented procedure is known to all categories of employees in the SHCO.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.3 Actions are taken to redress the grievance.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '36.1 Health problems of the employees are taken care of in accordance with the SHCO''s policy.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '36.2 Occupational health hazards are adequately addressed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '37.1 Personal files are maintained in respect of all employees.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '37.2 The personal files contain personal information regarding the employees qualification, disciplinary actions and health status'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.1 Every medical record has a unique identifier.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.2 The SHCO identifies those authorized to make entries in medical record.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.3 Every medical record entry is dated and timed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.4 The author of the entry can be identified.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.5 The contents of medical records are identified and documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.1 The records provides an up-to-date and chronological account of patient care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.2 The medical record contains information regarding reasons of admission, diagnosis and plan of care'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.3 Operative and other procedures performed are incorporated in the medical record'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.4 The medical record contains a copy of the discharge note duly signed by the appropriate and qualified personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.5 In case of death, the medical records contain a copy of the death certificate indicating the cause, date and time of death'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.6 Care providers have access to current and past medical record'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '40.1 Documented procedures exist for maintaining confidentiality, security and integrity of information.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '40.2 Privileged health information is used for the purposes identified or as required by law and not disclosed without the patient''s authorization.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.1 Documented procedures exist for retention time of the patient''s clinical records, data and information.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.2 The retention process provides expected confidentiality and security.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasDocumentation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.3 The destruction of medical records, data, and information is in accordance with the laid down procedure.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.1 The services being provided are clearly defined.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.2 The defined services are prominently displayed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '1.3 The relevant staff are oriented to these services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '2.1 Process addresses registering and admitting outpatients, inpatients, and emergency patients.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '2.2 Process addresses mechanism for transfer or referral of patients who do not match the SHCO''s resources'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.1 The SHCO defines the content of the assessments for inpatients and emergency patients.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.2 The SHCO determines who can perform the assessments.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.3 The initial assessment for inpatients is documented within 24 hours or earlier.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '3.4 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.1 All patients are reassessed at appropriate intervals'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.2 Staff involved in direct clinical care document reassessments'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '4.3 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.1 Scope of the laboratory services are commensurate with the services provided by the SHCO.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.2 Procedures guide collection, identification, handling, safe transportation, processing and disposal of specimens.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.3 Laboratory results are available within a defined time frame and critical results are intimated immediately to the concerned personnel.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '5.4 Laboratory personnel are trained in safe practices and are provided with appropriate safety equipment or devices.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.1 Imaging services comply with legal and other requirements'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.2 Scope of the imaging services are commensurate to the services provided by the SHCO'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '6.3 Imaging results are available within a defined time frame and critical results are intimated immediately to the concerned personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.1 Process addresses discharge of all patients including medico-legal cases (MLCs) and patients leaving against medical advice.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.2 A discharge summary is given to all the patients leaving the SHCO (including patients leaving against medical advice).'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.3 Discharge summary contains the reasons for admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given, and the patient''s condition at the time of discharge.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.4 Discharge summary contains follow-up advice, medication and other instructions in an understandable manner.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.5 Discharge summary incorporates information about when and how to obtain urgent care'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '7.6 In case of death the summary of the case also includes the cause of death'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '8.1 The care and treatment order are signed and dated by the concerned doctor'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '8.2 Clinical Practice Guidelines are adopted to guide patient care wherever possible'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.1 Documented procedures address care of patients arriving in the emergency including handling of medico-legal cases.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.2 Staff should be well versed in the care of Emergency patients in consonance with the scope of the services of hospital.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '9.3 Admission or discharge to home or transfer to another organization is also documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.1 The transfusion services are governed by the applicable laws and regulations.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.2 Informed consent is obtained for donation and transfusion of blood and blood products.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '10.3 Procedure addresses documenting and reporting of transfusion reactions'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '11.1 Care of patients is in consonance with the documented procedures.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '11.2 Adequate staff and equipment are available.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.1 The SHCO defines the scope of obstetric services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.2 Obstetric patient''s care includes regular antenatal check-ups, maternal nutrition, and postnatal care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '12.3 The SHCO has the facilities to take care of neonates.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.1 The SHCO defines the scope of its paediatric services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.2 Provisions are made for special care of children by competent staff.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.3 Patient assessment includes detailed nutritional growth and immunization assessment.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.4 Procedure addresses identification and security measures to prevent child or neonate abduction and abuse.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '13.5 The children''s family members are educated about nutrition, immunization and safe parenting.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.1 There is a documented policy and procedure for the administration of anaesthesia.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.2 All patients for anaesthesia have a pre-anaesthesia assessment by a qualified or trained individual.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.3 The pre-anaesthesia assessment results in formulation of an anaesthesia plan which is documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.4 An immediate preoperative revaluation is documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.5 Informed consent for administration of anaesthesia is obtained by the anesthetist.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.6 Anaesthesia monitoring includes regular and periodic recording of heart rate, cardiac rhythm, respiratory rate, blood pressure, oxygen saturation, airway security, and potency and level of anesthesia.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '14.7 Each patient''s post anaesthesia status is monitored and documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.1 Surgical patients have a preoperative assessment and a provisional diagnosis documented prior to surgery.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.2 Informed consent is obtained by a surgeon prior to the procedure.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.3 Documented procedures address the prevention of adverse events like wrong site, wrong patient, and wrong surgery.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.4 Qualified persons are permitted to perform the procedures that they are entitled to perform.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.5 The operating surgeon documents the operative notes and postoperative plan of care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '15.6 The operation theatre is adequately equipped and monitored for infection control practices.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.1 Documented procedures incorporate purchase, storage, prescription, and dispensation of medications.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.2 These comply with the applicable laws and regulations.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.3 Sound alike and look alike medications are stored separately.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.4 Medications beyond the expiry date are not stored or used.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '16.5 Documented procedures address procurement and usage of implantable prosthesis.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.1 The SHCO determines who can write orders.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.2 Orders are written in a uniform location in the medical records.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.3 Medication orders are clear, legible, dated and signed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '17.4 The SHCO defines a list of high-risk medication and process to prescribe them.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '18.1 Medications are checked prior to dispensing including expiry date to ensure that they are fit for use'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '18.2 High risk medication orders are verified prior to dispensing'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.1 Medications ae administered by trained personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.2 High risk medication orders are verified prior to administration, medication order including patient, dosage, route and timing are verified'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.3 Prepared medication is labelled prior to preparation of second drug'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.4 Medication administration is documented'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '19.5 A proper record is kept of the usage administration and disposal of narcotics and psychotropic medication'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '20.1 Adverse drug event are defined and monitored'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '20.2 Adverse drug events are documented and reported within a specified time frame'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.1 Patient rights include respect for personal dignity and privacy during examination procedures and treatment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.2 Patient rights include protection from physical abuse or neglect'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.3 Patient rights include treating patient information as confidential'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.4 Patient rights include obtaining informed consent before carrying out procedures'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.5 Patient rights include information on how to voice a complaint'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.6 Patient rights include on the expected cost of the treatment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '21.7 Patient has a right to have an access to his / her clinical records'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '22.1 Patients and families are educated on plan of care, preventive aspects, possible complications, medications, the expected results and cost as applicable'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '22.2 Patients are taught in a language and format that they can understand '
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.1 It focuses on adherence to standard precautions at all times.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.2 Cleanliness and general hygiene of facilities will be maintained and monitored.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.3 Cleaning and disinfection practices are defined and monitored as appropriate.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.4 Equipment cleaning, disinfection and sterilization practices are included.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '23.5 Laundry and linen management processes are also included.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.1 Hand hygiene facilities in all patient care areas are accessible to health care provide'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.2 Adequate gloves, masks, soaps, and disinfectants are available and used correctly'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '24.3 Appropriate pre and post exposure prophylaxis is provided to all concerned staff members'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.1 The hospital is authorized by prescribed authority for management and handling of bio-medical waste.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.2 Proper segregation and collection of bio-medical waste from all patient care areas of the hospital is implemented and monitored'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.3 Bio-medical waste treatment facility is managed as per statutory provisions (if in-house) or outsourced to authorized contractors'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.4 Requisite fees, documents and reports are submitted to competent authorities on stipulated dates'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '25.5 Appropriate personal protective measures are used by all categories of staff handling bio-medical waste'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.1 There is a designated individual for coordinating and implementing the quality improvement program'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.2 The quality improvement programme is a continuous process and updated at least once in a year'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '26.3 Hospital Management makes available adequate resources required for quality improvement programme'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '27.1 The SHCO identifies the appropriate key performance indicators in both clinical and managerial areas.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '27.2 These indicators shall be monitored.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.1 The SHCO has a documented organogram.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.2 The SHCO is registered with appropriate authorities as applicable.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '28.3 The SHCO has a designated individual(s) to oversee the hospital-wide safety program.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.1 The management makes public the mission statement of the organization'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.2 The leaders/management guide the organization to function in an ethical manner'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.3 The organization discloses its ownership'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '29.4 The organizationâ??s billing process is accurate and ethical'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.1 Internal and external signages shall be displayed in a language understood by the patients or families and communities.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.2 Maintenance staff is contactable round the clock for emergency repairs.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.3 The SHCO has a system to identify the potential safety and security risks including hazardous materials.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.4 Facility inspection rounds to ensure safety are conducted periodically.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '30.5 There is a safety education programme for relevant staff.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '31.1 The SHCO plans for equipment in accordance with its services.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '31.2 There is a documented operational and maintenance (preventive and breakdown) plan.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.1 Potable water and electricity are available round the clock.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.2 Alternate sources are provided for in case of failure and tested regularly.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '32.3 There is a maintenance plan for medical gas and vacuum systems.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.1 The SHCO has plans and provisions for early detection, abatement, and containment of fire and non-fire emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.2 The SHCO has a documented safe exit plan in case of fire and non-fire emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.3 Staff is trained for their role in case of such emergencies.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '33.4 Mock drills are held at least twice in a year.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.1 All staff is trained on the relevant risks within the hospital environment'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.2 Staff members can demonstrate and take actions to report, eliminate/ minimize risks'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '34.3 Training also occurs when job responsibilities change/ new equipment is introduced'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.1 A documented procedure regarding disciplinary and grievance handling is in place.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.2 The documented procedure is known to all categories of employees in the SHCO.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '35.3 Actions are taken to redress the grievance.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '36.1 Health problems of the employees are taken care of in accordance with the SHCO''s policy.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '36.2 Occupational health hazards are adequately addressed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '37.1 Personal files are maintained in respect of all employees.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '37.2 The personal files contain personal information regarding the employees qualification, disciplinary actions and health status'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.1 Every medical record has a unique identifier.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.2 The SHCO identifies those authorized to make entries in medical record.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.3 Every medical record entry is dated and timed.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.4 The author of the entry can be identified.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '38.5 The contents of medical records are identified and documented.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.1 The records provides an up-to-date and chronological account of patient care.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.2 The medical record contains information regarding reasons of admission, diagnosis and plan of care'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.3 Operative and other procedures performed are incorporated in the medical record'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.4 The medical record contains a copy of the discharge note duly signed by the appropriate and qualified personnel'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.5 In case of death, the medical records contain a copy of the death certificate indicating the cause, date and time of death'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '39.6 Care providers have access to current and past medical record'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '40.1 Documented procedures exist for maintaining confidentiality, security and integrity of information.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '40.2 Privileged health information is used for the purposes identified or as required by law and not disclosed without the patient''s authorization.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.1 Documented procedures exist for retention time of the patient''s clinical records, data and information.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.2 The retention process provides expected confidentiality and security.'
INSERT INTO dropdown.QualityStandardAssessmentEvidenceType (QualityStandardID, AssessmentEvidenceTypeID) SELECT QS.QualityStandardID, (SELECT AET.AssessmentEvidenceTypeID FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'HasImplementation') FROM dropdown.QualityStandard QS WHERE QS.EntityName = '41.3 The destruction of medical records, data, and information is in accordance with the laid down procedure.'
GO
--End table dropdown.QualityStandardAssessmentEvidenceType
