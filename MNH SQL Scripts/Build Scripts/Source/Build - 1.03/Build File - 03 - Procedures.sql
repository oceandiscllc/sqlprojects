USE MNH
GO

--Begin procedure contact.GetContactByContactID
EXEC Utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.10
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CellPhoneNumber,
		C.ContactID,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		C.EmailAddress,
		C.FacilityEndDate,
		core.FormatDate(C.FacilityEndDate) AS FacilityEndDateFormatted,
		C.FacilityStartDate,
		core.FormatDate(C.FacilityStartDate) AS FacilityStartDateFormatted,
		C.FirstName,
		C.IsActive,
		C.LastName,
		C.MiddleName,
		C.OtherContactFunction,
		C.OtherContactRole,
		C.PhoneNumber,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Title,
		C.YearsExperience,
		CF.ContactFunctionCode,
		CF.ContactFunctionID,
		ISNULL(C.OtherContactFunction, CF.ContactFunctionName) AS ContactFunctionName,
		CR.ContactRoleCode,
		CR.ContactRoleID,
		ISNULL(C.OtherContactRole, CR.ContactRoleName) AS ContactRoleName,
		DIBR.DIBRoleID,
		DIBR.DIBRoleName,
		F.FacilityID,
		F.FacilityName,
		G.GenderID,
		G.GenderName
	FROM contact.Contact C
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
		JOIN dropdown.DIBRole DIBR ON DIBR.DIBRoleID = C.DIBRoleID
		JOIN dropdown.Gender G ON G.GenderID = C.GenderID
		JOIN facility.Facility F ON F.FacilityID = C.FacilityID
			AND C.ContactID = @ContactID
	
END
GO 
--End procedure contact.GetContactByContactID

--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure dropdown.GetAssessmentEvidenceTypeData
EXEC Utility.DropObject 'dropdown.GetAssessmentEvidenceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to return data from the dropdown.AssessmentEvidenceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetAssessmentEvidenceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssessmentEvidenceTypeID, 
		T.AssessmentEvidenceTypeCode,
		T.AssessmentEvidenceTypeDataType,
		T.AssessmentEvidenceTypeName
	FROM dropdown.AssessmentEvidenceType T
	WHERE (T.AssessmentEvidenceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssessmentEvidenceTypeName, T.AssessmentEvidenceTypeID

END
GO
--End procedure dropdown.GetAssessmentEvidenceTypeData

--Begin procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataQualityStandardIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to return data from the dropdown.QualityStandardLookup table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupDataQualityStandardIDList

@QualityStandardIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		QSL.ManualName,
		QSL.ChapterName,
		QSL.StandardName,
		QSL.ObjectiveElementName,
		QSL.VerificationCriteriaName,
		QSL.QualityStandardID
	FROM dropdown.QualityStandardLookup QSL
		JOIN core.ListToTable(@QualityStandardIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = QSL.QualityStandardID

END
GO
--End procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitID, 	
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitStartDateTime,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT 
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.Score
	FROM dropdown.QualityStandardLookup QSL
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID'
GO
--End procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitQualityStandardID'
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT,
@UseDocumentEntityCodeList INT = 0,
@DocumentEntityCodeList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT 
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
	WHERE VQS.VisitID = @VisitID
		AND VQS.QualityStandardID = @QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode,
		AET.AssessmentEvidenceTypeDataType
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	IF @UseDocumentEntityCodeList = 0
		BEGIN

		SELECT
			D.DocumentTitle,
			DE.DocumentEntityCode,
			'<div id="div' + DE.DocumentEntityCode + '"><a class="m-r-10" href="javascript:deleteDocumentByDocumentEntityCode(''' + DE.DocumentEntityCode + ''')"><i class="fa fa-fw fa-times-circle"></i></a>'
				+	'<a href="/document/getDocumentByDocumentEntityCode/DocumentEntityCode/' + DE.DocumentEntityCode + '">' + D.DocumentTitle + '</a></div>' AS DocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'VisitQualityStandard'
				AND DE.EntityID = 
					(
					SELECT VQS.VisitQualityStandardID 
					FROM visit.VisitQualityStandard VQS
					WHERE VQS.VisitID = @VisitID
						AND VQS.QualityStandardID = @QualityStandardID
					)

		END
	ELSE IF @DocumentEntityCodeList IS NOT NULL
		BEGIN

		SELECT
			D.DocumentTitle,
			DE.DocumentEntityCode,
			'<div id="div' + DE.DocumentEntityCode + '"><a class="m-r-10" href="javascript:deleteDocumentByDocumentEntityCode(''' + DE.DocumentEntityCode + ''')"><i class="fa fa-fw fa-times-circle"></i></a>'
				+	'<a href="/document/getDocumentByDocumentEntityCode/DocumentEntityCode/' + DE.DocumentEntityCode + '">' + D.DocumentTitle + '</a></div>' AS DocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
			JOIN core.ListToTable(@DocumentEntityCodeList, ',') LTT ON LTT.ListItem = DE.DocumentEntityCode

		END
	ELSE
		BEGIN

		SELECT
			NULL AS DocumentTitle,
			NULL AS DocumentEntityCode,
			NULL AS DocumentLink

		END
	--ENDIF

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID