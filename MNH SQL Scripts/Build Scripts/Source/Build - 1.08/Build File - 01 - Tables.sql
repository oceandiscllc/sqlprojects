USE MNH
GO

--Begin table facility.Facility
--Begin trigger facility.TR_Facility
EXEC utility.DropObject 'facility.TR_Facility'
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09..5
-- Description:	A trigger to update the facility.Facility tables
-- ========================================================================
CREATE TRIGGER facility.TR_Facility ON facility.Facility AFTER INSERT, DELETE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM DELETED)
	BEGIN

	DELETE FGPS
	FROM facility.FacilityGraduationPlanStep FGPS
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FGPS.FacilityID
		)

	DELETE FIC
	FROM facility.FacilityInclusionCriteria FIC
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FIC.FacilityID
		)

	END
--ENDIF

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO facility.FacilityGraduationPlanStep
		(FacilityID, GraduationPlanStepID)
	SELECT
		I.FacilityID,
		GPS.GraduationPlanStepID
	FROM INSERTED I, dropdown.GraduationPlanStep GPS
	WHERE GPS.GraduationPlanStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM facility.FacilityGraduationPlanStep FGPS
			WHERE FGPS.FacilityID = I.FacilityID
			)

	INSERT INTO facility.FacilityInclusionCriteria
		(FacilityID, InclusionCriteriaID)
	SELECT
		I.FacilityID,
		IC.InclusionCriteriaID
	FROM INSERTED I, dropdown.InclusionCriteria IC
	WHERE IC.InclusionCriteriaID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM facility.FacilityInclusionCriteria FIC
			WHERE FIC.FacilityID = I.FacilityID
			)

	END
--ENDIF
GO		
--End trigger facility.TR_Facility
--End table facility.Facility