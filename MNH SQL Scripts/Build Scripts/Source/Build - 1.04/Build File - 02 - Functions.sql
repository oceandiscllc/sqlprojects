USE MNH
GO

--Begin function eventlog.GetFirstCreateDateTime
EXEC utility.DropObject 'eventlog.GetFirstCreateDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.16
-- Description:	A function to return a the first CreateDateTime for a specific entitytypecode and entityid from the eventlog.EventLog table
-- ========================================================================================================================================

CREATE FUNCTION eventlog.GetFirstCreateDateTime
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS DATETIME

AS
BEGIN
	
	DECLARE @dCreateDateTime DATETIME

	SELECT TOP 1 @dCreateDateTime = EL.CreateDateTime
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','update')
	ORDER BY EL.EventLogID
	
	RETURN @dCreateDateTime

END
GO
--End function eventlog.GetFirstCreateDateTime

--Begin function eventlog.GetLastCreateDateTime
EXEC utility.DropObject 'eventlog.GetLastCreateDateTime'
EXEC utility.DropObject 'eventlog.GetLastLogDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.16
-- Description:	A function to return a the last CreateDateTime for a specific entitytypecode and entityid from the eventlog.EventLog table
-- =======================================================================================================================================

CREATE FUNCTION eventlog.GetLastCreateDateTime
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS DATETIME

AS
BEGIN
	
	DECLARE @dCreateDateTime DATETIME

	SELECT TOP 1 @dCreateDateTime = EL.CreateDateTime
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','update')
	ORDER BY EL.EventLogID DESC
	
	RETURN @dCreateDateTime

END
GO
--End function eventlog.GetLastCreateDateTime

