USE MNH
GO

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table document.DocumentEntity

--Begin table dropdown.InclusionEligibilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionEligibilityStatus'

EXEC utility.AddColumn @TableName, 'InclusionEligibilityStatusCode', 'VARCHAR(50)'
GO
--End table dropdown.InclusionEligibilityStatus

