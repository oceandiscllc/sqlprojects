USE MNH
GO

--Begin table dropdown.InclusionEligibilityStatus
UPDATE IES
SET IES.InclusionEligibilityStatusCode = IES.InclusionEligibilityStatusName
FROM dropdown.InclusionEligibilityStatus IES
GO
--End table dropdown.InclusionEligibilityStatus

