USE MNH
GO

--Begin table document.Document
IF utility.HasColumn('document.Document', 'DocumentName') = 1
	EXEC sp_RENAME 'document.Document.DocumentName', 'DocumentGUID', 'COLUMN'
--ENDIF	
GO

IF utility.HasColumn('document.Document', 'Thumbnail') = 1
	EXEC sp_RENAME 'document.Document.Thumbnail', 'ThumbnailData', 'COLUMN';
--ENDIF	
GO

DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropColumn @TableName, 'IntegrationID'
EXEC utility.DropColumn @TableName, 'IntegrationCode'

EXEC utility.AddColumn @TableName, 'ThumbnailSize', 'BIGINT', '0'
EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table document.Document

--Begin table dropdown.InclusionEligibilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionEligibilityStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InclusionEligibilityStatus
	(
	InclusionEligibilityStatusID INT IDENTITY(0,1) NOT NULL,
	InclusionEligibilityStatusCode VARCHAR(50),
	InclusionEligibilityStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InclusionEligibilityStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InclusionEligibilityStatus', 'DisplayOrder,InclusionEligibilityStatusName', 'InclusionEligibilityStatusID'
GO
--End table dropdown.InclusionEligibilityStatus

--Begin table dropdown.QualityStandardLookup
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardLookup'

EXEC utility.AddColumn @TableName, 'HasChildren', 'BIT', '0'
GO
--End table dropdown.QualityStandardLookup

--Begin table dropdown.QualityStandard
EXEC utility.DropObject 'dropdown.TR_QualityStandard'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.09
-- Description:	A trigger to populate the dropdown.QualityStandardLookup table
-- ===========================================================================
CREATE TRIGGER dropdown.TR_QualityStandard ON dropdown.QualityStandard AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	TRUNCATE TABLE dropdown.QualityStandardLookup

	;
	WITH HD (QualityStandardID,ParentQualityStandardID,EntityTypeCode,NodeLevel)
		AS 
		(
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID,
			T.EntityTypeCode, 
			1 
		FROM dropdown.QualityStandard T
		WHERE T.ParentQualityStandardID = 0
			AND T.QualityStandardID > 0
		
		UNION ALL
			
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID, 
			T.EntityTypeCode, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dropdown.QualityStandard T 
			JOIN HD ON HD.QualityStandardID = T.ParentQualityStandardID 
		)
		
	INSERT INTO dropdown.QualityStandardLookup
		(EntityTypeCode, QualityStandardID, ManualID, ChapterID, StandardID, ObjectiveElementID, VerificationCriteriaID, HasChildren)
	SELECT
		A.EntityTypeCode, 
		A.QualityStandardID,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(E.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 1
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 4
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 5
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN EXISTS (SELECT 1 FROM HD F WHERE F.ParentQualityStandardID = A.QualityStandardID)
			THEN 1
			ELSE 0
		END

	FROM HD A
		LEFT JOIN HD B ON B.QualityStandardID = A.ParentQualityStandardID
		LEFT JOIN HD C ON C.QualityStandardID = B.ParentQualityStandardID
		LEFT JOIN HD D ON D.QualityStandardID = C.ParentQualityStandardID
		LEFT JOIN HD E ON E.QualityStandardID = D.ParentQualityStandardID

	UPDATE QSL
	SET
		QSL.ManualName = QSM.EntityName,
		QSL.ChapterName = QSC.EntityName,
		QSL.StandardName = QSS.EntityName,
		QSL.ObjectiveElementName = QSOE.EntityName,
		QSL.VerificationCriteriaName = QSVC.EntityName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QSM ON QSM.QualityStandardID = QSL.ManualID
		JOIN dropdown.QualityStandard QSC ON QSC.QualityStandardID = QSL.ChapterID
		JOIN dropdown.QualityStandard QSS ON QSS.QualityStandardID = QSL.StandardID
		JOIN dropdown.QualityStandard QSOE ON QSOE.QualityStandardID = QSL.ObjectiveElementID
		JOIN dropdown.QualityStandard QSVC ON QSVC.QualityStandardID = QSL.VerificationCriteriaID

	END
--ENDIF
GO

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.AddColumn @TableName, 'AuthorizerPersonID', 'INT', '0'
GO

--Begin trigger facility.TR_Facility
EXEC utility.DropObject 'facility.TR_Facility'
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09..5
-- Description:	A trigger to update the facility.Facility tables
-- ========================================================================
CREATE TRIGGER facility.TR_Facility ON facility.Facility AFTER INSERT, DELETE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM DELETED)
	BEGIN

	DELETE FGPS
	FROM facility.FacilityGraduationPlanStep 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FGPS.FacilityID
		)

	DELETE FIC
	FROM facility.FacilityInclusionCriteria 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FIC.FacilityID
		)

	END
--ENDIF

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO facility.FacilityGraduationPlanStep
		(FacilityID, GraduationPlanStepID)
	SELECT
		I.FacilityID,
		GPS.GraduationPlanStepID
	FROM INSERTED I, dropdown.GraduationPlanStep GPS
	WHERE GPS.GraduationPlanStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM facility.FacilityGraduationPlanStep FGPS
			WHERE FGPS.FacilityID = I.FacilityID
			)

	INSERT INTO facility.FacilityInclusionCriteria
		(FacilityID, InclusionCriteriaID)
	SELECT
		I.FacilityID,
		IC.InclusionCriteriaID
	FROM INSERTED I, dropdown.InclusionCriteria IC
	WHERE IC.InclusionCriteriaID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM facility.FacilityInclusionCriteria FIC
			WHERE FIC.FacilityID = I.FacilityID
			)

	END
--ENDIF
GO		
--End trigger facility.TR_Facility
--End table facility.Facility

--Begin table facility.FacilityInclusionCriteria
DECLARE @TableName VARCHAR(250) = 'facility.FacilityInclusionCriteria'

EXEC utility.AddColumn @TableName, 'IsAuthorized', 'BIT', '0'

EXEC utility.SetDefaultConstraint 'facility.FacilityInclusionCriteria', 'IsCriteriaMet', 'BIT', '0'
GO
--End table facility.FacilityInclusionCriteria

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.AddColumn @TableName, 'IsScored', 'BIT', '0'
GO
--End table visit.VisitQualityStandard
