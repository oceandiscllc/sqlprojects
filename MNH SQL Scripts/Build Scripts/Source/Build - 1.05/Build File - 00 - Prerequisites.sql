/* Build File - 00 - Prerequisites */
USE MNH
GO

EXEC utility.DropObject 'logicalframework.Indicator'
EXEC utility.DropObject 'logicalframework.Milestone'
EXEC utility.DropObject 'logicalframework.Objective'
EXEC utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
EXEC utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
EXEC utility.DropObject 'logicalframework.GetIntermediateOutcomeChartData'
EXEC utility.DropObject 'logicalframework.GetMilestoneByIndicatorID'
EXEC utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
EXEC utility.DropObject 'logicalframework.GetMilestoneDataByIndicatorID'
EXEC utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
EXEC utility.DropObject 'logicalframework.GetObjectiveByParentObjectiveID'

EXEC utility.DropSchema 'activity'
EXEC utility.DropSchema 'activityreport'
EXEC utility.DropSchema 'aggregator'
EXEC utility.DropSchema 'api'
EXEC utility.DropSchema 'asset'
EXEC utility.DropSchema 'atmospheric'
EXEC utility.DropSchema 'campaign'
EXEC utility.DropSchema 'course'
EXEC utility.DropSchema 'distributor'
EXEC utility.DropSchema 'facebook'
EXEC utility.DropSchema 'finding'
EXEC utility.DropSchema 'force'
EXEC utility.DropSchema 'impactstory'
EXEC utility.DropSchema 'implementer'
EXEC utility.DropSchema 'integration'
EXEC utility.DropSchema 'logicalframework'
EXEC utility.DropSchema 'mediareport'
EXEC utility.DropSchema 'metrics'
EXEC utility.DropSchema 'portalupdate'
EXEC utility.DropSchema 'procurement'
EXEC utility.DropSchema 'product'
EXEC utility.DropSchema 'productdistributor'
EXEC utility.DropSchema 'programreport'
EXEC utility.DropSchema 'recommendation'
EXEC utility.DropSchema 'spotreport'
EXEC utility.DropSchema 'survey'
EXEC utility.DropSchema 'temp'
EXEC utility.DropSchema 'territoryupdate'
EXEC utility.DropSchema 'training'
EXEC utility.DropSchema 'trendreport'
EXEC utility.DropSchema 'workflow'

--Begin function utility.HasColumn
EXEC utility.DropObject 'utility.HasColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.09
-- Description:	A function to return a bit indicating if a column exists in a table
-- ================================================================================

CREATE FUNCTION utility.HasColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250)
)

RETURNS BIT

AS
BEGIN

	DECLARE @bHasColumn BIT = 0

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		SET @bHasColumn = 1
	--ENDIF

	RETURN @bHasColumn

END
GO
--End function utility.HasColumn
