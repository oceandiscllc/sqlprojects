USE MNH
GO

--Begin table dropdown.InclusionEligibilityStatus
TRUNCATE TABLE dropdown.InclusionEligibilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.InclusionEligibilityStatus', 'InclusionEligibilityStatusID', 0
GO

INSERT INTO dropdown.InclusionEligibilityStatus 
	(InclusionEligibilityStatusCode, InclusionEligibilityStatusName, DisplayOrder)
VALUES
	('No', 'No', 1),
	('Pending', 'Pending', 2),
	('Yes', 'Yes', 3)
GO
--End table dropdown.InclusionEligibilityStatus

DELETE MI
FROM core.MenuItem MI
WHERE MI.MenuItemCode IN ('InclusionCriteriaList', 'MoUList')
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('InclusionCriteria', 'MoU')
GO

DELETE PTP
FROM permissionable.PermissionableTemplatePermissionable PTP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

DELETE MIPL
FROM core.MenuItemPermissionableLineage MIPL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM core.MenuItem MI
	WHERE MI.MenuItemID = MIPL.MenuItemID
	)
	OR NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = MIPL.PermissionableLineage
		)
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Allows access to the authorized checkbox on the inclusion criteria tab for inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate.CanAuthorizeInclusionCriteria', @PERMISSIONCODE='CanAuthorizeInclusionCriteria';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
