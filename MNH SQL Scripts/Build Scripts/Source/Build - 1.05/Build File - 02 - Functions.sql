USE MNH
GO

--Begin function facility.GetFacilityInclusionEligibilityStatusID
EXEC utility.DropObject 'facility.GetFacilityInclusionEligibilityStatus'
EXEC utility.DropObject 'facility.GetFacilityInclusionEligibilityStatusID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.29
-- Description:	A function to get the inclusion eligibility status for a facility
-- ==============================================================================

CREATE FUNCTION facility.GetFacilityInclusionEligibilityStatusID
(
@FacilityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nInclusionEligibilityStatusID INT

	SELECT @nInclusionEligibilityStatusID = 
		CASE
			WHEN @FacilityID = 0 OR NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = @FacilityID AND FIC.IsCriteriaMet = 1)
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
			WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = @FacilityID AND FIC.IsCriteriaMet = 0)
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Yes')
			ELSE (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Pending')
		END

	RETURN ISNULL(@nInclusionEligibilityStatusID, 0)

END
GO
--End function facility.GetFacilityInclusionEligibilityStatusID

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO
--End function person.CheckFileAccess

--Begin function visit.GetPriorVisitQualityStandardScore
EXEC utility.DropObject 'visit.GetPriorVisitQualityStandardScore'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.29
-- Description:	A function to return a the previous score for a specific facility and quality standard
-- ===================================================================================================

CREATE FUNCTION visit.GetPriorVisitQualityStandardScore
(
@QualityStandardID INT,
@VisitID INT
)

RETURNS INT

AS
BEGIN
	
	DECLARE @nPriorScore INT

	SELECT TOP 1 @nPriorScore = VQS.Score
	FROM visit.VisitQualityStandard VQS
		JOIN visit.Visit V1 ON V1.VisitID = VQS.VisitID
			AND VQS.QualityStandardID = @QualityStandardID
			AND V1.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
			AND V1.VisitID <> @VisitID
	ORDER BY VQS.VisitQualityStandardID DESC

	RETURN ISNULL(@nPriorScore, 0)

END
GO
--End function visit.GetPriorVisitQualityStandardScore

