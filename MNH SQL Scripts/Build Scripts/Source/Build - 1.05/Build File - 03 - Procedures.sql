USE MNH
GO

--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentTitle, 
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure dropdown.GetInclusionEligibilityStatusData
EXEC Utility.DropObject 'dropdown.GetInclusionEligibilityStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A stored procedure to return data from the dropdown.InclusionEligibilityStatus table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetInclusionEligibilityStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionEligibilityStatusID, 
		T.InclusionEligibilityStatusName
	FROM dropdown.InclusionEligibilityStatus T
	WHERE (T.InclusionEligibilityStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionEligibilityStatusName, T.InclusionEligibilityStatusID

END
GO
--End procedure dropdown.GetInclusionEligibilityStatusData

--Begin procedure dropdown.GetQualityStandardLookupDataByQualityStandardIDList
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataQualityStandardIDList'
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataByQualityStandardIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to return data from the dropdown.QualityStandardLookup table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupDataByQualityStandardIDList

@QualityStandardIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		QSL.ManualName,
		QSL.ChapterName,
		QSL.StandardName,
		QSL.ObjectiveElementName,
		QSL.VerificationCriteriaName,
		QSL.QualityStandardID
	FROM dropdown.QualityStandardLookup QSL
		JOIN core.ListToTable(@QualityStandardIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = QSL.QualityStandardID
			AND QSL.HasChildren = 0

END
GO
--End procedure dropdown.GetQualityStandardLookupDataByQualityStandardIDList

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.AuthorizerPersonID,
		person.FormatPersonNameByPersonID(F.AuthorizerPersonID, 'LastFirstTitle') AS AuthorizerPersonNameFormatted,		
		F.BedCount,
		F.Description, 			
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.FacilityID,
		core.FormatDate(eventlog.GetFirstCreateDateTime('Facility', F.FacilityID)) AS FacilityCreateDateFormatted,
		core.FormatDate(eventlog.GetLastCreateDateTime('Facility', F.FacilityID)) AS FacilityUpdateDateFormatted,
		F.FacilityName,
		F.InclusionCriteriaNotes,
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = facility.GetFacilityInclusionEligibilityStatusID(@FacilityID)
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	IF @FacilityID = 0
		BEGIN

		SELECT
			NULL AS AssessmentDate, 
			NULL AS AssessmentDateFormatted,
			0 AS FacilityInclusionCriteriaID, 
			0 AS IsAuthorized,
			0 AS IsCriteriaMet, 
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM dropdown.InclusionCriteria IC
		WHERE IC.InclusionCriteriaID > 0
			AND IC.IsActive = 1
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	ELSE
		BEGIN

		SELECT
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.AssessmentDate ELSE NULL END AS AssessmentDate, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN core.FormatDate(FIC.AssessmentDate) ELSE NULL END AS AssessmentDateFormatted,
			FIC.FacilityInclusionCriteriaID, 
			FIC.IsAuthorized,
			FIC.IsCriteriaMet, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.PersonID ELSE 0 END AS PersonID,
			CASE FIC.IsCriteriaMet WHEN 1 THEN person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') ELSE NULL END AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM facility.FacilityInclusionCriteria FIC
			JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
			JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
				AND FIC.FacilityID = @FacilityID
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	--ENDIF

	--FacilityFacilityInclusionCriteriaDocument
	SELECT
		REPLACE(DE.EntityTypeSubCode, 'InclusionCriteria', '') AS InclusionCriteriaID,
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode LIKE 'InclusionCriteria%' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID

	--FacilityMOUDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode = 'MOU' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.GetFacilityMOUByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityMOUByFacilityID'
GO
--End procedure facility.GetFacilityMOUByFacilityID

--Begin procedure facility.UpdateInclusionEligibilityStatus
EXEC Utility.DropObject 'facility.UpdateInclusionEligibilityStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.22
-- Description:	A stored procedure to set the InclusionEligibilityStatusID column 
-- ==============================================================================
CREATE PROCEDURE facility.UpdateInclusionEligibilityStatus

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE F
	SET F.InclusionEligibilityStatusID = 
		CASE
			WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F.FacilityID AND FIC.IsCriteriaMet = 1)
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
			WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F.FacilityID AND FIC.IsCriteriaMet = 0)
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Yes')
			ELSE (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Pending')
		END
	FROM facility.Facility F
	WHERE F.FacilityID = @FacilityID

END
GO
--End procedure facility.UpdateInclusionEligibilityStatus

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		V.IsPlanningComplete,
		V.IsVisitComplete,

		CASE 
			WHEN V.IsPlanningComplete = 0
			THEN 'Planning'
			WHEN V.IsVisitComplete = 1
			THEN 'Visit Complete'
			ELSE 'Planning Complete'
		END AS VisitStatusName,

		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitID, 	
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitStartDateTime,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT 
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.Score,
		VQS.IsScored,
		visit.GetPriorVisitQualityStandardScore(QSL.QualityStandardID, @VisitID) AS PriorScore
	FROM dropdown.QualityStandardLookup QSL
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT 
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.IsScored,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
	WHERE VQS.VisitID = @VisitID
		AND VQS.QualityStandardID = @QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode,
		AET.AssessmentEvidenceTypeDataType
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	SELECT
		ISNULL((
			SELECT
				D.DocumentTitle,
				DE.DocumentEntityCode
			FROM document.DocumentEntity DE
				JOIN document.Document D ON D.DocumentID = DE.DocumentID
					AND DE.EntityTypeCode = 'VisitQualityStandard'
					AND DE.EntityID = 
						(
						SELECT VQS.VisitQualityStandardID 
						FROM visit.VisitQualityStandard VQS
						WHERE VQS.VisitID = @VisitID
							AND VQS.QualityStandardID = @QualityStandardID
						)
			FOR JSON PATH
		), '[]') AS UploadedFiles

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID