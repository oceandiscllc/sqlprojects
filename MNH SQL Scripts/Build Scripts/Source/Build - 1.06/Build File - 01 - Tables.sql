USE MNH
GO

EXEC utility.AddSchema 'webapi'
GO

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.AddColumn @TableName, 'VisitLeadPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'VisitTime', 'NUMERIC(5,2)', '0'
GO
--End table visit.Visit

--Begin table dropdown.AssessmentEvidenceType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssessmentEvidenceType'

EXEC utility.DropColumn @TableName, 'AssessmentEvidenceTypeDataType'
GO
--End table dropdown.AssessmentEvidenceType

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.DropDefaultConstraint @TableName, 'HasObservation'
EXEC utility.DropDefaultConstraint @TableName, 'HasPhysicalVerification'
EXEC utility.DropDefaultConstraint @TableName, 'HasProviderInterview'

ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasObservation INT
ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasPhysicalVerification INT
ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasProviderInterview INT

EXEC utility.SetDefaultConstraint @TableName, 'HasObservation', 'INT', '-1', 1
EXEC utility.SetDefaultConstraint @TableName, 'HasPhysicalVerification', 'INT', '-1', 1
EXEC utility.SetDefaultConstraint @TableName, 'HasProviderInterview', 'INT', '-1', 1
GO
--End table visit.VisitQualityStandard

--Begin table webapi.Application
DECLARE @TableName VARCHAR(250) = 'webapi.Application'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.Application
	(
	ApplicationID INT IDENTITY(1,1) NOT NULL,
	ApplicationKey VARCHAR(36),
	ApplicationSecretKey VARCHAR(36),
	ApplicationName VARCHAR(50),
	CreateDateTime DATETIME,
	UpdateDateTime DATETIME,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationKey', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationSecretKey', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Application', 'ApplicationName', 'ApplicationID'
GO
--End table webapi.Application

--Begin table webapi.TransactionStatus
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionStatus'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionStatus
	(
	TransactionStatusID INT IDENTITY(1,1) NOT NULL,
	TransactionStatusName VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionStatus', 'TransactionStatusName', 'TransactionStatusID'
GO
--End table webapi.TransactionType

--Begin table webapi.TransactionType
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionType'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionType
	(
	TransactionTypeID INT IDENTITY(1,1) NOT NULL,
	TransactionTypeName VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionType', 'TransactionTypeName', 'TransactionTypeID'
GO
--End table webapi.TransactionType

--Begin table webapi.TransactionLog
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionLog'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionLog
	(
	TransactionLogID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	TransactionGUID VARCHAR(36),
	TransactionTypeID INT,
	TransactionStatusID INT,
	TransactionDetail VARCHAR(MAX),
	TransactionPayload VARCHAR(MAX),
	TransactionContentType VARCHAR(25),
	TransactionContentSubType VARCHAR(25),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionGUID', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionStatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionLogID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionLog', 'ApplicationID', 'TransactionLogID'
GO
--End table webapi.TransactionLog