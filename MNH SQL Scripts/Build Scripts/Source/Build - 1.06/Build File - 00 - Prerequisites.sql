/* Build File - 00 - Prerequisites */
USE MNH
GO

--Begin procedure utility.DropDefaultConstraint
EXEC utility.DropObject 'utility.DropDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
	FROM sys.Default_Constraints DC 
		JOIN sys.Objects O ON O.Object_ID = DC.Parent_Object_ID
			AND O.type = 'U' 
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID 
		JOIN sys.Columns C ON C.Object_ID = O.Object_ID 
			AND C.Column_ID = DC.Parent_Column_ID
			AND S.Name + '.' + O.Name = @TableName 
			AND C.Name = @ColumnName

	IF @cSQL IS NOT NULL
		EXECUTE (@cSQL)
	--ENDIF
		
END
GO
--End procedure utility.DropDefaultConstraint