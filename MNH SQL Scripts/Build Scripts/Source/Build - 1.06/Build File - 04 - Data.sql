USE MNH
GO

DECLARE @cEntityName VARCHAR(MAX) = '6.3.1 Explains danger signs and important care activities to mother and her companion'

IF NOT EXISTS (SELECT 1 FROM dropdown.QualityStandard QS WHERE QS.EntityName = @cEntityName)
	BEGIN

	INSERT INTO dropdown.QualityStandard
		(ParentQualityStandardID,EntityTypeCode,EntityName,DisplayOrder)
	SELECT
		QS.QualityStandardID,
		'Verification Criteria',
		@cEntityName,
		1
	FROM dropdown.QualityStandard QS
	WHERE QS.EntityTypeCode = 'Objective Element'
		AND QS.EntityName = '6.3 Explains danger signs and important care activities to mother and her companion'

	END
--ENDIF

DELETE QSAET
FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
	JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = QSAET.QualityStandardID
		AND QS.EntityTypeCode = 'Verification Criteria'
		AND QS.EntityName = @cEntityName

INSERT INTO dropdown.QualityStandardAssessmentEvidenceType
	(QualityStandardID, AssessmentEvidenceTypeID)
SELECT
	QS.QualityStandardID,
	AET.AssessmentEvidenceTypeID
FROM dropdown.AssessmentEvidenceType AET, dropdown.QualityStandard QS
WHERE QS.EntityTypeCode = 'Verification Criteria'
	AND QS.EntityName = @cEntityName
	AND AET.AssessmentEvidenceTypeCode IN ('HasObservation', 'HasProviderInterview')
GO

TRUNCATE TABLE webapi.Application
GO

INSERT INTO webapi.Application
	(ApplicationName)
VALUES 
	('MNH Mobile BuildKey'),
	('MNH WebApi')
GO

TRUNCATE TABLE webapi.TransactionStatus
GO

INSERT INTO webapi.TransactionStatus
	(TransactionStatusName)
VALUES
	('OK'),
	('ERROR'),
	('TIMEOUT'),
	('WARN')
GO

TRUNCATE TABLE webapi.TransactionType
GO

INSERT INTO webapi.TransactionType
	(TransactionTypeName)
VALUES 
	('HTTP-POST'),
	('HTTP-PUT'),
	('HTTPS-POST'),
	('HTTPS-PUT'),
	('SFTP-PULL'),
	('SFTP-PUSH')
GO

DECLARE @cSystemSetupValue VARCHAR(MAX) = (SELECT A.ApplicationKey FROM webapi.Application A WHERE ApplicationName = 'MNH WebApi')
EXEC core.SystemSetupAddUpdate 'WebApiApplicationKey', NULL, @cSystemSetupValue
GO

DECLARE @cSystemSetupValue VARCHAR(MAX) = (SELECT A.ApplicationSecretKey FROM webapi.Application A WHERE ApplicationName = 'MNH WebApi')
EXEC core.SystemSetupAddUpdate 'WebApiApplicationSecretKey', NULL, @cSystemSetupValue
GO

EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/mnhnewlogo.jpg'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/mnhnewlogo.jpg'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/mnhnewlogo.jpg'
GO