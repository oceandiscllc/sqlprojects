USE MNH
GO

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'DateOfBirth', 'DATE'
EXEC utility.AddColumn @TableName, 'FacilityEndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FacilityStartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'GenderID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'OtherContactFunction', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'OtherContactRole', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'YearsExperience', 'INT', '0'
GO
--End table contact.Contact

--Begin table dropdown.Gender
DECLARE @TableName VARCHAR(250) = 'dropdown.Gender'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Gender
	(
	GenderID INT IDENTITY(0,1) NOT NULL,
	GenderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'GenderID'
EXEC utility.SetIndexClustered @TableName, 'IX_Gender', 'DisplayOrder,GenderName'
GO
--End table dropdown.Gender

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.AddColumn @TableName, 'BedCount', 'INT', '0'
EXEC utility.AddColumn @TableName, 'Phone', 'NVARCHAR(15)'
EXEC utility.AddColumn @TableName, 'FacilityEstablishedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'IsAbortionProvider', 'BIT', '0'
GO
--End table facility.Facility
