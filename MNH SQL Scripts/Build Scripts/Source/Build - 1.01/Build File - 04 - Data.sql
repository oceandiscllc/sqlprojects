USE MNH
GO

--Begin table dropdown.ContactFunction
TRUNCATE TABLE dropdown.ContactFunction
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactFunction', 'ContactFunctionID', 0
GO

INSERT INTO dropdown.ContactFunction 
	(ContactFunctionName, DisplayOrder) 
VALUES
	('MBBS (Bachelor of medicine/Bachelor of surgery)', '1'),
	('MD/MS - DNB/DCH', '2'),
	('ObGyn/DGO', '3'),
	('DCH', '4'),
	('B.Pharma (Bachelor of pharmacy )', '5'),
	('B.Sc Nursing', '6'),
	('CSSD Technician', '7'),
	('OT Technician', '8'),
	('MPharma', '9'),
	('Other (with free entry field if selected)', '10')
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
TRUNCATE TABLE dropdown.ContactRole
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactRole', 'ContactRoleID', 0
GO

INSERT INTO dropdown.ContactRole 
	(ContactRoleName, DisplayOrder) 
VALUES
	('Owner', '1'),
	('Nurse in Charge', '2'),
	('Doctor', '3'),
	('Gynaecologist', '4'),
	('Radiologist', '5'),
	('Paediatrician', '6'),
	('GNM', '7'),
	('ANM', '8'),
	('Anaesthetist', '9'),
	('Pathologist', '10'),
	('Lab technician', '11'),
	('Sanitary inspector', '12'),
	('CSSD Technician', '13'),
	('OT Technician', '14'),
	('Other (with free text entry field)', '15')
GO
--End table dropdown.ContactRole

--Begin table dropdown.DIBRole
TRUNCATE TABLE dropdown.DIBRole
GO

EXEC utility.InsertIdentityValue 'dropdown.DIBRole', 'DIBRoleID', 0
GO

INSERT INTO dropdown.DIBRole 
	(DIBRoleName, DisplayOrder) 
VALUES
	('NABH Nodal Person', '1'),
	('FOGSI Nodal Person', '2'),
	('Primary POC', '3')
GO
--End table dropdown.DIBRole

--Begin table dropdown.FacilityStatus
TRUNCATE TABLE dropdown.FacilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityStatus', 'FacilityStatusID', 0
GO

INSERT INTO dropdown.FacilityStatus 
	(FacilityStatusName, DisplayOrder) 
VALUES
	('Progressive', '1'),
	('JQS Mapping', '2'),
	('Verified', '3')
GO
--End table dropdown.FacilityStatus

--Begin table dropdown.FacilityService
TRUNCATE TABLE dropdown.FacilityService
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityService', 'FacilityServiceID', 0
GO

INSERT INTO dropdown.FacilityService 
	(FacilityServiceName, DisplayOrder) 
VALUES
	('Multi-Speciality Facility', '1'),
	('MCH Services Facility', '2')
GO
--End table dropdown.FacilityService

--Begin table dropdown.Gender
TRUNCATE TABLE dropdown.Gender
GO

EXEC utility.InsertIdentityValue 'dropdown.Gender', 'GenderID', 0
GO

INSERT INTO dropdown.Gender 
	(GenderName, DisplayOrder) 
VALUES
	('Male', '1'),
	('Female', '2'),
	('Transgender', '3')
GO
--End table dropdown.Gender

--Begin table dropdown.MOUStatus
TRUNCATE TABLE dropdown.MOUStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.MOUStatus', 'MOUStatusID', 0
GO

INSERT INTO dropdown.MOUStatus 
	(MOUStatusName, DisplayOrder) 
VALUES
	('In Draft', '1'),
	('Ready for signature', '2'),
	('Signed', '3'),
	('Dropped-Out', '4')
GO
--End table dropdown.MOUStatus

UPDATE core.SystemSetup
SET SystemSetupValue = '/assets/img/rmn-logo.png'
WHERE SystemSetupKey LIKE 'SiteLogo%'
