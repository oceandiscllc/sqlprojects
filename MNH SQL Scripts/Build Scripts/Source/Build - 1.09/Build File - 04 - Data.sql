USE MNH
GO

--Begin table dropdown.FacilityService
TRUNCATE TABLE dropdown.FacilityService
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityService', 'FacilityServiceID', 0
GO

INSERT INTO dropdown.FacilityService 
	(FacilityServiceCode, FacilityServiceName, DisplayOrder) 
VALUES
	('MultiSpeciality', 'Multi-Speciality Facility', '1'),
	('MCHServices', 'MCH Services Facility', '2'),
	('Other', 'Other', '3')
GO
--End table dropdown.FacilityService

--Begin table facility.Facility
UPDATE F
SET F.FacilityServiceID = ISNULL((SELECT TOP 1 FS.FacilityServiceID FROM facility.FacilityFacilityService FS WHERE FS.FacilityID = F.FacilityID), 0)
FROM facility.Facility F
GO
--End table facility.Facility

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Allow changes to the facility active / inactive flag', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate.CanSetActiveStatus', @PERMISSIONCODE='CanSetActiveStatus';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
