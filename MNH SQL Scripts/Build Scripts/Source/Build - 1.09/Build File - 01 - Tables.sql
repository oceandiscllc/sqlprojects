USE MNH
GO

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.AddColumn @TableName, 'FacilityServiceID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '1'
GO
--End table facility.Facility

--Begin table dropdown.FacilityService
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityService'

EXEC utility.AddColumn @TableName, 'FacilityServiceCode', 'VARCHAR(50)'
GO
--End table dropdown.FacilityService

