USE MNH
GO

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function eventlog.GetPersonPermissionablesXMLByPersonID
EXEC utility.DropObject 'eventlog.GetPersonPermissionablesXMLByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A function to return PersonPermissionable data for a specific Person record
-- ========================================================================================

CREATE FUNCTION eventlog.GetPersonPermissionablesXMLByPersonID
(
@PersonID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
	
	SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
		FROM person.PersonPermissionable T 
		WHERE T.PersonID = @PersonID
		) D

	RETURN '<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>'

END

GO
--End function eventlog.GetPersonPermissionablesXMLByPersonID

--Begin function facility.GetFacilityInclusionCriteriaMetCount
EXEC utility.DropObject 'facility.GetFacilityInclusionCriteriaMetCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A function to data from the facility.FacilityInclusionCriteria table
-- =================================================================================

CREATE FUNCTION facility.GetFacilityInclusionCriteriaMetCount
(
@FacilityID INT
)

RETURNS VARCHAR(20)

AS
BEGIN
	
	DECLARE @nIsMetCount INT

	;
	WITH FICD AS
		(
		SELECT 
			COUNT(FIC.InclusionCriteriaID) AS IsCriteriaMetCount,
			FIC.IsCriteriaMet AS IsCriteriaMet
		FROM facility.FacilityInclusionCriteria FIC
		WHERE FIC.FacilityID = @FacilityID
		GROUP BY FIC.IsCriteriaMet
		)

	SELECT @nIsMetCount = FICD.IsCriteriaMetCount FROM FICD WHERE FICD.IsCriteriaMet = 1

	RETURN CAST(ISNULL(@nIsMetCount, 0) AS VARCHAR(5)) + ' of ' + CAST((SELECT COUNT(FIC.FacilityInclusionCriteriaID) FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = @FacilityID) AS VARCHAR(5))

END
GO
--End function facility.GetFacilityInclusionCriteriaMetCount

--Begin function facility.GetLastFacilityInclusionCriteriaAssessmentDate
EXEC utility.DropObject 'facility.GetLastFacilityInclusionCriteriaAssessmentDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A function to data from the facility.FacilityInclusionCriteria table
-- =================================================================================

CREATE FUNCTION facility.GetLastFacilityInclusionCriteriaAssessmentDate
(
@FacilityID INT
)

RETURNS DATE

AS
BEGIN
	
	DECLARE @dAssessmentDate DATE

	SELECT @dAssessmentDate = MAX(FIC.AssessmentDate)
	FROM facility.FacilityInclusionCriteria FIC
	WHERE FIC.FacilityID = @FacilityID

	RETURN @dAssessmentDate

END
GO
--End function facility.GetLastFacilityInclusionCriteriaAssessmentDate
