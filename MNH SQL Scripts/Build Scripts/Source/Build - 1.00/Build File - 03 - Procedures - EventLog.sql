/* Build File - 03 - Procedures - EventLog */
USE MNH
GO

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.05
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Contact'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM contact.Contact T
		WHERE T.ContactID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogFacilityAction
EXEC utility.DropObject 'eventlog.LogFacilityAction'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.09.04
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFacilityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Facility',
			T.FacilityID,
			@Comments,
			@ProjectID
		FROM facility.Facility T
		WHERE T.FacilityID = @EntityID

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogFacilityActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogFacilityActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogFacilityActionTable
		FROM facility.Facility T
		WHERE T.FacilityID = @EntityID
		
		ALTER TABLE #LogFacilityActionTable DROP COLUMN Location

		DECLARE @cFacilityGraduationPlanStep VARCHAR(MAX) = ''
		DECLARE @cFacilityInclusionCriteria VARCHAR(MAX) = ''
		DECLARE @cFacilityMOUContact VARCHAR(MAX) = ''
		
		SELECT @cFacilityGraduationPlanStep = COALESCE(@cFacilityGraduationPlanStep, '') + D.FacilityGraduationPlanStep
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityGraduationPlanStep'), ELEMENTS) AS FacilityGraduationPlanStep
			FROM facility.FacilityGraduationPlanStep T 
			WHERE T.FacilityID = @EntityID
			) D
		
		SELECT @cFacilityInclusionCriteria = COALESCE(@cFacilityInclusionCriteria, '') + D.FacilityInclusionCriteria
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityInclusionCriterion'), ELEMENTS) AS FacilityInclusionCriteria
			FROM facility.FacilityInclusionCriteria T 
			WHERE T.FacilityID = @EntityID
			) D
		
		SELECT @cFacilityMOUContact = COALESCE(@cFacilityMOUContact, '') + D.FacilityMOUContact
		FROM
			(
			SELECT
				(SELECT T.ContactID FOR XML RAW(''), ELEMENTS) AS FacilityMOUContact
			FROM facility.FacilityMOUContact T 
			WHERE T.FacilityID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Facility',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(F.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<FacilityGraduationPlanSteps>' + @cFacilityGraduationPlanStep + '</FacilityGraduationPlanSteps>') AS XML),
			CAST(('<FacilityInclusionCriteria>' + @cFacilityInclusionCriteria + '</FacilityInclusionCriteria>') AS XML),
			CAST(('<FacilityMOUContact>' + @cFacilityMOUContact + '</FacilityMOUContact>') AS XML)
			FOR XML RAW('Facility'), ELEMENTS
			)
		FROM #LogFacilityActionTable T
			JOIN facility.Facility F ON F.FacilityID = T.FacilityID

		DROP TABLE #LogFacilityActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFacilityAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PermissionableTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPermissionableTemplatePermissionables VARCHAR(MAX) = ''
		
		SELECT @cPermissionableTemplatePermissionables = COALESCE(@cPermissionableTemplatePermissionables, '') + D.PermissionableTemplatePermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PermissionableTemplatePermissionable
			FROM permissionable.PermissionableTemplatePermissionable T 
			WHERE T.PermissionableTemplateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST('<PermissionableTemplatePermissionables>' + ISNULL(@cPermissionableTemplatePermissionables, '') + '</PermissionableTemplatePermissionables>' AS XML)
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T
		WHERE T.PermissionableTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction