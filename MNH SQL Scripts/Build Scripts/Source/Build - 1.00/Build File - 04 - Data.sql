/* Build File - 04 - Data */
USE MNH
GO

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Budget', 
	@EntityTypeName = 'Budget', 
	@EntityTypeNamePlural = 'Budgets',
	@SchemaName = 'budget', 
	@TableName = 'Budget', 
	@PrimaryKeyFieldName = 'BudgetID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Contact', 
	@EntityTypeName = 'Contact', 
	@EntityTypeNamePlural = 'Contacts',
	@SchemaName = 'contact', 
	@TableName = 'Contact', 
	@PrimaryKeyFieldName = 'ContactID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Facility', 
	@EntityTypeName = 'Facility', 
	@EntityTypeNamePlural = 'Facilities',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'GraduationPlanStep', 
	@EntityTypeName = 'Graduation Plan Step', 
	@EntityTypeNamePlural = 'Graduation Plan Steps',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'InclusionCriteria', 
	@EntityTypeName = 'Inclusion Criteria', 
	@EntityTypeNamePlural = 'Inclusion Criteria',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'MOU', 
	@EntityTypeName = 'MoU', 
	@EntityTypeNamePlural = 'MoUs',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'Core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

--Begin Top Level Items
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-map-marker',
	@NewMenuItemCode = 'Sites',
	@NewMenuItemText = 'Sites'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Sites',
	@Icon = 'fa fa-fw fa-car',
	@NewMenuItemCode = 'Visits',
	@NewMenuItemText = 'Visits'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Visits',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Administration'
GO
--End Top Level Items

--Begin Sites Items
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'FacilityList',
	@NewMenuItemLink = '/facility/list',
	@NewMenuItemText = 'Facilities',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'Facility.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'FacilityList',
	@NewMenuItemCode = 'ContactList',
	@NewMenuItemLink = '/contact/list',
	@NewMenuItemText = 'Contacts',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ContactList',
	@NewMenuItemCode = 'InclusionCriteriaList',
	@NewMenuItemLink = '/inclusioncriteria/list',
	@NewMenuItemText = 'Inclusion Criteria',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'InclusionCriteria.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'InclusionCriteriaList',
	@NewMenuItemCode = 'MoUList',
	@NewMenuItemLink = '/mou/list',
	@NewMenuItemText = 'MoUs',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'MOU.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'MoUList',
	@NewMenuItemCode = 'GraduationPlanStepList',
	@NewMenuItemLink = '/graduationplanstep/list',
	@NewMenuItemText = 'Graduation Plans',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'GraduationPlan.List'
GO
--End Sites Items

--Begin Admin Items
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO
--End Admin Items

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Sites'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationKey', NULL, 'DIUZQ1ICXUIMHKKK6TCX'
EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationSecretKey', NULL, 'MylcNoqlXVduD3LBn981ZHRY7OGOcKjsZ8LDTOTH'
EXEC core.SystemSetupAddUpdate 'DuoApiEndPoint', NULL, 'api-8a6e671f.duosecurity.com'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationKey', NULL, 'DIT96UH6EI8OVAUXJVT2'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationSecretKey', NULL, 'q7NjG8UbJzEMEDKNTH9m3FFBTSkEmAxUodib025p'
EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'GMapClientID', NULL, 'AIzaSyCq7wqiKnE1dBkm0z9oGK46tucTyM9ewJk'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@mnh.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '30'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://mnh.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'MNH'
EXEC core.SystemSetupAddUpdate 'TwoFactorEnabled', NULL, '0'
GO
--End table core.SystemSetup

--Begin table dropdown.ContactFunction
TRUNCATE TABLE dropdown.ContactFunction
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactFunction', 'ContactFunctionID', 0
GO

INSERT INTO dropdown.ContactFunction 
	(ContactFunctionName)
VALUES
	('Contact Function 1'),
	('Contact Function 2'),
	('Contact Function 3'),
	('Contact Function 4'),
	('Contact Function 5'),
	('Contact Function 6')
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
TRUNCATE TABLE dropdown.ContactRole
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactRole', 'ContactRoleID', 0
GO

INSERT INTO dropdown.ContactRole 
	(ContactRoleName)
VALUES
	('Contact Role 1'),
	('Contact Role 2'),
	('Contact Role 3'),
	('Contact Role 4'),
	('Contact Role 5'),
	('Contact Role 6')
GO
--End table dropdown.ContactRole

--Begin table dropdown.DIBRole
TRUNCATE TABLE dropdown.DIBRole
GO

EXEC utility.InsertIdentityValue 'dropdown.DIBRole', 'DIBRoleID', 0
GO

INSERT INTO dropdown.DIBRole 
	(DIBRoleName)
VALUES
	('DIB Role 1'),
	('DIB Role 2'),
	('DIB Role 3'),
	('DIB Role 4'),
	('DIB Role 5'),
	('DIB Role 6')
GO
--End table dropdown.DIBRole

--Begin table dropdown.FacilityService
TRUNCATE TABLE dropdown.FacilityService
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityService', 'FacilityServiceID', 0
GO

INSERT INTO dropdown.FacilityService 
	(FacilityServiceName)
VALUES
	('Facility Service 1'),
	('Facility Service 2'),
	('Facility Service 3'),
	('Facility Service 4'),
	('Facility Service 5'),
	('Facility Service 6')
GO
--End table dropdown.FacilityService

--Begin table dropdown.FacilityStatus
TRUNCATE TABLE dropdown.FacilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityStatus', 'FacilityStatusID', 0
GO

INSERT INTO dropdown.FacilityStatus 
	(FacilityStatusName)
VALUES
	('Facility Status 1'),
	('Facility Status 2'),
	('Facility Status 3'),
	('Facility Status 4'),
	('Facility Status 5'),
	('Facility Status 6')
GO
--End table dropdown.FacilityStatus

--Begin table dropdown.GraduationPlanStep
TRUNCATE TABLE dropdown.GraduationPlanStep
GO

EXEC utility.InsertIdentityValue 'dropdown.GraduationPlanStep', 'GraduationPlanStepID', 0
GO

INSERT INTO dropdown.GraduationPlanStep 
	(GraduationPlanStepName, DisplayOrder)
VALUES
	('Mapped', 1),
	('Assessed', 2),
	('Engaged (MoU Signed)', 3),
	('Progressive NABH', 4),
	('Progressive FOGSI', 5),
	('Verification of Progressive', 6),
	('JQS NABH', 7),
	('JQS FOGSI', 8),
	('Verification of JQS', 9)
GO
--End table dropdown.GraduationPlanStep

--Begin table dropdown.InclusionCriteria
TRUNCATE TABLE dropdown.InclusionCriteria
GO

EXEC utility.InsertIdentityValue 'dropdown.InclusionCriteria', 'InclusionCriteriaID', 0
GO

INSERT INTO dropdown.InclusionCriteria 
	(InclusionCriteriaName)
VALUES
	('Compliance: Pollution control registration'),
	('Engagement: Demonstrated interest in quality improvement'),
	('Engagement: Willingness to share data'),
	('Infrastructure: 24/7 electricity'),
	('Infrastructure: 24/7 water supply'),
	('Infrastructure: Labour room'),
	('Infrastructure: Operating theatre'),
	('Scale: A minimum of 20 deliveries per month'),
	('Scale: Less than 50 beds (NABH SHCO cut off)'),
	('Staff: At least three full-time GNM/ANMs'),
	('Staff: Full-time MBBS/Gynaecologist')
GO
--End table dropdown.InclusionCriteria

--Begin table dropdown.InclusionEligibilityStatus
TRUNCATE TABLE dropdown.InclusionEligibilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.InclusionEligibilityStatus', 'InclusionEligibilityStatusID', 0
GO

INSERT INTO dropdown.InclusionEligibilityStatus 
	(InclusionEligibilityStatusName, DisplayOrder)
VALUES
	('No', 1),
	('Pending', 2),
	('Yes', 3)
GO
--End table dropdown.InclusionEligibilityStatus

--Begin table dropdown.MOUStatus
TRUNCATE TABLE dropdown.MOUStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.MOUStatus', 'MOUStatusID', 0
GO

INSERT INTO dropdown.MOUStatus 
	(MOUStatusName, DisplayOrder)
VALUES
	('In Draft', 1),
	('Draft Provided', 2),
	('Draft Approved', 3),
	('Ready for Signature', 4),
	('Date for Signature Set', 5),
	('Completed', 6)
GO
--End table dropdown.MOUStatus

--Begin table dropdown.Project
TRUNCATE TABLE dropdown.Project
GO

EXEC utility.InsertIdentityValue 'dropdown.Project', 'ProjectID', 0
GO

INSERT INTO dropdown.Project 
	(ProjectName, ProjectAlias) 
VALUES 
	('HLFPPT', 'HLFPPT'),
	('PSI', 'PSI'),
	('TEST', 'TEST')
GO
--End table dropdown.Project

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleName)
VALUES
	('Role 1'),
	('Role 2'),
	('Role 3'),
	('Role 4')
GO
--End table dropdown.Role

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='Add / edit an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View the list of inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='Add / edit an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View the list of MOUs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, Organization, RoleID, Password, PasswordSalt, PasswordExpirationDateTime, InvalidLoginAttempts, IsAccountLockedOut, Token, TokenCreateDateTime, EmailAddress, IsActive, CountryCallingCodeID, Phone, IsPhoneVerified, IsSuperAdministrator, DefaultProjectID) 
VALUES 
	('Jonathan', 'Burnhan', 'Mr.', 'jburnham', 'OD', 1, 'EB065F0E9CFE4AA399624ADFEF14427FA39070F25A92B0DFF210D0F13C11B46F', 'AA47A377-16D5-4319-9C92-EEB2C67C05E5', CAST(N'2020-01-01 00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2015-08-05 12:09:44.327' AS DateTime), 'jonathanburnham@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Jonathan', 'Cole', 'Mr.', 'JCole', 'SK', 0, '701737B188CB58B85C8A8115AA80400250886596FE96521498A681B716C37E6C', 'F4918DAE-5EDC-4394-AD64-FBB18AC4AEFD', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, '76547CC5-C3B4-110E-6CB3FBCDCDDC4888', CAST(N'2016-09-28T15:18:14.300' AS DateTime), 'jcole@skotkonung.com', 1, 0, '07740705500', 1, 1, 1),
	('Christopher', 'Crouch', 'Mr.', 'christopher.crouch', 'OD', 0, '2EB50FF5E58F8A4A1575C35A0FFDB0863438B678EF46BD0B75BD444E80B8540A', '6F079855-A34A-44AD-9C04-1556195B85BE', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 2, 0, NULL, CAST(N'2015-08-20T09:04:25.350' AS DateTime), 'stchris2opher@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Brandon', 'Green', 'Mr.', 'bgreen', 'OD', 0, 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF861AEFFD81DCD6E7', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, NULL, CAST(N'2015-09-05T10:27:53.993' AS DateTime), 'bjgreen10@gmail.com', 1, 0, '4104306495', 1, 1, 1),
	('Eric', 'Jones', 'Mr.', 'eric.jones', 'OD', 0, '499A9A4E993262EB9F107AD806F34028CF13F505C77540F1790DC4DAD0FAD9F4', '76BBC375-9B01-45DD-B526-C0DE638B3A91', CAST(N'2099-02-24T15:52:58.000' AS DateTime), 0, 0, NULL, CAST(N'2099-12-26T09:22:31.000' AS DateTime), 'gigado@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('John', 'Lyons', 'Mr.', 'jlyons', 'OD', 0, '47970F8635EF6AC39C9B0D350A897E17C70A5D0D620A4FABF20F99A9E35A5A4D', '74D51802-110F-4DD5-B020-C1F50CA55CE7', CAST(N'2116-12-14T18:16:32.273' AS DateTime), 0, 0, 'BD46BE81-D6B1-616F-F084E84EAAA354E9', CAST(N'2017-06-25T10:49:43.227' AS DateTime), 'jlyons@pridedata.com', 1, 0, '7065992970', 1, 1, 1),
	('Damon', 'Miller', 'Mr.', 'dmiller', 'OD', 0, 'B09F895AF1360AFB6C4CCD107B51010704903B12938ACB8193264BF320D7393E', '8FE8FEAB-EE29-4FA2-AF7A-BF67D66C1D82', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, 'CDF817CB-B8A2-D9FD-0A5BC0D61C033F77', CAST(N'2017-06-05T21:22:45.473' AS DateTime), 'damonmiller513@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'OD', 0, '2C0671AB42CFCB763D973A06469D56A077A57FAC2EAB7410A2738A5E60695EFB', 'BAC035E0-1C35-43A3-91DC-9F38888D95C1', CAST(N'2020-09-30T21:43:00.000' AS DateTime), 0, 0, NULL, CAST(N'2016-08-25T22:27:00.347' AS DateTime), 'todd.pires@oceandisc.com', 1, 0, '7275791066', 1, 1, 1),
	('Dave', 'Roberts', 'Mr.', 'DaveR', 'OD', 0, '2C0671AB42CFCB763D973A06469D56A077A57FAC2EAB7410A2738A5E60695EFB', 'BAC035E0-1C35-43A3-91DC-9F38888D95C1', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, NULL, CAST(N'2017-07-22T12:03:01.367' AS DateTime), 'djrobertsjr@comcast.net', 1, 0, '555-1212', 1, 1, 1),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'OD', 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, '6DBCEC7A-0275-6A21-12CBB9ABFB9934BF', CAST(N'2017-03-31T09:34:18.247' AS DateTime), 'kevin.ross@oceandisc.com', 1, 0, '4104303492', 1, 1, 1)
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.Person

--Begin table person.PersonProject
TRUNCATE TABLE person.PersonProject
GO

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P.PersonID,
	PR.ProjectID
FROM person.Person P, dropdown.Project PR
WHERE PR.ProjectID > 0
GO
--End table person.PersonProject

--Begin table territory.ProjectTerritoryType
TRUNCATE TABLE territory.ProjectTerritoryType
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID
FROM dropdown.Project P, territory.TerritoryType TT
WHERE P.ProjectID > 0
	AND TT.TerritoryTypeCode = 'District'
GO
--End table territory.ProjectTerritoryType
