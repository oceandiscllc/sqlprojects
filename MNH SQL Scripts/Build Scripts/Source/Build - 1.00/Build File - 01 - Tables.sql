USE MNH
GO

EXEC utility.DropObject 'mou.MOU'
EXEC utility.DropObject 'mou'
GO

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'ContactFunctionID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'ContactRoleID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DIBRoleID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'FacilityID', 'INT', '0'

EXEC utility.DropColumn @TableName, 'AssetID'
GO
--End table contact.Contact

--Begin table contact.ContactFacility
DECLARE @TableName VARCHAR(250) = 'contact.ContactFacility'

EXEC utility.DropObject @TableName
GO
--End table contact.ContactFacility

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.MenuItem
DECLARE @TableName VARCHAR(250) = 'core.MenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItem
	(
	MenuItemID INT IDENTITY(1,1) NOT NULL,
	ParentMenuItemID INT,
	MenuItemCode VARCHAR(50),
	MenuItemText VARCHAR(250),
	MenuItemLink VARCHAR(500),
	DisplayOrder INT,
	Icon VARCHAR(50),
	IsForNewTab BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsForNewTab', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentMenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MenuItemID'
GO
--End table core.MenuItem

--Begin table core.MenuItemPermissionableLineage
DECLARE @TableName VARCHAR(250) = 'core.MenuItemPermissionableLineage'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemPermissionableLineage
	(
	MenuItemPermissionableLineageID INT IDENTITY(1,1) NOT NULL,
	MenuItemID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemPermissionableLineageID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemPermissionable', 'MenuItemID'
GO
--End table core.MenuItemPermissionableLineage

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	SystemSetupBinaryValue VARBINARY(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table dropdown.ContactFunction
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactFunction'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactFunction
	(
	ContactFunctionID INT IDENTITY(0,1) NOT NULL,
	ContactFunctionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactFunctionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactFunction', 'DisplayOrder,ContactFunctionName', 'ContactFunctionID'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactRole
	(
	ContactRoleID INT IDENTITY(0,1) NOT NULL,
	ContactRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactRole', 'DisplayOrder,ContactRoleName', 'ContactRoleID'
GO
--End table dropdown.ContactRole

--Begin table dropdown.DIBRole
DECLARE @TableName VARCHAR(250) = 'dropdown.DIBRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DIBRole
	(
	DIBRoleID INT IDENTITY(0,1) NOT NULL,
	DIBRoleName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DIBRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DIBRole', 'DisplayOrder,DIBRoleName', 'DIBRoleID'
GO
--End table dropdown.DIBRole

--Begin table dropdown.FacilityService
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityService'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityService
	(
	FacilityServiceID INT IDENTITY(0,1) NOT NULL,
	FacilityServiceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityServiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityService', 'DisplayOrder,FacilityServiceName', 'FacilityServiceID'
GO
--End table dropdown.FacilityService

--Begin table dropdown.FacilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityStatus
	(
	FacilityStatusID INT IDENTITY(0,1) NOT NULL,
	FacilityStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityStatus', 'DisplayOrder,FacilityStatusName', 'FacilityStatusID'
GO
--End table dropdown.FacilityStatus

--Begin table dropdown.GraduationPlanStep
DECLARE @TableName VARCHAR(250) = 'dropdown.GraduationPlanStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.GraduationPlanStep
	(
	GraduationPlanStepID INT IDENTITY(0,1) NOT NULL,
	GraduationPlanStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'GraduationPlanStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_GraduationPlanStep', 'DisplayOrder,GraduationPlanStepName', 'GraduationPlanStepID'
GO
--End table dropdown.GraduationPlanStep

--Begin table dropdown.InclusionCriteria
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InclusionCriteria
	(
	InclusionCriteriaID INT IDENTITY(0,1) NOT NULL,
	InclusionCriteriaName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InclusionCriteriaID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InclusionCriteria', 'DisplayOrder,InclusionCriteriaName', 'InclusionCriteriaID'
GO
--End table dropdown.InclusionCriteria

--Begin table dropdown.InclusionEligibilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionEligibilityStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InclusionEligibilityStatus
	(
	InclusionEligibilityStatusID INT IDENTITY(0,1) NOT NULL,
	InclusionEligibilityStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InclusionEligibilityStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InclusionEligibilityStatus', 'DisplayOrder,InclusionEligibilityStatusName', 'InclusionEligibilityStatusID'
GO
--End table dropdown.InclusionEligibilityStatus

--Begin table dropdown.MOUStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.MOUStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MOUStatus
	(
	MOUStatusID INT IDENTITY(0,1) NOT NULL,
	MOUStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MOUStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MOUStatus', 'DisplayOrder,MOUStatusName', 'MOUStatusID'
GO
--End table dropdown.MOUStatus

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.DropObject @TableName

CREATE TABLE facility.Facility
	(
	FacilityID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	FacilityName NVARCHAR(250),
	Address VARCHAR(MAX),
	Description VARCHAR(MAX),
	Phone VARCHAR(50),
	FacilityStatusID INT,
	Location GEOMETRY,
	PrimaryContactID INT,
	MOUDate DATE,
	MOUNotes VARCHAR(MAX),
	MOUStatusID INT,
	GraduationPlanUpdateDate DATE,
	GraduationPlanUpdatePersonID INT,
	InclusionCriteriaNotes VARCHAR(MAX),
	InclusionEligibilityStatusID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GraduationPlanUpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InclusionEligibilityStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MOUStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PrimaryContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Facility', 'FacilityName'
GO
--End table facility.Facility

--Begin trigger facility.TR_Facility

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09..5
-- Description:	A trigger to update the facility.Facility tables
-- ========================================================================
CREATE TRIGGER facility.TR_Facility ON facility.Facility FOR INSERT, DELETE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM DELETED)
	BEGIN

	DELETE FGPS
	FROM facility.FacilityGraduationPlanStep 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FGPS.FacilityID
		)

	DELETE FIC
	FROM facility.FacilityInclusionCriteria 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FIC.FacilityID
		)

	END
--ENDIF

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO facility.FacilityGraduationPlanStep
		(FacilityID, GraduationPlanStepID)
	SELECT
		I.FacilityID,
		GPS.GraduationPlanStepID
	FROM INSERTED I, dropdown.GraduationPlanStep GPS
	WHERE GPS.GraduationPlanStepID > 0

	INSERT INTO facility.FacilityInclusionCriteria
		(FacilityID, InclusionCriteriaID)
	SELECT
		I.FacilityID,
		IC.InclusionCriteriaID
	FROM INSERTED I, dropdown.InclusionCriteria IC
	WHERE IC.InclusionCriteriaID > 0

	END
--ENDIF

GO		
--End trigger facility.TR_Facility

--Begin table facility.FacilityFacilityService
DECLARE @TableName VARCHAR(250) = 'facility.FacilityFacilityService'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityFacilityService
	(
	FacilityFacilityServiceID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	FacilityServiceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityServiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityFacilityServiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityFacilityService', 'FacilityID,FacilityServiceID'
GO
--End table facility.FacilityFacilityService

--Begin table facility.FacilityGraduationPlanStep
DECLARE @TableName VARCHAR(250) = 'facility.FacilityGraduationPlanStep'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityGraduationPlanStep
	(
	FacilityGraduationPlanStepID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	GraduationPlanStepID INT,
	OriginalTargetDate DATE,
	CurrentTargetDate DATE,
	IsComplete BIT,
	PersonID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GraduationPlanStepID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityGraduationPlanStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityGraduationPlanStep', 'FacilityID,GraduationPlanStepID'
GO
--End table facility.FacilityGraduationPlanStep

--Begin table facility.FacilityInclusionCriteria
DECLARE @TableName VARCHAR(250) = 'facility.FacilityInclusionCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityInclusionCriteria
	(
	FacilityInclusionCriteriaID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	InclusionCriteriaID INT,
	IsCriteriaMet BIT,
	AssessmentDate DATE,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InclusionCriteriaID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsCriteriaMet', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityInclusionCriteriaID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityInclusionCriteria', 'FacilityID,InclusionCriteriaID'
GO
--End table facility.FacilityInclusionCriteria

--Begin table facility.FacilityMOUContact
DECLARE @TableName VARCHAR(250) = 'facility.FacilityMOUContact'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityMOUContact
	(
	FacilityMOUContactID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityMOUContactID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityMOUContact', 'FacilityID,ContactID'
GO
--End table facility.FacilityMOUContact