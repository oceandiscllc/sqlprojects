/* Build File - 03 - Procedures - Other */
USE MNH
GO

--Begin procedure contact.GetContactByContactID
EXEC Utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.10
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CellPhoneNumber,
		C.ContactID,
		C.EmailAddress,
		C.FirstName,
		C.IsActive,
		C.LastName,
		C.MiddleName,
		C.PhoneNumber,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Title,
		CF.ContactFunctionID,
		CF.ContactFunctionName,
		CR.ContactRoleID,
		CR.ContactRoleName,
		DIBR.DIBRoleID,
		DIBR.DIBRoleName,
		F.FacilityID,
		F.FacilityName
	FROM contact.Contact C
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
		JOIN dropdown.DIBRole DIBR ON DIBR.DIBRoleID = C.DIBRoleID
		JOIN facility.Facility F ON F.FacilityID = C.FacilityID
			AND C.ContactID = @ContactID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		ET.EmailTemplateCode,
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

END
GO
--End procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- =================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- =============================================================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		MI.IsForNewTab,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetServerSetupDataByServerSetupID'
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetServerSetupValuesByServerSetupKey'
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE MI
			SET 
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab
			FROM core.MenuItem MI
			WHERE MI.MenuItemID = @nNewMenuItemID

			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin core.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
EXEC Utility.DropObject 'utility.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE core.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
		JOIN core.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO core.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM core.MenuItemPermissionableLineage MIPL1
		JOIN core.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure dropdown.GetContactRoleData
EXEC Utility.DropObject 'dropdown.GetContactRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactRoleID, 
		T.ContactRoleName
	FROM dropdown.ContactRole T
	WHERE (T.ContactRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactRoleName, T.ContactRoleID

END
GO
--End procedure dropdown.GetContactRoleData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure dropdown.GetDIBRoleData
EXEC Utility.DropObject 'dropdown.GetDIBRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.DIBRole table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetDIBRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DIBRoleID, 
		T.DIBRoleName
	FROM dropdown.DIBRole T
	WHERE (T.DIBRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DIBRoleName, T.DIBRoleID

END
GO
--End procedure dropdown.GetDIBRoleData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure dropdown.GetFacilityServiceData
EXEC Utility.DropObject 'dropdown.GetFacilityServiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityService table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetFacilityServiceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityServiceID, 
		T.FacilityServiceName
	FROM dropdown.FacilityService T
	WHERE (T.FacilityServiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityServiceName, T.FacilityServiceID

END
GO
--End procedure dropdown.GetFacilityServiceData

--Begin procedure dropdown.GetFacilityStatusData
EXEC Utility.DropObject 'dropdown.GetFacilityStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityStatus table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetFacilityStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityStatusID, 
		T.FacilityStatusName
	FROM dropdown.FacilityStatus T
	WHERE (T.FacilityStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityStatusName, T.FacilityStatusID

END
GO
--End procedure dropdown.GetFacilityStatusData

--Begin procedure dropdown.GetContactFunctionData
EXEC Utility.DropObject 'dropdown.GetContactFunctionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactFunctionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactFunctionID, 
		T.ContactFunctionName
	FROM dropdown.ContactFunction T
	WHERE (T.ContactFunctionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactFunctionName, T.ContactFunctionID

END
GO
--End procedure dropdown.GetContactFunctionData

--Begin procedure dropdown.GetGraduationPlanStepData
EXEC Utility.DropObject 'dropdown.GetGraduationPlanStepData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to return data from the dropdown.GraduationPlanStep table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetGraduationPlanStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GraduationPlanStepID, 
		T.GraduationPlanStepName
	FROM dropdown.GraduationPlanStep T
	WHERE (T.GraduationPlanStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GraduationPlanStepName, T.GraduationPlanStepID

END
GO
--End procedure dropdown.GetGraduationPlanStepData

--Begin procedure dropdown.GetInclusionCriteriaData
EXEC Utility.DropObject 'dropdown.GetInclusionCriteriaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.InclusionCriteria table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetInclusionCriteriaData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionCriteriaID, 
		T.InclusionCriteriaName
	FROM dropdown.InclusionCriteria T
	WHERE (T.InclusionCriteriaID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionCriteriaName, T.InclusionCriteriaID

END
GO
--End procedure dropdown.GetInclusionCriteriaData

--Begin procedure dropdown.GetInclusionEligibilityStatusData
EXEC Utility.DropObject 'dropdown.GetInclusionEligibilityStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A stored procedure to return data from the dropdown.InclusionEligibilityStatus table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetInclusionEligibilityStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionEligibilityStatusID, 
		T.InclusionEligibilityStatusName
	FROM dropdown.InclusionEligibilityStatus T
	WHERE (T.InclusionEligibilityStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionEligibilityStatusName, T.InclusionEligibilityStatusID

END
GO
--End procedure dropdown.GetInclusionEligibilityStatusData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin procedure dropdown.GetMOUStatusData
EXEC Utility.DropObject 'dropdown.GetMOUStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.MOUStatus table
-- ================================================================================
CREATE PROCEDURE dropdown.GetMOUStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MOUStatusID, 
		T.MOUStatusName
	FROM dropdown.MOUStatus T
	WHERE (T.MOUStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MOUStatusName, T.MOUStatusID

END
GO
--End procedure dropdown.GetMOUStatusData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID, 
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address, 	
		F.Description, 			
		F.FacilityID, 	
		F.FacilityName, 	
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.GetFacilityGraduationPlanStepsByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityGraduationPlanStepsByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityGraduationPlanStepsByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		F.GraduationPlanUpdateDate, 
		core.FormatDate(F.GraduationPlanUpdateDate) AS GraduationPlanUpdateDateFormatted,
		F.GraduationPlanUpdatePersonID, 
		person.FormatPersonNameByPersonID(F.GraduationPlanUpdatePersonID, 'LastFirst') AS PersonNameFormatted
	FROM facility.Facility F
	WHERE F.FacilityID = @FacilityID

	--FacilityGraduationPlanStep
	SELECT
		FGPS.FacilityGraduationPlanStepID, 
		FGPS.CurrentTargetDate, 
		core.FormatDate(FGPS.CurrentTargetDate) AS CurrentTargetDateFormatted,
		FGPS.IsComplete, 
		FGPS.Notes,
		FGPS.OriginalTargetDate, 
		core.FormatDate(FGPS.OriginalTargetDate) AS OriginalTargetDateFormatted,
		FGPS.PersonID, 
		person.FormatPersonNameByPersonID(FGPS.PersonID, 'LastFirst') AS PersonNameFormatted,
		GPS.GraduationPlanStepID, 
		GPS.GraduationPlanStepName
	FROM facility.FacilityGraduationPlanStep FGPS
		JOIN dropdown.GraduationPlanStep GPS ON GPS.GraduationPlanStepID = FGPS.GraduationPlanStepID
		JOIN facility.Facility F ON F.FacilityID = FGPS.FacilityID
			AND FGPS.FacilityID = @FacilityID
	ORDER BY GPS.DisplayOrder, GPS.GraduationPlanStepName, GPS.GraduationPlanStepID

END
GO
--End procedure facility.GetFacilityGraduationPlanStepsByFacilityID   

--Begin procedure facility.GetFacilityInclusionCriteriaByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityInclusionCriteriaByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityInclusionCriteriaByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		F.InclusionCriteriaNotes,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName
	FROM facility.Facility F
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = F.InclusionEligibilityStatusID
			AND F.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	SELECT
		FIC.AssessmentDate, 
		core.FormatDate(FIC.AssessmentDate) AS AssessmentDateFormatted,
		FIC.FacilityInclusionCriteriaID, 
		FIC.IsCriteriaMet, 
		FIC.PersonID,
		person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') AS PersonNameFormatted,
		IC.InclusionCriteriaID, 
		IC.InclusionCriteriaName
	FROM facility.FacilityInclusionCriteria FIC
		JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
		JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
			AND FIC.FacilityID = @FacilityID
	ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

END
GO
--End procedure facility.GetFacilityInclusionCriteriaByFacilityID   

--Begin procedure facility.GetFacilityMOUByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityMOUByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityMOUByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		MS.MOUStatusID,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID, 
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	ORDER BY C.LastName, C.FirstName, C.ContactID

END
GO
--End procedure facility.GetFacilityMOUByFacilityID

--Begin procedure territory.GetParentTerritoryByProjectID
EXEC utility.DropObject 'territory.GetParentTerritoryByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to associate a community to a project
-- =====================================================================
CREATE PROCEDURE territory.GetParentTerritoryByProjectID

@ProjectID INT,
@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
		JOIN 
			(
			SELECT PTT.ParentTerritoryTypeCode
			FROM territory.ProjectTerritoryType PTT
				JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
					AND PTT.ProjectID = @ProjectID
					AND TT.TerritoryTypeCode = @TerritoryTypeCode
			) D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1
	
END
GO
--End procedure territory.GetParentTerritoryByProjectID

--Begin procedure territory.GetTerritoryByProjectID
EXEC utility.DropObject 'territory.GetTerritoryByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to associate a lat/long to a project
-- ====================================================================
CREATE PROCEDURE territory.GetTerritoryByProjectID

@ProjectID INT,
@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
		JOIN 
			(
			SELECT TT.TerritoryTypeCode
			FROM territory.ProjectTerritoryType PTT
				JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
					AND PTT.ProjectID = @ProjectID
					AND TT.TerritoryTypeCode = @TerritoryTypeCode
			) D ON D.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1
	
END
GO
--End procedure territory.GetTerritoryByProjectID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
-- =====================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Facility
	SELECT 
		F.FacilityID, 		
		F.FacilityName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM facility.Facility F
	WHERE F.ProjectID = @ProjectID
	ORDER BY 2, 3
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID