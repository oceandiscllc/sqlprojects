
--Begin function visit.GetAssessmentEvidenceTypeIDList
EXEC utility.DropObject 'visit.GetAssessmentEvidenceTypeIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION visit.GetAssessmentEvidenceTypeIDList
(
@QualityStandardID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cAssessmentEvidenceTypeIDList VARCHAR(MAX) = ''

	SELECT @cAssessmentEvidenceTypeIDList = COALESCE(@cAssessmentEvidenceTypeIDList + ',', '') + CAST(QSAET.AssessmentEvidenceTypeID AS VARCHAR(5))
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
	WHERE QSAET.QualityStandardID = @QualityStandardID

	IF LEFT(@cAssessmentEvidenceTypeIDList, 1) = ','
		SET @cAssessmentEvidenceTypeIDList = STUFF(@cAssessmentEvidenceTypeIDList, 1, 1, '')
	--ENDIF

	RETURN ISNULL(@cAssessmentEvidenceTypeIDList, '')

END
GO
--End function visit.GetAssessmentEvidenceTypeIDList

--Begin function visit.GetPriorVisitQualityStandardScore
EXEC utility.DropObject 'visit.GetPriorVisitQualityStandardScore'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.29
-- Description:	A function to return a the previous score for a specific facility and quality standard
-- ===================================================================================================

CREATE FUNCTION visit.GetPriorVisitQualityStandardScore
(
@QualityStandardID INT,
@VisitID INT
)

RETURNS VARCHAR(50)

AS
BEGIN
	
	DECLARE @cPriorScore VARCHAR(50)

	SELECT TOP 1 
		@cPriorScore = CASE WHEN VQS.NotApplicable = 1 THEN 'Not Applicable' ELSE CAST(VQS.Score AS VARCHAR(50)) END
	FROM visit.VisitQualityStandard VQS
		JOIN visit.Visit V1 ON V1.VisitID = VQS.VisitID
			AND VQS.QualityStandardID = @QualityStandardID
			AND V1.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
			AND V1.VisitID <> @VisitID
	ORDER BY VQS.VisitQualityStandardID DESC

	RETURN ISNULL(@cPriorScore, '0')

END
GO
--End function visit.GetPriorVisitQualityStandardScore