--Begin table dropdown.QualityStandard
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandard'

EXEC utility.AddColumn @TableName, 'GuidanceNotes', 'VARCHAR(MAX)'
GO
--End table dropdown.QualityStandard

--Begin table dropdown.QualityStandardVerificationCriteria
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardVerificationCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandardVerificationCriteria
	(
	QualityStandardVerificationCriteriaID INT IDENTITY(1,1) NOT NULL,
	QualityStandardID INT,
	VerificationCriteriaCode VARCHAR(50),
	VerificationCriteriaID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VerificationCriteriaID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QualityStandardVerificationCriteriaID'
EXEC utility.SetIndexClustered @TableName, 'IX_QualityStandardVerificationCriteria', 'QualityStandardID,VerificationCriteriaCode,VerificationCriteriaID'
GO
--End table dropdown.QualityStandardVerificationCriteria

--Begin table dropdown.VerificationCriteriaCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaCategory
	(
	VerificationCriteriaCategoryID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaCategory', 'DisplayOrder,VerificationCriteriaCategoryName', 'VerificationCriteriaCategoryID'
GO
--End table dropdown.VerificationCriteriaCategory

--Begin table dropdown.VerificationCriteriaProtocol
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaProtocol'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaProtocol
	(
	VerificationCriteriaProtocolID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaProtocolName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaProtocolID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaProtocol', 'DisplayOrder,VerificationCriteriaProtocolName', 'VerificationCriteriaProtocolID'
GO
--End table dropdown.VerificationCriteriaProtocol

--Begin table dropdown.VerificationCriteriaSubCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaSubCategory
	(
	VerificationCriteriaSubCategoryID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaSubCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaSubCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaSubCategory', 'DisplayOrder,VerificationCriteriaSubCategoryName', 'VerificationCriteriaSubCategoryID'
GO
--End table dropdown.VerificationCriteriaSubCategory

--Begin table dropdown.VisitOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.VisitOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VisitOutcome
	(
	VisitOutcomeID INT IDENTITY(0,1) NOT NULL,
	VisitOutcomeName VARCHAR(50),
	HasNote BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasNote', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitOutcomeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitOutcome', 'DisplayOrder,VisitOutcomeName', 'VisitOutcomeID'
GO
--End table dropdown.VisitOutcome

--Begin table facility.FacilityFacilityService
EXEC utility.DropObject 'facility.FacilityFacilityService'
GO
--End table facility.FacilityFacilityService

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.AddColumn @TableName, 'VisitOutcomeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'VisitOutcomeNote', 'VARCHAR(250)'
GO
--End table visit.Visit

