--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure dropdown.GetFacilityServiceData
EXEC Utility.DropObject 'dropdown.GetFacilityServiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityService table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetFacilityServiceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityServiceID, 
		T.FacilityServiceCode,
		T.FacilityServiceName,
		T.DisplayOrder
	FROM dropdown.FacilityService T
	WHERE (T.FacilityServiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityServiceName, T.FacilityServiceID

END
GO
--End procedure dropdown.GetFacilityServiceData

--Begin procedure dropdown.GetQualityStandardLookupData
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Christopher Crouch
-- Create date: 2018.10.19
-- Description:	This gets the full list of quality standards with guidance notes
-- =============================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupData
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		QS.GuidanceNotes,
		QSL.ChapterID,
		QSL.ChapterName,
		QSL.EntityTypeCode,
		QSL.ManualID,
		QSL.ManualName,
		QSL.ObjectiveElementID,
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QS ON QSL.QualityStandardID = QS.QualityStandardID
			AND QS.GuidanceNotes IS NOT NULL
	ORDER BY
		QSL.ManualName,
		QSL.ManualID,
		QSL.ChapterName,
		QSL.ChapterID,
		QSL.StandardName,
		QSL.StandardID,
		QSL.ObjectiveElementName,
		QSL.ObjectiveElementID,
		QSL.VerificationCriteriaName,
		QSL.VerificationCriteriaID

END
GO
--End procedure dropdown.GetQualityStandardLookupData

--Begin procedure dropdown.GetVerificationCriteriaCategoryData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaCategory table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaCategoryID,
		T.VerificationCriteriaCategoryName
	FROM dropdown.VerificationCriteriaCategory T
	WHERE (T.VerificationCriteriaCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaCategoryName, T.VerificationCriteriaCategoryID

END
GO
--End procedure dropdown.GetVerificationCriteriaCategoryData

--Begin procedure dropdown.GetVerificationCriteriaProtocolData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaProtocolData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaProtocol table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaProtocolData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaProtocolID,
		T.VerificationCriteriaProtocolName
	FROM dropdown.VerificationCriteriaProtocol T
	WHERE (T.VerificationCriteriaProtocolID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaProtocolName, T.VerificationCriteriaProtocolID

END
GO
--End procedure dropdown.GetVerificationCriteriaProtocolData

--Begin procedure dropdown.GetVerificationCriteriaSubCategoryData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaSubCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaSubCategory table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaSubCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaSubCategoryID,
		T.VerificationCriteriaSubCategoryName
	FROM dropdown.VerificationCriteriaSubCategory T
	WHERE (T.VerificationCriteriaSubCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaSubCategoryName, T.VerificationCriteriaSubCategoryID

END
GO
--End procedure dropdown.GetVerificationCriteriaSubCategoryData

--Begin procedure dropdown.GetVisitOutcomeData
EXEC Utility.DropObject 'dropdown.GetVisitOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.20
-- Description:	A stored procedure to return data from the dropdown.VisitOutcome table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetVisitOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VisitOutcomeID,
		T.HasNote,
		T.VisitOutcomeName
	FROM dropdown.VisitOutcome T
	WHERE (T.VisitOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VisitOutcomeName, T.VisitOutcomeID

END
GO
--End procedure dropdown.GetVisitOutcomeData

--Begin procedure dropdown.SaveGuidanceNotesByQualityStandardID
EXEC Utility.DropObject 'dropdown.SaveGuidanceNotesByQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.20
-- Description:	A stored procedure to save data to the dropdown.QualityStandard table
-- ==================================================================================
CREATE PROCEDURE dropdown.SaveGuidanceNotesByQualityStandardID

@GuidanceNotes VARCHAR(MAX),
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE QS
	SET QS.GuidanceNotes = @GuidanceNotes
	FROM dropdown.QualityStandard QS
	WHERE QS.QualityStandardID = @QualityStandardID

END
GO
--End procedure dropdown.SaveGuidanceNotesByQualityStandardID

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.AuthorizedDate, 
		core.FormatDate(F.AuthorizedDate) AS AuthorizedDateFormatted,
		F.AuthorizerPersonID,
		person.FormatPersonNameByPersonID(F.AuthorizerPersonID, 'LastFirstTitle') AS AuthorizerPersonNameFormatted,		
		F.BedCount,
		F.Description, 			
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.FacilityID,
		core.FormatDate(eventlog.GetFirstCreateDateTime('Facility', F.FacilityID)) AS FacilityCreateDateFormatted,
		core.FormatDate(eventlog.GetLastCreateDateTime('Facility', F.FacilityID)) AS FacilityUpdateDateFormatted,
		F.IsActive,
		F.FacilityName,
		F.InclusionCriteriaNotes,
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		FS1.FacilityServiceID,
		FS1.FacilityServiceCode,
		FS1.FacilityServiceName,
		FS2.FacilityStatusID, 	
		FS2.FacilityStatusName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted
	FROM facility.Facility F
		JOIN dropdown.FacilityService FS1 ON FS1.FacilityServiceID = F.FacilityServiceID
		JOIN dropdown.FacilityStatus FS2 ON FS2.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = F.InclusionEligibilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	IF @FacilityID = 0
		BEGIN

		SELECT
			0 AS FacilityInclusionCriteriaID, 
			0 AS IsAuthorized,
			0 AS IsCriteriaMet, 
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM dropdown.InclusionCriteria IC
		WHERE IC.InclusionCriteriaID > 0
			AND IC.IsActive = 1
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	ELSE
		BEGIN

		SELECT
			FIC.FacilityInclusionCriteriaID, 
			FIC.IsAuthorized,
			FIC.IsCriteriaMet, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.PersonID ELSE 0 END AS PersonID,
			CASE FIC.IsCriteriaMet WHEN 1 THEN person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') ELSE NULL END AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM facility.FacilityInclusionCriteria FIC
			JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
			JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
				AND FIC.FacilityID = @FacilityID
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	--ENDIF

	--FacilityFacilityInclusionCriteriaDocument
	SELECT
		DE.EntityID AS FacilityInclusionCriteriaID,
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,

		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FOR JSON PATH
		), '[]') AS UploadedFiles

	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN facility.FacilityInclusionCriteria FIC ON FIC.FacilityInclusionCriteriaID = DE.EntityID
			AND DE.EntityTypeCode = 'FacilityInclusionCriteria'
			AND FIC.FacilityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID

	--FacilityMOUDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode = 'MOU' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.ImportFacilities
EXEC utility.DropObject 'facility.ImportFacilities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Tod Pires
-- Create date:	2018.09.15
-- Description:	A stored procedure to import facility data from the MNH database to the MATH database
-- ==================================================================================================
CREATE PROCEDURE facility.ImportFacilities

@FacilityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE MATHF
	SET
		MATHF.Address = MNHF.Address,
		MATHF.BedCount = MNHF.BedCount,
		MATHF.Description = MNHF.Description,
		MATHF.FacilityEstablishedDate = MNHF.FacilityEstablishedDate,
		MATHF.FacilityName = MNHF.FacilityName,
		MATHF.FacilityServiceID = MNHF.FacilityServiceID,
		MATHF.FacilityStatusID = MNHF.FacilityStatusID,
		MATHF.InclusionCriteriaNotes = MNHF.InclusionCriteriaNotes,
		MATHF.InclusionEligibilityStatusID = MNHF.InclusionEligibilityStatusID,
		MATHF.Location = MNHF.Location,
		MATHF.MOUDate = MNHF.MOUDate,
		MATHF.MOUNotes = MNHF.MOUNotes,
		MATHF.MOUStatusID = MNHF.MOUStatusID,
		MATHF.Phone = MNHF.Phone,
		MATHF.PrimaryContactID = MNHF.PrimaryContactID,
		MATHF.ProjectID = (SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MATHF.TerritoryID = MNHF.TerritoryID
	FROM MATH.facility.Facility MATHF
		JOIN MNH.facility.Facility MNHF ON MNHF.FacilityID = MATHF.FacilityID
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MATHF.FacilityID
	;

	SET IDENTITY_INSERT MATH.facility.Facility ON;

	INSERT INTO MATH.facility.Facility
		(Address,BedCount,Description,FacilityEstablishedDate,FacilityID,FacilityName,FacilityServiceID,FacilityStatusID,InclusionCriteriaNotes,InclusionEligibilityStatusID,Location,MOUDate,MOUNotes,MOUStatusID,Phone,PrimaryContactID,ProjectID,TerritoryID)
	SELECT 
		MNHF.Address,
		MNHF.BedCount,
		MNHF.Description,
		MNHF.FacilityEstablishedDate,
		MNHF.FacilityID,
		MNHF.FacilityName,
		MNHF.FacilityServiceID,
		MNHF.FacilityStatusID,
		MNHF.InclusionCriteriaNotes,
		MNHF.InclusionEligibilityStatusID,
		MNHF.Location,
		MNHF.MOUDate,
		MNHF.MOUNotes,
		MNHF.MOUStatusID,
		MNHF.Phone,
		MNHF.PrimaryContactID,
		(SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MNHF.TerritoryID
	FROM MNH.facility.Facility MNHF
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MNHF.FacilityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM MATH.facility.Facility MATHF
				WHERE MATHF.FacilityID = MNHF.FacilityID
				)
	;

	SET IDENTITY_INSERT MATH.facility.Facility OFF;

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria ON;

	--FacilityInclusionCriteria
	MERGE MATH.facility.FacilityInclusionCriteria T1
	USING 
		(
		SELECT 
			FIC.FacilityID, 
			FIC.FacilityInclusionCriteriaID,
			FIC.InclusionCriteriaID,
			FIC.IsCriteriaMet
		FROM MNH.facility.FacilityInclusionCriteria FIC
			JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FIC.FacilityID
		) T2
		ON T2.FacilityInclusionCriteriaID = T1.FacilityInclusionCriteriaID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.IsCriteriaMet = T2.IsCriteriaMet
	WHEN NOT MATCHED THEN
	INSERT 
		(FacilityID,FacilityInclusionCriteriaID,InclusionCriteriaID,IsCriteriaMet)
	VALUES
		(
		T2.FacilityID,
		T2.FacilityInclusionCriteriaID,
		T2.InclusionCriteriaID,
		T2.IsCriteriaMet
		);

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria OFF;

	SET IDENTITY_INSERT MATH.contact.Contact ON;

	--FacilityContact
	MERGE MATH.contact.Contact T1
	USING 
		(
		SELECT
			C1.CellPhoneNumber,
			C1.ContactFunctionID,
			C1.ContactID,
			C1.ContactRoleID,
			C1.DateOfBirth,
			C1.DIBRoleID,
			C1.EmailAddress,
			C1.FacilityEndDate,
			C1.FacilityID,
			C1.FacilityStartDate,
			C1.FirstName,
			C1.GenderID,
			C1.IsActive,
			C1.LastName,
			C1.MiddleName,
			C1.OtherContactFunction,
			C1.OtherContactRole,
			C1.PhoneNumber,
			C1.Title,
			C1.YearsExperience
		FROM MNH.contact.Contact C1
		WHERE EXISTS
			(
			SELECT 1
			FROM MNH.facility.FacilityMOUContact FMC
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FMC.FacilityID
					AND FMC.ContactID = C1.ContactID

			UNION

			SELECT 1
			FROM MNH.contact.Contact C2
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C2.FacilityID
					AND C2.ContactID = C1.ContactID
			) 
		) T2
		ON T2.ContactID = T1.ContactID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.CellPhoneNumber = T2.CellPhoneNumber,
		T1.ContactFunctionID = T2.ContactFunctionID,
		T1.ContactRoleID = T2.ContactRoleID,
		T1.DateOfBirth = T2.DateOfBirth,
		T1.DIBRoleID = T2.DIBRoleID,
		T1.EmailAddress = T2.EmailAddress,
		T1.FacilityEndDate = T2.FacilityEndDate,
		T1.FacilityID = T2.FacilityID,
		T1.FacilityStartDate = T2.FacilityStartDate,
		T1.FirstName = T2.FirstName,
		T1.GenderID = T2.GenderID,
		T1.IsActive = T2.IsActive,
		T1.LastName = T2.LastName,
		T1.MiddleName = T2.MiddleName,
		T1.OtherContactFunction = T2.OtherContactFunction,
		T1.OtherContactRole = T2.OtherContactRole,
		T1.PhoneNumber = T2.PhoneNumber,
		T1.Title = T2.Title,
		T1.YearsExperience = T2.YearsExperience 
	WHEN NOT MATCHED THEN
	INSERT 
		(CellPhoneNumber,ContactFunctionID,ContactID,ContactRoleID,DateOfBirth,DIBRoleID,EmailAddress,FacilityEndDate,FacilityID,FacilityStartDate,FirstName,GenderID,IsActive,LastName,MiddleName,OtherContactFunction,OtherContactRole,PhoneNumber,Title,YearsExperience)
	VALUES
		(
		T2.CellPhoneNumber,
		T2.ContactFunctionID,
		T2.ContactID,
		T2.ContactRoleID,
		T2.DateOfBirth,
		T2.DIBRoleID,
		T2.EmailAddress,
		T2.FacilityEndDate,
		T2.FacilityID,
		T2.FacilityStartDate,
		T2.FirstName,
		T2.GenderID,
		T2.IsActive,
		T2.LastName,
		T2.MiddleName,
		T2.OtherContactFunction,
		T2.OtherContactRole,
		T2.PhoneNumber,
		T2.Title,
		T2.YearsExperience
		);

	SET IDENTITY_INSERT MATH.contact.Contact OFF;

END
GO
--End procedure facility.ImportFacilities

IF '[[INSTANCENAME]]' <> 'MATH'
	EXEC Utility.DropObject 'facility.ImportFacilities'
--ENDIF
GO

--Begin procedure facility.UpdateInclusionEligibilityStatus
EXEC Utility.DropObject 'facility.UpdateInclusionEligibilityStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.22
-- Description:	A stored procedure to set the InclusionEligibilityStatusID column
-- ==============================================================================
CREATE PROCEDURE facility.UpdateInclusionEligibilityStatus

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE F1
	SET F1.InclusionEligibilityStatusID = 
		CASE 
			WHEN 
				(
				SELECT F2.BedCount 
				FROM facility.Facility F2
				WHERE F2.FacilityID = F1.FacilityID
				) = 0
				AND 
					(
					SELECT FS.FacilityServiceCode 
					FROM dropdown.FacilityService FS 
						JOIN facility.Facility F3 ON F3.FacilityServiceID = FS.FacilityServiceID 
							AND F3.FacilityID = F1.FacilityID
					) = 'Other'
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
			ELSE
				CASE
					WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F1.FacilityID AND FIC.IsCriteriaMet = 1)
					THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
					WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F1.FacilityID AND FIC.IsCriteriaMet = 0)
					THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Yes')
					ELSE (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Pending')
				END
		END
	FROM facility.Facility F1
	WHERE F1.FacilityID = @FacilityID

END
GO
--End procedure facility.UpdateInclusionEligibilityStatus

--Begin procedure reporting.GetFacilityData
EXEC Utility.DropObject 'reporting.GetFacilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE reporting.GetFacilityData

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.BedCount,
		F.FacilityEstablishedDate,
		F.FacilityID,
		F.IsActive,
		F.MOUDate,
		FS.FacilityServiceName,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = F.FacilityServiceID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID

END
GO
--End procedure reporting.GetFacilityData

--Begin procedure reporting.GetVisitData
EXEC Utility.DropObject 'reporting.GetVisitData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the visit.Visit table
-- ============================================================================
CREATE PROCEDURE reporting.GetVisitData

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		QSL.ChapterName,
		QSL.ManualName,
		QSL.ObjectiveElementName,
		QSL.StandardName,
		QSL.VerificationCriteriaName,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitID, 	
		V.VisitOutcomeNote,
		V.VisitStartDateTime,
		V.VisitTime,
		VO.VisitOutcomeName,
		VQS.CorrectRecords, 
		VQS.HasDocumentation, 
		VQS.HasImplementation, 
		VQS.HasObservation, 
		VQS.HasPhysicalVerification, 
		VQS.HasProviderInterview, 
		VQS.IsScored, 
		VQS.NotApplicable,
		VQS.Notes, 
		VQS.Score, 
		VQS.TotalRecordsChecked
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitID = V.VisitID
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID

END
GO
--End procedure reporting.GetVisitData

--Begin procedure visit.FilterVisitQualityStandardData
EXEC Utility.DropObject 'visit.FilterVisitQualityStandardData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.22
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.FilterVisitQualityStandardData

@VisitID INT,
@Score VARCHAR(MAX) = NULL,
@VerificationCriteriaCategoryID INT = 0,
@VerificationProtocolID VARCHAR(MAX) = NULL,
@VerificationSubCriteriaCategoryID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT VQS.QualityStandardID
	FROM visit.VisitQualityStandard VQS
	WHERE VQS.VisitID = @VisitID
		AND 
			(
			@Score IS NULL
				OR EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@Score, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = VQS.Score
					)
			)
		AND 
			(
			@VerificationCriteriaCategoryID = 0
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaCategory'
						AND QSVC.VerificationCriteriaID = @VerificationCriteriaCategoryID
					)
			)
		AND 
			(
			@VerificationProtocolID IS NULL
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaProtocol'
						AND EXISTS
							(
							SELECT 1
							FROM core.ListToTable(@VerificationProtocolID, ',') LTT
							WHERE CAST(LTT.ListItem AS INT) = QSVC.VerificationCriteriaID
							)
					)
			)
		AND 
			(
			@VerificationSubCriteriaCategoryID = 0
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaSubCategory'
						AND QSVC.VerificationCriteriaID = @VerificationSubCriteriaCategoryID
					)
			)

END
GO
--End procedure visit.FilterVisitQualityStandardData

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		F.ProjectID,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitOutcomeNote,

		CASE 
			WHEN V.IsPlanningComplete = 0
			THEN 'Planning'
			WHEN V.IsVisitComplete = 1
			THEN 'Visit Complete'
			ELSE 'Planning Complete'
		END AS VisitStatusName,

		V.VisitStartDateTime,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID, 	
		V.VisitLeadPersonID,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitTime,
		VO.VisitOutcomeID,
		VO.VisitOutcomeName,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT 
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.IsScored,
		VQS.NotApplicable,
		VQS.Score,
		visit.GetAssessmentEvidenceTypeIDList(QSL.QualityStandardID) AS AssessmentEvidenceTypeIDList,
		visit.GetPriorVisitQualityStandardScore(QSL.QualityStandardID, @VisitID) AS PriorScore
	FROM dropdown.QualityStandardLookup QSL
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT
		CASE WHEN CAST(core.GetSystemSetupValueBySystemSetupKey('IsMNH', 1) AS BIT) = 0 THEN QS.GuidanceNotes ELSE NULL END AS GuidanceNotes,
		QSL.ManualName,
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.IsScored,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = VQS.QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	SELECT
		ISNULL((
			SELECT
				D.DocumentTitle,
				DE.DocumentEntityCode
			FROM document.DocumentEntity DE
				JOIN document.Document D ON D.DocumentID = DE.DocumentID
					AND DE.EntityTypeCode = 'VisitQualityStandard'
					AND DE.EntityID = 
						(
						SELECT VQS.VisitQualityStandardID 
						FROM visit.VisitQualityStandard VQS
						WHERE VQS.VisitID = @VisitID
							AND VQS.QualityStandardID = @QualityStandardID
						)
			FOR JSON PATH
		), '[]') AS UploadedFiles

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

--Begin procedure visit.GetVisitsByPersonID
EXEC Utility.DropObject 'visit.GetVisitsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.25
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
 
	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		P.ProjectID,
		P.ProjectName,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = F.ProjectID
				)
			AND V.IsPlanningComplete = 1
			AND V.IsVisitComplete = 0
	ORDER BY F.FacilityName, F.FacilityID, V.VisitID

END
GO
--End procedure visit.GetVisitsByPersonID

--Begin procedure visit.GetVisitsByVisitIDList
EXEC Utility.DropObject 'visit.GetVisitsByVisitIDList'
GO
--End procedure visit.GetVisitsByVisitIDList