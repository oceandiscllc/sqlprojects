--Begin trigger document.TR_DocumentEntity
EXEC utility.DropObject 'document.TR_DocumentEntity'
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.05
-- Description:	A trigger to update the document.DocumentEntity table
-- ==================================================================
CREATE TRIGGER document.TR_DocumentEntity ON document.DocumentEntity AFTER INSERT
AS
SET ARITHABORT ON

	DECLARE @tTable TABLE (DocumentEntityID INT NOT NULL PRIMARY KEY, DocumentEntityCode VARCHAR(50) DEFAULT newID())

	IF EXISTS (SELECT 1 FROM INSERTED)
		BEGIN

		INSERT INTO @tTable
			(DocumentEntityID)
		SELECT
			DE.DocumentEntityID
		FROM document.DocumentEntity DE
		WHERE DE.DocumentEntityCode IS NULL

		UPDATE DE
		SET DE.DocumentEntityCode = T.DocumentEntityCode
		FROM document.DocumentEntity DE
			JOIN @tTable T ON T.DocumentEntityID = DE.DocumentEntityID

		END
	--ENDIF
GO

ALTER TABLE document.DocumentEntity ENABLE TRIGGER TR_DocumentEntity
GO
--End trigger document.TR_DocumentEntity

--Begin table dropdown.FacilityCostPatientTrend
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityCostPatientTrend'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityCostPatientTrend
	(
	FacilityCostPatientTrendID INT IDENTITY(0,1) NOT NULL,
	FacilityCostPatientTrendCode VARCHAR(50),
	FacilityCostPatientTrendName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityCostPatientTrendID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityCostPatientTrend', 'DisplayOrder,FacilityCostPatientTrendName', 'FacilityCostPatientTrendID'
GO
--End table dropdown.FacilityCostPatientTrend

--Begin table dropdown.FacilityCostStaffType
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityCostStaffType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityCostStaffType
	(
	FacilityCostStaffTypeID INT IDENTITY(0,1) NOT NULL,
	FacilityCostStaffTypeName VARCHAR(50),
	FacilityCostStaffTypeDescription VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityCostStaffTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityCostStaffType', 'DisplayOrder,FacilityCostStaffTypeName', 'FacilityCostStaffTypeID'
GO
--End table dropdown.FacilityCostStaffType

--Begin table dropdown.FacilityCostServiceChargeTrend
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityCostServiceChargeTrend'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityCostServiceChargeTrend
	(
	FacilityCostServiceChargeTrendID INT IDENTITY(0,1) NOT NULL,
	FacilityCostServiceChargeTrendCode VARCHAR(50),
	FacilityCostServiceChargeTrendName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityCostServiceChargeTrendID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityCostServiceChargeTrend', 'DisplayOrder,FacilityCostServiceChargeTrendName', 'FacilityCostServiceChargeTrendID'
GO
--End table dropdown.FacilityCostServiceChargeTrend

--Begin table dropdown.FacilityCostWagePeriod
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityCostWagePeriod'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityCostWagePeriod
	(
	FacilityCostWagePeriodID INT IDENTITY(0,1) NOT NULL,
	FacilityCostWagePeriodCode VARCHAR(50),
	FacilityCostWagePeriodName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityCostWagePeriodID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityCostWagePeriod', 'DisplayOrder,FacilityCostWagePeriodName', 'FacilityCostWagePeriodID'
GO
--End table dropdown.FacilityCostWagePeriod

--Begin table facility.FacilityCost
DECLARE @TableName VARCHAR(250) = 'facility.FacilityCost'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityCost
	(
	FacilityCostID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	AssessorPersonID INT,
	AssessmentDate DATE,
	IsComplete BIT,
	FacilityCostServiceChargeTrendID INT,
	FacilityCostPatientTrendID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssessmentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AssessorPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostPatientTrendID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostServiceChargeTrendID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityCostID'
EXEC utility.SetIndexClustered @TableName, 'IX_FacilityCost', 'FacilityID'
GO
--End table facility.FacilityCost

--Begin table facility.FacilityCostConsumable
DECLARE @TableName VARCHAR(250) = 'facility.FacilityCostConsumable'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityCostConsumable
	(
	FacilityCostConsumableID INT IDENTITY(1,1) NOT NULL,
	FacilityCostID INT,
	ConsumableName VARCHAR(250),
	ConsumablePurchased BIT,
	ConsumablePurchaseYear INT,
	ConsumablePurchaseMonth INT,
	ConsumablePurchaseQuantity INT,
	ConsumablePurchasePrice NUMERIC(10,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConsumablePurchased', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConsumablePurchaseMonth', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConsumablePurchasePrice', 'NUMERIC(10,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConsumablePurchaseQuantity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConsumablePurchaseYear', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityCostConsumableID'
EXEC utility.SetIndexClustered @TableName, 'IX_FacilityCostConsumable', 'FacilityCostID'
GO
--End table facility.FacilityCostConsumable

--Begin table facility.FacilityCostEquipment
DECLARE @TableName VARCHAR(250) = 'facility.FacilityCostEquipment'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityCostEquipment
	(
	FacilityCostEquipmentID INT IDENTITY(1,1) NOT NULL,
	FacilityCostID INT,
	EquipmentName VARCHAR(250),
	EquipmentPurchased BIT,
	EquipmentPurchaseYear INT,
	EquipmentPurchaseMonth INT,
	EquipmentPurchaseQuantity INT,
	EquipmentPurchasePrice NUMERIC(10,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentPurchased', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentPurchaseMonth', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentPurchasePrice', 'NUMERIC(10,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentPurchaseQuantity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentPurchaseYear', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityCostEquipmentID'
EXEC utility.SetIndexClustered @TableName, 'IX_FacilityCostEquipment', 'FacilityCostID'
GO
--End table facility.FacilityCostEquipment

--Begin table facility.FacilityCostRepair
DECLARE @TableName VARCHAR(250) = 'facility.FacilityCostRepair'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityCostRepair
	(
	FacilityCostRepairID INT IDENTITY(1,1) NOT NULL,
	FacilityCostID INT,
	RepairName VARCHAR(250),
	RepairPurchased BIT,
	RepairPurchaseYear INT,
	RepairPurchaseMonth INT,
	RepairPurchasePrice NUMERIC(10,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RepairPurchased', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RepairPurchaseMonth', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RepairPurchasePrice', 'NUMERIC(10,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RepairPurchaseYear', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityCostRepairID'
EXEC utility.SetIndexClustered @TableName, 'IX_FacilityCostRepair', 'FacilityCostID'
GO
--End table facility.FacilityCostRepair

--Begin table facility.FacilityCostStaff
DECLARE @TableName VARCHAR(250) = 'facility.FacilityCostStaff'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityCostStaff
	(
	FacilityCostStaffID INT IDENTITY(1,1) NOT NULL,
	FacilityCostID INT,
	FacilityCostStaffTypeID INT,
	StaffCount INT,
	WageRate NUMERIC(18,2),
	FacilityCostWagePeriodID INT,
	AverageHoursNoProgram NUMERIC(18,2),
	AverageHoursQuality NUMERIC(18,2),
	AverageHoursWorkedPerWeek NUMERIC(18,2),
	HasFreeHealthCare BIT,
	HasPaidTimeOff BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AverageHoursNoProgram', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AverageHoursQuality', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AverageHoursWorkedPerWeek', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostStaffTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityCostWagePeriodID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasFreeHealthCare', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasPaidTimeOff', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StaffCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WageRate', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityCostStaffID'
EXEC utility.SetIndexClustered @TableName, 'IX_FacilityCostStaff', 'FacilityCostID'
GO
--End table facility.FacilityCostStaff
