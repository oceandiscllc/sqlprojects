
--Begin procedure dropdown.GetFacilityCostPatientTrendData
EXEC Utility.DropObject 'dropdown.GetFacilityCostPatientTrendData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.16
-- Description:	A stored procedure to return data from the dropdown.FacilityCostPatientTrend table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetFacilityCostPatientTrendData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityCostPatientTrendID, 
		T.FacilityCostPatientTrendCode,
		T.FacilityCostPatientTrendName,
		T.DisplayOrder
	FROM dropdown.FacilityCostPatientTrend T
	WHERE (T.FacilityCostPatientTrendID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityCostPatientTrendName, T.FacilityCostPatientTrendID

END
GO
--End procedure dropdown.GetFacilityCostPatientTrendData

--Begin procedure dropdown.GetFacilityCostServiceChargeTrendData
EXEC Utility.DropObject 'dropdown.GetFacilityCostServiceChargeTrendData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.16
-- Description:	A stored procedure to return data from the dropdown.FacilityCostServiceChargeTrend table
-- =====================================================================================================
CREATE PROCEDURE dropdown.GetFacilityCostServiceChargeTrendData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityCostServiceChargeTrendID, 
		T.FacilityCostServiceChargeTrendCode,
		T.FacilityCostServiceChargeTrendName,
		T.DisplayOrder
	FROM dropdown.FacilityCostServiceChargeTrend T
	WHERE (T.FacilityCostServiceChargeTrendID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityCostServiceChargeTrendName, T.FacilityCostServiceChargeTrendID

END
GO
--End procedure dropdown.GetFacilityCostServiceChargeTrendData

--Begin procedure dropdown.GetFacilityCostStaffTypeData
EXEC Utility.DropObject 'dropdown.GetFacilityCostStaffTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.16
-- Description:	A stored procedure to return data from the dropdown.FacilityCostStaffType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetFacilityCostStaffTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		newID() AS FacilityCostStaffTypeGUID,
		T.FacilityCostStaffTypeID, 
		T.FacilityCostStaffTypeDescription,
		T.FacilityCostStaffTypeName,
		T.DisplayOrder
	FROM dropdown.FacilityCostStaffType T
	WHERE (T.FacilityCostStaffTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityCostStaffTypeName, T.FacilityCostStaffTypeID

END
GO
--End procedure dropdown.GetFacilityCostStaffTypeData

--Begin procedure dropdown.GetFacilityCostWagePeriodData
EXEC Utility.DropObject 'dropdown.GetFacilityCostWagePeriodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.16
-- Description:	A stored procedure to return data from the dropdown.FacilityCostWagePeriod table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetFacilityCostWagePeriodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityCostWagePeriodID, 
		T.FacilityCostWagePeriodCode,
		T.FacilityCostWagePeriodName,
		T.DisplayOrder
	FROM dropdown.FacilityCostWagePeriod T
	WHERE (T.FacilityCostWagePeriodID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityCostWagePeriodName, T.FacilityCostWagePeriodID

END
GO
--End procedure dropdown.GetFacilityCostWagePeriodData

--Begin procedure dropdown.GetQualityStandardLookupDataByQualityStandardIDList
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataByQualityStandardIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to return data from the dropdown.QualityStandardLookup table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupDataByQualityStandardIDList

@QualityStandardIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		QSL.ManualName,
		QSL.ChapterName,
		QSL.StandardName,
		QSL.ObjectiveElementName,
		QSL.VerificationCriteriaName,
		QSL.QualityStandardID
	FROM dropdown.QualityStandardLookup QSL
	WHERE QSL.HasChildren = 0
		AND 
			(
			@QualityStandardIDList IS NULL
				OR EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@QualityStandardIDList, ',') LTT 
					WHERE CAST(LTT.ListItem AS INT) = QSL.QualityStandardID
					)
			)

END
GO
--End procedure dropdown.GetQualityStandardLookupDataByQualityStandardIDList

--Begin procedure eventlog.LogDataExportAction
EXEC utility.DropObject 'eventlog.LogDataExportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDataExportAction

@PersonID INT,
@DataExportActionXML VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EventData)
	VALUES
		(
		@PersonID,
		'create',
		'Export',
		@DataExportActionXML
		)

END
GO
--End procedure eventlog.LogDataExportAction

--Begin procedure eventlog.LogFacilityCostAction
EXEC utility.DropObject 'eventlog.LogFacilityCostAction'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2019.03.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFacilityCostAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'FacilityCost',
			T.FacilityCostID,
			@Comments,
			@ProjectID
		FROM facility.FacilityCost T
		WHERE T.FacilityCostID = @EntityID

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogFacilityCostActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogFacilityCostActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogFacilityCostActionTable
		FROM facility.FacilityCost T
		WHERE T.FacilityCostID = @EntityID

		DECLARE @cFacilityCostConsumable VARCHAR(MAX) = ''
		DECLARE @cFacilityCostEquipment VARCHAR(MAX) = ''
		DECLARE @cFacilityCostRepair VARCHAR(MAX) = ''
		DECLARE @cFacilityCostStaff VARCHAR(MAX) = ''
		
		SELECT @cFacilityCostConsumable = COALESCE(@cFacilityCostConsumable, '') + D.FacilityCostConsumable
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityCostConsumable'), ELEMENTS) AS FacilityCostConsumable
			FROM facility.FacilityCostConsumable T 
			WHERE T.FacilityCostID = @EntityID
			) D
		
		SELECT @cFacilityCostEquipment = COALESCE(@cFacilityCostEquipment, '') + D.FacilityCostEquipment
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityCostInclusionCriterion'), ELEMENTS) AS FacilityCostEquipment
			FROM facility.FacilityCostEquipment T 
			WHERE T.FacilityCostID = @EntityID
			) D
		
		SELECT @cFacilityCostRepair = COALESCE(@cFacilityCostRepair, '') + D.FacilityCostRepair
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW(''), ELEMENTS) AS FacilityCostRepair
			FROM facility.FacilityCostRepair T 
			WHERE T.FacilityCostID = @EntityID
			) D
		
		SELECT @cFacilityCostStaff = COALESCE(@cFacilityCostStaff, '') + D.FacilityCostStaff
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW(''), ELEMENTS) AS FacilityCostStaff
			FROM facility.FacilityCostStaff T 
			WHERE T.FacilityCostID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FacilityCost',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<FacilityCostConsumables>' + @cFacilityCostConsumable + '</FacilityCostConsumables>') AS XML),
			CAST(('<FacilityCostEquipment>' + @cFacilityCostEquipment + '</FacilityCostEquipment>') AS XML),
			CAST(('<FacilityCostRepair>' + @cFacilityCostRepair + '</FacilityCostRepair>') AS XML),
			CAST(('<FacilityCostStaff>' + @cFacilityCostStaff + '</FacilityCostStaff>') AS XML)
			FOR XML RAW('FacilityCost'), ELEMENTS
			)
		FROM #LogFacilityCostActionTable T
			JOIN facility.FacilityCost F ON F.FacilityCostID = T.FacilityCostID

		DROP TABLE #LogFacilityCostActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFacilityCostAction

--Begin procedure facility.GetFacilityCostByFacilityCostID
EXEC utility.DropObject 'facility.GetFacilityCostByFacilityCostID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Tod Pires
-- Create date:	2019.03.16
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE facility.GetFacilityCostByFacilityCostID

@FacilityCostID INT

AS
BEGIN
	SET NOCOUNT ON;

	--FacilityCost
	SELECT
		F.FacilityID,
		F.FacilityName,
		FC.AssessmentDate,
		core.FormatDate(FC.AssessmentDate) AS AssessmentDateFormatted,
		FC.AssessorPersonID,
		person.FormatPersonNameByPersonID(FC.AssessorPersonID, 'LastFirst') AS AssessorPersonNameFormatted,
		FC.FacilityCostID,
		FC.IsComplete,
		FCPT.FacilityCostPatientTrendID,
		FCPT.FacilityCostPatientTrendName,
		FCSCT.FacilityCostServiceChargeTrendID,
		FCSCT.FacilityCostServiceChargeTrendName
	FROM facility.FacilityCost FC
		JOIN facility.Facility F ON F.FacilityID = FC.FacilityID
		JOIN dropdown.FacilityCostPatientTrend FCPT ON FCPT.FacilityCostPatientTrendID = FC.FacilityCostPatientTrendID
		JOIN dropdown.FacilityCostServiceChargeTrend FCSCT ON FCSCT.FacilityCostServiceChargeTrendID = FC.FacilityCostServiceChargeTrendID
			AND FC.FacilityCostID = @FacilityCostID

	--FacilityCostConsumable
	SELECT
		newID() AS FacilityCostConsumableGUID,
		FCC.ConsumableName,
		FCC.ConsumablePurchased,
		FCC.ConsumablePurchaseMonth,
		FCC.ConsumablePurchasePrice,
		FCC.ConsumablePurchaseQuantity,
		FCC.ConsumablePurchaseYear,
		FCC.FacilityCostConsumableID
	FROM facility.FacilityCostConsumable FCC
	WHERE FCC.FacilityCostID = @FacilityCostID

	--FacilityCostEquipment
	SELECT
		newID() AS FacilityCostEquipmentGUID,
		FCE.EquipmentName,
		FCE.EquipmentPurchased,
		FCE.EquipmentPurchaseMonth,
		FCE.EquipmentPurchasePrice,
		FCE.EquipmentPurchaseQuantity,
		FCE.EquipmentPurchaseYear,
		FCE.FacilityCostEquipmentID
	FROM facility.FacilityCostEquipment FCE
	WHERE FCE.FacilityCostID = @FacilityCostID

	--FacilityCostRepair
	SELECT
		newID() AS FacilityCostRepairGUID,
		FCR.FacilityCostRepairID,
		FCR.RepairName,
		FCR.RepairPurchased,
		FCR.RepairPurchaseMonth,
		FCR.RepairPurchasePrice,
		FCR.RepairPurchaseYear
	FROM facility.FacilityCostRepair FCR
	WHERE FCR.FacilityCostID = @FacilityCostID

	--FacilityCostStaff
	SELECT
		newID() AS FacilityCostStaffGUID,
		FCS.AverageHoursNoProgram,
		FCS.AverageHoursQuality,
		FCS.AverageHoursWorkedPerWeek,
		FCS.FacilityCostStaffID,
		FCS.HasFreeHealthCare,
		FCS.HasPaidTimeOff,
		FCS.StaffCount,
		FCS.WageRate,
		FCST.DisplayOrder, 
		FCST.FacilityCostStaffTypeID,
		FCST.FacilityCostStaffTypeName,
		FCWP.FacilityCostWagePeriodID,
		FCWP.FacilityCostWagePeriodName
	FROM facility.FacilityCostStaff FCS
		JOIN facility.FacilityCost FC ON FCS.FacilityCostID = FC.FacilityCostID
		JOIN dropdown.FacilityCostStaffType FCST ON FCST.FacilityCostStaffTypeID = FCS.FacilityCostStaffTypeID
		JOIN dropdown.FacilityCostWagePeriod FCWP ON FCWP.FacilityCostWagePeriodID = FCS.FacilityCostWagePeriodID
			AND FCS.FacilityCostID = @FacilityCostID
	ORDER BY FCST.DisplayOrder, FCST.FacilityCostStaffTypeName, FCST.FacilityCostStaffTypeID

END
GO
--End procedure facility.GetFacilityCostByFacilityCostID

--Begin procedure facility.ImportFacilities
EXEC utility.DropObject 'facility.ImportFacilities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Tod Pires
-- Create date:	2018.09.15
-- Description:	A stored procedure to import facility data from the MNH database to the MATH database
-- ==================================================================================================
CREATE PROCEDURE facility.ImportFacilities

@FacilityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE MATHF
	SET
		MATHF.Address = MNHF.Address,
		MATHF.BedCount = MNHF.BedCount,
		MATHF.Description = MNHF.Description,
		MATHF.FacilityEstablishedDate = MNHF.FacilityEstablishedDate,
		MATHF.FacilityName = MNHF.FacilityName,
		MATHF.FacilityServiceID = MNHF.FacilityServiceID,
		MATHF.FacilityStatusID = MNHF.FacilityStatusID,
		MATHF.InclusionCriteriaNotes = MNHF.InclusionCriteriaNotes,
		MATHF.InclusionEligibilityStatusID = MNHF.InclusionEligibilityStatusID,
		MATHF.Location = MNHF.Location,
		MATHF.MOUDate = MNHF.MOUDate,
		MATHF.MOUNotes = MNHF.MOUNotes,
		MATHF.MOUStatusID = MNHF.MOUStatusID,
		MATHF.Phone = MNHF.Phone,
		MATHF.PrimaryContactID = MNHF.PrimaryContactID,
		MATHF.ProjectID = (SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MATHF.TerritoryID = MNHF.TerritoryID
	FROM MATH.facility.Facility MATHF
		JOIN MNH.facility.Facility MNHF ON MNHF.FacilityID = MATHF.FacilityID
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MATHF.FacilityID
	;

	SET IDENTITY_INSERT MATH.facility.Facility ON;

	INSERT INTO MATH.facility.Facility
		(Address,BedCount,Description,FacilityEstablishedDate,FacilityID,FacilityName,FacilityServiceID,FacilityStatusID,InclusionCriteriaNotes,InclusionEligibilityStatusID,Location,MOUDate,MOUNotes,MOUStatusID,Phone,PrimaryContactID,ProjectID,TerritoryID)
	SELECT 
		MNHF.Address,
		MNHF.BedCount,
		MNHF.Description,
		MNHF.FacilityEstablishedDate,
		MNHF.FacilityID,
		MNHF.FacilityName,
		MNHF.FacilityServiceID,
		MNHF.FacilityStatusID,
		MNHF.InclusionCriteriaNotes,
		MNHF.InclusionEligibilityStatusID,
		MNHF.Location,
		MNHF.MOUDate,
		MNHF.MOUNotes,
		MNHF.MOUStatusID,
		MNHF.Phone,
		MNHF.PrimaryContactID,
		(SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MNHF.TerritoryID
	FROM MNH.facility.Facility MNHF
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MNHF.FacilityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM MATH.facility.Facility MATHF
				WHERE MATHF.FacilityID = MNHF.FacilityID
				)
	;

	SET IDENTITY_INSERT MATH.facility.Facility OFF;

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria ON;

	--FacilityInclusionCriteria
	MERGE MATH.facility.FacilityInclusionCriteria T1
	USING 
		(
		SELECT 
			FIC.FacilityID, 
			FIC.FacilityInclusionCriteriaID,
			FIC.InclusionCriteriaID,
			FIC.IsCriteriaMet
		FROM MNH.facility.FacilityInclusionCriteria FIC
			JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FIC.FacilityID
		) T2
		ON T2.FacilityID = T1.FacilityID
			AND T2.InclusionCriteriaID = T1.InclusionCriteriaID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.IsCriteriaMet = T2.IsCriteriaMet
	WHEN NOT MATCHED THEN
	INSERT 
		(FacilityID,FacilityInclusionCriteriaID,InclusionCriteriaID,IsCriteriaMet)
	VALUES
		(
		T2.FacilityID,
		T2.FacilityInclusionCriteriaID,
		T2.InclusionCriteriaID,
		T2.IsCriteriaMet
		);

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria OFF;

	SET IDENTITY_INSERT MATH.contact.Contact ON;

	--FacilityContact
	MERGE MATH.contact.Contact T1
	USING 
		(
		SELECT
			C1.CellPhoneNumber,
			C1.ContactFunctionID,
			C1.ContactID,
			C1.ContactRoleID,
			C1.DateOfBirth,
			C1.DIBRoleID,
			C1.EmailAddress,
			C1.FacilityEndDate,
			C1.FacilityID,
			C1.FacilityStartDate,
			C1.FirstName,
			C1.GenderID,
			C1.IsActive,
			C1.LastName,
			C1.MiddleName,
			C1.OtherContactFunction,
			C1.OtherContactRole,
			C1.PhoneNumber,
			C1.Title,
			C1.YearsExperience
		FROM MNH.contact.Contact C1
		WHERE EXISTS
			(
			SELECT 1
			FROM MNH.facility.FacilityMOUContact FMC
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FMC.FacilityID
					AND FMC.ContactID = C1.ContactID

			UNION

			SELECT 1
			FROM MNH.contact.Contact C2
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C2.FacilityID
					AND C2.ContactID = C1.ContactID
			) 
		) T2
		ON T2.ContactID = T1.ContactID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.CellPhoneNumber = T2.CellPhoneNumber,
		T1.ContactFunctionID = T2.ContactFunctionID,
		T1.ContactRoleID = T2.ContactRoleID,
		T1.DateOfBirth = T2.DateOfBirth,
		T1.DIBRoleID = T2.DIBRoleID,
		T1.EmailAddress = T2.EmailAddress,
		T1.FacilityEndDate = T2.FacilityEndDate,
		T1.FacilityID = T2.FacilityID,
		T1.FacilityStartDate = T2.FacilityStartDate,
		T1.FirstName = T2.FirstName,
		T1.GenderID = T2.GenderID,
		T1.IsActive = T2.IsActive,
		T1.LastName = T2.LastName,
		T1.MiddleName = T2.MiddleName,
		T1.OtherContactFunction = T2.OtherContactFunction,
		T1.OtherContactRole = T2.OtherContactRole,
		T1.PhoneNumber = T2.PhoneNumber,
		T1.Title = T2.Title,
		T1.YearsExperience = T2.YearsExperience 
	WHEN NOT MATCHED THEN
	INSERT 
		(CellPhoneNumber,ContactFunctionID,ContactID,ContactRoleID,DateOfBirth,DIBRoleID,EmailAddress,FacilityEndDate,FacilityID,FacilityStartDate,FirstName,GenderID,IsActive,LastName,MiddleName,OtherContactFunction,OtherContactRole,PhoneNumber,Title,YearsExperience)
	VALUES
		(
		T2.CellPhoneNumber,
		T2.ContactFunctionID,
		T2.ContactID,
		T2.ContactRoleID,
		T2.DateOfBirth,
		T2.DIBRoleID,
		T2.EmailAddress,
		T2.FacilityEndDate,
		T2.FacilityID,
		T2.FacilityStartDate,
		T2.FirstName,
		T2.GenderID,
		T2.IsActive,
		T2.LastName,
		T2.MiddleName,
		T2.OtherContactFunction,
		T2.OtherContactRole,
		T2.PhoneNumber,
		T2.Title,
		T2.YearsExperience
		);

	SET IDENTITY_INSERT MATH.contact.Contact OFF;

END
GO

IF '[[INSTANCENAME]]' <> 'MATH'
	EXEC Utility.DropObject 'facility.ImportFacilities'
--ENDIF
GO
--End procedure facility.ImportFacilities

--Begin procedure reporting.GetFacilityCostConsumables
EXEC Utility.DropObject 'reporting.GetFacilityCostConsumables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE reporting.GetFacilityCostConsumables

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	              DENSE_RANK() OVER(ORDER BY FC.FacilityCostID) AS Rank,
				  FC.FacilityCostID,
                  FCC.ConsumableName as ConsumableName,
				     CASE WHEN FCC.ConsumablePurchased = 1 THEN 'Yes' 
							 WHEN FCC.ConsumablePurchased = 0 THEN 'No'  END AS PurchasedifNotInProgram,
				  FCC.ConsumablePurchaseMonth AS PurchaseMonth,
				  FCC.ConsumablePurchaseYear AS PurchaseYear,
				  FCC.ConsumablePurchaseQuantity AS PurchaseQuantity,
				  FCC.ConsumablePurchasePrice AS UnitCost		 
	FROM		facility.FacilityCost FC
					LEFT JOIN facility.FacilityCostConsumable FCC ON FCC.FacilityCostID = FC.FacilityCostID
					JOIN Reporting.SearchResult SR ON SR.EntityID = FC.FacilityCostID
   WHERE	SR.EntityTypeCode = 'FacilityCost'
					 AND SR.PersonID = 2	

END
GO
--End procedure reporting.GetFacilityCostConsumables

--Begin procedure reporting.GetFacilityCostEquipmentDetails
EXEC Utility.DropObject 'reporting.GetFacilityCostEquipmentDetails'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE reporting.GetFacilityCostEquipmentDetails

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT     
	                DENSE_RANK() OVER(ORDER BY  FC.FacilityCostID) AS Rank ,
				   FC.FacilityCostID,
				   FC.FacilityID,
                   FCE.EquipmentName as EquipmentName,
				      CASE WHEN FCE.EquipmentPurchased = 1 THEN 'Yes' 
							 WHEN  FCE.EquipmentPurchased = 0 THEN 'No'  END  AS  IfPurchases,
				   FCE.EquipmentPurchaseMonth AS PurchaseMonth,
				   FCE.EquipmentPurchaseYear AS PurchaseYear,
				  FCE.EquipmentPurchaseQuantity AS PurchaseQuantity,
				  FCE.EquipmentPurchasePrice AS UnitCost

	FROM		facility.FacilityCost FC
					LEFT JOIN facility.FacilityCostEquipment FCE ON FCE.FacilityCostID = FC.FacilityCostID
					JOIN Reporting.SearchResult SR ON SR.EntityID = FC.FacilityCostID
   WHERE	SR.EntityTypeCode = 'FacilityCost'
					 AND SR.PersonID = @PersonID	
  ORDER BY Rank, FCE.EquipmentName

END
GO
--End procedure reporting.GetFacilityCostEquipmentDetails

--Begin procedure reporting.GetFacilityCostInformation
EXEC Utility.DropObject 'reporting.GetFacilityCostInformation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE reporting.GetFacilityCostInformation

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
SELECT
	                 DENSE_RANK() OVER(ORDER BY FC.FacilityCostID) AS Rank,
					FC.FacilityID,
					person.FormatPersonNameByPersonID(FC.AssessorPersonID, 'LastFirst') AS Assessor,		
					[dropdown].[GetFacilityNameByFacilityID](FC.FacilityID) AS Facility,
					core.FormatDate(FC.AssessmentDate) AS AssessmentDate,
					CASE WHEN FC.IsComplete = 1 THEN 'Yes' ELSE 'No' END AS AssessmentStatus,
					FCPT.FacilityCostPatientTrendName AS PatientTrend,
					FCCT.FacilityCostServiceChargeTrendName AS ServiceChargeTrend
   FROM		facility.FacilityCost FC
					JOIN Reporting.SearchResult SR ON SR.EntityID = FC.FacilityCostID
					LEFT JOIN dropdown.FacilityCostServiceChargeTrend FCCT ON FCCT.FacilityCostServiceChargeTrendID = FC.FacilityCostServiceChargeTrendID
					LEFT JOIN dropdown.FacilityCostPatientTrend FCPT ON FCPT.FacilityCostPatientTrendID = FC.FacilityCostPatientTrendID
   WHERE   SR.EntityTypeCode = 'FacilityCost'
					AND SR.PersonID = @PersonID
END
GO
--End procedure reporting.GetFacilityCostInformation

--Begin procedure reporting.GetFacilityCostRepairs
EXEC Utility.DropObject 'reporting.GetFacilityCostRepairs'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE reporting.GetFacilityCostRepairs

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	               DENSE_RANK() OVER(ORDER BY FC.FacilityCostID) AS Rank,
				    FC.FacilityCostID,
                   FCR.RepairName AS RepairsorImprovements,
				   CASE WHEN FCR.RepairPurchased = 1 THEN 'Yes' 
							 WHEN FCR.RepairPurchased = 0 THEN 'No'  END AS PurchasedifNotInProgram,
				   FCR.RepairPurchaseMonth AS PurchaseMonth,
				   FCR.RepairPurchaseYear AS PurchaseYear,
				   FCR.RepairPurchasePrice AS Cost
	FROM		facility.FacilityCost FC
					LEFT JOIN facility.FacilityCostRepair FCR ON FCR.FacilityCostID = FC.FacilityCostID
					JOIN Reporting.SearchResult SR ON SR.EntityID = FC.FacilityCostID
   WHERE	SR.EntityTypeCode = 'FacilityCost'
					 AND SR.PersonID = @PersonID

END
GO
--End procedure reporting.GetFacilityCostRepairs

--Begin procedure reporting.GetFacilityData
EXEC Utility.DropObject 'reporting.GetFacilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE reporting.GetFacilityData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.BedCount,
		F.FacilityEstablishedDate,
		F.FacilityID,
		CASE WHEN F.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActive,
		F.MOUDate,
		FS.FacilityServiceName,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = F.FacilityServiceID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
		JOIN Reporting.SearchResult SR ON SR.EntityID = F.FacilityID
	    AND SR.EntityTypeCode = 'Facility'
	    AND SR.PersonID = @PersonID

END
GO
--End procedure reporting.GetFacilityData

--Begin procedure reporting.GetFacilityStaffDetails
EXEC Utility.DropObject 'reporting.GetFacilityStaffDetails'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.FacilityCost table
-- ================================================================================
CREATE PROCEDURE reporting.GetFacilityStaffDetails

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	                 DENSE_RANK() OVER(ORDER BY FC.FacilityCostID) AS Rank,
					 FC.FacilityCostID,
                    FCST.FacilityCostStaffTypeName AS RepairsorImprovements,
				    FCS.StaffCount AS NoInvolved,
				    FCS.AverageHoursNoProgram AS CertHrs,
				    FCS.AverageHoursQuality  AS QltyImprvmntHrs,
				    FCS.WageRate  AS WageRate,
				    FCS.FacilityCostWagePeriodID  AS WageRatePeriod,
				    FCS.AverageHoursWorkedPerWeek  AS HoursperWeek,
				    CASE WHEN FCS.HasFreeHealthCare = 1 THEN 'Yes'
					           WHEN FCS.HasFreeHealthCare =  0 THEN  'No' END AS PdHealthCare,
				    CASE WHEN FCS.HasPaidTimeOff = 1 THEN 'Yes'
					          WHEN FCS.HasPaidTimeOff =  0 THEN  'No' END AS PdTimeOff		
	FROM		facility.FacilityCost FC
					LEFT JOIN facility.FacilityCostStaff FCS ON FCS.FacilityCostID = FC.FacilityCostID
					LEFT JOIN dropdown.FacilityCostStaffType FCST ON FCST.FacilityCostStaffTypeID = FCS.FacilityCostStaffTypeID
					JOIN Reporting.SearchResult SR ON SR.EntityID = FC.FacilityCostID
   WHERE	SR.EntityTypeCode = 'FacilityCost'
					 AND SR.PersonID = @PersonID	

END
GO
--End procedure reporting.GetFacilityStaffDetails

--Begin procedure reporting.GetVisitData
EXEC Utility.DropObject 'reporting.GetVisitData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the visit.Visit table
-- ============================================================================
CREATE PROCEDURE reporting.GetVisitData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		QSL.ChapterName,
		QSL.ManualName,
		QSL.ObjectiveElementName,
		QSL.StandardName,
		QSL.VerificationCriteriaName,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		CASE WHEN V.IsPlanningComplete = 1 THEN 'Yes' ELSE 'No' END AS IsPlanningComplete,
		CASE WHEN V.IsVisitComplete = 1 THEN 'Yes' ELSE 'No' END AS IsVisitComplete,
		V.VisitID, 	
		V.VisitOutcomeNote,
		V.VisitStartDateTime,
		V.VisitTime,
		VO.VisitOutcomeName,
		VQS.CorrectRecords, 
		VQS.HasDocumentation,
		VQS.HasImplementation, 
		VQS.HasObservation, 
		VQS.HasPhysicalVerification, 
		VQS.HasProviderInterview, 
		CASE WHEN VQS.IsScored = 1 THEN 'Yes' ELSE 'No' END AS IsScored, 
		VQS.NotApplicable,
		VQS.Notes, 
		VQS.Score, 
		VQS.TotalRecordsChecked
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitID = V.VisitID
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID
		JOIN Reporting.SearchResult SR ON SR.EntityID = V.VisitID
			AND SR.EntityTypeCode = 'Visit'
	    AND SR.PersonID = @PersonID

END
GO
--End procedure reporting.GetVisitData

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		F.ProjectID,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitOutcomeNote,

		CASE 
			WHEN V.IsPlanningComplete = 0
			THEN 'Planning'
			WHEN V.IsVisitComplete = 1
			THEN 'Visit Complete'
			ELSE 'Planning Complete'
		END AS VisitStatusName,

		V.VisitStartDateTime,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID, 	
		V.VisitLeadPersonID,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitTime,
		VO.VisitOutcomeID,
		VO.VisitOutcomeName,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT
		QS.GuidanceNotes,
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.IsScored,
		VQS.NotApplicable,
		VQS.Score,
		visit.GetAssessmentEvidenceTypeIDList(QSL.QualityStandardID) AS AssessmentEvidenceTypeIDList,
		visit.GetPriorVisitQualityStandardScore(QSL.QualityStandardID, @VisitID) AS PriorScore
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = QSL.QualityStandardID
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.scoreQualityStandardAssessment
EXEC Utility.DropObject 'visit.scoreQualityStandardAssessment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.31
-- Description:	A stored procedure to get data from the dropdown.QualityStandardAssessmentEvidenceType table
-- =========================================================================================================
CREATE PROCEDURE visit.scoreQualityStandardAssessment

@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		AET.AssessmentEvidenceTypeCode,
		QSL.ManualName
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = QSAET.QualityStandardID
			AND QSAET.QualityStandardID = @QualityStandardID
	
END
GO
--End procedure visit.scoreQualityStandardAssessment

--Begin procedure utility.ClonePasswordData
EXEC utility.DropObject 'utility.ClonePasswordData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.14
-- Description:	A stored procedure to clone the password and password salt values from a person.Person record to those of another person.Person record
-- ===================================================================================================================================================
CREATE PROCEDURE utility.ClonePasswordData

@PersonID INT,
@UserName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cXMLString XML

	SELECT @cXMLString = 
		(
		SELECT 
			P.Password, 
			P.PasswordSalt
		FROM person.Person P
		WHERE P.UserName = @UserName
		FOR XML PATH('Person')
		)

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
	SELECT
		@PersonID,
		'clonepassword',
		'Person',
		P.PersonID,
		'Password cloned for troubleshooting',
		@cXMLString
	FROM person.Person P
	WHERE P.UserName = @UserName

	UPDATE P1
	SET
		P1.Password = P2.Password,
		P1.PasswordSalt = P2.PasswordSalt
	FROM person.Person P1, person.Person P2
	WHERE P1.UserName = @UserName
		AND P2.PersonID = @PersonID

END
GO
--End procedure utility.ClonePasswordData

--Begin procedure utility.RevertPasswordData
EXEC utility.DropObject 'utility.RevertPasswordData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.14
-- Description:	A stored procedure to revert the password and password salt values of a person.Person record from those of another person.Person record
-- ====================================================================================================================================================
CREATE PROCEDURE utility.RevertPasswordData

@UserName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cXMLString XML

	SELECT TOP 1
		@cXMLString = EL.EventData
	FROM eventlog.EventLog EL
		JOIN person.Person P ON P.PersonID = EL.EntityID
			AND EL.EntityTypeCode = 'Person'
			AND EL.EventCode = 'clonepassword'
	ORDER BY EL.EventLogID DESC

	IF @cXMLString IS NOT NULL
		BEGIN

		DECLARE @cPassword VARCHAR(64)
		DECLARE @cPasswordSalt VARCHAR(50)

		SELECT
			@cPassword = Person.D.value('Password[1]', 'VARCHAR(64)'), 
			@cPasswordSalt = Person.D.value('PasswordSalt[1]', 'VARCHAR(50)')
		FROM @cXMLString.nodes('Person') Person(D)

		UPDATE P
		SET
			P.Password = @cPassword,
			P.PasswordSalt = @cPasswordSalt
		FROM person.Person P
		WHERE P.UserName = @UserName

		END
	--ENDIF

END
GO
--End procedure utility.RevertPasswordData
