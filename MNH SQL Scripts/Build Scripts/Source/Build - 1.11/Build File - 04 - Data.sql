--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'FacilityCost', 
	@EntityTypeName = 'Facility Cost', 
	@EntityTypeNamePlural = 'Facility Costs',
	@SchemaName = 'facility', 
	@TableName = 'FacilityCost', 
	@PrimaryKeyFieldName = 'FacilityCostID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'QualityAssessmentList',
	@NewMenuItemCode = 'FacilityCostList',
	@NewMenuItemLink = '/facilitycost/list',
	@NewMenuItemText = 'Facility Costs',
	@ParentMenuItemCode = 'Visits',
	@PermissionableLineageList = 'FacilityCost.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Visits'
GO
--End table core.MenuItem

--Begin table dropdown.FacilityCostPatientTrend
TRUNCATE TABLE dropdown.FacilityCostPatientTrend
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityCostPatientTrend', 'FacilityCostPatientTrendID', 0
GO

INSERT INTO dropdown.FacilityCostPatientTrend
	(FacilityCostPatientTrendCode, FacilityCostPatientTrendName, DisplayOrder) 
VALUES 
	('LargeIncrease', 'Yes, many more patients are coming to the facility', 1),
	('SmallIncrease', 'Yes, a few more patients are coming to the facility', 2),
	('NoChange', 'No, the number of patients has not changed much', 3),
	('SmallDecrease', 'Yes, slightly fewer patients are coming to the facility', 4),
	('LargeDecrease', 'Yes, a lot fewer patients are coming to the facility', 5)
GO
--End table dropdown.FacilityCostPatientTrend

--Begin table dropdown.FacilityCostServiceChargeTrend
TRUNCATE TABLE dropdown.FacilityCostServiceChargeTrend
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityCostServiceChargeTrend', 'FacilityCostServiceChargeTrendID', 0
GO

INSERT INTO dropdown.FacilityCostServiceChargeTrend
	(FacilityCostServiceChargeTrendCode, FacilityCostServiceChargeTrendName, DisplayOrder) 
VALUES 
	('LargeIncrease', 'Yes, prices have increased a lot', 1),
	('SmallIncrease', 'Yes, prices have increased a little', 2),
	('NoChange', 'No, prices have not changed much', 3),
	('SmallDecrease', 'Yes, prices have decreased a little', 4),
	('LargeDecrease', 'Yes, prices have decreased a lot', 5)
GO
--End table dropdown.FacilityCostServiceChargeTrend

--Begin table dropdown.FacilityCostStaffType
TRUNCATE TABLE dropdown.FacilityCostStaffType
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityCostStaffType', 'FacilityCostStaffTypeID', 0
GO

INSERT INTO dropdown.FacilityCostStaffType
	(FacilityCostStaffTypeName, FacilityCostStaffTypeDescription, DisplayOrder) 
VALUES 
	('Hospital Manager', '(e.g., the Chief Medical Officer, health officers, hospital administrators)', 1),
	('Allopathic Doctors', '(e.g., MBBS, obstetricians, gynaecologists, paediatricians)', 2),
	('AYUSH doctors', '(e.g., Homeopathic, Ayurvedic, Siddha, and Unani doctors)', 3),
	('Nurses', '(Nurses)', 4),
	('Pharmacists/Chemists', '(including pharmacy assistants)', 5),
	('All other health professionals', '(e.g., medical assistants, physiotherapists)', 6)
GO
--End table dropdown.FacilityCostStaffType

--Begin table dropdown.FacilityCostWagePeriod
TRUNCATE TABLE dropdown.FacilityCostWagePeriod
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityCostWagePeriod', 'FacilityCostWagePeriodID', 0
GO

INSERT INTO dropdown.FacilityCostWagePeriod
	(FacilityCostWagePeriodCode, FacilityCostWagePeriodName, DisplayOrder) 
VALUES 
	('Hour', 'Per Hour', 1),
	('Week', 'Per week', 2),
	('Month', 'Per month', 3),
	('Year', 'Per year', 4)
GO
--End table dropdown.FacilityCostWagePeriod

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='FacilityCost', @DESCRIPTION='Add / edit a facility cost', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='FacilityCost.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FacilityCost', @DESCRIPTION='Export the list of facility costs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='FacilityCost.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FacilityCost', @DESCRIPTION='View the list of facility costs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='FacilityCost.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FacilityCost', @DESCRIPTION='View a facility cost record', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='FacilityCost.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Export the facility list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List.Export', @PERMISSIONCODE='Export';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Export the visit list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitPlanList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitPlanList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Export the visit list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitReportList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitReportList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Reopen a clompleted visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.View.Reopen', @PERMISSIONCODE='Reopen';
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table core.SystemSetup
IF (SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'Environment') = 'Prod'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'SSRSDomain', NULL, 'https://skproddb02.skhosting.co.uk'
	EXEC core.SystemSetupAddUpdate 'SSRSPassword', NULL, 'R#$$R$User2018!!'
	EXEC core.SystemSetupAddUpdate 'SSRSReportPath', NULL, '/[[INSTANCENAME]]Reporting/'
	EXEC core.SystemSetupAddUpdate 'SSRSReportServerPath', NULL, 'ReportServer'
	EXEC core.SystemSetupAddUpdate 'SSRSUserName', NULL, 'ssrsuser'

	END
ELSE	
	BEGIN

	EXEC core.SystemSetupAddUpdate 'SSRSDomain', NULL, 'https://devdb01.oceandisc.com'
	EXEC core.SystemSetupAddUpdate 'SSRSPassword', NULL, 'R#nsha256'
	EXEC core.SystemSetupAddUpdate 'SSRSReportPath', NULL, '/[[INSTANCENAME]]Reporting/'
	EXEC core.SystemSetupAddUpdate 'SSRSReportServerPath', NULL, 'ReportServer'
	EXEC core.SystemSetupAddUpdate 'SSRSUserName', NULL, 'Administrator'

	END
--ENDIF
GO
--End table core.SystemSetup
