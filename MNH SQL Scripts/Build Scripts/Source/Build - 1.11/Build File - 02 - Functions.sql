
--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function visit.GetAssessmentEvidenceTypeIDList
EXEC utility.DropObject 'visit.GetAssessmentEvidenceTypeIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.01
-- Description:	A function to return a list of AssessmentEvidenceTypeID's for associated with a QualityStandardID
-- ==============================================================================================================

CREATE FUNCTION visit.GetAssessmentEvidenceTypeIDList
(
@QualityStandardID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cAssessmentEvidenceTypeIDList VARCHAR(MAX) = ''

	SELECT @cAssessmentEvidenceTypeIDList = COALESCE(@cAssessmentEvidenceTypeIDList + ',', '') + CAST(QSAET.AssessmentEvidenceTypeID AS VARCHAR(5))
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID
	ORDER BY AET.DisplayOrder, QSAET.AssessmentEvidenceTypeID

	IF LEFT(@cAssessmentEvidenceTypeIDList, 1) = ','
		SET @cAssessmentEvidenceTypeIDList = STUFF(@cAssessmentEvidenceTypeIDList, 1, 1, '')
	--ENDIF

	RETURN ISNULL(@cAssessmentEvidenceTypeIDList, '')

END
GO
--End function visit.GetAssessmentEvidenceTypeIDList

--Begin function visit.GetPriorVisitQualityStandardScore
EXEC utility.DropObject 'visit.GetPriorVisitQualityStandardScore'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.29
-- Description:	A function to return a the previous score for a specific facility and quality standard
--
-- Author:			Jon Cole 
-- Create date:	2019.02.12
-- Description:	Added the AND visit is complete clause 
-- ===================================================================================================

CREATE FUNCTION visit.GetPriorVisitQualityStandardScore
(
@QualityStandardID INT,
@VisitID INT
)

RETURNS VARCHAR(50)

AS
BEGIN
	
	DECLARE @cPriorScore VARCHAR(50)

	SELECT TOP 1 
		@cPriorScore = CASE WHEN VQS.NotApplicable = 1 THEN 'Not Applicable' ELSE CAST(VQS.Score AS VARCHAR(50)) END
	FROM visit.VisitQualityStandard VQS
		JOIN visit.Visit V1 ON V1.VisitID = VQS.VisitID
			AND VQS.QualityStandardID = @QualityStandardID
			AND V1.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
			AND V1.VisitID <> @VisitID
			AND V1.IsVisitComplete = 1
			--AND V1.VisitStartDateTime < (SELECT V2.VisitStartDateTime FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
	ORDER BY V1.VisitStartDateTime DESC

	RETURN ISNULL(@cPriorScore, 'Not Assessed')

END
GO
--End function visit.GetPriorVisitQualityStandardScore

--Begin function visit.GetQualityStandardScoreByFacilityID
EXEC utility.DropObject 'visit.GetQualityStandardScoreByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.01
-- Description:	A function to return the most recent score for a QualityStandardID for a Facility
-- ==============================================================================================

CREATE FUNCTION visit.GetQualityStandardScoreByFacilityID
(
@FacilityID INT
)

RETURNS VARCHAR(15)

AS
BEGIN

	DECLARE @cReturn VARCHAR(15)

	SELECT TOP 1 @cReturn =
		CASE 
			WHEN VQS.IsScored = 1
			THEN CAST(VQS.Score AS VARCHAR(15))
			ELSE 'Not Assessed'
		END
	FROM visit.VisitQualityStandard VQS
		JOIN visit.Visit V ON V.VisitID = VQS.VisitID
			AND V.FacilityID = @FacilityID
	ORDER BY VQS.AssessmentDate DESC

	RETURN ISNULL(@cReturn, 'Not Assessed')

END
GO
--End function visit.GetQualityStandardScoreByFacilityID
