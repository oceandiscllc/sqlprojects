USE MNH
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.AssessmentEvidenceType AET WHERE AET.AssessmentEvidenceTypeCode = 'NotApplicable')
	BEGIN

	INSERT INTO dropdown.AssessmentEvidenceType
		(AssessmentEvidenceTypeCode, AssessmentEvidenceTypeName, DisplayOrder)
	VALUES
		(
		'NotApplicable',
		'Applicablity',
		8
		)

	END
--ENDIF

DECLARE @nAssessmentEvidenceTypeID INT

SELECT @nAssessmentEvidenceTypeID = AET.AssessmentEvidenceTypeID
FROM dropdown.AssessmentEvidenceType AET 
WHERE AET.AssessmentEvidenceTypeCode = 'NotApplicable'

INSERT INTO dropdown.QualityStandardAssessmentEvidenceType
	(QualityStandardID, AssessmentEvidenceTypeID)
SELECT
	QSL.QualityStandardID,
	@nAssessmentEvidenceTypeID
FROM dropdown.QualityStandardLookup QSL
WHERE QSL.ManualName = 'NABH'
	AND 
		(
		LEFT(QSL.ObjectiveElementName, 3) IN ('5.1', '5.2', '5.3', '5.4', '6.1', '6.2', '6.3', '6.4')
			OR LEFT(QSL.ObjectiveElementName, 4) IN ('10.1', '10.2', '10.3', '19.5', '26.1', '31.1', '31.2', '32.3', '33.4', '34.3', '39.5')
		)
	AND NOT EXISTS
		(
		SELECT 1
		FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		WHERE QSAET.QualityStandardID = QSL.QualityStandardID
			AND QSAET.AssessmentEvidenceTypeID = @nAssessmentEvidenceTypeID
		)
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.QualityStandard QS WHERE QS.EntityTypeCode = 'Verification Criteria' AND QS.EntityName = '6.4 Imaging personnel are trained in safe practices and are provided with appropriate safety equipment/devices')
	BEGIN

	INSERT INTO dropdown.QualityStandard
		(ParentQualityStandardID, EntityTypeCode, EntityName, DisplayOrder)
	SELECT
		QS.QualityStandardID,
		'Verification Criteria',
		'6.4 Imaging personnel are trained in safe practices and are provided with appropriate safety equipment/devices',
		4
	FROM dropdown.QualityStandard QS 
	WHERE QS.EntityTypeCode = 'Standard'
		AND QS.EntityName = '6. Managing services are provided as per the scope of the hospital''s services and established radiation safety programme'

	END
--ENDIF

IF NOT EXISTS 
	(
	SELECT 1 
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = QSAET.QualityStandardID
			AND QS.EntityName = '6.4 Imaging personnel are trained in safe practices and are provided with appropriate safety equipment/devices'
	)

	BEGIN

	INSERT INTO dropdown.QualityStandardAssessmentEvidenceType
		(QualityStandardID, AssessmentEvidenceTypeID)
	SELECT
		QS.QualityStandardID,
		AET.AssessmentEvidenceTypeID
	FROM dropdown.AssessmentEvidenceType AET, dropdown.QualityStandard QS
	WHERE AET.AssessmentEvidenceTypeCode IN ('HasDocumentation', 'HasImplementation')
		AND QS.EntityTypeCode = 'Verification Criteria' 
		AND QS.EntityName = '6.4 Imaging personnel are trained in safe practices and are provided with appropriate safety equipment/devices'

	END
--ENDIF
GO

UPDATE QS
SET QS.EntityName = '4. Patient''s care is continuous and all patients cared for by the SHCO undergo a regular assessment.'
FROM dropdown.QualityStandard QS
WHERE QS.EntityTypeCode	= 'Standard'
	AND LEFT(QS.EntityName, 10) = '4. Patient'
GO

UPDATE QS
SET 
	QS.DisplayOrder = 1,
	QS.EntityName = '4.1 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization',
	QS.ParentQualityStandardID = (SELECT QS1.QualityStandardID FROM dropdown.QualityStandard QS1 WHERE QS1.EntityTypeCode	= 'Standard' AND LEFT(QS1.EntityName, 10) = '4. Patient')
FROM dropdown.QualityStandard QS
WHERE QS.EntityTypeCode	= 'Verification Criteria'
	AND QS.EntityName = '3.4 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization'
GO

UPDATE QS
SET 
	QS.DisplayOrder = 2,
	QS.EntityName = '4.2 All patients are reassessed at appropriate intervals'
FROM dropdown.QualityStandard QS
WHERE QS.EntityTypeCode	= 'Verification Criteria'
	AND QS.EntityName = '4.1 All patients are reassessed at appropriate intervals'
GO

UPDATE QS
SET 
	QS.DisplayOrder = 3,
	QS.EntityName = '4.3 Staff involved in direct clinical care document reassessments'
FROM dropdown.QualityStandard QS
WHERE QS.EntityTypeCode	= 'Verification Criteria'
	AND QS.EntityName = '4.2 Staff involved in direct clinical care document reassessments'
GO

UPDATE QS
SET 
	QS.DisplayOrder = 4,
	QS.EntityName = '4.4 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge'
FROM dropdown.QualityStandard QS
WHERE QS.EntityTypeCode	= 'Verification Criteria'
	AND QS.EntityName = '4.3 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge'
GO

