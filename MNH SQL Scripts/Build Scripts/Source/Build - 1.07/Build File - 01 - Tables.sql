USE MNH
GO

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.AddColumn @TableName, 'AuthorizedDate', 'DATE'
GO
--End table facility.Facility

--Begin table facility.FacilityInclusionCriteria
DECLARE @TableName VARCHAR(250) = 'facility.FacilityInclusionCriteria'

EXEC utility.DropColumn @TableName, 'AssessmentDate'
GO
--End table facility.FacilityInclusionCriteria

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.AddColumn @TableName, 'NotApplicable', 'bit', '0'
GO
--End table visit.VisitQualityStandard


