USE MNH
GO

--Begin procedure contact.GetContactByContactID
EXEC Utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.10
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CellPhoneNumber,
		C.ContactID,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		C.EmailAddress,
		C.FacilityEndDate,
		core.FormatDate(C.FacilityEndDate) AS FacilityEndDateFormatted,
		C.FacilityStartDate,
		core.FormatDate(C.FacilityStartDate) AS FacilityStartDateFormatted,
		C.FirstName,
		C.IsActive,
		C.LastName,
		C.MiddleName,
		C.OtherContactFunction,
		C.OtherContactRole,
		C.PhoneNumber,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Title,
		C.YearsExperience,
		CF.ContactFunctionCode,
		CF.ContactFunctionID,
		CF.ContactFunctionName,
		CR.ContactRoleCode,
		CR.ContactRoleID,
		CR.ContactRoleName,
		DIBR.DIBRoleID,
		DIBR.DIBRoleName,
		F.FacilityID,
		F.FacilityName,
		G.GenderID,
		G.GenderName
	FROM contact.Contact C
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
		JOIN dropdown.DIBRole DIBR ON DIBR.DIBRoleID = C.DIBRoleID
		JOIN dropdown.Gender G ON G.GenderID = C.GenderID
		JOIN facility.Facility F ON F.FacilityID = C.FacilityID
			AND C.ContactID = @ContactID
	
END
GO 
--End procedure contact.GetContactByContactID

--Begin procedure dropdown.GetContactFunctionData
EXEC Utility.DropObject 'dropdown.GetContactFunctionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactFunctionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactFunctionID, 
		T.ContactFunctionCode, 
		T.ContactFunctionName
	FROM dropdown.ContactFunction T
	WHERE (T.ContactFunctionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactFunctionName, T.ContactFunctionID

END
GO
--End procedure dropdown.GetContactFunctionData

--Begin procedure dropdown.GetContactRoleData
EXEC Utility.DropObject 'dropdown.GetContactRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactRoleID, 
		T.ContactRoleCode, 
		T.ContactRoleName
	FROM dropdown.ContactRole T
	WHERE (T.ContactRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactRoleName, T.ContactRoleID

END
GO
--End procedure dropdown.GetContactRoleData

--Begin procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataQualityStandardIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to return data from the dropdown.QualityStandardLookup table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupDataQualityStandardIDList

@QualityStandardIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		QSL.ManualName,
		QSL.ChapterName,
		QSL.StandardName,
		QSL.ObjectiveElementName,
		QSL.QualityStandardID
	FROM dropdown.QualityStandardLookup QSL
		JOIN core.ListToTable(@QualityStandardIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = QSL.QualityStandardID

END
GO
--End procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList

--Begin procedure dropdown.GetScoreChangeReasonData
EXEC Utility.DropObject 'dropdown.GetScoreChangeReasonData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to return data from the dropdown.ScoreChangeReason table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetScoreChangeReasonData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ScoreChangeReasonID, 
		T.ScoreChangeReasonName
	FROM dropdown.ScoreChangeReason T
	WHERE (T.ScoreChangeReasonID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ScoreChangeReasonName, T.ScoreChangeReasonID

END
GO
--End procedure dropdown.GetScoreChangeReasonData

--Begin procedure dropdown.GetVisitPurposeData
EXEC Utility.DropObject 'dropdown.GetVisitPurposeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to return data from the dropdown.VisitPurpose table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetVisitPurposeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VisitPurposeID, 
		T.VisitPurposeName
	FROM dropdown.VisitPurpose T
	WHERE (T.VisitPurposeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VisitPurposeName, T.VisitPurposeID

END
GO
--End procedure dropdown.GetVisitPurposeData

--Begin procedure eventlog.LogVisitAction
EXEC Utility.DropObject 'eventlog.LogVisitAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.09.04
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE [eventlog].[LogVisitAction]

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Visit',
			T.VisitID,
			@Comments,
			@ProjectID
		FROM visit.Visit T
		WHERE T.VisitID = @EntityID

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogVisitActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogVisitActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogVisitActionTable
		FROM visit.Visit T
		WHERE T.VisitID = @EntityID

		DECLARE @cVisitContact VARCHAR(MAX) = ''
		DECLARE @cVisitQualityStandard VARCHAR(MAX) = ''
		
		SELECT @cVisitContact = COALESCE(@cVisitContact, '') + D.VisitContact
		FROM
			(
			SELECT
				(SELECT T.ContactID FOR XML RAW(''), ELEMENTS) AS VisitContact
			FROM Visit.VisitContact T 
			WHERE T.VisitID = @EntityID
			) D
		
		SELECT @cVisitQualityStandard = COALESCE(@cVisitQualityStandard, '') + D.VisitQualityStandard
		FROM
			(
			SELECT
				(SELECT T.QualityStandardID FOR XML RAW(''), ELEMENTS) AS VisitQualityStandard
			FROM Visit.VisitQualityStandard T 
			WHERE T.VisitID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Visit',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<VisitContact>' + @cVisitContact + '</VisitContact>') AS XML), 
			CAST(('<VisitQualityStandard>' + @cVisitQualityStandard + '</VisitQualityStandard>') AS XML)
			FOR XML RAW('Visit'), ELEMENTS
			)
		FROM #LogVisitActionTable T
			JOIN Visit.Visit F ON F.VisitID = T.VisitID

		DROP TABLE #LogVisitActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVisitAction

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.BedCount,
		F.Description, 			
		F.FacilityID, 	
		F.FacilityName, 	
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.IsAbortionProvider
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID
EXEC Utility.DropObject 'facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.FacilityQualityAssessment table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID

@FacilityQualityAssessmentID INT

AS
BEGIN
	SET NOCOUNT ON;

	--FacilityQualityAssessment
	SELECT 
		FQA.FacilityQualityAssessmentID, 	
		FQA.FacilityID,
		F.FacilityName,
		FQA.VisitID,
		FQA.QualityStandardID,
		QS.EntityName AS QualityStandardName,
		FQA.AssessmentDate,
		core.FormatDate(FQA.AssessmentDate) AS AssessmentDateFormatted,
		FQA.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName,
		FQA.Score,
		FQA.TotalRecordsChecked,
		FQA.HasObservation,
		FQA.HasProviderInterview,
		FQA.HasPhysicalVerification,
		FQA.Notes,
		FQA.UpdatePersonID,
		person.FormatPersonNameByPersonID(FQA.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted
	FROM facility.FacilityQualityAssessment FQA
		JOIN facility.Facility F ON F.FacilityID = FQA.FacilityID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = FQA.QualityStandardID
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = FQA.ScoreChangeReasonID
			AND FQA.FacilityQualityAssessmentID = @FacilityQualityAssessmentID

END
GO
--End procedure facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.09.03
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		V.VisitID, 	
		V.FacilityID,
		F.FacilityName,
		V.VisitPurposeID,
		VP.VisitPurposeName,
		V.VisitStartDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitPlanNotes,
		V.VisitReportNotes,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID

	--VisitQualityStandard
	SELECT 
		VQS.VisitQualityStandardID,
		QS.QualityStandardID,
		QS.EntityTypeCode, 
		QS.ManualID, 
		QS.ManualName, 
		QS.ChapterID, 
		QS.ChapterName, 
		QS.StandardID, 
		QS.StandardName, 
		QS.ObjectiveElementID, 
		QS.ObjectiveElementName,
		0 AS CurrentScore
	FROM visit.VisitQualityStandard VQS
		JOIN dropdown.QualityStandardLookup QS ON QS.QualityStandardID = VQS.QualityStandardID
			AND VQS.VisitID = @VisitID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitQualityStandardID

@VisitQualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		V.VisitID, 	
		V.FacilityID,
		F.FacilityName,
		V.VisitPurposeID,
		VP.VisitPurposeName,
		V.VisitStartDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitPlanNotes,
		V.VisitReportNotes,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted
	FROM visit.Visit V
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitID = V.VisitID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
		JOIN facility.Facility F on F.FacilityID = V.FacilityID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

	--QualityStandard
	SELECT 
		QS.QualityStandardID,
		QS.EntityTypeCode, 
		QS.ManualID, 
		QS.ManualName, 
		QS.ChapterID, 
		QS.ChapterName, 
		QS.StandardID, 
		QS.StandardName, 
		QS.ObjectiveElementID, 
		QS.ObjectiveElementName,
		0 AS CurrentScore
	FROM dropdown.QualityStandardLookup QS
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.QualityStandardID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

	DECLARE @LastAssessedDate DATE = 
		(
		SELECT TOP 1
			(
			SELECT TOP 1 VQSA.AssessmentDate 
			FROM visit.VisitQualityStandardAssessment VQSA 
				JOIN visit.Visit V3 ON V3.VisitID = VQSA.VisitID
					AND V3.IsVisitComplete = 1
					AND V3.FacilityID = V.FacilityID
				ORDER BY VQSA.AssessmentDate DESC
			) AS LastAssessedDate
		FROM dropdown.QualityStandard QS 
			JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.ParentQualityStandardID
			JOIN visit.Visit V ON VQS.VisitID = V.VisitID
			JOIN facility.Facility F ON V.FacilityID = F.FacilityID
				AND VQS.VisitQualityStandardID = @VisitQualityStandardID
		)
	
	--QualityStandardCriteria
	SELECT QS.QualityStandardID, 
		QS.EntityTypeCode, 
		QS.EntityName, 
		(
		SELECT TOP 1 VQSA.Score 
		FROM visit.VisitQualityStandardAssessment VQSA 
			JOIN visit.Visit V2 ON V2.VisitID = VQSA.VisitID
				AND V2.IsVisitComplete = 1
				AND V2.FacilityID = V.FacilityID
		ORDER BY VQSA.AssessmentDate DESC
		) AS CurrentScore, 
		(
		SELECT TOP 1 VQSA.AssessmentDate 
		FROM visit.VisitQualityStandardAssessment VQSA 
			JOIN visit.Visit V3 ON V3.VisitID = VQSA.VisitID
				AND V3.IsVisitComplete = 1
				AND V3.FacilityID = V.FacilityID
		ORDER BY VQSA.AssessmentDate DESC
		) AS LastAssessedDate,
		core.FormatDate(@LastAssessedDate) AS LastAssessedDateFormatted
	FROM dropdown.QualityStandard QS 
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.ParentQualityStandardID
		JOIN visit.Visit V ON VQS.VisitID = V.VisitID
		JOIN facility.Facility F ON V.FacilityID = F.FacilityID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitQualityStandardID

--Begin procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID
EXEC Utility.DropObject 'facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID'
GO
EXEC Utility.DropObject 'visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the visit.FacilityQualityAssessment table
-- ============================================================================
CREATE PROCEDURE [visit].[GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID]

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastAssessedDate DATE
	DECLARE @CurrentScore INT

	SELECT TOP 1 
		@LastAssessedDate = VQSA.AssessmentDate,
		@CurrentScore = VQSA.Score
	FROM visit.VisitQualityStandardAssessment VQSA
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
			AND VQSA.QualityStandardID = @QualityStandardID
			AND V.IsVisitComplete = 1
			AND V.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
	ORDER BY VQSA.AssessmentDate DESC

	--FacilityQualityAssessment
	SELECT 
		VQSA.VisitQualityStandardAssessmentID, 	
		V.FacilityID,
		F.FacilityName,
		VQSA.VisitID,
		VQSA.QualityStandardID,
		QS.EntityName AS QualityStandardName,
		VQSA.AssessmentDate,
		core.FormatDate(VQSA.AssessmentDate) AS AssessmentDateFormatted,
		VQSA.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName,
		VQSA.Score,
		VQSA.TotalRecordsChecked,
		VQSA.CorrectRecords,
		VQSA.HasObservation,
		VQSA.HasProviderInterview,
		VQSA.HasPhysicalVerification,
		VQSA.Notes,
		@LastAssessedDate AS LastAssessedDate,
		core.FormatDate(@LastAssessedDate) AS LastAssessedDateFormatted,
		@CurrentScore AS CurrentScore
	FROM visit.VisitQualityStandardAssessment VQSA
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = VQSA.QualityStandardID
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = VQSA.ScoreChangeReasonID
			AND VQSA.VisitID = @VisitID
			AND VQSA.QualityStandardID = @QualityStandardID

	--VisitContact
	SELECT
		SCR.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName
	FROM visit.VisitQualityStandardAssessmentScoreChangeReason VC
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = VC.ScoreChangeReasonID
		JOIN visit.VisitQualityStandardAssessment VQSA ON VQSA.VisitQualityStandardAssessmentID = VC.VisitQualityStandardAssessmentID
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
			AND V.VisitID = @VisitID
			AND VQSA.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID
