USE MNH
GO

--Begin schemas
EXEC utility.AddSchema 'visit'
GO
--End schemas

--Begin table dropdown.ContactFunction
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactFunction'

EXEC utility.AddColumn @TableName, 'ContactFunctionCode', 'VARCHAR(50)'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactRole'

EXEC utility.AddColumn @TableName, 'ContactRoleCode', 'VARCHAR(50)'
GO
--End table dropdown.ContactRole

--Begin table dropdown.QualityStandard
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandard'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandard
	(
	QualityStandardID INT IDENTITY(1,1) NOT NULL,
	ParentQualityStandardID INT,
	EntityTypeCode VARCHAR(50),
	EntityName VARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentQualityStandardID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'QualityStandardID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_QualityStandard', 'QualityStandardID,ParentQualityStandardID'
GO

EXEC utility.DropObject 'dropdown.TR_QualityStandard'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.09
-- Description:	A trigger to populate the dropdown.QualityStandardLookup table
-- ===========================================================================
CREATE TRIGGER dropdown.TR_QualityStandard ON dropdown.QualityStandard AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	TRUNCATE TABLE dropdown.QualityStandardLookup

	;
	WITH HD (QualityStandardID,ParentQualityStandardID,EntityTypeCode,NodeLevel)
		AS 
		(
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID,
			T.EntityTypeCode, 
			1 
		FROM dropdown.QualityStandard T
		WHERE T.ParentQualityStandardID = 0
			AND T.QualityStandardID > 0
		
		UNION ALL
			
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID, 
			T.EntityTypeCode, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dropdown.QualityStandard T 
			JOIN HD ON HD.QualityStandardID = T.ParentQualityStandardID 
				AND T.EntityTypeCode <> 'VerificationCriteria'
		)
		
	INSERT INTO dropdown.QualityStandardLookup
		(EntityTypeCode, QualityStandardID, ManualID, ChapterID, StandardID, ObjectiveElementID)
	SELECT
		A.EntityTypeCode, 
		A.QualityStandardID,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 1
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END

	FROM HD A
		LEFT JOIN HD B ON B.QualityStandardID = A.ParentQualityStandardID
		LEFT JOIN HD C ON C.QualityStandardID = B.ParentQualityStandardID
		LEFT JOIN HD D ON D.QualityStandardID = C.ParentQualityStandardID

	UPDATE QSL
	SET
		QSL.ManualName = QSM.EntityName,
		QSL.ChapterName = QSC.EntityName,
		QSL.StandardName = QSS.EntityName,
		QSL.ObjectiveElementName = QSOE.EntityName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QSM ON QSM.QualityStandardID = QSL.ManualID
		JOIN dropdown.QualityStandard QSC ON QSC.QualityStandardID = QSL.ChapterID
		JOIN dropdown.QualityStandard QSS ON QSS.QualityStandardID = QSL.StandardID
		JOIN dropdown.QualityStandard QSOE ON QSOE.QualityStandardID = QSL.ObjectiveElementID

	END
--ENDIF
GO

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table dropdown.QualityStandardLookup
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardLookup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandardLookup
	(
	QualityStandardLookupID INT NOT NULL IDENTITY(1,1),
	EntityTypeCode VARCHAR(50),
	QualityStandardID INT,
	ManualID INT,
	ManualName VARCHAR(500),
	ChapterID INT,
	ChapterName VARCHAR(500),
	StandardID INT,
	StandardName VARCHAR(500),
	ObjectiveElementID INT,
	ObjectiveElementName VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ManualID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ChapterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveElementID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QualityStandardLookupID'
EXEC utility.SetIndexClustered @TableName, 'IX_QualityStandardLookup', 'QualityStandardID'
GO
--End table dropdown.QualityStandardLookup

--Begin table dropdown.ScoreChangeReason
DECLARE @TableName VARCHAR(250) = 'dropdown.ScoreChangeReason'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ScoreChangeReason
	(
	ScoreChangeReasonID INT IDENTITY(0,1) NOT NULL,
	ScoreChangeReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ScoreChangeReasonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ScoreChangeReason', 'DisplayOrder,ScoreChangeReasonName', 'ScoreChangeReasonID'
GO
--End table dropdown.ScoreChangeReason

--Begin table dropdown.VisitPurpose
DECLARE @TableName VARCHAR(250) = 'dropdown.VisitPurpose'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VisitPurpose
	(
	VisitPurposeID INT IDENTITY(0,1) NOT NULL,
	VisitPurposeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitPurposeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitPurpose', 'DisplayOrder,VisitPurposeName', 'VisitPurposeID'
GO
--End table dropdown.VisitPurpose

--Begin table facility.FacilityQualityStandardAssessment
DECLARE @TableName VARCHAR(250) = 'facility.FacilityQualityStandardAssessment'

EXEC utility.DropObject @TableName
GO
--End table facility.FacilityQualityStandardAssessment

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.DropObject @TableName

CREATE TABLE visit.Visit
	(
	VisitID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	VisitPurposeID INT,
	VisitStartDateTime DATETIME,
	VisitEndDateTime DATETIME,
	VisitPlanNotes VARCHAR(MAX),
	VisitReportNotes VARCHAR(MAX),
	IsPlanningComplete BIT,
	IsVisitComplete BIT,
	VisitPlanUpdatePersonID INT,
	VisitReportUpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsPlanningComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsVisitComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitPlanUpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitPurposeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitReportUpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitID'
GO
--End table visit.Visit

--Begin table visit.VisitContact
DECLARE @TableName VARCHAR(250) = 'visit.VisitContact'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitContact
	(
	VisitContactID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_VisitContact', 'VisitID,ContactID'
GO
--End table visit.VisitContact

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandard
	(
	VisitQualityStandardID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	QualityStandardID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardID'
EXEC utility.SetIndexClustered @TableName, 'IX_VisitQualityStandard', 'VisitID,QualityStandardID'
GO
--End table visit.VisitQualityStandard

--Begin table visit.VisitQualityStandardAssessment
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessment'
EXEC utility.DropObject @TableName
GO

CREATE TABLE visit.VisitQualityStandardAssessment
	(
	VisitQualityStandardAssessmentID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	QualityStandardID INT,
	AssessmentDate DATE,
	ScoreChangeReasonID INT,
	Score INT,
	TotalRecordsChecked INT,
	CorrectRecords INT,
	HasObservation BIT,
	HasProviderInterview BIT,
	HasPhysicalVerification BIT,
	Notes VARCHAR(MAX)
	)

DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessment'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssessmentDate', 'DATE', 'GetDate()'
EXEC utility.SetDefaultConstraint @TableName, 'ScoreChangeReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TotalRecordsChecked', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CorrectRecords', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasObservation', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasProviderInterview', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasPhysicalVerification', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardAssessmentID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitQualityStandardAssessment', 'VisitID,QualityStandardID'
GO
--End table visit.VisitQualityStandardAssessment

--Begin table visit.VisitQualityStandardAssessmentScoreChangeReason
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessmentScoreChangeReason'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandardAssessmentScoreChangeReason
	(
	VisitQualityStandardAssessmentScoreChangeReasonID INT IDENTITY(1,1) NOT NULL,
	VisitQualityStandardAssessmentID INT,
	ScoreChangeReasonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ScoreChangeReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitQualityStandardAssessmentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardAssessmentScoreChangeReasonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitQualityStandardAssessmentScoreChangeReason', 'VisitQualityStandardAssessmentID,ScoreChangeReasonID'
GO
--End table visit.VisitQualityStandardAssessmentScoreChangeReason



