-- File Name:	Build - 1.2 - MNH.sql
-- Build Key:	Build - 1.2 - 2017.11.12 17.51.16

--USE MNH
GO

-- ==============================================================================================================================
-- Schemas:
--		visit
--
-- Tables:
--		dropdown.QualityStandard
--		dropdown.QualityStandardLookup
--		dropdown.ScoreChangeReason
--		dropdown.VisitPurpose
--		visit.Visit
--		visit.VisitContact
--		visit.VisitQualityStandard
--		visit.VisitQualityStandardAssessment
--		visit.VisitQualityStandardAssessmentScoreChangeReason
--
-- Procedures:
--		[eventlog].[LogVisitAction]
--		[visit].[GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID]
--		contact.GetContactByContactID
--		dropdown.GetContactFunctionData
--		dropdown.GetContactRoleData
--		dropdown.GetQualityStandardLookupDataQualityStandardIDList
--		dropdown.GetScoreChangeReasonData
--		dropdown.GetVisitPurposeData
--		facility.GetFacilityByFacilityID
--		facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID
--		person.GeneratePassword
--		utility.InsertIdentityValue
--		visit.GetVisitByVisitID
--		visit.GetVisitQualityStandardByVisitQualityStandardID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE MNH
GO

--Begin schemas
EXEC utility.AddSchema 'visit'
GO
--End schemas

--Begin table dropdown.ContactFunction
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactFunction'

EXEC utility.AddColumn @TableName, 'ContactFunctionCode', 'VARCHAR(50)'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactRole'

EXEC utility.AddColumn @TableName, 'ContactRoleCode', 'VARCHAR(50)'
GO
--End table dropdown.ContactRole

--Begin table dropdown.QualityStandard
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandard'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandard
	(
	QualityStandardID INT IDENTITY(1,1) NOT NULL,
	ParentQualityStandardID INT,
	EntityTypeCode VARCHAR(50),
	EntityName VARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentQualityStandardID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'QualityStandardID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_QualityStandard', 'QualityStandardID,ParentQualityStandardID'
GO

EXEC utility.DropObject 'dropdown.TR_QualityStandard'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.09
-- Description:	A trigger to populate the dropdown.QualityStandardLookup table
-- ===========================================================================
CREATE TRIGGER dropdown.TR_QualityStandard ON dropdown.QualityStandard AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	TRUNCATE TABLE dropdown.QualityStandardLookup

	;
	WITH HD (QualityStandardID,ParentQualityStandardID,EntityTypeCode,NodeLevel)
		AS 
		(
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID,
			T.EntityTypeCode, 
			1 
		FROM dropdown.QualityStandard T
		WHERE T.ParentQualityStandardID = 0
			AND T.QualityStandardID > 0
		
		UNION ALL
			
		SELECT
			T.QualityStandardID, 
			T.ParentQualityStandardID, 
			T.EntityTypeCode, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dropdown.QualityStandard T 
			JOIN HD ON HD.QualityStandardID = T.ParentQualityStandardID 
				AND T.EntityTypeCode <> 'VerificationCriteria'
		)
		
	INSERT INTO dropdown.QualityStandardLookup
		(EntityTypeCode, QualityStandardID, ManualID, ChapterID, StandardID, ObjectiveElementID)
	SELECT
		A.EntityTypeCode, 
		A.QualityStandardID,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(D.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 1
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(C.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 2
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(B.QualityStandardID, 0)
			WHEN A.NodeLevel = 3
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END,

		CASE
			WHEN A.NodeLevel = 4
			THEN ISNULL(A.QualityStandardID, 0)
			ELSE 0
		END

	FROM HD A
		LEFT JOIN HD B ON B.QualityStandardID = A.ParentQualityStandardID
		LEFT JOIN HD C ON C.QualityStandardID = B.ParentQualityStandardID
		LEFT JOIN HD D ON D.QualityStandardID = C.ParentQualityStandardID

	UPDATE QSL
	SET
		QSL.ManualName = QSM.EntityName,
		QSL.ChapterName = QSC.EntityName,
		QSL.StandardName = QSS.EntityName,
		QSL.ObjectiveElementName = QSOE.EntityName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QSM ON QSM.QualityStandardID = QSL.ManualID
		JOIN dropdown.QualityStandard QSC ON QSC.QualityStandardID = QSL.ChapterID
		JOIN dropdown.QualityStandard QSS ON QSS.QualityStandardID = QSL.StandardID
		JOIN dropdown.QualityStandard QSOE ON QSOE.QualityStandardID = QSL.ObjectiveElementID

	END
--ENDIF
GO

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table dropdown.QualityStandardLookup
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardLookup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandardLookup
	(
	QualityStandardLookupID INT NOT NULL IDENTITY(1,1),
	EntityTypeCode VARCHAR(50),
	QualityStandardID INT,
	ManualID INT,
	ManualName VARCHAR(500),
	ChapterID INT,
	ChapterName VARCHAR(500),
	StandardID INT,
	StandardName VARCHAR(500),
	ObjectiveElementID INT,
	ObjectiveElementName VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ManualID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ChapterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveElementID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QualityStandardLookupID'
EXEC utility.SetIndexClustered @TableName, 'IX_QualityStandardLookup', 'QualityStandardID'
GO
--End table dropdown.QualityStandardLookup

--Begin table dropdown.ScoreChangeReason
DECLARE @TableName VARCHAR(250) = 'dropdown.ScoreChangeReason'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ScoreChangeReason
	(
	ScoreChangeReasonID INT IDENTITY(0,1) NOT NULL,
	ScoreChangeReasonName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ScoreChangeReasonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ScoreChangeReason', 'DisplayOrder,ScoreChangeReasonName', 'ScoreChangeReasonID'
GO
--End table dropdown.ScoreChangeReason

--Begin table dropdown.VisitPurpose
DECLARE @TableName VARCHAR(250) = 'dropdown.VisitPurpose'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VisitPurpose
	(
	VisitPurposeID INT IDENTITY(0,1) NOT NULL,
	VisitPurposeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitPurposeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitPurpose', 'DisplayOrder,VisitPurposeName', 'VisitPurposeID'
GO
--End table dropdown.VisitPurpose

--Begin table facility.FacilityQualityStandardAssessment
DECLARE @TableName VARCHAR(250) = 'facility.FacilityQualityStandardAssessment'

EXEC utility.DropObject @TableName
GO
--End table facility.FacilityQualityStandardAssessment

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.DropObject @TableName

CREATE TABLE visit.Visit
	(
	VisitID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	VisitPurposeID INT,
	VisitStartDateTime DATETIME,
	VisitEndDateTime DATETIME,
	VisitPlanNotes VARCHAR(MAX),
	VisitReportNotes VARCHAR(MAX),
	IsPlanningComplete BIT,
	IsVisitComplete BIT,
	VisitPlanUpdatePersonID INT,
	VisitReportUpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsPlanningComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsVisitComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitPlanUpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitPurposeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitReportUpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitID'
GO
--End table visit.Visit

--Begin table visit.VisitContact
DECLARE @TableName VARCHAR(250) = 'visit.VisitContact'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitContact
	(
	VisitContactID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_VisitContact', 'VisitID,ContactID'
GO
--End table visit.VisitContact

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandard
	(
	VisitQualityStandardID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	QualityStandardID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardID'
EXEC utility.SetIndexClustered @TableName, 'IX_VisitQualityStandard', 'VisitID,QualityStandardID'
GO
--End table visit.VisitQualityStandard

--Begin table visit.VisitQualityStandardAssessment
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessment'
EXEC utility.DropObject @TableName
GO

CREATE TABLE visit.VisitQualityStandardAssessment
	(
	VisitQualityStandardAssessmentID INT IDENTITY(1,1) NOT NULL,
	VisitID INT,
	QualityStandardID INT,
	AssessmentDate DATE,
	ScoreChangeReasonID INT,
	Score INT,
	TotalRecordsChecked INT,
	CorrectRecords INT,
	HasObservation BIT,
	HasProviderInterview BIT,
	HasPhysicalVerification BIT,
	Notes VARCHAR(MAX)
	)

DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessment'
EXEC utility.SetDefaultConstraint @TableName, 'VisitID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssessmentDate', 'DATE', 'GetDate()'
EXEC utility.SetDefaultConstraint @TableName, 'ScoreChangeReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TotalRecordsChecked', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CorrectRecords', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasObservation', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasProviderInterview', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasPhysicalVerification', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardAssessmentID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitQualityStandardAssessment', 'VisitID,QualityStandardID'
GO
--End table visit.VisitQualityStandardAssessment

--Begin table visit.VisitQualityStandardAssessmentScoreChangeReason
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandardAssessmentScoreChangeReason'

EXEC utility.DropObject @TableName

CREATE TABLE visit.VisitQualityStandardAssessmentScoreChangeReason
	(
	VisitQualityStandardAssessmentScoreChangeReasonID INT IDENTITY(1,1) NOT NULL,
	VisitQualityStandardAssessmentID INT,
	ScoreChangeReasonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ScoreChangeReasonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VisitQualityStandardAssessmentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VisitQualityStandardAssessmentScoreChangeReasonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitQualityStandardAssessmentScoreChangeReason', 'VisitQualityStandardAssessmentID,ScoreChangeReasonID'
GO
--End table visit.VisitQualityStandardAssessmentScoreChangeReason




--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE MNH
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE MNH
GO

--Begin procedure contact.GetContactByContactID
EXEC Utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.10
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CellPhoneNumber,
		C.ContactID,
		C.DateOfBirth,
		core.FormatDate(C.DateOfBirth) AS DateOfBirthFormatted,
		C.EmailAddress,
		C.FacilityEndDate,
		core.FormatDate(C.FacilityEndDate) AS FacilityEndDateFormatted,
		C.FacilityStartDate,
		core.FormatDate(C.FacilityStartDate) AS FacilityStartDateFormatted,
		C.FirstName,
		C.IsActive,
		C.LastName,
		C.MiddleName,
		C.OtherContactFunction,
		C.OtherContactRole,
		C.PhoneNumber,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Title,
		C.YearsExperience,
		CF.ContactFunctionCode,
		CF.ContactFunctionID,
		CF.ContactFunctionName,
		CR.ContactRoleCode,
		CR.ContactRoleID,
		CR.ContactRoleName,
		DIBR.DIBRoleID,
		DIBR.DIBRoleName,
		F.FacilityID,
		F.FacilityName,
		G.GenderID,
		G.GenderName
	FROM contact.Contact C
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
		JOIN dropdown.DIBRole DIBR ON DIBR.DIBRoleID = C.DIBRoleID
		JOIN dropdown.Gender G ON G.GenderID = C.GenderID
		JOIN facility.Facility F ON F.FacilityID = C.FacilityID
			AND C.ContactID = @ContactID
	
END
GO 
--End procedure contact.GetContactByContactID

--Begin procedure dropdown.GetContactFunctionData
EXEC Utility.DropObject 'dropdown.GetContactFunctionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactFunctionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactFunctionID, 
		T.ContactFunctionCode, 
		T.ContactFunctionName
	FROM dropdown.ContactFunction T
	WHERE (T.ContactFunctionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactFunctionName, T.ContactFunctionID

END
GO
--End procedure dropdown.GetContactFunctionData

--Begin procedure dropdown.GetContactRoleData
EXEC Utility.DropObject 'dropdown.GetContactRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactRoleID, 
		T.ContactRoleCode, 
		T.ContactRoleName
	FROM dropdown.ContactRole T
	WHERE (T.ContactRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactRoleName, T.ContactRoleID

END
GO
--End procedure dropdown.GetContactRoleData

--Begin procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupDataQualityStandardIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.11
-- Description:	A stored procedure to return data from the dropdown.QualityStandardLookup table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupDataQualityStandardIDList

@QualityStandardIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		QSL.ManualName,
		QSL.ChapterName,
		QSL.StandardName,
		QSL.ObjectiveElementName,
		QSL.QualityStandardID
	FROM dropdown.QualityStandardLookup QSL
		JOIN core.ListToTable(@QualityStandardIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = QSL.QualityStandardID

END
GO
--End procedure dropdown.GetQualityStandardLookupDataQualityStandardIDList

--Begin procedure dropdown.GetScoreChangeReasonData
EXEC Utility.DropObject 'dropdown.GetScoreChangeReasonData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to return data from the dropdown.ScoreChangeReason table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetScoreChangeReasonData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ScoreChangeReasonID, 
		T.ScoreChangeReasonName
	FROM dropdown.ScoreChangeReason T
	WHERE (T.ScoreChangeReasonID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ScoreChangeReasonName, T.ScoreChangeReasonID

END
GO
--End procedure dropdown.GetScoreChangeReasonData

--Begin procedure dropdown.GetVisitPurposeData
EXEC Utility.DropObject 'dropdown.GetVisitPurposeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to return data from the dropdown.VisitPurpose table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetVisitPurposeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VisitPurposeID, 
		T.VisitPurposeName
	FROM dropdown.VisitPurpose T
	WHERE (T.VisitPurposeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VisitPurposeName, T.VisitPurposeID

END
GO
--End procedure dropdown.GetVisitPurposeData

--Begin procedure eventlog.LogVisitAction
EXEC Utility.DropObject 'eventlog.LogVisitAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.09.04
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE [eventlog].[LogVisitAction]

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Visit',
			T.VisitID,
			@Comments,
			@ProjectID
		FROM visit.Visit T
		WHERE T.VisitID = @EntityID

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogVisitActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogVisitActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogVisitActionTable
		FROM visit.Visit T
		WHERE T.VisitID = @EntityID

		DECLARE @cVisitContact VARCHAR(MAX) = ''
		DECLARE @cVisitQualityStandard VARCHAR(MAX) = ''
		
		SELECT @cVisitContact = COALESCE(@cVisitContact, '') + D.VisitContact
		FROM
			(
			SELECT
				(SELECT T.ContactID FOR XML RAW(''), ELEMENTS) AS VisitContact
			FROM Visit.VisitContact T 
			WHERE T.VisitID = @EntityID
			) D
		
		SELECT @cVisitQualityStandard = COALESCE(@cVisitQualityStandard, '') + D.VisitQualityStandard
		FROM
			(
			SELECT
				(SELECT T.QualityStandardID FOR XML RAW(''), ELEMENTS) AS VisitQualityStandard
			FROM Visit.VisitQualityStandard T 
			WHERE T.VisitID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Visit',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<VisitContact>' + @cVisitContact + '</VisitContact>') AS XML), 
			CAST(('<VisitQualityStandard>' + @cVisitQualityStandard + '</VisitQualityStandard>') AS XML)
			FOR XML RAW('Visit'), ELEMENTS
			)
		FROM #LogVisitActionTable T
			JOIN Visit.Visit F ON F.VisitID = T.VisitID

		DROP TABLE #LogVisitActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVisitAction

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.BedCount,
		F.Description, 			
		F.FacilityID, 	
		F.FacilityName, 	
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.IsAbortionProvider
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID
EXEC Utility.DropObject 'facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.FacilityQualityAssessment table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID

@FacilityQualityAssessmentID INT

AS
BEGIN
	SET NOCOUNT ON;

	--FacilityQualityAssessment
	SELECT 
		FQA.FacilityQualityAssessmentID, 	
		FQA.FacilityID,
		F.FacilityName,
		FQA.VisitID,
		FQA.QualityStandardID,
		QS.EntityName AS QualityStandardName,
		FQA.AssessmentDate,
		core.FormatDate(FQA.AssessmentDate) AS AssessmentDateFormatted,
		FQA.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName,
		FQA.Score,
		FQA.TotalRecordsChecked,
		FQA.HasObservation,
		FQA.HasProviderInterview,
		FQA.HasPhysicalVerification,
		FQA.Notes,
		FQA.UpdatePersonID,
		person.FormatPersonNameByPersonID(FQA.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted
	FROM facility.FacilityQualityAssessment FQA
		JOIN facility.Facility F ON F.FacilityID = FQA.FacilityID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = FQA.QualityStandardID
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = FQA.ScoreChangeReasonID
			AND FQA.FacilityQualityAssessmentID = @FacilityQualityAssessmentID

END
GO
--End procedure facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.09.03
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		V.VisitID, 	
		V.FacilityID,
		F.FacilityName,
		V.VisitPurposeID,
		VP.VisitPurposeName,
		V.VisitStartDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitPlanNotes,
		V.VisitReportNotes,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID

	--VisitQualityStandard
	SELECT 
		VQS.VisitQualityStandardID,
		QS.QualityStandardID,
		QS.EntityTypeCode, 
		QS.ManualID, 
		QS.ManualName, 
		QS.ChapterID, 
		QS.ChapterName, 
		QS.StandardID, 
		QS.StandardName, 
		QS.ObjectiveElementID, 
		QS.ObjectiveElementName,
		0 AS CurrentScore
	FROM visit.VisitQualityStandard VQS
		JOIN dropdown.QualityStandardLookup QS ON QS.QualityStandardID = VQS.QualityStandardID
			AND VQS.VisitID = @VisitID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitQualityStandardID

@VisitQualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		V.VisitID, 	
		V.FacilityID,
		F.FacilityName,
		V.VisitPurposeID,
		VP.VisitPurposeName,
		V.VisitStartDateTime,
		core.FormatDateTime(V.VisitStartDateTime) AS VisitStartDateTimeFormatted,
		V.VisitEndDateTime,
		core.FormatDateTime(V.VisitEndDateTime) AS VisitEndDateTimeFormatted,
		V.VisitPlanNotes,
		V.VisitReportNotes,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted
	FROM visit.Visit V
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitID = V.VisitID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
		JOIN facility.Facility F on F.FacilityID = V.FacilityID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

	--QualityStandard
	SELECT 
		QS.QualityStandardID,
		QS.EntityTypeCode, 
		QS.ManualID, 
		QS.ManualName, 
		QS.ChapterID, 
		QS.ChapterName, 
		QS.StandardID, 
		QS.StandardName, 
		QS.ObjectiveElementID, 
		QS.ObjectiveElementName,
		0 AS CurrentScore
	FROM dropdown.QualityStandardLookup QS
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.QualityStandardID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

	DECLARE @LastAssessedDate DATE = 
		(
		SELECT TOP 1
			(
			SELECT TOP 1 VQSA.AssessmentDate 
			FROM visit.VisitQualityStandardAssessment VQSA 
				JOIN visit.Visit V3 ON V3.VisitID = VQSA.VisitID
					AND V3.IsVisitComplete = 1
					AND V3.FacilityID = V.FacilityID
				ORDER BY VQSA.AssessmentDate DESC
			) AS LastAssessedDate
		FROM dropdown.QualityStandard QS 
			JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.ParentQualityStandardID
			JOIN visit.Visit V ON VQS.VisitID = V.VisitID
			JOIN facility.Facility F ON V.FacilityID = F.FacilityID
				AND VQS.VisitQualityStandardID = @VisitQualityStandardID
		)
	
	--QualityStandardCriteria
	SELECT QS.QualityStandardID, 
		QS.EntityTypeCode, 
		QS.EntityName, 
		(
		SELECT TOP 1 VQSA.Score 
		FROM visit.VisitQualityStandardAssessment VQSA 
			JOIN visit.Visit V2 ON V2.VisitID = VQSA.VisitID
				AND V2.IsVisitComplete = 1
				AND V2.FacilityID = V.FacilityID
		ORDER BY VQSA.AssessmentDate DESC
		) AS CurrentScore, 
		(
		SELECT TOP 1 VQSA.AssessmentDate 
		FROM visit.VisitQualityStandardAssessment VQSA 
			JOIN visit.Visit V3 ON V3.VisitID = VQSA.VisitID
				AND V3.IsVisitComplete = 1
				AND V3.FacilityID = V.FacilityID
		ORDER BY VQSA.AssessmentDate DESC
		) AS LastAssessedDate,
		core.FormatDate(@LastAssessedDate) AS LastAssessedDateFormatted
	FROM dropdown.QualityStandard QS 
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QS.ParentQualityStandardID
		JOIN visit.Visit V ON VQS.VisitID = V.VisitID
		JOIN facility.Facility F ON V.FacilityID = F.FacilityID
			AND VQS.VisitQualityStandardID = @VisitQualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitQualityStandardID

--Begin procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID
EXEC Utility.DropObject 'facility.GetFacilityQualityAssessmentByFacilityQualityAssessmentID'
GO
EXEC Utility.DropObject 'visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the visit.FacilityQualityAssessment table
-- ============================================================================
CREATE PROCEDURE [visit].[GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID]

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastAssessedDate DATE
	DECLARE @CurrentScore INT

	SELECT TOP 1 
		@LastAssessedDate = VQSA.AssessmentDate,
		@CurrentScore = VQSA.Score
	FROM visit.VisitQualityStandardAssessment VQSA
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
			AND VQSA.QualityStandardID = @QualityStandardID
			AND V.IsVisitComplete = 1
			AND V.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
	ORDER BY VQSA.AssessmentDate DESC

	--FacilityQualityAssessment
	SELECT 
		VQSA.VisitQualityStandardAssessmentID, 	
		V.FacilityID,
		F.FacilityName,
		VQSA.VisitID,
		VQSA.QualityStandardID,
		QS.EntityName AS QualityStandardName,
		VQSA.AssessmentDate,
		core.FormatDate(VQSA.AssessmentDate) AS AssessmentDateFormatted,
		VQSA.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName,
		VQSA.Score,
		VQSA.TotalRecordsChecked,
		VQSA.CorrectRecords,
		VQSA.HasObservation,
		VQSA.HasProviderInterview,
		VQSA.HasPhysicalVerification,
		VQSA.Notes,
		@LastAssessedDate AS LastAssessedDate,
		core.FormatDate(@LastAssessedDate) AS LastAssessedDateFormatted,
		@CurrentScore AS CurrentScore
	FROM visit.VisitQualityStandardAssessment VQSA
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = VQSA.QualityStandardID
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = VQSA.ScoreChangeReasonID
			AND VQSA.VisitID = @VisitID
			AND VQSA.QualityStandardID = @QualityStandardID

	--VisitContact
	SELECT
		SCR.ScoreChangeReasonID,
		SCR.ScoreChangeReasonName
	FROM visit.VisitQualityStandardAssessmentScoreChangeReason VC
		JOIN dropdown.ScoreChangeReason SCR ON SCR.ScoreChangeReasonID = VC.ScoreChangeReasonID
		JOIN visit.VisitQualityStandardAssessment VQSA ON VQSA.VisitQualityStandardAssessmentID = VC.VisitQualityStandardAssessmentID
		JOIN visit.Visit V ON V.VisitID = VQSA.VisitID
			AND V.VisitID = @VisitID
			AND VQSA.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardAssessmentByVisitQualityStandardAssessmentID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE MNH
GO

--Begin table core.MenuItem
--Begin Sites Items
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'VisitPlanList',
	@NewMenuItemLink = '/visit/visitplanlist',
	@NewMenuItemText = 'Visit Plans',
	@ParentMenuItemCode = 'Visits',
	@PermissionableLineageList = 'Visit.VisitPlanList'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'VisitReportList',
	@NewMenuItemLink = '/visit/visitreportlist',
	@NewMenuItemText = 'Visit Reports',
	@ParentMenuItemCode = 'Visits',
	@PermissionableLineageList = 'Visit.VisitReportList',
	@AfterMenuItemCode = 'VisitPlanList'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'QualityAssessmentList',
	@NewMenuItemLink = '/qualityassessment/list',
	@NewMenuItemText = 'Quality Assessments',
	@ParentMenuItemCode = 'Visits',
	@PermissionableLineageList = 'QualityAssessment.List',
	@AfterMenuItemCode = 'VisitReportList'
GO
--End Sites Items

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Sites'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Visits'
GO
--End table core.MenuItem

--Begin table dropdown.ContactFunction
UPDATE CF
SET CF.ContactFunctionCode = 'OTH'
FROM dropdown.ContactFunction CF
WHERE CF.ContactFunctionName LIKE 'Other%'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
UPDATE CR
SET CR.ContactRoleCode = 'OTH'
FROM dropdown.ContactRole CR
WHERE CR.ContactRoleName LIKE 'Other%'
GO
--End table dropdown.ContactRole

--Begin table dropdown.QualityStandard
TRUNCATE TABLE dropdown.QualityStandard
GO

SET IDENTITY_INSERT dropdown.QualityStandard ON
GO

INSERT INTO dropdown.QualityStandard 
	(QualityStandardID, ParentQualityStandardID, EntityTypeCode, EntityName) 
VALUES 
	(0, 0, NULL, NULL),
	(1, 0, 'Manual', 'FOGSI'),
	(2, 1, 'Chapter', 'ANC-1'),
	(3, 1, 'Chapter', 'IP and IPPC-10'),
	(4, 1, 'Chapter', 'IP and IPPC-11'),
	(5, 1, 'Chapter', 'IP and IPPC-12'),
	(6, 1, 'Chapter', 'IP and IPPC-13'),
	(7, 1, 'Chapter', 'IP and IPPC-14'),
	(8, 1, 'Chapter', 'IP and IPPC-15'),
	(9, 1, 'Chapter', 'IP and IPPC-16'),
	(10, 1, 'Chapter', 'IP and IPPC-2'),
	(11, 1, 'Chapter', 'IP and IPPC-3'),
	(12, 1, 'Chapter', 'IP and IPPC-4'),
	(13, 1, 'Chapter', 'IP and IPPC-5'),
	(14, 1, 'Chapter', 'IP and IPPC-6'),
	(15, 1, 'Chapter', 'IP and IPPC-7'),
	(16, 1, 'Chapter', 'IP and IPPC-8'),
	(17, 1, 'Chapter', 'IP and IPPC-9'),
	(18, 2, 'Standard', '42. Provider screens for key clinical conditions that may lead to complications during pregnancy'),
	(19, 10, 'Standard', '43. Provider prepares for safe care during delivery (to be checked every day) '),
	(20, 11, 'Standard', '44. Provider assesses all pregnant women at admission '),
	(21, 12, 'Standard', '45. Provider conducts PV examination appropriately'),
	(22, 13, 'Standard', '46. Undertakes timely assessment of cervical dilatation and descent to monitor the progress of labor'),
	(23, 14, 'Standard', '47. Provider ensures respectful and supportive care '),
	(24, 15, 'Standard', '48. Provider assists the woman to have a safe and clean birth'),
	(25, 16, 'Standard', '49. Provider conducts a rapid initial assessment and performs immediate newborn care (if baby cried immediately)'),
	(26, 17, 'Standard', '50. Provider performs Active Management of Third Stage of Labor (AMTSL)'),
	(27, 3, 'Standard', '51. Provider identifies and manages Post-Partum Hemorrage (PPH) '),
	(28, 4, 'Standard', '52. Provider identifies and manages severe Pre-eclampsia/Eclampsia (PE/E)'),
	(29, 5, 'Standard', '53. Provider performs newborn resuscitation if baby does not cry immediately after birth'),
	(30, 6, 'Standard', '54. Provider ensures care of newborn with small size at birth'),
	(31, 7, 'Standard', '55. The facility adheres to universal infection prevention protocols'),
	(32, 8, 'Standard', '56. Provider ensures adequate postpartum care package is offered to the mother and the baby - at discharge'),
	(33, 9, 'Standard', '57. Provider reviews clinical practices related to C-section at regular intervals '),
	(34, 18, 'ObjectiveElement', '42.1 Screens for anemia'),
	(35, 18, 'ObjectiveElement', '42.2 Screens for hypertensive disorders of pregnancy'),
	(36, 18, 'ObjectiveElement', '42.3 Screens for DM'),
	(37, 18, 'ObjectiveElement', '42.4 Screens for HIV'),
	(38, 18, 'ObjectiveElement', '42.5 Screens for syphilis'),
	(39, 18, 'ObjectiveElement', '42.6 Screens for malaria'),
	(40, 18, 'ObjectiveElement', '42.7 Establishes blood group and Rh type during first ANC visit'),
	(41, 19, 'ObjectiveElement', '43.1 Ensures sterile/ HLD delivery tray is available'),
	(42, 19, 'ObjectiveElement', '43.2 Ensures functional items for newborn care and resuscitation'),
	(43, 20, 'ObjectiveElement', '44.1 Takes obstetric, medical and surgical history'),
	(44, 20, 'ObjectiveElement', '44.2 Assesses gestational age correctly'),
	(45, 20, 'ObjectiveElement', '44.3 Records fetal heart rate'),
	(46, 20, 'ObjectiveElement', '44.4 Records mother''s BP and temperature'),
	(47, 21, 'ObjectiveElement', '45.1 Conducts PV examination as per indication'),
	(48, 21, 'ObjectiveElement', '45.2 Conducts PV examination following infection prevention practices and records findings'),
	(49, 22, 'ObjectiveElement', '46.1 Undertakes timely assessment of cervical dilatation and descent to monitor the progress of labor'),
	(50, 22, 'ObjectiveElement', '46.2 Interprets partograph (condition of mother and fetus and progress of labor) correctly and adjusts care according to findings'),
	(51, 23, 'ObjectiveElement', '47.1 Encourages and welcomes the presence of a birth companion during labor'),
	(52, 23, 'ObjectiveElement', '47.2 Treats pregnant woman and her companion cordially and respectfully (RMC), ensures privacy and confidentiality for pregnant woman during her stay'),
	(53, 23, 'ObjectiveElement', '47.3 Explains danger signs and important care activities to mother and her companion'),
	(54, 24, 'ObjectiveElement', '48.1 Provider ensures six ''cleans'' while conducting delivery'),
	(55, 24, 'ObjectiveElement', '48.2 Performs episiotomy only when indicated with the use of appropriate local anesthetic'),
	(56, 24, 'ObjectiveElement', '48.3 Provider allows spontaneous delivery of head by flexing it and giving perineal support; manages cord round the neck; assists delivery of shoulders and body'),
	(57, 25, 'ObjectiveElement', '49.1 Delivers the baby on mother''s abdomen'),
	(58, 25, 'ObjectiveElement', '49.2 Ensures immediate drying, and asses breathing'),
	(59, 25, 'ObjectiveElement', '49.3 Performs delayed cord clamping and cutting'),
	(60, 25, 'ObjectiveElement', '49.4 Ensures early initiation of breastfeeding'),
	(61, 25, 'ObjectiveElement', '49.5 Assesses the newborn for any congenital anomalies'),
	(62, 25, 'ObjectiveElement', '49.6 Weighs the baby and administers Vitamin K"'),
	(63, 26, 'ObjectiveElement', '50.1 Performs AMTSL and examines placenta thoroughly'),
	(64, 27, 'ObjectiveElement', '51.1 Assesses uterine tone and bleeding per vaginum regularly after delivery'),
	(65, 27, 'ObjectiveElement', '51.2 Identifies shock'),
	(66, 27, 'ObjectiveElement', '51.3 Manages shock'),
	(67, 27, 'ObjectiveElement', '51.4 Manages atonic PPH'),
	(68, 27, 'ObjectiveElement', '51.5 Manages PPH due to retained placenta/ placental bits'),
	(69, 28, 'ObjectiveElement', '52.1 Identifies mothers with severe PE/E'),
	(70, 28, 'ObjectiveElement', '52.2 Gives correct regimen of Inj. MgSO4 for prevention and management of convulsions'),
	(71, 28, 'ObjectiveElement', '52.3 Facilitates prescription of anti- hypertensives'),
	(72, 28, 'ObjectiveElement', '52.4 Ensures specialist attention for care of mother and newborn'),
	(73, 28, 'ObjectiveElement', '52.5 Performs nursing care'),
	(74, 29, 'ObjectiveElement', '53.1 Performs steps for resuscitation within first 30 seconds'),
	(75, 29, 'ObjectiveElement', '53.2 Initiates bag and mask ventilation for 30 seconds if baby still not breathing'),
	(76, 29, 'ObjectiveElement', '53.3 Takes appropriate action if baby doesn''t respond to ambu bag ventilation after golden minute'),
	(77, 29, 'ObjectiveElement', '53.4 Performs advanced resuscitation in babies not responding to basic resuscitation when chest is rising and heart rate is < 60 per minute'),
	(78, 30, 'ObjectiveElement', '54.1 Facilitate specialist care in newborn weighing <1800 gm'),
	(79, 30, 'ObjectiveElement', '54.2 Facilitates assisted feeding whenever required'),
	(80, 30, 'ObjectiveElement', '54.3 Facilitates thermal management including kangaroo mother care (KMC)'),
	(81, 31, 'ObjectiveElement', '55.1 Instruments and re-usable items are adequately and appropriately processed after each use'),
	(82, 31, 'ObjectiveElement', '55.2 Biomedical waste is segregated and disposed of as per the guidelines'),
	(83, 31, 'ObjectiveElement', '55.3 Performs hand hygiene before and after each procedure, and sterile gloves are worn during delivery and internal examination'),
	(84, 32, 'ObjectiveElement', '56.1 Conducts proper physical examination of mother and newborn during postpartum visits'),
	(85, 32, 'ObjectiveElement', '56.2 Identifies and appropriately manages maternal and neonatal sepsis'),
	(86, 32, 'ObjectiveElement', '56.3 Correctly diagnoses postpartum depression based on history and symptoms'),
	(87, 32, 'ObjectiveElement', '56.4 Counsels on importance of exclusive breast feeding'),
	(88, 32, 'ObjectiveElement', '56.5 Counsels on danger signs, post- partum family planning'),
	(89, 33, 'ObjectiveElement', '57.1 Ensures classification as per Robson''s criteria and reviews indications and complications of C-section at regular intervals'),
	(90, 34, 'VerificationCriteria', '42.1.1 Estimates Hb at each scheduled ANC visit'),
	(91, 35, 'VerificationCriteria', '42.2.1 Functional BP instrument and stethoscope at point of use is available'),
	(92, 35, 'VerificationCriteria', '42.2.2 Records BP at each ANC visit'),
	(93, 35, 'VerificationCriteria', '42.2.3 Performs proteinuria testing during each scheduled ANC visit"'),
	(94, 36, 'VerificationCriteria', '42.3.1 Uses/Refers for standard 75gm OGTT for screening of GDM at first ANC visit and repeats OGTT test at second ANC visit (594 -598 weeks) if negative in first screening'),
	(95, 37, 'VerificationCriteria', '42.4.1.Screens/refers for HIV during first ANC visit in all cases, and in fourth ANC visit in high risk cases'),
	(96, 37, 'VerificationCriteria', '42.4.2 Facilitates testing and treatment of spouse/partner'),
	(97, 38, 'VerificationCriteria', '42.5.1.Screens/refers for syphilis in first ANC visit in all cases, and in fourth ANC visit in high risk cases'),
	(98, 39, 'VerificationCriteria', '42.6.1. Screens for malaria as per the national guidelines.'),
	(99, 40, 'VerificationCriteria', '42.7.1 Establishes blood group and Rh type during first ANC visit'),
	(100, 41, 'VerificationCriteria', '43.1.1'),
	(101, 42, 'VerificationCriteria', '43.2.1.Checks for functionality of neonatal resuscitation kit and availability of shoulder roll before every delivery'),
	(102, 42, 'VerificationCriteria', '43.2.2.Performs following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby and rubs the back 2-3 times; immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing'),
	(103, 42, 'VerificationCriteria', '43.2.3. Performs following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR)'),
	(104, 43, 'VerificationCriteria', '44.1.1.  Documents obstetric, medical and surgical history in case record '),
	(105, 43, 'VerificationCriteria', '44.1.2. Documents the presentation and lie of the fetus in the case record at admission '),
	(106, 44, 'VerificationCriteria', '44.2.1Assesses and records gestational age through either LMP or Fundal height or USG (if available) '),
	(107, 45, 'VerificationCriteria', '44.3.1  Functional Doppler/fetoscope/stethoscope at point of use is available '),
	(108, 45, 'VerificationCriteria', '44.3.2  Records FHR at admission'),
	(109, 46, 'VerificationCriteria', '44.4.1Functional BP instrument and stethoscope at point of use is available '),
	(110, 46, 'VerificationCriteria', '44.4.2 Functional thermometer at point of use is available '),
	(111, 46, 'VerificationCriteria', '44.4.3 Records mother''s temperature at admission '),
	(112, 47, 'VerificationCriteria', '45.1.1 Uses correct technique for PV examination'),
	(113, 47, 'VerificationCriteria', '45.1.2.Rules out CPD and records PV examination findings'),
	(114, 48, 'VerificationCriteria', '45.2.1 Adequate sterile equipment, gloves and supplies are available'),
	(115, 49, 'VerificationCriteria', '46.1.1. Initiates partograph plotting when cervical dilatation is >=4 cms in appropriate column on the alert line'),
	(116, 49, 'VerificationCriteria', '46.1.2.Plots all parameters in the partograph'),
	(117, 50, 'VerificationCriteria', '46.2.1'),
	(118, 51, 'VerificationCriteria', '47.1.1. Encourages the presence of birth companion during labor'),
	(119, 52, 'VerificationCriteria', '47.2.1. Curtains are installed in labor room to ensure privacy to pregnant woman'),
	(120, 52, 'VerificationCriteria', '47.2.2.Treats pregnant woman and her companion cordially and respectfully (RMC), ensures privacy and confidentiality for pregnant woman during her stay'),
	(121, 53, 'VerificationCriteria', '47.3.1 Explains danger signs and important care activities to pregnant woman and her companion during the stay (for the woman and her newborn)'),
	(122, 54, 'VerificationCriteria', '48.1.1'),
	(123, 55, 'VerificationCriteria', '48.2.1'),
	(124, 56, 'VerificationCriteria', '48.3.1'),
	(125, 57, 'VerificationCriteria', '49.1.1 Places two pre-warmed towels on mother''s abdomen before delivery '),
	(126, 57, 'VerificationCriteria', '49.1.2 Delivers and places the baby on mother''s abdomen '),
	(127, 58, 'VerificationCriteria', '49.2.1 Dries the baby immediately and wraps in second warm towel '),
	(128, 59, 'VerificationCriteria', '49.3.1 If baby''s breathing is normal, delays the clamping of cord for 1-3 minutes till the cord pulsations stop '),
	(129, 60, 'VerificationCriteria', '49.4.1 Initiates breast feeding within one hour of birth '),
	(130, 61, 'VerificationCriteria', '49.5.1 Records presence or absence of any congenital anomalies '),
	(131, 61, 'VerificationCriteria', '49.5.2  Ensures specialist attention for newborns with congenital anomalies '),
	(132, 62, 'VerificationCriteria', '49.6.1 Baby weighing scale is available'),
	(133, 62, 'VerificationCriteria', '49.6.2 Vitamin K injection is available'),
	(134, 62, 'VerificationCriteria', '49.6.3 Records baby weight and administration of vitamin K'),
	(135, 63, 'VerificationCriteria', '50.1.1 Palpates mother''s abdomen to rule out second baby'),
	(136, 63, 'VerificationCriteria', '50.1.2 Administers injection Oxytocin 10 I.U. IM/IV within one minute of delivery of baby'),
	(137, 63, 'VerificationCriteria', '50.1.3 Performs controlled cord traction (CCT) during contraction'),
	(138, 63, 'VerificationCriteria', '50.1.4 Performs uterine massage'),
	(139, 63, 'VerificationCriteria', '50.1.5 Checks placenta and membranes for completeness before discarding'),
	(140, 64, 'VerificationCriteria', '51.1.1 Assesses uterine tone and bleeding per vaginum regularly'),
	(141, 65, 'VerificationCriteria', '51.2.1'),
	(142, 66, 'VerificationCriteria', '51.3.1 Manages shock if present'),
	(143, 67, 'VerificationCriteria', '51.4.1.Initiates 20 IU oxytocin drip in 1000 ml of ringer lactate/normal saline at the rate of 40-60 drops per minute'),
	(144, 67, 'VerificationCriteria', '51.4.2 Continues uterine massage'),
	(145, 67, 'VerificationCriteria', '51.4.3 If uterus is still relaxed, gives other uterotonics as recommended'),
	(146, 67, 'VerificationCriteria', '51.4.4 If uterus is still relaxed, performs mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade'),
	(147, 67, 'VerificationCriteria', '51.4.5 If uterus is still relaxed, refers to higher center while continuing mechanical compression'),
	(148, 68, 'VerificationCriteria', '51.5.1 Identifies retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete'),
	(149, 68, 'VerificationCriteria', '51.5.2 Initiates 20 IU oxytocin drip in 1000 ml of ringer lactate/normal saline at the rate of 40-60 drops per minut'),
	(150, 68, 'VerificationCriteria', '51.5.3 Refers to higher center if unable to manage'),
	(151, 68, 'VerificationCriteria', '51.5.4 Performs MRP'),
	(152, 69, 'VerificationCriteria', '52.1.1 Dipsticks for proteinuria testing in labor room are available'),
	(153, 69, 'VerificationCriteria', '52.1.2 Records BP at admission'),
	(154, 69, 'VerificationCriteria', '52.1.3 Identifies danger signs such as severe headache, blurring of vision, difficulty breathing, epigastric pain, reduced urine output; or presence of convulsions'),
	(155, 70, 'VerificationCriteria', '52.2.1 MgSO4 (at least 14 ampoules) is available'),
	(156, 70, 'VerificationCriteria', '52.2.2 Gives correct first dose of MgSO4 (5 mg with 1 ml of 2% Xylocaine in each buttock deep IM (10 mg)) and refers to higher center'),
	(157, 70, 'VerificationCriteria', '52.2.3 Injection MgSO4 is appropriately administered (5 mg with 1 ml of 2% Xylocaine in each buttock deep IM (10 mg); 4gms (8ml) with 12 ml Normal saline IV slowly followed by maintenance dose of 5mg with 1 ml of 2% Xylocaine in alternate buttock deep IM every 4 hours for 24 hours after the last convulsion or delivery whichever occurs later)'),
	(158, 71, 'VerificationCriteria', '52.3.1 Antihypertensive are available'),
	(159, 71, 'VerificationCriteria', '52.3.2 Facilitates prescription or prescribes anti-hypertensives in case of hypertension in pregnancy'),
	(160, 72, 'VerificationCriteria', '52.4.1 Ensures specialist attention for care of mother and newborn'),
	(161, 73, 'VerificationCriteria', '52.5.1 Ensures specialist attention for care of mother and newborn'),
	(162, 74, 'VerificationCriteria', '53.1.1 Checks for functionality of neonatal resuscitation kit and availability of shoulder roll before every delivery'),
	(163, 74, 'VerificationCriteria', '53.1.2 Performs following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby and rubs the back 2-3 times; immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing'),
	(164, 74, 'VerificationCriteria', '53.1.3 Performs following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR)'),
	(165, 75, 'VerificationCriteria', '53.2.1 Functional ambu bag with mask (size 0 and 1) is available'),
	(166, 75, 'VerificationCriteria', '53.2.2 Initiates bag and mask ventilation for 30 seconds if baby still not breathing'),
	(167, 76, 'VerificationCriteria', '53.3.1 Functional oxygen cylinder (with wrench) and new born mask are available'),
	(168, 76, 'VerificationCriteria', '53.3.2 Assesses breathing, if baby still not breathing, continues bag and mask ventilation; starts oxygen'),
	(169, 76, 'VerificationCriteria', '53.3.3 Checks heart rate/cord pulsations'),
	(170, 76, 'VerificationCriteria', '53.3.4 Calls for advance help/arranges referral'),
	(171, 77, 'VerificationCriteria', '53.4.1 Performs chest compressions at the rate of 3 compressions to 1 breath till the heart rate is > 60 beats/minute'),
	(172, 78, 'VerificationCriteria', '54.1.1 Facilitates specialist care in newborn <1800 gm (refer to FBNC/seen by pediatrician)'),
	(173, 79, 'VerificationCriteria', '54.2.1 Facilitates assisted feeding whenever required'),
	(174, 80, 'VerificationCriteria', '54.3.1 Facilitates thermal management including KMC'),
	(175, 81, 'VerificationCriteria', '55.1.1 Facilities for sterilization of instruments are available'),
	(176, 81, 'VerificationCriteria', '55.1.2 Instruments are sterilized after each use'),
	(177, 81, 'VerificationCriteria', '55.1.3 Delivery environment such as labor table, contaminated surfaces and floors are cleaned after each delivery'),
	(178, 82, 'VerificationCriteria', '55.2.1 Color coded bags for disposal of biomedical waste are available'),
	(179, 82, 'VerificationCriteria', '55.2.2 Biomedical waste is segregated and disposed of as per the guidelines'),
	(180, 83, 'VerificationCriteria', '55.3.1 Performs hand hygiene before and after each procedure, and sterile gloves are worn during delivery and internal examination'),
	(181, 84, 'VerificationCriteria', '56.1.1 Conducts mother''s examination: breast, perineum for inflammation; status of episiotomy/tear suture; lochia; calf tenderness/redness/swelling; abdomen for involution of uterus, tenderness or distension'),
	(182, 84, 'VerificationCriteria', '56.1.2 Conducts newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump'),
	(183, 85, 'VerificationCriteria', '56.2.1 Checks mother''s temperature'),
	(184, 85, 'VerificationCriteria', '56.2.2 Gives correct regimen of antibiotics'),
	(185, 85, 'VerificationCriteria', '56.2.3. Checks baby''s temperature and other looks for other signs of infections'),
	(186, 85, 'VerificationCriteria', '56.2.4 Gives correct regime of antibiotics/refers for specialist care'),
	(187, 86, 'VerificationCriteria', '56.3.1. Makes correct diagnosis of postpartum maternal depression after ruling out postpartum blues based on history'),
	(188, 86, 'VerificationCriteria', '56.3.2. In cases of postpartum blues, provides emotional support and counsel''s family on the condition. Follows up in 2 weeks, and refers for specialist care if required'),
	(189, 86, 'VerificationCriteria', '56.3.3. In cases of postpartum depression, provides emotional support and refers for specialist care'),
	(190, 87, 'VerificationCriteria', '56.4.1'),
	(191, 88, 'VerificationCriteria', '56.5.1 A basket of choice of PPFP services is available at the facility'),
	(192, 88, 'VerificationCriteria', '56.5.2 Provider is trained for PPFP services being offered at the facility'),
	(193, 89, 'VerificationCriteria', '57.1.1 Ensures that all C-section cases are classified as per the modified Robson''s criteria and rates of different categories are monitored'),
	(194, 89, 'VerificationCriteria', '57.1.2 Reviews all cases of induction and C-section through a clinical audit'),
	(195, 89, 'VerificationCriteria', '57.1.3 Ensures that rate of complications of C-sections are periodically monitored'),
	(196, 0, 'Manual', 'NABH_Data_Field'),
	(197, 196, 'Chapter', 'Chapter-1'),
	(198, 196, 'Chapter', 'Chapter-10'),
	(199, 196, 'Chapter', 'Chapter-2'),
	(200, 196, 'Chapter', 'Chapter-3'),
	(201, 196, 'Chapter', 'Chapter-4'),
	(202, 196, 'Chapter', 'Chapter-5'),
	(203, 196, 'Chapter', 'Chapter-6'),
	(204, 196, 'Chapter', 'Chapter-7'),
	(205, 196, 'Chapter', 'Chapter-8'),
	(206, 196, 'Chapter', 'Chapter-9'),
	(207, 197, 'Standard', '1. The SCHO defines and displays the services that it can provide '),
	(208, 199, 'Standard', '10. Documented procedures define rational use of blood and blood products '),
	(209, 199, 'Standard', '11. Documented procedures guide the care of patients as per the scope of services provided by the SCHO in intensive care and high dependancy units. '),
	(210, 199, 'Standard', '12. Documented procedures guide the care of obstetrical patients as per the scope of serices provided by the SCHO.'),
	(211, 199, 'Standard', '12.Documented procedures guide the care of obstetrical patients as per the scope of services provided by the SCHO'),
	(212, 199, 'Standard', '13.Documented procedures guide the care of pediatric patients as per the scope of services provided by the SCHO.'),
	(213, 199, 'Standard', '14. Documented procedures guide the administration of anesthesia.'),
	(214, 199, 'Standard', '15. Documented procedured guide the care of patients undergoing surgical procedures. '),
	(215, 200, 'Standard', '16. Documented procedures that guide the organization of pharmacy services and usage of medication.'),
	(216, 200, 'Standard', '17. Documented procedures guide the prescription of medications. '),
	(217, 200, 'Standard', '18. Policies and procedure guide the safe dispensing of medicines.'),
	(218, 200, 'Standard', '19. There are defined procedures for medication administration.'),
	(219, 197, 'Standard', '2. The SCHO has a documented registration, admission and transfer process '),
	(220, 200, 'Standard', '20. Adverse drug events are monitored.'),
	(221, 201, 'Standard', '21. Patient rights are documented displayed and support individual beliefs, values and involve the patient and family in decision making process '),
	(222, 201, 'Standard', '22. Patient families have a right to information and education about their healthcare needs '),
	(223, 202, 'Standard', '23. The SCHO has an infection control manual which it periodically updates; the SHCO conducts surveillance activities '),
	(224, 202, 'Standard', '24. The SCHO rakes actions to prevent or reduce the risks of hospital associates infections (HAI) in patient and employees.'),
	(225, 202, 'Standard', '25. Bio-medical management practices are followed '),
	(226, 203, 'Standard', '26. There is a structures quality improvement and contonuous motnitring programme in the organisation'),
	(227, 203, 'Standard', '27. The SCHO identifies key indicators to monitor the structures, processes, and outcomes which are used as tools for continuous improvement '),
	(228, 204, 'Standard', '28. The responsibilities of management are defined. '),
	(229, 204, 'Standard', '29. The orgnisation is managed by the leaders by the leaders in an ethical manner '),
	(230, 197, 'Standard', '3. Patients cared for by the SHCO undergo an established initial assessment '),
	(231, 205, 'Standard', '30. The SHCO''s environment and facilities operate to ensure safety of patients, their families, staff and visitors.'),
	(232, 205, 'Standard', '31. The SCHO has a program for clinical and support service equipment management '),
	(233, 205, 'Standard', '32. The SCHO has provisions for safe water, electricity, medical gas, and vacuum systems.'),
	(234, 205, 'Standard', '33. The SCHO has plans for fire and nonfire emergencies within the facilities. '),
	(235, 206, 'Standard', '34. The SCHO has an ongoing programme for professional training and development of the staff'),
	(236, 206, 'Standard', '35. The SCHO has a well-documented disciplinary and grievance handling procedure '),
	(237, 206, 'Standard', '36. The SCHO addresses the health needs of employees '),
	(238, 206, 'Standard', '37. There is documented personal record for each staff member '),
	(239, 198, 'Standard', '38. The SCHO has a complete and accurate medical record for every patient.'),
	(240, 198, 'Standard', '39. The medical record reflects continuity of care '),
	(241, 197, 'Standard', '4. Patientâs care is continuous and all patients cared for by the SHCO undergo a regular assessment.'),
	(242, 198, 'Standard', '40. Documented policies and procedures are in place for maintaining confidentiality, security, and integrity of records, data and information.'),
	(243, 198, 'Standard', '41. Documented procedures exist for retention of the patient''s records, data and information. '),
	(244, 197, 'Standard', '5. Laboratory serivces are provided as per the scope of the SCHO''s services and laboratory safety requirements '),
	(245, 197, 'Standard', '6. Managing services are provided as per the scope of the hospital''s services and established radiation safety programme '),
	(246, 197, 'Standard', '7. The SHCO has qa defined discharge process '),
	(247, 199, 'Standard', '8. Care of patients is guided by accepted norms and practice '),
	(248, 199, 'Standard', '9. Emergency services including ambulance and guided by documented procedures and applicable laws and regulations '),
	(249, 207, 'VerificationCriteria', '1.1 The services being provided are clearly defined.'),
	(250, 207, 'VerificationCriteria', '1.2 The defined services are prominently displayed.'),
	(251, 207, 'VerificationCriteria', '1.3 The relevant staff are oriented to these services.'),
	(252, 208, 'VerificationCriteria', '10.1 The transfusion services are governed by the applicable laws and regulations.'),
	(253, 208, 'VerificationCriteria', '10.2 Informed consent is obtained for donation and transfusion of blood and blood products.'),
	(254, 208, 'VerificationCriteria', '10.3 Procedure addresses documenting and reporting of transfusion reactions'),
	(255, 209, 'VerificationCriteria', '11.1 Care of patients is in consonance with the documented procedures.'),
	(256, 209, 'VerificationCriteria', '11.2 Adequate staff and equipment are available.'),
	(257, 211, 'VerificationCriteria', '12.1 The SHCO defines the scope of obstetric services.'),
	(258, 210, 'VerificationCriteria', '12.2 Obstetric patient''s care includes regular antenatal check-ups, maternal nutrition, and postnatal care.'),
	(259, 210, 'VerificationCriteria', '12.3 The SHCO has the facilities to take care of neonates.'),
	(260, 212, 'VerificationCriteria', '13.1 The SHCO defines the scope of its paediatric services.'),
	(261, 212, 'VerificationCriteria', '13.2 Provisions are made for special care of children by competent staff.'),
	(262, 212, 'VerificationCriteria', '13.3 Patient assessment includes detailed nutritional growth and immunization assessment.'),
	(263, 212, 'VerificationCriteria', '13.4 Procedure addresses identification and security measures to prevent child or neonate abduction and abuse.'),
	(264, 212, 'VerificationCriteria', '13.5 The children''s family members are educated about nutrition, immunization and safe parenting.'),
	(265, 213, 'VerificationCriteria', '14.1 There is a documented policy and procedure for the administration of anaesthesia.'),
	(266, 213, 'VerificationCriteria', '14.2 All patients for anaesthesia have a pre-anaesthesia assessment by a qualified or trained individual.'),
	(267, 213, 'VerificationCriteria', '14.3 The pre-anaesthesia assessment results in formulation of an anaesthesia plan which is documented.'),
	(268, 213, 'VerificationCriteria', '14.4 An immediate preoperative revaluation is documented.'),
	(269, 213, 'VerificationCriteria', '14.5 Informed consent for administration of anaesthesia is obtained by the anesthetist.'),
	(270, 213, 'VerificationCriteria', '14.6 Anaesthesia monitoring includes regular and periodic recording of heart rate, cardiac rhythm, respiratory rate, blood pressure, oxygen saturation, airway security, and potency and level of anesthesia.'),
	(271, 213, 'VerificationCriteria', '14.7 Each patient''s post anaesthesia status is monitored and documented.'),
	(272, 214, 'VerificationCriteria', '15.1 Surgical patients have a preoperative assessment and a provisional diagnosis documented prior to surgery.'),
	(273, 214, 'VerificationCriteria', '15.2 Informed consent is obtained by a surgeon prior to the procedure.'),
	(274, 214, 'VerificationCriteria', '15.3 Documented procedures address the prevention of adverse events like wrong site, wrong patient, and wrong surgery.'),
	(275, 214, 'VerificationCriteria', '15.4 Qualified persons are permitted to perform the procedures that they are entitled to perform.'),
	(276, 214, 'VerificationCriteria', '15.5 The operating surgeon documents the operative notes and postoperative plan of care.'),
	(277, 214, 'VerificationCriteria', '15.6 The operation theatre is adequately equipped and monitored for infection control practices.'),
	(278, 215, 'VerificationCriteria', '16.1 Documented procedures incorporate purchase, storage, prescription, and dispensation of medications.'),
	(279, 215, 'VerificationCriteria', '16.2 These comply with the applicable laws and regulations.'),
	(280, 215, 'VerificationCriteria', '16.3 Sound alike and look alike medications are stored separately.'),
	(281, 215, 'VerificationCriteria', '16.4 Medications beyond the expiry date are not stored or used.'),
	(282, 215, 'VerificationCriteria', '16.5 Documented procedures address procurement and usage of implantable prosthesis.'),
	(283, 216, 'VerificationCriteria', '17.1 The SHCO determines who can write orders.'),
	(284, 216, 'VerificationCriteria', '17.2 Orders are written in a uniform location in the medical records.'),
	(285, 216, 'VerificationCriteria', '17.3 Medication orders are clear, legible, dated and signed.'),
	(286, 216, 'VerificationCriteria', '17.4 The SHCO defines a list of high-risk medication and process to prescribe them.'),
	(287, 217, 'VerificationCriteria', '18.1 Medications are checked prior to dispensing including expiry date to ensure that they are fit for use'),
	(288, 217, 'VerificationCriteria', '18.2 High risk medication orders are verified prior to dispensing'),
	(289, 218, 'VerificationCriteria', '19.1 Medications ae administered by trained personnel'),
	(290, 218, 'VerificationCriteria', '19.2 High risk medication orders are verified prior to administration, medication order including patient, dosage, route and timing are verified'),
	(291, 218, 'VerificationCriteria', '19.3 Prepared medication is labelled prior to preparation of second drug'),
	(292, 218, 'VerificationCriteria', '19.4 Medication administration is documented'),
	(293, 218, 'VerificationCriteria', '19.5 A proper record is kept of the usage administration and disposal of narcotics and psychotropic medication'),
	(294, 219, 'VerificationCriteria', '2.1 Process addresses registering and admitting outpatients, inpatients, and emergency patients.'),
	(295, 219, 'VerificationCriteria', '2.2 Process addresses mechanism for transfer or referral of patients who do not match the SHCO''s resources'),
	(296, 220, 'VerificationCriteria', '20.1 Adverse drug event are defined and monitored'),
	(297, 220, 'VerificationCriteria', '20.2 Adverse drug events are documented and reported within a specified time frame'),
	(298, 221, 'VerificationCriteria', '21.1 Patient rights include respect for personal dignity and privacy during examination procedures and treatment'),
	(299, 221, 'VerificationCriteria', '21.2 Patient rights include protection from physical abuse or neglect'),
	(300, 221, 'VerificationCriteria', '21.3 Patient rights include treating patient information as confidential'),
	(301, 221, 'VerificationCriteria', '21.4 Patient rights include obtaining informed consent before carrying out procedures'),
	(302, 221, 'VerificationCriteria', '21.5 Patient rights include information on how to voice a complaint'),
	(303, 221, 'VerificationCriteria', '21.6 Patient rights include on the expected cost of the treatment'),
	(304, 221, 'VerificationCriteria', '21.7 Patient has a right to have an access to his / her clinical records'),
	(305, 222, 'VerificationCriteria', '22.1 Patients and families are educated on plan of care, preventive aspects, possible complications, medications, the expected results and cost as applicable'),
	(306, 222, 'VerificationCriteria', '22.2 Patients are taught in a language and format that they can understand '),
	(307, 223, 'VerificationCriteria', '23.1 It focuses on adherence to standard precautions at all times.'),
	(308, 223, 'VerificationCriteria', '23.2 Cleanliness and general hygiene of facilities will be maintained and monitored.'),
	(309, 223, 'VerificationCriteria', '23.3 Cleaning and disinfection practices are defined and monitored as appropriate.'),
	(310, 223, 'VerificationCriteria', '23.4 Equipment cleaning, disinfection and sterilization practices are included.'),
	(311, 223, 'VerificationCriteria', '23.5 Laundry and linen management processes are also included.'),
	(312, 224, 'VerificationCriteria', '24.1 Hand hygiene facilities in all patient care areas are accessible to health care provide'),
	(313, 224, 'VerificationCriteria', '24.2 Adequate gloves, masks, soaps, and disinfectants are available and used correctly'),
	(314, 224, 'VerificationCriteria', '24.3 Appropriate pre and post exposure prophylaxis is provided to all concerned staff members'),
	(315, 225, 'VerificationCriteria', '25.1 The hospital is authorized by prescribed authority for management and handling of bio-medical waste.'),
	(316, 225, 'VerificationCriteria', '25.2 Proper segregation and collection of bio-medical waste from all patient care areas of the hospital is implemented and monitored'),
	(317, 225, 'VerificationCriteria', '25.3 Bio-medical waste treatment facility is managed as per statutory provisions (if in-house) or outsourced to authorized contractors'),
	(318, 225, 'VerificationCriteria', '25.4 Requisite fees, documents and reports are submitted to competent authorities on stipulated dates'),
	(319, 225, 'VerificationCriteria', '25.5 Appropriate personal protective measures are used by all categories of staff handling bio-medical waste'),
	(320, 226, 'VerificationCriteria', '26.1 There is a designated individual for coordinating and implementing the quality improvement program'),
	(321, 226, 'VerificationCriteria', '26.2 The quality improvement programme is a continuous process and updated at least once in a year'),
	(322, 226, 'VerificationCriteria', '26.3 Hospital Management makes available adequate resources required for quality improvement programme'),
	(323, 227, 'VerificationCriteria', '27.1 The SHCO identifies the appropriate key performance indicators in both clinical and managerial areas.'),
	(324, 227, 'VerificationCriteria', '27.2 These indicators shall be monitored.'),
	(325, 228, 'VerificationCriteria', '28.1 The SHCO has a documented organogram.'),
	(326, 228, 'VerificationCriteria', '28.2 The SHCO is registered with appropriate authorities as applicable.'),
	(327, 228, 'VerificationCriteria', '28.3 The SHCO has a designated individual(s) to oversee the hospital-wide safety program.'),
	(328, 229, 'VerificationCriteria', '29.1 The management makes public the mission statement of the organization'),
	(329, 229, 'VerificationCriteria', '29.2 The leaders/management guide the organization to function in an ethical manner'),
	(330, 229, 'VerificationCriteria', '29.3 The organization discloses its ownership'),
	(331, 229, 'VerificationCriteria', '29.4 The organizationâs billing process is accurate and ethical'),
	(332, 230, 'VerificationCriteria', '3.1 The SHCO defines the content of the assessments for inpatients and emergency patients.'),
	(333, 230, 'VerificationCriteria', '3.2 The SHCO determines who can perform the assessments.'),
	(334, 230, 'VerificationCriteria', '3.3 The initial assessment for inpatients is documented within 24 hours or earlier.'),
	(335, 230, 'VerificationCriteria', '3.4 During all phases of care, there is a qualified individual identified as responsible for the patients care, who coordinate the care in all the setting within the organization'),
	(336, 231, 'VerificationCriteria', '30.1 Internal and external signages shall be displayed in a language understood by the patients or families and communities.'),
	(337, 231, 'VerificationCriteria', '30.2 Maintenance staff is contactable round the clock for emergency repairs.'),
	(338, 231, 'VerificationCriteria', '30.3 The SHCO has a system to identify the potential safety and security risks including hazardous materials.'),
	(339, 231, 'VerificationCriteria', '30.4 Facility inspection rounds to ensure safety are conducted periodically.'),
	(340, 231, 'VerificationCriteria', '30.5 There is a safety education programme for relevant staff.'),
	(341, 232, 'VerificationCriteria', '31.1 The SHCO plans for equipment in accordance with its services.'),
	(342, 232, 'VerificationCriteria', '31.2 There is a documented operational and maintenance (preventive and breakdown) plan.'),
	(343, 233, 'VerificationCriteria', '32.1 Potable water and electricity are available round the clock.'),
	(344, 233, 'VerificationCriteria', '32.2 Alternate sources are provided for in case of failure and tested regularly.'),
	(345, 233, 'VerificationCriteria', '32.3 There is a maintenance plan for medical gas and vacuum systems.'),
	(346, 234, 'VerificationCriteria', '33.1 The SHCO has plans and provisions for early detection, abatement, and containment of fire and non-fire emergencies.'),
	(347, 234, 'VerificationCriteria', '33.2 The SHCO has a documented safe exit plan in case of fire and non-fire emergencies.'),
	(348, 234, 'VerificationCriteria', '33.3 Staff is trained for their role in case of such emergencies.'),
	(349, 234, 'VerificationCriteria', '33.4 Mock drills are held at least twice in a year.'),
	(350, 235, 'VerificationCriteria', '34.1 All staff is trained on the relevant risks within the hospital environment'),
	(351, 235, 'VerificationCriteria', '34.2 Staff members can demonstrate and take actions to report, eliminate/ minimize risks'),
	(352, 235, 'VerificationCriteria', '34.3 Training also occurs when job responsibilities change/ new equipment is introduced'),
	(353, 236, 'VerificationCriteria', '35.1 A documented procedure regarding disciplinary and grievance handling is in place.'),
	(354, 236, 'VerificationCriteria', '35.2 The documented procedure is known to all categories of employees in the SHCO.'),
	(355, 236, 'VerificationCriteria', '35.3 Actions are taken to redress the grievance.'),
	(356, 237, 'VerificationCriteria', '36.1 Health problems of the employees are taken care of in accordance with the SHCO''s policy.'),
	(357, 237, 'VerificationCriteria', '36.2 Occupational health hazards are adequately addressed.'),
	(358, 238, 'VerificationCriteria', '37.1 Personal files are maintained in respect of all employees.'),
	(359, 238, 'VerificationCriteria', '37.2 The personal files contain personal information regarding the employees qualification, disciplinary actions and health status'),
	(360, 239, 'VerificationCriteria', '38.1 Every medical record has a unique identifier.'),
	(361, 239, 'VerificationCriteria', '38.2 The SHCO identifies those authorized to make entries in medical record.'),
	(362, 239, 'VerificationCriteria', '38.3 Every medical record entry is dated and timed.'),
	(363, 239, 'VerificationCriteria', '38.4 The author of the entry can be identified.'),
	(364, 239, 'VerificationCriteria', '38.5 The contents of medical records are identified and documented.'),
	(365, 240, 'VerificationCriteria', '39.1 The records provides an up-to-date and chronological account of patient care.'),
	(366, 240, 'VerificationCriteria', '39.2 The medical record contains information regarding reasons of admission, diagnosis and plan of care'),
	(367, 240, 'VerificationCriteria', '39.3 Operative and other procedures performed are incorporated in the medical record'),
	(368, 240, 'VerificationCriteria', '39.4 The medical record contains a copy of the discharge note duly signed by the appropriate and qualified personnel'),
	(369, 240, 'VerificationCriteria', '39.5 In case of death, the medical records contain a copy of the death certificate indicating the cause, date and time of death'),
	(370, 240, 'VerificationCriteria', '39.6 Care providers have access to current and past medical record'),
	(371, 241, 'VerificationCriteria', '4.1 All patients are reassessed at appropriate intervals'),
	(372, 241, 'VerificationCriteria', '4.2 Staff involved in direct clinical care document reassessments'),
	(373, 241, 'VerificationCriteria', '4.3 Patients are reassessed to determine their response to treatment and to plan further treatment or discharge'),
	(374, 242, 'VerificationCriteria', '40.1 Documented procedures exist for maintaining confidentiality, security and integrity of information.'),
	(375, 242, 'VerificationCriteria', '40.2 Privileged health information is used for the purposes identified or as required by law and not disclosed without the patient''s authorization.'),
	(376, 243, 'VerificationCriteria', '41.1 Documented procedures exist for retention time of the patient''s clinical records, data and information.'),
	(377, 243, 'VerificationCriteria', '41.2 The retention process provides expected confidentiality and security.'),
	(378, 243, 'VerificationCriteria', '41.3 The destruction of medical records, data, and information is in accordance with the laid down procedure.'),
	(379, 244, 'VerificationCriteria', '5.1 Scope of the laboratory services are commensurate with the services provided by the SHCO.'),
	(380, 244, 'VerificationCriteria', '5.2 Procedures guide collection, identification, handling, safe transportation, processing and disposal of specimens.'),
	(381, 244, 'VerificationCriteria', '5.3 Laboratory results are available within a defined time frame and critical results are intimated immediately to the concerned personnel.'),
	(382, 244, 'VerificationCriteria', '5.4 Laboratory personnel are trained in safe practices and are provided with appropriate safety equipment or devices.'),
	(383, 245, 'VerificationCriteria', '6.1 Imaging services comply with legal and other requirements'),
	(384, 245, 'VerificationCriteria', '6.2 Scope of the imaging services are commensurate to the services provided by the SHCO'),
	(385, 245, 'VerificationCriteria', '6.3 Imaging results are available within a defined time frame and critical results are intimated immediately to the concerned personnel'),
	(386, 246, 'VerificationCriteria', '7.1 Process addresses discharge of all patients including medico-legal cases (MLCs) and patients leaving against medical advice.'),
	(387, 246, 'VerificationCriteria', '7.2 A discharge summary is given to all the patients leaving the SHCO (including patients leaving against medical advice).'),
	(388, 246, 'VerificationCriteria', '7.3 Discharge summary contains the reasons for admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given, and the patient''s condition at the time of discharge.'),
	(389, 246, 'VerificationCriteria', '7.4 Discharge summary contains follow-up advice, medication and other instructions in an understandable manner.'),
	(390, 246, 'VerificationCriteria', '7.5 Discharge summary incorporates information about when and how to obtain urgent care'),
	(391, 246, 'VerificationCriteria', '7.6 In case of death the summary of the case also includes the cause of death'),
	(392, 247, 'VerificationCriteria', '8.1 The care and treatment order are signed and dated by the concerned doctor'),
	(393, 247, 'VerificationCriteria', '8.2 Clinical Practice Guidelines are adopted to guide patient care wherever possible'),
	(394, 248, 'VerificationCriteria', '9.1 Documented procedures address care of patients arriving in the emergency including handling of medico-legal cases.'),
	(395, 248, 'VerificationCriteria', '9.2 Staff should be well versed in the care of Emergency patients in consonance with the scope of the services of hospital.'),
	(396, 248, 'VerificationCriteria', '9.3 Admission or discharge to home or transfer to another organization is also documented.')
GO

SET IDENTITY_INSERT dropdown.QualityStandard OFF
GO

UPDATE QS
SET QS.DisplayOrder = 
	CASE
		WHEN QS.QualityStandardID = 197
		THEN 1
		WHEN QS.QualityStandardID = 198
		THEN 10
		ELSE QS.QualityStandardID - 197
	END
FROM dropdown.QualityStandard QS
WHERE QS.QualityStandardID > 196
	AND  QS.QualityStandardID < 207
GO
--End table dropdown.QualityStandard

--Begin table dropdown.ScoreChangeReason
TRUNCATE TABLE dropdown.ScoreChangeReason
GO

EXEC utility.InsertIdentityValue 'dropdown.ScoreChangeReason', 'ScoreChangeReasonID', 0
GO

INSERT INTO dropdown.ScoreChangeReason 
	(ScoreChangeReasonName)
VALUES
	('Score Change Reason 1'),
	('Score Change Reason 2'),
	('Score Change Reason 3')
GO
--End table dropdown.ScoreChangeReason

--Begin table dropdown.VisitPurpose
TRUNCATE TABLE dropdown.VisitPurpose
GO

EXEC utility.InsertIdentityValue 'dropdown.VisitPurpose', 'VisitPurposeID', 0
GO

INSERT INTO dropdown.VisitPurpose 
	(VisitPurposeName)
VALUES
	('Baseline Assesment'),
	('Ongoing Review'),
	('Pre-Verification Review')
GO
--End table dropdown.VisitPurpose

--Begin permissions
EXEC permissionable.SavePermissionableGroup 'Visits', 'Visits', 0;
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityStandardAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityStandardAssessment', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='Add / edit a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='Add / edit a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='Add / edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View the list of quality assessments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.List', @PERMISSIONCODE=NULL;

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End permissions

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
EXEC permissionable.SavePermissionableGroup 'Visits', 'Visits', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='Add / edit an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View the list of inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='Add / edit an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View the list of MOUs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='Add / edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View the list of quality assessments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Add / edit a visit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='QualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.QualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityStandardAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityStandardAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitPlanList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitPlanList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitReportList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitReportList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='Add / edit a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='Add / edit a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.2 - 2017.11.12 17.51.16')
GO
--End build tracking

