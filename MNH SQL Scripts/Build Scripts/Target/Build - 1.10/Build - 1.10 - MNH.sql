-- File Name:	MNH.sql
-- Build Key:	Build - 1.10 - 2018.10.29 18.19.52

USE MNH
GO

-- ==============================================================================================================================
-- Schemas:
--		reporting
--
-- Tables:
--		dropdown.QualityStandardVerificationCriteria
--		dropdown.VerificationCriteriaCategory
--		dropdown.VerificationCriteriaProtocol
--		dropdown.VerificationCriteriaSubCategory
--		dropdown.VisitOutcome
--
-- Functions:
--		visit.GetAssessmentEvidenceTypeIDList
--		visit.GetPriorVisitQualityStandardScore
--
-- Procedures:
--		document.GetDocumentByDocumentEntityCode
--		document.processGeneralFileUploads
--		dropdown.GetFacilityServiceData
--		dropdown.GetQualityStandardLookupData
--		dropdown.GetVerificationCriteriaCategoryData
--		dropdown.GetVerificationCriteriaProtocolData
--		dropdown.GetVerificationCriteriaSubCategoryData
--		dropdown.GetVisitOutcomeData
--		dropdown.SaveGuidanceNotesByQualityStandardID
--		facility.GetFacilityByFacilityID
--		facility.ImportFacilities
--		facility.UpdateInclusionEligibilityStatus
--		reporting.GetFacilityData
--		reporting.GetVisitData
--		utility.InsertIdentityValue
--		visit.FilterVisitQualityStandardData
--		visit.GetVisitByVisitID
--		visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
--		visit.GetVisitsByPersonID
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

EXEC utility.AddSchema 'reporting'
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
--Begin table dropdown.QualityStandard
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandard'

EXEC utility.AddColumn @TableName, 'GuidanceNotes', 'VARCHAR(MAX)'
GO
--End table dropdown.QualityStandard

--Begin table dropdown.QualityStandardVerificationCriteria
DECLARE @TableName VARCHAR(250) = 'dropdown.QualityStandardVerificationCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.QualityStandardVerificationCriteria
	(
	QualityStandardVerificationCriteriaID INT IDENTITY(1,1) NOT NULL,
	QualityStandardID INT,
	VerificationCriteriaCode VARCHAR(50),
	VerificationCriteriaID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'QualityStandardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VerificationCriteriaID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QualityStandardVerificationCriteriaID'
EXEC utility.SetIndexClustered @TableName, 'IX_QualityStandardVerificationCriteria', 'QualityStandardID,VerificationCriteriaCode,VerificationCriteriaID'
GO
--End table dropdown.QualityStandardVerificationCriteria

--Begin table dropdown.VerificationCriteriaCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaCategory
	(
	VerificationCriteriaCategoryID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaCategory', 'DisplayOrder,VerificationCriteriaCategoryName', 'VerificationCriteriaCategoryID'
GO
--End table dropdown.VerificationCriteriaCategory

--Begin table dropdown.VerificationCriteriaProtocol
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaProtocol'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaProtocol
	(
	VerificationCriteriaProtocolID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaProtocolName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaProtocolID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaProtocol', 'DisplayOrder,VerificationCriteriaProtocolName', 'VerificationCriteriaProtocolID'
GO
--End table dropdown.VerificationCriteriaProtocol

--Begin table dropdown.VerificationCriteriaSubCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.VerificationCriteriaSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VerificationCriteriaSubCategory
	(
	VerificationCriteriaSubCategoryID INT IDENTITY(0,1) NOT NULL,
	VerificationCriteriaSubCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VerificationCriteriaSubCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VerificationCriteriaSubCategory', 'DisplayOrder,VerificationCriteriaSubCategoryName', 'VerificationCriteriaSubCategoryID'
GO
--End table dropdown.VerificationCriteriaSubCategory

--Begin table dropdown.VisitOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.VisitOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VisitOutcome
	(
	VisitOutcomeID INT IDENTITY(0,1) NOT NULL,
	VisitOutcomeName VARCHAR(50),
	HasNote BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasNote', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VisitOutcomeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VisitOutcome', 'DisplayOrder,VisitOutcomeName', 'VisitOutcomeID'
GO
--End table dropdown.VisitOutcome

--Begin table facility.FacilityFacilityService
EXEC utility.DropObject 'facility.FacilityFacilityService'
GO
--End table facility.FacilityFacilityService

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.AddColumn @TableName, 'VisitOutcomeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'VisitOutcomeNote', 'VARCHAR(250)'
GO
--End table visit.Visit


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql

--Begin function visit.GetAssessmentEvidenceTypeIDList
EXEC utility.DropObject 'visit.GetAssessmentEvidenceTypeIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION visit.GetAssessmentEvidenceTypeIDList
(
@QualityStandardID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cAssessmentEvidenceTypeIDList VARCHAR(MAX) = ''

	SELECT @cAssessmentEvidenceTypeIDList = COALESCE(@cAssessmentEvidenceTypeIDList + ',', '') + CAST(QSAET.AssessmentEvidenceTypeID AS VARCHAR(5))
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
	WHERE QSAET.QualityStandardID = @QualityStandardID

	IF LEFT(@cAssessmentEvidenceTypeIDList, 1) = ','
		SET @cAssessmentEvidenceTypeIDList = STUFF(@cAssessmentEvidenceTypeIDList, 1, 1, '')
	--ENDIF

	RETURN ISNULL(@cAssessmentEvidenceTypeIDList, '')

END
GO
--End function visit.GetAssessmentEvidenceTypeIDList

--Begin function visit.GetPriorVisitQualityStandardScore
EXEC utility.DropObject 'visit.GetPriorVisitQualityStandardScore'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.29
-- Description:	A function to return a the previous score for a specific facility and quality standard
-- ===================================================================================================

CREATE FUNCTION visit.GetPriorVisitQualityStandardScore
(
@QualityStandardID INT,
@VisitID INT
)

RETURNS VARCHAR(50)

AS
BEGIN
	
	DECLARE @cPriorScore VARCHAR(50)

	SELECT TOP 1 
		@cPriorScore = CASE WHEN VQS.NotApplicable = 1 THEN 'Not Applicable' ELSE CAST(VQS.Score AS VARCHAR(50)) END
	FROM visit.VisitQualityStandard VQS
		JOIN visit.Visit V1 ON V1.VisitID = VQS.VisitID
			AND VQS.QualityStandardID = @QualityStandardID
			AND V1.FacilityID = (SELECT V2.FacilityID FROM visit.Visit V2 WHERE V2.VisitID = @VisitID)
			AND V1.VisitID <> @VisitID
	ORDER BY VQS.VisitQualityStandardID DESC

	RETURN ISNULL(@cPriorScore, '0')

END
GO
--End function visit.GetPriorVisitQualityStandardScore
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure document.GetDocumentByDocumentEntityCode
EXEC Utility.DropObject 'document.GetDocumentByDocumentEntityCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentEntityCode

@DocumentEntityCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.Extension,
		D.PhysicalFileSize
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.DocumentEntityCode = @DocumentEntityCode
	
END
GO
--End procedure document.GetDocumentByDocumentEntityCode

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure dropdown.GetFacilityServiceData
EXEC Utility.DropObject 'dropdown.GetFacilityServiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityService table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetFacilityServiceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityServiceID, 
		T.FacilityServiceCode,
		T.FacilityServiceName,
		T.DisplayOrder
	FROM dropdown.FacilityService T
	WHERE (T.FacilityServiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityServiceName, T.FacilityServiceID

END
GO
--End procedure dropdown.GetFacilityServiceData

--Begin procedure dropdown.GetQualityStandardLookupData
EXEC Utility.DropObject 'dropdown.GetQualityStandardLookupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Christopher Crouch
-- Create date: 2018.10.19
-- Description:	This gets the full list of quality standards with guidance notes
-- =============================================================================
CREATE PROCEDURE dropdown.GetQualityStandardLookupData
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		QS.GuidanceNotes,
		QSL.ChapterID,
		QSL.ChapterName,
		QSL.EntityTypeCode,
		QSL.ManualID,
		QSL.ManualName,
		QSL.ObjectiveElementID,
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName
	FROM dropdown.QualityStandardLookup QSL
		JOIN dropdown.QualityStandard QS ON QSL.QualityStandardID = QS.QualityStandardID
			AND QS.GuidanceNotes IS NOT NULL
	ORDER BY
		QSL.ManualName,
		QSL.ManualID,
		QSL.ChapterName,
		QSL.ChapterID,
		QSL.StandardName,
		QSL.StandardID,
		QSL.ObjectiveElementName,
		QSL.ObjectiveElementID,
		QSL.VerificationCriteriaName,
		QSL.VerificationCriteriaID

END
GO
--End procedure dropdown.GetQualityStandardLookupData

--Begin procedure dropdown.GetVerificationCriteriaCategoryData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaCategory table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaCategoryID,
		T.VerificationCriteriaCategoryName
	FROM dropdown.VerificationCriteriaCategory T
	WHERE (T.VerificationCriteriaCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaCategoryName, T.VerificationCriteriaCategoryID

END
GO
--End procedure dropdown.GetVerificationCriteriaCategoryData

--Begin procedure dropdown.GetVerificationCriteriaProtocolData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaProtocolData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaProtocol table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaProtocolData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaProtocolID,
		T.VerificationCriteriaProtocolName
	FROM dropdown.VerificationCriteriaProtocol T
	WHERE (T.VerificationCriteriaProtocolID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaProtocolName, T.VerificationCriteriaProtocolID

END
GO
--End procedure dropdown.GetVerificationCriteriaProtocolData

--Begin procedure dropdown.GetVerificationCriteriaSubCategoryData
EXEC Utility.DropObject 'dropdown.GetVerificationCriteriaSubCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.21
-- Description:	A stored procedure to return data from the dropdown.VerificationCriteriaSubCategory table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetVerificationCriteriaSubCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VerificationCriteriaSubCategoryID,
		T.VerificationCriteriaSubCategoryName
	FROM dropdown.VerificationCriteriaSubCategory T
	WHERE (T.VerificationCriteriaSubCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VerificationCriteriaSubCategoryName, T.VerificationCriteriaSubCategoryID

END
GO
--End procedure dropdown.GetVerificationCriteriaSubCategoryData

--Begin procedure dropdown.GetVisitOutcomeData
EXEC Utility.DropObject 'dropdown.GetVisitOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.20
-- Description:	A stored procedure to return data from the dropdown.VisitOutcome table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetVisitOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VisitOutcomeID,
		T.HasNote,
		T.VisitOutcomeName
	FROM dropdown.VisitOutcome T
	WHERE (T.VisitOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VisitOutcomeName, T.VisitOutcomeID

END
GO
--End procedure dropdown.GetVisitOutcomeData

--Begin procedure dropdown.SaveGuidanceNotesByQualityStandardID
EXEC Utility.DropObject 'dropdown.SaveGuidanceNotesByQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.20
-- Description:	A stored procedure to save data to the dropdown.QualityStandard table
-- ==================================================================================
CREATE PROCEDURE dropdown.SaveGuidanceNotesByQualityStandardID

@GuidanceNotes VARCHAR(MAX),
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE QS
	SET QS.GuidanceNotes = @GuidanceNotes
	FROM dropdown.QualityStandard QS
	WHERE QS.QualityStandardID = @QualityStandardID

END
GO
--End procedure dropdown.SaveGuidanceNotesByQualityStandardID

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.AuthorizedDate, 
		core.FormatDate(F.AuthorizedDate) AS AuthorizedDateFormatted,
		F.AuthorizerPersonID,
		person.FormatPersonNameByPersonID(F.AuthorizerPersonID, 'LastFirstTitle') AS AuthorizerPersonNameFormatted,		
		F.BedCount,
		F.Description, 			
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.FacilityID,
		core.FormatDate(eventlog.GetFirstCreateDateTime('Facility', F.FacilityID)) AS FacilityCreateDateFormatted,
		core.FormatDate(eventlog.GetLastCreateDateTime('Facility', F.FacilityID)) AS FacilityUpdateDateFormatted,
		F.IsActive,
		F.FacilityName,
		F.InclusionCriteriaNotes,
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		FS1.FacilityServiceID,
		FS1.FacilityServiceCode,
		FS1.FacilityServiceName,
		FS2.FacilityStatusID, 	
		FS2.FacilityStatusName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted
	FROM facility.Facility F
		JOIN dropdown.FacilityService FS1 ON FS1.FacilityServiceID = F.FacilityServiceID
		JOIN dropdown.FacilityStatus FS2 ON FS2.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = F.InclusionEligibilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	IF @FacilityID = 0
		BEGIN

		SELECT
			0 AS FacilityInclusionCriteriaID, 
			0 AS IsAuthorized,
			0 AS IsCriteriaMet, 
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM dropdown.InclusionCriteria IC
		WHERE IC.InclusionCriteriaID > 0
			AND IC.IsActive = 1
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	ELSE
		BEGIN

		SELECT
			FIC.FacilityInclusionCriteriaID, 
			FIC.IsAuthorized,
			FIC.IsCriteriaMet, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.PersonID ELSE 0 END AS PersonID,
			CASE FIC.IsCriteriaMet WHEN 1 THEN person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') ELSE NULL END AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM facility.FacilityInclusionCriteria FIC
			JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
			JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
				AND FIC.FacilityID = @FacilityID
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	--ENDIF

	--FacilityFacilityInclusionCriteriaDocument
	SELECT
		DE.EntityID AS FacilityInclusionCriteriaID,
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,

		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FOR JSON PATH
		), '[]') AS UploadedFiles

	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN facility.FacilityInclusionCriteria FIC ON FIC.FacilityInclusionCriteriaID = DE.EntityID
			AND DE.EntityTypeCode = 'FacilityInclusionCriteria'
			AND FIC.FacilityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID

	--FacilityMOUDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode = 'MOU' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.ImportFacilities
EXEC utility.DropObject 'facility.ImportFacilities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Tod Pires
-- Create date:	2018.09.15
-- Description:	A stored procedure to import facility data from the MNH database to the MATH database
-- ==================================================================================================
CREATE PROCEDURE facility.ImportFacilities

@FacilityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE MATHF
	SET
		MATHF.Address = MNHF.Address,
		MATHF.BedCount = MNHF.BedCount,
		MATHF.Description = MNHF.Description,
		MATHF.FacilityEstablishedDate = MNHF.FacilityEstablishedDate,
		MATHF.FacilityName = MNHF.FacilityName,
		MATHF.FacilityServiceID = MNHF.FacilityServiceID,
		MATHF.FacilityStatusID = MNHF.FacilityStatusID,
		MATHF.InclusionCriteriaNotes = MNHF.InclusionCriteriaNotes,
		MATHF.InclusionEligibilityStatusID = MNHF.InclusionEligibilityStatusID,
		MATHF.Location = MNHF.Location,
		MATHF.MOUDate = MNHF.MOUDate,
		MATHF.MOUNotes = MNHF.MOUNotes,
		MATHF.MOUStatusID = MNHF.MOUStatusID,
		MATHF.Phone = MNHF.Phone,
		MATHF.PrimaryContactID = MNHF.PrimaryContactID,
		MATHF.ProjectID = (SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MATHF.TerritoryID = MNHF.TerritoryID
	FROM MATH.facility.Facility MATHF
		JOIN MNH.facility.Facility MNHF ON MNHF.FacilityID = MATHF.FacilityID
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MATHF.FacilityID
	;

	SET IDENTITY_INSERT MATH.facility.Facility ON;

	INSERT INTO MATH.facility.Facility
		(Address,BedCount,Description,FacilityEstablishedDate,FacilityID,FacilityName,FacilityServiceID,FacilityStatusID,InclusionCriteriaNotes,InclusionEligibilityStatusID,Location,MOUDate,MOUNotes,MOUStatusID,Phone,PrimaryContactID,ProjectID,TerritoryID)
	SELECT 
		MNHF.Address,
		MNHF.BedCount,
		MNHF.Description,
		MNHF.FacilityEstablishedDate,
		MNHF.FacilityID,
		MNHF.FacilityName,
		MNHF.FacilityServiceID,
		MNHF.FacilityStatusID,
		MNHF.InclusionCriteriaNotes,
		MNHF.InclusionEligibilityStatusID,
		MNHF.Location,
		MNHF.MOUDate,
		MNHF.MOUNotes,
		MNHF.MOUStatusID,
		MNHF.Phone,
		MNHF.PrimaryContactID,
		(SELECT P2.ProjectID FROM MATH.dropdown.Project P2 WHERE P2.ProjectCode = 'Math'),
		MNHF.TerritoryID
	FROM MNH.facility.Facility MNHF
		JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MNHF.FacilityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM MATH.facility.Facility MATHF
				WHERE MATHF.FacilityID = MNHF.FacilityID
				)
	;

	SET IDENTITY_INSERT MATH.facility.Facility OFF;

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria ON;

	--FacilityInclusionCriteria
	MERGE MATH.facility.FacilityInclusionCriteria T1
	USING 
		(
		SELECT 
			FIC.FacilityID, 
			FIC.FacilityInclusionCriteriaID,
			FIC.InclusionCriteriaID,
			FIC.IsCriteriaMet
		FROM MNH.facility.FacilityInclusionCriteria FIC
			JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FIC.FacilityID
		) T2
		ON T2.FacilityInclusionCriteriaID = T1.FacilityInclusionCriteriaID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.IsCriteriaMet = T2.IsCriteriaMet
	WHEN NOT MATCHED THEN
	INSERT 
		(FacilityID,FacilityInclusionCriteriaID,InclusionCriteriaID,IsCriteriaMet)
	VALUES
		(
		T2.FacilityID,
		T2.FacilityInclusionCriteriaID,
		T2.InclusionCriteriaID,
		T2.IsCriteriaMet
		);

	SET IDENTITY_INSERT MATH.facility.FacilityInclusionCriteria OFF;

	SET IDENTITY_INSERT MATH.contact.Contact ON;

	--FacilityContact
	MERGE MATH.contact.Contact T1
	USING 
		(
		SELECT
			C1.CellPhoneNumber,
			C1.ContactFunctionID,
			C1.ContactID,
			C1.ContactRoleID,
			C1.DateOfBirth,
			C1.DIBRoleID,
			C1.EmailAddress,
			C1.FacilityEndDate,
			C1.FacilityID,
			C1.FacilityStartDate,
			C1.FirstName,
			C1.GenderID,
			C1.IsActive,
			C1.LastName,
			C1.MiddleName,
			C1.OtherContactFunction,
			C1.OtherContactRole,
			C1.PhoneNumber,
			C1.Title,
			C1.YearsExperience
		FROM MNH.contact.Contact C1
		WHERE EXISTS
			(
			SELECT 1
			FROM MNH.facility.FacilityMOUContact FMC
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = FMC.FacilityID
					AND FMC.ContactID = C1.ContactID

			UNION

			SELECT 1
			FROM MNH.contact.Contact C2
				JOIN core.ListToTable(@FacilityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C2.FacilityID
					AND C2.ContactID = C1.ContactID
			) 
		) T2
		ON T2.ContactID = T1.ContactID
	WHEN MATCHED THEN UPDATE 
	SET 
		T1.CellPhoneNumber = T2.CellPhoneNumber,
		T1.ContactFunctionID = T2.ContactFunctionID,
		T1.ContactRoleID = T2.ContactRoleID,
		T1.DateOfBirth = T2.DateOfBirth,
		T1.DIBRoleID = T2.DIBRoleID,
		T1.EmailAddress = T2.EmailAddress,
		T1.FacilityEndDate = T2.FacilityEndDate,
		T1.FacilityID = T2.FacilityID,
		T1.FacilityStartDate = T2.FacilityStartDate,
		T1.FirstName = T2.FirstName,
		T1.GenderID = T2.GenderID,
		T1.IsActive = T2.IsActive,
		T1.LastName = T2.LastName,
		T1.MiddleName = T2.MiddleName,
		T1.OtherContactFunction = T2.OtherContactFunction,
		T1.OtherContactRole = T2.OtherContactRole,
		T1.PhoneNumber = T2.PhoneNumber,
		T1.Title = T2.Title,
		T1.YearsExperience = T2.YearsExperience 
	WHEN NOT MATCHED THEN
	INSERT 
		(CellPhoneNumber,ContactFunctionID,ContactID,ContactRoleID,DateOfBirth,DIBRoleID,EmailAddress,FacilityEndDate,FacilityID,FacilityStartDate,FirstName,GenderID,IsActive,LastName,MiddleName,OtherContactFunction,OtherContactRole,PhoneNumber,Title,YearsExperience)
	VALUES
		(
		T2.CellPhoneNumber,
		T2.ContactFunctionID,
		T2.ContactID,
		T2.ContactRoleID,
		T2.DateOfBirth,
		T2.DIBRoleID,
		T2.EmailAddress,
		T2.FacilityEndDate,
		T2.FacilityID,
		T2.FacilityStartDate,
		T2.FirstName,
		T2.GenderID,
		T2.IsActive,
		T2.LastName,
		T2.MiddleName,
		T2.OtherContactFunction,
		T2.OtherContactRole,
		T2.PhoneNumber,
		T2.Title,
		T2.YearsExperience
		);

	SET IDENTITY_INSERT MATH.contact.Contact OFF;

END
GO
--End procedure facility.ImportFacilities

IF 'MNH' <> 'MATH'
	EXEC Utility.DropObject 'facility.ImportFacilities'
--ENDIF
GO

--Begin procedure facility.UpdateInclusionEligibilityStatus
EXEC Utility.DropObject 'facility.UpdateInclusionEligibilityStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.22
-- Description:	A stored procedure to set the InclusionEligibilityStatusID column
-- ==============================================================================
CREATE PROCEDURE facility.UpdateInclusionEligibilityStatus

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE F1
	SET F1.InclusionEligibilityStatusID = 
		CASE 
			WHEN 
				(
				SELECT F2.BedCount 
				FROM facility.Facility F2
				WHERE F2.FacilityID = F1.FacilityID
				) = 0
				AND 
					(
					SELECT FS.FacilityServiceCode 
					FROM dropdown.FacilityService FS 
						JOIN facility.Facility F3 ON F3.FacilityServiceID = FS.FacilityServiceID 
							AND F3.FacilityID = F1.FacilityID
					) = 'Other'
			THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
			ELSE
				CASE
					WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F1.FacilityID AND FIC.IsCriteriaMet = 1)
					THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'No')
					WHEN NOT EXISTS (SELECT 1 FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = F1.FacilityID AND FIC.IsCriteriaMet = 0)
					THEN (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Yes')
					ELSE (SELECT IES.InclusionEligibilityStatusID FROM dropdown.InclusionEligibilityStatus IES WHERE IES.InclusionEligibilityStatusCode = 'Pending')
				END
		END
	FROM facility.Facility F1
	WHERE F1.FacilityID = @FacilityID

END
GO
--End procedure facility.UpdateInclusionEligibilityStatus

--Begin procedure reporting.GetFacilityData
EXEC Utility.DropObject 'reporting.GetFacilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE reporting.GetFacilityData

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.BedCount,
		F.FacilityEstablishedDate,
		F.FacilityID,
		F.IsActive,
		F.MOUDate,
		FS.FacilityServiceName,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = F.FacilityServiceID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID

END
GO
--End procedure reporting.GetFacilityData

--Begin procedure reporting.GetVisitData
EXEC Utility.DropObject 'reporting.GetVisitData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.18
-- Description:	A stored procedure to get data from the visit.Visit table
-- ============================================================================
CREATE PROCEDURE reporting.GetVisitData

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		QSL.ChapterName,
		QSL.ManualName,
		QSL.ObjectiveElementName,
		QSL.StandardName,
		QSL.VerificationCriteriaName,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitID, 	
		V.VisitOutcomeNote,
		V.VisitStartDateTime,
		V.VisitTime,
		VO.VisitOutcomeName,
		VQS.CorrectRecords, 
		VQS.HasDocumentation, 
		VQS.HasImplementation, 
		VQS.HasObservation, 
		VQS.HasPhysicalVerification, 
		VQS.HasProviderInterview, 
		VQS.IsScored, 
		VQS.NotApplicable,
		VQS.Notes, 
		VQS.Score, 
		VQS.TotalRecordsChecked
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitID = V.VisitID
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID

END
GO
--End procedure reporting.GetVisitData

--Begin procedure visit.FilterVisitQualityStandardData
EXEC Utility.DropObject 'visit.FilterVisitQualityStandardData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.22
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.FilterVisitQualityStandardData

@VisitID INT,
@Score VARCHAR(MAX) = NULL,
@VerificationCriteriaCategoryID INT = 0,
@VerificationProtocolID VARCHAR(MAX) = NULL,
@VerificationSubCriteriaCategoryID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT VQS.QualityStandardID
	FROM visit.VisitQualityStandard VQS
	WHERE VQS.VisitID = @VisitID
		AND 
			(
			@Score IS NULL
				OR EXISTS
					(
					SELECT 1
					FROM core.ListToTable(@Score, ',') LTT
					WHERE CAST(LTT.ListItem AS INT) = VQS.Score
					)
			)
		AND 
			(
			@VerificationCriteriaCategoryID = 0
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaCategory'
						AND QSVC.VerificationCriteriaID = @VerificationCriteriaCategoryID
					)
			)
		AND 
			(
			@VerificationProtocolID IS NULL
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaProtocol'
						AND EXISTS
							(
							SELECT 1
							FROM core.ListToTable(@VerificationProtocolID, ',') LTT
							WHERE CAST(LTT.ListItem AS INT) = QSVC.VerificationCriteriaID
							)
					)
			)
		AND 
			(
			@VerificationSubCriteriaCategoryID = 0
				OR EXISTS
					(
					SELECT 1
					FROM dropdown.QualityStandardVerificationCriteria QSVC
					WHERE QSVC.QualityStandardID = VQS.QualityStandardID
						AND QSVC.VerificationCriteriaCode = 'VerificationCriteriaSubCategory'
						AND QSVC.VerificationCriteriaID = @VerificationSubCriteriaCategoryID
					)
			)

END
GO
--End procedure visit.FilterVisitQualityStandardData

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		F.ProjectID,
		V.IsPlanningComplete,
		V.IsVisitComplete,
		V.VisitOutcomeNote,

		CASE 
			WHEN V.IsPlanningComplete = 0
			THEN 'Planning'
			WHEN V.IsVisitComplete = 1
			THEN 'Visit Complete'
			ELSE 'Planning Complete'
		END AS VisitStatusName,

		V.VisitStartDateTime,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID, 	
		V.VisitLeadPersonID,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitTime,
		VO.VisitOutcomeID,
		VO.VisitOutcomeName,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitOutcome VO ON VO.VisitOutcomeID = V.VisitOutcomeID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT 
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.IsScored,
		VQS.NotApplicable,
		VQS.Score,
		visit.GetAssessmentEvidenceTypeIDList(QSL.QualityStandardID) AS AssessmentEvidenceTypeIDList,
		visit.GetPriorVisitQualityStandardScore(QSL.QualityStandardID, @VisitID) AS PriorScore
	FROM dropdown.QualityStandardLookup QSL
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT
		CASE WHEN CAST(core.GetSystemSetupValueBySystemSetupKey('IsMNH', 1) AS BIT) = 0 THEN QS.GuidanceNotes ELSE NULL END AS GuidanceNotes,
		QSL.ManualName,
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.IsScored,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID
		JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = VQS.QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	SELECT
		ISNULL((
			SELECT
				D.DocumentTitle,
				DE.DocumentEntityCode
			FROM document.DocumentEntity DE
				JOIN document.Document D ON D.DocumentID = DE.DocumentID
					AND DE.EntityTypeCode = 'VisitQualityStandard'
					AND DE.EntityID = 
						(
						SELECT VQS.VisitQualityStandardID 
						FROM visit.VisitQualityStandard VQS
						WHERE VQS.VisitID = @VisitID
							AND VQS.QualityStandardID = @QualityStandardID
						)
			FOR JSON PATH
		), '[]') AS UploadedFiles

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

--Begin procedure visit.GetVisitsByPersonID
EXEC Utility.DropObject 'visit.GetVisitsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.09.25
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
 
	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		P.ProjectID,
		P.ProjectName,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = F.ProjectID
				)
			AND V.IsPlanningComplete = 1
			AND V.IsVisitComplete = 0
	ORDER BY F.FacilityName, F.FacilityID, V.VisitID

END
GO
--End procedure visit.GetVisitsByPersonID

--Begin procedure visit.GetVisitsByVisitIDList
EXEC Utility.DropObject 'visit.GetVisitsByVisitIDList'
GO
--End procedure visit.GetVisitsByVisitIDList
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Visit', 
	@EntityTypeName = 'Visit', 
	@EntityTypeNamePlural = 'Visits',
	@SchemaName = 'core', 
	@TableName = 'Visit', 
	@PrimaryKeyFieldName = 'VisitID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'FacilityList',
	@PermissionableLineageList = 'Facility.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ContactList',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'GraduationPlanStepList',
	@PermissionableLineageList = 'GraduationPlanStep.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'EmailTemplateList',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'EventLogList',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PermissionableList',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PermissionableTemplateList',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'PersonList',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'QualityAssessmentList',
	@PermissionableLineageList = 'QualityAssessment.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'SystemSetupList',
	@PermissionableLineageList = 'SystemSetup.List'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'VisitPlanList',
	@PermissionableLineageList = 'Visit.VisitPlanList'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'VisitReportList',
	@PermissionableLineageList = 'Visit.VisitReportList'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'QualityStandardList',
	@NewMenuItemLink = '/qualitystandard/list',
	@NewMenuItemText = 'Quality Standards',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'QualityStandard.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Sites'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Visits'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
IF 'MNH' = 'MNH'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'IsMNH', NULL, '1'
	EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@mnh.oceandisc.com'	
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/mnhnewlogo.jpg'
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/mnhnewlogo.jpg'
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/mnhnewlogo.jpg'
	EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://mnh.oceandisc.com'
	EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'MNH'

	END
ELSE	
	BEGIN

	EXEC core.SystemSetupAddUpdate 'IsMNH', NULL, '0'
	EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@math.oceandisc.com'	
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/math-logo.png'
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/math-logo.png'
	EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/math-logo.png'
	EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://math.oceandisc.com'
	EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'Mathematica'

	END
--ENDIF	
GO
--End table core.SystemSetup

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityStandard', @DESCRIPTION='View the list of quality standards & edit the guidance notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='QualityStandard.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--Begin table dropdown.Project
UPDATE P
SET 
	P.ProjectCode = 'Math',
	P.ProjectAlias = 'Mathematica',
	P.ProjectName = 'Mathematica'
FROM dropdown.Project P
WHERE P.ProjectName = 'TEST'
GO

UPDATE P
SET 
	P.IsActive = 
		CASE
			WHEN CAST(core.GetSystemSetupValueBySystemSetupKey('IsMNH', 1) AS BIT) = 1
			THEN
				CASE 
					WHEN P.ProjectCode = 'Math'
					THEN 0
					ELSE 1
				END
			ELSE
				CASE 
					WHEN P.ProjectCode = 'Math' OR P.ProjectID = 0
					THEN 1
					ELSE 0
				END
		END
FROM dropdown.Project P
GO
--End table dropdown.Project

--Begin table person.PersonProject
IF 'MNH' = 'MATH'
	BEGIN

	TRUNCATE TABLE person.PersonProject
	
	INSERT INTO person.PersonProject
		(PersonID, ProjectID)
	SELECT
		P1.PersonID,
		(SELECT P2.ProjectID FROM dropdown.Project P2 WHERE P2.ProjectCode = 'Math')
	FROM person.Person P1

	UPDATE P1
	SET P1.DefaultProjectID = (SELECT P2.ProjectID FROM dropdown.Project P2 WHERE P2.ProjectCode = 'Math')
	FROM person.Person P1

	END
--ENDIF
GO

--Begin table dropdown.QualityStandard
ALTER TABLE dropdown.QualityStandard DISABLE TRIGGER TR_QualityStandard
GO

UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Review the ANC section of 5 case records from antenatal care clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where any quantitative estimate of hemoglobin is recorded in the ANC section of the record. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 80;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the location where antenatal care is provided. This may be in the maternity ward, or in some other designated location â ask the facility contact if you are not sure.
 If both a functional blood pressure instrument and functional stethoscope are available in this location, record Y. Test the blood pressure instrument and stethoscope to ensure functionality.  If either the blood pressure instrument or the stethoscope are not present, or if they are not functional, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 81;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Review the ANC section of 5 case records from antenatal care clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where blood pressure was recorded in the ANC section of the record. There should be two values (systolic and diastolic blood pressure). Only record the number of case records where both values are recorded.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 82;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records whether a urine proteinuria test was performed.
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility. 
 If respondent reports that urine proteinuria testing is performed at every scheduled antenatal care visit, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 83;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where provider recorded that they used or referred for standard 75gm oral glucose tolerance test for screening of gestational diabetes at first antenatal visit and repeated oral glucose tolerance test at second antenatal visit (24-28 weeks) if negative in first screening
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility..
 If respondent reports that providers always use or refer for standard 75gm oral glucose tolerance test for screening of gestational diabetes at first antenatal care visit and repeat oral glucose tolerance test at second antenatal care visit (24-28 weeks) if negative in first screening, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 84;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where provider noted that they screened or referred for HIV during first antenatal care visit in all cases, and in fourth antenatal care visit in high risk cases. Confirm that an HIV kit was used to screed the client for HIV.
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility.
 If respondent reports that providers always screen or refer for HIV during first antenatal care visit in all cases, and in fourth antenatal care visit in high risk cases, and use an HIV kit to do so, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 85;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where provider recorded that they screened or referred for syphilis in first antenatal care visit in all cases, and in fourth antenatal care visit in high risk cases
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility.
 If respondent reports that providers always screen or refer for syphilis in first antenatal care visit in all cases, and in fourth antenatal care visit in high risk cases, record Y. Otherwise, record N')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 86;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where provider recorded that they screened for malaria 
 Note: Rajasthan is a malaria-endemic area as per the National Vector Borne Disease Guidance
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility.
 If respondent reports that providers always screen for malaria, record Y. Otherwise, record N. 
 Note: Rajasthan is a malaria-endemic area as per the National Vector Borne Disease Guidance')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 87;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review the ANC section of 5 case records from ANC clients. These are clients who visit the facility specifically to receive antenatal care.
 Record the number of case records where provider recorded the clientâs blood group and Rh type during first antenatal care visit
Provider interview
 Interview the head of the facility. If the head of the facility is not a gynecologist, also interview a gynecologist at the facility.
 If respondent reports that providers always establish blood group and Rh type during first antenatal care visit, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 88;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available on a sterile or high-level disinfection tray in the labor room. 
 If misoprostol is available on a sterile or high-level disinfection tray in the labor room, and if oxytocin is placed on the tray no more than 30 minutes prior to a delivery, record Y. If not, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 89;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the presence of a newborn corner in the location where client is admitted 
 If a designated newborn corner is present and clearly labeled in this location, record Y. If not, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 90;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the location where client is admitted 
 If functional items (all items listed below) for newborn care and resuscitation are available in this location, record Y. If one or more items from the list below are missing, record N.
For Baby:
Suction equipment
Mucous extractor
Bag or mask â size 0 or 1
Radiant warmer
Oxygen or ambu bag
Clock with second hand
Shoulder roll
2 towels
Injection Vitamin K (with syringe)

For Health Provider:
Sterile gloves
Plastic apron
Face mask
Shoe cover
Slippers
Caps
Boots
Goggles')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 91;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment and provider in the location where client is admitted
 If a functional radiant warmer is available and provider switches it on 30 min. before childbirth, record Y. Ensure functionality of the radiant warmer by switching it on. If no functional radiant warmer is available or if provider does not switch the radiant warmer on 30 min. before childbirth, record N. 
 If observation of a case is not possible, score this element based on item 5 in Vignette #8.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 92;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider records the clientâs obstetric, medical and surgical history at admission. This should include the clientâs history with pregnancies and births, any major medical issues, and any prior surgeries.
 If the provider takes the clientâs obstetric, medical and surgical history at admission, record Y. If the provider does not take the clientâs obstetric, medical and surgical history at admission, record N. 
Case records
 Review 5 case records from clients being admitted for delivery 
 Record the number of case records where a provider took a clientâs obstetric, medical and surgical history at admission. This should include the clientâs history with pregnancies and births, any major medical issues, and any prior surgeries.
Provider interview
 Interview the head of the facility. If the head of the facility is not available, interview a gynecologist at the facility. 
 If respondent reports that providers take clientsâ obstetric, medical and surgical history, record Y. This should include the clientâs history with pregnancies and births, any major medical issues, and any prior surgeries Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 93;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider assesses gestational age at admission through either last menstrual period or Fundal height or ultrasonography (previous or present if available)
 If the provider does assess the gestational age at admission through either last menstrual period or Fundal height or ultrasonography (previous or present if available), record Y. If the provider does not assess gestational age through either last menstrual period or Fundal height or ultrasonography (previous or present if available), record N.
Case records
 Review 5 case records from clients being admitted for delivery 
 Record the number of case records where a provider recorded the clientâs gestational age and noted that it was assessed through either last menstrual period or Fundal height or ultrasonography (previous or present if available) at admission 
Provider interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility. 
 If respondent reports that providers always assess clientsâ gestational age at admission through either last menstrual period or Fundal height or ultrasonography (previous or present if available), record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 94;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the location where clients awaiting delivery are admitted 
 If functional Doppler or fetoscope or stethoscope is available at point of use, record Y. If not, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 95;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider records fetal heart rate at the time that clients are admitted for delivery. 
 If the provider records fetal heart rate at admission, record Y. If the provider does not record fetal heart rate at admission, record N. 
Case records
 Review 5 case records from clients being admitted for delivery 
 Record the number of case records where a provider recorded the fetal heart rate at admission ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 96;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the location where client is admitted 
 If (1) functional blood pressure instrument, (2) functional stethoscope and (3) functional thermometer are available in the admission area, record Y. If one or more of the instruments are missing, or are not functional, record N. Test all three instruments to ensure functionality. For example, to ensure functionality of the thermometer, check your own temperature. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 97;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider records blood pressure and temperature at when a client is admitted for delivery 
 If the provider records blood pressure (both systolic and diastolic) and temperature at admission, record Y. If the provider does not record blood pressure and temperature at admission, record N. 
Case records
 Review 5 case records from clients being admitted for delivery 
 Record the number of case records where a provider recorded blood pressure (both systolic and diastolic) and temperature')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 98;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider conducts per vaginum examination of women admitted for delivery, only as indicated (4 hourly or based on clinical indication)
 If the provider conducts per vaginum examination only as indicated (4 hourly or based on clinical indication), record Y. If the provider does not conduct examination as indicated (4 hourly or based on clinical indication), record N. 
 If observation of a case is not possible, AND item ADM4.1.1b cannot be observed, score this element based on item 1 in Vignette #1.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of the facility is not a gynecologist, interview a gynecologist at the facility.
 If respondent reports that per vaginum examinations are conducted only as indicated (4 hourly or based on clinical indication), record Y. Otherwise, record .N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 99;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the location where client is admitted for delivery. 
 If soap, running water, antiseptic solution, sterile gauze or  pad is available, record Y.  If doctors do not report washing their hands before and after a delivery, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 100;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider conducting pelvic exams performs hand hygiene (washes hands and wears sterile gloves on both the hands with correct technique)
 If the provider performs hand hygiene (washes hands and wears sterile gloves on both the hands with correct technique), record Y. Otherwise, record N. 
 If observation of a case is not possible, AND item ADM4.2.2b cannot be observed, score this element based on item 2 in Vignette #1.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of the facility is not a gynecologist, interview a gynecologist at the facility. 
 If respondent reports that hand hygiene (washes hands and wears sterile gloves on both the hands with correct technique) is performed for conducting pelvic examinations following infection prevention practices, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 101;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider cleans the perineum with antiseptic swab and discards the soiled swab in yellow container before conducting pelvic examination
 If the provider cleans the perineum with antiseptic swab and discards the soiled swab in yellow container before conducting pelvic examination, record Y. If the provider does not clean the perineum with antiseptic swab and discards the soiled swab in yellow container before conducting pelvic examination, record N. 
 If observation of a case is not possible, AND item ADM4.2.3b cannot be observed, score this element based on item 3 in Vignette #1.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of the facility is not a gynecologist, interview a gynecologist at the facility.
 If respondent reports that the perineum is always cleaned appropriately before conducting PV examinations following infection prevention practices, record Y. Otherwise, record N.   ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 102;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe whether the provider alerts specialist or doctor if liquor is found to be meconium stained during pelvic examination.
 If the provider alerts specialist or doctor if liquor is meconium stained, record Y. If the provider does not alert specialist or doctor if liquor is meconium stained, record N. 
 If observation of a case is not possible, score this element based on item 4 in Vignette #1.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 103;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider records findings of PV examination in clientâs records.
 If the provider records findings of pelvic examination, record Y. If the provider does not record findings of pelvic examination, record N. 
 If observation of a case is not possible, AND item ADM4.2.5b and or or ADM4.2.5c cannot be observed, score this element based on item 5 in Vignette #1. 
Case records
 Review 5 case records from clients admitted for delivery 
 Record the number of case records where a provider recorded findings of pelvic examination(s) conducted at the time of admission.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of the facility is not a gynecologist, interview a gynecologist at the facility.
 If respondent reports that findings of pelvic examinations conducted at the time of delivery are always recorded, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 104;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If partographs are available in labor room, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 105;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During delivery, observe whether the provider initiates Partograph plotting once the cervical dilation is greater than or equal to 4 cms
 If the provider initiates Partograph plotting once the cervical dilation is greater than or equal to 4 cms, record Y. If the provider does not initiate Partograph plotting once the cervical dilation is greater than or equal to 4 cms, record N. 
Case records
 Review 5 case records from clients admitted for delivery. 
 Record the number of case records where a provider initiated Partograph plotting once the cervical dilation was greater than or equal to 4 cms
Provider interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility. 
 If respondent reports that Partograph plotting is always initiated once the cervical dilation is greater than or equal to 4 cms, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 106;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During delivery, observe whether the provider identifies complications, records the diagnosis and makes appropriate adjustments in the birth plan if parameters are not normal
 If the provider identifies complications, records the diagnosis and makes appropriate adjustments in the birth plan if parameters are not normal, record Y. If the provider does not identify complications, record the diagnosis and make appropriate adjustments in the birth plan if parameters are not normal, record N. 
Case records
 Review 5 case records from clients admitted for delivery.
 Record the number of case records where a provider identified complications, recorded the diagnosis and made appropriate adjustments in the birth plan if parameters were not normal
Provider interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility.
 If respondent reports that complications are identified, the diagnosis are recorded, and appropriate adjustments are made in the birth plan if partograph parameters are not normal, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 107;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider encourages and welcomes the presence of birth companion during labor 
 If the provider encourages and welcomes the presence of birth companion during labor, record Y. If the provider does not encourage and welcome the presence of birth companion during labor, record N. 
 If observation of a case is not possible, AND item ADM6.1.1b cannot be observed, score this element based on item 1 in Vignette #2. 
Provider interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility.
 If respondent reports that providers encourage and welcome the presence of birth companion during labor, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 108;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the provisions for privacy available in the labor room  
 If there are provisions for privacy in labor room (curtains  or partition between tables) and non-see through windows, record Y. If either provisions for privacy in the labor room (curtains or partition between tables), or non-see through windows are not present, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 109;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe whether the provider treats pregnant woman and her companion cordially and respectfully 
 If the provider treats pregnant woman and her companion cordially and respectfully, record Y. If the provider does not treat pregnant woman and her companion cordially and respectfully, record N. 
 If observation of a case is not possible, score this element based on item 3  in Vignette #2.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 110;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During delivery, observe whether the provider explains all of the danger signs (listed below)  and important care activities to mother and her companion
Danger Signs for mother:
 Vaginal bleeding 
 Rupture of membranes and delivery not happening for 12 hrs
 Convulsions 
 Severe headache and blurred vision 
 Severe abdominal pain  
 Respiratory difficulty
  Fever
 Foul smelling vaginal discharge
 Reduced or absent fetal movements
Danger signs for baby:
 Fast or difficulty in breathing
 Fever
 Unusually cold
 Stops feeding well
 Less activity than normal
 Whole body becomes yellow
Care activities:
 Instructing mother to bear down only during contractions
 Take breaths during every 2 contractions
 Pass urine every 2 hours
 Maintain hydration
 If the provider explains all of the above danger signs and important care activities to mother and her companion, record Y. If the provider does not explain danger signs and important care activities to mother and her companion, record N. 
 If observation of a case is not possible, AND item ADM6.3.1b cannot be observed, score this element based on item 4 in Vignette #2.
Provider interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility.
 If respondent reports that providers always explain danger signs and important care activities to mother and her companion, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 111;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If antiseptic solution (Betadine or  Savlon) is available, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 112;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If a sterile cord clamp is available, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 113;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If a sterile cutting edge (blade or scissors) is available, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 114;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs an episiotomy only if indicated and uses local anesthesia 
 The following are indications to perform an episiotomy:
  Breech
  Shoulder dystocia
  Forceps
  Vacuum
  Poorly healed 3rd or 4th degree tear
  Fetal distress and prematurity
 If the provider performs an episiotomy only if indicated and uses local anesthesia, record Y. If the provider does not performs an episiotomy only when indicated or does not use local anesthesia, record N. 
 If observation of a case is not possible, AND item ADM7.2.1b cannot be observed, score this element based on item 10-13 in Vignette #3.
Provider interview
 Interview the head of the facility. If the head of facility is not available, interview a gynecologist. 
 If respondent reports that providers always perform an episiotomy only if indicated and always use local anesthesia, record Y. If provider does not perform an episiotomy only when indicated or does not use local anesthesia, record N. 
 The following are indications to perform an episiotomy:
  Breech
  Shoulder dystocia
  Forceps
  Vacuum
  Poorly healed 3rd or 4th degree tear
  Fetal distress and prematurity')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 115;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider allows spontaneous delivery of head by maintaining flexion and giving perineal support; managing cord round the neck; assisting delivery of shoulders and body 
 If the provider allows spontaneous delivery of head by maintaining flexion and giving perineal support; managing cord round the neck; assisting delivery of shoulders and body, record Y. If the provider does not allow spontaneous delivery of head by maintaining flexion and giving perineal support; managing cord round the neck; assisting delivery of shoulders and body, record N. 
 If observation of a case is not possible, AND item ADM7.3.1b cannot be observed, score this element based on items 6-9 in Vignette #3.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility.
 Record Y if respondent reports that providers always allow:
 spontaneous delivery of head by maintaining flexion and giving perineal support; 
 managing cord round the neck; 
 assisting delivery of shoulders and body. 
 If respondent does not allow all of the following listed above, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 116;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether two towels at normal room temperature or pre warmed to room temperature are available at the time of delivery 
 If two towels at normal room temperature or pre warmed to room temperature are available, record Y. If there are less than two towels available, or if towels are not at normal room temperature or pre warmed to room temperature at delivery, record N.
 If observation of a case is not possible, AND item DEL8.1.1b cannot be observed, score this element based on item 1  in Vignette #4.
Provider interview
 Interview a labor room nurse. 
 If respondent reports that two towels at normal room temperature or pre warmed to room temperature are always available at delivery, record Y. If there are less than two towels are available, or if towels not at normal room temperature or pre warmed to room temperature at delivery, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 117;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider delivers the baby on the mother''s abdomen 
 If the provider delivers the baby on the mother''s abdomen, record Y. If the provider does not deliver the baby on mother''s abdomen, record N. 
 If observation of a case is not possible, AND item DEL8.1.2b cannot be observed, score this element based item 2 in Vignette #4.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always delivers the baby on the mother''s abdomen, record Y. If the provider does not deliver the baby on the mother''s abdomen, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 118;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider dries the baby immediately and wraps in second warm towel if breathing is normal after delivery 
 To assess whether breathing is normal, put your finger near the babyâs nose and mouth to determine whether the baby is breathing normally 
 If the provider dries the baby immediately and wraps in second warm towel if breathing is normal after delivery, record Y. If the provider does not dry the baby immediately or wrap it in second warm towel if breathing is normal after delivery, record N. 
 If observation of a case is not possible, AND item DEL8.2.1b cannot be observed, score this element based on items 4 and 5 in Vignette #4.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always dries the baby immediately and wraps in second warm towel if breathing is normal after delivery, record Y. If the provider does not dry the baby immediately or wrap it in second warm towel if breathing is normal after delivery, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 119;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs delayed cord clamping and cutting (waiting at least 1 minute after birth) unless medical indication otherwise (i.e. if the baby is not breathing)
 If the provider performs delayed cord clamping and cutting (waiting at least 1 minute after birth) unless medical indication otherwise, record Y. If the provider does not perform delayed cord clamping and cutting and there is no medical reason for this, record N. 
  If observation of a case is not possible, AND item DEL8.3.1b cannot be observed, score this element based on item 6 in Vignette #4.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider performs delayed cord clamping and cutting (waiting at least 1 minute after birth) unless medical indication otherwise, record Y. If the provider does not perform delayed cord clamping and cutting and there is no medical reason for this, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 120;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients who delivered at the facility. 
 Look at the recorded time of birth and recorded time of first breastfeeding. Record the number of case records where a provider initiated breastfeeding within one hour of birth
Providers interview
 Interview a labor room nurse. 
 If respondent reports that the provider always initiates breast feeding within one hour of birth, record Y. If the provider does not initiate breast feeding within one hour of birth, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 121;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients who delivered at the facility.
 Record the number of case records where a provider recorded having immediately assessed the newborn for any congenital anomalies including neural tube defects, Down syndrome, cleft lip and palate, club foot, congenital cataracts, and congenital deafness.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always immediately assesses the newborn for any congenital anomalies, record Y. If the provider does not immediately assess the newborn for any congenital anomalies, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 122;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always seeks specialist care at a higher-level facility for neural tube defects, Down syndrome, cleft lip and palate, club foot, congenital cataracts, and congenital deafness, , record Y. If the provider does not ensure specialist care if required, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 123;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If a baby weighing scale is available and functional in the labor room, record Y. Test the scale to ensure functionality. If a scale is not available, or is not functional, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 124;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room and operating theater.  
 If Vitamin K injection is available in the labor room and operating theater, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 125;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Review 5 case records from clients who delivered at the facility. 
 Record the number of case records where a provider recorded the babyâs weight and recorded having administered Vitamin K')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 126;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider palpates mother''s abdomen post-delivery to rule out a second baby
 If the provider palpates mother''s abdomen post-delivery to rule out a second baby, record Y. If the provider does not palpate mother''s abdomen post-delivery to rule out a second baby, record N. 
 If observation of a case is not possible, AND item DEL9.1.1b cannot be observed, score this element based on item 1 in Vignette #5.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always palpates the mother''s abdomen post-delivery to rule out a second baby, record Y. If the provider does not palpate the mother''s abdomen to rule out a second baby, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 127;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider administers uterotonic post-delivery. Preferred is Inj. Oxytocin 10 I.U. IM or IV within one minute of delivery of baby. If oxytocin is not available, observe whether Misoprostol 600 micrograms is administered. 
 If the provider administers uterotonic post-delivery as described above, record Y. If the provider does not administer uterotonic post-delivery as described above, record N. 
 If observation of a case is not possible, AND item DEL9.1.2b cannot be observed, score this element based on item 2 in Vignette #5.
Case records
 Review 5 case records from clients who delivered at the facility. 
 Record the number of case records where a provider recorded having administered a uterotonic (either Inj. Oxytocin 10 I.U. IM or IV within one minute of delivery of baby or Misoprostol 600 micrograms if oxytocin was not available.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always administers a uterotonic (either Inj. Oxytocin 10 I.U. IM or IV within one minute of delivery of baby orMisoprostol 600 micrograms if oxytocin is not available), record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 128;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs controlled cord traction (CCT) during contraction
 If the provider performs controlled cord traction (CCT) during contraction, record Y. If the provider does not perform controlled cord traction (CCT) during contraction, record N. 
 If observation of a case is not possible, AND item DEL9.1.3b cannot be observed, score this element based on item 3 in Vignette #5.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider performs controlled cord traction (CCT) during contraction, record Y. If the provider does not perform controlled cord traction (CCT) during contraction, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 129;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs uterine massage if the uterus is atonic or if there is excessive bleeding.
 If the provider performs uterine massage in cases of atonic uterus or excessive bleeding, record Y. If the provider does not provider perform uterine massage, record N. 
 If observation of a case is not possible, AND item DEL9.1.4b cannot be observed, score this element based on item 4 in Vignette #5.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always performs uterine massage, record Y. If the provider does not perform uterine massage, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 130;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider checks placenta and membranes for completeness before discarding
 If the provider checks placenta and membranes for completeness before discarding, record Y. If the provider does not check placenta and membranes for completeness before discarding, record N. 
 If observation of a case is not possible, AND item DEL9.1.5b cannot be observed, score this element based on item 5 in Vignette #5.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always checks placenta and membranes for completeness before discarding, record Y. If the provider does not check placenta and membranes for completeness before discarding, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 131;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider assesses uterine tone and bleeding per vaginum every 15 minutes in the first hour and every 30 minutes in the second hour.
 If the provider assesses uterine tone and bleeding per vaginum every 15 minutes in the first hour and every 30 minutes in the second hour, record Y. If the provider does not assess uterine tone and bleeding per vaginum regularly, record N. 
 If observation of a case is not possible, AND item DEL10.1.1b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always assesses uterine tone and bleeding per vaginum regularly in cases of postpartum hemmorhage, record Y. If the provider does not assess uterine tone and bleeding per vaginum regularly, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 132;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always identifies shock by signs and symptoms (pulse greater than 110 per minute, systolic BP less than 90 mmHg, cold clammy skin, respiratory rate greater than 30 per minute, altered sensorium and scanty urine output less than 30 ml per hour), record Y. If the provider does not identify shock by signs and symptoms, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 133;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room and operating theater.  
 If availability of wide bore cannulas (No. 14 or 16), intravenous infusion sets and fluids and containers for collection of blood for hemoglobin, blood grouping and cross matching is ensured in both labor room and operating theater, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 134;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe whether the provider shouts for help, follows ABC approach, monitors vitals, elevates the foot end and keeps the woman warm
 If the provider does all of the following, record Y. Otherwise, record N. 
  Shouts for help when needed
  Follows ABC approach (assesses airway, breathing, and circulation)
  Monitors vitals
  Elevates the foot end and keeps the woman warm
 If observation of a case is not possible, score this element based on Vignette #6.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 135;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe whether the provider starts intravenous infusions, collects blood for hemoglobin and grouping and cross matching, catheterizes the bladder and monitors I or O, gives oxygen at the rate of 6-8 liters per minute 
 If the provider does all of the following, record Y. Otherwise, record N. 
  starts intravenous infusions
  Collects blood for hemoglobin and grouping and cross matching
  Catheterizes the bladder and monitors I or O
 Gives oxygen at the rate of 6-8 liters per minuteIf observation of a case is not possible, score this element based on Vignette #6.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 136;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider Identifies cause specific postpartum hemorrhage and addresses the 4 Tâs (tone, trauma, tissue and thromboembolic disorders). 
 If the provider Identifies cause specific postpartum hemorrhage, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.3.4b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always Identifies cause specific postpartum hemorrhage, record Y. If the provider does not identify cause specific postpartum hemorrhage, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 137;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases of atonic postpartum hemorrhage, observe whether the provider initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute
 If the provider initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.4.1b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that in cases of atonic postpartum hemorrhage, the provider always initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record Y. If the provider does not initiate 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 138;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider continues uterine massage for atonic PPH
 If the provider continues uterine massage, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.4.2b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider continues uterine massage, record Y. If the provider does not continue uterine massage, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 139;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider gives other uterotonics as recommended if uterus is still relaxed 
 If the provider gives other uterotonics as recommended if uterus is still relaxed, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.4.3b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always gives other uterotonics as recommended if uterus is still relaxed, record Y. If the provider does not give other uterotonics as recommended if uterus is still relaxed, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 140;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade if uterus is still relaxed
 If the provider performs mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade if uterus is still relaxed, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.4.4b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the always provider performs mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade, if uterus is still relaxed, record Y. If the provider does not perform mechanical compression in the form of bimanual uterine compression or external aortic compression or balloon tamponade if uterus is still relaxed, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 141;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider refers to higher centers while continuing mechanical compression, if uterus is still relaxed and if the condition appears to be beyond the scope of the facility
 If the provider refers to higher centers while continuing mechanical compression if uterus is still relaxed and the condition appears to be beyond the scope of the facility, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.4.5b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always refers to higher centers while continuing mechanical compression if uterus is still relaxed, record Y. If the provider does not refer to higher centers while continuing mechanical compression if uterus is still relaxed, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 142;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider identifies retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete 
 If the provider identifies retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.5.1b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always identifies retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete, record Y. If the provider does not identify retained placenta if placenta is not delivered within 30 minutes of delivery of baby or the delivered placenta is not complete, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 143;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases of postpartum hemorrhage due to retained placenta or placental bits, observe whether the provider initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute
 If the provider initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.5.2b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that in cases of postpartum hemorrhage due to retained placenta or placental bits, the provider always initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record Y. If the provider does not initiates 20 IU oxytocin drip in 1000 ml of ringer lactate or normal saline at the rate of 40-60 drops per minute, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 144;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients with Post-Partum Hemorrhage (PPH) 
 Record the number of case records where a provider recorded that they referred client to higher center if unable to manage their postpartum hemorrhage
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always refers client to higher center if unable to manage a clientâs postpartum hemorrhage, record Y. If the provider does not refer client to higher center if unable to manage, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 145;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs manual removal of placenta (MRP) if necessary
 If the provider performs manual removal of placenta (MRP), record Y. Otherwise, record N.  
 If observation of a case is not possible, AND item DEL10.5.4b cannot be observed, score this element based on Vignette #6.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always performs manual removal of placenta (MRP) when necessary, record Y. If the provider does not perform manual removal of placenta (MRP), record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 146;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If dipsticks for proteinuria testing are available in the labor room, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 147;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Review 5 case records from clients admitted with Pre-eclampsia or Eclampsia (PE or E)
 Record the number of case records where a provider recorded blood pressure (both systolic and diastolic) at admission')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 148;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In confirmed or suspected cases of severe pre-eclampsia or eclampsia, observe whether the provider identifies danger signs or presence of convulsions
 If the provider identifies danger signs (listed below) or presence of convulsions, record Y. Otherwise, record N. 
 Headache
 Blurring of vision
 Epigastric pain
 Generalized edema
 Decreased urine output
 If observation of a case is not possible, AND item DEL11.1.3b cannot be observed, score this element based on item 1 in Vignette #7.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always identifies danger signs or presence of convulsions, record Y. If the provider does not identify danger signs or presence of convulsions, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 149;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If MgSO4 (at least 20 ampoules) is available in the labor room, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 150;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients admitted with Pre-eclampsia or Eclampsia (PE or E)
 Record the number of case records where a provider appropriately administered Inj. MgSO4
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always appropriately administers Inj. MgSO4, record Y. If the provider does not appropriately administers Inj. MgSO4, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 151;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If antihypertensives  (both nifedipine and labetalol) are available, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 152;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients admitted with Pre-eclampsia or Eclampsia (PE or E)
 Record the number of case records where a provider facilitated prescription of anti-hypertensives
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always facilitates prescription of anti-hypertensives, record Y. If the provider does not facilitate prescription of anti-hypertensives, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 153;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients admitted with Pre-eclampsia or Eclampsia (PE or E)
 Record the number of case records where a provider ensured specialist attention (from a physician or pediatrician) for care of mother and newborn in cases of uncontrolled hypertension, convulsions, or fetal distress.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist, interview a gynecologist at the facility
 If respondent reports that the provider always ensures specialist attention for care of mother and newborn when needed, record Y. If the provider does not ensure specialist attention for care of mother and newborn, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 154;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe whether the provider performs nursing care for patients with severe pre-eclampsia or eclampsia.
 If the provider performs nursing care (including at least one item described below), record Y. Otherwise, record N. 
 Start IV fluids at 75 mL or hour
 Maintain strict fluid balance chart, monitor mount of fluids administered, and monitor urine output
 Catheterize bladder to monitor urine output and proteinuria
 If urine output is less than 100 mL or 4 hours, withhold next dose of MgSO4 and monitor for development of pulmonary edema')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 155;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If suction equipment or mucus extractor and shoulder roll are available, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 156;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' In cases where newborn resuscitation is required, observe whether the provider considers endotracheal intubation and tracheal suctioning if available. If not available, observe whether the provider performs oro-pharyngeal suction and proceeds with next steps (in item DEL12.1.3a). 
 If the provider considers endotracheal intubation and tracheal suctioning if available, or if not available, the provider performs oro-pharyngeal suction and proceeds with next steps, record Y. Otherwise, record N. 
 If observation of a case is not possible, score this element based on item 3  in Vignette #8')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 157;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases where newborn resuscitation is required, observe whether the provider performs the following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby, immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing. 
 If the provider performs the following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby, immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
 If observation of a case is not possible, AND item DEL12.1.3b cannot be observed, score this element based on items 1, 2, and 5 of Vignette #8
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that in cases where newborn resuscitation is needed, the provider always performs the following steps within first 30 seconds on motherâs abdomen: Suction if indicated; dries the baby, immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing, record Y. If the provider does not perform the following steps within first 30 seconds on mothers abdomen: Suction if indicated; dries the baby, immediate clamping and cutting of cord; and shifting to radiant warmer if baby still not breathing, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 158;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases where newborn resuscitation is needed, bserve whether the provider performs the following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR)
 If the provider performs the following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR), record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that in cases where newborn resuscitation is needed, the provider always performs the following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR), record Y. If the provider does not perform the following steps within first 30 seconds under radiant warmer: Positioning, Suctioning, Stimulation, Repositioning (PSSR), record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 159;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If a functional ambu bag with mask (size 0 and 1) is available, record Y. Ensure the functionality of the ambu bag by pressing the bag. If an ambu bag is not available or is not functional, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 160;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases where newborn resuscitation is needed, observe whether the provider initiates bag and mask ventilation for 30 seconds if baby still not breathing
 If the provider initiates bag and mask ventilation for 30 seconds if baby still not breathing, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that in cases where newborn resuscitation is needed, the provider always initiates bag and mask ventilation for 30 seconds if baby still not breathing, record Y. If the provider does not initiate bag and mask ventilation for 30 seconds if baby still not breathing, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 161;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If a functional oxygen cylinder (with wrench) and new born mask is available, record Y.Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 162;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider assesses breathing, if baby still not breathing, continues bag and mask ventilation; starts oxygen
 If the provider assesses breathing, if baby still not breathing, continues bag and mask ventilation; starts oxygen, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider assesses breathing, if baby still not breathing, continues bag and mask ventilation; starts oxygen, record Y. If the provider does not assess breathing, if baby still not breathing, continue bag and mask ventilation; does not start oxygen, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 163;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider checks heart rate or cord pulsations immediately after birth.
 If the provider checks heart rate or cord pulsations immediately after birth, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always checks heart rate or cord pulsations, record Y. If the provider does not check heart rate or cord pulsations, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 164;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients whose newborns did not cry immediately after birth 
 Record the number of case records where a provider recorded that they called for advance help or arranged referral in cases of birth asphyxia
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always calls for advance help or arranges referral when needed, record Y. If the provider does not call for advance help or arrange referral, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 165;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases where newborn is not responding to basic resuscitation when chest is rising and heart rate is less than 60 beats per minute, observe whether the provider performs chest compressions at the rate of 3 compressions to 1 breath till the heart rate is greater than 60 beats or minute
 If the provider performs chest compressions at the rate of 3 compressions to 1 breath till the heart rate is greater than 60 beats or minute, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that in cases where newborn is not responding to basic resuscitation when chest is rising and heart rate is less than 60 beats per minute, the provider always performs chest compressions at the rate of 3 compressions to 1 breath till the heart rate is greater than 60 beats or minute, record Y. If the provider does not perform chest compressions at the rate of 3 compressions to 1 breath till the heart rate is greater than 60 beats or minute, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 166;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider administers epinephrine (1:10000), 0.1 - 0.3 ml or kg intravenous if heart rate persists to be undetectable or less than 60 beats or  minute
 If the provider administers epinephrine (1:10000), 0.1 - 0.3 ml or kg intravenous if heart rate persists to be undetectable or less than 60 beats or  minute, record Y. Otherwise, record N. If no case of newborn resuscitation was available for observation, record NA. 
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility.
 If respondent reports that the provider always administers epinephrine (1:10000), 0.1 - 0.3 ml or kg intravenous if heart rate persists to be undetectable or less than 60 beats or  minute, record Y. If the provider does not administer epinephrine (1:10000), 0.1 - 0.3 ml or kg intravenous if heart rate persists to be undetectable or less than 60 beats or  minute, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 167;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients with newborns less than 1800 gm 
 Record the number of case records where a provider facilitated specialist care in newborn less than 1800 gm (referred to FBNC or seen by pediatrician)
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility.
 If respondent reports that the provider always facilitates specialist care in newborn less than 1800 gm (refer to FBNC or seen by pediatrician), record Y. If the provider does not facilitate specialist care in newborn less than 1800 gm (refer to FBNC or seen by pediatrician), record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 168;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients with newborns less than 1800 gm 
 Record the number of case records where a provider facilitated assisted feeding if the baby was not sucking.
Provider interview
 Interview a labor room nurse. 
 If respondent reports that the provider always facilitates assisted feeding whenever required, record Y. If the provider does not facilitate assisted feeding whenever required, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 169;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from clients with newborns with newborns less than 1800 gm 
 Record the number of case records where a provider recorded having facilitated thermal management including kangaroo mother care
Provider interview
 Interview a labor room nurse. 
 If respondent reports that the provider always facilitates thermal management including kangaroo mother care when newborn weighs less than  1800 gm, record Y. If the provider does not facilitate thermal management including kangaroo mother care when needed, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 170;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If facilities for sterilization(listed below) of instruments are available, record Y. Otherwise, record N. 
 Facilities for sterilization:
 Steam autoclave
 High level disinfectants: glutaraldehyde 2 percent, ethylene oxide
 Intermediate level disinfectants: alcohols, chlorine compounds, hydrogen peroxide, chlorhexidine, glutaraldehyde (short-term exposure)
 Low level disinfectants: benzalkonium chloride, some soaps.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 171;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the instruments are sterilized after each use in the labor room
 If the instruments are sterilized after each use, record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
Provider interview
 Interview a labor room nurse 
 If respondent reports that the instruments are always sterilized after each use, record Y. If the instruments are not sterilized after each use, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 172;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the delivery environment such as labor table, contaminated surfaces and floors are cleaned after a delivery
 If the delivery environment such as labor table, contaminated surfaces and floors are cleaned after each delivery, record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
Case records
 Review 5 case records from clients after delivery
 Record the number of case records where the delivery environment such as labor table, contaminated surfaces and floors were cleaned after each delivery
Provider interview
 Interview facility labor room nurse.
 If respondent reports that the delivery environment such as labor table, contaminated surfaces and floors are cleaned after each delivery, record Y. If the delivery environment such as labor table, contaminated surfaces and floors are not cleaned after each delivery, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 173;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Observe the equipment available in the labor room  
 If color coded bags for disposal of biomedical waste are available, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 174;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the biomedical waste is segregated and disposed of at the source, as per the 2016 bio-medical waste guidelines
 If the biomedical waste is segregated and disposed of as per the guidelines, record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
Provider interview
 Interview a labor room nurse. 
 If respondent reports that biomedical waste is always segregated and disposed of as per the guidelines, record Y. If biomedical waste is not segregated and disposed of as per the guidelines, record N. 
Physical verification
 Observe the area in the labor room where biomedical waste is disposed of.
 If biomedical waste is segregated and disposed of as per the guidelines, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 175;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider performs hand hygiene before and after each procedure, and sterile gloves are worn during delivery and internal examination
 If the provider performs hand hygiene before and after each procedure (including proper handwashing techniques with soap or alcoholic rub), and sterile gloves are worn during delivery and internal examination, record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
Provider interview
 Interview a gynecologist or labor room nurse. 
 If respondent reports that the provider always performs hand hygiene before and after each procedure, and sterile gloves are always worn during delivery and internal examination, record Y. If the provider does not perform hand hygiene before and after each procedure, or sterile gloves are not worn during delivery and internal examination, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 176;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During postpartum exam, observe whether the provider conducts mother''s examination, including all of the following:
 Conducts breast examinations
 Examines the perineum for inflammation, status of episiotomy or tears, lochia for colour, amount, consistency and odour during second and third postpartum visits
 Checks calf tenderness, redness or swelling during first three postpartum visit
 Checks abdomen for involution of uterus, tenderness or distension
 If the provider conducts mother''s examination including all of the exams listed above, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.1.1b and or or PNC15.1.1c cannot be observed, score this element based items 1-6 in Vignette #9.
Case records
 Review 5 case records from clients at postpartum visits.
 Record the number of case records where the provider conducted all of the following during mother''s examination: breast, perineum for inflammation; status of episiotomy or tear suture; lochia; calf tenderness or redness or swelling; abdomen for involution of uterus, tenderness or distension
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always assesses the following when they conduct mother''s examination during postpartum visit: breast, perineum for inflammation; status of episiotomy or tear suture; lochia; calf tenderness or redness or swelling; abdomen for involution of uterus, tenderness or distension, record Y. If the provider does not conduct mother''s examination: breast, perineum for inflammation; status of episiotomy or tear suture; lochia; calf tenderness or redness or swelling; abdomen for involution of uterus, tenderness or distension, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 177;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During postpartum visit, observe whether the provider conducts newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump 
 If the provider conducts newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.1.2b and or or PNC15.1.2c cannot be observed, score this element based items 7-12 in Vignette #9.
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always assesses the following during newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump, record Y. If the provider does not conduct newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump, record N. 
Case records
 Review 5 case records from clients at postpartum visits
 Record the number of case records where the provider conducted all of the following during newborn''s examination: assesses feeding of baby; checks weight, temperature, respiration, color of skin and cord stump')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 178;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM(' Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always checks mother''s history related to maternal infection, record Y. If the provider does not check mother''s history related to maternal infection, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 179;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider checks mother''s temperature in cases of confirmed or suspected sepsis
 If the provider checks mother''s temperature, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.2.2b and or or PNC15.2.2c cannot be observed, score this element based item 14 in Vignette #9.
Case records
 Review 5 case records from clients admitted for confirmed or suspected sepsis
 Record the number of case records where the mother''s temperature is noted
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always checks mother''s temperature in cases of confirmed or suspected sepsis, record Y. If the provider does not check mother''s temperature, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 180;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases of sepsis, observe whether the provider gives correct regimen of antibiotics
 If the provider gives correct regimen of antibiotics (in line with the hospitalâs antibiotics policy), record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
 If observation of a case is not possible, AND items PNC15.2.3b and or or 15.2.3c cannot be observed, score this element based item 14 in Vignette #9.
Case records
 Review 5 case records from clients admitted for sepsis
 Record the number of case records where the provider gave correct regimen of antibiotics
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always gives correct regimen of antibiotics, record Y. If the provider does not gives correct regimen of antibiotics, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 181;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 Observe whether the provider checks babyâs temperature and looks for other signs of infections.
 If the provider checks baby''s temperature and looks for other signs of infections, record Y. Otherwise, record N. If no relevant case was available for observation, record NA.
  If observation of a case is not possible, AND items PNC15.2.4b and or or PNC15.2.4c cannot be observed, score this element based item 15 in Vignette #9.
Case records
 Review 5 case records from clients whose babies have sepsis
 Record the number of case records where the provider recorded having checked baby''s temperature and looked for other signs of infections
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility.
 If respondent reports that the provider always checks baby''s temperature and looks for other signs of infections, record Y. If the provider does not checks baby''s temperature or look for other signs of infections, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 182;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases of neonatal sepsis, observe whether the provider gives correct regime of antibiotics or refers for specialist care
 If the provider gives correct regime of antibiotics or refers for specialist care, record Y. Otherwise, record N. If no relevant case was available for observation, record NA. 
 If observation of a case is not possible, AND items PNC15.2.5b and or or PNC15.2.5c cannot be observed, score this element based items 14 and 15 in Vignette #9.
Case records
 Review 5 case records from clients whose babies have sepsis
 Record the number of case records where the provider gave correct regime of antibiotics or referred for specialist care
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always gives correct regime of antibiotics or refers for specialist care, record Y. If the provider does not give correct regime of antibiotics and does not refer for specialist care, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 183;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 In cases of potential postpartum depression, observe whether the provider provides emotional support and refers woman to specialist psychiatric care
 If the provider provides emotional support and refers woman to specialist care, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.3.1b and or or PNC15.3.1c cannot be observed, score this element based item 16 in Vignette #9.
Case records
 Review 5 case records from clients at postpartum visits with suspected or confirmed postpartum depression
 Record the number of case records where the client had suspected postpartum depression and the provider provided emotional support and referred woman to specialist care
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that in cases of suspected postpartum depression, the provider always provides emotional support and refers woman to specialist care, record Y. If the provider does not provide emotional support or refer woman to specialist care, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 184;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During postpartum visit, observe whether the provider provides counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding 
 If the provider provides counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.4.1b and or or PNC15.4.1c cannot be observed, score this element based item 17 in Vignette #9.
Case records
 Review 5 case records from clients at postpartum visits
 Record the number of case records where the provider recorded having provided counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that at postpartum visits, the provider always provides counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding, record Y. If the provider does not provide counselling and assistance on the importance of exclusive breast feeding and techniques of breast feeding, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 185;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During postpartum visit, observe whether the provider counsels  on all of the following: 
 Counsels on danger signs of mother and baby 
 Counsels on exclusive and on demand breast feeding to mother 
 Counsels on the importance of maintaining hygiene especially hand hygiene 
 Counsels on importance of adequate nutrition for mother
 Counsels on return of fertility and healthy timing and spacing of pregnancy
 Counsels on importance of complete immunization
 Counsels on baby''s growth monitoring
 Counsels on postnatal exercises
 If the provider counsels mother on all items listed above, record Y. Otherwise, record N. 
 If observation of a case is not possible, AND items PNC15.5.1b and or or PNC15.5.1c cannot be observed, score this element based item 18 in Vignette #9.
Case records
 Review 5 case records from clients at postpartum visits
 Record the number of case records where the provider recorded having counselled on return of fertility and healthy timing and spacing of pregnancy - Counselled on postpartum family planning to mother at discharge
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist or labor room nurse at the facility
 If respondent reports that the provider always counsels on return of fertility and healthy timing and spacing of pregnancy - counsels on postpartum family planning to mother at discharge, record Y. If the provider does not counsel on return of fertility and healthy timing and spacing of pregnancy â does not counsel on postpartum family planning to mother at discharge, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 186;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from C-section clients
 Record the number of case records where the provider ensured that C-section cases are classified as per the modified Robson''s criteria and rates of different categories are monitored in facility
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist at the facility.
 If respondent reports that the facility ensures that all C-section cases are classified as per the modified Robson''s criteria and rates of different categories are monitored in facility, record Y. If the provider does not ensure that all C-section cases are classified as per the modified Robson''s criteria and rates of different categories are monitored in facility, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 187;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from C-section clients
 Record the number of case records where the provider reviewed C-section cases through a clinical audit once every quarter in facility
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist at the facility
 If respondent reports that C-section cases are reviewed through a clinical audit once every quarter in facility, record Y. If the respondent reports that C-section cases are not reviewed through a clinical audit once every quarter in facility, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 188;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Case records
 Review 5 case records from C-section clients
 Record the number of case records where the provider ensured that rate of complications of C-sections are periodically monitored in facility
Provider interview
 Interview the head of the facility if he or she is a gynecologist. If the head of facility is not a gynecologist or is not available, interview a gynecologist at the facility
 If respondent ensures that rate of complications of C-sections are periodically monitored in facility, record Y. If the respondent does not ensure that rate of complications of C-sections are periodically monitored in facility, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 189;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If a policy document defining the facilityâs scope of services exists, including license for relevant services (such as MTP license or Prenatal Diagnostic Techniques license), record Y. Otherwise, record N.  
Implementation
Ask gynecologist and head nurse if they are aware of a policy document defining the facilityâs scope of services. If yes, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 243;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy document defining the facilityâs scope of services specifies that the scope of services should be prominently displayed outside the facility, record Y. If not, or if the policy does not exist, record N. Implementation
Ask gynecologist and head nurse to show you the list of clearly defined services being provided. If staff can show you the list of defined services prominently displayed outside the facility, in both English and the local language, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 244;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If staff training records indicate that new staff are routinely oriented to the scope of services offered at the facility, record Y. Otherwise, record N. 
Implementation
Ask head nurse if they can list the scope of services available at the facility. If they all can, record Y. Otherwise, record N. The reception staff who are interviewed should be different from the key point of contact guiding the assessment.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 245;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 Policies and SOPs on patient registration and admission exist, and cover the following:
 Instructions regarding outpatient registration process
 The establishment of a uniform registration process and maintenance of records of all patients coming to the hospital
 Provision of registration for inpatient department if it matches the scope of services provided
 A specific mechanism for admission, including registration or admission of emergency patients
 A standard registration form exists for registering all patients
 A standard admission form exists for admitting patients, and the form includes consent
Implementation
Ask facility head and gynecologist if they are aware of documented policies and SOP on patient registration and admission. If yes, record Y. Otherwise, record N. The registration and admission staff who are interviewed should be different from the key point of contact guiding the assessment.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 246;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 Policy and SOP for patient transfer and referral-out to another organization exist, and cover the following:
- Conditions under which the facility can refer the patient out
- Provisions for ensuring that the patient is shifted only after first aid is provided and the patient is stabilized
 A standardized transfer-out form is available for documenting patient transfers
A consent form is available for ensuring that patients consent to transfers or referrals out
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 Ask gynecologist or labor room chief nurse if they are aware of policies and SOPs for patient transfer and referral-out. All staff should be aware of policies and SOPs.
A transfer-out register or record exists in the labor room, and it correctly documents the 5 most recent transfers out.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 247;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 A policy and SOP exists describing initial assessment of patients being admitted to the labor room. 
 A standard form exists for completing the initial assessment in the labor room, which includes the following:
 Weight and height
 Blood pressure
 Months of pregnancy
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 Observe equipment in the labor room. The labor room should have a functional BP apparatus, thermometer, screen, adequate light, beds with stirrups, oxygen tank with face mask, wrench, and regulator, emergency drugs tray, and surgical tray.
 Ask gynecologist and head nurse if they are aware of policies and SOPs for the initial assessment of patients being admitted to the labor room. All staff should be aware of policies and SOPs.
 Review 5 randomly selected case records from the labor room. All case records should document an initial assessment upon the patientâs admission to the labor room, and the initial assessment should include the following:
 Weight and height
 Blood pressure
 Months of pregnancy')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 248;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policy and SOPs for conducting initial assessments of patients in the labor room specifies that a doctor or nurse should conduct the initial assessment, record Y. Otherwise, record N.
Implementation
Review the same 5 randomly selected case records from Item ACC.3a:Initial2. If all 5 records show that the initial assessment was conducted and signed by a doctor or nurse, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 249;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policy and SOPs for conducting initial assessments of patients in the labor room specifies that initial assessment should be documented within 24 hours, record Y. Otherwise, record N.
Implementation
Review the same 5 randomly selected case records from Item ACC.3a:Initial2. If all 5 records show that the initial assessment was recorded within 24 hours of the patientâs arrival in the labor room, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 250;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility-wide policies and procedures (such as inpatient admission or outpatient registration policies or forms) state that a single qualified individual is identified as responsible for the care of each patient, record Y. Otherwise, record N.
Implementation
Ask gynecologist and head nurse whether facility-wide policies or procedures ensure that a single, qualified individual is identified for care for each patient, and that this individual coordinates the care for the patient in all the settings within the organization. If all say yes, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 251;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies and SOPs for the labor room indicate that women in labor should be re-assessed at fixed intervals (which is usually every 4 hours), record Y. Otherwise, record N.
Implementation
Ask gynecologist and head nurse what the procedure is for re-assessing women in labor. If the procedure described by staff is in line with the written procedure, or if there is no written procedure but labor room staff report following a consistent protocol, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 252;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If labor room policies and SOPs indicate that the nurse or doctor directly involved in a patientâs care is in charge of conducting reassessments, record Y. Otherwise, record N.
Implementation
Ask gynecologist and head nurse to describe labor room policies and SOPs. If staff state that the policies and SOPs state that nurses or doctors directly involved in patient care are supposed to conduct reassessments, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 253;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If labor room policies and SOPs for patient reassessment include instructions for developing plans for any additional treatment or discharge, record Y. Otherwise, record N.
Implementation
Ask gynecologist and head nurse to describe labor room policies and SOPs for patient reassessment. If all staff state that the policies and SOPs include instructions for developing plans for any additional treatment or discharge, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 254;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs lab manual states that essential laboratory services for maternal health care (HIV tests, hemoglobin tests, and blood sugar tests) must be available in-house or through a contracted external laboratory, record Y. Otherwise, record N.
Implementation
If the facility offers in-house laboratory services, or has a contract with an external laboratory, for HIV tests, hemoglobin tests, and blood sugar tests, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 255;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 A policy or lab manual describing collection, identification, handling, safe transportation, processing, and disposal of specimens exists at the facility
 The following laboratory-related documents are also present:
 Lab safety manual
 Critical result intimation book (for communicating laboratory results which require immediate attention by doctor or nurse)
 External quality register
 Internal quality register
 Refrigerator temperature register
 Quality indicator register
 List of hazardous material
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 The laboratory has the necessary equipment and supplies to conduct HIV tests, hemoglobin tests, and urine tests (if the facility contracts these tests to an external laboratory, skip this.)
 Disposal of specimens in the laboratory conforms to biomedical waste handling rules as laid out in the facilityâs infection control manual.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 256;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the lab manual covers all of the issues listed below, record Y. Otherwise, record N.
 All laboratory results are communicated to the relevant care provider within 24 hours of receipt of results.
 Critical results (those result values which require immediate attention by the doctor or nurse) are communicated to the relevant care provider immediately.
 If the relevant care provider is not reachable, the result is shared with the Medical Officer on duty.
 If the patient has been admitted, the Ward Nurse is informed of critical results.
 The list of values considered as critical should be displayed in prominent locations in the laboratory.
Implementation
Review 5 randomly selected entries in the critical result intimation book. If all entries indicate that critical results were communicated to the relevant care provider(s) immediately, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 257;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the lab manual specifies that lab personnel must be trained in safe practices and provided with personal protective equipment, record Y. Otherwise, record N.
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
Records from the most recent training of laboratory staff indicate that staff have been informed of laboratory SOPs covered in the lab manual, including safety procedures.
Personal protective equipment (gloves, gowns, medical masks, eye protection) is available in the laboratory.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 258;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If diagnostic imaging policies and SOPs state that imaging services must comply with legal or other requirements, including procedures for reviewing and updating the facilitiesâ compliance with these requirements, record Y. Otherwise, record N.
Implementation
If appropriate licenses and other documentation of legal compliance for imaging units (including a notice about not conducting sex determination tests) are displayed in their respective areas, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 259;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the diagnostic imaging policies and SOPs specify that ultrasound services must be available for pregnant patients, record Y. Otherwise, record N.
Implementation
Observe ultrasound equipment in the maternity ward or labor room. If ultrasound is available and functional, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 260;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the diagnostic imaging policy or SOPs cover all of the issues listed below, record Y. Otherwise, record N.
 All imaging results are communicated to the relevant care provider within 24 hours of receipt of results.
 Critical results (those results which require immediate attention by the doctor or nurse) are communicated to the relevant care provider immediately.
 If the relevant care provider is not reachable, the result is shared with the Medical Officer on duty.
If the patient has been admitted, the Ward Nurse is informed of critical results.
Implementation
Review 5 randomly selected entries in the critical result intimation book. If all entries indicate that critical results were communicated to the relevant care provider(s) immediately, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 261;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 A policy describing processes for the discharge of all patients, including medico-legal cases and patients leaving against medical advice, exists at the facility.
 A standardized discharge form exists.
 A separate discharge form for DAMA or LAMA patients exists.
Implementation
Review 5 randomly selected case records of patients who were discharged from the facility. If all 5 records reflect that the facilityâs discharge processes were followed, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 262;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the discharge policy or SOPs specifies that all patients should be given a discharge summary leaving the facility, record Y. Otherwise, record N.
Implementation
Review the 5 randomly selected case records from item ACC.7a:Dis2. If all records indicate that the patient received a discharge summary, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 263;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the discharge policy or SOPs ensure that a discharge summary includes all of the following topics, record Y: reasons admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given and the patient''s condition at the time of discharge. Otherwise, record N.  
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 The facilityâs records indicate that a medical record audit assessing whether discharge summaries comply with the facilityâs content requirements was conducted within the last year.
Review 5 randomly selected discharge summaries. All must contain the reasons for admission, significant findings, investigations results, diagnosis, procedure performed (if any), treatment given and the patient''s condition at the time of discharge.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 264;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the discharge policy or SOPs state that discharge summaries should contain follow up advice, medication, and other instructions, and either be typed or clearly written, record Y. Otherwise, record N.   
Implementation
Review the 5 randomly selected discharge summaries from Item ACC.7c:Dis6. If all discharge summaries contain follow up advice, medication and other instructions, and all are either typed or neatly written, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 265;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the discharge policy or SOPs ensure that discharge summaries incorporate instructions about when and how to obtain urgent care, record Y. Otherwise, record N.   
Implementation
Review the 5 randomly selected discharge summaries from Item ACC.7c:Dis6. If all discharge summaries contain instructions about when and how to obtain urgent care, record Y. Otherwise, record N.
')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 266;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the discharge policy or SOPs ensure that discharge summaries contain the cause of death in cases of death, record Y. Otherwise, record N.   
Implementation
Randomly select 5 discharge summaries for patients who died. If all discharge summaries document the cause of death, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 267;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policy or SOPs for clinical practice instruct that the care and treatment orders are signed and dated by the concerned doctor, record Y. Otherwise, record N.  
Implementation
Review 5 randomly selected care and treatment orders for maternity clients. If all are signed and dated by a doctor, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 268;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has adopted specific clinical practice guidelines to guide patient care in the labor room, record Y. Otherwise, record N. Example clinical practice guidelines could include those developed by FOGSI, ACOG, or other societies of obstetricians and gynecologists.
Implementation
Interview gynecologist and labor room head nurse. If staff report that specific clinical practice guidelines guide patient care in the labor room, record Y. Otherwise, record N. Example clinical practice guidelines could include those developed by FOGSI, ACOG, or other societies of obstetricians and gynecologists.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 269;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all of the conditions below are met, record Y. Otherwise, record N.
 Policies and SOPs for potential emergency cases exist; these cover receiving, managing, transferring, discharging, and referring clients, including DAMA cases
 Policies and SOPs for potential medico-legal cases exist; these cover receiving, managing, transferring, discharging, and referring clients, including DAMA cases
 For facilities that do not handle emergencies, policies and SOPs should address the delivery of basic care (such as first aid) and provisions for referring the patient to an appropriate facility.
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
  Documents for medico-legal cases are available, including MLC registers, police intimation forms, and MLC certification
 Staff interviews with the head of facility or gynecologist indicate that staff handling emergencies are aware of facility policies on ensuring availability of equipment, medications, and consumables in the case of mass emergencies
Most recent training records for new doctors and staff indicate that all have been trained on handling of emergency and medico-legal cases.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 270;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs for emergency cases require that staff be aware of how to care for emergency patients within the scope of the facilityâs services, record Y. Otherwise, record N.
Implementation
Interview gynecologist. If they report that they have received training on the emergency services available at their facility, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 271;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs for emergency cases state that patients seen for emergency services should be documented, and their admission, discharge, or transfer should be included in documentation, record Y.  Otherwise, record N.
Implementation
Review 5 randomly selected case records for emergency patients. If all case records include information about admission, discharge to home, or transfer to another organization, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 272;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOPs on blood transfusion services includes all of the following, record Y. Otherwise, record N.
 Requirement that the facility should have an MOU with accredited blood bank or blood storage center
 Policies for obtaining blood and blood components, including at night and on holidays
 Guidelines for safe transportation of blood and blood components, including cold chain maintenance, and who should be in charge of making transportation arrangements
Guidelines for relevant paperwork to ensure cross-match and patient identity and safety
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 Interviewed labor room staff (gynecologist and labor room head nurse) report that they are aware of the facilityâs policy or SOPs on blood transfusion services
 Interviewed labor room staff report that they have been trained on the facilityâs policies for obtaining blood and blood components
The facility has an MOU with an accredited blood bank or blood storage center')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 273;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOPs on blood transfusion services states that informed consent must be obtained for donation and transfusion of blood and blood products, record Y. Otherwise, record N.
Implementation
Review 5 randomly selected case records from patients who received blood transfusions. If all case records indicate that patients gave informed consent for every transfusion they received, record Y. Otherwise, record N. Note that the same consent can be used for multiple transfusions in one sitting.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 274;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOPs on blood transfusion services includes all of the following, record Y. Otherwise, record N.
 Procedures for appropriate checking of blood (including blood type, antibody screening and identification, anti-HIV, anti-HCV, HBsAg, test for syphilis, and test for malaria parasites) before starting the transfusion and monitoring the transfusion
 Requirement that the facility have a transfusion reaction reporting form
Provisions for ensuring that all necessary human resources, equipment, and consumables are available
Implementation
If all of the following conditions are met, record Y. Otherwise, record N.
 Review the same 5 case records selected in Item COP.3b: Blood4. All case records should reflect that the blood was checked prior to the transfusion and that the transfusion was monitored.
 The facility should have all necessary blood transfusion equipment and consumables.
The head of facility and head nurse report that they have been trained on the procedure for documenting and reporting blood transfusion reactions.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 275;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has an ICU or HDU manual that contains SOPs for providing critical care services to patients, record Y. Otherwise, record N.
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 Training records for staff in the labor room indicate that staff have been trained on the facilityâs HDU or ICU capabilities
 Informed consent forms and monitoring sheets are available for maternity patients in the HDU or ICU
 Necessary equipment, medications, and consumables are available in the HDU or ICU')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 276;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs ICU or HDU manual specifies the number and qualifications of staff to be available and the essential equipment required, record Y. Otherwise, record N. 
Implementation
If facility head indicates that staff in the ICU or HDU are available as follows, record Y. Otherwise, record N.
 HDU has at least 1 nurse  for every 3 beds, and at least 1 nurse in the HDU is a GNM or BSc nurse
ICU has at least 1 GNM or BSc nurse for every bed')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 277;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If a policy document defining the scope of the facilityâs obstetric services exists, including license for relevant services (such as MTP or PNDT licenses), record Y. Otherwise, record N.
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 The scope of obstetric services is displayed prominently in the entrance area for the maternity ward, and is displayed in both English and the local language
 Training records for labor room staff indicate that staff have been trained on the scope of obstetric services offered at the facility')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 278;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy document defining the scope of the facilityâs obstetric services includes regular antenatal care checkups, maternal nutrition, and postnatal care, record Y. Otherwise, record N.
Implementation
Interview gynecologist and labor room head nurse. If all staff report that obstetric care offered at the facility includes regular antenatal care checkups, maternal nutrition, and postnatal care, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 279;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all of the conditions below are met, record Y. Otherwise, record N.
 The policy document defining the scope of the facilityâs obstetric services requires that the facility have a nursery or newborn care corner to care for neonates, which includes basic equipment such as radiant warmers.
 If the facility offers high-risk obstetric services, the policy document requires that the facility have an ICU and NICU.
If the facility does not offer high-risk obstetric services, the policy document states that an MOU exists with another facility that does provide high-risk services, and explains the procedure for referring patients to that facility when necessary.
Implementation
Observe conditions in the facility. If all of the conditions below are met, record Y. Otherwise, record N.
 A nursery or newborn care corner is available to care for neonates, and includes basic equipment such as radiant warmers
 If the facility offers high-risk obstetric services, an ICU and NICU are available
 If the facility does not offer high-risk obstetric services, an MOU with a nearby facility that does offer high-risk services is on file.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 280;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If a policy document defining the scope of the facilityâs pediatric services exists, including license for relevant services, record Y. Otherwise, record N.
Implementation
If the scope of pediatric services is displayed prominently in the entrance area for the pediatric ward, and is displayed in both English and the local language, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 281;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy document defining the scope of the facilityâs pediatric services specifies which staff can provide care to children, record Y. Otherwise, record N.
Implementation
Interview facility head. If they say that only staff with specialized training can provide care to children, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 282;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy document defining the scope of the facilityâs pediatric services includes detailed nutritional growth and immunization assessments, record Y. Otherwise, record N.
Implementation
Interview the facility head. If they report that pediatric care offered at the facility includes detailed nutritional growth and immunization assessments, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 283;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has a policy and SOP on child abduction and abuse that includes provisions for identification and security measures, record Y. Otherwise, record N.  
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 The facility head is aware that the facility has a policy and SOP on child abduction and abuse
 The head nurse reports that mock drills are conducted, deviations are pointed out, and corrective and preventive actions are undertaken to practice the procedures for handling child abduction and abuse
 The head nurse states that they have the necessary infrastructure and manpower to follow the facilityâs SOP on child abduction and abuse')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 284;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy document defining the scope of the facilityâs pediatric services states that during pediatric consultations, children''s family members should be educated about nutrition, immunization, and safe parenting, record Y. Otherwise, record N.
Implementation
If head of facility states that they educate childrenâs family members on nutrition, immunization, and safe parenting during pediatric consultations, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 285;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has a policy and SOP for the administration of anesthesia, record Y. Otherwise, record N.
Implementation
Interview head of facility. If they report knowledge of the facilityâs policy and procedure for administration of anesthesia, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 286;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia states that a pre-anesthesia assessment must be done (up to 1 day before the procedure), record Y. Otherwise, record N.
Implementation
Review 5 randomly selected medical records from patients who underwent anesthesia. If possible, select 2 records from patients who underwent a C-section. If all reflect that a pre-anesthesia assessment was done, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 287;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia states that a documented anesthesia plan should be developed and confirmed prior to the procedure, record Y. Otherwise, record N.   
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain a documented anesthesia plan and evidence that the plan was confirmed prior to the procedure, record Y. Otherwise, record N.  ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 288;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia states that all patients should undergo an immediate pre-operative assessment or re-evaluation prior to their procedure and that the results must be documented, record Y. Otherwise, record N.
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain documented evidence of an immediate pre-operative assessment or re-evaluation, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 289;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia states that informed consent for administration of anesthesia must be obtained by the anesthetist, record Y. Otherwise, record N. 
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain a record of informed consent obtained by the anesthetist, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 290;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia states that periodic and regular monitoring of all of the items listed below must be conducted during the procedure, record Y. Otherwise, record N.
 heart rate
 cardiac rhythm
 respiratory rate
 blood pressure
 oxygen saturation
 airway security
 potency and level of anesthesia
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain an intraoperative monitoring chart covering periodic and regular monitoring of the following items, record Y. Otherwise, record N.
 heart rate
 cardiac rhythm
 respiratory rate
 blood pressure
 oxygen saturation
 airway security
potency and level of anesthesia')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 291;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP on administration of anesthesia requires that each patientâs post-anesthesia status is monitored and documented, record Y. Otherwise, record N.
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain evidence of post-anesthesia monitoring, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 292;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has a policy or SOP for providing safe surgical services that states that all surgical patients should have a preoperative assessment and a provisional diagnosis documented prior to surgery, record Y. Otherwise, record N.  
Implementation
Review the case records selected in Item COP.7b:Ane4. If all records show that patients had a preoperative assessment and a documented provisional diagnosis, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 293;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP for providing safe surgical services states that informed consent for must be obtained by the surgeon prior to the procedure, record Y. Otherwise, record N.
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain a record of informed consent obtained by the surgeon, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 294;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP for providing safe surgical services includes a requirement for a surgical safety checklist, record Y. Otherwise, record N. The checklist should be designed to prevent adverse events like wrong site, wrong patient, and wrong surgery
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 Training records for surgical staff indicate that staff have been oriented to the surgical safety checklist
 Interviews with head of facility, gynecologist, and labor room head nurse confirm that they follow the surgical safety checklist at key junctures:
 When the patient is transferred to the OT
  Before the induction of anesthesia
  Before skin incision
  Before sign-out or completion of procedure')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 295;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP for providing safe surgical services specifies which staff are entitled to perform surgical procedures, record Y. Otherwise, record N.  
Implementation
If interviewed staff (head of facility and gynecologist) state that only authorized providers perform surgical procedures at the facility, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 296;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP for providing safe surgical services states that the operating surgeon should document operative notes and post-operative plan of care, record Y. Otherwise, record N.
Implementation
Review the case records selected in Item COP.7b:Ane4. If all contain operative notes and post-operative plan of care documented by the operating surgeon, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 297;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs policy or SOP for providing safe surgical services states that the operation theatre must be adequately equipped, and the facility has an infection control policy that addresses the operation theatre, record Y. Otherwise, record N.
Implementation
If all of the conditions below are met, record Y. Otherwise, record N.
 Operation Theatre logs indicate that the OT is cleaned and biomedical waste is removed on a regular basis
 The OT has personal protective equipment (gloves, goggles, masks, aprons, gowns, boots or shoe covers, and cap or hair cover) available
 The OT has other necessary equipment available, including ventilator support, cardiac monitoring equipment, defibrillators, infusion pumps, central oxygen supply, and suction.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 298;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has procedures for the purchase, storage, prescription, and dispensing of medications, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If they report knowing of facility procedures for purchase, storage, prescription, and dispensing of medications, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 299;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures related to medications state that they must comply with the Drugs and Cosmetics Act, the Psychotropics and Narcotics Act, and any other applicable laws and regulations, record Y. Otherwise, record N.
Implementation
Check to see if the facility has licenses and other documentation indicating compliance with the Drugs, Cosmetics act and Psychotropics and Narcotics act, and Drugs and Magical Remedies act. If so, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 300;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures related to medications state that sound alike and look alike medications should be stored separately, record Y. Otherwise, record N.
Implementation
Observe medication storage conditions in the pharmacy. If a clear system for separating sound alike and look alike medications is observed, record Y. Otherwise, record N. Examples of sound-alike drugs include Rikcin or Roxin, Dilantin or Dilcontin, and Aldactone or Aldarone.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 301;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures related to medications state that medications beyond their expiry date should not be stored or used, record Y. Otherwise, record N.
Implementation
Observe medication storage conditions in the pharmacy. If no observed medications have expired, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 302;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has a procedure that addresses how to procure and use implantable prostheses, record Y. Otherwise, record N.
Implementation
Interview the pharmacy in-charge. If they report being aware of the facilityâs procedure on implantable prostheses, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 303;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for prescribing medications specifies that only registered doctors are authorized to prescribe medications, record Y. Otherwise, record N.
Implementation
Review 5 randomly selected prescriptions. If all show that the prescription was written by a registered doctor, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 304;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for prescribing medications specifies that medication orders should be written in a specific location in medical records, record Y. Otherwise, record N.
Implementation
Review 5 randomly selected medical records. If all show that medication orders were written in a single, uniform location, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 305;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for prescribing medications specifies that medication orders must be clear, legible, dated, and signed, record Y. Otherwise, record N.
Implementation
Review the 5 prescriptions selected in Item MOM.2a:Presc2. If all are typed or printed clearly, dated, and signed by a registered doctor, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 306;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for prescribing medications states that the facility must keep a list of high-risk medications, define identifiers of high-risk medications, and define a process for prescribing them, record Y. Otherwise, record N.
Implementation
If the facility has an up-to-date list of high-risk medications available, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 307;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for dispensing of medications states that medications must be checked prior to dispensing to ensure that they are fit for use, record Y. Otherwise, record N.
Implementation
Interview the pharmacy in-charge. If they report that they are aware of the facilityâs guidelines for checking medications to ensure that they are fit for use, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 308;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedure for dispensing of medications states that high risk medication orders must be verified prior to dispensing, record Y. Otherwise, record N.
Implementation
Review 5 randomly selected medication orders for high-risk medications. If all indicate that the person dispensing the order verified the order with the prescribing doctor, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 309;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications state that certain medications can only be administered by trained personnel, record Y. Otherwise, record N.
Implementation
Review the 5 randomly selected medication orders for medications that must be administered by trained personnel. If all indicate that medications were administered by trained personnel, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 310;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications state all of the following, record Y. Otherwise, record N.
 High-risk medications must be defined and identified
 The patient, dosage, route, and timing of each high-risk medication order must be verified prior to administration
Implementation
Interview gynecologist and head nurse. If they state that the patient, dosage, route, and timing are always verified prior to administration of medication, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 311;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications state that prepared medications must be labeled prior to preparation of subsequent drugs, record Y. Otherwise, record N.
Implementation
Observe preparation of medications in the operation theatre. If staff preparing medications label each medication prior to preparation of subsequent drugs, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 312;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications state that the administration of medications must be documented, record Y. Otherwise, record N.
Implementation
Review the 5 medical records selected in Item MOM.2b:Presc4. If all show that medication administration was documented, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 313;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications state that proper records of the usage, administration and disposal of narcotics and psychotropic medications must be kept, and specify all of the following, record Y. Otherwise, record N. 
 Narcotics and psychotropic medications should be stored under lock and key, with a designated person being responsible for them.
 Records are kept of the usage, administration, and disposal of these drugs.
 Inventory control procedures for these drugs are specified.
Empty vials for these drugs are disposed of as per local regulatory requirements.
Implementation
Interview gynecologist and head nurse. If staff are aware of the facilityâs procedures on keeping records of the usage, administration and disposal of narcotics and psychotropic medications, record Y. Otherwise, record N')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 314;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications defines adverse drug events and states that they must be monitored, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If staff report that they know what adverse drug events are and report that they are aware of the facilityâs policy on monitoring them, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 315;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs procedures on medications specify that adverse drug events must be documented and reported to the pharmacy and concerned doctor within 24 hours, record Y. Otherwise, record N.
Implementation
Review documentation from 5 randomly selected adverse drug events. If all show that the pharmacy and concerned doctor were informed within 24 hours, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 316;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that  a citizen charter exists, and it includes the right to respect for personal dignity and privacy during examination, procedures, and treatment, record Y. Otherwise, record N.
Implementation
If citizen charter is prominently displayed in lobby or entry area and includes the right to respect for personal dignity and privacy during examination, procedures, and treatment, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 317;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that the citizen charter includes patientsâ protection from physical abuse or neglect, record Y. Otherwise, record N.
Implementation
If citizen charter states that patients should be protected from physical abuse or neglect, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 318;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that the citizen charter includes treating patient information as confidential, record Y. Otherwise, record N.
Implementation
If citizen charter states that patient information will be treated as confidential, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 319;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that the citizen charter includes information about obtaining informed consent before carrying out procedures, record Y. Otherwise, record N.
Implementation
If citizen charter states that informed consent must be obtained before carrying out procedures, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 320;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs requires that the citizen charter includes information on how to voice a complaint, record Y. Otherwise, record N.
Implementation
If citizen charter includes information on how to voice a complaint, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 321;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that the citizen charter includes the provision to convey charges to the patients in a manner that they understand, record Y. Otherwise, record N.
Implementation
If citizen charter states that charges must be conveyed to the patients in a manner that they understand, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 322;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that the citizen charter includes the provision that patients have a right to their clinical records, record Y. Otherwise, record N.
Implementation
If citizen charter states that patients have a right to their clinical records, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 323;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
Facility policy or SOPs require that information is shared with patients or attendants regularly on all of the following items, record Y:
 plan of care
 preventive aspects
 possible complications
 medications
 expected results
 costs 
Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If both report that they share information with clients or their attendants on all of the topics listed below, record Y. Otherwise, record N.
 plan of care
 preventive aspects
 possible complications
 medications
 expected results
 costs')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 324;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility policy or SOPs require that patients and families are offered translation services if needed and providers take care make sure that patients and families understand the care they are provided, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If both report that clients understand the care they are provided, and that translation services are offered as needed,  record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 325;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the hospital infection control manual provides guidance on adherence to standard precautions, including all of the precautions listed below, record Y. Otherwise, record N.
 Washing hands before and after all patient or specimen contact
 Handling blood of all patients as potentially infectious
 Wearing gloves for potential contact with blood and bodily fluid
 Preventing needle stick or sharp injuries
 Wearing personal protective equipment while handling blood or body fluids
 Handling all linen soiled with blood and or or body secretion as potentially infectious
 Processing all lab specimens as potentially infectious
 Wearing a mask for TB and other contagious respiratory infections
 Correctly processing instruments and patient care equipment
 Maintaining environmental cleanliness
 Following proper waste disposal practices
Implementation
Interview gynecologist and head nurse. If all staff report being familiar with standard precautions, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 326;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the hospital infection control manual includes guidelines on routine cleaning to maintain the cleanliness and general hygiene of the facility, and guidelines for monitoring cleanliness and hygiene, record Y. Otherwise, record N. 
Implementation
If all of the following conditions are met, record Y. Otherwise, record N.
 Observe the labor room. The following conditions must be observed:
 Beds have clean sheets
 The floor is clean
 Curtains separating beds are clean and not torn
 Labor room toilet is clean
 Equipment trays are laid out and covered
 Logs or records of cleaning in the facility exist, and indicate that key areas are cleaned at least every 24 hours.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 327;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the hospital infection control manual includes guidelines for cleaning and disinfection practices, including guidelines for monitoring these practices, record Y. Otherwise, record N.
Implementation
Review logs or records of cleaning in the facility. If such logs or records provide information on specific cleaning and disinfection practices used, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 328;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the hospital infection control manual includes provisions for all of the following listed below, record Y. Otherwise, record N. 
 equipment cleaning
 equipment disinfection
 equipment sterilization practices
Implementation
Interview gynecologist and head nurse
If all staff report being aware of the hospital infection control manualâs provisions for equipment cleaning, disinfection, and sterilization practices (including timing of any cleaning and the methods to be used), record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 329;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual includes provisions for laundry and linen management, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If all staff report being aware of the hospital infection control manualâs provisions for laundry and linen management, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 330;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual includes provisions for hand washing facilities (stations) at point of use, record Y. Otherwise, record N.
Implementation
Observe hand washing facilities in the labor room. If there is a sink with running water (or a bucket with a tap), and soap, available at the hand washing station, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 331;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual requires that adequate personal protective equipment is available and that staff adheres to standard personal protection practices, record Y. Otherwise, record N. 
Implementation
Observe practices in the labor room. If all of the following conditions are met, record Y. Otherwise, record N.
 Gloves, masks, soap, and disinfectant are available in the labor room
 Staff are observed to wash their hands in all of the following situations:
 Before touching a patient
 Before a clean or aseptic procedure
 After body fluid exposure risk and glove removal
 After touching a patient
 After touching a patientâs surroundings')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 332;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual requires that pre- and post-exposure prophylaxis for HIV and hepatitis B be provided to concerned staff members, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If all staff report that they are offered pre- and post-exposure prophylaxis for HIV and hepatitis B when necessary, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 333;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs general policies or SOPs state that they require authorization from the state pollution control board for management and handling of bio-medical waste, record Y. Otherwise, record N.
Implementation
If authorization from the prescribed authority (such as the state pollution control board) exists for management and handling of bio-medical waste, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 334;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual provides guidelines for the segregation and collection of bio-medical waste, including the monitoring of these practices, record Y. Otherwise, record N.
Implementation
If all of the following conditions are met, record Y. Otherwise, record N.
 There are separate, color-coded bags used for different categories of waste in the labor room
 Gynecologist and head nurse that collection and disposal of bio-medical waste is done according to state guidelines
 Facility records show that collection of biomedical waste in labor room is monitored regularly')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 335;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual requires that bio-medical waste is handled in accordance with state guidelines or outsourced to authorized contractors, record Y. Otherwise, record N.
Implementation
Interview facility head, gynecologist, and head nurse. If all staff report that bio-medical waste is handled in accordance with state guidelines or outsourced to an authorized contractor, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 336;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual states that fees, documents, and reports related to bio-medical waste management must be submitted to the relevant authorities, record Y. Otherwise, record N.
Implementation
Review records of fees, documents, and reports related to bio-medical waste management. If the last 5 reports or documents indicate that fees and reports were submitted on time, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 337;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If hospital infection control manual notes that staff should use all of the personal protective equipment noted below when handling bio-medical waste, record Y. Otherwise, record N.
 Gloves
 Protective eye wear (goggles)
 Mask
 Apron
 Gown
 Boots or shoe coversâ
 Cap or hair cover
Implementation
Observe staff in the labor room. If staff use all of the personal protective equipment listed below when handling bio-medical waste, record Y. Otherwise, record N.
  Gloves
 Protective eye wear (goggles)
 Mask
 Apron
 Gown
 Boots or shoe covers
 Cap or hair cover')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 338;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs quality improvement policy or SOPs require that there is a designated individual for coordinating and implementing the quality improvement programme, record Y. Otherwise, record N.
Implementation
If the head of facility and head HR person (if available) report that a designated individual coordinates and implements the facilityâs quality improvement programme, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 339;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs quality improvement policy or SOPs state that quality improvement should be a continuous process and that it should be updated at least once in a year, record Y. Otherwise, record N.
Implementation
Interview a staff person who is knowledgeable about the facilityâs quality improvement plan (either the head of facility or head HR person, if available). If this person reports that the facility engages in a continuous quality improvement process and updates its plan at least once in a year, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 340;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs quality improvement policy or SOPs state that the facility will make adequate resources available for the quality improvement program, record Y. Otherwise, record N.
Implementation
Interview the person identified in CQI.1b:Qual4. If this person reports that the facility makes any resources available for the quality improvement programme, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 341;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs SOP for collection and analysis of key performance indicators states that key indicators should be identified in both clinical and managerial areas, record Y. Otherwise, record N.
Implementation
If key performance indicators exist for the facility, and the formula, sample size, and method of data collection to be used for each indicator are specified, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 342;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs SOP for collection and analysis of key performance indicators specifies that the indicators should be monitored on a regular basis, record Y. Otherwise, record N. Monitoring for each indicator should be done at least annually.
Implementation
If facility documentation shows that indicators have been monitored recently (in alignment with the SOPâs specified time frame for monitoring), record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 343;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
Do facility procedures or documents or guidelines or protocols include an organogram describing the management structure? If yes, record Y. Otherwise, record N.
Implementation
Ask facility head if an organogram describing the management structure exists. If they respond yes, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 344;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facilityâs general policies or SOPs stipulate that the organization should be registered with appropriate authorities, record Y. Otherwise, record N.
Implementation
If documentation exists showing registration of the organization with appropriate authorities, record Y. Otherwise, record N. Examples of key registrations or licenses could include AERB requirements, PCPNDT Act, MTP Act, etc.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 345;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility procedures or documents or guidelines or protocols name an individual (or group of people) to oversee the hospital wide safety program, record Y. Otherwise, record N.
Implementation
Interview facility head. If they report that there is a designated individual (or group of people) who are responsible for overseeing the hospital wide safety programme, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 346;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility procedures or documents or guidelines or protocols require that the facility''s mission statement is displayed clearly in the entrance lobby, foundation stone, and in all common waiting areas, record Y. Otherwise, record N.  
Implementation
Observe where the facilityâs mission statement is displayed. If it is displayed clearly in the entrance lobby, foundation stone, and in all common waiting areas, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 347;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility procedures or documents or guidelines or protocols include information on organizational ethics, record Y. Otherwise, record N.  Guidelines or protocols should cover topics such as standard billing tariff, transparency, and concessions to BPL, EWS, and widows.
Implementation
Interview facility head. If they report that there is information on organizational ethics, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 348;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility procedures or documents or guidelines or protocols require that the facility''s ownership is displayed clearly in a common area of the facility, on its website, on the registration certificate, and or or in other locations easily accessible to the public, record Y. Otherwise, record N.
Implementation
Observe areas where facility ownership should be displayed. If it is displayed clearly in a common area of the facility, on its website, on the registration certificate, and or or in other locations easily accessible to the public, record Y. Otherwise, record N. ')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 349;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If facility procedures or documents or guidelines or protocols include information on ethical billing process, record Y. Otherwise, record N. Guidelines and protocols should specify that the organization does not charge differentially for different patients in the same bed category for the same surgery or procedure.
Implementation
Interview facility head. If they report that there is information on ethical billing processes, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 350;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols requires that the facility has uniform and user-friendly signage in all departments listed below,â and that signs are available in the local language and English, record Y. Otherwise, record N.
 Main lobby
 Pharmacy 
 Outpatient department
 Laboratory or imaging area
 Labor room  
Implementation
If uniform and user-friendly signage is available in all departments listed below, and signs are available in the local language and English, record Y. Otherwise, record N.
 Main lobby
 Pharmacy 
 Outpatient department
 Laboratory or imaging area
 Labor room')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 351;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols provide information about how to contact maintenance staff for emergency repairs around the clock, record Y. Otherwise, record N.  
Implementation
Interview head nurse and gynecologist regarding emergency repairs around the clock. If staff report that maintenance staff can be contacted for emergency repairs around the clock, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 352;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If protocols for reporting and managing risks cover the topics below, then Y. Otherwise, record N. 
 Training to identify and report safety and security risks for all staff. 
 Procedures for documentation of reported potential risks
 Protocol for addressing the reported incident or potential risk
Implementation
If all of the conditions below are met, then record Y. Otherwise, record N. 
 The most recent staff training records show that participating staff were trained on identification, management, and reporting of risks
 When interviewed, gynecologist and head nurse are aware of risks and process for identification, management and reporting of risks
 Documentation of reported risks shows risk mitigation in terms of corrective and preventive action taken')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 353;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If protocols for reporting and managing risks specify that inspection rounds should be conducted at least once per month to assess safety, record Y. Otherwise, record N.
Implementation
Interview head nurse and gynecologist. If all staff report that inspection rounds to assess safety are conducted at least once per month, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 354;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If protocols for reporting and managing require a safety education program (for example, including fire safety, hazardous materials, use of personal protective equipment, etc.)  at the facility, record Y. Otherwise, record N.
Implementation
Interview head nurse and gynecologist. If all staff report knowledge of a safety education program at the facility, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 355;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols specify facility plans and procedures for procuring and maintaining equipment, record Y. Otherwise, record N.
If standard facility procedures or documents or guidelines or protocols specify the facility''s plans for maintaining equipment, but not for procuring equipment, or vice versa, record N.
Implementation
Interview gynecologist and head nurse. If staff are aware of the facility procedures or documents or guideline or protocols for procuring and maintaining equipment, then Y, otherwise, N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 356;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols on equipment operation and maintenance (preventive and breakdown) exists and includes all the topics in the checklist below, record Y. Otherwise, record N.
 inventory of all equipment 
 training of the technician operating the equipment 
 operational plan for every machine based on the operatorâs manual
 addressing breakdown and repairs 
 records of preventive and breakdown maintenance  
Implementation
If standard records on maintenance (preventive and breakdown) include all the topics in the checklist below, record Y. Otherwise, record N.
 availability of inventory number for each machine
 operational plan for the equipment on every machine
 preventive maintenance schedule for each machine
 breakdown maintenance or complaint register')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 357;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols require that potable water and electricity are available at around the clock in key areas, record Y. Otherwise, record N.  
Implementation
Verify that electricity and water are available in the lobby, labor room, toilets, operation theatre, and lab areas at the time of the visit.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 358;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols require that backup electricity is available, record Y. Otherwise, record N.  
Implementation
Verify that back-up electricity is available in the lobby, labor room, operation theatre, and lab areas at the time of the visit.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 359;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If protocols for operating and maintaining medical gas and vacuum installations specify weekly or monthly or annual maintenance plans for medical gas and vacuum systems specified below, record Y. Otherwise, record N. 
 LMO tank level and pressure
 Air compressor pressure, machine running status, oil level, belt tension, temperature, water pressure, cooling tower working, loading and unloading pressure range.
 Nitrous oxide, carbon dioxide oxygen manifold line pressure, heater coil, cylinder stock.
Implementation
If records of weekly or monthly or annual maintenance plans for medical gas and vacuum systems cover all of the topics specified below, record Y. Otherwise, record N. 
 safety signage present
 actual storage of empty and filled cylinders
 by-pass in case of emergencies and back up
 valves shut off in different loops
 chained cylinders
 mechanism of loading and unloading cylinders
 leak detection systems
 daily, weekly and monthly checks by operator
 annual overhaul
 standardized colour coding of pipelines
 condition of the cylinders, colour coding
 personnel protective equipment for the staff')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 360;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If protocols for managing fire and non-fire emergencies exists, record Y. Otherwise, record N.
Implementation
If evidence of all of the following exists in the facility, record Y. Otherwise, record N.
 fire detection systems')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 361;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the facility has an emergency floor plan and emergency evacuation plan in case of emergencies, record Y. Otherwise, record N.
Implementation
If the following are present, check Y, otherwise, check N.
 Green-coloured exit signage is clearly visible
 Emergency lighting
 Emergency floor plans are visible on all the floors and conspicuous spaces
 An emergency evacuation plan exists
 Staff are trained in the emergency evacuation plan
 Staff are aware of their roles during an emergency evacuation
 Mock drills are conducted to test the plan')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 362;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols require that the facility has routine (at least annual) staff training for fire or disaster situations, record Y. Otherwise, record N.
Implementation
If training records indicate that staff were trained for fire or disaster situations within the last year, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 363;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols require mock drills for fire or other disaster situations at least twice per year, record Y. Otherwise, record N.
Implementation
If training records or other HR records indicate that at least two mock drills for fire or other disaster situations were conducted in the last year, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 364;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If a description of relevant risks (occupational hazards) in the facility is included in training materials on hospital-wide policies and procedures, record Y. Otherwise, record N.
Implementation
Review documentation from the most recent training provided to new staff and most recent annual re-training provided to existing staff. If both include a description of relevant risks (occupational hazards) that may be encountered in the facility, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 365;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If HR policy or SOPs describe how staff can report on and eliminate or minimize risks, record Y. Otherwise, record N.
Implementation
Interview the head HR person (if available), or the gynecologist and head nurse (if head HR person is unavailable or does not exist) about their awareness of reporting procedures. If all staff interviewed are aware of HR policy or SOPs on reporting and eliminating or minimizing risks, then Y. Otherwise, N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 366;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If staff training guidelines describe procedures for training staff when job duties change or new equipment is introduced, record Y. Otherwise, record N.
Implementation
Review records from the most recent staff training that involved training of staff in new positions, or the most recent staff training that was held after new equipment was introduced at the facility. If training documentation shows that staff were trained when job duties changed or new equipment was introduced, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 367;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all conditions listed below are met, record Y. Otherwise, record N.
 A documented procedure for disciplinary action exists.
 A documented procedure for addressing sexual harassment complaints in the workplace exists
 A documented procedure is available for addressing grievance-handling
 The procedures listed above are included in HR SOPs
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 The facility head states that the grievance handling procedure is reviewed and approved by top management on a yearly basis
 Records of disciplinary proceedings are maintained (Confirm that these documents exist)
 Records of grievance handling proceedings are maintained (Confirm that these documents exist)
 Records of proceedings that handle complaints of sexual harassment in the workplace are maintained confidentially (Confirm that these documents exist and are stored in a manner that prevents others from viewing them unless necessary)')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 368;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the compilation of SOPs in the HR department and the material for training staff on hospital-wide policies and procedures contain the procedures for disciplinary action, addressing sexual harassment complaints, and addressing grievance-handling that were reviewed in item HRM.2a: Griev1, record Y. Otherwise, record N.
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 Gynecologist and head nurse report that they know of the facilityâs disciplinary procedure. 
 Gynecologist and head nurse report that they know of the facilityâs grievance handling procedure
 Gynecologist and head nurse report that they know of the facilityâs process for addressing sexual harassment complaints')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 369;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If documented procedures or documents or guideline or protocols include steps be taken to resolve grievances, record Y. Otherwise, record N.
Implementation
Randomly select documents from 5 grievances. If records from all 5 cases indicate that actions were taken to resolve the grievance, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 370;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all of the conditions listed below are met, record Y. Otherwise, record N. 
 A documented policy on employeesâ health exists
 The facility has documented wither the Employee State Insurance (ESI) Act applies to it
 The facility maintains a list of staff whose gross salary is less than Rs. 15,000 per month and demonstrates that the list is updated every month
Implementation
If all of the conditions listed below are met, record Y. Otherwise, record N.
 The facilityâs ESI records show that eligible new staff are enrolled under ESI on a regular basis and that appropriate ESI payments are made by the facility.
  Interviews with gynecologist and head nurse show awareness of the health problems policy of the organization and the ESI Act. 
 Gynecologist and head nurse report that pre-exposure and post-exposure prophylaxis are provided.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 371;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols include steps for addressing occupational health hazards, record Y. Otherwise, record N.
Implementation
Interview gynecologist and head nurse. If staff interviews reveal that pre-exposure and post-exposure prophylaxis are regularly provided, record Y. Otherwise, record No. Examples could include: Hepatitis B vaccine or influenza vaccine for staff at risk; post-exposure prophylaxis such as immunoglobulin treatment post-Hepatitis B exposure; antiviral medication for staff involved in treatment of patients with H1N1; and regular check-ups for any staff dealing with direct patient care.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 372;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policy or SOPs on maintaining employee records include procedures for maintaining employee personal files for each staff member, record Y. Otherwise, record N.
Implementation
Ask to see where employeesâ personal files are stored. If all of the conditions below are met, record Y. Otherwise, record N.
 Employeesâ personal files are kept on hand at the facility
 Employeesâ personal files are stored in a secure location
 Only relevant staff can access the files
If no personal files for staff members exist, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 373;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If standard facility procedures or documents or guidelines or protocols require that the personal files include information regarding each employeeâs qualification, disciplinary actions, and health status, record Y. Otherwise, record N.
Implementation
Review 5 randomly selected employee files. If all files include information regarding the employeeâs qualification, disciplinary actions, and health status, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 374;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records state that each medical record should have a unique identifier, record Y. Otherwise, record N.
Implementation
Check 5 randomly selected medical records, including at least 3 medical records from inpatients. If all have a unique identifier, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 375;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify who in the facility is authorized to make entries in the medical records, record Y. Otherwise, record N.
Implementation
Ask facility head, gynecologist, and head nurse if policies or SOPs specify who is authorized to make entries in the medical records. If all say yes, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 376;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records require that all records or register entries are dated, timed, and stamped or signed, record Y. Otherwise, record N. 
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all are dated, timed, and signed or stamped, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 377;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records ensure that all records or register entries note the author of the entry, record Y. Otherwise, record N. 
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all note the author of the latest entry, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 378;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If all of the below are true, check Y. If not, check N.
 The SOP on medical records specifies the contents of medical records that are to be identified and documented.
 The SOP on medical records specifies that date, time, name and signature of the medical documentations should be accurately recorded.
 The SOP on medical records specifies that medical records should be checked for deficiencies in terms of accuracy and completeness.
Implementation
If all of the following conditions are met, record Y. Otherwise, record N.
 Check the 5 records selected in item IMS.1a: Rec2. All must have all the documents, records and formats filed in a chronological manner as per the SOP.
 Interview head of facility, gynecologist, and head nurse about procedures for auditing medical records. All staff must report that medical records are checked for deficiencies in terms of accuracy and completeness.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 379;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If SOP on medical records specifies that all of the items below should be in the medical record, check Y.  Otherwise, record N.
 Final diagnosis in the admission record
 Final outcome
 Signatures with date, name and time
 Discharge summary (for inpatients)
 Initial assessment form
 Consent forms
 OT or post-operative notes (if applicable)
 Death case sheet (if applicable)
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all contain all of the following information, record Y. Otherwise, record N. 
 Final diagnosis in the admission record
 Final outcome
 Signatures with date, name and time
 Discharge summary (for inpatients)
 Initial assessment form
 Consent forms
 OT or post-operative notes (if applicable)
 Death case sheet (if applicable)')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 380;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify that all records or register entries must include information about all items listed below, record Y. Otherwise, record N.
 admission
 diagnosis
 plan of care
 any procedures performed
 discharge note (for inpatients)
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all contain all of the following information, record Y. Otherwise, record N.
 admission
 diagnosis
 plan of care
 any procedures performed
 discharge note (for inpatients)')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 381;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify that operative and other procedures performed should be incorporated in the medical record, record Y. Otherwise, record N.
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all contain information on operative or other procedures performed, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 382;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify that the medical record must contain a copy of the discharge note duly signed by appropriate and qualified personnel, record Y. Otherwise, record N. 
Implementation
Check the 5 records selected in item IMS.1a: Rec2. If all records pertaining to discharged patients (i.e. inpatients) contain a copy of the discharge note duly signed by appropriate and qualified personnel (as specified in discharge policy), record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 383;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify that all records or register entries for patients who have died must include all items in the checklist below, record Y. Otherwise, record N.
 copy of the death certificate 
 cause of death listed on death certificate 
 date of death listed on the death certificate
 time of death listed on the death certificate 
Implementation
Randomly select 5 medical records for patients who died. If all records contain all of the items below, record Y. Otherwise, record N.
 copy of the death certificate 
 cause of death listed on death certificate 
 date of death listed on the death certificate
 time of death listed on the death certificate')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 384;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on medical records specify that care providers should have access to current and past medical record, record Y. Otherwise, record N. 
Implementation
Interview facility head, gynecologist, and head nurse. If all providers report that the facility facilitates access to current and past medical records for their patients, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 385;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy on maintaining confidentiality, security and integrity of information covers all of the following items, check Y. Otherwise, check N.
 Procedures to maintain the confidentiality, security and integrity of information are available.
 The process of retrieval of files (i.e. obtaining files when needed) is specified.â
 A process for tracing missing files exists.
Implementation
If all of the following conditions are met, check Y. Otherwise, check N.
 Staff report knowing about the facilityâs procedures to maintain the confidentiality, security and integrity of information.
 Observe where case sheets are stored. Case sheets appear to be well protected from loss, theft and tampering. Paper files should be stored in fire proof cabinets with provisions for pest and rodent control. Electronic data should be protected from viruses, be password-protected, and have appropriate backup procedures.
 Staff report knowing about the process for retrieval of files.
 Staff report that missing files are traced.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 386;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the policy on maintaining confidentiality, security and integrity of information includes provisions for ensuring that information is used for identified purposes and is not disclosed without the patient''s authorization, record Y. Otherwise, record N. 
Implementation
Interview facility head, gynecologist, and head nurse. If all staff report that provisions are in place ensuring that information is used for identified purposes and is not disclosed without the patient''s authorization, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 387;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on retention and destruction of medical records documents procedures for retaining the patients'' clinical records, data and information, record Y. Otherwise, record N.
Implementation
Randomly select 5 case sheets to review. If no case sheets should be dated from a period that exceeds the retention period outlined in the facilityâs policy or SOP, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 388;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on retention and destruction of medical detail how to maintain confidentiality and security of retained records, record Y. Otherwise, record N.
Implementation
Interview facility head, gynecologist, and head nurse. If all staff report that the medical records are retained in a secure manner and protect patient confidentiality, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 389;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If policies or SOPs on retention and destruction of medical records cover all of the following, record Y. Otherwise, record N. 
 The process of destruction of medical records is defined.
 If the process of destruction is outsourced, the policy or SOP outlines adequate measures to be taken to safeguard confidentiality and security of these records during transmission to and destruction by third party.
Implementation
Observe where and how medical records are destroyed (if on site). If destruction of medical records compiles with the facilityâs policy or SOP, record Y. Otherwise, record N.
If destruction of medical records is outsourced, ask staff how these records confidentiality and security are safeguarded after it leaves the facility. If all can provide an answer, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 390;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Observation
 During delivery, observe whether the provider explains all of the danger signs (listed below)  and important care activities to mother and her companion
Danger Signs for mother:
 Vaginal bleeding 
 Rupture of membranes and delivery not happening for 12 hrs
 Convulsions 
 Severe headache and blurred vision 
 Severe abdominal pain  
 Respiratory difficulty
  Fever
 Foul smelling vaginal discharge
 Reduced or absent fetal movements
Danger signs for baby:
 Fast or difficulty in breathing
 Fever
 Unusually cold
 Stops feeding well
 Less activity than normal
 Whole body becomes yellow
Care activities:
 Instructing mother to bear down only during contractions
 Take breaths during every 2 contractions
 Pass urine every 2 hours
 Maintain hydration
 If the provider explains all of the above danger signs and important care activities to mother and her companion, record Y. If the provider does not explain danger signs and important care activities to mother and her companion, record N. 
If observation of a case is not possible, AND item ADM6.3.1b cannot be observed, score this element based on item 4 in Vignette #2.
Provider Interview
 Interview a labor room nurse. If a labor room nurse is not available, interview a gynecologist at the facility.
 If respondent reports that providers always explain danger signs and important care activities to mother and her companion, record Y. Otherwise, record N.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 391;
UPDATE QS SET QS.GuidanceNotes = RTRIM(LTRIM('Documentation
If the diagnostic imaging policy or SOPs specify that imaging personnel must be trained in safe practices and provided with personal protective equipment, record Y. Otherwise, record N.
Implementation
If all conditions listed below are met, record Y. Otherwise, record N.
 Records from the most recent training of imaging staff indicate that staff have been informed of diagnostic imaging SOPs, including safety procedures.
Personal protective equipment (gloves, gowns, medical masks, eye protection, TLD badges) is available for imaging staff.')) FROM dropdown.QualityStandard QS WHERE QS.QualityStandardID = 392;

ALTER TABLE dropdown.QualityStandard ENABLE TRIGGER TR_QualityStandard
GO
--End table dropdown.QualityStandard

--Begin table dropdown.VerificationCriteriaCategory
TRUNCATE TABLE dropdown.VerificationCriteriaCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.VerificationCriteriaCategory', 'VerificationCriteriaCategoryID', 0
GO

INSERT INTO dropdown.VerificationCriteriaCategory (VerificationCriteriaCategoryName) VALUES ('Interview')
INSERT INTO dropdown.VerificationCriteriaCategory (VerificationCriteriaCategoryName) VALUES ('Observation')
INSERT INTO dropdown.VerificationCriteriaCategory (VerificationCriteriaCategoryName) VALUES ('Policy/SOPs')
INSERT INTO dropdown.VerificationCriteriaCategory (VerificationCriteriaCategoryName) VALUES ('Records')
GO
--End table dropdown.VerificationCriteriaCategory

--Begin table dropdown.VerificationCriteriaProtocol
TRUNCATE TABLE dropdown.VerificationCriteriaProtocol
GO

EXEC utility.InsertIdentityValue 'dropdown.VerificationCriteriaProtocol', 'VerificationCriteriaProtocolID', 0
GO

INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('01. General facility policy documents')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('02. Gynecologist interview')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('03. Head nurse interview')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('04. Observation of outside and main lobby area')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('05. Staff training records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('06. Policy/SOPs on registration and admission')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('07. Review of registration and admission forms')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('08. Facility head interview')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('09. Policy and SOP for patient transfer and referral')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('10. Review of forms for transfer and referral')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('11a. Labor room records (transfer register)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('11b. Labor room records (5 randomly selected cases)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('12. Policy/SOPs for labor room/maternity ward')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('13. Labor room forms')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('14. Observation of labor room/maternity ward')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('15. Policy/SOPs, mcnucl, and documentation for  laboratory')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('16. Observation of laboratory')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('17. Infection control manual')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('18. Lab records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('19. Policy/SOPs and documentation for diagnostic imaging')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('20. Observation of imaging area')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('21. Diagnostic imaging records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('22. Discharge policy/SOPs')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('23. Discharge summaries (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('24. Facility medical record audit')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('25. Clinical practice guidelines')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('26. Maternity care and treatment orders (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('27. Policy/SOPs for emergency and medico-legal cases')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('28. Medico-legal records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('29. Emergency case records (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('30. Policy/SOPs and documentation on blood transfusion services')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('31. Blood transfusion client records (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('32. Observation of blood transfusion facilities')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('33. Manual/SOPs, forms, and documentation for ICU/HDU')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('34. Observation of ICU/HDU')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('35. Policy/SOPs for pediatric services')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('36. Observation of pediatric ward')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('37. Policy/SOPs for administration of anesthesia')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('38. Anesthesia/C-section client records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('39. Policy/SOPs on safe surgical services')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('40. Operation theatre logs/records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('41. Observation of operation theatre')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('42. Policy/SOPs for medications')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('43. Observation of pharmacy')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('44. Pharmacy in-charge interview')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('45. Prescription/medication order review (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('46. High-risk medication order review (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('47. Medication orders for medicines to be administered by trained personnel (5 randomly selected)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('48. Post-delivery client interviews')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('49. Facility cleaning records/logs')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('50. Biomedical waste documentation/records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('51. Policy/SOPs for quality improvement')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('52. HR head interview')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('53. Performance indicators for facility')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('54. Policy/SOPs on facilities/environment management')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('55. Observation of OPD')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('56. Documentation of reported risks')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('57. Policy/SOPs for procuring, maintaining, and operating equipment')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('58. Equipment maintenance records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('59. Maintenance plans for medical gas and vacuum systems')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('60. Protocols for handling fire and non-fire emergencies')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('61. Staff training and orientation materials and policies/SOPs for staff training')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('62. HR policies/SOPs')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('63. Records of disciplinary proceedings, grievances, and sexual harassment complaints')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('64. Employee records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('65. Policy/SOPs on medical records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('66. Case records of patients who died (5 randomly selected cases)')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('67. Observation of medical record storage and disposal')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('68. Observation of antenatal care area')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('69. Observation of exam room')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('70. Observation of location where client admitted')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('71. Client''s records')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('72. Postpartum visit')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('73. Observation of delivery room')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('74. Observation of newborn care')
INSERT INTO dropdown.VerificationCriteriaProtocol (VerificationCriteriaProtocolName) VALUES ('75. Interview labor room nurse')
GO
--End table dropdown.VerificationCriteriaProtocol

--Begin table dropdown.VerificationCriteriaSubCategory
TRUNCATE TABLE dropdown.VerificationCriteriaSubCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.VerificationCriteriaSubCategory', 'VerificationCriteriaSubCategoryID', 0
GO

INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Employees')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Facility maintenance')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Facility')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Head')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Intensive care rooms')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Labor room')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Manuals/guidelines')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Medication')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Other medical records')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Other procedure rooms')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Other records')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Other')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Patient')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Pharmacy')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Post-natal')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Rooms for before and during birth')
INSERT INTO dropdown.VerificationCriteriaSubCategory (VerificationCriteriaSubCategoryName) VALUES ('Rooms near entry')
GO
--End table dropdown.VerificationCriteriaSubCategory

--Begin table dropdown.QualityStandardVerificationCriteria
TRUNCATE TABLE dropdown.QualityStandardVerificationCriteria
GO

INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (100, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (100, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (101, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (101, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (101, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (102, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (102, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (102, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (103, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (104, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (104, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (104, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (105, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (106, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (106, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (106, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (106, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (107, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (107, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (107, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (107, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (108, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (108, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (109, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (110, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (111, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (112, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (113, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (114, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (115, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (115, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (115, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (116, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (116, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (116, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (117, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (117, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (118, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (118, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (118, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (119, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (119, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (119, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (119, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (120, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (120, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (120, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (120, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (121, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (121, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (122, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (122, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (122, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (123, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (123, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (124, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (125, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (125, 'VerificationCriteriaProtocol', 42)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (126, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (127, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (127, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (127, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (128, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (128, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (128, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (128, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (129, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (129, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (129, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (130, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (130, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (130, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (131, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (131, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (131, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (132, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (132, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (132, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (133, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (133, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (134, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (134, 'VerificationCriteriaProtocol', 42)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (135, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (136, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (137, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (137, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (137, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (138, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (138, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (138, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (139, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (139, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (139, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (140, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (140, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (140, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (141, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (141, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (141, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (142, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (142, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (142, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (143, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (143, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (143, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (144, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (144, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (144, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (145, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (145, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (145, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (146, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (146, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (146, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (147, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (148, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (149, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (149, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (149, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (150, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (151, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (151, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (151, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (152, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (153, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (153, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (153, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (154, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (154, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (154, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (155, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (156, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (157, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (157, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (158, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (158, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (158, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (158, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (159, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (159, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (159, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (159, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (160, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (161, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (161, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (161, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (161, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (162, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (163, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (163, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (163, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (163, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (164, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (164, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (164, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (164, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (165, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (165, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (165, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (166, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (166, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (166, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (166, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (167, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (167, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (167, 'VerificationCriteriaProtocol', 75)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (167, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (168, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (168, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (168, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (169, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (169, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (170, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (170, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (171, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (172, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (172, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (173, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (173, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (173, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (174, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (175, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (175, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (176, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (176, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (176, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (177, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (177, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (177, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (177, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (178, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (178, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (178, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (178, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (178, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (179, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (179, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (180, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (180, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (180, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (180, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (180, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (181, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (181, 'VerificationCriteriaProtocol', 74)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (181, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (181, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (182, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (183, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (184, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (185, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 73)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (186, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (187, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (187, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (187, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (188, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (188, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (188, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (189, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (189, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (189, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (243, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (243, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (243, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (244, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (244, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (244, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (245, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (245, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (246, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (246, 'VerificationCriteriaProtocol', 7)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (246, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (247, 'VerificationCriteriaProtocol', 11)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (247, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (248, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (248, 'VerificationCriteriaProtocol', 14)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (248, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (248, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (248, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (249, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (250, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (251, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (251, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (252, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (252, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (253, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (253, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (254, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (254, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (255, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (256, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (256, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (256, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (257, 'VerificationCriteriaProtocol', 19)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (258, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (258, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (258, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (259, 'VerificationCriteriaProtocol', 21)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (260, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (261, 'VerificationCriteriaProtocol', 22)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (262, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (263, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (264, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (264, 'VerificationCriteriaProtocol', 25)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (265, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (266, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (267, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (268, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (268, 'VerificationCriteriaProtocol', 27)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (269, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (269, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (269, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (270, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (270, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (270, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (271, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (272, 'VerificationCriteriaProtocol', 30)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (273, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (273, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (274, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (275, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (275, 'VerificationCriteriaProtocol', 32)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (275, 'VerificationCriteriaProtocol', 33)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (275, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (276, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (277, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (278, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (278, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (278, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (279, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (279, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (280, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (281, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (281, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (281, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (282, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (283, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (284, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (284, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (285, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (286, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (287, 'VerificationCriteriaProtocol', 21)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (287, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (288, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (289, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (290, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (290, 'VerificationCriteriaProtocol', 25)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (291, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (292, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (293, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (294, 'VerificationCriteriaProtocol', 39)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (294, 'VerificationCriteriaProtocol', 40)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (295, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (295, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (295, 'VerificationCriteriaProtocol', 40)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (295, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (295, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (296, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (296, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (296, 'VerificationCriteriaProtocol', 40)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (297, 'VerificationCriteriaProtocol', 39)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (297, 'VerificationCriteriaProtocol', 40)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (298, 'VerificationCriteriaProtocol', 40)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (298, 'VerificationCriteriaProtocol', 42)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (299, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (299, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (299, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (300, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (300, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (300, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (301, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (301, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (302, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (302, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (303, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (303, 'VerificationCriteriaProtocol', 45)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (304, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (304, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (305, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (306, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (306, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (307, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (308, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (308, 'VerificationCriteriaProtocol', 45)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (309, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (309, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (310, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (310, 'VerificationCriteriaProtocol', 48)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (311, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (311, 'VerificationCriteriaProtocol', 47)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (312, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (312, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (312, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (313, 'VerificationCriteriaProtocol', 19)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (314, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (314, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (314, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (315, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (315, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (315, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (316, 'VerificationCriteriaProtocol', 21)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (317, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (317, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (318, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (318, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (319, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (319, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (320, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (320, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (321, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (321, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (322, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (322, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (323, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (323, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (324, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (324, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (324, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (325, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (325, 'VerificationCriteriaProtocol', 49)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (326, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (326, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (326, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (327, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (327, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (327, 'VerificationCriteriaProtocol', 50)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (328, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (328, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (329, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (329, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (329, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (330, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (330, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (330, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (331, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (331, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (332, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (333, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (333, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (333, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (334, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (335, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (335, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (335, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (335, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (336, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (336, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (336, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (337, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (337, 'VerificationCriteriaProtocol', 51)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (338, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (338, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (339, 'VerificationCriteriaProtocol', 53)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (339, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (340, 'VerificationCriteriaProtocol', 53)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (340, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (341, 'VerificationCriteriaProtocol', 53)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (341, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (342, 'VerificationCriteriaProtocol', 54)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (343, 'VerificationCriteriaProtocol', 54)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (344, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (344, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (345, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (346, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (346, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (347, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (347, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (348, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (348, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (349, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (349, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (350, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (350, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (351, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (351, 'VerificationCriteriaProtocol', 27)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (352, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (352, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (353, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (353, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (353, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (353, 'VerificationCriteriaProtocol', 57)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (354, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (354, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (355, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (355, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (356, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (356, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (357, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (358, 'VerificationCriteriaProtocol', 1)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (358, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (358, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (358, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (358, 'VerificationCriteriaProtocol', 42)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (359, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (360, 'VerificationCriteriaProtocol', 60)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (361, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (361, 'VerificationCriteriaProtocol', 61)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 4)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 61)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (362, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (363, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (364, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (364, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (364, 'VerificationCriteriaProtocol', 18)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (365, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (365, 'VerificationCriteriaProtocol', 27)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (366, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (366, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (366, 'VerificationCriteriaProtocol', 53)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (367, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (367, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (368, 'VerificationCriteriaProtocol', 64)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (368, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (369, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (369, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (370, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (370, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (371, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (371, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (371, 'VerificationCriteriaProtocol', 65)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (372, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (372, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (373, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (374, 'VerificationCriteriaProtocol', 60)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (375, 'VerificationCriteriaProtocol', 26)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (375, 'VerificationCriteriaProtocol', 27)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (376, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (376, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (376, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (377, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (377, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (378, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (378, 'VerificationCriteriaProtocol', 44)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (379, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (379, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (379, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (379, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (380, 'VerificationCriteriaProtocol', 43)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (380, 'VerificationCriteriaProtocol', 46)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (381, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (382, 'VerificationCriteriaProtocol', 12)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (382, 'VerificationCriteriaProtocol', 66)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (383, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (384, 'VerificationCriteriaProtocol', 24)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (385, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (385, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (385, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (386, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (386, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (386, 'VerificationCriteriaProtocol', 68)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (386, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (387, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (387, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (387, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (388, 'VerificationCriteriaProtocol', 19)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (389, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (389, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (389, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (390, 'VerificationCriteriaProtocol', 16)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (390, 'VerificationCriteriaProtocol', 17)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (390, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (391, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (391, 'VerificationCriteriaProtocol', 3)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (391, 'VerificationCriteriaProtocol', 66)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (391, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (392, 'VerificationCriteriaProtocol', 21)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (392, 'VerificationCriteriaProtocol', 5)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (80, 'VerificationCriteriaProtocol', 15)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (80, 'VerificationCriteriaProtocol', 69)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (80, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (81, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (82, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (82, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (83, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (83, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (84, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (84, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (85, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (85, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (86, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (86, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (86, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (87, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (87, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (88, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (88, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (88, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (89, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (89, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (90, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (91, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (91, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (92, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (92, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (93, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (93, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (93, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (93, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (93, 'VerificationCriteriaProtocol', 8)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (94, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (94, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (94, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (94, 'VerificationCriteriaProtocol', 76)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (95, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (95, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (96, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (96, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (96, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (97, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (97, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (98, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (98, 'VerificationCriteriaProtocol', 71)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (98, 'VerificationCriteriaProtocol', 72)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (99, 'VerificationCriteriaProtocol', 2)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (99, 'VerificationCriteriaProtocol', 70)
INSERT INTO dropdown.QualityStandardVerificationCriteria (QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) VALUES (99, 'VerificationCriteriaProtocol', 8)
GO

INSERT INTO dropdown.QualityStandardVerificationCriteria 
	(QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) 
SELECT
	QSVC.QualityStandardID,
	'VerificationCriteriaCategory',

	CASE
		WHEN QSVC.VerificationCriteriaID IN (2,3,8,45,49,53,76)
		THEN (SELECT VCC.VerificationCriteriaCategoryID FROM dropdown.VerificationCriteriaCategory VCC WHERE VCC.VerificationCriteriaCategoryName = 'Interview')
		WHEN QSVC.VerificationCriteriaID IN (4,15,17,21,33,35,37,42,44,56,68,69,70,71,73,74,75)
		THEN (SELECT VCC.VerificationCriteriaCategoryID FROM dropdown.VerificationCriteriaCategory VCC WHERE VCC.VerificationCriteriaCategoryName = 'Observation')
		WHEN QSVC.VerificationCriteriaID IN (1,6,9,13,16,18,20,23,26,28,31,34,36,38,40,43,52,55,58,62,63,66)
		THEN (SELECT VCC.VerificationCriteriaCategoryID FROM dropdown.VerificationCriteriaCategory VCC WHERE VCC.VerificationCriteriaCategoryName = 'Policy/SOPs')
		WHEN QSVC.VerificationCriteriaID IN (5,7,10,11,12,14,19,22,24,25,27,29,30,32,39,41,46,47,48,50,51,54,57,59,60,61,64,65,67,72)
		THEN (SELECT VCC.VerificationCriteriaCategoryID FROM dropdown.VerificationCriteriaCategory VCC WHERE VCC.VerificationCriteriaCategoryName = 'Records')
	END

FROM dropdown.QualityStandardVerificationCriteria QSVC
WHERE QSVC.VerificationCriteriaCode = 'VerificationCriteriaProtocol'
GO	

INSERT INTO dropdown.QualityStandardVerificationCriteria 
	(QualityStandardID, VerificationCriteriaCode, VerificationCriteriaID) 
SELECT
	QSVC.QualityStandardID,
	'VerificationCriteriaSubCategory',

	CASE
		WHEN QSVC.VerificationCriteriaID IN (5,62,63,64,65)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Employees')
		WHEN QSVC.VerificationCriteriaID IN (1,13,16,55,58)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Facility')
		WHEN QSVC.VerificationCriteriaID IN (50,51,59,60,61)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Facility maintenance')
		WHEN QSVC.VerificationCriteriaID IN (3,8,45,53)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Head')
		WHEN QSVC.VerificationCriteriaID IN (35,42)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Intensive care rooms')
		WHEN QSVC.VerificationCriteriaID IN (11,12,14)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Labor room')
		WHEN QSVC.VerificationCriteriaID IN (18,26)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Manuals/guidelines')
		WHEN QSVC.VerificationCriteriaID IN (46,47,48)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Medication')
		WHEN QSVC.VerificationCriteriaID IN (2,20,36,38,40,43,49,52,66,76)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Other')
		WHEN QSVC.VerificationCriteriaID IN (19,22,24,25,27,29,30,32,39,41,67,72)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Other medical records')
		WHEN QSVC.VerificationCriteriaID IN (17,21,33)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Other procedure rooms')
		WHEN QSVC.VerificationCriteriaID IN (7,10,54,57)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Other records')
		WHEN QSVC.VerificationCriteriaID IN (6,9,23,28,31,34)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Patient')
		WHEN QSVC.VerificationCriteriaID IN (44,68)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Pharmacy')
		WHEN QSVC.VerificationCriteriaID IN (37,73,75)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Post-natal')
		WHEN QSVC.VerificationCriteriaID IN (15,69,70,74)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Rooms for before and during birth')
		WHEN QSVC.VerificationCriteriaID IN (4,56,71)
		THEN (SELECT VCSC.VerificationCriteriaSubCategoryID FROM dropdown.VerificationCriteriaSubCategory VCSC WHERE VCSC.VerificationCriteriaSubCategoryName = 'Rooms near entry')
	END

FROM dropdown.QualityStandardVerificationCriteria QSVC
WHERE QSVC.VerificationCriteriaCode = 'VerificationCriteriaProtocol'
GO	
--End table dropdown.QualityStandardVerificationCriteria

--Begin table dropdown.VisitOutcome
TRUNCATE TABLE dropdown.VisitOutcome
GO

EXEC utility.InsertIdentityValue 'dropdown.VisitOutcome', 'VisitOutcomeID', 0
GO

INSERT INTO dropdown.VisitOutcome 
	(VisitOutcomeName, HasNote)
VALUES
	('Assessment Completed', 0),
	('Assessment Partially Completed', 0),
	('Facility Cannot Be Located', 0),
	('Refusal (Specify Reason)', 1),
	('Other (Specify Reason)', 1)
GO
--End table dropdown.VisitOutcome

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Display the facility name and district in the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List.ShowFacilityName', @PERMISSIONCODE='ShowFacilityName';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Display the facility name and district in the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitReportList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitReportList.ShowFacilityName', @PERMISSIONCODE='ShowFacilityName';
GO

UPDATE PP
SET PP.PermissionableLineage = 'Visit.AddUpdate.VisitPlan'
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'VisitPlan.AddUpdate'

UPDATE PP
SET PP.PermissionableLineage = 'Visit.AddUpdate.VisitReport'
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'VisitReport.AddUpdate'

UPDATE PP
SET PP.PermissionableLineage = 'Visit.View.VisitPlan'
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage IN ('Visit.View', 'VisitPlan.View')

UPDATE PP
SET PP.PermissionableLineage = 'Visit.View.VisitReport'
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage IN ('Visit.View', 'VisitReport.View')

DELETE PP
FROM person.PersonPermissionable PP
WHERE PP.PermissionableLineage IN 
	(
	'Visit.AddUpdate',
	'Visit.List',
	'Visit.QualityAssessment',
	'Visit.SaveQualityAssessment',
	'Visit.SaveQualityStandardAssessment',
	'Visit.View',
	'VisitPlan.AddUpdate',
	'VisitPlan.List',
	'VisitPlan.View',
	'VisitReport.AddUpdate',
	'VisitReport.List',
	'VisitReport.View'
	)
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End file Build File - 04 - Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.10 - 2018.10.29 18.19.52')
GO
--End build tracking

