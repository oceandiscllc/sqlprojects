-- File Name:	Build - 1.6 - MNH.sql
-- Build Key:	Build - 1.6 - 2018.05.09 17.32.57

USE MNH
GO

-- ==============================================================================================================================
-- Schemas:
--		webapi
--
-- Tables:
--		webapi.Application
--		webapi.TransactionLog
--		webapi.TransactionStatus
--		webapi.TransactionType
--
-- Procedures:
--		document.GetFileTypeData
--		dropdown.GetAssessmentEvidenceTypeData
--		dropdown.GetContactFunctionData
--		dropdown.GetContactRoleData
--		dropdown.GetDIBRoleData
--		dropdown.GetDocumentCategoryData
--		dropdown.GetFacilityServiceData
--		dropdown.GetGenderData
--		dropdown.GetInclusionCriteriaData
--		dropdown.GetProjectData
--		dropdown.GetRoleData
--		facility.GetFacilityByFacilityID
--		utility.DropDefaultConstraint
--		visit.GetVisitByVisitID
--		visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
/* Build File - 00 - Prerequisites */
USE MNH
GO

--Begin procedure utility.DropDefaultConstraint
EXEC utility.DropObject 'utility.DropDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
	FROM sys.Default_Constraints DC 
		JOIN sys.Objects O ON O.Object_ID = DC.Parent_Object_ID
			AND O.type = 'U' 
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID 
		JOIN sys.Columns C ON C.Object_ID = O.Object_ID 
			AND C.Column_ID = DC.Parent_Column_ID
			AND S.Name + '.' + O.Name = @TableName 
			AND C.Name = @ColumnName

	IF @cSQL IS NOT NULL
		EXECUTE (@cSQL)
	--ENDIF
		
END
GO
--End procedure utility.DropDefaultConstraint
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE MNH
GO

EXEC utility.AddSchema 'webapi'
GO

--Begin table visit.Visit
DECLARE @TableName VARCHAR(250) = 'visit.Visit'

EXEC utility.AddColumn @TableName, 'VisitLeadPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'VisitTime', 'NUMERIC(5,2)', '0'
GO
--End table visit.Visit

--Begin table dropdown.AssessmentEvidenceType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssessmentEvidenceType'

EXEC utility.DropColumn @TableName, 'AssessmentEvidenceTypeDataType'
GO
--End table dropdown.AssessmentEvidenceType

--Begin table visit.VisitQualityStandard
DECLARE @TableName VARCHAR(250) = 'visit.VisitQualityStandard'

EXEC utility.DropDefaultConstraint @TableName, 'HasObservation'
EXEC utility.DropDefaultConstraint @TableName, 'HasPhysicalVerification'
EXEC utility.DropDefaultConstraint @TableName, 'HasProviderInterview'

ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasObservation INT
ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasPhysicalVerification INT
ALTER TABLE visit.VisitQualityStandard ALTER COLUMN HasProviderInterview INT

EXEC utility.SetDefaultConstraint @TableName, 'HasObservation', 'INT', '-1', 1
EXEC utility.SetDefaultConstraint @TableName, 'HasPhysicalVerification', 'INT', '-1', 1
EXEC utility.SetDefaultConstraint @TableName, 'HasProviderInterview', 'INT', '-1', 1
GO
--End table visit.VisitQualityStandard

--Begin table webapi.Application
DECLARE @TableName VARCHAR(250) = 'webapi.Application'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.Application
	(
	ApplicationID INT IDENTITY(1,1) NOT NULL,
	ApplicationKey VARCHAR(36),
	ApplicationSecretKey VARCHAR(36),
	ApplicationName VARCHAR(50),
	CreateDateTime DATETIME,
	UpdateDateTime DATETIME,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationKey', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationSecretKey', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Application', 'ApplicationName', 'ApplicationID'
GO
--End table webapi.Application

--Begin table webapi.TransactionStatus
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionStatus'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionStatus
	(
	TransactionStatusID INT IDENTITY(1,1) NOT NULL,
	TransactionStatusName VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionStatus', 'TransactionStatusName', 'TransactionStatusID'
GO
--End table webapi.TransactionType

--Begin table webapi.TransactionType
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionType'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionType
	(
	TransactionTypeID INT IDENTITY(1,1) NOT NULL,
	TransactionTypeName VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionType', 'TransactionTypeName', 'TransactionTypeID'
GO
--End table webapi.TransactionType

--Begin table webapi.TransactionLog
DECLARE @TableName VARCHAR(250) = 'webapi.TransactionLog'

EXEC utility.DropObject @TableName

CREATE TABLE webapi.TransactionLog
	(
	TransactionLogID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	TransactionGUID VARCHAR(36),
	TransactionTypeID INT,
	TransactionStatusID INT,
	TransactionDetail VARCHAR(MAX),
	TransactionPayload VARCHAR(MAX),
	TransactionContentType VARCHAR(25),
	TransactionContentSubType VARCHAR(25),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionGUID', 'VARCHAR(36)', 'newID()'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TransactionStatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TransactionLogID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TransactionLog', 'ApplicationID', 'TransactionLogID'
GO
--End table webapi.TransactionLog
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE MNH
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE MNH
GO

--Begin procedure document.GetFileTypeData
EXEC utility.DropObject 'document.GetFileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.08
-- Description:	A stored procedure to get data from the document.FileType table
-- ============================================================================
CREATE PROCEDURE document.GetFileTypeData

@MimeType VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		FT.Extension,
		LEFT(FT.MimeType, CHARINDEX('/', FT.MimeType) - 1) AS ContentType,
		REVERSE(LEFT(REVERSE(FT.MimeType), CHARINDEX('/', REVERSE(FT.MimeType)) - 1)) AS ContentSubType
	FROM document.FileType FT
	WHERE FT.MimeType = @MimeType

END
GO
--End procedure document.GetFileTypeData

--Begin procedure dropdown.GetAssessmentEvidenceTypeData
EXEC Utility.DropObject 'dropdown.GetAssessmentEvidenceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to return data from the dropdown.AssessmentEvidenceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetAssessmentEvidenceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssessmentEvidenceTypeID, 
		T.AssessmentEvidenceTypeCode,
		T.AssessmentEvidenceTypeName
	FROM dropdown.AssessmentEvidenceType T
	WHERE (T.AssessmentEvidenceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssessmentEvidenceTypeName, T.AssessmentEvidenceTypeID

END
GO
--End procedure dropdown.GetAssessmentEvidenceTypeData

--Begin procedure dropdown.GetContactFunctionData
EXEC Utility.DropObject 'dropdown.GetContactFunctionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactFunctionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactFunctionID, 
		T.ContactFunctionCode, 
		T.ContactFunctionName,
		T.DisplayOrder
	FROM dropdown.ContactFunction T
	WHERE (T.ContactFunctionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactFunctionName, T.ContactFunctionID

END
GO
--End procedure dropdown.GetContactFunctionData

--Begin procedure dropdown.GetContactRoleData
EXEC Utility.DropObject 'dropdown.GetContactRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactRole table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactRoleID, 
		T.ContactRoleCode, 
		T.ContactRoleName,
		T.DisplayOrder
	FROM dropdown.ContactRole T
	WHERE (T.ContactRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactRoleName, T.ContactRoleID

END
GO
--End procedure dropdown.GetContactRoleData

--Begin procedure dropdown.GetDIBRoleData
EXEC Utility.DropObject 'dropdown.GetDIBRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.DIBRole table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetDIBRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DIBRoleID, 
		T.DIBRoleName,
		T.DisplayOrder
	FROM dropdown.DIBRole T
	WHERE (T.DIBRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DIBRoleName, T.DIBRoleID

END
GO
--End procedure dropdown.GetDIBRoleData

--Begin procedure dropdown.GetDocumentCategoryData
EXEC Utility.DropObject 'dropdown.GetDocumentCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.DocumentCategory table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetDocumentCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentCategoryID, 
		T.DocumentCategoryName,
		T.DisplayOrder
	FROM dropdown.DocumentCategory T
	WHERE (T.DocumentCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DocumentCategoryName, T.DocumentCategoryID

END
GO
--End procedure dropdown.GetDocumentCategoryData

--Begin procedure dropdown.GetFacilityServiceData
EXEC Utility.DropObject 'dropdown.GetFacilityServiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityService table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetFacilityServiceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityServiceID, 
		T.FacilityServiceName,
		T.DisplayOrder
	FROM dropdown.FacilityService T
	WHERE (T.FacilityServiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityServiceName, T.FacilityServiceID

END
GO
--End procedure dropdown.GetFacilityServiceData

--Begin procedure dropdown.GetInclusionCriteriaData
EXEC Utility.DropObject 'dropdown.GetInclusionCriteriaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.InclusionCriteria table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetInclusionCriteriaData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionCriteriaID, 
			T.InclusionCriteriaName,
			T.DisplayOrder
	FROM dropdown.InclusionCriteria T
	WHERE (T.InclusionCriteriaID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionCriteriaName, T.InclusionCriteriaID

END
GO
--End procedure dropdown.GetInclusionCriteriaData

--Begin procedure dropdown.GetGenderData
EXEC Utility.DropObject 'dropdown.GetGenderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.10.15
-- Description:	A stored procedure to return data from the dropdown.Gender table
-- =============================================================================
CREATE PROCEDURE dropdown.GetGenderData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GenderID,
		T.GenderName,
		T.DisplayOrder
	FROM dropdown.Gender T
	WHERE (T.GenderID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GenderName, T.GenderID

END
GO
--End procedure dropdown.GetGenderData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetProjectData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName,
		T.DisplayOrder
	FROM dropdown.Project T
	WHERE (T.ProjectID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID, 
		T.RoleName,
		T.DisplayOrder
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.AuthorizerPersonID,
		person.FormatPersonNameByPersonID(F.AuthorizerPersonID, 'LastFirstTitle') AS AuthorizerPersonNameFormatted,		
		F.BedCount,
		F.Description, 			
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.FacilityID,
		core.FormatDate(eventlog.GetFirstCreateDateTime('Facility', F.FacilityID)) AS FacilityCreateDateFormatted,
		core.FormatDate(eventlog.GetLastCreateDateTime('Facility', F.FacilityID)) AS FacilityUpdateDateFormatted,
		F.FacilityName,
		F.InclusionCriteriaNotes,
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = facility.GetFacilityInclusionEligibilityStatusID(@FacilityID)
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	IF @FacilityID = 0
		BEGIN

		SELECT
			NULL AS AssessmentDate, 
			NULL AS AssessmentDateFormatted,
			0 AS FacilityInclusionCriteriaID, 
			0 AS IsAuthorized,
			0 AS IsCriteriaMet, 
			0 AS PersonID,
			NULL AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM dropdown.InclusionCriteria IC
		WHERE IC.InclusionCriteriaID > 0
			AND IC.IsActive = 1
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	ELSE
		BEGIN

		SELECT
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.AssessmentDate ELSE NULL END AS AssessmentDate, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN core.FormatDate(FIC.AssessmentDate) ELSE NULL END AS AssessmentDateFormatted,
			FIC.FacilityInclusionCriteriaID, 
			FIC.IsAuthorized,
			FIC.IsCriteriaMet, 
			CASE FIC.IsCriteriaMet WHEN 1 THEN FIC.PersonID ELSE 0 END AS PersonID,
			CASE FIC.IsCriteriaMet WHEN 1 THEN person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') ELSE NULL END AS PersonNameFormatted,
			IC.InclusionCriteriaID, 
			IC.InclusionCriteriaName
		FROM facility.FacilityInclusionCriteria FIC
			JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
			JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
				AND FIC.FacilityID = @FacilityID
		ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

		END
	--ENDIF

	--FacilityFacilityInclusionCriteriaDocument
	SELECT
		REPLACE(DE.EntityTypeSubCode, 'InclusionCriteria', '') AS InclusionCriteriaID,
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode LIKE 'InclusionCriteria%' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID

	--FacilityMOUDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Facility' 
			AND DE.EntityTypeSubCode = 'MOU' 
			AND DE.EntityID = @FacilityID
	ORDER BY D.DocumentTitle, D.DocumentID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure visit.GetVisitByVisitID
EXEC Utility.DropObject 'visit.GetVisitByVisitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.09
-- Description:	A stored procedure to get data from the visit.Visit table
-- ======================================================================
CREATE PROCEDURE visit.GetVisitByVisitID

@VisitID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Visit
	SELECT 
		F.FacilityID,
		F.FacilityName,
		V.IsPlanningComplete,
		V.IsVisitComplete,

		CASE 
			WHEN V.IsPlanningComplete = 0
			THEN 'Planning'
			WHEN V.IsVisitComplete = 1
			THEN 'Visit Complete'
			ELSE 'Planning Complete'
		END AS VisitStatusName,

		V.VisitStartDateTime,
		core.FormatDate(V.VisitStartDateTime) AS VisitStartDateFormatted,
		V.VisitID, 	
		V.VisitLeadPersonID,
		person.FormatPersonNameByPersonID(V.VisitLeadPersonID, 'LastFirst') AS VisitLeadPersonNameFormatted,
		V.VisitPlanNotes,
		V.VisitPlanUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitPlanUpdatePersonID, 'LastFirst') AS VisitPlanPersonNameFormatted,
		V.VisitPurposeID,
		V.VisitReportNotes,
		V.VisitReportUpdatePersonID,
		person.FormatPersonNameByPersonID(V.VisitReportUpdatePersonID, 'LastFirst') AS VisitReportPersonNameFormatted,
		V.VisitTime,
		VP.VisitPurposeName
	FROM visit.Visit V
		JOIN facility.Facility F ON F.FacilityID = V.FacilityID
		JOIN dropdown.VisitPurpose VP ON VP.VisitPurposeID = V.VisitPurposeID
			AND V.VisitID = @VisitID

	--VisitContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM visit.VisitContact VC
		JOIN contact.Contact C ON C.ContactID = VC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND VC.VisitID = @VisitID
	ORDER BY 2, 1

	--VisitQualityStandard
	SELECT 
		QSL.ChapterID, 
		QSL.ChapterName, 
		QSL.EntityTypeCode, 
		QSL.ManualID, 
		QSL.ManualName, 
		QSL.ObjectiveElementID, 
		QSL.ObjectiveElementName,
		QSL.QualityStandardID,
		QSL.StandardID,
		QSL.StandardName,
		QSL.VerificationCriteriaID,
		QSL.VerificationCriteriaName,
		VQS.Score,
		VQS.IsScored,
		visit.GetPriorVisitQualityStandardScore(QSL.QualityStandardID, @VisitID) AS PriorScore
	FROM dropdown.QualityStandardLookup QSL
		JOIN visit.VisitQualityStandard VQS ON VQS.QualityStandardID = QSL.QualityStandardID
			AND VQS.VisitID = @VisitID
	ORDER BY QSL.ManualName, QSL.ManualID, QSL.ChapterName, QSL.ChapterID, QSL.StandardName, QSL.StandardID, QSL.ObjectiveElementName, QSL.ObjectiveElementID, QSL.VerificationCriteriaName, QSL.VerificationCriteriaID

END
GO
--End procedure visit.GetVisitByVisitID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT
		QSL.ManualName,
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.IsScored,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
		JOIN dropdown.QualityStandardLookup QSL ON QSL.QualityStandardID = VQS.QualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	SELECT
		ISNULL((
			SELECT
				D.DocumentTitle,
				DE.DocumentEntityCode
			FROM document.DocumentEntity DE
				JOIN document.Document D ON D.DocumentID = DE.DocumentID
					AND DE.EntityTypeCode = 'VisitQualityStandard'
					AND DE.EntityID = 
						(
						SELECT VQS.VisitQualityStandardID 
						FROM visit.VisitQualityStandard VQS
						WHERE VQS.VisitID = @VisitID
							AND VQS.QualityStandardID = @QualityStandardID
						)
			FOR JSON PATH
		), '[]') AS UploadedFiles

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE MNH
GO

DECLARE @cEntityName VARCHAR(MAX) = '6.3.1 Explains danger signs and important care activities to mother and her companion'

IF NOT EXISTS (SELECT 1 FROM dropdown.QualityStandard QS WHERE QS.EntityName = @cEntityName)
	BEGIN

	INSERT INTO dropdown.QualityStandard
		(ParentQualityStandardID,EntityTypeCode,EntityName,DisplayOrder)
	SELECT
		QS.QualityStandardID,
		'Verification Criteria',
		@cEntityName,
		1
	FROM dropdown.QualityStandard QS
	WHERE QS.EntityTypeCode = 'Objective Element'
		AND QS.EntityName = '6.3 Explains danger signs and important care activities to mother and her companion'

	END
--ENDIF

DELETE QSAET
FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
	JOIN dropdown.QualityStandard QS ON QS.QualityStandardID = QSAET.QualityStandardID
		AND QS.EntityTypeCode = 'Verification Criteria'
		AND QS.EntityName = @cEntityName

INSERT INTO dropdown.QualityStandardAssessmentEvidenceType
	(QualityStandardID, AssessmentEvidenceTypeID)
SELECT
	QS.QualityStandardID,
	AET.AssessmentEvidenceTypeID
FROM dropdown.AssessmentEvidenceType AET, dropdown.QualityStandard QS
WHERE QS.EntityTypeCode = 'Verification Criteria'
	AND QS.EntityName = @cEntityName
	AND AET.AssessmentEvidenceTypeCode IN ('HasObservation', 'HasProviderInterview')
GO

TRUNCATE TABLE webapi.Application
GO

INSERT INTO webapi.Application
	(ApplicationName)
VALUES 
	('MNH Mobile BuildKey'),
	('MNH WebApi')
GO

TRUNCATE TABLE webapi.TransactionStatus
GO

INSERT INTO webapi.TransactionStatus
	(TransactionStatusName)
VALUES
	('OK'),
	('ERROR'),
	('TIMEOUT'),
	('WARN')
GO

TRUNCATE TABLE webapi.TransactionType
GO

INSERT INTO webapi.TransactionType
	(TransactionTypeName)
VALUES 
	('HTTP-POST'),
	('HTTP-PUT'),
	('HTTPS-POST'),
	('HTTPS-PUT'),
	('SFTP-PULL'),
	('SFTP-PUSH')
GO

DECLARE @cSystemSetupValue VARCHAR(MAX) = (SELECT A.ApplicationKey FROM webapi.Application A WHERE ApplicationName = 'MNH WebApi')
EXEC core.SystemSetupAddUpdate 'WebApiApplicationKey', NULL, @cSystemSetupValue
GO

DECLARE @cSystemSetupValue VARCHAR(MAX) = (SELECT A.ApplicationSecretKey FROM webapi.Application A WHERE ApplicationName = 'MNH WebApi')
EXEC core.SystemSetupAddUpdate 'WebApiApplicationSecretKey', NULL, @cSystemSetupValue
GO

EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/mnhnewlogo.jpg'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/mnhnewlogo.jpg'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/mnhnewlogo.jpg'
GO
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
EXEC permissionable.SavePermissionableGroup 'Visits', 'Visits', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Allows access to the authorized checkbox on the inclusion criteria tab for inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate.CanAuthorizeInclusionCriteria', @PERMISSIONCODE='CanAuthorizeInclusionCriteria';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='Add / edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View the list of quality assessments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Add / edit a visit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='QualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.QualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityStandardAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityStandardAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitPlanList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitPlanList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitReportList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitReportList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='Add / edit a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='Add / edit a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.6 - 2018.05.09 17.32.57')
GO
--End build tracking

