-- File Name:	Build - 1.0 - MNH.sql
-- Build Key:	Build - 1.0 - 2017.09.13 19.32.11

USE MNH
GO

-- ==============================================================================================================================
-- Functions:
--		core.GetDescendantMenuItemsByMenuItemCode
--		core.GetSystemSetupValueBySystemSetupKey
--		eventlog.GetPersonPermissionablesXMLByPersonID
--		facility.GetFacilityInclusionCriteriaMetCount
--		facility.GetLastFacilityInclusionCriteriaAssessmentDate
--
-- Procedures:
--		contact.GetContactByContactID
--		core.DeleteAnnouncementByAnnouncementID
--		core.EntityTypeAddUpdate
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		core.GetAnnouncementsByDate
--		core.GetEmailTemplateByEmailTemplateID
--		core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode
--		core.GetEmailTemplateFieldsByEntityTypeCode
--		core.GetMenuItemsByPersonID
--		core.GetSystemSetupDataBySystemSetupID
--		core.GetSystemSetupValuesBySystemSetupKey
--		core.GetSystemSetupValuesBySystemSetupKeyList
--		core.MenuItemAddUpdate
--		core.SystemSetupAddUpdate
--		core.UpdateParentPermissionableLineageByMenuItemCode
--		dropdown.GetContactFunctionData
--		dropdown.GetContactRoleData
--		dropdown.GetControllerData
--		dropdown.GetDIBRoleData
--		dropdown.GetEntityTypeNameData
--		dropdown.GetEventCodeNameData
--		dropdown.GetFacilityServiceData
--		dropdown.GetFacilityStatusData
--		dropdown.GetGraduationPlanStepData
--		dropdown.GetInclusionCriteriaData
--		dropdown.GetInclusionEligibilityStatusData
--		dropdown.GetMethodData
--		dropdown.GetMOUStatusData
--		dropdown.GetRoleData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogContactAction
--		eventlog.LogFacilityAction
--		eventlog.LogPermissionableTemplateAction
--		facility.GetFacilityByFacilityID
--		facility.GetFacilityGraduationPlanStepsByFacilityID
--		facility.GetFacilityInclusionCriteriaByFacilityID
--		facility.GetFacilityMOUByFacilityID
--		territory.GetParentTerritoryByProjectID
--		territory.GetTerritoryByProjectID
--		territory.GetTerritoryByTerritoryID
--		utility.InsertIdentityValue
--
-- Schemas:
--		facility
--
-- Tables:
--		core.EntityType
--		core.MenuItem
--		core.MenuItemPermissionableLineage
--		core.SystemSetup
--		dropdown.ContactFunction
--		dropdown.ContactRole
--		dropdown.DIBRole
--		dropdown.FacilityService
--		dropdown.FacilityStatus
--		dropdown.GraduationPlanStep
--		dropdown.InclusionCriteria
--		dropdown.InclusionEligibilityStatus
--		dropdown.MOUStatus
--		dropdown.Role
--		facility.Facility
--		facility.FacilityFacilityService
--		facility.FacilityGraduationPlanStep
--		facility.FacilityInclusionCriteria
--		facility.FacilityMOUContact
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
/* Build File - 00 - Prerequisites */
USE MNH
GO

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.09.03
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

EXEC utility.AddSchema 'facility'
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE MNH
GO

EXEC utility.DropObject 'mou.MOU'
EXEC utility.DropObject 'mou'
GO

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'ContactFunctionID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'ContactRoleID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DIBRoleID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'FacilityID', 'INT', '0'

EXEC utility.DropColumn @TableName, 'AssetID'
GO
--End table contact.Contact

--Begin table contact.ContactFacility
DECLARE @TableName VARCHAR(250) = 'contact.ContactFacility'

EXEC utility.DropObject @TableName
GO
--End table contact.ContactFacility

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.MenuItem
DECLARE @TableName VARCHAR(250) = 'core.MenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItem
	(
	MenuItemID INT IDENTITY(1,1) NOT NULL,
	ParentMenuItemID INT,
	MenuItemCode VARCHAR(50),
	MenuItemText VARCHAR(250),
	MenuItemLink VARCHAR(500),
	DisplayOrder INT,
	Icon VARCHAR(50),
	IsForNewTab BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsForNewTab', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentMenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MenuItemID'
GO
--End table core.MenuItem

--Begin table core.MenuItemPermissionableLineage
DECLARE @TableName VARCHAR(250) = 'core.MenuItemPermissionableLineage'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemPermissionableLineage
	(
	MenuItemPermissionableLineageID INT IDENTITY(1,1) NOT NULL,
	MenuItemID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemPermissionableLineageID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemPermissionable', 'MenuItemID'
GO
--End table core.MenuItemPermissionableLineage

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	SystemSetupBinaryValue VARBINARY(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table dropdown.ContactFunction
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactFunction'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactFunction
	(
	ContactFunctionID INT IDENTITY(0,1) NOT NULL,
	ContactFunctionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactFunctionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactFunction', 'DisplayOrder,ContactFunctionName', 'ContactFunctionID'
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactRole
	(
	ContactRoleID INT IDENTITY(0,1) NOT NULL,
	ContactRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactRole', 'DisplayOrder,ContactRoleName', 'ContactRoleID'
GO
--End table dropdown.ContactRole

--Begin table dropdown.DIBRole
DECLARE @TableName VARCHAR(250) = 'dropdown.DIBRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DIBRole
	(
	DIBRoleID INT IDENTITY(0,1) NOT NULL,
	DIBRoleName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DIBRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DIBRole', 'DisplayOrder,DIBRoleName', 'DIBRoleID'
GO
--End table dropdown.DIBRole

--Begin table dropdown.FacilityService
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityService'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityService
	(
	FacilityServiceID INT IDENTITY(0,1) NOT NULL,
	FacilityServiceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityServiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityService', 'DisplayOrder,FacilityServiceName', 'FacilityServiceID'
GO
--End table dropdown.FacilityService

--Begin table dropdown.FacilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.FacilityStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FacilityStatus
	(
	FacilityStatusID INT IDENTITY(0,1) NOT NULL,
	FacilityStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FacilityStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityStatus', 'DisplayOrder,FacilityStatusName', 'FacilityStatusID'
GO
--End table dropdown.FacilityStatus

--Begin table dropdown.GraduationPlanStep
DECLARE @TableName VARCHAR(250) = 'dropdown.GraduationPlanStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.GraduationPlanStep
	(
	GraduationPlanStepID INT IDENTITY(0,1) NOT NULL,
	GraduationPlanStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'GraduationPlanStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_GraduationPlanStep', 'DisplayOrder,GraduationPlanStepName', 'GraduationPlanStepID'
GO
--End table dropdown.GraduationPlanStep

--Begin table dropdown.InclusionCriteria
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InclusionCriteria
	(
	InclusionCriteriaID INT IDENTITY(0,1) NOT NULL,
	InclusionCriteriaName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InclusionCriteriaID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InclusionCriteria', 'DisplayOrder,InclusionCriteriaName', 'InclusionCriteriaID'
GO
--End table dropdown.InclusionCriteria

--Begin table dropdown.InclusionEligibilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionEligibilityStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InclusionEligibilityStatus
	(
	InclusionEligibilityStatusID INT IDENTITY(0,1) NOT NULL,
	InclusionEligibilityStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InclusionEligibilityStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InclusionEligibilityStatus', 'DisplayOrder,InclusionEligibilityStatusName', 'InclusionEligibilityStatusID'
GO
--End table dropdown.InclusionEligibilityStatus

--Begin table dropdown.MOUStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.MOUStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MOUStatus
	(
	MOUStatusID INT IDENTITY(0,1) NOT NULL,
	MOUStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MOUStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MOUStatus', 'DisplayOrder,MOUStatusName', 'MOUStatusID'
GO
--End table dropdown.MOUStatus

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role

--Begin table facility.Facility
DECLARE @TableName VARCHAR(250) = 'facility.Facility'

EXEC utility.DropObject @TableName

CREATE TABLE facility.Facility
	(
	FacilityID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	FacilityName NVARCHAR(250),
	Address VARCHAR(MAX),
	Description VARCHAR(MAX),
	Phone VARCHAR(50),
	FacilityStatusID INT,
	Location GEOMETRY,
	PrimaryContactID INT,
	MOUDate DATE,
	MOUNotes VARCHAR(MAX),
	MOUStatusID INT,
	GraduationPlanUpdateDate DATE,
	GraduationPlanUpdatePersonID INT,
	InclusionCriteriaNotes VARCHAR(MAX),
	InclusionEligibilityStatusID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GraduationPlanUpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InclusionEligibilityStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MOUStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PrimaryContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Facility', 'FacilityName'
GO
--End table facility.Facility

--Begin trigger facility.TR_Facility

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09..5
-- Description:	A trigger to update the facility.Facility tables
-- ========================================================================
CREATE TRIGGER facility.TR_Facility ON facility.Facility FOR INSERT, DELETE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM DELETED)
	BEGIN

	DELETE FGPS
	FROM facility.FacilityGraduationPlanStep 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FGPS.FacilityID
		)

	DELETE FIC
	FROM facility.FacilityInclusionCriteria 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM facility.Facility F
		WHERE F.FacilityID = FIC.FacilityID
		)

	END
--ENDIF

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO facility.FacilityGraduationPlanStep
		(FacilityID, GraduationPlanStepID)
	SELECT
		I.FacilityID,
		GPS.GraduationPlanStepID
	FROM INSERTED I, dropdown.GraduationPlanStep GPS
	WHERE GPS.GraduationPlanStepID > 0

	INSERT INTO facility.FacilityInclusionCriteria
		(FacilityID, InclusionCriteriaID)
	SELECT
		I.FacilityID,
		IC.InclusionCriteriaID
	FROM INSERTED I, dropdown.InclusionCriteria IC
	WHERE IC.InclusionCriteriaID > 0

	END
--ENDIF

GO		
--End trigger facility.TR_Facility

--Begin table facility.FacilityFacilityService
DECLARE @TableName VARCHAR(250) = 'facility.FacilityFacilityService'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityFacilityService
	(
	FacilityFacilityServiceID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	FacilityServiceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityServiceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityFacilityServiceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityFacilityService', 'FacilityID,FacilityServiceID'
GO
--End table facility.FacilityFacilityService

--Begin table facility.FacilityGraduationPlanStep
DECLARE @TableName VARCHAR(250) = 'facility.FacilityGraduationPlanStep'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityGraduationPlanStep
	(
	FacilityGraduationPlanStepID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	GraduationPlanStepID INT,
	OriginalTargetDate DATE,
	CurrentTargetDate DATE,
	IsComplete BIT,
	PersonID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GraduationPlanStepID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityGraduationPlanStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityGraduationPlanStep', 'FacilityID,GraduationPlanStepID'
GO
--End table facility.FacilityGraduationPlanStep

--Begin table facility.FacilityInclusionCriteria
DECLARE @TableName VARCHAR(250) = 'facility.FacilityInclusionCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityInclusionCriteria
	(
	FacilityInclusionCriteriaID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	InclusionCriteriaID INT,
	IsCriteriaMet BIT,
	AssessmentDate DATE,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InclusionCriteriaID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsCriteriaMet', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityInclusionCriteriaID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityInclusionCriteria', 'FacilityID,InclusionCriteriaID'
GO
--End table facility.FacilityInclusionCriteria

--Begin table facility.FacilityMOUContact
DECLARE @TableName VARCHAR(250) = 'facility.FacilityMOUContact'

EXEC utility.DropObject @TableName

CREATE TABLE facility.FacilityMOUContact
	(
	FacilityMOUContactID INT IDENTITY(1,1) NOT NULL,
	FacilityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FacilityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FacilityMOUContactID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FacilityMOUContact', 'FacilityID,ContactID'
GO
--End table facility.FacilityMOUContact
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE MNH
GO

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function eventlog.GetPersonPermissionablesXMLByPersonID
EXEC utility.DropObject 'eventlog.GetPersonPermissionablesXMLByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A function to return PersonPermissionable data for a specific Person record
-- ========================================================================================

CREATE FUNCTION eventlog.GetPersonPermissionablesXMLByPersonID
(
@PersonID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
	
	SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
		FROM person.PersonPermissionable T 
		WHERE T.PersonID = @PersonID
		) D

	RETURN '<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>'

END

GO
--End function eventlog.GetPersonPermissionablesXMLByPersonID

--Begin function facility.GetFacilityInclusionCriteriaMetCount
EXEC utility.DropObject 'facility.GetFacilityInclusionCriteriaMetCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A function to data from the facility.FacilityInclusionCriteria table
-- =================================================================================

CREATE FUNCTION facility.GetFacilityInclusionCriteriaMetCount
(
@FacilityID INT
)

RETURNS VARCHAR(20)

AS
BEGIN
	
	DECLARE @nIsMetCount INT

	;
	WITH FICD AS
		(
		SELECT 
			COUNT(FIC.InclusionCriteriaID) AS IsCriteriaMetCount,
			FIC.IsCriteriaMet AS IsCriteriaMet
		FROM facility.FacilityInclusionCriteria FIC
		WHERE FIC.FacilityID = @FacilityID
		GROUP BY FIC.IsCriteriaMet
		)

	SELECT @nIsMetCount = FICD.IsCriteriaMetCount FROM FICD WHERE FICD.IsCriteriaMet = 1

	RETURN CAST(ISNULL(@nIsMetCount, 0) AS VARCHAR(5)) + ' of ' + CAST((SELECT COUNT(FIC.FacilityInclusionCriteriaID) FROM facility.FacilityInclusionCriteria FIC WHERE FIC.FacilityID = @FacilityID) AS VARCHAR(5))

END
GO
--End function facility.GetFacilityInclusionCriteriaMetCount

--Begin function facility.GetLastFacilityInclusionCriteriaAssessmentDate
EXEC utility.DropObject 'facility.GetLastFacilityInclusionCriteriaAssessmentDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A function to data from the facility.FacilityInclusionCriteria table
-- =================================================================================

CREATE FUNCTION facility.GetLastFacilityInclusionCriteriaAssessmentDate
(
@FacilityID INT
)

RETURNS DATE

AS
BEGIN
	
	DECLARE @dAssessmentDate DATE

	SELECT @dAssessmentDate = MAX(FIC.AssessmentDate)
	FROM facility.FacilityInclusionCriteria FIC
	WHERE FIC.FacilityID = @FacilityID

	RETURN @dAssessmentDate

END
GO
--End function facility.GetLastFacilityInclusionCriteriaAssessmentDate

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures - EventLog.sql
/* Build File - 03 - Procedures - EventLog */
USE MNH
GO

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.05
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Contact'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM contact.Contact T
		WHERE T.ContactID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogFacilityAction
EXEC utility.DropObject 'eventlog.LogFacilityAction'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.09.04
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFacilityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			'Facility',
			T.FacilityID,
			@Comments,
			@ProjectID
		FROM facility.Facility T
		WHERE T.FacilityID = @EntityID

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogFacilityActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogFacilityActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogFacilityActionTable
		FROM facility.Facility T
		WHERE T.FacilityID = @EntityID
		
		ALTER TABLE #LogFacilityActionTable DROP COLUMN Location

		DECLARE @cFacilityGraduationPlanStep VARCHAR(MAX) = ''
		DECLARE @cFacilityInclusionCriteria VARCHAR(MAX) = ''
		DECLARE @cFacilityMOUContact VARCHAR(MAX) = ''
		
		SELECT @cFacilityGraduationPlanStep = COALESCE(@cFacilityGraduationPlanStep, '') + D.FacilityGraduationPlanStep
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityGraduationPlanStep'), ELEMENTS) AS FacilityGraduationPlanStep
			FROM facility.FacilityGraduationPlanStep T 
			WHERE T.FacilityID = @EntityID
			) D
		
		SELECT @cFacilityInclusionCriteria = COALESCE(@cFacilityInclusionCriteria, '') + D.FacilityInclusionCriteria
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FacilityInclusionCriterion'), ELEMENTS) AS FacilityInclusionCriteria
			FROM facility.FacilityInclusionCriteria T 
			WHERE T.FacilityID = @EntityID
			) D
		
		SELECT @cFacilityMOUContact = COALESCE(@cFacilityMOUContact, '') + D.FacilityMOUContact
		FROM
			(
			SELECT
				(SELECT T.ContactID FOR XML RAW(''), ELEMENTS) AS FacilityMOUContact
			FROM facility.FacilityMOUContact T 
			WHERE T.FacilityID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Facility',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(F.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<FacilityGraduationPlanSteps>' + @cFacilityGraduationPlanStep + '</FacilityGraduationPlanSteps>') AS XML),
			CAST(('<FacilityInclusionCriteria>' + @cFacilityInclusionCriteria + '</FacilityInclusionCriteria>') AS XML),
			CAST(('<FacilityMOUContact>' + @cFacilityMOUContact + '</FacilityMOUContact>') AS XML)
			FOR XML RAW('Facility'), ELEMENTS
			)
		FROM #LogFacilityActionTable T
			JOIN facility.Facility F ON F.FacilityID = T.FacilityID

		DROP TABLE #LogFacilityActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFacilityAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PermissionableTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPermissionableTemplatePermissionables VARCHAR(MAX) = ''
		
		SELECT @cPermissionableTemplatePermissionables = COALESCE(@cPermissionableTemplatePermissionables, '') + D.PermissionableTemplatePermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PermissionableTemplatePermissionable
			FROM permissionable.PermissionableTemplatePermissionable T 
			WHERE T.PermissionableTemplateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST('<PermissionableTemplatePermissionables>' + ISNULL(@cPermissionableTemplatePermissionables, '') + '</PermissionableTemplatePermissionables>' AS XML)
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T
		WHERE T.PermissionableTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction
--End file Build File - 03 - Procedures - EventLog.sql

--Begin file Build File - 03 - Procedures - Other.sql
/* Build File - 03 - Procedures - Other */
USE MNH
GO

--Begin procedure contact.GetContactByContactID
EXEC Utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.10
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CellPhoneNumber,
		C.ContactID,
		C.EmailAddress,
		C.FirstName,
		C.IsActive,
		C.LastName,
		C.MiddleName,
		C.PhoneNumber,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Title,
		CF.ContactFunctionID,
		CF.ContactFunctionName,
		CR.ContactRoleID,
		CR.ContactRoleName,
		DIBR.DIBRoleID,
		DIBR.DIBRoleName,
		F.FacilityID,
		F.FacilityName
	FROM contact.Contact C
		JOIN dropdown.ContactFunction CF ON CF.ContactFunctionID = C.ContactFunctionID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
		JOIN dropdown.DIBRole DIBR ON DIBR.DIBRoleID = C.DIBRoleID
		JOIN facility.Facility F ON F.FacilityID = C.FacilityID
			AND C.ContactID = @ContactID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.04.11
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		ET.EmailTemplateCode,
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

END
GO
--End procedure core.GetEmailTemplateByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- =================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
-- =============================================================================================================================
CREATE PROCEDURE core.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM core.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM core.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM core.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM person.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM core.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM core.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		MI.IsForNewTab,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetServerSetupDataByServerSetupID'
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetServerSetupValuesByServerSetupKey'
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE MI
			SET 
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab
			FROM core.MenuItem MI
			WHERE MI.MenuItemID = @nNewMenuItemID

			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.ServerSetupKeyAddUpdate'
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin core.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
EXEC Utility.DropObject 'utility.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE core.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
		JOIN core.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO core.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM core.MenuItemPermissionableLineage MIPL1
		JOIN core.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure dropdown.GetContactRoleData
EXEC Utility.DropObject 'dropdown.GetContactRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactRole table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactRoleID, 
		T.ContactRoleName
	FROM dropdown.ContactRole T
	WHERE (T.ContactRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactRoleName, T.ContactRoleID

END
GO
--End procedure dropdown.GetContactRoleData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure dropdown.GetDIBRoleData
EXEC Utility.DropObject 'dropdown.GetDIBRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.DIBRole table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetDIBRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DIBRoleID, 
		T.DIBRoleName
	FROM dropdown.DIBRole T
	WHERE (T.DIBRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DIBRoleName, T.DIBRoleID

END
GO
--End procedure dropdown.GetDIBRoleData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure dropdown.GetFacilityServiceData
EXEC Utility.DropObject 'dropdown.GetFacilityServiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityService table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetFacilityServiceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityServiceID, 
		T.FacilityServiceName
	FROM dropdown.FacilityService T
	WHERE (T.FacilityServiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityServiceName, T.FacilityServiceID

END
GO
--End procedure dropdown.GetFacilityServiceData

--Begin procedure dropdown.GetFacilityStatusData
EXEC Utility.DropObject 'dropdown.GetFacilityStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.FacilityStatus table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetFacilityStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FacilityStatusID, 
		T.FacilityStatusName
	FROM dropdown.FacilityStatus T
	WHERE (T.FacilityStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FacilityStatusName, T.FacilityStatusID

END
GO
--End procedure dropdown.GetFacilityStatusData

--Begin procedure dropdown.GetContactFunctionData
EXEC Utility.DropObject 'dropdown.GetContactFunctionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetContactFunctionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactFunctionID, 
		T.ContactFunctionName
	FROM dropdown.ContactFunction T
	WHERE (T.ContactFunctionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactFunctionName, T.ContactFunctionID

END
GO
--End procedure dropdown.GetContactFunctionData

--Begin procedure dropdown.GetGraduationPlanStepData
EXEC Utility.DropObject 'dropdown.GetGraduationPlanStepData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to return data from the dropdown.GraduationPlanStep table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetGraduationPlanStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GraduationPlanStepID, 
		T.GraduationPlanStepName
	FROM dropdown.GraduationPlanStep T
	WHERE (T.GraduationPlanStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GraduationPlanStepName, T.GraduationPlanStepID

END
GO
--End procedure dropdown.GetGraduationPlanStepData

--Begin procedure dropdown.GetInclusionCriteriaData
EXEC Utility.DropObject 'dropdown.GetInclusionCriteriaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.InclusionCriteria table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetInclusionCriteriaData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionCriteriaID, 
		T.InclusionCriteriaName
	FROM dropdown.InclusionCriteria T
	WHERE (T.InclusionCriteriaID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionCriteriaName, T.InclusionCriteriaID

END
GO
--End procedure dropdown.GetInclusionCriteriaData

--Begin procedure dropdown.GetInclusionEligibilityStatusData
EXEC Utility.DropObject 'dropdown.GetInclusionEligibilityStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.09
-- Description:	A stored procedure to return data from the dropdown.InclusionEligibilityStatus table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetInclusionEligibilityStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InclusionEligibilityStatusID, 
		T.InclusionEligibilityStatusName
	FROM dropdown.InclusionEligibilityStatus T
	WHERE (T.InclusionEligibilityStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InclusionEligibilityStatusName, T.InclusionEligibilityStatusID

END
GO
--End procedure dropdown.GetInclusionEligibilityStatusData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin procedure dropdown.GetMOUStatusData
EXEC Utility.DropObject 'dropdown.GetMOUStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.MOUStatus table
-- ================================================================================
CREATE PROCEDURE dropdown.GetMOUStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MOUStatusID, 
		T.MOUStatusName
	FROM dropdown.MOUStatus T
	WHERE (T.MOUStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MOUStatusName, T.MOUStatusID

END
GO
--End procedure dropdown.GetMOUStatusData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID, 
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address, 	
		F.Description, 			
		F.FacilityID, 	
		F.FacilityName, 	
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure facility.GetFacilityGraduationPlanStepsByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityGraduationPlanStepsByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityGraduationPlanStepsByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		F.GraduationPlanUpdateDate, 
		core.FormatDate(F.GraduationPlanUpdateDate) AS GraduationPlanUpdateDateFormatted,
		F.GraduationPlanUpdatePersonID, 
		person.FormatPersonNameByPersonID(F.GraduationPlanUpdatePersonID, 'LastFirst') AS PersonNameFormatted
	FROM facility.Facility F
	WHERE F.FacilityID = @FacilityID

	--FacilityGraduationPlanStep
	SELECT
		FGPS.FacilityGraduationPlanStepID, 
		FGPS.CurrentTargetDate, 
		core.FormatDate(FGPS.CurrentTargetDate) AS CurrentTargetDateFormatted,
		FGPS.IsComplete, 
		FGPS.Notes,
		FGPS.OriginalTargetDate, 
		core.FormatDate(FGPS.OriginalTargetDate) AS OriginalTargetDateFormatted,
		FGPS.PersonID, 
		person.FormatPersonNameByPersonID(FGPS.PersonID, 'LastFirst') AS PersonNameFormatted,
		GPS.GraduationPlanStepID, 
		GPS.GraduationPlanStepName
	FROM facility.FacilityGraduationPlanStep FGPS
		JOIN dropdown.GraduationPlanStep GPS ON GPS.GraduationPlanStepID = FGPS.GraduationPlanStepID
		JOIN facility.Facility F ON F.FacilityID = FGPS.FacilityID
			AND FGPS.FacilityID = @FacilityID
	ORDER BY GPS.DisplayOrder, GPS.GraduationPlanStepName, GPS.GraduationPlanStepID

END
GO
--End procedure facility.GetFacilityGraduationPlanStepsByFacilityID   

--Begin procedure facility.GetFacilityInclusionCriteriaByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityInclusionCriteriaByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityInclusionCriteriaByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		F.InclusionCriteriaNotes,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		IES.InclusionEligibilityStatusID,
		IES.InclusionEligibilityStatusName
	FROM facility.Facility F
		JOIN dropdown.InclusionEligibilityStatus IES ON IES.InclusionEligibilityStatusID = F.InclusionEligibilityStatusID
			AND F.FacilityID = @FacilityID

	--FacilityInclusionCriteria
	SELECT
		FIC.AssessmentDate, 
		core.FormatDate(FIC.AssessmentDate) AS AssessmentDateFormatted,
		FIC.FacilityInclusionCriteriaID, 
		FIC.IsCriteriaMet, 
		FIC.PersonID,
		person.FormatPersonNameByPersonID(FIC.PersonID, 'LastFirst') AS PersonNameFormatted,
		IC.InclusionCriteriaID, 
		IC.InclusionCriteriaName
	FROM facility.FacilityInclusionCriteria FIC
		JOIN dropdown.InclusionCriteria IC ON IC.InclusionCriteriaID = FIC.InclusionCriteriaID
		JOIN facility.Facility F ON F.FacilityID = FIC.FacilityID
			AND FIC.FacilityID = @FacilityID
	ORDER BY IC.DisplayOrder, IC.InclusionCriteriaName, IC.InclusionCriteriaID

END
GO
--End procedure facility.GetFacilityInclusionCriteriaByFacilityID   

--Begin procedure facility.GetFacilityMOUByFacilityID
EXEC Utility.DropObject 'facility.GetFacilityMOUByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.07
-- Description:	A stored procedure to return data from the facility.ContactFunction table
-- ======================================================================================
CREATE PROCEDURE facility.GetFacilityMOUByFacilityID

@FacilityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT
		F.FacilityID, 
		F.FacilityName, 
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		MS.MOUStatusID,
		MS.MOUStatusName
	FROM facility.Facility F
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID, 
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactNameFormatted,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	ORDER BY C.LastName, C.FirstName, C.ContactID

END
GO
--End procedure facility.GetFacilityMOUByFacilityID

--Begin procedure territory.GetParentTerritoryByProjectID
EXEC utility.DropObject 'territory.GetParentTerritoryByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to associate a community to a project
-- =====================================================================
CREATE PROCEDURE territory.GetParentTerritoryByProjectID

@ProjectID INT,
@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
		JOIN 
			(
			SELECT PTT.ParentTerritoryTypeCode
			FROM territory.ProjectTerritoryType PTT
				JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
					AND PTT.ProjectID = @ProjectID
					AND TT.TerritoryTypeCode = @TerritoryTypeCode
			) D ON D.ParentTerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1
	
END
GO
--End procedure territory.GetParentTerritoryByProjectID

--Begin procedure territory.GetTerritoryByProjectID
EXEC utility.DropObject 'territory.GetTerritoryByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to associate a lat/long to a project
-- ====================================================================
CREATE PROCEDURE territory.GetTerritoryByProjectID

@ProjectID INT,
@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
		JOIN 
			(
			SELECT TT.TerritoryTypeCode
			FROM territory.ProjectTerritoryType PTT
				JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = PTT.TerritoryTypeID
					AND PTT.ProjectID = @ProjectID
					AND TT.TerritoryTypeCode = @TerritoryTypeCode
			) D ON D.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1
	
END
GO
--End procedure territory.GetTerritoryByProjectID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
-- =====================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Facility
	SELECT 
		F.FacilityID, 		
		F.FacilityName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM facility.Facility F
	WHERE F.ProjectID = @ProjectID
	ORDER BY 2, 3
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID
--End file Build File - 03 - Procedures - Other.sql

--Begin file Build File - 04 - Data.sql
/* Build File - 04 - Data */
USE MNH
GO

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Budget', 
	@EntityTypeName = 'Budget', 
	@EntityTypeNamePlural = 'Budgets',
	@SchemaName = 'budget', 
	@TableName = 'Budget', 
	@PrimaryKeyFieldName = 'BudgetID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Contact', 
	@EntityTypeName = 'Contact', 
	@EntityTypeNamePlural = 'Contacts',
	@SchemaName = 'contact', 
	@TableName = 'Contact', 
	@PrimaryKeyFieldName = 'ContactID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Facility', 
	@EntityTypeName = 'Facility', 
	@EntityTypeNamePlural = 'Facilities',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'GraduationPlanStep', 
	@EntityTypeName = 'Graduation Plan Step', 
	@EntityTypeNamePlural = 'Graduation Plan Steps',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'InclusionCriteria', 
	@EntityTypeName = 'Inclusion Criteria', 
	@EntityTypeNamePlural = 'Inclusion Criteria',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'MOU', 
	@EntityTypeName = 'MoU', 
	@EntityTypeNamePlural = 'MoUs',
	@SchemaName = 'facility', 
	@TableName = 'Facility', 
	@PrimaryKeyFieldName = 'FacilityID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'Core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

--Begin Top Level Items
EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-map-marker',
	@NewMenuItemCode = 'Sites',
	@NewMenuItemText = 'Sites'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Sites',
	@Icon = 'fa fa-fw fa-car',
	@NewMenuItemCode = 'Visits',
	@NewMenuItemText = 'Visits'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Visits',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Administration'
GO
--End Top Level Items

--Begin Sites Items
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'FacilityList',
	@NewMenuItemLink = '/facility/list',
	@NewMenuItemText = 'Facilities',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'Facility.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'FacilityList',
	@NewMenuItemCode = 'ContactList',
	@NewMenuItemLink = '/contact/list',
	@NewMenuItemText = 'Contacts',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ContactList',
	@NewMenuItemCode = 'InclusionCriteriaList',
	@NewMenuItemLink = '/inclusioncriteria/list',
	@NewMenuItemText = 'Inclusion Criteria',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'InclusionCriteria.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'InclusionCriteriaList',
	@NewMenuItemCode = 'MoUList',
	@NewMenuItemLink = '/mou/list',
	@NewMenuItemText = 'MoUs',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'MOU.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'MoUList',
	@NewMenuItemCode = 'GraduationPlanStepList',
	@NewMenuItemLink = '/graduationplanstep/list',
	@NewMenuItemText = 'Graduation Plans',
	@ParentMenuItemCode = 'Sites',
	@PermissionableLineageList = 'GraduationPlan.List'
GO
--End Sites Items

--Begin Admin Items
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO
--End Admin Items

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Sites'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationKey', NULL, 'DIUZQ1ICXUIMHKKK6TCX'
EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationSecretKey', NULL, 'MylcNoqlXVduD3LBn981ZHRY7OGOcKjsZ8LDTOTH'
EXEC core.SystemSetupAddUpdate 'DuoApiEndPoint', NULL, 'api-8a6e671f.duosecurity.com'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationKey', NULL, 'DIT96UH6EI8OVAUXJVT2'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationSecretKey', NULL, 'q7NjG8UbJzEMEDKNTH9m3FFBTSkEmAxUodib025p'
EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'GMapClientID', NULL, 'AIzaSyCq7wqiKnE1dBkm0z9oGK46tucTyM9ewJk'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@mnh.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '30'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/mnh-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://mnh.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'MNH'
EXEC core.SystemSetupAddUpdate 'TwoFactorEnabled', NULL, '0'
GO
--End table core.SystemSetup

--Begin table dropdown.ContactFunction
TRUNCATE TABLE dropdown.ContactFunction
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactFunction', 'ContactFunctionID', 0
GO

INSERT INTO dropdown.ContactFunction 
	(ContactFunctionName)
VALUES
	('Contact Function 1'),
	('Contact Function 2'),
	('Contact Function 3'),
	('Contact Function 4'),
	('Contact Function 5'),
	('Contact Function 6')
GO
--End table dropdown.ContactFunction

--Begin table dropdown.ContactRole
TRUNCATE TABLE dropdown.ContactRole
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactRole', 'ContactRoleID', 0
GO

INSERT INTO dropdown.ContactRole 
	(ContactRoleName)
VALUES
	('Contact Role 1'),
	('Contact Role 2'),
	('Contact Role 3'),
	('Contact Role 4'),
	('Contact Role 5'),
	('Contact Role 6')
GO
--End table dropdown.ContactRole

--Begin table dropdown.DIBRole
TRUNCATE TABLE dropdown.DIBRole
GO

EXEC utility.InsertIdentityValue 'dropdown.DIBRole', 'DIBRoleID', 0
GO

INSERT INTO dropdown.DIBRole 
	(DIBRoleName)
VALUES
	('DIB Role 1'),
	('DIB Role 2'),
	('DIB Role 3'),
	('DIB Role 4'),
	('DIB Role 5'),
	('DIB Role 6')
GO
--End table dropdown.DIBRole

--Begin table dropdown.FacilityService
TRUNCATE TABLE dropdown.FacilityService
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityService', 'FacilityServiceID', 0
GO

INSERT INTO dropdown.FacilityService 
	(FacilityServiceName)
VALUES
	('Facility Service 1'),
	('Facility Service 2'),
	('Facility Service 3'),
	('Facility Service 4'),
	('Facility Service 5'),
	('Facility Service 6')
GO
--End table dropdown.FacilityService

--Begin table dropdown.FacilityStatus
TRUNCATE TABLE dropdown.FacilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.FacilityStatus', 'FacilityStatusID', 0
GO

INSERT INTO dropdown.FacilityStatus 
	(FacilityStatusName)
VALUES
	('Facility Status 1'),
	('Facility Status 2'),
	('Facility Status 3'),
	('Facility Status 4'),
	('Facility Status 5'),
	('Facility Status 6')
GO
--End table dropdown.FacilityStatus

--Begin table dropdown.GraduationPlanStep
TRUNCATE TABLE dropdown.GraduationPlanStep
GO

EXEC utility.InsertIdentityValue 'dropdown.GraduationPlanStep', 'GraduationPlanStepID', 0
GO

INSERT INTO dropdown.GraduationPlanStep 
	(GraduationPlanStepName, DisplayOrder)
VALUES
	('Mapped', 1),
	('Assessed', 2),
	('Engaged (MoU Signed)', 3),
	('Progressive NABH', 4),
	('Progressive FOGSI', 5),
	('Verification of Progressive', 6),
	('JQS NABH', 7),
	('JQS FOGSI', 8),
	('Verification of JQS', 9)
GO
--End table dropdown.GraduationPlanStep

--Begin table dropdown.InclusionCriteria
TRUNCATE TABLE dropdown.InclusionCriteria
GO

EXEC utility.InsertIdentityValue 'dropdown.InclusionCriteria', 'InclusionCriteriaID', 0
GO

INSERT INTO dropdown.InclusionCriteria 
	(InclusionCriteriaName)
VALUES
	('Compliance: Pollution control registration'),
	('Engagement: Demonstrated interest in quality improvement'),
	('Engagement: Willingness to share data'),
	('Infrastructure: 24/7 electricity'),
	('Infrastructure: 24/7 water supply'),
	('Infrastructure: Labour room'),
	('Infrastructure: Operating theatre'),
	('Scale: A minimum of 20 deliveries per month'),
	('Scale: Less than 50 beds (NABH SHCO cut off)'),
	('Staff: At least three full-time GNM/ANMs'),
	('Staff: Full-time MBBS/Gynaecologist')
GO
--End table dropdown.InclusionCriteria

--Begin table dropdown.InclusionEligibilityStatus
TRUNCATE TABLE dropdown.InclusionEligibilityStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.InclusionEligibilityStatus', 'InclusionEligibilityStatusID', 0
GO

INSERT INTO dropdown.InclusionEligibilityStatus 
	(InclusionEligibilityStatusName, DisplayOrder)
VALUES
	('No', 1),
	('Pending', 2),
	('Yes', 3)
GO
--End table dropdown.InclusionEligibilityStatus

--Begin table dropdown.MOUStatus
TRUNCATE TABLE dropdown.MOUStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.MOUStatus', 'MOUStatusID', 0
GO

INSERT INTO dropdown.MOUStatus 
	(MOUStatusName, DisplayOrder)
VALUES
	('In Draft', 1),
	('Draft Provided', 2),
	('Draft Approved', 3),
	('Ready for Signature', 4),
	('Date for Signature Set', 5),
	('Completed', 6)
GO
--End table dropdown.MOUStatus

--Begin table dropdown.Project
TRUNCATE TABLE dropdown.Project
GO

EXEC utility.InsertIdentityValue 'dropdown.Project', 'ProjectID', 0
GO

INSERT INTO dropdown.Project 
	(ProjectName, ProjectAlias) 
VALUES 
	('HLFPPT', 'HLFPPT'),
	('PSI', 'PSI'),
	('TEST', 'TEST')
GO
--End table dropdown.Project

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleName)
VALUES
	('Role 1'),
	('Role 2'),
	('Role 3'),
	('Role 4')
GO
--End table dropdown.Role

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='Add / edit an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View the list of inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='Add / edit an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View the list of MOUs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
GO
--End table permissionable.Permissionable

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, Organization, RoleID, Password, PasswordSalt, PasswordExpirationDateTime, InvalidLoginAttempts, IsAccountLockedOut, Token, TokenCreateDateTime, EmailAddress, IsActive, CountryCallingCodeID, Phone, IsPhoneVerified, IsSuperAdministrator, DefaultProjectID) 
VALUES 
	('Jonathan', 'Burnhan', 'Mr.', 'jburnham', 'OD', 1, 'EB065F0E9CFE4AA399624ADFEF14427FA39070F25A92B0DFF210D0F13C11B46F', 'AA47A377-16D5-4319-9C92-EEB2C67C05E5', CAST(N'2020-01-01 00:00:00.000' AS DateTime), 0, 0, NULL, CAST(N'2015-08-05 12:09:44.327' AS DateTime), 'jonathanburnham@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Jonathan', 'Cole', 'Mr.', 'JCole', 'SK', 0, '701737B188CB58B85C8A8115AA80400250886596FE96521498A681B716C37E6C', 'F4918DAE-5EDC-4394-AD64-FBB18AC4AEFD', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, '76547CC5-C3B4-110E-6CB3FBCDCDDC4888', CAST(N'2016-09-28T15:18:14.300' AS DateTime), 'jcole@skotkonung.com', 1, 0, '07740705500', 1, 1, 1),
	('Christopher', 'Crouch', 'Mr.', 'christopher.crouch', 'OD', 0, '2EB50FF5E58F8A4A1575C35A0FFDB0863438B678EF46BD0B75BD444E80B8540A', '6F079855-A34A-44AD-9C04-1556195B85BE', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 2, 0, NULL, CAST(N'2015-08-20T09:04:25.350' AS DateTime), 'stchris2opher@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Brandon', 'Green', 'Mr.', 'bgreen', 'OD', 0, 'C776EC7C282669A5128B86D0DC27D09852064002100D80AF861AEFFD81DCD6E7', 'DEFBDB68-978F-4153-8C7E-F24D3D2329B2', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, NULL, CAST(N'2015-09-05T10:27:53.993' AS DateTime), 'bjgreen10@gmail.com', 1, 0, '4104306495', 1, 1, 1),
	('Eric', 'Jones', 'Mr.', 'eric.jones', 'OD', 0, '499A9A4E993262EB9F107AD806F34028CF13F505C77540F1790DC4DAD0FAD9F4', '76BBC375-9B01-45DD-B526-C0DE638B3A91', CAST(N'2099-02-24T15:52:58.000' AS DateTime), 0, 0, NULL, CAST(N'2099-12-26T09:22:31.000' AS DateTime), 'gigado@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('John', 'Lyons', 'Mr.', 'jlyons', 'OD', 0, '47970F8635EF6AC39C9B0D350A897E17C70A5D0D620A4FABF20F99A9E35A5A4D', '74D51802-110F-4DD5-B020-C1F50CA55CE7', CAST(N'2116-12-14T18:16:32.273' AS DateTime), 0, 0, 'BD46BE81-D6B1-616F-F084E84EAAA354E9', CAST(N'2017-06-25T10:49:43.227' AS DateTime), 'jlyons@pridedata.com', 1, 0, '7065992970', 1, 1, 1),
	('Damon', 'Miller', 'Mr.', 'dmiller', 'OD', 0, 'B09F895AF1360AFB6C4CCD107B51010704903B12938ACB8193264BF320D7393E', '8FE8FEAB-EE29-4FA2-AF7A-BF67D66C1D82', CAST(N'2020-01-01T00:00:00.000' AS DateTime), 0, 0, 'CDF817CB-B8A2-D9FD-0A5BC0D61C033F77', CAST(N'2017-06-05T21:22:45.473' AS DateTime), 'damonmiller513@gmail.com', 1, 0, '555-1212', 1, 1, 1),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'OD', 0, '2C0671AB42CFCB763D973A06469D56A077A57FAC2EAB7410A2738A5E60695EFB', 'BAC035E0-1C35-43A3-91DC-9F38888D95C1', CAST(N'2020-09-30T21:43:00.000' AS DateTime), 0, 0, NULL, CAST(N'2016-08-25T22:27:00.347' AS DateTime), 'todd.pires@oceandisc.com', 1, 0, '7275791066', 1, 1, 1),
	('Dave', 'Roberts', 'Mr.', 'DaveR', 'OD', 0, '2C0671AB42CFCB763D973A06469D56A077A57FAC2EAB7410A2738A5E60695EFB', 'BAC035E0-1C35-43A3-91DC-9F38888D95C1', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, NULL, CAST(N'2017-07-22T12:03:01.367' AS DateTime), 'djrobertsjr@comcast.net', 1, 0, '555-1212', 1, 1, 1),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'OD', 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST(N'2020-09-30T21:30:28.000' AS DateTime), 0, 0, '6DBCEC7A-0275-6A21-12CBB9ABFB9934BF', CAST(N'2017-03-31T09:34:18.247' AS DateTime), 'kevin.ross@oceandisc.com', 1, 0, '4104303492', 1, 1, 1)
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.Person

--Begin table person.PersonProject
TRUNCATE TABLE person.PersonProject
GO

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P.PersonID,
	PR.ProjectID
FROM person.Person P, dropdown.Project PR
WHERE PR.ProjectID > 0
GO
--End table person.PersonProject

--Begin table territory.ProjectTerritoryType
TRUNCATE TABLE territory.ProjectTerritoryType
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID
FROM dropdown.Project P, territory.TerritoryType TT
WHERE P.ProjectID > 0
	AND TT.TerritoryTypeCode = 'District'
GO
--End table territory.ProjectTerritoryType

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='Add / edit an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View the list of inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='Add / edit an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View the list of MOUs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2017.09.13 19.32.11')
GO
--End build tracking

