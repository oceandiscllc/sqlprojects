-- File Name:	Build - 1.4 - MNH.sql
-- Build Key:	Build - 1.4 - 2018.03.16 21.26.27

--USE MNH
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetFirstCreateDateTime
--		eventlog.GetLastCreateDateTime
--
-- Procedures:
--		facility.GetFacilityByFacilityID
--		visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE MNH
GO

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
GO
--End table document.DocumentEntity

--Begin table dropdown.InclusionEligibilityStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.InclusionEligibilityStatus'

EXEC utility.AddColumn @TableName, 'InclusionEligibilityStatusCode', 'VARCHAR(50)'
GO
--End table dropdown.InclusionEligibilityStatus


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE MNH
GO

--Begin function eventlog.GetFirstCreateDateTime
EXEC utility.DropObject 'eventlog.GetFirstCreateDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.16
-- Description:	A function to return a the first CreateDateTime for a specific entitytypecode and entityid from the eventlog.EventLog table
-- ========================================================================================================================================

CREATE FUNCTION eventlog.GetFirstCreateDateTime
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS DATETIME

AS
BEGIN
	
	DECLARE @dCreateDateTime DATETIME

	SELECT TOP 1 @dCreateDateTime = EL.CreateDateTime
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','update')
	ORDER BY EL.EventLogID
	
	RETURN @dCreateDateTime

END
GO
--End function eventlog.GetFirstCreateDateTime

--Begin function eventlog.GetLastCreateDateTime
EXEC utility.DropObject 'eventlog.GetLastCreateDateTime'
EXEC utility.DropObject 'eventlog.GetLastLogDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.16
-- Description:	A function to return a the last CreateDateTime for a specific entitytypecode and entityid from the eventlog.EventLog table
-- =======================================================================================================================================

CREATE FUNCTION eventlog.GetLastCreateDateTime
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS DATETIME

AS
BEGIN
	
	DECLARE @dCreateDateTime DATETIME

	SELECT TOP 1 @dCreateDateTime = EL.CreateDateTime
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','update')
	ORDER BY EL.EventLogID DESC
	
	RETURN @dCreateDateTime

END
GO
--End function eventlog.GetLastCreateDateTime


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE MNH
GO

--Begin procedure facility.GetFacilityByFacilityID
EXEC utility.DropObject 'facility.GetFacilityByFacilityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.04
-- Description:	A stored procedure to get data from the facility.Facility table
-- ============================================================================
CREATE PROCEDURE facility.GetFacilityByFacilityID

@FacilityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Facility
	SELECT 
		F.Address,
		F.BedCount,
		F.Description, 			
		F.FacilityEstablishedDate,
		core.FormatDate(F.FacilityEstablishedDate) AS FacilityEstablishedDateFormatted,
		F.FacilityID, 	
		core.FormatDate(eventlog.GetFirstCreateDateTime('Facility', F.FacilityID)) AS FacilityCreateDateFormatted,
		core.FormatDate(eventlog.GetLastCreateDateTime('Facility', F.FacilityID)) AS FacilityUpdateDateFormatted,
		F.FacilityName, 	
		F.Location.STAsText() AS Location,
		F.MOUDate,
		core.FormatDate(F.MOUDate) AS MOUDateFormatted,
		F.MOUNotes,
		F.Phone,
		F.PrimaryContactID, 		
		contact.FormatContactNameByContactID(F.PrimaryContactID, 'LastFirstMiddle') AS PrimaryContactNameFormatted,		
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 		
		FS.FacilityStatusID, 	
		FS.FacilityStatusName,
		MS.MOUStatusID,
		MS.MOUStatusName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted
	FROM facility.Facility F
		JOIN dropdown.FacilityStatus FS ON FS.FacilityStatusID = F.FacilityStatusID
		JOIN dropdown.MOUStatus MS ON MS.MOUStatusID = F.MOUStatusID
			AND F.FacilityID = @FacilityID

	--FacilityContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		C.EmailAddress,
		C.PhoneNumber,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM contact.Contact C
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND C.FacilityID = @FacilityID

	--FacilityFacilityService
	SELECT
		FS.FacilityServiceID,
		FS.FacilityServiceName
	FROM facility.FacilityFacilityService FFS
		JOIN dropdown.FacilityService FS ON FS.FacilityServiceID = FFS.FacilityServiceID
			AND FFS.FacilityID = @FacilityID

	--FacilityMOUContact
	SELECT
		C.ContactID,
		contact.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS ContactNameFormatted,
		CR.ContactRoleID,
		CR.ContactRoleName
	FROM facility.FacilityMOUContact FMC
		JOIN contact.Contact C ON C.ContactID = FMC.ContactID
		JOIN dropdown.ContactRole CR ON CR.ContactRoleID = C.ContactRoleID
			AND FMC.FacilityID = @FacilityID
	
END
GO
--End procedure facility.GetFacilityByFacilityID

--Begin procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID
EXEC Utility.DropObject 'visit.GetVisitQualityStandardByVisitIDAndQualityStandardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.11.11
-- Description:	A stored procedure to get data from the visit.VisitQualityStandard table
-- =====================================================================================
CREATE PROCEDURE visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

@VisitID INT,
@QualityStandardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--VisitQualityStandard
	SELECT 
		VQS.AssessmentDate,
		core.FormatDate(VQS.AssessmentDate) AS AssessmentDateFormatted,
		VQS.CorrectRecords,
		VQS.HasDocumentation,
		VQS.HasImplementation,
		VQS.HasObservation,
		VQS.HasPhysicalVerification,
		VQS.HasProviderInterview,
		VQS.Notes,
		VQS.Score,
		VQS.TotalRecordsChecked
	FROM visit.VisitQualityStandard VQS
	WHERE VQS.VisitID = @VisitID
		AND VQS.QualityStandardID = @QualityStandardID

	--VisitQualityStandardAssessmentEvidenceType
	SELECT 
		AET.AssessmentEvidenceTypeCode,
		AET.AssessmentEvidenceTypeDataType
	FROM dropdown.QualityStandardAssessmentEvidenceType QSAET
		JOIN dropdown.AssessmentEvidenceType AET ON AET.AssessmentEvidenceTypeID = QSAET.AssessmentEvidenceTypeID
			AND QSAET.QualityStandardID = @QualityStandardID

	--VisitQualityStandardDocument
	SELECT
		ISNULL((
			SELECT
				D.DocumentTitle,
				DE.DocumentEntityCode
			FROM document.DocumentEntity DE
				JOIN document.Document D ON D.DocumentID = DE.DocumentID
					AND DE.EntityTypeCode = 'VisitQualityStandard'
					AND DE.EntityID = 
						(
						SELECT VQS.VisitQualityStandardID 
						FROM visit.VisitQualityStandard VQS
						WHERE VQS.VisitID = @VisitID
							AND VQS.QualityStandardID = @QualityStandardID
						)
			FOR JSON PATH
		), '[]') AS UploadedFiles

	--VisitQualityStandardScoreChangeReason
	SELECT
		VQSSCR.ScoreChangeReasonID
	FROM visit.VisitQualityStandardScoreChangeReason VQSSCR
		JOIN visit.VisitQualityStandard VQS ON VQS.VisitQualityStandardID = VQSSCR.VisitQualityStandardID
			AND VQS.VisitID = @VisitID
			AND VQS.QualityStandardID = @QualityStandardID

END
GO
--End procedure visit.GetVisitQualityStandardByVisitIDAndQualityStandardID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE MNH
GO

--Begin table dropdown.InclusionEligibilityStatus
UPDATE IES
SET IES.InclusionEligibilityStatusCode = IES.InclusionEligibilityStatusName
FROM dropdown.InclusionEligibilityStatus IES
GO
--End table dropdown.InclusionEligibilityStatus


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Sites', 'Sites', 0;
EXEC permissionable.SavePermissionableGroup 'Visits', 'Visits', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='getDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.getDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the ColdFusion error dump', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='Add / edit a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View the list of facilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Facility', @DESCRIPTION='View a facility', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='Facility.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='Add / edit a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View the list of graduation plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='GraduationPlanStep', @DESCRIPTION='View a graduation plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='GraduationPlanStep.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='Add / edit an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View the list of inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='InclusionCriteria', @DESCRIPTION='View an inclusion criteria', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='InclusionCriteria.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='Add / edit an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View the list of MOUs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MOU', @DESCRIPTION='View an MOU', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Sites', @PERMISSIONABLELINEAGE='MOU.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='Add / edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View the list of quality assessments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='QualityAssessment', @DESCRIPTION='View a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='QualityAssessment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Add / edit a visit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Edit a quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='QualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.QualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='Save quality assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQualityStandardAssessment', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.SaveQualityStandardAssessment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitPlanList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitPlanList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Visit', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VisitReportList', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='Visit.VisitReportList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='Add / edit a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View the list of visit plans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitPlan', @DESCRIPTION='View a visit plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitPlan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='Add / edit a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View the list of visit reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VisitReport', @DESCRIPTION='View a visit report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Visits', @PERMISSIONABLELINEAGE='VisitReport.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.4 - 2018.03.16 21.26.27')
GO
--End build tracking

