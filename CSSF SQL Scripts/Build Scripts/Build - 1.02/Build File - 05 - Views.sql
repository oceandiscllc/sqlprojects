USE CSSF
GO

--Begin view productdistributor.Product
EXEC utility.DropObject 'productdistributor.Product'
GO

CREATE VIEW productdistributor.Product
AS

SELECT
	dropdown.GetProjectIDByProjectCode(P2.ProjectCode) AS ProjectID,
	P1.IsFocusGrouped,
	P1.Notes,
	P1.ProductCode,
	P1.ProductDuration,
	P1.ProductID,
	P1.ProductLink,
	P1.ProductName,
	P1.ReleaseDate,
	P1.ResponsesToContent,
	PS.ProductStatusName,
	PT.ProductTypeName,
	TA.TargetAudienceName
FROM ICS.productdistributor.Product P1
	JOIN ICS.dropdown.ProductStatus PS ON PS.ProductStatusID = P1.ProductStatusID
	JOIN ICS.dropdown.ProductType PT ON PT.ProductTypeID = P1.ProductTypeID
	JOIN ICS.dropdown.Project P2 ON P2.ProjectID = P1.ProjectID
	JOIN ICS.dropdown.TargetAudience TA ON TA.TargetAudienceID = P1.TargetAudienceID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + P2.ProjectCode + '-Product-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view productdistributor.Product

--Begin view productdistributor.ProductCommunicationTheme
EXEC utility.DropObject 'productdistributor.ProductCommunicationTheme'
GO

CREATE VIEW productdistributor.ProductCommunicationTheme
AS

SELECT
	dropdown.GetProjectIDByProjectCode(P2.ProjectCode) AS ProjectID,
	CT.CommunicationThemeName,
	P1.ProductID
FROM ICS.productdistributor.ProductCommunicationTheme PCT
	JOIN ICS.dropdown.CommunicationTheme CT ON CT.CommunicationThemeID = PCT.CommunicationThemeID
	JOIN ICS.productdistributor.Product P1 ON P1.ProductID = PCT.ProductID
	JOIN ICS.dropdown.Project P2 ON P2.ProjectID = P1.ProjectID
	
GO
--End view productdistributor.ProductCommunicationTheme

--Begin view productdistributor.ProductDistribution
EXEC utility.DropObject 'productdistributor.ProductDistribution'
GO

CREATE VIEW productdistributor.ProductDistribution
AS

SELECT
	dropdown.GetProjectIDByProjectCode(P2.ProjectCode) AS ProjectID,
	PD.Analysis,
	PD.CommentCount,
	PD.DistributionDate,
	PD.Evidence,
	PD.LikeCount,
	PD.ProductDistributionID,
	PD.OrganicReach,
	PD.ShareCount,
	PD.TotalReach,
	PD.ViewCount,
	D.DistributorID,
	D.DistributorName,
	P.ProductID,
	P.ProductName
FROM ICS.productdistributor.ProductDistribution PD
	JOIN ICS.productdistributor.Distributor D ON D.DistributorID = PD.DistributorID
	JOIN ICS.productdistributor.Product P ON P.ProductID = PD.ProductID
	JOIN ICS.dropdown.Project P2 ON P2.ProjectID = P.ProjectID
	
GO
--End view productdistributor.ProductDistribution

