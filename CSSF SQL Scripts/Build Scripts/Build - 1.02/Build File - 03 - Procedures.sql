USE CSSF
GO

--Begin procedure productdistributor.GetProductByProductID
EXEC utility.DropObject 'productdistributor.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [productdistributor].[GetProductByProductID]

@ProjectID INT,
@ProductID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProductID,
		P.IsFocusGrouped,
		P.Notes,
		P.ProductCode,
		dbo.FormatTime(P.ProductDuration) AS ProductDurationFormatted,
		P.ProductLink,
		P.ProductName,
		P.ProjectID,
		dropdown.GetProjectNameByProjectID(P.ProjectID) AS ProjectName,
		P.ReleaseDate,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		P.ProductStatusName,
		P.ProductTypeName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ResponsesToContent,
		P.TargetAudienceName
	FROM productdistributor.Product P
	WHERE P.ProjectID = @ProjectID
		AND P.ProductID = @ProductID

	SELECT
		C.CampaignID,
		C.CampaignName
	FROM productdistributor.ProductCampaign PC
		JOIN productdistributor.Campaign C ON C.CampaignID = PC.CampaignID
			AND PC.ProductID = @ProductID
			AND PC.ProjectID = @ProjectID
			AND C.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	SELECT
		PCT.CommunicationThemeName
	FROM productdistributor.ProductCommunicationTheme PCT
	WHERE PCT.ProjectID = @ProjectID
		AND PCT.ProductID = @ProductID
	ORDER BY 1

	SELECT
		PD.Analysis,
		PD.CommentCount,
		dbo.FormatDate(PD.DistributionDate) AS DistributionDateFormatted,
		PD.Evidence,
		PD.LikeCount,	 
		PD.OrganicReach,	 
		PD.ProductDistributionID,
		PD.ProductID,
		PD.ShareCount,
		PD.TotalReach,
		PD.ViewCount, 
		PD.ProjectID,
		dropdown.GetProjectNameByProjectID(PD.ProjectID) AS ProjectName,
		D.DistributorName,
		D.DistributorTypeName
	FROM productdistributor.ProductDistribution PD
		JOIN productdistributor.Distributor D ON D.DistributorID = PD.DistributorID
			AND PD.ProductID = @ProductID
			AND PD.ProjectID = @ProjectID
			AND D.ProjectID = @ProjectID
	ORDER BY D.DistributorName, D.DistributorID

	SELECT
		ISNULL(SUM(PD.CommentCount), 0) AS CommentCountTotal,
		ISNULL(SUM(PD.LikeCount), 0) AS LikeCountTotal,
		ISNULL(SUM(PD.OrganicReach), 0) AS OrganicReachTotal,
		ISNULL(SUM(PD.ShareCount), 0) AS ShareCountTotal,
		ISNULL(SUM(PD.TotalReach), 0) AS TotalReachTotal,
		ISNULL(SUM(PD.ViewCount), 0) AS ViewCount
	FROM productdistributor.ProductDistribution PD
	WHERE PD.ProjectID = @ProjectID
		AND PD.ProductID = @ProductID

	SELECT
		PT.TerritoryID,
		PT.TerritoryTypeCode,
		PT.TerritoryName
	FROM productdistributor.ProductTerritory PT
	WHERE PT.ProjectID = @ProjectID
		AND PT.ProductID = @ProductID
	ORDER BY 3, 1
	
END
GO
--End procedure productdistributor.GetProductByProductID
