USE CSSF
GO

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.AddColumn @TableName, 'PermissionableLineage', 'VARCHAR(MAX)'
GO

UPDATE PTP
SET PTP.PermissionableLineage = P.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP
	JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplatePermissionable
