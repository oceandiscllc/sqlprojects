-- File Name:	Build-1.01 File 01 - CSSF.sql
-- Build Key:	Build-1.01 File 01 - CSSF - 2016.04.29 21.02.14

USE CSSF
GO

-- ==============================================================================================================================
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE CSSF
GO

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.AddColumn @TableName, 'PermissionableLineage', 'VARCHAR(MAX)'
GO

UPDATE PTP
SET PTP.PermissionableLineage = P.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP
	JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplatePermissionable

--End file Build File - 01 - Tables.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'ForceAsset', 'Forces & Assets', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'ProductDistributor', 'Product Distribution', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Situational Data', 0
GO
EXEC utility.SavePermissionableGroup 'Territories', 'Territories', 0
GO
EXEC utility.SavePermissionableGroup 'Utility', 'Utility', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log entry', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'View the list of permissionable templates', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'View the list of system users', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the list of server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'View the list of forces', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'ListUnits', NULL, 'Force.ListUnits', 'View the list of units attached to a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'ViewUnit', NULL, 'Force.ViewUnit', 'View a unit attached to a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', 'AJACS', 'Objective.ChartList.AJACS', 'View the objectives overview for project AJACS', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', 'MOA', 'Objective.ChartList.MOA', 'View the objectives overview for project MOA', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage objectives', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'View the objectives overview', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Campaign', 'List', NULL, 'Campaign.List', 'View the list of campaigns', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Campaign', 'View', NULL, 'Campaign.View', 'View a campaign', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Distributor', 'List', NULL, 'Distributor.List', 'View the list of distributors', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Distributor', 'View', NULL, 'Distributor.View', 'View a distributor', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Product', 'List', NULL, 'Product.List', 'View the list of products', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Product', 'View', NULL, 'Product.View', 'View a product', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'View the list of atmospheric reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'View the list of findings', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'View the list of incident reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'View the list of recommendations', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'View the list of requests for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'View the list of risks', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'View the list of spot reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'View the list of communities', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View a community', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the community analysis tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'View the community implementation tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the community information tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub-district', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View the actual error on the cf error page', 0, 0, 'Utility'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.01 File 01 - CSSF - 2016.04.29 21.02.14')
GO
--End build tracking

