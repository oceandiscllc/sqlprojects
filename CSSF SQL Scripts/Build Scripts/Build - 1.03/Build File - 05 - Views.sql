USE CSSF
GO

--Begin view territory.FrontView
EXEC utility.DropObject 'territory.FrontView'
GO

CREATE VIEW territory.FrontView
AS

SELECT
	FL.FrontID,
	FL.ProjectID,
	FL.FrontName,
	FL.ParentTerritoryID,
	FL.Location,
	
	FR.Implications,
	FR.KeyPoints,
	FR.Population,
	FR.PopulationSource,
	FR.ProgramNotes1,
	FR.ProgramNotes2,
	FR.ProgramNotes3,
	FR.ProgramNotes4,
	FR.ProgramNotes5,
	FR.RiskMitigation,
	FR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Front FL
	JOIN ICS.territory.Front FR ON FR.FrontID = FL.FrontID
	JOIN dropdown.Project PL ON PL.ProjectID = FL.ProjectID
	JOIN ICS.dropdown.Project PR ON PR.ProjectID = FR.ProjectID
		AND PR.ProjectCode = PL.ProjectCode
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = FR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = FR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = FR.StatusChangeID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + PR.ProjectCode + '-Front-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view territory.FrontView

--Begin view territory.GovernorateView
EXEC utility.DropObject 'territory.GovernorateView'
GO

CREATE VIEW territory.GovernorateView
AS

SELECT
	GL.GovernorateID,
	GL.ProjectID,
	GL.GovernorateName,
	GL.ParentTerritoryID,
	GL.Location,
	
	P.Implications,
	P.KeyPoints,
	0 AS Population,
	'' AS PopulationSource,
	P.ProgramNotes1,
	P.ProgramNotes2,
	'' AS ProgramNotes3,
	'' AS ProgramNotes4,
	'' AS ProgramNotes5,
	P.RiskMitigation,
	P.Summary,

	'' AS EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Governorate GL
	JOIN AJACS.dbo.Province P ON P.ProvinceID = GL.GovernorateID
		AND GL.ProjectID = dropdown.GetProjectIDByDatabaseName('AJACS')
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
	JOIN AJACS.dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID

UNION

SELECT
	GL.GovernorateID,
	GL.ProjectID,
	GL.GovernorateName,
	GL.ParentTerritoryID,
	GL.Location,
	
	GR.Implications,
	GR.KeyPoints,
	GR.Population,
	GR.PopulationSource,
	GR.ProgramNotes1,
	GR.ProgramNotes2,
	GR.ProgramNotes3,
	GR.ProgramNotes4,
	GR.ProgramNotes5,
	GR.RiskMitigation,
	GR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Governorate GL
	JOIN ICS.territory.Governorate GR ON GR.GovernorateID = GL.GovernorateID
	JOIN dropdown.Project PL ON PL.ProjectID = GL.ProjectID
	JOIN ICS.dropdown.Project PR ON PR.ProjectID = GR.ProjectID
		AND PR.ProjectCode = PL.ProjectCode
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = GR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = GR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = GR.StatusChangeID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + PR.ProjectCode + '-Governorate-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view territory.GovernorateView

--Begin view territory.DistrictView
EXEC utility.DropObject 'territory.DistrictView'
GO

CREATE VIEW territory.DistrictView
AS

SELECT
	DL.DistrictID,
	DL.ProjectID,
	DL.DistrictName,
	DL.ParentTerritoryID,
	DL.Location,
	
	DR.Implications,
	DR.KeyPoints,
	DR.Population,
	DR.PopulationSource,
	DR.ProgramNotes1,
	DR.ProgramNotes2,
	DR.ProgramNotes3,
	DR.ProgramNotes4,
	DR.ProgramNotes5,
	DR.RiskMitigation,
	DR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.District DL
	JOIN ICS.territory.District DR ON DR.DistrictID = DL.DistrictID
	JOIN dropdown.Project PL ON PL.ProjectID = DL.ProjectID
	JOIN ICS.dropdown.Project PR ON PR.ProjectID = DR.ProjectID
		AND PR.ProjectCode = PL.ProjectCode
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = DR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = DR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = DR.StatusChangeID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + PR.ProjectCode + '-District-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view territory.DistrictView

--Begin view territory.SubDistrictView
EXEC utility.DropObject 'territory.SubDistrictView'
GO

CREATE VIEW territory.SubDistrictView
AS

SELECT
	SDL.SubDistrictID,
	SDL.ProjectID,
	SDL.SubDistrictName,
	SDL.ParentTerritoryID,
	SDL.Location,
	
	SDR.Implications,
	SDR.KeyPoints,
	SDR.Population,
	SDR.PopulationSource,
	SDR.ProgramNotes1,
	SDR.ProgramNotes2,
	SDR.ProgramNotes3,
	SDR.ProgramNotes4,
	SDR.ProgramNotes5,
	SDR.RiskMitigation,
	SDR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.SubDistrict SDL
	JOIN ICS.territory.SubDistrict SDR ON SDR.SubDistrictID = SDL.SubDistrictID
	JOIN dropdown.Project PL ON PL.ProjectID = SDL.ProjectID
	JOIN ICS.dropdown.Project PR ON PR.ProjectID = SDR.ProjectID
		AND PR.ProjectCode = PL.ProjectCode
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = SDR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SDR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = SDR.StatusChangeID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + PR.ProjectCode + '-SubDistrict-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view territory.SubDistrictView

--Begin view territory.CommunityView
EXEC utility.DropObject 'territory.CommunityView'
GO

CREATE VIEW territory.CommunityView
AS

SELECT
	CL.CommunityID,
	CL.ProjectID,
	CL.CommunityName,
	CL.ParentTerritoryID,
	CL.Location,
	
	CR.Implications,
	CR.KeyPoints,
	CR.Population,
	CR.PopulationSource,
	CR.ProgramNotes1,
	CR.ProgramNotes2,
	CR.ProgramNotes3,
	CR.ProgramNotes4,
	CR.ProgramNotes5,
	CR.RiskMitigation,
	CR.Summary,

	'' AS EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Community CL
	JOIN AJACS.dbo.Community CR ON CR.CommunityID = CL.CommunityID
		AND CL.ProjectID = dropdown.GetProjectIDByDatabaseName('AJACS')
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CR.ImpactDecisionID
	JOIN AJACS.dropdown.StatusChange SC ON SC.StatusChangeID = CR.StatusChangeID

UNION

SELECT
	CL.CommunityID,
	CL.ProjectID,
	CL.CommunityName,
	CL.ParentTerritoryID,
	CL.Location,
	
	CR.Implications,
	CR.KeyPoints,
	CR.Population,
	CR.PopulationSource,
	CR.ProgramNotes1,
	CR.ProgramNotes2,
	CR.ProgramNotes3,
	CR.ProgramNotes4,
	CR.ProgramNotes5,
	CR.RiskMitigation,
	CR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Community CL
	JOIN ICS.territory.Community CR ON CR.CommunityID = CL.CommunityID
	JOIN dropdown.Project PL ON PL.ProjectID = CL.ProjectID
	JOIN ICS.dropdown.Project PR ON PR.ProjectID = CR.ProjectID
		AND PR.ProjectCode = PL.ProjectCode
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = CR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = CR.StatusChangeID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + PR.ProjectCode + '-Community-OnCSSFPortal', '0') AS INT) = 1
		
GO
--End view territory.CommunityView
