USE CSSF
GO

--Begin procedure territory.GetFrontByFrontID
EXEC Utility.DropObject 'territory.GetFrontByFrontID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.18
-- Description:	A stored procedure to data from the territory.Front table and view
-- ===============================================================================
CREATE PROCEDURE territory.GetFrontByFrontID

@ProjectID INT,
@FrontID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Front
	SELECT
		F.FrontID,
		F.FrontName,
		F.Location,
		F.ParentTerritoryID,
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,

		FV.EngagementStatusName,
		FV.ImpactDecisionName,
		FV.Implications,
		FV.KeyPoints,
		FV.Population,
		FV.PopulationSource,
		FV.ProgramNotes1,
		FV.ProgramNotes2,
		FV.ProgramNotes3,
		FV.ProgramNotes4,
		FV.ProgramNotes5,
		FV.RiskMitigation,
		FV.StatusChangeName,
		FV.Summary
	FROM territory.Front F
		JOIN territory.FrontView FV ON FV.FrontID = F.FrontID
			AND FV.ProjectID = F.ProjectID
			AND F.FrontID = @FrontID
			AND F.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName,
		A.ProjectID
	FROM asset.Asset A
	WHERE A.TerritoryTypeCode = 'Front'
		AND A.TerritoryID = @FrontID
		AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName,
		A.ProjectID
	FROM asset.Asset A
		JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = A.TerritoryID
			AND A.TerritoryTypeCode = 'Governorate'
			AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName,
		A.ProjectID
	FROM asset.Asset A
		JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = A.TerritoryID
			AND A.TerritoryTypeCode = 'District'
			AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName,
		A.ProjectID
	FROM asset.Asset A
		JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = A.TerritoryID
			AND A.TerritoryTypeCode = 'SubDistrict'
			AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName,
		A.ProjectID
	FROM asset.Asset A
		JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = A.TerritoryID
			AND A.TerritoryTypeCode = 'Community'
			AND A.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1
	
	--Atmospherics
	SELECT 
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID,
		A.AtmosphericTypeName,
		A.ConfidenceLevelName,
		A.ProjectID
	FROM atmospheric.Atmospheric A
	WHERE A.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
			WHERE T.AtmosphericID = A.AtmosphericID
				AND T.TerritoryTypeCode = 'Front'
				AND T.TerritoryID = @FrontID
				AND T.ProjectID = A.ProjectID
		
			UNION
 
 			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Governorate'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID
			)
	ORDER BY 2, 3, 1

	--Distributors
	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.DistributorTypeName,
		D.ProjectID
	FROM productdistributor.Distributor D
	WHERE D.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.DistributorTerritory T
			WHERE T.DistributorID = D.DistributorID
				AND T.TerritoryTypeCode = 'Front'
				AND T.TerritoryID = @FrontID
				AND T.ProjectID = D.ProjectID
		
			UNION
	
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Governorate'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID
			)
	ORDER BY 2, 3

	--Equipment
	SELECT 1

	--Findings
	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingStatusName,
		F.FindingTypeName,
		F.ProjectID
	FROM finding.Finding F
	WHERE F.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM finding.FindingTerritory T
			WHERE T.FindingID = F.FindingID
				AND T.TerritoryTypeCode = 'Front'
				AND T.TerritoryID = @FrontID
				AND T.ProjectID = F.ProjectID
		
			UNION
	
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Governorate'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID
			)
	ORDER BY 1, 2, 3

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.ProjectID
	FROM Force.Force F
	WHERE F.TerritoryTypeCode = 'Front'
		AND F.TerritoryID = @FrontID
		AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.ProjectID
	FROM Force.Force F
		JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = F.TerritoryID
			AND F.TerritoryTypeCode = 'Governorate'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.ProjectID
	FROM Force.Force F
		JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = F.TerritoryID
			AND F.TerritoryTypeCode = 'District'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.ProjectID
	FROM Force.Force F
		JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = F.TerritoryID
			AND F.TerritoryTypeCode = 'SubDistrict'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.ProjectID
	FROM Force.Force F
		JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = F.TerritoryID
			AND F.TerritoryTypeCode = 'Community'
			AND F.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1

	--Incidents
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName,
		I.ProjectID
	FROM dbo.Incident I
	WHERE I.TerritoryTypeCode = 'Front'
		AND I.TerritoryID = @FrontID
		AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName,
		I.ProjectID
	FROM dbo.Incident I
		JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = I.TerritoryID
			AND I.TerritoryTypeCode = 'Governorate'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName,
		I.ProjectID
	FROM dbo.Incident I
		JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = I.TerritoryID
			AND I.TerritoryTypeCode = 'District'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName,
		I.ProjectID
	FROM dbo.Incident I
		JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = I.TerritoryID
			AND I.TerritoryTypeCode = 'SubDistrict'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName,
		I.ProjectID
	FROM dbo.Incident I
		JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = I.TerritoryID
			AND I.TerritoryTypeCode = 'Community'
			AND I.ProjectID = @ProjectID
	
	ORDER BY 3, 4, 1, 5

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ProductTypeName,
		P.TargetAudienceName,
		P.ProjectID
	FROM productdistributor.Product P
	WHERE P.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.ProductTerritory T
			WHERE T.ProductID = P.ProductID
				AND T.TerritoryTypeCode = 'Front'
				AND T.TerritoryID = @FrontID
				AND T.ProjectID = @ProjectID

			UNION
	
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Governorate'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID
			)
	ORDER BY 1, 4, 3, 5, 2

	--Risks
	SELECT 1

	--Spot Reports
	SELECT 1

	--Units
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.ProjectID
	FROM force.ForceUnit FU
	WHERE FU.TerritoryTypeCode = 'Front'
		AND FU.TerritoryID = @FrontID
		AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.ProjectID
	FROM force.ForceUnit FU
		JOIN territory.GetGovernoratesByFrontID(@ProjectID, @FrontID) GBF ON GBF.GovernorateID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'Governorate'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.ProjectID
	FROM force.ForceUnit FU
		JOIN territory.GetDistrictsByFrontID(@ProjectID, @FrontID) DBF ON DBF.DistrictID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'District'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.ProjectID
	FROM force.ForceUnit FU
		JOIN territory.GetSubDistrictsByFrontID(@ProjectID, @FrontID) SDBF ON SDBF.SubDistrictID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'SubDistrict'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.ProjectID
	FROM force.ForceUnit FU
		JOIN territory.GetCommunitiesByFrontID(@ProjectID, @FrontID) CBF ON CBF.CommunityID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'Community'
			AND FU.ProjectID = @ProjectID

	ORDER BY 1, 2, 3

END
GO
--End procedure territory.GetFrontByFrontID

--Begin procedure territory.GetGovernorateByGovernorateID
EXEC Utility.DropObject 'territory.GetGovernorateByGovernorateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.18
-- Description:	A stored procedure to data from the territory.Governorate table and view
-- =====================================================================================
CREATE PROCEDURE territory.GetGovernorateByGovernorateID

@ProjectID INT,
@GovernorateID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Governorate
	SELECT
		G.GovernorateID,
		G.GovernorateName,
		G.Location,
		G.ParentTerritoryID,
		G.ProjectID,
		dropdown.GetProjectNameByProjectID(G.ProjectID) AS ProjectName,

		GV.EngagementStatusName,
		GV.ImpactDecisionName,
		GV.Implications,
		GV.KeyPoints,
		GV.Population,
		GV.PopulationSource,
		GV.ProgramNotes1,
		GV.ProgramNotes2,
		GV.ProgramNotes3,
		GV.ProgramNotes4,
		GV.ProgramNotes5,
		GV.RiskMitigation,
		GV.StatusChangeName,
		GV.Summary
	FROM territory.Governorate G
		JOIN territory.GovernorateView GV ON GV.GovernorateID = G.GovernorateID
			AND GV.ProjectID = G.ProjectID
			AND G.GovernorateID = @GovernorateID
			AND G.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
	WHERE A.TerritoryTypeCode = 'Governorate'
		AND A.TerritoryID = @GovernorateID
		AND A.ProjectID = @ProjectID

	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = A.TerritoryID
			AND A.TerritoryTypeCode = 'District'
			AND A.ProjectID = @ProjectID

	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = A.TerritoryID
			AND A.TerritoryTypeCode = 'SubDistrict'
			AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = A.TerritoryID
			AND A.TerritoryTypeCode = 'Community'
			AND A.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1
	
	--Atmospherics
	SELECT 
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID,
		A.AtmosphericTypeName,
		A.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
	WHERE A.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
			WHERE T.AtmosphericID = A.AtmosphericID
				AND T.TerritoryTypeCode = 'Governorate'
				AND T.TerritoryID = @GovernorateID
				AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID

			UNION
				
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID
			)
	ORDER BY 2, 3, 1

	--Distributors
	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.DistributorTypeName
	FROM productdistributor.Distributor D
	WHERE D.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.DistributorTerritory T
			WHERE T.DistributorID = D.DistributorID
				AND T.TerritoryTypeCode = 'Governorate'
				AND T.TerritoryID = @GovernorateID
				AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID
			)
	ORDER BY 2, 3

	--Equipment
	SELECT 1

	--Findings
	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingStatusName,
		F.FindingTypeName
	FROM finding.Finding F
	WHERE F.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM finding.FindingTerritory T
			WHERE T.FindingID = F.FindingID
				AND T.TerritoryTypeCode = 'Governorate'
				AND T.TerritoryID = @GovernorateID
				AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID

			UNION
				
			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID
			)
	ORDER BY 1, 2, 3

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
	WHERE F.TerritoryTypeCode = 'Governorate'
		AND F.TerritoryID = @GovernorateID
		AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = F.TerritoryID
			AND F.TerritoryTypeCode = 'District'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = F.TerritoryID
			AND F.TerritoryTypeCode = 'SubDistrict'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = F.TerritoryID
			AND F.TerritoryTypeCode = 'Community'
			AND F.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1

	--Incidents
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
	WHERE I.TerritoryTypeCode = 'Governorate'
		AND I.TerritoryID = @GovernorateID
		AND I.ProjectID = @ProjectID

	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = I.TerritoryID
			AND I.TerritoryTypeCode = 'District'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = I.TerritoryID
			AND I.TerritoryTypeCode = 'SubDistrict'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = I.TerritoryID
			AND I.TerritoryTypeCode = 'Community'
			AND I.ProjectID = @ProjectID
	
	ORDER BY 3, 4, 1, 5

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ProductTypeName,
		P.TargetAudienceName
	FROM productdistributor.Product P
WHERE P.ProjectID = @ProjectID 
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.ProductTerritory T
			WHERE T.ProductID = P.ProductID
				AND T.TerritoryTypeCode = 'Governorate'
				AND T.TerritoryID = @GovernorateID
				AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'District'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID

			UNION
				
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID
			)
	ORDER BY 1, 4, 3, 5, 2

	--Risks
	SELECT 1

	--Spot Reports
	SELECT 1

	--Units
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
	WHERE FU.TerritoryTypeCode = 'Governorate'
		AND FU.TerritoryID = @GovernorateID
		AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetDistrictsByGovernorateID(@ProjectID, @GovernorateID) DBG ON DBG.DistrictID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'District'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetSubDistrictsByGovernorateID(@ProjectID, @GovernorateID) SDBG ON SDBG.SubDistrictID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'SubDistrict'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetCommunitiesByGovernorateID(@ProjectID, @GovernorateID) CBG ON CBG.CommunityID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'Community'
			AND FU.ProjectID = @ProjectID

	ORDER BY 1, 2, 3
	
END
GO
--End procedure territory.GetGovernorateByGovernorateID

--Begin procedure territory.GetDistrictByDistrictID
EXEC Utility.DropObject 'territory.GetDistrictByDistrictID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.18
-- Description:	A stored procedure to data from the territory.District table and view
-- ==================================================================================
CREATE PROCEDURE territory.GetDistrictByDistrictID

@ProjectID INT,
@DistrictID INT

AS
BEGIN
	SET NOCOUNT ON;

	--District
	SELECT
		D.DistrictID,
		D.DistrictName,
		D.Location,
		D.ParentTerritoryID,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,

		DV.EngagementStatusName,
		DV.ImpactDecisionName,
		DV.Implications,
		DV.KeyPoints,
		DV.Population,
		DV.PopulationSource,
		DV.ProgramNotes1,
		DV.ProgramNotes2,
		DV.ProgramNotes3,
		DV.ProgramNotes4,
		DV.ProgramNotes5,
		DV.RiskMitigation,
		DV.StatusChangeName,
		DV.Summary
	FROM territory.District D
		JOIN territory.DistrictView DV ON DV.DistrictID = D.DistrictID
			AND DV.ProjectID = D.ProjectID
			AND D.DistrictID = @DistrictID
			AND D.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
	WHERE A.TerritoryTypeCode = 'District'
		AND A.TerritoryID = @DistrictID
		AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = A.TerritoryID
			AND A.TerritoryTypeCode = 'SubDistrict'
			AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = A.TerritoryID
			AND A.TerritoryTypeCode = 'Community'
			AND A.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1
	
	--Atmospherics
	SELECT
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID,
		A.AtmosphericTypeName,
		A.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
	WHERE A.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
			WHERE T.AtmosphericID = A.AtmosphericID
				AND T.TerritoryTypeCode = 'District'
				AND T.TerritoryID = @DistrictID
				AND T.ProjectID = A.ProjectID
		
			UNION
	
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID
		
			UNION
	
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID
			)
	ORDER BY 2, 3, 1

	--Distributors
	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.DistributorTypeName
	FROM productdistributor.Distributor D
	WHERE D.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.DistributorTerritory T
			WHERE T.DistributorID = D.DistributorID
				AND T.TerritoryTypeCode = 'District'
				AND T.TerritoryID = @DistrictID
				AND T.ProjectID = D.ProjectID

			UNION
	
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID
		
			UNION
	
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID
			)
	ORDER BY 2, 3

	--Equipment
	SELECT 1

	--Findings
	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingStatusName,
		F.FindingTypeName
	FROM finding.Finding F
	WHERE F.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM finding.FindingTerritory T
			WHERE T.FindingID = F.FindingID
				AND T.TerritoryTypeCode = 'District'
				AND T.TerritoryID = @DistrictID
				AND T.ProjectID = F.ProjectID
		
			UNION

			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = T.TerritoryID
					AND T.TerritoryTypeCode = 'SubDistrict'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID
		
			UNION

			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID
			)
	ORDER BY 1, 2, 3

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
	WHERE F.TerritoryTypeCode = 'District'
		AND F.TerritoryID = @DistrictID
		AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = F.TerritoryID
			AND F.TerritoryTypeCode = 'SubDistrict'
			AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = F.TerritoryID
			AND F.TerritoryTypeCode = 'Community'
			AND F.ProjectID = @ProjectID
	
	ORDER BY 2, 3, 4, 1

	--Incidents
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
	WHERE I.TerritoryTypeCode = 'District'
		AND I.TerritoryID = @DistrictID
		AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = I.TerritoryID
			AND I.TerritoryTypeCode = 'SubDistrict'
			AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = I.TerritoryID
			AND I.TerritoryTypeCode = 'Community'
			AND I.ProjectID = @ProjectID
	
	ORDER BY 3, 4, 1, 5

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ProductTypeName,
		P.TargetAudienceName
	FROM productdistributor.Product P
	WHERE P.ProjectID = @ProjectID
		AND EXISTS
		(
		SELECT 1
		FROM productdistributor.ProductTerritory T
		WHERE T.ProductID = P.ProductID
			AND T.TerritoryTypeCode = 'District'
			AND T.TerritoryID = @DistrictID
			
		UNION
	
		SELECT 1
		FROM productdistributor.ProductTerritory T
			JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = T.TerritoryID
				AND T.TerritoryTypeCode = 'SubDistrict'
				AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID
			
		UNION
	
		SELECT 1
		FROM productdistributor.ProductTerritory T
			JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = T.TerritoryID
				AND T.TerritoryTypeCode = 'Community'
				AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID
		)
	ORDER BY 1, 4, 3, 5, 2

	--Risks
	SELECT 1

	--Spot Reports
	SELECT 1

	--Units
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
	WHERE FU.TerritoryTypeCode = 'District'
		AND FU.TerritoryID = @DistrictID
		AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetSubDistrictsByDistrictID(@ProjectID, @DistrictID) SDBD ON SDBD.SubDistrictID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'SubDistrict'
			AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetCommunitiesByDistrictID(@ProjectID, @DistrictID) CBD ON CBD.CommunityID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'Community'
			AND FU.ProjectID = @ProjectID

	ORDER BY 1, 2, 3
	
END
GO
--End procedure territory.GetDistrictByDistrictID

--Begin procedure territory.GetSubDistrictBySubDistrictID
EXEC Utility.DropObject 'territory.GetSubDistrictBySubDistrictID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.18
-- Description:	A stored procedure to data from the territory.SubDistrict table and view
-- =====================================================================================
CREATE PROCEDURE territory.GetSubDistrictBySubDistrictID

@ProjectID INT,
@SubDistrictID INT

AS
BEGIN
	SET NOCOUNT ON;

	--SubDistrict
	SELECT
		SD.SubDistrictID,
		SD.SubDistrictName,
		SD.Location,
		SD.ParentTerritoryID,
		SD.ProjectID,
		dropdown.GetProjectNameByProjectID(SD.ProjectID) AS ProjectName,

		SDV.EngagementStatusName,
		SDV.ImpactDecisionName,
		SDV.Implications,
		SDV.KeyPoints,
		SDV.Population,
		SDV.PopulationSource,
		SDV.ProgramNotes1,
		SDV.ProgramNotes2,
		SDV.ProgramNotes3,
		SDV.ProgramNotes4,
		SDV.ProgramNotes5,
		SDV.RiskMitigation,
		SDV.StatusChangeName,
		SDV.Summary
	FROM territory.SubDistrict SD
		JOIN territory.SubDistrictView SDV ON SDV.SubDistrictID = SD.SubDistrictID
			AND SDV.ProjectID = SD.ProjectID
			AND SD.SubDistrictID = @SubDistrictID
			AND SD.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
	WHERE A.TerritoryTypeCode = 'SubDistrict'
		AND A.TerritoryID = @SubDistrictID
		AND A.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = A.TerritoryID
			AND A.TerritoryTypeCode = 'Community'
			AND A.ProjectID = @ProjectID

	ORDER BY 2, 3, 4, 1
	
	--Atmospherics
	SELECT 
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID,
		A.AtmosphericTypeName,
		A.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
	WHERE A.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
			WHERE T.AtmosphericID = A.AtmosphericID
				AND T.TerritoryTypeCode = 'SubDistrict'
				AND T.TerritoryID = @SubDistrictID
				AND T.ProjectID = A.ProjectID
		
			UNION
	
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
				JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.AtmosphericID = A.AtmosphericID
					AND T.ProjectID = A.ProjectID
			)
	ORDER BY 2, 3, 1

	--Distributors
	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.DistributorTypeName
	FROM productdistributor.Distributor D
	WHERE D.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.DistributorTerritory T
			WHERE T.DistributorID = D.DistributorID
				AND T.TerritoryTypeCode = 'SubDistrict'
				AND T.TerritoryID = @SubDistrictID
				AND T.ProjectID = D.ProjectID
		
			UNION
	
			SELECT 1
			FROM productdistributor.DistributorTerritory T
				JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.DistributorID = D.DistributorID
					AND T.ProjectID = D.ProjectID
			)
	ORDER BY 2, 3

	--Equipment
	SELECT 1

	--Findings
	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingStatusName,
		F.FindingTypeName
	FROM finding.Finding F
	WHERE F.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM finding.FindingTerritory T
			WHERE T.FindingID = F.FindingID
				AND T.TerritoryTypeCode = 'SubDistrict'
				AND T.TerritoryID = @SubDistrictID
				AND T.ProjectID = F.ProjectID
		
			UNION

			SELECT 1
			FROM finding.FindingTerritory T
				JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.FindingID = F.FindingID
					AND T.ProjectID = F.ProjectID
			)
	ORDER BY 1, 2, 3

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
	WHERE F.TerritoryTypeCode = 'SubDistrict'
		AND F.TerritoryID = @SubDistrictID
		AND F.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = F.TerritoryID
			AND F.TerritoryTypeCode = 'Community'
			AND F.ProjectID = @ProjectID

	ORDER BY 2, 3, 4, 1

	--Incidents
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
	WHERE I.TerritoryTypeCode = 'SubDistrict'
		AND I.TerritoryID = @SubDistrictID
		AND I.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
		JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = I.TerritoryID
			AND I.TerritoryTypeCode = 'Community'
			AND I.ProjectID = @ProjectID

	ORDER BY 3, 4, 1, 5

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ProductTypeName,
		P.TargetAudienceName
	FROM productdistributor.Product P
	WHERE P.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.ProductTerritory T
			WHERE T.ProductID = P.ProductID
				AND T.TerritoryTypeCode = 'SubDistrict'
				AND T.TerritoryID = @SubDistrictID
				AND T.ProjectID = @ProjectID
		
			UNION
	
			SELECT 1
			FROM productdistributor.ProductTerritory T
				JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = T.TerritoryID
					AND T.TerritoryTypeCode = 'Community'
					AND T.ProductID = P.ProductID
					AND T.ProjectID = @ProjectID
			)
	ORDER BY 1, 4, 3, 5, 2

	--Risks
	SELECT 1

	--Spot Reports
	SELECT 1

	--Units
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
	WHERE FU.TerritoryTypeCode = 'SubDistrict'
		AND FU.TerritoryID = @SubDistrictID
		AND FU.ProjectID = @ProjectID
			
	UNION
	
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
		JOIN territory.GetCommunitiesBySubDistrictID(@ProjectID, @SubDistrictID) CBSD ON CBSD.CommunityID = FU.TerritoryID
			AND FU.TerritoryTypeCode = 'Community'
			AND FU.ProjectID = @ProjectID

	ORDER BY 1, 2, 3
	
END
GO
--End procedure territory.GetSubDistrictBySubDistrictID

--Begin procedure territory.GetCommunityByCommunityID
EXEC Utility.DropObject 'territory.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.18
-- Description:	A stored procedure to data from the territory.Community table and view
-- ===================================================================================
CREATE PROCEDURE territory.GetCommunityByCommunityID

@ProjectID INT,
@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Community
	SELECT
		C.CommunityID,
		C.CommunityName,
		C.Location,
		C.ParentTerritoryID,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,

		CV.EngagementStatusName,
		CV.ImpactDecisionName,
		CV.Implications,
		CV.KeyPoints,
		CV.Population,
		CV.PopulationSource,
		CV.ProgramNotes1,
		CV.ProgramNotes2,
		CV.ProgramNotes3,
		CV.ProgramNotes4,
		CV.ProgramNotes5,
		CV.RiskMitigation,
		CV.StatusChangeName,
		CV.Summary
	FROM territory.Community C
		JOIN territory.CommunityView CV ON CV.CommunityID = C.CommunityID
			AND CV.ProjectID = C.ProjectID
			AND C.CommunityID = @CommunityID
			AND C.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		A.AssetTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(A.ProjectID, A.TerritoryTypeCode, A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
	WHERE A.TerritoryTypeCode = 'Community'
		AND A.TerritoryID = @CommunityID
		AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Atmospherics
	SELECT 
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID,
		A.AtmosphericTypeName,
		A.ConfidenceLevelName
	FROM atmospheric.Atmospheric A
	WHERE A.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM atmospheric.AtmosphericTerritory T
			WHERE T.AtmosphericID = A.AtmosphericID
				AND T.TerritoryTypeCode = 'Community'
				AND T.TerritoryID = @CommunityID
				AND T.ProjectID = A.ProjectID
			)
	ORDER BY 2, 3, 1

	--Distributors
	SELECT 
		D.DistributorID,
		D.DistributorName,
		D.DistributorTypeName
	FROM productdistributor.Distributor D
	WHERE D.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.DistributorTerritory T
			WHERE T.DistributorID = D.DistributorID
				AND T.TerritoryTypeCode = 'Community'
				AND T.TerritoryID = @CommunityID
				AND T.ProjectID = D.ProjectID
			)
	ORDER BY 2, 3

	--Equipment
	SELECT 1

	--Findings
	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingStatusName,
		F.FindingTypeName
	FROM finding.Finding F
	WHERE F.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM finding.FindingTerritory T
			WHERE T.FindingID = F.FindingID
				AND T.TerritoryTypeCode = 'Community'
				AND T.TerritoryID = @CommunityID
				AND T.ProjectID = F.ProjectID
			)
	ORDER BY 1, 2, 3

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		F.AreaOfOperationTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName
	FROM Force.Force F
	WHERE F.TerritoryTypeCode = 'Community'
		AND F.TerritoryID = @CommunityID
		AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--Incidents
	SELECT
		dbo.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IncidentTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, I.TerritoryTypeCode, I.TerritoryID) AS TerritoryName
	FROM dbo.Incident I
	WHERE I.TerritoryTypeCode = 'Community'
		AND I.TerritoryID = @CommunityID
		AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		dbo.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		P.ProductTypeName,
		P.TargetAudienceName
	FROM productdistributor.Product P
	WHERE P.ProjectID = @ProjectID 
		AND EXISTS
			(
			SELECT 1
			FROM productdistributor.ProductTerritory T
			WHERE T.ProductID = P.ProductID
				AND T.TerritoryTypeCode = 'Community'
				AND T.TerritoryID = @CommunityID
				AND T.ProjectID = @ProjectID
			)
	ORDER BY 1, 4, 3, 5, 2

	--Risks
	SELECT 1

	--Spot Reports
	SELECT 1

	--Units
	SELECT
		FU.ProjectID,
		FU.ForceUnitID,
		FU.UnitName,
		FU.UnitTypeName,
		territory.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(@ProjectID, FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName
	FROM force.ForceUnit FU
	WHERE FU.TerritoryTypeCode = 'Community'
		AND FU.TerritoryID = @CommunityID
		AND FU.ProjectID = @ProjectID

	ORDER BY 1, 2, 3
	
END
GO
--End procedure territory.GetCommunityByCommunityID
