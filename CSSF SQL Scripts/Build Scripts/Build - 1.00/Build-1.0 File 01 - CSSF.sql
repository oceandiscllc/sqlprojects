-- File Name:	Build-1.0 File 01 - CSSF.sql
-- Build Key:	Build-1.0 File 01 - CSSF - 2016.03.01 21.11.34

USE CSSF
GO

-- ==============================================================================================================================
-- Procedures:
--		force.GetForceByForceID
--
-- Tables:
--		territory.Community
--		territory.District
--		territory.Front
--		territory.Governorate
--		territory.ProjectTerritoryType
--		territory.SubDistrict
--
-- Views:
--		territory.CommunityView
--		territory.DistrictView
--		territory.FrontView
--		territory.GovernorateView
--		territory.SubDistrictView
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE CSSF
GO

--Begin table territory.Front
DECLARE @TableName VARCHAR(250) = 'territory.Front'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Front
	(
	FrontID INT,
	ProjectID INT,
	FrontName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'FrontID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,FrontID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Front', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Front
	(FrontID, ProjectID, ParentTerritoryID, FrontName, Location)
SELECT
	D.FrontID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	0,
	D.FrontName,
	D.Location.STAsText()
FROM ICS.territory.Front D
	JOIN ICS.dropdown.Project P ON P.ProjectID = D.ProjectID
GO
--End table territory.Front

--Begin table territory.Governorate
DECLARE @TableName VARCHAR(250) = 'territory.Governorate'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Governorate
	(
	GovernorateID INT,
	ProjectID INT,
	GovernorateName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'GovernorateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,GovernorateID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Governorate', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Governorate
	(GovernorateID, ProjectID, ParentTerritoryID, GovernorateName, Location)
SELECT 
	P.ProvinceID,
	dropdown.GetProjectIDByDatabaseName('AJACS') AS ProjectID,
	0,
	P.ProvinceName,
	NULL
FROM AJACS.dbo.Province P
	
UNION

SELECT
	G.GovernorateID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	G.ParentTerritoryID,
	G.GovernorateName,
	G.Location.STAsText()
FROM ICS.territory.Governorate G
	JOIN ICS.dropdown.Project P ON P.ProjectID = G.ProjectID
GO
--End table territory.Governorate

--Begin table territory.District
DECLARE @TableName VARCHAR(250) = 'territory.District'

EXEC utility.DropObject @TableName

CREATE TABLE territory.District
	(
	DistrictID INT,
	ProjectID INT,
	DistrictName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DistrictID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,DistrictID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_District', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.District
	(DistrictID, ProjectID, ParentTerritoryID, DistrictName, Location)
SELECT
	D.DistrictID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	D.ParentTerritoryID,
	D.DistrictName,
	D.Location.STAsText()
FROM ICS.territory.District D
	JOIN ICS.dropdown.Project P ON P.ProjectID = D.ProjectID
GO
--End table territory.District

--Begin table territory.SubDistrict
DECLARE @TableName VARCHAR(250) = 'territory.SubDistrict'

EXEC utility.DropObject @TableName

CREATE TABLE territory.SubDistrict
	(
	SubDistrictID INT,
	ProjectID INT,
	SubDistrictName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SubDistrictID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,SubDistrictID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SubDistrict', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.SubDistrict
	(SubDistrictID, ProjectID, ParentTerritoryID, SubDistrictName, Location)
SELECT
	SD.SubDistrictID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	SD.ParentTerritoryID,
	SD.SubDistrictName,
	SD.Location.STAsText()
FROM ICS.territory.SubDistrict SD
	JOIN ICS.dropdown.Project P ON P.ProjectID = SD.ProjectID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + P.ProjectCode + '-SubDistrict-OnCSSFPortal', '0') AS INT) = 1
GO
--End table territory.SubDistrict

--Begin table territory.Community
DECLARE @TableName VARCHAR(250) = 'territory.Community'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Community
	(
	CommunityID INT,
	ProjectID INT,
	CommunityName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,CommunityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Community', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Community
	(CommunityID, ProjectID, ParentTerritoryID, CommunityName, Location)
SELECT
	C.CommunityID,
	dropdown.GetProjectIDByDatabaseName('AJACS') AS ProjectID,
	C.ProvinceID,
	C.CommunityName,
	C.Location.STAsText()
FROM AJACS.dbo.Community C 
WHERE CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey('ShowCommunitiesOnCSSFPortal', '1') AS INT) = 1
	AND C.IsActive = 1

UNION

SELECT
	C.CommunityID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	C.ParentTerritoryID,
	C.CommunityName,
	C.Location.STAsText()
FROM ICS.territory.Community C 
	JOIN ICS.dropdown.Project P ON P.ProjectID = C.ProjectID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + P.ProjectCode + '-Community-OnCSSFPortal', '0') AS INT) = 1
GO
--End table territory.Community

--Begin table territory.ProjectTerritoryType
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritoryType'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritoryType
	(
	ProjectTerritoryTypeID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryTypeID INT,
	ParentTerritoryTypeCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryTypeID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritoryType', 'TerritoryTypeID, ParentTerritoryTypeCode'
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID, ParentTerritoryTypeCode)
SELECT
	dropdown.GetProjectIDByDatabaseName('AJACS'),
	TT.TerritoryTypeID,
		
	CASE
		WHEN TT.TerritoryTypeCode = 'Community'
		THEN 'Governorate'
		ELSE NULL
	END
			
FROM territory.TerritoryType TT
WHERE TT.TerritoryTypeCode IN ('Governorate','Community')

UNION
		
SELECT
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	PTT.TerritoryTypeID,
	PTT.ParentTerritoryTypeCode
FROM ICS.territory.ProjectTerritoryType PTT
	JOIN ICS.dropdown.Project P ON P.ProjectID = PTT.ProjectID
		AND P.ProjectID > 0
GO
--End table territory.ProjectTerritoryType

--End file Build File - 01 - Tables.sql

--Begin file Build File - 03 - Procedures.sql
USE CSSF
GO

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.02
-- Description:	A stored procedure to data from the force.Force view
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ProjectID INT,
@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location,		
		F.Notes,
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		F.TerritoryTypeCode,
		F.TerritoryName,
		F.WebLinks,
		F.AreaOfOperationTypeName
	FROM Force.Force F
	WHERE F.ProjectID = @ProjectID
		AND F.ForceID = @ForceID

	SELECT
		FERP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
	WHERE FERP.ProjectID = @ProjectID
		AND FERP.ForceID = @ForceID
	ORDER BY FERP.ResourceProviderName

	SELECT
		FFRP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
	WHERE FFRP.ProjectID = @ProjectID
		AND FFRP.ForceID = @ForceID
	ORDER BY FFRP.ResourceProviderName

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		R.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
			AND R.ProjectID = @ProjectID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullName,
		FU.ForceUnitID,
		FU.TerritoryName,
		FU.UnitName,
		FU.UnitTypeName,
		FU.ProjectID
	FROM force.ForceUnit FU
	WHERE FU.ProjectID = @ProjectID
		AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID

END
GO
--End procedure force.GetForceByForceID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 05 - Views.sql
USE CSSF
GO

--Begin view territory.FrontView
EXEC utility.DropObject 'territory.FrontView'
GO

CREATE VIEW territory.FrontView
AS

SELECT
	FL.FrontID,
	FL.ProjectID,
	FL.FrontName,
	FL.ParentTerritoryID,
	FL.Location,
	
	FR.Implications,
	FR.KeyPoints,
	FR.Population,
	FR.PopulationSource,
	FR.ProgramNotes1,
	FR.ProgramNotes2,
	FR.ProgramNotes3,
	FR.ProgramNotes4,
	FR.ProgramNotes5,
	FR.RiskMitigation,
	FR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Front FL
	JOIN ICS.territory.Front FR ON FR.FrontID = FL.FrontID
	JOIN ICS.dropdown.Project P ON P.ProjectID = FR.ProjectID
		AND FL.ProjectID = dropdown.GetProjectIDByProjectCode(P.ProjectCode)
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = FR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = FR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = FR.StatusChangeID
GO
--End view territory.FrontView

--Begin view territory.GovernorateView
EXEC utility.DropObject 'territory.GovernorateView'
GO

CREATE VIEW territory.GovernorateView
AS

SELECT
	GL.GovernorateID,
	GL.ProjectID,
	GL.GovernorateName,
	GL.ParentTerritoryID,
	GL.Location,
	
	P.Implications,
	P.KeyPoints,
	0 AS Population,
	'' AS PopulationSource,
	P.ProgramNotes1,
	P.ProgramNotes2,
	'' AS ProgramNotes3,
	'' AS ProgramNotes4,
	'' AS ProgramNotes5,
	P.RiskMitigation,
	P.Summary,

	'' AS EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Governorate GL
	JOIN AJACS.dbo.Province P ON P.ProvinceID = GL.GovernorateID
		AND GL.ProjectID = dropdown.GetProjectIDByDatabaseName('AJACS')
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
	JOIN AJACS.dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID

UNION

SELECT
	GL.GovernorateID,
	GL.ProjectID,
	GL.GovernorateName,
	GL.ParentTerritoryID,
	GL.Location,
	
	GR.Implications,
	GR.KeyPoints,
	GR.Population,
	GR.PopulationSource,
	GR.ProgramNotes1,
	GR.ProgramNotes2,
	GR.ProgramNotes3,
	GR.ProgramNotes4,
	GR.ProgramNotes5,
	GR.RiskMitigation,
	GR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Governorate GL
	JOIN ICS.territory.Governorate GR ON GR.GovernorateID = GL.GovernorateID
	JOIN ICS.dropdown.Project P ON P.ProjectID = GR.ProjectID
		AND GL.ProjectID = dropdown.GetProjectIDByProjectCode(P.ProjectCode)
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = GR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = GR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = GR.StatusChangeID
GO
--End view territory.GovernorateView

--Begin view territory.DistrictView
EXEC utility.DropObject 'territory.DistrictView'
GO

CREATE VIEW territory.DistrictView
AS

SELECT
	DL.DistrictID,
	DL.ProjectID,
	DL.DistrictName,
	DL.ParentTerritoryID,
	DL.Location,
	
	DR.Implications,
	DR.KeyPoints,
	DR.Population,
	DR.PopulationSource,
	DR.ProgramNotes1,
	DR.ProgramNotes2,
	DR.ProgramNotes3,
	DR.ProgramNotes4,
	DR.ProgramNotes5,
	DR.RiskMitigation,
	DR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.District DL
	JOIN ICS.territory.District DR ON DR.DistrictID = DL.DistrictID
	JOIN ICS.dropdown.Project P ON P.ProjectID = DR.ProjectID
		AND DL.ProjectID = dropdown.GetProjectIDByProjectCode(P.ProjectCode)
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = DR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = DR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = DR.StatusChangeID
GO
--End view territory.DistrictView

--Begin view territory.SubDistrictView
EXEC utility.DropObject 'territory.SubDistrictView'
GO

CREATE VIEW territory.SubDistrictView
AS

SELECT
	SDL.SubDistrictID,
	SDL.ProjectID,
	SDL.SubDistrictName,
	SDL.ParentTerritoryID,
	SDL.Location,
	
	SDR.Implications,
	SDR.KeyPoints,
	SDR.Population,
	SDR.PopulationSource,
	SDR.ProgramNotes1,
	SDR.ProgramNotes2,
	SDR.ProgramNotes3,
	SDR.ProgramNotes4,
	SDR.ProgramNotes5,
	SDR.RiskMitigation,
	SDR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.SubDistrict SDL
	JOIN ICS.territory.SubDistrict SDR ON SDR.SubDistrictID = SDL.SubDistrictID
	JOIN ICS.dropdown.Project P ON P.ProjectID = SDR.ProjectID
		AND SDL.ProjectID = dropdown.GetProjectIDByProjectCode(P.ProjectCode)
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = SDR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SDR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = SDR.StatusChangeID
GO
--End view territory.SubDistrictView

--Begin view territory.CommunityView
EXEC utility.DropObject 'territory.CommunityView'
GO

CREATE VIEW territory.CommunityView
AS

SELECT
	CL.CommunityID,
	CL.ProjectID,
	CL.CommunityName,
	CL.ParentTerritoryID,
	CL.Location,
	
	CR.Implications,
	CR.KeyPoints,
	CR.Population,
	CR.PopulationSource,
	CR.ProgramNotes1,
	CR.ProgramNotes2,
	CR.ProgramNotes3,
	CR.ProgramNotes4,
	CR.ProgramNotes5,
	CR.RiskMitigation,
	CR.Summary,

	'' AS EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Community CL
	JOIN AJACS.dbo.Community CR ON CR.CommunityID = CL.CommunityID
		AND CL.ProjectID = dropdown.GetProjectIDByDatabaseName('AJACS')
	JOIN AJACS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CR.ImpactDecisionID
	JOIN AJACS.dropdown.StatusChange SC ON SC.StatusChangeID = CR.StatusChangeID

UNION

SELECT
	CL.CommunityID,
	CL.ProjectID,
	CL.CommunityName,
	CL.ParentTerritoryID,
	CL.Location,
	
	CR.Implications,
	CR.KeyPoints,
	CR.Population,
	CR.PopulationSource,
	CR.ProgramNotes1,
	CR.ProgramNotes2,
	CR.ProgramNotes3,
	CR.ProgramNotes4,
	CR.ProgramNotes5,
	CR.RiskMitigation,
	CR.Summary,

	ES.EngagementStatusName,
	ID.HexColor,
	ID.ImpactDecisionName,
	SC.Direction,
	SC.StatusChangeName
FROM territory.Community CL
	JOIN ICS.territory.Community CR ON CR.CommunityID = CL.CommunityID
	JOIN ICS.dropdown.Project P ON P.ProjectID = CR.ProjectID
		AND CL.ProjectID = dropdown.GetProjectIDByProjectCode(P.ProjectCode)
	JOIN ICS.dropdown.EngagementStatus ES ON ES.EngagementStatusID = CR.EngagementStatusID
	JOIN ICS.dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CR.ImpactDecisionID
	JOIN ICS.dropdown.StatusChange SC ON SC.StatusChangeID = CR.StatusChangeID
GO
--End view territory.CommunityView

--End file Build File - 05 - Views.sql

--Begin table permissionable.PermissionableGroup
EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'ForceAsset', 'Forces & Assets', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'ProductDistributor', 'Product Distribution', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Situational Data', 0
GO
EXEC utility.SavePermissionableGroup 'Territories', 'Territories', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log entry', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'View the list of permissionable templates', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'View the list of system users', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View a system user', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the list of server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'View the list of forces', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'ListUnits', NULL, 'Force.ListUnits', 'View the list of units attached to a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Force', 'ViewUnit', NULL, 'Force.ViewUnit', 'View a unit attached to a force', 0, 0, 'ForceAsset'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', 'AJACS', 'Objective.ChartList.AJACS', 'View the objectives overview for project AJACS', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', 'MOA', 'Objective.ChartList.MOA', 'View the objectives overview for project MOA', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage objectives', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'View the objectives overview', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Campaign', 'List', NULL, 'Campaign.List', 'View the list of campaigns', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Campaign', 'View', NULL, 'Campaign.View', 'View a campaign', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Distributor', 'List', NULL, 'Distributor.List', 'View the list of distributors', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Distributor', 'View', NULL, 'Distributor.View', 'View a distributor', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Product', 'List', NULL, 'Product.List', 'View the list of products', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Product', 'View', NULL, 'Product.View', 'View a product', 0, 0, 'ProductDistributor'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'View the list of atmospheric reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'View the list of findings', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'View the list of incident reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'View the list of recommendations', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'View the list of requests for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'View the list of risks', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'View the list of spot reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'View the list of communities', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View a community', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the community analysis tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'View the community implementation tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the community information tab', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territories'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub-district', 0, 0, 'Territories'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.0 File 01 - CSSF - 2016.03.01 21.11.34')
GO
--End build tracking

