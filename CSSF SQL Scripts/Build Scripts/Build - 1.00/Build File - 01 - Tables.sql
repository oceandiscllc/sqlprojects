USE CSSF
GO

--Begin table territory.Front
DECLARE @TableName VARCHAR(250) = 'territory.Front'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Front
	(
	FrontID INT,
	ProjectID INT,
	FrontName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'FrontID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,FrontID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Front', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Front
	(FrontID, ProjectID, ParentTerritoryID, FrontName, Location)
SELECT
	D.FrontID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	0,
	D.FrontName,
	D.Location.STAsText()
FROM ICS.territory.Front D
	JOIN ICS.dropdown.Project P ON P.ProjectID = D.ProjectID
GO
--End table territory.Front

--Begin table territory.Governorate
DECLARE @TableName VARCHAR(250) = 'territory.Governorate'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Governorate
	(
	GovernorateID INT,
	ProjectID INT,
	GovernorateName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'GovernorateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,GovernorateID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Governorate', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Governorate
	(GovernorateID, ProjectID, ParentTerritoryID, GovernorateName, Location)
SELECT 
	P.ProvinceID,
	dropdown.GetProjectIDByDatabaseName('AJACS') AS ProjectID,
	0,
	P.ProvinceName,
	NULL
FROM AJACS.dbo.Province P
	
UNION

SELECT
	G.GovernorateID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	G.ParentTerritoryID,
	G.GovernorateName,
	G.Location.STAsText()
FROM ICS.territory.Governorate G
	JOIN ICS.dropdown.Project P ON P.ProjectID = G.ProjectID
GO
--End table territory.Governorate

--Begin table territory.District
DECLARE @TableName VARCHAR(250) = 'territory.District'

EXEC utility.DropObject @TableName

CREATE TABLE territory.District
	(
	DistrictID INT,
	ProjectID INT,
	DistrictName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DistrictID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,DistrictID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_District', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.District
	(DistrictID, ProjectID, ParentTerritoryID, DistrictName, Location)
SELECT
	D.DistrictID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	D.ParentTerritoryID,
	D.DistrictName,
	D.Location.STAsText()
FROM ICS.territory.District D
	JOIN ICS.dropdown.Project P ON P.ProjectID = D.ProjectID
GO
--End table territory.District

--Begin table territory.SubDistrict
DECLARE @TableName VARCHAR(250) = 'territory.SubDistrict'

EXEC utility.DropObject @TableName

CREATE TABLE territory.SubDistrict
	(
	SubDistrictID INT,
	ProjectID INT,
	SubDistrictName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SubDistrictID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,SubDistrictID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SubDistrict', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.SubDistrict
	(SubDistrictID, ProjectID, ParentTerritoryID, SubDistrictName, Location)
SELECT
	SD.SubDistrictID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	SD.ParentTerritoryID,
	SD.SubDistrictName,
	SD.Location.STAsText()
FROM ICS.territory.SubDistrict SD
	JOIN ICS.dropdown.Project P ON P.ProjectID = SD.ProjectID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + P.ProjectCode + '-SubDistrict-OnCSSFPortal', '0') AS INT) = 1
GO
--End table territory.SubDistrict

--Begin table territory.Community
DECLARE @TableName VARCHAR(250) = 'territory.Community'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Community
	(
	CommunityID INT,
	ProjectID INT,
	CommunityName NVARCHAR(250),
	ParentTerritoryID INT,
	Location VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID,CommunityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Community', 'ProjectID,ParentTerritoryID'
GO

INSERT INTO territory.Community
	(CommunityID, ProjectID, ParentTerritoryID, CommunityName, Location)
SELECT
	C.CommunityID,
	dropdown.GetProjectIDByDatabaseName('AJACS') AS ProjectID,
	C.ProvinceID,
	C.CommunityName,
	C.Location.STAsText()
FROM AJACS.dbo.Community C 
WHERE CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey('ShowCommunitiesOnCSSFPortal', '1') AS INT) = 1
	AND C.IsActive = 1

UNION

SELECT
	C.CommunityID,
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	C.ParentTerritoryID,
	C.CommunityName,
	C.Location.STAsText()
FROM ICS.territory.Community C 
	JOIN ICS.dropdown.Project P ON P.ProjectID = C.ProjectID
		AND CAST(ICS.dbo.GetServerSetupValueByServerSetupKey('Show-' + P.ProjectCode + '-Community-OnCSSFPortal', '0') AS INT) = 1
GO
--End table territory.Community

--Begin table territory.ProjectTerritoryType
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritoryType'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritoryType
	(
	ProjectTerritoryTypeID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryTypeID INT,
	ParentTerritoryTypeCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryTypeID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritoryType', 'TerritoryTypeID, ParentTerritoryTypeCode'
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID, ParentTerritoryTypeCode)
SELECT
	dropdown.GetProjectIDByDatabaseName('AJACS'),
	TT.TerritoryTypeID,
		
	CASE
		WHEN TT.TerritoryTypeCode = 'Community'
		THEN 'Governorate'
		ELSE NULL
	END
			
FROM territory.TerritoryType TT
WHERE TT.TerritoryTypeCode IN ('Governorate','Community')

UNION
		
SELECT
	dropdown.GetProjectIDByProjectCode(P.ProjectCode) AS ProjectID,
	PTT.TerritoryTypeID,
	PTT.ParentTerritoryTypeCode
FROM ICS.territory.ProjectTerritoryType PTT
	JOIN ICS.dropdown.Project P ON P.ProjectID = PTT.ProjectID
		AND P.ProjectID > 0
GO
--End table territory.ProjectTerritoryType
