USE CSSF
GO

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.02
-- Description:	A stored procedure to data from the force.Force view
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ProjectID INT,
@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location,		
		F.Notes,
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		F.TerritoryTypeCode,
		F.TerritoryName,
		F.WebLinks,
		F.AreaOfOperationTypeName
	FROM Force.Force F
	WHERE F.ProjectID = @ProjectID
		AND F.ForceID = @ForceID

	SELECT
		FERP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
	WHERE FERP.ProjectID = @ProjectID
		AND FERP.ForceID = @ForceID
	ORDER BY FERP.ResourceProviderName

	SELECT
		FFRP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
	WHERE FFRP.ProjectID = @ProjectID
		AND FFRP.ForceID = @ForceID
	ORDER BY FFRP.ResourceProviderName

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		R.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
			AND R.ProjectID = @ProjectID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullName,
		FU.ForceUnitID,
		FU.TerritoryName,
		FU.UnitName,
		FU.UnitTypeName,
		FU.ProjectID
	FROM force.ForceUnit FU
	WHERE FU.ProjectID = @ProjectID
		AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID

END
GO
--End procedure force.GetForceByForceID
