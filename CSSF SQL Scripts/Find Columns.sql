DECLARE @cColumnName2 VARCHAR(50) = '%SpotReportID%'

SELECT
	S.Name AS SchemaName,
	T.Name AS TableName,
	C.Name AS ColumnName,
	'SELECT * FROM ' + S.Name + '.' + T.Name + ' WHERE ' + C.Name + ' = 43',
	'DELETE FROM ' + S.Name + '.' + T.Name + ' WHERE ' + C.Name + ' = 43'
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND 
			(
				C.Name LIKE @cColumnName2
			)
		--AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')

ORDER BY 2, 3
