USE EFE
GO

SELECT 
	PG.PermissionableGroupID,
	P.PermissionableID,
	PG.PermissionableGroupName,
	P.ControllerName,
	P.MethodName,
	P.PermissionCode,
	P.PermissionableLineage,
	P.Description,
	P.IsGlobal,
	PG.DisplayOrder AS GroupDisplayOrder,
	P.DisplayOrder
FROM permissionable.Permissionable P
	JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID, P.DisplayOrder, P.PermissionableLineage, P.PermissionableID

SELECT PG.*
FROM permissionable.PermissionableGroup PG
ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID

/*
DELETE PG
FROM permissionable.PermissionableGroup PG
WHERE PG.PermissionableGroupCode IN ('eee','iiii','jjj','qaqa','ststst','zzz')

UPDATE PG
SET PG.DisplayOrder = 0
FROM permissionable.PermissionableGroup PG
*/
