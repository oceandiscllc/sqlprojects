	SELECT
		'JLLIS' AS DatabaseName, 
		MAX(S.SetupValue) AS LastBuild,
		LEFT(S.SetupValue, CHARINDEX('-', S.SetupValue, 0) - 1) AS BuildName
	FROM JLLIS.Utility.Setup S
	WHERE S.SetupKey = 'BuildKey'
	GROUP BY LEFT(S.SetupValue, CHARINDEX('-', S.SetupValue, 0) - 1)
	
	UNION

	SELECT
		'JSCC' AS DatabaseName, 
		MAX(S.SetupValue) AS LastBuild,
		LEFT(S.SetupValue, CHARINDEX('-', S.SetupValue, 0) - 1) AS BuildName
	FROM JSCC.Utility.Setup S
	WHERE S.SetupKey = 'BuildKey'
	GROUP BY LEFT(S.SetupValue, CHARINDEX('-', S.SetupValue, 0) - 1)
	
	ORDER BY LEFT(S.SetupValue, CHARINDEX('-', S.SetupValue, 0) - 1) DESC, DatabaseName
