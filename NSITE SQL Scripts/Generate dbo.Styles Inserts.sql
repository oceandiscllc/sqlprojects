/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
'INSERT INTO dbo.Styles (tier,cpColor,lpColor,pocboxColor,pocoutlineColor,cfColor,isActive,roundbox,containerColor,containerBorderColor,tableHeaderColor,tableHeaderSelectedColor,tableHeaderBorderColor,oddRowColor,evenRowColor,rowHoverColor,tableFontColor,uiTheme) VALUES ('
+ CASE WHEN S.Tier IS NULL OR LEN(LTRIM(S.Tier)) = 0 THEN 'NULL' ELSE '''' + S.Tier + '''' END + ','
+ CASE WHEN S.cpColor IS NULL OR LEN(LTRIM(S.cpColor)) = 0 THEN 'NULL' ELSE '''' + S.cpColor + '''' END + ','
+ CASE WHEN S.lpColor IS NULL OR LEN(LTRIM(S.lpColor)) = 0 THEN 'NULL' ELSE '''' + S.lpColor + '''' END + ','
+ CASE WHEN S.pocboxColor IS NULL OR LEN(LTRIM(S.pocboxColor)) = 0 THEN 'NULL' ELSE '''' + S.pocboxColor + '''' END + ','
+ CASE WHEN S.pocoutlineColor IS NULL OR LEN(LTRIM(S.pocoutlineColor)) = 0 THEN 'NULL' ELSE '''' + S.pocoutlineColor + '''' END + ','
+ CASE WHEN S.cfColor IS NULL OR LEN(LTRIM(S.cfColor)) = 0 THEN 'NULL' ELSE '''' + S.cfColor + '''' END + ','
+ CAST(S.isActive as char(1)) + ','
+ CASE WHEN S.roundbox IS NULL OR LEN(LTRIM(S.roundbox)) = 0 THEN 'NULL' ELSE '''' + S.roundbox + '''' END + ','
+ CASE WHEN S.containerColor IS NULL OR LEN(LTRIM(S.containerColor)) = 0 THEN 'NULL' ELSE '''' + S.containerColor + '''' END + ','
+ CASE WHEN S.containerBorderColor IS NULL OR LEN(LTRIM(S.containerBorderColor)) = 0 THEN 'NULL' ELSE '''' + S.containerBorderColor + '''' END + ','
+ CASE WHEN S.tableHeaderColor IS NULL OR LEN(LTRIM(S.tableHeaderColor)) = 0 THEN 'NULL' ELSE '''' + S.tableHeaderColor + '''' END + ','
+ CASE WHEN S.tableHeaderSelectedColor IS NULL OR LEN(LTRIM(S.tableHeaderSelectedColor)) = 0 THEN 'NULL' ELSE '''' + S.tableHeaderSelectedColor + '''' END + ','
+ CASE WHEN S.tableHeaderBorderColor IS NULL OR LEN(LTRIM(S.tableHeaderBorderColor)) = 0 THEN 'NULL' ELSE '''' + S.tableHeaderBorderColor + '''' END + ','
+ CASE WHEN S.oddRowColor IS NULL OR LEN(LTRIM(S.oddRowColor)) = 0 THEN 'NULL' ELSE '''' + S.oddRowColor + '''' END + ','
+ CASE WHEN S.evenRowColor IS NULL OR LEN(LTRIM(S.evenRowColor)) = 0 THEN 'NULL' ELSE '''' + S.evenRowColor + '''' END + ','
+ CASE WHEN S.rowHoverColor IS NULL OR LEN(LTRIM(S.rowHoverColor)) = 0 THEN 'NULL' ELSE '''' + S.rowHoverColor + '''' END + ','
+ CASE WHEN S.tableFontColor IS NULL OR LEN(LTRIM(S.tableFontColor)) = 0 THEN 'NULL' ELSE '''' + S.tableFontColor + '''' END + ','
+ CASE WHEN S.uiTheme IS NULL OR LEN(LTRIM(S.uiTheme)) = 0 THEN 'NULL' ELSE '''' + S.uiTheme + '''' END + ')'
FROM ussocom.dbo.styles S