DECLARE @tTable TABLE
	(
	MenuItemID int,
	ParentMenuItemID int,
	Lineage varchar(max),
	NodeLevel int
	)
;
WITH HD (MenuItemID,ParentMenuItemID,Lineage,NodeLevel)
	AS 
	(
	SELECT
		MI.MenuItemID,
		MI.ParentMenuItemID,
		CAST(MI.MenuItemID as varchar(50)),
		1
	FROM JLLIS.dbo.MenuItem MI
	WHERE MI.ParentMenuItemID = 0

	UNION ALL
	
	SELECT
		MI.MenuItemID,
		MI.ParentMenuItemID,
		CAST(HD.Lineage + ',' + CAST(MI.MenuItemID as varchar(50)) as varchar(50)),
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.MenuItem MI
		JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID 
	)

INSERT INTO @tTable
	(MenuItemID,ParentMenuItemID,Lineage,NodeLevel)
SELECT 
	HD.MenuItemID,
	HD.ParentMenuItemID,
	HD.Lineage,
	HD.NodeLevel
FROM HD

SELECT 
	T1.MenuItemID,
	T1.ParentMenuItemID,
	T1.Lineage,
	T1.NodeLevel,
	MI.MenuItem,
	MI.MenuItemCode,
	MI.MenuItemLink,
	MI.DisplayOrder,
	MI.IsActive,
	MI.SecLev,
	
	CASE
		WHEN (SELECT COUNT(T2.Lineage) FROM @tTable T2 WHERE T2.Lineage LIKE CAST(T1.MenuItemID as varchar(50)) + ',%') > 0
		THEN 1
		ELSE 0
	END AS HasChildren
FROM @tTable T1
	JOIN JLLIS.dbo.MenuItem MI ON MI.MenuItemID = T1.MenuItemID
ORDER BY T1.NodeLevel, MI.DisplayOrder

