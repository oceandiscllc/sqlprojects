/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	COUNT(ErrorMessage) AS ItemCount,
	ErrorMessage
FROM [Todd].[dbo].[ErrorLog]
GROUP BY ErrorMessage
ORDER BY COUNT(ErrorMessage) DESC
