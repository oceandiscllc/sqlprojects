SELECT
	S.Name AS SchemaName,
	O.Name AS TableName, 
	DC.Name,
	DC.Definition
FROM sys.default_constraints DC
	JOIN sys.Objects O ON O.Object_ID = DC.Parent_Object_ID
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND DC.Name LIKE '%LegacyInstanceID'
		AND DC.Definition LIKE '%11%'
ORDER BY
	S.Name,
	O.Name,
	DC.Name
