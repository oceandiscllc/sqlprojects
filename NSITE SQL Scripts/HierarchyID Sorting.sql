SELECT 
	Row_Number() OVER(ORDER BY CAST('/' + T.VersionNumber + '/' AS HIERARCHYID)) AS RowIndex, 
	T.VersionNumber, 
	T.DownloadPath
FROM 
	(
	VALUES
		('1','a.com'),
    ('1.11','b.com'),
    ('2.1.4','c.com'),
    ('2.1','d.com'),
    ('2.0.0','e.com')        
	) AS T (VersionNumber, DownloadPath)
ORDER BY CAST('/' + T.VersionNumber + '/' AS HIERARCHYID)