-- The Silo Model --

DECLARE @nTopTierID int

--Step 1:  Find out what tier I am in
SET @nTopTierID = (SELECT T.TierID FROM dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'MARSOC Child 2');

--Step 2:  Get my descendants for the dropdown
WITH HD (DisplayOrder,TierName,TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(255), T.TierName),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		1 
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTopTierID

	UNION ALL 
	
	SELECT
		CONVERT(varchar(255), RTRIM(DisplayOrder) + ',' + T.TierName),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
SELECT 
	HD.DisplayOrder,
	HD.TierName,
	HD.TierID, 
	HD.ParentTierID, 
	ABS(HD.NodeLevel) AS NodeLevel
FROM HD
ORDER BY DisplayOrder