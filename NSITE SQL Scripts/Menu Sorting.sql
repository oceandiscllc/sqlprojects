DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
FROM JLLIS.dbo.MenuItem MI

;
WITH HD (DisplayIndex,Lineage,MenuItemID,ParentMenuItemID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItem) AS VARCHAR(10)), @nPadLength)),
		CAST(MI.MenuItem AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		1
	FROM JLLIS.dbo.MenuItem MI
	WHERE MI.ParentMenuItemID = 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItem) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.Lineage  + ' > ' + MI.MenuItem AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.MenuItem MI
		JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
	)

SELECT
	HD1.DisplayIndex,
	HD1.NodeLevel,
	HD1.ParentMenuItemID,
	HD1.MenuItemID,
	HD1.Lineage,
	MI.MenuItemCode,
	MI.MenuItem,
	MI.MenuItemLink,
	MI.SecLev,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN JLLIS.dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
ORDER BY HD1.DisplayIndex