USE JLLIS
GO

DECLARE @tNew TABLE (TaskCategoryPrefix VARCHAR(10), TaskNumber VARCHAR(50), TaskDefinition VARCHAR(MAX))
DECLARE @tOld TABLE (TaskCategoryPrefix VARCHAR(10), TaskNumber VARCHAR(50), TaskDefinition VARCHAR(MAX))

INSERT INTO @tNew (TaskCategoryPrefix, TaskNumber, TaskDefinition) SELECT 'ART' AS TaskCategoryPrefix, REPLACE(TL.TaskNumber, 'ART ', ''), RTRIM(LTRIM(TL.TaskDefinition)) AS TaskDefinition FROM tasklist.Army TL ORDER BY 2
INSERT INTO @tOld (TaskCategoryPrefix, TaskNumber, TaskDefinition) SELECT TC.TaskCategoryPrefix, T.TaskNumber, REPLACE(T.TaskDefinition, '"', '') AS TaskDefinition FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix LIKE 'ART%' ORDER BY 2

--SELECT * FROM @tNew
--SELECT * FROM @tOld

--SELECT COUNT(*) FROM @tNew N WHERE NOT EXISTS (SELECT 1	FROM @tOld O WHERE O.TaskNumber = N.taskNumber)
--SELECT COUNT(*) FROM @tOld O WHERE NOT EXISTS (SELECT 1 FROM @tNew N WHERE N.TaskNumber = O.taskNumber)

--SELECT * FROM @tNew N WHERE NOT EXISTS (SELECT 1 FROM @tOld O WHERE O.TaskNumber = N.taskNumber) ORDER BY 2
--SELECT * FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix LIKE 'ART%' ORDER BY 4


SELECT
	'UPDATE T SET T.TaskName = ''' 
		+ REPLACE(LTRIM(RTRIM(TL.TaskName)), '''', '''''')
		+ ''', T.TaskDefinition = '''
		+ REPLACE(LTRIM(RTRIM(TL.TaskDefinition)), '''', '''''')
		+ ''' FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix = ''' + LEFT(TL.TaskNumber, 3) + ''' AND T.TaskNumber = '''
		+ REPLACE(TL.TaskNumber, 'ART ', '')
		+ ''''
		+ ';' AS SQLText
FROM tasklist.Army TL
	JOIN dropdown.Task T ON T.TaskNumber = REPLACE(TL.TaskNumber, 'ART ', '')
	JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID 
		AND TC.TaskCategoryPrefix = LEFT(TL.TaskNumber, 3)

SELECT
	'IF NOT EXISTS (SELECT 1 FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix = '''
	+ LEFT(TL.TaskNumber, 3)
	+ ''' AND T.TaskNumber = '''
	+ REPLACE(TL.TaskNumber, 'ART ', '')
	+ ''') BEGIN INSERT INTO dropdown.Task (TaskCategoryID, TaskName, TaskNumber, TaskDefinition, ParentTaskID) SELECT TC.TaskCategoryID,'''
	+ REPLACE(LTRIM(RTRIM(TL.TaskName)), '''', '''''')
	+ ''','''
	+ REPLACE(TL.TaskNumber, 'ART ', '')
	+ ''','''
	+ REPLACE(LTRIM(RTRIM(TL.TaskDefinition)), '''', '''''')
	+ ''','
	+ 'ISNULL((SELECT T.TaskID FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix = ''' + LEFT(TL.TaskNumber, 3) + ''' AND T.TaskNumber = ' 
	+ ''''
	+ REPLACE(TL.TaskNumber, 'ART ', '')
	+ '''), 0) FROM dropdown.TaskCategory TC WHERE TC.TaskCategoryPrefix = ''' + LEFT(TL.TaskNumber, 3) + ''' END;' AS SQLText
FROM tasklist.Army TL
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM dropdown.Task T
	JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID 
		AND TC.TaskCategoryPrefix = LEFT(TL.TaskNumber, 3)
		AND T.TaskNumber = REPLACE(TL.TaskNumber, 'ART ', '')
		)

SELECT
	'UPDATE T SET T.TaskName = ''DELETED - ' + T.TaskName + ''' FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix = '''
	--'UPDATE T SET T.TaskName = ''***********NOT CURRENTLY USED FOR A MARINE CORPS TASK***********'' FROM dropdown.Task T JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID AND TC.TaskCategoryPrefix = '''
	+ TC.TaskCategoryPrefix 
	+ ''' AND T.TaskNumber = '''
	+ T.TaskNumber
	+ ''';' AS SQLText
FROM dropdown.Task T
	JOIN dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID 
		AND TC.TaskCategoryPrefix LIKE 'ART%'
		AND NOT EXISTS 
			(
			SELECT 1
			FROM tasklist.Army TL
			WHERE REPLACE(TL.TaskNumber, 'ART ', '') = T.TaskNumber
			)
