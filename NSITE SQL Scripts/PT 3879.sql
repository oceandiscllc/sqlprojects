USE JSCC
GO

INSERT INTO dbo.EventLog
	(UserID, GoldMineID, Type, Descrip, TierID)
SELECT
	119840,
	B.BinderID,
	'Updated Binder Owner',
	'Binder owner changed from Tier User ID ' + CAST(B.CreatedBy AS VARCHAR(10)) + ' to Tier User ID 119840',
	B.OriginatingTierID
FROM dbo.Binder B
WHERE B.BinderID IN (8423,8424,8425,8426,8427,8428,8429,8430,8431,8432,8433,1235)

UPDATE B
SET B.CreatedBy = 119840
FROM dbo.Binder B
WHERE B.BinderID IN (8423,8424,8425,8426,8427,8428,8429,8430,8431,8432,8433,1235)
GO