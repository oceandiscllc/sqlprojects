USE JLLIS
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.ClassificationDataMapping'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Utility.ClassificationDataMapping
	
CREATE TABLE Utility.ClassificationDataMapping
	(
	ClassificationDataMappingID int IDENTITY(1,1) NOT NULL,
	SchemaName varchar(250),
	TableName varchar(250),
	Prefix varchar(50),
	ClassificationDataCode varchar(50),
	ColumnName varchar(250)
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ClassificationDataMappingID'
GO

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Background','Classification','backgroundClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Background','Caveat','discussioncaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Background','ReleasableTo','backgroundrelto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','EventDescription','Classification','eventdescriptionClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','EventDescription','Caveat','eventcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','EventDescription','ReleasableTo','eventdescriptionrelto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Implications','Classification','implicationsClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Implications','Caveat','implicationcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Implications','ReleasableTo','implicationsrelto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','LMS','Classification','classification')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','LMS','Caveat','overallcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','LMS','ReleasableTo','releasableto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Observations','Classification','observationsClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Observations','Caveat','observationcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Observations','ReleasableTo','observationsrelto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Recommendations','Classification','recommendationsClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Recommendations','Caveat','recommendationcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Recommendations','ReleasableTo','recommendationsrelto')

INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Summary','Classification','summaryClass')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Summary','Caveat','commentcaveat')
INSERT INTO Utility.ClassificationDataMapping (SchemaName,TableName,Prefix,ClassificationDataCode,ColumnName) VALUES ('dbo','LMS','Summary','ReleasableTo','summaryrelto')
GO