-- Begin PT 2790
DECLARE @nStatusID INT

SELECT @nStatusID = S.StatusID
FROM JLLIS.Dropdown.Status S
WHERE S.Status = 'Initial Analysis'

UPDATE I
SET I.StatusID = @nStatusID
FROM JLLIS.dbo.Issue I
	JOIN JLLIS.Dropdown.Status S ON S.StatusID = I.StatusID
WHERE S.Status = 'Analysis'
GO
--End PT 2790
