USE prototypeManagementTool
GO

--SELECT * FROM permissionable.PermissionGroup

DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(PG.PermissionGroupID) AS VARCHAR(50)))
FROM permissionable.PermissionGroup PG

;
WITH HD (DisplayIndex,Lineage,PermissionGroupID,ParentPermissionGroupID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY PG.IsRole DESC, PG.DisplayOrder, PG.PermissionGroupName) AS VARCHAR(10)), @nPadLength)),
		CAST(PG.PermissionGroupName AS VARCHAR(MAX)),
		PG.PermissionGroupID,
		PG.ParentPermissionGroupID,
		1
	FROM permissionable.PermissionGroup PG
	WHERE PG.ParentPermissionGroupID = 0

	UNION ALL

	SELECT
		CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY PG.DisplayOrder, PG.PermissionGroupName) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.Lineage  + ' > ' + PG.PermissionGroupName AS VARCHAR(MAX)),
		PG.PermissionGroupID,
		PG.ParentPermissionGroupID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM permissionable.PermissionGroup PG
		JOIN HD ON HD.PermissionGroupID = PG.ParentPermissionGroupID
	)

SELECT
	HD1.DisplayIndex,
	HD1.NodeLevel,
	HD1.ParentPermissionGroupID,
	HD1.Lineage,
	HD1.PermissionGroupID,
	PG1.PermissionGroupName,
	PG1.IsActive,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionGroupID = HD1.PermissionGroupID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN permissionable.PermissionGroup PG1 ON PG1.PermissionGroupID = HD1.PermissionGroupID
ORDER BY HD1.DisplayIndex
