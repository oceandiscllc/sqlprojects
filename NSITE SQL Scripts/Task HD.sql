USE JLLIS
GO

DECLARE @nPadLength INT = (SELECT LEN(CAST(COUNT(T.TaskID) AS VARCHAR(50))) FROM Dropdown.Task T)

;
WITH HD AS
	(
	SELECT
		CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY TT.TaskType, TC.TaskCategoryPrefix ORDER BY TT.DisplayOrder, TC.DisplayOrder, TC.TaskCategoryPrefix, T.TaskNumber) AS VARCHAR(10)), @nPadLength)) AS RowIndex,
		T.TaskID,
		T.ParentTaskID,
		1 AS NodeLevel
	FROM Dropdown.Task T
		JOIN Dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID
		JOIN Dropdown.TaskType TT ON TT.TaskTypeID = TC.TaskTypeID
			AND T.ParentTaskID = 0
			AND T.TaskID <> 0

	UNION ALL

	SELECT
		CONVERT(VARCHAR(255), RTRIM(HD.RowIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY TT.TaskType, TC.TaskCategoryPrefix ORDER BY TT.DisplayOrder, TC.DisplayOrder, TC.TaskCategoryPrefix, T.TaskNumber) AS VARCHAR(10)), @nPadLength)) AS RowIndex,
		T.TaskID,
		T.ParentTaskID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM Dropdown.Task T
		JOIN HD ON HD.TaskID = T.ParentTaskID
		JOIN Dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID
		JOIN Dropdown.TaskType TT ON TT.TaskTypeID = TC.TaskTypeID
	)

SELECT
		T.TaskID,
    TT.TaskType,
    TC.TaskCategory,
    TC.TaskCategoryPrefix,
    T.TaskNumber,
    T.TaskName,
    T.TaskDefinition,
    T.ParentTaskID,
    T.IsActive
FROM HD
  JOIN Dropdown.Task T ON T.TaskID = HD.TaskID
  JOIN Dropdown.TaskCategory TC ON TC.TaskCategoryID = T.TaskCategoryID
  JOIN Dropdown.TaskType TT ON TT.TaskTypeID = TC.TaskTypeID
ORDER BY TT.TaskType, TC.TaskCategoryPrefix , RowIndex
