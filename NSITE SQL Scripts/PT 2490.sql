USE JLLIS
GO

--Begin PT 2490
DECLARE @cTableName VARCHAR(250) = 'Dropdown.Classification'

EXEC Utility.AddColumn @cTableName, 'ClassificationAbbreviationParenthetical', 'VARCHAR(100)'
GO

UPDATE Dropdown.Classification
SET ClassificationAbbreviationParenthetical = '(' + ClassificationAbbreviation + ')'
WHERE ClassificationAbbreviationParenthetical IS NULL
	AND ClassificationID > 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.Classification C WHERE C.Classification = 'Unclassified//FGI')
	BEGIN
	
	DECLARE @nDisplayOrder INT
	
	SELECT @nDisplayOrder = MAX(C.DisplayOrder) + 1
	FROM Dropdown.Classification C
	
	INSERT INTO Dropdown.Classification
		(Classification,ClassificationAbbreviation,ClassificationAbbreviationParenthetical,ClassificationWeight,DisplayOrder,IsRestricted)
	VALUES
		('Unclassified//FGI','U//FGI','(U)//FGI',180,@nDisplayOrder,1)
		
	END
--ENDIF
GO

UPDATE Dropdown.Classification
SET ClassificationAbbreviationParenthetical = '(U)//FGI'
WHERE ClassificationAbbreviation = 'U//FGI'
GO

--Begin function dbo.GetClassificationAbbreviationByClassificationDataID
EXEC Utility.DropObject 'dbo.GetClassificationAbbreviationByClassificationDataID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date: 2011.08.16
-- Description: A function to return a ClassificationAbbreviation from a ClassificationDataID
--
-- Author:			Todd Pires
-- Modify date: 2012.07.26
-- Description: changed the length of the return value
--
-- Author:			Todd Pires
-- Modify date: 2013.11.13
-- Description: repointed the IncludeParenthesis logic to the ClassificationAbbreviationParenthetical field
-- ========================================================================================================
CREATE FUNCTION dbo.GetClassificationAbbreviationByClassificationDataID
(
@nClassificationDataID INT,
@nIncludeParenthesis BIT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cRetVal VARCHAR(50) = ''

SET @cRetVal = 
	(
	SELECT 

		CASE
			WHEN @nIncludeParenthesis = 0
			THEN ISNULL(C.ClassificationAbbreviation, 'U')
			ELSE ISNULL(C.ClassificationAbbreviationParenthetical, '(U)')
		END
		
	FROM Dropdown.Classification C
		JOIN dbo.ClassificationData CD ON CD.ClassificationID = C.ClassificationID
			AND CD.ClassificationDataID = @nClassificationDataID
	)

IF @cRetVal IS NULL
	BEGIN

	SET @cRetVal = 
		CASE
			WHEN @nIncludeParenthesis = 0
			THEN 'U'
			ELSE '(U)'
		END
	
	END
--ENDIF

RETURN @cRetVal
END
GO
--End function dbo.GetClassificationAbbreviationByClassificationDataID
--End PT 2490