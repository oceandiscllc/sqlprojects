DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
	DROP TABLE #tTable

CREATE TABLE #tTable
	(
	DatabaseName varchar(50),
	ItemCount int,
	Event varchar(250),
	Organization varchar(250),
	MonthNumber int,
	YearNumber int
	)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB
	WHERE DB.Name IN ('ACGU','ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
		AND DB.Name NOT LIKE '%SOCOM%'
	ORDER BY DB.Name
	
OPEN oCursor
FETCH oCursor INTO @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 
		'INSERT INTO #tTable (DatabaseName, ItemCount, Event, Organization, MonthNumber, YearNumber) '
		+ 'SELECT ''' + @cDatabaseName + ''', COUNT(D.LMSID) AS ItemCount, E.Event, T.TierLabel, D.MonthNumber, D.YearNumber '
		+ 'FROM (SELECT L.LMSID, L.EventID, L.OriginatingTierID, MONTH(L.CreationDate) AS MonthNumber, YEAR(L.CreationDate) AS YearNumber FROM '
		+ @cDatabaseName + '.dbo.LMS L WHERE ImportSource = ''JTIMS'' ) D JOIN '
		+ @cDatabaseName + '.Dropdown.Event E ON E.EventID = D.EventID JOIN JLLIS.dbo.Tier T ON T.TierID = D.OriginatingTierID '
		+ 'GROUP BY D.YearNumber, D.MonthNumber, T.TierLabel, E.Event '
		+ 'ORDER BY D.YearNumber, D.MonthNumber, T.TierLabel, E.Event'
	
	EXEC(@cSQL)

	FETCH oCursor into @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

SELECT 
	T.DatabaseName AS Instance, 
	T.ItemCount AS [Count], 
	T.Event, 
	T.Organization, 
	LEFT(DATENAME(month, DATEADD(month, T.MonthNumber, 0) - 1), 3) AS [Create Month],
	T.YearNumber AS [Create Year]
FROM #tTable T