SELECT
	COUNT(MD.MetadataID) AS ItemCount,
	MD.Metadata,
	MDT.MetadataType
FROM Dropdown.Metadata MD WITH (NOLOCK)
	JOIN Dropdown.MetadataType MDT WITH (NOLOCK) ON MDT.MetadataTypeID = MD.MetadataTypeID
	JOIN dbo.LMSMetadata LMD WITH (NOLOCK) ON LMD.MetadataID = MD.MetadataID
	JOIN dbo.LMS L WITH (NOLOCK) ON L.LMSID = LMD.LMSID
		AND L.OriginatingTierID = 16
	JOIN dbo.IssueLMS IL ON IL.LMSID = L.LMSID
GROUP BY MD.Metadata, MDT.MetadataType
ORDER BY MDT.MetadataType, MD.Metadata, COUNT(MD.MetadataID)
