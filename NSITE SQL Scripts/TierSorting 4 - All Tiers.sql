DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(T.TierID) AS VARCHAR(50)))
FROM JLLIS.dbo.Tier T

;
WITH HD (DisplayIndex,InstanceID,TierLineage,TierID,ParentTierID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY T.TierLabel) AS VARCHAR(10)), @nPadLength)),
		T.InstanceID,
		CAST(T.TierLabel AS VARCHAR(MAX)),
		T.TierID,
		T.ParentTierID,
		1
	FROM JLLIS.dbo.Tier T
	WHERE T.ParentTierID = 0
		--AND T.TierID = 25

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY T.TierLabel) AS VARCHAR(10)), @nPadLength)),
		T.InstanceID,
		CAST(HD.TierLineage  + ' > ' + T.TierLabel AS VARCHAR(MAX)),
		T.TierID,
		T.ParentTierID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.Tier T
		JOIN HD ON HD.TierID = T.ParentTierID
	)

SELECT
	HD1.DisplayIndex,
	HD1.InstanceID,
	HD1.NodeLevel,
	HD1.ParentTierID,
	HD1.TierLineage,
	HD1.TierID,
	T1.TierLabel AS InstanceLabel,
	T1.TierName AS InstanceName,
	T2.TierLabel,
	T2.TierName,
	T2.IsActive,
	T2.IsForNavigation,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentTierID = HD1.TierID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = HD1.InstanceID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = HD1.TierID
ORDER BY HD1.DisplayIndex

/*
USE JLLIS
GO

INSERT INTO dbo.JLLISUserSecLev
	(JLLISUserID,TierID,SecLev)
VALUES
	(1, 5, 6000),
	(1, 25, 2500),
	(1, 550, 4000),
	(1, 569, 6000),
	(1, 560, 3000)
GO
*/