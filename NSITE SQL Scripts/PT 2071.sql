IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.Coalition C WHERE C.CoalitionAbbreviation = 'ROKUS')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.Coalition
		(Coalition,CoalitionAbbreviation)
	VALUES
		('ROKUS: Republic of Korea / United States','ROKUS')
		
	END
--ENDIF