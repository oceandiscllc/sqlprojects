USE USAF
GO

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'BackgroundClassificationDataID'
FROM dbo.LMS L
WHERE L.BackgroundClassificationDataID = 0
	AND 
		(
		L.BackgroundClass IS NULL
			OR LEN(RTRIM(L.BackgroundClass)) = 0
			OR L.BackgroundClass = 'Unclassified'
		)
		AND
		(
		L.DiscussionCaveat IS NULL
			OR LEN(RTRIM(L.DiscussionCaveat)) = 0
		)
		AND 
		(
		L.BackgroundRelTo IS NULL
			OR LEN(RTRIM(L.BackgroundRelTo)) = 0
		)
	
UPDATE L
SET L.BackgroundClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'BackgroundClassificationDataID'
		
COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'EventDescriptionClassificationDataID'
FROM dbo.LMS L
WHERE L.EventDescriptionClassificationDataID = 0
	AND 
		(
		L.EventDescriptionClass IS NULL
			OR LEN(RTRIM(L.EventDescriptionClass)) = 0
			OR L.EventDescriptionClass = 'Unclassified'
		)
		AND
		(
		L.EventCaveat IS NULL
			OR LEN(RTRIM(L.EventCaveat)) = 0
		)
		AND 
		(
		L.EventDescriptionRelTo IS NULL
			OR LEN(RTRIM(L.EventDescriptionRelTo)) = 0
		)

UPDATE L
SET L.EventDescriptionClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'EventDescriptionClassificationDataID'

COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'ImplicationsClassificationDataID'
FROM dbo.LMS L
WHERE L.ImplicationsClassificationDataID = 0
	AND 
		(
		L.ImplicationsClass IS NULL
			OR LEN(RTRIM(L.ImplicationsClass)) = 0
			OR L.ImplicationsClass = 'Unclassified'
		)
		AND
		(
		L.ImplicationCaveat IS NULL
			OR LEN(RTRIM(L.ImplicationCaveat)) = 0
		)
		AND 
		(
		L.ImplicationsRelTo IS NULL
			OR LEN(RTRIM(L.ImplicationsRelTo)) = 0
		)

UPDATE L
SET L.ImplicationsClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'ImplicationsClassificationDataID'
		
COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'LMSClassificationDataID'
FROM dbo.LMS L
WHERE L.LMSClassificationDataID = 0
	AND 
		(
		L.Classification IS NULL
			OR LEN(RTRIM(L.Classification)) = 0
			OR L.Classification = 'Unclassified'
		)
		AND
		(
		L.OverallCaveat IS NULL
			OR LEN(RTRIM(L.OverallCaveat)) = 0
		)
		AND 
		(
		L.ReleasableTo IS NULL
			OR LEN(RTRIM(L.ReleasableTo)) = 0
		)
	
UPDATE L
SET L.LMSClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'LMSClassificationDataID'
		
COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'ObservationsClassificationDataID'
FROM dbo.LMS L
WHERE L.ObservationsClassificationDataID = 0
	AND 
		(
		L.ObservationsClass IS NULL
			OR LEN(RTRIM(L.ObservationsClass)) = 0
			OR L.ObservationsClass = 'Unclassified'
		)
		AND
		(
		L.ObservationCaveat IS NULL
			OR LEN(RTRIM(L.ObservationCaveat)) = 0
		)
		AND 
		(
		L.ObservationsRelTo IS NULL
			OR LEN(RTRIM(L.ObservationsRelTo)) = 0
		)
	
UPDATE L
SET L.ObservationsClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'ObservationsClassificationDataID'
		
COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'RecommendationsClassificationDataID'
FROM dbo.LMS L
WHERE L.RecommendationsClassificationDataID = 0
	AND 
		(
		L.RecommendationsClass IS NULL
			OR LEN(RTRIM(L.RecommendationsClass)) = 0
			OR L.RecommendationsClass = 'Unclassified'
		)
		AND
		(
		L.RecommendationCaveat IS NULL
			OR LEN(RTRIM(L.RecommendationCaveat)) = 0
		)
		AND 
		(
		L.RecommendationsRelTo IS NULL
			OR LEN(RTRIM(L.RecommendationsRelTo)) = 0
		)
	
UPDATE L
SET L.RecommendationsClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'RecommendationsClassificationDataID'
		
COMMIT TRANSACTION

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	L.LMSID,
	'SummaryClassificationDataID'
FROM dbo.LMS L
WHERE L.SummaryClassificationDataID = 0
	AND 
		(
		L.SummaryClass IS NULL
			OR LEN(RTRIM(L.SummaryClass)) = 0
			OR L.SummaryClass = 'Unclassified'
		)
		AND
		(
		L.CommentCaveat IS NULL
			OR LEN(RTRIM(L.CommentCaveat)) = 0
		)
		AND 
		(
		L.SummaryRelTo IS NULL
			OR LEN(RTRIM(L.SummaryRelTo)) = 0
		)
	
UPDATE L
SET L.SummaryClassificationDataID = CD.ClassificationDataID
FROM dbo.LMS L
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = L.LMSID
		AND CD.TempFieldName = 'SummaryClassificationDataID'

COMMIT TRANSACTION