USE JLLIS
GO

UPDATE dbo.JLLISUser
SET
	AccessID = NULL,
	Account = NULL,
	Address = CAST(JLLISUserID AS VARCHAR(10)) + ' Main Street',
	Admin = NULL,
	AllowedCompanies = NULL,
	Anthrax = NULL,
	AutoResponderBCC = NULL,
	BloodType = NULL,
	BulkMail = NULL,
	CellPhone = '(101) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	City = FirstName + 'ville',
	Company = NULL,
	Confirmation = NULL,
	Country = 'USA',
	DateReported = NULL,
	DayPhone = '(102) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	DefaultColor = NULL,
	DefaultCompany = NULL,
	DefaultFont = 'Arial, Helvetica, sans-serif',
	DefaultProject = NULL,
	DefaultSort = NULL,
	DefaultView = NULL,
	DeleteEvents = NULL,
	DeleteTasks = NULL,
	Department = NULL,
	DeploymentCity = FirstName + 'deploycity',
	DeploymentCountry = FirstName + 'deploycountry',
	DeploymentPlace = FirstName + 'deployplace',
	DeploymentProvince = FirstName + 'deployprovince',
	DeploymentRegion = FirstName + 'deploycounty',
	DOB = NULL,
	Email = 'dummy.test@nsitellc.com',
	EMail2 = 'dummy.test@nsitellc.com',
	EMail3 = 'dummy.test@nsitellc.com',
	Expertise = NULL,
	Fax = '(103) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	FitRepDue = NULL,
	FitRepRO = NULL,
	FitRepRS = NULL,
	GasMaskSize = NULL,
	IridiumPhone = '(104) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	MOS = NULL,
	MPassword = NULL,
	MultiCompany = NULL,
	NightPhone = '(105) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	Notes = NULL,
	Password = NULL,
	PasswordAnswer = NULL,
	PasswordQuestion = NULL,
	PayGrade = CASE WHEN RIGHT(CAST(JLLISUserID AS VARCHAR(10)), 1) = 1 THEN 'CIV' WHEN JLLISUserID % 2 = 0 THEN 'O' + '-' + RIGHT(CAST(JLLISUserID AS VARCHAR(10)), 1) ELSE 'E' + '-' + RIGHT(CAST(JLLISUserID AS VARCHAR(10)), 1) END,
	PistolSerial = NULL,
	ShipEMail = 'dummy.test@nsitellc.com',
	ShowHelp = NULL,
	SIPREMail = 'dummy.test@nsitellc.com',
	State = 'FL',
	TaskTrackerToken = NULL,
	TempInt = NULL,
	TollFreePhone = '(106) 555-' + RIGHT('0000' + CAST(JLLISUserID AS VARCHAR(10)), 4),
	Zip = RIGHT('000000' + CAST(JLLISUserID AS VARCHAR(10)), 5) + '-0000'
WHERE EMail NOT LIKE '%nsite%'
	AND JLLISUserID > 0
	AND LastName + FirstName NOT IN ('WardJane', 'WillardJeff')
GO
	
UPDATE SA
SET SecurityAnswer = JU.LastName
FROM PasswordSecurity.SecurityAnswer SA
	JOIN dbo.JLLISUser JU ON JU.JLLISUserID = SA.JLLISUserID
		AND JU.EMail = 'dummy.test@nsitellc.com'
GO

DECLARE @cColumnName VARCHAR(50)
DECLARE @cCRLF varchar(2)
DECLARE @cSchemaName VARCHAR(50)
DECLARE @cSQL VARCHAR(MAX)
DECLARE @cTab1 varchar(1)
DECLARE @cTab2 varchar(2)
DECLARE @cTab3 varchar(3)
DECLARE @cTab4 varchar(4)
DECLARE @cTab5 varchar(5)
DECLARE @cTableName VARCHAR(50)

SET @cCRLF = CHAR(13) + CHAR(10)
SET @cTab1 = CHAR(9)
SET @cTab2 = REPLICATE(CHAR(9), 2)
SET @cTab3 = REPLICATE(CHAR(9), 3)
SET @cTab4 = REPLICATE(CHAR(9), 4)
SET @cTab5 = REPLICATE(CHAR(9), 5)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		S1.Name AS SchemaName,
		O1.Name AS TableName,
		C1.Name AS ColumnName
	FROM sys.objects O1
		JOIN sys.schemas S1 ON S1.schema_ID = O1.schema_ID
		JOIN sys.columns C1 ON O1.object_id = C1.object_id
		JOIN sys.types T1 ON C1.user_type_id = T1.user_type_id
			AND O1.type = 'U'
			AND S1.Name NOT IN ('Deprecated','Staging')
			AND O1.Name NOT IN ('Caveat','releasableto')
			AND O1.Name NOT LIKE '%ClassificationData%'
			AND 
				(
				C1.Name LIKE '%Caveat%'
					OR C1.Name LIKE '%Rele%'
					OR C1.Name LIKE '%Relt%'
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM sys.objects O2
					JOIN sys.schemas S2 ON S2.schema_ID = O2.schema_ID
					JOIN sys.columns C2 ON O2.object_id = C2.object_id
					JOIN sys.types T2 ON C2.user_type_id = T2.user_type_id
						AND O2.type = 'U'
						AND C2.Name LIKE '%ClassificationDataID'
						AND S2.Name = S1.Name
						AND O2.Name = O1.Name
				)
				
	UNION

	SELECT
		S3.Name AS SchemaName,
		O3.Name AS TableName,
		C3.Name AS ColumnName
	FROM sys.objects O3
		JOIN sys.schemas S3 ON S3.schema_ID = O3.schema_ID
		JOIN sys.columns C3 ON O3.object_id = C3.object_id
		JOIN sys.types T3 ON C3.user_type_id = T3.user_type_id
			AND O3.type = 'U'
			AND S3.Name <> 'Deprecated'
			AND O3.Name NOT IN ('Caveat','releasableto')
			AND O3.Name NOT LIKE '%ClassificationData%'
			AND C3.Name LIKE '%ClassificationDataID'

	ORDER BY 
		SchemaName,
		TableName,
		ColumnName

OPEN oCursor
FETCH oCursor INTO @cSchemaName,@cTableName,@cColumnName
WHILE @@fetch_status = 0
	BEGIN
	
	IF @cColumnName LIKE '%ClassificationDataID'
		BEGIN

		SET @cSQL = 'DELETE T' + @cCRLF
		SET @cSQL += 'FROM ' + @cSchemaName + '.' + @cTableName + ' T' + @cCRLF
		SET @cSQL += @cTab1 + 'JOIN JLLIS.dbo.ClassificationData CD ON CD.ClassificationDataID = T.' + @cColumnName + @cCRLF
		SET @cSQL += @cTab2 + 'AND' + @cCRLF
		SET @cSQL += @cTab3 + '(' + @cCRLF
		SET @cSQL += @cTab3 + 'CD.CaveatID <> 0' + @cCRLF
		SET @cSQL += @cTab4 + 'OR EXISTS' + @cCRLF
		SET @cSQL += @cTab5 + '(' + @cCRLF
		SET @cSQL += @cTab5 + 'SELECT 1' + @cCRLF
		SET @cSQL += @cTab5 + 'FROM JLLIS.dbo.ClassificationDataCoalition CDC1' + @cCRLF
		SET @cSQL += @cTab5 + 'WHERE CDC1.ClassificationDataID = CD.ClassificationDataID' + @cCRLF
		SET @cSQL += @cCRLF
		SET @cSQL += @cTab5 + 'UNION' + @cCRLF
		SET @cSQL += @cCRLF
		SET @cSQL += @cTab5 + 'SELECT 1' + @cCRLF
		SET @cSQL += @cTab5 + 'FROM JLLIS.dbo.ClassificationDataCountry CDC2' + @cCRLF
		SET @cSQL += @cTab5 + 'WHERE CDC2.ClassificationDataID = CD.ClassificationDataID' + @cCRLF
		SET @cSQL += @cTab5 + ')' + @cCRLF
		SET @cSQL += @cTab3 + ')' + @cCRLF
	
		print @cSQL
		EXEC (@cSQL)
		print @cCRLF + '-----' + @cCRLF + @cCRLF

		END
	ELSE
		BEGIN

		SET @cSQL = 'DELETE T' + @cCRLF
		SET @cSQL += 'FROM ' + @cSchemaName + '.' + @cTableName + ' T' + @cCRLF
		SET @cSQL += 'WHERE T.' + @cColumnName + ' IS NOT NULL' + @cCRLF
		SET @cSQL += @cTab1 + 'OR LEN(RTRIM(T.' + @cColumnName + ')) > 0' + @cCRLF
	
		print @cSQL
		EXEC (@cSQL)
		print @cCRLF + '-----' + @cCRLF + @cCRLF
		
		END
	--ENDIF
	
	FETCH oCursor INTO @cSchemaName,@cTableName,@cColumnName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	