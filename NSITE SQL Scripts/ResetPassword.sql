--Begin procedure Utility.ResetPassword
EXEC Utility.DropObject 'Utility.ResetPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.02.19
-- Description:	A stored procedure to reset a password to a known value
-- Usage:				Pass any ONE of the following:  
--							If you know the JLLIS User ID of the target record, pass that as @nJLLISUserID
--							If you know the Tier User ID of the target record, pass that as @nTierUserID
--							If you know the JLLIS User Name on the target record, pass that as @cJLLISUserName
-- Note:				This script presumes the existance of a record with a JLLIS User Name of Reset.Password in the JLLISUser Table
-- ===========================================================================================================================
CREATE PROCEDURE Utility.ResetPassword
	@nJLLISUserID INT = 0,
	@nTierUserID INT = 0,
	@cJLLISUserName VARCHAR(250) = ''
	
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @nJLLISUserID = 0
		BEGIN
		
		IF @nTierUserID > 0
			SET @nJLLISUserID = dbo.GetJLLISUserIDFromTierUserID(@nTierUserID)
		ELSE IF LEN(RTRIM(@cJLLISUserName)) > 0 AND @cJLLISUserName IS NOT NULL
			BEGIN
			
			SELECT @nJLLISUserID = JU.JLLISUserID
			FROM dbo.JLLISUser JU
			WHERE JU.JLLISUserName = @cJLLISUserName
			
			END
		--ENDIF
		
		END
	--ENDIF

	IF @nJLLISUserID = 0
		SELECT 'No JLLIS User ID Found For This User'
	ELSE
		BEGIN		
		
		INSERT INTO PasswordSecurity.Password
			(JLLISUserID, PasswordHash, PasswordSalt, CreationDate, IsActive)
		SELECT TOP 1
			@nJLLISUserID,
			P.PasswordHash, 
			P.PasswordSalt, 
			getDate(), 
			1
		FROM dbo.JLLISUser JU
			JOIN PasswordSecurity.Password P ON P.JLLISUserID = JU.JLLISUserID
				AND JU.JLLISUserName = 'Reset.Password'
				AND P.IsActive = 1

		SELECT 'Password has been reset'

		END
	--ENDIF
END	
GO
--End procedure Utility.ResetPassword
