USE JSCC
GO

/****** Object:  UserDefinedFunction [dbo].[GetLMSRecordsByUserID]    Script Date: 08/13/2013 16:14:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2011.12.13
-- Description:	A function to manage the buisness rules regarding LMS table record visibility based on TierUserID and InstanceID
--
-- Author:			Todd Pires
-- Modify Date:	2012.03.05
-- Description:	Added CanHaveComment support
--
-- Author:			Todd Pires
-- Modify Date:	2012.08.29
-- Description:	Added the foreign national exclusion
--
-- Author:			Todd Pires
-- Modify Date:	2012.09.21
-- Description:	Implemented the TierUserInstanceID functionality
--
-- Author:			Todd Pires
-- Modify Date:	2013.01.25
-- Description:	Implemented the CanHaveCopy functionality
--
-- Author:			Todd Pires
-- Modify Date:	2013.02.11
-- Description:	Implemented JTIMS edit/deletes for admin/superadmins
--
-- Author:			Todd Pires
-- Modify Date:	2013.02.21
-- Description:	Applied SoT logic to shared items, removed all references to InstanceType
--
-- Author:			Todd Pires
-- Modify Date:	2013.02.26
-- Description:	Implemented UserGroup 2.0 functionality / Renamed the function from GetLMSRecordsByUserIDAndInstanceID to GetLMSRecordsByUserID
--
-- Author:			Todd Pires
-- Modify Date:	2013.06.26
-- Description:	Implemented the IsRestrictedClassification functionality
-- ============================================================================================================================================

ALTER FUNCTION [dbo].[GetLMSRecordsByUserID]
(
@nUserID INT
)

RETURNS @tReturn table 
	(
	LMSID INT PRIMARY KEY NOT NULL,
	CanHaveComment BIT NOT NULL DEFAULT 0,
	CanHaveCopy BIT NOT NULL DEFAULT 0,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0,
	CanHaveHistory BIT NOT NULL DEFAULT 0,
	CanHaveTransfer BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @cISOCountryCode CHAR(3)
	DECLARE @nDefaultTierID INT
	DECLARE @nSecLev INT

	DECLARE @tRestrictedBinderObservations TABLE (LMSID INT PRIMARY KEY NOT NULL) -- This table stores the restricted observations associated with restricted binders associated with the user
	DECLARE @tUserGroupObservations TABLE (LMSID INT PRIMARY KEY NOT NULL) -- This table stores the observations associated with user groups associated with the user

	SELECT
		@cISOCountryCode = C.ISOCode3,
		@nDefaultTierID = TU.DefaultTierID,
		@nSecLev = TU.SecLev
	FROM JLLIS.dbo.TierUser TU
		JOIN JLLIS.dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = JU.CountryID
			AND TU.TierID = JLLIS.dbo.GetTierUserInstanceID()
			AND TU.TierUserID = @nUserID

	IF @nSecLev = 7000
		BEGIN

		INSERT INTO @tReturn
			(LMSID,CanHaveDelete,CanHaveEdit,CanHaveHistory,CanHaveTransfer)
		SELECT
			L.LMSID,
			1,
			1,
			1,
			1
		FROM dbo.LMS L
		
		END
	ELSE
		BEGIN

		--Get the restricted observations associated with restricted binders associated with the user
		INSERT INTO @tRestrictedBinderObservations
			(L.LMSID)
		SELECT DISTINCT BI.ItemID
		FROM dbo.BinderItem BI
			JOIN dbo.LMS L ON L.LMSID = BI.ItemID
				AND L.Status = 'Restricted'
			JOIN dbo.Binder B ON B.BinderID = BI.BinderID
				AND B.Type = 'Restricted'
				AND BI.Type = 'LMS'
				AND EXISTS
					(
					SELECT 1
					FROM JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'Binder') ME
					WHERE ME.EntityID = BI.BinderID
					)

		--Get the records associated the usergroups associated with the user
		INSERT INTO @tUserGroupObservations
			(L.LMSID)
		SELECT
			L.LMSID
		FROM dbo.LMS L
			JOIN JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'Observation') ME ON ME.EntityID = L.LMSID

		IF @nSecLev = 6000
			BEGIN

			INSERT INTO @tReturn
				(LMSID,CanHaveDelete,CanHaveEdit,CanHaveHistory,CanHaveTransfer)
			SELECT
				L.LMSID,
				1,
				1,
				1,
				1
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM JLLIS.dbo.GetDescendantTiersByTierID(@nDefaultTierID) DT
				WHERE DT.TierID = L.OriginatingTierID

				UNION
				
				SELECT 1
				FROM @tUserGroupObservations UGO
				WHERE UGO.LMSID = L.LMSID
				)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)

			INSERT INTO @tReturn
				(LMSID)
			SELECT
				L.LMSID
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.TierEntities TE
					JOIN JLLIS.dbo.GetDescendantTiersByTierID(@nDefaultTierID) DT ON DT.TierID = TE.TierID
						AND TE.EntityTypeCode = 'LESSON'
						AND TE.EntityID = L.LMSID
				)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					
					UNION
					
					SELECT 1
					FROM JLLIS.dbo.EntityMember EM
					WHERE EM.EntityTypeCode = 'Observation'
						AND EM.MemberTypeCode = 'UserGroup'
						AND EM.EntityID = L.LMSID
					)

			END
		--ENDIF

		IF @nSecLev = 4000
			BEGIN

			INSERT INTO @tReturn
				(LMSID,CanHaveDelete,CanHaveEdit,CanHaveHistory,CanHaveTransfer)
			SELECT
				L.LMSID,
				1,
				1,
				1,
				1
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM JLLIS.dbo.GetDescendantTiersByTierID(@nDefaultTierID) DT
				WHERE DT.TierID = L.OriginatingTierID

				UNION
				
				SELECT 1
				FROM @tUserGroupObservations UGO
				WHERE UGO.LMSID = L.LMSID
				)
				AND 
					(
					L.IsPublished = 1
						OR L.OriginatingTierID = @nDefaultTierID
					)
				AND 
					(
					L.Status <> 'Restricted'
						OR L.CreatedBy = @nUserID
						OR EXISTS
							(
							SELECT 1
							FROM @tRestrictedBinderObservations RBO
							WHERE L.LMSID = RBO.LMSID
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					
					UNION
					
					SELECT 1
					FROM JLLIS.dbo.EntityMember EM
					WHERE EM.EntityTypeCode = 'Observation'
						AND EM.MemberTypeCode = 'UserGroup'
						AND EM.EntityID = L.LMSID
					)

			INSERT INTO @tReturn
				(LMSID)
			SELECT
				L.LMSID
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.TierEntities TE
					JOIN JLLIS.dbo.GetDescendantTiersByTierID(@nDefaultTierID) DT ON DT.TierID = TE.TierID
						AND TE.EntityTypeCode = 'LESSON'
						AND TE.EntityID = L.LMSID
				)
				AND 
					(
					L.Status <> 'Restricted'
						OR L.CreatedBy = @nUserID
						OR EXISTS
							(
							SELECT 1
							FROM JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'Observation') ME
							WHERE ME.EntityID = L.LMSID
							
							UNION

							SELECT 1
							FROM @tRestrictedBinderObservations RBO
							WHERE RBO.LMSID = L.LMSID
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					
					UNION
					
					SELECT 1
					FROM JLLIS.dbo.EntityMember EM
					WHERE EM.EntityTypeCode = 'Observation'
						AND EM.MemberTypeCode = 'UserGroup'
						AND EM.EntityID = L.LMSID
					)

			END
		--ENDIF

		IF @nSecLev = 3000
			BEGIN
				
			INSERT INTO @tReturn
				(LMSID,CanHaveDelete,CanHaveEdit)
			SELECT
				L.LMSID,

				CASE
					WHEN L.Status IN ('Draft','Restricted')
					THEN 1
					ELSE 0
				END,

				CASE
					WHEN L.Status IN ('Draft','Restricted')
					THEN 1
					ELSE 0
				END

			FROM dbo.LMS L
			WHERE L.OriginatingTierID = @nDefaultTierID
				AND 
					(
					L.Status IN ('Active','Closed','Validated')
						OR
							(
							L.Status IN ('Draft','Pending')
								AND L.CreatedBy = @nUserID
							)
						OR
							(
							L.Status = 'Restricted'
								AND
									(
									L.CreatedBy = @nUserID
										OR EXISTS
											(
											SELECT 1
											FROM JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'Observation') ME
											WHERE ME.EntityID = L.LMSID

											UNION

											SELECT 1
											FROM @tRestrictedBinderObservations RBO
											WHERE RBO.LMSID = L.LMSID
											)
									)
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)

			UNION
				
			SELECT
				L.LMSID,

				CASE
					WHEN L.Status IN ('Draft','Restricted')
					THEN 1
					ELSE 0
				END,

				CASE
					WHEN L.Status IN ('Draft','Restricted')
					THEN 1
					ELSE 0
				END

			FROM dbo.LMS L
				JOIN @tUserGroupObservations UGO ON UGO.LMSID = L.LMSID
					AND NOT EXISTS
						(
						SELECT 1
						FROM @tReturn R
						WHERE R.LMSID = L.LMSID
						)
				
			INSERT INTO @tReturn
				(LMSID)
			SELECT
				L.LMSID
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.TierEntities TE
				WHERE TE.EntityTypeCode = 'LESSON'
						AND TE.EntityID = L.LMSID
						AND TE.TierID = @nDefaultTierID
				)						
				AND 
					(
					L.Status IN ('Active','Closed','Validated')
						OR
							(
							L.Status IN ('Draft','Pending')
								AND L.CreatedBy = @nUserID
							)
						OR
							(
							L.Status = 'Restricted'
								AND
									(
									L.CreatedBy = @nUserID
										OR EXISTS
											(
											SELECT 1
											FROM @tRestrictedBinderObservations RBO
											WHERE RBO.LMSID = L.LMSID

											UNION

											SELECT 1
											FROM @tUserGroupObservations UGO
											WHERE UGO.LMSID = L.LMSID
											)
									)
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)
				
			END
		--ENDIF

		IF @nSecLev = 2500
			BEGIN
				
			INSERT INTO @tReturn
				(LMSID,CanHaveDelete,CanHaveEdit,CanHaveHistory,CanHaveTransfer)
			SELECT
				L.LMSID,
				1,
				1,
				1,
				1
			FROM dbo.LMS L
			WHERE L.OriginatingTierID = @nDefaultTierID
				AND 
					(
					L.Status <> 'Restricted'
						OR 
							(
							L.CreatedBy = @nUserID
								OR EXISTS
									(
									SELECT 1
									FROM @tRestrictedBinderObservations RBO
									WHERE RBO.LMSID = L.LMSID

									UNION

									SELECT 1
									FROM @tUserGroupObservations UGO
									WHERE UGO.LMSID = L.LMSID
									)
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)

			INSERT INTO @tReturn
				(LMSID)
			SELECT
				L.LMSID
			FROM dbo.LMS L
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.TierEntities TE
				WHERE TE.EntityTypeCode = 'LESSON'
						AND TE.EntityID = L.LMSID
						AND TE.TierID = @nDefaultTierID
				)						
				AND 
					(
					L.Status <> 'Restricted'
						OR 
							(
							L.CreatedBy = @nUserID
								OR EXISTS
									(
									SELECT 1
									FROM @tRestrictedBinderObservations RBO
									WHERE RBO.LMSID = L.LMSID

									UNION

									SELECT 1
									FROM @tUserGroupObservations UGO
									WHERE UGO.LMSID = L.LMSID
									)
							)
					)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)
				
			END
		--ENDIF

		INSERT INTO @tReturn
			(LMSID,CanHaveDelete,CanHaveEdit,CanHaveTransfer)
		SELECT
			L.LMSID,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END

		FROM dbo.LMS L
		WHERE 
			(
			L.IsPublished = 1
				OR L.OriginatingTierID = @nDefaultTierID
			)
			AND 
				(
				L.Status IN ('Active','Closed','Validated')
					OR
						(
						L.Status IN ('Draft','Pending')
							AND L.CreatedBy = @nUserID
						)
					OR
						(
						L.Status = 'Restricted'
							AND
								(
								L.CreatedBy = @nUserID
									OR EXISTS
										(
										SELECT 1
										FROM @tUserGroupObservations UGO
										WHERE UGO.LMSID = L.LMSID
										)
								)
						)
					OR EXISTS
						(
						SELECT 1
						FROM @tRestrictedBinderObservations RBO
						WHERE RBO.LMSID = L.LMSID
						)
				)
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)

		INSERT INTO @tReturn
			(LMSID,CanHaveDelete,CanHaveEdit,CanHaveTransfer)
		SELECT
			L.LMSID,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN L.Status = 'Draft' AND L.CreatedBy = @nUserID
				THEN 1
				ELSE 0
			END

		FROM dbo.LMS L
			JOIN @tUserGroupObservations UGO ON UGO.LMSID = L.LMSID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tReturn R
					WHERE R.LMSID = L.LMSID
					)

		END
	--ENDIF

	UPDATE R
	SET R.CanHaveComment = R.CanHaveEdit
	FROM @tReturn R
		JOIN dbo.LMS L ON L.LMSID = R.LMSID
			AND L.ImportSource = 'JTIMS'
					
	UPDATE R
	SET
		R.CanHaveDelete = 0,
		R.CanHaveEdit = 0,
		R.CanHaveTransfer = 0
	FROM @tReturn R
		JOIN dbo.LMS L ON L.LMSID = R.LMSID
			AND L.ImportSource = 'JCISFA'
					
	UPDATE R
	SET
		R.CanHaveDelete = 0,
		R.CanHaveEdit = 0,
		R.CanHaveTransfer = 0
	FROM @tReturn R
		JOIN dbo.LMS L ON L.LMSID = R.LMSID
			AND L.ImportSource = 'JTIMS'
			AND	@nSecLev NOT IN (2500,4000,6000,7000)

	UPDATE R
	SET
		R.CanHaveCopy = 1
	FROM @tReturn R
		JOIN dbo.LMS L ON L.LMSID = R.LMSID
			AND 
				(
				L.Status IN ('Active','Closed','Validated')
					OR
						(
						L.CreatedBy = @nUserID
							AND L.Status = 'Draft'
						)
					OR
						(
						@nSecLev IN (2500,4000,6000,7000)
							AND 
								(
								L.Status <> 'Pending'
									OR R.CanHaveEdit = 1
								)
						)
				)

	IF @cISOCountryCode <> 'USA'
		BEGIN
		
		DELETE R1
		FROM @tReturn R1
		WHERE EXISTS
			(
			SELECT 1
			FROM @tReturn R2
				JOIN dbo.LMS L ON L.LMSID = R2.LMSID
					AND R2.LMSID = R1.LMSID
					AND 
						(
						JLLIS.dbo.IsRestrictedCaveat(L.LMSClassificationDataID) = 1
							OR JLLIS.dbo.IsRestrictedClassification(L.LMSClassificationDataID) = 1
							OR LEN(RTRIM(JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID))) > 0
						)
			)
		
		END
	--ENDIF
	
	RETURN

END

GO
