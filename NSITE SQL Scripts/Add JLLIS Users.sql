USE JLLIS
GO

DECLARE @nTierUserID INT = (SELECT MAX(TU.TierUserID) FROM JLLIS.dbo.TierUser TU)
DECLARE @tPerson TABLE (FirstName VARCHAR(50), LastName VARCHAR(50), Email VARCHAR(320))
DECLARE @tOutput TABLE (ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, JLLISUserID INT)

INSERT INTO @tPerson
	(firstname, lastname, email)
VALUES
	('Steven','Hornbeak','steven.a.hornbeak.ctr@mail.mil'),
	('Lynn','Vince','lynn.m.vince.ctr@mail.mil'),
	('Brian','Turner','brian.j.turner40.ctr@mail.mil')

INSERT INTO JLLIS.dbo.JLLISUser
	(jllisusername, originatingtierid, firstname, lastname, email, paygrade, title, pkiforce, CountryID, IsWeeklyRollupSubscriber)
OUTPUT INSERTED.JLLISUserID INTO @tOutput
SELECT
	P.FirstName + '.' + P.LastName,
	JU.originatingtierid,
	P.FirstName,
	P.LastName,
	P.EMail,
	JU.paygrade,
	JU.title,
	JU.pkiforce,
	JU.CountryID,
	JU.IsWeeklyRollupSubscriber
FROM JLLIS.dbo.JLLISUser JU, @tPerson P
WHERE JU.JLLISUserID = 3399

INSERT INTO JLLIS.dbo.TierUser
	(tieruserid, jllisuserid, tierid, status, defaulttierid)
SELECT
	@nTierUserID + O.ID,
	O.JLLISUserID,
	8,
	'Active',
	5
FROM @tOutput O

INSERT INTO JLLIS.passwordsecurity.Password
	(JLLISUserID, PasswordHash, PasswordSalt, CreationDate, IsActive)
SELECT
	O.JLLISUserID,
	P.PasswordHash,
	P.PasswordSalt,
	P.CreationDate,
	P.IsActive
FROM @tOutput O, JLLIS.passwordsecurity.Password P
WHERE P.JLLISUserID = 3399

INSERT INTO JLLIS.dbo.JLLISUserSecLev
	(JLLISUserID, TierID, SecLev, IsActive)
SELECT
	JU.JLLISUserID,
	JU.OriginatingTierID,
	7000,
	1
FROM JLLIS.dbo.JLLISUser JU
	JOIN @tOutput O ON O.JLLISUserID = JU.JLLISUserID

SELECT TOP 6 * FROM JLLIS.dbo.JLLISUser JU ORDER BY JU.JLLISUserID DESC
SELECT TOP 6 * FROM JLLIS.dbo.TierUser TU WHERE TU.TierID = 8 ORDER BY TU.JLLISUserID DESC
SELECT TOP 6 * FROM JLLIS.passwordsecurity.Password P ORDER BY P.JLLISUserID DESC
SELECT TOP 6 * FROM JLLIS.dbo.JLLISUserSecLev P ORDER BY P.JLLISUserID DESC

