--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'LessonIssue')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('LessonIssue','Lesson Issues','Dropdown','LessonIssue','LessonIssueID','LessonIssue',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'LessonIssue'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LessonIssueID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: LessonIssue
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.LessonIssue'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.LessonIssue
		(
		LessonIssueID int IDENTITY(0,1) NOT NULL,
		LessonIssue varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LessonIssue', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LessonIssue', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LessonIssueID'
EXEC Utility.SetIndexNonClustered 'IX_LessonIssue', @cTableName, 'LessonIssue ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: LessonIssue

IF NOT EXISTS (SELECT 1 FROM Dropdown.LessonIssue LI WITH (NOLOCK) WHERE LI.LessonIssueID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.LessonIssue ON
	INSERT INTO Dropdown.LessonIssue (LessonIssueID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.LessonIssue OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CI_Issues'
WHERE Category = 'CI_Issues_Old'
GO

INSERT INTO Dropdown.LessonIssue
	(LessonIssue)
SELECT DISTINCT 
	LTRIM(D.DisplayText)
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CI_Issues'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.LessonIssue LI WITH (NOLOCK)
		WHERE LI.LessonIssue = LTRIM(D.DisplayText)
		)
ORDER BY LTRIM(D.DisplayText)
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name='Issues')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.LessonIssue (LessonIssue) SELECT DISTINCT LTRIM(L.Issues) FROM dbo.LMS L WITH (NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM Dropdown.LessonIssue LI WITH (NOLOCK) WHERE LI.LessonIssue = LTRIM(L.Issues)) AND L.Issues IS NOT NULL ORDER BY LTRIM(L.Issues)'

	EXEC (@cSQL)
	
	SET @cSQL = 'UPDATE L SET L.LessonIssueID = LI.LessonIssueID FROM dbo.LMS L JOIN Dropdown.LessonIssue LI ON LI.LessonIssue = L.Issues AND L.LessonIssueID = 0'

	EXEC (@cSQL)

	END
--ENDIF
GO		

UPDATE Dropdown.LessonIssue
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND LessonIssueID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'LessonIssue'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	LI.LessonIssueID
FROM Dropdown.LessonIssue LI WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = LI.LessonIssue
		AND D.Category = 'CI_Issues'
		AND LI.LessonIssueID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = LI.LessonIssueID
			)
ORDER BY LI.LessonIssueID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.LessonIssue LI WITH (NOLOCK) ON LI.LessonIssueID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY LI.LessonIssue

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CI_Issues_Old'
WHERE Category = 'CI_Issues'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'LessonIssue'
GO