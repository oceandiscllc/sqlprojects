--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'EventLocation')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('EventLocation','Event Locations','Dropdown','EventLocation','EventLocationID','EventLocation',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'EventLocation'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: EventLocation
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.EventLocation'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.EventLocation
		(
		EventLocationID int IDENTITY(0,1) NOT NULL,
		EventLocation varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EventLocation', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EventLocation', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventLocationID'
EXEC Utility.SetIndexNonClustered 'IX_EventType', @cTableName, 'EventLocation ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.EventLocation T WITH (NOLOCK) WHERE T.EventLocationID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.EventLocation ON
	INSERT INTO Dropdown.EventLocation (EventLocationID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.EventLocation OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.EventLocation
	(EventLocation)
SELECT DISTINCT 
	LTRIM(OE.Location)
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE OE.Location IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.EventLocation EL WITH (NOLOCK)
		WHERE EL.EventLocation = LTRIM(OE.Location)
		)
ORDER BY LTRIM(OE.Location)
GO		
		
UPDATE Dropdown.EventLocation
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventLocationID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'EventLocation'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	EL.EventLocationID
FROM Dropdown.EventLocation EL WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.Location) = EL.EventLocation
		AND EL.EventLocationID > 0
		AND OE.Location IS NOT NULL
		AND LEN(LTRIM(OE.Location)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = EL.EventLocationID
			)
ORDER BY EL.EventLocationID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.EventLocation EL WITH (NOLOCK) ON EL.EventLocationID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY EL.EventLocation

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'EventLocation'
GO