--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Event')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Event','Events','Dropdown','Event','EventID','Event',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Event'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: Binder
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.Binder'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: Binder

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: PortVisit
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.PortVisit'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit

--Begin table: Event
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Event'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.Event
		(
		EventID int IDENTITY(0,1) NOT NULL,
		EventLocationID int,
		EventSponsorID int,
		EventTypeID int,
		Event varchar(250),
		StartDate datetime,
		EndDate datetime,
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetDefault @cTableName, 'Event', ''
EXEC Utility.SetDefault @cTableName, 'EventLocationID', 0
EXEC Utility.SetDefault @cTableName, 'EventSponsorID', 0
EXEC Utility.SetDefault @cTableName, 'EventTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Event', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'EventLocationID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EventSponsorID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EventTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventID'
EXEC Utility.SetIndexNonClustered 'IX_Event', @cTableName, 'Event ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.Event T WITH (NOLOCK) WHERE T.EventID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.Event ON
	INSERT INTO Dropdown.Event (EventID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.Event OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.Event
	(Event)
SELECT DISTINCT 
	LTRIM(OE.OpExName) AS OpExName
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE OE.OpExName IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.Event EL WITH (NOLOCK)
		WHERE EL.Event = LTRIM(OE.OpExName)
		)

UNION

SELECT DISTINCT
	LTRIM(B.EventName) AS OpExName
FROM dbo.Binder B WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Event E WITH (NOLOCK)
	WHERE E.Event = LTRIM(B.EventName)
	)
	AND B.EventName IS NOT NULL

UNION

SELECT DISTINCT
	LTRIM(L.EventOpEx) AS OpExName
FROM dbo.LMS L WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Event E WITH (NOLOCK)
	WHERE E.Event = LTRIM(L.EventOpEx)
	)
	AND L.EventOpEx IS NOT NULL

ORDER BY OpExName
GO		
		
UPDATE E
SET E.EventLocationID = EL.EventLocationID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventLocation EL ON EL.EventLocation = OE.Location
GO

UPDATE E
SET E.EventSponsorID = ES.EventSponsorID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventSponsor ES ON ES.EventSponsor = OE.Sponsor
GO

UPDATE E
SET E.EventTypeID = ET.EventTypeID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventType ET ON ET.EventType = OE.OpExType
GO

UPDATE E
SET 
	E.StartDate = OE.StartDate,
	E.EndDate = OE.EndDate
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
GO

UPDATE Dropdown.Event
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventID > 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'Event'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	E.EventID
FROM Dropdown.Event E WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.OpexName) = E.Event
		AND E.EventID > 0
		AND OE.OpexName IS NOT NULL
		AND LEN(LTRIM(OE.OpexName)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = E.EventID
			)
ORDER BY E.EventID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.Event E WITH (NOLOCK) ON E.EventID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY E.Event

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.EventID = E.EventID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.Event E ON E.Event = PV.EventOpex
			AND PV.EventID = 0

	END
--ENDIF
GO

UPDATE L
SET L.EventID = E.EventID 
FROM dbo.LMS L
	JOIN Dropdown.Event E ON E.Event = L.EventOpex
		AND L.EventID = 0
GO

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'EVENTS'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Event'
GO