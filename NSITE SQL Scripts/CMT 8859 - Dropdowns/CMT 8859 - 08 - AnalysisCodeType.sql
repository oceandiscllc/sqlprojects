--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'AnalysisCodeType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('AnalysisCodeType','Analysis Code Types','Dropdown','AnalysisCodeType','AnalysisCodeTypeID','AnalysisCodeType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'AnalysisCodeType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: AnalysisCodeType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.AnalysisCodeType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.AnalysisCodeType
		(
		AnalysisCodeTypeID int IDENTITY(0,1) NOT NULL,
		AnalysisCodeType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCodeType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AnalysisCodeTypeID'
EXEC Utility.SetIndexNonClustered 'IX_AnalysisCodeType', @cTableName, 'AnalysisCodeType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK) WHERE ACT.AnalysisCodeTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCodeType ON
	INSERT INTO Dropdown.AnalysisCodeType (AnalysisCodeTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCodeType OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.AnalysisCodeType
	(AnalysisCodeType)
SELECT DISTINCT 
	UPPER(LTRIM(AC.Type))
FROM dbo.AnalysisCodes AC WITH (NOLOCK)
WHERE AC.Type IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK)
		WHERE ACT.AnalysisCodeType = UPPER(LTRIM(AC.Type))
		)
ORDER BY UPPER(LTRIM(AC.Type))
GO		

UPDATE Dropdown.AnalysisCodeType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND AnalysisCodeTypeID <> 0
GO
--End table: AnalysisCodeType

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nInstanceID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'AnalysisCodeType'
	)

SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	@nInstanceID,
	@nDropdownMetadataID,
	ACT.AnalysisCodeTypeID
FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK)
WHERE ACT.AnalysisCodeTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = @nInstanceID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = ACT.AnalysisCodeTypeID
		)
ORDER BY ACT.AnalysisCodeTypeID

INSERT INTO #oTable
	(TierDropdownID)
SELECT
	TD.TierDropdownID
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN Dropdown.AnalysisCodeType ACT WITH (NOLOCK) ON ACT.AnalysisCodeTypeID = TD.DropdownEntityID
		AND TD.TierID = @nInstanceID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
ORDER BY ACT.AnalysisCodeType

UPDATE TD
SET TD.DisplayOrder = T.DisplayOrder
FROM Dropdown.TierDropdown TD
	JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
		AND TD.DisplayOrder = 0

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'AnalysisCodeType'
GO