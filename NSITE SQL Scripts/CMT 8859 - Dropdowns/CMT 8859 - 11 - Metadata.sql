--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Metadata')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Metadata','Metadata','Dropdown','Metadata','MetadataID','Metadata',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Metadata'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: Metadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Metadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.Metadata
		(
		MetadataID int IDENTITY(0,1) NOT NULL,
		MetadataTypeID int,
		Metadata varchar(250),
		Description varchar(max),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'Description', 'varchar(max)'

EXEC Utility.SetDefault @cTableName, 'Metadata', ''
EXEC Utility.SetDefault @cTableName, 'MetadataTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Metadata', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'MetadataTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'MetadataID'
EXEC Utility.SetIndexNonClustered 'IX_Metadata', @cTableName, 'Metadata ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WITH (NOLOCK) WHERE MD.MetadataID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.Metadata ON
	INSERT INTO Dropdown.Metadata (MetadataID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.Metadata OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.Metadata
	(Metadata)
SELECT DISTINCT
	LTRIM(MD1.Name)
FROM dbo.Metadata MD1 WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Metadata MD2
	WHERE MD2.Metadata = LTRIM(MD1.Name)
	)
ORDER BY LTRIM(MD1.Name)
GO

UPDATE MD2
SET MD2.Description = MD1.Description
FROM Dropdown.Metadata MD2
	JOIN dbo.Metadata MD1 ON LTRIM(MD1.Name) = MD2.Metadata
		AND MD1.Description IS NOT NULL
		AND MD2.Description IS NULL
GO
	
DECLARE @cMetadata varchar(250)
DECLARE @nMetadataTypeID int

DECLARE oCursor CURSOR FOR
	SELECT 
		MD.Name,
		MDT.MetadataTypeID
	FROM dbo.Metadata MD
		JOIN Dropdown.MetadataType MDT ON MDT.MetadataType = UPPER(MD.Type)
	ORDER BY MD.Name, MD.MDID

OPEN oCursor
FETCH oCursor INTO @cMetadata, @nMetadataTypeID
WHILE @@fetch_status = 0
	BEGIN
	
	IF EXISTS (SELECT 1 FROM Dropdown.Metadata MD WITH (NOLOCK) WHERE Metadata = @cMetadata AND MetadataTypeID = 0)
		BEGIN
		
		UPDATE Dropdown.Metadata
		SET MetadataTypeID = @nMetadataTypeID
		WHERE Metadata = @cMetadata
		
		END
	--ENDIF
	
	FETCH oCursor INTO @cMetadata, @nMetadataTypeID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
		
UPDATE Dropdown.Metadata
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND MetadataID <> 0
GO
--End table: Metadata

--Begin table: CDRMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDRMetadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.CDRMetadata
		(
		CDRMetadataID int IDENTITY(1,1) NOT NULL,
		CDRID int,
		MetadataID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'MetadataID', 0
EXEC Utility.SetDefault @cTableName, 'CDRID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CDRID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'CDRMetadataID'
EXEC Utility.SetIndexClustered 'IX_CDRMetadata', @cTableName, 'CDRID ASC,MetadataID ASC'
GO
--End table: CDRMetadata

--Begin table: LMSMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSMetadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LMSMetadata
		(
		LMSMetadataID int IDENTITY(1,1) NOT NULL,
		LMSID int,
		MetadataID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'MetadataID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSMetadataID'
EXEC Utility.SetIndexClustered 'IX_LMSMetadata', @cTableName, 'LMSID ASC,MetadataID ASC'
GO
--End table: LMSMetadata

EXEC Utility.AddColumn 'ReportToMetadata', 'MetadataID', 'int'
EXEC Utility.SetDefault 'ReportToMetadata', 'MetadataID', 0
EXEC Utility.SetColumnNotNull 'ReportToMetadata', 'MetadataID', 'int'
GO

UPDATE RTMD
SET MetadataID = MD2.MetadataID
FROM dbo.ReportToMetadata RTMD
	JOIN dbo.Metadata MD1 ON MD1.MDID = RTMD.MDID
	JOIN Dropdown.Metadata MD2 ON MD2.Metadata = MD1.Name
WHERE MD2.MetadataID = 0
GO

INSERT INTO dbo.CDRMetadata
	(CDRID,MetadataID)
SELECT DISTINCT
	RTMD.ReportID,
	RTMD.MetadataID
FROM dbo.ReportToMetadata RTMD
WHERE RTMD.ReportType = 'CDR'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.CDRMetadata CMD
		WHERE CMD.CDRID = RTMD.ReportID
			AND CMD.MetadataID = RTMD.MetadataID
		)
	AND EXISTS
		(
		SELECT 1
		FROM dbo.CDR C
		WHERE C.CDRID = RTMD.ReportID
		)
GO

INSERT INTO dbo.LMSMetadata
	(LMSID,MetadataID)
SELECT DISTINCT
	RTMD.ReportID,
	RTMD.MetadataID
FROM dbo.ReportToMetadata RTMD
WHERE RTMD.ReportType = 'LMS'
	AND RTMD.MetadataID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.LMSMetadata LMD
		WHERE LMD.LMSID = RTMD.ReportID
			AND LMD.MetadataID = RTMD.MetadataID
		)
	AND EXISTS
		(
		SELECT 1
		FROM dbo.LMS L
		WHERE L.LMSID = RTMD.ReportID
		)
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'Metadata'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	MD1.TierID,
	@nDropdownMetadataID,
	MD2.MetadataID
FROM Dropdown.Metadata MD2 WITH (NOLOCK)
	JOIN dbo.Metadata MD1 WITH (NOLOCK) ON LTRIM(MD1.Name) = MD2.Metadata
WHERE MD2.MetadataID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = MD1.TierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = MD2.MetadataID
		)
ORDER BY MD2.MetadataID

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.Metadata MD WITH (NOLOCK) ON MD.MetadataID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY MD.Metadata

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

--Begin table: UserSubscription
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.UserSubscription') AND O.type in (N'U')) 
	DROP TABLE dbo.UserSubscription

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.UserSubscription'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	DROP TABLE Dropdown.UserSubscription
	
CREATE TABLE Dropdown.UserSubscription
	(
	UserSubscriptionID int IDENTITY(1,1) NOT NULL,
	JLLISUserID int,
	DropdownEntityID int,
	DropdownMetadataCode varchar(50),
	Action varchar(50)
	) ON [PRIMARY]

EXEC Utility.SetDefault @cTableName, 'DropdownEntityID', 0
EXEC Utility.SetDefault @cTableName, 'JLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Action', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownEntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataCode', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'JLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'UserSubscriptionID'
EXEC Utility.SetIndexClustered 'IX_UserSubscription', @cTableName, 'JLLISUserID ASC,DropdownMetadataCode ASC,Action ASC,DropdownEntityID ASC'
GO
--End table: UserSubscription

INSERT INTO Dropdown.UserSubscription
	(JLLISUserID,DropdownEntityID,DropdownMetadataCode,Action)
SELECT 
	TU.JLLISUserID,
	MD2.MetadataID,
	'Metadata',
	'OnLessonSubmit'
FROM dbo.MetadataUser MDU WITH (NOLOCK)
	JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.TierUserID = MDU.UserID
		AND TU.TierID = 
			(
			SELECT 
				T.InstanceID
			FROM JLLIS.dbo.Tier T WITH (NOLOCK)
			WHERE T.TierID = 
				(
				SELECT TOP 1
					C.OriginatingTierID
				FROM dbo.CDR C WITH (NOLOCK)
				WHERE C.OriginatingTierID > 0
				)
			)
	JOIN dbo.Metadata MD1 WITH (NOLOCK) ON MD1.MDID = MDU.MDID
		AND MDU.UserID IS NOT NULL
		AND MDU.UserID > 0
	JOIN Dropdown.Metadata MD2 WITH (NOLOCK) ON MD2.Metadata = LTRIM(MD1.Name)
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.UserSubscription US
			WHERE US.Action = 'OnLessonSubmit'
				AND US.DropdownEntityID = MD2.MetadataID
				AND US.DropdownMetadataCode = 'Metadata'
				AND US.JLLISUserID = TU.JLLISUserID
			)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.Title = 'DROPDOWN SUBSCRIPTIONS')
	BEGIN
	
	UPDATE MI1
	SET MI1.Level = MI1.Level + 1
	FROM dbo.MenuItem MI1
	WHERE MI1.ParentID = 
		(
		SELECT MI2.ParentID
		FROM dbo.MenuItem MI2 WITH (NOLOCK)
		WHERE MI2.Title = 'DROPDOWNS'
		)
		AND MI1.Level >
			(
			SELECT MI3.Level
			FROM dbo.MenuItem MI3 WITH (NOLOCK)
			WHERE MI3.Title = 'DROPDOWNS'
			)
		
	INSERT INTO dbo.MenuItem 
		(MenuID,ParentID,Level,Title,SecLev,Link,type)
	SELECT
		MI.MenuID,
		MI.ParentID,
		MI.Level + 1,
		'DROPDOWN SUBSCRIPTIONS',
		6000,
		'admin/usersubscription/',
		'yes'
	FROM dbo.MenuItem MI WITH (NOLOCK)
	WHERE MI.Title = 'DROPDOWNS'

	END
--ENDIF

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'METADATA'
GO

DECLARE @nMenuID int
DECLARE @nParentMenuItemID int

SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Title = 'Default' AND M.Type = 'Logged In')
SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SITE MANAGEMENT' AND MI.MenuID = @nMenuID)
	
IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SETUP')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'SETUP',NULL,NULL,'index.cfm?disp=../admin/setup.cfm&menudisp=adminmenu.cfm','yes')
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Metadata'
GO