--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'TaskType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('TaskType','Task Types','Dropdown','TaskType','TaskTypeID','TaskType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'TaskType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: BB
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'TaskTypeID'
SET @cTableName = 'dbo.BB'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

EXEC Utility.AddColumn @cTableName, 'OriginatingTierID', 'int NULL'
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
GO

UPDATE BB
SET BB.OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
FROM dbo.BB BB
WHERE BB.OriginatingTierID = 0
--End table: BB

--Begin table: TaskType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TaskType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.TaskType
		(
		TaskTypeID int IDENTITY(0,1) NOT NULL,
		TaskType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'TaskType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TaskType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TaskTypeID'
EXEC Utility.SetIndexNonClustered 'IX_TaskType', @cTableName, 'TaskType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: TaskType

IF NOT EXISTS (SELECT 1 FROM Dropdown.TaskType TT WITH (NOLOCK) WHERE TT.TaskTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.TaskType ON
	INSERT INTO Dropdown.TaskType (TaskTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.TaskType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'BBType'
WHERE Category = 'TaskType_Old'
GO

INSERT INTO Dropdown.TaskType
	(TaskType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS TaskType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'BBType'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TaskType TT WITH (NOLOCK)
		WHERE TT.TaskType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(BB.Type) AS TaskType
FROM dbo.BB BB WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.TaskType TT WITH (NOLOCK)
	WHERE TT.TaskType = LTRIM(BB.Type)
	)
	AND BB.Type IS NOT NULL

ORDER BY TaskType
GO		

UPDATE BB
SET BB.TaskTypeID = TT.TaskTypeID 
FROM dbo.BB BB
	JOIN Dropdown.TaskType TT ON TT.TaskType = BB.Type
		AND BB.TaskTypeID = 0
GO
		
UPDATE Dropdown.TaskType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND TaskTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'TaskType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	TT.TaskTypeID
FROM Dropdown.TaskType TT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = TT.TaskType
		AND D.Category = 'BBType'
		AND TT.TaskTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = TT.TaskTypeID
			)
ORDER BY TT.TaskTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.TaskType TT WITH (NOLOCK) ON TT.TaskTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY TT.TaskType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'TaskType_Old'
WHERE Category = 'BBType'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'TaskType'
GO