--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlock' AND IsConfigurableByTier = 1)
	BEGIN
	
	UPDATE JLLIS.Dropdown.DropdownMetadata 
	SET
		DropdownMetadataCode = 'COPBlockType',
		DropdownMetadataLabel = 'COP Block Types',
		TableName = 'COPBlockType',
		PrimaryKeyFieldName = 'COPBlockTypeID',
		LabelFieldName = 'COPBlockType'
	WHERE DropdownMetadataCode = 'COPBlock' AND IsConfigurableByTier = 1

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlockType' AND IsConfigurableByTier = 1)
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('COPBlockType','COP Block Types','Dropdown','COPBlockType','COPBlockTypeID','COPBlockType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'COPBlockType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: SSiteItem
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'COPBlockTypeID'
SET @cTableName = 'dbo.SSiteItem'

IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) JOIN sys.objects O WITH (NOLOCK) ON O.object_id = C.object_id AND C.Name = 'COPBlockID' AND O.Name = @cTableName AND O.Type = 'U')
	ALTER TABLE dbo.SSiteItem DROP COPBlockID
--ENDIF

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: SSiteItem

--Begin table: COPBlockType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.COPBlockType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.COPBlockType
		(
		COPBlockTypeID int IDENTITY(0,1) NOT NULL,
		COPBlockCategoryID int,
		COPBlockType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'COPBlockType', ''
EXEC Utility.SetDefault @cTableName, 'COPBlockCategoryID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockCategoryID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'COPBlockTypeID'
EXEC Utility.SetIndexNonClustered 'IX_COPBlockType', @cTableName, 'COPBlockType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: COPBlockType

IF NOT EXISTS (SELECT 1 FROM Dropdown.COPBlockType CBT WITH (NOLOCK) WHERE CBT.COPBlockTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.COPBlockType ON
	INSERT INTO Dropdown.COPBlockType (COPBlockTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.COPBlockType OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.COPBlockType
	(COPBlockType)
SELECT DISTINCT 
	LTRIM(SI.Block) AS COPBlockType
FROM dbo.SSiteItem SI WITH (NOLOCK)
WHERE SI.Block IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.COPBlockType CBT WITH (NOLOCK)
		WHERE CBT.COPBlockType = LTRIM(SI.Block)
		)

ORDER BY LTRIM(SI.Block)
GO		

UPDATE CBT
SET CBT.COPBlockCategoryID = 

	CASE
		WHEN CBT.COPBlockType = 'BattleBoard'
		THEN (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'TaskTracker')
		WHEN CBT.COPBlockType = 'Library'
		THEN (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'Library')
		ELSE (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'General')
	END

FROM Dropdown.COPBlockType CBT
WHERE CBT.COPBlockTypeID > 0
GO

UPDATE SI
SET SI.COPBlockTypeID = CBT.COPBlockTypeID 
FROM dbo.SSiteItem SI
	JOIN Dropdown.COPBlockType CBT ON CBT.COPBlockType = LTRIM(SI.Block)
		AND SI.COPBlockTypeID = 0
GO
		
UPDATE Dropdown.COPBlockType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND COPBlockTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'COPBlockType'
	)

SET @nTierID = 	
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	@nTierID,
	@nDropdownMetadataID,
	CBT.COPBlockTypeID
FROM Dropdown.COPBlockType CBT WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.TierDropdown TD
	WHERE TD.TierID = @nTierID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
		AND TD.DropdownEntityID = CBT.COPBlockTypeID
	)
	AND CBT.COPBlockTypeID > 0
ORDER BY CBT.COPBlockTypeID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.COPBlockType CBT WITH (NOLOCK) ON CBT.COPBlockTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CBT.COPBlockType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'COPBlockType'
GO