--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Port')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Port','Ports','Dropdown','Port','PortID','Port',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Port'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: PortVisit
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @cColumnName varchar(250)
	DECLARE @cTableName varchar(250)

	SET @cColumnName = 'PortID'
	SET @cTableName = 'dbo.PortVisit'

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit

--Begin table: Port
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cTableName varchar(250)

	SET @cTableName = 'Dropdown.Port'

	IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
		BEGIN

		CREATE TABLE Dropdown.Port
			(
			PortID int IDENTITY(0,1) NOT NULL,
			Port varchar(250),
			CountryID int,
			OriginatingTierID int,
			UpdateJLLISUserID int,
			IsActive bit
			) ON [PRIMARY]
	
		END
	--ENDIF

	EXEC Utility.DropConstraintsAndIndexes @cTableName

	EXEC Utility.SetDefault @cTableName, 'CountryID', 0
	EXEC Utility.SetDefault @cTableName, 'IsActive', 1
	EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
	EXEC Utility.SetDefault @cTableName, 'Port', ''
	EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

	EXEC Utility.SetColumnNotNull @cTableName, 'CountryID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
	EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'Port', 'varchar(250)'
	EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
	EXEC Utility.SetPrimaryKeyClustered @cTableName, 'PortID'
	EXEC Utility.SetIndexNonClustered 'IX_Port', @cTableName, 'Port ASC'
	EXEC Utility.EnableAuditing @cTableName, 0

	END
--ENDIF
GO
--End table: Port
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM Dropdown.Port HT WITH (NOLOCK) WHERE HT.PortID = 0)
		BEGIN
		
		SET IDENTITY_INSERT Dropdown.Port ON
		INSERT INTO Dropdown.Port (PortID) VALUES (0)
		SET IDENTITY_INSERT Dropdown.Port OFF
	
		END
	--ENDIF

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	INSERT INTO Dropdown.Port
		(Port)
	SELECT DISTINCT 
		LTRIM(PV.Port)
	FROM dbo.PortVisit PV WITH (NOLOCK)
	WHERE PV.Port IS NOT NULL
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.Port HT WITH (NOLOCK)
			WHERE HT.Port = LTRIM(PV.Port)
			)
	ORDER BY LTRIM(PV.Port)

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE dbo.Port
	SET CountryCode = 'GB'
	WHERE CountryCode = 'UK'

	UPDATE dbo.PortCountry
	SET CountryCode = 'GB'
	WHERE CountryCode = 'UK'

	UPDATE dbo.Port
	SET CountryCode = 'IO'
	WHERE CountryCode = 'DG'

	UPDATE dbo.PortCountry
	SET CountryCode = 'IO'
	WHERE CountryCode = 'DG'

	UPDATE dbo.Port
	SET CountryCode = 'ME'
	WHERE CountryCode = 'YU'

	UPDATE dbo.PortCountry
	SET CountryCode = 'ME'
	WHERE CountryCode = 'YU'

	UPDATE dbo.Port
	SET CountryCode = 'MQ'
	WHERE CountryCode = 'FQ'

	UPDATE dbo.PortCountry
	SET CountryCode = 'MQ'
	WHERE CountryCode = 'FQ'

	UPDATE dbo.Port
	SET CountryCode = 'NA'
	WHERE CountryCode = 'NH'

	UPDATE dbo.PortCountry
	SET CountryCode = 'NA'
	WHERE CountryCode = 'NH'

	UPDATE dbo.Port
	SET CountryCode = 'NO'
	WHERE CountryCode = 'NOr'

	UPDATE dbo.PortCountry
	SET CountryCode = 'NO'
	WHERE CountryCode = 'NOr'

	UPDATE dbo.Port
	SET CountryCode = 'PT'
	WHERE CountryCode = 'ZS'

	UPDATE dbo.PortCountry
	SET CountryCode = 'PT'
	WHERE CountryCode = 'ZS'

	UPDATE dbo.Port
	SET CountryCode = 'TL'
	WHERE CountryCode = 'TP'

	UPDATE dbo.PortCountry
	SET CountryCode = 'TL'
	WHERE CountryCode = 'TP'

	UPDATE P1
	SET P1.CountryID = C.CountryID
	FROM Dropdown.Port P1
		JOIN dbo.Port P2 WITH (NOLOCK) ON P2.PortName = P1.Port
  	JOIN JLLIS.Dropdown.Country C ON C.IsoCode2 = P2.CountryCode

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.PortID = HT.PortID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.Port HT ON HT.Port = PV.Port
			AND PV.PortID = 0
		
	UPDATE Dropdown.Port
	SET OriginatingTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	WHERE OriginatingTierID = 0
		AND PortID <> 0

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @nDropdownMetadataID int
	DECLARE @nTierID int
	
	SET @nDropdownMetadataID = 
		(
		SELECT DMD.DropdownMetadataID
		FROM JLLIS.Dropdown.DropdownMetadata DMD
		WHERE DMD.DropdownMetadataCode = 'Port'
		)
	
	SET @nTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	
	INSERT INTO Dropdown.TierDropdown
		(TierID, DropdownMetadataID, DropdownEntityID)
	SELECT DISTINCT
		@nTierID,
		@nDropdownMetadataID,
		HT.PortID
	FROM Dropdown.Port HT WITH (NOLOCK)
		JOIN dbo.PortVisit PV WITH (NOLOCK) ON LTRIM(PV.Port) = HT.Port
			AND HT.PortID > 0
			AND PV.Port IS NOT NULL
			AND LEN(LTRIM(PV.Port)) > 0
			AND NOT EXISTS
				(
				SELECT 1
				FROM Dropdown.TierDropdown TD
				WHERE TD.TierID = @nTierID
					AND TD.DropdownMetadataID = @nDropdownMetadataID
					AND TD.DropdownEntityID = HT.PortID
				)
	ORDER BY HT.PortID

	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
		DROP TABLE #oTable

	CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)
	
	DECLARE oCursor CURSOR FOR
		SELECT DISTINCT 
			TD.TierID
		FROM Dropdown.TierDropdown TD
		WHERE TD.DropdownMetadataID = @nDropdownMetadataID

	OPEN oCursor
	FETCH oCursor INTO @nTierID
	WHILE @@fetch_status = 0
		BEGIN
	
		TRUNCATE TABLE #oTable
		
		INSERT INTO #oTable
			(TierDropdownID)
		SELECT
			TD.TierDropdownID
		FROM Dropdown.TierDropdown TD WITH (NOLOCK)
			JOIN Dropdown.Port HT WITH (NOLOCK) ON HT.PortID = TD.DropdownEntityID
				AND TD.TierID = @nTierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
		ORDER BY HT.Port
	
		UPDATE TD
		SET TD.DisplayOrder = T.DisplayOrder
		FROM Dropdown.TierDropdown TD
			JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
				AND TD.DisplayOrder = 0
	
		FETCH oCursor INTO @nTierID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'NLD'),
		Port = 'Curacao, Netherlands Antilles'
	WHERE Port = 'Curacoa, Netherlands Antilles'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'GNQ')
	WHERE Port = 'Malabo'
	
	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'ZAF'),
		Port = 'Port Owen'
	WHERE Port = 'Port Owendo'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'JAM')
	WHERE Port = 'PORT ROYAL'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'USA')
	WHERE Port = 'Rockland, Maine'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'KHM')
	WHERE Port = 'Sihanoukville'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'MNP')
	WHERE Port = 'Tanapag Harbor'
	
	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'PAN'),
		Port = 'VNB'
	WHERE Port = 'VBN'

	END
--ENDIF
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Port'
GO