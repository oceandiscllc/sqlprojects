--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'CDRType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('CDRType','CDR Types','Dropdown','CDRType','CDRTypeID','CDRType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'CDRType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'CDRTypeID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: CDRType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CDRType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CDRType
		(
		CDRTypeID int IDENTITY(0,1) NOT NULL,
		CDRType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CDRType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CDRType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRTypeID'
EXEC Utility.SetIndexNonClustered 'IX_CDRType', @cTableName, 'CDRType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: CDRType

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRType CT WITH (NOLOCK) WHERE CT.CDRTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.CDRType ON
	INSERT INTO Dropdown.CDRType (CDRTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.CDRType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CDR_Type'
WHERE Category = 'CDRType_Old'
GO

INSERT INTO Dropdown.CDRType
	(CDRType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS CDRType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CDR_Type'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.CDRType CT WITH (NOLOCK)
		WHERE CT.CDRType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(C.Type) AS CDRType
FROM dbo.CDR C WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.CDRType CT WITH (NOLOCK)
	WHERE CT.CDRType = LTRIM(C.Type)
	)
	AND C.Type IS NOT NULL

ORDER BY CDRType
GO

UPDATE C 
SET C.CDRTypeID = CT.CDRTypeID 
FROM dbo.CDR C 
	JOIN Dropdown.CDRType CT ON CT.CDRType = C.Type
		AND C.CDRTypeID = 0
GO
		
UPDATE Dropdown.CDRType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND CDRTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'CDRType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	CT.CDRTypeID
FROM Dropdown.CDRType CT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = CT.CDRType
		AND D.Category = 'CDR_Type'
		AND CT.CDRTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = CT.CDRTypeID
			)
ORDER BY CT.CDRTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.CDRType CT WITH (NOLOCK) ON CT.CDRTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CT.CDRType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CDRType_Old'
WHERE Category = 'CDR_Type'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'CDRType'
GO