IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.CDRFileView'))
	DROP VIEW dbo.CDRFileView
GO

CREATE VIEW dbo.CDRFileView
AS
SELECT
	C.CDRID,
	C.City,
	C.Classification,
	C.Condition,
	C.CountryID,
	C.CreationDate,
	C.DeclassifyOn,
	C.Dissemination,
	C.DocCountryDropDownID,
	C.DocDate,
	C.EMail,
	C.FirstName + ' ' + C.LastName AS AuthorName,
	C.FirstName,
	C.Jointcdr,
	C.Keywords,
	C.LastName,
	C.OriginatorCountryID,
	C.OverallCaveat,
	C.Phone,
	C.Place,
	C.Province,
	C.ReleasableTo,
	C.Status,
	C.Subtitle,
	C.Summary,
	C.ThumbHeight,
	C.Thumbnail,
	C.ThumbWidth,
	C.Title,
	C.UpdateDate,
	CC.CDRCategory AS Category,
	CF.CDRFileID, 
	CF.CDRID AS CDRFile_CDRID, 
	CF.Condition AS CDRFile_Condition, 
	CF.FileName, 
	CF.Status AS CDRFile_Status,
	CO.CountryName AS Country,
	CQ.CDRCreatorQualifier AS CreatorQualifier,
	CT.CDRType AS Type,
	T.TierName AS Organization
FROM dbo.CDR C WITH (NOLOCK)
	JOIN dbo.CDRFile CF WITH (NOLOCK) ON CF.CDRID = C.CDRID
		AND C.JointCDR = 'C'
		AND C.Status = 'Active'
		AND CF.Status = 'Active'
	JOIN JLLIS.dbo.Tier T ON T.TierID = C.OriginatingTierID
	JOIN JLLIS.dbo.Country CO ON CO.CountryID = C.CountryID
	JOIN Dropdown.CDRCategory CC ON CC.CDRCategoryID = C.CDRCategoryID
	JOIN Dropdown.CDRCreatorQualifier CQ ON CQ.CDRCreatorQualifierID = C.CDRCreatorQualifierID
	JOIN Dropdown.CDRType CT ON CT.CDRTypeID = C.CDRTypeID
GO
