--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'AnalysisCode')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('AnalysisCode','Analysis Codes','Dropdown','AnalysisCode','AnalysisCodeID','AnalysisCode',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'AnalysisCode'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: AnalysisCode
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.AnalysisCode'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.AnalysisCode
		(
		AnalysisCodeID int IDENTITY(0,1) NOT NULL,
		AnalysisCodeTypeID int,
		AnalysisCode varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCode', ''
EXEC Utility.SetDefault @cTableName, 'AnalysisCodeTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCode', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AnalysisCodeID'
EXEC Utility.SetIndexNonClustered 'IX_AnalysisCode', @cTableName, 'AnalysisCode ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AC.AnalysisCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCode ON
	INSERT INTO Dropdown.AnalysisCode (AnalysisCodeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCode OFF

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AC.AnalysisCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCode ON
	INSERT INTO Dropdown.AnalysisCode (AnalysisCodeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCode OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.AnalysisCode
	(AnalysisCode)
SELECT DISTINCT
	LTRIM(AC1.Description)
FROM dbo.AnalysisCodes AC1 WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.AnalysisCode AC2
	WHERE AC2.AnalysisCode = LTRIM(AC1.Description)
	)
ORDER BY LTRIM(AC1.Description)
GO

DECLARE @cAnalysisCode varchar(250)
DECLARE @nAnalysisCodeTypeID int

DECLARE oCursor CURSOR FOR
	SELECT 
		AC.Description,
		ACT.AnalysisCodeTypeID
	FROM dbo.AnalysisCodes AC
		JOIN Dropdown.AnalysisCodeType ACT ON ACT.AnalysisCodeType = AC.Type
	ORDER BY Description, AC.AnalysisCodeID

OPEN oCursor
FETCH oCursor INTO @cAnalysisCode, @nAnalysisCodeTypeID
WHILE @@fetch_status = 0
	BEGIN
	
	IF EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AnalysisCode = @cAnalysisCode AND AnalysisCodeTypeID = 0)
		BEGIN
		
		UPDATE Dropdown.AnalysisCode
		SET AnalysisCodeTypeID = @nAnalysisCodeTypeID
		WHERE AnalysisCode = @cAnalysisCode
		
		END
	--ENDIF
	
	FETCH oCursor INTO @cAnalysisCode, @nAnalysisCodeTypeID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
		
UPDATE Dropdown.AnalysisCode
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND AnalysisCodeID <> 0
GO
--End table: AnalysisCode

--Begin table: LMSAnalysisCode
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSAnalysisCode'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LMSAnalysisCode
		(
		LMSAnalysisCodeID int IDENTITY(1,1) NOT NULL,
		LMSID int,
		AnalysisCodeID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCodeID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSAnalysisCodeID'
EXEC Utility.SetIndexClustered 'IX_LMSAnalysisCode', @cTableName, 'LMSID ASC,AnalysisCodeID ASC'
GO
--End table: LMSAnalysisCode

INSERT INTO dbo.LMSAnalysisCode
	(LMSID,AnalysisCodeID)
SELECT DISTINCT
	LAC1.LMSID,
	AC2.AnalysisCodeID
FROM dbo.LMSAnalysisCodes LAC1
	JOIN dbo.AnalysisCodes AC1 ON AC1.AnalysisCodeID = LAC1.AnalysisCodeID
	JOIN Dropdown.AnalysisCode AC2 ON AC2.AnalysisCode = AC1.Description
		AND AC2.AnalysisCodeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.LMSAnalysisCode LAC2
			WHERE LAC2.LMSID = LAC1.LMSID
				AND LAC2.AnalysisCodeID = AC2.AnalysisCodeID
			)

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nInstanceID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'AnalysisCode'
	)

SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	@nInstanceID,
	@nDropdownMetadataID,
	AC.AnalysisCodeID
FROM Dropdown.AnalysisCode AC WITH (NOLOCK)
WHERE AC.AnalysisCodeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = @nInstanceID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = AC.AnalysisCodeID
		)
ORDER BY AC.AnalysisCodeID

INSERT INTO #oTable
	(TierDropdownID)
SELECT
	TD.TierDropdownID
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN Dropdown.AnalysisCode AC WITH (NOLOCK) ON AC.AnalysisCodeID = TD.DropdownEntityID
		AND TD.TierID = @nInstanceID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
ORDER BY AC.AnalysisCode

UPDATE TD
SET TD.DisplayOrder = T.DisplayOrder
FROM Dropdown.TierDropdown TD
	JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
		AND TD.DisplayOrder = 0
GO

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'ANALYSIS'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'AnalysisCode'
GO