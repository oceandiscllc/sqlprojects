--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'LessonType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('LessonType','Lesson Types','Dropdown','LessonType','LessonTypeID','LessonType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'LessonType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LessonTypeID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: LessonType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.LessonType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.LessonType
		(
		LessonTypeID int IDENTITY(0,1) NOT NULL,
		LessonType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LessonType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LessonType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LessonTypeID'
EXEC Utility.SetIndexNonClustered 'IX_LessonType', @cTableName, 'LessonType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: LessonType

IF NOT EXISTS (SELECT 1 FROM Dropdown.LessonType LT WITH (NOLOCK) WHERE LT.LessonTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.LessonType ON
	INSERT INTO Dropdown.LessonType (LessonTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.LessonType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'LMS_LessonType'
WHERE Category = 'LMS_LessonType_Old'
GO

INSERT INTO Dropdown.LessonType
	(LessonType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS LessonType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'LMS_LessonType'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.LessonType LT WITH (NOLOCK)
		WHERE LT.LessonType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(L.LessonType) AS LessonType
FROM dbo.LMS L WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.LessonType LT WITH (NOLOCK)
	WHERE LT.LessonType = LTRIM(L.LessonType)
	)
	AND L.LessonType IS NOT NULL

ORDER BY LessonType
GO		

UPDATE L 
SET L.LessonTypeID = LT.LessonTypeID 
FROM dbo.LMS L 
	JOIN Dropdown.LessonType LT ON LT.LessonType = L.LessonType
		AND L.LessonTypeID = 0
GO
		
UPDATE Dropdown.LessonType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND LessonTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'LessonType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	LT.LessonTypeID
FROM Dropdown.LessonType LT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = LT.LessonType
		AND D.Category = 'LMS_LessonType'
		AND LT.LessonTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = LT.LessonTypeID
			)
ORDER BY LT.LessonTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.LessonType LT WITH (NOLOCK) ON LT.LessonTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY LT.LessonType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'LMS_LessonType_Old'
WHERE Category = 'LMS_LessonType'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'LessonType'
GO