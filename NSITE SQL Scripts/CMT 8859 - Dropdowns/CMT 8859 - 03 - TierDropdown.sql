--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

--Begin table Dropdown.TierDropdown
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TierDropdown'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN

	CREATE TABLE Dropdown.TierDropdown
		(
		TierDropdownID int IDENTITY(1,1) NOT NULL,
		TierID int,
		DropdownMetadataID int,
		DropdownEntityID int,
		DisplayOrder int,
		IsForSubordinateTiers bit,
		UpdateJLLISUserID int
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'DropdownEntityID', 0
EXEC Utility.SetDefault @cTableName, 'DropdownMetadataID', 0
EXEC Utility.SetDefault @cTableName, 'IsForSubordinateTiers', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownEntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForSubordinateTiers', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'TierDropdownID'
EXEC Utility.SetIndexClustered 'IX_TierDropdown', @cTableName, 'DropdownMetadataID ASC,TierID ASC,DisplayOrder ASC,DropdownEntityID ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table Dropdown.TierDropdown

--Begin view Dropdown.TierDropdownView
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID('Dropdown.TierDropdownView'))
	DROP VIEW Dropdown.TierDropdownView
GO

CREATE VIEW Dropdown.TierDropdownView AS
SELECT 
	TD.TierDropdownID,
	TD.TierID,
	T.TierName,
	TD.DropdownMetadataID,
	DMD.DropdownMetadataCode,
	TD.DropdownEntityID,
	TD.DisplayOrder,
	TD.IsForSubordinateTiers
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T ON T.TierID = TD.TierID
	JOIN JLLIS.Dropdown.DropdownMetadata DMD ON DMD.DropdownMetadataID = TD.DropdownMetadataID
GO
--End view Dropdown.TierDropdownView

--Begin menu table update
UPDATE dbo.MenuItem
SET Link = 'admin/dropdown/manage.cfm'
WHERE Title = 'DROPDOWNS'
GO
--End menu table update