--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'WarfareMission')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('WarfareMission','Warfare Missions','Dropdown','WarfareMission','WarfareMissionID','WarfareMission',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'WarfareMission'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'WarfareMissionID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'WarfareMissionID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS
	
--Begin table: WarfareMission
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.WarfareMission'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.WarfareMission
		(
		WarfareMissionID int IDENTITY(0,1) NOT NULL,
		WarfareMission varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'WarfareMission', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'WarfareMission', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
EXEC Utility.SetPrimaryKeyClustered @cTableName, 'WarfareMissionID'
EXEC Utility.SetIndexNonClustered 'IX_WarfareMission', @cTableName, 'WarfareMission ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: WarfareMission
	
IF NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMissionID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.WarfareMission ON
	INSERT INTO Dropdown.WarfareMission (WarfareMissionID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.WarfareMission OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'Warfare/Mission Area'
WHERE Category = 'Warfare/Mission Area_Old'
GO

INSERT INTO Dropdown.WarfareMission
	(WarfareMission)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS WarfareMission
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'Warfare/Mission Area'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.WarfareMission WM WITH (NOLOCK)
		WHERE WM.WarfareMission = LTRIM(D.DisplayText)
		)
GO
	
IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID('dbo.CDR') AND C.Name = 'WarfareMission')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.WarfareMission (WarfareMission) SELECT DISTINCT LTRIM(C.WarfareMission) FROM dbo.CDR C WITH (NOLOCK) WHERE C.WarfareMission IS NOT NULL AND NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMission = LTRIM(C.WarfareMission)) ORDER BY LTRIM(C.WarfareMission)'
	EXEC (@cSQL)

	SET @cSQL = 'UPDATE C SET C.WarfareMissionID = WM.WarfareMissionID FROM dbo.CDR C JOIN Dropdown.WarfareMission WM ON WM.WarfareMission = LTRIM(C.WarfareMission) AND C.WarfareMissionID = 0'
	EXEC (@cSQL)
	
	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID('dbo.LMS') AND C.Name = 'WarfareMission')
	BEGIN

	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.WarfareMission (WarfareMission) SELECT DISTINCT LTRIM(L.WarfareMission) FROM dbo.LMS L WITH (NOLOCK) WHERE L.WarfareMission IS NOT NULL AND NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMission = LTRIM(L.WarfareMission)) ORDER BY LTRIM(L.WarfareMission)'
	EXEC (@cSQL)

	SET @cSQL = 'UPDATE L SET L.WarfareMissionID = WM.WarfareMissionID FROM dbo.LMS L JOIN Dropdown.WarfareMission WM ON WM.WarfareMission = LTRIM(L.WarfareMission) AND L.WarfareMissionID = 0'
	EXEC (@cSQL)

	END
--ENDIF
GO

UPDATE Dropdown.WarfareMission
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND WarfareMissionID <> 0
GO
	
DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'WarfareMission'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	WM.WarfareMissionID
FROM Dropdown.WarfareMission WM WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = WM.WarfareMission
		AND D.Category = 'Warfare/Mission Area'
		AND WM.WarfareMissionID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = WM.WarfareMissionID
			)
ORDER BY WM.WarfareMissionID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.WarfareMission WM WITH (NOLOCK) ON WM.WarfareMissionID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY WM.WarfareMission

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
	
UPDATE dbo.Dropdown
SET Category = 'Warfare/Mission Area_Old'
WHERE Category = 'Warfare/Mission Area'
GO
	
UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'WarfareMission'
GO