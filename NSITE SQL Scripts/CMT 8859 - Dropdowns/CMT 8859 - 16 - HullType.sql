--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'HullType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('HullType','Hull Types','Dropdown','HullType','HullTypeID','HullType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'HullType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: PortVisit
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @cColumnName varchar(250)
	DECLARE @cTableName varchar(250)

	SET @cColumnName = 'HullTypeID'
	SET @cTableName = 'dbo.PortVisit'

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit
	
--Begin table: HullType
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cTableName varchar(250)

	SET @cTableName = 'Dropdown.HullType'

	IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
		BEGIN

		CREATE TABLE Dropdown.HullType
			(
			HullTypeID int IDENTITY(0,1) NOT NULL,
			HullType varchar(250),
			OriginatingTierID int,
			UpdateJLLISUserID int,
			IsActive bit
			) ON [PRIMARY]
	
		END
	--ENDIF

	EXEC Utility.DropConstraintsAndIndexes @cTableName

	EXEC Utility.SetDefault @cTableName, 'HullType', ''
	EXEC Utility.SetDefault @cTableName, 'IsActive', 1
	EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
	EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

	EXEC Utility.SetColumnNotNull @cTableName, 'HullType', 'varchar(250)'
	EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
	EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
	EXEC Utility.SetPrimaryKeyClustered @cTableName, 'HullTypeID'
	EXEC Utility.SetIndexNonClustered 'IX_HullType', @cTableName, 'HullType ASC'
	EXEC Utility.EnableAuditing @cTableName, 0

	END
--ENDIF
GO
--End table: HullType
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM Dropdown.HullType HT WITH (NOLOCK) WHERE HT.HullTypeID = 0)
		BEGIN
		
		SET IDENTITY_INSERT Dropdown.HullType ON
		INSERT INTO Dropdown.HullType (HullTypeID) VALUES (0)
		SET IDENTITY_INSERT Dropdown.HullType OFF
	
		END
	--ENDIF

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	INSERT INTO Dropdown.HullType
		(HullType)
	SELECT DISTINCT 
		LTRIM(PV.HullType)
	FROM dbo.PortVisit PV WITH (NOLOCK)
	WHERE PV.HullType IS NOT NULL
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.HullType HT WITH (NOLOCK)
			WHERE HT.HullType = LTRIM(PV.HullType)
			)
	ORDER BY LTRIM(PV.HullType)

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.HullTypeID = HT.HullTypeID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.HullType HT ON HT.HullType = PV.HullType
			AND PV.HullTypeID = 0
		
	UPDATE Dropdown.HullType
	SET OriginatingTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	WHERE OriginatingTierID = 0
		AND HullTypeID <> 0

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @nDropdownMetadataID int
	DECLARE @nTierID int
	
	SET @nDropdownMetadataID = 
		(
		SELECT DMD.DropdownMetadataID
		FROM JLLIS.Dropdown.DropdownMetadata DMD
		WHERE DMD.DropdownMetadataCode = 'HullType'
		)
	
	SET @nTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	
	INSERT INTO Dropdown.TierDropdown
		(TierID, DropdownMetadataID, DropdownEntityID)
	SELECT DISTINCT
		@nTierID,
		@nDropdownMetadataID,
		HT.HullTypeID
	FROM Dropdown.HullType HT WITH (NOLOCK)
		JOIN dbo.PortVisit PV WITH (NOLOCK) ON LTRIM(PV.HullType) = HT.HullType
			AND HT.HullTypeID > 0
			AND PV.HullType IS NOT NULL
			AND LEN(LTRIM(PV.HullType)) > 0
			AND NOT EXISTS
				(
				SELECT 1
				FROM Dropdown.TierDropdown TD
				WHERE TD.TierID = @nTierID
					AND TD.DropdownMetadataID = @nDropdownMetadataID
					AND TD.DropdownEntityID = HT.HullTypeID
				)
	ORDER BY HT.HullTypeID

	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
		DROP TABLE #oTable

	CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)
	
	DECLARE oCursor CURSOR FOR
		SELECT DISTINCT 
			TD.TierID
		FROM Dropdown.TierDropdown TD
		WHERE TD.DropdownMetadataID = @nDropdownMetadataID

	OPEN oCursor
	FETCH oCursor INTO @nTierID
	WHILE @@fetch_status = 0
		BEGIN
	
		TRUNCATE TABLE #oTable
		
		INSERT INTO #oTable
			(TierDropdownID)
		SELECT
			TD.TierDropdownID
		FROM Dropdown.TierDropdown TD WITH (NOLOCK)
			JOIN Dropdown.HullType HT WITH (NOLOCK) ON HT.HullTypeID = TD.DropdownEntityID
				AND TD.TierID = @nTierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
		ORDER BY HT.HullType
	
		UPDATE TD
		SET TD.DisplayOrder = T.DisplayOrder
		FROM Dropdown.TierDropdown TD
			JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
				AND TD.DisplayOrder = 0
	
		FETCH oCursor INTO @nTierID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
	
UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'HullType'
GO