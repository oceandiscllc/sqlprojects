USE DLA
GO
--Begin file CMT 8859 - 02 - Enable Auditing.sql
--Begin create AuditLog table
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.AuditLog') AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.AuditLog
		(
		AuditLogID int IDENTITY(1,1) NOT NULL,
		SchemaName varchar(50) NOT NULL,
		TableName varchar(50) NOT NULL,
		EntityID int NOT NULL CONSTRAINT DF_AuditLog_EntityID DEFAULT 0,
		AuditAction varchar(50) NOT NULL,
		AuditDate datetime NOT NULL CONSTRAINT DF_AuditLog_AuditDate DEFAULT getDate(),
		AuditData xml NOT NULL,
		UpdateJLLISUserID int NOT NULL CONSTRAINT DF_AuditLog_UpdateJLLISUserID DEFAULT 0,
		Title varchar(50) NULL,
		FirstName varchar(50) NULL,
		LastName varchar(50) NULL,
		CONSTRAINT PK_AuditLog PRIMARY KEY NONCLUSTERED 
			(
			AuditLogID ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	CREATE CLUSTERED INDEX IX_AuditLog ON dbo.AuditLog
		(
		SchemaName ASC,
		TableName ASC,
		EntityID ASC,
		AuditLogID DESC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	END
--ENDIF
GO
--End create AuditLog table

--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--Begin create EnableAuditing stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.EnableAuditing') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.EnableAuditing
GO

-- ==================================================================
-- Author:		Todd Pires
-- Create date: 2011.02.15
-- Description:	A stored procedure to enable data auditing on a table
-- ==================================================================
CREATE PROCEDURE Utility.EnableAuditing
	@cTargetTable varchar(250),
	@nPrintSQL bit

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cCursorName varchar(100)
	DECLARE @cCursorSQL1 varchar(max)
	DECLARE @cCursorSQL2 varchar(max)
	DECLARE @cInsertSQL varchar(max)
	DECLARE @cMemoryTableName varchar(50)
	DECLARE @cPrimaryKeyFieldName varchar(100)
	DECLARE @cSchemaName varchar(50)
	DECLARE @cSQL varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cTab5 varchar(5)
	DECLARE @cTab6 varchar(6)
	DECLARE @cTab7 varchar(7)
	DECLARE @cTab8 varchar(8)
	DECLARE @cTableName varchar(100)
	DECLARE @cTriggerName varchar(250)
	DECLARE @cWhereSQL varchar(max)
	DECLARE @nHasUpdateJLLISUserID bit
	DECLARE @nLength int

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)
	SET @cTab5 = REPLICATE(CHAR(9), 5)
	SET @cTab6 = REPLICATE(CHAR(9), 6)
	SET @cTab7 = REPLICATE(CHAR(9), 7)
	SET @cTab8 = REPLICATE(CHAR(9), 8)
	SET @nHasUpdateJLLISUserID = 0
	
	IF CHARINDEX('.', @cTargetTable) = 0
		SET @cTargetTable = 'dbo.' + @cTargetTable

	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTargetTable) AND C.Name = 'UpdateJLLISUserID')
		SET @nHasUpdateJLLISUserID = 1

	SET @nLength = LEN(@cTargetTable) - CHARINDEX('.', @cTargetTable)

	SET @cSchemaName = LEFT(@cTargetTable, CHARINDEX('.', @cTargetTable) - 1)
	SET @cTableName = RIGHT(@cTargetTable, @nLength)
	SET @cCursorName = 'o' + @cTableName + 'AuditLogCursor'

	SET @cPrimaryKeyFieldName = 
		(
		SELECT COL_NAME(ic.object_ID, ic.column_ID)
		FROM sys.Indexes I WITH (NOLOCK)
			JOIN sys.index_columns IC WITH (NOLOCK) ON I.object_ID = IC.object_ID
			JOIN sys.objects O WITH (NOLOCK) ON O.object_ID = I.object_ID
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID
				AND I.Index_ID = IC.Index_ID
				AND I.Is_Primary_Key = 1
				AND S.Name = @cSchemaName
				AND O.Name = @cTableName
		)

	SET @cCursorSQL1 = 'INSERT INTO @oTable'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + '(PrimaryKey, AuditData)'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab1 + 'SELECT'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + 'T.' + @cPrimaryKeyFieldName + ','
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + '(SELECT T.* FOR XML RAW(''Root''), ELEMENTS)'

	SET @cCursorSQL2 = 'DECLARE ' + @cCursorName + ' CURSOR FOR'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SELECT'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab3 + 'T.PrimaryKey,'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab3 + 'T.AuditData'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'FROM @oTable T'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'OPEN ' + @cCursorName
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'FETCH ' + @cCursorName + ' INTO @nPrimaryKey, @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'WHILE @@fetch_status = 0'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'BEGIN'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SET @oXML.modify(''delete (/Root/UpdateJLLISUserID)'')'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'UPDATE @oTable'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SET AuditData = @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'WHERE PrimaryKey = @nPrimaryKey'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'FETCH ' + @cCursorName + ' INTO @nPrimaryKey, @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'END'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + '--END WHILE'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'CLOSE ' + @cCursorName
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'DEALLOCATE ' + @cCursorName

	SET @cInsertSQL = 'INSERT INTO dbo.AuditLog'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SchemaName,TableName,EntityID,AuditAction,AuditData,UpdateJLLISUserID,Title,FirstName,LastName)'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab1 + 'SELECT'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '''' + @cSchemaName + ''','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '''' + @cTableName + ''','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T1.' + @cPrimaryKeyFieldName + ','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '@cAuditAction,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T2.AuditData,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T1.UpdateJLLISUserID,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.Title FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID),'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID),'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID)'

	SET @cWhereSQL = @cTab1 + 'JOIN @oTable T2 ON T2.PrimaryKey = T1.' + @cPrimaryKeyFieldName
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab3 + 'AND NOT EXISTS'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + '('
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'SELECT 1'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'FROM dbo.AuditLog AL1 WITH (NOLOCK)'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'WHERE AL1.AuditAction = @cAuditAction'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab5 + 'AND CAST(AL1.AuditData as varchar(max)) = CAST(@oXML as varchar(max))'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.EntityID = T1.' + @cPrimaryKeyFieldName
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.SchemaName = ''' + @cSchemaName + ''''
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.TableName = ''' + @cTableName + ''''
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.AuditLogID ='
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + '('
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'SELECT TOP 1'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AL2.AuditLogID'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'FROM dbo.AuditLog AL2 WITH (NOLOCK)'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'WHERE AL2.EntityID = AL1.EntityID'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AND AL2.SchemaName = AL1.SchemaName'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AND AL2.TableName = AL1.TableName'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'ORDER BY AL2.AuditLogID DESC'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + ')'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + ')'

	IF @cPrimaryKeyFieldName IS NULL
		BEGIN
		
		print 'Cannot create trigger on table ' + @cTargetTable + '.  The table has no primary key.'
		
		END
	ELSE IF @nHasUpdateJLLISUserID = 0
		BEGIN
		
		print 'Cannot create trigger on table ' + @cTargetTable + '.  The table does not have an UpdateJLLISUserID field.'
		
		END
	ELSE
		BEGIN
		
		SET @cTriggerName = @cSchemaName + '.TR_' + @cTableName + '_AuditLog'
		SET @cSQL = 'IF EXISTS (SELECT 1 FROM sys.triggers T WITH (NOLOCK) WHERE T.object_id = OBJECT_ID(''' + @cTriggerName + '''))'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DROP TRIGGER ' + @cTriggerName
		
		IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
			print @cSQL
		ELSE
			EXEC (@cSQL)
		--ENDIF
		
		SET @cSQL = 'CREATE TRIGGER ' + @cTriggerName + ' ON ' + @cTargetTable + ' FOR INSERT, UPDATE, DELETE AS'
		SET @cSQL = @cSQL + @cCRLF + 'SET ARITHABORT ON'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @cAuditAction varchar(50)'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @cMemoryTableName varchar(50)'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nHaveDeleted bit'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nHaveInserted bit'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nPrimaryKey int'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @oXML xml'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @oTable table (PrimaryKey int, AuditData xml)'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'SET @nHaveDeleted = 0'
		SET @cSQL = @cSQL + @cCRLF + 'SET @nHaveInserted = 0'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'IF EXISTS (SELECT 1 FROM Deleted)'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @nHaveDeleted = 1'
		SET @cSQL = @cSQL + @cCRLF + 'IF EXISTS (SELECT 1 FROM Inserted)'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'IF @nHaveDeleted = 1 AND @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''UPDATE'''
		SET @cSQL = @cSQL + @cCRLF + 'ELSE IF @nHaveDeleted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''DELETE'''
		SET @cSQL = @cSQL + @cCRLF + 'ELSE IF @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''INSERT'''
		SET @cSQL = @cSQL + @cCRLF

		SET @cMemoryTableName = 'Deleted'

		SET @cSQL = @cSQL + @cCRLF + 'IF @cAuditAction = ''DELETE'''
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'BEGIN'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL1
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL2
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cInsertSQL
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cWhereSQL
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'END'

		SET @cMemoryTableName = 'Inserted'

		SET @cSQL = @cSQL + @cCRLF + 'ELSE'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'BEGIN'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL1
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL2
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cInsertSQL
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cWhereSQL
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'END'
		SET @cSQL = @cSQL + @cCRLF + '--ENDIF'
		
		IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
			print @cSQL
		ELSE
			EXEC (@cSQL)
		--ENDIF

		print 'Trigger ' + @cTriggerName + ' has been created on table ' + @cTargetTable + '.'

		IF EXISTS (SELECT 1 FROM sys.triggers T WITH (NOLOCK) WHERE T.object_id = OBJECT_ID('TR_AuditLog_AuditLog'))
			BEGIN
			
			DROP TRIGGER TR_AuditLog_AuditLog
	
			print 'Trigger TR_AuditLog_AuditLog has been dropped from table dbo.AuditLog.'

			END
		--ENDIF
		
		END
	--ENDIF
	
END
GO
--End create EnableAuditing stored procedure
--End file CMT 8859 - 02 - Enable Auditing.sql
GO
--Begin file CMT 8859 - 03 - TierDropdown.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

--Begin table Dropdown.TierDropdown
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TierDropdown'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN

	CREATE TABLE Dropdown.TierDropdown
		(
		TierDropdownID int IDENTITY(1,1) NOT NULL,
		TierID int,
		DropdownMetadataID int,
		DropdownEntityID int,
		DisplayOrder int,
		IsForSubordinateTiers bit,
		UpdateJLLISUserID int
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'DropdownEntityID', 0
EXEC Utility.SetDefault @cTableName, 'DropdownMetadataID', 0
EXEC Utility.SetDefault @cTableName, 'IsForSubordinateTiers', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownEntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForSubordinateTiers', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'TierDropdownID'
EXEC Utility.SetIndexClustered 'IX_TierDropdown', @cTableName, 'DropdownMetadataID ASC,TierID ASC,DisplayOrder ASC,DropdownEntityID ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table Dropdown.TierDropdown

--Begin view Dropdown.TierDropdownView
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID('Dropdown.TierDropdownView'))
	DROP VIEW Dropdown.TierDropdownView
GO

CREATE VIEW Dropdown.TierDropdownView AS
SELECT 
	TD.TierDropdownID,
	TD.TierID,
	T.TierName,
	TD.DropdownMetadataID,
	DMD.DropdownMetadataCode,
	TD.DropdownEntityID,
	TD.DisplayOrder,
	TD.IsForSubordinateTiers
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T ON T.TierID = TD.TierID
	JOIN JLLIS.Dropdown.DropdownMetadata DMD ON DMD.DropdownMetadataID = TD.DropdownMetadataID
GO
--End view Dropdown.TierDropdownView

--Begin menu table update
UPDATE dbo.MenuItem
SET Link = 'admin/dropdown/manage.cfm'
WHERE Title = 'DROPDOWNS'
GO
--End menu table update
--End file CMT 8859 - 03 - TierDropdown.sql
GO
--Begin file CMT 8859 - 04 - CDRCategory.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'CDRCategory')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('CDRCategory','CDR Categories','Dropdown','CDRCategory','CDRCategoryID','CDRCategory',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'CDRCategory'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'CDRCategoryID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: CDRCategory
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CDRCategory'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CDRCategory
		(
		CDRCategoryID int IDENTITY(0,1) NOT NULL,
		CDRCategory varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CDRCategory', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CDRCategory', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRCategoryID'
EXEC Utility.SetIndexNonClustered 'IX_CDRCategory', @cTableName, 'CDRCategory ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: CDRCategory

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRCategory CC WITH (NOLOCK) WHERE CC.CDRCategoryID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.CDRCategory ON
	INSERT INTO Dropdown.CDRCategory (CDRCategoryID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.CDRCategory OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CDR_Category'
WHERE Category = 'CDRCategory_Old'
GO

INSERT INTO Dropdown.CDRCategory
	(CDRCategory)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS CDRCategory
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CDR_Category'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.CDRCategory CC WITH (NOLOCK)
		WHERE CC.CDRCategory = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(C.Category) AS CDRCategory
FROM dbo.CDR C WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.CDRCategory CC WITH (NOLOCK)
	WHERE CC.CDRCategory = LTRIM(C.Category)
	)
	AND C.Category IS NOT NULL

ORDER BY CDRCategory
GO

UPDATE C 
SET C.CDRCategoryID = CC.CDRCategoryID 
FROM dbo.CDR C 
	JOIN Dropdown.CDRCategory CC ON CC.CDRCategory = C.Category
		AND C.CDRCategoryID = 0
GO
		
UPDATE Dropdown.CDRCategory
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND CDRCategoryID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'CDRCategory'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	CC.CDRCategoryID
FROM Dropdown.CDRCategory CC WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = CC.CDRCategory
		AND D.Category = 'CDR_Category'
		AND CC.CDRCategoryID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = CC.CDRCategoryID
			)
ORDER BY CC.CDRCategoryID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.CDRCategory CC WITH (NOLOCK) ON CC.CDRCategoryID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CC.CDRCategory

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CDRCategory_Old'
WHERE Category = 'CDR_Category'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'CDRCategory'
GO
--End file CMT 8859 - 04 - CDRCategory.sql
GO
--Begin file CMT 8859 - 05 - CDRType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'CDRType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('CDRType','CDR Types','Dropdown','CDRType','CDRTypeID','CDRType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'CDRType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'CDRTypeID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: CDRType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CDRType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CDRType
		(
		CDRTypeID int IDENTITY(0,1) NOT NULL,
		CDRType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CDRType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CDRType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRTypeID'
EXEC Utility.SetIndexNonClustered 'IX_CDRType', @cTableName, 'CDRType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: CDRType

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRType CT WITH (NOLOCK) WHERE CT.CDRTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.CDRType ON
	INSERT INTO Dropdown.CDRType (CDRTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.CDRType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CDR_Type'
WHERE Category = 'CDRType_Old'
GO

INSERT INTO Dropdown.CDRType
	(CDRType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS CDRType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CDR_Type'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.CDRType CT WITH (NOLOCK)
		WHERE CT.CDRType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(C.Type) AS CDRType
FROM dbo.CDR C WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.CDRType CT WITH (NOLOCK)
	WHERE CT.CDRType = LTRIM(C.Type)
	)
	AND C.Type IS NOT NULL

ORDER BY CDRType
GO

UPDATE C 
SET C.CDRTypeID = CT.CDRTypeID 
FROM dbo.CDR C 
	JOIN Dropdown.CDRType CT ON CT.CDRType = C.Type
		AND C.CDRTypeID = 0
GO
		
UPDATE Dropdown.CDRType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND CDRTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'CDRType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	CT.CDRTypeID
FROM Dropdown.CDRType CT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = CT.CDRType
		AND D.Category = 'CDR_Type'
		AND CT.CDRTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = CT.CDRTypeID
			)
ORDER BY CT.CDRTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.CDRType CT WITH (NOLOCK) ON CT.CDRTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CT.CDRType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CDRType_Old'
WHERE Category = 'CDR_Type'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'CDRType'
GO
--End file CMT 8859 - 05 - CDRType.sql
GO
--Begin file CMT 8859 - 06 - TaskSection.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'TaskSection')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('TaskSection','Task Sections','Dropdown','TaskSection','TaskSectionID','TaskSection',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'TaskSection'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: BB
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'TaskSectionID'
SET @cTableName = 'dbo.BB'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

EXEC Utility.AddColumn @cTableName, 'OriginatingTierID', 'int NULL'
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
GO

UPDATE BB
SET BB.OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
FROM dbo.BB BB
WHERE BB.OriginatingTierID = 0
--End table: BB

--Begin table: TaskSection
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TaskSection'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.TaskSection
		(
		TaskSectionID int IDENTITY(0,1) NOT NULL,
		TaskSection varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'TaskSection', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TaskSection', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TaskSectionID'
EXEC Utility.SetIndexNonClustered 'IX_TaskSection', @cTableName, 'TaskSection ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: TaskSection

IF NOT EXISTS (SELECT 1 FROM Dropdown.TaskSection TS WITH (NOLOCK) WHERE TS.TaskSectionID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.TaskSection ON
	INSERT INTO Dropdown.TaskSection (TaskSectionID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.TaskSection OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'BBSection'
WHERE Category = 'TaskSection_Old'
GO

INSERT INTO Dropdown.TaskSection
	(TaskSection)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS TaskSection
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'BBSection'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TaskSection TS WITH (NOLOCK)
		WHERE TS.TaskSection = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(BB.IOBV) AS TaskSection
FROM dbo.BB BB WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.TaskSection TS WITH (NOLOCK)
	WHERE TS.TaskSection = LTRIM(BB.IOBV)
	)
	AND BB.IOBV IS NOT NULL

ORDER BY TaskSection
GO		

UPDATE BB
SET BB.TaskSectionID = TS.TaskSectionID 
FROM dbo.BB BB
	JOIN Dropdown.TaskSection TS ON TS.TaskSection = BB.IOBV
		AND BB.TaskSectionID = 0
GO
		
UPDATE Dropdown.TaskSection
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND TaskSectionID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'TaskSection'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	TS.TaskSectionID
FROM Dropdown.TaskSection TS WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = TS.TaskSection
		AND D.Category = 'BBSection'
		AND TS.TaskSectionID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = TS.TaskSectionID
			)
ORDER BY TS.TaskSectionID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.TaskSection TS WITH (NOLOCK) ON TS.TaskSectionID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY TS.TaskSection

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'TaskSection_Old'
WHERE Category = 'BBSection'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'TaskSection'
GO
--End file CMT 8859 - 06 - TaskSection.sql
GO
--Begin file CMT 8859 - 07 - TaskType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'TaskType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('TaskType','Task Types','Dropdown','TaskType','TaskTypeID','TaskType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'TaskType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: BB
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'TaskTypeID'
SET @cTableName = 'dbo.BB'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

EXEC Utility.AddColumn @cTableName, 'OriginatingTierID', 'int NULL'
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
GO

UPDATE BB
SET BB.OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
FROM dbo.BB BB
WHERE BB.OriginatingTierID = 0
--End table: BB

--Begin table: TaskType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TaskType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.TaskType
		(
		TaskTypeID int IDENTITY(0,1) NOT NULL,
		TaskType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'TaskType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TaskType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TaskTypeID'
EXEC Utility.SetIndexNonClustered 'IX_TaskType', @cTableName, 'TaskType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: TaskType

IF NOT EXISTS (SELECT 1 FROM Dropdown.TaskType TT WITH (NOLOCK) WHERE TT.TaskTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.TaskType ON
	INSERT INTO Dropdown.TaskType (TaskTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.TaskType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'BBType'
WHERE Category = 'TaskType_Old'
GO

INSERT INTO Dropdown.TaskType
	(TaskType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS TaskType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'BBType'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TaskType TT WITH (NOLOCK)
		WHERE TT.TaskType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(BB.Type) AS TaskType
FROM dbo.BB BB WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.TaskType TT WITH (NOLOCK)
	WHERE TT.TaskType = LTRIM(BB.Type)
	)
	AND BB.Type IS NOT NULL

ORDER BY TaskType
GO		

UPDATE BB
SET BB.TaskTypeID = TT.TaskTypeID 
FROM dbo.BB BB
	JOIN Dropdown.TaskType TT ON TT.TaskType = BB.Type
		AND BB.TaskTypeID = 0
GO
		
UPDATE Dropdown.TaskType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND TaskTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'TaskType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	TT.TaskTypeID
FROM Dropdown.TaskType TT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = TT.TaskType
		AND D.Category = 'BBType'
		AND TT.TaskTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = TT.TaskTypeID
			)
ORDER BY TT.TaskTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.TaskType TT WITH (NOLOCK) ON TT.TaskTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY TT.TaskType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'TaskType_Old'
WHERE Category = 'BBType'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'TaskType'
GO
--End file CMT 8859 - 07 - TaskType.sql
GO
--Begin file CMT 8859 - 08 - AnalysisCodeType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'AnalysisCodeType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('AnalysisCodeType','Analysis Code Types','Dropdown','AnalysisCodeType','AnalysisCodeTypeID','AnalysisCodeType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'AnalysisCodeType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: AnalysisCodeType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.AnalysisCodeType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.AnalysisCodeType
		(
		AnalysisCodeTypeID int IDENTITY(0,1) NOT NULL,
		AnalysisCodeType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCodeType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AnalysisCodeTypeID'
EXEC Utility.SetIndexNonClustered 'IX_AnalysisCodeType', @cTableName, 'AnalysisCodeType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK) WHERE ACT.AnalysisCodeTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCodeType ON
	INSERT INTO Dropdown.AnalysisCodeType (AnalysisCodeTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCodeType OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.AnalysisCodeType
	(AnalysisCodeType)
SELECT DISTINCT 
	UPPER(LTRIM(AC.Type))
FROM dbo.AnalysisCodes AC WITH (NOLOCK)
WHERE AC.Type IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK)
		WHERE ACT.AnalysisCodeType = UPPER(LTRIM(AC.Type))
		)
ORDER BY UPPER(LTRIM(AC.Type))
GO		

UPDATE Dropdown.AnalysisCodeType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND AnalysisCodeTypeID <> 0
GO
--End table: AnalysisCodeType

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nInstanceID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'AnalysisCodeType'
	)

SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	@nInstanceID,
	@nDropdownMetadataID,
	ACT.AnalysisCodeTypeID
FROM Dropdown.AnalysisCodeType ACT WITH (NOLOCK)
WHERE ACT.AnalysisCodeTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = @nInstanceID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = ACT.AnalysisCodeTypeID
		)
ORDER BY ACT.AnalysisCodeTypeID

INSERT INTO #oTable
	(TierDropdownID)
SELECT
	TD.TierDropdownID
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN Dropdown.AnalysisCodeType ACT WITH (NOLOCK) ON ACT.AnalysisCodeTypeID = TD.DropdownEntityID
		AND TD.TierID = @nInstanceID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
ORDER BY ACT.AnalysisCodeType

UPDATE TD
SET TD.DisplayOrder = T.DisplayOrder
FROM Dropdown.TierDropdown TD
	JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
		AND TD.DisplayOrder = 0

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'AnalysisCodeType'
GO
--End file CMT 8859 - 08 - AnalysisCodeType.sql
GO
--Begin file CMT 8859 - 09 - AnalysisCode.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'AnalysisCode')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('AnalysisCode','Analysis Codes','Dropdown','AnalysisCode','AnalysisCodeID','AnalysisCode',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'AnalysisCode'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: AnalysisCode
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.AnalysisCode'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.AnalysisCode
		(
		AnalysisCodeID int IDENTITY(0,1) NOT NULL,
		AnalysisCodeTypeID int,
		AnalysisCode varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCode', ''
EXEC Utility.SetDefault @cTableName, 'AnalysisCodeTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCode', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AnalysisCodeID'
EXEC Utility.SetIndexNonClustered 'IX_AnalysisCode', @cTableName, 'AnalysisCode ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AC.AnalysisCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCode ON
	INSERT INTO Dropdown.AnalysisCode (AnalysisCodeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCode OFF

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AC.AnalysisCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.AnalysisCode ON
	INSERT INTO Dropdown.AnalysisCode (AnalysisCodeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.AnalysisCode OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.AnalysisCode
	(AnalysisCode)
SELECT DISTINCT
	LTRIM(AC1.Description)
FROM dbo.AnalysisCodes AC1 WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.AnalysisCode AC2
	WHERE AC2.AnalysisCode = LTRIM(AC1.Description)
	)
ORDER BY LTRIM(AC1.Description)
GO

DECLARE @cAnalysisCode varchar(250)
DECLARE @nAnalysisCodeTypeID int

DECLARE oCursor CURSOR FOR
	SELECT 
		AC.Description,
		ACT.AnalysisCodeTypeID
	FROM dbo.AnalysisCodes AC
		JOIN Dropdown.AnalysisCodeType ACT ON ACT.AnalysisCodeType = AC.Type
	ORDER BY Description, AC.AnalysisCodeID

OPEN oCursor
FETCH oCursor INTO @cAnalysisCode, @nAnalysisCodeTypeID
WHILE @@fetch_status = 0
	BEGIN
	
	IF EXISTS (SELECT 1 FROM Dropdown.AnalysisCode AC WITH (NOLOCK) WHERE AnalysisCode = @cAnalysisCode AND AnalysisCodeTypeID = 0)
		BEGIN
		
		UPDATE Dropdown.AnalysisCode
		SET AnalysisCodeTypeID = @nAnalysisCodeTypeID
		WHERE AnalysisCode = @cAnalysisCode
		
		END
	--ENDIF
	
	FETCH oCursor INTO @cAnalysisCode, @nAnalysisCodeTypeID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
		
UPDATE Dropdown.AnalysisCode
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND AnalysisCodeID <> 0
GO
--End table: AnalysisCode

--Begin table: LMSAnalysisCode
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSAnalysisCode'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LMSAnalysisCode
		(
		LMSAnalysisCodeID int IDENTITY(1,1) NOT NULL,
		LMSID int,
		AnalysisCodeID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AnalysisCodeID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSAnalysisCodeID'
EXEC Utility.SetIndexClustered 'IX_LMSAnalysisCode', @cTableName, 'LMSID ASC,AnalysisCodeID ASC'
GO
--End table: LMSAnalysisCode

INSERT INTO dbo.LMSAnalysisCode
	(LMSID,AnalysisCodeID)
SELECT DISTINCT
	LAC1.LMSID,
	AC2.AnalysisCodeID
FROM dbo.LMSAnalysisCodes LAC1
	JOIN dbo.AnalysisCodes AC1 ON AC1.AnalysisCodeID = LAC1.AnalysisCodeID
	JOIN Dropdown.AnalysisCode AC2 ON AC2.AnalysisCode = AC1.Description
		AND AC2.AnalysisCodeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.LMSAnalysisCode LAC2
			WHERE LAC2.LMSID = LAC1.LMSID
				AND LAC2.AnalysisCodeID = AC2.AnalysisCodeID
			)

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nInstanceID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'AnalysisCode'
	)

SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	@nInstanceID,
	@nDropdownMetadataID,
	AC.AnalysisCodeID
FROM Dropdown.AnalysisCode AC WITH (NOLOCK)
WHERE AC.AnalysisCodeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = @nInstanceID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = AC.AnalysisCodeID
		)
ORDER BY AC.AnalysisCodeID

INSERT INTO #oTable
	(TierDropdownID)
SELECT
	TD.TierDropdownID
FROM Dropdown.TierDropdown TD WITH (NOLOCK)
	JOIN Dropdown.AnalysisCode AC WITH (NOLOCK) ON AC.AnalysisCodeID = TD.DropdownEntityID
		AND TD.TierID = @nInstanceID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
ORDER BY AC.AnalysisCode

UPDATE TD
SET TD.DisplayOrder = T.DisplayOrder
FROM Dropdown.TierDropdown TD
	JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
		AND TD.DisplayOrder = 0
GO

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'ANALYSIS'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'AnalysisCode'
GO
--End file CMT 8859 - 09 - AnalysisCode.sql
GO
--Begin file CMT 8859 - 10 - MetadataType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'MetadataType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('MetadataType','Metadata Types','Dropdown','MetadataType','MetadataTypeID','MetadataType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'MetadataType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: MetadataType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.MetadataType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.MetadataType
		(
		MetadataTypeID int IDENTITY(0,1) NOT NULL,
		MetadataType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'MetadataType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MetadataType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'MetadataTypeID'
EXEC Utility.SetIndexNonClustered 'IX_MetadataType', @cTableName, 'MetadataType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.MetadataType ON
	INSERT INTO Dropdown.MetadataType (MetadataTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.MetadataType OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.MetadataType
	(MetadataType)
SELECT DISTINCT 
	UPPER(LTRIM(MD.Type))
FROM dbo.Metadata MD WITH (NOLOCK)
WHERE MD.Type IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.MetadataType MDT WITH (NOLOCK)
		WHERE MDT.MetadataType = UPPER(LTRIM(MD.Type))
		)
ORDER BY UPPER(LTRIM(MD.Type))
GO		

UPDATE Dropdown.MetadataType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND MetadataTypeID <> 0
GO
--End table: MetadataType

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'MetadataType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT 
	MD.TierID,
	@nDropdownMetadataID,
	MDT.MetadataTypeID
FROM dbo.Metadata MD WITH (NOLOCK)
	JOIN Dropdown.MetadataType MDT WITH (NOLOCK) ON MDT.MetadataType = UPPER(MD.Type)
		AND MDT.MetadataTypeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = MD.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = MDT.MetadataTypeID
			)
ORDER BY MD.TierID, MDT.MetadataTypeID

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.MetadataType MDT WITH (NOLOCK) ON MDT.MetadataTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY MDT.MetadataType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'MetadataType'
GO
--End file CMT 8859 - 10 - MetadataType.sql
GO
--Begin file CMT 8859 - 11 - Metadata.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Metadata')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Metadata','Metadata','Dropdown','Metadata','MetadataID','Metadata',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Metadata'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: Metadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Metadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.Metadata
		(
		MetadataID int IDENTITY(0,1) NOT NULL,
		MetadataTypeID int,
		Metadata varchar(250),
		Description varchar(max),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'Description', 'varchar(max)'

EXEC Utility.SetDefault @cTableName, 'Metadata', ''
EXEC Utility.SetDefault @cTableName, 'MetadataTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Metadata', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'MetadataTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'MetadataID'
EXEC Utility.SetIndexNonClustered 'IX_Metadata', @cTableName, 'Metadata ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WITH (NOLOCK) WHERE MD.MetadataID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.Metadata ON
	INSERT INTO Dropdown.Metadata (MetadataID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.Metadata OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.Metadata
	(Metadata)
SELECT DISTINCT
	LTRIM(MD1.Name)
FROM dbo.Metadata MD1 WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Metadata MD2
	WHERE MD2.Metadata = LTRIM(MD1.Name)
	)
ORDER BY LTRIM(MD1.Name)
GO

UPDATE MD2
SET MD2.Description = MD1.Description
FROM Dropdown.Metadata MD2
	JOIN dbo.Metadata MD1 ON LTRIM(MD1.Name) = MD2.Metadata
		AND MD1.Description IS NOT NULL
		AND MD2.Description IS NULL
GO
	
DECLARE @cMetadata varchar(250)
DECLARE @nMetadataTypeID int

DECLARE oCursor CURSOR FOR
	SELECT 
		MD.Name,
		MDT.MetadataTypeID
	FROM dbo.Metadata MD
		JOIN Dropdown.MetadataType MDT ON MDT.MetadataType = UPPER(MD.Type)
	ORDER BY MD.Name, MD.MDID

OPEN oCursor
FETCH oCursor INTO @cMetadata, @nMetadataTypeID
WHILE @@fetch_status = 0
	BEGIN
	
	IF EXISTS (SELECT 1 FROM Dropdown.Metadata MD WITH (NOLOCK) WHERE Metadata = @cMetadata AND MetadataTypeID = 0)
		BEGIN
		
		UPDATE Dropdown.Metadata
		SET MetadataTypeID = @nMetadataTypeID
		WHERE Metadata = @cMetadata
		
		END
	--ENDIF
	
	FETCH oCursor INTO @cMetadata, @nMetadataTypeID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
		
UPDATE Dropdown.Metadata
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND MetadataID <> 0
GO
--End table: Metadata

--Begin table: CDRMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDRMetadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.CDRMetadata
		(
		CDRMetadataID int IDENTITY(1,1) NOT NULL,
		CDRID int,
		MetadataID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'MetadataID', 0
EXEC Utility.SetDefault @cTableName, 'CDRID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CDRID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'CDRMetadataID'
EXEC Utility.SetIndexClustered 'IX_CDRMetadata', @cTableName, 'CDRID ASC,MetadataID ASC'
GO
--End table: CDRMetadata

--Begin table: LMSMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSMetadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LMSMetadata
		(
		LMSMetadataID int IDENTITY(1,1) NOT NULL,
		LMSID int,
		MetadataID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'MetadataID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSMetadataID'
EXEC Utility.SetIndexClustered 'IX_LMSMetadata', @cTableName, 'LMSID ASC,MetadataID ASC'
GO
--End table: LMSMetadata

EXEC Utility.AddColumn 'ReportToMetadata', 'MetadataID', 'int'
EXEC Utility.SetDefault 'ReportToMetadata', 'MetadataID', 0
EXEC Utility.SetColumnNotNull 'ReportToMetadata', 'MetadataID', 'int'
GO

UPDATE RTMD
SET MetadataID = MD2.MetadataID
FROM dbo.ReportToMetadata RTMD
	JOIN dbo.Metadata MD1 ON MD1.MDID = RTMD.MDID
	JOIN Dropdown.Metadata MD2 ON MD2.Metadata = MD1.Name
WHERE MD2.MetadataID = 0
GO

INSERT INTO dbo.CDRMetadata
	(CDRID,MetadataID)
SELECT DISTINCT
	RTMD.ReportID,
	RTMD.MetadataID
FROM dbo.ReportToMetadata RTMD
WHERE RTMD.ReportType = 'CDR'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.CDRMetadata CMD
		WHERE CMD.CDRID = RTMD.ReportID
			AND CMD.MetadataID = RTMD.MetadataID
		)
	AND EXISTS
		(
		SELECT 1
		FROM dbo.CDR C
		WHERE C.CDRID = RTMD.ReportID
		)
GO

INSERT INTO dbo.LMSMetadata
	(LMSID,MetadataID)
SELECT DISTINCT
	RTMD.ReportID,
	RTMD.MetadataID
FROM dbo.ReportToMetadata RTMD
WHERE RTMD.ReportType = 'LMS'
	AND RTMD.MetadataID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.LMSMetadata LMD
		WHERE LMD.LMSID = RTMD.ReportID
			AND LMD.MetadataID = RTMD.MetadataID
		)
	AND EXISTS
		(
		SELECT 1
		FROM dbo.LMS L
		WHERE L.LMSID = RTMD.ReportID
		)
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'Metadata'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	MD1.TierID,
	@nDropdownMetadataID,
	MD2.MetadataID
FROM Dropdown.Metadata MD2 WITH (NOLOCK)
	JOIN dbo.Metadata MD1 WITH (NOLOCK) ON LTRIM(MD1.Name) = MD2.Metadata
WHERE MD2.MetadataID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = MD1.TierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
			AND TD.DropdownEntityID = MD2.MetadataID
		)
ORDER BY MD2.MetadataID

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.Metadata MD WITH (NOLOCK) ON MD.MetadataID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY MD.Metadata

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

--Begin table: UserSubscription
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.UserSubscription') AND O.type in (N'U')) 
	DROP TABLE dbo.UserSubscription

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.UserSubscription'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	DROP TABLE Dropdown.UserSubscription
	
CREATE TABLE Dropdown.UserSubscription
	(
	UserSubscriptionID int IDENTITY(1,1) NOT NULL,
	JLLISUserID int,
	DropdownEntityID int,
	DropdownMetadataCode varchar(50),
	Action varchar(50)
	) ON [PRIMARY]

EXEC Utility.SetDefault @cTableName, 'DropdownEntityID', 0
EXEC Utility.SetDefault @cTableName, 'JLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Action', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownEntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataCode', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'JLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'UserSubscriptionID'
EXEC Utility.SetIndexClustered 'IX_UserSubscription', @cTableName, 'JLLISUserID ASC,DropdownMetadataCode ASC,Action ASC,DropdownEntityID ASC'
GO
--End table: UserSubscription

INSERT INTO Dropdown.UserSubscription
	(JLLISUserID,DropdownEntityID,DropdownMetadataCode,Action)
SELECT 
	TU.JLLISUserID,
	MD2.MetadataID,
	'Metadata',
	'OnLessonSubmit'
FROM dbo.MetadataUser MDU WITH (NOLOCK)
	JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.TierUserID = MDU.UserID
		AND TU.TierID = 
			(
			SELECT 
				T.InstanceID
			FROM JLLIS.dbo.Tier T WITH (NOLOCK)
			WHERE T.TierID = 
				(
				SELECT TOP 1
					C.OriginatingTierID
				FROM dbo.CDR C WITH (NOLOCK)
				WHERE C.OriginatingTierID > 0
				)
			)
	JOIN dbo.Metadata MD1 WITH (NOLOCK) ON MD1.MDID = MDU.MDID
		AND MDU.UserID IS NOT NULL
		AND MDU.UserID > 0
	JOIN Dropdown.Metadata MD2 WITH (NOLOCK) ON MD2.Metadata = LTRIM(MD1.Name)
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.UserSubscription US
			WHERE US.Action = 'OnLessonSubmit'
				AND US.DropdownEntityID = MD2.MetadataID
				AND US.DropdownMetadataCode = 'Metadata'
				AND US.JLLISUserID = TU.JLLISUserID
			)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.Title = 'DROPDOWN SUBSCRIPTIONS')
	BEGIN
	
	UPDATE MI1
	SET MI1.Level = MI1.Level + 1
	FROM dbo.MenuItem MI1
	WHERE MI1.ParentID = 
		(
		SELECT MI2.ParentID
		FROM dbo.MenuItem MI2 WITH (NOLOCK)
		WHERE MI2.Title = 'DROPDOWNS'
		)
		AND MI1.Level >
			(
			SELECT MI3.Level
			FROM dbo.MenuItem MI3 WITH (NOLOCK)
			WHERE MI3.Title = 'DROPDOWNS'
			)
		
	INSERT INTO dbo.MenuItem 
		(MenuID,ParentID,Level,Title,SecLev,Link,type)
	SELECT
		MI.MenuID,
		MI.ParentID,
		MI.Level + 1,
		'DROPDOWN SUBSCRIPTIONS',
		6000,
		'admin/usersubscription/',
		'yes'
	FROM dbo.MenuItem MI WITH (NOLOCK)
	WHERE MI.Title = 'DROPDOWNS'

	END
--ENDIF

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'METADATA'
GO

DECLARE @nMenuID int
DECLARE @nParentMenuItemID int

SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Title = 'Default' AND M.Type = 'Logged In')
SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SITE MANAGEMENT' AND MI.MenuID = @nMenuID)
	
IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SETUP')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'SETUP',NULL,NULL,'index.cfm?disp=../admin/setup.cfm&menudisp=adminmenu.cfm','yes')
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Metadata'
GO
--End file CMT 8859 - 11 - Metadata.sql
GO
--Begin file CMT 8859 - 12 - Event Location.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'EventLocation')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('EventLocation','Event Locations','Dropdown','EventLocation','EventLocationID','EventLocation',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'EventLocation'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: EventLocation
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.EventLocation'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.EventLocation
		(
		EventLocationID int IDENTITY(0,1) NOT NULL,
		EventLocation varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EventLocation', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EventLocation', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventLocationID'
EXEC Utility.SetIndexNonClustered 'IX_EventType', @cTableName, 'EventLocation ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.EventLocation T WITH (NOLOCK) WHERE T.EventLocationID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.EventLocation ON
	INSERT INTO Dropdown.EventLocation (EventLocationID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.EventLocation OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.EventLocation
	(EventLocation)
SELECT DISTINCT 
	LTRIM(OE.Location)
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE OE.Location IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.EventLocation EL WITH (NOLOCK)
		WHERE EL.EventLocation = LTRIM(OE.Location)
		)
ORDER BY LTRIM(OE.Location)
GO		
		
UPDATE Dropdown.EventLocation
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventLocationID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'EventLocation'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	EL.EventLocationID
FROM Dropdown.EventLocation EL WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.Location) = EL.EventLocation
		AND EL.EventLocationID > 0
		AND OE.Location IS NOT NULL
		AND LEN(LTRIM(OE.Location)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = EL.EventLocationID
			)
ORDER BY EL.EventLocationID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.EventLocation EL WITH (NOLOCK) ON EL.EventLocationID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY EL.EventLocation

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'EventLocation'
GO
--End file CMT 8859 - 12 - Event Location.sql
GO
--Begin file CMT 8859 - 13 - Event Sponsor.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'EventSponsor')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('EventSponsor','Event Sponsors','Dropdown','EventSponsor','EventSponsorID','EventSponsor',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'EventSponsor'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: EventSponsor
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.EventSponsor'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.EventSponsor
		(
		EventSponsorID int IDENTITY(0,1) NOT NULL,
		EventSponsor varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EventSponsor', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EventSponsor', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventSponsorID'
EXEC Utility.SetIndexNonClustered 'IX_EventType', @cTableName, 'EventSponsor ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.EventSponsor T WITH (NOLOCK) WHERE T.EventSponsorID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.EventSponsor ON
	INSERT INTO Dropdown.EventSponsor (EventSponsorID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.EventSponsor OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.EventSponsor
	(EventSponsor)
SELECT DISTINCT 
	LTRIM(OE.Sponsor)
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE OE.Sponsor IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.EventSponsor ES WITH (NOLOCK)
		WHERE ES.EventSponsor = LTRIM(OE.Sponsor)
		)
ORDER BY LTRIM(OE.Sponsor)
GO		
		
UPDATE Dropdown.EventSponsor
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventSponsorID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'EventSponsor'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	ES.EventSponsorID
FROM Dropdown.EventSponsor ES WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.Sponsor) = ES.EventSponsor
		AND ES.EventSponsorID > 0
		AND OE.Sponsor IS NOT NULL
		AND LEN(LTRIM(OE.Sponsor)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = ES.EventSponsorID
			)
ORDER BY ES.EventSponsorID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.EventSponsor ES WITH (NOLOCK) ON ES.EventSponsorID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY ES.EventSponsor

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'EventSponsor'
GO
--End file CMT 8859 - 13 - Event Sponsor.sql
GO
--Begin file CMT 8859 - 14 - Event Type.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'EventType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('EventType','Event Types','Dropdown','EventType','EventTypeID','EventType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'EventType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: EventType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.EventType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.EventType
		(
		EventTypeID int IDENTITY(0,1) NOT NULL,
		EventType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EventType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EventType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventTypeID'
EXEC Utility.SetIndexNonClustered 'IX_EventType', @cTableName, 'EventType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.EventType T WITH (NOLOCK) WHERE T.EventTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.EventType ON
	INSERT INTO Dropdown.EventType (EventTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.EventType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'OperationType'
WHERE Category = 'OperationType_Old'
GO

INSERT INTO Dropdown.EventType
	(EventType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS EventType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'OperationType'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.EventType ET WITH (NOLOCK)
		WHERE ET.EventType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(OE.OpExType) AS EventType
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.EventType ET WITH (NOLOCK)
	WHERE ET.EventType = LTRIM(OE.OpExType)
	)
	AND OE.OpExType IS NOT NULL

ORDER BY EventType
GO		
		
UPDATE Dropdown.EventType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'EventType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	ET.EventTypeID
FROM Dropdown.EventType ET WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.OpExType) = ET.EventType
		AND ET.EventTypeID > 0
		AND OE.OpExType IS NOT NULL
		AND LEN(LTRIM(OE.OpExType)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = ET.EventTypeID
			)
ORDER BY ET.EventTypeID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.EventType ET WITH (NOLOCK) ON ET.EventTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY ET.EventType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'EventType'
GO
--End file CMT 8859 - 14 - Event Type.sql
GO
--Begin file CMT 8859 - 15 - Event.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Event')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Event','Events','Dropdown','Event','EventID','Event',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Event'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: Binder
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.Binder'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: Binder

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: PortVisit
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'EventID'
SET @cTableName = 'dbo.PortVisit'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit

--Begin table: Event
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Event'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.Event
		(
		EventID int IDENTITY(0,1) NOT NULL,
		EventLocationID int,
		EventSponsorID int,
		EventTypeID int,
		Event varchar(250),
		StartDate datetime,
		EndDate datetime,
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetDefault @cTableName, 'Event', ''
EXEC Utility.SetDefault @cTableName, 'EventLocationID', 0
EXEC Utility.SetDefault @cTableName, 'EventSponsorID', 0
EXEC Utility.SetDefault @cTableName, 'EventTypeID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Event', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'EventLocationID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EventSponsorID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EventTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventID'
EXEC Utility.SetIndexNonClustered 'IX_Event', @cTableName, 'Event ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.Event T WITH (NOLOCK) WHERE T.EventID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.Event ON
	INSERT INTO Dropdown.Event (EventID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.Event OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.Event
	(Event)
SELECT DISTINCT 
	LTRIM(OE.OpExName) AS OpExName
FROM dbo.OpEx OE WITH (NOLOCK)
WHERE OE.OpExName IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.Event EL WITH (NOLOCK)
		WHERE EL.Event = LTRIM(OE.OpExName)
		)

UNION

SELECT DISTINCT
	LTRIM(B.EventName) AS OpExName
FROM dbo.Binder B WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Event E WITH (NOLOCK)
	WHERE E.Event = LTRIM(B.EventName)
	)
	AND B.EventName IS NOT NULL

UNION

SELECT DISTINCT
	LTRIM(L.EventOpEx) AS OpExName
FROM dbo.LMS L WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Event E WITH (NOLOCK)
	WHERE E.Event = LTRIM(L.EventOpEx)
	)
	AND L.EventOpEx IS NOT NULL

ORDER BY OpExName
GO		
		
UPDATE E
SET E.EventLocationID = EL.EventLocationID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventLocation EL ON EL.EventLocation = OE.Location
GO

UPDATE E
SET E.EventSponsorID = ES.EventSponsorID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventSponsor ES ON ES.EventSponsor = OE.Sponsor
GO

UPDATE E
SET E.EventTypeID = ET.EventTypeID
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
	JOIN Dropdown.EventType ET ON ET.EventType = OE.OpExType
GO

UPDATE E
SET 
	E.StartDate = OE.StartDate,
	E.EndDate = OE.EndDate
FROM Dropdown.Event E
	JOIN dbo.OpEx OE ON LTRIM(OE.OpExName) = E.Event
GO

UPDATE Dropdown.Event
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND EventID > 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'Event'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	OE.TierID,
	@nDropdownMetadataID,
	E.EventID
FROM Dropdown.Event E WITH (NOLOCK)
	JOIN dbo.OpEx OE WITH (NOLOCK) ON LTRIM(OE.OpexName) = E.Event
		AND E.EventID > 0
		AND OE.OpexName IS NOT NULL
		AND LEN(LTRIM(OE.OpexName)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = OE.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = E.EventID
			)
ORDER BY E.EventID, OE.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.Event E WITH (NOLOCK) ON E.EventID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY E.Event

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.EventID = E.EventID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.Event E ON E.Event = PV.EventOpex
			AND PV.EventID = 0

	END
--ENDIF
GO

UPDATE L
SET L.EventID = E.EventID 
FROM dbo.LMS L
	JOIN Dropdown.Event E ON E.Event = L.EventOpex
		AND L.EventID = 0
GO

DELETE 
FROM dbo.MenuItem 
WHERE Title = 'EVENTS'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Event'
GO
--End file CMT 8859 - 15 - Event.sql
GO
--Begin file CMT 8859 - 16 - HullType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'HullType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('HullType','Hull Types','Dropdown','HullType','HullTypeID','HullType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'HullType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: PortVisit
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @cColumnName varchar(250)
	DECLARE @cTableName varchar(250)

	SET @cColumnName = 'HullTypeID'
	SET @cTableName = 'dbo.PortVisit'

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit
	
--Begin table: HullType
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cTableName varchar(250)

	SET @cTableName = 'Dropdown.HullType'

	IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
		BEGIN

		CREATE TABLE Dropdown.HullType
			(
			HullTypeID int IDENTITY(0,1) NOT NULL,
			HullType varchar(250),
			OriginatingTierID int,
			UpdateJLLISUserID int,
			IsActive bit
			) ON [PRIMARY]
	
		END
	--ENDIF

	EXEC Utility.DropConstraintsAndIndexes @cTableName

	EXEC Utility.SetDefault @cTableName, 'HullType', ''
	EXEC Utility.SetDefault @cTableName, 'IsActive', 1
	EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
	EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

	EXEC Utility.SetColumnNotNull @cTableName, 'HullType', 'varchar(250)'
	EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
	EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
	EXEC Utility.SetPrimaryKeyClustered @cTableName, 'HullTypeID'
	EXEC Utility.SetIndexNonClustered 'IX_HullType', @cTableName, 'HullType ASC'
	EXEC Utility.EnableAuditing @cTableName, 0

	END
--ENDIF
GO
--End table: HullType
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM Dropdown.HullType HT WITH (NOLOCK) WHERE HT.HullTypeID = 0)
		BEGIN
		
		SET IDENTITY_INSERT Dropdown.HullType ON
		INSERT INTO Dropdown.HullType (HullTypeID) VALUES (0)
		SET IDENTITY_INSERT Dropdown.HullType OFF
	
		END
	--ENDIF

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	INSERT INTO Dropdown.HullType
		(HullType)
	SELECT DISTINCT 
		LTRIM(PV.HullType)
	FROM dbo.PortVisit PV WITH (NOLOCK)
	WHERE PV.HullType IS NOT NULL
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.HullType HT WITH (NOLOCK)
			WHERE HT.HullType = LTRIM(PV.HullType)
			)
	ORDER BY LTRIM(PV.HullType)

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.HullTypeID = HT.HullTypeID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.HullType HT ON HT.HullType = PV.HullType
			AND PV.HullTypeID = 0
		
	UPDATE Dropdown.HullType
	SET OriginatingTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	WHERE OriginatingTierID = 0
		AND HullTypeID <> 0

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @nDropdownMetadataID int
	DECLARE @nTierID int
	
	SET @nDropdownMetadataID = 
		(
		SELECT DMD.DropdownMetadataID
		FROM JLLIS.Dropdown.DropdownMetadata DMD
		WHERE DMD.DropdownMetadataCode = 'HullType'
		)
	
	SET @nTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	
	INSERT INTO Dropdown.TierDropdown
		(TierID, DropdownMetadataID, DropdownEntityID)
	SELECT DISTINCT
		@nTierID,
		@nDropdownMetadataID,
		HT.HullTypeID
	FROM Dropdown.HullType HT WITH (NOLOCK)
		JOIN dbo.PortVisit PV WITH (NOLOCK) ON LTRIM(PV.HullType) = HT.HullType
			AND HT.HullTypeID > 0
			AND PV.HullType IS NOT NULL
			AND LEN(LTRIM(PV.HullType)) > 0
			AND NOT EXISTS
				(
				SELECT 1
				FROM Dropdown.TierDropdown TD
				WHERE TD.TierID = @nTierID
					AND TD.DropdownMetadataID = @nDropdownMetadataID
					AND TD.DropdownEntityID = HT.HullTypeID
				)
	ORDER BY HT.HullTypeID

	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
		DROP TABLE #oTable

	CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)
	
	DECLARE oCursor CURSOR FOR
		SELECT DISTINCT 
			TD.TierID
		FROM Dropdown.TierDropdown TD
		WHERE TD.DropdownMetadataID = @nDropdownMetadataID

	OPEN oCursor
	FETCH oCursor INTO @nTierID
	WHILE @@fetch_status = 0
		BEGIN
	
		TRUNCATE TABLE #oTable
		
		INSERT INTO #oTable
			(TierDropdownID)
		SELECT
			TD.TierDropdownID
		FROM Dropdown.TierDropdown TD WITH (NOLOCK)
			JOIN Dropdown.HullType HT WITH (NOLOCK) ON HT.HullTypeID = TD.DropdownEntityID
				AND TD.TierID = @nTierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
		ORDER BY HT.HullType
	
		UPDATE TD
		SET TD.DisplayOrder = T.DisplayOrder
		FROM Dropdown.TierDropdown TD
			JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
				AND TD.DisplayOrder = 0
	
		FETCH oCursor INTO @nTierID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO
	
UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'HullType'
GO
--End file CMT 8859 - 16 - HullType.sql
GO
--Begin file CMT 8859 - 17 - Port.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Port')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('Port','Ports','Dropdown','Port','PortID','Port',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'Port'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: PortVisit
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @cColumnName varchar(250)
	DECLARE @cTableName varchar(250)

	SET @cColumnName = 'PortID'
	SET @cTableName = 'dbo.PortVisit'

	EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
	EXEC Utility.SetDefault @cTableName, @cColumnName, 0
	EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'

	END
--ENDIF
GO
--End table: PortVisit

--Begin table: Port
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cTableName varchar(250)

	SET @cTableName = 'Dropdown.Port'

	IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
		BEGIN

		CREATE TABLE Dropdown.Port
			(
			PortID int IDENTITY(0,1) NOT NULL,
			Port varchar(250),
			CountryID int,
			OriginatingTierID int,
			UpdateJLLISUserID int,
			IsActive bit
			) ON [PRIMARY]
	
		END
	--ENDIF

	EXEC Utility.DropConstraintsAndIndexes @cTableName

	EXEC Utility.SetDefault @cTableName, 'CountryID', 0
	EXEC Utility.SetDefault @cTableName, 'IsActive', 1
	EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
	EXEC Utility.SetDefault @cTableName, 'Port', ''
	EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

	EXEC Utility.SetColumnNotNull @cTableName, 'CountryID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
	EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
	EXEC Utility.SetColumnNotNull @cTableName, 'Port', 'varchar(250)'
	EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
	EXEC Utility.SetPrimaryKeyClustered @cTableName, 'PortID'
	EXEC Utility.SetIndexNonClustered 'IX_Port', @cTableName, 'Port ASC'
	EXEC Utility.EnableAuditing @cTableName, 0

	END
--ENDIF
GO
--End table: Port
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM Dropdown.Port HT WITH (NOLOCK) WHERE HT.PortID = 0)
		BEGIN
		
		SET IDENTITY_INSERT Dropdown.Port ON
		INSERT INTO Dropdown.Port (PortID) VALUES (0)
		SET IDENTITY_INSERT Dropdown.Port OFF
	
		END
	--ENDIF

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	INSERT INTO Dropdown.Port
		(Port)
	SELECT DISTINCT 
		LTRIM(PV.Port)
	FROM dbo.PortVisit PV WITH (NOLOCK)
	WHERE PV.Port IS NOT NULL
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.Port HT WITH (NOLOCK)
			WHERE HT.Port = LTRIM(PV.Port)
			)
	ORDER BY LTRIM(PV.Port)

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE dbo.Port
	SET CountryCode = 'GB'
	WHERE CountryCode = 'UK'

	UPDATE dbo.PortCountry
	SET CountryCode = 'GB'
	WHERE CountryCode = 'UK'

	UPDATE dbo.Port
	SET CountryCode = 'IO'
	WHERE CountryCode = 'DG'

	UPDATE dbo.PortCountry
	SET CountryCode = 'IO'
	WHERE CountryCode = 'DG'

	UPDATE dbo.Port
	SET CountryCode = 'ME'
	WHERE CountryCode = 'YU'

	UPDATE dbo.PortCountry
	SET CountryCode = 'ME'
	WHERE CountryCode = 'YU'

	UPDATE dbo.Port
	SET CountryCode = 'MQ'
	WHERE CountryCode = 'FQ'

	UPDATE dbo.PortCountry
	SET CountryCode = 'MQ'
	WHERE CountryCode = 'FQ'

	UPDATE dbo.Port
	SET CountryCode = 'NA'
	WHERE CountryCode = 'NH'

	UPDATE dbo.PortCountry
	SET CountryCode = 'NA'
	WHERE CountryCode = 'NH'

	UPDATE dbo.Port
	SET CountryCode = 'NO'
	WHERE CountryCode = 'NOr'

	UPDATE dbo.PortCountry
	SET CountryCode = 'NO'
	WHERE CountryCode = 'NOr'

	UPDATE dbo.Port
	SET CountryCode = 'PT'
	WHERE CountryCode = 'ZS'

	UPDATE dbo.PortCountry
	SET CountryCode = 'PT'
	WHERE CountryCode = 'ZS'

	UPDATE dbo.Port
	SET CountryCode = 'TL'
	WHERE CountryCode = 'TP'

	UPDATE dbo.PortCountry
	SET CountryCode = 'TL'
	WHERE CountryCode = 'TP'

	UPDATE P1
	SET P1.CountryID = C.CountryID
	FROM Dropdown.Port P1
		JOIN dbo.Port P2 WITH (NOLOCK) ON P2.PortName = P1.Port
  	JOIN JLLIS.Dropdown.Country C ON C.IsoCode2 = P2.CountryCode

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.PortID = HT.PortID 
	FROM dbo.PortVisit PV
		JOIN Dropdown.Port HT ON HT.Port = PV.Port
			AND PV.PortID = 0
		
	UPDATE Dropdown.Port
	SET OriginatingTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	WHERE OriginatingTierID = 0
		AND PortID <> 0

	END
--ENDIF
GO
	
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN
	
	DECLARE @nDropdownMetadataID int
	DECLARE @nTierID int
	
	SET @nDropdownMetadataID = 
		(
		SELECT DMD.DropdownMetadataID
		FROM JLLIS.Dropdown.DropdownMetadata DMD
		WHERE DMD.DropdownMetadataCode = 'Port'
		)
	
	SET @nTierID = 
		(
		SELECT 
			T.InstanceID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK)
		WHERE T.TierID = 
			(
			SELECT TOP 1
				C.OriginatingTierID
			FROM dbo.CDR C WITH (NOLOCK)
			WHERE C.OriginatingTierID > 0
			)
		)
	
	INSERT INTO Dropdown.TierDropdown
		(TierID, DropdownMetadataID, DropdownEntityID)
	SELECT DISTINCT
		@nTierID,
		@nDropdownMetadataID,
		HT.PortID
	FROM Dropdown.Port HT WITH (NOLOCK)
		JOIN dbo.PortVisit PV WITH (NOLOCK) ON LTRIM(PV.Port) = HT.Port
			AND HT.PortID > 0
			AND PV.Port IS NOT NULL
			AND LEN(LTRIM(PV.Port)) > 0
			AND NOT EXISTS
				(
				SELECT 1
				FROM Dropdown.TierDropdown TD
				WHERE TD.TierID = @nTierID
					AND TD.DropdownMetadataID = @nDropdownMetadataID
					AND TD.DropdownEntityID = HT.PortID
				)
	ORDER BY HT.PortID

	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
		DROP TABLE #oTable

	CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)
	
	DECLARE oCursor CURSOR FOR
		SELECT DISTINCT 
			TD.TierID
		FROM Dropdown.TierDropdown TD
		WHERE TD.DropdownMetadataID = @nDropdownMetadataID

	OPEN oCursor
	FETCH oCursor INTO @nTierID
	WHILE @@fetch_status = 0
		BEGIN
	
		TRUNCATE TABLE #oTable
		
		INSERT INTO #oTable
			(TierDropdownID)
		SELECT
			TD.TierDropdownID
		FROM Dropdown.TierDropdown TD WITH (NOLOCK)
			JOIN Dropdown.Port HT WITH (NOLOCK) ON HT.PortID = TD.DropdownEntityID
				AND TD.TierID = @nTierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
		ORDER BY HT.Port
	
		UPDATE TD
		SET TD.DisplayOrder = T.DisplayOrder
		FROM Dropdown.TierDropdown TD
			JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
				AND TD.DisplayOrder = 0
	
		FETCH oCursor INTO @nTierID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'NLD'),
		Port = 'Curacao, Netherlands Antilles'
	WHERE Port = 'Curacoa, Netherlands Antilles'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'GNQ')
	WHERE Port = 'Malabo'
	
	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'ZAF'),
		Port = 'Port Owen'
	WHERE Port = 'Port Owendo'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'JAM')
	WHERE Port = 'PORT ROYAL'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'USA')
	WHERE Port = 'Rockland, Maine'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'KHM')
	WHERE Port = 'Sihanoukville'
	
	UPDATE Dropdown.Port
	SET CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'MNP')
	WHERE Port = 'Tanapag Harbor'
	
	UPDATE Dropdown.Port
	SET 
		CountryID = (SELECT C.CountryID FROM JLLIS.Dropdown.Country C WITH (NOLOCK) WHERE C.ISOCode3 = 'PAN'),
		Port = 'VNB'
	WHERE Port = 'VBN'

	END
--ENDIF
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'Port'
GO
--End file CMT 8859 - 17 - Port.sql
GO
--Begin file CMT 8859 - 18 - CDRCreatorQualifier.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'CDRCreatorQualifier')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('CDRCreatorQualifier','CDR Creator Qualifiers','Dropdown','CDRCreatorQualifier','CDRCreatorQualifierID','CDRCreatorQualifier',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'CDRCreatorQualifier'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'CDRCreatorQualifierID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: CDRCreatorQualifier
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CDRCreatorQualifier'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CDRCreatorQualifier
		(
		CDRCreatorQualifierID int IDENTITY(0,1) NOT NULL,
		CDRCreatorQualifier varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CDRCreatorQualifier', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CDRCreatorQualifier', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRCreatorQualifierID'
EXEC Utility.SetIndexNonClustered 'IX_CDRCreatorQualifier', @cTableName, 'CDRCreatorQualifier ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: CDRCreatorQualifier

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK) WHERE CQ.CDRCreatorQualifierID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.CDRCreatorQualifier ON
	INSERT INTO Dropdown.CDRCreatorQualifier (CDRCreatorQualifierID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.CDRCreatorQualifier OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CDR_CreatorQualifier'
WHERE Category = 'CDR_CreatorQualifier_Old'
GO

INSERT INTO Dropdown.CDRCreatorQualifier
	(CDRCreatorQualifier)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS CDRCreatorQualifier
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CDR_CreatorQualifier'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
		WHERE CQ.CDRCreatorQualifier = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(C.CreatorQualifier) AS CDRCreatorQualifier
FROM dbo.CDR C WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
	WHERE CQ.CDRCreatorQualifier = LTRIM(C.CreatorQualifier)
	)
	AND C.CreatorQualifier IS NOT NULL

ORDER BY CDRCreatorQualifier
GO

UPDATE C 
SET C.CDRCreatorQualifierID = CQ.CDRCreatorQualifierID 
FROM dbo.CDR C 
	JOIN Dropdown.CDRCreatorQualifier CQ ON CQ.CDRCreatorQualifier = C.CreatorQualifier
		AND C.CDRCreatorQualifierID = 0
GO
		
UPDATE Dropdown.CDRCreatorQualifier
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND CDRCreatorQualifierID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'CDRCreatorQualifier'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	CQ.CDRCreatorQualifierID
FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = CQ.CDRCreatorQualifier
		AND D.Category = 'CDR_CreatorQualifier'
		AND CQ.CDRCreatorQualifierID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = CQ.CDRCreatorQualifierID
			)
ORDER BY CQ.CDRCreatorQualifierID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK) ON CQ.CDRCreatorQualifierID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CQ.CDRCreatorQualifier

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CDR_CreatorQualifier_Old'
WHERE Category = 'CDR_CreatorQualifier'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'CDRCreatorQualifier'
GO
--End file CMT 8859 - 18 - CDRCreatorQualifier.sql
GO
--Begin file CMT 8859 - 19 - CDRFileView.sql
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.CDRFileView'))
	DROP VIEW dbo.CDRFileView
GO

CREATE VIEW dbo.CDRFileView
AS
SELECT
	C.CDRID,
	C.City,
	C.Classification,
	C.Condition,
	C.CountryID,
	C.CreationDate,
	C.DeclassifyOn,
	C.Dissemination,
	C.DocCountryDropDownID,
	C.DocDate,
	C.EMail,
	C.FirstName + ' ' + C.LastName AS AuthorName,
	C.FirstName,
	C.Jointcdr,
	C.Keywords,
	C.LastName,
	C.OriginatorCountryID,
	C.OverallCaveat,
	C.Phone,
	C.Place,
	C.Province,
	C.ReleasableTo,
	C.Status,
	C.Subtitle,
	C.Summary,
	C.ThumbHeight,
	C.Thumbnail,
	C.ThumbWidth,
	C.Title,
	C.UpdateDate,
	CC.CDRCategory AS Category,
	CF.CDRFileID, 
	CF.CDRID AS CDRFile_CDRID, 
	CF.Condition AS CDRFile_Condition, 
	CF.FileName, 
	CF.Status AS CDRFile_Status,
	CO.CountryName AS Country,
	CQ.CDRCreatorQualifier AS CreatorQualifier,
	CT.CDRType AS Type,
	T.TierName AS Organization
FROM dbo.CDR C WITH (NOLOCK)
	JOIN dbo.CDRFile CF WITH (NOLOCK) ON CF.CDRID = C.CDRID
		AND C.JointCDR = 'C'
		AND C.Status = 'Active'
		AND CF.Status = 'Active'
	JOIN JLLIS.dbo.Tier T ON T.TierID = C.OriginatingTierID
	JOIN JLLIS.dbo.Country CO ON CO.CountryID = C.CountryID
	JOIN Dropdown.CDRCategory CC ON CC.CDRCategoryID = C.CDRCategoryID
	JOIN Dropdown.CDRCreatorQualifier CQ ON CQ.CDRCreatorQualifierID = C.CDRCreatorQualifierID
	JOIN Dropdown.CDRType CT ON CT.CDRTypeID = C.CDRTypeID
GO

--End file CMT 8859 - 19 - CDRFileView.sql
GO
--Begin file CMT 8859 - 20 - LessonIssue.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'LessonIssue')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('LessonIssue','Lesson Issues','Dropdown','LessonIssue','LessonIssueID','LessonIssue',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'LessonIssue'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LessonIssueID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: LessonIssue
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.LessonIssue'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.LessonIssue
		(
		LessonIssueID int IDENTITY(0,1) NOT NULL,
		LessonIssue varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LessonIssue', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LessonIssue', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LessonIssueID'
EXEC Utility.SetIndexNonClustered 'IX_LessonIssue', @cTableName, 'LessonIssue ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: LessonIssue

IF NOT EXISTS (SELECT 1 FROM Dropdown.LessonIssue LI WITH (NOLOCK) WHERE LI.LessonIssueID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.LessonIssue ON
	INSERT INTO Dropdown.LessonIssue (LessonIssueID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.LessonIssue OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CI_Issues'
WHERE Category = 'CI_Issues_Old'
GO

INSERT INTO Dropdown.LessonIssue
	(LessonIssue)
SELECT DISTINCT 
	LTRIM(D.DisplayText)
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CI_Issues'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.LessonIssue LI WITH (NOLOCK)
		WHERE LI.LessonIssue = LTRIM(D.DisplayText)
		)
ORDER BY LTRIM(D.DisplayText)
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name='Issues')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.LessonIssue (LessonIssue) SELECT DISTINCT LTRIM(L.Issues) FROM dbo.LMS L WITH (NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM Dropdown.LessonIssue LI WITH (NOLOCK) WHERE LI.LessonIssue = LTRIM(L.Issues)) AND L.Issues IS NOT NULL ORDER BY LTRIM(L.Issues)'

	EXEC (@cSQL)
	
	SET @cSQL = 'UPDATE L SET L.LessonIssueID = LI.LessonIssueID FROM dbo.LMS L JOIN Dropdown.LessonIssue LI ON LI.LessonIssue = L.Issues AND L.LessonIssueID = 0'

	EXEC (@cSQL)

	END
--ENDIF
GO		

UPDATE Dropdown.LessonIssue
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND LessonIssueID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'LessonIssue'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	LI.LessonIssueID
FROM Dropdown.LessonIssue LI WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = LI.LessonIssue
		AND D.Category = 'CI_Issues'
		AND LI.LessonIssueID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = LI.LessonIssueID
			)
ORDER BY LI.LessonIssueID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.LessonIssue LI WITH (NOLOCK) ON LI.LessonIssueID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY LI.LessonIssue

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CI_Issues_Old'
WHERE Category = 'CI_Issues'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'LessonIssue'
GO
--End file CMT 8859 - 20 - LessonIssue.sql
GO
--Begin file CMT 8859 - 21 - LessonTrainingCourse.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'LessonTrainingCourse')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('LessonTrainingCourse','Lesson Training Courses','Dropdown','LessonTrainingCourse','LessonTrainingCourseID','LessonTrainingCourse',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'LessonTrainingCourse'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LessonTrainingCourseID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: LessonTrainingCourse
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.LessonTrainingCourse'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.LessonTrainingCourse
		(
		LessonTrainingCourseID int IDENTITY(0,1) NOT NULL,
		LessonTrainingCourse varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LessonTrainingCourse', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LessonTrainingCourse', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LessonTrainingCourseID'
EXEC Utility.SetIndexNonClustered 'IX_LessonTrainingCourse', @cTableName, 'LessonTrainingCourse ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: LessonTrainingCourse

IF NOT EXISTS (SELECT 1 FROM Dropdown.LessonTrainingCourse LTC WITH (NOLOCK) WHERE LTC.LessonTrainingCourseID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.LessonTrainingCourse ON
	INSERT INTO Dropdown.LessonTrainingCourse (LessonTrainingCourseID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.LessonTrainingCourse OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'Training_Course'
WHERE Category = 'Training_Course_Old'
GO

INSERT INTO Dropdown.LessonTrainingCourse
	(LessonTrainingCourse)
SELECT DISTINCT 
	LTRIM(D.DisplayText)
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'Training_Course'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.LessonTrainingCourse LTC WITH (NOLOCK)
		WHERE LTC.LessonTrainingCourse = LTRIM(D.DisplayText)
		)
ORDER BY LTRIM(D.DisplayText)
GO		

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name='TrainingCourse')
	BEGIN
	
	DECLARE @cSQL varchar(max)

	SET @cSQL = 'INSERT INTO Dropdown.LessonTrainingCourse (LessonTrainingCourse) SELECT DISTINCT LTRIM(L.TrainingCourse) FROM dbo.LMS L WITH (NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM Dropdown.LessonTrainingCourse LTC WITH (NOLOCK) WHERE LTC.LessonTrainingCourse = LTRIM(L.TrainingCourse)) AND L.TrainingCourse IS NOT NULL ORDER BY LTRIM(L.TrainingCourse)'	

	EXEC (@cSQL)

	SET @cSQL = 'UPDATE L SET L.LessonTrainingCourseID = LTC.LessonTrainingCourseID FROM dbo.LMS L JOIN Dropdown.LessonTrainingCourse LTC ON LTC.LessonTrainingCourse = L.TrainingCourse AND L.LessonTrainingCourseID = 0'	

	EXEC (@cSQL)	

	END
--ENDIF
		
UPDATE Dropdown.LessonTrainingCourse
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND LessonTrainingCourseID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'LessonTrainingCourse'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	LTC.LessonTrainingCourseID
FROM Dropdown.LessonTrainingCourse LTC WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = LTC.LessonTrainingCourse
		AND D.Category = 'Training_Course'
		AND LTC.LessonTrainingCourseID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = LTC.LessonTrainingCourseID
			)
ORDER BY LTC.LessonTrainingCourseID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.LessonTrainingCourse LTC WITH (NOLOCK) ON LTC.LessonTrainingCourseID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY LTC.LessonTrainingCourse

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'Training_Course_Old'
WHERE Category = 'Training_Course'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'LessonTrainingCourse'
GO
--End file CMT 8859 - 21 - LessonTrainingCourse.sql
GO
--Begin file CMT 8859 - 22 - LessonType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'LessonType')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('LessonType','Lesson Types','Dropdown','LessonType','LessonTypeID','LessonType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'LessonType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LessonTypeID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS

--Begin table: LessonType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.LessonType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.LessonType
		(
		LessonTypeID int IDENTITY(0,1) NOT NULL,
		LessonType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LessonType', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LessonType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LessonTypeID'
EXEC Utility.SetIndexNonClustered 'IX_LessonType', @cTableName, 'LessonType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: LessonType

IF NOT EXISTS (SELECT 1 FROM Dropdown.LessonType LT WITH (NOLOCK) WHERE LT.LessonTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.LessonType ON
	INSERT INTO Dropdown.LessonType (LessonTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.LessonType OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'LMS_LessonType'
WHERE Category = 'LMS_LessonType_Old'
GO

INSERT INTO Dropdown.LessonType
	(LessonType)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS LessonType
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'LMS_LessonType'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.LessonType LT WITH (NOLOCK)
		WHERE LT.LessonType = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(L.LessonType) AS LessonType
FROM dbo.LMS L WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.LessonType LT WITH (NOLOCK)
	WHERE LT.LessonType = LTRIM(L.LessonType)
	)
	AND L.LessonType IS NOT NULL

ORDER BY LessonType
GO		

UPDATE L 
SET L.LessonTypeID = LT.LessonTypeID 
FROM dbo.LMS L 
	JOIN Dropdown.LessonType LT ON LT.LessonType = L.LessonType
		AND L.LessonTypeID = 0
GO
		
UPDATE Dropdown.LessonType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND LessonTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'LessonType'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	LT.LessonTypeID
FROM Dropdown.LessonType LT WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = LT.LessonType
		AND D.Category = 'LMS_LessonType'
		AND LT.LessonTypeID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = LT.LessonTypeID
			)
ORDER BY LT.LessonTypeID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.LessonType LT WITH (NOLOCK) ON LT.LessonTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY LT.LessonType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'LMS_LessonType_Old'
WHERE Category = 'LMS_LessonType'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'LessonType'
GO
--End file CMT 8859 - 22 - LessonType.sql
GO
--Begin file CMT 8859 - 23 - COPBlockType.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlock' AND IsConfigurableByTier = 1)
	BEGIN
	
	UPDATE JLLIS.Dropdown.DropdownMetadata 
	SET
		DropdownMetadataCode = 'COPBlockType',
		DropdownMetadataLabel = 'COP Block Types',
		TableName = 'COPBlockType',
		PrimaryKeyFieldName = 'COPBlockTypeID',
		LabelFieldName = 'COPBlockType'
	WHERE DropdownMetadataCode = 'COPBlock' AND IsConfigurableByTier = 1

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlockType' AND IsConfigurableByTier = 1)
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('COPBlockType','COP Block Types','Dropdown','COPBlockType','COPBlockTypeID','COPBlockType',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'COPBlockType'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: SSiteItem
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'COPBlockTypeID'
SET @cTableName = 'dbo.SSiteItem'

IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) JOIN sys.objects O WITH (NOLOCK) ON O.object_id = C.object_id AND C.Name = 'COPBlockID' AND O.Name = @cTableName AND O.Type = 'U')
	ALTER TABLE dbo.SSiteItem DROP COPBlockID
--ENDIF

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int	NULL'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: SSiteItem

--Begin table: COPBlockType
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.COPBlockType'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.COPBlockType
		(
		COPBlockTypeID int IDENTITY(0,1) NOT NULL,
		COPBlockCategoryID int,
		COPBlockType varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'COPBlockType', ''
EXEC Utility.SetDefault @cTableName, 'COPBlockCategoryID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockType', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockCategoryID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'COPBlockTypeID'
EXEC Utility.SetIndexNonClustered 'IX_COPBlockType', @cTableName, 'COPBlockType ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: COPBlockType

IF NOT EXISTS (SELECT 1 FROM Dropdown.COPBlockType CBT WITH (NOLOCK) WHERE CBT.COPBlockTypeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.COPBlockType ON
	INSERT INTO Dropdown.COPBlockType (COPBlockTypeID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.COPBlockType OFF

	END
--ENDIF
GO

INSERT INTO Dropdown.COPBlockType
	(COPBlockType)
SELECT DISTINCT 
	LTRIM(SI.Block) AS COPBlockType
FROM dbo.SSiteItem SI WITH (NOLOCK)
WHERE SI.Block IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.COPBlockType CBT WITH (NOLOCK)
		WHERE CBT.COPBlockType = LTRIM(SI.Block)
		)

ORDER BY LTRIM(SI.Block)
GO		

UPDATE CBT
SET CBT.COPBlockCategoryID = 

	CASE
		WHEN CBT.COPBlockType = 'BattleBoard'
		THEN (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'TaskTracker')
		WHEN CBT.COPBlockType = 'Library'
		THEN (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'Library')
		ELSE (SELECT CBC.COPBlockCategoryID FROM JLLIS.Dropdown.COPBlockCategory CBC WITH (NOLOCK) WHERE CBC.COPBlockCategoryCode = 'General')
	END

FROM Dropdown.COPBlockType CBT
WHERE CBT.COPBlockTypeID > 0
GO

UPDATE SI
SET SI.COPBlockTypeID = CBT.COPBlockTypeID 
FROM dbo.SSiteItem SI
	JOIN Dropdown.COPBlockType CBT ON CBT.COPBlockType = LTRIM(SI.Block)
		AND SI.COPBlockTypeID = 0
GO
		
UPDATE Dropdown.COPBlockType
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND COPBlockTypeID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'COPBlockType'
	)

SET @nTierID = 	
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	@nTierID,
	@nDropdownMetadataID,
	CBT.COPBlockTypeID
FROM Dropdown.COPBlockType CBT WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.TierDropdown TD
	WHERE TD.TierID = @nTierID
		AND TD.DropdownMetadataID = @nDropdownMetadataID
		AND TD.DropdownEntityID = CBT.COPBlockTypeID
	)
	AND CBT.COPBlockTypeID > 0
ORDER BY CBT.COPBlockTypeID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.COPBlockType CBT WITH (NOLOCK) ON CBT.COPBlockTypeID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CBT.COPBlockType

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'COPBlockType'
GO
--End file CMT 8859 - 23 - COPBlockType.sql
GO
--Begin file CMT 8859 - 24 - WarfareMission.sql
--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'WarfareMission')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('WarfareMission','Warfare Missions','Dropdown','WarfareMission','WarfareMissionID','WarfareMission',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'WarfareMission'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'WarfareMissionID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: LMS
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'WarfareMissionID'
SET @cTableName = 'dbo.LMS'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: LMS
	
--Begin table: WarfareMission
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.WarfareMission'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.WarfareMission
		(
		WarfareMissionID int IDENTITY(0,1) NOT NULL,
		WarfareMission varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'WarfareMission', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'WarfareMission', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'
	
EXEC Utility.SetPrimaryKeyClustered @cTableName, 'WarfareMissionID'
EXEC Utility.SetIndexNonClustered 'IX_WarfareMission', @cTableName, 'WarfareMission ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: WarfareMission
	
IF NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMissionID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.WarfareMission ON
	INSERT INTO Dropdown.WarfareMission (WarfareMissionID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.WarfareMission OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'Warfare/Mission Area'
WHERE Category = 'Warfare/Mission Area_Old'
GO

INSERT INTO Dropdown.WarfareMission
	(WarfareMission)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS WarfareMission
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'Warfare/Mission Area'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.WarfareMission WM WITH (NOLOCK)
		WHERE WM.WarfareMission = LTRIM(D.DisplayText)
		)
GO
	
IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID('dbo.CDR') AND C.Name = 'WarfareMission')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.WarfareMission (WarfareMission) SELECT DISTINCT LTRIM(C.WarfareMission) FROM dbo.CDR C WITH (NOLOCK) WHERE C.WarfareMission IS NOT NULL AND NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMission = LTRIM(C.WarfareMission)) ORDER BY LTRIM(C.WarfareMission)'
	EXEC (@cSQL)

	SET @cSQL = 'UPDATE C SET C.WarfareMissionID = WM.WarfareMissionID FROM dbo.CDR C JOIN Dropdown.WarfareMission WM ON WM.WarfareMission = LTRIM(C.WarfareMission) AND C.WarfareMissionID = 0'
	EXEC (@cSQL)
	
	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID('dbo.LMS') AND C.Name = 'WarfareMission')
	BEGIN

	DECLARE @cSQL varchar(max)
	
	SET @cSQL = 'INSERT INTO Dropdown.WarfareMission (WarfareMission) SELECT DISTINCT LTRIM(L.WarfareMission) FROM dbo.LMS L WITH (NOLOCK) WHERE L.WarfareMission IS NOT NULL AND NOT EXISTS (SELECT 1 FROM Dropdown.WarfareMission WM WITH (NOLOCK) WHERE WM.WarfareMission = LTRIM(L.WarfareMission)) ORDER BY LTRIM(L.WarfareMission)'
	EXEC (@cSQL)

	SET @cSQL = 'UPDATE L SET L.WarfareMissionID = WM.WarfareMissionID FROM dbo.LMS L JOIN Dropdown.WarfareMission WM ON WM.WarfareMission = LTRIM(L.WarfareMission) AND L.WarfareMissionID = 0'
	EXEC (@cSQL)

	END
--ENDIF
GO

UPDATE Dropdown.WarfareMission
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND WarfareMissionID <> 0
GO
	
DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'WarfareMission'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	WM.WarfareMissionID
FROM Dropdown.WarfareMission WM WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = WM.WarfareMission
		AND D.Category = 'Warfare/Mission Area'
		AND WM.WarfareMissionID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = WM.WarfareMissionID
			)
ORDER BY WM.WarfareMissionID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.WarfareMission WM WITH (NOLOCK) ON WM.WarfareMissionID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY WM.WarfareMission

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
	
UPDATE dbo.Dropdown
SET Category = 'Warfare/Mission Area_Old'
WHERE Category = 'Warfare/Mission Area'
GO
	
UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'WarfareMission'
GO
--End file CMT 8859 - 24 - WarfareMission.sql
GO

