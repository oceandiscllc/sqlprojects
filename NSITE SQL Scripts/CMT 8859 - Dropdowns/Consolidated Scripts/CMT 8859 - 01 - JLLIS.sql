--Begin file CMT 8859 - 01 - JLLIS Tables.sql
USE JLLIS
GO

--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

--Begin dropdown data table creation
--Begin table Dropdown.Caveat
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Caveat'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.Caveat
	
CREATE TABLE Dropdown.Caveat
	(
	CaveatID int IDENTITY(0,1) NOT NULL,
	Caveat varchar(250),
	CaveatWeight int,
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Caveat', ''
EXEC Utility.SetDefault @cTableName, 'CaveatWeight', 0
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'Caveat', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'CaveatWeight', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CaveatID'
EXEC Utility.SetIndexNonClustered 'IX_Caveat', @cTableName, 'DisplayOrder ASC,Caveat ASC'
GO

SET IDENTITY_INSERT Dropdown.Caveat ON
INSERT INTO Dropdown.Caveat (CaveatID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Caveat OFF

INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('Sensitive But Unclassified', 100, 1)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('FOUO', 200, 2)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('NOFORN', 300, 3)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('Restricted', 400, 4)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('PROPIN', 500, 5)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('COMSEC', 600, 6)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('CNWDI', 700, 7)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('CRYPTO', 800, 8)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('NOCONTRACT', 900, 9)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('ORCON', 1000, 10)
INSERT INTO Dropdown.Caveat (Caveat,CaveatWeight,DisplayOrder) VALUES ('WNINTEL', 11000, 11)
GO
--End table Dropdown.Caveat

--Begin table Dropdown.Classification
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Classification'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.Classification

CREATE TABLE Dropdown.Classification
	(
	ClassificationID int IDENTITY(0,1) NOT NULL,
	Classification varchar(250),
	ClassificationAbbreviation varchar(5),
	ClassificationColor varchar(50),
	ClassificationWeight int,
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Classification', ''
EXEC Utility.SetDefault @cTableName, 'ClassificationAbbreviation', ''
EXEC Utility.SetDefault @cTableName, 'ClassificationColor', '#000000'
EXEC Utility.SetDefault @cTableName, 'ClassificationWeight', 0
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'Classification', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'ClassificationAbbreviation', 'varchar(5)'
EXEC Utility.SetColumnNotNull @cTableName, 'ClassificationColor', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'ClassificationWeight', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ClassificationID'
EXEC Utility.SetIndexNonClustered 'IX_Classification', @cTableName, 'DisplayOrder ASC,Classification ASC'
GO

SET IDENTITY_INSERT Dropdown.Classification ON
INSERT INTO Dropdown.Classification (ClassificationID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Classification OFF

INSERT INTO Dropdown.Classification (Classification,ClassificationAbbreviation,ClassificationColor,ClassificationWeight,DisplayOrder) VALUES ('Unclassified','U','#008000',100,1)
INSERT INTO Dropdown.Classification (Classification,ClassificationAbbreviation,ClassificationColor,ClassificationWeight,DisplayOrder) VALUES ('Confidential','C','#0000FF',200,2)
INSERT INTO Dropdown.Classification (Classification,ClassificationAbbreviation,ClassificationColor,ClassificationWeight,DisplayOrder) VALUES ('Secret','S','#FF0000',300,3)
INSERT INTO Dropdown.Classification (Classification,ClassificationAbbreviation,ClassificationColor,ClassificationWeight,DisplayOrder) VALUES ('Top Secret','TS','#000000',400,4)
GO
--End table Dropdown.Classification

--Begin table Dropdown.ClassificationReason
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.ClassificationReason'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.ClassificationReason

CREATE TABLE Dropdown.ClassificationReason
	(
	ClassificationReasonID int IDENTITY(1,1) NOT NULL,
	Section varchar(50),
	SubSection varchar(50),
	ClassificationReason varchar(max),
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ClassificationReason', ''
EXEC Utility.SetDefault @cTableName, 'Section', ''
EXEC Utility.SetDefault @cTableName, 'SubSection', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'ClassificationReason', 'varchar(max)'
EXEC Utility.SetColumnNotNull @cTableName, 'Section', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'SubSection', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ClassificationReasonID'
EXEC Utility.SetIndexNonClustered 'IX_ClassificationReason', @cTableName, 'Section ASC,SubSection ASC'
GO

INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'a', '"Military plans, weapons systems, or operations"')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'b', 'Foreign government information')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'c', '"Intelligence activities (including special activities), intelligence sources or methods, or cryptology"')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'd', '"Foreign relations or foreign activities of the United States, including confidential sources"')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'e', '"Scientific, technological, or economic matters relating to the national security, which includes defense against transnational terrorism"')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'f', 'United States Government programs for safeguarding nuclear materials of facilities')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'g', '"Vulnerabilities of capabilities of systems, installations, infrastructures, projects, plans, or protection services relating to the national security, which includes defense against transnational terrorism"')
INSERT INTO Dropdown.ClassificationReason (Section,SubSection,ClassificationReason) VALUES ('1.4', 'h', 'Weapons of mass destruction')
GO
--End table Dropdown.ClassificationReason

--Begin table: Coalition
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Coalition'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	DROP TABLE Dropdown.Coalition

CREATE TABLE Dropdown.Coalition
	(
	CoalitionID int IDENTITY(1,1) NOT NULL,
	Coalition varchar(250),
	CoalitionAbbreviation varchar(5),
	OriginatingTierID int,
	IsActive bit
	) ON [PRIMARY]

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Coalition', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CoalitionID'
EXEC Utility.SetIndexNonClustered 'IX_Coalition', @cTableName, 'Coalition ASC'
GO

INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Athens Olympics Security Coalition', 'AOSC')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Biological Weapons Convention States', 'BWCS')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Civilian Protection Monitoring Team for Sudan', 'CMPT')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Combined Naval Forces Central Command', 'CNFC')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Chemical Weapons Convention States', 'CWCS')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('European Counter-Terrorism Forces', 'ECTF')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Global Counter-Terrorism Forces', 'GCTF')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Global Maritime Interception Forces', 'GMIF')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('International Security Assistance Forces for Afghanistan', 'ISAF')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Stabilization Forces in Kosovo', 'KFOR')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Multinational Coalition Forces - Iraq', 'MCFI')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Multinational Interim Force Haiti', 'MIFH')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Multinational Peacekeeping Forces - Liberia', 'MPFL')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('North Atlantic Treaty Organization', 'NATO')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Olympic Security Advisory Group', 'OSAG')
INSERT INTO Dropdown.Coalition (Coalition,CoalitionAbbreviation) VALUES ('Stabilization Forces in Bosnia', 'SFOR')
--End table: Coalition

--Begin table: CoalitionCountry
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CoalitionCountry'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CoalitionCountry
		(
		CoalitionCountryID int IDENTITY(0,1) NOT NULL,
		CoalitionID int,
		CountryID int
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CoalitionID', 0
EXEC Utility.SetDefault @cTableName, 'CountryID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CoalitionID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CountryID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CoalitionCountryID'
EXEC Utility.SetIndexNonClustered 'IX_Coalition', @cTableName, 'CoalitionID ASC, CountryID ASC'
GO
--End table: CoalitionCountry

--Begin table Dropdown.COPBlockCategory
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Dropdown.COPBlockType') AND O.type in ('U'))
	DROP TABLE Dropdown.COPBlockType

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.COPBlockCategory'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.COPBlockCategory
	
CREATE TABLE Dropdown.COPBlockCategory
	(
	COPBlockCategoryID int IDENTITY(0,1) NOT NULL,
	COPBlockCategoryCode varchar(50),
	COPBlockCategory varchar(250),
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'COPBlockCategory', ''
EXEC Utility.SetDefault @cTableName, 'COPBlockCategoryCode', ''
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockCategory', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'COPBlockCategoryCode', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'COPBlockCategoryID'
EXEC Utility.SetIndexNonClustered 'IX_COPBlockCategory', @cTableName, 'DisplayOrder ASC,COPBlockCategory ASC'
GO

SET IDENTITY_INSERT Dropdown.COPBlockCategory ON
INSERT INTO Dropdown.COPBlockCategory (COPBlockCategoryID) VALUES (0)
SET IDENTITY_INSERT Dropdown.COPBlockCategory OFF

INSERT INTO Dropdown.COPBlockCategory (COPBlockCategory,COPBlockCategoryCode) VALUES ('General','General')
INSERT INTO Dropdown.COPBlockCategory (COPBlockCategory,COPBlockCategoryCode) VALUES ('Library','Library')
INSERT INTO Dropdown.COPBlockCategory (COPBlockCategory,COPBlockCategoryCode) VALUES ('Task Tracker','TaskTracker')
GO
--End table Dropdown.COPBlockCategory

--Begin table Dropdown.Country
DECLARE @cTableName varchar(250)
DECLARE @nAddData bit

SET @cTableName = 'Dropdown.Country'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.Country

CREATE TABLE Dropdown.Country
	(
	CountryID int IDENTITY(0,1) NOT NULL,
	Country varchar(250),
	ISOCode2 varchar(2),
	ISOCode3 varchar(3),
	Region varchar(250),
	SubRegion varchar(250),
	CDHID int,
	LAT varchar(10),
	LONG varchar(10),
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Country', ''
EXEC Utility.SetDefault @cTableName, 'CDHID', 0
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'Country', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'CDHID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CountryID'
EXEC Utility.SetIndexNonClustered 'IX_Country', @cTableName, 'DisplayOrder ASC,Country ASC'
GO

SET IDENTITY_INSERT Dropdown.Country ON
INSERT INTO Dropdown.Country (CountryID) VALUES (0)

INSERT INTO Dropdown.Country 
	(CountryID,Country,ISOCode2,ISOCode3,Region,SubRegion,CDHID,LAT,LONG,DisplayOrder) 
SELECT
	C1.CountryID,
	C1.CountryName,
	C1.ISOCode2,
	C1.ISOCode3,
	C1.Region,
	C1.SubRegion,
	C1.CDHID,
	C1.LAT,
	C1.LONG,
	C1.DisplayOrder
FROM dbo.Country C1 WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Country C2 WITH (NOLOCK)
	WHERE C1.CountryID = 0
	)
ORDER BY C1.CountryName, C1.ISOCode2
SET IDENTITY_INSERT Dropdown.Country OFF

GO
--End table Dropdown.Country

--Begin table Dropdown.Paygrade
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Paygrade'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.Paygrade

CREATE TABLE Dropdown.Paygrade
	(
	PaygradeID int IDENTITY(0,1) NOT NULL,
	Paygrade varchar(250),
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'Paygrade', ''

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'Paygrade', 'varchar(250)'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'PaygradeID'
EXEC Utility.SetIndexNonClustered 'IX_Paygrade', @cTableName, 'DisplayOrder ASC,Paygrade ASC'
GO

SET IDENTITY_INSERT Dropdown.Paygrade ON
INSERT INTO Dropdown.Paygrade (PaygradeID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Paygrade OFF

INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CADET',1)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('MIDN',2)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CIV',3)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CTR',4)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-1',5)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-2',6)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-3',7)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-4',8)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-5',9)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-6',10)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-7',11)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-8',12)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('E-9',13)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CWO-1',14)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CWO-2',15)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CWO-3',16)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CWO-4',17)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('CWO-5',18)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-1',19)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-2',20)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-3',21)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-4',22)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-5',23)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-6',24)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-7',25)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-8',26)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-9',27)
INSERT INTO Dropdown.Paygrade (Paygrade,DisplayOrder) VALUES ('0-10',28)
GO
--End table Dropdown.Paygrade

--Begin table Dropdown.SecLev
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.SecLev'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.SecLev

CREATE TABLE Dropdown.SecLev
	(
	SecLevID int IDENTITY(0,1) NOT NULL,
	SecLev int,
	Role varchar(250),
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'Role', ''
EXEC Utility.SetDefault @cTableName, 'SecLev', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'SecLev', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Role', 'varchar(250)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'SecLevID'
EXEC Utility.SetIndexClustered 'IX_SecLev', @cTableName, 'DisplayOrder ASC,SecLev ASC'
GO

SET IDENTITY_INSERT Dropdown.SecLev ON
INSERT INTO Dropdown.SecLev (SecLevID) VALUES (0)
SET IDENTITY_INSERT Dropdown.SecLev OFF

INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('1000','Registered', 1)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('2000','Authorized', 2)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('2100','COI Manager', 3)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('2300','IMS_SME', 4)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('2400','IMS_Gatekeeper', 5)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('2500','CLM', 6)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('3000','TEAM', 7)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('4000','Manager', 8)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('6000','Administrator', 9)
INSERT INTO Dropdown.SecLev (SecLev,Role,DisplayOrder) VALUES ('7000','Super Administrator', 10)
GO
--End table Dropdown.SecLev

--Begin table Dropdown.State
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.State'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.State

CREATE TABLE Dropdown.State
	(
	Stateid int IDENTITY(0,1) NOT NULL,
	State varchar(250),
	StateCode varchar(2),
	DisplayOrder int,
	IsActive bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'State', ''
EXEC Utility.SetDefault @cTableName, 'StateCode', ''

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'State', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'StateCode', 'varchar(2)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'StateID'
EXEC Utility.SetIndexClustered 'IX_State', @cTableName, 'DisplayOrder ASC,State ASC'
GO

SET IDENTITY_INSERT Dropdown.State ON
INSERT INTO Dropdown.State (StateID) VALUES (0)
SET IDENTITY_INSERT Dropdown.State OFF

INSERT INTO Dropdown.State (State,StateCode) VALUES ('Alabama','AL')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Alaska','AK')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Arizona','AZ')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Arkansas','AR')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('California','CA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Colorado','CO')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Connecticut','CT')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Delaware','DE')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('District of Columbia','DC')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Florida','FL')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Georgia','GA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Guam','GU')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Hawaii','HI')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Idaho','ID')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Illinois','IL')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Indiana','IN')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Iowa','IA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Kansas','KS')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Kentucky','KY')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Louisiana','LA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Maine','ME')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Maryland','MD')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Massachusetts','MA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Michigan','MI')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Minnesota','MN')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Mississippi','MS')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Missouri','MO')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Montana','MT')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Nebraska','NE')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Nevada','NV')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('New Hampshire','NH')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('New Jersey','NJ')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('New Mexico','NM')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('New York','NY')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('North Carolina','NC')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('North Dakota','ND')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Ohio','OH')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Oklahoma','OK')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Oregon','OR')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Pennsylvania','PA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Puerto Rico','PR')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Rhode Island','RI')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('South Carolina','SC')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('South Dakota','SD')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Tennessee','TN')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Texas','TX')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Utah','UT')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Vermont','VT')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Virgin Islands','VI')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Virginia','VA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Washington','WA')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('West Virginia','WV')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Wisconsin','WI')
INSERT INTO Dropdown.State (State,StateCode) VALUES ('Wyoming','WY')
GO
--End table Dropdown.State

--Begin table Dropdown.Status
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Status'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.Status

CREATE TABLE Dropdown.Status
	(
	StatusID int IDENTITY(0,1) NOT NULL,
	Status varchar(250) NOT NULL,
	DisplayOrder int,
	IsActive bit,
	IsForJointSearch bit
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'IsForJointSearch', 0
EXEC Utility.SetDefault @cTableName, 'Status', ''

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForJointSearch', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'Status', 'varchar(250)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'StatusID'
EXEC Utility.SetIndexClustered 'IX_Status', @cTableName, 'DisplayOrder ASC,Status ASC'
GO

SET IDENTITY_INSERT Dropdown.Status ON
INSERT INTO Dropdown.Status (StatusID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Status OFF

INSERT INTO Dropdown.Status (Status) VALUES ('Active')
INSERT INTO Dropdown.Status (Status) VALUES ('Canceled') 
INSERT INTO Dropdown.Status (Status) VALUES ('Closed') 
INSERT INTO Dropdown.Status (Status) VALUES ('Deleted') 
INSERT INTO Dropdown.Status (Status) VALUES ('Draft') 
INSERT INTO Dropdown.Status (Status) VALUES ('Hold') 
INSERT INTO Dropdown.Status (Status) VALUES ('Pending') 
INSERT INTO Dropdown.Status (Status) VALUES ('PendingMobile') 
INSERT INTO Dropdown.Status (Status) VALUES ('Validated') 
INSERT INTO Dropdown.Status (Status) VALUES ('Xfer') 
GO

UPDATE Dropdown.Status
SET IsForJointSearch = 1
WHERE Status IN ('Active','Closed','Validated')
GO
--End table Dropdown.Status
--Begin dropdown data table creation

--Begin tier table modification
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Tier'

EXEC Utility.AddColumn @cTableName, 'DropdownSearchMode', 'int'
EXEC Utility.SetDefault @cTableName, 'DropdownSearchMode', 1
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownSearchMode', 'int'
GO
--End tier table modification

--Begin dropdown system table creation
--Begin table Dropdown.DropdownMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.DropdownMetadata'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE Dropdown.DropdownMetadata
		(
		DropdownMetadataID int IDENTITY(1,1) NOT NULL,
		DropdownMetadataCode varchar(50),
		DropdownMetadataLabel varchar(250),
		DatabaseName varchar(50),
		SchemaName varchar(50),
		TableName varchar(50),
		PrimaryKeyFieldName varchar(50),
		LabelFieldName varchar(50),
		IsConfigurableByTier bit,
		IsActive bit
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'DatabaseName', 'varchar(50)'

EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'IsConfigurableByTier', 1

EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataCode', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataLabel', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsConfigurableByTier', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'LabelFieldName', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'PrimaryKeyFieldName', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'SchemaName', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'TableName', 'varchar(50)'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'DropdownMetadataID'
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Caveat' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Caveat','Caveats','JLLIS','Dropdown','Caveat','CaveatID','Caveat',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Classification' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Classification','Classifications','JLLIS','Dropdown','Classification','ClassificationID','Classification',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Coalition' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Coalition','Coalitions','JLLIS','Dropdown','Coalition','CoalitionID','Coalition',0)
GO

IF EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlockType' AND DMD.IsConfigurableByTier = 0)
	BEGIN
	
	UPDATE Dropdown.DropdownMetadata 
	SET 
		DropdownMetadataCode = 'COPBlockCategory',
		DropdownMetadataLabel = 'COP Block Categories',
		TableName = 'COPBlockCategory',
		PrimaryKeyFieldName = 'COPBlockCategoryID',
		LabelFieldName = 'COPBlockCategory'
	WHERE DropdownMetadataCode = 'COPBlockType' AND IsConfigurableByTier = 0
	
	END
--ENDIF
GO
	
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'COPBlockCategory' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('COPBlockCategory','COP Block Categories','JLLIS','Dropdown','COPBlockCategory','COPBlockCategoryID','COPBlockCategory',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Country' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Country','Countries','JLLIS','Dropdown','Country','CountryID','Country',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Paygrade' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Paygrade','Paygrades','JLLIS','Dropdown','Paygrade','PaygradeID','Paygrade',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'SecLev' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('SecLev','Roles','JLLIS','Dropdown','SecLev','SecLev','Role',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'State' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('State','States','JLLIS','Dropdown','State','StateCode','State',0)
GO
IF NOT EXISTS (SELECT 1 FROM Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Status' AND DMD.IsConfigurableByTier = 0)
	INSERT INTO Dropdown.DropdownMetadata (DropdownMetadataCode,DropdownMetadataLabel,DatabaseName,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsConfigurableByTier) VALUES ('Status','Statuses','JLLIS','Dropdown','Status','StatusID','Status',0)
GO
--End table Dropdown.DropdownMetadata

--Begin table Dropdown.TierDropdownMetadata
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.TierDropdownMetadata'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE Dropdown.TierDropdownMetadata

CREATE TABLE Dropdown.TierDropdownMetadata
	(
	TierDropdownMetadataID int IDENTITY(1,1) NOT NULL,
	DropdownMetadataID int,
	TierID int
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DropdownMetadataID', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'TierDropdownMetadataID'
EXEC Utility.SetIndexClustered 'IX_TierDropdownMetadata', @cTableName, 'TierID ASC,DropdownMetadataID ASC'
GO

INSERT INTO Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	T.TierID
FROM dbo.Tier T WITH (NOLOCK), Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DMD.IsConfigurableByTier = 1
	AND T.IsInstance = 1
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdownMetadata TD WITH (NOLOCK)
		WHERE TD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TD.TierID = T.TierID
		)
ORDER BY DMD.DropdownMetadataID, T.TierID
GO
--End table Dropdown.TierDropdownMetadata
--End dropdown system table creation
--End file CMT 8859 - 01 - JLLIS Tables.sql
GO

