--Begin schema validation: Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Dropdown

IF NOT EXISTS (SELECT 1 FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'CDRCreatorQualifier')
	BEGIN
	
	INSERT INTO JLLIS.Dropdown.DropdownMetadata 
		(DropdownMetadataCode,DropdownMetadataLabel,SchemaName,TableName,PrimaryKeyFieldName,LabelFieldName,IsActive) 
	VALUES 
		('CDRCreatorQualifier','CDR Creator Qualifiers','Dropdown','CDRCreatorQualifier','CDRCreatorQualifierID','CDRCreatorQualifier',0)

	END
--ENDIF
GO

DECLARE @nInstanceID int
SET @nInstanceID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID,TierID)
SELECT
	DMD.DropdownMetadataID,
	@nInstanceID
FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK)
WHERE DropdownMetadataCode = 'CDRCreatorQualifier'
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = @nInstanceID
		)
GO	

--Begin table: CDR
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'CDRCreatorQualifierID'
SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End table: CDR

--Begin table: CDRCreatorQualifier
DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.CDRCreatorQualifier'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE Dropdown.CDRCreatorQualifier
		(
		CDRCreatorQualifierID int IDENTITY(0,1) NOT NULL,
		CDRCreatorQualifier varchar(250),
		OriginatingTierID int,
		UpdateJLLISUserID int,
		IsActive bit
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CDRCreatorQualifier', ''
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateJLLISUserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CDRCreatorQualifier', 'varchar(250)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateJLLISUserID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRCreatorQualifierID'
EXEC Utility.SetIndexNonClustered 'IX_CDRCreatorQualifier', @cTableName, 'CDRCreatorQualifier ASC'
EXEC Utility.EnableAuditing @cTableName, 0
GO
--End table: CDRCreatorQualifier

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK) WHERE CQ.CDRCreatorQualifierID = 0)
	BEGIN
	
	SET IDENTITY_INSERT Dropdown.CDRCreatorQualifier ON
	INSERT INTO Dropdown.CDRCreatorQualifier (CDRCreatorQualifierID) VALUES (0)
	SET IDENTITY_INSERT Dropdown.CDRCreatorQualifier OFF

	END
--ENDIF
GO

UPDATE dbo.Dropdown
SET Category = 'CDR_CreatorQualifier'
WHERE Category = 'CDR_CreatorQualifier_Old'
GO

INSERT INTO Dropdown.CDRCreatorQualifier
	(CDRCreatorQualifier)
SELECT DISTINCT 
	LTRIM(D.DisplayText) AS CDRCreatorQualifier
FROM dbo.Dropdown D WITH (NOLOCK)
WHERE D.Category = 'CDR_CreatorQualifier'
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
		WHERE CQ.CDRCreatorQualifier = LTRIM(D.DisplayText)
		)

UNION

SELECT DISTINCT
	LTRIM(C.CreatorQualifier) AS CDRCreatorQualifier
FROM dbo.CDR C WITH (NOLOCK)
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
	WHERE CQ.CDRCreatorQualifier = LTRIM(C.CreatorQualifier)
	)
	AND C.CreatorQualifier IS NOT NULL

ORDER BY CDRCreatorQualifier
GO

UPDATE C 
SET C.CDRCreatorQualifierID = CQ.CDRCreatorQualifierID 
FROM dbo.CDR C 
	JOIN Dropdown.CDRCreatorQualifier CQ ON CQ.CDRCreatorQualifier = C.CreatorQualifier
		AND C.CDRCreatorQualifierID = 0
GO
		
UPDATE Dropdown.CDRCreatorQualifier
SET OriginatingTierID = 
	(
	SELECT 
		T.InstanceID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = 
		(
		SELECT TOP 1
			C.OriginatingTierID
		FROM dbo.CDR C WITH (NOLOCK)
		WHERE C.OriginatingTierID > 0
		)
	)
WHERE OriginatingTierID = 0
	AND CDRCreatorQualifierID <> 0
GO

DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @nDropdownMetadataID = 
	(
	SELECT DMD.DropdownMetadataID
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'CDRCreatorQualifier'
	)

INSERT INTO Dropdown.TierDropdown
	(TierID, DropdownMetadataID, DropdownEntityID)
SELECT DISTINCT
	D.TierID,
	@nDropdownMetadataID,
	CQ.CDRCreatorQualifierID
FROM Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK)
	JOIN dbo.Dropdown D WITH (NOLOCK) ON LTRIM(D.DisplayText) = CQ.CDRCreatorQualifier
		AND D.Category = 'CDR_CreatorQualifier'
		AND CQ.CDRCreatorQualifierID > 0
		AND D.DisplayText IS NOT NULL
		AND LEN(LTRIM(D.DisplayText)) > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = D.TierID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.DropdownEntityID = CQ.CDRCreatorQualifierID
			)
ORDER BY CQ.CDRCreatorQualifierID, D.TierID

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable

CREATE TABLE #oTable (DisplayOrder int identity(1, 1), TierDropdownID int)

DECLARE oCursor CURSOR FOR
	SELECT DISTINCT 
		TD.TierID
	FROM Dropdown.TierDropdown TD
	WHERE TD.DropdownMetadataID = @nDropdownMetadataID

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	TRUNCATE TABLE #oTable
	
	INSERT INTO #oTable
		(TierDropdownID)
	SELECT
		TD.TierDropdownID
	FROM Dropdown.TierDropdown TD WITH (NOLOCK)
		JOIN Dropdown.CDRCreatorQualifier CQ WITH (NOLOCK) ON CQ.CDRCreatorQualifierID = TD.DropdownEntityID
			AND TD.TierID = @nTierID
			AND TD.DropdownMetadataID = @nDropdownMetadataID
	ORDER BY CQ.CDRCreatorQualifier

	UPDATE TD
	SET TD.DisplayOrder = T.DisplayOrder
	FROM Dropdown.TierDropdown TD
		JOIN #oTable T ON T.TierDropdownID = TD.TierDropdownID
			AND TD.DisplayOrder = 0

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
	
UPDATE dbo.Dropdown
SET Category = 'CDR_CreatorQualifier_Old'
WHERE Category = 'CDR_CreatorQualifier'
GO

UPDATE JLLIS.Dropdown.DropdownMetadata
SET IsActive = 1
WHERE DropdownMetadataCode = 'CDRCreatorQualifier'
GO