--Begin create AuditLog table
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.AuditLog') AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.AuditLog
		(
		AuditLogID int IDENTITY(1,1) NOT NULL,
		SchemaName varchar(50) NOT NULL,
		TableName varchar(50) NOT NULL,
		EntityID int NOT NULL CONSTRAINT DF_AuditLog_EntityID DEFAULT 0,
		AuditAction varchar(50) NOT NULL,
		AuditDate datetime NOT NULL CONSTRAINT DF_AuditLog_AuditDate DEFAULT getDate(),
		AuditData xml NOT NULL,
		UpdateJLLISUserID int NOT NULL CONSTRAINT DF_AuditLog_UpdateJLLISUserID DEFAULT 0,
		Title varchar(50) NULL,
		FirstName varchar(50) NULL,
		LastName varchar(50) NULL,
		CONSTRAINT PK_AuditLog PRIMARY KEY NONCLUSTERED 
			(
			AuditLogID ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	CREATE CLUSTERED INDEX IX_AuditLog ON dbo.AuditLog
		(
		SchemaName ASC,
		TableName ASC,
		EntityID ASC,
		AuditLogID DESC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	END
--ENDIF
GO
--End create AuditLog table

--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--Begin create EnableAuditing stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.EnableAuditing') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.EnableAuditing
GO

-- ==================================================================
-- Author:		Todd Pires
-- Create date: 2011.02.15
-- Description:	A stored procedure to enable data auditing on a table
-- ==================================================================
CREATE PROCEDURE Utility.EnableAuditing
	@cTargetTable varchar(250),
	@nPrintSQL bit

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cCursorName varchar(100)
	DECLARE @cCursorSQL1 varchar(max)
	DECLARE @cCursorSQL2 varchar(max)
	DECLARE @cInsertSQL varchar(max)
	DECLARE @cMemoryTableName varchar(50)
	DECLARE @cPrimaryKeyFieldName varchar(100)
	DECLARE @cSchemaName varchar(50)
	DECLARE @cSQL varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cTab5 varchar(5)
	DECLARE @cTab6 varchar(6)
	DECLARE @cTab7 varchar(7)
	DECLARE @cTab8 varchar(8)
	DECLARE @cTableName varchar(100)
	DECLARE @cTriggerName varchar(250)
	DECLARE @cWhereSQL varchar(max)
	DECLARE @nHasUpdateJLLISUserID bit
	DECLARE @nLength int

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)
	SET @cTab5 = REPLICATE(CHAR(9), 5)
	SET @cTab6 = REPLICATE(CHAR(9), 6)
	SET @cTab7 = REPLICATE(CHAR(9), 7)
	SET @cTab8 = REPLICATE(CHAR(9), 8)
	SET @nHasUpdateJLLISUserID = 0
	
	IF CHARINDEX('.', @cTargetTable) = 0
		SET @cTargetTable = 'dbo.' + @cTargetTable

	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTargetTable) AND C.Name = 'UpdateJLLISUserID')
		SET @nHasUpdateJLLISUserID = 1

	SET @nLength = LEN(@cTargetTable) - CHARINDEX('.', @cTargetTable)

	SET @cSchemaName = LEFT(@cTargetTable, CHARINDEX('.', @cTargetTable) - 1)
	SET @cTableName = RIGHT(@cTargetTable, @nLength)
	SET @cCursorName = 'o' + @cTableName + 'AuditLogCursor'

	SET @cPrimaryKeyFieldName = 
		(
		SELECT COL_NAME(ic.object_ID, ic.column_ID)
		FROM sys.Indexes I WITH (NOLOCK)
			JOIN sys.index_columns IC WITH (NOLOCK) ON I.object_ID = IC.object_ID
			JOIN sys.objects O WITH (NOLOCK) ON O.object_ID = I.object_ID
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID
				AND I.Index_ID = IC.Index_ID
				AND I.Is_Primary_Key = 1
				AND S.Name = @cSchemaName
				AND O.Name = @cTableName
		)

	SET @cCursorSQL1 = 'INSERT INTO @oTable'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + '(PrimaryKey, AuditData)'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab1 + 'SELECT'
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + 'T.' + @cPrimaryKeyFieldName + ','
	SET @cCursorSQL1 = @cCursorSQL1 + @cCRLF + @cTab2 + '(SELECT T.* FOR XML RAW(''Root''), ELEMENTS)'

	SET @cCursorSQL2 = 'DECLARE ' + @cCursorName + ' CURSOR FOR'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SELECT'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab3 + 'T.PrimaryKey,'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab3 + 'T.AuditData'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'FROM @oTable T'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'OPEN ' + @cCursorName
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'FETCH ' + @cCursorName + ' INTO @nPrimaryKey, @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'WHILE @@fetch_status = 0'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'BEGIN'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SET @oXML.modify(''delete (/Root/UpdateJLLISUserID)'')'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'UPDATE @oTable'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'SET AuditData = @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'WHERE PrimaryKey = @nPrimaryKey'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'FETCH ' + @cCursorName + ' INTO @nPrimaryKey, @oXML'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab2 + 'END'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + '--END WHILE'
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'CLOSE ' + @cCursorName
	SET @cCursorSQL2 = @cCursorSQL2 + @cCRLF + @cTab1 + 'DEALLOCATE ' + @cCursorName

	SET @cInsertSQL = 'INSERT INTO dbo.AuditLog'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SchemaName,TableName,EntityID,AuditAction,AuditData,UpdateJLLISUserID,Title,FirstName,LastName)'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab1 + 'SELECT'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '''' + @cSchemaName + ''','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '''' + @cTableName + ''','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T1.' + @cPrimaryKeyFieldName + ','
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '@cAuditAction,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T2.AuditData,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + 'T1.UpdateJLLISUserID,'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.Title FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID),'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID),'
	SET @cInsertSQL = @cInsertSQL + @cCRLF + @cTab2 + '(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) WHERE JU.JLLISUserID = T1.UpdateJLLISUserID)'

	SET @cWhereSQL = @cTab1 + 'JOIN @oTable T2 ON T2.PrimaryKey = T1.' + @cPrimaryKeyFieldName
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab3 + 'AND NOT EXISTS'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + '('
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'SELECT 1'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'FROM dbo.AuditLog AL1 WITH (NOLOCK)'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + 'WHERE AL1.AuditAction = @cAuditAction'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab5 + 'AND CAST(AL1.AuditData as varchar(max)) = CAST(@oXML as varchar(max))'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.EntityID = T1.' + @cPrimaryKeyFieldName
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.SchemaName = ''' + @cSchemaName + ''''
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.TableName = ''' + @cTableName + ''''
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab6 + 'AND AL1.AuditLogID ='
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + '('
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'SELECT TOP 1'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AL2.AuditLogID'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'FROM dbo.AuditLog AL2 WITH (NOLOCK)'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'WHERE AL2.EntityID = AL1.EntityID'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AND AL2.SchemaName = AL1.SchemaName'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab8 + 'AND AL2.TableName = AL1.TableName'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + 'ORDER BY AL2.AuditLogID DESC'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab7 + ')'
	SET @cWhereSQL = @cWhereSQL + @cCRLF + @cTab4 + ')'

	IF @cPrimaryKeyFieldName IS NULL
		BEGIN
		
		print 'Cannot create trigger on table ' + @cTargetTable + '.  The table has no primary key.'
		
		END
	ELSE IF @nHasUpdateJLLISUserID = 0
		BEGIN
		
		print 'Cannot create trigger on table ' + @cTargetTable + '.  The table does not have an UpdateJLLISUserID field.'
		
		END
	ELSE
		BEGIN
		
		SET @cTriggerName = @cSchemaName + '.TR_' + @cTableName + '_AuditLog'
		SET @cSQL = 'IF EXISTS (SELECT 1 FROM sys.triggers T WITH (NOLOCK) WHERE T.object_id = OBJECT_ID(''' + @cTriggerName + '''))'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DROP TRIGGER ' + @cTriggerName
		
		IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
			print @cSQL
		ELSE
			EXEC (@cSQL)
		--ENDIF
		
		SET @cSQL = 'CREATE TRIGGER ' + @cTriggerName + ' ON ' + @cTargetTable + ' FOR INSERT, UPDATE, DELETE AS'
		SET @cSQL = @cSQL + @cCRLF + 'SET ARITHABORT ON'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @cAuditAction varchar(50)'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @cMemoryTableName varchar(50)'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nHaveDeleted bit'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nHaveInserted bit'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @nPrimaryKey int'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @oXML xml'
		SET @cSQL = @cSQL + @cCRLF + 'DECLARE @oTable table (PrimaryKey int, AuditData xml)'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'SET @nHaveDeleted = 0'
		SET @cSQL = @cSQL + @cCRLF + 'SET @nHaveInserted = 0'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'IF EXISTS (SELECT 1 FROM Deleted)'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @nHaveDeleted = 1'
		SET @cSQL = @cSQL + @cCRLF + 'IF EXISTS (SELECT 1 FROM Inserted)'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + 'IF @nHaveDeleted = 1 AND @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''UPDATE'''
		SET @cSQL = @cSQL + @cCRLF + 'ELSE IF @nHaveDeleted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''DELETE'''
		SET @cSQL = @cSQL + @cCRLF + 'ELSE IF @nHaveInserted = 1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SET @cAuditAction = ''INSERT'''
		SET @cSQL = @cSQL + @cCRLF

		SET @cMemoryTableName = 'Deleted'

		SET @cSQL = @cSQL + @cCRLF + 'IF @cAuditAction = ''DELETE'''
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'BEGIN'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL1
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL2
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cInsertSQL
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cWhereSQL
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'END'

		SET @cMemoryTableName = 'Inserted'

		SET @cSQL = @cSQL + @cCRLF + 'ELSE'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'BEGIN'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL1
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T'
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cCursorSQL2
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cInsertSQL
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cMemoryTableName + ' T1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + @cWhereSQL
		SET @cSQL = @cSQL + @cCRLF
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'END'
		SET @cSQL = @cSQL + @cCRLF + '--ENDIF'
		
		IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
			print @cSQL
		ELSE
			EXEC (@cSQL)
		--ENDIF

		print 'Trigger ' + @cTriggerName + ' has been created on table ' + @cTargetTable + '.'

		IF EXISTS (SELECT 1 FROM sys.triggers T WITH (NOLOCK) WHERE T.object_id = OBJECT_ID('TR_AuditLog_AuditLog'))
			BEGIN
			
			DROP TRIGGER TR_AuditLog_AuditLog
	
			print 'Trigger TR_AuditLog_AuditLog has been dropped from table dbo.AuditLog.'

			END
		--ENDIF
		
		END
	--ENDIF
	
END
GO
--End create EnableAuditing stored procedure