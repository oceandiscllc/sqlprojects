USE JSCC
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
  DROP TABLE #tTable
--ENDIF
GO

CREATE TABLE #tTable
	(
	ClassificationDataID INT,
	IssueID INT,
	LMSID INT
	)
	
INSERT INTO #tTable
	(ClassificationDataID,IssueID)
SELECT
	I.IssueClassificationDataID,
	I.IssueID
FROM JLLIS.dbo.Issue I
WHERE EXISTS
	(
	SELECT 1
	FROM dbo.LMS L
	WHERE L.LMSClassificationDataID = I.IssueClassificationDataID
		AND LMSClassificationDataID > 0
	)
	
UPDATE T
SET T.LMSID = L.LMSID
FROM #tTable T
	JOIN dbo.LMS L ON L.LMSClassificationDataID = T.ClassificationDataID
	
SELECT 
	T.LMSID, 
	T.IssueID, 
	CONVERT(char(11), I.CreateDate, 113) AS IssueCreateDate,
	T1.TierLabel AS [Owning Organization], 
	T2.TierLabel AS [Tier I Organization (Instance)], 
	T3.TierLabel AS OPR
FROM #tTable T
	JOIN JLLIS.dbo.Issue I ON I.IssueID = T.IssueID
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = I.OriginatingTierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
	JOIN JLLIS.dbo.Tier T3 ON T3.TierID = I.OPRTierID
ORDER BY I.CreateDate, T.LMSID

DROP TABLE #tTable
GO