-- The GLOBAL / ORCHID Model --
USE JLLIS

DECLARE @cInstanceType varchar (50)
DECLARE @nInstanceTierID int
DECLARE @nTierID int
DECLARE @oTable table (TierID int)

--Step 1:  Find out what tier I am in
SET @nTierID = (SELECT T.TierID FROM dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'NGA');

--Step 2:  Get my progenitor
WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable (TierID) SELECT TierID FROM HD

SET @cInstanceType = (SELECT TOP 1 T1.InstanceType FROM dbo.Tier T1 WITH (NOLOCK) JOIN @oTable T2 ON T2.TierID = T1.TierID AND T1.IsInstance = 1)
SET @nInstanceTierID = (SELECT TOP 1 T1.TierID FROM dbo.Tier T1 WITH (NOLOCK) JOIN @oTable T2 ON T2.TierID = T1.TierID AND T1.IsInstance = 1)

--Step 3:  Get my descendants for the dropdown

WITH HD (InstanceType,DisplayOrder,TierLineage,TierName,TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		T.InstanceType,
		CONVERT(varchar(255), T.TierName),
		CAST(T.TierID as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		1 
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nInstanceTierID

	UNION ALL 
	
	SELECT
		HD.InstanceType,
		CONVERT(varchar(255), RTRIM(DisplayOrder) + ',' + T.TierName),
		CAST(HD.TierLineage + ',' + CAST(T.TierID as varchar(50)) as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
SELECT 

	CASE
		WHEN @cInstanceType = 'GLOBAL' OR HD.TierID = @nTierID
		THEN 1
		ELSE 0
	END AS IsSelected,

	HD.DisplayOrder,
	HD.TierLineage,
	HD.TierName,
	HD.TierID, 
	ABS(HD.NodeLevel) AS NodeLevel
FROM HD
ORDER BY HD.TierName, DisplayOrder