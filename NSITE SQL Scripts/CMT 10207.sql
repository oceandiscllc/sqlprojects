--Begin CMT 10207
USE JLLIS
GO

--Begin function dbo.GetClassificationAuthorityByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetClassificationAuthorityByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationAuthorityByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a classification authority for a specific classification data id
-- ==================================================================================================

CREATE FUNCTION dbo.GetClassificationAuthorityByClassificationDataID
(
@nClassificationDataID int
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cRetVal varchar(max)

SELECT 
	@cRetVal = ISNULL(CD.ClassificationAuthority, '')
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @cRetVal
END
GO
--End function dbo.GetClassificationAuthorityByClassificationDataID

--Begin function dbo.GetClassificationSourceByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetClassificationSourceByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationSourceByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a classification source for a specific classification data id
-- ===============================================================================================

CREATE FUNCTION dbo.GetClassificationSourceByClassificationDataID
(
@nClassificationDataID int
)

RETURNS varchar(50)

AS
BEGIN

DECLARE @cRetVal varchar(50)

SELECT 
	@cRetVal = ISNULL(CD.ClassificationSource, '')
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @cRetVal
END
GO
--End function dbo.GetClassificationSourceByClassificationDataID

--Begin function dbo.GetDeclassificationDateByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetDeclassificationDateByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetDeclassificationDateByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a declassification date for a specific classification data id
-- ===============================================================================================

CREATE FUNCTION dbo.GetDeclassificationDateByClassificationDataID
(
@nClassificationDataID int
)

RETURNS datetime

AS
BEGIN

DECLARE @dRetVal datetime

SELECT 
	@dRetVal = DeclassificationDate
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @dRetVal
END
GO
--End function dbo.GetDeclassificationDateByClassificationDataID

--Begin procedure Reporting.ClassificationReasonsByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ClassificationReasonsByClassificationDataID') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ClassificationReasonsByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A procedure to return classification reasons for a specific classification data id
-- ===============================================================================================

CREATE PROCEDURE Reporting.ClassificationReasonsByClassificationDataID
(
@nClassificationDataID int
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		CR.ClassificationReason
	FROM Dropdown.ClassificationReason CR
		JOIN dbo.ClassificationDataClassificationReason CDCR ON CDCR.ClassificationReasonID = CR.ClassificationReasonID
			AND CDCR.ClassificationDataID = @nClassificationDataID
	ORDER BY CR.DisplayOrder, CR.ClassificationReason

END	
GO
--End procedure Reporting.ClassificationReasonsByClassificationDataID

USE NAVY
GO

--Begin procedure Reporting.ObservationsByKeyword
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationsByKeyword') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationsByKeyword
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.01.05
-- Description:	A stored procedure to return LMS records containing a specified keyword
--
-- Modify Date:	2012.02.01
-- Description:	Added logic to limit the number of characters returned
--
-- Modify Date:	2012.02.13
-- Description:	Added additional fields to the result set
--
-- Modify Date:	2012.02.27
-- Description:	Added an optional truncation bit
-- ====================================================================================
CREATE PROCEDURE Reporting.ObservationsByKeyword
	@nKeywordSearchID int,
	@cEntityTypeCode varchar(50),
	@nTruncateData bit = 1

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nMaxLength int
	SET @nMaxLength = 32767

	SELECT
		C.Country,
		E.Event, 
		ET.EventType,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.BackgroundClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Background), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.BackgroundClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Background)
		END AS Discussion,

		CONVERT(char(11), L.EventDate, 113) AS EventDateFormatted,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.EventDescriptionClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.EventDescriptionClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription)
		END AS EventDescription, 
		
		CONVERT(char(11), L.DocumentDate, 113) AS DocumentDateFormatted,
		L.CreatedBy,
		L.FirstName + ' ' + L.LastName AS PostedBy,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ImplicationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Implications), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ImplicationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Implications)
		END AS Implications,

		L.ImportCode,
		L.ImportSource,
		L.IsDraft,
		L.JointLesson,
		L.LegacyID,
		L.LMSID,
		L.LMSUnit,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS LMSCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS LMSClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS LMSReleasableTo,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ObservationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Observations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ObservationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Observations)
		END AS Observation,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.RecommendationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.RecommendationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations)
		END AS Recommendation,

		L.Status, 

		JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID) AS CommentCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID) AS CommentClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID) AS CommentReleasableTo,
		
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.SummaryClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Summary), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.SummaryClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Summary)
		END AS Comment,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS TopicCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS TopicClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS TopicReleasableTo,
		JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.LMSClassificationDataID, 1) + ' ' + L.Topic AS Topic,

		JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationAuthority,
		JLLIS.dbo.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationSource,
		CONVERT(char(11), JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID), 113) AS LMSDeclassificationDateFormatted,

		L.Unit, 
		L.ViewCount,
		T.TierLabel AS Organiztion
	FROM dbo.LMS L
		JOIN Dropdown.Event E ON E.EventID = L.EventID
		JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
		JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = L.CountryID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = @cEntityTypeCode 
			AND KSR.EntityID = L.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	ORDER BY L.LMSID DESC

END	
GO
--End procedure Reporting.ObservationsByKeyword
--End CMT 10207
