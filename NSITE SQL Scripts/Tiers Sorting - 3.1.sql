DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(T.TierID) AS VARCHAR(50)))
FROM JLLIS.dbo.Tier T

;
WITH HD (DisplayIndex,InstanceID,TierLineage,TierID,ParentTierID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY T.TierLabel) AS VARCHAR(10)), @nPadLength)),
		T.InstanceID,
		CAST(T.TierLabel AS VARCHAR(MAX)),
		T.TierID,
		T.ParentTierID,
		1
	FROM JLLIS.dbo.Tier T
	WHERE T.ParentTierID = 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY T.TierLabel) AS VARCHAR(10)), @nPadLength)),
		T.InstanceID,
		CAST(HD.TierLineage  + ' > ' + T.TierLabel AS VARCHAR(MAX)),
		T.TierID,
		T.ParentTierID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.Tier T
		JOIN HD ON HD.TierID = T.ParentTierID
	)

SELECT
	HD1.InstanceID,
	HD1.NodeLevel,
	HD1.ParentTierID,
	HD1.TierLineage,
	HD1.TierID,
	T2.TierLabel,
	T2.TierName

FROM HD HD1
	LEFT JOIN JLLIS.dbo.Tier T1 ON T1.TierID = HD1.InstanceID
	LEFT JOIN JLLIS.dbo.Tier T2 ON T2.TierID = HD1.TierID
ORDER BY HD1.DisplayIndex
