USE SOCOM
GO

SELECT
	'SELECT ' + C.Name + ' FROM ' + S.Name + '.' + O.Name + ' WHERE ' + C.Name + ' NOT IN (''Unclass'',''Unclassified'') AND (' + C.Name + ' IS NOT NULL OR LEN(LTRIM(RTRIM(' + C.Name + '))) > 0) ORDER BY ' + C.Name AS SelectSQLText,
	'UPDATE ' + S.Name + '.' + O.Name + ' SET ' + C.Name + ' = NULL WHERE LEN(LTRIM(RTRIM(' + C.Name + '))) = 0 OR ' + C.Name + ' IN (''= = = = = = = ='',''DanTest'',''NA'',''NFWN'',''null'',''OC'',''RD'',''REL'',''RELEASABLE'')' AS UpdateSQLText,
	'DELETE FROM ' + S.Name + '.' + O.Name + ' WHERE ' + C.Name + ' NOT IN (''Unclass'',''Unclassified'') AND (' + C.Name + ' IS NOT NULL OR LEN(LTRIM(RTRIM(' + C.Name + '))) > 0)' AS DeleteSQLText
FROM sys.objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.Columns C WITH (NOLOCK) ON O.Object_ID = C.Object_ID
	JOIN sys.Types T WITH (NOLOCK) ON C.User_Type_ID = T.User_Type_ID
		AND O.type = 'U' 
	JOIN JLLIS.Utility.ClassificationDataMapping CDM ON CDM.SchemaName = S.Name
		AND CDM.TableName = O.Name
		AND CDM.ColumnName = C.name
ORDER BY S.Name, O.Name, C.Name
GO