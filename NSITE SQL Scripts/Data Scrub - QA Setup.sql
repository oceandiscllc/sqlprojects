UPDATE JLLIS.dbo.Tier SET AutoRegister = 0
GO

UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'QASQL.nsitellc.com' WHERE ServerSetupKey = 'DatabaseServerDomainName'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = '10.10.20.71' WHERE ServerSetupKey = 'DatabaseServerIPV4Address'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'QA' WHERE ServerSetupKey = 'NetworkName'
GO
/*
qa ntier
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'https://qa-jllis.nsitellc.com' WHERE ServerSetupKey = 'serverURL'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = '10.10.20.103' WHERE ServerSetupKey = 'SolrServerIPV4Address'
GO
*/
/*
qa socom
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'https://qa-socom.nsitellc.com' WHERE ServerSetupKey = 'serverURL'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = '10.10.20.104' WHERE ServerSetupKey = 'SolrServerIPV4Address'
GO
*/
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = ')(*&UIOP0987uiop' WHERE ServerSetupKey = 'SSRSPassword'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'ReportServer' WHERE ServerSetupKey = 'SSRSReportServerPath'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = 'ssrsadmin' WHERE ServerSetupKey = 'SSRSUsername'
UPDATE JLLIS.dbo.ServerSetup SET ServerSetupValue = '0' WHERE ServerSetupKey = 'UseCAC'
GO

UPDATE JSCC.dbo.Setup SET NetworkType = 'QA'
GO
