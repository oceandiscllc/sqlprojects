SELECT 
	JU.JLLISUserID,
	TU.TierUserID,
	C.ISOCode3,
	JU.OriginatingTierID,
	JU.JLLISUserName,
	JU.LastName,
	JU.FirstName,
	TU.SecLev,
	TU.DefaultTierID,
	T.TierName AS DefaultTierName,
	T.TierLabel AS DefaultTierLabel
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.Dropdown.Country C ON C.CountryID = JU.CountryID
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
	JOIN JLLIS.dbo.Tier T ON T.TierID = TU.DefaultTierID
		AND TU.TierID = JLLIS.dbo.GetTierUserInstanceID()
		AND TU.Status = 'Active'
		AND TU.SecLev = 7000
ORDER BY JU.LastName, JU.FirstName, JU.JLLISUserID, T.TierName
