--DOTMLPF 
SELECT
	SMD1.DisplayOrder,
	SMD1.SystemMetadataID,
	SMD1.SystemMetadata
FROM JLLIS.Dropdown.SystemMetadata SMD1
	JOIN JLLIS.Dropdown.SystemMetadata SMD2 ON SMD2.SystemMetadataID = SMD1.ParentSystemMetadataID
		AND SMD2.SystemMetadata = 'DOTMLPF & Policy'
ORDER BY SMD1.DisplayOrder

--JCA
USE JSCC
GO

DECLARE @nDropdownMetaDataID int
DECLARE @nRootTierID int
DECLARE @nTierID int

SET @nDropdownMetaDataID = 
	(
	SELECT 
		DMD.DropdownMetaDataID 
	FROM JLLIS.Dropdown.DropdownMetadata DMD
	WHERE DMD.DropdownMetadataCode = 'Metadata' 
	)

SET @nRootTierID = 
	(
	SELECT TOP 1
		T2.TierID 
	FROM JLLIS.dbo.Tier T1 
		JOIN JLLIS.dbo.Tier T2 on T2.TierID = T1.InstanceID
		  AND T2.TierName = 'Navy'
		  AND T2.IsInstance = 1
	)

DECLARE @oTable1 table
	(
	DisplayOrder1 int NOT NULL DEFAULT 0,
	DisplayOrder2 int NOT NULL DEFAULT 0,
	IsRequired bit NOT NULL DEFAULT 0,
	IsSelected bit NOT NULL DEFAULT 0,
	Description varchar(max),
	MetaDataID int,
	MetaData varchar(250),
	MetadataTypeID int,
	MetadataType varchar(250)
	)

DECLARE @oTable2 table
	(
	TierID int,
	NodeLevel int primary key
	)

;
WITH HD (TierID,ParentTierID,NodeLevel)
	AS
	(
	SELECT
		T.TierID,
		T.ParentTierID,
		1
	FROM JLLIS.dbo.Tier T
	WHERE T.TierID = @nRootTierID

	UNION ALL

	SELECT
		T.TierID,
		T.ParentTierID,
		HD.NodeLevel + 1
	FROM JLLIS.dbo.Tier T
		JOIN HD ON HD.ParentTierID = T.TierID
	)

INSERT INTO @oTable2
	(TierID, NodeLevel)
SELECT
	HD.TierID,
	HD.NodeLevel
FROM HD
	JOIN JLLIS.Dropdown.TierDropdownMetadata TDMD ON TDMD.TierID = HD.TierID
	JOIN JLLIS.Dropdown.DropdownMetadata DMD ON DMD.DropdownMetadataID = TDMD.DropdownMetadataID
		AND DMD.DropdownMetadataCode = 'Metadata'
ORDER BY HD.NodeLevel

SET @nTierID = (SELECT TOP 1 T2.TierID FROM @oTable2 T2 ORDER BY T2.NodeLevel)

INSERT INTO @oTable1
	(
	DisplayOrder1,
	DisplayOrder2,
	
		Description,
	
		IsRequired,
	MetaDataID,
	MetaData,
	MetadataTypeID,
	MetadataType
	)
SELECT
	
		TTD.DisplayOrder AS DisplayOrder1,
		BTD.DisplayOrder AS DisplayOrder2,
	
		BT.Description,
	
		TT.IsRequired,
	
	BT.MetaDataID,
	BT.MetaData,
	TT.MetadataTypeID,
	TT.MetadataType
FROM Dropdown.MetaData BT
	JOIN Dropdown.MetadataType TT ON TT.MetadataTypeID = BT.MetadataTypeID
		AND BT.MetaDataID > 0
		AND BT.IsActive = 1 
			JOIN Dropdown.TierDropdown TTD ON TTD.DropdownEntityID = TT.MetadataTypeID
				AND TTD.DropdownMetadataID = (SELECT DMD.DropdownMetadataID FROM JLLIS.Dropdown.DropdownMetadata DMD WHERE DMD.DropdownMetadataCode = 'MetadataType')
				AND TTD.TierID = @nRootTierID
			JOIN Dropdown.TierDropdown BTD ON BTD.DropdownEntityID = BT.MetaDataID
				AND BTD.DropdownMetadataID = @nDropdownMetaDataID
				AND BTD.TierID = @nRootTierID 
			AND EXISTS
				(
				SELECT 1
				FROM Dropdown.TierDropdown TD
				WHERE TD.DropdownEntityID = BT.MetaDataID
					AND TD.DropdownMetadataID = @nDropdownMetaDataID
					AND TD.TierID = @nTierID
				)
		

SELECT
	T1.DisplayOrder1,
	T1.DisplayOrder2,
	T1.Description,
	T1.IsRequired,
	T1.IsSelected,
	T1.MetaDataID,
	T1.MetaData,
	T1.MetadataTypeID,
	T1.MetadataType
FROM @oTable1 T1
WHERE T1.MetadataType = 'JCA'
ORDER BY
	DisplayOrder1,
	MetadataType,
	MetadataTypeID,
	DisplayOrder2,
	MetaData,
	MetaDataID 
