USE USSOCOM
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WITH (NOLOCK) WHERE name = 'countryid' AND Object_ID = OBJECT_ID(N'dbo.cdr'))
	ALTER TABLE dbo.cdr ADD countryid int NOT NULL DEFAULT 0
	
IF NOT EXISTS (SELECT 1 FROM sys.columns WITH (NOLOCK) WHERE name = 'countryid' AND Object_ID = OBJECT_ID(N'dbo.sodars'))
	ALTER TABLE dbo.sodars ADD countryid int NOT NULL DEFAULT 0			
	
IF EXISTS (SELECT 1 FROM dbo.sysobjects WITH (NOLOCK) WHERE id = OBJECT_ID(N'dbo.commands') AND type in (N'U'))
	BEGIN
	
	DROP TABLE dbo.commands
	
	END
--ENDIF

CREATE TABLE dbo.commands
	(
	commandid int IDENTITY(1,1) NOT NULL,
	commandname varchar(250) NOT NULL,
	commandtypecode varchar(50) NOT NULL,
	displayorder int CONSTRAINT DF_commands_displayorder NOT NULL DEFAULT 0,
	createdatetime datetime CONSTRAINT DF_commands_createdatetime NOT NULL DEFAULT getDate(),
	createuserid int CONSTRAINT DF_commands_createuserid NOT NULL DEFAULT 0,
	updatetime datetime CONSTRAINT DF_commands_updatetime NOT NULL DEFAULT getDate(),
	updateuserid int CONSTRAINT DF_commands_updateuserid NOT NULL DEFAULT 0,
	CONSTRAINT PK_commands PRIMARY KEY CLUSTERED 
		(
		commandid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] 
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WITH (NOLOCK) WHERE id = OBJECT_ID(N'dbo.countries') AND type in (N'U'))
	BEGIN
	
	DROP TABLE dbo.countries
	
	END
--ENDIF
	
CREATE TABLE dbo.countries
	(
	countryid int IDENTITY(1,1) NOT NULL,
	countryname varchar(250) NOT NULL,
	isocode2 char(2) NULL,
	isocode3 char(3) NULL,
	region varchar(250) NULL,
	subregion varchar(250) NULL,
	cdhid int CONSTRAINT DF_countries_cdhid NOT NULL DEFAULT 0,
	lat varchar(10) NULL,
	long varchar(10) NULL,
	displayorder int CONSTRAINT DF_countries_displayorder NOT NULL DEFAULT 0,
	createdatetime datetime CONSTRAINT DF_countries_createdatetime NOT NULL DEFAULT getDate(),
	createuserid int CONSTRAINT DF_countries_createuserid NOT NULL DEFAULT 0,
	updatetime datetime CONSTRAINT DF_countries_updatetime NOT NULL DEFAULT getDate(),
	updateuserid int CONSTRAINT DF_countries_updateuserid NOT NULL DEFAULT 0,
	CONSTRAINT PK_Countries PRIMARY KEY NONCLUSTERED 
		(
		countryid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] 
	
CREATE UNIQUE CLUSTERED INDEX IX_CountryName ON dbo.countries 
	(
	displayorder ASC,
	countryname ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.commandcountryassociations') AND type in (N'U'))
	BEGIN
	
	DROP TABLE dbo.commandcountryassociations
	
	END
--ENDIF

CREATE TABLE dbo.commandcountryassociations
	(
	commandcountryassociationid int IDENTITY(1,1) NOT NULL,
	commandid int CONSTRAINT DF_commandcountryAssociations_CommandID NOT NULL DEFAULT 0,
	countryid int CONSTRAINT DF_commandcountryAssociations_CountryID NOT NULL DEFAULT 0,
	CONSTRAINT PK_commandcountryassociations PRIMARY KEY NONCLUSTERED 
		(
		commandcountryassociationid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE UNIQUE CLUSTERED INDEX IX_commandcountry ON dbo.commandcountryassociations 
	(
	CommandID ASC,
	CountryID ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

SET IDENTITY_INSERT dbo.Commands ON
INSERT INTO dbo.Commands (CommandID, CommandName, CommandtypeCode) VALUES (0, '', '')
SET IDENTITY_INSERT dbo.Commands OFF

INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('NORTHCOM', 'UNIFIED')
INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('PACCOM', 'UNIFIED')
INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('SOUTHCOM', 'UNIFIED')
INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('AFRICACOM', 'UNIFIED')
INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('CENTCOM', 'UNIFIED')
INSERT INTO dbo.Commands (CommandName, CommandtypeCode) VALUES ('EUCOM', 'UNIFIED')
GO

SET IDENTITY_INSERT dbo.Countries ON
INSERT INTO dbo.Countries (CountryID, CountryName) VALUES (0, '')
SET IDENTITY_INSERT dbo.Countries OFF

INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Afghanistan','AF','AFG','Asia','Southern Asia',2,'33 00 N','65 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Albania','AL','ALB','Europe','Southern Europe',4,'41 00 N','20 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Algeria','DZ','DZA','Africa','Northern Africa',5,'28 00 N','3 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('American Samoa','AS','ASM','Oceania','Polynesia',6,'14 20 S','170 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Andorra','AD','AND','Europe','Southern Europe',7,'42 30 N','1 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Angola','AO','AGO','Africa','Middle Africa',8,'12 30 S','18 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Anguilla','AI','AIA','Americas','Caribbean',9,'18 15 N','63 10 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Antarctica','AQ','ATA',NULL,NULL,10,'90 00 S','0 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Antigua and Barbuda','AG','ATG','Americas','Caribbean',11,'17 03 N','61 48 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Argentina','AR','ARG','Americas','South America',12,'34 00 S','64 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Armenia','AM','ARM','Asia','Western Asia',13,'40 00 N','45 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Aruba','AW','ABW','Americas','Caribbean',14,'12 30 N','69 58 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Australia','AU','AUS','Oceania','Australia and New Zealand',15,'27 00 S','133 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Austria','AT','AUT','Europe','Western Europe',16,'47 20 N','13 20 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Azerbaijan','AZ','AZE','Asia','Western Asia',17,'40 30 N','47 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bahamas','BS','BHS','Americas','Caribbean',18,'24 15 N','76 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bahrain','BH','BHR','Asia','Western Asia',21,'26 00 N','50 33 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bangladesh','BD','BGD','Asia','Southern Asia',22,'24 00 N','90 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Barbados','BB','BRB','Americas','Caribbean',23,'13 10 N','59 32 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belarus','BY','BLR','Europe','Eastern Europe',24,'53 00 N','28 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belgium','BE','BEL','Europe','Western Europe',25,'50 50 N','4 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belize','BZ','BLZ','Americas','Central America',26,'17 15 N','88 45 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Benin','BJ','BEN','Africa','Western Africa',27,'9 30 N','2 15 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bermuda','BM','BMU','Americas','Northern America',28,'32 20 N','64 45 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bhutan','BT','BTN','Asia','Southern Asia',29,'27 30 N','90 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bolivia','BO','BOL','Americas','South America',30,'17 00 S','65 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bosnia and Herzegovina','BA','BIH','Europe','Southern Europe',31,'44 00 N','18 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Botswana','BW','BWA','Africa','Southern Africa',32,'22 00 S','24 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bouvet Island','BV','BVT',NULL,NULL,33,'54 26 S','3 24 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Brazil','BR','BRA','Americas','South America',34,'10 00 S','55 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('British Indian Ocean Territory','IO','IOT','Africa','Eastern Africa',35,'6 00 S','71 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Brunei','BN','BRN','Asia','South-Eastern Asia',36,'4 30 N','114 40 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bulgaria','BG','BGR','Europe','Eastern Europe',37,'43 00 N','25 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Burkina Faso','BF','BFA','Africa','Western Africa',38,'13 00 N','2 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Burundi','BI','BDI','Africa','Eastern Africa',39,'3 30 S','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cambodia','KH','KHM','Asia','South-Eastern Asia',40,'13 00 N','105 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cameroon','CM','CMR','Africa','Middle Africa',41,'6 00 N','12 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Canada','CA','CAN','Americas','Northern America',42,'60 00 N','95 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cape Verde','CV','CPV','Africa','Western Africa',43,'16 00 N','24 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cayman Islands','KY','CYM','Americas','Caribbean',44,'19 30 N','80 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Central African Republic','CF','CAF','Africa','Middle Africa',45,'7 00 N','21 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Chad','TD','TCD','Africa','Middle Africa',46,'15 00 N','19 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Chile','CL','CHL','Americas','South America',47,'30 00 S','71 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('China','CN','CHN','Asia','Eastern Asia',48,'35 00 N','105 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Christmas Island','CX','CXR','Oceania','Australia and New Zealand',49,'10 30 S','105 40 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cocos (Keeling) Islands','CC','CCK','Oceania','Australia and New Zealand',50,'12 30 S','96 50 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Colombia','CO','COL','Americas','South America',51,'4 00 N','72 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Comoros','KM','COM','Africa','Eastern Africa',52,'12 10 S','44 15 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Congo','CG','COG','Africa','Middle Africa',53,'1 00 S','15 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Congo, The Democratic Republic Of The','CD','COD','Africa','Middle Africa',54,'0 00 N','25 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cook Islands','CK','COK','Oceania','Polynesia',55,'21 14 S','159 46 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Costa Rica','CR','CRI','Americas','Central America',56,'10 00 N','84 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Croatia','HR','HRV','Europe','Southern Europe',58,'45 10 N','15 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cuba','CU','CUB','Americas','Caribbean',59,'21 30 N','80 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cyprus','CY','CYP','Asia','Western Asia',60,'35 00 N','33 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Czech Republic','CZ','CZE','Europe','Eastern Europe',61,'49 45 N','15 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cote D''Ivoire','CI','CIV','Africa','Western Africa',57,'8 00 N','5 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Denmark','DK','DNK','Europe','Northern Europe',62,'56 00 N','10 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Djibouti','DJ','DJI','Africa','Eastern Africa',63,'11 30 N','43 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Dominica','DM','DMA','Americas','Caribbean',64,'15 25 N','61 20 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Dominican Republic','DO','DOM','Americas','Caribbean',65,'19 00 N','70 40 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('East Timor','TL','TLS','Asia','South-Eastern Asia',226,'8 50 S','125 55 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ecuador','EC','ECU','Americas','South America',66,'2 00 S','77 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Egypt','EG','EGY','Africa','Northern Africa',67,'27 00 N','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('El Salvador','SV','SLV','Americas','Central America',68,'13 50 N','88 55 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Equatorial Guinea','GQ','GNQ','Africa','Middle Africa',69,'2 00 N','10 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Eritrea','ER','ERI','Africa','Eastern Africa',70,'15 00 N','39 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Estonia','EE','EST','Europe','Northern Europe',71,'59 00 N','26 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ethiopia','ET','ETH','Africa','Eastern Africa',73,'8 00 N','38 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Falkland Islands (Malvinas)','FK','FLK','Americas','South America',74,'51 45 S','59 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Faroe Islands','FO','FRO','Europe','Northern Europe',75,'62 00 N','7 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Fiji','FJ','FJI','Oceania','Melanesia',76,'18 00 S','175 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Finland','FI','FIN','Europe','Northern Europe',77,'64 00 N','26 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('France','FR','FRA','Europe','Western Europe',78,'46 00 N','2 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Guiana','GF','GUF','Americas','South America',80,'4 00 N','53 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Polynesia','PF','PYF','Oceania','Polynesia',81,'15 00 S','140 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Southern Territories','TF','ATF',NULL,NULL,82,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gabon','GA','GAB','Africa','Middle Africa',83,'1 00 S','11 45 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gambia','GM','GMB','Africa','Western Africa',84,'13 28 N','16 34 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Georgia','GE','GEO','Asia','Western Asia',1,'42 00 N','43 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Germany','DE','DEU','Europe','Western Europe',85,'51 00 N','9 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ghana','GH','GHA','Africa','Western Africa',86,'8 00 N','2 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gibraltar','GI','GIB','Europe','Southern Europe',87,'36 08 N','5 21 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Greece','GR','GRC','Europe','Southern Europe',88,'39 00 N','22 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Greenland','GL','GRL','Americas','Northern America',89,'72 00 N','40 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Grenada','GD','GRD','Americas','Caribbean',90,'12 07 N','61 40 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guadeloupe','GP','GLP','Americas','Caribbean',91,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guam','GU','GUM','Oceania','Micronesia',92,'13 28 N','144 47 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guatemala','GT','GTM','Americas','Central America',93,'15 30 N','90 15 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guernsey','GG','GGY','Europe','Northern Europe',94,'49 28 N','2 35 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guinea','GN','GIN','Africa','Western Africa',95,'11 00 N','10 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guinea-Bissau','GW','GNB','Africa','Western Africa',96,'12 00 N','15 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guyana','GY','GUY','Americas','South America',97,'5 00 N','59 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Haiti','HT','HTI','Americas','Caribbean',98,'19 00 N','72 25 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Heard and McDonald Islands','HM','HMD',NULL,NULL,99,'53 06 S','72 31 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Honduras','HN','HND','Americas','Central America',101,'15 00 N','86 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Hong Kong','HK','HKG','Asia','Eastern Asia',102,'22 15 N','114 10 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Hungary','HU','HUN','Europe','Eastern Europe',103,'47 00 N','20 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iceland','IS','ISL','Europe','Northern Europe',104,'65 00 N','18 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('India','IN','IND','Asia','Southern Asia',105,'20 00 N','77 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Indonesia','ID','IDN','Asia','South-Eastern Asia',106,'5 00 S','120 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iran','IR','IRN','Asia','Southern Asia',107,'32 00 N','53 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iraq','IQ','IRQ','Asia','Western Asia',108,'33 00 N','44 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ireland','IE','IRL','Europe','Northern Europe',109,'53 00 N','8 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Isle of Man','IM','IMN','Europe','Northern Europe',110,'54 15 N','4 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Israel','IL','ISR','Asia','Western Asia',111,'31 30 N','34 45 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Italy','IT','ITA','Europe','Southern Europe',112,'42 50 N','12 50 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jamaica','JM','JAM','Americas','Caribbean',113,'18 15 N','77 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Japan','JP','JPN','Asia','Eastern Asia',114,'36 00 N','138 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jersey','JE','JEY','Europe','Northern Europe',115,'49 15 N','2 10 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jordan','JO','JOR','Asia','Western Asia',116,'31 00 N','36 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kazakhstan','KZ','KAZ','Asia','Central Asia',117,'48 00 N','68 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kenya','KE','KEN','Africa','Eastern Africa',118,'1 00 N','38 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kiribati','KI','KIR','Oceania','Micronesia',119,'1 25 N','173 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Korea, North','KP','PRK','Asia','Eastern Asia',121,'40 00 N','127 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Korea, South','KR','KOR','Asia','Eastern Asia',120,'37 00 N','127 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kosovo',NULL,NULL,'Europe','Southern Europe',0,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kuwait','KW','KWT','Asia','Western Asia',122,'29 30 N','45 45 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kyrgyzstan','KG','KGZ','Asia','Central Asia',123,'41 00 N','75 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Laos','LA','LAO','Asia','South-Eastern Asia',124,'18 00 N','105 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Latvia','LV','LVA','Europe','Northern Europe',125,'57 00 N','25 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lebanon','LB','LBN','Asia','Western Asia',126,'33 50 N','35 50 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lesotho','LS','LSO','Africa','Southern Africa',127,'29 30 S','28 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Liberia','LR','LBR','Africa','Western Africa',128,'6 30 N','9 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Libya','LY','LBY','Africa','Northern Africa',129,'25 00 N','17 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Liechtenstein','LI','LIE','Europe','Western Europe',130,'47 16 N','9 32 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lithuania','LT','LTU','Europe','Northern Europe',131,'56 00 N','24 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Luxembourg','LU','LUX','Europe','Western Europe',132,'49 45 N','6 10 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Macao','MO','MAC','Asia','Eastern Asia',133,'22 10 N','113 33 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Macedonia','MK','MKD','Europe','Southern Europe',134,'41 50 N','22 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Madagascar','MG','MDG','Africa','Eastern Africa',135,'20 00 S','47 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malawi','MW','MWI','Africa','Eastern Africa',136,'13 30 S','34 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malaysia','MY','MYS','Asia','South-Eastern Asia',137,'2 30 N','112 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Maldives','MV','MDV','Asia','Southern Asia',138,'3 15 N','73 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mali','ML','MLI','Africa','Western Africa',139,'17 00 N','4 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malta','MT','MLT','Europe','Southern Europe',140,'35 50 N','14 35 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Marshall Islands','MH','MHL','Oceania','Micronesia',141,'9 00 N','168 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Martinique','MQ','MTQ','Americas','Caribbean',142,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mauritania','MR','MRT','Africa','Western Africa',143,'20 00 N','12 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mauritius','MU','MUS','Africa','Eastern Africa',144,'20 17 S','57 33 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mayotte','YT','MYT','Africa','Eastern Africa',145,'12 50 S','45 10 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mexico','MX','MEX','Americas','Central America',146,'23 00 N','102 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Micronesia','FM','FSM','Oceania','Micronesia',147,'6 55 N','158 15 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Moldova','MD','MDA','Europe','Eastern Europe',148,'47 00 N','29 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Monaco','MC','MCO','Europe','Western Europe',149,'43 44 N','7 24 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mongolia','MN','MNG','Asia','Eastern Asia',150,'46 00 N','105 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Montenegro','ME','MNE','Europe','Southern Europe',151,'42 30 N','19 18 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Montserrat','MS','MSR','Americas','Caribbean',152,'16 45 N','62 12 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Morocco','MA','MAR','Africa','Northern Africa',153,'32 00 N','5 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mozambique','MZ','MOZ','Africa','Eastern Africa',154,'18 15 S','35 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Myanmar','MM','MMR','Asia','South-Eastern Asia',155,'22 00 N','98 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Namibia','NA','NAM','Africa','Southern Africa',156,'22 00 S','17 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nauru','NR','NRU','Oceania','Micronesia',157,'0 32 S','166 55 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nepal','NP','NPL','Asia','Southern Asia',158,'28 00 N','84 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Netherlands','NL','NLD','Europe','Western Europe',159,'52 30 N','5 45 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Netherlands Antilles','AN','ANT','Americas','Caribbean',160,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('New Caledonia','NC','NCL','Oceania','Melanesia',161,'21 30 S','165 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('New Zealand','NZ','NZL','Oceania','Australia and New Zealand',162,'41 00 S','174 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nicaragua','NI','NIC','Americas','Central America',163,'13 00 N','85 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Niger','NE','NER','Africa','Western Africa',164,'16 00 N','8 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nigeria','NG','NGA','Africa','Western Africa',165,'10 00 N','8 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Niue','NU','NIU','Oceania','Polynesia',166,'19 02 S','169 52 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Norfolk Island','NF','NFK','Oceania','Australia and New Zealand',167,'29 02 S','167 57 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Northern Mariana Islands','MP','MNP','Oceania','Micronesia',168,'15 12 N','145 45 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Norway','NO','NOR','Europe','Northern Europe',169,'62 00 N','10 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Oman','OM','OMN','Asia','Western Asia',170,'21 00 N','57 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Pakistan','PK','PAK','Asia','Southern Asia',171,'30 00 N','70 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Palau','PW','PLW','Oceania','Micronesia',172,'7 30 N','134 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Palestinian Territory, Occupied','PS','PSE','Asia','Western Asia',173,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Panama','PA','PAN','Americas','Central America',175,'9 00 N','80 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Papua New Guinea','PG','PNG','Oceania','Melanesia',176,'6 00 S','147 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Paraguay','PY','PRY','Americas','South America',177,'23 00 S','58 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Peru','PE','PER','Americas','South America',178,'10 00 S','76 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Philippines','PH','PHL','Asia','South-Eastern Asia',180,'13 00 N','122 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Pitcairn','PN','PCN','Oceania','Polynesia',182,'25 04 S','130 06 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Poland','PL','POL','Europe','Eastern Europe',183,'52 00 N','20 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Portugal','PT','PRT','Europe','Southern Europe',184,'39 30 N','8 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Puerto Rico','PR','PRI','Americas','Caribbean',185,'18 15 N','66 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Qatar','QA','QAT','Asia','Western Asia',186,'25 30 N','51 15 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Romania','RO','ROU','Europe','Eastern Europe',189,'46 00 N','25 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Russia','RU','RUS','Europe','Eastern Europe',190,'60 00 N','100 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Rwanda','RW','RWA','Africa','Eastern Africa',191,'2 00 S','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('R�union','RE','REU','Africa','Eastern Africa',187,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Barth�lemy','BL','BLM','Americas','Caribbean',300,'17 90 N','62 85 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Helena, Ascension and Tristan Da Cunha','SH','SHN','Africa','Western Africa',192,'15 57','5 42 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Kitts And Nevis','KN','KNA','Americas','Caribbean',193,'17 20 N','62 45 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Lucia','LC','LCA','Americas','Caribbean',194,'13 53 N','60 58 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Martin','MF','MAF','Americas','Caribbean',301,'18 05 N','63 57 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Pierre And Miquelon','PM','SPM','Americas','Northern America',181,'46 50 N','56 20 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Vincent And The Grenadines','VC','VCT','Americas','Caribbean',195,'13 15 N','61 12 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Samoa','WS','WSM','Oceania','Polynesia',196,'13 35 S','172 20 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('San Marino','SM','SMR','Europe','Southern Europe',197,'43 46 N','12 25 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sao Tome and Principe','ST','STP','Africa','Middle Africa',198,'1 00 N','7 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saudi Arabia','SA','SAU','Asia','Western Asia',199,'25 00 N','45 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Senegal','SN','SEN','Africa','Western Africa',200,'14 00 N','14 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Serbia','RS','SRB','Europe','Southern Europe',201,'44 00 N','21 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Seychelles','SC','SYC','Africa','Eastern Africa',202,'4 35 S','55 40 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sierra Leone','SL','SLE','Africa','Western Africa',203,'8 30 N','11 30 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Singapore','SG','SGP','Asia','South-Eastern Asia',205,'1 22 N','103 48 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Slovakia','SK','SVK','Europe','Eastern Europe',206,'48 40 N','19 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Slovenia','SI','SVN','Europe','Southern Europe',207,'46 07 N','14 49 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Solomon Islands','SB','SLB','Oceania','Melanesia',208,'8 00 S','159 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Somalia','SO','SOM','Africa','Eastern Africa',209,'10 00 N','49 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('South Africa','ZA','ZAF','Africa','Southern Africa',210,'29 00 S','24 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('South Georgia and the South Sandwich Islands','GS','SGS','Americas','South America',211,'54 30 S','37 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Spain','ES','ESP','Europe','Southern Europe',212,'40 00 N','4 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sri Lanka','LK','LKA','Asia','Southern Asia',213,'7 00 N','81 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sudan','SD','SDN','Africa','Northern Africa',214,'15 00 N','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Suriname','SR','SUR','Americas','South America',215,'4 00 N','56 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Svalbard And Jan Mayen','SJ','SJM','Europe','Northern Europe',216,'78 00 N','20 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Swaziland','SZ','SWZ','Africa','Southern Africa',217,'26 30 S','31 30 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sweden','SE','SWE','Europe','Northern Europe',218,'62 00 N','15 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Switzerland','CH','CHE','Europe','Western Europe',219,'47 00 N','8 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Syria','SY','SYR','Asia','Western Asia',220,'35 00 N','38 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Taiwan','TW','TWN','Asia','Eastern Asia',221,'23 30 N','121 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tajikistan','TJ','TJK','Asia','Central Asia',222,'39 00 N','71 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tanzania','TZ','TZA','Africa','Eastern Africa',223,'6 00 S','35 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Thailand','TH','THA','Asia','South-Eastern Asia',224,'15 00 N','100 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Togo','TG','TGO','Africa','Western Africa',227,'8 00 N','1 10 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tokelau','TK','TKL','Oceania','Polynesia',228,'9 00 S','172 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tonga','TO','TON','Oceania','Polynesia',229,'20 00 S','175 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Trinidad and Tobago','TT','TTO','Americas','Caribbean',230,'11 00 N','61 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tunisia','TN','TUN','Africa','Northern Africa',231,'34 00 N','9 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turkey','TR','TUR','Asia','Western Asia',232,'39 00 N','35 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turkmenistan','TM','TKM','Asia','Central Asia',233,'40 00 N','60 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turks and Caicos Islands','TC','TCA','Americas','Caribbean',234,'21 45 N','71 35 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tuvalu','TV','TUV','Oceania','Polynesia',235,'8 00 S','178 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uganda','UG','UGA','Africa','Eastern Africa',236,'1 00 N','32 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ukraine','UA','UKR','Europe','Eastern Europe',237,'49 00 N','32 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United Arab Emirates','AE','ARE','Asia','Western Asia',238,'24 00 N','54 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United Kingdom','GB','GBR','Europe','Northern Europe',239,'54 00 N','2 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United States','US','USA','Americas','Northern America',240,'38 00 N','97 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United States Minor Outlying Islands','UM','UMI','Americas','Northern America',241,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uruguay','UY','URY','Americas','South America',242,'33 00 S','56 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uzbekistan','UZ','UZB','Asia','Central Asia',243,'41 00 N','64 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Vanuatu','VU','VUT','Oceania','Melanesia',244,'16 00 S','167 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Vatican City','VA','VAT','Europe','Southern Europe',100,'41 54 N','12 27 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Venezuela','VE','VEN','Americas','South America',245,'8 00 N','66 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Viet Nam','VN','VNM','Asia','South-Eastern Asia',247,'16 10 N','107 50 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Virgin Islands, British','VG','VGB','Americas','Caribbean',248,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Virgin Islands, U.S.','VI','VIR','Americas','Caribbean',249,NULL,NULL)
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Wallis and Futuna','WF','WLF','Oceania','Polynesia',250,'13 18 S','176 12 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Western Sahara','EH','ESH','Africa','Northern Africa',251,'24 30 N','13 00 W')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Yemen','YE','YEM','Asia','Western Asia',252,'15 00 N','48 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Zambia','ZM','ZMB','Africa','Eastern Africa',255,'15 00 S','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Zimbabwe','ZW','ZWE','Africa','Eastern Africa',256,'20 00 S','30 00 E')
INSERT INTO dbo.Countries (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('�land Islands','AX','ALA','Europe','Northern Europe',3,NULL,NULL)
GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'NORTHCOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('United States','Canada','Mexico')
GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'PACCOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('American Samoa','Australia','Bangladesh','Bhutan','Brunei','Cambodia','China','East Timor','Fiji','India','Indonesia','Japan','Kiribati','Korea, North','Korea, South','Laos','Malaysia','Maldives','Marshall Islands','Micronesia','Mongolia','Nauru','Nepal','New Zealand','Palau','Papua New Guinea','Philippines','Samoa','Singapore','Solomon Islands','South Georgia and the South Sandwich Islands','Sri Lanka','Taiwan','Thailand','Tokelau','Tonga','Tuvalu','United States Minor Outlying Islands','Vanuatu','Viet Nam','Wallis and Futuna')
GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'SOUTHCOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('Anguilla','Antigua and Barbuda','Argentina','Aruba','Bahamas','Barbados','Belize','Bolivia','Brazil','Cayman Islands','Chile','Colombia','Costa Rica','Cuba','Dominica','Dominican Republic','Ecuador','El Salvador','Grenada','Guadeloupe','Guyana','Haiti','Honduras','Jamaica','Martinique','Montserrat','Netherlands Antilles','Nicaragua','Panama','Paraguay','Peru','Saint Kitts and Nevis','Saint Lucia','Suriname','Trinidad and Tobago','Turks and Caicos Islands','Uruguay','Venezuela','Virgin Islands, British','Virgin Islands, U.S.')
GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'AFRICACOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('Algeria','Angola','Benin','Botswana','Burkina Faso','Burundi','Cameroon','Cape Verde','Central African Republic','Chad','Comoros','Congo','Congo, The Democratic Republic of the','Cote D''Ivoire','Djibouti','Equatorial Guinea','Eritrea','Ethiopia','Gabon','Gambia','Ghana','Guinea','Guinea-Bissau','Kenya','Lesotho','Liberia','Libya','Madagascar','Malawi','Mali','Mauritania','Mauritius','Morocco','Mozambique','Namibia','Niger','Nigeria','Rwanda','Saint Vincent and the Grenadines','Sao Tome and Principe','Senegal','Seychelles','Sierra Leone','Somalia','South Africa','Sudan','Suriname','Swaziland','Tanzania','Togo','Tunisia','Uganda','Western Sahara','Zambia','Zimbabwe')
GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'CENTCOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('Afghanistan','Bahrain','Egypt','Iran','Iraq','Jordan','Kazakhstan','Kuwait','Kyrgyzstan','Lebanon','Oman','Pakistan','Qatar','Saudi Arabia','Sri Lanka','Syria','Tajikistan','Turkmenistan','United Arab Emirates','Uzbekistan','Yemen')

GO

INSERT INTO dbo.commandcountryAssociations
	(CommandID, CountryID)
SELECT
	(
	SELECT C2.CommandID
	FROM dbo.Commands C2 WITH (NOLOCK)
	WHERE C2.CommandName = 'EUCOM'
	) AS CommandID,
	C1.CountryID
FROM dbo.Countries C1 WITH (NOLOCK)
WHERE CountryName IN ('Albania','Andorra','Armenia','Austria','Azerbaijan','Belarus','Belgium','Bosnia and Herzegovina','Bulgaria','Croatia','Cyprus','Czech Republic','Denmark','Estonia','Finland','France','Georgia','Germany','Greece','Hungary','Iceland','Ireland','Israel','Italy','Kosovo','Latvia','Liechtenstein','Lithuania','Luxembourg','Macedonia','Malta','Moldova','Monaco','Montenegro','Netherlands','Norway','Poland','Portugal','Romania','Russia','San Marino','Serbia','Slovakia','Slovenia','Spain','Sweden','Switzerland','Turkey','Ukraine','United Kingdom','Vatican City')
GO