DECLARE @cBackupName varchar(50)
DECLARE @cDatabaseName varchar(50)
DECLARE @cDate varchar(50)
DECLARE @cFilePath varchar(500)

SET @cDate = CONVERT(varchar(20), GETDATE(), 112) 
SET @cFilePath = 'C:\Users\Public'  

print @cDate

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN 
		(
		'ACGU',
		'ARMY',
		'CCO',
		'DISA',
		'DLA',
		'DOS',
		'DTRA',
		'HPRC',
		'JSCC',
		'JSOC',
		'NAVY',
		'NCCS',
		'NGA',
		'NGB',
		'NOMI',
		'ORCHID',
		'RPB',
		'SOCOM',
		'SOCOMNEW',
		'USAF',
		'USSOCOM',
		'USUHS'
		) 
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
BEGIN   

	SET @cBackupName = @cFilePath + '\' + @cDatabaseName + '_' + @cDate + '.bak'  
	BACKUP DATABASE @cDatabaseName TO DISK = @cBackupName WITH NO_LOG  
	FETCH NEXT FROM oCursor INTO @cDatabaseName   

END   

CLOSE oCursor   
DEALLOCATE oCursor