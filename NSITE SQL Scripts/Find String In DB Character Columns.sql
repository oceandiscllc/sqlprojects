USE JSCC
GO

DECLARE @cSearchString VARCHAR(MAX)

SET @cSearchString = 'Cropper'

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable1', 'u')) IS NOT NULL
	DROP TABLE #tTable1

CREATE TABLE #tTable1
	(
	SchemaName varchar(50),
	TableName varchar(50),
	ColumnName varchar(50),
	DataTypeName varchar(50),
	ItemCount int NOT NULL DEFAULT 0
	)

INSERT INTO #tTable1
	(SchemaName,TableName,ColumnName,DataTypeName)
SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ColumnName,
	T.Name AS DataTypeName
FROM sys.objects O WITH (NOLOCK)
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
	JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
WHERE O.type = 'U' 
	AND 
		(
		T.Name LIKE '%char%'
			OR T.Name LIKE '%Text%'
		)
ORDER BY S.Name, O.Name, C.Name

DECLARE @cColumnName varchar(50)
DECLARE @cSchemaName varchar(50)
DECLARE @cTableName varchar(50)
DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		SchemaName,
		TableName,
		ColumnName
	FROM #tTable1 T1
	ORDER BY T1.SchemaName, T1.TableName, T1.ColumnName
		
OPEN oCursor
FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName
WHILE @@fetch_status = 0
	BEGIN

	BEGIN TRY
		SET @cSQL = 'UPDATE #tTable1'
		SET @cSQL += ' SET ItemCount = (SELECT COUNT(*) FROM ' + @cSchemaName + '.' + @cTableName + ' T'
		SET @cSQL += ' WHERE T.[' + @cColumnName + '] LIKE ''%' + @cSearchString + '%'')'
		SET @cSQL += ' WHERE SchemaName = ''' + @cSchemaName + ''''
		SET @cSQL += ' AND TableName = ''' + @cTableName + ''''
		SET @cSQL += ' AND ColumnName = ''' + @cColumnName + ''''

		EXEC(@cSQL)
	END TRY
	
	BEGIN CATCH
		print @cSQL
		BREAK
	END CATCH
	
	FETCH oCursor into @cSchemaName, @cTableName, @cColumnName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

SELECT * FROM #tTable1
WHERE ItemCount > 0

DROP TABLE #tTable1
