USE JLLIS
GO

--Begin PT 4071
IF Utility.GetServerSetupValueByServerSetupKey('NetworkName') = 'SIPR'
	BEGIN

	DELETE CD
	FROM dbo.ClassificationData CD
	WHERE EXISTS
		(
		SELECT 1
		FROM dbo.CollectionPlan CP
		WHERE CP.CollectionPlanID = 868
			AND 
				(
				CP.CollectionPlanClassificationDataID = CD.ClassificationDataID
					OR CP.TitleClassificationDataID = CD.ClassificationDataID
					OR CP.DescriptionClassificationDataID = CD.ClassificationDataID
				)
		)

	DELETE T FROM dbo.ClassificationDataClassificationReason T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)
	DELETE T FROM dbo.ClassificationDataCoalition T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)
	DELETE T FROM dbo.ClassificationDataCountry T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)

	DELETE CP
	FROM dbo.CollectionPlan CP
	WHERE CP.CollectionPlanID = 868

	END
--ENDIF
GO
--End PT 4071
