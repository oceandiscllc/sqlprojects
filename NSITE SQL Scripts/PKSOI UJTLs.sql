
WITH HD (DisplayOrder,TaskID,TaskNumber,ParentTaskNumber,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(255), T.TaskNumber),
		T.TaskID,
		T.TaskNumber,
		T.ParentTaskNumber, 
		1
	FROM dbo.Task T
	WHERE T.ParentTaskNumber = 'JOINT'

	UNION ALL
	
	SELECT
		CONVERT(varchar(255), RTRIM(DisplayOrder) + ',' + T.TaskNumber),
		T.TaskID,
		T.TaskNumber,
		T.ParentTaskNumber, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Task T
		JOIN HD ON HD.TaskNumber = T.ParentTaskNumber 
	)

SELECT 
	'' AS DisplayOrder,
	'JOINT'	AS TaskNumber,
	'' AS ParentTaskNumber,
	0 AS NodeLevel,
	'Root' AS TaskTitle

UNION

SELECT 
	HD.DisplayOrder,
	HD.TaskNumber,
	HD.ParentTaskNumber, 
	HD.NodeLevel,
	T.TaskTitle
FROM HD
	JOIN dbo.Task T ON T.TaskID = HD.TaskID
ORDER BY DisplayOrder