DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'QA.CA.AUTH'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'QA.CACLM'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'QA.CA.MGR'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'QA.CA.ADMIN'
GO

DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-AUTH'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-CLM'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-TEAM'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-MGR'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-ADMIN'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'DR-SA'
GO

DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'CR-AUTH'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'CR-CLM'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'CR-MGR'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'CR-ADMIN'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'CR-SA'
GO

DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'ragmanauth'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'ragmanclm'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'ragmanteam'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'ragmanmgr'
DELETE TU FROM dbo.TierUser TU JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID AND JU.JLLISUserName = 'ragmanadmin'
GO

DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'QA.CA.AUTH'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'QA.CACLM'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'QA.CA.MGR'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'QA.CA.ADMIN'
GO

DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-AUTH'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-CLM'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-TEAM'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-MGR'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-ADMIN'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'DR-SA'
GO

DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'CR-AUTH'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'CR-CLM'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'CR-MGR'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'CR-ADMIN'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'CR-SA'
GO

DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'ragmanauth'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'ragmanclm'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'ragmanteam'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'ragmanmgr'
DELETE FROM dbo.JLLISUser WHERE JLLISUserName = 'ragmanadmin'
GO

DELETE P FROM PasswordSecurity.Password P WHERE NOT EXISTS (SELECT 1 FROM dbo.JLLISUser JU WHERE JU.JLLISUserID = P.JLLISUserID)
GO

INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'QA.CA.AUTH','Chris','Andrews','chris.andrews.test.authuser@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'QA.CACLM','Chris','Andrews','chris.andrews.test.clm@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'QA.CA.MGR','Chris','Andrews','chris.andrews.test.manager@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'QA.CA.ADMIN','Chris','Andrews','chris.andrews.test.admin@nsitellc.com')
GO

INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-AUTH','David','Roberts','david.roberts.test.authuser@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-CLM','David','Roberts','david.roberts.test.clm@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-TEAM','David','Roberts','david.roberts.test.team@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-MGR','David','Roberts','david.roberts.test.mgr@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-ADMIN','David','Roberts','david.roberts.test.admin@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),'DR-SA','David','Roberts','david.roberts.test.sa@nsitellc.com')
GO

INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'CR-AUTH','Christian','Ready','christian.ready.test.authuser@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'CR-CLM','Christian','Ready','christian.ready.test.clm@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'CR-MGR','Christian','Ready','christian.ready.test.manager@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'CR-ADMIN','Christian','Ready','christian.ready.test.admin@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'CR-SA','Christian','Ready','christian.ready.test.sa@nsitellc.com')
GO

INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'ragmanauth','David','Demming','david.demming.test.authuser@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'ragmanclm','David','Demming','david.demming.test.clm@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'ragmanteam','David','Demming','david.demming.test.team@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'ragmanmgr','David','Demming','david.demming.test.mgr@nsitellc.com')
INSERT INTO dbo.JLLISUser (CountryID,OriginatingTierID,JLLISUserName,FirstName,LastName,Email) VALUES ((SELECT C.CountryID FROM Dropdown.Country C WHERE C.ISOCode3 = 'USA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),'ragmanadmin','David','Demming','david.demming.test.admin@nsitellc.com')
GO

INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.AUTH'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CACLM'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2500)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.MGR'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),4000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.ADMIN'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),6000)
GO

INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-AUTH'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),2000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-CLM'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),2500)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-TEAM'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),3000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-MGR'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),4000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-ADMIN'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),6000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-SA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'JS'),7000)
GO

INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-AUTH'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-CLM'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2500)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-MGR'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),4000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-ADMIN'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),6000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-SA'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),7000)
GO

INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanauth'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanclm'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),2500)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanteam'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),3000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanmgr'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),4000)
INSERT INTO dbo.TierUser (Status,TierID,TierUserID,JLLISUserID,DefaultTierID,SecLev) VALUES ('Active',8,(SELECT MAX(TU.TierUserID) + 1 FROM dbo.TierUser TU),(SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanadmin'),(SELECT T.TierID FROM dbo.Tier T WHERE T.TierName = 'Army'),6000)
GO

-- dav r pw data here
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-AUTH'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-CLM'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-TEAM'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-MGR'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-ADMIN'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'DR-SA'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.roberts'
GO

INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.AUTH'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'candrews'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CACLM'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'candrews'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.MGR'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'candrews'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'QA.CA.ADMIN'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'candrews'
GO

INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-AUTH'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'christianready'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-CLM'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'christianready'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-MGR'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'christianready'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-ADMIN'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'christianready'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'CR-SA'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'christianready'
GO

INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanauth'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.demming'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanclm'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.demming'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanteam'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.demming'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanmgr'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.demming'
INSERT INTO PasswordSecurity.Password (CreationDate,IsActive,JLLISUserID,PasswordHash,PasswordSalt) SELECT '2017-01-01',1, (SELECT JU.JLLISUserID FROM dbo.JLLISUser JU WHERE JU.JLLISUserName = 'ragmanadmin'), P.PasswordHash, P.PasswordSalt FROM PasswordSecurity.Password P JOIN dbo.JLLISUser JU ON JU.JLLISUserID = P.JLLISUserID AND P.IsActive = 1 AND JU.JLLISUserName = 'david.demming'
GO
