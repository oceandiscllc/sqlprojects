	SELECT 
		TU.JLLISUserID, 
		TU.TierUserID,
		TU.Status,
		TU.DefaultTierID,
		TU.TierID,
		TU.SecLev
	FROM JLLIS.dbo.TierUser TU
		JOIN 
			(
			SELECT
				TU.JLLISUserID
			FROM JLLIS.dbo.TierUser TU
			WHERE TU.TierID = jllis.dbo.GetTierUserInstanceID()
			GROUP BY TU.JLLISUserID
			HAVING COUNT(TU.JLLISUserID) > 1
			) D ON D.JLLISUserID = TU.JLLISUserID
		AND TU.TierID = jllis.dbo.GetTierUserInstanceID()
ORDER BY TU.JLLISUserID, TU.TierUserID
