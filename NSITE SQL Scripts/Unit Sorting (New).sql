DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(U.UnitID) AS VARCHAR(50)))
FROM JSCC.dbo.Unit U

;
WITH HD (DisplayIndex,InstanceID,UnitLineage,UnitID,ParentUnitID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY U.UnitName) AS VARCHAR(10)), @nPadLength)),
		U.InstanceID,
		CAST(U.UnitName AS VARCHAR(MAX)),
		U.UnitID,
		U.ParentUnitID,
		1
	FROM JSCC.dbo.Unit U
	WHERE U.ParentUnitID = 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY U.UnitName) AS VARCHAR(10)), @nPadLength)),
		U.InstanceID,
		CAST(HD.UnitLineage  + ' > ' + U.UnitName AS VARCHAR(MAX)),
		U.UnitID,
		U.ParentUnitID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JSCC.dbo.Unit U
		JOIN HD ON HD.UnitID = U.ParentUnitID
	)

SELECT 
	HD.InstanceID,
	T.TierName AS InstanceName,
	HD.UnitLineage,
	HD.ParentUnitID,
	U2.UnitName AS ParentUnitName,
	HD.UnitID,
	U1.UnitName,
	U1.Status
FROM HD
	LEFT JOIN JSCC.dbo.Unit U1 ON U1.UnitID = HD.UnitID
	LEFT JOIN JSCC.dbo.Unit U2 ON U2.UnitID = HD.ParentUnitID
	LEFT JOIN JLLIS.dbo.Tier T ON T.TierID = HD.InstanceID
ORDER BY T.TierName, HD.InstanceID, HD.DisplayIndex