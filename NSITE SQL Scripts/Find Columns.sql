DECLARE @cColumnName VARCHAR(50) = 'autoemail'

/*
SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ColumnName
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.Columns C ON O.Object_ID = C.Object_ID
	JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
		AND O.type = 'U' 
		AND C.Name LIKE @cColumnName
		AND O.Name = 'CollectionPlan'
*/
/*
DECLARE @cColumnName VARCHAR(50) = '%submissiontype%'
*/

SELECT
	'JLLIS' AS DatabaseName,
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ColumnName
FROM JLLIS.sys.Objects O
	JOIN JLLIS.sys.Schemas S ON S.schema_ID = O.schema_ID
	JOIN JLLIS.sys.Columns C ON O.Object_ID = C.Object_ID
	JOIN JLLIS.sys.Types T ON C.User_Type_ID = T.User_Type_ID
		AND O.type = 'U' 
		AND C.Name LIKE @cColumnName
		--AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')

UNION

SELECT
	'JSCC' AS DatabaseName,
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ColumnName
FROM JSCC.sys.Objects O
	JOIN JSCC.sys.Schemas S ON S.schema_ID = O.schema_ID
	JOIN JSCC.sys.Columns C ON O.Object_ID = C.Object_ID
	JOIN JSCC.sys.Types T ON C.User_Type_ID = T.User_Type_ID
		AND O.type = 'U' 
		AND C.Name LIKE @cColumnName
		--AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')

ORDER BY 3
