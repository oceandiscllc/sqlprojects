USE ARMY
GO

--Get restricted lessons associated with restricted binders associated with the user
SELECT BI.ItemID
FROM dbo.BinderItem BI WITH (NOLOCK)
	JOIN dbo.LMS L WITH (NOLOCK) ON L.LMSID = BI.ItemID
		AND L.Status = 'Restricted'

  JOIN dbo.Binder B1 WITH (NOLOCK) ON B1.BinderID = BI.BinderID
		AND B1.Type = 'Restricted'
		AND BI.type = 'LMS' 
		AND BI.BinderID IN
			(
			--Get restricted binders associated with the user
			SELECT EM.EntityID
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityTypeCode = 'BINDER'
				AND EM.MemberTypeCode = 'User'
				AND EM.MemberID = 235

			UNION

			--Get restricted binders associated with the unit associated with the user
			SELECT EM.EntityID
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityTypeCode = 'BINDER'
				AND EM.MemberTypeCode = 'UNIT'
				AND EXISTS
					(
					SELECT 1
					FROM dbo.Unit U WITH (NOLOCK)
						JOIN JLLIS.dbo.JLLISUser JU WITH (NOLOCK) ON JU.Unit = U.UnitName
						JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
							AND TU.TierUserID = 235
							AND TU.TierID = 25
							AND U.UnitID = EM.MemberID
					)
			
			UNION

			--Get restricted binders associated with usergroups associated with the user
			SELECT EM1.EntityID
			FROM dbo.EntityMembers EM1 WITH (NOLOCK)
			WHERE EM1.EntityTypeCode = 'BINDER'
				AND EM1.MemberTypeCode = 'USERGROUP'
				AND EXISTS
					(
					SELECT 1
					FROM dbo.EntityMembers EM2 WITH (NOLOCK)
					WHERE EM2.EntityTypeCode = 'UserGroup'
						AND EM2.MemberTypeCode = 'User'
						AND EM2.MemberID = 235
						AND EM2.EntityID = EM1.MemberID
					)

			UNION

			--Get restricted binders associated with usergroups associated with the unit associated with the user
			SELECT EM1.EntityID
			FROM dbo.EntityMembers EM1 WITH (NOLOCK)
			WHERE EM1.EntityTypeCode = 'BINDER'
				AND EM1.MemberTypeCode = 'USERGROUP'
				AND EXISTS
					(
					SELECT 1
					FROM dbo.EntityMembers EM2 WITH (NOLOCK)
					WHERE EM2.EntityTypeCode = 'USERGROUP'
						AND EM2.MemberTypeCode = 'UNIT'
						AND EXISTS
							(
							SELECT 1
							FROM dbo.Unit U WITH (NOLOCK)
								JOIN JLLIS.dbo.JLLISUser JU WITH (NOLOCK) ON JU.Unit = U.UnitName
								JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
									AND TU.TierUserID = 235
									AND TU.TierID = 25
									AND U.UnitID = EM2.MemberID
							)
						AND EM2.EntityID = EM1.MemberID
					)
			)
