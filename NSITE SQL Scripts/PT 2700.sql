--Begin PT 2700
USE JSCC
GO

--Begin procedure Utility.AddEvent
EXEC Utility.DropObject 'Utility.AddEvent'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2014.01.07
-- Description:	A stored procedure to add an event to every tier in the system
-- ===========================================================================
CREATE PROCEDURE Utility.AddEvent
	@cEventType VARCHAR(250),
	@cEvent VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE Dropdown.EventType T1
	USING (SELECT @cEventType AS EventType) T2
		ON T2.EventType = T1.EventType
	WHEN NOT MATCHED THEN
	INSERT 
		(EventType)
	VALUES
		(
		T2.EventType
		);
	
	INSERT INTO Dropdown.Event
		(EventTypeID,Event)
	SELECT
		ET.EventTypeID,
		@cEvent
	FROM Dropdown.EventType ET
	WHERE ET.EventType = @cEventType
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.Event E
			WHERE E.Event = @cEvent
			)

	INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
		(DropdownMetadataID, TierID)
	SELECT
		DMD.DropdownMetadataID,
		T.TierID
	FROM JLLIS.Dropdown.DropdownMetaData DMD, JLLIS.dbo.Tier T
	WHERE DMD.IsConfigurableByTier = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
			WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
				AND TDMD.TierID = T.TierID
			)

	INSERT INTO Dropdown.TierDropdown
		(TierID,DropdownMetaDataID,DropdownEntityID,IsForSubordinateTiers)
	SELECT
		T.TierID,
		(SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'EventType'),
		(SELECT TOP 1 ET.EventTypeID FROM Dropdown.EventType ET WHERE ET.EventType = @cEventType),
		1
	FROM JLLIS.dbo.Tier T
	WHERE NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = T.TierID
				AND TD.DropdownMetaDataID = (SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'EventType')
				AND TD.DropdownEntityID = (SELECT TOP 1 ET.EventTypeID FROM Dropdown.EventType ET WHERE ET.EventType = @cEventType)
			)

	INSERT INTO Dropdown.TierDropdown
		(TierID,DropdownMetaDataID,DropdownEntityID,IsForSubordinateTiers)
	SELECT
		T.TierID,
		(SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'Event'),
		(SELECT TOP 1 E.EventID FROM Dropdown.Event E WHERE E.Event = @cEvent),
		1
	FROM JLLIS.dbo.Tier T
	WHERE NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.TierID = T.TierID
				AND TD.DropdownMetaDataID = (SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'Event')
				AND TD.DropdownEntityID = (SELECT TOP 1 E.EventID FROM Dropdown.Event E WHERE E.Event = @cEvent)
			)

END	
GO
--End procedure Utility.AddEvent

IF EXISTS (SELECT 1 FROM JLLIS.dbo.ServerSetup SS WHERE SS.ServerSetupKey = 'NetworkName' AND SS.ServerSetupValue IN ('JWICS','SIPR'))
	EXEC Utility.AddEvent 'Operation', 'OPCW-Destruction of Syrian Chemicals'
--ENDIF
GO
--End PT 2700
