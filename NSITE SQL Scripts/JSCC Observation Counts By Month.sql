DECLARE @cInstanceName varchar(50)
DECLARE @dDateStart datetime
DECLARE @dDateStop datetime
DECLARE @tLMS table (LMSID int NOT NULL primary key, TierID int)
DECLARE @tSubTiers table (TierID int NOT NULL primary key)
DECLARE @nTierID int

SET @cInstanceName = 'JSCC';
SET @dDateStart = '12/01/2011'
SET @dDateStop = '01/01/2012'
SET @nTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cInstanceName AND T.IsInstance = 1)

;
WITH HD (TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID, 
		1
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL
	
	SELECT
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)

INSERT INTO @tSubTiers
	(TierID)
SELECT 
	HD.TierID
FROM HD
WHERE HD.NodeLevel = 2

INSERT INTO @tLMS
	(LMSID,TierID)
SELECT 
	L.LMSID,
	@nTierID
FROM dbo.LMS L WITH (NOLOCK)
WHERE L.OriginatingTierID = @nTierID
	AND L.CreationDate BETWEEN @dDateStart AND @dDateStop

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT TierID
	FROM @tSubTiers

OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	;
	WITH HD (TierID,ParentTierID)
		AS 
		(
		SELECT
			T.TierID, 
			T.ParentTierID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		WHERE T.TierID = @nTierID

		UNION ALL
		
		SELECT
			T.TierID, 
			T.ParentTierID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
			JOIN HD ON HD.TierID = T.ParentTierID 
		)

	INSERT INTO @tLMS
		(LMSID,TierID)
	SELECT 
		L.LMSID,
		@nTierID
	FROM dbo.LMS L WITH (NOLOCK)
		JOIN HD ON HD.TierID = L.OriginatingTierID
			AND L.CreationDate BETWEEN @dDateStart AND @dDateStop

	FETCH oCursor into @nTierID
			
	END
--END WHILE
			
CLOSE oCursor
DEALLOCATE oCursor
	
SELECT 
	COUNT(L1.LMSID) AS ItemCount,
	MONTH(L2.CreationDate) AS CreateMonth,
	YEAR(L2.CreationDate) AS CreateYear, 
	T.TierLabel
FROM @tLMS L1
	JOIN dbo.LMS L2 ON L2.LMSID = L1.LMSID
	JOIN JLLIS.dbo.Tier T ON L1.TierID = T.TierID
GROUP BY MONTH(L2.CreationDate), YEAR(L2.CreationDate), T.TierLabel
ORDER BY YEAR(L2.CreationDate), MONTH(L2.CreationDate), T.TierLabel