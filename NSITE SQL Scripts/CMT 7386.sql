USE JLLIS
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetPersonNameByUserID') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetPersonNameByUserID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2010.06.22
-- Description:	A function to return the name of a user in a specified format from a userid
-- ========================================================================================

CREATE FUNCTION dbo.GetPersonNameByUserID
(
@nUserID int,
@cUserIDType varchar(50),
@cTierName varchar(50),
@cFormat varchar(50)
)

RETURNS varchar(250)

AS
BEGIN

DECLARE @cRetVal varchar(250)
SET @cRetVal = ''

IF @nUserID IS NOT NULL AND @nUserID > 0
	BEGIN
	
	IF @cUserIDType = 'TierUserID'
		BEGIN

		SET @nUserID = 
			(
			SELECT TU.JLLISUserID
			FROM dbo.TierUser TU WITH (NOLOCK)
				JOIN dbo.Tier T WITH (NOLOCK) ON T.TierID = TU.TierID
					AND T.TierName = @cTierName
					AND TU.TierUserID = @nUserID
			)
		
		SET @nUserID = ISNULL(@nUserID, 0)
		
		END
	--ENDIF
	
	SET @cRetVal = 
		(
		SELECT

			CASE
				WHEN @cFormat = 'FirstLast'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.FirstName) + ' ' + LTRIM(JU.LastName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END

				WHEN @cFormat = 'LastFirst'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.LastName) + ', ' + LTRIM(JU.FirstName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END
			END
						
		FROM dbo.JLLISUser JU
		WHERE JU.jllisuserid = @nUserID
		)
		
	END
--ENDIF

RETURN ISNULL(@cRetVal, '')

END
GO