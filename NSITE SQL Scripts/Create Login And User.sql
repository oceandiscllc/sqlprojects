IF  EXISTS (SELECT 1 FROM sys.server_principals WHERE name = 'todd.pires')
	DROP LOGIN [todd.pires]
--ENDIF
GO

CREATE LOGIN [todd.pires] 
	WITH PASSWORD='Yankee01', 
	DEFAULT_DATABASE=[JLLIS], 
	DEFAULT_LANGUAGE=[us_english], 
	CHECK_EXPIRATION=OFF, 
	CHECK_POLICY=OFF
GO

ALTER LOGIN [todd.pires] ENABLE
GO

EXEC master..sp_addsrvrolemember @loginame = N'todd.pires', @rolename = N'sysadmin'
GO

CREATE USER [todd.pires] FOR LOGIN [todd.pires] WITH DEFAULT_SCHEMA=[dbo]
GO

USE JLLIS
GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'todd.pires')
	DROP USER [todd.pires]
--ENDIF
GO

CREATE USER [todd.pires] FOR LOGIN [todd.pires] WITH DEFAULT_SCHEMA=[dbo]
GO

DECLARE @cSQL VARCHAR(MAX) = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'ALTER AUTHORIZATION ON SCHEMA::[' + S.Name + '] TO [todd.pires]'
	FROM sys.Schemas S
	WHERE S.Name NOT IN ('dbo','guest','INFORMATION_SCHEMA','sys')
	ORDER BY S.Name

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC (@cSQL)
	
	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

USE JSCC
GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'todd.pires')
	DROP USER [todd.pires]
--ENDIF
GO

CREATE USER [todd.pires] FOR LOGIN [todd.pires] WITH DEFAULT_SCHEMA=[dbo]
GO

DECLARE @cSQL VARCHAR(MAX) = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'ALTER AUTHORIZATION ON SCHEMA::[' + S.Name + '] TO [todd.pires]'
	FROM sys.Schemas S
	WHERE S.Name NOT IN ('dbo','guest','INFORMATION_SCHEMA','sys')
	ORDER BY S.Name

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC (@cSQL)
	
	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

/*
USE BIO
GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'todd.pires')
	DROP USER [todd.pires]
--ENDIF
GO

CREATE USER [todd.pires] FOR LOGIN [todd.pires] WITH DEFAULT_SCHEMA=[dbo]
GO

DECLARE @cSQL VARCHAR(MAX) = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'ALTER AUTHORIZATION ON SCHEMA::[' + S.Name + '] TO [todd.pires]'
	FROM sys.Schemas S
	WHERE S.Name NOT IN ('dbo','guest','INFORMATION_SCHEMA','sys')
	ORDER BY S.Name

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC (@cSQL)
	
	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

USE SOCOM
GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'todd.pires')
	DROP USER [todd.pires]
--ENDIF
GO

CREATE USER [todd.pires] FOR LOGIN [todd.pires] WITH DEFAULT_SCHEMA=[dbo]
GO

DECLARE @cSQL VARCHAR(MAX) = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'ALTER AUTHORIZATION ON SCHEMA::[' + S.Name + '] TO [todd.pires]'
	FROM sys.Schemas S
	WHERE S.Name NOT IN ('dbo','guest','INFORMATION_SCHEMA','sys')
	ORDER BY S.Name

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC (@cSQL)
	
	FETCH oCursor INTO @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
*/