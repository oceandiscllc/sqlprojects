SELECT 
	MD.MetadataID,
	MD.Metadata
FROM JSCC.Dropdown.Metadata MD
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType LIKE 'DOTMLPF%'
ORDER BY MD.Metadata
