DECLARE @cTierName VARCHAR(50)
SET @cTierName = 'JS';

SELECT 
	JU.JLLISUserID,
	TU.TierUserID,
	JU.LastName,
	JU.FirstName,
	JU.JLLISUserName,
	JU.DayPhone,
	JU.NightPhone,
	JU.State,
	CONVERT(CHAR(11), TU.ActiveDate, 113) AS LastActiveDate,
	CONVERT(CHAR(11), TU.StartDate, 113) AS StartDate,
	JU.Email,
	TU.SecLev,
	TU.Status,
	T1.TierLabel AS MemberHomePage,
	T2.TierLabel AS MemberOrganization,
	SL.Role
FROM JLLIS.dbo.GetDescendantTiersByTierName(@cTierName) HD
	JOIN JLLIS.dbo.TierUser TU ON TU.DefaultTierID = HD.TierID
		AND TU.Status = 'Active'
		AND TU.TierID = 
			(
			SELECT CAST(SS.ServerSetupValue AS INT)
			FROM JLLIS.dbo.ServerSetup SS
			WHERE SS.ServerSetupKey = 'TierUserInstanceID'
			)		
	JOIN JLLIS.dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = HD.TierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = JU.OriginatingTierID
	JOIN JLLIS.Dropdown.SecLev SL ON SL.SecLev = TU.SecLev
ORDER BY JU.LastName, JU.FirstName