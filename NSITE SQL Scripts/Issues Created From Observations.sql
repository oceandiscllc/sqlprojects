SELECT 
	I.LMSID, 
	I.IssueID, 
	CONVERT(char(11), I.CreateDate, 113) AS IssueCreateDate,
	T1.TierLabel AS [Owning Organization], 
	T2.TierLabel AS [Tier I Organization (Instance)], 
	T3.TierLabel AS OPR
FROM JLLIS.dbo.Issue I
	JOIN JSCC.dbo.LMS L ON L.LMSID = I.LMSID
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = I.OriginatingTierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
	JOIN JLLIS.dbo.Tier T3 ON T3.TierID = I.OPRTierID
ORDER BY I.CreateDate, I.LMSID
