DELETE D
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY EntityID, EntityTypeCode ORDER BY EntityID) AS RowIndex
	FROM JLLIS.dbo.EntityLocation
	WHERE EntityTypeCode = 'Lesson'
	) D
WHERE D.RowIndex > 1