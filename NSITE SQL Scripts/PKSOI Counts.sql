SELECT
	COUNT(P.CountryID) AS ItemCount,
	P.CountryID,
	C.CountryName
FROM person.Person P
	JOIN dropdown.Country C ON C.CountryID = P.CountryID
GROUP BY P.CountryID, C.CountryName
ORDER BY 1 DESC, C.CountryName

SELECT
	COUNT(P.PersonID) AS ItemCount,
	RIGHT(LTRIM(RTRIM(P.EmailAddress)), CHARINDEX('@', REVERSE(LTRIM(RTRIM(P.EmailAddress))))) AS Domain
FROM person.Person P
WHERE CHARINDEX('@', LTRIM(RTRIM(P.EmailAddress))) > 0
GROUP BY RIGHT(LTRIM(RTRIM(P.EmailAddress)), CHARINDEX('@', REVERSE(LTRIM(RTRIM(P.EmailAddress)))))
ORDER BY 1 DESC, 2

SELECT
	COUNT(EL.EventLogID) AS ItemCount,
	YEAR(EL.CreateDateTime) AS ItemYear,
	MONTH(EL.CreateDateTime) AS ItemMonth
FROM core.EventLog EL
WHERE EL.EventCode = 'View'
	AND EL.EntityTypeCode = 'Lesson'
	AND EL.CreateDateTime >= '10/1/2017' AND EL.CreateDateTime < '10/1/2018'
GROUP BY YEAR(EL.CreateDateTime), MONTH(EL.CreateDateTime)
ORDER BY YEAR(EL.CreateDateTime) desc, MONTH(EL.CreateDateTime) desc

SELECT
	COUNT(EL.PersonID) AS ItemCount,
	EL.PersonID,
	person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
	P.EmailAddress,
	(SELECT PT.PortalName FROM portal.Portal PT WHERE PT.PortalID = P.PortalID) AS PortalName
FROM core.EventLog EL
	JOIN person.Person P ON P.PersonID = EL.PersonID
		AND EL.EventCode = 'View'
		AND EL.EntityTypeCode = 'Lesson'
		--AND EL.PersonID NOT IN (1668,3337)
		AND EL.CreateDateTime >= '10/1/2017' AND EL.CreateDateTime < '10/1/2018'
GROUP BY EL.PersonID, P.EmailAddress, P.PortalID
ORDER BY 1 DESC

SELECT
	COUNT(EL.EntityID) AS ViewCount,
	EL.EntityID AS LessonID,
	L.LessonName,
	(SELECT PT.PortalName FROM portal.Portal PT WHERE PT.PortalID = L.PortalID) AS PortalName
FROM core.EventLog EL
	JOIN lesson.Lesson L ON L.LessonID = EL.EntityID
		AND EL.EventCode = 'View'
		AND EL.EntityTypeCode = 'Lesson'
		--AND EL.PersonID NOT IN (1668,3337)
		AND EL.CreateDateTime >= '10/1/2017' AND EL.CreateDateTime < '10/1/2018'
GROUP BY EL.EntityID, L.LessonName, L.PortalID
ORDER BY 1 DESC

SELECT 
	COUNT(P.PersonID) AS ItemCount,
	P.Unit
FROM person.Person P
GROUP BY P.Unit
ORDER BY 1 DESC, 2

