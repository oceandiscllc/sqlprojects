--Begin PT 4044
USE JLLIS
GO

DECLARE @tTable TABLE (CoalitionAbbreviation VARCHAR(5), Coalition VARCHAR(250))

INSERT INTO @tTable
	(CoalitionAbbreviation, Coalition)
VALUES
	('CTOC', 'CTOC:  Countering Transnational Organized Crime'),
	('NCFE', 'NCFE:  NATO CFE'),
	('OSTY', 'OSTY:  Open Skies Treaty'),
	('RSMA', 'RSMA:  Resolute Support Mission Afghanistan'),
	('SOFP', 'SOFP:  Special Operations Forces Partners')

INSERT INTO Dropdown.Coalition
	(CoalitionAbbreviation, Coalition)
SELECT
	T.CoalitionAbbreviation, 
	T.Coalition
FROM @tTable T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM Dropdown.Coalition C
	WHERE C.CoalitionAbbreviation = T.CoalitionAbbreviation
	)
	
UPDATE Dropdown.Coalition
SET IsActive = 0
WHERE CoalitionAbbreviation IN ('CFUP', 'IESC', 'ROKUS')
GO
--End PT 4044