SELECT COUNT(JU.JLLISUserID) AS ItemCount
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = JU.OriginatingTierID
		AND T1.TierName = 'JSCC'
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = TU.TierID
		AND T2.TierName = 'JSCC'
		AND TU.Status = 'Active'
GO

SELECT COUNT(TU.TierUserID) AS ItemCount
FROM JLLIS.dbo.TierUser TU
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TU.TierID
		AND T1.TierName = 'JSCC'
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = TU.DefaultTierID
		AND T2.TierName = 'JSCC'
		AND TU.Status = 'Active'
GO		
