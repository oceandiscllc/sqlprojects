USE JSCC
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
	DROP TABLE #tTable
--ENDIF
GO
	
CREATE TABLE #tTable 
	(
	EventLocation VARCHAR(250), 
	EventSponsor VARCHAR(250),
	EventType VARCHAR(250)
	)
GO

INSERT INTO #tTable
	(EventLocation,EventSponsor,EventType)
VALUES
	('Philippines','USPACOM','Operation')
GO

MERGE Dropdown.EventLocation T1
USING (SELECT T.EventLocation FROM #tTable T) T2
	ON T2.EventLocation = T1.EventLocation
WHEN NOT MATCHED THEN
INSERT 
	(EventLocation)
VALUES
	(
	T2.EventLocation
	);
GO

MERGE Dropdown.EventSponsor T1
USING (SELECT T.EventSponsor FROM #tTable T) T2
	ON T2.EventSponsor = T1.EventSponsor
WHEN NOT MATCHED THEN
INSERT 
	(EventSponsor)
VALUES
	(
	T2.EventSponsor
	);
GO

MERGE Dropdown.EventType T1
USING (SELECT T.EventType FROM #tTable T) T2
	ON T2.EventType = T1.EventType
WHEN NOT MATCHED THEN
INSERT 
	(EventType)
VALUES
	(
	T2.EventType
	);
GO

INSERT INTO Dropdown.Event
	(EventLocationID,EventSponsorID,EventTypeID,Event,StartDate,EndDate)
SELECT
	EL.EventLocationID,
	ES.EventSponsorID,
	ET.EventTypeID,
	'Operation DAMAYAN (Philippine Typhoon)',
	'11/12/2013',
	'11/12/2014'
FROM #tTable T
	JOIN Dropdown.EventLocation EL ON EL.EventLocation = T.EventLocation
	JOIN Dropdown.EventSponsor ES ON ES.EventSponsor = T.EventSponsor
	JOIN Dropdown.EventType ET ON ET.EventType = T.EventType
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.Event E
			WHERE E.Event = 'Operation DAMAYAN (Philippine Typhoon)'
			)
GO		

INSERT INTO JLLIS.Dropdown.TierDropdownMetadata
	(DropdownMetadataID, TierID)
SELECT
	DMD.DropdownMetadataID,
	T.TierID
FROM JLLIS.Dropdown.DropdownMetaData DMD, JLLIS.dbo.Tier T
WHERE DMD.IsConfigurableByTier = 1
	AND NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Dropdown.TierDropdownMetadata TDMD
		WHERE TDMD.DropdownMetadataID = DMD.DropdownMetadataID
			AND TDMD.TierID = T.TierID
		)
GO

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetaDataID,DropdownEntityID,IsForSubordinateTiers)
SELECT
	T.TierID,
	(SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'EventType'),
	(SELECT TOP 1 ET.EventTypeID FROM Dropdown.EventType ET WHERE ET.EventType = 'Operation'),
	1
FROM JLLIS.dbo.Tier T
WHERE NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = T.TierID
			AND TD.DropdownMetaDataID = (SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'EventType')
			AND TD.DropdownEntityID = (SELECT TOP 1 ET.EventTypeID FROM Dropdown.EventType ET WHERE ET.EventType = 'Operation')
		)
GO

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetaDataID,DropdownEntityID,IsForSubordinateTiers)
SELECT
	T.TierID,
	(SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'Event'),
	(SELECT TOP 1 E.EventID FROM Dropdown.Event E WHERE E.Event = 'Operation DAMAYAN (Philippine Typhoon)'),
	1
FROM JLLIS.dbo.Tier T
WHERE NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.TierDropdown TD
		WHERE TD.TierID = T.TierID
			AND TD.DropdownMetaDataID = (SELECT DMD.DropdownMetaDataID FROM JLLIS.Dropdown.DropdownMetaData DMD WHERE DMD.DropdownMetaDataCode = 'Event')
			AND TD.DropdownEntityID = (SELECT TOP 1 E.EventID FROM Dropdown.Event E WHERE E.Event = 'Operation DAMAYAN (Philippine Typhoon)')
		)
GO

DROP TABLE #tTable
GO
