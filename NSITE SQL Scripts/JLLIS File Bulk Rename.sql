SELECT 
	D.PhysicalFileExtension,
	JF3.PhysicalFileName
FROM
	(
	SELECT 
		MAX(JF2.JLLISFileID) AS JLLISFileID, 
		JF2.PhysicalFileExtension
	FROM dbo.JLLISFile JF2
	WHERE JF2.PhysicalFileExtension IN 
		(
		'CSV',
		'DOC',
		'DOCX',
		'ODT',
		'PDF',
		'RTF',
		'TXT',
		'XLS',
		'XLSX'	
		)
	GROUP BY JF2.PhysicalFileExtension
	) D
	JOIN dbo.JLLISFile JF3 ON JF3.JLLISFileID = D.JLLISFileID
ORDER BY D.PhysicalFileExtension

UPDATE JF1
SET JF1.PhysicalFileName = E.PhysicalFileName
FROM dbo.JLLISFile JF1
	JOIN
		(
		SELECT 
			JF3.PhysicalFileName,
			D.PhysicalFileExtension
		FROM
			(
			SELECT 
				MAX(JF2.JLLISFileID) AS JLLISFileID, 
				JF2.PhysicalFileExtension
			FROM dbo.JLLISFile JF2
			WHERE JF2.PhysicalFileExtension IN 
				(
				'CSV',
				'DOC',
				'DOCX',
				'ODT',
				'PDF',
				'RTF',
				'TXT',
				'XLS',
				'XLSX'	
				)
			GROUP BY JF2.PhysicalFileExtension
			) D
			JOIN dbo.JLLISFile JF3 ON JF3.JLLISFileID = D.JLLISFileID
		) E ON E.PhysicalFileExtension = JF1.PhysicalFileExtension