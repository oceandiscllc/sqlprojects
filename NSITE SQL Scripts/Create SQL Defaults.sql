USE AAA
GO

DECLARE @cSchemaName varchar(250)
DECLARE @cSchemaNameTableName varchar(500)
DECLARE @cTableName varchar(250)

SET @cSchemaName = 'dbo'
SET @cTableName = 'MenuItem'
SET @cSchemaNameTableName = @cSchemaName + '.' + @cTableName

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cSchemaNameTableName) AND O.type IN ('U'))
	BEGIN

	CREATE TABLE dbo.MenuItem
		(
		MenuItemID int IDENTITY(1,1) not null,
		ParentMenuItemID int,
		MenuItem varchar(250),
		MenuItemCode varchar(50),
		MenuItemLink varchar(500),
		DisplayOrder int,
		IsActive bit,
		SecLev int
		)

	END
--ENDIF

SELECT 
	1 AS DisplayOrder,
	'EXEC Utility.SetDefault @cTableName, ' 
	+ '''' + C.Name + ''', ' + 
	CASE
		WHEN T.Name = 'datetime'
		THEN '''getDate()'''
		WHEN C.Name = 'IsActive'
		THEN '1'
		ELSE '0'
	END
	+ ''
	AS SQLText
FROM sys.objects O WITH (NOLOCK)
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
	JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
		AND O.Name = @cTableName
		AND O.type = 'U'
		AND S.Name = @cSchemaName
		AND T.Name IN ('bit','datetime','int')
		AND C.Is_Identity = 0

UNION

SELECT 
	2 AS DisplayOrder,
	'' AS SQLText
	
UNION

SELECT 
	3 AS DisplayOrder,
	'EXEC Utility.SetColumnNotNull @cTableName, ' 
	+ '''' + C.Name + ''', ' + 
	+ '''' + T.Name + ''''
	AS SQLText
FROM sys.objects O WITH (NOLOCK)
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
	JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
		AND O.Name = @cTableName
		AND O.type = 'U'
		AND S.Name = @cSchemaName
		AND T.Name IN ('bit','datetime','int')
		AND C.Is_Identity = 0

UNION

SELECT 
	4 AS DisplayOrder,
	'' AS SQLText
	
UNION

SELECT 
	5 AS DisplayOrder,
	'EXEC Utility.SetPrimaryKeyClustered @cTableName, ' 
	+ '''' + C.Name + ''''
	AS SQLText
FROM sys.objects O WITH (NOLOCK)
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
	JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
		AND O.Name = @cTableName
		AND O.type = 'U'
		AND S.Name = @cSchemaName
		AND T.Name IN ('bit','datetime','int')
		AND C.Is_Identity = 1

ORDER BY DisplayOrder, SQLText