DECLARE @cDeployID varchar(20)
DECLARE @cFirstName varchar(250)
DECLARE @cLastName varchar(250)
DECLARE @cNewStatus varchar(250)
DECLARE @cOldStatus varchar(250)
DECLARE @nUserID int

SET @cDeployID = '2.1.3'
SET @cFirstName = 'Todd'
SET @cLastName = 'Pires'
SET @cNewStatus = 'Ready to Move SIPR'
SET @cOldStatus = 'Staging Test Passed & Ready for NIPR Deployment'

SET @nUserID = ISNULL((SELECT TOP 1 LU.ID FROM CMT.dbo.loginuser LU WITH (NOLOCK) WHERE LU.FirstName = @cFirstName AND LU.LastName = @cLastName), 0)

SELECT 
	@nUserID AS UserID,
	BB.BBID,
	BB.Status,
	BB.Type,
	BB.DeployID,
	BB.IOBV,
	BB.Description
FROM CMT.dbo.BB BB WITH (NOLOCK)
WHERE BB.IOBV = 'N-TIER'
	AND BB.DeployID = @cDeployID
	AND BB.Status = @cOldStatus
ORDER BY BB.BBID

INSERT INTO CMT.dbo.eventlog
	(UserID,GoldMineID,Type,Descrip)
SELECT
	@nUserID,
	BB.BBID,
	'Updated CMT Item',
	@cFirstName + ' Changed Status ' + BB.Status + ' to ' + @cNewStatus
FROM CMT.dbo.BB WITH (NOLOCK) 
WHERE BB.IOBV = 'N-TIER'
	AND BB.DeployID = @cDeployID
	AND BB.Status = @cOldStatus
	
UPDATE BB
SET 
	BB.Status = @cNewStatus,
	BB.UpdateBy = @nUserID
FROM CMT.dbo.BB
WHERE BB.IOBV = 'N-TIER'
	AND BB.DeployID = @cDeployID
	AND BB.Status = @cOldStatus
GO