DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(T.OrganizationID) AS VARCHAR(50)))
FROM PFDR.Dropdown.Organization T

;
WITH HD (DisplayIndex,OrganizationNameLineage,OrganizationIDLineage,OrganizationID,ParentOrganizationID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY T.DisplayOrder, T.OrganizationName) AS VARCHAR(10)), @nPadLength)),
		CAST(T.OrganizationName AS VARCHAR(MAX)),
		CAST(T.OrganizationID AS VARCHAR(MAX)),
		T.OrganizationID,
		T.ParentOrganizationID,
		1
	FROM PFDR.Dropdown.Organization T
	WHERE T.ParentOrganizationID = 0
		AND T.OrganizationID > 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY T.OrganizationName) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.OrganizationNameLineage  + ' > ' + T.OrganizationName AS VARCHAR(MAX)),
		CAST(HD.OrganizationIDLineage  + ',' + CAST(T.OrganizationID AS VARCHAR(50)) AS VARCHAR(MAX)),
		T.OrganizationID,
		T.ParentOrganizationID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM PFDR.Dropdown.Organization T
		JOIN HD ON HD.OrganizationID = T.ParentOrganizationID
	)

SELECT
	HD1.DisplayIndex,
	HD1.NodeLevel,
	HD1.ParentOrganizationID,
	HD1.OrganizationNameLineage,
	HD1.OrganizationIDLineage,
	O.OrganizationID,
	O.OrganizationName,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentOrganizationID = HD1.OrganizationID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN Dropdown.Organization O ON O.OrganizationID = HD1.OrganizationID
ORDER BY HD1.DisplayIndex
