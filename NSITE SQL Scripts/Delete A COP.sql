USE JLLIS
GO

DECLARE @COPID INT = 0

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.COP C ON C.COPClassificationDataID = T.ClassificationDataID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.COP C ON C.DescriptionClassificationDataID = T.ClassificationDataID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.COP C ON C.TitleClassificationDataID = T.ClassificationDataID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.SectionItem SI ON SI.DescriptionClassificationDataID = T.ClassificationDataID
	JOIN COP.Section S ON S.SectionID = SI.SectionID
	JOIN COP.COP C ON C.COPID = S.COPID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.SectionItem SI ON SI.SectionItemClassificationDataID = T.ClassificationDataID
	JOIN COP.Section S ON S.SectionID = SI.SectionID
	JOIN COP.COP C ON C.COPID = S.COPID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationData T
	JOIN COP.SectionItem SI ON SI.TitleClassificationDataID = T.ClassificationDataID
	JOIN COP.Section S ON S.SectionID = SI.SectionID
	JOIN COP.COP C ON C.COPID = S.COPID
		AND C.COPID = @COPID
print 'dbo.ClassificationData'

DELETE T
FROM dbo.ClassificationDataClassificationReason T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.ClassificationData CD
	WHERE CD.ClassificationDataID = T.ClassificationDataID
	)
print 'dbo.ClassificationDataClassificationReason'

DELETE T
FROM dbo.ClassificationDataCoalition T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.ClassificationData CD
	WHERE CD.ClassificationDataID = T.ClassificationDataID
	)
print 'dbo.ClassificationDataCoalition'

DELETE T
FROM dbo.ClassificationDataCountry T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.ClassificationData CD
	WHERE CD.ClassificationDataID = T.ClassificationDataID
	)
print 'dbo.ClassificationDataCountry'

DELETE T
FROM COP.COP T
WHERE T.COPID = @COPID
print 'COP.COP'

DELETE T
FROM COP.Favorite T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.COPID
	)
print 'COP.Favorite'

DELETE T
FROM dbo.IssueCOP T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.COPID
	)
print 'dbo.IssueCOP'

DELETE T
FROM COP.Section T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.COPID
	)
print 'COP.Section'

DELETE T
FROM COP.SectionItem T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM COP.Section S
	WHERE S.SectionID = T.SectionID
	)
print 'COP.SectionItem'

DELETE T
FROM dbo.EntityFavorite T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityFavorite'

DELETE T
FROM dbo.EntityJLLISFile T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityJLLISFile'

DELETE T
FROM dbo.EntityLink T
WHERE T.EntityTypeCode1 = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID1
	)
print 'dbo.EntityLink'

DELETE T
FROM dbo.EntityLink T
WHERE T.EntityTypeCode2 = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID2
	)
print 'dbo.EntityLink'

DELETE T
FROM dbo.EntityLinkDisplayOrder T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.EntityLink EL
	WHERE EL.EntityLinkID = T.EntityLinkID
	)
print 'dbo.EntityLinkDisplayOrder'

DELETE T
FROM dbo.EntityLocation T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityLocation'

DELETE T
FROM dbo.EntityMember T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityMember'

DELETE T
FROM dbo.EntityMetadata T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityMetadata'

DELETE T
FROM dbo.EntityMilestone T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityMilestone'

DELETE T
FROM dbo.EntityObjective T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityObjective'

DELETE T
FROM dbo.EntityOrganization T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityOrganization'

DELETE T
FROM dbo.EntityPublication T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityPublication'

DELETE T
FROM dbo.EntitySME T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntitySME'

DELETE T
FROM dbo.EntitySubscription T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntitySubscription'

DELETE T
FROM dbo.EntitySystemMetadata T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntitySystemMetadata'

DELETE T
FROM dbo.EntityTask T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.EntityTask'

DELETE T
FROM dbo.JLLISFeaturedItem T
WHERE T.EntityTypeCode = 'COP'
	AND NOT EXISTS
	(
	SELECT 1
	FROM COP.COP C
	WHERE C.COPID = T.EntityID
	)
print 'dbo.JLLISFeaturedItem'

DELETE T
FROM dbo.JLLISFeaturedItemTier T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.JLLISFeaturedItem JFI
	WHERE JFI.JLLISFeaturedItemID = T.JLLISFeaturedItemID
	)
print 'dbo.JLLISFeaturedItemTier'
