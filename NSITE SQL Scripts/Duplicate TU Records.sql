SELECT 
	JU1.FirstName,
	JU1.LastName, 
	TU1.TierUserID,
	TU1.JLLISUserID,
	T.TierName,
	T.TierID
FROM JLLIS.dbo.TierUser TU1
	JOIN JLLIS.dbo.Tier T ON T.TierID = TU1.TierID
	JOIN JLLIS.dbo.JLLISUser JU1 ON JU1.JLLISUserID = TU1.JLLISUserID
		AND EXISTS
			(
			SELECT
				JU2.JLLISUserID,
				TU2.TierID
			FROM JLLIS.dbo.TierUser TU2 
				JOIN JLLIS.dbo.JLLISUser JU2 ON JU2.JLLISUserID = TU2.JLLISUserID
					AND JU2.JLLISUserID = JU1.JLLISUserID
					AND TU1.TierID = TU2.TierID
			GROUP BY JU2.JLLISUserID, TU2.TierID
			HAVING COUNT(*) > 1
			)
ORDER BY T.TierID, JU1.LastName, JU1.FirstName, TU1.JLLISUserID, TU1.TierUserID

/**************/

SELECT 
	JU1.FirstName,
	JU1.LastName, 
	TU1.TierUserID,
	TU1.JLLISUserID,
	T1.TierName,
	T1.TierID
FROM JLLIS.dbo.TierUser TU1
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TU1.TierID
	JOIN JLLIS.dbo.JLLISUser JU1 ON JU1.JLLISUserID = TU1.JLLISUserID
		AND EXISTS
			(
			SELECT
				JU2.JLLISUserID,
				TU2.TierID
			FROM JLLIS.dbo.TierUser TU2 
				JOIN JLLIS.dbo.JLLISUser JU2 ON JU2.JLLISUserID = TU2.JLLISUserID
					AND JU2.JLLISUserID = JU1.JLLISUserID
					AND TU1.TierID = TU2.TierID
			GROUP BY JU2.JLLISUserID, TU2.TierID
			HAVING COUNT(*) > 1
			)
		AND NOT EXISTS
			(
			SELECT
				T4.MinID,
				T4.TierID
			FROM
				(
				SELECT 
					MIN(TU2.TierUserID) AS MinID,
					TU2.TierID
				FROM JLLIS.dbo.TierUser TU2
					JOIN JLLIS.dbo.Tier T2 ON T2.TierID = TU2.TierID
					JOIN JLLIS.dbo.JLLISUser JU2 ON JU2.JLLISUserID = TU2.JLLISUserID
						AND EXISTS
							(
							SELECT
								JU3.JLLISUserID,
								TU3.TierID
							FROM JLLIS.dbo.TierUser TU3 
								JOIN JLLIS.dbo.JLLISUser JU3 ON JU3.JLLISUserID = TU3.JLLISUserID
									AND JU3.JLLISUserID = JU2.JLLISUserID
									AND TU2.TierID = TU3.TierID
							GROUP BY JU3.JLLISUserID, TU3.TierID
							HAVING COUNT(*) > 1
							)
				GROUP BY TU2.JLLISUserID, TU2.TierID
				) T4
			WHERE T4.MinID = TU1.TierUserID
				AND T4.TierID = T1.TierID
			)
ORDER BY T1.TierID, JU1.LastName, JU1.FirstName, TU1.JLLISUserID, TU1.TierUserID
