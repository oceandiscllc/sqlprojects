--Begin PT 2299
DECLARE @nTierID INT

SELECT @nTierID = T.TierID
FROM JLLIS.dbo.Tier T
WHERE T.TierName LIKE '%Navy%'
	AND T.IsInstance = 1

UPDATE L
SET L.IsPublished = 1
FROM dbo.LMS L
WHERE L.Status IN ('Active','Closed','Validated')
	AND EXISTS
		(
		SELECT 1
		FROM JLLIS.dbo.GetDescendantTiersByTierID(@nTierID) DT
		WHERE DT.TierID = L.OriginatingTierID
		)
GO
--End PT 2299
