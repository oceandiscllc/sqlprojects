--Begin PT 2134
--Begin procedure Reporting.ObservationsByKeyword
EXEC Utility.DropObject 'Reporting.ObservationsByKeyword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.01.05
-- Description:	A stored procedure to return LMS records containing a specified keyword
--
-- Author:			Todd Pires
-- Modify Date:	2012.02.01
-- Description:	Added logic to limit the number of characters returned
--
-- Author:			Todd Pires
-- Modify Date:	2012.02.13
-- Description:	Added additional fields to the result set
--
-- Author:			Todd Pires
-- Modify Date:	2012.02.27
-- Description:	Added an optional truncation bit
--
-- Author:			Todd Pires
-- Modify Date:	2012.03.13
-- Description:	Added an optional IsSOLR bit
--
-- Author:			Todd Pires
-- Modify Date:	2012.03.15
-- Description:	Standardized the output column names
--
-- Author:			Todd Pires
-- Modify Date:	2012.03.19
-- Description:	Added additional fields for CaS support
--
-- Author:			Todd Pires
-- Modify Date:	2012.03.27
-- Description:	Added additional fields for Book Format support
--
-- Author:			Todd Pires
-- Modify Date:	2012.04.11
-- Description:	Added the SiteURL field
--
-- Author:			Todd Pires
-- Modify Date:	2012.04.13
-- Description:	Added the ClassificationHtml field
--
-- Author:			Todd Pires
-- Modify Date:	2012.08.23
-- Description:	Added the TierName field
--
-- Author:			Todd Pires
-- Modify Date:	2012.08.30
-- Description:	Added the ClassificationLabel support
--
-- Author:			Todd Pires
-- Modify Date:	2012.11.27
-- Description:	Added the LoginToken field
--
-- Author:			Todd Pires
-- Modify Date:	2013.02.28
-- Description:	Added additional fields to support observation exports
--
-- Author:			Daria Norris
-- Modify Date:	2013.04.11
-- Description:	Added additional fields to support observation exports
--
-- Author:			Todd Pires
-- Modify Date:	2013.04.29
-- Description:	Added the UpdatedByFormatted field
--
-- Author:			Todd Pires
-- Modify Date:	2013.04.29
-- Description:	Added the UpdatedByFormatted field
--
-- Author:			Todd Pires
-- Modify Date:	2013.08.30
-- Description:	Changed the JLLISUserID field to a sub-select
-- ====================================================================================
CREATE PROCEDURE Reporting.ObservationsByKeyword
	@nKeywordSearchID int,
	@cEntityTypeCode VARCHAR(50),
	@nTruncateData bit = 1,
	@nIsSOLR bit = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cDatabaseServerIPV4Address VARCHAR(MAX)
	DECLARE @nIsSecure int
	DECLARE @nMaxLength int
	
	SELECT @cDatabaseServerIPV4Address = SS.ServerSetupValue
	FROM JLLIS.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'DatabaseServerIPV4Address'

	SET @nIsSecure = 0
	
	IF EXISTS 
		(
		SELECT 1 
		FROM JLLIS.Dropdown.Classification CL 
		WHERE CL.IsActive = 1 
			AND CL.Classification <> 'Unclassified' 
			AND CL.Classificationid > 0
		) 

		BEGIN

		SET @nIsSecure = 1

		END
	--ENDIF
	
	SET @nMaxLength = 32767

	SELECT
		C.Country,
		E.Event, 
		E.StartDate AS EventStartDate,
		CONVERT(char(11), E.StartDate, 113) AS EventStartDateFormatted,
		E.EndDate AS EventEndDate,
		CONVERT(char(11), E.EndDate, 113) AS EventEndDateFormatted,
		EL.EventLocation,
		ES.EventSponsor,
		ET.EventType,
		ISNULL((SELECT S.SetupValue FROM Utility.Setup S WHERE S.SetupKey = 'SiteURL'), '') AS SiteURL,
		JLLIS.dbo.GetLoginToken('RedirectLMS', 'LMS', 0) AS LoginToken,
		L.Background,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassificationLabel,

		CASE

			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background)
		END AS BackgroundFormatted,

		L.CreatedBy,
		L.CreationDate,
		CONVERT(char(11), L.CreationDate, 113) AS CreationDateFormatted,
		L.DocumentDate,
		CONVERT(char(11), L.DocumentDate, 113) AS DocumentDateFormatted,
		L.DayPhone,
		L.DNSNumber,
		L.Email,
		L.EventDate,
		CONVERT(char(11), L.EventDate, 113) AS EventDateFormatted,

		L.EventDescription,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassificationLabel,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription)
		END AS EventDescriptionFormatted, 

		(SELECT JU.DayPhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateDayPhone,
		(SELECT JU.Email FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateEmail,
		(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateFirstName,
		(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateLastName,
		(SELECT T.TierLabel FROM JLLIS.dbo.Tier T JOIN JLLIS.dbo.JLLISUser JU ON JU.OriginatingTierID = T.TierID JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateOrganization,
		(SELECT JU.PayGrade FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreatePayGrade,
		(SELECT JU.TollFreePhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateTollFreePhone,
		ISNULL((SELECT JU.JLLISUserID FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')), 0) AS JLLISUserID,

		L.FirstName,
		L.FirstName + ' ' + L.LastName AS PostedBy,

		L.Implications,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassificationLabel,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications)
		END AS ImplicationsFormatted,

		L.ImportCode,
		L.ImportSource,
		L.IsDraft,
		L.JointLesson,
		L.LastName,
		L.LegacyID,
		L.LegacyLMSID,
		L.LMSID,
		L.LMSUnit,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS LMSCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS LMSClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS LMSReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationLabel,

		L.Observations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsClassificationLabel,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations)
		END AS ObservationsFormatted,
		L.OriginatingTierID,
		L.Rank,

		L.Recommendations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsClassificationLabel,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations)
		END AS RecommendationsFormatted,

		L.Status, 
		L.SubDayphone,
		L.SubDnsnumber,
		L.SubEmail,
		L.SubFirstname,
		L.SubLastname,
		L.SubRank,
		L.SubUnit,			
		L.Summary,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID) AS SummaryCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID) AS SummaryClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID) AS SummaryReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) AS SummaryClassificationLabel,		
		
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary)
		END AS SummaryFormatted,

		L.Topic,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS TopicCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS TopicClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS TopicReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) + ' ' + L.Topic AS TopicFormatted,

		JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationAuthority,
		JLLIS.dbo.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationSource,
		JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID) AS LMSDeclassificationDate,
		CONVERT(char(11), JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID), 113) AS LMSDeclassificationDateFormatted,

		L.Unit, 
		L.UpdateDate,
		CONVERT(char(11), L.UpdateDate, 113) AS UpdateDateFormatted,
		L.ViewCount,
		T.TierLabel AS Organization,
		T.TierName,
		W.WarfareMission,

		--begin book format support
		JLLIS.dbo.GetClassificationReasonListByClassificationDataID(L.LMSClassificationDataID, '     ') as LMSClassificationReasons,
		JLLIS.dbo.GetClassificationHeaderByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationHeader,
		@nIsSecure AS IsSecure,
		DB_NAME() AS cDatabaseName,
		@nKeywordSearchID AS nKeywordSearchID,
		@cDatabaseServerIPV4Address AS cDatabaseServerIPV4Address,

		CASE
			WHEN JLLIS.dbo.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) = 'Original'
			THEN '<b>Classification:</b>' + JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) + '<br>'
				+ '<b>Declassify Date:</b>' + CONVERT(char(11), JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID), 113) + '<br>'
				+ '<b>Classification Authority:</b>' + JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) + '<br>'
				+ '<b>Classification Source:</b>' + JLLIS.DBO.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) + '<br>'
				+ '<b>Classification Reasons:</b><br>' + JLLIS.DBO.GetClassificationReasonListByClassificationDataID(L.LMSClassificationDataID, '<br>')
			ELSE '<b>Classification:</b>' + JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) + '<br>'
				+ '<b>Classification Source:</b>' + JLLIS.DBO.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) + '<br>'
				+ '<b>Classification Authority:</b>' + JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) + '<br>'
		END AS ClassificationHtml,

		E.EventID,
		JLLIS.Utility.GetPersonNameByTierUserID(L.UpdatedBy, 'FirstLast') AS UpdatedByFormatted		
	FROM dbo.LMS L
		JOIN Dropdown.Event E ON E.EventID = L.EventID
		JOIN Dropdown.EventLocation EL ON EL.EventLocationID = E.EventLocationID
		JOIN Dropdown.EventSponsor ES ON ES.EventSponsorID = E.EventSponsorID
		JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
		JOIN Dropdown.WarfareMission W ON W.warfareMissionID = L.WarfareMissionID
		JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = L.CountryID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = @cEntityTypeCode 
			AND KSR.EntityID = L.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
			AND ((@nIsSOLR = 0) OR (@nIsSOLR = 1 AND KSR.IsSOLR = 1))
	ORDER BY L.LMSID DESC

END	
GO
--End procedure Reporting.ObservationsByKeyword
GO
--End PT 2134
