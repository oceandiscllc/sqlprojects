USE USAF
GO

--Begin file Common.sql
IF (SELECT OBJECT_ID('tempdb.dbo.#oTable1', 'u')) IS NOT NULL
  DROP TABLE #oTable1
--ENDIF
GO

CREATE TABLE #oTable1 
	(
	CodeName varchar(20),
	Name varchar(100),
	Description varchar(500),
	DisplayOrder int,
	Status varchar(20)
	)
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable2', 'u')) IS NOT NULL
  DROP TABLE #oTable2
--ENDIF
GO

CREATE TABLE #oTable2 (TierID int)
GO

INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('About','About','About',1,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('Featured','Featured','These are HOT Observations, Insights, or Recommendations requiring widest and immediate dissemination',2,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('Governance','Governance','Governance',3,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('ICOI','Internal Communities of Practice','Internal Communities of Practice',4,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('JCOI','Joint Communities of Practice','Joint Communities of Practice',5,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('MostViewed','Most Popular','Most Popular Observations',6,'Visible')
INSERT INTO #oTable1 (CodeName,Name,Description,DisplayOrder,Status) VALUES ('Newsletter','Newsletter','Observation & Recommendations Newsletters',7,'Visible')
GO

DECLARE @cInstanceName varchar(50)
DECLARE @nTierID int

SET @cInstanceName = 'USAF';
SET @nTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cInstanceName AND T.IsInstance = 1)

;
WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
INSERT INTO #oTable2
	(TierID)
SELECT 
	HD.TierID
FROM HD
	JOIN JLLIS.dbo.Tier T1 WITH (NOLOCK) ON T1.TierID = HD.TierID
		AND T1.CanManageTabs = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.Tab T2 WITH (NOLOCK)
			WHERE T2.TierID = T1.TierID
			)
GO

INSERT INTO dbo.Tab
	(CodeName,Name,Description,DisplayOrder,Status,TierID)
SELECT 
	T1.CodeName,
	T1.Name,
	T1.Description,
	T1.DisplayOrder,
	T1.Status,
	T2.TierID
FROM #oTable1 T1, #oTable2 T2
ORDER BY T2.TierID
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable1', 'u')) IS NOT NULL
  DROP TABLE #oTable1
--ENDIF
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable2', 'u')) IS NOT NULL
  DROP TABLE #oTable2
--ENDIF
GO
--End file Common.sql

