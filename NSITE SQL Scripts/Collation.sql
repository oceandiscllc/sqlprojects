SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ColumnName,
	T.Name,
	C.Max_Length,
	C.Is_Nullable,
	C.Collation_Name 
FROM sys.objects O
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C ON O.object_id = C.object_id
	JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
WHERE C.Collation_Name IS NOT NULL
	AND S.Name <> 'sys'
ORDER BY S.Name, O.Name, C.Name


SELECT 
	'ALTER TABLE ' 
	+ S.Name 
	+ '.' 
	+ O.Name
	+ ' ALTER COLUMN ' 
	+ C.Name
	+ ' ' 
	+ T.Name
	+ '('
	+ 
		CASE
			WHEN C.Max_Length = -1
			THEN 'max'
			ELSE CAST(C.Max_Length as varchar(10))
		END
	+ ') COLLATE Latin1_General_CI_AS'
	+ 
		CASE
			WHEN C.Is_Nullable = 1
			THEN ' NULL'
			ELSE ''
		END AS SQLText
FROM sys.objects O
	JOIN sys.schemas S ON S.schema_ID = O.schema_ID
	JOIN sys.columns C ON O.object_id = C.object_id
	JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
WHERE C.Collation_Name IS NOT NULL
	AND S.Name <> 'sys'
ORDER BY S.Name, O.Name, C.Name

ALTER TABLE dbo.AuditLog ALTER COLUMN AuditAction varchar(50) COLLATE Latin1_General_CI_AS NULL