--Begin PT 1053
IF NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WHERE LT.TierID = 0)
	BEGIN
	
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Header','Header',1,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Observation','Observation',2,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Event Description','Description',3,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Discussion','Discussion',4,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Recommendation','Recommendation',5,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Implications','Implication',6,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Comments','Comment',7,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Member Perspectives','Perspective',8,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Tags','Tag',9,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Files','File',10,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Posted By','Originator',11,0)
	INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,DisplayOrder,TierID) VALUES ('Shared','Shared',12,0)
	
	END
--ENDIF
GO
--End PT 1053