IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.IsAncestor') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.IsAncestor
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2010.09.27
-- Description:	A function to validate that one tier is an ancestor of another.
--							Parameter @nTierID1 is the ancestral tier.
--							Parameter @nTierID2 is the descendant tier.
--							A call of dbo.IsAncestor(x, y) will return a 1 if x is an 
--							ancestor of y and a 0 if it is not.  If x = y, the function
--							returns 1.
-- ============================================================================

CREATE FUNCTION dbo.IsAncestor
(
@nTierID1 int,
@nTierID2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
DECLARE @oTable table (TierID int)
	
SET @nRetVal = 0;

WITH HD (TierID,ParentTierID,CanManageContent,NodeLevel)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID, 
		T.CanManageContent,
		1 
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID2

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID, 
		T.CanManageContent,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable
	(TierID)
SELECT HD.TierID
FROM HD 
WHERE HD.TierID = @nTierID1

IF EXISTS (SELECT 1 FROM @oTable)
	SET @nRetVal = 1

RETURN @nRetVal

END
GO
