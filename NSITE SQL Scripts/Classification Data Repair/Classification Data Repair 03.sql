USE JLLIS
GO

DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

SET @cSQL = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		UPPER(DB.Name)
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('ACGU','ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','USAF','USUHS')
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN
		
	SET @cSQL = 'UPDATE L SET L.CommentCaveat = TCD.CommentCaveat, L.ReleasableTo = TCD.ReleasableTo FROM'
	SET @cSQL = @cSQL + ' ' + @cDatabaseName + '.dbo.LMS L'
	SET @cSQL = @cSQL + ' JOIN JLLIS.Utility.TemporaryClassificationData TCD ON TCD.LMSID = L.LMSID AND TCD.DatabaseName =' 
	SET @cSQL = @cSQL + ' ''' + @cDatabaseName + ''''

	EXECUTE (@cSQL)

	FETCH oCursor into @cDatabaseName
		
	END
--END WHILE

CLOSE oCursor   
DEALLOCATE oCursor

DROP TABLE Utility.TemporaryClassificationData