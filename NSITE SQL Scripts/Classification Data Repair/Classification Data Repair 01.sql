USE JLLIS
GO

EXEC JLLIS.Utility.AddColumn 'JLLIS.dbo.ClassificationData', 'TempFieldName', 'varchar(50)'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.TemporaryClassificationData'

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	DROP TABLE Utility.TemporaryClassificationData

CREATE TABLE Utility.TemporaryClassificationData
	(
	TemporaryClassificationDataID int IDENTITY(1,1) NOT NULL,
	LMSID int,
	DatabaseName varchar(50),
	CommentCaveat varchar(50),
	ReleasableTo varchar(50)
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TemporaryClassificationDataID'
GO

DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		UPPER(DB.Name)
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('ACGU','ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','USAF','USUHS')
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN
		
	SET @cSQL = 'INSERT INTO Utility.TemporaryClassificationData (LMSID,DatabaseName,CommentCaveat,ReleasableTo) SELECT'
	SET @cSQL = @cSQL + ' CAST(L.LMSID as varchar(10)),'
	SET @cSQL = @cSQL + ' ''' + @cDatabaseName + ''','
	SET @cSQL = @cSQL + ' L.CommentCaveat,'
	SET @cSQL = @cSQL + ' L.ReleasableTo'
	SET @cSQL = @cSQL + ' FROM ' + @cDatabaseName + '.dbo.LMS L'

	EXECUTE (@cSQL)

	FETCH oCursor into @cDatabaseName
		
	END
--END WHILE

CLOSE oCursor   
DEALLOCATE oCursor

SELECT 
	'INSERT INTO JLLIS.Utility.TemporaryClassificationData (LMSID,DatabaseName,CommentCaveat,ReleasableTo) VALUES ('
		+	CAST(TCD.LMSID as varchar(10)) + ','
		+ '''' + TCD.DatabaseName + ''','
		+ '' 
		+ CASE
				WHEN TCD.CommentCaveat IS NULL OR LEN(RTRIM(TCD.CommentCaveat)) = 0
				THEN 'NULL'
				ELSE '''' + TCD.CommentCaveat + ''''
			END
		+ ',' 
		+ CASE
				WHEN TCD.ReleasableTo IS NULL OR LEN(RTRIM(TCD.ReleasableTo)) = 0
				THEN 'NULL'
				ELSE '''' + TCD.ReleasableTo + ''''
			END
		+ ')'
	AS SQLText
FROM JLLIS.Utility.TemporaryClassificationData TCD