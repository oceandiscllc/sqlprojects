USE JLLIS
GO

EXEC JLLIS.Utility.AddColumn 'JLLIS.dbo.ClassificationData', 'TempFieldName', 'varchar(50)'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.TemporaryClassificationData'

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	DROP TABLE Utility.TemporaryClassificationData

CREATE TABLE Utility.TemporaryClassificationData
	(
	TemporaryClassificationDataID int IDENTITY(1,1) NOT NULL,
	LMSID int,
	DatabaseName varchar(50),
	CommentCaveat varchar(50),
	ReleasableTo varchar(50)
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TemporaryClassificationDataID'
GO
