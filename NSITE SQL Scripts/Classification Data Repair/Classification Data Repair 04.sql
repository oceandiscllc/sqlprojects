USE JLLIS
GO

DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		UPPER(DB.Name)
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('ACGU','ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','USAF','USUHS')
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
		DROP TABLE #tTable
	
	CREATE TABLE #tTable 
		(
		ClassificationDataID int
		)
		
	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.BackgroundClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.BackgroundClassificationDataID > 0'

	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.EventDescriptionClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.EventDescriptionClassificationDataID > 0'
	
	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.ImplicationsClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.ImplicationsClassificationDataID > 0'

	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.LMSClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.LMSClassificationDataID > 0'

	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.ObservationsClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.ObservationsClassificationDataID > 0'

	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.RecommendationsClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.RecommendationsClassificationDataID > 0'

	EXECUTE (@cSQL)

	SET @cSQL = 'INSERT INTO #tTable (ClassificationDataID) SELECT L.SummaryClassificationDataID FROM ' + @cDatabaseName + '.dbo.LMS L WHERE L.SummaryClassificationDataID > 0'

	EXECUTE (@cSQL)

	DELETE T
	FROM #tTable T
	WHERE EXISTS
		(
		SELECT D.ClassificationDataID
		FROM
			(
			SELECT 
				COUNT(ClassificationDataID) AS ItemCount, 
				ClassificationDataID
			FROM #tTable
			GROUP BY ClassificationDataID
			HAVING COUNT(ClassificationDataID) = 1
			) D
		WHERE D.ClassificationDataID = T.ClassificationDataID
		)

	DELETE CD
	FROM JLLIS.dbo.ClassificationData CD
	WHERE EXISTS
		(
		SELECT 1
		FROM #tTable T
		WHERE T.ClassificationDataID = CD.ClassificationDataID
		)
		
	DELETE CDCR
	FROM JLLIS.dbo.ClassificationDataClassificationReason CDCR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.dbo.ClassificationData CD
		WHERE CD.ClassificationDataID = CDCR.ClassificationDataID
		)
		
	DELETE CDC
	FROM JLLIS.dbo.ClassificationDataCoalition CDC
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.dbo.ClassificationData CD
		WHERE CD.ClassificationDataID = CDC.ClassificationDataID
		)
		
	DELETE CDC
	FROM JLLIS.dbo.ClassificationDataCountry CDC
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.dbo.ClassificationData CD
		WHERE CD.ClassificationDataID = CDC.ClassificationDataID
		)

	SET @cSQL = 'UPDATE L SET L.BackgroundClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.BackgroundClassificationDataID)'
	EXECUTE (@cSQL)
	
	SET @cSQL = 'UPDATE L SET L.EventDescriptionClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.EventDescriptionClassificationDataID)'
	EXECUTE (@cSQL)

	SET @cSQL = 'UPDATE L SET L.ImplicationsClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.ImplicationsClassificationDataID)'
	EXECUTE (@cSQL)

	SET @cSQL = 'UPDATE L SET L.LMSClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.LMSClassificationDataID)'
	EXECUTE (@cSQL)

	SET @cSQL = 'UPDATE L SET L.ObservationsClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.ObservationsClassificationDataID)'
	EXECUTE (@cSQL)

	SET @cSQL = 'UPDATE L SET L.RecommendationsClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.RecommendationsClassificationDataID)'
	EXECUTE (@cSQL)

	SET @cSQL = 'UPDATE L SET L.SummaryClassificationDataID = 0 FROM ' + @cDatabaseName + '.dbo.LMS L WHERE EXISTS (SELECT 1 FROM #tTable T WHERE T.ClassificationDataID = L.SummaryClassificationDataID)'
	EXECUTE (@cSQL)

	FETCH oCursor into @cDatabaseName
		
	END
--END WHILE

CLOSE oCursor   
DEALLOCATE oCursor

DROP TABLE #tTable
GO