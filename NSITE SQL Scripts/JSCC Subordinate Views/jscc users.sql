SELECT 
	JU.JLLISUserID,
	JU.jllisusername,
	JU.LastName,
	JU.FirstName,
	JU.email,
	JU.dayphone,
	JU.paygrade, 
	JU.title, 	
	T1.TierLabel AS Organization
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = JU.OriginatingTierID
		AND T1.TierName = 'JSCC'
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = TU.TierID
		AND T2.TierName = 'JSCC'
		AND TU.Status = 'Active'
GO