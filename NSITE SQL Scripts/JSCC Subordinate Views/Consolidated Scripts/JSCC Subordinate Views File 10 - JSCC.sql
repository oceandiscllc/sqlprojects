USE JSCC
GO

--Begin file Common.sql

--End file Common.sql

--Begin file JSCC.sql
--Begin CDRFileView.sql
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.CDRFileView'))
	DROP VIEW dbo.CDRFileView
GO

CREATE VIEW dbo.CDRFileView
AS
SELECT
	C.CDRID,
	C.City,
	C.Classification,
	C.Condition,
	C.CountryID,
	C.CreationDate,
	C.DeclassifyOn,
	C.Dissemination,
	C.DocCountryDropDownID,
	C.DocDate,
	C.EMail,
	C.FirstName + ' ' + C.LastName AS AuthorName,
	C.FirstName,
	'C' AS JointCDR,
	C.Keywords,
	C.LastName,
	C.OriginatorCountryID,
	C.OverallCaveat,
	C.Phone,
	C.Place,
	C.Province,
	C.ReleasableTo,
	C.Status,
	C.Subtitle,
	C.Summary,
	C.ThumbHeight,
	C.Thumbnail,
	C.ThumbWidth,
	C.Title,
	C.UpdateDate,
	CC.CDRCategory AS Category,
	CF.CDRFileID, 
	CF.CDRID AS CDRFile_CDRID, 
	CF.Condition AS CDRFile_Condition, 
	CF.FileName, 
	CF.Status AS CDRFile_Status,
	CO.Country,
	CQ.CDRCreatorQualifier AS CreatorQualifier,
	CT.CDRType AS Type,
	T.TierName AS Organization
FROM dbo.CDR C WITH (NOLOCK)
	JOIN dbo.CDRFile CF WITH (NOLOCK) ON CF.CDRID = C.CDRID
		AND C.JointCDR = 'C'
		AND C.Status = 'Active'
		AND CF.Status = 'Active'
	JOIN Dropdown.CDRCategory CC ON CC.CDRCategoryID = C.CDRCategoryID
	JOIN Dropdown.CDRCreatorQualifier CQ ON CQ.CDRCreatorQualifierID = C.CDRCreatorQualifierID
	JOIN Dropdown.CDRType CT ON CT.CDRTypeID = C.CDRTypeID
	JOIN JLLIS.dbo.Tier T ON T.TierID = C.OriginatingTierID
	JOIN JLLIS.Dropdown.Country CO ON CO.CountryID = C.CountryID
	JOIN JLLIS.Dropdown.Status S WITH (NOLOCK) ON S.Status = C.Status 
			AND C.JointCDR IN ('C','Y')
			AND S.IsForJointSearch = 1
GO
--End CDRFileView.sql

-- Begin dbo.LMSFileView
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.LMSFileView'))
	DROP VIEW dbo.LMSFileView
GO

CREATE VIEW dbo.LMSFileView
AS
SELECT
	C.Country,
	L.actionactive,
	L.activatedate,
	L.background,
	L.backgroundClass,
	L.backgroundrelto,
	L.battleboardids,
	L.bbid,
	L.campaign,
	L.category,
	L.classification,
	L.classifiedby,
	L.commentcaveat,
	L.condition,
	L.countryid,
	L.createdby,
	L.creationdate,
	L.dailydigest,
	L.dateofsource,
	L.dayphone,
	L.declassifiedon,
	L.derivedfrom,
	L.discussioncaveat,
	L.disposition,
	L.distributionechelon,
	L.dnsnumber,
	L.documentDate,
	L.email,
	L.environmentalattributes,
	L.eventcaveat,
	L.eventdate,
	L.eventdescription,
	L.eventdescriptionClass,
	L.eventdescriptionrelto,
	L.eventopex,
	L.eventwx,
	L.exemptedsource,
	L.exercise,
	L.externalid,
	L.firstname + ' ' + L.lastname AS authorname,
	L.firstname,
	L.fixedWingDivision,
	L.forces,
	L.formalactionrequired,
	L.implicationcaveat,
	L.implications,
	L.implicationsClass,
	L.implicationsrelto,
	L.intelInfoSystems,
	L.interimaction,
	L.iscandc,
	L.isdoctrine,
	L.isDraft,
	L.isfacilities,
	L.isfires,
	L.isforceprotection,
	L.isintelligence,
	L.isleadershipeducation,
	L.islogistics,
	L.ismaneuver,
	L.ismaterial,
	L.ismedical,
	L.isorganization,
	L.ispersonnel,
	L.issafety,
	L.istraining,
	'C' AS jointlesson,
	L.lastname,
	L.legacyid,
	L.lessontype,
	L.lmsid,
	L.lmsunit,
	L.maritimeRotaryDivision,
	L.mobileid,
	L.observationcaveat,
	L.observations,
	L.observationsClass,
	L.observationsrelto,
	L.operationalattributes,
	L.operationtype,
	L.overallcaveat,
	L.rank,
	L.recommendationcaveat,
	L.recommendations,
	L.recommendationsClass,
	L.recommendationsrelto,
	L.releasableto,
	L.ruc,
	L.specialprograms,
	L.status,
	L.summary,
	L.summaryClass,
	L.summaryrelto,
	L.taskforce,
	L.topic,
	L.unit,
	L.updatedate,
	L.updatedby,
	L.usereventname,
	LF.condition AS lmsfile_condition, 
	LF.filename, 
	LF.lmsfileid, 
	LF.lmsid AS lmsfile_lmsid, 
	LF.status AS lmsfile_status, 
	T.TierName AS majorcommand
FROM dbo.LMS L WITH (NOLOCK)
	LEFT JOIN dbo.LMSFile LF WITH (NOLOCK) ON LF.LMSID = L.LMSID
		AND LF.Status = 'Active'
	JOIN JLLIS.Dropdown.Country C WITH (NOLOCK) ON C.Countryid = L.Countryid
	JOIN JLLIS.Dropdown.Status S WITH (NOLOCK) ON S.Status = L.Status 
		AND L.JointLesson IN ('C','Y')
		AND S.IsForJointSearch = 1
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = L.OriginatingTierID
GO
--End dbo.LMSFileView

--Begin Utility.CreateSubordinateCDRFileView
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Utility.CreateSubordinateCDRFileView') AND type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CreateSubordinateCDRFileView
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2011.11.09
-- Description:	a stored procedure to drop and create the various CDRFileViews
--							for subordiante tiers
-- ===========================================================================
CREATE PROCEDURE Utility.CreateSubordinateCDRFileView
	@cTierName varchar(250),
	@nPrintSQL bit

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)
	DECLARE @cSQL4 varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cViewName varchar(250)
	
	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)

	SET @cViewName = 'dbo.CDRFileView_' + @cTierName

	SET @cSQL1 = 'IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID(''' + @cViewName + ''')) DROP VIEW ' + @cViewName
	
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cCRLF + 'GO'
		print @cSQL1
		
		END
	ELSE
		EXEC (@cSQL1)
	--ENDIF

	DECLARE @oTable1 table (RowID int identity(1,1) primary key, SQLText varchar(max))
	DECLARE @oTable2 table (RowID int identity(1,1) primary key, SQLText varchar(max))

	IF @cTierName <> 'JSCC'
		BEGIN
		
		INSERT INTO @oTable1 (SQLText) VALUES ('CREATE VIEW ' + @cViewName + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('AS' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('WITH HD (TierID,ParentTierID)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'AS' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + '(' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'SELECT' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.TierID,' )
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'WHERE T.TierID =' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + '(' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT T1.TierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T1 WITH(NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN JLLIS.dbo.Tier T2 WITH(NOLOCK) ON T2.TierID = T1.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T1.TierName = ''' + @cTierName + '''' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.TierName = ''JSCC''' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.IsInstance = 1' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + ')' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'UNION ALL' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.TierID,' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN HD ON HD.TierID = T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + ')' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('SELECT' + @cCRLF)

		END
	--ENDIF
	
	INSERT INTO @oTable2 exec sp_helptext 'dbo.CDRFileView'

	IF @cTierName <> 'JSCC'
		BEGIN

		DELETE 
		FROM @oTable2
		WHERE RowID < 
			(
			SELECT TOP 1 RowID
			FROM @oTable2
			WHERE SQLText LIKE '%,%'
			ORDER BY RowID
			)
	
		UPDATE @oTable2
		SET SQLText = REPLACE(SQLText, '    ', @cTab1)
	
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab2 + 'AND EXISTS' + @cCRLF)
	
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + '(' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'SELECT 1' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'FROM HD' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'WHERE HD.TierID = C.OriginatingTierID' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + ')' + @cCRLF)

		END
	ELSE
		BEGIN
	
		UPDATE @oTable2
		SET SQLText = REPLACE(SQLText, 'CDRFileView', 'CDRFileView_' + @cTierName)

		END
	--ENDIF
	
	INSERT INTO @oTable1 (SQLText) SELECT T2.SQLText FROM @oTable2 T2

	SET @cSQL1 = ''
	SET @cSQL2 = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SQLText
		FROM @oTable1
		ORDER BY RowID

	OPEN oCursor
	FETCH oCursor into @cSQL2
	WHILE @@fetch_status = 0
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cSQL2
		
		FETCH oCursor into @cSQL2
			
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor
	
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cCRLF + 'GO'
		print @cSQL1

		END
	ELSE
		EXEC (@cSQL1)
	--ENDIF
END
GO
--End Utility.CreateSubordinateCDRFileView

--Begin Utility.CreateSubordinateLMSFileView
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Utility.CreateSubordinateLMSFileView') AND type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CreateSubordinateLMSFileView
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2011.11.09
-- Description:	a stored procedure to drop and create the various LMSFileViews
--							for subordiante tiers
-- ===========================================================================
CREATE PROCEDURE Utility.CreateSubordinateLMSFileView
	@cTierName varchar(250),
	@nPrintSQL bit

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)
	DECLARE @cSQL4 varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cViewName varchar(250)

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)

	SET @cViewName = 'dbo.LMSFileView_' + @cTierName

	SET @cSQL1 = 'IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID(''' + @cViewName + ''')) DROP VIEW ' + @cViewName
	
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cCRLF + 'GO'
		print @cSQL1

		END
	ELSE
		EXEC (@cSQL1)
	--ENDIF

	DECLARE @oTable1 table (RowID int identity(1,1) primary key, SQLText varchar(max))
	DECLARE @oTable2 table (RowID int identity(1,1) primary key, SQLText varchar(max))

	IF @cTierName <> 'JSCC'
		BEGIN

		INSERT INTO @oTable1 (SQLText) VALUES ('CREATE VIEW ' + @cViewName + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('AS' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('WITH HD (TierID,ParentTierID)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'AS' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + '(' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'SELECT' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.TierID,' )
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'WHERE T.TierID =' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + '(' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT T1.TierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T1 WITH(NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN JLLIS.dbo.Tier T2 WITH(NOLOCK) ON T2.TierID = T1.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T1.TierName = ''' + @cTierName + '''' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.TierName = ''JSCC''' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.IsInstance = 1' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + ')' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'UNION ALL' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.TierID,' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN HD ON HD.TierID = T.ParentTierID' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + ')' + @cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES (@cCRLF)
		INSERT INTO @oTable1 (SQLText) VALUES ('SELECT' + @cCRLF)

		END
	--ENDIF
	
	INSERT INTO @oTable2 exec sp_helptext 'dbo.LMSFileView'

	IF @cTierName <> 'JSCC'
		BEGIN

		DELETE
		FROM @oTable2
		WHERE RowID < 
			(
			SELECT TOP 1 RowID
			FROM @oTable2
			WHERE SQLText LIKE '%,%'
			ORDER BY RowID
			)
	
		UPDATE @oTable2
		SET SQLText = REPLACE(SQLText, '    ', @cTab1)
	
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab2 + 'AND EXISTS' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + '(' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'SELECT 1' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'FROM HD' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'WHERE HD.TierID = L.OriginatingTierID' + @cCRLF)
		INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + ')' + @cCRLF)

		END
	ELSE
		BEGIN
	
		UPDATE @oTable2
		SET SQLText = REPLACE(SQLText, 'LMSFileView', 'LMSFileView_' + @cTierName)

		END
	--ENDIF
	
	INSERT INTO @oTable1 (SQLText) SELECT T2.SQLText FROM @oTable2 T2

	SET @cSQL1 = ''
	SET @cSQL2 = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SQLText
		FROM @oTable1
		ORDER BY RowID

	OPEN oCursor
	FETCH oCursor into @cSQL2
	WHILE @@fetch_status = 0
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cSQL2
		
		FETCH oCursor into @cSQL2
			
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor
		
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cCRLF + 'GO'
		print @cSQL1

		END
	ELSE
		EXEC (@cSQL1)
	--ENDIF
END
GO
--End Utility.CreateSubordinateLMSFileView

--Begin subordinate view creation
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'africom', @nPrintSQL = 0
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'centcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'eucom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jcisfa', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jfcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jpra', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'js', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jscc', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'northcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'pacom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'southcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'stratcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'transcom', @nPrintSQL = 0
GO

EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'africom', @nPrintSQL = 0
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'centcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'eucom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jcisfa', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jfcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jpra', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'js', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jscc', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'northcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'pacom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'southcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'stratcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'transcom', @nPrintSQL = 0
GO
--End subordinate view creation
--End file JSCC.sql

