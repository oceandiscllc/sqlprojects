--Begin CMT 10056
USE Navy
GO

DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LMSDiscussClassificationDataID'
SET @cTableName = 'dbo.LMSDiscuss'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--End CMT 10056

--Begin CMT 10069
--Begin table Reporting.KeywordSearch
USE JLLIS
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Reporting.KeywordSearchResult'

EXEC Utility.AddColumn @cTableName, 'IsKeywordSearch', 'bit'
EXEC Utility.SetDefault @cTableName, 'IsKeywordSearch', 1
EXEC Utility.SetColumnNotNull @cTableName, 'IsKeywordSearch', 'bit'
GO
--End table Reporting.KeywordSearchResult
--End CMT 10069

--Begin CMT 10237
--Begin procedure Reporting.ObservationDiscuss
USE Navy
GO

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationDiscuss') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationDiscuss
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to return LMSDiscuss records
-- ============================================================
CREATE PROCEDURE Reporting.ObservationDiscuss
	@nKeywordSearchID int,
	@nTruncateData bit = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMaxLength int
	SET @nMaxLength = 32767

	SELECT
		LD.LMSID,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.Utility.StripHTML(LD.Discussion), @nMaxLength)
			ELSE JLLIS.Utility.StripHTML(LD.Discussion)
		END AS Discussion,

		LD.CreatedBy, 
		LD.CreationDate,
		CONVERT(char(11), LD.CreationDate, 113) AS CreationDateFormatted,
		
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.Classification 
			ELSE JLLIS.dbo.GetClassificationByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS Classification,
		
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.OverallCaveat
			ELSE JLLIS.dbo.GetClassificationByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS Caveat,
			
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.ReleasableTo
			ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS ReleasableTo
	
	FROM dbo.LMSDiscuss LD
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Lesson'
			AND KSR.EntityID = LD.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	ORDER BY LD.LMSID, LD.LMSDiscussID DESC

END
GO
--End procedure Reporting.ObservationDiscuss

--Begin procedure Reporting.ObservationExport
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationExport') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationExport
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to export an observation
-- ========================================================
CREATE PROCEDURE Reporting.ObservationExport
	@nLMSID int = 0,
	@dDateStart date = NULL,
	@dDateStop date = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nJLLISUserID int
	DECLARE @nKeywordSearchID int

	SELECT @nJLLISUserID = (MAX(JU.JLLISUserID) * 10)
	FROM JLLIS.dbo.JLLISUser JU
	
	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)
	
	INSERT INTO JLLIS.Reporting.KeywordSearch
		(JLLISUserID)
	VALUES
		(@nJLLISUserID)
	
	SELECT @nKeywordSearchID = SCOPE_IDENTITY()

	IF @dDateStart IS NOT NULL
		BEGIN

		IF @dDateStop IS NULL
			SET @dDateStop = getDate()
			
		SET @dDateStop = DATEADD(d, 1, @dDateStop)
		
		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		SELECT
			@nKeywordSearchID,
			'Lesson',
			L.LMSID,
			0
		FROM dbo.LMS L
		WHERE L.CreationDate BETWEEN @dDateStart AND @dDateStop
			OR L.UpdateDate BETWEEN @dDateStart AND @dDateStop
		
		END
	ELSE
		BEGIN

		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		VALUES
			(
			@nKeywordSearchID,
			'Lesson',
			@nLMSID,
			0
			)
		
		END
		
	--ENDIF	

	EXEC Reporting.ObservationsByKeyword @nKeywordSearchID, 'Lesson', 0
	EXEC Reporting.ObservationDiscuss @nKeywordSearchID, 0
	EXEC Reporting.ObservationMetadata @nKeywordSearchID

	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)

END
GO
--End procedure Reporting.ObservationExport

--Begin procedure Reporting.ObservationMetadata
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationMetadata') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationMetadata
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to return Metadata records associated with LMS records
-- ======================================================================================
CREATE PROCEDURE Reporting.ObservationMetadata
	@nKeywordSearchID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LAC.LMSID,
		'AnalysisCode' AS Category,
		ACT.AnalysisCodeType AS Type,
		AC.AnalysisCode AS Item
	FROM dbo.LMSAnalysisCode LAC
		JOIN Dropdown.AnalysisCode AC ON AC.AnalysisCodeID = LAC.AnalysisCodeID
		JOIN Dropdown.AnalysisCodeType ACT ON ACT.AnalysisCodeTypeID = AC.AnalysisCodeTypeID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Lesson'
			AND KSR.EntityID = LAC.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	
	UNION
	
	SELECT
		LMD.LMSID,
		'Metadata' AS Category,
		MDT.MetadataType AS Type,
		MD.Metadata AS Item
	FROM dbo.LMSMetadata LMD
		JOIN Dropdown.Metadata MD ON MD.MetadataID = LMD.MetadataID
		JOIN Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Lesson'
			AND KSR.EntityID = LMD.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	
	UNION
	
	SELECT
		LT.LMSID,
		'Task' AS Category,
		T.TaskNumber AS Type,
		T.TaskTitle AS Item
	FROM dbo.LMSTask LT
		JOIN dbo.Task T ON T.TaskID = LT.TaskID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Lesson'
			AND KSR.EntityID = LT.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	
	ORDER BY LMSID, Category, Type, Item

END
GO
--End procedure Reporting.ObservationMetadata
--End CMT 10237