WITH DupeUsers (GoodTierUserID,JLLISUserID) AS
	(
	SELECT
		MIN(TU.TierUserID) AS GoodTierUserID,
		TU.JLLISUserID
	FROM JLLIS.dbo.TierUser TU
		JOIN JLLIS.dbo.Tier T ON T.TierID = TU.TierID
			AND T.TierName = 'JSCC'
			AND T.IsInstance = 1
			AND TU.JLLISUserID > 0
			AND TU.Status = 'Active'
	GROUP BY 
		TU.JLLISUserID
	HAVING COUNT(TU.TierUserID) > 1
	)
	
UPDATE TU
SET TU.Status = 'Deleted'
FROM JLLIS.dbo.TierUser TU
	JOIN DupeUsers DU ON DU.JLLISUserID = TU.JLLISUserID
	JOIN JLLIS.dbo.Tier T ON T.TierID = TU.TierID
		AND T.TierName = 'JSCC'
		AND T.IsInstance = 1
		AND TU.JLLISUserID > 0
		AND TU.Status = 'Active'
		AND TU.TierUserID <> DU.GoodTierUserID
