USE JSCC
GO

SELECT
	T2.TierLabel AS InstanceName,
	
	CASE
		WHEN T1.ParentTierID = 0
		THEN ''
		ELSE (SELECT T3.TierLabel FROM JLLIS.dbo.Tier T3 WHERE T3.TierID = T1.ParentTierID)
	END AS ParentOrganizationName, 

	T1.TierLabel AS OrganizationName,
	T1.CanManageDropdowns, 
	ET.EventTypeID,
	ET.EventType,
	ET.IsActive AS EventTypeIsActive
FROM Dropdown.TierDropdown TD
	JOIN JLLIS.Dropdown.DropdownMetaData DMD ON DMD.DropdownMetaDataID = TD.DropdownMetaDataID
		AND DMD.DropdownMetaDataCode = 'EventType'
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TD.TierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
	JOIN Dropdown.EventType ET ON ET.EventTypeID = TD.DropdownEntityID
ORDER BY 1, 2, 3, 5
GO

SELECT
	T2.TierLabel AS InstanceName,
	
	CASE
		WHEN T1.ParentTierID = 0
		THEN ''
		ELSE (SELECT T3.TierLabel FROM JLLIS.dbo.Tier T3 WHERE T3.TierID = T1.ParentTierID)
	END AS ParentOrganizationName, 

	T1.TierLabel AS OrganizationName, 
	T1.CanManageDropdowns, 
	ET.EventTypeID,
	ET.EventType,
	ET.IsActive AS EventTypeIsActive,
	E.EventID,
	E.Event,
	E.IsActive AS EventIsActive
FROM Dropdown.TierDropdown TD
	JOIN JLLIS.Dropdown.DropdownMetaData DMD ON DMD.DropdownMetaDataID = TD.DropdownMetaDataID
		AND DMD.DropdownMetaDataCode = 'Event'
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TD.TierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
	JOIN Dropdown.Event E ON E.EventID = TD.DropdownEntityID
	JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
ORDER BY 1, 2, 3, 5, 7
GO