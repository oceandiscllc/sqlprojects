-- The HYBRID Model --
USE JLLIS

DECLARE @cInstanceType varchar (50)
DECLARE @nInstanceTierID int
DECLARE @nTierID int
DECLARE @oTable table (TierID int)

--Step 1:  Find out what tier I am in
SET @nTierID = 172--(SELECT T.TierID FROM dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'MARSOC Child 2');

--Step 2:  Get my progenitor
WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable (TierID) SELECT TierID FROM HD

SET @cInstanceType = (SELECT TOP 1 T1.InstanceType FROM dbo.Tier T1 WITH (NOLOCK) JOIN @oTable T2 ON T2.TierID = T1.TierID AND T1.IsInstance = 1)
SET @nInstanceTierID = (SELECT TOP 1 T1.TierID FROM dbo.Tier T1 WITH (NOLOCK) JOIN @oTable T2 ON T2.TierID = T1.TierID AND T1.IsInstance = 1)

--Step 3:  Get me and my parent for the dropdown
SELECT 

	CASE
		WHEN T.TierID = @nTierID
		THEN 1
		ELSE 0
	END AS IsSelected,

	CASE
		WHEN T.TierID = @nInstanceTierID
		THEN 0
		ELSE 1
	END AS DisplayOrder,

	CASE
		WHEN T.TierID = @nInstanceTierID
		THEN CAST(T.TierID as varchar(50))
		ELSE CAST(@nInstanceTierID as varchar(50)) + ',' + CAST(T.TierID as varchar(50))
	END AS TierLineage,

	T.TierName,
	T.TierID, 

	CASE
		WHEN T.TierID = @nInstanceTierID
		THEN 0
		ELSE 1
	END AS NodeLevel
	
FROM dbo.Tier T WITH (NOLOCK) 
WHERE T.TierID IN (@nInstanceTierID, @nTierID)
ORDER BY DisplayOrder


