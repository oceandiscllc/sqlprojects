USE JSCC
GO

SELECT 
	T1.TierID,
	T1.TierLabel,
	ET.EventTypeID,
	ET.EventType,
	E.EventID,
	E.Event
FROM Dropdown.TierDropdown TD
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TD.TierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
		AND T2.TierName LIKE '%SOCOM%'
		AND T2.IsInstance = 1
	JOIN JLLIS.Dropdown.DropdownMetadata DMD ON DMD.DropdownMetadataID = TD.DropdownMetadataID
		AND DMD.DropdownMetadataCode = 'Event'
	JOIN Dropdown.Event E ON E.EventID = TD.DropdownEntityID
	JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
ORDER BY T1.TierLabel, T1.TierID, ET.EventType, ET.EventTypeID, E.Event, E.EventID
