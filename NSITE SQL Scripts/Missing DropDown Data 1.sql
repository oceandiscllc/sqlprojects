USE ARMY
GO

DECLARE @cSQL varchar(max)
DECLARE @oTable table (TierID int, TierName varchar(50), Category varchar(50))

DECLARE oCursor CURSOR FOR 
	SELECT DISTINCT DD.Category
	FROM dbo.DropDown DD WITH (NOLOCK)
	WHERE DD.Category NOT LIKE '%_Old'
		AND LEN(LTRIM(DD.Category)) > 0
	ORDER BY DD.Category

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN
		
	INSERT INTO @oTable
		(TierID,TierName,Category)
	SELECT
		T1.TierID,
		T1.TierName,
		@cSQL AS Category
	FROM JLLIS.dbo.Tier T1 WITH (NOLOCK)
		JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierID = T1.InstanceID
			AND T2.TierName = 'ARMY'
			AND T2.IsInstance = 1
			AND T1.CanManageDropdowns = 1
			AND NOT EXISTS 
				(
				SELECT 1
				FROM dbo.DropDown DD WITH (NOLOCK)
				WHERE DD.TierID = T1.TierID
					AND DD.Category = @cSQL
				)

	FETCH oCursor INTO @cSQL
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
			
SELECT *
FROM @oTable T
ORDER BY T.TierName, T.Category