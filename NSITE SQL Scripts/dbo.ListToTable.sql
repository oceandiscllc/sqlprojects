SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ListToTable') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.ListToTable
GO

-- -------------------------------------------------
-- Author:			Todd Pires
-- Create date: 2010.06.10
-- Description:	Converts a delimited list to a table
-- -------------------------------------------------

CREATE FUNCTION dbo.ListToTable 
	(
	@cList varchar (max),
	@cDelimiter char(1)
	)

RETURNS @oTable table 
	(
	ListPosition int NOT NULL identity(1,1) primary key,
	ListElement varchar(max) NULL
	)

AS
BEGIN

DECLARE @cStr varchar(max)
DECLARE @nPos int

IF RIGHT(RTRIM(@cList), 1) <> @cDelimiter
	BEGIN
	
	SET @cList = @cList + @cDelimiter
	
	END
--ENDIF
 
SET @nPos = CHARINDEX(@cDelimiter, @cList)

WHILE @nPos > 0
	BEGIN
	
	SET @cStr = LEFT(@cList, @nPos - 1)
	
	IF (LEN(RTRIM(@cStr))) > 0
		BEGIN
		
		INSERT INTO @oTable (ListElement) VALUES (@cStr)
		
		END
	--ENDIF

	SET @cList = RIGHT(@cList, LEN(@cList) - LEN(@cStr) - 1)
	SET @nPos = CHARINDEX(@cDelimiter, @cList)
	
	END
--ENDWHILE

RETURN 
END

GO