USE JSCC
GO

SELECT 
	L.LMSID, 
	E.Event,
	'' AS ImportSource,
	T1.TierLabel AS [Owning Organization], 
	T2.TierLabel AS [Tier I Organization (Instance)], 
	LEFT(DATENAME(month, DATEADD(month, MONTH(L.CreationDate), 0) - 1), 3) AS [Create Month],
	YEAR(L.CreationDate) AS [Create Year]
FROM dbo.LMS L 
	JOIN Dropdown.Event E ON E.EventID = L.EventID 
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = L.OriginatingTierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.InstanceID
		AND L.ImportSource IS NULL
		AND L.CreationDate >= '12/01/2012'
ORDER BY YEAR(L.CreationDate), MONTH(L.CreationDate), T1.TierLabel, T2.TierLabel, L.LMSID