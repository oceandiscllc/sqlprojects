DECLARE @tTable TABLE (XMLData XML)

INSERT INTO @tTable
	(XMLData)
SELECT CAST(REPLACE(C1, CHAR(146), CHAR(139)) AS XML) AS XMLData
FROM OPENROWSET (BULK 'C:\www\TaskExport.xml', SINGLE_BLOB) AS T1(C1)

SELECT
	D.
SELECT
  ROWSET.value('(TASKTYPE)[1]', 'varchar(50)') AS ,
  ROWSET.value('(TASKDESCRIPTION)[1]', 'varchar(max)'),
  ROWSET.value('(TASKPREFIX)[1]', 'varchar(50)'),
  ROWSET.value('(TASKNUMBER)[1]', 'varchar(50)'),
  ROWSET.value('(TASKNAME)[1]', 'varchar(250)')
FROM @tTable
CROSS APPLY XMLData.nodes('/ROWSET/ROW') AS T(ROWSET)
ORDER BY ROWSET.value('(TASKPREFIX)[1]', 'varchar(50)'), ROWSET.value('(TASKNUMBER)[1]', 'varchar(50)')

