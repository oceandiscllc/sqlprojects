ALTER TABLE dbo.SODARSData ADD IsForDelete bit NOT NULL CONSTRAINT DF_SODARSData_IsForDelete DEFAULT 0
GO

DECLARE @cContent varchar(max)

DECLARE @nClassificationWeight1 int
DECLARE @nClassificationWeight2 int
DECLARE @nSODARSID int
DECLARE @nSODARSDataID int

DECLARE @oTable table 
	(
	SODARSID int,
	SODARSDataID int,
	Content varchar(max),
	ClassificationWeight int
	)
	
DECLARE oCursor CURSOR FOR
	SELECT 
		SD.SODARSID, 
		SD.SODARSDataID, 
		SD.Content, 
		C.ClassificationWeight
	FROM dbo.SODARSData SD WITH (NOLOCK)
		JOIN dbo.SODARSfield SF on SF.SODARSFieldID = SD.SODARSFieldID 
			AND SF.SODARSsectionid = 17
		LEFT JOIN JLLIS.dbo.Classification C ON C.Classification = SD.Classification
	ORDER BY SD.SODARSID, SD.SODARSDataID, SD.Content


OPEN oCursor
FETCH oCursor into @nSODARSID, @nSODARSDataID, @cContent, @nClassificationWeight1
WHILE @@fetch_status = 0
	BEGIN
		
	IF NOT EXISTS (SELECT 1 FROM @oTable T WHERE T.SODARSID = @nSODARSID)
		BEGIN
		
		INSERT INTO @oTable
			(SODARSID, SODARSDataID, Content, ClassificationWeight)
		VALUES
			(@nSODARSID, @nSODARSDataID, @cContent, ISNULL(@nClassificationWeight1, 0))
			
		END
	ELSE
		BEGIN
		
		SET @nClassificationWeight2 = 
			(
			SELECT
				C.ClassificationWeight 
			FROM dbo.SODARSData SD WITH (NOLOCK) 
				JOIN dbo.SODARSfield SF on SF.SODARSFieldID = SD.SODARSFieldID 
					AND SF.SODARSsectionid = 17
				JOIN JLLIS.dbo.Classification C ON C.Classification = SD.Classification
					AND SD.SODARSID = @nSODARSID 
					AND SD.SODARSDataID <> @nSODARSDataID
			)

		UPDATE @oTable
		SET 
			ClassificationWeight = dbo.GetMaxInteger(ISNULL(@nClassificationWeight1, 0), ISNULL(@nClassificationWeight2, 0)),
			Content = Content + ' ------ ' + @cContent
		WHERE SODARSID = @nSODARSID

		UPDATE dbo.SODARSData
		SET IsForDelete = 1
		WHERE SODARSID = @nSODARSID
			AND SODARSDataID = @nSODARSDataID
					
		END
	--ENDIF

	FETCH oCursor into @nSODARSID, @nSODARSDataID, @cContent, @nClassificationWeight1
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor

UPDATE SD
SET 
	SD.Content = T.Content,
	SD.Classification = C.Classification
FROM dbo.SODARSData SD
	JOIN @oTable T ON T.SODARSID = SD.SODARSID
		AND T.SODARSDataID = SD.SODARSDataID
	LEFT JOIN JLLIS.dbo.Classification C ON C.ClassificationWeight = T.ClassificationWeight
	
DELETE dbo.SODARSData
WHERE IsForDelete = 1
GO

ALTER TABLE dbo.SODARSData DROP CONSTRAINT DF_SODARSData_IsForDelete
GO
ALTER TABLE dbo.SODARSData DROP COLUMN IsForDelete
GO