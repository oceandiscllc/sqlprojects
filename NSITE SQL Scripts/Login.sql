SELECT 
	JU.JLLISUserID,
	TU.TierUserID,
	C.ISOCode3,
	JU.OriginatingTierID,
	T3.TierName AS OriginatingTierName,
	JU.JLLISUserName,
	JU.FirstName,
	JU.LastName,
	JU.Email,
	JU.DayPhone,
	TU.TierID,
	T1.TierName,
	--(
	--SELECT JUSL.SecLev
	--FROM JLLIS.dbo.JLLISUserSecLev JUSL
	--WHERE JUSL.JLLISUserID = JU.JLLISUserID
	--	AND JUSL.TierID = TU.DefaultTierID
	--) AS SecLev,
	--TU.SecLev,
	TU.DefaultTierID,
	T2.TierName AS DefaultTierName,
	TU.Status,
	TU.ActiveDate,
	JU.IsAccountLocked,
	JU.IsPasswordResetRequired,
	TU.Disable_Email
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.Dropdown.Country C ON C.CountryID = JU.CountryID
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TU.TierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = TU.DefaultTierID
	JOIN JLLIS.dbo.Tier T3 ON T3.TierID = JU.OriginatingTierID
		AND TU.TierID = JLLIS.dbo.GetTierUserInstanceID()
		--AND TU.Status = 'Active'
		--AND JU.JLLISUserName LIKE '%navy-manager%'
		--AND JU.FirstName LIKE '%sheryl%'
		--AND JU.LastName LIKE '%gilmore%'
		AND JU.JLLISUserID IN (3399)
		--AND TU.TierUserID IN (111)
		--AND JU.JLLISUserName = 'xenoshogun'
ORDER BY JU.JLLISUserID,JU.LastName, T1.TierName


/*

SELECT *
FROM JLLIS.dbo.JLLISUserSecLev
WHERE JLLISUserID IN (3549)


UPDATE JLLIS.dbo.JLLISUserSecLev 
SET 
	SecLev = 7000
	--TierID = 189
WHERE JLLISUserSecLevID = 2991

INSERT INTO JLLIS.dbo.JLLISUserSecLev (JLLISUserID, TierID, SecLev) VALUES (9, 8, 7000)
DELETE JLLIS.dbo.JLLISUserSecLev WHERE JLLISUserSecLevID IN (788,3284)

UPDATE JLLIS.dbo.TierUser
SET ActiveDate = '01/01/2020'
WHERE JLLISUserID IN (3549)

UPDATE JLLIS.dbo.JLLISUser
SET DayPhone = '727.579.1066'
WHERE JLLISUserID = 3399

SELECT
	JU.JLLISUserID,
	TU.TierUserID,
	JU.OriginatingTierID,
	JU.JLLISUserName,
	JU.Password,
	JU.FirstName,
	JU.LastName,
	TU.TierID,
	T1.TierName,
	TU.SecLev,
	TU.DefaultTierID,
	T2.TierName AS DefaultTierName,
	TU.Status
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
		AND TU.TierUserID = 85
	JOIN JLLIS.dbo.Tier T1 WITH (NOLOCK) ON T1.TierID = TU.TierID
	JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierID = TU.DefaultTierID

DELETE
FROM JLLIS.dbo.TierUser
WHERE JLLISUSerID = 70751
	AND TierUserID = 2467

DELETE
FROM JLLIS.dbo.JLLISUserSecLev
WHERE JLLISUserSecLevID IN (2923)

UPDATE JLLIS.PasswordSecurity.Password
SET CreationDate = DATEADD(yy, 5, CreationDate)
FROM JLLIS.PasswordSecurity.Password
WHERE JLLISUserID = 3549

DELETE 
FROM JLLIS.PasswordSecurity.Password
WHERE JLLISUserID = 4090

UPDATE JLLIS.dbo.TierUser
SET DefaultTierID = 698
WHERE JLLISUserID = 4072

UPDATE JLLIS.dbo.TierUser
SET SecLev = 2000
WHERE JLLISUSERID = 3511

UPDATE JLLIS.dbo.TierUser
SET SecLev = 2000
WHERE JLLISUserID = 3399
	AND TierUserID = 19083

SELECT * 
FROM JLLIS.dbo.TierUser
WHERE TierUserID = 19093
and tierid = 25

DECLARE @cTierName varchar(50)
DECLARE @nJLLISUserID int

SET @cTierName = 'ARMY'
SET @nJLLISUserID = 3399

INSERT INTO JLLIS.dbo.TierUser
	(TierUserID,JLLISUserID,TierID,SecLev,DefaultTierID,Status)
SELECT
	(
	SELECT MAX(TierUserID) + 1
	FROM JLLIS.dbo.TierUser TU WITH (NOLOCK)
		JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = TU.TierID
			AND T.TierName = @cTierName
			AND T.IsInstance = 1
	),
	@nJLLISUserID,
	T.TierID,
	7000,
	T.TierID,
	'Active'
FROM JLLIS.dbo.Tier T WITH (NOLOCK)
WHERE T.TierName = @cTierName
	AND T.IsInstance = 1
	
UPDATE JLLIS.dbo.TierUser
SET Status = 'Active'
WHERE TierUserID = 18810

UPDATE JLLIS.dbo.TierUser
SET TierUserID = 19093
WHERE TierUserID = 19083
	AND JLLISUSERID = 3399
	AND TierID = 25

UPDATE JLLIS.dbo.TierUser
SET SecLev = 2500
WHERE TierUserID = 217
	AND JLLISUserID = 3455




DECLARE @nJLLISUserID INT = 3549

SELECT *
FROM JLLIS.PasswordSecurity.Password
WHERE JLLISUserID IN (@nJLLISUserID)

DELETE PR
FROM JLLIS.PasswordSecurity.PasswordRemnant PR
	JOIN JLLIS.PasswordSecurity.Password P ON P.PasswordID = PR.PasswordID
		AND P.JLLISUserID = @nJLLISUserID

UPDATE JLLIS.PasswordSecurity.Password
SET IsActive = 0
WHERE JLLISUserID = @nJLLISUserID

INSERT INTO JLLIS.PasswordSecurity.Password
	(JLLISUSerID,PasswordHash,PasswordSalt,IsActive)
SELECT
	@nJLLISUserID,
	P.PasswordHash,
	P.PasswordSalt, 
	1
FROM JLLIS.PasswordSecurity.Password P
WHERE P.JLLISUSerID = 3399

SELECT *
FROM JLLIS.PasswordSecurity.Password
WHERE JLLISUserID = @nJLLISUserID

------------------------------------------------
	
UPDATE JLLIS.dbo.JLLISUser
SET IsAccountLocked = 0
WHERE JLLISUserID IN (3895,3969,3549)
*/
