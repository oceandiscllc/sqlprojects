SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ConstraintName
FROM sys.default_constraints C
	JOIN sys.Objects O ON O.Object_ID = C.Parent_Object_ID
		AND O.Type = 'U'
	JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
ORDER BY 1,2,3	

SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ConstraintName
FROM sys.key_constraints C
	JOIN sys.Objects O ON O.Object_ID = C.Parent_Object_ID
		AND O.Type = 'U'
	JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
ORDER BY 1,2,3	

SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS ConstraintName
FROM sys.foreign_keys C
	JOIN sys.Objects O ON O.Object_ID = C.Parent_Object_ID
		AND O.Type = 'U'
	JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
ORDER BY 1,2,3	

SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	COUNT(C.Name) AS IndexCount
FROM sys.indexes C
	JOIN sys.Objects O ON O.Object_ID = C.Object_ID
		AND O.Type = 'U'
	JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
GROUP BY S.Name, O.Name
ORDER BY 1,2

SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	C.Name AS IndexName
FROM sys.indexes C
	JOIN sys.Objects O ON O.Object_ID = C.Object_ID
		AND O.Type = 'U'
	JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
ORDER BY 1,2,3	
