--DELETE D
SELECT *
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY TierID, DropdownMetaDataID, DropdownEntityID ORDER BY TierID, DropdownMetaDataID, DropdownEntityID) AS RowIndex,
		TD.TierDropdownID,
		TD.TierID, 
		TD.DropdownMetaDataID, 
		TD.DropdownEntityID
	FROM Dropdown.TierDropdown TD
	) D
WHERE D.RowIndex > 1
