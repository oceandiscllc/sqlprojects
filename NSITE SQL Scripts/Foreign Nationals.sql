SELECT 
	JU.JLLISUserID,
	JU.JLLISUserName,
	JU.FirstName,
	JU.LastName,
	JU.Email,
	JU.PKIForce,
	C.ISOCode3
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.Dropdown.Country C ON C.CountryID = JU.CountryID
		AND 
			(
			JU.PKIForce = 'O'
				OR C.ISOCode3 <> 'USA'
			)
