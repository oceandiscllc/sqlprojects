USE JLLIS
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Menu')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Menu'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO

IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Menu.MenuItem') AND O.Type IN ('U'))
	DROP TABLE Menu.MenuItem
GO

CREATE TABLE Menu.MenuItem
	(
	MenuItemID int IDENTITY(1,1) NOT NULL,
	MenuItemLabel varchar(250) NOT NULL,
	MenuItemLink varchar(500) NULL,
	MenuItemHTML varchar(500) NULL,
	CanHaveChildren bit NOT NULL,
	CanHaveDuplicates bit NOT NULL,
	IsHTMLConfigurable bit NOT NULL,
	IsForLoggedIn bit NOT NULL,
	IsForLoggedOut bit NOT NULL
	CONSTRAINT PK_MenuItem PRIMARY KEY CLUSTERED 
		(
		MenuItemID ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE Menu.MenuItem ADD CONSTRAINT DF_MenuItem_CanHaveChildren DEFAULT (0) FOR CanHaveChildren
ALTER TABLE Menu.MenuItem ADD CONSTRAINT DF_MenuItem_CanHaveDuplicates DEFAULT (0) FOR CanHaveDuplicates
ALTER TABLE Menu.MenuItem ADD CONSTRAINT DF_MenuItem_IsHTMLConfigurable DEFAULT (0) FOR IsHTMLConfigurable
ALTER TABLE Menu.MenuItem ADD CONSTRAINT DF_MenuItem_IsForLoggedIn DEFAULT (1) FOR IsForLoggedIn
ALTER TABLE Menu.MenuItem ADD CONSTRAINT DF_MenuItem_IsForLoggedOut DEFAULT (0) FOR IsForLoggedOut
GO

INSERT INTO Menu.MenuItem (MenuItemLabel) VALUES ('PRIMARY NODE')
INSERT INTO Menu.MenuItem (MenuItemLabel,CanHaveChildren,CanHaveDuplicates) VALUES ('NODE',1,1)
INSERT INTO Menu.MenuItem (MenuItemLabel,CanHaveDuplicates,IsHTMLConfigurable) VALUES ('USER DEFINED',1,1)
GO

INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('ADD NEW GROUP','index.cfm?disp=UserGroups&Action=AddUpdateUserGroup')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('ANALYSIS','index.cfm?disp=analysis_codes.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('BINDERS','index.cfm?disp=binder.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('CDRs w/ Multiple Files','index.cfm?disp=showMultCDRfiles.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('CONTENT','index.cfm?disp=content.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('CoP SITES','index.cfm?disp=ssite.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('DOCUMENT LIBRARY (CDR)','index.cfm?disp=cdr.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('DROPDOWNS','index.cfm?disp=dropdown.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('EVENT LOG','index.cfm?disp=rep-eventlog.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('EVENTS','index.cfm?disp=../admin/opexmaint.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('FEEDBACK','index.cfm?disp=feedback.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('FILE SYSTEM CHECK','index.cfm?disp=../admin/fileCheck.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('FORM BUILDER','index.cfm?disp=form_builder')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('HOME','index.cfm?menudisp=menu.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('INTERAGENCY SEARCH','../JKMS/main.jsp')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('JLLIS-LITE','lite/')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('LESSON ROLLUP','index.cfm?disp=lessonrollup.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('LESSON VIEWS REPORT','index.cfm?disp=rpt-lmsviews.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('LINKS','index.cfm?disp=links.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('LOGIN','index.cfm?disp=login.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MANAGE LOOK/FEEL','index.cfm?disp=lookFeel.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MEMBERS','index.cfm?disp=prospectors.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MENUS','index.cfm?disp=menu_admin.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('METADATA','index.cfm?disp=../admin/admin_metadata.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('mJLLIS Import','http://socom.nsite.info/ussocom/?disp=mobile_import')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY AARS','index.cfm?disp=aar.cfm&mystuff=1')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY BINDERS','index.cfm?disp=binder.cfm&myBinders=yes')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY DAILY DIGESTS','index.cfm?disp=profile.cfm#dd')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY FILES','index.cfm?disp=files.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY FORMS','index.cfm?disp=display_form')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY LESSONS','index.cfm?disp=lms.cfm&mylessons=yes')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('MY TIER MAINTENANCE','index.cfm?disp=../admin/tiers/tierList.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('PUBLICATIONS','index.cfm?disp=publicationTabs.cfm&activeTab=default')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('QUICK ADD REPORT','index.cfm?disp=quickAddReport.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('REGISTER','index.cfm?disp=register.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('SEARCH GROUPS','index.cfm?disp=UserGroups&Action=SearchUserGroups')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('SEARCH OBSERVATIONS','index.cfm?disp=lms.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('SEND EMAIL','index.cfm?disp=../admin/sendTier2Email.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('SOF ACRONYMS','popups/acryo.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('SWITCH ROLE','index.cfm?disp=changeRole.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('TABS','index.cfm?disp=tabMaintenance.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('TASK MANAGER','index.cfm?disp=bb.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('TIER MAINTENANCE','index.cfm?disp=../admin/tiers/tierList.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('TIER METRICS REPORT','index.cfm?disp=adminmetrics.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('UJTLS','index.cfm?disp=ujtlmaint.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('UNITS','index.cfm?disp=../admin/unitmaint.cfm&doit=display')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('UPLOAD FILES','index.cfm?disp=../admin/uploadimages.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('UPLOAD IMAGES','index.cfm?disp=../admin/dspFileManager.cfm')
INSERT INTO Menu.MenuItem (MenuItemLabel,MenuItemLink) VALUES ('VIEW MY GROUPS','index.cfm?disp=UserGroups&Action=ViewMyUserGroups')
GO

UPDATE Menu.MenuItem
SET IsForLoggedIn = 0
WHERE MenuItemLabel IN ('LOGIN','REGISTER')
GO

UPDATE Menu.MenuItem
SET IsForLoggedOut = 1
WHERE MenuItemLabel IN ('HOME','LOGIN','REGISTER')
GO

IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Menu.TierMenu') AND O.Type IN ('U'))
	DROP TABLE Menu.TierMenu
GO

CREATE TABLE Menu.TierMenu
	(
	TierMenuID int IDENTITY(1,1) NOT NULL,
	TierID int NOT NULL,
	IsLoggedIn bit NOT NULL
	CONSTRAINT PK_TierMenu PRIMARY KEY CLUSTERED 
		(
		TierMenuID ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE Menu.TierMenu ADD CONSTRAINT DF_TierMenu_IsLoggedIn  DEFAULT (1) FOR IsLoggedIn
ALTER TABLE Menu.TierMenu ADD CONSTRAINT DF_TierMenu_TierID  DEFAULT (0) FOR TierID
GO

IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Menu.TierMenuItem') AND O.Type IN ('U'))
	DROP TABLE Menu.TierMenuItem
GO

CREATE TABLE Menu.TierMenuItem
	(
	TierMenuItemID int IDENTITY(1,1) NOT NULL,
	SourceTierMenuItemID int NOT NULL,
	TierMenuID int NOT NULL,
	MenuItemID int NOT NULL,
	ParentTierMenuItemID int NOT NULL,
	DisplayOrder int NOT NULL,
	MenuItemLabel varchar(250) NULL,
	MenuItemHTML varchar(500) NULL,
	SecLev int NOT NULL
	CONSTRAINT PK_TierMenuItem PRIMARY KEY NONCLUSTERED 
		(
		TierMenuItemID ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.object_id = OBJECT_ID('Menu.TierMenuItem') AND I.name = 'IX_TierMenuItem')
	DROP INDEX IX_TierMenuItem ON Menu.TierMenuItem
GO

CREATE CLUSTERED INDEX IX_TierMenuItem ON Menu.TierMenuItem
	(
	TierMenuID ASC,
	ParentTierMenuItemID ASC,
	DisplayOrder ASC,
	MenuItemLabel ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_DisplayOrder DEFAULT (0) FOR DisplayOrder
ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_MenuItemID DEFAULT (0) FOR MenuItemID
ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_ParentTierMenuItemID DEFAULT (0) FOR ParentTierMenuItemID
ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_SecLev DEFAULT (0) FOR SecLev
ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_SourceTierMenuItemID DEFAULT (0) FOR SourceTierMenuItemID
ALTER TABLE Menu.TierMenuItem ADD CONSTRAINT DF_TierMenuItem_TierMenuID DEFAULT (0) FOR TierMenuID
GO

INSERT INTO Menu.TierMenu (TierID,IsLoggedIn) VALUES (0,0)
INSERT INTO Menu.TierMenu (TierID,IsLoggedIn) VALUES (0,1)
GO

CREATE TABLE #oTable (ID int NOT NULL identity (1,1), TierMenuItemID int)

DECLARE @nParentTierMenuItemID int
DECLARE @nTierMenuID int

SET @nTierMenuID = (SELECT TM.TierMenuID FROM Menu.TierMenu TM WITH (NOLOCK) WHERE TM.TierID = 0 AND TM.IsLoggedIn = 0)

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID) SELECT @nTierMenuID,MI.MenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'PRIMARY NODE'

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) JOIN Menu.MenuItem MI WITH (NOLOCK) ON MI.MenuItemID = TMI.MenuItemID AND TMI.TierMenuID = @nTierMenuID AND MI.MenuItemLabel = 'PRIMARY NODE')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'REGISTER'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'LOGIN'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'HOME'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel,MenuItemHTML) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'EMAIL US','onclick="javascript:location.href=''mailto:dan.parker@nsite.info,Christopher.Andrews@socom.mil''"' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'USER DEFINED'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nTierMenuID = (SELECT TM.TierMenuID FROM Menu.TierMenu TM WITH (NOLOCK) WHERE TM.IsLoggedIn = 1 AND TM.TierID = 0)

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID) SELECT @nTierMenuID,MI.MenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'PRIMARY NODE'

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) JOIN Menu.MenuItem MI WITH (NOLOCK) ON MI.MenuItemID = TMI.MenuItemID AND TMI.TierMenuID = @nTierMenuID AND MI.MenuItemLabel = 'PRIMARY NODE')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'HOME'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'MY STUFF' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'SEARCH' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'TOOLS' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'TEAM TOOLS' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'LINKS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'PUBLICATIONS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel,MenuItemHTML) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'MAIL TO ME','onclick="javascript:location.href=''mailto:christian.ready@nsitellc.com''"' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'USER DEFINED'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel,MenuItemHTML) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'SUBMIT RFI','https://call2.army.mil/rfi/' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'USER DEFINED'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'CONTACT US' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'ADMIN' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'MY STUFF')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY DAILY DIGESTS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY LESSONS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY BINDERS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY AARS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY FILES'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY FORMS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'MY GROUPS' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'MY GROUPS')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'VIEW MY GROUPS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'SEARCH GROUPS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'ADD NEW GROUP'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'SEARCH')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'INTERAGENCY SEARCH'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'SEARCH OBSERVATIONS'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'TOOLS')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'DOCUMENT LIBRARY (CDR)'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'BINDERS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'SOF ACRONYMS'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'TEAM TOOLS')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'CoP SITES'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'TASK MANAGER'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'CONTACT US')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel,MenuItemHTML) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'EMAIL US','onclick="javascript:location.href=''mailto:dan.parker@nsitellc.com''"' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'USER DEFINED'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'FEEDBACK'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

SET @nParentTierMenuItemID = (SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.MenuItemLabel = 'ADMIN')

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'CONTENT'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'CDRs w/ Multiple Files'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'EVENT LOG'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'FORM BUILDER'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'FILE SYSTEM CHECK'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'JLLIS-LITE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'LESSON ROLLUP'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MANAGE LOOK/FEEL'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MEMBERS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID,MenuItemLabel) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID,'SITE MANAGEMENT' FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'NODE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'LESSON VIEWS REPORT'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'TIER METRICS REPORT'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'SEND EMAIL'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'SWITCH ROLE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'QUICK ADD REPORT'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'mJLLIS Import'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'ANALYSIS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'TIER MAINTENANCE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'DROPDOWNS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'EVENTS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MENUS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'METADATA'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'MY TIER MAINTENANCE'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'UNITS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'UJTLS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'UPLOAD FILES'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'TABS'
INSERT INTO Menu.TierMenuItem (TierMenuID,MenuItemID,ParentTierMenuItemID) SELECT @nTierMenuID,MI.MenuItemID,@nParentTierMenuItemID FROM Menu.MenuItem MI WITH (NOLOCK) WHERE MI.MenuItemLabel = 'UPLOAD IMAGES'

TRUNCATE TABLE #oTable
INSERT INTO #oTable (TierMenuItemID) SELECT TMI.TierMenuItemID FROM Menu.TierMenuItem TMI WITH (NOLOCK) WHERE TMI.TierMenuID = @nTierMenuID AND TMI.ParentTierMenuItemID = @nParentTierMenuItemID ORDER BY TMI.TierMenuItemID
UPDATE TMI SET TMI.DisplayOrder = T.ID FROM Menu.TierMenuItem TMI JOIN #oTable T ON T.TierMenuItemID = TMI.TierMenuItemID

DROP TABLE #oTable