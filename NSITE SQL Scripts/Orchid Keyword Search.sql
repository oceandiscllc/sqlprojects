USE JLLIS
GO

DECLARE @cDatabaseName varchar(50)
DECLARE @cKeyword varchar(max)
DECLARE @cSQL varchar(max)
DECLARE @nInstanceID int

SET @cKeyword = 'todd'

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable1', 'u')) IS NOT NULL
	DROP TABLE #tTable1

CREATE TABLE #tTable1
	(
	LMSID int,
	InstanceID int,
	Databasename varchar(50)
	)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		UPPER(DB.Name),
		T.TierID
	FROM sys.Databases DB
		JOIN dbo.Tier T ON UPPER(T.TierName) = UPPER(DB.Name)
			AND T.IsInstance = 1
			AND UPPER(DB.Name) IN ('ACGU','ARMY','CCO','CFMCCLL','DISA','DLA','DOS','DTRA','HPRC','JLLIS','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
			AND UPPER(DB.Name) <> 'ORCHID'
	ORDER BY UPPER(DB.Name)
		
OPEN oCursor
FETCH oCursor INTO @cDatabaseName, @nInstanceID
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'INSERT INTO #tTable1 (LMSID,InstanceID,Databasename) SELECT D.LMSID,'
	SET @cSQL += CAST(@nInstanceID as varchar(5)) + ','
	SET @cSQL += '''' + @cDatabaseName + ''' '
	SET @cSQL += 'FROM ' + @cDatabaseName + '.Reporting.GetObservationsByKeyword(''' + @cKeyword + ''', NULL, NULL) D'

	EXEC(@cSQL)

	FETCH oCursor into @cDatabaseName, @nInstanceID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

SELECT COUNT(*) AS ItemCount
FROM #tTable1

DROP TABLE #tTable1
GO