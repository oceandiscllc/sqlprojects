-- ==============================================================================================================================
-- Functions:
--
-- Schemas:
--		Social
--
-- Stored Procedures:
--
-- Tables:
--		Social.EntityVote
--		Social.UserEntityVote
--
-- Views:
--		
-- ==============================================================================================================================

--Begin CMT 10058
--Begin schema validation: Social
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'Social')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Social'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Social

--Begin table Social.EntityVote
DECLARE @cTableName varchar(250)

SET @cTableName = 'Social.EntityVote'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	BEGIN
	
	CREATE TABLE Social.EntityVote
		(
		EntityVoteID int IDENTITY(1,1) NOT NULL,
		EntityTypeCode varchar(50),
		EntityID int,
		TierID int,
		UpVotes int,
		DownVotes int,
		Average float,
		Aggregate int,
		CreationDate datetime,
		UpdateDate datetime
		)
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Aggregate', 0
EXEC Utility.SetDefault @cTableName, 'Average', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'DownVotes', 0
EXEC Utility.SetDefault @cTableName, 'EntityID', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'UpVotes', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Aggregate', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Average', 'float'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'DownVotes', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'UpVotes', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EntityVoteID'
GO
--End table Social.EntityVote

--Begin table Social.UserEntityVote
DECLARE @cTableName varchar(250)

SET @cTableName = 'Social.UserEntityVote'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	BEGIN
	
	CREATE TABLE Social.UserEntityVote
		(
		UserEntityVoteID int IDENTITY(1,1) NOT NULL,
		JLLISUserID int,
		EntityVoteID int,
		Vote int,
		CreationDate datetime,
		UpdateDate datetime
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'EntityVoteID', 0
EXEC Utility.SetDefault @cTableName, 'JLLISUserID', 0
EXEC Utility.SetDefault @cTableName, 'UpdateDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'Vote', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'EntityVoteID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'JLLISUserID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'Vote', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'UserEntityVoteID'
EXEC Utility.SetIndexClustered 'IX_UserEntityVote_EntityVoteID', @cTableName, 'EntityVoteID ASC'
EXEC Utility.SetIndexNonClustered 'IX_UserEntityVote_JLLISUserID', @cTableName, 'JLLISUserID ASC'
GO
--End table Social.UserEntityVote
--End CMT 10058