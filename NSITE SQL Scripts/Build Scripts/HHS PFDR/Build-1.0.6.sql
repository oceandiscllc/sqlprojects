DECLARE @WorkflowID INT

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID
GO

INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Emmanuel.Djokou@ACF.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mike.etzinger@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'suzanne.fialkoff@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Blenda.Perez@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Lynn.Tantardini@samhsa.hhs.gov'))
GO

DECLARE @WorkflowID INT

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
GO

