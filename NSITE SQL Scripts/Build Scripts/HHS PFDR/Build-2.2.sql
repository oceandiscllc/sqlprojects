USE PFDR
GO

--Begin Data
--End Data

--Begin Functions
--Begin function AccessControl.GetContractRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetContractRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.23
-- Description:	A function to manage the buisness rules regarding Contract table record visibility based on PersonID
--
-- Author:			Todd Pires
-- Create Date: 2015.01.19
-- Description:	Added the CanHaveRecall and CanHaveArchive bits
-- =================================================================================================================

CREATE FUNCTION AccessControl.GetContractRecordsByPersonID
(
@PersonID INT,
@Contract AccessControl.Contract READONLY
)

RETURNS @tReturn table 
	(
	ContractID INT PRIMARY KEY NOT NULL,
	CanHaveArchive BIT NOT NULL DEFAULT 0,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0,
	CanHaveRecall BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @tPersonRole TABLE (RoleCode VARCHAR(50), OrganizationID INT)
	
	INSERT INTO @tPersonRole
		(RoleCode,OrganizationID)
	SELECT
		R.RoleCode,
		PR.OrganizationID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID

	IF EXISTS (SELECT 1 FROM @tPersonRole PR WHERE PR.RoleCode = 'SuperAdministrator')
		BEGIN

		INSERT INTO @tReturn
			(ContractID,CanHaveArchive,CanHaveDelete,CanHaveEdit,CanHaveRecall)
		SELECT
			CAC.ContractID,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 1
				ELSE 0
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				ELSE 1
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				ELSE 1
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 1
				ELSE 0
			END

		FROM @Contract CAC
			JOIN dbo.Contract C ON C.ContractID = CAC.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

		END
	ELSE
		BEGIN

		INSERT INTO @tReturn
			(ContractID,CanHaveArchive,CanHaveDelete,CanHaveEdit,CanHaveRecall)
		SELECT
			CAC.ContractID,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved' AND EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID AND WP.WorkflowStepNumber = C.WorkflowStepNumber AND WP.PersonID = @PersonID)
				THEN 1
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')   
				THEN 1
				ELSE 0
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID AND WP.WorkflowStepNumber = C.WorkflowStepNumber AND WP.PersonID = @PersonID)
				THEN 1
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')   
				THEN 1
				ELSE 0
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved' AND EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')
				THEN 1
				ELSE 0
			END

		FROM @Contract CAC
			JOIN dbo.Contract C ON C.ContractID = CAC.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

		UPDATE T
		SET
			T.CanHaveEdit = 1
		FROM @tReturn T
			JOIN dbo.Contract C ON C.ContractID = T.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND WS.WorkflowStatusCode = 'PendingApproval'
				AND EXISTS
					(
					SELECT 1
					FROM dbo.WorkflowPerson WP
					WHERE WP.WorkflowID = C.WorkflowID 
						AND WP.WorkflowStepNumber = C.WorkflowStepNumber
						AND WP.PersonID = @PersonID
					)

		END
	--ENDIF
					
	RETURN

END
GO
--End function AccessControl.GetContractRecordsByPersonID
--End Functions

--Begin Procedures
--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Create Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
--
-- Author:			Todd Pires
-- Create Date: 2015.01.19
-- Description:	Added the CanHaveRecall bit
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		C.ContractDateEnd,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		UR.CanHaveRecall,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.GetContractByEventLogID
EXEC Utility.DropObject 'dbo.GetContractByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.01.20
-- Description:	A stored procedure to get contract data from the dbo.EventLog table based on an EventLogID
-- =======================================================================================================
CREATE PROCEDURE dbo.GetContractByEventLogID
@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @oContract XML = (SELECT EL.EventData FROM dbo.EventLog EL WHERE EL.EventLogID = @EventLogID)

	DECLARE @tContract TABLE
		(
		ContractID int NOT NULL PRIMARY KEY,
		ContractingOfficeName varchar (250) NULL,
		ContractName varchar (250) NULL,
		ContractDescription varchar (max) NULL,
		TargetAwardDate date NULL,
		ContractDateStart date NULL,
		ContractDateEnd date NULL,
		IncumbentContractorName varchar (250) NULL,
		ReferenceIDV varchar (250) NULL,
		TransactionNumber varchar (100) NULL,
		CompetitionTypeID int NOT NULL DEFAULT 0,
		ContractTypeID int NOT NULL DEFAULT 0,
		FiscalYearContractRangeID int NOT NULL DEFAULT 0,
		NAICSID int NOT NULL DEFAULT 0,
		PSCID int NOT NULL DEFAULT 0,
		ContractingOrganizationID int NOT NULL DEFAULT 0,
		FundingOrganizationID int NOT NULL DEFAULT 0,
		OriginatingOrganizationID int NOT NULL DEFAULT 0,
		RequirementTypeID int NOT NULL DEFAULT 0,
		TotalContractRangeID int NOT NULL DEFAULT 0,
		EstimatedSolicitationQuarter int NOT NULL DEFAULT 0,
		WorkflowStatusID int NOT NULL DEFAULT 1,
		WorkflowStepNumber int NOT NULL DEFAULT 1,
		PersonID int NOT NULL DEFAULT 0,
		CreateDateTime datetime
		)

	INSERT INTO @tContract
		(CompetitionTypeID,ContractDateEnd,ContractDateStart,ContractDescription,ContractID,ContractingOfficeName,ContractingOrganizationID,ContractName,ContractTypeID,EstimatedSolicitationQuarter,FiscalYearContractRangeID,FundingOrganizationID,IncumbentContractorName,NAICSID,OriginatingOrganizationID,PSCID,ReferenceIDV,RequirementTypeID,TargetAwardDate,TotalContractRangeID,TransactionNumber,WorkflowStatusID,WorkflowStepNumber,PersonID,CreateDateTime)
	SELECT
		T.Col.value('CompetitionTypeID[1]', 'int'),
		T.Col.value('ContractDateEnd[1]', 'date'),
		T.Col.value('ContractDateStart[1]', 'date'),
		T.Col.value('ContractDescription[1]', 'varchar(max)'),
		T.Col.value('ContractID[1]', 'int'),
		T.Col.value('ContractingOfficeName[1]', 'varchar(250)'),
		T.Col.value('ContractingOrganizationID[1]', 'int'),
		T.Col.value('ContractName[1]', 'varchar(250)'),
		T.Col.value('ContractTypeID[1]', 'int'),
		T.Col.value('EstimatedSolicitationQuarter[1]', 'int'),
		T.Col.value('FiscalYearContractRangeID[1]', 'int'),
		T.Col.value('FundingOrganizationID[1]', 'int'),
		T.Col.value('IncumbentContractorName[1]', 'varchar(250)'),
		T.Col.value('NAICSID[1]', 'int'),
		T.Col.value('OriginatingOrganizationID[1]', 'int'),
		T.Col.value('PSCID[1]', 'int'),
		T.Col.value('ReferenceIDV[1]', 'varchar(250)'),
		T.Col.value('RequirementTypeID[1]', 'int'),
		T.Col.value('TargetAwardDate[1]', 'date'),
		T.Col.value('TotalContractRangeID[1]', 'int'),
		T.Col.value('TransactionNumber[1]', 'varchar(100)'),
		T.Col.value('WorkflowStatusID[1]', 'int'),
		T.Col.value('WorkflowStepNumber[1]', 'int'),
		EL.PersonID,
		EL.CreateDateTime
	FROM @oContract.nodes('//Contract') T(Col), dbo.EventLog EL
	WHERE EL.EventLogID = @EventLogID
	
	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		C.ContractDateEnd,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime(C.CreateDateTime, 'mm/dd/yyyy hh:mm tt') + 
			CASE
				WHEN EXISTS (SELECT 1 FROM dbo.Person P WHERE P.PersonID = C.PersonID AND C.PersonID > 0)
				THEN ' by ' + dbo.GetPersonNameByPersonID(C.PersonID, 'LastFirst')
				ELSE ''
			END AS UpdateDateTimeFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		0 AS CanHaveDelete,
		0 AS CanHaveEdit,
		0 AS CanHaveRecall,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM @tContract C
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByEventLogID

--Begin procedure dbo.GetContractEventLogDataByContractID
EXEC Utility.DropObject 'dbo.GetContractEventLogDataByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.11
-- Description:	A stored procedure to get data from the dbo.EventLog table based on a ContractID
--
-- Author:			Todd Pires
-- Modify Date: 2015.01.20
-- Description:	Added the EventLogID
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractEventLogDataByContractID
@ContractID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EL.EventLogID,
		dbo.FormatDateTime(EL.CreateDateTime, 'mm/dd/yyyy hh:mm tt') AS CreateDateTimeFormatted,
		dbo.GetPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
	
		CASE
			WHEN EL.EventCode IN ('Create', 'Update')
			THEN EL.EventCode + 'd contract'
			WHEN EL.EventCode = 'AssignWorkflow'
			THEN 'Set workflow to step 1'
			WHEN EL.EventCode = 'IncrementWorkflow'
			THEN
				CASE
					WHEN (SELECT WS.WorkflowStatusCode FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusID = N.C.value('(WorkflowStatusID)[1]', 'INT')) = 'Approved'
					THEN 'Workflow complete, contract approved'
					ELSE 'Set workflow to step ' + N.C.value('(WorkflowStepNumber)[1]', 'VARCHAR')
				END
			ELSE EL.EventCode
		END AS ActionTaken,
	
		EL.Comments
	FROM dbo.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Contract') AS N(C)
	WHERE EL.EntityTypeCode = 'Contract'
		AND EL.EntityID = @ContractID
		AND EL.EventCode <> 'AssignWorkflowPersons'
	ORDER BY EL.EventLogID

END
GO
--End procedure dbo.GetContractEventLogDataByContractID

--Begin procedure dbo.GetContractPlacesOfPerformanceByEventLogID
EXEC Utility.DropObject 'dbo.GetContractPlacesOfPerformanceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.01.20
-- Description:	A stored procedure to get contract place of performance data from the dbo.EventLog table based on an EventLogID
-- ============================================================================================================================
CREATE PROCEDURE dbo.GetContractPlacesOfPerformanceByEventLogID
@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @oContract XML = (SELECT EL.EventData FROM dbo.EventLog EL WHERE EL.EventLogID = @EventLogID)
	DECLARE @oContractPlacesOfPerformance XML = @oContract.query('/Contract/ContractPlacesOfPerformance') 

	DECLARE @tContractPlaceOfPerformance TABLE
		(
		ContractPlaceOfPerformanceID int NOT NULL PRIMARY KEY,
		ContractID int NOT NULL DEFAULT 0,
		PlaceOfPerformanceCity varchar(250) NULL,
		PlaceOfPerformanceStateID int NOT NULL DEFAULT 0,
		IsPrimary bit NOT NULL DEFAULT 0
		)

	INSERT INTO @tContractPlaceOfPerformance
		(ContractID,ContractPlaceOfPerformanceID,IsPrimary,PlaceOfPerformanceCity,PlaceOfPerformanceStateID)
	SELECT
		T.Col.value('ContractID[1]', 'int'),
		T.Col.value('ContractPlaceOfPerformanceID[1]', 'int'),
		T.Col.value('IsPrimary[1]', 'bit'),
		T.Col.value('PlaceOfPerformanceCity[1]', 'varchar(250)'),
		T.Col.value('PlaceOfPerformanceStateID[1]', 'int')
	FROM @oContractPlacesOfPerformance.nodes('//ContractPlaceOfPerformance') T(Col)
	
	SELECT
		CPOP.ContractPlaceOfPerformanceID, 
		CPOP.PlaceOfPerformanceCity, 
		CPOP.IsPrimary,
		S.StateCode
	FROM @tContractPlaceOfPerformance CPOP
		JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
	ORDER BY CPOP.IsPrimary DESC, CPOP.PlaceOfPerformanceCity, S.StateCode

END
GO
--End procedure dbo.GetContractPlacesOfPerformanceByEventLogID

--Begin procedure dbo.GetContractPointsOfContactByEventLogID
EXEC Utility.DropObject 'dbo.GetContractPointsOfContactByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.01.20
-- Description:	A stored procedure to get contract point of contact data from the dbo.EventLog table based on an EventLogID
-- ========================================================================================================================
CREATE PROCEDURE dbo.GetContractPointsOfContactByEventLogID
@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @oContract XML = (SELECT EL.EventData FROM dbo.EventLog EL WHERE EL.EventLogID = @EventLogID)
	DECLARE @oContractPointsOfContact XML = @oContract.query('/Contract/ContractPointsOfContact') 

	DECLARE @tContractPointOfContact TABLE
		(
		ContractPointOfContactID int NOT NULL PRIMARY KEY,
		ContractID int NOT NULL DEFAULT 0,
		PointOfContactAgency varchar(250) NULL,
		PointOfContactEmail varchar(320) NULL,
		PointOfContactName varchar(250) NULL,
		PointOfContactPhone varchar(50) NULL,
		PointOfContactTitle varchar(50) NULL,
		IsPrimary bit NOT NULL DEFAULT 0
		)
	
	INSERT INTO @tContractPointOfContact
		(ContractID,ContractPointOfContactID,IsPrimary,PointOfContactAgency,PointOfContactEmail,PointOfContactName,PointOfContactPhone,PointOfContactTitle)
	SELECT
		T.Col.value('ContractID[1]', 'int') AS ContractID,
		T.Col.value('ContractPointOfContactID[1]', 'int'),
		T.Col.value('IsPrimary[1]', 'bit'),
		T.Col.value('PointOfContactAgency[1]', 'varchar(250)'),
		T.Col.value('PointOfContactEmail[1]', 'varchar(320)'),
		T.Col.value('PointOfContactName[1]', 'varchar(250)'),
		T.Col.value('PointOfContactPhone[1]', 'varchar(50)'),
		T.Col.value('PointOfContactTitle[1]', 'varchar(50)')
	FROM @oContractPointsOfContact.nodes('//ContractPointOfContact') T(Col)

	SELECT
		CPOC.ContractPointOfContactID,
		CPOC.PointOfContactAgency, 
		CPOC.PointOfContactEmail, 
		CPOC.PointOfContactName, 
		CPOC.PointOfContactPhone, 
		CPOC.PointOfContactTitle, 
		CPOC.IsPrimary
	FROM @tContractPointOfContact CPOC
	ORDER BY CPOC.IsPrimary DESC, CPOC.PointOfContactName, CPOC.ContractPointOfContactID

END
GO
--End procedure dbo.GetContractPointsOfContactByEventLogID
--End Procedures
