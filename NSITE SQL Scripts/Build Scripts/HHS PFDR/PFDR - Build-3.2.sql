USE PFDR
GO

--Begin Tables
--End Tables

--Begin Functions
--Begin function dbo.GetCurrentFiscalYear
EXEC Utility.DropObject 'dbo.GetCurrentFiscalYear'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2017.01.30
-- Description:	A function to return the current fiscal year from the current date
-- ===============================================================================
CREATE FUNCTION dbo.GetCurrentFiscalYear
(
)

RETURNS INT

AS
BEGIN

	DECLARE @nFiscalYear INT = YEAR(getDate())

	IF MONTH(getDate()) > 9
		SET @nFiscalYear = @nFiscalYear + 1
	--ENDIF

	RETURN @nFiscalYear

END
GO
--End function dbo.GetCurrentFiscalYear

--Begin function dbo.GetDashboardWorkflowRawData
EXEC Utility.DropObject 'dbo.GetDashboardWorkflowRawData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.06.18
-- Description:	A function to get the raw data for the dashboard workflow statistics
-- =================================================================================

CREATE FUNCTION dbo.GetDashboardWorkflowRawData
(
@FiscalYear INT
)

RETURNS @tReturn table 
	(
	ContractID INT PRIMARY KEY NOT NULL,
	AgeInDays INT NOT NULL DEFAULT 0,
	PercentComplete NUMERIC(18,2) NOT NULL DEFAULT 0
	) 

AS
BEGIN

	INSERT INTO @tReturn
		(ContractID, AgeInDays, PercentComplete)
	SELECT 
		EL2.EntityID,
		DATEDIFF(d, EL2.CreateDateTime, getDate()),
		ISNULL(CAST(CAST(C.WorkflowStepNumber - 1 AS NUMERIC(18,2)) / CAST((SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS NUMERIC(18,2)) AS NUMERIC(18,2)) * 100, 0)
	FROM dbo.EventLog EL2
		JOIN 
			(
			SELECT
				MAX(EL1.EventLogID) AS EventLogID
			FROM dbo.EventLog EL1
				JOIN dbo.Contract C ON C.ContractID = EL1.EntityID
					AND C.TargetAwardDate BETWEEN '10/01/' + CAST(@FiscalYear - 1 AS CHAR(4)) AND '09/30/' + CAST(@FiscalYear AS CHAR(4))
					AND EL1.EntityTypeCode = 'Contract'
				JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
					AND WS.WorkflowStatusCode <> 'Approved'
			GROUP BY EL1.EntityID
			) D ON D.EventLogID = EL2.EventLogID
		JOIN dbo.Contract C ON C.ContractID = EL2.EntityID
					
	RETURN

END
GO
--End function dbo.GetDashboardWorkflowRawData
--End Functions

--Begin Procedures

--Begin procedure dbo.GetDashboardContractRangeData
EXEC Utility.DropObject 'dbo.GetDashboardContractRangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardContractRangeData

@ContractRangeTypeCode VARCHAR(50),
@WorkflowStatusCodeList VARCHAR(MAX),
@FiscalYear INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFiscalYear INT = @FiscalYear
	
	IF @nFiscalYear = 0
		SET @nFiscalYear = dbo.GetCurrentFiscalYear()
	--ENDIF

	IF @ContractRangeTypeCode = 'FiscalYear'
		BEGIN
		
		SELECT
			CR.ContractRangeID AS ItemCode,
			CR.ContractRangeName AS ItemName,
			COUNT(C.ContractID) AS ItemCount
		FROM dbo.Contract C
			JOIN Dropdown.ContractRange CR ON CR.ContractRangeID = C.FiscalYearContractRangeID
				AND CR.ContractRangeID > 0
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND C.TargetAwardDate BETWEEN '10/01/' + CAST(@nFiscalYear - 1 AS CHAR(4)) AND '09/30/' + CAST(@nFiscalYear AS CHAR(4))
				AND EXISTS
					(
					SELECT 1
					FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
					WHERE LTT.ListItem = WS.WorkflowStatusCode
					)
		GROUP BY CR.ContractRangeID, CR.ContractRangeName
		ORDER BY CR.ContractRangeName

		END
	ELSE IF @ContractRangeTypeCode = 'Total'
		BEGIN
		
		SELECT
			CR.ContractRangeID AS ItemCode,
			CR.ContractRangeName AS ItemName,
			COUNT(C.ContractID) AS ItemCount
		FROM dbo.Contract C
			JOIN Dropdown.ContractRange CR ON CR.ContractRangeID = C.TotalContractRangeID
				AND CR.ContractRangeID > 0
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND C.TargetAwardDate BETWEEN '10/01/' + CAST(@nFiscalYear - 1 AS CHAR(4)) AND '09/30/' + CAST(@nFiscalYear AS CHAR(4))
				AND EXISTS
					(
					SELECT 1
					FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
					WHERE LTT.ListItem = WS.WorkflowStatusCode
					)
		GROUP BY CR.ContractRangeID, CR.ContractRangeName
		ORDER BY CR.ContractRangeName

		END
	--ENDIF

END
GO
--End procedure dbo.GetDashboardContractRangeData

--Begin procedure dbo.GetDashboardContractsByOperatingDivisionData
EXEC Utility.DropObject 'dbo.GetDashboardContractsByOperatingDivisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardContractsByOperatingDivisionData

@WorkflowStatusCodeList VARCHAR(MAX),
@FiscalYear INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFiscalYear INT = @FiscalYear
	
	IF @nFiscalYear = 0
		SET @nFiscalYear = dbo.GetCurrentFiscalYear()
	--ENDIF
		
	SELECT
		O.OrganizationID AS ItemCode,
		O.OrganizationName AS ItemName,
		COUNT(C.ContractID) AS ItemCount
	FROM dbo.Contract C
		JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.Organization O ON O.OrganizationID = OH.TopLevelOrganizationID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			AND C.TargetAwardDate BETWEEN '10/01/' + CAST(@nFiscalYear - 1 AS CHAR(4)) AND '09/30/' + CAST(@nFiscalYear AS CHAR(4))
			AND EXISTS
				(
				SELECT 1
				FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
				WHERE LTT.ListItem = WS.WorkflowStatusCode
				)
	GROUP BY O.OrganizationID, O.OrganizationName
	ORDER BY O.OrganizationName

END
GO
--End procedure dbo.GetDashboardContractsByOperatingDivisionData
	
--Begin procedure dbo.GetDashboardWorkflowData
EXEC Utility.DropObject 'dbo.GetDashboardWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardWorkflowData
@EntityTypeCode VARCHAR(50),
@FiscalYear INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nFiscalYear INT = @FiscalYear
	DECLARE @tTable TABLE (ContractID INT NOT NULL PRIMARY KEY, AgeInDays INT, PercentComplete NUMERIC(18,2))
	
	IF @nFiscalYear = 0
		SET @nFiscalYear = dbo.GetCurrentFiscalYear()
	--ENDIF

	INSERT INTO @tTable
		(ContractID, AgeInDays, PercentComplete)
	SELECT 
		DWRD.ContractID,
		DWRD.AgeInDays,
		DWRD.PercentComplete
	FROM dbo.GetDashboardWorkflowRawData(@nFiscalYear) DWRD

	IF @EntityTypeCode = 'WorkflowAgeing'
		BEGIN
		
		SELECT
			1 AS DisplayOrder,
			'LTE07' AS ItemCode,
			'<= 7' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 0 AND 7
	
		UNION
	
		SELECT 
			2 AS DisplayOrder, 
			'LTE14' AS ItemCode,
			'<= 14' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 8 AND 14
	
		UNION
	
		SELECT 
			3 AS DisplayOrder, 
			'LTE30' AS ItemCode,
			'<= 30' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 15 AND 30
	
		UNION
	
		SELECT 
			4 AS DisplayOrder, 
			'LTE60' AS ItemCode,
			'<= 60' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 31 AND 60
	
		UNION
	
		SELECT 
			5 AS DisplayOrder, 
			'LTE90' AS ItemCode,
			'<= 90' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 61 AND 90
	
		UNION
	
		SELECT 
			6 AS DisplayOrder, 
			'GT90' AS ItemCode,
			'> 90' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays > 90
	
		ORDER BY DisplayOrder

		END
	ELSE IF @EntityTypeCode = 'WorkflowPercentComplete'	
		BEGIN
		
		SELECT 
			1 AS DisplayOrder, 
			'LTE25' AS ItemCode,
			'<= 25%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 0 AND 25
	
		UNION
	
		SELECT 
			2 AS DisplayOrder, 
			'LTE50' AS ItemCode,
			'<= 50%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 26 AND 50
	
		UNION
	
		SELECT 
			3 AS DisplayOrder, 
			'LTE75' AS ItemCode,
			'<= 75%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 51 AND 75
	
		UNION
	
		SELECT 
			4 AS DisplayOrder, 
			'GT75' AS ItemCode,
			'> 75%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete > 75
	
		ORDER BY DisplayOrder

		END
	--ENDIF

END
GO
--End procedure dbo.GetDashboardWorkflowData

--Begin procedure dropdown.GetFiscalYears
EXEC Utility.DropObject 'dbo.GetFiscalYears'
EXEC Utility.DropObject 'dropdown.GetFiscalYears'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.10
-- Description:	A stored procedure to get a distinct list of fiscal years from the dbo.Contract table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetFiscalYears

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
	
		CASE
			WHEN MONTH(C.TargetAwardDate) > 9
			THEN YEAR(C.TargetAwardDate) + 1
			ELSE YEAR(C.TargetAwardDate)
		END AS Year

	FROM dbo.Contract C
	WHERE C.TargetAwardDate IS NOT NULL
	ORDER BY 1 DESC
	
END
GO
--End procedure dropdown.GetFiscalYears
--End Procedures
