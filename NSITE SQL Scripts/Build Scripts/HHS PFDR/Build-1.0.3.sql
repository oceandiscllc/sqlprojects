USE PFDR
GO

INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kaminskiS@od.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'joseph.marshall@nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'caol@od.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'glynis.fisher@nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'colet@od.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phillip.osborne@nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'veenu.varma@nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jonessi@mail.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'hollidayg@odepsml.od.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'carolyn.keeseman@nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'darlene.walls@nih.gov'))
GO

DECLARE @WorkflowID INT

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'NIH')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
GO
