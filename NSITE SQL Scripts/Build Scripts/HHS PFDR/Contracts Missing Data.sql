SELECT
	'There are ' + CAST(D.ItemCount AS VARCHAR(10)) + ' records with no ' + D.DataElementName
FROM
	(
	SELECT 
		'ContractName' AS DataElementName,
		COUNT(C.ContractID) AS ItemCount
	FROM dbo.Contract C	
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND LEN(RTRIM(LTRIM(C.ContractName))) = 0

	UNION

	SELECT 
		'ContractingOfficeName',
		COUNT(C.ContractID)
	FROM dbo.Contract C 
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND LEN(RTRIM(LTRIM(C.ContractingOfficeName))) = 0

	UNION

	SELECT 
		'ContractDescription',
		COUNT(C.ContractID)
	FROM dbo.Contract C 
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND LEN(RTRIM(LTRIM(C.ContractDescription))) = 0

	UNION

	SELECT 
		'ContractType' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.ContractTypeID = 0

	UNION

	SELECT 
		'RequirementType' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.RequirementTypeID = 0

	UNION

	SELECT 
		'CompetitionType' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.CompetitionTypeID = 0

	UNION

	SELECT 
		'FundingOrganization' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.FundingOrganizationID = 0

	UNION

	SELECT 
		'FiscalYearContractRange' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.FiscalYearContractRangeID = 0

	UNION

	SELECT 
		'TotalContractRange' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.TotalContractRangeID = 0

	UNION

	SELECT 
		'EstimatedSolicitationQuarter' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.EstimatedSolicitationQuarter = 0

	UNION

	SELECT 
		'TargetAwardDate' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.TargetAwardDate IS NULL

	UNION

	SELECT 
		'ContractDateStart' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.ContractDateStart IS NULL

	UNION

	SELECT 
		'ContractDateEnd' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND C.ContractDateEnd IS NULL

	UNION

	SELECT 
		'PointOfContact' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.ContractPointOfContact CPOC
			WHERE CPOC.ContractID = C.ContractID
			)

	UNION

	SELECT 
		'PlaceOfPerformance' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.ContractPlaceOfPerformance CPOP
			WHERE CPOP.ContractID = C.ContractID
			)

	UNION

	SELECT 
		'NAICS / PSC' AS DataElementName,
		COUNT(C.ContractID)
	FROM dbo.Contract C
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
		AND 
			(C.NAICSID = 0
				OR C.PSCID = 0
			)
	) D
WHERE D.ItemCount > 0
ORDER BY D.DataElementName

	SELECT 
		'Total',
		COUNT(C.ContractID)
	FROM dbo.Contract C 
	WHERE C.WorkflowID = 10
		AND C.WorkflowStepNumber = 1
