DECLARE @WorkflowID INT

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASH')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASL')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA')
DELETE FROM dbo.WorkflowPerson WHERE WorkflowID = @WorkflowID

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
GO

INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'angela.johnson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.randolph@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'karen.robinson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'neil.kaufman@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'osmani.banos-diaz@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'georgine.delacruz@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'constance.tobias@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Asha.Abraham@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'barbara.miller@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tri.thai@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Asha.Abraham@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'barbara.miller@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tri.thai@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tri.thai@fda.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.trejo2@hrsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.trejo2@hrsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sandra.jones@hrsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'hanock.john@hrsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASH'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lavonnia.persaud@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASH'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sharonda.green@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASH'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michelle.grifka@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASL'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Arlene.Franklin@HHS.GOV'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASL'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Rose.Duckett@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASL'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Miles.Daniels@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'carol.brown@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'iliana.peters@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kurt.temple@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Christina.Heide@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Robinsue.Frohboese@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Steve.Novy@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brian.sissom@ees.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'frank.brindisi@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kim.devoto@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jeff.davis@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.blecker@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Emmanuel.Djokou@ACF.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mike.etzinger@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'suzanne.fialkoff@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Blenda.Perez@samhsa.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'Lynn.Tantardini@samhsa.hhs.gov'))
GO

DECLARE @WorkflowID INT

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'DAB')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'FDA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'HRSA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASH')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OASL')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OCR')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'OMHA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0

SET @WorkflowID = (SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'SAMHSA')
EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
GO
