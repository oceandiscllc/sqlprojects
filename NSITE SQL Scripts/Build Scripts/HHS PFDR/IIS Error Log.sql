SELECT 
	COUNT(IISErrorLogID) AS ErrorFrequency,
  URL,
	URLQueryString,
	StatusCode,
	SubStatusCode
FROM [Todd].[dbo].[IISErrorLog]
GROUP BY 
  URL,
	URLQueryString,
	StatusCode,
	SubStatusCode
ORDER BY 
	COUNT(IISErrorLogID) DESC,
  URL,
	URLQueryString,
	StatusCode,
	SubStatusCode
