USE PFDR
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Contract'
EXEC utility.AddColumn @TableName, 'BulkDataImportID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'BulkDataImportID', 'INT', 0
GO

print 'Added column BulkDataImportID to table dbo.Contract'

IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'BulkData')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX)
	SET @cSQL = 'CREATE SCHEMA BulkData'
	
	EXEC (@cSQL)
	
	print 'Added schema BulkData to database PFDR'

	END
--ENDIF

DECLARE @TableName VARCHAR(250) = 'BulkData.BulkDataImport'
EXEC utility.DropObject @TableName

CREATE TABLE BulkData.BulkDataImport
	(
	BulkDataImportID INT IDENTITY(1,1) NOT NULL,
	ContractingOfficeName VARCHAR(250),
	ContractName VARCHAR(250),
	ContractDescription VARCHAR(MAX),
	TargetAwardDate DATE,
	ContractDateStart DATE,
	ContractDateEnd DATE,
	IncumbentContractorName VARCHAR(250),
	ReferenceIDV VARCHAR(250),
	TransactionNumber VARCHAR(100),
	CompetitionType VARCHAR(250),
	CompetitionTypeID INT,
	ContractType VARCHAR(250),
	ContractTypeID INT,
	FiscalYearContractRange VARCHAR(250),
	FiscalYearContractRangeID INT,
	NAICSCode VARCHAR(250),
	NAICSID INT,
	PSCCode VARCHAR(250),
	PSCID INT,
	ContractingOrganization VARCHAR(250),
	ContractingOrganizationID INT,
	FundingOrganization VARCHAR(250),
	FundingOrganizationID INT,
	OriginatingOrganization VARCHAR(250),
	OriginatingOrganizationID INT,
	RequirementType VARCHAR(250),
	RequirementTypeID INT,
	TotalContractRange VARCHAR(250),
	TotalContractRangeID INT,
	EstimatedSolicitationQuarter VARCHAR(5),	
	PlaceOfPerformanceCity VARCHAR(250),
	PlaceOfPerformanceStateCode VARCHAR(10),	
	PlaceOfPerformanceStateID INT,	
	PointOfContactName VARCHAR(250),
	PointOfContactEmail VARCHAR(320),
	PointOfContactPhone VARCHAR(50),
	PointOfContactAgency VARCHAR(250),
	PointOfContactTitle VARCHAR(50),
	)

EXEC utility.SetDefaultConstraint @TableName, 'CompetitionTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContractingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContractTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FiscalYearContractRangeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PSCID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlaceOfPerformanceStateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequirementTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TotalContractRangeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'BulkDataImportID'
GO

print 'Created table BulkData.BulkDataImport'

SET IDENTITY_INSERT BulkData.BulkDataImport ON 
GO

INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (1, N'SAMHSA', N'Behavioral Health Services Information System (BHSIS)', N'The purpose of BHSIS is to provide current data on substance abuse and mental health treatment facilities and the people they serve, to issue reports on the data collections, and to produce an on-line treatment locator of these treatment facilities. In cooperation with state substance abuse and mental health agencies and individual facilities, five complexly inter-related components combine and integrate census survey data and state administrative data to provide an accurate picture of the state of substance abuse and mental health treatment in the United States, including each state and territory/jurisdiction.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Synectics Management', NULL, N'283-16-0490', N'Small Business Set Aside - Total', 0, N'Definitive Contract', 0, N'>= $10M and < $20M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'> $100M', 0, N'2', NULL, NULL, 0, N'Blenda Perez', N'blenda.perez@samhsa.hhs.gov', N'(240) 276-1505', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (2, N'SAMHSA', N'National Survey on Drug Use and Health (NSDUH)', N'The purpose of the NSDUH is to provide current data on substance use and mental health prevalence for the U.S. civilian, non-institutionalized population (aged 12 and older) and each state, and to issue reports on survey results. The survey sample will support annual estimates of prevalence for the nation, the 50 states and the District of Columbia. The contractor will be required to: (1) provide sufficient numbers of interviewers and interviewer supervisory staff to screen approximately 150,000 dwelling units and conduct 67,500 interviews per survey year, using computer-assisted interviewing technology; (2) develop training procedures/materials and conduct training classes in order to maintain a sufficient, qualified field staff and satisfy survey fieldwork goals; (3) use statistical expertise to (a) employ a 50-state sample design, with an independent, multi-stage area sample for each of the 50 states and the District of Columbia, and (b) employ small area estimation techniques in order to develop model-based state estimates; and (4) perform technical writing, analysis, and methodological studies to complete and support the contract tasking.

The NSDUH contract will also include an option to conduct a separate nationally representative (U.S. civilian, non-institutionalized populations aged 13 or older) data collection of approximately 13,500 interviews in a survey year that will support estimates of prevalence of specific mental disorders and its correlates, as well as establishing a platform for further studies of populations of individuals with specific mental disorders.', CAST(N'2016-10-31' AS Date), NULL, NULL, N'Research Triangle Institute (RTI)', NULL, N'283-16-0491', N'Full and Open', 0, N'Definitive Contract', 0, N'>= $5M and < $10M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'> $100M', 0, N'2', NULL, NULL, 0, N'Cassandra Ellis', N'cassandra.ellis@samhsa.hhs.gov', N'(240) 276-1503', N'SAMHSA', N'Contracting Officer')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (3, N'SAMHSA', N'Social Inclusion and Awards', N'The purpose of this contract is to educate the public on behavioral health issues and to encourage people with serious mental illness and other behavioral health conditions to seek treatment and services when needed.  In so doing, this effort will develop and implement a strategy that recognizes: a) TV and film depictions that are accurate, respectful and recovery-based, b) community leaders who have conducted model public awareness events on the topic of recovery and c) consumer/peer leaders who have conducted exemplary work in promoting behavioral health. This contract supports the annual Voice Awards program honoring consumer and peer leaders and television and film professionals who promote behavioral health education and awareness. It also supports the Recovery Month event award program acknowledging the effort, dedication, and hard work of the Recovery Month event organizers in local communities.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Vanguard Communications', NULL, N'280-16-0475', N'Small Business Set Aside - Total', 0, N'Definitive Contract', 0, N'>= $500K and < $1M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $2M and < $5M', 0, N'2', NULL, NULL, 0, N'Johnny Mack', N'johnny.mack@samsha.hhs.gov', N'(240) 276-1522', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (4, N'SAMHSA', N'Homeless and Housing Resource Network (HHRN)', N'The Homeless and Housing Resource Network (HHRN) contract is designed to ensure the success of CMHS and CSAT�s homeless grant programs by providing a broad array of technical assistance activities.  The procurement is expected to support over 100 grantees, specifically targeted at the homeless population.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Advocates for Human Potential (AHP)', NULL, N'280-16-0476', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $5M and < $10M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $20M and < $50M', 0, N'2', NULL, NULL, 0, N'Sheryl Sanders', N'sheryl.sanders@samhsa.hhs.gov', N'(240) 276-1506', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (5, N'SAMHSA', N'Now is the Time Technical Assistance', N'Based on the President�s �Now is the Time� initiative, the Substance Abuse and Mental Health Services Administration (SAMHSA), Center for Mental Health Services (CMHS), is continuing to support two Cooperative Agreement Programs: Healthy Transitions (HT): Improving Lifelong Trajectories for Youth and Young Adults with, or at Risk for, Serious Mental Health Conditions and Now is the Time State Education Agency Cooperative Agreements (Project AWARE-SEA).  The purpose of this contract is to provide technical assistance and training to grantees in these grant programs and to assist in wide scale dissemination of best practices and lessons learned. This technical assistance will focus on evidence informed practices, the development of cross-system collaboration, the development of infrastructure and policy, promoting mental, emotional, and behavioral health, creating safe and violence free schools; connecting families, schools, and communities, and the creation of sustainable financing strategies.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'CARS', NULL, N'280-16-0477', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Sheryl Sanders', N'sheryl.sanders@samhsa.hhs.gov', N'(240) 276-1506', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (6, N'SAMHSA', N'Technical Assistance Coalitions and TTI�. Technology Transfer Initiative', N'The purpose of this contract is to serve as the major vehicle that CMHS has solely designed to assist State Mental Health Authorities (SMHAs) in planning for, and expanding, community mental health services, and in meeting the challenges of system changes required as a result of health care reform. This contract will make available, on a continuous basis, a full array of timely, state of the art program, training activities customized to the unique needs of each state as well as provide opportunities for states to pursue relatively modest initiatives that will be supported by peer-to-peer technical assistance (TA) and ongoing guidance to produce significant change in their behavioral health systems. ', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'NASMHPD', NULL, N'280-16-0478', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $1M and < $2M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Johnny Mack', N'johnny.mack@samsha.hhs.gov', N'(240) 276-1522', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (7, N'SAMHSA', N'Behavioral Health and Justice Transformation Center', N'The purpose of this contract is to provide technical assistance to public service agencies and SAMHSA grantees delivering community based treatment and recovery support services to adults with serious mental illness and/or substance use disorders.  This technical assistance will promote the use of evidence based practices and comprehensive services that enhance recovery and safety, are trauma-informed and strength-based, are cost-effective and coordinated among community agencies, and that maximize the involvement of consumers and their families in the planning, evaluation and delivery of services.  ', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Policy Research Associates (PRA)', NULL, N'280-16-0479', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Khaled Gohar', N'khaled.gohar@samhsa.hhs.gov', N'(240) 276-1514', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (8, N'SAMHSA', N'Evaluation of Programs to Provide Services to Persons who are Homeless with Mental and or Substance Use', N'This task order will evaluate SAMHSA�s new �Cooperative Agreements to Benefit Homeless Individuals for States and Communities (CABHI-States and Communities)� program to enhance and/or expand the infrastructure and treatment service systems of states and communities.  This includes domestic public and private nonprofit entities; local governments; federally-recognized American Indian/Alaska Native tribes and tribal organizations; urban Indian organizations; public or private universities and colleges and community- and faith-based organizations.  Goals include increasing capacity to provide accessible, effective, comprehensive, coordinated, integrated, and evidence-based treatment services; permanent supportive housing; peer supports, and other critical services. In addition, this task order will include the triennial evaluation of the PATH program required in its legislation.  The evaluation findings will guide SAMHSA in monitoring performance and implementing future programs, and add to the evidence base for providing services to homeless persons with serious mental and/or co-occurring substance abuse disorders and improve program outcomes.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Research Triangle Institute (RTI)', NULL, N'280-16-0480', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Linda Tillery', N'linda.tillery@samsha.hhs.gov', N'(240) 276-1513', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (9, N'SAMHSA', N'Bringing Recovery Supports to Scale Technical Assistance Center Strategy (BRSS TACS)', N'This contract supports efforts of the Recovery Support Strategic Initiative (RSSI).  The RSSI aims to promote partnerships with people in recovery from mental illness and substance use disorders and their family members to guide the behavioral health system and promote individual, program and system-level approaches that foster health and resilience.  The RSSI also focuses on efforts to increase housing to support recovery; reduce barriers to employment, education and other life goals; support peer workforce in recovery and behavioral health; and help people in recovery secure necessary social supports in their chosen communities. This contract aims to expand recovery support efforts including and not limited to peer workforce development that address gaps that exist in having peers as behavioral health providers; provide  targeted technical assistance to address emerging recovery support issues that may cluster in a specific area; engaging both start-up and existing peer-run and recovery community organizations in developing organizational and programming capacity; provide technical assistance for leadership development and transition; address challenges and/or barriers for financing recovery, peer enrollment or other activities to expand the support and implementation of the Affordable Care Act by peer-run and recovery providers and organizations.  The contract will provide policy/data analysis, training, technical assistance, and needed information tailored to the perspectives of States, counties, behavioral health systems officials and providers, including consumer/peer providers, family members, and other stakeholders in recovery-oriented services and systems. Training and technical assistance shall serve as a primary source to provide information that is tailored to assist states, counties and other health systems officials; peer-run and recovery community organizations including SAMHSA grantees, peer specialists, recovery coaches and other peer providers; and others to promote the adoption of recovery supports.  ', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Center for Social Innovation (C4SI)', NULL, N'280-16-0481', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $10M and < $20M', 0, N'2', NULL, NULL, 0, N'Shana Freno', N'shana.freno@samhsa.hhs.gov', N'(240) 276-1519', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (10, N'SAMHSA', N'Mental Health Block Grant 10% Set Aside Evaluation', N'The purpose of this contract is to evaluate the fidelity and outcome of the implementation of evidence-based programs that addresses the needs of individuals with early serious mental illness through the 5% Mental Health Block Grant set aside funds.  ', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), NULL, NULL, N'280-16-0482', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>  $150K and < $500K', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'New Requirement', 0, N'>= $1M and < $2M', 0, N'2', NULL, NULL, 0, N'Linda Tillery', N'linda.tillery@samsha.hhs.gov', N'(240) 276-1513', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (11, N'SAMHSA', N'Addressing Opioid Misuse and Overdose', N'The purpose of this task order is to continue building on previous workforce competency efforts for professionals in the pain management and addiction specialty field and top develop and implement multifaceted initiatives and strategies to address opioid misuse and overdose.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'JBS International', NULL, N'270-16-0484', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $1M and < $2M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Sophia Janus', N'sophia.janus@samhsa.hhs.gov', N'(240) 276-1235', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (12, N'SAMHSA', N'Technical Assistance & Training for Women and Families Impacted by Substance Abuse and Mental Health Disorders', N'This task order provides technical assistance and training to strengthen the behavioral health workforce''s capacity to address the treatment and recovery issues specific and important for working with women and families. The main tasks involve delivery of a webinar series, leadership training, support to the NASADAD Women''s'' Services Network, and updating a SAMHSA webpage on women and family issues.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Advocates for Human Potential', NULL, N'270-16-0485', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $500K and < $1M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $2M and < $5M', 0, N'2', NULL, NULL, 0, N'Carla Carter (Morris)', N'carla.morris@samhsa.hhs.gov', N'(240) 276-1520', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (13, N'SAMHSA', N'OTP Quality', N'The purpose of this task order is to implement and maintain a Technical Assistance (TA) program to (1) aid communities and States in developing high quality capacity for opioid addiction treatment services and other medication assisted treatment services, (2) develop and deliver TA in forms of continuing education and professional development for addictions professionals working in OTPs to assure appropriate use of all forms of medication assisted treatment, and (3) develop and deliver technical assistance to support OTPs'' development and implementation outcomes oriented quality improvement activities.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'DB Consulting', NULL, N'270-16-0486', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $1M and < $2M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $5M and < $10M', 0, N'2', NULL, NULL, 0, N'Tracy Davidson', N'tracy.davidson@samhsa.hhs.gov', N'(240) 276-1509', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (14, N'SAMHSA', N'SAMHSA Internship Program', N'The SAMHSA internship program/student program is designed to promote entry into public health careers by women, persons with disabilities, and members of diverse groups where those groups are under-represented in the industry. The internship would offer internship opportunities during the fall, spring, and summers sessions to undergraduate and graduate students primarily from Historically Black Colleges and Universities, Hispanic Servicing Institutions, Tribal Colleges and Universities, Asian American and Pacific Islanders, and disability communities.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'DB Consulting', NULL, N'283-16-0488', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $500K and < $1M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $2M and < $5M', 0, N'2', NULL, NULL, 0, N'Susan Dawson', N'susan.dawson@samhsa.hhs.gov', N'(240) 276-1521', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (15, N'SAMHSA', N'Center for Financing Reform and Innovation (CFRI)', N'The Center for Behavioral Health Financing Reform and Innovation (CFRI) provides analysis and technical assistance to address changes in the organization and financing of behavioral health care and to guide federal officials; states, territories, and tribes; providers; grantees; health systems; and private payers on the most effective and efficient use of available resources to meet the prevention, treatment, and recovery support needs of the American public. In this iteration of the contract, CFRI will significantly strengthen its focus on TA and consulting efforts.  This will include TA to grantees as well as the field at large on financing-related issues.  Analysis will still be maintained as a component of the contract. Through an optional task, CFRI also produces biennial estimates of national expenditures on mental and substance use disorder services.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), N'Truven Health Analysis', NULL, N'283-16-0489', N'Fair Opportunity (for orders under existing contracts)', 0, N'Delivery Order', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $10M and < $20M', 0, N'2', NULL, NULL, 0, N'Khaled Gohar', N'khaled.gohar@samhsa.hhs.gov', N'(240) 276-1514', N'SAMHSA', N'Contract Specialist')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (16, N'SAMHSA', N'Public Engagement and Communication Platform (PEP)', N'The purpose of the PEP contract is to provide the public and the Federal Government with one-stop, quick access to mental health, substance abuse prevention and treatment information, materials, and services; and serve as a single interaction point for the public for communication products, services, and messages developed by SAMHSA, through a large-scale information dissemination program.', CAST(N'2016-10-31' AS Date), NULL, NULL, N'IQ Solutions', NULL, N'283-17-0492 ', N'Small Business Set Aside - Total', 0, N'Definitive Contract', 0, N'>= $5M and < $10M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'Recompete', 0, N'>= $20M and < $50M', 0, N'3', NULL, NULL, 0, N'Craig Sager', N'craig.sager@samhsa.hhs.gov', N'(240) 276-1512', N'SAMHSA', N'Contracting Officer')
GO
INSERT [BulkData].[BulkDataImport] ([BulkDataImportID], [ContractingOfficeName], [ContractName], [ContractDescription], [TargetAwardDate], [ContractDateStart], [ContractDateEnd], [IncumbentContractorName], [ReferenceIDV], [TransactionNumber], [CompetitionType], [CompetitionTypeID], [ContractType], [ContractTypeID], [FiscalYearContractRange], [FiscalYearContractRangeID], [NAICSCode], [NAICSID], [PSCCode], [PSCID], [ContractingOrganization], [ContractingOrganizationID], [FundingOrganization], [FundingOrganizationID], [OriginatingOrganization], [OriginatingOrganizationID], [RequirementType], [RequirementTypeID], [TotalContractRange], [TotalContractRangeID], [EstimatedSolicitationQuarter], [PlaceOfPerformanceCity], [PlaceOfPerformanceStateCode], [PlaceOfPerformanceStateID], [PointOfContactName], [PointOfContactEmail], [PointOfContactPhone], [PointOfContactAgency], [PointOfContactTitle]) VALUES (17, N'SAMHSA', N'Native Connections', N'The purpose of this contract is to provide training and technical assistance (TTA) to Native Connections (NC) grantees.  Native Connections is a five-year grant program that helps American Indian and Alaska Native communities identify and address the behavioral health needs of Native youth.  Specifically, the program supports grantees in:  Reducing suicidal behavioral and substance use and misuse among Native youth up to age 24; Easing the impacts of substance use, mental illness, and trauma in tribal communities; supporting youth as they transition into adulthood.', CAST(N'2016-09-08' AS Date), CAST(N'2016-09-08' AS Date), CAST(N'2017-09-07' AS Date), NULL, NULL, N'280-16-0483', N'8A Competed', 0, N'Definitive Contract', 0, N'>= $2M and < $5M', 0, NULL, 0, NULL, 0, N'SAMHSA', 0, N'SAMHSA', 0, NULL, 0, N'New Requirement', 0, N'>= $10M and < $20M', 0, N'2', NULL, NULL, 0, N'Carla Carter (Morris)', N'carla.morris@samhsa.hhs.gov', N'(240) 276-1520', N'SAMHSA', N'Contract Specialist')
GO

print 'Completed Insert into table BulkData.BulkDataImport'

SET IDENTITY_INSERT BulkData.BulkDataImport OFF
GO

UPDATE dbo.Contract SET BulkDataImportID = 0
GO

print 'Initialized the BulkDataImportID column in table dbo.Contract'

UPDATE BDI
SET 
	BDI.CompetitionTypeID = 0,
	BDI.FiscalYearContractRangeID = 0,
	BDI.TotalContractRangeID = 0,
	BDI.ContractTypeID = 0,
	BDI.NAICSID = 0,
	BDI.ContractingOrganizationID = 0,
	BDI.FundingOrganizationID = 0,
	BDI.OriginatingOrganization = 'CDC',
	BDI.OriginatingOrganizationID = 0,
	BDI.PSCID = 0,
	BDI.PlaceOfPerformanceStateID = 0,
	BDI.RequirementTypeID = 0
FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET BDI.EstimatedSolicitationQuarter = 0
FROM BulkData.BulkDataImport BDI
WHERE BDI.EstimatedSolicitationQuarter IS NULL
GO

UPDATE BDI
SET BDI.CompetitionTypeID = T.CompetitionTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.CompetitionType T ON T.CompetitionTypeName = BDI.CompetitionType
GO

UPDATE BDI
SET BDI.FiscalYearContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.FiscalYearContractRange
GO

UPDATE BDI
SET BDI.TotalContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.TotalContractRange
GO

UPDATE BDI
SET BDI.ContractTypeID = T.ContractTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractType T ON T.ContractTypeName = BDI.ContractType
GO

UPDATE BDI
SET BDI.NAICSID = T.NAICSID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.NAICS T ON T.NAICSCode = BDI.NAICSCode
GO

UPDATE BDI
SET BDI.ContractingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.ContractingOrganization
GO

UPDATE BDI
SET BDI.FundingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.FundingOrganization
GO

UPDATE BDI
SET BDI.OriginatingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.OriginatingOrganization
GO

UPDATE BDI
SET BDI.PSCID = T.PSCID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.PSC T ON T.PSCCode = BDI.PSCCode
GO

UPDATE BDI
SET BDI.RequirementTypeID = T.RequirementTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.RequirementType T ON T.RequirementTypeName = BDI.RequirementType
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceCity = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.PlaceOfPerformanceCity = 'TBD'
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceStateCode = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.PlaceOfPerformanceStateCode = 'TBD'
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceStateID = S.StateID 
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.State S ON S.StateCode = BDI.PlaceOfPerformanceStateCode
GO

print 'Initialized table BulkData.BulkDataImport'

BEGIN TRANSACTION

INSERT INTO dbo.Contract
	(BulkDataImportID, ContractingOfficeName, ContractName, ContractDescription, TargetAwardDate, ContractDateStart, ContractDateEnd, IncumbentContractorName, ReferenceIDV, TransactionNumber, CompetitionTypeID, ContractTypeID, FiscalYearContractRangeID, NAICSID, PSCID, ContractingOrganizationID, FundingOrganizationID, OriginatingOrganizationID, RequirementTypeID, TotalContractRangeID, EstimatedSolicitationQuarter)
SELECT
	BDI.BulkDataImportID, 
	BDI.ContractingOfficeName, 
	BDI.ContractName, 
	BDI.ContractDescription, 
	BDI.TargetAwardDate, 
	BDI.ContractDateStart, 
	BDI.ContractDateEnd, 
	BDI.IncumbentContractorName, 
	BDI.ReferenceIDV, 
	BDI.TransactionNumber, 
	BDI.CompetitionTypeID, 
	BDI.ContractTypeID, 
	BDI.FiscalYearContractRangeID, 
	BDI.NAICSID, 
	BDI.PSCID, 
	BDI.ContractingOrganizationID, 
	BDI.FundingOrganizationID, 
	BDI.OriginatingOrganizationID, 
	BDI.RequirementTypeID, 
	BDI.TotalContractRangeID, 
	BDI.EstimatedSolicitationQuarter
FROM BulkData.BulkDataImport BDI

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.Contract'
	ROLLBACK TRANSACTION
	RETURN
	
	END
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.Contract'
--ENDIF

INSERT INTO dbo.ContractPlaceOfPerformance
	(ContractID, PlaceOfPerformanceCity, PlaceOfPerformanceStateID, IsPrimary)
SELECT
	C.ContractID,
	BDI.PlaceOfPerformanceCity,
	BDI.PlaceOfPerformanceStateID,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PlaceOfPerformanceCity IS NOT NULL
				OR BDI.PlaceOfPerformanceStateID > 0
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPlaceOfPerformance'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPlaceOfPerformance'
--ENDIF

INSERT INTO dbo.ContractPointOfContact
	(ContractID, PointOfContactAgency, PointOfContactEmail, PointOfContactName, PointOfContactPhone, PointOfContactTitle, IsPrimary)
SELECT
	C.ContractID,
	BDI.PointOfContactAgency,
	BDI.PointOfContactEmail,
	BDI.PointOfContactName,
	BDI.PointOfContactPhone,
	BDI.PointOfContactTitle,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PointOfContactAgency IS NOT NULL
				OR BDI.PointOfContactEmail IS NOT NULL
				OR BDI.PointOfContactName IS NOT NULL
				OR BDI.PointOfContactPhone IS NOT NULL
				OR BDI.PointOfContactTitle IS NOT NULL
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPointOfContact'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPointOfContact'
--ENDIF

DECLARE @nContractID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT C.ContractID
	FROM dbo.Contract C
	WHERE C.BulkDataImportID > 0
	ORDER BY C.ContractID

OPEN oCursor
FETCH oCursor INTO @nContractID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogContractEntry 'Bulk Imported', @nContractID, 'Create'
	EXEC dbo.AssignWorkflowToContract 'Bulk Imported', @nContractID, 0

	FETCH oCursor INTO @nContractID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

IF @@ERROR <> 0
 BEGIN

	print 'Error creating event log entries for dbo.Contract bulk data import'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Created event log entries for dbo.Contract bulk data import'
--ENDIF

COMMIT TRANSACTION
GO