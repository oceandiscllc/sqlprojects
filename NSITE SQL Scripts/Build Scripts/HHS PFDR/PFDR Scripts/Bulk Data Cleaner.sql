USE PFDR
GO

-- ********************************************************************** Remember to Reset This ********************************************************************** --
--Begin set originating organization
/*
--Use this if all records are from a single Organization
UPDATE BDI
SET BDI.OriginatingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = 'IHS'  -- <============ Set this to the proper OpDiv
GO
*/

--Use this if records are from multiple Organizations
UPDATE BDI
SET BDI.OriginatingOrganization = 
	CASE
		WHEN BDI.ContractingOfficeName = 'Albuquerque Area Indian Health Service' THEN 'Albuquerque Area'
		WHEN BDI.ContractingOfficeName = 'Bemidji' THEN 'Bemidji Area'
		WHEN BDI.ContractingOfficeName = 'Bemidji Area Office' THEN 'Bemidji Area'
		WHEN BDI.ContractingOfficeName = 'Bemidji Area Office - Ind.HS' THEN 'Bemidji Area'
		WHEN BDI.ContractingOfficeName = 'IHS Bemidji Indian Health Service' THEN 'Bemidji Area'
		WHEN BDI.ContractingOfficeName = 'BFSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'Billings Area' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'CRSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'FBSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'FPSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'NCSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'WRSU' THEN 'Billings Area'
		WHEN BDI.ContractingOfficeName = 'CAO' THEN 'California Area'
		WHEN BDI.ContractingOfficeName = 'Great Plains Area' THEN 'Great Plains'
		WHEN BDI.ContractingOfficeName = 'Division of Engineering Services' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'Division of Health Professions Support' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'Division of Planning, Evaluation and Research' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'Division of Program Statistics' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'IHS Division of Acquisition and Planning' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'Indian Health Service DAP' THEN 'IHSHQ'
		WHEN BDI.ContractingOfficeName = 'Indian Health Service, Nashville Area Office' THEN 'Nashville Area'
		WHEN BDI.ContractingOfficeName = 'Navajo Area Office' THEN 'Navajo Area'
		WHEN BDI.ContractingOfficeName = 'Claremore Indian Hospital' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Clinton' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Haskell Health Clinic' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'National Supply Service Center' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Oklahoma City Area Office - IT&T' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Administration' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Behavioral Health' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Dental' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Indian Clinic Laboratory' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Medical Imaging' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Optometry' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Outpatient Clinic' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee Pharmacy' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Pawnee PRC' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'Watonga' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'White Cloud Health Center' THEN 'Oklahoma City Area'
		WHEN BDI.ContractingOfficeName = 'PA Office of IHS Area Director' THEN 'Phoenix Area'
		WHEN BDI.ContractingOfficeName = 'PAO Division of Finance' THEN 'Phoenix Area'
		WHEN BDI.ContractingOfficeName = 'Phoenix Area IHS' THEN 'Phoenix Area'
		WHEN BDI.ContractingOfficeName = 'Fort Hall Service Unit' THEN 'Portland Area'
		WHEN BDI.ContractingOfficeName = 'Western Oregon Service Unit' THEN 'Portland Area'
		WHEN BDI.ContractingOfficeName = 'DES' THEN 'Seatlle Area'
		WHEN BDI.ContractingOfficeName = 'DES- Seattle' THEN 'Seatlle Area'
		WHEN BDI.ContractingOfficeName = 'Wellpinit Service Unit' THEN 'Seatlle Area'
	END

FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET BDI.OriginatingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.OriginatingOrganization
GO
--End set originating organization
-- ********************************************************************** Remember to Reset This ********************************************************************** --

--Begin raw data repair
DELETE BDI
FROM BulkData.BulkDataImport BDI
WHERE BDI.ContractingOfficeName IS NULL
GO

UPDATE BDI
SET
	BDI.CompetitionType = LTRIM(RTRIM(BDI.CompetitionType)),
	BDI.ContractDescription = LTRIM(RTRIM(BDI.ContractDescription)),
	BDI.ContractingOfficeName = LTRIM(RTRIM(BDI.ContractingOfficeName)),
	BDI.ContractingOrganization = LTRIM(RTRIM(BDI.ContractingOrganization)),
	BDI.ContractName = LTRIM(RTRIM(BDI.ContractName)),
	BDI.ContractType = LTRIM(RTRIM(BDI.ContractType)),
	BDI.ContractVehicle = LTRIM(RTRIM(BDI.ContractVehicle)),
	BDI.EstimatedSolicitationQuarter = LTRIM(RTRIM(BDI.EstimatedSolicitationQuarter)),
	BDI.FiscalYearContractRange = LTRIM(RTRIM(BDI.FiscalYearContractRange)),
	BDI.FundingOrganization = LTRIM(RTRIM(BDI.FundingOrganization)),
	BDI.IncumbentContractorName = LTRIM(RTRIM(BDI.IncumbentContractorName)),
	BDI.NAICSCode = LTRIM(RTRIM(BDI.NAICSCode)),
	BDI.OriginatingOrganization = LTRIM(RTRIM(BDI.OriginatingOrganization)),
	BDI.PlaceOfPerformanceCity = LTRIM(RTRIM(BDI.PlaceOfPerformanceCity)),
	BDI.PlaceOfPerformanceStateCode = LTRIM(RTRIM(BDI.PlaceOfPerformanceStateCode)),
	BDI.PointOfContactAgency = LTRIM(RTRIM(BDI.PointOfContactAgency)),
	BDI.PointOfContactEmail = LTRIM(RTRIM(BDI.PointOfContactEmail)),
	BDI.PointOfContactName = LTRIM(RTRIM(BDI.PointOfContactName)),
	BDI.PointOfContactPhone = LTRIM(RTRIM(BDI.PointOfContactPhone)),
	BDI.PointOfContactTitle = LTRIM(RTRIM(BDI.PointOfContactTitle)),
	BDI.PSCCode = LTRIM(RTRIM(BDI.PSCCode)),
	BDI.ReferenceIDV = LTRIM(RTRIM(BDI.ReferenceIDV)),
	BDI.RequirementType = LTRIM(RTRIM(BDI.RequirementType)),
	BDI.TotalContractRange = LTRIM(RTRIM(BDI.TotalContractRange)),
	BDI.TransactionNumber = LTRIM(RTRIM(BDI.TransactionNumber))
FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET
	BDI.CompetitionType = CASE WHEN LEN(BDI.CompetitionType) = 0 THEN NULL ELSE BDI.CompetitionType END,
	BDI.ContractDescription = CASE WHEN LEN(BDI.ContractDescription) = 0 THEN NULL ELSE BDI.ContractDescription END,
	BDI.ContractingOfficeName = CASE WHEN LEN(BDI.ContractingOfficeName) = 0 THEN NULL ELSE BDI.ContractingOfficeName END,
	BDI.ContractingOrganization = CASE WHEN LEN(BDI.ContractingOrganization) = 0 THEN NULL ELSE BDI.ContractingOrganization END,
	BDI.ContractName = CASE WHEN LEN(BDI.ContractName) = 0 THEN NULL ELSE BDI.ContractName END,
	BDI.ContractType = CASE WHEN LEN(BDI.ContractType) = 0 THEN NULL ELSE BDI.ContractType END,
	BDI.ContractVehicle = CASE WHEN LEN(BDI.ContractVehicle) = 0 THEN NULL ELSE BDI.ContractVehicle END,
	BDI.EstimatedSolicitationQuarter = CASE WHEN LEN(BDI.EstimatedSolicitationQuarter) = 0 THEN NULL ELSE BDI.EstimatedSolicitationQuarter END,
	BDI.FiscalYearContractRange = CASE WHEN LEN(BDI.FiscalYearContractRange) = 0 THEN NULL ELSE BDI.FiscalYearContractRange END,
	BDI.FundingOrganization = CASE WHEN LEN(BDI.FundingOrganization) = 0 THEN NULL ELSE BDI.FundingOrganization END,
	BDI.IncumbentContractorName = CASE WHEN LEN(BDI.IncumbentContractorName) = 0 THEN NULL ELSE BDI.IncumbentContractorName END,
	BDI.NAICSCode = CASE WHEN LEN(BDI.NAICSCode) = 0 THEN NULL ELSE BDI.NAICSCode END,
	BDI.OriginatingOrganization = CASE WHEN LEN(BDI.OriginatingOrganization) = 0 THEN NULL ELSE BDI.OriginatingOrganization END,
	BDI.PlaceOfPerformanceCity = CASE WHEN LEN(BDI.PlaceOfPerformanceCity) = 0 THEN NULL ELSE BDI.PlaceOfPerformanceCity END,
	BDI.PlaceOfPerformanceStateCode = CASE WHEN LEN(BDI.PlaceOfPerformanceStateCode) = 0 THEN NULL ELSE BDI.PlaceOfPerformanceStateCode END,
	BDI.PointOfContactAgency = CASE WHEN LEN(BDI.PointOfContactAgency) = 0 THEN NULL ELSE BDI.PointOfContactAgency END,
	BDI.PointOfContactEmail = CASE WHEN LEN(BDI.PointOfContactEmail) = 0 THEN NULL ELSE BDI.PointOfContactEmail END,
	BDI.PointOfContactName = CASE WHEN LEN(BDI.PointOfContactName) = 0 THEN NULL ELSE BDI.PointOfContactName END,
	BDI.PointOfContactPhone = CASE WHEN LEN(BDI.PointOfContactPhone) = 0 THEN NULL ELSE BDI.PointOfContactPhone END,
	BDI.PointOfContactTitle = CASE WHEN LEN(BDI.PointOfContactTitle) = 0 THEN NULL ELSE BDI.PointOfContactTitle END,
	BDI.PSCCode = CASE WHEN LEN(BDI.PSCCode) = 0 THEN NULL ELSE BDI.PSCCode END,
	BDI.ReferenceIDV = CASE WHEN LEN(BDI.ReferenceIDV) = 0 THEN NULL ELSE BDI.ReferenceIDV END,
	BDI.RequirementType = CASE WHEN LEN(BDI.RequirementType) = 0 THEN NULL ELSE BDI.RequirementType END,
	BDI.TotalContractRange = CASE WHEN LEN(BDI.TotalContractRange) = 0 THEN NULL ELSE BDI.TotalContractRange END,
	BDI.TransactionNumber = CASE WHEN LEN(BDI.TransactionNumber) = 0 THEN NULL ELSE BDI.TransactionNumber END
FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET BDI.CompetitionType = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = 'New Requirement'

UPDATE BDI
SET BDI.CompetitionType = 'Not Competed - Small'
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = 'Not Competed – Small'

UPDATE BDI
SET BDI.CompetitionType = 'Not Competed - Other Than Small'
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = 'Not Competed – Other Than Small'

UPDATE BDI
SET BDI.EstimatedSolicitationQuarter = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.EstimatedSolicitationQuarter = 'TBD'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '> $25K and < $150K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '>=$25K and <$150K'
GO

UPDATE BDI
SET BDI.ContractType = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.ContractType IN ('8(A) Sole Source', 'Small Business Set Aside - Partial', 'Small Business Set Aside - Total')
GO

UPDATE BDI
SET BDI.ContractVehicle = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.ContractVehicle = 'TBD'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '>= $150K and < $500K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange IN ('>  $150K and < $500K', '> $150K and < $500K')
GO

UPDATE BDI
SET BDI.TotalContractRange = '> $25K and < $150K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '>=$25K and <$150K'
GO

UPDATE BDI
SET BDI.TotalContractRange = '>= $150K and < $500K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange IN ('>  $150K and < $500K', '> $150K and < $500K')
GO

UPDATE BDI
SET BDI.TotalContractRange = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '1 Year'
GO
--End raw data repair

--Begin ID field initialization
UPDATE BDI
SET 
	BDI.CompetitionTypeID = 0,
	BDI.ContractingOrganizationID = 0,
	BDI.ContractTypeID = 0,
	BDI.FiscalYearContractRangeID = 0,
	BDI.FundingOrganizationID = 0,
	BDI.NAICSID = 0,
	BDI.PSCID = 0,
	BDI.RequirementTypeID = 0,
	BDI.TotalContractRangeID = 0
FROM BulkData.BulkDataImport BDI
GO
--End ID field initialization

--Begin ID field population
UPDATE BDI
SET BDI.CompetitionTypeID = T.CompetitionTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.CompetitionType T ON T.CompetitionTypeName = BDI.CompetitionType
GO

UPDATE BDI
SET BDI.FiscalYearContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.FiscalYearContractRange
GO

UPDATE BDI
SET BDI.TotalContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.TotalContractRange
GO

UPDATE BDI
SET BDI.ContractTypeID = T.ContractTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractType T ON T.ContractTypeName = BDI.ContractType
GO

UPDATE BDI
SET BDI.ContractVehicleID = T.ContractVehicleID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractVehicle T ON T.ContractVehicleName = BDI.ContractVehicle
GO

UPDATE BDI
SET BDI.NAICSCode = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.NAICSCode = 'NAICSCode'
GO

UPDATE BDI
SET BDI.NAICSID = T.NAICSID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.NAICS T ON T.NAICSCode = BDI.NAICSCode
GO

UPDATE BDI
SET BDI.ContractingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.ContractingOrganization
GO

UPDATE BDI
SET BDI.FundingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.FundingOrganization
GO

UPDATE BDI
SET BDI.PSCID = T.PSCID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.PSC T ON T.PSCCode = BDI.PSCCode
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceStateID = T.StateID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.State T ON T.StateCode = BDI.PlaceOfPerformanceStateCode
GO

UPDATE BDI
SET BDI.RequirementTypeID = T.RequirementTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.RequirementType T ON T.RequirementTypeName = BDI.RequirementType
GO
--End ID field population

SELECT
  BDI.BulkDataImportID,
  BDI.CompetitionType,
  BDI.CompetitionTypeID,
  BDI.ContractingOrganization,
  BDI.ContractingOrganizationID,
  BDI.ContractType,
  BDI.ContractTypeID,
  BDI.ContractVehicle,
  BDI.ContractVehicleID,
  BDI.FiscalYearContractRange,
  BDI.FiscalYearContractRangeID,
  BDI.FundingOrganization,
  BDI.FundingOrganizationID,
  BDI.NAICSCode,
  BDI.NAICSID,
  BDI.OriginatingOrganization,
  BDI.OriginatingOrganizationID,
  BDI.PlaceOfPerformanceStateCode,
  BDI.PlaceOfPerformanceStateID,
  BDI.RequirementType,
  BDI.RequirementTypeID,
  BDI.TotalContractRange,
  BDI.TotalContractRangeID
FROM BulkData.BulkDataImport BDI
WHERE 
  (BDI.CompetitionType IS NOT NULL AND CompetitionTypeID = 0) OR
  (BDI.ContractingOrganization IS NOT NULL AND ContractingOrganizationID = 0) OR
  (BDI.ContractType IS NOT NULL AND ContractTypeID = 0) OR
  (BDI.ContractVehicle IS NOT NULL AND ContractVehicleID = 0) OR
  (BDI.FiscalYearContractRange IS NOT NULL AND FiscalYearContractRangeID = 0) OR
  (BDI.FundingOrganization IS NOT NULL AND FundingOrganizationID = 0) OR
  (BDI.NAICSCode IS NOT NULL AND NAICSID = 0) OR
  (BDI.OriginatingOrganization IS NOT NULL AND OriginatingOrganizationID = 0) OR
  (BDI.PlaceOfPerformanceStateCode IS NOT NULL AND PlaceOfPerformanceStateID = 0) OR
  (BDI.RequirementType IS NOT NULL AND RequirementTypeID = 0) OR
  (BDI.TotalContractRange IS NOT NULL AND TotalContractRangeID = 0)
GO

SELECT *
FROM BulkData.BulkDataImport BDI
GO