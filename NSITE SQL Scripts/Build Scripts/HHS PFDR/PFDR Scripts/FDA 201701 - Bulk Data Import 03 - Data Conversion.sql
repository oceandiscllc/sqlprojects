USE PFDR_DHHS
GO

UPDATE dbo.Contract SET BulkDataImportID = 0
GO

UPDATE Dropdown.CompetitionType
SET CompetitionTypeName = 'Not Competed - Small'
WHERE CompetitionTypeName = 'Not Competed � Small'
GO

UPDATE Dropdown.CompetitionType
SET CompetitionTypeName = 'Not Competed - Other Than Small'
WHERE CompetitionTypeName = 'Not Competed � Other Than Small'
GO

UPDATE Dropdown.ContractRange
SET IsActive = 0
WHERE ContractRangeID = 12
GO

print 'Initialized the BulkDataImportID column in table dbo.Contract'

UPDATE BDI
SET 
	BDI.CompetitionTypeID = 0,
	BDI.FiscalYearContractRangeID = 0,
	BDI.TotalContractRangeID = 0,
	BDI.ContractTypeID = 0,
	BDI.ContractVehicleID = 0,
	BDI.NAICSID = 0,
	BDI.ContractingOrganizationID = 0,
	BDI.FundingOrganizationID = 0,
	BDI.OriginatingOrganizationID = 0,
	BDI.PSCID = 0,
	BDI.PlaceOfPerformanceStateID = 0,
	BDI.RequirementTypeID = 0
FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET BDI.EstimatedSolicitationQuarter = 0
FROM BulkData.BulkDataImport BDI
WHERE BDI.EstimatedSolicitationQuarter IS NULL
GO

UPDATE BDI
SET BDI.CompetitionTypeID = T.CompetitionTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.CompetitionType T ON T.CompetitionTypeName = BDI.CompetitionType
GO

UPDATE BDI
SET BDI.FiscalYearContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T.ContractRangeName, '>', ''), '=', ''), '<', ''), '$', ''), ' ', '') = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(BDI.FiscalYearContractRange, '>', ''), '=', ''), '<', ''), '$', ''), ' ', ''), 'amd', 'and')
		AND T.IsActive = 1
GO

UPDATE BDI
SET BDI.TotalContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T.ContractRangeName, '>', ''), '=', ''), '<', ''), '$', ''), ' ', '') = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(BDI.TotalContractRange, '>', ''), '=', ''), '<', ''), '$', ''), ' ', ''), 'amd', 'and')
		AND T.IsActive = 1
GO

UPDATE BDI
SET BDI.ContractTypeID = T.ContractTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractType T ON T.ContractTypeName = BDI.ContractType
GO

UPDATE BDI
SET BDI.ContractVehicleID = T.ContractVehicleID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractVehicle T ON T.ContractVehicleName = BDI.ContractVehicle
GO

UPDATE BDI
SET BDI.NAICSID = T.NAICSID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.NAICS T ON T.NAICSCode = BDI.NAICSCode
GO

UPDATE BDI
SET BDI.ContractingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.ContractingOrganization
GO

UPDATE BDI
SET BDI.FundingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.FundingOrganization
GO

UPDATE BDI
SET BDI.OriginatingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.OriginatingOrganization
GO

UPDATE BDI
SET BDI.PSCID = T.PSCID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.PSC T ON T.PSCCode = BDI.PSCCode
GO

UPDATE BDI
SET BDI.RequirementTypeID = T.RequirementTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.RequirementType T ON T.RequirementTypeName = BDI.RequirementType
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceCity = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.PlaceOfPerformanceCity = 'TBD'
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceStateCode = NULL
FROM BulkData.BulkDataImport BDI
WHERE BDI.PlaceOfPerformanceStateCode = 'TBD'
GO

UPDATE BDI
SET BDI.PlaceOfPerformanceStateID = S.StateID 
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.State S ON S.StateCode = BDI.PlaceOfPerformanceStateCode
GO

print 'Initialized table BulkData.BulkDataImport'

BEGIN TRANSACTION

INSERT INTO dbo.Contract
	(BulkDataImportID, ContractingOfficeName, ContractName, ContractDescription, TargetAwardDate, ContractDateStart, ContractDateEnd, IncumbentContractorName, ReferenceIDV, TransactionNumber, CompetitionTypeID, ContractTypeID, ContractVehicleID, FiscalYearContractRangeID, NAICSID, PSCID, ContractingOrganizationID, FundingOrganizationID, OriginatingOrganizationID, RequirementTypeID, TotalContractRangeID, EstimatedSolicitationQuarter)
SELECT
	BDI.BulkDataImportID, 
	BDI.ContractingOfficeName, 
	BDI.ContractName, 
	BDI.ContractDescription, 
	BDI.TargetAwardDate, 
	BDI.ContractDateStart, 
	BDI.ContractDateEnd, 
	BDI.IncumbentContractorName, 
	BDI.ReferenceIDV, 
	BDI.TransactionNumber, 
	BDI.CompetitionTypeID, 
	BDI.ContractTypeID,
	BDI.ContractVehicleID,
	BDI.FiscalYearContractRangeID, 
	BDI.NAICSID, 
	BDI.PSCID, 
	BDI.ContractingOrganizationID, 
	BDI.FundingOrganizationID, 
	BDI.OriginatingOrganizationID, 
	BDI.RequirementTypeID, 
	BDI.TotalContractRangeID, 
	BDI.EstimatedSolicitationQuarter
FROM BulkData.BulkDataImport BDI

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.Contract'
	ROLLBACK TRANSACTION
	RETURN
	
	END
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.Contract'
--ENDIF

INSERT INTO dbo.ContractPlaceOfPerformance
	(ContractID, PlaceOfPerformanceCity, PlaceOfPerformanceStateID, IsPrimary)
SELECT
	C.ContractID,
	BDI.PlaceOfPerformanceCity,
	BDI.PlaceOfPerformanceStateID,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PlaceOfPerformanceCity IS NOT NULL
				OR BDI.PlaceOfPerformanceStateID > 0
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPlaceOfPerformance'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPlaceOfPerformance'
--ENDIF

INSERT INTO dbo.ContractPointOfContact
	(ContractID, PointOfContactAgency, PointOfContactEmail, PointOfContactName, PointOfContactPhone, PointOfContactTitle, IsPrimary)
SELECT
	C.ContractID,
	BDI.PointOfContactAgency,
	BDI.PointOfContactEmail,
	BDI.PointOfContactName,
	BDI.PointOfContactPhone,
	BDI.PointOfContactTitle,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PointOfContactAgency IS NOT NULL
				OR BDI.PointOfContactEmail IS NOT NULL
				OR BDI.PointOfContactName IS NOT NULL
				OR BDI.PointOfContactPhone IS NOT NULL
				OR BDI.PointOfContactTitle IS NOT NULL
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPointOfContact'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPointOfContact'
--ENDIF

DECLARE @nContractID INT
DECLARE @nWorkflowStatusID INT = (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusCode = 'Approved')

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT C.ContractID
	FROM dbo.Contract C
	WHERE C.BulkDataImportID > 0
	ORDER BY C.ContractID

OPEN oCursor
FETCH oCursor INTO @nContractID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogContractEntry 'Bulk Imported', @nContractID, 'Create'
	EXEC dbo.AssignWorkflowToContract 'Bulk Imported', @nContractID, 0

	--UPDATE C
	--SET 
	--	C.WorkflowStatusID = @nWorkflowStatusID,
	--	C.WorkflowStepNumber = (SELECT MAX(WP.WorkflowStepNumber) FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID)
	--FROM dbo.Contract C
	--WHERE C.ContractID = @nContractID

	--EXEC dbo.AddEventLogContractEntry 'Bulk Approved', @nContractID, 'Approve'

	FETCH oCursor INTO @nContractID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

IF @@ERROR <> 0
 BEGIN

	print 'Error creating event log entries for dbo.Contract bulk data import'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Created event log entries for dbo.Contract bulk data import'
--ENDIF

COMMIT TRANSACTION
GO