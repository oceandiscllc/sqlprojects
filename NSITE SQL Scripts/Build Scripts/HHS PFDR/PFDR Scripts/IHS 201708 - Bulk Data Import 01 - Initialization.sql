USE PFDR
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC utility.AddColumn @TableName, 'BulkDataImportCode', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'BulkDataImportID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'BulkDataImportID', 'INT', 0
GO

print 'Verified columns BulkDataImportCode and BulkDataImportID exists in table dbo.Contract'

IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'BulkData')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX)
	SET @cSQL = 'CREATE SCHEMA BulkData'
	
	EXEC (@cSQL)
	
	print 'Added schema BulkData to database PFDR'

	END
--ENDIF

DECLARE @TableName VARCHAR(250) = 'BulkData.BulkDataImport'
EXEC utility.DropObject @TableName

CREATE TABLE BulkData.BulkDataImport
	(
	BulkDataImportID INT IDENTITY(1,1) NOT NULL,
	ContractingOfficeName VARCHAR(250),
	ContractName VARCHAR(250),
	ContractDescription VARCHAR(MAX),
	TargetAwardDate DATE,
	ContractDateStart DATE,
	ContractDateEnd DATE,
	IncumbentContractorName VARCHAR(250),
	ReferenceIDV VARCHAR(250),
	TransactionNumber VARCHAR(100),
	CompetitionType VARCHAR(250),
	CompetitionTypeID INT,
	ContractType VARCHAR(250),
	ContractTypeID INT,
	ContractVehicle VARCHAR(250),
	ContractVehicleID INT,
	FiscalYearContractRange VARCHAR(250),
	FiscalYearContractRangeID INT,
	NAICSCode VARCHAR(250),
	NAICSID INT,
	PSCCode VARCHAR(250),
	PSCID INT,
	ContractingOrganization VARCHAR(250),
	ContractingOrganizationID INT,
	FundingOrganization VARCHAR(250),
	FundingOrganizationID INT,
	OriginatingOrganization VARCHAR(250),
	OriginatingOrganizationID INT,
	RequirementType VARCHAR(250),
	RequirementTypeID INT,
	TotalContractRange VARCHAR(250),
	TotalContractRangeID INT,
	EstimatedSolicitationQuarter VARCHAR(5),	
	PlaceOfPerformanceCity VARCHAR(250),
	PlaceOfPerformanceStateCode VARCHAR(10),	
	PlaceOfPerformanceStateID INT,	
	PointOfContactName VARCHAR(250),
	PointOfContactEmail VARCHAR(320),
	PointOfContactPhone VARCHAR(50),
	PointOfContactAgency VARCHAR(250),
	PointOfContactTitle VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CompetitionTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContractingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContractTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContractVehicleID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FiscalYearContractRangeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PSCID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlaceOfPerformanceStateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequirementTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TotalContractRangeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'BulkDataImportID'
GO

print 'Created table BulkData.BulkDataImport'
GO