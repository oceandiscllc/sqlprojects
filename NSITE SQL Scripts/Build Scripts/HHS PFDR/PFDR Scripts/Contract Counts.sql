USE PFDR
GO

WITH D AS
	(
	SELECT 
		O.OrganizationName, 
		C.WorkflowStepNumber,
		(SELECT MAX(WorkflowStepNumber) FROM [dbo].[ContractWorkflowPerson] CWP WHERE CWP.ContractID = C.ContractID GROUP BY CWP.ContractID) AS MaxWorkflowStepNumber
	FROM dbo.Contract C
		JOIN [Dropdown].[OrganizationHierarchy] OH ON OH.[OrganizationID] = C.[OriginatingOrganizationID]
		JOIN [Dropdown].[Organization] O ON O.[OrganizationID] = OH.[TopLevelOrganizationID]
		LEFT JOIN [dbo].[EventLog] E ON E.EntityID = C.ContractID
			AND E.EntityTypeCode = 'Contract' 
			AND E.EventCode = 'Create'
	WHERE E.CreateDateTime <= '05/19/2015'
		OR E.CreateDateTime IS NULL
	)

SELECT
	'Complete' AS WorkflowStatus,
	COUNT(D.OrganizationName) AS ItemCount,
	D.OrganizationName
FROM D
WHERE D.WorkflowStepNumber > D.MaxWorkflowStepNumber
GROUP BY D.OrganizationName
	

--UNION

SELECT
	'Incomplete' AS WorkflowStatus,
	COUNT(D.OrganizationName) AS ItemCount,
	D.OrganizationName
FROM D
WHERE D.WorkflowStepNumber <= D.MaxWorkflowStepNumber
GROUP BY D.OrganizationName
ORDER BY D.OrganizationName

/*
SELECT top 1000 *
FROM [dbo].[EventLog] E
WHERE EntityTypeCode = 'Contract' AND EntityID IN 
	(
748,
749,
750,
751,
752,
753,
754,
755,
756,
757,
758,
759,
760,
761,
762,
763,
764,
765,
766,
767,
768,
769,
770,
771,
772,
773,
774,
775,
776,
777,
778,
779,
780,
781,
782,
783,
784,
785,
786,
787,
788,
789,
790,
791,
792,
793,
794,
795,
796,
797,
798,
799,
800,
801,
802,
803,
804,
805,
806,
807,
808,
809,
810,
811,
812,
813,
814,
815,
816,
817,
818,
819,
820,
821,
822,
823,
824,
825,
826,
827,
828,
829,
830,
831,
832,
833,
834,
835,
836,
837,
838,
839,
840,
841,
842,
843,
844,
845,
846,
847,
848,
849,
850,
851,
852,
853,
854,
855,
856,
857,
858,
859,
860,
861,
862,
863,
864,
865,
866,
867,
868,
869,
870,
871,
872,
873,
874,
875,
876,
877,
878,
879,
880,
881,
882,
883,
884,
885
	)
ORDER BY 1 DESC
*/