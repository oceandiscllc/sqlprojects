USE PFDR
GO

DECLARE @OpDivCode VARCHAR(5) = 'IHS'

UPDATE dbo.Contract SET BulkDataImportID = 0

print 'Initialized table dbo.Contract'

BEGIN TRANSACTION

INSERT INTO dbo.Contract
	(BulkDataImportCode, BulkDataImportID, ContractingOfficeName, ContractName, ContractDescription, TargetAwardDate, ContractDateStart, ContractDateEnd, IncumbentContractorName, ReferenceIDV, TransactionNumber, CompetitionTypeID, ContractTypeID, ContractVehicleID, FiscalYearContractRangeID, NAICSID, PSCID, ContractingOrganizationID, FundingOrganizationID, OriginatingOrganizationID, RequirementTypeID, TotalContractRangeID, EstimatedSolicitationQuarter)
SELECT
	'BulkDataImport ' + CAST((YEAR(getDate()) * 100 + Month(getDate())) * 100 + DAY(getDate()) AS CHAR(8)) + ' - ' + @OpDivCode,
	BDI.BulkDataImportID, 
	BDI.ContractingOfficeName, 
	ISNULL(BDI.ContractName, 'No Contract Name Provided'),
	BDI.ContractDescription, 
	BDI.TargetAwardDate, 
	BDI.ContractDateStart, 
	BDI.ContractDateEnd, 
	BDI.IncumbentContractorName, 
	BDI.ReferenceIDV, 
	BDI.TransactionNumber, 
	BDI.CompetitionTypeID, 
	BDI.ContractTypeID,
	BDI.ContractVehicleID,
	BDI.FiscalYearContractRangeID, 
	BDI.NAICSID, 
	BDI.PSCID, 
	BDI.ContractingOrganizationID, 
	BDI.FundingOrganizationID, 
	BDI.OriginatingOrganizationID, 
	BDI.RequirementTypeID, 
	BDI.TotalContractRangeID, 
	ISNULL(BDI.EstimatedSolicitationQuarter, 0)
FROM BulkData.BulkDataImport BDI

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.Contract'
	ROLLBACK TRANSACTION
	RETURN
	
	END
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.Contract'
--ENDIF

INSERT INTO dbo.ContractPlaceOfPerformance
	(ContractID, PlaceOfPerformanceCity, PlaceOfPerformanceStateID, IsPrimary)
SELECT
	C.ContractID,
	BDI.PlaceOfPerformanceCity,
	BDI.PlaceOfPerformanceStateID,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PlaceOfPerformanceCity IS NOT NULL
				OR BDI.PlaceOfPerformanceStateID > 0
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPlaceOfPerformance'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPlaceOfPerformance'
--ENDIF

INSERT INTO dbo.ContractPointOfContact
	(ContractID, PointOfContactAgency, PointOfContactEmail, PointOfContactName, PointOfContactPhone, PointOfContactTitle, IsPrimary)
SELECT
	C.ContractID,
	BDI.PointOfContactAgency,
	BDI.PointOfContactEmail,
	BDI.PointOfContactName,
	BDI.PointOfContactPhone,
	BDI.PointOfContactTitle,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND 
			(
			BDI.PointOfContactAgency IS NOT NULL
				OR BDI.PointOfContactEmail IS NOT NULL
				OR BDI.PointOfContactName IS NOT NULL
				OR BDI.PointOfContactPhone IS NOT NULL
				OR BDI.PointOfContactTitle IS NOT NULL
			)

IF @@ERROR <> 0
 BEGIN

	print 'Error inserting into dbo.ContractPointOfContact'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Inserted BulkData.BulkDataImport records into dbo.ContractPointOfContact'
--ENDIF

DECLARE @nContractID INT
--DECLARE @nWorkflowStatusID INT = (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusCode = 'Approved')

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT C.ContractID
	FROM dbo.Contract C
	WHERE C.BulkDataImportID > 0
	ORDER BY C.ContractID

OPEN oCursor
FETCH oCursor INTO @nContractID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogContractEntry 'Bulk Imported', @nContractID, 'Create'
	EXEC dbo.AssignWorkflowToContract 'Bulk Imported', @nContractID, 0

	--Move to approved step
	--UPDATE C
	--SET 
	--	C.WorkflowStatusID = @nWorkflowStatusID,
	--	C.WorkflowStepNumber = (SELECT MAX(WP.WorkflowStepNumber) FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID)
	--FROM dbo.Contract C
	--WHERE C.ContractID = @nContractID

	--EXEC dbo.AddEventLogContractEntry 'Bulk Approved', @nContractID, 'Approve'

	FETCH oCursor INTO @nContractID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

IF @@ERROR <> 0
 BEGIN

	print 'Error creating event log entries for dbo.Contract bulk data import'
	ROLLBACK TRANSACTION
	RETURN
	
	END		
ELSE
	print 'Created event log entries for dbo.Contract bulk data import'
--ENDIF

COMMIT TRANSACTION
GO