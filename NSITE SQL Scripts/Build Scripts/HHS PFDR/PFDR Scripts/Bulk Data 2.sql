USE PFDR
GO

UPDATE Dropdown.CompetitionType
SET CompetitionTypeName = 'Not Competed - Small'
WHERE CompetitionTypeName = 'Not Competed – Small'
GO

UPDATE Dropdown.CompetitionType
SET CompetitionTypeName = 'Not Competed - Other Than Small'
WHERE CompetitionTypeName = 'Not Competed – Other Than Small'
GO

UPDATE Dropdown.ContractRange
SET IsActive = 0
WHERE ContractRangeID = 12
GO

UPDATE BDI
SET BDI.CompetitionType = '8A Competed'
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = '8(A) Competed'
GO

UPDATE BDI
SET BDI.CompetitionType = 'Not Competed - Small'
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = 'Not Competed-Small'
GO

UPDATE BDI
SET BDI.CompetitionType = 'Small Business Set Aside - Total'
FROM BulkData.BulkDataImport BDI
WHERE BDI.CompetitionType = 'Small Business Set Aside-Total'
GO

UPDATE BDI
SET BDI.ContractType = 'FSS'
FROM BulkData.BulkDataImport BDI
WHERE BDI.ContractType = 'Federal Supply Schedule'
GO

UPDATE BDI
SET BDI.ContractType = 'IDIQ'
FROM BulkData.BulkDataImport BDI
WHERE BDI.ContractType = 'Indefinite Delivery'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '> $100M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '>$10,000,001'
GO

UPDATE BDI
SET BDI.TotalContractRange = '> $100M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '>$10,000,001'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '>= $500K and < $1M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '$500,001 to $1,000,000'
GO

UPDATE BDI
SET BDI.TotalContractRange = '>= $500K and < $1M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '$500,001 to $1,000,000'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '>= $5M and < $10M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '$5,000,001 to $10,000,000'
GO

UPDATE BDI
SET BDI.TotalContractRange = '>= $5M and < $10M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '$5,000,001 to $10,000,000'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '>= $150K and < $500K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '$150,001 to $500,000'
GO

UPDATE BDI
SET BDI.TotalContractRange = '>= $150K and < $500K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '$150,001 to $500,000'
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '> $25K and < $150K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange IN ('$25,000 to $50,000','$50,001 to $75,000','$75,001 to $100,000','$100,001 to $125,000','$125,001 to $150,000')
GO

UPDATE BDI
SET BDI.TotalContractRange = '> $25K and < $150K'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange IN ('$25,000 to $50,000','$50,001 to $75,000','$75,001 to $100,000','$100,001 to $125,000','$125,001 to $150,000')
GO

UPDATE BDI
SET BDI.FiscalYearContractRange = '>= $2M and < $5M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.FiscalYearContractRange = '$1,000,001 to $5,000,000'
GO

UPDATE BDI
SET BDI.TotalContractRange = '>= $2M and < $5M'
FROM BulkData.BulkDataImport BDI
WHERE BDI.TotalContractRange = '$1,000,001 to $5,000,000'
GO





UPDATE dbo.Contract SET BulkDataImportID = 0
GO

UPDATE BDI
SET 
	BDI.CompetitionTypeID = 0,
	BDI.FiscalYearContractRangeID = 0,
	BDI.TotalContractRangeID = 0,
	BDI.ContractTypeID = 0,
	BDI.NAICSID = 0,
	BDI.ContractingOrganizationID = 0,
	BDI.FundingOrganizationID = 0,
	BDI.OriginatingOrganizationID = 0,
	BDI.PSCID = 0,
	BDI.RequirementTypeID = 0
FROM BulkData.BulkDataImport BDI
GO

UPDATE BDI
SET BDI.CompetitionTypeID = T.CompetitionTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.CompetitionType T ON T.CompetitionTypeName = BDI.CompetitionType
GO

UPDATE BDI
SET BDI.FiscalYearContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.FiscalYearContractRange
GO

UPDATE BDI
SET BDI.TotalContractRangeID = T.ContractRangeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.TotalContractRange
GO

UPDATE BDI
SET BDI.ContractTypeID = T.ContractTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.ContractType T ON T.ContractTypeName = BDI.ContractType
GO

UPDATE BDI
SET BDI.NAICSID = T.NAICSID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.NAICS T ON T.NAICSCode = BDI.NAICSCode
GO

UPDATE BDI
SET BDI.ContractingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.ContractingOrganization
GO

UPDATE BDI
SET BDI.FundingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = BDI.FundingOrganization
GO

UPDATE BDI
SET BDI.OriginatingOrganizationID = T.OrganizationID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.Organization T ON T.OrganizationName = 'CDC'
GO

UPDATE BDI
SET BDI.PSCID = T.PSCID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.PSC T ON T.PSCCode = BDI.PSCCode
GO

UPDATE BDI
SET BDI.RequirementTypeID = T.RequirementTypeID
FROM BulkData.BulkDataImport BDI
	JOIN Dropdown.RequirementType T ON T.RequirementTypeName = BDI.RequirementType
GO
