USE PFDR
GO

DECLARE @OrganizationID INT = 14

SELECT
	D.PersonID,
	P.EmailAddress,
	dbo.GetPersonNameByPersonID(D.PersonID, 'LastFirst') AS PersonNameFormatted
FROM
	(
	SELECT DISTINCT
		PR.PersonID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Organization O ON O.OrganizationID = PR.OrganizationID
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND R.RoleCode = 'OrganizationAdministrator'
			AND EXISTS
				(
				SELECT 1
				FROM Utility.ListToTable((SELECT OH.OrganizationIDLineage FROM Dropdown.OrganizationHierarchy OH WHERE OH.OrganizationID = @OrganizationID), ',', 0) LTT
				WHERE LTT.ListItem = PR.OrganizationID
				)
	) D
	JOIN dbo.Person P ON P.PersonID = D.PersonID
ORDER BY 3