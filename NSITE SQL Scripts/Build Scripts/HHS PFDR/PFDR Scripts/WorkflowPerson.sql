SELECT 
	O.OrganizationName,
	WP1.WorkflowStepNumber,

	CASE
		WHEN WP1.WorkflowStepNumber = 1
		THEN 'Originator'
		WHEN WP1.WorkflowStepNumber = (SELECT MAX(WP2.WorkflowStepNumber) FROM dbo.WorkflowPerson WP2 WHERE WP2.WorkflowID = WP1.WorkflowID)
		THEN 'Final Approver'
		ELSE 'Reviewer'
	END AS WorkflowAssignment,

	P.FirstName,
	P.LastName,
	P.EmailAddress
FROM dbo.WorkflowPerson WP1
	JOIN dbo.Person P ON P.PersonID = WP1.PersonID
	JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
ORDER BY
	O.OrganizationName,
	WP1.WorkflowStepNumber,
	P.FirstName,
	P.LastName,
	P.EmailAddress
