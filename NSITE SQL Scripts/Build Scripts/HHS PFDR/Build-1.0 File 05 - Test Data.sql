USE PFDR
GO

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Alex',
	'Adams',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'FDA'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Bart',
	'Benson',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'NIH'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Chelsea',
	'Cook',
	'Mrs.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'CDC'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Doug',
	'Dorner',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'FDA'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Edna',
	'Edwards',
	'Mrs.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'NIH'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Frank',
	'Fixx',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'CDC'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Greg',
	'Gibson',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'AHRQ'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Hal',
	'Holmes',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'HRSA'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Isabel',
	'Innes',
	'Miss'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'NIH'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Joanne',
	'Johnson',
	'Miss'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'SAMHSA'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Kyle',
	'Kapps',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'PSC'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Lisa',
	'Loring',
	'Miss'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'OS/ASPR'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Monty',
	'Morris',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Non-HHS'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Nate',
	'Newsome',
	'Mr.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'NIH'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Ophelia',
	'Olsen',
	'Mrs.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'IHS'

INSERT dbo.Person
	(OrganizationID, FirstName, LastName, Title)
SELECT
	O.OrganizationID,
	'Pat',
	'Petersen',
	'Mrs.'
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'HRSA'
GO

UPDATE dbo.Person
SET 
	EmailAddress = 'test.pfdr@nsitellc.com',
	UserName = LOWER(LEFT(FirstName, 1) + LastName), 
	Phone = '555-' + RIGHT('0000' + CAST(PersonID AS VARCHAR(5)), 4)
WHERE EmailAddress IS NULL
GO		
		
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (3, 4, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (4, 7, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (5, 2, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (5, 24, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (5, 35, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (5, 100, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (9, 10, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (13, 6, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (15, 4, 2)
INSERT dbo.PersonRole (PersonID, OrganizationID, RoleID) VALUES (17, 2, 2)
GO

DECLARE @TargetPersonID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT P.PersonID
	FROM dbo.Person P
	WHERE NOT EXISTS
		(
		SELET 1
		FROM dbo.EventLog EL
		WHERE EL.EntityTypeCode = 'Person'
			AND EL.EntityID = P.PersonID
			AND EL.EventCode = 'Create'
		)
	ORDER BY P.PersonID

OPEN oCursor
FETCH oCursor INTO @TargetPersonID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = 'Create', @SourcePersonID = 0
	FETCH oCursor INTO @TargetPersonID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tEventLog', 'u')) IS NOT NULL
	DROP TABLE #tEventLog
--ENDIF
GO

SELECT EL.* 
INTO #tEventLog 
FROM dbo.EventLog EL
WHERE EntityTypeCode = 'Contract'
GO

INSERT dbo.WorkflowPerson 
	(WorkflowID, WorkflowStepNumber, PersonID) 
VALUES 
	(4, 2, 3),
	(4, 2, 4),
	(4, 2, 5),
	(4, 3, 9),
	(4, 3, 10),
	(4, 4, 1),
	(95, 1, 3),
	(95, 1, 4),
	(95, 1, 5),
	(95, 3, 11),
	(95, 2, 9),
	(95, 2, 10),
	(95, 3, 7),
	(95, 3, 8),
	(95, 4, 6),
	(4, 1, 10),
	(95, 4, 12),
	(95, 4, 13),
	(1, 1, 3),
	(1, 1, 4),
	(1, 2, 6),
	(1, 2, 7),
	(4, 1, 11),
	(1, 3, 10),
	(1, 3, 11),
	(1, 4, 1),
	(1, 4, 5)
GO

DECLARE @WorkflowID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT WP.WorkflowID
	FROM dbo.WorkflowPerson WP
	ORDER BY WP.WorkflowID

OPEN oCursor
FETCH oCursor INTO @WorkflowID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
	FETCH oCursor INTO @WorkflowID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

INSERT INTO dbo.EventLog
	(PersonID, EventCode, EntityID, EntityTypeCode, Comments, EventData, CreateDateTime)
SELECT
	TEL.PersonID, 
	TEL.EventCode, 
	TEL.EntityID, 
	TEL.EntityTypeCode, 
	TEL.Comments, 
	TEL.EventData, 
	TEL.CreateDateTime
FROM #tEventLog TEL
GO

DROP TABLE #tEventLog
GO

INSERT INTO Dropdown.QuickSearch
	(QuickSearchName,QuickSearchCriteria,QuickSearchDescription)
VALUES
	(
	'Contract opportunities containing the word "Workflow" in the title',
	'{"ContractName":"Workflow"}',
	'This search will return a list of all contract opportunities the word "Workflow" in the title.'
	)
GO

