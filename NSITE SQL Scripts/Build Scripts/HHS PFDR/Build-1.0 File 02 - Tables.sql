USE PFDR
GO

--Begin Tables
--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@TableName) AND O.Type = 'U')
	BEGIN

	CREATE TABLE dbo.Contract
		(
		ContractID INT IDENTITY(1,1) NOT NULL,
		ContractingOfficeName VARCHAR(250),
		ContractName VARCHAR(250),
		ContractDescription VARCHAR(MAX),
		TargetAwardDate DATE,
		ContractDateStart DATE,
		ContractDateEnd DATE,
		IncumbentContractorName VARCHAR(250),
		ReferenceIDV VARCHAR(250),
		TransactionNumber VARCHAR(100),
		CompetitionTypeID INT,
		ContractTypeID INT,
		FiscalYearContractRangeID INT,
		NAICSID INT,
		PSCID INT,
		ContractingOrganizationID INT,
		FundingOrganizationID INT,
		OriginatingOrganizationID INT,
		RequirementTypeID INT,
		TotalContractRangeID INT,
		EstimatedSolicitationQuarter INT,
		WorkflowID INT,
		WorkflowStatusID INT,
		WorkflowStepNumber INT
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'CompetitionTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ContractingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ContractTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'EstimatedSolicitationQuarter', 'INT', 0, 1
EXEC Utility.SetDefaultConstraint @TableName, 'FiscalYearContractRangeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PSCID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'RequirementTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'TotalContractRangeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', 0, 1
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStatusID', 'INT', 1, 1
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 1, 1

EXEC Utility.SetPrimaryKeyClustered @TableName, 'ContractID'
EXEC Utility.SetIndexNonclustered 'IX_Contract', 'dbo.Contract', 'ContractName', 'CompetitionTypeID,ContractDateEnd,ContractID,ContractingOfficeName,ContractingOrganizationID,ContractDateStart,ContractTypeID,EstimatedSolicitationQuarter,FiscalYearContractRangeID,FundingOrganizationID,IncumbentContractorName,NAICSID,PSCID,ReferenceIDV,RequirementTypeID,TargetAwardDate,TotalContractRangeID'
EXEC Utility.SetIndexNonclustered 'IX_ContractWorkflowStatus', 'dbo.Contract', 'WorkflowStatusID', 'ContractID'
GO
--End table dbo.Contract

--Begin table dbo.ContractWorkflowPerson
DECLARE @TableName VARCHAR(250) = 'dbo.ContractWorkflowPerson'

EXEC Utility.DropObject @TableName

CREATE TABLE dbo.ContractWorkflowPerson
	(
	ContractWorkflowPersonID INT IDENTITY(1,1) NOT NULL,
	ContractID INT,
	WorkflowStepNumber INT,
	PersonID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'ContractID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractWorkflowPersonID'
EXEC Utility.SetIndexClustered 'IX_ContractWorkflowPerson', 'dbo.ContractWorkflowPerson', 'ContractID,WorkflowStepNumber,PersonID'
GO
--End table dbo.ContractWorkflowPerson

--Begin table dbo.ContractPlaceOfPerformance
DECLARE @TableName VARCHAR(250) = 'dbo.ContractPlaceOfPerformance'

IF NOT EXISTS (SELECT 1 FROM sys.tables T WHERE T.Name = 'ContractPlaceOfPerformance')
	BEGIN

	CREATE TABLE dbo.ContractPlaceOfPerformance
		(
		ContractPlaceOfPerformanceID INT IDENTITY(1,1) NOT NULL,
		ContractID INT,
		PlaceOfPerformanceCity VARCHAR(250),
		PlaceOfPerformanceStateID INT,
		IsPrimary BIT
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'ContractID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsPrimary', 'BIT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractPlaceOfPerformanceID'
EXEC Utility.SetIndexClustered 'IX_ContractPlaceOfPerformance', @TableName, 'ContractID, IsPrimary DESC, PlaceOfPerformanceCity, PlaceOfPerformanceStateID'
GO
--End table dbo.ContractPlaceOfPerformance

--Begin table dbo.ContractPointOfContact
DECLARE @TableName VARCHAR(250) = 'dbo.ContractPointOfContact'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@TableName) AND O.Type = 'U')
	BEGIN

	CREATE TABLE dbo.ContractPointOfContact
		(
		ContractPointOfContactID INT IDENTITY(1,1) NOT NULL,
		ContractID INT,
		PointOfContactAgency VARCHAR(250),
		PointOfContactEmail VARCHAR(320),
		PointOfContactName VARCHAR(250),
		PointOfContactPhone VARCHAR(50),
		PointOfContactTitle VARCHAR(50),
		IsPrimary BIT
		)

	END
--ENDIF
	
EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'ContractID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsPrimary', 'BIT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractPointOfContactID'
EXEC Utility.SetIndexClustered 'IX_ContractPointOfContact', @TableName, 'ContractID, IsPrimary DESC, PointOfContactName'
GO
--End table dbo.ContractPointOfContact

--Begin table dbo.EventLog
DECLARE @TableName VARCHAR(250) = 'dbo.EventLog'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@TableName) AND O.Type = 'U')
	BEGIN

	CREATE TABLE dbo.EventLog
		(
		EventLogID INT IDENTITY(1,1) NOT NULL,
		PersonID INT,
		EventCode VARCHAR(50),
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		Comments VARCHAR(MAX),
		EventData XML,
		CreateDateTime DATETIME
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'EventLogID'
EXEC Utility.SetIndexClustered 'IX_EventLog', @TableName, 'EntityTypeCode, EntityID, EventLogID'
GO
--End table dbo.EventLog

--Begin table dbo.Person
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC Utility.DropObject @TableName

CREATE TABLE dbo.Person
	(
	PersonID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	Phone VARCHAR(50),
	Title VARCHAR(50),
	EmailAddress VARCHAR(320),
	UserName VARCHAR(50),
	IsActive BIT
	)

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', 0

EXEC Utility.SetPrimaryKeyclustered @TableName, 'PersonID'
EXEC Utility.SetIndexNonclustered 'IX_Person', @TableName, 'EmailAddress,UserName', 'PersonID'
GO
--End table dbo.Person

--Begin table dbo.PersonRole
DECLARE @TableName VARCHAR(250) = 'dbo.PersonRole'

EXEC Utility.DropObject @TableName

CREATE TABLE dbo.PersonRole
	(
	PersonRoleID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	OrganizationID INT,
	RoleID INT,
	CreateDateTime DATETIME
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PersonRoleID'
EXEC Utility.SetIndexClustered 'IX_PersonRole', @TableName, 'PersonID, OrganizationID'
GO
--End table dbo.PersonRole

--Begin table dbo.Workflow
DECLARE @TableName VARCHAR(250) = 'dbo.Workflow'

EXEC Utility.DropObject @TableName

CREATE TABLE dbo.Workflow
	(
	WorkflowID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', 0

EXEC Utility.SetPrimaryKeyClustered @TableName, 'WorkflowID'
GO
--End table dbo.Workflow

--Begin table dbo.WorkflowPerson
DECLARE @TableName VARCHAR(250) = 'dbo.WorkflowPerson'

EXEC Utility.DropObject @TableName

CREATE TABLE dbo.WorkflowPerson
	(
	WorkflowPersonID INT IDENTITY(1,1) NOT NULL,
	WorkflowID INT,
	WorkflowStepNumber INT,
	PersonID INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC Utility.SetPrimaryKeyClustered @TableName, 'WorkflowPersonID'
EXEC Utility.SetIndexNonclustered 'IX_WorkflowPerson', 'dbo.WorkflowPerson', 'WorkflowID,WorkflowStepNumber,PersonID'
GO
--End table dbo.WorkflowPerson

--Begin table Dropdown.CompetitionType
DECLARE @TableName VARCHAR(250) = 'Dropdown.CompetitionType'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.CompetitionType
	(
	CompetitionTypeID INT IDENTITY(0,1) NOT NULL,
	CompetitionTypeCode VARCHAR(50),
	CompetitionTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'CompetitionTypeID'
EXEC Utility.SetIndexClustered 'IX_CompetitionType', @TableName, 'DisplayOrder,CompetitionTypeName,CompetitionTypeID'
GO
--End table Dropdown.CompetitionType

--Begin table Dropdown.ContractRange
DECLARE @TableName VARCHAR(250) = 'Dropdown.ContractRange'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ContractRange
	(
	ContractRangeID INT IDENTITY(0,1) NOT NULL,
	ContractRangeCode VARCHAR(50),
	ContractRangeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractRangeID'
EXEC Utility.SetIndexClustered 'IX_CompetitionType', @TableName, 'DisplayOrder,ContractRangeName,ContractRangeID'
GO
--End table Dropdown.ContractRange

--Begin table Dropdown.ContractType
DECLARE @TableName VARCHAR(250) = 'Dropdown.ContractType'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ContractType
	(
	ContractTypeID INT IDENTITY(0,1) NOT NULL,
	ContractTypeCode VARCHAR(50),
	ContractTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractTypeID'
EXEC Utility.SetIndexClustered 'IX_CompetitionType', @TableName, 'DisplayOrder,ContractTypeName,ContractTypeID'
GO
--End table Dropdown.ContractType

--Begin table Dropdown.NAICS
DECLARE @TableName VARCHAR(250) = 'Dropdown.NAICS'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.NAICS
	(
	NAICSID INT IDENTITY(0,1) NOT NULL,
	NAICSCode VARCHAR(50),
	NAICSName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'NAICSID'
EXEC Utility.SetIndexClustered 'IX_NAICS', @TableName, 'DisplayOrder,NAICSCode,NAICSName,NAICSID'
GO
--End table Dropdown.NAICS

--Begin table Dropdown.Organization
DECLARE @TableName VARCHAR(250) = 'Dropdown.Organization'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.Organization
	(
	OrganizationID INT IDENTITY(0,1) NOT NULL,
	ParentOrganizationID INT NOT NULL,
	OrganizationCode VARCHAR(50),
	OrganizationName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC Utility.SetDefaultConstraint @TableName, 'ParentOrganizationID', 'INT', 0

EXEC Utility.SetPrimaryKeyClustered @TableName, 'OrganizationID'
GO
--End table Dropdown.Organization

--Begin table Dropdown.OrganizationHierarchy
DECLARE @TableName VARCHAR(250) = 'Dropdown.OrganizationHierarchy'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.OrganizationHierarchy
	(
	OrganizationHierarchyID INT IDENTITY(1,1) NOT NULL,
	DisplayIndex VARCHAR(MAX),
	OrganizationNameLineage VARCHAR(MAX),
	OrganizationIDLineage VARCHAR(MAX),
	OrganizationID INT,
	OrganizationName VARCHAR(250),
	ParentOrganizationID INT,
	TopLevelOrganizationID INT,
	NodeLevel INT,
	HasChildren BIT
	)

EXEC Utility.SetPrimaryKeyClustered @TableName, 'OrganizationHierarchyID'
GO
--End table Dropdown.OrganizationHierarchy

--Begin table Dropdown.PSC
DECLARE @TableName VARCHAR(250) = 'Dropdown.PSC'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.PSC
	(
	PSCID INT IDENTITY(0,1) NOT NULL,
	PSCCode VARCHAR(50),
	PSCName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'PSCID'
EXEC Utility.SetIndexClustered 'IX_PSC', @TableName, 'DisplayOrder,PSCCode,PSCName,PSCID'
GO
--End table Dropdown.PSC

--Begin table Dropdown.QuickSearch
DECLARE @TableName VARCHAR(250) = 'Dropdown.QuickSearch'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.QuickSearch
	(
	QuickSearchID INT IDENTITY(1,1) NOT NULL,
	QuickSearchName VARCHAR(250),
	QuickSearchCriteria VARCHAR(MAX),
	QuickSearchDescription VARCHAR(MAX),
	DisplayOrder INT,
	IsPublic BIT,
	IsActive BIT
	)

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC Utility.SetDefaultConstraint @TableName, 'IsPublic', 'BIT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'QuickSearchID'
EXEC Utility.SetIndexClustered 'IX_QuickSearch', @TableName, 'DisplayOrder,QuickSearchName,QuickSearchID'
GO
--End table Dropdown.QuickSearch

--Begin table Dropdown.RequirementType
DECLARE @TableName VARCHAR(250) = 'Dropdown.RequirementType'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.RequirementType
	(
	RequirementTypeID INT IDENTITY(0,1) NOT NULL,
	RequirementTypeCode VARCHAR(50),
	RequirementTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'RequirementTypeID'
EXEC Utility.SetIndexClustered 'IX_RequirementType', @TableName, 'DisplayOrder,RequirementTypeName,RequirementTypeID'
GO
--End table Dropdown.RequirementType

--Begin table Dropdown.Role
DECLARE @TableName VARCHAR(250) = 'Dropdown.Role'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleCode VARCHAR(50),
	RoleName VARCHAR(50),
	RoleWeight INT,
	DisplayOrder INT,
	IsAdministrator BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsAdministrator', 'BIT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'RoleWeight', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'RoleID'
EXEC Utility.SetIndexClustered 'IX_Role', @TableName, 'DisplayOrder, RoleName'
GO
--End table Dropdown.Role

--Begin table Dropdown.State
DECLARE @TableName VARCHAR(250) = 'Dropdown.State'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.State
	(
	StateID INT IDENTITY(0,1) NOT NULL,
	StateCode VARCHAR(50),
	StateName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'StateID'
EXEC Utility.SetIndexClustered 'IX_State', @TableName, 'DisplayOrder,StateName,StateID'
GO
--End table Dropdown.State

--Begin table Dropdown.WorkflowStatus
DECLARE @TableName VARCHAR(250) = 'Dropdown.WorkflowStatus'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.WorkflowStatus
	(
	WorkflowStatusID INT IDENTITY(0,1) NOT NULL,
	WorkflowStatusCode VARCHAR(50),
	WorkflowStatusName VARCHAR(250),
	DisplayOrder INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStatusID'
EXEC Utility.SetIndexClustered 'IX_RequirementType', @TableName, 'WorkflowStatusCode'
GO
--End table Dropdown.WorkflowStatus
--End Tables

--Begin Triggers
--Begin trigger Dropdown.TR_Organization
EXEC Utility.DropObject 'Dropdown.TR_Organization'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2014.05.29
-- Description:	A trigger to update the Dropdown.OrganizationHierarchy table
-- =========================================================================
CREATE TRIGGER Dropdown.TR_Organization ON Dropdown.Organization FOR INSERT, DELETE, UPDATE
AS
SET ARITHABORT ON

TRUNCATE TABLE Dropdown.OrganizationHierarchy

DECLARE @PadLength INT

SELECT @PadLength = LEN(CAST(COUNT(O.OrganizationID) AS VARCHAR(50)))
FROM Dropdown.Organization O

;
WITH HD (DisplayIndex,OrganizationNameLineage,OrganizationIDLineage,OrganizationID,ParentOrganizationID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY O.DisplayOrder, O.OrganizationName) AS VARCHAR(10)), @PadLength)),
		CAST(O.OrganizationName AS VARCHAR(MAX)),
		CAST(O.OrganizationID AS VARCHAR(MAX)),
		O.OrganizationID,
		O.ParentOrganizationID,
		1
	FROM Dropdown.Organization O
	WHERE O.ParentOrganizationID = 0
		AND O.OrganizationID > 0

	UNION ALL

	SELECT
		CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY O.OrganizationName) AS VARCHAR(10)), @PadLength)),
		CAST(HD.OrganizationNameLineage  + ' > ' + O.OrganizationName AS VARCHAR(MAX)),
		CAST(HD.OrganizationIDLineage  + ',' + CAST(O.OrganizationID AS VARCHAR(50)) AS VARCHAR(MAX)),
		O.OrganizationID,
		O.ParentOrganizationID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM Dropdown.Organization O
		JOIN HD ON HD.OrganizationID = O.ParentOrganizationID
	)

INSERT INTO Dropdown.OrganizationHierarchy
	(DisplayIndex, OrganizationNameLineage, OrganizationIDLineage, OrganizationID, OrganizationName, ParentOrganizationID, TopLevelOrganizationID, NodeLevel, HasChildren)
SELECT
	HD1.DisplayIndex, 
	HD1.OrganizationNameLineage, 
	HD1.OrganizationIDLineage, 
	HD1.OrganizationID, 
	O.OrganizationName,
	HD1.ParentOrganizationID, 

	CASE
		WHEN HD1.ParentOrganizationID = 0
		THEN HD1.OrganizationID
		ELSE LEFT(HD1.OrganizationIDLineage, CHARINDEX(',', HD1.OrganizationIDLineage) - 1)
	END,

	HD1.NodeLevel, 

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentOrganizationID = HD1.OrganizationID)
		THEN 1
		ELSE 0
	END

FROM HD HD1
	JOIN Dropdown.Organization O ON O.OrganizationID = HD1.OrganizationID
ORDER BY HD1.DisplayIndex
GO		
--End trigger Dropdown.TR_Organization
--End Triggers
