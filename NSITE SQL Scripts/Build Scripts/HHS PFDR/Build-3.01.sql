USE PFDR
GO

--Begin Procedures
--Begin procedure dbo.GetContractEventLogDataByContractID
EXEC Utility.DropObject 'dbo.GetContractEventLogDataByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.11
-- Description:	A stored procedure to get data from the dbo.EventLog table based on a ContractID
--
-- Author:			Todd Pires
-- Modify Date: 2015.01.20
-- Description:	Added the EventLogID
--
-- Author:			Todd Pires
-- Modify Date: 2016.05.09
-- Description:	Changed text
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractEventLogDataByContractID
@ContractID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EL.EventLogID,
		dbo.FormatDateTime(EL.CreateDateTime, 'mm/dd/yyyy hh:mm tt') AS CreateDateTimeFormatted,
		dbo.GetPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
	
		CASE
			WHEN EL.EventCode IN ('Create', 'Update')
			THEN EL.EventCode + 'd contract opportunity'
			WHEN EL.EventCode = 'AssignWorkflow'
			THEN 'Set workflow to step 1'
			WHEN EL.EventCode = 'IncrementWorkflow'
			THEN
				CASE
					WHEN (SELECT WS.WorkflowStatusCode FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusID = N.C.value('(WorkflowStatusID)[1]', 'INT')) = 'Approved'
					THEN 'Workflow complete, contract opportunity approved'
					ELSE 'Set workflow to step ' + N.C.value('(WorkflowStepNumber)[1]', 'VARCHAR')
				END
			ELSE EL.EventCode
		END AS ActionTaken,
	
		EL.Comments
	FROM dbo.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Contract') AS N(C)
	WHERE EL.EntityTypeCode = 'Contract'
		AND EL.EntityID = @ContractID
		AND EL.EventCode <> 'AssignWorkflowPersons'
	ORDER BY EL.EventLogID

END
GO
--End procedure dbo.GetContractEventLogDataByContractID

--Begin procedure Dropdown.GetTargetAwardFiscalYears
EXEC Utility.DropObject 'Dropdown.GetTargetAwardFiscalYears'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.24
-- Description:	A stored procedure to get a list of target award fiscal years from the dbo.Contract table
-- ======================================================================================================
CREATE PROCEDURE Dropdown.GetTargetAwardFiscalYears

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT

		CASE
			WHEN MONTH(C.TargetAwardDate) > 9
			THEN YEAR(C.TargetAwardDate) + 1
			ELSE YEAR(C.TargetAwardDate)
		END AS TargetAwardFiscalYearNumber

	FROM dbo.Contract C
	WHERE C.TargetAwardDate IS NOT NULL
	ORDER BY 1
		
END
GO
--End procedure Dropdown.GetTargetAwardFiscalYears
--End Procedures
