USE PFDR
GO

--Begin Data
UPDATE Dropdown.QuickSearch
SET QuickSearchCriteria = '{"ContractDateEndStop":"EndOfCurrentFiscalYear"}'
WHERE QuickSearchName = 'Contract opportunities expiring at the end of the fiscal year'
GO
--End Data

--Begin Functions
--Begin function Dropdown.ParseQuickSearchCriteria
EXEC Utility.DropObject 'Dropdown.ParseQuickSearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2013.12.10
-- Description:	A function to replace static QuickSearchVariables with their dynamic equivalents
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the ISNULL to the dbo.Person SELECT
-- =============================================================================================

CREATE FUNCTION Dropdown.ParseQuickSearchCriteria
(
@QuickSearchCriteria VARCHAR(MAX),
@PersonID INT = 0
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @EndOfCurrentFiscalYear VARCHAR(10) = '10/01/'

IF MONTH(getDate()) > 9
	SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) + 1 AS CHAR(4))
ELSE	
	SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) AS CHAR(4))
--ENDIF

SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"EndOfCurrentFiscalYear"', '"' + @EndOfCurrentFiscalYear + '"')
SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"PersonID"', '"' + CAST(@PersonID AS VARCHAR) + '"')
SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"PersonOrganizationID"', '"' + CAST(ISNULL((SELECT P.OrganizationID FROM dbo.Person P WHERE P.PersonID = @PersonID), 0) AS VARCHAR) + '"')

RETURN @QuickSearchCriteria
END
GO
--End function Dropdown.ParseQuickSearchCriteria
--End Functions

--Begin Procedures
--Begin procedure dbo.GetContractsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetContractsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on search criteria
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the NULL screen on ContractingOfficeName and ContractName
-- ================================================================================================
CREATE PROCEDURE dbo.GetContractsBySearchCriteria
@CompetitionTypeIDList VARCHAR(MAX) = NULL,
@ContractDateEndStart DATE = NULL,
@ContractDateEndStop DATE = NULL,
@ContractDateStartStart DATE = NULL,
@ContractDateStartStop DATE = NULL,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationIDList VARCHAR(MAX) = NULL,
@ContractName VARCHAR(250) = NULL,
@ContractTypeIDList VARCHAR(MAX) = NULL,
@EstimatedSolicitationQuarterList VARCHAR(MAX) = NULL,
@FiscalYearContractRangeIDList VARCHAR(MAX) = NULL,
@FundingOrganizationIDList VARCHAR(MAX) = NULL,
@Keyword VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'ContractName',
@OrganizationIDList VARCHAR(MAX) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50,
@PendingApprovalPersonID INT = 0,
@PersonID INT = 0,
@PlaceOfPerformanceStateIDList VARCHAR(MAX) = NULL,
@PointOfContactAgency VARCHAR(250) = NULL,
@PointOfContactEmail VARCHAR(320) = NULL,
@PointOfContactName VARCHAR(250) = NULL,
@PointOfContactTitle VARCHAR(50) = NULL,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL,
@RequirementTypeIDList VARCHAR(MAX) = NULL,
@TargetAwardDateStart DATE = NULL,
@TargetAwardDateStop DATE = NULL,
@TargetAwardFiscalYearList VARCHAR(MAX) = NULL,
@TotalContractRangeIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT
	
	DECLARE @tContract AS AccessControl.Contract
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetContractsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetContractsBySearchCriteria
	--ENDIF
		
	CREATE TABLE #tGetContractsBySearchCriteria
		(
		ContractID INT PRIMARY KEY NOT NULL,
		PlaceOfPerformanceCity VARCHAR(300)
		)
	
	IF @Keyword IS NULL
		BEGIN
	
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
	
		--Date Fields
		IF @ContractDateEndStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd < @ContractDateEndStart
	
			END
		--ENDIF
		IF @ContractDateEndStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd >= @ContractDateEndStop
	
			END
		--ENDIF
		IF @ContractDateStartStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart < @ContractDateStartStart
	
			END
		--ENDIF
		IF @ContractDateStartStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart >= @ContractDateStartStop
	
			END
		--ENDIF
		IF @TargetAwardDateStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate < @TargetAwardDateStart
	
			END
		--ENDIF
		IF @TargetAwardDateStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate > @TargetAwardDateStop
	
			END
		--ENDIF
	
		--Dropdown Fields
		IF @CompetitionTypeIDList IS NOT NULL AND LEN(RTRIM(@CompetitionTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@CompetitionTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.CompetitionTypeID
						)
	
			END
		--ENDIF
		IF @ContractingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@ContractingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractingOrganizationID
						)
	
			END
		--ENDIF
		IF @ContractTypeIDList IS NOT NULL AND LEN(RTRIM(@ContractTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractTypeID
						)
	
			END
		--ENDIF
		IF @EstimatedSolicitationQuarterList IS NOT NULL AND LEN(RTRIM(@EstimatedSolicitationQuarterList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@EstimatedSolicitationQuarterList, ',', 0) LTT 
						WHERE LTT.ListItem = C.EstimatedSolicitationQuarter
						)
	
			END
		--ENDIF
		IF @FiscalYearContractRangeIDList IS NOT NULL AND LEN(RTRIM(@FiscalYearContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FiscalYearContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FiscalYearContractRangeID
						)
	
			END
		--ENDIF
		IF @FundingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@FundingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FundingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FundingOrganizationID
						)
	
			END
		--ENDIF
		IF @NAICSID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.NAICSID <> @NAICSID
	
			END
		--ENDIF
		IF @OrganizationIDList IS NOT NULL AND LEN(RTRIM(@OrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@OrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.OriginatingOrganizationID
						)
	
			END
		--ENDIF
		IF @PlaceOfPerformanceStateIDList IS NOT NULL AND LEN(RTRIM(@PlaceOfPerformanceStateIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@PlaceOfPerformanceStateIDList, ',', 0) LTT 
							JOIN dbo.ContractPlaceOfPerformance CPOP ON CPOP.PlaceOfPerformanceStateID = LTT.ListItem
								AND CPOP.ContractID = C.ContractID
						)
	
			END
		--ENDIF
		IF @PSCID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.PSCID <> @PSCID
	
			END
		--ENDIF
		IF @RequirementTypeIDList IS NOT NULL AND LEN(RTRIM(@RequirementTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@RequirementTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.RequirementTypeID
						)
	
			END
		--ENDIF
		IF @TargetAwardFiscalYearList IS NOT NULL AND LEN(RTRIM(@TargetAwardFiscalYearList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TargetAwardFiscalYearList, ',', 0) LTT 
						WHERE LTT.ListItem = dbo.GetFiscalYearFromDate(C.TargetAwardDate)
						)
	
			END
		--ENDIF
		IF @TotalContractRangeIDList IS NOT NULL AND LEN(RTRIM(@TotalContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TotalContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.TotalContractRangeID
						)
	
			END
		--ENDIF
	
		-- Text Fields (individual)
		IF @ContractingOfficeName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractingOfficeName NOT LIKE '%' + @ContractingOfficeName + '%'
							OR C.ContractingOfficeName IS NULL
						)					
	
			END
		--ENDIF
		IF @ContractName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractName NOT LIKE '%' + @ContractName + '%'
							OR C.ContractName IS NULL
						)
					AND 
						(
						C.ContractDescription NOT LIKE '%' + @ContractName + '%'
							OR C.ContractDescription IS NULL
						)
	
			END
		--ENDIF
		IF @PointOfContactAgency IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactAgency LIKE '%' + @PointOfContactAgency + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactEmail IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactEmail LIKE '%' + @PointOfContactEmail + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactName LIKE '%' + @PointOfContactName + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactTitle IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactTitle LIKE '%' + @PointOfContactTitle + '%'
						)
	
			END
		--ENDIF
		IF @ReferenceIDV IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ReferenceIDV NOT LIKE '%' + @ReferenceIDV + '%'
	
			END
		--ENDIF
	
		-- Workflow field
		IF @PendingApprovalPersonID IS NOT NULL AND @PendingApprovalPersonID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractWorkflowPerson CWP
						WHERE CWP.ContractID = C.ContractID
							AND CWP.WorkflowStepNumber = C.WorkflowStepNumber
							AND CWP.PersonID = @PendingApprovalPersonID
						)
	
			END
		--ENDIF
	
		END
	ELSE
		BEGIN
	
		SET @Keyword = '%' + @Keyword + '%'

		-- Text Fields (multiple)
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND
					(
					C.ContractingOfficeName LIKE @Keyword
						OR C.ContractName LIKE @Keyword
						OR C.ContractDescription LIKE @Keyword
						OR C.IncumbentContractorName LIKE @Keyword
						OR C.ReferenceIDV LIKE @Keyword
						OR C.TransactionNumber LIKE @Keyword
						OR EXISTS
							(
							SELECT 1
							FROM dbo.ContractPointOfContact CPOC
							WHERE CPOC.ContractID = C.ContractID
								AND 
									(
									CPOC.PointOfContactEmail LIKE @Keyword
										OR CPOC.PointOfContactName LIKE @Keyword
										OR CPOC.PointOfContactPhone LIKE @Keyword
									)
							)
/*
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.CompetitionType CT
							WHERE CT.CompetitionTypeID = C.CompetitionTypeID
								AND CT.CompetitionTypeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.ContractRange CR
							WHERE 
								(
								CR.ContractRangeID = C.FiscalYearContractRangeID
									OR CR.ContractRangeID = C.TotalContractRangeID
								)
								AND CR.ContractRangeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.ContractType CT
							WHERE CT.ContractTypeID = C.ContractTypeID
								AND CT.ContractTypeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.NAICS N 
							WHERE N.NAICSID = C.NAICSID
								AND
									(
									N.NAICSCode LIKE @Keyword
										OR N.NAICSName LIKE @Keyword
									)
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.Organization O
							WHERE 
								(
								O.OrganizationID = C.ContractingOrganizationID
									OR O.OrganizationID = C.FundingOrganizationID
									OR O.OrganizationID = C.OriginatingOrganizationID
								)
								AND O.OrganizationName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.RequirementType RT 
							WHERE RT.RequirementTypeID = C.RequirementTypeID
								AND RT.RequirementTypeName LIKE @Keyword
							)
*/
						OR EXISTS
							(
							SELECT 1 
							FROM dbo.ContractPlaceOfPerformance CPOP 
								JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
									AND CPOP.ContractID = C.ContractID
									AND 
										(
										CPOP.PlaceOfPerformanceCity LIKE @Keyword
											OR S.StateCode LIKE @Keyword
											OR S.StateName LIKE @Keyword
										)
							)
					)
	
		END
	--ENDIF
	
	SELECT 
		@TotalRecordCount = COUNT(T1.ContractID)			
	FROM #tGetContractsBySearchCriteria T1
	
	IF @OrderByField = 'PlaceOfPerformanceCity'
		BEGIN
	
		UPDATE T1
		SET 
			T1.PlaceOfPerformanceCity = 
				CASE
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
					THEN 'Multiple'
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
					THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
					ELSE ''
				END
	
		FROM #tGetContractsBySearchCriteria T1
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
	
		END
	--ENDIF
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/
	
	IF @PageSize > 0
		BEGIN
	
		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)
	
		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
						CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
							T1.ContractID ASC
					) AS RowIndex,
				C.ContractID
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
				JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
				JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
				JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID				
			)
	
		DELETE T1
		FROM #tGetContractsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.ContractID = T1.ContractID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)
	
		END
	--ENDIF
	
	INSERT INTO @tContract (ContractID) SELECT T1.ContractID FROM #tGetContractsBySearchCriteria T1
	
	SELECT
		@PageCount AS PageCount,
		@PageIndex AS PageIndex,
		@TotalRecordCount AS TotalRecordCount,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		C.CompetitionTypeID, 
		LEFT(C.ContractDescription, 50) + 
			CASE
				WHEN LEN(RTRIM(C.ContractDescription)) > 50
				THEN '...' 
				ELSE ''
			END AS ContractDescription, 
				
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.IncumbentContractorName, 
		C.NAICSID, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TotalContractRangeID,
		C.ContractDateEnd, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		RT.RequirementTypeName,
	
		CASE 
			WHEN @OrderByField = 'PlaceOfPerformanceCity'
			THEN T1.PlaceOfPerformanceCity
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
			THEN 'Multiple'
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
			THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
			ELSE ''
		END AS PlaceOfPerformanceCity
	
	FROM #tGetContractsBySearchCriteria T1
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
		JOIN dbo.Contract C ON C.ContractID = T1.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
	ORDER BY 
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
		CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
		CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
		CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
			T1.ContractID ASC
	
	DROP TABLE #tGetContractsBySearchCriteria
	
END
GO
--End procedure dbo.GetContractsBySearchCriteria

--Begin procedure dbo.SavePerson
EXEC Utility.DropObject 'dbo.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Person table
--
-- Author:			Todd Pires
-- Update Date: 2014.08.01
-- Description:	Added the check for duplicate email addresses on insert
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the LTRIM() to the VARCHAR params
-- ====================================================================
CREATE PROCEDURE dbo.SavePerson
@EmailAddress VARCHAR(320) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsSuperAdministrator BIT = NULL,
@LastName VARCHAR(50) = NULL,
@OrganizationID INT = 0,
@OrganizationRoleIDList VARCHAR(MAX) = NULL,
@Phone VARCHAR(50) = NULL,
@SourcePersonID INT = 0,
@TargetPersonID INT = 0,
@Title VARCHAR(50) = NULL,
@UserName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)

	SET @EmailAddress = LTRIM(@EmailAddress)
	SET @FirstName = LTRIM(@FirstName)
	SET @LastName = LTRIM(@LastName)
	SET @Phone = LTRIM(@Phone)
	SET @Title = LTRIM(@Title)
	SET @UserName = LTRIM(@UserName)
	
	IF @TargetPersonID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput1 TABLE (PersonID INT)
		
		IF NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.EmailAddress = @EmailAddress)
			BEGIN
			
			INSERT INTO dbo.Person
				(
				EmailAddress,
				FirstName,
				IsActive,
				LastName,
				OrganizationID,
				Phone,
				Title,
				UserName
				)
			OUTPUT INSERTED.PersonID INTO @tOutput1
			VALUES
				(
				@EmailAddress,
				@FirstName,
				@IsActive,
				@LastName,
				@OrganizationID,
				@Phone,
				@Title,
				@UserName
				)
	
			SELECT @TargetPersonID = O.PersonID 
			FROM @tOutput1 O

			END
		--ENDIF

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Person
		SET
			EmailAddress = @EmailAddress,
			FirstName = @FirstName,
			IsActive = @IsActive,
			LastName = @LastName,
			OrganizationID = @OrganizationID,
			Phone = @Phone,
			Title = @Title,
			UserName = @UserName
		WHERE PersonID = @TargetPersonID
	
		END
	--ENDIF
	
	IF @TargetPersonID > 0
		BEGIN
		
		DECLARE @tOutput2 TABLE 
			(
			RoleID INT NOT NULL DEFAULT 0,
			OrganizationID INT NOT NULL DEFAULT 0,
			Mode VARCHAR(10)
			)
	
		IF @IsSuperAdministrator = 1
			BEGIN
	
			DELETE PR
			OUTPUT DELETED.RoleID, 0, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM Dropdown.Role R
					WHERE R.RoleCode = 'SuperAdministrator'
						AND R.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, RoleID)
			OUTPUT INSERTED.RoleID, 0, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				R.RoleID
			FROM Dropdown.Role R
			WHERE R.RoleCode = 'SuperAdministrator'
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.RoleID = R.RoleID
					)
			
			END
		ELSE
			BEGIN
			
			DECLARE @tPersonRole TABLE 
				(
				OrganizationID INT NOT NULL DEFAULT 0, 
				RoleID INT NOT NULL DEFAULT 0
				)
	
			INSERT INTO @tPersonRole
				(OrganizationID,RoleID)
			SELECT
				LEFT(LTT.ListItem, CHARINDEX('_', LTT.ListItem) - 1),
				RIGHT(LTT.ListItem, LEN(LTT.ListItem) - CHARINDEX('_', LTT.ListItem))
			FROM Utility.ListToTable(@OrganizationRoleIDList, ',', 0) LTT 
	
			DELETE PR
			OUTPUT DELETED.RoleID, DELETED.OrganizationID, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tPersonRole TPR
					WHERE TPR.OrganizationID = PR.OrganizationID
						AND TPR.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, OrganizationID, RoleID)
			OUTPUT INSERTED.RoleID, INSERTED.OrganizationID, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				TPR.OrganizationID,
				TPR.RoleID
			FROM @tPersonRole TPR
			WHERE NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.OrganizationID = TPR.OrganizationID
						AND PR.RoleID = TPR.RoleID
					)
			
			END
		--ENDIF

		EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = @EventCode, @SourcePersonID = @SourcePersonID
		
		END
	--ENDIF

END
GO
--End procedure dbo.SavePerson
--End Procedures
