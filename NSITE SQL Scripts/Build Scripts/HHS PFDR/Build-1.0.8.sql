USE PFDR
GO

--Begin Tables
--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC Utility.DropColumn @TableName, 'ContractITDescription'
EXEC Utility.DropColumn @TableName, 'IsITContract'
EXEC Utility.DropColumn @TableName, 'IsITForNewRequirement'
EXEC Utility.DropColumn @TableName, 'ITCategoryID'
EXEC Utility.DropColumn @TableName, 'ITInvestmentIdentifier'
EXEC Utility.DropColumn @TableName, 'ITInvestmentType'
EXEC Utility.DropColumn @TableName, 'ITSubcategoryID'
GO
--End table dbo.Contract

--Begin table Dropdown.ITCategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITCategory'

EXEC Utility.DropObject @TableName
GO
--End table Dropdown.ITCategory

--Begin table Dropdown.ITSubcategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITSubcategory'

EXEC Utility.DropObject @TableName
GO
--End table Dropdown.ITSubcategory

--Begin table Dropdown.ITCategoryITSubcategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITCategoryITSubcategory'

EXEC Utility.DropObject @TableName
GO
--End table Dropdown.ITCategoryITSubcategory
--End Tables

--Begin Procedures
--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Create Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		C.ContractDateEnd,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.SaveContract
EXEC Utility.DropObject 'dbo.SaveContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Contract table based on a ContractID
-- ============================================================================================
CREATE PROCEDURE dbo.SaveContract
@ActiveContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@ActiveContractPointOfContactIDList VARCHAR(MAX) = '',
@CompetitionTypeID INT = 0,
@ContractDateEnd DATE = NULL,
@ContractDateStart DATE = NULL,
@ContractDescription VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationID INT = 0, 
@ContractName VARCHAR(250) = NULL,
@ContractTypeID INT = 0,
@DeletedContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@DeletedContractPointOfContactIDList VARCHAR(MAX) = '',
@EstimatedSolicitationQuarter INT = 0,
@FiscalYearContractRangeID INT = 0,
@FundingOrganizationID INT = 0, 
@IncumbentContractorName VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OriginatingOrganizationID INT = 0, 
@PersonID INT = 0,
@PrimaryContractPlaceOfPerformanceID INT = 0,
@PrimaryContractPointOfContactID INT = 0,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL, 
@RequirementTypeID INT = 0,
@TargetAwardDate DATE = NULL,
@TotalContractRangeID INT = 0,
@TransactionNumber VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @ContractID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput TABLE (ContractID INT)

		INSERT INTO dbo.Contract
			(
			CompetitionTypeID,
			ContractDateEnd,
			ContractDateStart,
			ContractDescription,
			ContractingOfficeName,
			ContractingOrganizationID,
			ContractName,
			ContractTypeID,
			EstimatedSolicitationQuarter,
			FiscalYearContractRangeID,
			FundingOrganizationID,
			IncumbentContractorName,
			NAICSID,
			OriginatingOrganizationID,
			PSCID,
			ReferenceIDV,
			RequirementTypeID,
			TargetAwardDate,
			TotalContractRangeID,
			TransactionNumber
			)
		OUTPUT INSERTED.ContractID INTO @tOutput
		VALUES
			(
			@CompetitionTypeID,
			@ContractDateEnd,
			@ContractDateStart,
			@ContractDescription,
			@ContractingOfficeName,
			@ContractingOrganizationID,
			@ContractName,
			@ContractTypeID,
			@EstimatedSolicitationQuarter,
			@FiscalYearContractRangeID,
			@FundingOrganizationID,
			@IncumbentContractorName,
			@NAICSID,
			@OriginatingOrganizationID,
			@PSCID,
			@ReferenceIDV,
			@RequirementTypeID,
			@TargetAwardDate,
			@TotalContractRangeID,
			@TransactionNumber
			)
		
		SELECT @ContractID = O.ContractID 
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Contract
		SET
			CompetitionTypeID = @CompetitionTypeID,
			ContractDateEnd = @ContractDateEnd,
			ContractDateStart = @ContractDateStart,
			ContractDescription = @ContractDescription,
			ContractingOfficeName = @ContractingOfficeName,
			ContractingOrganizationID = @ContractingOrganizationID,
			ContractName = @ContractName,
			ContractTypeID = @ContractTypeID,
			EstimatedSolicitationQuarter = @EstimatedSolicitationQuarter,
			FiscalYearContractRangeID = @FiscalYearContractRangeID,
			FundingOrganizationID = @FundingOrganizationID,
			IncumbentContractorName = @IncumbentContractorName,
			NAICSID = @NAICSID,
			OriginatingOrganizationID = @OriginatingOrganizationID,
			PSCID = @PSCID,
			ReferenceIDV = @ReferenceIDV,
			RequirementTypeID = @RequirementTypeID,
			TargetAwardDate = @TargetAwardDate,
			TotalContractRangeID = @TotalContractRangeID,
			TransactionNumber = @TransactionNumber
		WHERE ContractID = @ContractID
		
		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPlaceOfPerformanceIDList)) > 0
		BEGIN
	
		UPDATE CPOP
		SET 
			CPOP.ContractID = @ContractID,
			CPOP.IsPrimary = 
				CASE
					WHEN CPOP.ContractPlaceOfPerformanceID = @PrimaryContractPlaceOfPerformanceID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@ActiveContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPointOfContactIDList)) > 0
		BEGIN
	
		UPDATE CPOC
		SET 
			CPOC.ContractID = @ContractID,
			CPOC.IsPrimary = 
				CASE
					WHEN CPOC.ContractPointOfContactID = @PrimaryContractPointOfContactID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@ActiveContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPlaceOfPerformanceIDList)) > 0
		BEGIN
		
		DELETE CPOP
		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@DeletedContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPointOfContactIDList)) > 0
		BEGIN
		
		DELETE CPOC
		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@DeletedContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	EXEC dbo.AddEventLogContractEntry @ContractID = @ContractID, @EventCode = @EventCode, @PersonID = @PersonID
	EXEC dbo.GetContractByContractID @ContractID = @ContractID, @PersonID = @PersonID

END
GO
--End procedure dbo.SaveContract

--Begin procedure Dropdown.GetITCategories
EXEC Utility.DropObject 'Dropdown.GetITCategories'
GO
--End procedure Dropdown.GetITCategories

--Begin procedure Dropdown.GetITSubcategoriesByITCategoryID
EXEC Utility.DropObject 'Dropdown.GetITSubcategoriesByITCategoryID'
GO
--End procedure Dropdown.GetITSubcategoriesByITCategoryID
--End Procedures
