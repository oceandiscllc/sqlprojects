USE PFDR
GO

--Begin Schemas
--Begin schema BulkData
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'BulkData')
	BEGIN
	
	DECLARE @SQL VARCHAR(MAX) = 'CREATE SCHEMA BulkData'
	
	EXEC (@SQL)
	
	END
--ENDIF
GO
--End schema BulkData

--Begin Tables
--Begin table BulkData.BulkDataImport
DECLARE @TableName VARCHAR(250) = 'BulkData.BulkDataImport'

EXEC Utility.DropObject @TableName

CREATE TABLE BulkData.BulkDataImport
	(
	BulkDataImportID INT IDENTITY(1,1) NOT NULL,
	OriginatingOrganizationName VARCHAR(250),
	ContractingOfficeName VARCHAR(250),
	ContractName VARCHAR(250),
	ContractDescription VARCHAR(MAX),
	TargetAwardDate DATE,
	ContractDateStart DATE,
	ContractDateEnd DATE,
	IncumbentContractorName VARCHAR(250),
	ReferenceIDV VARCHAR(250),
	TransactionNumber VARCHAR(100),
	CompetitionType VARCHAR(250),
	FiscalYearContractRange VARCHAR(250),
	TotalContractRange VARCHAR(250),
	ContractType VARCHAR(250),
	EstimatedSolicitationQuarter INT,
	NAICSCode VARCHAR(250),
	ContractingOrganization VARCHAR(250),
	FundingOrganization VARCHAR(250),
	RequirementType VARCHAR(250),
	PlaceOfPerformanceCity VARCHAR(250),
	PlaceOfPerformanceStateCode VARCHAR(50),
	PointOfContactName VARCHAR(250),
	PointOfContactEmail VARCHAR(320),
	PointOfContactPhone VARCHAR(50),
	PointOfContactAgency VARCHAR(250),
	PointOfContactTitle VARCHAR(50),
	CompetitionTypeID INT,
	ContractingOrganizationID INT,
	ContractTypeID INT,
	FiscalYearContractRangeID INT,
	FundingOrganizationID INT,
	NAICSID INT,
	OriginatingOrganizationID INT,
	PlaceOfPerformanceStateID INT,
	RequirementTypeID INT,
	TotalContractRangeID INT,
	HasPlaceOfPerformance BIT,
	HasPointOfContact BIT
	)

EXEC Utility.DropConstraintsAndIndexes @TableName

EXEC Utility.SetDefaultConstraint @TableName, 'CompetitionTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ContractingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ContractTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'FiscalYearContractRangeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'FundingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'NAICSID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'OriginatingOrganizationID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PlaceOfPerformanceStateID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'RequirementTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'TotalContractRangeID', 'INT', 0

EXEC Utility.SetPrimaryKeyClustered @TableName, 'BulkDataImportID'
GO
--End table BulkData.BulkDataImport

--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC Utility.AddColumn @TableName, 'BulkDataImportID', 'INT'
EXEC Utility.SetDefaultConstraint @TableName, 'BulkDataImportID', 'INT', 0
GO
--End table dbo.Contract

--Begin procedure dbo.GetContractEventLogDataByContractID
EXEC Utility.DropObject 'dbo.GetContractEventLogDataByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.11
-- Description:	A stored procedure to get data from the dbo.EventLog table based on a ContractID
--
-- Author:			Todd Pires
-- Modify Date: 2015.01.20
-- Description:	Added the EventLogID
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractEventLogDataByContractID
@ContractID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EL.EventLogID,
		dbo.FormatDateTime(EL.CreateDateTime, 'mm/dd/yyyy hh:mm tt') AS CreateDateTimeFormatted,
		dbo.GetPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
	
		CASE
			WHEN EL.EventCode IN ('Create', 'Update')
			THEN EL.EventCode + 'd contract'
			WHEN EL.EventCode = 'AssignWorkflow'
			THEN 'Set workflow to step 1'
			WHEN EL.EventCode = 'IncrementWorkflow'
			THEN
				CASE
					WHEN (SELECT WS.WorkflowStatusCode FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusID = N.C.value('(WorkflowStatusID)[1]', 'INT')) = 'Approved'
					THEN 'Workflow complete, contract approved'
					ELSE 'Set workflow to step ' + N.C.value('(WorkflowStepNumber)[1]', 'VARCHAR')
				END
			ELSE EL.EventCode
		END AS ActionTaken,
	
		EL.Comments
	FROM dbo.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Contract') AS N(C)
	WHERE EL.EntityTypeCode = 'Contract'
		AND EL.EntityID = @ContractID
		AND EL.EventCode <> 'AssignWorkflowPersons'
	ORDER BY EL.EventLogID

END
GO
--End procedure dbo.GetContractEventLogDataByContractID

--Begin procedure dbo.GetContractPlacesOfPerformanceByEventLogID
EXEC Utility.DropObject 'dbo.GetContractPlacesOfPerformanceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.01.20
-- Description:	A stored procedure to get contract place of performance data from the dbo.EventLog table based on an EventLogID
-- ============================================================================================================================
CREATE PROCEDURE dbo.GetContractPlacesOfPerformanceByEventLogID
@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @oContract XML = (SELECT EL.EventData FROM dbo.EventLog EL WHERE EL.EventLogID = @EventLogID)
	DECLARE @oContractPlacesOfPerformance XML = @oContract.query('/Contract/ContractPlacesOfPerformance') 

	DECLARE @tContractPlaceOfPerformance TABLE
		(
		ContractPlaceOfPerformanceID int NOT NULL PRIMARY KEY,
		ContractID int NOT NULL DEFAULT 0,
		PlaceOfPerformanceCity varchar(250) NULL,
		PlaceOfPerformanceStateID int NOT NULL DEFAULT 0,
		IsPrimary bit NOT NULL DEFAULT 0
		)

	INSERT INTO @tContractPlaceOfPerformance
		(ContractID,ContractPlaceOfPerformanceID,IsPrimary,PlaceOfPerformanceCity,PlaceOfPerformanceStateID)
	SELECT
		T.Col.value('ContractID[1]', 'int'),
		T.Col.value('ContractPlaceOfPerformanceID[1]', 'int'),
		T.Col.value('IsPrimary[1]', 'bit'),
		T.Col.value('PlaceOfPerformanceCity[1]', 'varchar(250)'),
		T.Col.value('PlaceOfPerformanceStateID[1]', 'int')
	FROM @oContractPlacesOfPerformance.nodes('//ContractPlaceOfPerformance') T(Col)
	
	SELECT
		CPOP.ContractPlaceOfPerformanceID, 
		CPOP.PlaceOfPerformanceCity, 
		CPOP.IsPrimary,
		S.StateCode
	FROM @tContractPlaceOfPerformance CPOP
		JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
	ORDER BY CPOP.IsPrimary DESC, CPOP.PlaceOfPerformanceCity, S.StateCode

END
GO
--End procedure dbo.GetContractPlacesOfPerformanceByEventLogID

INSERT BulkData.BulkDataImport 
	(OriginatingOrganizationName, ContractingOfficeName, ContractName, ContractDescription, TargetAwardDate, ContractDateStart, ContractDateEnd, IncumbentContractorName, ReferenceIDV, TransactionNumber, CompetitionType, FiscalYearContractRange, TotalContractRange, ContractType, EstimatedSolicitationQuarter, NAICSCode, ContractingOrganization, FundingOrganization, RequirementType, PlaceOfPerformanceCity, PlaceOfPerformanceStateCode, PointOfContactName, PointOfContactEmail, PointOfContactPhone, PointOfContactAgency, PointOfContactTitle) 
VALUES 
	('ASPR', 'Office of the Secretary', 'Medical Countermeasures for Pandemic Influenza Preparedness and Response', 'Supply of the following items and/or services: cGMP Influenza Vaccine Master and Working Seed Lot(s), Influenza Vaccine research Lot(s), cGMP Influenza Vaccine Investigational Lot(s), cGMP Influenza Vaccine Commercial Scale Bulk Lot(sFormulation and Filling (Antigen: Single dose Vials, Antigen: Syringes or sprayers Antigen: Multi-dose vials, Adjuvant: Single dose Vials, Adjuvant: Multi-dose vials Co-formulated antigen and adjuvant: Syringes, Co-formulated antigen and adjuvant: multi-dose vials), Storage and Stability (Storage of investigational lots of antigen Storage of commercial scale bulk lots of antigen, Storage of antigen in final container Storage of clinical/commercial scale lots of adjuvant, Storage of adjuvant in final container, Storage of licensed product in final container), Storage of licensed product in final container), Shipping, Candidate Vaccine Virus (CVV), Potency Reagent and standards manufacture and testing, Analytical Laboratory Testing/Assay, Non-clinical studies, Clinical studies, Disposal of product BARDA Tracking Tool Development and Testing of New Standard Data Reporting formats, and Additional Reporting', CAST('2015-07-01' AS Date), CAST('2015-08-01' AS Date), CAST('2020-08-01' AS Date), NULL, NULL, NULL, N'Full and Open', '>= $20M and < $50M', '> $100M', 'Delivery Order', 2, NULL, N'ASPR', 'ASPR', 'Recompete', 'TBD', NULL, N'Lynda Brown', 'lynda.brown@hhs.gov', '202.692.4760', NULL, NULL),
	('CMS', 'AGG/DBSC', 'General Market Exchange Open Enrollment Campaign', 'General Market Exchange Open Enrollment Campaign', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Weber Shandwick', NULL, N'150612', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'General Market Medicare Open Enrollment Campaign', 'General Market Medicare Open Enrollment Campaign', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Porter Novelli', NULL, N'150600', 'Competitive Task/Delivery Order', '>= $5M and < $10M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'CHIPRA Education and Outreach', 'CHIPRA Education and Outreach', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Fleishman Hillard', NULL, N'150623', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Fraud Outreach and Education Campaign', 'Fraud Outreach and Education Campaign', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Porter Novelli Public Services', NULL, N'150615', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Beneficiary and Provider Assessment Research', 'Beneficiary and Provider Assessment Research', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Salter Mitchell', NULL, N'150622', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Provider Outreach and Education (Medscape)', 'Provider Outreach and Education (Medscape)', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'WebMD Health Corp.', NULL, N'150610', 'Competitive Task/Delivery Order', '>= $500K and < $1M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Research to Support/Inform Communication Campaigns', 'Research to Support/Inform Communication Campaigns', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'L&M Policy Research', NULL, N'150607', 'Competitive Task/Delivery Order', '>= $500K and < $1M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Generational Research', 'Generational Research', CAST('2015-04-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'Aeffect', NULL, N'150602', 'Competitive Task/Delivery Order', '>= $500K and < $1M', '>= $500K and < $1M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Communications planning technical support', 'Communications planning technical support', NULL, NULL, NULL, N'N/A', NULL, N'150599', 'New Competitive Contract', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410.786.1029', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Camera Upgrade', 'Camera Upgrade', CAST('2015-06-15' AS Date), CAST('2014-12-01' AS Date), NULL, N'N/A', NULL, N'150624', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410-786-5656', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Administrative Simplification, Electronic Data Exchange Pilot', 'Administrative Simplification, Electronic Data Exchange Pilot', CAST('2015-05-30' AS Date), CAST('2015-01-01' AS Date), NULL, N'TBD', NULL, N'150627', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-8797', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Quality Measures Development Contract', 'Quality Measures Development Contract', CAST('2015-03-01' AS Date), CAST('2014-11-01' AS Date), NULL, N'N/A', NULL, N'150581', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-0437', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Beneficiary Focus Testing', 'Beneficiary Focus Testing', CAST('2015-06-15' AS Date), CAST('2015-02-01' AS Date), NULL, N'N/A', NULL, N'150584', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-8045', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Analytics Contract', 'Analytics Contract', CAST('2015-07-15' AS Date), CAST('2015-03-01' AS Date), NULL, N'N/A', NULL, N'150582', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-6696', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Commuincations with Medicare-Medicaid Enrollees', 'Commuincations with Medicare-Medicaid Enrollees', CAST('2015-06-15' AS Date), CAST('2015-02-01' AS Date), NULL, N'N/A', NULL, N'150583', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-8045', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Medicare Current Beneficiary Survey (MCBS)', 'Medicare Current Beneficiary Survey (MCBS)', NULL, CAST('2015-01-01' AS Date), NULL, N'National Opinion Research Center (NORC)', NULL, N'150695', 'New Competitive Contract', '>= $20M and < $50M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Heather Robertson', NULL, N'410-786-7927', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'ICD-10 Competition', 'ICD-10 Competition', CAST('2015-01-31' AS Date), CAST('2014-11-01' AS Date), NULL, N'NOBLIS', NULL, N'150257', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Debbie Lester', NULL, N'410-786-8797', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DBSC', 'Research Data Assistance Center (ResDAC)', 'Research Data Assistance Center (ResDAC)', CAST('2015-09-01' AS Date), CAST('2014-11-01' AS Date), NULL, N'University of Minnesota', NULL, N'150578', 'New Competitive Contract', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'TBD', NULL, N'410-786-7055', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Hospital Engagement Networks - 2', 'Hospital Engagement Networks - 2', CAST('2015-06-01' AS Date), NULL, NULL, N'N/A', NULL, N'150491', 'New Contract - Competitive', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Rich Asher', NULL, N'410-786-6871', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'TCPI Data Support and Feedback Reporting', 'TCPI Data Support and Feedback Reporting', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150536', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'TCPI Content Development and Diffusion', 'TCPI Content Development and Diffusion', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150537', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'TCPI Evaluation Measurement and Monitoring', 'TCPI Evaluation Measurement and Monitoring', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150539', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Survey and Cert National Training Program', 'Survey and Cert National Training Program', CAST('2015-06-01' AS Date), NULL, NULL, N'N/A', NULL, N'150518', '8(a) Competitive', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-2890', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Home Health Agency Survey Oversight', 'Home Health Agency Survey Oversight', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150515', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP MACBIS and IAP Data Analytics', 'IAP MACBIS and IAP Data Analytics', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150542', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP Development of Medicaid Chronic Care and IAP Priority Area Metric Sets', 'IAP Development of Medicaid Chronic Care and IAP Priority Area Metric Sets', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150544', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'CMCS Data Analytics', 'CMCS Data Analytics', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150546', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'SGR Extender, PL 113-93 Community Mental Health', 'SGR Extender, PL 113-93 Community Mental Health', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150561', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP Medicaid Metrics Development in Program Priority Areas', 'IAP Medicaid Metrics Development in Program Priority Areas', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150563', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP Medicaid Service Delivery and Payment Models', 'IAP Medicaid Service Delivery and Payment Models', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150565', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP Rapid Cycle Evaluation and Technical Assistance', 'IAP Rapid Cycle Evaluation and Technical Assistance', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150568', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'IAP Rapid Cycle Quality Improvement', 'IAP Rapid Cycle Quality Improvement', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150570', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'DSW Technical Assistance', 'DSW Technical Assistance', CAST('2015-08-01' AS Date), NULL, NULL, N'N/A', NULL, N'150258', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-0278', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Housing Technical Assistance', 'Housing Technical Assistance', CAST('2015-09-15' AS Date), NULL, NULL, N'N/A', NULL, N'150311', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-5933', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Behavioral Health Clinic Demonstration', 'Behavioral Health Clinic Demonstration', CAST('2014-12-15' AS Date), NULL, NULL, N'N/A', NULL, N'150335', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-0292', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Strong Start: Mothers and Newborns S2 - ', 'Strong Start: Mothers and Newborns S2 - ', CAST('2014-12-30' AS Date), NULL, NULL, N'N/A', NULL, N'150223', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-0360', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Comprehensive ESRD Care - CEC Evaluation', 'Comprehensive ESRD Care - CEC Evaluation', CAST('2015-06-01' AS Date), NULL, NULL, N'N/A', NULL, N'150249', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-0263', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Million Hears', 'Million Hears', CAST('2015-08-30' AS Date), NULL, NULL, N'N/A', NULL, N'150300', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-6103', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Bundled Approaches Design of Future Models 5', 'Bundled Approaches Design of Future Models 5', CAST('2015-07-15' AS Date), NULL, NULL, N'N/A', NULL, N'150310', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'N/A', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Comprehensive ESRD Care - CEC Learning System', 'Comprehensive ESRD Care - CEC Learning System', CAST('2015-09-01' AS Date), NULL, NULL, N'N/A', NULL, N'150312', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-5794', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'ASA Sec 5509 Graduate Nurse Education', 'ASA Sec 5509 Graduate Nurse Education', CAST('2015-05-01' AS Date), NULL, NULL, N'N/A', NULL, N'150322', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-8625', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Frontier Community Health Integration', 'Frontier Community Health Integration', CAST('2015-09-01' AS Date), NULL, NULL, N'N/A', NULL, N'150323', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-6678', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'ACO Investment Model Application Review', 'ACO Investment Model Application Review', CAST('2014-12-15' AS Date), NULL, NULL, N'N/A', NULL, N'150482', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-6756', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'FCHIP Implementation', 'FCHIP Implementation', CAST('2014-02-15' AS Date), NULL, NULL, N'N/A', NULL, N'150483', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Lou Anderson', NULL, N'410-786-3331', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'HCIA 2 Evaluation', 'HCIA 2 Evaluation', CAST('2014-12-01' AS Date), NULL, NULL, N'N/A', NULL, N'150485', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-4933', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Million Hearts Evaluation', 'Million Hearts Evaluation', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150535', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'For Profit PACE possible extension of evaluation', 'For Profit PACE possible extension of evaluation', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150541', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Pipeline Actuarial Support - Portfolio Simulation', 'Pipeline Actuarial Support - Portfolio Simulation', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150545', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Pipeline Research', 'Pipeline Research', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150548', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Market Research', 'Market Research', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150549', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'FCHIP Implementation 2', 'FCHIP Implementation 2', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150550', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Quality Measures', 'Quality Measures', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150551', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Prior Authorization Ambulance Evaluation', 'Prior Authorization Ambulance Evaluation', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150552', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Prior Authorization HBO: Evaluation', 'Prior Authorization HBO: Evaluation', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150554', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Population Health Measuring', 'Population Health Measuring', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150557', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'FCHIP Evaluarion', 'FCHIP Evaluarion', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150559', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'BPCI - payment reconciliation and data provision', 'BPCI - payment reconciliation and data provision', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150560', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'CMMI Learning System', 'CMMI Learning System', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150564', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'FCHIP Audits', 'FCHIP Audits', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150566', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Pipeline Actuarial Support-Financial / Actuarial Analysis and Design of Demo', 'Pipeline Actuarial Support-Financial / Actuarial Analysis and Design of Demo', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150569', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Pipeline Actuarial Support-Scaling Analysis and support for decision making', 'Pipeline Actuarial Support-Scaling Analysis and support for decision making', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150571', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Centralized Contracting Support', 'Centralized Contracting Support', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150556', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Evaluation of the Impact of the CEC Model', 'Evaluation of the Impact of the CEC Model', CAST('2015-01-02' AS Date), NULL, NULL, N'N/A', NULL, N'150466', 'Competitive Task/Delivery Order', NULL, NULL, N'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'410-786-0263', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'MCCM Evaluation', 'MCCM Evaluation', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150553', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'For the processing of the second round of CEC applications', 'For the processing of the second round of CEC applications', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150538', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'CPC Analytical Contractor', 'CPC Analytical Contractor', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150543', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'CPC CareSourced', 'CPC CareSourced', CAST('2015-04-01' AS Date), NULL, NULL, N'N/A', NULL, N'150555', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'Application Review Process Support', 'Application Review Process Support', CAST('2015-01-01' AS Date), NULL, NULL, N'N/A', NULL, N'150558', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'AGG/DSC', 'CPC Design and Operations', 'CPC Design and Operations', CAST('2015-08-31' AS Date), NULL, NULL, N'N/A', NULL, N'150562', 'Competitive Task/Delivery Order', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'John Cruse', NULL, N'TBD', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Operation Analytics and Fraud Surveillance', 'Operation Analytics and Fraud Surveillance', CAST('2015-04-01' AS Date), CAST('2014-12-01' AS Date), NULL, N'Opera', NULL, N'150674', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Batte, Rico', NULL, N'301-492-4146', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Network Adequacy', 'Network Adequacy', CAST('2015-01-31' AS Date), CAST('2015-01-01' AS Date), NULL, N'N/A', NULL, N'150533', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Batte, Rico', NULL, N'301-492-5219', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Summary of Benefits and Coverage', 'Summary of Benefits and Coverage', CAST('2015-07-01' AS Date), CAST('2014-12-01' AS Date), NULL, N'N/A', NULL, N'150656', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Batte, Rico', NULL, N'301-492-4244', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'RADV Medical Record Review Contractor', 'RADV Medical Record Review Contractor', CAST('2015-07-01' AS Date), CAST('2015-03-01' AS Date), NULL, NULL, NULL, N'150672', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Eberhart, Christina', NULL, N'410-786-4671', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'PSC Managed Care Payment Validation', 'PSC Managed Care Payment Validation', CAST('2015-07-03' AS Date), CAST('2015-03-01' AS Date), NULL, N'Reed & Associates', NULL, N'150668', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, N'410-786-7702', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Part C and Part D Error Rates', 'Part C and Part D Error Rates', CAST('2015-09-01' AS Date), CAST('2015-04-01' AS Date), NULL, N'BAH', NULL, N'150671', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Eberhart, Christina', NULL, N'410-786-4671', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Misvalued Codes Data Collectionunder PFS', 'Misvalued Codes Data Collectionunder PFS', CAST('2015-08-15' AS Date), CAST('2015-04-01' AS Date), NULL, N'RAND Corporation Urban Institute', NULL, N'150662', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Eberhart, Christina', NULL, N'410-786-2298', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Risk Adjustment Model Research, Development, and Maintenance (Part C/D)', 'Risk Adjustment Model Research, Development, and Maintenance (Part C/D)', CAST('2015-06-01' AS Date), CAST('2014-11-01' AS Date), NULL, N'RTI', NULL, N'150288', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Eberhart, Christina', NULL, N'410-786-9212', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Medicare Part D Reconsiderations', 'Medicare Part D Reconsiderations', CAST('2015-03-02' AS Date), CAST('2014-12-01' AS Date), NULL, N'MAXIMUS Federal', NULL, N'150669', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $50M and < $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, N'410-786-0512', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Emergency Department Patient Experience of Care Survey', 'Emergency Department Patient Experience of Care Survey', CAST('2015-09-01' AS Date), CAST('2015-04-01' AS Date), NULL, N'RAND Corporation', NULL, N'150666', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, N'410-786-6665', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', '1011 Contract', '1011 Contract', CAST('2015-09-01' AS Date), CAST('2015-04-01' AS Date), NULL, N'Novitas', NULL, N'150673', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, N'404-562-7205', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Monitoring Methods and Design', 'Monitoring Methods and Design', CAST('2015-08-20' AS Date), CAST('2015-03-01' AS Date), NULL, N'None', NULL, N'150974', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, N'410-786-6920', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Coverage Gap Discount Program Analysis', 'Coverage Gap Discount Program Analysis', CAST('2015-07-01' AS Date), CAST('2015-03-01' AS Date), NULL, N'None', NULL, N'150664', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Smolenski, Mark', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DMSC', 'Long Term Care Hospital', 'Long Term Care Hospital', CAST('2015-06-01' AS Date), CAST('2015-02-01' AS Date), NULL, N'N/A', NULL, N'150921', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Eberhart, Christina', NULL, N'410-786-6504', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Incurred Cost Proposal (ICP) Audit Support Effort', 'Incurred Cost Proposal (ICP) Audit Support Effort', CAST('2015-03-15' AS Date), CAST('2015-01-01' AS Date), NULL, NULL, NULL, N'150690', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, N'410-786-7839', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'HITECH-Payment File Development Contract (PFDC)', 'HITECH-Payment File Development Contract (PFDC)', CAST('2015-08-10' AS Date), CAST('2015-01-01' AS Date), NULL, N'NGS', NULL, N'150692', 'New Competitive Contract', '>= $2M and < $5M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Schultz, Theresa', NULL, N'410-786-8075', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Appeals Support', 'Appeals Support', CAST('2014-10-30' AS Date), NULL, NULL, NULL, NULL, N'150486', 'New Competitive Contract', '>= $2M and < $5M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Rutherford, Edward', NULL, N'410-786-4058', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'HITECH-Audit', 'HITECH-Audit', CAST('2015-01-01' AS Date), NULL, NULL, N'Figliozzi', NULL, N'150245', 'New Competitive Contract', '>= $5M and < $10M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Rutherford, Edward', NULL, N'410-786-4058', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Comprehensive Error Rate Testing-Review Contractor (CERT-RC)', 'Comprehensive Error Rate Testing-Review Contractor (CERT-RC)', CAST('2015-09-01' AS Date), CAST('2015-03-01' AS Date), NULL, N'TBD', NULL, N'150693', 'New Competitive Contract', '>= $10M and < $20M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, N'410-786-6537', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Payment Error Rate Measurement-Review Contractor (PERM-RC)', 'Payment Error Rate Measurement-Review Contractor (PERM-RC)', CAST('2015-08-01' AS Date), CAST('2015-03-01' AS Date), NULL, N'TBD', NULL, N'150694', 'New Competitive Contract', '>= $5M and < $10M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, N'410-786-8519', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Medical Review (MR) Database', 'Medical Review (MR) Database', NULL, NULL, NULL, NULL, NULL, N'150696', 'New Competitive Contract', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Print-Mail Contractor', 'Print-Mail Contractor', NULL, NULL, NULL, NULL, NULL, N'150698', 'New Competitive Contract', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'PA Initiatives', 'PA Initiatives', NULL, NULL, NULL, NULL, NULL, N'150699', 'New Competitive Contract', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Part C/D Audit', 'Part C/D Audit', CAST('2015-08-01' AS Date), CAST('2015-01-01' AS Date), NULL, NULL, NULL, N'150701', 'Competitive Task/Delivery Order', '>= $5M and < $10M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Schultz, Theresa', NULL, N'410-786-5688', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', '1/3 Financial Audits - Review of PDE and TROOP', '1/3 Financial Audits - Review of PDE and TROOP', CAST('2015-08-01' AS Date), CAST('2015-01-01' AS Date), NULL, NULL, NULL, N'150703', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Schultz, Theresa', NULL, N'410-786-5688', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Cost Plan Auidt', 'Cost Plan Auidt', CAST('2015-08-01' AS Date), CAST('2015-01-01' AS Date), NULL, NULL, NULL, N'150704', 'Competitive Task/Delivery Order', '>= $1M and < $2M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Schultz, Theresa', NULL, N'410-786-8692', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Cost Plan Auidt', 'Cost Plan Auidt', CAST('2015-08-01' AS Date), CAST('2015-01-01' AS Date), NULL, NULL, NULL, N'150705', 'Competitive Task/Delivery Order', '>= $500K and < $1M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Schultz, Theresa', NULL, N'410-786-6502', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Fraud System Enhancements', 'Fraud System Enhancements', NULL, NULL, NULL, N'TBD', NULL, N'150706', 'Competitive Task/Delivery Order', '>= $2M and < $5M', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Dean, Nathaniel', NULL, N'410-786-5650', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'DMEPOS Validation Contractor', 'DMEPOS Validation Contractor', NULL, NULL, NULL, N'TBD', NULL, N'150707', 'New Competitive Contract', '>= $1M and < $2M', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Dean, Nathaniel', NULL, N'410-786-6599', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Part C RAC', 'Part C RAC', CAST('2015-06-30' AS Date), CAST('2014-12-01' AS Date), NULL, NULL, NULL, N'150443', 'New Competitive Contract', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Hoey, Nicole', NULL, N'410-786-1152', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Marketplace Financial Services Support', 'Marketplace Financial Services Support', CAST('2015-01-05' AS Date), CAST('2014-11-01' AS Date), NULL, N'Novitas', NULL, N'150277', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $50M and < $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Duckworth, Amy', NULL, N'410-786-6958', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DPIFMC', 'Unified Program Integrity Contractor', 'Unified Program Integrity Contractor', CAST('2015-09-18' AS Date), NULL, NULL, N'multiple', NULL, N'150213', 'New Competitive Contract', 'TBD', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Dean, Nathaniel', NULL, N'410-786-1126', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Independent Evaluation Center (IEC) of the 11th SOW Quality Improvement Network (QIN) Quality Improvement Organization (QIO)', 'Independent Evaluation Center (IEC) of the 11th SOW Quality Improvement Network (QIN) Quality Improvement Organization (QIO)', CAST('2015-01-30' AS Date), CAST('2014-12-14' AS Date), NULL, N'New Work', NULL, N'150593', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Christina Heller', NULL, N'410-786-1515', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Beneficiary and Family Centered Care Oversight & Review Center (BFCC-ORC)', 'Beneficiary and Family Centered Care Oversight & Review Center (BFCC-ORC)', CAST('2015-01-30' AS Date), CAST('2014-11-12' AS Date), NULL, N'New Work', NULL, N'150591', 'New Competitive Contract 8(a) Set A Side', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Christina Heller', NULL, N'410-786-2112', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Quality Improvement Organization 11th SOW Collaboration Portal/Website', 'Quality Improvement Organization 11th SOW Collaboration Portal/Website', CAST('2015-01-30' AS Date), CAST('2014-12-14' AS Date), NULL, N'New Work', NULL, N'150611', 'New Competitive Contract ', '>= $500K and < $1M', '>= $500K and < $1M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Christina Heller', NULL, N'x61515', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Strategic Innovation Engine (SIE)', 'Strategic Innovation Engine (SIE)', CAST('2015-01-30' AS Date), CAST('2014-11-14' AS Date), NULL, N'New Work', NULL, N'150499', 'New Competitive Contract', '>  $150K and < $500K', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Christina Heller', NULL, N'410-786-5611', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Data Analysis; M&E; APU; Process Calculations & Support; Education, Outreach, Help Desk', 'Data Analysis; M&E; APU; Process Calculations & Support; Education, Outreach, Help Desk', CAST('2015-09-01' AS Date), CAST('2015-04-15' AS Date), NULL, N'New Work', NULL, N'150614', 'New Competitive Contract', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Micheal Millenese', NULL, N'410-786-9667', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Meaningful Use Clinical Quality Measures Program and Operations Support', 'Meaningful Use Clinical Quality Measures Program and Operations Support', CAST('2015-06-01' AS Date), CAST('2015-04-15' AS Date), NULL, N'New Work', NULL, N'150606', 'New Competitive Contract', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Micheal Millenese', NULL, N'410-786-1879', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'S-CAHPS Implementation Support', 'S-CAHPS Implementation Support', CAST('2015-06-01' AS Date), CAST('2015-04-01' AS Date), NULL, N'New Work', NULL, N'150641', 'New Competitive Contract', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Kim Tatum', NULL, N'410-786-1879', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'SNF All-cause measure development', 'SNF All-cause measure development', CAST('2015-09-01' AS Date), CAST('2015-04-01' AS Date), NULL, N'New Work', NULL, N'150605', 'New Competitive Contract', '>= $2M and < $5M', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Scott Filipovits', NULL, N'410-786-1175', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Experience of Care', 'Experience of Care', CAST('2015-09-01' AS Date), CAST('2015-03-15' AS Date), NULL, N'New Work', NULL, N'150630', 'New Competitive Contract', '>= $1M and < $2M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Micheal Crow', NULL, N'410-786-9667', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'e-CQM Standards', 'e-CQM Standards', CAST('2015-09-30' AS Date), NULL, NULL, N'New Work', NULL, N'150643', 'New Competitive Contract', '>= $1M and < $2M', '>= $5M and < $10M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Micheal Millenese', NULL, N'410-786-5264', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Maintenance of CMS data element library/oversight council and HIT standards updates', 'Maintenance of CMS data element library/oversight council and HIT standards updates', CAST('2015-09-30' AS Date), NULL, NULL, N'New Work', NULL, N'150644', 'New Competitive Contract', '>= $1M and < $2M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Kim Tatum', NULL, N'410-786-2547', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC', 'Training Implementation-4QRPs', 'Training Implementation-4QRPs', CAST('2015-09-01' AS Date), NULL, NULL, N'New Work', NULL, N'150619', 'New Competitive Contract', '>= $1M and < $2M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Scott Filipovita', NULL, N'410-786-2119', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ASG/DQC ', 'CrownWeb OCT', 'CrownWeb OCT', CAST('2015-04-01' AS Date), CAST('2014-12-14' AS Date), NULL, N'FMQAI', NULL, N'150637', 'New Competitive', '>= $500K and < $1M', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Michael Crow', NULL, N'410-786-7217', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'Single Testing Contractor (STC) A&B ', 'Single Testing Contractor (STC) A&B ', CAST('2015-03-23' AS Date), CAST('2014-07-01' AS Date), NULL, N'QSSI', NULL, N'150289', 'New Competitive Contract', '>= $10M and < $20M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Fredericks, Al', NULL, N'410-786-0318', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'National Level Repository (NLR) Testing', 'National Level Repository (NLR) Testing', CAST('2015-04-28' AS Date), CAST('2014-12-01' AS Date), NULL, N'QSSI', NULL, N'150529', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Fredericks, Al', NULL, N'410-786-6826', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'MSIS/NMUD - SCA', 'MSIS/NMUD - SCA', CAST('2015-04-14' AS Date), CAST('2015-02-01' AS Date), NULL, N'Blue Canopy', NULL, N'150528', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Fredericks, Al', NULL, N'410-786-0483', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'OIS contract: Security Control Accessment (SCA)  of the Encounter Data System ', 'OIS contract: Security Control Accessment (SCA)  of the Encounter Data System ', CAST('2015-05-01' AS Date), CAST('2015-03-01' AS Date), NULL, N'Blue Canopy', NULL, N'150525', 'Competitive Task/Delivery Order', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Fredericks, Al', NULL, N'410-786-1566', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'National Level Repository (NLR) Hosting', 'National Level Repository (NLR) Hosting', CAST('2015-01-25' AS Date), NULL, NULL, N'CDS', NULL, N'150529', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Littleton, Chuck', NULL, N'410-786-xxxx', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'Common Working File (CWF) Hosting', 'Common Working File (CWF) Hosting', CAST('2015-11-21' AS Date), NULL, NULL, N'HP', NULL, N'150228', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $50M and < $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Simcock, Ben', NULL, N'410-786-xxxx', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'CA Software', 'CA Software', CAST('2014-12-31' AS Date), CAST('2014-11-01' AS Date), NULL, N'HMS Tech', NULL, N'150113', 'Competitive Task/Delivery Order', '>= $5M and < $10M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Simcock, Ben', NULL, N'410-786-9372', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DDCC', 'SAP Business Objects', 'SAP Business Objects', CAST('2014-12-15' AS Date), CAST('2014-11-01' AS Date), NULL, N'EC America', NULL, N'150222', 'New Competitive Contract', '>  $150K and < $500K', '>= $500K and < $1M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Simcock, Ben', NULL, N'410-786-1137', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Marketplace Testing Contractor', 'Marketplace Testing Contractor', CAST('2015-05-01' AS Date), CAST('2014-11-01' AS Date), NULL, NULL, NULL, N'150573', 'Competitive Task/Delivery Order', '>= $20M and < $50M', '>= $20M and < $50M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Blackshire, Aaron', NULL, N'410-786-0247', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Marketplace Independent Validation & Verification', 'Marketplace Independent Validation & Verification', CAST('2015-05-01' AS Date), CAST('2014-12-01' AS Date), NULL, N'Turning Point', NULL, N'150574', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Blackshire, Aaron', NULL, N'410-786-0247', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Marketplace Systems Integrator', 'Marketplace Systems Integrator', CAST('2015-02-28' AS Date), CAST('2014-07-01' AS Date), NULL, N'QSSI', NULL, N'150442', 'Competitive Task/Delivery Order', '>= $20M and < $50M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Mummert, Andy', NULL, N'410-786-1193', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Automated Delilverable Tracking Tool', 'Automated Delilverable Tracking Tool', CAST('2015-01-01' AS Date), CAST('2014-12-01' AS Date), NULL, N'New', NULL, N'150586', 'New Competitive Contract', '>= $1M and < $2M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Mummert, Andy', NULL, N'410-786-5510', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'SOW Requirements Developer', 'SOW Requirements Developer', CAST('2014-10-01' AS Date), CAST('2014-11-01' AS Date), NULL, NULL, NULL, N'150332', 'New Competitive Contract', '>  $150K and < $500K', '>  $150K and < $500K', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Mummert, Andy', NULL, N'410-786-5510', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Business Intelligence Tool', 'Business Intelligence Tool', CAST('2014-10-01' AS Date), CAST('2014-11-01' AS Date), NULL, N'Centeva', NULL, N'150587', 'New Competitive Contract', '>  $150K and < $500K', '>= $500K and < $1M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Mummert, Andy', NULL, N'410-786-5510', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Federal Facilitated Exchange (FFE) - Maintenance and Enhancement', 'Federal Facilitated Exchange (FFE) - Maintenance and Enhancement', CAST('2015-01-15' AS Date), NULL, NULL, N'Accenture Federal Services', NULL, N'150244', 'New Competitive Contract', '> $100M', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Savoy, Candice', NULL, N'410-786-6089', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Enterprise Content Management Support Services', 'Enterprise Content Management Support Services', CAST('2015-09-17' AS Date), CAST('2015-03-01' AS Date), NULL, N'CAS Severn', NULL, N'150575', 'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $2M and < $5M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Wilkins, Dawn', NULL, N'410-786-1137', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DISC', 'Possible Integration with CAMS', 'Possible Integration with CAMS', CAST('2015-01-15' AS Date), CAST('2014-11-01' AS Date), NULL, NULL, NULL, N'150420', 'New Competitive Contract', '>= $1M and < $2M', '>= $1M and < $2M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Wilkins, Dawn', NULL, N'410-786-5141', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DMITSC', 'HIDS/Compete (Infrastructure new)', 'HIDS/Compete (Infrastructure new)', CAST('2014-12-15' AS Date), NULL, NULL, N'Buccanner', NULL, N'150209', 'New Competitive Contract', '>= $50M and < $100M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Anderson, Tonya', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DMITSC', 'Provider Enrollment Chain & Own. System (PECOS) - Contract recompete', 'Provider Enrollment Chain & Own. System (PECOS) - Contract recompete', CAST('2015-06-15' AS Date), CAST('2015-06-01' AS Date), NULL, N'CGI', NULL, N'151045', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $50M and < $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Anderson, Tonya', NULL, N'410-786-0243	410-786-6373', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DMITSC', 'Quality Management and Reviews Systems', 'Quality Management and Reviews Systems', CAST('2015-03-25' AS Date), CAST('2014-12-01' AS Date), NULL, N'Visual Connection', NULL, NULL, N'Competitive Task/Delivery Order', '>= $2M and < $5M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Anderson, Tonya', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DMITSC', 'Survey and Certification Application Software will replace current ASPEN application', 'Survey and Certification Application Software will replace current ASPEN application', CAST('2015-08-01' AS Date), CAST('2015-02-01' AS Date), NULL, NULL, NULL, N'151047', 'Competitive Task/Delivery Order', '>= $5M and < $10M', 'TBD', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Kooy, Ryan', NULL, NULL, N'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'ITG/DMITSC', 'RDS Program IT & Operations ', 'RDS Program IT & Operations ', CAST('2015-01-14' AS Date), NULL, NULL, N'GDIT', NULL, N'150439', 'Competitive Task/Delivery Order', '>= $10M and < $20M', '>= $10M and < $20M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Dixon, Evelyn', NULL, N'410-786-5751', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'MACG/DEMAC', 'JD DME MAC', 'JD DME MAC', CAST('2015-09-14' AS Date), CAST('2015-01-01' AS Date), NULL, N'Noridian', NULL, NULL, N'New Competitive Contract', '>= $1M and < $2M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'tbd', NULL, N'CM/Jim Ralls 410-786-9504', 'OAGM', 'CONTRACT SPECIALIST'),
	('CMS', 'MACG/DWMAC', 'J15 A/B MAC', 'J15 A/B MAC', CAST('2015-06-26' AS Date), CAST('2014-12-01' AS Date), NULL, N'CGS', NULL, NULL, N'New Competitive Contract', '>= $2M and < $5M', '> $100M', 'TBD', NULL, NULL, N'CMS', 'CMS', NULL, NULL, NULL, N'Brenda Clark', NULL, N'CM/Steven Smetak 303-844-7123', 'OAGM', 'CONTRACT SPECIALIST')
GO

UPDATE BDI SET BDI.CompetitionTypeID = T.CompetitionTypeID FROM BulkData.BulkDataImport BDI JOIN Dropdown.CompetitionType T ON T.CompetitionTypeName = BDI.CompetitionType
UPDATE BDI SET BDI.ContractTypeID = T.ContractTypeID FROM BulkData.BulkDataImport BDI JOIN Dropdown.ContractType T ON T.ContractTypeName = BDI.ContractType
UPDATE BDI SET BDI.FiscalYearContractRangeID = T.ContractRangeID FROM BulkData.BulkDataImport BDI JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.FiscalYearContractRange
UPDATE BDI SET BDI.EstimatedSolicitationQuarter = 0 FROM BulkData.BulkDataImport BDI WHERE BDI.EstimatedSolicitationQuarter IS NULL
UPDATE BDI SET BDI.NAICSID = T.NAICSID FROM BulkData.BulkDataImport BDI JOIN Dropdown.NAICS T ON T.NAICSCode = BDI.NAICSCode
UPDATE BDI SET BDI.ContractingOrganizationID = T.OrganizationID FROM BulkData.BulkDataImport BDI JOIN Dropdown.Organization T ON T.OrganizationName = BDI.ContractingOrganization
UPDATE BDI SET BDI.FundingOrganizationID = T.OrganizationID FROM BulkData.BulkDataImport BDI JOIN Dropdown.Organization T ON T.OrganizationName = BDI.FundingOrganization
UPDATE BDI SET BDI.OriginatingOrganizationID = T.OrganizationID FROM BulkData.BulkDataImport BDI JOIN Dropdown.Organization T ON T.OrganizationName = BDI.OriginatingOrganizationName
UPDATE BDI SET BDI.PlaceOfPerformanceStateID = T.StateID FROM BulkData.BulkDataImport BDI JOIN Dropdown.State T ON T.StateCode = BDI.PlaceOfPerformanceStateCode
UPDATE BDI SET BDI.RequirementTypeID = T.RequirementTypeID FROM BulkData.BulkDataImport BDI JOIN Dropdown.RequirementType T ON T.RequirementTypeName = BDI.RequirementType
UPDATE BDI SET BDI.TotalContractRangeID = T.ContractRangeID FROM BulkData.BulkDataImport BDI JOIN Dropdown.ContractRange T ON T.ContractRangeName = BDI.TotalContractRange
GO

UPDATE BDI 
SET 
	BDI.HasPlaceOfPerformance = 
		CASE
			WHEN BDI.PlaceOfPerformanceCity IS NOT NULL OR BDI.PlaceOfPerformanceStateCode IS NOT NULL 
			THEN 1
			ELSE 0
		END,

	BDI.HasPointOfContact = 
		CASE
			WHEN BDI.PointOfContactName IS NOT NULL OR BDI.PointOfContactEmail IS NOT NULL OR BDI.PointOfContactPhone IS NOT NULL OR BDI.PointOfContactAgency IS NOT NULL OR BDI.PointOfContactTitle IS NOT NULL
			THEN 1
			ELSE 0
		END
FROM BulkData.BulkDataImport BDI
GO

INSERT INTO dbo.Contract
	(BulkDataImportID, ContractingOfficeName, ContractName, ContractDescription, TargetAwardDate, ContractDateStart, ContractDateEnd, IncumbentContractorName, ReferenceIDV, TransactionNumber, CompetitionTypeID, ContractTypeID, FiscalYearContractRangeID, NAICSID, ContractingOrganizationID, FundingOrganizationID, OriginatingOrganizationID, RequirementTypeID, TotalContractRangeID, EstimatedSolicitationQuarter, WorkflowID)
SELECT
	BDI.BulkDataImportID,
	BDI.ContractingOfficeName, 
	BDI.ContractName, 
	BDI.ContractDescription, 
	BDI.TargetAwardDate, 
	BDI.ContractDateStart, 
	BDI.ContractDateEnd, 
	BDI.IncumbentContractorName, 
	BDI.ReferenceIDV, 
	BDI.TransactionNumber, 
	BDI.CompetitionTypeID, 
	BDI.ContractTypeID, 
	BDI.FiscalYearContractRangeID, 
	BDI.NAICSID, 
	BDI.ContractingOrganizationID, 
	BDI.FundingOrganizationID, 
	BDI.OriginatingOrganizationID, 
	BDI.RequirementTypeID, 
	BDI.TotalContractRangeID, 
	BDI.EstimatedSolicitationQuarter,
	W.WorkflowID	
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Workflow W ON W.OrganizationID = BDI.OriginatingOrganizationID
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.Contract C
			WHERE C.BulkDataImportID = BDI.BulkDataImportID
			)
GO

INSERT INTO dbo.ContractWorkflowPerson
	(ContractID, WorkflowStepNumber, PersonID)
SELECT
	C.ContractID,
	WP.WorkflowStepNumber,
	WP.PersonID
FROM dbo.Contract C, dbo.WorkflowPerson WP
WHERE C.BulkDataImportID > 0
	AND WP.WorkflowID = C.WorkflowID
GO

INSERT INTO dbo.ContractPlaceOfPerformance
	(ContractID, PlaceOfPerformanceCity, PlaceOfPerformanceStateID, IsPrimary)
SELECT
	C.ContractID,
	BDI.PlaceOfPerformanceCity,
	BDI.PlaceOfPerformanceStateID,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND BDI.HasPlaceOfPerformance = 1
GO

INSERT INTO dbo.ContractPointOfContact
	(ContractID, PointOfContactAgency, PointOfContactEmail, PointOfContactName, PointOfContactPhone, PointOfContactTitle, IsPrimary)
SELECT
	C.ContractID,
	BDI.PointOfContactAgency,
	BDI.PointOfContactEmail,
	BDI.PointOfContactName,
	BDI.PointOfContactPhone,
	BDI.PointOfContactTitle,
	1
FROM BulkData.BulkDataImport BDI
	JOIN dbo.Contract C ON C.BulkDataImportID = BDI.BulkDataImportID
		AND BDI.HasPointOfContact = 1
GO

/*
DELETE C FROM dbo.Contract C WHERE C.BulkDataImportID > 0
DELETE CPC FROM dbo.ContractPointOfContact CPC WHERE NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractID = CPC.ContractID)
DELETE CPOP FROM dbo.ContractPlaceOfPerformance CPOP WHERE NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractID = CPOP.ContractID)
*/