USE PFDR
GO

--Begin Functions
--Begin function AccessControl.GetContractRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetContractRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.23
-- Description:	A function to manage the buisness rules regarding Contract table record visibility based on PersonID
-- =================================================================================================================

CREATE FUNCTION AccessControl.GetContractRecordsByPersonID
(
@PersonID INT,
@Contract AccessControl.Contract READONLY
)

RETURNS @tReturn table 
	(
	ContractID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @tPersonRole TABLE (RoleCode VARCHAR(50), OrganizationID INT)
	
	INSERT INTO @tPersonRole
		(RoleCode,OrganizationID)
	SELECT
		R.RoleCode,
		PR.OrganizationID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID

	IF EXISTS (SELECT 1 FROM @tPersonRole PR WHERE PR.RoleCode = 'SuperAdministrator')
		BEGIN

		INSERT INTO @tReturn
			(ContractID,CanHaveDelete,CanHaveEdit)
		SELECT
			CAC.ContractID,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				ELSE 1
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				ELSE 1
			END

		FROM @Contract CAC
			JOIN dbo.Contract C ON C.ContractID = CAC.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

		END
	ELSE
		BEGIN

		INSERT INTO @tReturn
			(ContractID,CanHaveDelete,CanHaveEdit)
		SELECT
			CAC.ContractID,

			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID AND WP.WorkflowStepNumber = C.WorkflowStepNumber AND WP.PersonID = @PersonID)
				THEN 1
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')   
				THEN 1
				ELSE 0
			END,
			
			CASE
				WHEN WS.WorkflowStatusCode = 'Approved'
				THEN 0
				WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = C.WorkflowID AND WP.WorkflowStepNumber = C.WorkflowStepNumber AND WP.PersonID = @PersonID)
				THEN 1
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = C.OriginatingOrganizationID AND TPR.RoleCode = 'OrganizationAdministrator')   
				THEN 1
				ELSE 0
			END

		FROM @Contract CAC
			JOIN dbo.Contract C ON C.ContractID = CAC.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

		UPDATE T
		SET
			T.CanHaveEdit = 1
		FROM @tReturn T
			JOIN dbo.Contract C ON C.ContractID = T.ContractID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND WS.WorkflowStatusCode = 'PendingApproval'
				AND EXISTS
					(
					SELECT 1
					FROM dbo.WorkflowPerson WP
					WHERE WP.WorkflowID = C.WorkflowID 
						AND WP.WorkflowStepNumber = C.WorkflowStepNumber
						AND WP.PersonID = @PersonID
					)

		END
	--ENDIF
					
	RETURN

END
GO
--End function AccessControl.GetContractRecordsByPersonID

--Begin function AccessControl.GetOrganizationRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetOrganizationRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.06.16
-- Description:	A function to manage the buisness rules regarding Organization table record visibility based on PersonID
-- =====================================================================================================================

CREATE FUNCTION AccessControl.GetOrganizationRecordsByPersonID
(
@PersonID INT,
@Organization AccessControl.Organization READONLY
)

RETURNS @tReturn table 
	(
	OrganizationID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @tPersonRole TABLE (RoleCode VARCHAR(50), OrganizationID INT)
	
	INSERT INTO @tPersonRole
		(RoleCode,OrganizationID)
	SELECT
		R.RoleCode,
		PR.OrganizationID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM Dropdown.Role R
				WHERE R.RoleID = PR.RoleID
					AND R.IsAdministrator = 1
				)

	IF EXISTS (SELECT 1 FROM @tPersonRole PR WHERE PR.RoleCode = 'SuperAdministrator')
		BEGIN

		INSERT INTO @tReturn
			(OrganizationID,CanHaveDelete,CanHaveEdit)
		SELECT
			OAC.OrganizationID,

			CASE
				WHEN OH.HasChildren = 0 
					AND NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractingOrganizationID = OH.OrganizationID OR C.FundingOrganizationID = OH.OrganizationID OR C.OriginatingOrganizationID = OH.OrganizationID)
					AND NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.OrganizationID = OH.OrganizationID)
				THEN 1
				ELSE 0
			END,
			
			1
		FROM @Organization OAC
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = OAC.OrganizationID

		END
	ELSE
		BEGIN

		INSERT INTO @tReturn
			(OrganizationID,CanHaveDelete,CanHaveEdit)
		SELECT
			OAC.OrganizationID,

			CASE
				WHEN OH.HasChildren = 0 
					AND NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractingOrganizationID = OH.OrganizationID OR C.FundingOrganizationID = OH.OrganizationID OR C.OriginatingOrganizationID = OH.OrganizationID)
					AND NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.OrganizationID = OH.OrganizationID)
					AND EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = OH.OrganizationID) 
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = OH.OrganizationID) 
				THEN 1
				ELSE 0
			END
			
		FROM @Organization OAC
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = OAC.OrganizationID

		END
	--ENDIF
					
	RETURN

END
GO
--End function AccessControl.GetOrganizationRecordsByPersonID

--Begin function AccessControl.GetPersonRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetPersonRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.23
-- Description:	A function to manage the buisness rules regarding Person table record visibility based on PersonID
-- ===============================================================================================================

CREATE FUNCTION AccessControl.GetPersonRecordsByPersonID
(
@PersonID INT,
@Person AccessControl.Person READONLY
)

RETURNS @tReturn table 
	(
	PersonID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @tPersonRole TABLE (RoleCode VARCHAR(50), OrganizationID INT)
	
	INSERT INTO @tPersonRole
		(RoleCode,OrganizationID)
	SELECT
		R.RoleCode,
		PR.OrganizationID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM Dropdown.Role R
				WHERE R.RoleID = PR.RoleID
					AND R.IsAdministrator = 1
				)

	INSERT INTO @tReturn
		(PersonID,CanHaveDelete,CanHaveEdit)
	SELECT 
		PAC.PersonID,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.RoleCode = 'SuperAdministrator')
			THEN 1
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR WHERE PR.PersonID = PAC.PersonID AND EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = PR.OrganizationID))
			THEN 1
			ELSE 0
		END,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.RoleCode = 'SuperAdministrator')
			THEN 1
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR WHERE PR.PersonID = PAC.PersonID AND EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = PR.OrganizationID))
			THEN 1
			ELSE 0
		END
					
	FROM @Person PAC
					
	RETURN

END
GO
--End function AccessControl.GetPersonRecordsByPersonID

--Begin function AccessControl.GetWorkflowRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetWorkflowRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.23
-- Description:	A function to manage the buisness rules regarding Workflow table record visibility based on PersonID
-- =================================================================================================================

CREATE FUNCTION AccessControl.GetWorkflowRecordsByPersonID
(
@PersonID INT,
@Workflow AccessControl.Workflow READONLY
)

RETURNS @tReturn table 
	(
	WorkflowID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @tPersonRole TABLE (RoleCode VARCHAR(50), OrganizationID INT)
	
	INSERT INTO @tPersonRole
		(RoleCode,OrganizationID)
	SELECT
		R.RoleCode,
		PR.OrganizationID
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID
			AND EXISTS
				(
				SELECT 1
				FROM Dropdown.Role R
				WHERE R.RoleID = PR.RoleID
					AND R.IsAdministrator = 1
				)

	IF EXISTS (SELECT 1 FROM @tPersonRole PR WHERE PR.RoleCode = 'SuperAdministrator')
		BEGIN

		INSERT INTO @tReturn
			(WorkflowID,CanHaveDelete,CanHaveEdit)
		SELECT
			WAC.WorkflowID,
			0, --1 until we permit multiple workflows per opdiv this must be 0
			1
		FROM @Workflow WAC

		END
	ELSE
		BEGIN

		INSERT INTO @tReturn
			(WorkflowID,CanHaveDelete,CanHaveEdit)
		SELECT
			WAC.WorkflowID,
			0, --until we permit multiple workflows per opdiv this must be 0

			CASE
				WHEN EXISTS (SELECT 1 FROM @tPersonRole TPR WHERE TPR.OrganizationID = W.OrganizationID)
				THEN 1
				ELSE 0
			END
			
		FROM @Workflow WAC
			JOIN dbo.Workflow W ON W.WorkflowID = WAC.WorkflowID

		END
	--ENDIF
					
	RETURN

END
GO
--End function AccessControl.GetWorkflowRecordsByPersonID

--Begin function dbo.FormatDateTime
EXEC Utility.DropObject 'dbo.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2014.06.10
-- Description:	A function to return a formatted string from a DATE or DATETIME
-- ============================================================================

CREATE FUNCTION dbo.FormatDateTime
(
@DateTime DATETIME,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

DECLARE @DateTimeFormatted VARCHAR(250) = ''

IF @Format = 'mm/dd/yyyy'
	SET @DateTimeFormatted = CONVERT(VARCHAR, @DateTime, 101)
ELSE IF @Format = 'mm/dd/yyyy hh:mm tt'
	SET @DateTimeFormatted = CONVERT(VARCHAR, @DateTime, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTime, 9), 13, 5) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTime, 9), 25, 2)
ELSE IF @Format = 'hh:mm tt'
	SET @DateTimeFormatted = SUBSTRING(CONVERT(VARCHAR, @DateTime, 9), 13, 5) + ' ' + SUBSTRING(CONVERT(VARCHAR, @DateTime, 9), 25, 2)
--ENDIF

RETURN @DateTimeFormatted
END
GO
--End function dbo.FormatDateTime

--Begin function dbo.GetDashboardWorkflowRawData
EXEC Utility.DropObject 'dbo.GetDashboardWorkflowRawData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.06.18
-- Description:	A function to get the raw data for the dashboard workflow statistics
-- =================================================================================

CREATE FUNCTION dbo.GetDashboardWorkflowRawData
(
)

RETURNS @tReturn table 
	(
	ContractID INT PRIMARY KEY NOT NULL,
	AgeInDays INT NOT NULL DEFAULT 0,
	PercentComplete NUMERIC(18,2) NOT NULL DEFAULT 0
	) 

AS
BEGIN

	INSERT INTO @tReturn
		(ContractID, AgeInDays, PercentComplete)
	SELECT 
		EL2.EntityID,
		DATEDIFF(d, EL2.CreateDateTime, getDate()),
		ISNULL(CAST(CAST(C.WorkflowStepNumber - 1 AS NUMERIC(18,2)) / CAST((SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS NUMERIC(18,2)) AS NUMERIC(18,2)) * 100, 0)
	FROM dbo.EventLog EL2
		JOIN 
			(
			SELECT
				MAX(EL1.EventLogID) AS EventLogID
			FROM dbo.EventLog EL1
				JOIN dbo.Contract C ON C.ContractID = EL1.EntityID
					AND EL1.EntityTypeCode = 'Contract'
				JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
					AND WS.WorkflowStatusCode <> 'Approved'
			GROUP BY EL1.EntityID
			) D ON D.EventLogID = EL2.EventLogID
		JOIN dbo.Contract C ON C.ContractID = EL2.EntityID
					
	RETURN

END
GO
--End function dbo.GetDashboardWorkflowRawData

--Begin function dbo.GetFiscalYearFromDate
EXEC Utility.DropObject 'dbo.GetFiscalYearFromDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2014.06.24
-- Description:	A function to return a fiscal year from a from a DATE or DATETIME
-- ==============================================================================

CREATE FUNCTION dbo.GetFiscalYearFromDate
(
@DateTime DATETIME
)

RETURNS VARCHAR(250)

AS
BEGIN

DECLARE @FiscalYear INT = YEAR(@DateTime)

IF MONTH(@DateTime) > 9
	SET @FiscalYear = YEAR(@DateTime) + 1
--ENDIF

RETURN @FiscalYear
END
GO
--End function dbo.GetFiscalYearFromDate

--Begin function dbo.GetPersonNameByPersonID
EXEC Utility.DropObject 'dbo.GetPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2014.04.22
-- Description:	A function to return the name of a person in a specified format from a PersonID
-- ============================================================================================

CREATE FUNCTION dbo.GetPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

DECLARE @FirstName VARCHAR(25)
DECLARE @LastName VARCHAR(25)
DECLARE @Title VARCHAR(10)
DECLARE @RetVal VARCHAR(250)

SET @RetVal = ''

IF @PersonID IS NOT NULL AND @PersonID > 0
	BEGIN
	
	SELECT
		@FirstName = ISNULL(P.FirstName, ''),
		@LastName = ISNULL(P.LastName, ''),
		@Title = ISNULL(P.Title, '')
	FROM dbo.Person P
	WHERE P.PersonID = @PersonID

	IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
		BEGIN
		
		SET @RetVal = @FirstName + ' ' + @LastName

		IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@Title)) > 0
			BEGIN
			
			SET @RetVal = @Title + ' ' + RTRIM(LTRIM(@RetVal))

			END
		--ENDIF
		
		END
	--ENDIF
		
	IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
		BEGIN
		
		IF LEN(RTRIM(@LastName)) > 0
			BEGIN
			
			SET @RetVal = @LastName + ', '

			END
		--ENDIF
			
		SET @RetVal = @RetVal + @FirstName + ' '

		IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@Title)) > 0
			BEGIN
			
			SET @RetVal = @RetVal + @Title

			END
		--ENDIF
		
		END
	--ENDIF
	END
--ENDIF

RETURN RTRIM(LTRIM(@RetVal))
END
GO
--End function dbo.GetPersonNameByPersonID

--Begin function Dropdown.ParseQuickSearchCriteria
EXEC Utility.DropObject 'Dropdown.ParseQuickSearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2013.12.10
-- Description:	A function to replace static QuickSearchVariables with their dynamic equivalents
-- =============================================================================================

CREATE FUNCTION Dropdown.ParseQuickSearchCriteria
(
@QuickSearchCriteria VARCHAR(MAX),
@PersonID INT = 0
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @EndOfCurrentFiscalYear VARCHAR(10) = '09/30/'

IF MONTH(getDate()) > 9
	SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) + 1 AS CHAR(4))
ELSE	
	SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) AS CHAR(4))
--ENDIF

SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"EndOfCurrentFiscalYear"', '"' + @EndOfCurrentFiscalYear + '"')
SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"PersonID"', '"' + CAST(@PersonID AS VARCHAR) + '"')
SET @QuickSearchCriteria = REPLACE(@QuickSearchCriteria, '"PersonOrganizationID"', '"' + CAST((SELECT P.OrganizationID FROM dbo.Person P WHERE P.PersonID = @PersonID) AS VARCHAR) + '"')

RETURN @QuickSearchCriteria
END
GO
--End function Dropdown.ParseQuickSearchCriteria
--End Functions

--Begin Procedures
--Begin procedure dbo.AddContractPlaceOfPerformance
EXEC Utility.DropObject 'dbo.AddContractPlaceOfPerformance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.12.09
-- Description:	A stored procedure to save data to the dbo.ContractPlaceOfPerformance table
-- ========================================================================================
CREATE PROCEDURE dbo.AddContractPlaceOfPerformance
@ContractID INT = 0,
@IsPrimary BIT = 0,
@PlaceOfPerformanceCity VARCHAR(250) = NULL,
@PlaceOfPerformanceStateID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tOutput TABLE (ContractPlaceOfPerformanceID INT)
		
	INSERT INTO dbo.ContractPlaceOfPerformance
		(
		ContractID,
		IsPrimary,
		PlaceOfPerformanceCity,
		PlaceOfPerformanceStateID
		)
	OUTPUT INSERTED.ContractPlaceOfPerformanceID INTO @tOutput
	VALUES
		(
		@ContractID,
		@IsPrimary,
		@PlaceOfPerformanceCity,
		@PlaceOfPerformanceStateID
		)

	SELECT 
		O.ContractPlaceOfPerformanceID, 
		CPOP.IsPrimary,
		CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode AS PlaceOfPerformance
	FROM @tOutput O
		JOIN dbo.ContractPlaceOfPerformance CPOP ON CPOP.ContractPlaceOfPerformanceID = O.ContractPlaceOfPerformanceID
		JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID

END
GO
--End procedure dbo.AddContractPlaceOfPerformance

--Begin procedure dbo.AddContractPointOfContact
EXEC Utility.DropObject 'dbo.AddContractPointOfContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.12.09
-- Description:	A stored procedure to save data to the dbo.ContractPointOfContact table
-- ====================================================================================
CREATE PROCEDURE dbo.AddContractPointOfContact
@ContractID INT = 0,
@IsPrimary BIT = 0,
@PointOfContactAgency VARCHAR(250) = NULL,
@PointOfContactEmail VARCHAR(320) = NULL,
@PointOfContactName VARCHAR(250) = NULL,
@PointOfContactPhone VARCHAR(50) = NULL,
@PointOfContactTitle VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tOutput TABLE (ContractPointOfContactID INT)
		
	INSERT INTO dbo.ContractPointOfContact
		(
		ContractID,
		IsPrimary,
		PointOfContactAgency,
		PointOfContactEmail,
		PointOfContactName,
		PointOfContactPhone,
		PointOfContactTitle
		)
	OUTPUT INSERTED.ContractPointOfContactID INTO @tOutput
	VALUES
		(
		@ContractID,
		@IsPrimary,
		@PointOfContactAgency,
		@PointOfContactEmail,
		@PointOfContactName,
		@PointOfContactPhone,
		@PointOfContactTitle
		)
	
	SELECT 
		O.ContractPointOfContactID, 
		CPOC.IsPrimary,
		CPOC.PointOfContactAgency,
		CPOC.PointOfContactEmail, 
		CPOC.PointOfContactName, 
		CPOC.PointOfContactPhone,
		CPOC.PointOfContactTitle
	FROM @tOutput O
		JOIN dbo.ContractPointOfContact CPOC ON CPOC.ContractPointOfContactID = O.ContractPointOfContactID

END
GO
--End procedure dbo.AddContractPointOfContact

--Begin procedure dbo.AddEventLogContractEntry
EXEC Utility.DropObject 'dbo.AddEventLogContractEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.10
-- Description:	A stored procedure to add data to the dbo.EventLog table
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogContractEntry
@Comments VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ContractPlacesOfPerformance VARCHAR(MAX) 
	DECLARE @ContractPointsOfContact VARCHAR(MAX) 

	SELECT 
		@ContractPlacesOfPerformance = COALESCE(@ContractPlacesOfPerformance, '') + D.ContractPlaceOfPerformance 
	FROM
		(
		SELECT
			(SELECT C.* FOR XML RAW('ContractPlaceOfPerformance'), ELEMENTS) AS ContractPlaceOfPerformance
		FROM dbo.ContractPlaceOfPerformance C
		WHERE C.ContractID = @ContractID
		) D
		
	SELECT 
		@ContractPointsOfContact = COALESCE(@ContractPointsOfContact, '') + D.ContractPointOfContact 
	FROM
		(
		SELECT
			(SELECT C.* FOR XML RAW('ContractPointOfContact'), ELEMENTS) AS ContractPointOfContact
		FROM dbo.ContractPointOfContact C
		WHERE C.ContractID = @ContractID
		) D
		
	INSERT INTO dbo.EventLog
		(Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	SELECT
		@Comments,
		C.ContractID,
		'Contract',
		@EventCode,
		(
		SELECT C.*, 
		CAST(('<ContractPlacesOfPerformance>' + @ContractPlacesOfPerformance + '</ContractPlacesOfPerformance>') AS XML), 
		CAST(('<ContractPointsOfContact>' + @ContractPointsOfContact + '</ContractPointsOfContact>') AS XML) 
		FOR XML RAW('Contract'), ELEMENTS
		),
		@PersonID
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID
		
END
GO
--End procedure dbo.AddEventLogContractEntry

--Begin procedure dbo.AddEventLogOrganizationEntry
EXEC Utility.DropObject 'dbo.AddEventLogOrganizationEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.12
-- Description:	A stored procedure to add data to the dbo.EventLog table
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogOrganizationEntry
@OrganizationID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.EventLog
		(EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	SELECT
		O.OrganizationID,
		'Organization',
		@EventCode,
		(
		SELECT O.*
		FOR XML RAW('Organization'), ELEMENTS
		),
		@PersonID
	FROM Dropdown.Organization O
	WHERE O.OrganizationID = @OrganizationID
		
END
GO
--End procedure dbo.AddEventLogOrganizationEntry

--Begin procedure dbo.AddEventLogPersonEntry
EXEC Utility.DropObject 'dbo.AddEventLogPersonEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.12
-- Description:	A stored procedure to add data to the dbo.EventLog table
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogPersonEntry
@TargetPersonID INT = 0,
@EventCode VARCHAR(50) = NULL,
@SourcePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PersonRoles VARCHAR(MAX) 

	SELECT 
		@PersonRoles = COALESCE(@PersonRoles, '') + D.PersonRole 
	FROM
		(
		SELECT
			(SELECT PR.* FOR XML RAW('PersonRole'), ELEMENTS) AS PersonRole
		FROM dbo.PersonRole PR
		WHERE PR.PersonID = @TargetPersonID
		) D
		
	INSERT INTO dbo.EventLog
		(EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	SELECT
		P.PersonID,
		'Person',
		@EventCode,
		(
		SELECT P.*, 
		CAST(('<PersonRoles>' + @PersonRoles + '</PersonRoles>') AS XML)
		FOR XML RAW('Person'), ELEMENTS
		),
		@SourcePersonID
	FROM dbo.Person P
	WHERE P.PersonID = @TargetPersonID
		
END
GO
--End procedure dbo.AddEventLogPersonEntry

--Begin procedure dbo.AddEventLogWorkflowEntry
EXEC Utility.DropObject 'dbo.AddEventLogWorkflowEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.13
-- Description:	A stored procedure to add data to the dbo.EventLog table
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogWorkflowEntry
@WorkflowID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @WorkflowPersons VARCHAR(MAX) 

	SELECT 
		@WorkflowPersons = COALESCE(@WorkflowPersons, '') + D.WorkflowPerson
	FROM
		(
		SELECT
			(SELECT WP.* FOR XML RAW('WorkflowPerson'), ELEMENTS) AS WorkflowPerson
		FROM dbo.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
		) D

	INSERT INTO dbo.EventLog
		(EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	SELECT
		W.WorkflowID,
		'Workflow',
		@EventCode,
		(
		SELECT W.*,
		CAST(('<WorkflowPersons>' + @WorkflowPersons + '</WorkflowPersons>') AS XML)
		FOR XML RAW('Workflow'), ELEMENTS
		),
		@PersonID
	FROM dbo.Workflow W
	WHERE W.WorkflowID = @WorkflowID
		
END
GO
--End procedure dbo.AddEventLogWorkflowEntry

--Begin procedure dbo.AddWorkflowPersons
EXEC Utility.DropObject 'dbo.AddWorkflowPersons'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.09
-- Description:	A stored procedure to add data to the dbo.WorkflowPerson table based on a WorkflowID, WorkflowStepNumber and PersonIDList
-- ======================================================================================================================================
CREATE PROCEDURE dbo.AddWorkflowPersons
@WorkflowID INT = 0,
@WorkflowStepNumber INT = 0,
@PersonIDList VARCHAR(MAX) = '',
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.WorkflowPerson
		(WorkflowID, WorkflowStepNumber, PersonID)
	SELECT
		@WorkflowID,
		@WorkflowStepNumber,
		LTT.ListItem
	FROM Utility.ListToTable(@PersonIDList, ',', 0) LTT 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = @WorkflowStepNumber
			AND WP.PersonID = LTT.ListItem
		)

END
GO
--End procedure dbo.AddWorkflowPersons

--Begin procedure dbo.AssignWorkflowToContract
EXEC Utility.DropObject 'dbo.AssignWorkflowToContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.09
-- Description:	A stored procedure to add data to the dbo.ContractWorkflowPerson table based on a ContractID
-- =========================================================================================================
CREATE PROCEDURE dbo.AssignWorkflowToContract
@Comments VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ContractWorkflowPersons VARCHAR(MAX) 

	UPDATE C
	SET 
		C.WorkflowID = W.WorkflowID,
		C.WorkflowStatusID = (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusCode = 'PendingSubmission'),
		C.WorkflowStepNumber = 1
	FROM dbo.Contract C
		JOIN dbo.Workflow W ON W.OrganizationID = C.OriginatingOrganizationID
			AND C.ContractID = @ContractID

	EXEC dbo.AddEventLogContractEntry @Comments = @Comments, @ContractID = @ContractID, @EventCode = 'AssignWorkflow', @PersonID = @PersonID

	DELETE CWP
	FROM dbo.ContractWorkflowPerson CWP
	WHERE CWP.ContractID = @ContractID
	
	INSERT INTO dbo.ContractWorkflowPerson
		(ContractID, WorkflowStepNumber, PersonID)
	SELECT
		C.ContractID,
		WP.WorkflowStepNumber,
		WP.PersonID
	FROM dbo.WorkflowPerson WP
		JOIN dbo.Contract C ON C.WorkflowID = WP.WorkflowID
			AND C.ContractID = @ContractID

	SELECT 
		@ContractWorkflowPersons = COALESCE(@ContractWorkflowPersons, '') + D.ContractWorkflowPerson 
	FROM
		(
		SELECT
			(SELECT CWP.* FOR XML RAW('ContractWorkflowPerson'), ELEMENTS) AS ContractWorkflowPerson
		FROM dbo.ContractWorkflowPerson CWP
		WHERE CWP.ContractID = @ContractID
		) D

	INSERT INTO dbo.EventLog
		(EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	VALUES
		(
		@ContractID,
		'Contract',
		'AssignWorkflowPersons',
		CAST(('<ContractWorkflowPersons>' + @ContractWorkflowPersons + '</ContractWorkflowPersons>') AS XML),
		@PersonID
		)

END
GO
--End procedure dbo.AssignWorkflowToContract

--Begin procedure dbo.DeactivatePersonByPersonID
EXEC Utility.DropObject 'dbo.DeactivatePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.11
-- Description:	A stored procedure to set the IsActive bit on a dbo.Person record based on a PersonID
-- ==================================================================================================
CREATE PROCEDURE dbo.DeactivatePersonByPersonID
@SourcePersonID INT = 0,
@TargetPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.Person
	SET IsActive = 0
	WHERE PersonID = @TargetPersonID

	EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = 'Deactivate', @SourcePersonID = @SourcePersonID

END
GO
--End procedure dbo.DeactivatePersonByPersonID

--Begin procedure dbo.DeleteContractByContractID
EXEC Utility.DropObject 'dbo.DeleteContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.22
-- Description:	A stored procedure to delete data to the dbo.Contract table based on a ContractID
-- ==============================================================================================
CREATE PROCEDURE dbo.DeleteContractByContractID
@ContractID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	EXEC dbo.AddEventLogContractEntry @ContractID = @ContractID, @EventCode = 'DeleteContract', @PersonID = @PersonID
	
	DELETE
	FROM dbo.Contract
	WHERE ContractID = @ContractID

	DELETE
	FROM dbo.ContractPlaceOfPerformance
	WHERE ContractID = @ContractID

	DELETE
	FROM dbo.ContractPointOfContact
	WHERE ContractID = @ContractID

	DELETE
	FROM dbo.ContractWorkflowPerson
	WHERE ContractID = @ContractID

END
GO
--End procedure dbo.DeleteContractByContractID

--Begin procedure dbo.DeleteWorkflowPersonByWorkflowPersonID
EXEC Utility.DropObject 'dbo.DeleteWorkflowPersonByWorkflowPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.09
-- Description:	A stored procedure to delete data from the dbo.WorkflowPerson table based on an WorkflowPersonID
-- =============================================================================================================
CREATE PROCEDURE dbo.DeleteWorkflowPersonByWorkflowPersonID
@WorkflowPersonID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE
	FROM dbo.WorkflowPerson
	WHERE WorkflowPersonID = @WorkflowPersonID

END
GO
--End procedure dbo.DeleteWorkflowPersonByWorkflowPersonID

--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		C.ContractDateEnd,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.GetContractEventLogDataByContractID
EXEC Utility.DropObject 'dbo.GetContractEventLogDataByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.11
-- Description:	A stored procedure to get data from the dbo.EventLog table based on a ContractID
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractEventLogDataByContractID
@ContractID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatDateTime(EL.CreateDateTime, 'mm/dd/yyyy hh:mm tt') AS CreateDateTimeFormatted,
		dbo.GetPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
	
		CASE
			WHEN EL.EventCode IN ('Create', 'Update')
			THEN EL.EventCode + 'd contract'
			WHEN EL.EventCode = 'AssignWorkflow'
			THEN 'Set workflow to step 1'
			WHEN EL.EventCode = 'IncrementWorkflow'
			THEN
				CASE
					WHEN (SELECT WS.WorkflowStatusCode FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusID = N.C.value('(WorkflowStatusID)[1]', 'INT')) = 'Approved'
					THEN 'Workflow complete, contract approved'
					ELSE 'Set workflow to step ' + N.C.value('(WorkflowStepNumber)[1]', 'VARCHAR')
				END
			ELSE EL.EventCode
		END AS ActionTaken,
	
		EL.Comments
	FROM dbo.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Contract') AS N(C)
	WHERE EL.EntityTypeCode = 'Contract'
		AND EL.EntityID = @ContractID
		AND EL.EventCode <> 'AssignWorkflowPersons'
	ORDER BY EL.EventLogID

END
GO
--End procedure dbo.GetContractEventLogDataByContractID

--Begin procedure dbo.GetContractPlacesOfPerformanceByContractID
EXEC Utility.DropObject 'dbo.GetContractPlacesOfPerformanceByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	A stored procedure to get data from the dbo.ContractPlaceOfPerformance table based on a ContractID
-- ===============================================================================================================
CREATE PROCEDURE dbo.GetContractPlacesOfPerformanceByContractID
@ContractID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CPOP.ContractPlaceOfPerformanceID, 
		CPOP.PlaceOfPerformanceCity, 
		CPOP.IsPrimary,
		S.StateCode
	FROM dbo.ContractPlaceOfPerformance CPOP
		JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
			AND CPOP.ContractID = @ContractID
	ORDER BY CPOP.IsPrimary DESC, CPOP.PlaceOfPerformanceCity, S.StateCode

END
GO
--End procedure dbo.GetContractPlacesOfPerformanceByContractID

--Begin procedure dbo.GetContractPointsOfContactByContractID
EXEC Utility.DropObject 'dbo.GetContractPointsOfContactByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.12.08
-- Description:	A stored procedure to get data from the dbo.ContractPointOfContact table based on a ContractID
-- ===========================================================================================================
CREATE PROCEDURE dbo.GetContractPointsOfContactByContractID
@ContractID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CPOC.ContractPointOfContactID,
		CPOC.PointOfContactAgency, 
		CPOC.PointOfContactEmail, 
		CPOC.PointOfContactName, 
		CPOC.PointOfContactPhone, 
		CPOC.PointOfContactTitle, 
		CPOC.IsPrimary
	FROM dbo.ContractPointOfContact CPOC
	WHERE CPOC.ContractID = @ContractID
	ORDER BY CPOC.IsPrimary DESC, CPOC.PointOfContactName, CPOC.ContractPointOfContactID

END
GO
--End procedure dbo.GetContractPointsOfContactByContractID

--Begin procedure dbo.GetContractsByDashboardFilter
EXEC Utility.DropObject 'dbo.GetContractsByDashboardFilter'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a dashboard filter
-- ===================================================================================================
CREATE PROCEDURE dbo.GetContractsByDashboardFilter
@DashboardFilter VARCHAR(100) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'ContractName',
@PageIndex INT = 1,
@PageSize INT = 50,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ApprovedWorkflowStatusID INT
	DECLARE @EntityTypeCode VARCHAR(50) = Utility.ListGetAt(@DashboardFilter, 1, '_')
	DECLARE @FilterCode VARCHAR(50) = Utility.ListGetAt(@DashboardFilter, 2, '_')
	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT
	
	DECLARE @tContract AS AccessControl.Contract
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetContractsByDashboardFilter', 'u')) IS NOT NULL
		DROP TABLE #tGetContractsByDashboardFilter
	--ENDIF
	
	CREATE TABLE #tGetContractsByDashboardFilter
		(
		ContractID INT PRIMARY KEY NOT NULL,
		AgeInDays INT NOT NULL DEFAULT 0,
		PercentComplete NUMERIC(18,2) NOT NULL DEFAULT 0,
		PlaceOfPerformanceCity VARCHAR(300)
		)

	IF @EntityTypeCode NOT IN ('WorkflowAgeing','WorkflowPercentComplete')
		BEGIN

		SELECT @ApprovedWorkflowStatusID = WS.WorkflowStatusID
		FROM Dropdown.WorkflowStatus WS
		WHERE WS.WorkflowStatusCode = 'Approved'
		
		END
	--ENDIF

	IF @EntityTypeCode IN ('ApprovedByOperatingDivision','InWorkByOperatingDivision')
		BEGIN
		
		INSERT INTO #tGetContractsByDashboardFilter
			(ContractID)
		SELECT 
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = C.OriginatingOrganizationID
				AND OH.TopLevelOrganizationID = CAST(@FilterCode AS INT)
				AND 
					(
					(@EntityTypeCode = 'ApprovedByOperatingDivision' AND C.WorkflowStatusID = @ApprovedWorkflowStatusID)
						OR (@EntityTypeCode = 'InWorkByOperatingDivision' AND C.WorkflowStatusID <> @ApprovedWorkflowStatusID)
					)

		END
	ELSE IF @EntityTypeCode IN ('ApprovedByFiscalYearContractRange','InWorkByFiscalYearContractRange')
		BEGIN
		
		INSERT INTO #tGetContractsByDashboardFilter
			(ContractID)
		SELECT 
			C.ContractID
		FROM dbo.Contract C
		WHERE C.FiscalYearContractRangeID = CAST(@FilterCode AS INT)
			AND
				(
				(@EntityTypeCode = 'ApprovedByFiscalYearContractRange' AND C.WorkflowStatusID = @ApprovedWorkflowStatusID)
					OR (@EntityTypeCode = 'InWorkByFiscalYearContractRange' AND C.WorkflowStatusID <> @ApprovedWorkflowStatusID)
				)
		
		END
	ELSE IF @EntityTypeCode IN ('ApprovedByTotalContractRange','InWorkByTotalContractRange')
		BEGIN
		
		INSERT INTO #tGetContractsByDashboardFilter
			(ContractID)
		SELECT 
			C.ContractID
		FROM dbo.Contract C
		WHERE C.TotalContractRangeID = CAST(@FilterCode AS INT)
			AND
				(
				(@EntityTypeCode = 'ApprovedByTotalContractRange' AND C.WorkflowStatusID = @ApprovedWorkflowStatusID)
					OR (@EntityTypeCode = 'InWorkByTotalContractRange' AND C.WorkflowStatusID <> @ApprovedWorkflowStatusID)
				)
		
		END
	ELSE IF @EntityTypeCode IN ('WorkflowAgeing','WorkflowPercentComplete')
		BEGIN
		
		INSERT INTO #tGetContractsByDashboardFilter
			(ContractID, AgeInDays, PercentComplete)
		SELECT 
			DWRD.ContractID,
			DWRD.AgeInDays, 
			DWRD.PercentComplete
		FROM dbo.GetDashboardWorkflowRawData() DWRD

		IF @EntityTypeCode = 'WorkflowAgeing'
			BEGIN
	
			IF @FilterCode = 'LTE07'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays NOT BETWEEN 0 AND 7
		
				END
			--ENDIF
			IF @FilterCode = 'LTE14'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays NOT BETWEEN 8 AND 14
		
				END
			--ENDIF
			IF @FilterCode = 'LTE30'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays NOT BETWEEN 15 AND 30
		
				END
			--ENDIF
			IF @FilterCode = 'LTE60'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays NOT BETWEEN 31 AND 60
		
				END
			--ENDIF
			IF @FilterCode = 'LTE90'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays NOT BETWEEN 61 AND 90
		
				END
			--ENDIF
			IF @FilterCode = 'GT90'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.AgeInDays > 90
		
				END
			--ENDIF

			END
		ELSE IF @EntityTypeCode = 'WorkflowPercentComplete'
			BEGIN

			IF @FilterCode = 'LTE25'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.PercentComplete NOT BETWEEN 0 AND 25
		
				END
			--ENDIF
			IF @FilterCode = 'LTE50'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.PercentComplete NOT BETWEEN 26 AND 50
		
				END
			--ENDIF
			IF @FilterCode = 'LTE75'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.PercentComplete NOT BETWEEN 51 AND 75
		
				END
			--ENDIF
			IF @FilterCode = 'GT75'
				BEGIN
		
				DELETE T1
				FROM #tGetContractsByDashboardFilter T1
				WHERE T1.PercentComplete > 75
		
				END
			--ENDIF

			END
		--ENDIF	

		END
	--ENDIF
	
	SELECT 
		@TotalRecordCount = COUNT(T1.ContractID)			
	FROM #tGetContractsByDashboardFilter T1
	
	IF @OrderByField = 'PlaceOfPerformanceCity'
		BEGIN
	
		UPDATE T1
		SET 
			T1.PlaceOfPerformanceCity = 
				CASE
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
					THEN 'Multiple'
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
					THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
					ELSE ''
				END
	
		FROM #tGetContractsByDashboardFilter T1
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
	
		END
	--ENDIF
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/
	
	IF @PageSize > 0
		BEGIN
	
		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)
	
		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
						CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
							T1.ContractID ASC
					) AS RowIndex,
				C.ContractID
			FROM #tGetContractsByDashboardFilter T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
				JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
				JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
				JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID				
			)
	
		DELETE T1
		FROM #tGetContractsByDashboardFilter T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.ContractID = T1.ContractID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)
	
		END
	--ENDIF
	
	INSERT INTO @tContract (ContractID) SELECT T1.ContractID FROM #tGetContractsByDashboardFilter T1
	
	SELECT
		@PageCount AS PageCount,
		@PageIndex AS PageIndex,
		@TotalRecordCount AS TotalRecordCount,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		C.CompetitionTypeID, 
		C.ContractDateEnd, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		LEFT(C.ContractDescription, 50) + 
			CASE
				WHEN LEN(RTRIM(C.ContractDescription)) > 50
				THEN '...' 
				ELSE ''
			END AS ContractDescription, 
				
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.IncumbentContractorName, 
		C.NAICSID, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TotalContractRangeID,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		RT.RequirementTypeName,
	
		CASE 
			WHEN @OrderByField = 'PlaceOfPerformanceCity'
			THEN T1.PlaceOfPerformanceCity
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
			THEN 'Multiple'
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
			THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
			ELSE ''
		END AS PlaceOfPerformanceCity
	
	FROM #tGetContractsByDashboardFilter T1
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
		JOIN dbo.Contract C ON C.ContractID = T1.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
	ORDER BY 
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
		CASE WHEN @OrderByField = 'ContractDateEnd' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
		CASE WHEN @OrderByField = 'ContractDateEnd' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC,
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
		CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
			T1.ContractID ASC
	
	DROP TABLE #tGetContractsByDashboardFilter
	
END
GO
--End procedure dbo.GetContractsByDashboardFilter

--Begin procedure dbo.GetContractsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetContractsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on search criteria
-- ================================================================================================
CREATE PROCEDURE dbo.GetContractsBySearchCriteria
@CompetitionTypeIDList VARCHAR(MAX) = NULL,
@ContractDateEndStart DATE = NULL,
@ContractDateEndStop DATE = NULL,
@ContractDateStartStart DATE = NULL,
@ContractDateStartStop DATE = NULL,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationIDList VARCHAR(MAX) = NULL,
@ContractName VARCHAR(250) = NULL,
@ContractTypeIDList VARCHAR(MAX) = NULL,
@EstimatedSolicitationQuarterList VARCHAR(MAX) = NULL,
@FiscalYearContractRangeIDList VARCHAR(MAX) = NULL,
@FundingOrganizationIDList VARCHAR(MAX) = NULL,
@Keyword VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'ContractName',
@OrganizationIDList VARCHAR(MAX) = NULL,
@PageIndex INT = 1,
@PageSize INT = 50,
@PendingApprovalPersonID INT = 0,
@PersonID INT = 0,
@PlaceOfPerformanceStateIDList VARCHAR(MAX) = NULL,
@PointOfContactAgency VARCHAR(250) = NULL,
@PointOfContactEmail VARCHAR(320) = NULL,
@PointOfContactName VARCHAR(250) = NULL,
@PointOfContactTitle VARCHAR(50) = NULL,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL,
@RequirementTypeIDList VARCHAR(MAX) = NULL,
@TargetAwardDateStart DATE = NULL,
@TargetAwardDateStop DATE = NULL,
@TargetAwardFiscalYearList VARCHAR(MAX) = NULL,
@TotalContractRangeIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT
	
	DECLARE @tContract AS AccessControl.Contract
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetContractsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetContractsBySearchCriteria
	--ENDIF
		
	CREATE TABLE #tGetContractsBySearchCriteria
		(
		ContractID INT PRIMARY KEY NOT NULL,
		PlaceOfPerformanceCity VARCHAR(300)
		)
	
	IF @Keyword IS NULL
		BEGIN
	
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
	
		--Date Fields
		IF @ContractDateEndStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd < @ContractDateEndStart
	
			END
		--ENDIF
		IF @ContractDateEndStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd >= @ContractDateEndStop
	
			END
		--ENDIF
		IF @ContractDateStartStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart < @ContractDateStartStart
	
			END
		--ENDIF
		IF @ContractDateStartStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart >= @ContractDateStartStop
	
			END
		--ENDIF
		IF @TargetAwardDateStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate < @TargetAwardDateStart
	
			END
		--ENDIF
		IF @TargetAwardDateStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate > @TargetAwardDateStop
	
			END
		--ENDIF
	
		--Dropdown Fields
		IF @CompetitionTypeIDList IS NOT NULL AND LEN(RTRIM(@CompetitionTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@CompetitionTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.CompetitionTypeID
						)
	
			END
		--ENDIF
		IF @ContractingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@ContractingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractingOrganizationID
						)
	
			END
		--ENDIF
		IF @ContractTypeIDList IS NOT NULL AND LEN(RTRIM(@ContractTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractTypeID
						)
	
			END
		--ENDIF
		IF @EstimatedSolicitationQuarterList IS NOT NULL AND LEN(RTRIM(@EstimatedSolicitationQuarterList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@EstimatedSolicitationQuarterList, ',', 0) LTT 
						WHERE LTT.ListItem = C.EstimatedSolicitationQuarter
						)
	
			END
		--ENDIF
		IF @FiscalYearContractRangeIDList IS NOT NULL AND LEN(RTRIM(@FiscalYearContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FiscalYearContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FiscalYearContractRangeID
						)
	
			END
		--ENDIF
		IF @FundingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@FundingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FundingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FundingOrganizationID
						)
	
			END
		--ENDIF
		IF @NAICSID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.NAISCID <> @NAICSID
	
			END
		--ENDIF
		IF @OrganizationIDList IS NOT NULL AND LEN(RTRIM(@OrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@OrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.OriginatingOrganizationID
						)
	
			END
		--ENDIF
		IF @PlaceOfPerformanceStateIDList IS NOT NULL AND LEN(RTRIM(@PlaceOfPerformanceStateIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@PlaceOfPerformanceStateIDList, ',', 0) LTT 
							JOIN dbo.ContractPlaceOfPerformance CPOP ON CPOP.PlaceOfPerformanceStateID = LTT.ListItem
								AND CPOP.ContractID = C.ContractID
						)
	
			END
		--ENDIF
		IF @PSCID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.PSCID <> @PSCID
	
			END
		--ENDIF
		IF @RequirementTypeIDList IS NOT NULL AND LEN(RTRIM(@RequirementTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@RequirementTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.RequirementTypeID
						)
	
			END
		--ENDIF
		IF @TargetAwardFiscalYearList IS NOT NULL AND LEN(RTRIM(@TargetAwardFiscalYearList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TargetAwardFiscalYearList, ',', 0) LTT 
						WHERE LTT.ListItem = dbo.GetFiscalYearFromDate(C.TargetAwardDate)
						)
	
			END
		--ENDIF
		IF @TotalContractRangeIDList IS NOT NULL AND LEN(RTRIM(@TotalContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TotalContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.TotalContractRangeID
						)
	
			END
		--ENDIF
	
		-- Text Fields (individual)
		IF @ContractingOfficeName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractingOfficeName NOT LIKE '%' + @ContractingOfficeName + '%'
	
			END
		--ENDIF
		IF @ContractName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractName NOT LIKE '%' + @ContractName + '%'
					AND 
						(
						C.ContractDescription IS NULL
							OR C.ContractDescription NOT LIKE '%' + @ContractName + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactAgency IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactAgency LIKE '%' + @PointOfContactAgency + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactEmail IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactEmail LIKE '%' + @PointOfContactEmail + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactName LIKE '%' + @PointOfContactName + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactTitle IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactTitle LIKE '%' + @PointOfContactTitle + '%'
						)
	
			END
		--ENDIF
		IF @ReferenceIDV IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ReferenceIDV NOT LIKE '%' + @ReferenceIDV + '%'
	
			END
		--ENDIF
	
		-- Workflow field
		IF @PendingApprovalPersonID IS NOT NULL AND @PendingApprovalPersonID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractWorkflowPerson CWP
						WHERE CWP.ContractID = C.ContractID
							AND CWP.WorkflowStepNumber = C.WorkflowStepNumber
							AND CWP.PersonID = @PendingApprovalPersonID
						)
	
			END
		--ENDIF
	
		END
	ELSE
		BEGIN
	
		SET @Keyword = '%' + @Keyword + '%'

		-- Text Fields (multiple)
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND
					(
					C.ContractingOfficeName LIKE @Keyword
						OR C.ContractName LIKE @Keyword
						OR C.ContractDescription LIKE @Keyword
						OR C.IncumbentContractorName LIKE @Keyword
						OR C.ReferenceIDV LIKE @Keyword
						OR C.TransactionNumber LIKE @Keyword
						OR EXISTS
							(
							SELECT 1
							FROM dbo.ContractPointOfContact CPOC
							WHERE CPOC.ContractID = C.ContractID
								AND 
									(
									CPOC.PointOfContactEmail LIKE @Keyword
										OR CPOC.PointOfContactName LIKE @Keyword
										OR CPOC.PointOfContactPhone LIKE @Keyword
									)
							)
/*
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.CompetitionType CT
							WHERE CT.CompetitionTypeID = C.CompetitionTypeID
								AND CT.CompetitionTypeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.ContractRange CR
							WHERE 
								(
								CR.ContractRangeID = C.FiscalYearContractRangeID
									OR CR.ContractRangeID = C.TotalContractRangeID
								)
								AND CR.ContractRangeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.ContractType CT
							WHERE CT.ContractTypeID = C.ContractTypeID
								AND CT.ContractTypeName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.NAICS N 
							WHERE N.NAICSID = C.NAICSID
								AND
									(
									N.NAICSCode LIKE @Keyword
										OR N.NAICSName LIKE @Keyword
									)
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.Organization O
							WHERE 
								(
								O.OrganizationID = C.ContractingOrganizationID
									OR O.OrganizationID = C.FundingOrganizationID
									OR O.OrganizationID = C.OriginatingOrganizationID
								)
								AND O.OrganizationName LIKE @Keyword
							)
						OR EXISTS
							(
							SELECT 1 
							FROM Dropdown.RequirementType RT 
							WHERE RT.RequirementTypeID = C.RequirementTypeID
								AND RT.RequirementTypeName LIKE @Keyword
							)
*/
						OR EXISTS
							(
							SELECT 1 
							FROM dbo.ContractPlaceOfPerformance CPOP 
								JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
									AND CPOP.ContractID = C.ContractID
									AND 
										(
										CPOP.PlaceOfPerformanceCity LIKE @Keyword
											OR S.StateCode LIKE @Keyword
											OR S.StateName LIKE @Keyword
										)
							)
					)
	
		END
	--ENDIF
	
	SELECT 
		@TotalRecordCount = COUNT(T1.ContractID)			
	FROM #tGetContractsBySearchCriteria T1
	
	IF @OrderByField = 'PlaceOfPerformanceCity'
		BEGIN
	
		UPDATE T1
		SET 
			T1.PlaceOfPerformanceCity = 
				CASE
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
					THEN 'Multiple'
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
					THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
					ELSE ''
				END
	
		FROM #tGetContractsBySearchCriteria T1
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
	
		END
	--ENDIF
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/
	
	IF @PageSize > 0
		BEGIN
	
		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)
	
		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
						CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
							T1.ContractID ASC
					) AS RowIndex,
				C.ContractID
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
				JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
				JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
				JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID				
			)
	
		DELETE T1
		FROM #tGetContractsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.ContractID = T1.ContractID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)
	
		END
	--ENDIF
	
	INSERT INTO @tContract (ContractID) SELECT T1.ContractID FROM #tGetContractsBySearchCriteria T1
	
	SELECT
		@PageCount AS PageCount,
		@PageIndex AS PageIndex,
		@TotalRecordCount AS TotalRecordCount,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		C.CompetitionTypeID, 
		LEFT(C.ContractDescription, 50) + 
			CASE
				WHEN LEN(RTRIM(C.ContractDescription)) > 50
				THEN '...' 
				ELSE ''
			END AS ContractDescription, 
				
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.IncumbentContractorName, 
		C.NAICSID, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TotalContractRangeID,
		C.ContractDateEnd, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		RT.RequirementTypeName,
	
		CASE 
			WHEN @OrderByField = 'PlaceOfPerformanceCity'
			THEN T1.PlaceOfPerformanceCity
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
			THEN 'Multiple'
			WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
			THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
			ELSE ''
		END AS PlaceOfPerformanceCity
	
	FROM #tGetContractsBySearchCriteria T1
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
		JOIN dbo.Contract C ON C.ContractID = T1.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
	ORDER BY 
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
		CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
		CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
		CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
		CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
		CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
		CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
		CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
		CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
			T1.ContractID ASC
	
	DROP TABLE #tGetContractsBySearchCriteria
	
END
GO
--End procedure dbo.GetContractsBySearchCriteria

--Begin procedure dbo.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber
EXEC Utility.DropObject 'dbo.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.09
-- Description:	A stored procedure to get data from the dbo.ContractWorkflowPerson table based on a ContractID and a WorkflowStepNumber
-- ====================================================================================================================================
CREATE PROCEDURE dbo.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber
@ContractID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CWP.ContractID,
		CWP.WorkflowStepNumber,
		O.OrganizationName,
		P.EmailAddress,
		P.PersonID,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.Phone
	FROM dbo.ContractWorkflowPerson CWP
		JOIN dbo.Person P ON P.PersonID = CWP.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND CWP.ContractID = @ContractID
			AND CWP.WorkflowStepNumber = @WorkflowStepNumber
	ORDER BY CWP.WorkflowStepNumber, 6, P.PersonID
		
END
GO
--End procedure dbo.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber

--Begin procedure dbo.GetDashboardContractRangeData
EXEC Utility.DropObject 'dbo.GetDashboardContractRangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardContractRangeData
@ContractRangeTypeCode VARCHAR(50),
@WorkflowStatusCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @ContractRangeTypeCode = 'FiscalYear'
		BEGIN
		
		SELECT
			CR.ContractRangeID AS ItemCode,
			CR.ContractRangeName AS ItemName,
			COUNT(C.ContractID) AS ItemCount
		FROM dbo.Contract C
			JOIN Dropdown.ContractRange CR ON CR.ContractRangeID = C.FiscalYearContractRangeID
				AND CR.ContractRangeID > 0
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND EXISTS
					(
					SELECT 1
					FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
					WHERE LTT.ListItem = WS.WorkflowStatusCode
					)
		GROUP BY CR.ContractRangeID, CR.ContractRangeName
		ORDER BY CR.ContractRangeName

		END
	ELSE IF @ContractRangeTypeCode = 'Total'
		BEGIN
		
		SELECT
			CR.ContractRangeID AS ItemCode,
			CR.ContractRangeName AS ItemName,
			COUNT(C.ContractID) AS ItemCount
		FROM dbo.Contract C
			JOIN Dropdown.ContractRange CR ON CR.ContractRangeID = C.TotalContractRangeID
				AND CR.ContractRangeID > 0
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND EXISTS
					(
					SELECT 1
					FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
					WHERE LTT.ListItem = WS.WorkflowStatusCode
					)
		GROUP BY CR.ContractRangeID, CR.ContractRangeName
		ORDER BY CR.ContractRangeName

		END
	--ENDIF

END
GO
--End procedure dbo.GetDashboardContractRangeData
	
--Begin procedure dbo.GetDashboardContractsByOperatingDivisionData
EXEC Utility.DropObject 'dbo.GetDashboardContractsByOperatingDivisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardContractsByOperatingDivisionData
@WorkflowStatusCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		O.OrganizationID AS ItemCode,
		O.OrganizationName AS ItemName,
		COUNT(C.ContractID) AS ItemCount
	FROM dbo.Contract C
		JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.Organization O ON O.OrganizationID = OH.TopLevelOrganizationID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			AND EXISTS
				(
				SELECT 1
				FROM Utility.ListToTable(@WorkflowStatusCodeList, ',', 0) LTT 
				WHERE LTT.ListItem = WS.WorkflowStatusCode
				)
	GROUP BY O.OrganizationID, O.OrganizationName
	ORDER BY O.OrganizationName

END
GO
--End procedure dbo.GetDashboardContractsByOperatingDivisionData
	
--Begin procedure dbo.GetDashboardWorkflowData
EXEC Utility.DropObject 'dbo.GetDashboardWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.15
-- Description:	A stored procedure to get dashboard data from the PFDR database
-- ============================================================================
CREATE PROCEDURE dbo.GetDashboardWorkflowData
@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
			
	DECLARE @tTable TABLE (ContractID INT NOT NULL PRIMARY KEY, AgeInDays INT, PercentComplete NUMERIC(18,2))

	INSERT INTO @tTable
		(ContractID, AgeInDays, PercentComplete)
	SELECT 
		DWRD.ContractID,
		DWRD.AgeInDays,
		DWRD.PercentComplete
	FROM dbo.GetDashboardWorkflowRawData() DWRD

	IF @EntityTypeCode = 'WorkflowAgeing'
		BEGIN
		
		SELECT
			1 AS DisplayOrder,
			'LTE07' AS ItemCode,
			'<= 7' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 0 AND 7
	
		UNION
	
		SELECT 
			2 AS DisplayOrder, 
			'LTE14' AS ItemCode,
			'<= 14' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 8 AND 14
	
		UNION
	
		SELECT 
			3 AS DisplayOrder, 
			'LTE30' AS ItemCode,
			'<= 30' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 15 AND 30
	
		UNION
	
		SELECT 
			4 AS DisplayOrder, 
			'LTE60' AS ItemCode,
			'<= 60' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 31 AND 60
	
		UNION
	
		SELECT 
			5 AS DisplayOrder, 
			'LTE90' AS ItemCode,
			'<= 90' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays BETWEEN 61 AND 90
	
		UNION
	
		SELECT 
			6 AS DisplayOrder, 
			'GT90' AS ItemCode,
			'> 90' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.AgeInDays > 90
	
		ORDER BY DisplayOrder

		END
	ELSE IF @EntityTypeCode = 'WorkflowPercentComplete'	
		BEGIN
		
		SELECT 
			1 AS DisplayOrder, 
			'LTE25' AS ItemCode,
			'<= 25%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 0 AND 25
	
		UNION
	
		SELECT 
			2 AS DisplayOrder, 
			'LTE50' AS ItemCode,
			'<= 50%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 26 AND 50
	
		UNION
	
		SELECT 
			3 AS DisplayOrder, 
			'LTE75' AS ItemCode,
			'<= 75%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete BETWEEN 51 AND 75
	
		UNION
	
		SELECT 
			4 AS DisplayOrder, 
			'GT75' AS ItemCode,
			'> 75%' AS ItemName,
			COUNT(T.ContractID) AS ItemCount 
		FROM @tTable T 
		WHERE T.PercentComplete > 75
	
		ORDER BY DisplayOrder

		END
	--ENDIF

END
GO
--End procedure dbo.GetDashboardWorkflowData

--Begin procedure dbo.GetPersonByHTTPContext
EXEC Utility.DropObject 'dbo.GetPersonByHTTPContext'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.01.23
-- Description:	A stored procedure to get data from the dbo.Person table based on an EmailAddress or a UserName
-- ============================================================================================================
CREATE PROCEDURE dbo.GetPersonByHTTPContext
@EmailAddress VARCHAR(320) = NULL,
@UserName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PersonID, 
		P.EmailAddress, 
		P.UserName,
		dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.PersonID = P.PersonID AND WP.WorkflowStepNumber = 1)
			THEN 1
			ELSE 0
		END AS IsOriginator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'OrganizationAdministrator')
			THEN 1
			ELSE 0
		END AS IsAdministrator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'SuperAdministrator')
			THEN 1
			ELSE 0
		END AS IsSuperAdministrator

	FROM dbo.Person P
	WHERE P.IsActive = 1
		AND
			(
				(@EmailAddress IS NULL OR P.EmailAddress = @EmailAddress)
					OR (@UserName IS NULL OR P.UserName = @UserName)
			)
	
END
GO
--End procedure dbo.GetPersonByHTTPContext

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.24
-- Description:	A stored procedure to get data from the dbo.Person table based on a PersonID
-- =========================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID
@SourcePersonID INT = 0,
@TargetPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPerson AS AccessControl.Person

	INSERT INTO @tPerson (PersonID) SELECT P.PersonID FROM dbo.Person P WHERE P.PersonID = @TargetPersonID
	
	SELECT
		O.OrganizationName, 		
		P.EmailAddress, 		
		P.FirstName, 		
		P.IsActive, 		
		P.LastName, 		
		P.OrganizationID,
		P.PersonID,		
		P.Phone, 		
		P.Title,
		P.UserName,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'SuperAdministrator')
			THEN 1
			ELSE 0
		END AS IsSuperAdministrator,

		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM dbo.Person P
		JOIN AccessControl.GetPersonRecordsByPersonID(@SourcePersonID, @tPerson) UR ON UR.PersonID = P.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetPersonRolesByPersonID
EXEC Utility.DropObject 'dbo.GetPersonRolesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.22
-- Description:	A stored procedure to get data from the dbo.PersonRole table based on a PersonID
-- =============================================================================================
CREATE PROCEDURE dbo.GetPersonRolesByPersonID
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		R.RoleID,
		R.RoleName,
		O.OrganizationID,
		O.OrganizationName
	FROM dbo.PersonRole PR
		JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = PR.OrganizationID
	ORDER BY O.OrganizationName, O.OrganizationID

END
GO
--End procedure dbo.GetPersonRolesByPersonID

--Begin procedure dbo.GetPersonsByPersonIDList
EXEC Utility.DropObject 'dbo.GetPersonsByPersonIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.03
-- Description:	A stored procedure to get data from the dbo.Person table based on a PersonID
-- =========================================================================================
CREATE PROCEDURE dbo.GetPersonsByPersonIDList
@PersonIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.OrganizationName, 		
		P.PersonID,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,		
		P.Phone
	FROM dbo.Person P
		JOIN Utility.ListToTable(@PersonIDList, ',', 0) LTT ON CAST(LTT.ListItem AS INT) = P.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID

END
GO
--End procedure dbo.GetPersonsByPersonIDList

--Begin procedure dbo.GetPersonsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetPersonsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.22
-- Description:	A stored procedure to get data from the dbo.Person table based on search criteria
-- ==============================================================================================
CREATE PROCEDURE dbo.GetPersonsBySearchCriteria
@EmailAddress VARCHAR(320) = NULL,
@FirstLetter CHAR(1) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@Keyword VARCHAR(MAX) = NULL,
@LastName VARCHAR(50) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'LastName',
@OrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PersonID INT = 0,
@Phone VARCHAR(50) = NULL,
@Title VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT

	DECLARE @tPerson AS AccessControl.Person

	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetPersonsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetPersonsBySearchCriteria
	--ENDIF
	
	CREATE TABLE #tGetPersonsBySearchCriteria
		(
		PersonID INT PRIMARY KEY NOT NULL
		)

	IF @Keyword IS NOT NULL
		SET @Keyword = '%' + @Keyword + '%'
	--ENDIF

	IF @Keyword IS NULL
		BEGIN

		INSERT INTO #tGetPersonsBySearchCriteria
			(PersonID)
		SELECT
			P.PersonID
		FROM dbo.Person P
		WHERE P.IsActive = @IsActive

		IF @EmailAddress IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.EmailAddress NOT LIKE '%' + @EmailAddress + '%'
	
			END
		--ENDIF
		IF @FirstLetter IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND LEFT(P.LastName, 1) <> @FirstLetter
	
			END
		--ENDIF
		IF @FirstName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.FirstName NOT LIKE '%' + @FirstName + '%'
	
			END
		--ENDIF
		IF @LastName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.LastName NOT LIKE '%' + @LastName + '%'
	
			END
		--ENDIF
		IF @Phone IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.Phone NOT LIKE '%' + @Phone + '%'
	
			END
		--ENDIF
		IF @Title IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.Title NOT LIKE '%' + @Title + '%'
	
			END
		--ENDIF

		-- Integer Fields (individual)
		IF @OrganizationID > 0
			BEGIN

			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
				JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = P.OrganizationID
					AND OH.TopLevelOrganizationID = @OrganizationID
	
			END
		--ENDIF

		END
	ELSE
		BEGIN

		SET @Keyword = '%' + @Keyword + '%'
	
		-- Text Fields (multiple)
		INSERT INTO #tGetPersonsBySearchCriteria
			(PersonID)
		SELECT
			P.PersonID
		FROM dbo.Person P
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = P.OrganizationID
				AND 
				(
				OH.OrganizationName LIKE @Keyword
					OR P.EmailAddress LIKE @Keyword
					OR P.FirstName LIKE @Keyword
					OR P.LastName LIKE @Keyword
					OR P.Phone LIKE @Keyword
					OR P.Title LIKE @Keyword
				)

		END
	--ENDIF

	SELECT 
		@TotalRecordCount = COUNT(T1.PersonID)			
	FROM #tGetPersonsBySearchCriteria T1

	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	IF @PageSize > 0
		BEGIN

		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)

		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
						CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
						CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
						CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
						CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
						CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
						CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
						CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
						CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
						CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
						CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'ASC' THEN P.Phone END ASC,
						CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'DESC' THEN P.Phone END DESC,
						CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'ASC' THEN P.Title END ASC,
						CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'DESC' THEN P.Title END DESC,
						CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
						CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
							T1.PersonID ASC
					) AS RowIndex,
				T1.PersonID
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
				JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			)

		DELETE T1
		FROM #tGetPersonsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.PersonID = T1.PersonID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)

		END
	--ENDIF
	
	INSERT INTO @tPerson (PersonID) SELECT T1.PersonID FROM #tGetPersonsBySearchCriteria T1

	SELECT
		@PageCount AS PageCount,
		@PageIndex AS PageIndex,
		@TotalRecordCount AS TotalRecordCount,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		O.OrganizationName,
		P.IsActive,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PersonID, 
		P.EmailAddress,
		P.Phone,
		P.Title
	FROM #tGetPersonsBySearchCriteria T1
		JOIN AccessControl.GetPersonRecordsByPersonID(@PersonID, @tPerson) UR ON UR.PersonID = T1.PersonID
		JOIN dbo.Person P ON P.PersonID = T1.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
	ORDER BY 
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'ASC' THEN P.Phone END ASC,
		CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'DESC' THEN P.Phone END DESC,
		CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'ASC' THEN P.Title END ASC,
		CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'DESC' THEN P.Title END DESC,
		CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
		CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
			T1.PersonID ASC

	DROP TABLE #tGetPersonsBySearchCriteria

END
GO
--End procedure dbo.GetPersonsBySearchCriteria

--Begin procedure dbo.GetRolesByPersonID
EXEC Utility.DropObject 'dbo.GetRolesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.22
-- Description:	A stored procedure to get data from the Dropdown.Role table based on a PersonID
-- ============================================================================================
CREATE PROCEDURE dbo.GetRolesByPersonID
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		R1.RoleID,
		R1.RoleCode,
		R1.RoleName
	FROM Dropdown.Role R1
	WHERE R1.RoleID > 0
		AND R1.RoleCode <> 'SuperAdministrator'
		AND R1.RoleWeight <=
			(
			SELECT MAX(R2.RoleWeight)
			FROM dbo.PersonRole PR
				JOIN Dropdown.Role R2 ON R2.RoleID = PR.RoleID
					AND PR.PersonID = @PersonID
			)
	ORDER BY R1.DisplayOrder, R1.RoleName, R1.RoleID

END
GO
--End procedure dbo.GetRolesByPersonID

--Begin procedure dbo.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber
EXEC Utility.DropObject 'dbo.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.03
-- Description:	A stored procedure to get data from the dbo.WorkflowPerson table based on a WorkflowID and a WorkflowStepNumber
-- ============================================================================================================================
CREATE PROCEDURE dbo.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber
@WorkflowID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		@WorkflowID AS WorkflowID,
		@WorkflowStepNumber AS WorkflowStepNumber,
		O.OrganizationName,
		P.EmailAddress,
		P.PersonID,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.Phone
	FROM dbo.WorkflowPerson WP
		JOIN dbo.Person P ON P.PersonID = WP.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = @WorkflowStepNumber
	ORDER BY 2, 6, P.PersonID
		
END
GO
--End procedure dbo.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber

--Begin procedure dbo.GetWorkflowStepNumberByContractID
EXEC Utility.DropObject 'dbo.GetWorkflowStepNumberByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.09
-- Description:	A stored procedure to validate a workflow step number for a contract
-- =================================================================================
CREATE PROCEDURE dbo.GetWorkflowStepNumberByContractID
@ContractID INT = 0,

@ReturnValue INT OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT @ReturnValue = C.WorkflowStepNumber
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID
	
END
GO
--End procedure dbo.GetWorkflowStepNumberByContractID

--Begin procedure dbo.GetWorkflowStepsByWorkflowID
EXEC Utility.DropObject 'dbo.GetWorkflowStepsByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.08
-- Description:	A stored procedure to get data from the dbo.WorkflowPerson table based on a WorkflowID
-- ===================================================================================================
CREATE PROCEDURE dbo.GetWorkflowStepsByWorkflowID
@WorkflowID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		WorkflowStepPersonCount INT NOT NULL DEFAULT 0,
		WorkflowID INT NOT NULL DEFAULT 0,
		WorkflowStepNumber INT NOT NULL DEFAULT 0,
		IsFirst BIT NOT NULL DEFAULT 0,
		IsLast BIT NOT NULL DEFAULT 0
		)
	
	IF NOT EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.WorkflowID = @WorkflowID)
		BEGIN

		INSERT INTO @tTable
			(WorkflowStepPersonCount,WorkflowID,WorkflowStepNumber)
		VALUES
			(0,@WorkflowID,1),
			(0,@WorkflowID,2)
		
		END
	ELSE
		BEGIN
		
		INSERT INTO @tTable
			(WorkflowStepPersonCount,WorkflowID,WorkflowStepNumber)
		SELECT
			COUNT(WP.WorkflowPersonID),
			WP.WorkflowID,
			WP.WorkflowStepNumber
		FROM dbo.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
		GROUP BY 
			WP.WorkflowID,
			WP.WorkflowStepNumber
		ORDER BY WP.WorkflowStepNumber

		END
	--ENDIF

	UPDATE @tTable 
	SET 
		IsFirst = 
			CASE 
				WHEN RowIndex = 1
				THEN 1
				ELSE 0
			END,
		
		IsLast = 
			CASE 
				WHEN RowIndex = (SELECT MAX(T.RowIndex) FROM @tTable T)
				THEN 1
				ELSE 0
			END

	SELECT
		T.WorkflowStepPersonCount,
		T.WorkflowID,
		T.RowIndex AS WorkflowStepNumber,
		(SELECT COUNT(T.RowIndex) FROM @tTable T) AS WorkflowStepCount,
		T.IsFirst,
		T.IsLast
	FROM @tTable T
	ORDER BY RowIndex
		
END
GO
--End procedure dbo.GetWorkflowStepsByWorkflowID

--Begin procedure dbo.GetWorkflowsByOrganizationID
EXEC Utility.DropObject 'dbo.GetWorkflowsByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.24
-- Description:	A stored procedure to get data from the dbo.Workflow table based on an OrganizationID
-- ==================================================================================================
CREATE PROCEDURE dbo.GetWorkflowsByOrganizationID
@OrganizationID INT = 0,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tWorkflow AS AccessControl.Workflow
	
	INSERT INTO @tWorkflow (WorkflowID) SELECT W.WorkflowID FROM dbo.Workflow W WHERE W.OrganizationID = @OrganizationID

	SELECT
		W.WorkflowID,
		W.OrganizationID,
		O.OrganizationName,
		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM dbo.Workflow W
		JOIN AccessControl.GetWorkflowRecordsByPersonID(@PersonID, @tWorkflow) UR ON UR.WorkflowID = W.WorkflowID
		JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID

END
GO
--End procedure dbo.GetWorkflowsByOrganizationID

--Begin procedure dbo.IncrementWorkflow
EXEC Utility.DropObject 'dbo.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.06
-- Description:	A stored procedure to move a contract record to the next step in its workflow
-- ==========================================================================================
CREATE PROCEDURE dbo.IncrementWorkflow
@Comments VARCHAR(MAX),
@ContractID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentWorkflowStepNumber INT
	DECLARE @FinalWorkflowStepNumber INT
	
	SELECT
		@CurrentWorkflowStepNumber = C.WorkflowStepNumber,
		@FinalWorkflowStepNumber = (SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID)
	FROM dbo.Contract C
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			AND C.ContractID = @ContractID

	UPDATE C
	SET 
		C.WorkflowStatusID = 
			CASE 
				WHEN @CurrentWorkflowStepNumber < @FinalWorkflowStepNumber
				THEN (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'PendingApproval')
				ELSE (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'Approved')
			END,

		C.WorkflowStepNumber = @CurrentWorkflowStepNumber + 1
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID

	EXEC dbo.AddEventLogContractEntry @Comments = @Comments, @ContractID = @ContractID, @EventCode = 'IncrementWorkflow', @PersonID = @PersonID

	SELECT
		@CurrentWorkflowStepNumber + 1 AS CurrentWorkflowStepNumber,
		@FinalWorkflowStepNumber AS FinalWorkflowStepNumber
	
END
GO
--End procedure dbo.IncrementWorkflow

--Begin procedure dbo.SaveContract
EXEC Utility.DropObject 'dbo.SaveContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Contract table based on a ContractID
-- ============================================================================================
CREATE PROCEDURE dbo.SaveContract
@ActiveContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@ActiveContractPointOfContactIDList VARCHAR(MAX) = '',
@CompetitionTypeID INT = 0,
@ContractDateEnd DATE = NULL,
@ContractDateStart DATE = NULL,
@ContractDescription VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationID INT = 0, 
@ContractName VARCHAR(250) = NULL,
@ContractTypeID INT = 0,
@DeletedContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@DeletedContractPointOfContactIDList VARCHAR(MAX) = '',
@EstimatedSolicitationQuarter INT = 0,
@FiscalYearContractRangeID INT = 0,
@FundingOrganizationID INT = 0, 
@IncumbentContractorName VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OriginatingOrganizationID INT = 0, 
@PersonID INT = 0,
@PrimaryContractPlaceOfPerformanceID INT = 0,
@PrimaryContractPointOfContactID INT = 0,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL, 
@RequirementTypeID INT = 0,
@TargetAwardDate DATE = NULL,
@TotalContractRangeID INT = 0,
@TransactionNumber VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @ContractID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput TABLE (ContractID INT)

		INSERT INTO dbo.Contract
			(
			CompetitionTypeID,
			ContractDateEnd,
			ContractDateStart,
			ContractDescription,
			ContractingOfficeName,
			ContractingOrganizationID,
			ContractName,
			ContractTypeID,
			EstimatedSolicitationQuarter,
			FiscalYearContractRangeID,
			FundingOrganizationID,
			IncumbentContractorName,
			NAICSID,
			OriginatingOrganizationID,
			PSCID,
			ReferenceIDV,
			RequirementTypeID,
			TargetAwardDate,
			TotalContractRangeID,
			TransactionNumber
			)
		OUTPUT INSERTED.ContractID INTO @tOutput
		VALUES
			(
			@CompetitionTypeID,
			@ContractDateEnd,
			@ContractDateStart,
			@ContractDescription,
			@ContractingOfficeName,
			@ContractingOrganizationID,
			@ContractName,
			@ContractTypeID,
			@EstimatedSolicitationQuarter,
			@FiscalYearContractRangeID,
			@FundingOrganizationID,
			@IncumbentContractorName,
			@NAICSID,
			@OriginatingOrganizationID,
			@PSCID,
			@ReferenceIDV,
			@RequirementTypeID,
			@TargetAwardDate,
			@TotalContractRangeID,
			@TransactionNumber
			)
		
		SELECT @ContractID = O.ContractID 
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Contract
		SET
			CompetitionTypeID = @CompetitionTypeID,
			ContractDateEnd = @ContractDateEnd,
			ContractDateStart = @ContractDateStart,
			ContractDescription = @ContractDescription,
			ContractingOfficeName = @ContractingOfficeName,
			ContractingOrganizationID = @ContractingOrganizationID,
			ContractName = @ContractName,
			ContractTypeID = @ContractTypeID,
			EstimatedSolicitationQuarter = @EstimatedSolicitationQuarter,
			FiscalYearContractRangeID = @FiscalYearContractRangeID,
			FundingOrganizationID = @FundingOrganizationID,
			IncumbentContractorName = @IncumbentContractorName,
			NAICSID = @NAICSID,
			OriginatingOrganizationID = @OriginatingOrganizationID,
			PSCID = @PSCID,
			ReferenceIDV = @ReferenceIDV,
			RequirementTypeID = @RequirementTypeID,
			TargetAwardDate = @TargetAwardDate,
			TotalContractRangeID = @TotalContractRangeID,
			TransactionNumber = @TransactionNumber
		WHERE ContractID = @ContractID
		
		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPlaceOfPerformanceIDList)) > 0
		BEGIN
	
		UPDATE CPOP
		SET 
			CPOP.ContractID = @ContractID,
			CPOP.IsPrimary = 
				CASE
					WHEN CPOP.ContractPlaceOfPerformanceID = @PrimaryContractPlaceOfPerformanceID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@ActiveContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPointOfContactIDList)) > 0
		BEGIN
	
		UPDATE CPOC
		SET 
			CPOC.ContractID = @ContractID,
			CPOC.IsPrimary = 
				CASE
					WHEN CPOC.ContractPointOfContactID = @PrimaryContractPointOfContactID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@ActiveContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPlaceOfPerformanceIDList)) > 0
		BEGIN
		
		DELETE CPOP
		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@DeletedContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPointOfContactIDList)) > 0
		BEGIN
		
		DELETE CPOC
		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@DeletedContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	EXEC dbo.AddEventLogContractEntry @ContractID = @ContractID, @EventCode = @EventCode, @PersonID = @PersonID
	EXEC dbo.GetContractByContractID @ContractID = @ContractID, @PersonID = @PersonID

END
GO
--End procedure dbo.SaveContract

--Begin procedure dbo.SavePerson
EXEC Utility.DropObject 'dbo.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Person table
-- ====================================================================
CREATE PROCEDURE dbo.SavePerson
@EmailAddress VARCHAR(320) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsSuperAdministrator BIT = NULL,
@LastName VARCHAR(50) = NULL,
@OrganizationID INT = 0,
@OrganizationRoleIDList VARCHAR(MAX) = NULL,
@Phone VARCHAR(50) = NULL,
@SourcePersonID INT = 0,
@TargetPersonID INT = 0,
@Title VARCHAR(50) = NULL,
@UserName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @TargetPersonID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput1 TABLE (PersonID INT)
		
		INSERT INTO dbo.Person
			(
			EmailAddress,
			FirstName,
			IsActive,
			LastName,
			OrganizationID,
			Phone,
			Title,
			UserName
			)
		OUTPUT INSERTED.PersonID INTO @tOutput1
		VALUES
			(
			@EmailAddress,
			@FirstName,
			@IsActive,
			@LastName,
			@OrganizationID,
			@Phone,
			@Title,
			@UserName
			)

		SELECT @TargetPersonID = O.PersonID 
		FROM @tOutput1 O
		
		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Person
		SET
			EmailAddress = @EmailAddress,
			FirstName = @FirstName,
			IsActive = @IsActive,
			LastName = @LastName,
			OrganizationID = @OrganizationID,
			Phone = @Phone,
			Title = @Title,
			UserName = @UserName
		WHERE PersonID = @TargetPersonID
	
		END
	--ENDIF
	
	DECLARE @tOutput2 TABLE 
		(
		RoleID INT NOT NULL DEFAULT 0,
		OrganizationID INT NOT NULL DEFAULT 0,
		Mode VARCHAR(10)
		)

	IF @IsSuperAdministrator = 1
		BEGIN

		DELETE PR
		OUTPUT DELETED.RoleID, 0, 'DELETED' INTO @tOutput2
		FROM dbo.PersonRole PR
		WHERE PR.PersonID = @TargetPersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM Dropdown.Role R
				WHERE R.RoleCode = 'SuperAdministrator'
					AND R.RoleID = PR.RoleID
				)
				
		INSERT INTO dbo.PersonRole
			(PersonID, RoleID)
		OUTPUT INSERTED.RoleID, 0, 'INSERTED' INTO @tOutput2
		SELECT
			@TargetPersonID,
			R.RoleID
		FROM Dropdown.Role R
		WHERE R.RoleCode = 'SuperAdministrator'
			AND NOT EXISTS
				(
				SELECT 1
				FROM dbo.PersonRole PR
				WHERE PR.PersonID = @TargetPersonID
					AND PR.RoleID = R.RoleID
				)
		
		END
	ELSE
		BEGIN
		
		DECLARE @tPersonRole TABLE 
			(
			OrganizationID INT NOT NULL DEFAULT 0, 
			RoleID INT NOT NULL DEFAULT 0
			)

		INSERT INTO @tPersonRole
			(OrganizationID,RoleID)
		SELECT
			LEFT(LTT.ListItem, CHARINDEX('_', LTT.ListItem) - 1),
			RIGHT(LTT.ListItem, LEN(LTT.ListItem) - CHARINDEX('_', LTT.ListItem))
		FROM Utility.ListToTable(@OrganizationRoleIDList, ',', 0) LTT 

		DELETE PR
		OUTPUT DELETED.RoleID, DELETED.OrganizationID, 'DELETED' INTO @tOutput2
		FROM dbo.PersonRole PR
		WHERE PR.PersonID = @TargetPersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tPersonRole TPR
				WHERE TPR.OrganizationID = PR.OrganizationID
					AND TPR.RoleID = PR.RoleID
				)
				
		INSERT INTO dbo.PersonRole
			(PersonID, OrganizationID, RoleID)
		OUTPUT INSERTED.RoleID, INSERTED.OrganizationID, 'INSERTED' INTO @tOutput2
		SELECT
			@TargetPersonID,
			TPR.OrganizationID,
			TPR.RoleID
		FROM @tPersonRole TPR
		WHERE NOT EXISTS
				(
				SELECT 1
				FROM dbo.PersonRole PR
				WHERE PR.PersonID = @TargetPersonID
					AND PR.OrganizationID = TPR.OrganizationID
					AND PR.RoleID = TPR.RoleID
				)
		
		END
	--ENDIF

	EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = @EventCode, @SourcePersonID = @SourcePersonID

END
GO
--End procedure dbo.SavePerson

--Begin procedure dbo.SaveWorkflowPerson
EXEC Utility.DropObject 'dbo.SaveWorkflowPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.04
-- Description:	A stored procedure to save data to the dbo.WorkflowPerson table
-- ============================================================================
CREATE PROCEDURE dbo.SaveWorkflowPerson
@WorkflowID INT,
@WorkflowStepPersonData VARCHAR(MAX) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tWorkflowStepPerson TABLE 
		(
		WorkflowStepNumber INT,
		PersonID INT
		)

	INSERT INTO @tWorkflowStepPerson
		(WorkflowStepNumber,PersonID)
	SELECT
		CAST(Utility.ListGetAt(LTT.ListItem, 1, '_') AS INT),
		CAST(Utility.ListGetAt(LTT.ListItem, 2, '_') AS INT)
	FROM Utility.ListToTable(@WorkflowStepPersonData, ',', 1) LTT 

	DELETE WP
	FROM dbo.WorkflowPerson WP
	WHERE WP.WorkflowID = @WorkflowID
		AND NOT EXISTS
			(
			SELECT 1
			FROM @tWorkflowStepPerson WSP
			WHERE WSP.WorkflowStepNumber = WP.WorkflowStepNumber
				AND WSP.PersonID = WP.PersonID
			)

	INSERT INTO dbo.WorkflowPerson
		(WorkflowID,WorkflowStepNumber,PersonID)
	SELECT
		@WorkflowID,
		WSP.WorkflowStepNumber,
		WSP.PersonID
	FROM @tWorkflowStepPerson WSP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = WSP.WorkflowStepNumber
			AND WP.PersonID = WSP.PersonID
		)

	EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = @PersonID
		
END
GO
--End procedure dbo.SaveWorkflowPerson

--Begin procedure Dropdown.DeleteOrganization
EXEC Utility.DropObject 'Dropdown.DeleteOrganization'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.02
-- Description:	A stored procedure to delete data in the Dropdown.Organization table
-- =================================================================================
CREATE PROCEDURE Dropdown.DeleteOrganization
@OrganizationID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @WorkflowID INT
	
	SELECT @WorkflowID = W.WorkflowID 
	FROM dbo.Workflow W 
	WHERE W.OrganizationID = @OrganizationID 
	
	EXEC dbo.AddEventLogOrganizationEntry @OrganizationID = @OrganizationID, @EventCode = 'Delete', @PersonID = @PersonID
	
	DELETE
	FROM Dropdown.Organization
	WHERE OrganizationID = @OrganizationID

	EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Delete', @PersonID = @PersonID

	DELETE
	FROM dbo.Workflow
	WHERE WorkflowID = @WorkflowID

	DELETE
	FROM dbo.WorkflowPerson
	WHERE WorkflowID = @WorkflowID
	
END
GO
--End procedure Dropdown.DeleteOrganization

--Begin procedure Dropdown.GetCompetitionTypes
EXEC Utility.DropObject 'Dropdown.GetCompetitionTypes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.CompetitionType table
-- ===============================================================================
CREATE PROCEDURE Dropdown.GetCompetitionTypes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CompetitionTypeID,
		T.CompetitionTypeName
	FROM Dropdown.CompetitionType T
	WHERE T.IsActive = 1
		AND T.CompetitionTypeID > 0
	ORDER BY T.DisplayOrder, T.CompetitionTypeName, T.CompetitionTypeID

END
GO
--End procedure Dropdown.GetCompetitionTypes

--Begin procedure Dropdown.GetContractRanges
EXEC Utility.DropObject 'Dropdown.GetContractRanges'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.ContractRange table
-- =============================================================================
CREATE PROCEDURE Dropdown.GetContractRanges

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractRangeID,
		T.ContractRangeName
	FROM Dropdown.ContractRange T
	WHERE T.IsActive = 1
		AND T.ContractRangeID > 0
	ORDER BY T.DisplayOrder, T.ContractRangeName, T.ContractRangeID

END
GO
--End procedure Dropdown.GetContractRanges

--Begin procedure Dropdown.GetContractTypes
EXEC Utility.DropObject 'Dropdown.GetContractTypes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.ContractType table
-- ============================================================================
CREATE PROCEDURE Dropdown.GetContractTypes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractTypeID,
		T.ContractTypeName
	FROM Dropdown.ContractType T
	WHERE T.IsActive = 1
		AND T.ContractTypeID > 0
	ORDER BY T.DisplayOrder, T.ContractTypeName, T.ContractTypeID

END
GO
--End procedure Dropdown.GetContractTypes

--Begin procedure Dropdown.GetNAICS
EXEC Utility.DropObject 'Dropdown.GetNAICS'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.NAICS table
-- =====================================================================
CREATE PROCEDURE Dropdown.GetNAICS

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.NAICSID,
		T.NAICSCode,
		T.NAICSName
	FROM Dropdown.NAICS T
	WHERE T.IsActive = 1
		AND T.NAICSID > 0
	ORDER BY T.DisplayOrder, T.NAICSName, T.NAICSID

END
GO
--End procedure Dropdown.GetNAICS

--Begin procedure Dropdown.GetOperatingDivisions
EXEC Utility.DropObject 'Dropdown.GetOperatingDivisions'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the Dropdown.Organization table
-- ================================================================================
CREATE PROCEDURE Dropdown.GetOperatingDivisions

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM Dropdown.Organization O
	WHERE O.OrganizationID > 0
		AND O.ParentOrganizationID = 0
	ORDER BY O.DisplayOrder, O.OrganizationName, O.OrganizationID

END
GO
--End procedure Dropdown.GetOperatingDivisions

--Begin procedure Dropdown.GetOrganizationByOrganizationID
EXEC Utility.DropObject 'Dropdown.GetOrganizationByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.0616
-- Description:	A stored procedure to get data from the Dropdown.OrganizationHierarchy table based on an organizationid
-- ====================================================================================================================
CREATE PROCEDURE Dropdown.GetOrganizationByOrganizationID
@OrganizationID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOrganization AS AccessControl.Organization

	INSERT INTO @tOrganization (OrganizationID) VALUES (@OrganizationID)
	
	SELECT
		OH.ParentOrganizationID,
		OH.OrganizationID,
		OH.OrganizationName,
		OH.NodeLevel,
		OH.HasChildren,
		UR.CanHaveDelete,
		UR.CanHaveEdit				
	FROM Dropdown.OrganizationHierarchy OH
		JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = OH.OrganizationID
	ORDER BY OH.DisplayIndex

END
GO
--End procedure Dropdown.GetOrganizationByOrganizationID

--Begin procedure Dropdown.GetOrganizationHierarchy
EXEC Utility.DropObject 'Dropdown.GetOrganizationHierarchy'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.30
-- Description:	A stored procedure to get data from the Dropdown.OrganizationHierarchy table
-- =========================================================================================
CREATE PROCEDURE Dropdown.GetOrganizationHierarchy
@OrganizationID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (OrganizationID INT NOT NULL PRIMARY KEY)
	
	IF @OrganizationID > 0
		BEGIN

		INSERT INTO @tTable
			(OrganizationID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM Utility.ListToTable((SELECT OH.OrganizationIDLineage FROM Dropdown.OrganizationHierarchy OH WHERE OH.OrganizationID = @OrganizationID), ',', 0) LTT 
	
		END
	--ENDIF

	DECLARE @tOrganization AS AccessControl.Organization
	
	INSERT INTO @tOrganization (OrganizationID) SELECT O.OrganizationID FROM Dropdown.Organization O
	
	SELECT
		OH.ParentOrganizationID,
		OH.OrganizationID,
		OH.OrganizationName,
		OH.NodeLevel,
		OH.HasChildren,

		CASE
			WHEN @OrganizationID = 0
			THEN 0
			ELSE
				CASE
				 WHEN EXISTS (SELECT 1 FROM @tTable T WHERE T.OrganizationID = OH.OrganizationID)
				 THEN 1
				 ELSE 0
				END
		END AS IsNodeOpen,

		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM Dropdown.OrganizationHierarchy OH
		JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = OH.OrganizationID
	ORDER BY OH.DisplayIndex

END
GO
--End procedure Dropdown.GetOrganizationHierarchy

--Begin procedure Dropdown.GetOrganizationHierarchyByPersonID
EXEC Utility.DropObject 'Dropdown.GetOrganizationHierarchyByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.29
-- Description:	A stored procedure to get data from the Dropdown.OrganizationHierarchy table
-- =========================================================================================
CREATE PROCEDURE Dropdown.GetOrganizationHierarchyByPersonID
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOrganization AS AccessControl.Organization
	
	INSERT INTO @tOrganization (OrganizationID) SELECT O.OrganizationID FROM Dropdown.Organization O

	IF EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = @PersonID AND R.RoleCode = 'SuperAdministrator')
		BEGIN
	
		SELECT
			OH.ParentOrganizationID,
			OH.OrganizationID,
			OH.OrganizationName,
			OH.NodeLevel,
			OH.HasChildren,
			UR.CanHaveDelete,
			UR.CanHaveEdit
		FROM Dropdown.OrganizationHierarchy OH
			JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = OH.OrganizationID
		ORDER BY OH.DisplayIndex
		
		END
	ELSE
		BEGIN
	
		DECLARE @tTable1 TABLE 
			(
			OrganizationIDLineage VARCHAR(MAX)
			)
		
		INSERT INTO @tTable1
			(OrganizationIDLineage)
		SELECT
			OH.OrganizationIDLineage
		FROM Dropdown.OrganizationHierarchy OH
		WHERE EXISTS
			(
			SELECT 1
			FROM dbo.PersonRole PR
				JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
					AND R.RoleCode = 'OrganizationAdministrator'
					AND PR.OrganizationID = OH.OrganizationID
					AND PR.PersonID = @PersonID
			)
		
		SELECT
			OH2.ParentOrganizationID,
			OH2.OrganizationID,
			OH2.OrganizationName,
			E.NodeLevel,
			OH2.HasChildren,
			UR.CanHaveDelete,
			UR.CanHaveEdit
		FROM
			(	
			SELECT
				D.OrganizationID,
				MAX(D.NodeLevel) AS NodeLevel
			FROM
				(	
				SELECT 
					OH1.OrganizationID,
					LEN(REPLACE(OH1.OrganizationIDLineage, T1.OrganizationIDLineage, '1')) - LEN(REPLACE(REPLACE(OH1.OrganizationIDLineage, T1.OrganizationIDLineage, '1'), ',', '')) + 1 AS NodeLevel
				FROM Dropdown.OrganizationHierarchy OH1
					JOIN @tTable1 T1 ON T1.OrganizationIDLineage = LEFT(OH1.OrganizationIDLineage, LEN(T1.OrganizationIDLineage))
				) D
			GROUP BY D.OrganizationID
			) E
			JOIN Dropdown.OrganizationHierarchy OH2 ON OH2.OrganizationID = E.OrganizationID
			JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = E.OrganizationID
		ORDER BY OH2.DisplayIndex
		
		END
	--ENDIF

END
GO
--End procedure Dropdown.GetOrganizationHierarchyByPersonID

--Begin procedure Dropdown.GetOrganizationsByPersonIDAndRoleCodeList
EXEC Utility.DropObject 'Dropdown.GetOrganizationsByPersonIDAndRoleCodeList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the Dropdown.Organization table
-- ================================================================================
CREATE PROCEDURE Dropdown.GetOrganizationsByPersonIDAndRoleCodeList
@PersonID INT,
@RoleCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IsSuperAdministrator BIT = 0

	IF EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = @PersonID AND R.RoleCode = 'SuperAdministrator')
		SET @IsSuperAdministrator = 1
	--ENDIF

	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM Dropdown.Organization O
	WHERE O.IsActive = 1
		AND
			(
			@IsSuperAdministrator = 1
				OR EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
						JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
							AND PR.PersonID = @PersonID
							AND PR.OrganizationID = O.OrganizationID
							AND EXISTS
								(
								SELECT 1
								FROM Utility.ListToTable(@RoleCodeList, ',', 0) LTT 
								WHERE LTT.ListItem = R.RoleCode
								)
					)
			)
	ORDER BY O.DisplayOrder, O.OrganizationName, O.OrganizationID

END
GO
--End procedure Dropdown.GetOrganizationsByPersonIDAndRoleCodeList

--Begin procedure Dropdown.GetOriginatingOrganizationsByPersonID
EXEC Utility.DropObject 'Dropdown.GetOriginatingOrganizationsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.005.28
-- Description:	A stored procedure to get all of the organizations from which a person can originate a contract
-- ============================================================================================================
CREATE PROCEDURE Dropdown.GetOriginatingOrganizationsByPersonID
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = @PersonID AND R.RoleCode = 'SuperAdministrator')
		BEGIN
		
		SELECT
			O.OrganizationID,
			O.OrganizationName
		FROM Dropdown.Organization O
		WHERE EXISTS 
			(
			SELECT 1 
			FROM dbo.Workflow W
			WHERE W.OrganizationID = O.OrganizationID
				AND EXISTS
					(
					SELECT 1
					FROM dbo.WorkflowPerson WP
					WHERE WP.WorkflowID = W.WorkflowID
						AND WP.WorkflowStepNumber = 1
					)
			)
		ORDER BY O.OrganizationName, O.OrganizationID

		END
	ELSE 		
		BEGIN
		
		SELECT
			O.OrganizationID,
			O.OrganizationName
		FROM Dropdown.Organization O
		WHERE EXISTS 
			(
			SELECT 1 
			FROM dbo.PersonRole PR 
				JOIN Dropdown.Role R ON R.RoleID = PR.RoleID 
					AND PR.PersonID = @PersonID
					AND R.RoleCode = 'OrganizationAdministrator'
					AND PR.OrganizationID = O.OrganizationID
			)
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.Workflow W
			WHERE W.OrganizationID = O.OrganizationID
				AND EXISTS
					(
					SELECT 1
					FROM dbo.WorkflowPerson WP
					WHERE WP.WorkflowID = W.WorkflowID
						AND WP.WorkflowStepNumber = 1
					)
			)

		UNION

		SELECT
			O.OrganizationID,
			O.OrganizationName
		FROM Dropdown.Organization O
		WHERE EXISTS 
			(
			SELECT 1 
			FROM dbo.WorkflowPerson WP
				JOIN dbo.Workflow W ON W.WorkflowID = WP.WorkflowID
					AND WP.PersonID = @PersonID 
					AND WP.WorkflowStepNumber = 1
					AND W.OrganizationID = O.OrganizationID
			)

		ORDER BY O.OrganizationName, O.OrganizationID

		END
	--ENDIF

END
GO
--End procedure Dropdown.GetOriginatingOrganizationsByPersonID

--Begin procedure Dropdown.GetPSCS
EXEC Utility.DropObject 'Dropdown.GetPSCS'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.23
-- Description:	A stored procedure to data from the Dropdown.PSC table
-- ===================================================================
CREATE PROCEDURE Dropdown.GetPSCS

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PSCID,
		T.PSCCode,
		T.PSCName
	FROM Dropdown.PSC T
	WHERE T.IsActive = 1
		AND T.PSCID > 0
	ORDER BY T.DisplayOrder, T.PSCName, T.PSCID

END
GO
--End procedure Dropdown.GetPSCS

--Begin procedure Dropdown.GetQuickSearchByQuickSearchID
EXEC Utility.DropObject 'Dropdown.GetQuickSearchByQuickSearchID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.12.07
-- Description:	A stored procedure to get data from the Dropdown.QuickSearch table based on a QuickSearchID
-- ========================================================================================================
CREATE PROCEDURE Dropdown.GetQuickSearchByQuickSearchID
@QuickSearchID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		QS.DisplayOrder, 
		QS.IsActive, 
		Dropdown.ParseQuickSearchCriteria(QS.QuickSearchCriteria, @PersonID) AS QuickSearchCriteria, 
		QS.QuickSearchDescription, 
		QS.QuickSearchName, 
		QS.QuickSearchID
	FROM Dropdown.QuickSearch QS
	WHERE QS.QuickSearchID = @QuickSearchID

END
GO
--End procedure Dropdown.GetQuickSearchByQuickSearchID

--Begin procedure Dropdown.GetQuickSearches
EXEC Utility.DropObject 'Dropdown.GetQuickSearches'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2013.12.07
-- Description:	A stored procedure to get data from the Dropdown.QuickSearch table
-- ===============================================================================
CREATE PROCEDURE Dropdown.GetQuickSearches
@IsPublicOnly BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		QS.QuickSearchDescription, 
		QS.QuickSearchName, 
		QS.QuickSearchID
	FROM Dropdown.QuickSearch QS
	WHERE QS.IsActive = 1
		AND 
			(
			@IsPublicOnly = 0
				OR QS.IsPublic = 1
			)
	ORDER BY QS.DisplayOrder, QS.QuickSearchName, QS.QuickSearchID

END
GO
--End procedure Dropdown.GetQuickSearches

--Begin procedure Dropdown.GetRequirementTypes
EXEC Utility.DropObject 'Dropdown.GetRequirementTypes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.RequirementType table
-- ===============================================================================
CREATE PROCEDURE Dropdown.GetRequirementTypes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequirementTypeID,
		T.RequirementTypeName
	FROM Dropdown.RequirementType T
	WHERE T.IsActive = 1
		AND T.RequirementTypeID > 0
	ORDER BY T.DisplayOrder, T.RequirementTypeName, T.RequirementTypeID

END
GO
--End procedure Dropdown.GetRequirementTypes

--Begin procedure Dropdown.GetStates
EXEC Utility.DropObject 'Dropdown.GetStates'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to data from the Dropdown.State table
-- =====================================================================
CREATE PROCEDURE Dropdown.GetStates

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StateID,
		T.StateName
	FROM Dropdown.State T
	WHERE T.IsActive = 1
		AND T.StateID > 0
	ORDER BY T.StateName, T.StateID

END
GO
--End procedure Dropdown.GetStates

--Begin procedure Dropdown.GetTargetAwardFiscalYears
EXEC Utility.DropObject 'Dropdown.GetTargetAwardFiscalYears'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.24
-- Description:	A stored procedure to get a list of target award fiscal years from the dbo.Contract table
-- ======================================================================================================
CREATE PROCEDURE Dropdown.GetTargetAwardFiscalYears

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT

		CASE
			WHEN MONTH(C.TargetAwardDate) > 9
			THEN YEAR(C.TargetAwardDate) + 1
			ELSE YEAR(C.TargetAwardDate)
		END AS TargetAwardFiscalYearNumber

	FROM dbo.Contract C
	ORDER BY 1
		
END
GO
--End procedure Dropdown.GetTargetAwardFiscalYears

--Begin procedure Dropdown.GetWorkflowStatusByWorkflowStatusCode
EXEC Utility.DropObject 'Dropdown.GetWorkflowStatusByWorkflowStatusCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.28
-- Description:	A stored procedure to return data from the Dropdown.WorkflowStatus table based on a WorkflowStatusCode
-- ===================================================================================================================
CREATE PROCEDURE Dropdown.GetWorkflowStatusByWorkflowStatusCode
@WorkflowStatusCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		WS.WorkflowStatusID,
		WS.WorkflowStatusName
	FROM Dropdown.WorkflowStatus WS
	WHERE WS.WorkflowStatusCode = @WorkflowStatusCode

END
GO
--End procedure Dropdown.GetWorkflowStatusByWorkflowStatusCode

--Begin procedure Dropdown.SaveOrganization
EXEC Utility.DropObject 'Dropdown.SaveOrganization'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.02
-- Description:	A stored procedure to save data in the Dropdown.Organization table
-- ===============================================================================
CREATE PROCEDURE Dropdown.SaveOrganization
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@OrganizationCode VARCHAR(50) = NULL,
@OrganizationID INT = 0,
@OrganizationName VARCHAR(250) = NULL,
@ParentOrganizationID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @OrganizationID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput1 TABLE (OrganizationID INT)
		
		INSERT INTO Dropdown.Organization
			(
			DisplayOrder,
			IsActive,
			OrganizationCode,
			OrganizationName,
			ParentOrganizationID
			)
		OUTPUT INSERTED.OrganizationID INTO @tOutput1
		VALUES
			(
			@DisplayOrder,
			@IsActive,
			@OrganizationCode,
			@OrganizationName,
			@ParentOrganizationID
			)

		SELECT @OrganizationID = O1.OrganizationID 
		FROM @tOutput1 O1

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE Dropdown.Organization
		SET
			DisplayOrder = @DisplayOrder,
			IsActive = @IsActive,
			OrganizationCode = @OrganizationCode,
			OrganizationName = @OrganizationName
		WHERE OrganizationID = @OrganizationID

		END
	--ENDIF

	EXEC dbo.AddEventLogOrganizationEntry @OrganizationID = @OrganizationID, @EventCode = @EventCode, @PersonID = @PersonID

	IF @EventCode = 'Create'
		BEGIN

		DECLARE @tOutput2 TABLE (WorkflowID INT)
		DECLARE @WorkflowID INT
		
		INSERT INTO dbo.Workflow
			(OrganizationID)
		OUTPUT INSERTED.WorkflowID INTO @tOutput2
		VALUES
			(@OrganizationID)

		SELECT @WorkflowID = O2.WorkflowID 
		FROM @tOutput2 O2

		EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = @EventCode, @PersonID = @PersonID
			
		END
	--ENDIF
	
	SELECT
		O.DisplayOrder,
		O.IsActive,
		O.OrganizationCode,
		O.OrganizationID,
		O.OrganizationName,
		O.ParentOrganizationID
	FROM Dropdown.Organization O
	WHERE O.OrganizationID = @OrganizationID
	
END
GO
--End procedure Dropdown.SaveOrganization
--End Procedures