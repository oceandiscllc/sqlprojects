USE PFDR
GO

--Begin Functions
--Begin function dbo.IsSuperAdministrator
EXEC Utility.DropObject 'dbo.IsSuperAdministrator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.08.07
-- Description:	A function to determine if a person has the SuperAdministrator role
-- ================================================================================

CREATE FUNCTION dbo.IsSuperAdministrator
(
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @IsSuperAdministrator BIT = 0

	IF EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = @PersonID AND R.RoleCode = 'SuperAdministrator')
		SET @IsSuperAdministrator = 1
	--ENDIF

	RETURN @IsSuperAdministrator
END
GO
--End function dbo.IsSuperAdministrator

--Begin function AccessControl.GetOrganizationRecordsByPersonID
EXEC Utility.DropObject 'AccessControl.GetOrganizationRecordsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.06.16
-- Description:	A function to manage the buisness rules regarding Organization table record visibility based on PersonID
--
-- Author:			Todd Pires
-- Modify Date:	2014.08.06
-- Description:	Added the call to Dropdown.GetAdministerdOrganizationsByPersonID to support inherited permissions
-- =====================================================================================================================

CREATE FUNCTION AccessControl.GetOrganizationRecordsByPersonID
(
@PersonID INT,
@Organization AccessControl.Organization READONLY
)

RETURNS @tReturn table 
	(
	OrganizationID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	IF dbo.IsSuperAdministrator(@PersonID) = 1
		BEGIN

		INSERT INTO @tReturn
			(OrganizationID,CanHaveDelete,CanHaveEdit)
		SELECT
			OAC.OrganizationID,

			CASE
				WHEN OH.HasChildren = 0 
					AND NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractingOrganizationID = OH.OrganizationID OR C.FundingOrganizationID = OH.OrganizationID OR C.OriginatingOrganizationID = OH.OrganizationID)
					AND NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.OrganizationID = OH.OrganizationID)
				THEN 1
				ELSE 0
			END,
			
			1
		FROM @Organization OAC
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = OAC.OrganizationID

		END
	ELSE
		BEGIN
		
		DECLARE @tAdministerdOrganizations TABLE (OrganizationID INT PRIMARY KEY NOT NULL)
		
		INSERT INTO @tAdministerdOrganizations
			(OrganizationID)
		SELECT
			AO.OrganizationID
		FROM Dropdown.GetAdministerdOrganizationsByPersonID(@PersonID) AO

		INSERT INTO @tReturn
			(OrganizationID,CanHaveDelete,CanHaveEdit)
		SELECT
			OAC.OrganizationID,

			CASE
				WHEN OH.HasChildren = 0 
					AND NOT EXISTS (SELECT 1 FROM dbo.Contract C WHERE C.ContractingOrganizationID = OH.OrganizationID OR C.FundingOrganizationID = OH.OrganizationID OR C.OriginatingOrganizationID = OH.OrganizationID)
					AND NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.OrganizationID = OH.OrganizationID)
					AND EXISTS (SELECT 1 FROM @tAdministerdOrganizations AO WHERE AO.OrganizationID = OH.OrganizationID)
				THEN 1
				ELSE 0
			END,

			CASE
				WHEN EXISTS (SELECT 1 FROM @tAdministerdOrganizations AO WHERE AO.OrganizationID = OH.OrganizationID) 
				THEN 1
				ELSE 0
			END
			
		FROM @Organization OAC
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = OAC.OrganizationID

		END
	--ENDIF
					
	RETURN

END
GO
--End function AccessControl.GetOrganizationRecordsByPersonID

--Begin function Dropdown.GetAdministerdOrganizationsByPersonID
EXEC Utility.DropObject 'Dropdown.GetAdministerdOrganizationsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.08.06
-- Description:	A function to return the organizations for which a given PersonID is an administrator
-- ==================================================================================================

CREATE FUNCTION Dropdown.GetAdministerdOrganizationsByPersonID
(
@PersonID INT
)

RETURNS @tReturn table 
	(
	OrganizationID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (OrganizationID,ParentOrganizationID,NodeLevel)
		AS 
		(
		SELECT
			O.OrganizationID, 
			O.ParentOrganizationID, 
			1 
		FROM Dropdown.Organization O
			JOIN
				(
				SELECT
					R.RoleCode,
					PR.OrganizationID
				FROM dbo.PersonRole PR
					JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
						AND PR.PersonID = @PersonID
						AND EXISTS
							(
							SELECT 1
							FROM Dropdown.Role R
							WHERE R.RoleID = PR.RoleID
								AND R.IsAdministrator = 1
							)
				) D ON D.OrganizationID = O.OrganizationID
	
		UNION ALL
		
		SELECT
			O.OrganizationID, 
			O.ParentOrganizationID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM Dropdown.Organization O
			JOIN HD ON HD.OrganizationID = O.ParentOrganizationID 
		)
	
	INSERT INTO @tReturn
		(OrganizationID,NodeLevel)
	SELECT 
		HD.OrganizationID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function Dropdown.GetAdministerdOrganizationsByPersonID
--End Functions

--Begin Stored Procedures
--Begin procedure dbo.AddEventLogLoginEntry
EXEC Utility.DropObject 'dbo.AddEventLogLoginEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.08.05
-- Description:	A stored procedure to add data to the dbo.EventLog table
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogLoginEntry
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.EventLog
		(EntityID,EntityTypeCode,EventCode)
	VALUES
		(
		@PersonID,
		'Person',
		'Login'
		)
		
END
GO
--End procedure dbo.AddEventLogPersonEntry

--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		C.ContractDateEnd,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.GetPersonByHTTPContext
EXEC Utility.DropObject 'dbo.GetPersonByHTTPContext'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.01.23
-- Description:	A stored procedure to get data from the dbo.Person table based on an EmailAddress or a UserName
-- ============================================================================================================
CREATE PROCEDURE dbo.GetPersonByHTTPContext
@EmailAddress VARCHAR(320) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PersonID, 
		P.EmailAddress, 
		P.UserName,
		dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.PersonID = P.PersonID AND WP.WorkflowStepNumber = 1)
			THEN 1
			ELSE 0
		END AS IsOriginator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'OrganizationAdministrator')
			THEN 1
			ELSE 0
		END AS IsAdministrator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'SuperAdministrator')
			THEN 1
			ELSE 0
		END AS IsSuperAdministrator

	FROM dbo.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.IsActive = 1
	
END
GO
--End procedure dbo.GetPersonByHTTPContext

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.24
-- Description:	A stored procedure to get data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.08
-- Description:	Implemented the dbo.IsSuperAdministrator function
-- =========================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID
@SourcePersonID INT = 0,
@TargetPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPerson AS AccessControl.Person

	INSERT INTO @tPerson (PersonID) SELECT P.PersonID FROM dbo.Person P WHERE P.PersonID = @TargetPersonID
	
	SELECT
		O.OrganizationName, 		
		P.EmailAddress, 		
		P.FirstName, 		
		P.IsActive, 		
		P.LastName, 		
		P.OrganizationID,
		P.PersonID,		
		P.Phone, 		
		P.Title,
		P.UserName,
		(SELECT dbo.IsSuperAdministrator(P.PersonID)) IsSuperAdministrator,
		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM dbo.Person P
		JOIN AccessControl.GetPersonRecordsByPersonID(@SourcePersonID, @tPerson) UR ON UR.PersonID = P.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetPersonsByPersonIDList
EXEC Utility.DropObject 'dbo.GetPersonsByPersonIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.03
-- Description:	A stored procedure to get data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.04
-- Description:	Added the ISNULL on the phone column
-- =========================================================================================
CREATE PROCEDURE dbo.GetPersonsByPersonIDList
@PersonIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.OrganizationName, 		
		P.PersonID,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,		
		ISNULL(P.Phone, '') AS Phone
	FROM dbo.Person P
		JOIN Utility.ListToTable(@PersonIDList, ',', 0) LTT ON CAST(LTT.ListItem AS INT) = P.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID

END
GO
--End procedure dbo.GetPersonsByPersonIDList

--Begin procedure dbo.GetPersonsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetPersonsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.22
-- Description:	A stored procedure to get data from the dbo.Person table based on search criteria
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.04
-- Description:	Added support for the TopLevelOrganizationID parameter
-- ==============================================================================================
CREATE PROCEDURE dbo.GetPersonsBySearchCriteria
@EmailAddress VARCHAR(320) = NULL,
@FirstLetter CHAR(1) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@Keyword VARCHAR(MAX) = NULL,
@LastName VARCHAR(50) = NULL,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'LastName',
@OrganizationID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PersonID INT = 0,
@Phone VARCHAR(50) = NULL,
@Title VARCHAR(50) = NULL,
@TopLevelOrganizationID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT

	DECLARE @tPerson AS AccessControl.Person

	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetPersonsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetPersonsBySearchCriteria
	--ENDIF
	
	CREATE TABLE #tGetPersonsBySearchCriteria
		(
		PersonID INT PRIMARY KEY NOT NULL
		)

	IF @Keyword IS NOT NULL
		SET @Keyword = '%' + @Keyword + '%'
	--ENDIF

	IF @Keyword IS NULL
		BEGIN

		INSERT INTO #tGetPersonsBySearchCriteria
			(PersonID)
		SELECT
			P.PersonID
		FROM dbo.Person P
		WHERE P.IsActive = @IsActive

		IF @EmailAddress IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.EmailAddress NOT LIKE '%' + @EmailAddress + '%'
	
			END
		--ENDIF
		IF @FirstLetter IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND LEFT(P.LastName, 1) <> @FirstLetter
	
			END
		--ENDIF
		IF @FirstName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.FirstName NOT LIKE '%' + @FirstName + '%'
	
			END
		--ENDIF
		IF @LastName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.LastName NOT LIKE '%' + @LastName + '%'
	
			END
		--ENDIF
		IF @Phone IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.Phone NOT LIKE '%' + @Phone + '%'
	
			END
		--ENDIF
		IF @Title IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.Title NOT LIKE '%' + @Title + '%'
	
			END
		--ENDIF

		-- Integer Fields (individual)
		IF @OrganizationID > 0
			BEGIN

			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
					AND P.OrganizationID <> @OrganizationID
	
			END
		--ENDIF
		IF @TopLevelOrganizationID > 0
			BEGIN

			DELETE T1
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
				JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = P.OrganizationID
					AND OH.TopLevelOrganizationID <> @TopLevelOrganizationID
	
			END
		--ENDIF

		END
	ELSE
		BEGIN

		SET @Keyword = '%' + @Keyword + '%'
	
		-- Text Fields (multiple)
		INSERT INTO #tGetPersonsBySearchCriteria
			(PersonID)
		SELECT
			P.PersonID
		FROM dbo.Person P
			JOIN Dropdown.OrganizationHierarchy OH ON OH.OrganizationID = P.OrganizationID
				AND 
				(
				OH.OrganizationName LIKE @Keyword
					OR P.EmailAddress LIKE @Keyword
					OR P.FirstName LIKE @Keyword
					OR P.LastName LIKE @Keyword
					OR P.Phone LIKE @Keyword
					OR P.Title LIKE @Keyword
				)

		END
	--ENDIF

	SELECT 
		@TotalRecordCount = COUNT(T1.PersonID)			
	FROM #tGetPersonsBySearchCriteria T1

	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/

	IF @PageSize > 0
		BEGIN

		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)

		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
						CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
						CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
						CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
						CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
						CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
						CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
						CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
						CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
						CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
						CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'ASC' THEN P.Phone END ASC,
						CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'DESC' THEN P.Phone END DESC,
						CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'ASC' THEN P.Title END ASC,
						CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'DESC' THEN P.Title END DESC,
						CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
						CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
							T1.PersonID ASC
					) AS RowIndex,
				T1.PersonID
			FROM #tGetPersonsBySearchCriteria T1
				JOIN dbo.Person P ON P.PersonID = T1.PersonID
				JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			)

		DELETE T1
		FROM #tGetPersonsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.PersonID = T1.PersonID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)

		END
	--ENDIF
	
	INSERT INTO @tPerson (PersonID) SELECT T1.PersonID FROM #tGetPersonsBySearchCriteria T1

	SELECT
		@PageCount AS PageCount,
		@PageIndex AS PageIndex,
		@TotalRecordCount AS TotalRecordCount,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		O.OrganizationName,
		P.IsActive,
		dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PersonID, 
		P.EmailAddress,
		ISNULL(P.Phone, '') AS Phone,
		ISNULL(P.Title, '') AS Title
	FROM #tGetPersonsBySearchCriteria T1
		JOIN AccessControl.GetPersonRecordsByPersonID(@PersonID, @tPerson) UR ON UR.PersonID = T1.PersonID
		JOIN dbo.Person P ON P.PersonID = T1.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
	ORDER BY 
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'ASC' THEN P.EmailAddress END ASC,
		CASE WHEN @OrderByField = 'EmailAddress' AND @OrderByDirection = 'DESC' THEN P.EmailAddress END DESC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'ASC' THEN P.FirstName END ASC,
		CASE WHEN @OrderByField = 'FirstName' AND @OrderByDirection = 'DESC' THEN P.FirstName END DESC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'ASC' THEN P.LastName END ASC,
		CASE WHEN @OrderByField = 'LastName' AND @OrderByDirection = 'DESC' THEN P.LastName END DESC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'ASC' THEN O.OrganizationName END ASC,
		CASE WHEN @OrderByField = 'OrganizationName' AND @OrderByDirection = 'DESC' THEN O.OrganizationName END DESC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'ASC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END ASC,
		CASE WHEN @OrderByField = 'PersonNameFormatted' AND @OrderByDirection = 'DESC' THEN dbo.GetPersonNameByPersonID(P.PersonID, 'LastFirst') END DESC,
		CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'ASC' THEN P.Phone END ASC,
		CASE WHEN @OrderByField = 'Phone' AND @OrderByDirection = 'DESC' THEN P.Phone END DESC,
		CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'ASC' THEN P.Title END ASC,
		CASE WHEN @OrderByField = 'Title' AND @OrderByDirection = 'DESC' THEN P.Title END DESC,
		CASE WHEN @OrderByField <> 'LastName' THEN P.LastName END ASC, 
		CASE WHEN @OrderByField <> 'FirstName' THEN P.FirstName END ASC,
			T1.PersonID ASC

	DROP TABLE #tGetPersonsBySearchCriteria

END
GO
--End procedure dbo.GetPersonsBySearchCriteria

--Begin procedure dbo.IncrementWorkflow
EXEC Utility.DropObject 'dbo.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.06
-- Description:	A stored procedure to move a contract record to the next step in its workflow
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
-- ==========================================================================================
CREATE PROCEDURE dbo.IncrementWorkflow
@Comments VARCHAR(MAX),
@ContractID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentWorkflowStepNumber INT
	DECLARE @FinalWorkflowStepNumber INT
	
	SELECT
		@CurrentWorkflowStepNumber = C.WorkflowStepNumber,
		@FinalWorkflowStepNumber = CAST((SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS INT)
	FROM dbo.Contract C
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			AND C.ContractID = @ContractID

	UPDATE C
	SET 
		C.WorkflowStatusID = 
			CASE 
				WHEN @CurrentWorkflowStepNumber < @FinalWorkflowStepNumber
				THEN (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'PendingApproval')
				ELSE (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'Approved')
			END,

		C.WorkflowStepNumber = @CurrentWorkflowStepNumber + 1
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID

	EXEC dbo.AddEventLogContractEntry @Comments = @Comments, @ContractID = @ContractID, @EventCode = 'IncrementWorkflow', @PersonID = @PersonID

	SELECT
		@CurrentWorkflowStepNumber + 1 AS CurrentWorkflowStepNumber,
		@FinalWorkflowStepNumber AS FinalWorkflowStepNumber
	
END
GO
--End procedure dbo.IncrementWorkflow

--Begin procedure dbo.SavePerson
EXEC Utility.DropObject 'dbo.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Person table
--
-- Author:			Todd Pires
-- Create Date: 2014.08.01
-- Description:	Added the check for duplicate email addresses on insert
-- ====================================================================
CREATE PROCEDURE dbo.SavePerson
@EmailAddress VARCHAR(320) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsSuperAdministrator BIT = NULL,
@LastName VARCHAR(50) = NULL,
@OrganizationID INT = 0,
@OrganizationRoleIDList VARCHAR(MAX) = NULL,
@Phone VARCHAR(50) = NULL,
@SourcePersonID INT = 0,
@TargetPersonID INT = 0,
@Title VARCHAR(50) = NULL,
@UserName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @TargetPersonID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput1 TABLE (PersonID INT)
		
		IF NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.EmailAddress = @EmailAddress)
			BEGIN
			
			INSERT INTO dbo.Person
				(
				EmailAddress,
				FirstName,
				IsActive,
				LastName,
				OrganizationID,
				Phone,
				Title,
				UserName
				)
			OUTPUT INSERTED.PersonID INTO @tOutput1
			VALUES
				(
				@EmailAddress,
				@FirstName,
				@IsActive,
				@LastName,
				@OrganizationID,
				@Phone,
				@Title,
				@UserName
				)
	
			SELECT @TargetPersonID = O.PersonID 
			FROM @tOutput1 O

			END
		--ENDIF

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Person
		SET
			EmailAddress = @EmailAddress,
			FirstName = @FirstName,
			IsActive = @IsActive,
			LastName = @LastName,
			OrganizationID = @OrganizationID,
			Phone = @Phone,
			Title = @Title,
			UserName = @UserName
		WHERE PersonID = @TargetPersonID
	
		END
	--ENDIF
	
	IF @TargetPersonID > 0
		BEGIN
		
		DECLARE @tOutput2 TABLE 
			(
			RoleID INT NOT NULL DEFAULT 0,
			OrganizationID INT NOT NULL DEFAULT 0,
			Mode VARCHAR(10)
			)
	
		IF @IsSuperAdministrator = 1
			BEGIN
	
			DELETE PR
			OUTPUT DELETED.RoleID, 0, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM Dropdown.Role R
					WHERE R.RoleCode = 'SuperAdministrator'
						AND R.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, RoleID)
			OUTPUT INSERTED.RoleID, 0, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				R.RoleID
			FROM Dropdown.Role R
			WHERE R.RoleCode = 'SuperAdministrator'
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.RoleID = R.RoleID
					)
			
			END
		ELSE
			BEGIN
			
			DECLARE @tPersonRole TABLE 
				(
				OrganizationID INT NOT NULL DEFAULT 0, 
				RoleID INT NOT NULL DEFAULT 0
				)
	
			INSERT INTO @tPersonRole
				(OrganizationID,RoleID)
			SELECT
				LEFT(LTT.ListItem, CHARINDEX('_', LTT.ListItem) - 1),
				RIGHT(LTT.ListItem, LEN(LTT.ListItem) - CHARINDEX('_', LTT.ListItem))
			FROM Utility.ListToTable(@OrganizationRoleIDList, ',', 0) LTT 
	
			DELETE PR
			OUTPUT DELETED.RoleID, DELETED.OrganizationID, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tPersonRole TPR
					WHERE TPR.OrganizationID = PR.OrganizationID
						AND TPR.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, OrganizationID, RoleID)
			OUTPUT INSERTED.RoleID, INSERTED.OrganizationID, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				TPR.OrganizationID,
				TPR.RoleID
			FROM @tPersonRole TPR
			WHERE NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.OrganizationID = TPR.OrganizationID
						AND PR.RoleID = TPR.RoleID
					)
			
			END
		--ENDIF

		EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = @EventCode, @SourcePersonID = @SourcePersonID
		
		END
	--ENDIF

END
GO
--End procedure dbo.SavePerson

--Begin procedure Dropdown.GetOrganizationHierarchyByPersonID
EXEC Utility.DropObject 'Dropdown.GetOrganizationHierarchyByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.29
-- Description:	A stored procedure to get data from the Dropdown.OrganizationHierarchy table
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.05
-- Description:	Added a CAST to the MAX(E.NodeLevel) call to play nice with dapper
-- =========================================================================================
CREATE PROCEDURE Dropdown.GetOrganizationHierarchyByPersonID
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOrganization AS AccessControl.Organization
	
	INSERT INTO @tOrganization (OrganizationID) SELECT O.OrganizationID FROM Dropdown.Organization O

	IF dbo.IsSuperAdministrator(@PersonID) = 1
		BEGIN
	
		SELECT
			OH.ParentOrganizationID,
			OH.OrganizationID,
			OH.OrganizationName,
			OH.NodeLevel,
			OH.HasChildren,
			UR.CanHaveDelete,
			UR.CanHaveEdit
		FROM Dropdown.OrganizationHierarchy OH
			JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = OH.OrganizationID
		ORDER BY OH.DisplayIndex
		
		END
	ELSE
		BEGIN
	
		DECLARE @tTable1 TABLE 
			(
			OrganizationIDLineage VARCHAR(MAX)
			)
		
		INSERT INTO @tTable1
			(OrganizationIDLineage)
		SELECT
			OH.OrganizationIDLineage
		FROM Dropdown.OrganizationHierarchy OH
		WHERE EXISTS
			(
			SELECT 1
			FROM dbo.PersonRole PR
				JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
					AND R.RoleCode = 'OrganizationAdministrator'
					AND PR.OrganizationID = OH.OrganizationID
					AND PR.PersonID = @PersonID
			)
		
		SELECT
			OH2.ParentOrganizationID,
			OH2.OrganizationID,
			OH2.OrganizationName,
			CAST(E.NodeLevel AS INT) AS NodeLevel,
			OH2.HasChildren,
			UR.CanHaveDelete,
			UR.CanHaveEdit
		FROM
			(	
			SELECT
				D.OrganizationID,
				MAX(D.NodeLevel) AS NodeLevel
			FROM
				(	
				SELECT 
					OH1.OrganizationID,
					LEN(REPLACE(OH1.OrganizationIDLineage, T1.OrganizationIDLineage, '1')) - LEN(REPLACE(REPLACE(OH1.OrganizationIDLineage, T1.OrganizationIDLineage, '1'), ',', '')) + 1 AS NodeLevel
				FROM Dropdown.OrganizationHierarchy OH1
					JOIN @tTable1 T1 ON T1.OrganizationIDLineage = LEFT(OH1.OrganizationIDLineage, LEN(T1.OrganizationIDLineage))
				) D
			GROUP BY D.OrganizationID
			) E
			JOIN Dropdown.OrganizationHierarchy OH2 ON OH2.OrganizationID = E.OrganizationID
			JOIN AccessControl.GetOrganizationRecordsByPersonID(@PersonID, @tOrganization) UR ON UR.OrganizationID = E.OrganizationID
		ORDER BY OH2.DisplayIndex
		
		END
	--ENDIF

END
GO
--End procedure Dropdown.GetOrganizationHierarchyByPersonID

--Begin procedure Dropdown.GetOrganizationsByPersonIDAndRoleCodeList
EXEC Utility.DropObject 'Dropdown.GetOrganizationsByPersonIDAndRoleCodeList'
GO
--End procedure Dropdown.GetOrganizationsByPersonIDAndRoleCodeList

--Begin procedure Dropdown.GetOriginatingOrganizationsByPersonID
EXEC Utility.DropObject 'Dropdown.GetOriginatingOrganizationsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.05.28
-- Description:	A stored procedure to get all of the organizations from which a person can originate a contract
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.08
-- Description:	Modified to implement the strict origination rule (no inherited or implicit permissions)
-- ============================================================================================================
CREATE PROCEDURE Dropdown.GetOriginatingOrganizationsByPersonID
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM Dropdown.Organization O
	WHERE EXISTS 
		(
		SELECT 1 
		FROM dbo.WorkflowPerson WP
			JOIN dbo.Workflow W ON W.WorkflowID = WP.WorkflowID
				AND WP.PersonID = @PersonID 
				AND WP.WorkflowStepNumber = 1
				AND W.OrganizationID = O.OrganizationID
		)

	ORDER BY O.OrganizationName, O.OrganizationID

END
GO
--End procedure Dropdown.GetOriginatingOrganizationsByPersonID
--End Stored Procedures

--Begin Data Seed
TRUNCATE TABLE dbo.Contract
TRUNCATE TABLE dbo.ContractPlaceOfPerformance
TRUNCATE TABLE dbo.ContractPointOfContact
TRUNCATE TABLE dbo.ContractWorkflowPerson
TRUNCATE TABLE dbo.EventLog
TRUNCATE TABLE dbo.Person
TRUNCATE TABLE dbo.PersonRole
TRUNCATE TABLE dbo.Workflow
TRUNCATE TABLE dbo.WorkflowPerson
TRUNCATE TABLE Dropdown.CompetitionType
TRUNCATE TABLE Dropdown.ContractRange
TRUNCATE TABLE Dropdown.ContractType
TRUNCATE TABLE Dropdown.NAICS
TRUNCATE TABLE Dropdown.Organization
TRUNCATE TABLE Dropdown.OrganizationHierarchy
TRUNCATE TABLE Dropdown.PSC
TRUNCATE TABLE Dropdown.QuickSearch
TRUNCATE TABLE Dropdown.RequirementType
TRUNCATE TABLE Dropdown.Role
TRUNCATE TABLE Dropdown.State
TRUNCATE TABLE Dropdown.WorkflowStatus
GO

--Begin table Dropdown.CompetitionType
SET IDENTITY_INSERT Dropdown.CompetitionType ON
INSERT INTO Dropdown.CompetitionType (CompetitionTypeID) VALUES (0)
SET IDENTITY_INSERT Dropdown.CompetitionType OFF
GO

INSERT INTO Dropdown.CompetitionType 
	(CompetitionTypeName, DisplayOrder) 
VALUES
	('8(A) Sole Source', 1),
	('8A Competed', 2),
	('Buy Indian', 3),
	('Economically Disadvanted Women Owned Small Business', 4),
	('Fair Opportunity (for orders under existing contracts)', 5),
	('Full and Open', 6),
	('HUBZone Set-Aside', 7),
	('HUBZone Sole Source', 8),
	('Not Competed – Other Than Small', 9),
	('Not Competed – Small', 10),
	('Reserved for Small Business', 11),
	('SDVOSB Sole Source', 12),
	('Service Disabled Veteran Owned Small Business Set-Aside', 13),
	('Small Business Set Aside - Partial', 14),
	('Small Business Set Aside - Total', 15),
	('Women Owned Small Business', 16),
	('TBD', 17)
GO
--End table Dropdown.CompetitionType

--Begin table Dropdown.ContractRange
SET IDENTITY_INSERT Dropdown.ContractRange ON
INSERT INTO Dropdown.ContractRange (ContractRangeID) VALUES (0)
SET IDENTITY_INSERT Dropdown.ContractRange OFF
GO

INSERT INTO Dropdown.ContractRange 
	(ContractRangeName, DisplayOrder) 
VALUES
	('>  $150K and < $500K', 1),
	('>= $500K and < $1M', 2),
	('>= $1M and < $2M', 3),
	('>= $2M and < $5M', 4),
	('>= $5M and < $10M', 5),
	('>= $10M and < $20M', 6),
	('>= $20M and < $50M', 7),
	('>= $50M and < $100M', 8),
	('> $100M', 9),
	('TBD', 10)
GO
--End table Dropdown.ContractRange

--Begin table Dropdown.ContractType
SET IDENTITY_INSERT Dropdown.ContractType ON
INSERT INTO Dropdown.ContractType (ContractTypeID) VALUES (0)
SET IDENTITY_INSERT Dropdown.ContractType OFF
GO

INSERT INTO Dropdown.ContractType 
	(ContractTypeName, DisplayOrder) 
VALUES
	('BOA', 1),
	('BPA', 2),
	('BPA Call', 3),
	('Definitive Contract', 4),
	('Delivery Order', 5),
	('FSS', 6),
	('GWAC', 7),
	('IDC', 8),
	('Purchase Order', 9),
	('TBD', 10)
GO
--End table Dropdown.ContractType

--Begin table Dropdown.NAICS
SET IDENTITY_INSERT Dropdown.NAICS ON
INSERT INTO Dropdown.NAICS (NAICSID) VALUES (0)
SET IDENTITY_INSERT Dropdown.NAICS OFF
GO

INSERT INTO Dropdown.NAICS 
	(NAICSCode,NAICSName)
VALUES
	('111110','Soybean Farming'),
	('111120','Oilseed (except Soybean) Farming'),
	('111130','Dry Pea and Bean Farming'),
	('111140','Wheat Farming'),
	('111150','Corn Farming'),
	('111160','Rice Farming'),
	('111191','Oilseed and Grain Combination Farming'),
	('111199','All Other Grain Farming'),
	('111211','Potato Farming'),
	('111219','Other Vegetable (except Potato) and Melon Farming'),
	('111310','Orange Groves'),
	('111320','Citrus (except Orange) Groves'),
	('111331','Apple Orchards'),
	('111332','Grape Vineyards'),
	('111333','Strawberry Farming'),
	('111334','Berry (except Strawberry) Farming'),
	('111335','Tree Nut Farming'),
	('111336','Fruit and Tree Nut Combination Farming'),
	('111339','Other Noncitrus Fruit Farming'),
	('111411','Mushroom Production'),
	('111419','Other Food Crops Grown Under Cover'),
	('111421','Nursery and Tree Production'),
	('111422','Floriculture Production'),
	('111910','Tobacco Farming'),
	('111920','Cotton Farming'),
	('111930','Sugarcane Farming'),
	('111940','Hay Farming'),
	('111991','Sugar Beet Farming'),
	('111992','Peanut Farming'),
	('111998','All Other Miscellaneous Crop Farming'),
	('112111','Beef Cattle Ranching and Farming'),
	('112112','Cattle Feedlots'),
	('112120','Dairy Cattle and Milk Production'),
	('112130','Dual-Purpose Cattle Ranching and Farming'),
	('112210','Hog and Pig Farming'),
	('112310','Chicken Egg Production'),
	('112320','Broilers and Other Meat Type Chicken Production'),
	('112330','Turkey Production'),
	('112340','Poultry Hatcheries'),
	('112390','Other Poultry Production'),
	('112410','Sheep Farming'),
	('112420','Goat Farming'),
	('112511','Finfish Farming and Fish Hatcheries'),
	('112512','Shellfish Farming'),
	('112519','Other Aquaculture'),
	('112910','Apiculture'),
	('112920','Horses and Other Equine Production'),
	('112930','Fur-Bearing Animal and Rabbit Production'),
	('112990','All Other Animal Production'),
	('113110','Timber Tract Operations'),
	('113210','Forest Nurseries and Gathering of Forest Products'),
	('113310','Logging'),
	('114111','Finfish Fishing'),
	('114112','Shellfish Fishing'),
	('114119','Other Marine Fishing'),
	('114210','Hunting and Trapping'),
	('115111','Cotton Ginning'),
	('115112','Soil Preparation, Planting, and Cultivating'),
	('115113','Crop Harvesting, Primarily by Machine'),
	('115114','Postharvest Crop Activities (except Cotton Ginning)'),
	('115115','Farm Labor Contractors and Crew Leaders'),
	('115116','Farm Management Services'),
	('115210','Support Activities for Animal Production'),
	('115310','Support Activities for Forestry'),
	('211111','Crude Petroleum and Natural Gas Extraction'),
	('211112','Natural Gas Liquid Extraction'),
	('212111','Bituminous Coal and Lignite Surface Mining'),
	('212112','Bituminous Coal Underground Mining'),
	('212113','Anthracite Mining'),
	('212210','Iron Ore Mining'),
	('212221','Gold Ore Mining'),
	('212222','Silver Ore Mining'),
	('212231','Lead Ore and Zinc Ore Mining'),
	('212234','Copper Ore and Nickel Ore Mining'),
	('212291','Uranium-Radium-Vanadium Ore Mining'),
	('212299','All Other Metal Ore Mining'),
	('212311','Dimension Stone Mining and Quarrying'),
	('212312','Crushed and Broken Limestone Mining and Quarrying'),
	('212313','Crushed and Broken Granite Mining and Quarrying'),
	('212319','Other Crushed and Broken Stone Mining and Quarrying'),
	('212321','Construction Sand and Gravel Mining'),
	('212322','Industrial Sand Mining'),
	('212324','Kaolin and Ball Clay Mining'),
	('212325','Clay and Ceramic and Refractory Minerals Mining'),
	('212391','Potash, Soda, and Borate Mineral Mining'),
	('212392','Phosphate Rock Mining'),
	('212393','Other Chemical and Fertilizer Mineral Mining'),
	('212399','All Other Nonmetallic Mineral Mining'),
	('213111','Drilling Oil and Gas Wells'),
	('213112','Support Activities for Oil and Gas Operations'),
	('213113','Support Activities for Coal Mining'),
	('213114','Support Activities for Metal Mining'),
	('213115','Support Activities for Nonmetallic Minerals (except Fuels) Mining'),
	('221111','Hydroelectric Power Generation'),
	('221112','Fossil Fuel Electric Power Generation'),
	('221113','Nuclear Electric Power Generation'),
	('221114','Solar Electric Power Generation'),
	('221115','Wind Electric Power Generation'),
	('221116','Geothermal Electric Power Generation'),
	('221117','Biomass Electric Power Generation'),
	('221118','Other Electric Power Generation'),
	('221121','Electric Bulk Power Transmission and Control'),
	('221122','Electric Power Distribution'),
	('221210','Natural Gas Distribution'),
	('221310','Water Supply and Irrigation Systems'),
	('221320','Sewage Treatment Facilities'),
	('221330','Steam and Air-Conditioning Supply'),
	('236115','New Single-Family Housing Construction (except For-Sale Builders)'),
	('236116','New Multifamily Housing Construction (except For-Sale Builders)'),
	('236117','New Housing For-Sale Builders'),
	('236118','Residential Remodelers'),
	('236210','Industrial Building Construction'),
	('236220','Commercial and Institutional Building Construction'),
	('237110','Water and Sewer Line and Related Structures Construction'),
	('237120','Oil and Gas Pipeline and Related Structures Construction'),
	('237130','Power and Communication Line and Related Structures Construction'),
	('237210','Land Subdivision'),
	('237310','Highway, Street, and Bridge Construction'),
	('237990','Other Heavy and Civil Engineering Construction'),
	('238110','Poured Concrete Foundation and Structure Contractors'),
	('238120','Structural Steel and Precast Concrete Contractors'),
	('238130','Framing Contractors'),
	('238140','Masonry Contractors'),
	('238150','Glass and Glazing Contractors'),
	('238160','Roofing Contractors'),
	('238170','Siding Contractors'),
	('238190','Other Foundation, Structure, and Building Exterior Contractors'),
	('238210','Electrical Contractors and Other Wiring Installation Contractors'),
	('238220','Plumbing, Heating, and Air-Conditioning Contractors'),
	('238290','Other Building Equipment Contractors'),
	('238310','Drywall and Insulation Contractors'),
	('238320','Painting and Wall Covering Contractors'),
	('238330','Flooring Contractors'),
	('238340','Tile and Terrazzo Contractors'),
	('238350','Finish Carpentry Contractors'),
	('238390','Other Building Finishing Contractors'),
	('238910','Site Preparation Contractors'),
	('238990','All Other Specialty Trade Contractors'),
	('311111','Dog and Cat Food Manufacturing'),
	('311119','Other Animal Food Manufacturing'),
	('311211','Flour Milling'),
	('311212','Rice Milling'),
	('311213','Malt Manufacturing'),
	('311221','Wet Corn Milling'),
	('311224','Soybean and Other Oilseed Processing'),
	('311225','Fats and Oils Refining and Blending'),
	('311230','Breakfast Cereal Manufacturing'),
	('311313','Beet Sugar Manufacturing'),
	('311314','Cane Sugar Manufacturing'),
	('311340','Nonchocolate Confectionery Manufacturing'),
	('311351','Chocolate and Confectionery Manufacturing from Cacao Beans'),
	('311352','Confectionery Manufacturing from Purchased Chocolate'),
	('311411','Frozen Fruit, Juice, and Vegetable Manufacturing'),
	('311412','Frozen Specialty Food Manufacturing'),
	('311421','Fruit and Vegetable Canning'),
	('311422','Specialty Canning'),
	('311423','Dried and Dehydrated Food Manufacturing'),
	('311511','Fluid Milk Manufacturing'),
	('311512','Creamery Butter Manufacturing'),
	('311513','Cheese Manufacturing'),
	('311514','Dry, Condensed, and Evaporated Dairy Product Manufacturing'),
	('311520','Ice Cream and Frozen Dessert Manufacturing'),
	('311611','Animal (except Poultry) Slaughtering'),
	('311612','Meat Processed from Carcasses'),
	('311613','Rendering and Meat Byproduct Processing'),
	('311615','Poultry Processing'),
	('311710','Seafood Product Preparation and Packaging'),
	('311811','Retail Bakeries'),
	('311812','Commercial Bakeries'),
	('311813','Frozen Cakes, Pies, and Other Pastries Manufacturing'),
	('311821','Cookie and Cracker Manufacturing'),
	('311824','Dry Pasta, Dough, and Flour Mixes Manufacturing from Purchased Flour'),
	('311830','Tortilla Manufacturing'),
	('311911','Roasted Nuts and Peanut Butter Manufacturing'),
	('311919','Other Snack Food Manufacturing'),
	('311920','Coffee and Tea Manufacturing'),
	('311930','Flavoring Syrup and Concentrate Manufacturing'),
	('311941','Mayonnaise, Dressing, and Other Prepared Sauce Manufacturing'),
	('311942','Spice and Extract Manufacturing'),
	('311991','Perishable Prepared Food Manufacturing'),
	('311999','All Other Miscellaneous Food Manufacturing'),
	('312111','Soft Drink Manufacturing'),
	('312112','Bottled Water Manufacturing'),
	('312113','Ice Manufacturing'),
	('312120','Breweries'),
	('312130','Wineries'),
	('312140','Distilleries'),
	('312230','Tobacco Manufacturing'),
	('313110','Fiber, Yarn, and Thread Mills'),
	('313210','Broadwoven Fabric Mills'),
	('313220','Narrow Fabric Mills and Schiffli Machine Embroidery'),
	('313230','Nonwoven Fabric Mills'),
	('313240','Knit Fabric Mills'),
	('313310','Textile and Fabric Finishing Mills'),
	('313320','Fabric Coating Mills'),
	('314110','Carpet and Rug Mills'),
	('314120','Curtain and Linen Mills'),
	('314910','Textile Bag and Canvas Mills'),
	('314994','Rope, Cordage, Twine, Tire Cord, and Tire Fabric Mills'),
	('314999','All Other Miscellaneous Textile Product Mills'),
	('315110','Hosiery and Sock Mills'),
	('315190','Other Apparel Knitting Mills'),
	('315210','Cut and Sew Apparel Contractors'),
	('315220','Men’s and Boys’ Cut and Sew Apparel Manufacturing'),
	('315240','Women’s, Girls’, and Infants’ Cut and Sew Apparel Manufacturing'),
	('315280','Other Cut and Sew Apparel Manufacturing'),
	('315990','Apparel Accessories and Other Apparel Manufacturing'),
	('316110','Leather and Hide Tanning and Finishing'),
	('316210','Footwear Manufacturing'),
	('316992','Women''s Handbag and Purse Manufacturing'),
	('316998','All Other Leather Good and Allied Product Manufacturing'),
	('321113','Sawmills'),
	('321114','Wood Preservation'),
	('321211','Hardwood Veneer and Plywood Manufacturing'),
	('321212','Softwood Veneer and Plywood Manufacturing'),
	('321213','Engineered Wood Member (except Truss) Manufacturing'),
	('321214','Truss Manufacturing'),
	('321219','Reconstituted Wood Product Manufacturing'),
	('321911','Wood Window and Door Manufacturing'),
	('321912','Cut Stock, Resawing Lumber, and Planing'),
	('321918','Other Millwork (including Flooring)'),
	('321920','Wood Container and Pallet Manufacturing'),
	('321991','Manufactured Home (Mobile Home) Manufacturing'),
	('321992','Prefabricated Wood Building Manufacturing'),
	('321999','All Other Miscellaneous Wood Product Manufacturing'),
	('322110','Pulp Mills'),
	('322121','Paper (except Newsprint) Mills'),
	('322122','Newsprint Mills'),
	('322130','Paperboard Mills'),
	('322211','Corrugated and Solid Fiber Box Manufacturing'),
	('322212','Folding Paperboard Box Manufacturing'),
	('322219','Other Paperboard Container Manufacturing'),
	('322220','Paper Bag and Coated and Treated Paper Manufacturing'),
	('322230','Stationery Product Manufacturing'),
	('322291','Sanitary Paper Product Manufacturing'),
	('322299','All Other Converted Paper Product Manufacturing'),
	('323111','Commercial Printing (except Screen and Books)'),
	('323113','Commercial Screen Printing'),
	('323117','Books Printing'),
	('323120','Support Activities for Printing'),
	('324110','Petroleum Refineries'),
	('324121','Asphalt Paving Mixture and Block Manufacturing'),
	('324122','Asphalt Shingle and Coating Materials Manufacturing'),
	('324191','Petroleum Lubricating Oil and Grease Manufacturing'),
	('324199','All Other Petroleum and Coal Products Manufacturing'),
	('325110','Petrochemical Manufacturing'),
	('325120','Industrial Gas Manufacturing'),
	('325130','Synthetic Dye and Pigment Manufacturing'),
	('325180','Other Basic Inorganic Chemical Manufacturing'),
	('325193','Ethyl Alcohol Manufacturing'),
	('325194','Cyclic Crude, Intermediate, and Gum and Wood Chemical Manufacturing'),
	('325199','All Other Basic Organic Chemical Manufacturing'),
	('325211','Plastics Material and Resin Manufacturing'),
	('325212','Synthetic Rubber Manufacturing'),
	('325220','Artificial and Synthetic Fibers and Filaments Manufacturing'),
	('325311','Nitrogenous Fertilizer Manufacturing'),
	('325312','Phosphatic Fertilizer Manufacturing'),
	('325314','Fertilizer (Mixing Only) Manufacturing'),
	('325320','Pesticide and Other Agricultural Chemical Manufacturing'),
	('325411','Medicinal and Botanical Manufacturing'),
	('325412','Pharmaceutical Preparation Manufacturing'),
	('325413','In-Vitro Diagnostic Substance Manufacturing'),
	('325414','Biological Product (except Diagnostic) Manufacturing'),
	('325510','Paint and Coating Manufacturing'),
	('325520','Adhesive Manufacturing'),
	('325611','Soap and Other Detergent Manufacturing'),
	('325612','Polish and Other Sanitation Good Manufacturing'),
	('325613','Surface Active Agent Manufacturing'),
	('325620','Toilet Preparation Manufacturing'),
	('325910','Printing Ink Manufacturing'),
	('325920','Explosives Manufacturing'),
	('325991','Custom Compounding of Purchased Resins'),
	('325992','Photographic Film, Paper, Plate, and Chemical Manufacturing'),
	('325998','All Other Miscellaneous Chemical Product and Preparation Manufacturing'),
	('326111','Plastics Bag and Pouch Manufacturing'),
	('326112','Plastics Packaging Film and Sheet (including Laminated) Manufacturing'),
	('326113','Unlaminated Plastics Film and Sheet (except Packaging) Manufacturing'),
	('326121','Unlaminated Plastics Profile Shape Manufacturing'),
	('326122','Plastics Pipe and Pipe Fitting Manufacturing'),
	('326130','Laminated Plastics Plate, Sheet (except Packaging), and Shape Manufacturing'),
	('326140','Polystyrene Foam Product Manufacturing'),
	('326150','Urethane and Other Foam Product (except Polystyrene) Manufacturing'),
	('326160','Plastics Bottle Manufacturing'),
	('326191','Plastics Plumbing Fixture Manufacturing'),
	('326199','All Other Plastics Product Manufacturing'),
	('326211','Tire Manufacturing (except Retreading)'),
	('326212','Tire Retreading'),
	('326220','Rubber and Plastics Hoses and Belting Manufacturing'),
	('326291','Rubber Product Manufacturing for Mechanical Use'),
	('326299','All Other Rubber Product Manufacturing'),
	('327110','Pottery, Ceramics, and Plumbing Fixture Manufacturing'),
	('327120','Clay Building Material and Refractories Manufacturing'),
	('327211','Flat Glass Manufacturing'),
	('327212','Other Pressed and Blown Glass and Glassware Manufacturing'),
	('327213','Glass Container Manufacturing'),
	('327215','Glass Product Manufacturing Made of Purchased Glass'),
	('327310','Cement Manufacturing'),
	('327320','Ready-Mix Concrete Manufacturing'),
	('327331','Concrete Block and Brick Manufacturing'),
	('327332','Concrete Pipe Manufacturing'),
	('327390','Other Concrete Product Manufacturing'),
	('327410','Lime Manufacturing'),
	('327420','Gypsum Product Manufacturing'),
	('327910','Abrasive Product Manufacturing'),
	('327991','Cut Stone and Stone Product Manufacturing'),
	('327992','Ground or Treated Mineral and Earth Manufacturing'),
	('327993','Mineral Wool Manufacturing'),
	('327999','All Other Miscellaneous Nonmetallic Mineral Product Manufacturing'),
	('331110','Iron and Steel Mills and Ferroalloy Manufacturing'),
	('331210','Iron and Steel Pipe and Tube Manufacturing from Purchased Steel'),
	('331221','Rolled Steel Shape Manufacturing'),
	('331222','Steel Wire Drawing'),
	('331313','Alumina Refining and Primary Aluminum Production'),
	('331314','Secondary Smelting and Alloying of Aluminum'),
	('331315','Aluminum Sheet, Plate, and Foil Manufacturing'),
	('331318','Other Aluminum Rolling, Drawing, and Extruding'),
	('331410','Nonferrous Metal (except Aluminum) Smelting and Refining'),
	('331420','Copper Rolling, Drawing, Extruding, and Alloying'),
	('331491','Nonferrous Metal (except Copper and Aluminum) Rolling, Drawing, and Extruding'),
	('331492','Secondary Smelting, Refining, and Alloying of Nonferrous Metal (except Copper and Aluminum)'),
	('331511','Iron Foundries'),
	('331512','Steel Investment Foundries'),
	('331513','Steel Foundries (except Investment)'),
	('331523','Nonferrous Metal Die-Casting Foundries'),
	('331524','Aluminum Foundries (except Die-Casting)'),
	('331529','Other Nonferrous Metal Foundries (except Die-Casting)'),
	('332111','Iron and Steel Forging'),
	('332112','Nonferrous Forging'),
	('332114','Custom Roll Forming'),
	('332117','Powder Metallurgy Part Manufacturing'),
	('332119','Metal Crown, Closure, and Other Metal Stamping (except Automotive)'),
	('332215','Metal Kitchen Cookware, Utensil, Cutlery, and Flatware (except Precious) Manufacturing'),
	('332216','Saw Blade and Handtool Manufacturing'),
	('332311','Prefabricated Metal Building and Component Manufacturing'),
	('332312','Fabricated Structural Metal Manufacturing'),
	('332313','Plate Work Manufacturing'),
	('332321','Metal Window and Door Manufacturing'),
	('332322','Sheet Metal Work Manufacturing'),
	('332323','Ornamental and Architectural Metal Work Manufacturing'),
	('332410','Power Boiler and Heat Exchanger Manufacturing'),
	('332420','Metal Tank (Heavy Gauge) Manufacturing'),
	('332431','Metal Can Manufacturing'),
	('332439','Other Metal Container Manufacturing'),
	('332510','Hardware Manufacturing'),
	('332613','Spring Manufacturing'),
	('332618','Other Fabricated Wire Product Manufacturing'),
	('332710','Machine Shops'),
	('332721','Precision Turned Product Manufacturing'),
	('332722','Bolt, Nut, Screw, Rivet, and Washer Manufacturing'),
	('332811','Metal Heat Treating'),
	('332812','Metal Coating, Engraving (except Jewelry and Silverware), and Allied Services to Manufacturers'),
	('332813','Electroplating, Plating, Polishing, Anodizing, and Coloring'),
	('332911','Industrial Valve Manufacturing'),
	('332912','Fluid Power Valve and Hose Fitting Manufacturing'),
	('332913','Plumbing Fixture Fitting and Trim Manufacturing'),
	('332919','Other Metal Valve and Pipe Fitting Manufacturing'),
	('332991','Ball and Roller Bearing Manufacturing'),
	('332992','Small Arms Ammunition Manufacturing'),
	('332993','Ammunition (except Small Arms) Manufacturing'),
	('332994','Small Arms, Ordnance, and Ordnance Accessories Manufacturing'),
	('332996','Fabricated Pipe and Pipe Fitting Manufacturing'),
	('332999','All Other Miscellaneous Fabricated Metal Product Manufacturing'),
	('333111','Farm Machinery and Equipment Manufacturing'),
	('333112','Lawn and Garden Tractor and Home Lawn and Garden Equipment Manufacturing'),
	('333120','Construction Machinery Manufacturing'),
	('333131','Mining Machinery and Equipment Manufacturing'),
	('333132','Oil and Gas Field Machinery and Equipment Manufacturing'),
	('333241','Food Product Machinery Manufacturing'),
	('333242','Semiconductor Machinery Manufacturing'),
	('333243','Sawmill, Woodworking, and Paper Machinery Manufacturing'),
	('333244','Printing Machinery and Equipment Manufacturing'),
	('333249','Other Industrial Machinery Manufacturing'),
	('333314','Optical Instrument and Lens Manufacturing'),
	('333316','Photographic and Photocopying Equipment Manufacturing'),
	('333318','Other Commercial and Service Industry Machinery Manufacturing'),
	('333413','Industrial and Commercial Fan and Blower and Air Purification Equipment Manufacturing'),
	('333414','Heating Equipment (except Warm Air Furnaces) Manufacturing'),
	('333415','Air-Conditioning and Warm Air Heating Equipment and Commercial and Industrial Refrigeration Equipment Manufacturing'),
	('333511','Industrial Mold Manufacturing'),
	('333514','Special Die and Tool, Die Set, Jig, and Fixture Manufacturing'),
	('333515','Cutting Tool and Machine Tool Accessory Manufacturing'),
	('333517','Machine Tool Manufacturing'),
	('333519','Rolling Mill and Other Metalworking Machinery Manufacturing'),
	('333611','Turbine and Turbine Generator Set Units Manufacturing'),
	('333612','Speed Changer, Industrial High-Speed Drive, and Gear Manufacturing'),
	('333613','Mechanical Power Transmission Equipment Manufacturing'),
	('333618','Other Engine Equipment Manufacturing'),
	('333911','Pump and Pumping Equipment Manufacturing'),
	('333912','Air and Gas Compressor Manufacturing'),
	('333913','Measuring and Dispensing Pump Manufacturing'),
	('333921','Elevator and Moving Stairway Manufacturing'),
	('333922','Conveyor and Conveying Equipment Manufacturing'),
	('333923','Overhead Traveling Crane, Hoist, and Monorail System Manufacturing'),
	('333924','Industrial Truck, Tractor, Trailer, and Stacker Machinery Manufacturing'),
	('333991','Power-Driven Handtool Manufacturing'),
	('333992','Welding and Soldering Equipment Manufacturing'),
	('333993','Packaging Machinery Manufacturing'),
	('333994','Industrial Process Furnace and Oven Manufacturing'),
	('333995','Fluid Power Cylinder and Actuator Manufacturing'),
	('333996','Fluid Power Pump and Motor Manufacturing'),
	('333997','Scale and Balance Manufacturing'),
	('333999','All Other Miscellaneous General Purpose Machinery Manufacturing'),
	('334111','Electronic Computer Manufacturing'),
	('334112','Computer Storage Device Manufacturing'),
	('334118','Computer Terminal and Other Computer Peripheral Equipment Manufacturing'),
	('334210','Telephone Apparatus Manufacturing'),
	('334220','Radio and Television Broadcasting and Wireless Communications Equipment Manufacturing'),
	('334290','Other Communications Equipment Manufacturing'),
	('334310','Audio and Video Equipment Manufacturing'),
	('334412','Bare Printed Circuit Board Manufacturing'),
	('334413','Semiconductor and Related Device Manufacturing'),
	('334416','Capacitor, Resistor, Coil, Transformer, and Other Inductor Manufacturing'),
	('334417','Electronic Connector Manufacturing'),
	('334418','Printed Circuit Assembly (Electronic Assembly) Manufacturing'),
	('334419','Other Electronic Component Manufacturing'),
	('334510','Electromedical and Electrotherapeutic Apparatus Manufacturing'),
	('334511','Search, Detection, Navigation, Guidance, Aeronautical, and Nautical System and Instrument Manufacturing'),
	('334512','Automatic Environmental Control Manufacturing for Residential, Commercial, and Appliance Use'),
	('334513','Instruments and Related Products Manufacturing for Measuring, Displaying, and Controlling Industrial Process Variables'),
	('334514','Totalizing Fluid Meter and Counting Device Manufacturing'),
	('334515','Instrument Manufacturing for Measuring and Testing Electricity and Electrical Signals'),
	('334516','Analytical Laboratory Instrument Manufacturing'),
	('334517','Irradiation Apparatus Manufacturing'),
	('334519','Other Measuring and Controlling Device Manufacturing'),
	('334613','Blank Magnetic and Optical Recording Media Manufacturing'),
	('334614','Software and Other Prerecorded Compact Disc, Tape, and Record Reproducing'),
	('335110','Electric Lamp Bulb and Part Manufacturing'),
	('335121','Residential Electric Lighting Fixture Manufacturing'),
	('335122','Commercial, Industrial, and Institutional Electric Lighting Fixture Manufacturing'),
	('335129','Other Lighting Equipment Manufacturing'),
	('335210','Small Electrical Appliance Manufacturing'),
	('335221','Household Cooking Appliance Manufacturing'),
	('335222','Household Refrigerator and Home Freezer Manufacturing'),
	('335224','Household Laundry Equipment Manufacturing'),
	('335228','Other Major Household Appliance Manufacturing'),
	('335311','Power, Distribution, and Specialty Transformer Manufacturing'),
	('335312','Motor and Generator Manufacturing'),
	('335313','Switchgear and Switchboard Apparatus Manufacturing'),
	('335314','Relay and Industrial Control Manufacturing'),
	('335911','Storage Battery Manufacturing'),
	('335912','Primary Battery Manufacturing'),
	('335921','Fiber Optic Cable Manufacturing'),
	('335929','Other Communication and Energy Wire Manufacturing'),
	('335931','Current-Carrying Wiring Device Manufacturing'),
	('335932','Noncurrent-Carrying Wiring Device Manufacturing'),
	('335991','Carbon and Graphite Product Manufacturing'),
	('335999','All Other Miscellaneous Electrical Equipment and Component Manufacturing'),
	('336111','Automobile Manufacturing'),
	('336112','Light Truck and Utility Vehicle Manufacturing'),
	('336120','Heavy Duty Truck Manufacturing'),
	('336211','Motor Vehicle Body Manufacturing'),
	('336212','Truck Trailer Manufacturing'),
	('336213','Motor Home Manufacturing'),
	('336214','Travel Trailer and Camper Manufacturing'),
	('336310','Motor Vehicle Gasoline Engine and Engine Parts Manufacturing'),
	('336320','Motor Vehicle Electrical and Electronic Equipment Manufacturing'),
	('336330','Motor Vehicle Steering and Suspension Components (except Spring) Manufacturing'),
	('336340','Motor Vehicle Brake System Manufacturing'),
	('336350','Motor Vehicle Transmission and Power Train Parts Manufacturing'),
	('336360','Motor Vehicle Seating and Interior Trim Manufacturing'),
	('336370','Motor Vehicle Metal Stamping'),
	('336390','Other Motor Vehicle Parts Manufacturing'),
	('336411','Aircraft Manufacturing'),
	('336412','Aircraft Engine and Engine Parts Manufacturing'),
	('336413','Other Aircraft Parts and Auxiliary Equipment Manufacturing'),
	('336414','Guided Missile and Space Vehicle Manufacturing'),
	('336415','Guided Missile and Space Vehicle Propulsion Unit and Propulsion Unit Parts Manufacturing'),
	('336419','Other Guided Missile and Space Vehicle Parts and Auxiliary Equipment Manufacturing'),
	('336510','Railroad Rolling Stock Manufacturing'),
	('336611','Ship Building and Repairing'),
	('336612','Boat Building'),
	('336991','Motorcycle, Bicycle, and Parts Manufacturing'),
	('336992','Military Armored Vehicle, Tank, and Tank Component Manufacturing'),
	('336999','All Other Transportation Equipment Manufacturing'),
	('337110','Wood Kitchen Cabinet and Countertop Manufacturing'),
	('337121','Upholstered Household Furniture Manufacturing'),
	('337122','Nonupholstered Wood Household Furniture Manufacturing'),
	('337124','Metal Household Furniture Manufacturing'),
	('337125','Household Furniture (except Wood and Metal) Manufacturing'),
	('337127','Institutional Furniture Manufacturing'),
	('337211','Wood Office Furniture Manufacturing'),
	('337212','Custom Architectural Woodwork and Millwork Manufacturing'),
	('337214','Office Furniture (except Wood) Manufacturing'),
	('337215','Showcase, Partition, Shelving, and Locker Manufacturing'),
	('337910','Mattress Manufacturing'),
	('337920','Blind and Shade Manufacturing'),
	('339112','Surgical and Medical Instrument Manufacturing'),
	('339113','Surgical Appliance and Supplies Manufacturing'),
	('339114','Dental Equipment and Supplies Manufacturing'),
	('339115','Ophthalmic Goods Manufacturing'),
	('339116','Dental Laboratories'),
	('339910','Jewelry and Silverware Manufacturing'),
	('339920','Sporting and Athletic Goods Manufacturing'),
	('339930','Doll, Toy, and Game Manufacturing'),
	('339940','Office Supplies (except Paper) Manufacturing'),
	('339950','Sign Manufacturing'),
	('339991','Gasket, Packing, and Sealing Device Manufacturing'),
	('339992','Musical Instrument Manufacturing'),
	('339993','Fastener, Button, Needle, and Pin Manufacturing'),
	('339994','Broom, Brush, and Mop Manufacturing')
GO

INSERT INTO Dropdown.NAICS 
	(NAICSCode,NAICSName) 
VALUES
	('339995','Burial Casket Manufacturing'),
	('339999','All Other Miscellaneous Manufacturing'),
	('423110','Automobile and Other Motor Vehicle Merchant Wholesalers'),
	('423120','Motor Vehicle Supplies and New Parts Merchant Wholesalers'),
	('423130','Tire and Tube Merchant Wholesalers'),
	('423140','Motor Vehicle Parts (Used) Merchant Wholesalers'),
	('423210','Furniture Merchant Wholesalers'),
	('423220','Home Furnishing Merchant Wholesalers'),
	('423310','Lumber, Plywood, Millwork, and Wood Panel Merchant Wholesalers'),
	('423320','Brick, Stone, and Related Construction Material Merchant Wholesalers'),
	('423330','Roofing, Siding, and Insulation Material Merchant Wholesalers'),
	('423390','Other Construction Material Merchant Wholesalers'),
	('423410','Photographic Equipment and Supplies Merchant Wholesalers'),
	('423420','Office Equipment Merchant Wholesalers'),
	('423430','Computer and Computer Peripheral Equipment and Software Merchant Wholesalers'),
	('423440','Other Commercial Equipment Merchant Wholesalers'),
	('423450','Medical, Dental, and Hospital Equipment and Supplies Merchant Wholesalers'),
	('423460','Ophthalmic Goods Merchant Wholesalers'),
	('423490','Other Professional Equipment and Supplies Merchant Wholesalers'),
	('423510','Metal Service Centers and Other Metal Merchant Wholesalers'),
	('423520','Coal and Other Mineral and Ore Merchant Wholesalers'),
	('423610','Electrical Apparatus and Equipment, Wiring Supplies, and Related Equipment Merchant Wholesalers'),
	('423620','Household Appliances, Electric Housewares, and Consumer Electronics Merchant Wholesalers'),
	('423690','Other Electronic Parts and Equipment Merchant Wholesalers'),
	('423710','Hardware Merchant Wholesalers'),
	('423720','Plumbing and Heating Equipment and Supplies (Hydronics) Merchant Wholesalers'),
	('423730','Warm Air Heating and Air-Conditioning Equipment and Supplies Merchant Wholesalers'),
	('423740','Refrigeration Equipment and Supplies Merchant Wholesalers'),
	('423810','Construction and Mining (except Oil Well) Machinery and Equipment Merchant Wholesalers'),
	('423820','Farm and Garden Machinery and Equipment Merchant Wholesalers'),
	('423830','Industrial Machinery and Equipment Merchant Wholesalers'),
	('423840','Industrial Supplies Merchant Wholesalers'),
	('423850','Service Establishment Equipment and Supplies Merchant Wholesalers'),
	('423860','Transportation Equipment and Supplies (except Motor Vehicle) Merchant Wholesalers'),
	('423910','Sporting and Recreational Goods and Supplies Merchant Wholesalers'),
	('423920','Toy and Hobby Goods and Supplies Merchant Wholesalers'),
	('423930','Recyclable Material Merchant Wholesalers'),
	('423940','Jewelry, Watch, Precious Stone, and Precious Metal Merchant Wholesalers'),
	('423990','Other Miscellaneous Durable Goods Merchant Wholesalers'),
	('424110','Printing and Writing Paper Merchant Wholesalers'),
	('424120','Stationery and Office Supplies Merchant Wholesalers'),
	('424130','Industrial and Personal Service Paper Merchant Wholesalers'),
	('424210','Drugs and Druggists'' Sundries Merchant Wholesalers'),
	('424310','Piece Goods, Notions, and Other Dry Goods Merchant Wholesalers'),
	('424320','Men''s and Boy''s Clothing and Furnishings Merchant Wholesalers'),
	('424330','Women''s, Children''s, and Infant''s Clothing and Accessories Merchant Wholesalers'),
	('424340','Footwear Merchant Wholesalers'),
	('424410','General Line Grocery Merchant Wholesalers'),
	('424420','Packaged Frozen Food Merchant Wholesalers'),
	('424430','Dairy Product (except Dried or Canned) Merchant Wholesalers'),
	('424440','Poultry and Poultry Product Merchant Wholesalers'),
	('424450','Confectionery Merchant Wholesalers'),
	('424460','Fish and Seafood Merchant Wholesalers'),
	('424470','Meat and Meat Product Merchant Wholesalers'),
	('424480','Fresh Fruit and Vegetable Merchant Wholesalers'),
	('424490','Other Grocery and Related Products Merchant Wholesalers'),
	('424510','Grain and Field Bean Merchant Wholesalers'),
	('424520','Livestock Merchant Wholesalers'),
	('424590','Other Farm Product Raw Material Merchant Wholesalers'),
	('424610','Plastics Materials and Basic Forms and Shapes Merchant Wholesalers'),
	('424690','Other Chemical and Allied Products Merchant Wholesalers'),
	('424710','Petroleum Bulk Stations and Terminals'),
	('424720','Petroleum and Petroleum Products Merchant Wholesalers (except Bulk Stations and Terminals)'),
	('424810','Beer and Ale Merchant Wholesalers'),
	('424820','Wine and Distilled Alcoholic Beverage Merchant Wholesalers'),
	('424910','Farm Supplies Merchant Wholesalers'),
	('424920','Book, Periodical, and Newspaper Merchant Wholesalers'),
	('424930','Flower, Nursery Stock, and Florists'' Supplies Merchant Wholesalers'),
	('424940','Tobacco and Tobacco Product Merchant Wholesalers'),
	('424950','Paint, Varnish, and Supplies Merchant Wholesalers'),
	('424990','Other Miscellaneous Nondurable Goods Merchant Wholesalers'),
	('425110','Business to Business Electronic Markets'),
	('425120','Wholesale Trade Agents and Brokers'),
	('441110','New Car Dealers'),
	('441120','Used Car Dealers'),
	('441210','Recreational Vehicle Dealers'),
	('441222','Boat Dealers'),
	('441228','Motorcycle, ATV, and All Other Motor Vehicle Dealers'),
	('441310','Automotive Parts and Accessories Stores'),
	('441320','Tire Dealers'),
	('442110','Furniture Stores'),
	('442210','Floor Covering Stores'),
	('442291','Window Treatment Stores'),
	('442299','All Other Home Furnishings Stores'),
	('443141','Household Appliance Stores'),
	('443142','Electronics Stores'),
	('444110','Home Centers'),
	('444120','Paint and Wallpaper Stores'),
	('444130','Hardware Stores'),
	('444190','Other Building Material Dealers'),
	('444210','Outdoor Power Equipment Stores'),
	('444220','Nursery, Garden Center, and Farm Supply Stores'),
	('445110','Supermarkets and Other Grocery (except Convenience) Stores'),
	('445120','Convenience Stores'),
	('445210','Meat Markets'),
	('445220','Fish and Seafood Markets'),
	('445230','Fruit and Vegetable Markets'),
	('445291','Baked Goods Stores'),
	('445292','Confectionery and Nut Stores'),
	('445299','All Other Specialty Food Stores'),
	('445310','Beer, Wine, and Liquor Stores'),
	('446110','Pharmacies and Drug Stores'),
	('446120','Cosmetics, Beauty Supplies, and Perfume Stores'),
	('446130','Optical Goods Stores'),
	('446191','Food (Health) Supplement Stores'),
	('446199','All Other Health and Personal Care Stores'),
	('447110','Gasoline Stations with Convenience Stores'),
	('447190','Other Gasoline Stations'),
	('448110','Men''s Clothing Stores'),
	('448120','Women''s Clothing Stores'),
	('448130','Children''s and Infant''s Clothing Stores'),
	('448140','Family Clothing Stores'),
	('448150','Clothing Accessories Stores'),
	('448190','Other Clothing Stores'),
	('448210','Shoe Stores'),
	('448310','Jewelry Stores'),
	('448320','Luggage and Leather Goods Stores'),
	('451110','Sporting Goods Stores'),
	('451120','Hobby, Toy, and Game Stores'),
	('451130','Sewing, Needlework, and Piece Goods Stores'),
	('451140','Musical Instrument and Supplies Stores'),
	('451211','Book Stores'),
	('451212','News Dealers and Newsstands'),
	('452111','Department Stores (except Discount Department Stores)'),
	('452112','Discount Department Stores'),
	('452910','Warehouse Clubs and Supercenters'),
	('452990','All Other General Merchandise Stores'),
	('453110','Florists'),
	('453210','Office Supplies and Stationery Stores'),
	('453220','Gift, Novelty, and Souvenir Stores'),
	('453310','Used Merchandise Stores'),
	('453910','Pet and Pet Supplies Stores'),
	('453920','Art Dealers'),
	('453930','Manufactured (Mobile) Home Dealers'),
	('453991','Tobacco Stores'),
	('453998','All Other Miscellaneous Store Retailers (except Tobacco Stores)'),
	('454111','Electronic Shopping'),
	('454112','Electronic Auctions'),
	('454113','Mail-Order Houses'),
	('454210','Vending Machine Operators'),
	('454310','Fuel Dealers'),
	('454390','Other Direct Selling Establishments'),
	('481111','Scheduled Passenger Air Transportation'),
	('481112','Scheduled Freight Air Transportation'),
	('481211','Nonscheduled Chartered Passenger Air Transportation'),
	('481212','Nonscheduled Chartered Freight Air Transportation'),
	('481219','Other Nonscheduled Air Transportation'),
	('482111','Line-Haul Railroads'),
	('482112','Short Line Railroads'),
	('483111','Deep Sea Freight Transportation'),
	('483112','Deep Sea Passenger Transportation'),
	('483113','Coastal and Great Lakes Freight Transportation'),
	('483114','Coastal and Great Lakes Passenger Transportation'),
	('483211','Inland Water Freight Transportation'),
	('483212','Inland Water Passenger Transportation'),
	('484110','General Freight Trucking, Local'),
	('484121','General Freight Trucking, Long-Distance, Truckload'),
	('484122','General Freight Trucking, Long-Distance, Less Than Truckload'),
	('484210','Used Household and Office Goods Moving'),
	('484220','Specialized Freight (except Used Goods) Trucking, Local'),
	('484230','Specialized Freight (except Used Goods) Trucking, Long-Distance'),
	('485111','Mixed Mode Transit Systems'),
	('485112','Commuter Rail Systems'),
	('485113','Bus and Other Motor Vehicle Transit Systems'),
	('485119','Other Urban Transit Systems'),
	('485210','Interurban and Rural Bus Transportation'),
	('485310','Taxi Service'),
	('485320','Limousine Service'),
	('485410','School and Employee Bus Transportation'),
	('485510','Charter Bus Industry'),
	('485991','Special Needs Transportation'),
	('485999','All Other Transit and Ground Passenger Transportation'),
	('486110','Pipeline Transportation of Crude Oil'),
	('486210','Pipeline Transportation of Natural Gas'),
	('486910','Pipeline Transportation of Refined Petroleum Products'),
	('486990','All Other Pipeline Transportation'),
	('487110','Scenic and Sightseeing Transportation, Land'),
	('487210','Scenic and Sightseeing Transportation, Water'),
	('487990','Scenic and Sightseeing Transportation, Other'),
	('488111','Air Traffic Control'),
	('488119','Other Airport Operations'),
	('488190','Other Support Activities for Air Transportation'),
	('488210','Support Activities for Rail Transportation'),
	('488310','Port and Harbor Operations'),
	('488320','Marine Cargo Handling'),
	('488330','Navigational Services to Shipping'),
	('488390','Other Support Activities for Water Transportation'),
	('488410','Motor Vehicle Towing'),
	('488490','Other Support Activities for Road Transportation'),
	('488510','Freight Transportation Arrangement'),
	('488991','Packing and Crating'),
	('488999','All Other Support Activities for Transportation'),
	('491110','Postal Service'),
	('492110','Couriers and Express Delivery Services'),
	('492210','Local Messengers and Local Delivery'),
	('493110','General Warehousing and Storage'),
	('493120','Refrigerated Warehousing and Storage'),
	('493130','Farm Product Warehousing and Storage'),
	('493190','Other Warehousing and Storage'),
	('511110','Newspaper Publishers'),
	('511120','Periodical Publishers'),
	('511130','Book Publishers'),
	('511140','Directory and Mailing List Publishers'),
	('511191','Greeting Card Publishers'),
	('511199','All Other Publishers'),
	('511210','Software Publishers'),
	('512110','Motion Picture and Video Production'),
	('512120','Motion Picture and Video Distribution'),
	('512131','Motion Picture Theaters (except Drive-Ins)'),
	('512132','Drive-In Motion Picture Theaters'),
	('512191','Teleproduction and Other Postproduction Services'),
	('512199','Other Motion Picture and Video Industries'),
	('512210','Record Production'),
	('512220','Integrated Record Production/Distribution'),
	('512230','Music Publishers'),
	('512240','Sound Recording Studios'),
	('512290','Other Sound Recording Industries'),
	('515111','Radio Networks'),
	('515112','Radio Stations'),
	('515120','Television Broadcasting'),
	('515210','Cable and Other Subscription Programming'),
	('517110','Wired Telecommunications Carriers'),
	('517210','Wireless Telecommunications Carriers (except Satellite)'),
	('517410','Satellite Telecommunications'),
	('517911','Telecommunications Resellers'),
	('517919','All Other Telecommunications'),
	('518210','Data Processing, Hosting, and Related Services'),
	('519110','News Syndicates'),
	('519120','Libraries and Archives'),
	('519130','Internet Publishing and Broadcasting and Web Search Portals'),
	('519190','All Other Information Services'),
	('521110','Monetary Authorities-Central Bank'),
	('522110','Commercial Banking'),
	('522120','Savings Institutions'),
	('522130','Credit Unions'),
	('522190','Other Depository Credit Intermediation'),
	('522210','Credit Card Issuing'),
	('522220','Sales Financing'),
	('522291','Consumer Lending'),
	('522292','Real Estate Credit'),
	('522293','International Trade Financing'),
	('522294','Secondary Market Financing'),
	('522298','All Other Nondepository Credit Intermediation'),
	('522310','Mortgage and Nonmortgage Loan Brokers'),
	('522320','Financial Transactions Processing, Reserve, and Clearinghouse Activities'),
	('522390','Other Activities Related to Credit Intermediation'),
	('523110','Investment Banking and Securities Dealing'),
	('523120','Securities Brokerage'),
	('523130','Commodity Contracts Dealing'),
	('523140','Commodity Contracts Brokerage'),
	('523210','Securities and Commodity Exchanges'),
	('523910','Miscellaneous Intermediation'),
	('523920','Portfolio Management'),
	('523930','Investment Advice'),
	('523991','Trust, Fiduciary, and Custody Activities'),
	('523999','Miscellaneous Financial Investment Activities'),
	('524113','Direct Life Insurance Carriers'),
	('524114','Direct Health and Medical Insurance Carriers'),
	('524126','Direct Property and Casualty Insurance Carriers'),
	('524127','Direct Title Insurance Carriers'),
	('524128','Other Direct Insurance (except Life, Health, and Medical) Carriers'),
	('524130','Reinsurance Carriers'),
	('524210','Insurance Agencies and Brokerages'),
	('524291','Claims Adjusting'),
	('524292','Third Party Administration of Insurance and Pension Funds'),
	('524298','All Other Insurance Related Activities'),
	('525110','Pension Funds'),
	('525120','Health and Welfare Funds'),
	('525190','Other Insurance Funds'),
	('525910','Open-End Investment Funds'),
	('525920','Trusts, Estates, and Agency Accounts'),
	('525990','Other Financial Vehicles'),
	('531110','Lessors of Residential Buildings and Dwellings'),
	('531120','Lessors of Nonresidential Buildings (except Miniwarehouses)'),
	('531130','Lessors of Miniwarehouses and Self-Storage Units'),
	('531190','Lessors of Other Real Estate Property'),
	('531210','Offices of Real Estate Agents and Brokers'),
	('531311','Residential Property Managers'),
	('531312','Nonresidential Property Managers'),
	('531320','Offices of Real Estate Appraisers'),
	('531390','Other Activities Related to Real Estate'),
	('532111','Passenger Car Rental'),
	('532112','Passenger Car Leasing'),
	('532120','Truck, Utility Trailer, and RV (Recreational Vehicle) Rental and Leasing'),
	('532210','Consumer Electronics and Appliances Rental'),
	('532220','Formal Wear and Costume Rental'),
	('532230','Video Tape and Disc Rental'),
	('532291','Home Health Equipment Rental'),
	('532292','Recreational Goods Rental'),
	('532299','All Other Consumer Goods Rental'),
	('532310','General Rental Centers'),
	('532411','Commercial Air, Rail, and Water Transportation Equipment Rental and Leasing'),
	('532412','Construction, Mining, and Forestry Machinery and Equipment Rental and Leasing'),
	('532420','Office Machinery and Equipment Rental and Leasing'),
	('532490','Other Commercial and Industrial Machinery and Equipment Rental and Leasing'),
	('533110','Lessors of Nonfinancial Intangible Assets (except Copyrighted Works)'),
	('541110','Offices of Lawyers'),
	('541120','Offices of Notaries'),
	('541191','Title Abstract and Settlement Offices'),
	('541199','All Other Legal Services'),
	('541211','Offices of Certified Public Accountants'),
	('541213','Tax Preparation Services'),
	('541214','Payroll Services'),
	('541219','Other Accounting Services'),
	('541310','Architectural Services'),
	('541320','Landscape Architectural Services'),
	('541330','Engineering Services'),
	('541340','Drafting Services'),
	('541350','Building Inspection Services'),
	('541360','Geophysical Surveying and Mapping Services'),
	('541370','Surveying and Mapping (except Geophysical) Services'),
	('541380','Testing Laboratories'),
	('541410','Interior Design Services'),
	('541420','Industrial Design Services'),
	('541430','Graphic Design Services'),
	('541490','Other Specialized Design Services'),
	('541511','Custom Computer Programming Services'),
	('541512','Computer Systems Design Services'),
	('541513','Computer Facilities Management Services'),
	('541519','Other Computer Related Services'),
	('541611','Administrative Management and General Management Consulting Services'),
	('541612','Human Resources Consulting Services'),
	('541613','Marketing Consulting Services'),
	('541614','Process, Physical Distribution, and Logistics Consulting Services'),
	('541618','Other Management Consulting Services'),
	('541620','Environmental Consulting Services'),
	('541690','Other Scientific and Technical Consulting Services'),
	('541711','Research and Development in Biotechnology'),
	('541712','Research and Development in the Physical, Engineering, and Life Sciences (except Biotechnology)'),
	('541720','Research and Development in the Social Sciences and Humanities'),
	('541810','Advertising Agencies'),
	('541820','Public Relations Agencies'),
	('541830','Media Buying Agencies'),
	('541840','Media Representatives'),
	('541850','Outdoor Advertising'),
	('541860','Direct Mail Advertising'),
	('541870','Advertising Material Distribution Services'),
	('541890','Other Services Related to Advertising'),
	('541910','Marketing Research and Public Opinion Polling'),
	('541921','Photography Studios, Portrait'),
	('541922','Commercial Photography'),
	('541930','Translation and Interpretation Services'),
	('541940','Veterinary Services'),
	('541990','All Other Professional, Scientific, and Technical Services'),
	('551111','Offices of Bank Holding Companies'),
	('551112','Offices of Other Holding Companies'),
	('551114','Corporate, Subsidiary, and Regional Managing Offices'),
	('561110','Office Administrative Services'),
	('561210','Facilities Support Services'),
	('561311','Employment Placement Agencies'),
	('561312','Executive Search Services'),
	('561320','Temporary Help Services'),
	('561330','Professional Employer Organizations'),
	('561410','Document Preparation Services'),
	('561421','Telephone Answering Services'),
	('561422','Telemarketing Bureaus and Other Contact Centers'),
	('561431','Private Mail Centers'),
	('561439','Other Business Service Centers (including Copy Shops)'),
	('561440','Collection Agencies'),
	('561450','Credit Bureaus'),
	('561491','Repossession Services'),
	('561492','Court Reporting and Stenotype Services'),
	('561499','All Other Business Support Services'),
	('561510','Travel Agencies'),
	('561520','Tour Operators'),
	('561591','Convention and Visitors Bureaus'),
	('561599','All Other Travel Arrangement and Reservation Services'),
	('561611','Investigation Services'),
	('561612','Security Guards and Patrol Services'),
	('561613','Armored Car Services'),
	('561621','Security Systems Services (except Locksmiths)'),
	('561622','Locksmiths'),
	('561710','Exterminating and Pest Control Services'),
	('561720','Janitorial Services'),
	('561730','Landscaping Services'),
	('561740','Carpet and Upholstery Cleaning Services'),
	('561790','Other Services to Buildings and Dwellings'),
	('561910','Packaging and Labeling Services'),
	('561920','Convention and Trade Show Organizers'),
	('561990','All Other Support Services'),
	('562111','Solid Waste Collection'),
	('562112','Hazardous Waste Collection'),
	('562119','Other Waste Collection'),
	('562211','Hazardous Waste Treatment and Disposal'),
	('562212','Solid Waste Landfill'),
	('562213','Solid Waste Combustors and Incinerators'),
	('562219','Other Nonhazardous Waste Treatment and Disposal'),
	('562910','Remediation Services'),
	('562920','Materials Recovery Facilities'),
	('562991','Septic Tank and Related Services'),
	('562998','All Other Miscellaneous Waste Management Services'),
	('611110','Elementary and Secondary Schools'),
	('611210','Junior Colleges'),
	('611310','Colleges, Universities, and Professional Schools'),
	('611410','Business and Secretarial Schools'),
	('611420','Computer Training'),
	('611430','Professional and Management Development Training'),
	('611511','Cosmetology and Barber Schools'),
	('611512','Flight Training'),
	('611513','Apprenticeship Training'),
	('611519','Other Technical and Trade Schools'),
	('611610','Fine Arts Schools'),
	('611620','Sports and Recreation Instruction'),
	('611630','Language Schools'),
	('611691','Exam Preparation and Tutoring'),
	('611692','Automobile Driving Schools'),
	('611699','All Other Miscellaneous Schools and Instruction'),
	('611710','Educational Support Services'),
	('621111','Offices of Physicians (except Mental Health Specialists)'),
	('621112','Offices of Physicians, Mental Health Specialists'),
	('621210','Offices of Dentists'),
	('621310','Offices of Chiropractors'),
	('621320','Offices of Optometrists'),
	('621330','Offices of Mental Health Practitioners (except Physicians)'),
	('621340','Offices of Physical, Occupational and Speech Therapists, and Audiologists'),
	('621391','Offices of Podiatrists'),
	('621399','Offices of All Other Miscellaneous Health Practitioners'),
	('621410','Family Planning Centers'),
	('621420','Outpatient Mental Health and Substance Abuse Centers'),
	('621491','HMO Medical Centers'),
	('621492','Kidney Dialysis Centers'),
	('621493','Freestanding Ambulatory Surgical and Emergency Centers'),
	('621498','All Other Outpatient Care Centers'),
	('621511','Medical Laboratories'),
	('621512','Diagnostic Imaging Centers'),
	('621610','Home Health Care Services'),
	('621910','Ambulance Services'),
	('621991','Blood and Organ Banks'),
	('621999','All Other Miscellaneous Ambulatory Health Care Services'),
	('622110','General Medical and Surgical Hospitals'),
	('622210','Psychiatric and Substance Abuse Hospitals'),
	('622310','Specialty (except Psychiatric and Substance Abuse) Hospitals'),
	('623110','Nursing Care Facilities (Skilled Nursing Facilities)'),
	('623210','Residential Intellectual and Developmental Disability Facilities'),
	('623220','Residential Mental Health and Substance Abuse Facilities'),
	('623311','Continuing Care Retirement Communities'),
	('623312','Assisted Living Facilities for the Elderly'),
	('623990','Other Residential Care Facilities'),
	('624110','Child and Youth Services'),
	('624120','Services for the Elderly and Persons with Disabilities'),
	('624190','Other Individual and Family Services'),
	('624210','Community Food Services'),
	('624221','Temporary Shelters'),
	('624229','Other Community Housing Services'),
	('624230','Emergency and Other Relief Services'),
	('624310','Vocational Rehabilitation Services'),
	('624410','Child Day Care Services'),
	('711110','Theater Companies and Dinner Theaters'),
	('711120','Dance Companies'),
	('711130','Musical Groups and Artists'),
	('711190','Other Performing Arts Companies'),
	('711211','Sports Teams and Clubs'),
	('711212','Racetracks'),
	('711219','Other Spectator Sports'),
	('711310','Promoters of Performing Arts, Sports, and Similar Events with Facilities'),
	('711320','Promoters of Performing Arts, Sports, and Similar Events without Facilities'),
	('711410','Agents and Managers for Artists, Athletes, Entertainers, and Other Public Figures'),
	('711510','Independent Artists, Writers, and Performers'),
	('712110','Museums'),
	('712120','Historical Sites'),
	('712130','Zoos and Botanical Gardens'),
	('712190','Nature Parks and Other Similar Institutions'),
	('713110','Amusement and Theme Parks'),
	('713120','Amusement Arcades'),
	('713210','Casinos (except Casino Hotels)'),
	('713290','Other Gambling Industries'),
	('713910','Golf Courses and Country Clubs'),
	('713920','Skiing Facilities'),
	('713930','Marinas'),
	('713940','Fitness and Recreational Sports Centers'),
	('713950','Bowling Centers'),
	('713990','All Other Amusement and Recreation Industries'),
	('721110','Hotels (except Casino Hotels) and Motels'),
	('721120','Casino Hotels'),
	('721191','Bed-and-Breakfast Inns'),
	('721199','All Other Traveler Accommodation'),
	('721211','RV (Recreational Vehicle) Parks and Campgrounds'),
	('721214','Recreational and Vacation Camps (except Campgrounds)'),
	('721310','Rooming and Boarding Houses'),
	('722310','Food Service Contractors'),
	('722320','Caterers'),
	('722330','Mobile Food Services'),
	('722410','Drinking Places (Alcoholic Beverages)'),
	('722511','Full-Service Restaurants'),
	('722513','Limited-Service Restaurants'),
	('722514','Cafeterias, Grill Buffets, and Buffets'),
	('722515','Snack and Nonalcoholic Beverage Bars'),
	('811111','General Automotive Repair'),
	('811112','Automotive Exhaust System Repair'),
	('811113','Automotive Transmission Repair'),
	('811118','Other Automotive Mechanical and Electrical Repair and Maintenance'),
	('811121','Automotive Body, Paint, and Interior Repair and Maintenance'),
	('811122','Automotive Glass Replacement Shops'),
	('811191','Automotive Oil Change and Lubrication Shops'),
	('811192','Car Washes'),
	('811198','All Other Automotive Repair and Maintenance'),
	('811211','Consumer Electronics Repair and Maintenance'),
	('811212','Computer and Office Machine Repair and Maintenance'),
	('811213','Communication Equipment Repair and Maintenance'),
	('811219','Other Electronic and Precision Equipment Repair and Maintenance')
GO

INSERT INTO Dropdown.NAICS 
	(NAICSCode,NAICSName) 
VALUES
	('811310','Commercial and Industrial Machinery and Equipment (except Automotive and Electronic) Repair and Maintenance'),
	('811411','Home and Garden Equipment Repair and Maintenance'),
	('811412','Appliance Repair and Maintenance'),
	('811420','Reupholstery and Furniture Repair'),
	('811430','Footwear and Leather Goods Repair'),
	('811490','Other Personal and Household Goods Repair and Maintenance'),
	('812111','Barber Shops'),
	('812112','Beauty Salons'),
	('812113','Nail Salons'),
	('812191','Diet and Weight Reducing Centers'),
	('812199','Other Personal Care Services'),
	('812210','Funeral Homes and Funeral Services'),
	('812220','Cemeteries and Crematories'),
	('812310','Coin-Operated Laundries and Drycleaners'),
	('812320','Drycleaning and Laundry Services (except Coin-Operated)'),
	('812331','Linen Supply'),
	('812332','Industrial Launderers'),
	('812910','Pet Care (except Veterinary) Services'),
	('812921','Photofinishing Laboratories (except One-Hour)'),
	('812922','One-Hour Photofinishing'),
	('812930','Parking Lots and Garages'),
	('812990','All Other Personal Services'),
	('813110','Religious Organizations'),
	('813211','Grantmaking Foundations'),
	('813212','Voluntary Health Organizations'),
	('813219','Other Grantmaking and Giving Services'),
	('813311','Human Rights Organizations'),
	('813312','Environment, Conservation and Wildlife Organizations'),
	('813319','Other Social Advocacy Organizations'),
	('813410','Civic and Social Organizations'),
	('813910','Business Associations'),
	('813920','Professional Organizations'),
	('813930','Labor Unions and Similar Labor Organizations'),
	('813940','Political Organizations'),
	('813990','Other Similar Organizations (except Business, Professional, Labor, and Political Organizations)'),
	('814110','Private Households'),
	('921110','Executive Offices'),
	('921120','Legislative Bodies'),
	('921130','Public Finance Activities'),
	('921140','Executive and Legislative Offices, Combined'),
	('921150','American Indian and Alaska Native Tribal Governments'),
	('921190','Other General Government Support'),
	('922110','Courts'),
	('922120','Police Protection'),
	('922130','Legal Counsel and Prosecution'),
	('922140','Correctional Institutions'),
	('922150','Parole Offices and Probation Offices'),
	('922160','Fire Protection'),
	('922190','Other Justice, Public Order, and Safety Activities'),
	('923110','Administration of Education Programs'),
	('923120','Administration of Public Health Programs'),
	('923130','Administration of Human Resource Programs (except Education, Public Health, and Veterans'' Affairs Programs)'),
	('923140','Administration of Veterans'' Affairs'),
	('924110','Administration of Air and Water Resource and Solid Waste Management Programs'),
	('924120','Administration of Conservation Programs'),
	('925110','Administration of Housing Programs'),
	('925120','Administration of Urban Planning and Community and Rural Development'),
	('926110','Administration of General Economic Programs'),
	('926120','Regulation and Administration of Transportation Programs'),
	('926130','Regulation and Administration of Communications, Electric, Gas, and Other Utilities'),
	('926140','Regulation of Agricultural Marketing and Commodities'),
	('926150','Regulation, Licensing, and Inspection of Miscellaneous Commercial Sectors'),
	('927110','Space Research and Technology'),
	('928110','National Security'),
	('928120','International Affairs')
GO
--End table Dropdown.NAICS

--Begin table Dropdown.Organization
SET IDENTITY_INSERT Dropdown.Organization ON
INSERT INTO Dropdown.Organization (OrganizationID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Organization OFF
GO

--Begin OpDivs
INSERT INTO Dropdown.Organization 
	(OrganizationName)
VALUES
	('ACF'),
	('ACL'),
	('AHRQ'),
	('ASA'),
	('ASFR'),
	('ASPA'),
	('ASPE'),
	('ASPR'),
	('ATSDR'),
	('CDC'),
	('CMS'),
	('DAB'),
	('FDA'),
	('HRSA'),
	('IEA'),
	('IHS'),
	('NIH'),
	('OASH'),
	('OASL'),
	('OCR'),
	('OGC'),
	('OMHA'),
	('OS'),
	('PSC'),
	('SAMHSA'),
	('Non-HHS')	
GO

UPDATE Dropdown.Organization 
SET DisplayOrder = 99
WHERE OrganizationName = 'Non-HHS'
GO

--Begin ACF
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'ACF'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Administration for Native Americans (ANA)', @ParentOrganizationID),
	('Administration on Children, Youth and Families (ACYF)', @ParentOrganizationID),
	('Children''s Bureau (CB)', @ParentOrganizationID),
	('Early Childhood Development Interagency Coordination (ECDIC)', @ParentOrganizationID),
	('Family and Youth Services Bureau (FYSB)', @ParentOrganizationID),
	('Office of Administration (OA)', @ParentOrganizationID),
	('Office of Child Care (OCC)', @ParentOrganizationID),
	('Office of Child Support Enforcement (OCSE)', @ParentOrganizationID),
	('Office of Community Services (OCS)', @ParentOrganizationID),
	('Office of Family Assistance (OFA)', @ParentOrganizationID),
	('Office of Head Start (OHS)', @ParentOrganizationID),
	('Office of Human Services Emergency Preparedness and Response (OHSEPR)', @ParentOrganizationID),
	('Office of Legislative Affairs and Budget (OLAB)', @ParentOrganizationID),
	('Office of Planning, Research & Evaluation (OPRE)', @ParentOrganizationID),
	('Office of Public Affairs (OPA)', @ParentOrganizationID),
	('Office of Refugee Resettlement (ORR)', @ParentOrganizationID),
	('Office of Regional Operations (ORO)', @ParentOrganizationID),
	('Public Assistance Reporting Information System (PARIS)', @ParentOrganizationID)
GO

--Begin ACL
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'ACL'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Administration on Aging', @ParentOrganizationID),
	('Administration on Intellectual and Developmental Disabilities', @ParentOrganizationID),
	('Center for Disability and Aging Policy', @ParentOrganizationID),
	('Center for Management and Budget', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Administration on Aging'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Office of American Indian, Alaskan Native and Native Hawaiian Programs', @ParentOrganizationID),
	('Office of Elder Rights Protection', @ParentOrganizationID),
	('Office of Long-Term Care Ombudsman Programs', @ParentOrganizationID),
	('Office of Nutrition and Health Promotion Programs', @ParentOrganizationID),
	('Office of Supportive and Caregiver Services', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Administration on Intellectual and Developmental Disabilities'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Office for the President''s Committee for People with Intellectual Disabilities', @ParentOrganizationID),
	('Office of Innovation', @ParentOrganizationID),
	('Office of Program Support', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Center for Disability and Aging Policy'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Office of Integrated Programs', @ParentOrganizationID),
	('Office of Performance and Evaluation', @ParentOrganizationID),
	('Office of Policy Analysis and Development', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Center for Management and Budget'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Office of Administration and Personnel', @ParentOrganizationID),
	('Office of Budget and Finance', @ParentOrganizationID),
	('Office of Grants Management', @ParentOrganizationID),
	('Office of Information Resources Management', @ParentOrganizationID)
GO

--Begin AHRQ
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'AHRQ'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center for Delivery, Organization, and Markets', @ParentOrganizationID),
	('Center for Financing, Access and Cost Trends', @ParentOrganizationID),
	('Center for Outcomes and Evidence', @ParentOrganizationID),
	('Center for Primary Care, Prevention, and Clinical Partnerships', @ParentOrganizationID),
	('Center for Quality Improvement and Patient Safety', @ParentOrganizationID),
	('Office of Communications and Knowledge Transfer', @ParentOrganizationID),
	('Office of Extramural Research, Education and Priority Populations', @ParentOrganizationID),
	('Office of Performance Accountability, Resources, and Technology', @ParentOrganizationID)
GO

--Begin ASA
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'ASA'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('EEO Compliance & Operations', @ParentOrganizationID),
	('Office of Business Management and Transformation', @ParentOrganizationID),
	('Office of the Chief Information Officer', @ParentOrganizationID),
	('Office of Human Resources', @ParentOrganizationID),
	('Office of Security and Strategic Information', @ParentOrganizationID),
	('Program Support Center', @ParentOrganizationID)
GO

--Begin ASFR
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'ASFR'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Immediate Office', @ParentOrganizationID),
	('Office of Budget', @ParentOrganizationID),
	('Office of Finance', @ParentOrganizationID),
	('Office of Grants and Acquisition Policy and Accountability', @ParentOrganizationID)
GO

--Begin ATSDR
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'ATSDR'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Division of Community Health Investigations', @ParentOrganizationID),
	('Division of Emergency & Environmental Health Services', @ParentOrganizationID),
	('Division of Environmental Hazards & Health Effects', @ParentOrganizationID),
	('Division of Laboratory Sciences', @ParentOrganizationID),
	('Division of Toxicology and Human Health Sciences', @ParentOrganizationID),
	('Office of Communication', @ParentOrganizationID),
	('Office of Environmental Health Emergencies', @ParentOrganizationID),
	('Office of Financial, Administrative, & Information Services', @ParentOrganizationID),
	('Office of Noncommunicable Disease, Injury and Environmental Health', @ParentOrganizationID),
	('Office of Policy, Planning, & Evaluation', @ParentOrganizationID),
	('Office of Science', @ParentOrganizationID)
GO

--Begin CDC
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'CDC'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center for Global Health', @ParentOrganizationID),
	('National Institute for Occupational Safety and Health', @ParentOrganizationID),
	('Office for State, Tribal, Local and Territorial Support', @ParentOrganizationID),
	('Office of Infectious Diseases', @ParentOrganizationID),
	('Office of Noncommunicable Diseases, Injury and Environmental Health', @ParentOrganizationID),
	('Office of Public Health Preparedness and Response', @ParentOrganizationID),
	('Office of Public Health Scientific Services', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Office of Infectious Diseases'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('National Center for Emerging and Zoonotic Infectious Diseases', @ParentOrganizationID),
	('National Center for HIV/AIDS, Viral Hepatitis, STD and TB Prevention', @ParentOrganizationID),
	('National Center for Immunization and Respiratory Diseases', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Office of Noncommunicable Diseases, Injury and Environmental Health'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('National Center for Chronic Disease Prevention and Health Promotion', @ParentOrganizationID),
	('National Center for Environmental Health/Agency for Toxic Substances and Disease Registry', @ParentOrganizationID),
	('National Center for Injury Prevention and Control', @ParentOrganizationID),
	('National Center on Birth Defects and Developmental Disabilities', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Office of Public Health Scientific Services'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center for Surveillance, Epidemiology, and Laboratory Services', @ParentOrganizationID),
	('National Center for Health Statistics', @ParentOrganizationID)
GO

--Begin CMS
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'CMS'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center For Clinical Standards And Quality', @ParentOrganizationID),
	('Center For Consumer Information And Insurance Oversight', @ParentOrganizationID),
	('Center For Medicaid And Chip Services', @ParentOrganizationID),
	('Center For Medicare', @ParentOrganizationID),
	('Center For Medicare And Medicaid Innovation', @ParentOrganizationID),
	('Center For Program Integrity', @ParentOrganizationID),
	('Consortium For Financial Management & Fee-For-Service Operations', @ParentOrganizationID),
	('Consortium For Medicaid And Children’s Health Operations', @ParentOrganizationID),
	('Consortium For Medicare Health Plan Operations', @ParentOrganizationID),
	('Consortium For Quality Improvement And Survey & Certification Operations', @ParentOrganizationID),
	('Federal Coordinated Health Care Office', @ParentOrganizationID),
	('Office of Acquisition And Grants Management', @ParentOrganizationID),
	('Office of Communications', @ParentOrganizationID),
	('Office of Equal Opportunity And Civil Rights', @ParentOrganizationID),
	('Office of Financial Management', @ParentOrganizationID),
	('Office of Information Services', @ParentOrganizationID),
	('Office of Legislation', @ParentOrganizationID),
	('Office of Minority Health', @ParentOrganizationID),
	('Office of Operations Management', @ParentOrganizationID),
	('Office of Strategic Operations And Regulatory Affairs', @ParentOrganizationID),
	('Office of the Actuary', @ParentOrganizationID),
	('Offices of Enterprise Management', @ParentOrganizationID),
	('Offices of Hearings And Inquiries', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Offices of Enterprise Management'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Office of E-Health Standards And Services', @ParentOrganizationID),
	('Office of Enterprise Business', @ParentOrganizationID),
	('Office of Enterprise Strategy & Performance', @ParentOrganizationID),
	('Office of Information Products And Data Analytics', @ParentOrganizationID)
GO

--Begin FDA
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'FDA'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center for Biologics Evaluation and Research', @ParentOrganizationID),
	('Center for Devices and Radiological Health', @ParentOrganizationID), 
	('Center for Drug Evaluation and Research', @ParentOrganizationID), 
	('Center for Food and Safety and Applied Nutrition', @ParentOrganizationID), 
	('Center for Tobacco Products', @ParentOrganizationID), 
	('Center for Veterinary Medicine', @ParentOrganizationID), 
	('National Center for Toxicological Research', @ParentOrganizationID), 
	('Office of Foods and Veterinary Medicine Organzation', @ParentOrganizationID), 
	('Office of Global Regulatory Operations and Policy', @ParentOrganizationID), 
	('Office of Medical Products and Tobacco', @ParentOrganizationID), 
	('Office of Operations', @ParentOrganizationID), 
	('Office of Regulatory Affairs', @ParentOrganizationID)
GO

--Begin HRSA
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'HRSA'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Bureau of Clinician Recruitment and Services', @ParentOrganizationID), 
	('Bureau of Health Professions', @ParentOrganizationID), 
	('Bureau of Primary Health Care', @ParentOrganizationID), 
	('Healthcare Systems Bureau', @ParentOrganizationID), 
	('HIV/AIDS Bureau', @ParentOrganizationID), 
	('Maternal and Child Health Bureau', @ParentOrganizationID), 
	('Office of Communications', @ParentOrganizationID), 
	('Office of Equal Opportunity, Civil Rights, and Diversity Management', @ParentOrganizationID), 
	('Office of Federal Assistance Management', @ParentOrganizationID), 
	('Office of Health Equity', @ParentOrganizationID), 
	('Office of Legislation', @ParentOrganizationID), 
	('Office of Operations', @ParentOrganizationID), 
	('Office of Planning, Analysis and Evaluation', @ParentOrganizationID), 
	('Office of Regional Operations', @ParentOrganizationID), 
	('Office of Rural Health Policy', @ParentOrganizationID), 
	('Office of Women''s Health', @ParentOrganizationID)
GO

--Begin IHS
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'IHS'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Aberdeen Area', @ParentOrganizationID), 
	('Alaska Area', @ParentOrganizationID), 
	('Albuquerque Area', @ParentOrganizationID), 
	('Bemidji Area', @ParentOrganizationID), 
	('Billings Area', @ParentOrganizationID), 
	('California Area', @ParentOrganizationID), 
	('Dallas Area', @ParentOrganizationID), 
	('Nashville Area', @ParentOrganizationID), 
	('Navajo Area', @ParentOrganizationID), 
	('Office of Clinical and Preventive Services (OCPS)', @ParentOrganizationID), 
	('Office of Environmental Health and Engineering (OEHE)', @ParentOrganizationID), 
	('Office of Finance and Accounting (OFA)', @ParentOrganizationID), 
	('Office of Information Technology (OIT)', @ParentOrganizationID), 
	('Office of Management Services (OMS)', @ParentOrganizationID), 
	('Office of Public Health Support (OPHS)', @ParentOrganizationID), 
	('Office of Resource Access and Partnerships (ORAP)', @ParentOrganizationID), 
	('Office of the Director', @ParentOrganizationID), 
	('Oklahoma City Area', @ParentOrganizationID), 
	('Phoenix Area', @ParentOrganizationID), 
	('Portland Area', @ParentOrganizationID), 
	('Seattle Area', @ParentOrganizationID), 
	('Tucson Area', @ParentOrganizationID)
GO

DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'Office of the Director'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Congressional and Legislative Affairs Office', @ParentOrganizationID), 
	('Diversity Management and Equal Opportunity Office', @ParentOrganizationID), 
	('Executive Secretariat Office', @ParentOrganizationID), 
	('Office of Direct Service and Contracting Tribes (ODSCT)', @ParentOrganizationID), 
	('Office of Tribal Self Governance (OTSG)', @ParentOrganizationID), 
	('Office of Urban Indian Health Programs (OUIHP)', @ParentOrganizationID), 
	('Public Affairs Office', @ParentOrganizationID)
GO

--Begin NIH
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'NIH'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('CC', @ParentOrganizationID), 
	('CIT', @ParentOrganizationID), 
	('CSR', @ParentOrganizationID), 
	('FIC', @ParentOrganizationID), 
	('NCATS', @ParentOrganizationID), 
	('NCCAM', @ParentOrganizationID), 
	('NCI', @ParentOrganizationID), 
	('NEI', @ParentOrganizationID), 
	('NHGRI', @ParentOrganizationID), 
	('NHLBI', @ParentOrganizationID), 
	('NIA', @ParentOrganizationID), 
	('NIAAA', @ParentOrganizationID), 
	('NIAID', @ParentOrganizationID), 
	('NIAMS', @ParentOrganizationID), 
	('NIBIB', @ParentOrganizationID), 
	('NICHD', @ParentOrganizationID), 
	('NIDA', @ParentOrganizationID), 
	('NIDCD', @ParentOrganizationID), 
	('NIDCR', @ParentOrganizationID), 
	('NIDDK', @ParentOrganizationID), 
	('NIEHS', @ParentOrganizationID), 
	('NIGMS', @ParentOrganizationID), 
	('NIMH', @ParentOrganizationID), 
	('NIMHD', @ParentOrganizationID), 
	('NINDS', @ParentOrganizationID), 
	('NINR', @ParentOrganizationID), 
	('NLM', @ParentOrganizationID), 
	('OD', @ParentOrganizationID)
GO

--Begin OCR
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'OCR'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Atlanta Regional Office', @ParentOrganizationID), 
	('Boston Regional Office', @ParentOrganizationID), 
	('Chicago Regional Office', @ParentOrganizationID), 
	('Dallas Regional Office', @ParentOrganizationID), 
	('Denver Regional Office', @ParentOrganizationID), 
	('Kansas City Regional Office', @ParentOrganizationID), 
	('New York Regional Office', @ParentOrganizationID), 
	('Philadelphia Regional Office', @ParentOrganizationID), 
	('San Francisco Regional Office', @ParentOrganizationID),
	('Seattle Regional Office', @ParentOrganizationID)
GO

--Begin PSC
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'PSC'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Administrative Operations Service (AOS)', @ParentOrganizationID), 
	('Facilities and Logistics Service (FLS)', @ParentOrganizationID),  
	('Federal Occupational Health (FOH)', @ParentOrganizationID), 
	('Financial Management Service (FMS)', @ParentOrganizationID), 
	('Strategic Acquisition Service (SAS)', @ParentOrganizationID)
GO

--Begin SAMHSA
DECLARE @ParentOrganizationID INT

SELECT @ParentOrganizationID = O.OrganizationID
FROM Dropdown.Organization O
WHERE O.OrganizationName = 'SAMHSA'

INSERT INTO Dropdown.Organization 
	(OrganizationName,ParentOrganizationID)
VALUES
	('Center for Behavioral Health Statistics and Quality', @ParentOrganizationID), 
	('Center for Mental Health Services', @ParentOrganizationID), 
	('Center for Substance Abuse Prevention', @ParentOrganizationID), 
	('Center for Substance Abuse Treatment', @ParentOrganizationID), 
	('Office of Behavioral Health Equity', @ParentOrganizationID), 
	('Office of Communications', @ParentOrganizationID), 
	('Office of Financial Resources', @ParentOrganizationID), 
	('Office of Management, Technology, and Operations', @ParentOrganizationID), 
	('Office of Policy, Planning, and Innovation', @ParentOrganizationID), 
	('Office of the Administrator', @ParentOrganizationID)
GO
--End table Dropdown.Organization

DECLARE @Table TABLE
	(
	OrganizationID INT DEFAULT 0,
	FirstName VARCHAR(50) NULL,
	LastName VARCHAR(50) NULL,
	Phone VARCHAR(50) NULL,
	Title VARCHAR(50) NULL,
	EmailAddress VARCHAR(320) NULL
	)
	
--Begin table @Table
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'aaron.blackshire@cms.hhs.gov','Aaron','Blackshire' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'abby.smith@cms.hhs.gov','Abby','Smith' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'adam.forgione@cms.hhs.gov','Adam','Forgione' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alan.fredericks@cms.hhs.gov','Alan','Fredericks' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alec.blakeley@hhs.gov','Alec','Blakely' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alex.jarema@cms.hhs.gov','Alex','Jarema' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alexandra.hohensee@hhs.gov','Alexandra','Hohensee' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'algirdas.veliuona@cms.hhs.gov','Al','Veliuona' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alice.calabro@cms.hhs.gov','Alice','Calabro' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'alice.mcgruder@cms.hhs.gov','Alice','McGruder' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'allisan.hafner@cms.hhs.gov','Allisan','Hafner' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'amanda.merricks@hhs.gov','Amanda','Merricks' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Boston Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'amy.brenner@hhs.gov','Amy','Brenner' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'amy.duckworth@cms.hhs.gov','Amy','Duckworth' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'amy.haseltine@hhs.gov','Amy','Haseltine' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'andrea.oliver@hhs.gov','Andrea','Oliver' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Denver Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'andrea.ormiston@cms.hhs.gov','Andrea','Ormiston' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'andrew.anuszewski@cms.hhs.gov','Andrew','Anuszewski' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'andrew.crochunis@cms.hhs.gov','Andrew','Crochunis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'andrew.mummert@cms.hhs.gov','Andy','Mummert' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'angela.billups@hhs.gov','Angela','Billups' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'angela.britton@cms.hhs.gov','Angela','Britton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'angela.johnson@hhs.gov','Angela','Johnson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'angela.reviere@cms.hhs.gov','Angela','Reviere' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'anginna.sims@cms.hhs.gov','Anginna','Sims' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'anna.castaldo@hhs.gov','Anna','Castaldo' FROM Dropdown.Organization O WHERE O.OrganizationName = 'New York Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'anne.morazzanoteeter@hhs.gov','Anne','Morazzano-Teeter' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'antoinette.hazelwood@cms.hhs.gov','Antoinette','Hazelwood' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'arlene.franklin@hhs.gov','Arlene','Franklin' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'asha.abraham@fda.hhs.gov','Asha','Abraham' FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'audrey.mirsky-ashby@hhs.gov','Audry','Mirsky-ashby' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'barbara.bigelow@hhs.gov','Barbara','Bigelow' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'barbara.greene@hhs.gov','Barbara','Greene' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'barbara.holland@hhs.gov','Barbara','Holland' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Philadelphia Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'barbara.miller@fda.hhs.gov','Barbara','Miller' FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'barry.mikesell@cms.hhs.gov','Barry','Mikesell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'benjamin.simcock@cms.hhs.gov','Ben','Simcock' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'benjamin.stidham@cms.hhs.gov','Ben','Stidham' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'beth.citeroni@cms.hhs.gov','Beth','Citeroni' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'beth.marlow@cms.hhs.gov','Beth','Marlow' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'beth.waskiewicz@cms.hhs.gov','Beth','Waskiewicz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'beverly.pannell@hhs.gov','Beverly','Pannell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'bill.hall@hhs.gov','Bill','Hall' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'blenda.perez@samhsa.hhs.gov','Blenda','Perez' FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brenda.clark@cms.hhs.gov','Brenda','Clark' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brenda.thomas@cms.hhs.gov','Brenda','Thomas' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brendan.doherty@cms.hhs.gov','Brendan','Doherty' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brian.hebbel@cms.hhs.gov','Brian','Hebbel' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brian.humes@cms.hhs.gov','Brian','Humes' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'brian.sissom@ees.hhs.gov','Brian','Sissom' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'bridget.rineker@cms.hhs.gov','Bridget','Rineker' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'bryan.maynard@hhs.gov','Bryan','Maynard' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'bryce.golwalla@cms.hhs.gov','Bryce','Golwalla' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'buddy.frye@hhs.gov','Eugene (Buddy)','Frye' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'burton.humphrey@ihs.gov','Burt','Humphrey' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Alaska Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'candice.savoy@cms.hhs.gov','Candice','Savoy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'caol@od.nih.gov','Lahn','Cao' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'cara.berman@hhs.gov','Cara','Berman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'carol.brown@hhs.gov','Carol','Brown' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'carol.diaz@ihs.gov','Carol','Diaz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Aberdeen Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'carol.sevel@cms.hhs.gov','Carol','Sevel' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'carolyn.keeseman@nih.gov','Carolyn','Keeseman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'cassandra.worsham-carey@cms.hhs.gov','Cassandra','Worsham Carey' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'catherine.teti@hhs.gov','Catherine','Teti' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'cathryn.kim@cms.hhs.gov','Cathryn','Kim' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'celeste.davis@hhs.gov','Celeste','Davis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Chicago Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'charlene.barnes@cms.hhs.gov','Charlene','Barnes' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'charles.brewer@cms.hhs.gov','Charles','Brewer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'charles.littleton@cms.hhs.gov','Chuck','Littleton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christina.eberhart@cms.hhs.gov','Christina','Eberhart' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christina.heide@hhs.gov','Christina','Heide' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christina.heller@cms.hhs.gov','Christina','Heller' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christina.honey@cms.hhs.gov','Christina','Honey' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christine.meade@cms.hhs.gov','Christine','Meade' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christine.nagengast@cms.hhs.gov','Christine','Nagengast' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.clark@cms.hhs.gov','Christopher','Clark' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.crouch@hhs.gov','Christopher','Crouch' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Non-HHS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.hagepanos@cms.hhs.gov','Chris','Hagepanos' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.randolph@hhs.gov','Christopher','Randolph' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.tanks@cms.hhs.gov','Christopher','Tanks' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'christopher.zelenik@hhs.gov','Christopher','Zeleznik' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Finance'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'cindy.cento@hhs.gov','Cindy','Cento' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'colet@od.nih.gov','Todd','Cole' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'constance.tobias@hhs.gov','Constance','Tobias' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'corey.lloyd@cms.hhs.gov','Corey','Lloyd' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'courtney.carter@nih.gov','Courtney','Carter' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'courtney.garnes@cms.hhs.gov','Courtney','Garnes' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'craig.dash@cms.hhs.gov','Craig','Dash' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'crystal.mcwilliams@cms.hhs.gov','Crystal','McWilliams' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dan.berger@acl.hhs.gov','Dan','Berger' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'danie.ridgway@cms.hhs.gov','Danie','Ridgway' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'daniel.levenson@cms.hhs.gov','Daniel','Levenson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'danielle.tapp@cms.hhs.gov','Danielle','Tapp' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'darlene.walls@nih.gov','Darlene','Walls' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.fitton@cms.hhs.gov','David','Fitton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.flynn@hhs.gov','David','Flynn' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Program Support Center'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.flynn@psc.hhs.gov','David','Flynn' FROM Dropdown.Organization O WHERE O.OrganizationName = 'PSC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.hansen@cms.hhs.gov','David','Hansen' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.hauge@hhs.gov','David','Hauge' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Security and Strategic Information'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.trejo2@hrsa.hhs.gov   ','David','Trejo' FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'david.trejo2@hrsa.hhs.gov','David','Trejo Jr.' FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dawn.graham@cms.hhs.gov','Dawn','Graham' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dawn.sekayumptewa@ihs.gov','Dawn','Sekayumptewa' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Tucson Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dawn.wilkins@cms.hhs.gov','Dawn','Wilkins' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dce1@cdc.gov','David','Elswick' FROM Dropdown.Organization O WHERE O.OrganizationName = 'National Center for Emerging and Zoonotic Infectious Diseases'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'debbie.kramer@hhs.gov','Debbie','Kramer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'deborah.lester@cms.hhs.gov','Debbie','Lester' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'deborah.osborne@hhs.gov','Deborah','Osborne' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'debra.hoffman@cms.hhs.gov','Debra','Hoffman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'debra.stidham@cms.hhs.gov','Debbie','Stidham' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'denizejoice.hammond@cms.hhs.gov','Denize','Hammond' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dennis.gonzalez@hhs.gov','Dennis','Gonzalez' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'diane.bogusz@hhs.gov','Diane','Bogusz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Business Management and Transformation'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'diane.rogler@hhs.gov','Diane','Rogler' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dillonr@od.nih.gov','Raymond','Dillon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'dionet.perry@hhs.gov','Dionte','Perry' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'djere.memene@hhs.gov','Djere','Memene' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'djeri.memene@hhs.gov','Djeri','Memene' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'donald.bozimski@cms.hhs.gov','Donald','Bozimski' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'donald.knode@cms.hhs.gov','Don','Knode' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'doyle.forrestal@hhs.gov','Doyle','Forrestal' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'eddie.woodard@cms.hhs.gov','Eddie','Woodard' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'edward.farmer@cms.hhs.gov','Edward','Farmer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'edward.hughes@cms.hhs.gov','Ed','Hughes' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'eisha.banks@cms.hhs.gov','Eisha','Banks' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'elizabeth.jeffries@hhs.gov','Elizabeth','Jeffries' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ella.curtis@cms.hhs.gov','Ella','Curtis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'elliott.sloan@cms.hhs.gov','Elliott','Sloan' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'emmanuel.djokou@acf.hhs.gov','Tera','Allen-Djokou' FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'erin.crockett@cms.hhs.gov','Erin','Crockett' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'erin.sparwasser@cms.hhs.gov','Erin','Sparwasser' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'erin.willford-skipworth@cms.hhs.gov','Erin','Willford-Skipworth' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'evan.seltzer@cms.hhs.gov','Evan','Seltzer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'evelyn.dixon@cms.hhs.gov','Evelyn','Dixon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'evelyn.dixon1@cms.hhs.gov','Evelyn','Dixon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'famane.brown@cms.hhs.gov','Famane','Brown' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'fpp0@cdc.gov','Ted','Pestorious' FROM Dropdown.Organization O WHERE O.OrganizationName = 'National Center for Emerging and Zoonotic Infectious Diseases'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frank.brindisi.osees.hhs.gov','Frank','Brindisi' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frank.brindisi@hhs.gov','Frank','Brindisi' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frank.campbell@hhs.gov','Frank','Campbell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frank.dayish@ihs.gov','Frank','Dayish' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Navajo Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frankie.green@hhs.gov','Frankie','Green' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'frederick.filberg@cms.hhs.gov','Frederick','Filberg' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'gabriel.nah@cms.hhs.gov','Gabriel','Nah' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'geoffrey.ntosi@cms.hhs.gov','Geoffrey','Ntosi' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'georgine.delacruz@hhs.gov','Georgine','Delacruz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'gillian.goldthwaite@cms.hhs.gov','Gillian','Goldthwaite' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'glynis.fisher@nih.gov','Glynis','Fisher' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'greg.gesterling@cms.hhs.gov','Greg','Gesterling' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'gregory.stark@cms.hhs.gov','Gregory','Stark' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'guimondmi@mail.nih.gov','Mike','Guimond' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'gvr6@cdc.gov','Shanda','Blue' FROM Dropdown.Organization O WHERE O.OrganizationName = 'National Center for Emerging and Zoonotic Infectious Diseases'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'hanock.john@hrsa.hhs.gov','John','Hancock' FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'harold.henderson@hhs.gov','Harold','Henderson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'heather.robertson@cms.hhs.gov','Heather','Robertson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'helen.white@hhs.gov','Helen','White' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'herman.harris@cms.hhs.gov','Herman','Harris' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'hollidayg@odepsml.od.nih.gov','Greg','Holliday' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'holly.blake@cms.hhs.gov','Holly','Blake' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'holly.stephens@cms.hhs.gov','Holly','Stephens' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'holly.stephens1@cms.hhs.gov','Holly','Stephens' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'iliana.peters@hhs.gov','Iliana','Peters' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'irina.perl@cms.hhs.gov','Irina','Perl' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'iris.grady@cms.hhs.gov','Iris','Grady' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ivette.otero@hhs.gov','Ivette','Otero' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ivey.belton@hhs.gov','Ivey','Belton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Atlanta Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jacob.reinert@cms.hhs.gov','Jacob','Reinert' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'james.vanderdonck@cms.hhs.gov','Todd','VanderDonck' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jamie.atwood@cms.hhs.gov','Jamie','Atwood' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'janice.rosiak@hhs.gov','Janice','Rosiak' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jarrett.brown@cms.hhs.gov','Jarrett','Brown' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jason.vollmer@cms.hhs.gov','Jason','Vollmer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'javina.wilkinson@cms.hhs.gov','Javina','Wilkinson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jeanette.wadley-mitchell@cms.hhs.gov','Jeanette','Wadley-Mitchell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jeannine.bohlen@cms.hhs.gov','Jeannine','Bohlen' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jeff.davis@hhs.gov','Jeff','Davis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jennifer.hennessy@cms.hhs.gov','Jennifer','Hennessy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jeremy.steel@cms.hhs.gov','Jeremy','Steel' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jerry.black@ihs.gov','Jerry','Black' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Billings Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jessica.sanders@cms.hhs.gov','Jessica','Sanders' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jino.matthews@hhs.gov','Jino','Matthews' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'john.cruse@cms.hhs.gov','John','Cruse' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'john.webster@cms.hhs.gov','John','Webster' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'johnny.vo@cms.hhs.gov','Johnny','Vo' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jonathan.chattler@cms.hhs.gov','Jonathan','Chattler' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jonessi@mail.nih.gov','Silver','Jones' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'jorge.lozano@hhs.gov','Jorge','Lozano' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Dallas Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'joseph.marshall@nih.gov','Joe','Marshall' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'joseph.pressley@cms.hhs.gov','Joseph','Pressley' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'joyce.irvin@hhs.gov','Joyce','Irvin' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'juan.wooten@hhs.gov','Juan','Wooten' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'juanita.wilson@cms.hhs.gov','Juanita','Wilson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'julia.howard@cms.hhs.gov','Julia','Howard' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'julia.lothrop@hhs.gov','Julia','Lothrop' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'justin.menefee@cms.hhs.gov','Justin','Menefee' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kaminskis@od.nih.gov','Sue','Kaminski' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'karen.johnson1@cms.hhs.gov','Karen','Johnson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'karen.robinson@hhs.gov','Karen','Robinson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'karmen.lyles@cms.hhs.gov','Karmen','Lyles' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kathleen.chuhran@cms.hhs.gov','Kathleen','Chuhran' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kathryn.salmon@hhs.gov','Kathryn','Salmon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kathy.markman@cms.hhs.gov','Kathy','Markman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kelley.williams-vollmer@cms.hhs.gov','Kelley','Williams-Vollmer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kelly.housein@cms.hhs.gov','Kelly','Housein' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kenneth.shapiro@hhs.gov','Kenneth','Shapiro' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kenneth.truesdale@ihs.gov','Ken','Truesdale' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Dallas Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kevin.bumatay@hhs.gov','Kevin','Bumatay' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kevin.cramer@hhs.gov','Kevin','Cramer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kevin.greene@hrsa.hhs.gov','Kevin','Greene' FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kim.devoto@hhs.gov','Kim','Devoto' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kimberly.tatum@cms.hhs.gov','Kim','Tatum' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kiran.grooms@hhs.gov','Kiran','Grooms' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Dallas Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kristen.lawrence@cms.hhs.gov','Kristen','Lawrence' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kurt.temple@hhs.gov','Kurt','Temple' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'kyle.stearns@hhs.gov','Kyle','Stearns' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'latoya.young@hhs.gov','Latoya','Young' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Finance'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'latrina.baker@cms.hhs.gov','Latrina','Baker' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lauren.teal@cms.hhs.gov','Lauren','Teal' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'laurie.lloyd@cms.hhs.gov','Laurie','Lloyd' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lavonnia.persaud@hhs.gov','LaVonnia','Persaud' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'leisa.bodway@cms.hhs.gov','Leisa','Bodway' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'leo.murphy@hhs.gov','Leo','Murphy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Denver Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lesley.wreden@hhs.gov','Lesley','Wreden' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'letrina.holley@acl.hhs.gov','LeTrina','Holley' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'licinda.peters@cms.hhs.gov','Licinda','Peters' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.colon@hhs.gov','Linda','Colon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'New York Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.connor@hhs.gov','Linda','Connor' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Seattle Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.conte@hhs.gov','Linda','Conte' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.gmeiner@cms.hhs.gov','Linda','Gmeiner' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.hook@cms.hhs.gov','Linda','Hook' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.johnson@hhs.gov','Linda','Johnson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'linda.waters@hhs.gov','Linda','Waters' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'louis.anderson@cms.hhs.gov','Louis','Anderson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'louise.amburgey1@cms.hhs.gov','Louise','Amburgey' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'louise.hsu@cms.hhs.gov','Louise','Hsu' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lucille.lee@cms.hhs.gov','Lucille','Lee' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lyandra.emmanuel@cms.hhs.gov','Lyandra','Emmanuel' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'lynn.tantardini@samhsa.hhs.gov','Lynn','Tantardini' FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'maiatu.davis@cms.hhs.gov','Maiatu','Davis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'malissa.shin@cms.hhs.gov','Malissa','Shin' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'maranelli.corpuz@hhs.gov','Maranelli','Corpuz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'margie.yanchuk@hhs.gov','Margie','Yanchuk' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Finance'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'marjorie.carter@hhs.gov','Marjorie','Carter' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Philadelphia Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mark.blecker@hhs.gov','Mark','Blecker' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mark.smolenski@cms.hhs.gov','Mark','Smolenski' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mark.weber@hhs.gov','Mark','Weber' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mark.werder@cms.hhs.gov','Mark','Werder' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'martha.young@ihs.gov','Martha','Young' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Portland Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mary.greene@cms.hhs.gov','Mary Beth','Greene' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mary.jones@cms.hhs.gov','Mary','Jones' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mary.mcgill@hhs.gov','Mary','McGill' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mary.young@hhs.gov','Mary','Young' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'marybeth.foley@hhs.gov','MaryBeth','Foley' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'matthew.clark@cms.hhs.gov','Matt','Clark' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'matthew.kelly@hhs.gov','Matthew','Kelly' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'matthew.levenson@cms.hhs.gov','Matt','Levenson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'matthew.waskiewicz@cms.hhs.gov','Matthew','Waskiewicz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'maureen.turner-cooper@nih.gov','Maureen','Turner-Cooper' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'meghan.critzman@cms.hhs.gov','Megan','Critzman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'melissa.middleton@acl.hhs.gov','Melissa','Middleton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.connors@cms.hhs.gov','Michael','Connors' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.crow@cms.hhs.gov','Michael','Crow' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.leoz@hhs.gov','Michael','Leoz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'San Francisco Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.marquis@hhs,gov','Michael','Marquis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.marquis@hhs.gov','Michael','Marquis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.milanese@cms.hhs.gov','Michael','Milanese' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.shirk@cms.hhs.gov','Michael','Shirk' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michael.wilker@hhs.gov','Michael','Wilker' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michele.lanasa@cms.hhs.gov','Michele','Lanasa' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michelle.feagins@cms.hhs.gov','Michelle','Feagins' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michelle.grifka@hhs.gov','Michelle','Grifka' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'michon.kretschmaier@hhs.gov','Michon','Kretschmaier' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mike.etzinger@samhsa.hhs.gov','Mike','Etzinger' FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'miles.daniels@hhs.gov','Daniels','Miles J.' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'mohammed.islam@cms.hhs.gov','Mohammed','Islam' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nancy.gunderson@hhs.gov','Nancy','Gunderson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'natalia.cales@hhs.gov','Natalia','Cales' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'natesa.robinson@hhs.gov','Natesa','Robinson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nathaniel.barbato@cms.hhs.gov','Nathaniel','Barbato' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nathaniel.dean@cms.hhs.gov','Nathaniel','Dean' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'neil.kaufman@hhs.gov','Neil','Kaufman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nicholas.proy@cms.hhs.gov','Nicholas','Proy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nicole.hoey@cms.hhs.gov','Nicole','Hoey' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nikki.bratcher-bowman@hhs.gov','Nikki','Bratcher' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'noel.manlove@cms.hhs.gov','Noel','Manlove' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'nydia.sagna@hhs.gov','Nydia','Sagna' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Finance'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'osmani.banos-diaz@hhs.gov','Osmani','Banos-Diaz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'pam.sessoms@hhs.gov','Pamela','Sessoms' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASFR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'patricia.erskine@cms.hhs.gov','Patricia','Erskine' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'patricia.ingram@cms.hhs.gov','Patricia','Ingram' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'patrick.wynne@cms.hhs.gov','Patrick','Wynne' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'paul.jacobsen@hhs.gov','Paul','Jacobsen' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'paul.reed@hhs.gov','Paul','Reed' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Seattle Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'paul.reed@ihs.gov','Paul','Reed' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IHS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'paula.formoso@hhs.gov','Paula','Formoso' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'penny.williams@cms.hhs.gov','Penny','Williams' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'pete.burr@hhs.gov','Pete','Burr' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of the Chief Information Officer'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'peter.haas@cms.hhs.gov','Peter','Haas' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'phillip.harrell@cms.hhs.gov','Phillip','Harrell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'phillip.osborne@nih.gov','Phillip','Osborne' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'phillip.smith@cms.hhs.gov','Phillip','Smith' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'phyllis.lewis@cms.hhs.gov','Phyllis','Lewis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'phyllis.noble@hhs.gov','Phyllis','Noble' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'prudence.goforth@hhs.gov','Prudence','Goforth' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'radee.skipworth@cms.hhs.gov','Radee','Skipworth' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'rafael.mack@cms.hhs.gov','Rafael','Mack' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ralph.ketcher@ihs.gov','Ralph','Ketcher' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Nashville Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ray.tchoulakian@cms.hhs.gov','Ray','Tchoulakian' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'renee.wallace-abney@cms.hhs.gov','Renee','Wallace-Abney' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'richard.asher@cms.hhs.gov','Richard','Asher' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'richard.potocek1@cms.hhs.gov','Richard','Potocek' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'rick.vredenburg@ihs.gov','Rick','Vredenburg' FROM Dropdown.Organization O WHERE O.OrganizationName = 'California Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'rico.batte@cms.hhs.gov','Rico','Batte' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'robert.abadsanto@hhs.gov','Robert','AbadSanto' FROM Dropdown.Organization O WHERE O.OrganizationName = 'San Francisco Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'robinsue.frohboese@hhs.gov','Robinsue','Frohboese' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'rochelle.jones@hhs.gov','Rochelle','Jones' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'roestta.rodwell@cms.hhs.gov','Rosetta','Rodwell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ronald.depas@hhs.gov','Ron','DePas' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ronda.longbrake@ihs.gov','Ronda','Longbrake' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Oklahoma City Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'rose.duckett@hhs.gov','Rose','Duckett' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'roshawn.majors@ihs.gov','Roshawn','Majors' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IHS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'roy.brunson@hhs.gov','Roy','Brunson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ryan.hax@cms.hhs.gov','Ryan','Hax' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ryan.kaufman@cms.hhs.gov','Ryan','Kaufman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'ryan.kooy@cms.hhs.gov','Ryan','Kooy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'salem.fussell@cms.hhs.gov','R. Salem','Fussell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'samuel.gobrail@cms.hhs.gov','Samuel','Gobrail' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sandra.jones@hrsa.hhs.gov','Sandi','Jones' FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sandy.fry@hhs.gov','Sandy','Fry' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'scott.filipovits@cms.hhs.gov','Scott','Filipovits' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'scott.yeager@cms.hhs.gov','Scott','Yeager' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'shamia.blannks@cms.hhs.gov','Shamia','Blanks' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sharon.brause2@cms.hhs.gov','Sherry','Brause' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sharon.jacksonhall@cms.hhs.gov','Sharon','Jackson-Hall' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sharon.white2@cms.hhs.gov','Sharon','White' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sharonda.green@hhs.gov','Sharonda','Green' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'shaun.major@nih.gov','Roshawn','Major' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IHS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sheila.conley@hhs.gov','Sheila','Conley' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'shelby.minchew@cms.hhs.gov','Shelby','Minchew' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sherene.vann@hhs.gov','Sherene','Vann' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Chicago Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sherri.stephens-washington@cms.hhs.gov','Sherri','Stephens-Washington' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sherry.perrier@cms.hhs.gov','Sherry','Perrier' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'shoney.whitfield@hhs.gov','Shoney','Whitfield' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sqt8@cdc.gov','Stephanie','Thomas' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CDC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'stephanie.coppel@cms.hhs.gov','Stephanie','Coppel' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'stephen.stoyer@cms.hhs.gov','Stephen','Stoyer' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'stephen.weber@cms.hhs.gov','Stephen','Weber' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'steve.mitchell@hhs.gov','Steve','Mitchell' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Kansas City Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'steve.novy@hhs.gov','Steve','Novy' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'stu.scrom@hhs.gov','Stu','Scrom' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Budget'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'sulochana.nunna@nih.gov','Sulochana','Nunna' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CIT'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'susan.panasik@hhs.gov','Susan','Panasik' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'susan.rhodes@hhs.gov','Susan','Rhodes' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Boston Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'suzanne.cruz@acl.hhs.gov','Suzanne','Cruz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'suzanne.fialkoff@samhsa.hhs.gov','Suzanne','Fialkoff' FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'suzy.krohn@hhs.gov','Suzy','Krohn' FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tara.jordan@hhs.gov','Tara','Jordan' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tasha.logan@cms.hhs.gov','Tasha','Logan' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'taylortn@mail.nih.gov','Tiffany','Taylor' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'teresa.lewis@hhs.gov','Teresa','Lewis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'terry.nicolosi@acl.hhs.gov','Terry','Nicolosi' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tessie.fitton@cms.hhs.gov','Tessie','Fitton' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'theresa.evans@hhs.gov','Theresa','Evans' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Human Resources'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'theresa.schultz@cms.hhs.gov','Theresa','Schultz' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'therese.martin@hhs.gov','Theresa','Martin' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Seattle Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tiara.freeman@cms.hhs.gov','Tiara','Freeman' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tiffany.richardson@cms.hhs.gov','Tiffany','Richardson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'timothy.barfield@hhs.gov','Timothy','Barfield' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASFR'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'timothy.kelly@cms.hhs.gov','Tim','Kelly' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'timothy.noonan@hhs.gov','Timothy','Noonan' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Atlanta Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tina.zanti@cms.hhs.gov','Tina','Zanti' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tishawn.miller@hhs.gov','Tishawn','Miller' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASL'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'todd.brown@cms.hhs.gov','Todd','Brown' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'todd.pires@hhs.gov','Todd','Pires' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Non-HHS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tony.richardson@cms.hhs.gov','Tony','Richardson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tonya.anderson@cms.hhs.gov','Tonya','Anderson' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tracy.amos@cms.hhs.gov','Tracy','Amos' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tracy.hall@hhs.gov','Tracy','Hall' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tracy.pon@hhs.gov','Tracy','Pon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tri.thai@fda.hhs.gov','Tri','Thai' FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'tyra.jeffries@cms.hhs.gov','Tyra','Jeffries' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'veenu.varma@nih.gov','Veenu','Varma' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'verna.kuwanhoyioma@ihs.gov','Verna','Kuwanhoyioma' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Phoenix Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'veronica.zuni@ihs.gov','Veronica','Zuni' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Albuquerque Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'virginia.clark@hhs.gov','Virginia','Clark' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Kansas City Regional Office'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'vivian.smith@cms.hhs.gov','Vivian','Smith' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'vqh5@cdc.gov','Dauda','Fadeyi' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CDC'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'wendy.blancke@hhs.gov','Wendy','Blancke' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'wendy.blanke@hhs.gov','Wendy','Blancke' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'william.diggs@cms.hhs.gov','William','Diggs' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'william.fisher@ihs.gov','William','Fisher' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Bemidji Area'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'williambro@od.nih.gov','Bronte','Williams' FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'yolanda.charon@cms.hhs.gov','Yolanda','Charon' FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'yvette.alonso@hhs.gov','Yvette','Alonso' FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'
INSERT INTO @Table (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'zvq7@cdc.gov','Lynnette','Swerdlow' FROM Dropdown.Organization O WHERE O.OrganizationName = 'National Center for Emerging and Zoonotic Infectious Diseases'
--End table @Table

--Begin table dbo.Person
INSERT INTO dbo.Person (OrganizationID,EmailAddress,FirstName,LastName,Title) SELECT O.OrganizationID,'christopher.crouch@hhs.gov','Christopher','Crouch','Contractor' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Non-HHS'
INSERT INTO dbo.Person (OrganizationID,EmailAddress,FirstName,LastName,Title) SELECT O.OrganizationID,'todd.pires@hhs.gov','Todd','Pires','Contractor' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Non-HHS'
INSERT INTO dbo.Person (OrganizationID,EmailAddress,FirstName,LastName,Phone,Title) SELECT O.OrganizationID,'linda.waters@hhs.gov','Linda','Waters','301-451-2024','Small Business Specialist' FROM Dropdown.Organization O WHERE O.OrganizationName = 'OS'
INSERT INTO dbo.Person (OrganizationID,EmailAddress,FirstName,LastName) SELECT O.OrganizationID,'teresa.lewis@hhs.gov','Teresa','Lewis' FROM Dropdown.Organization O WHERE O.OrganizationName = 'Office of Grants and Acquisition Policy and Accountability'

DECLARE 
	@OrganizationID INT,
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Phone VARCHAR(50),
	@Title VARCHAR(50),
	@EmailAddress VARCHAR(320)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT T.OrganizationID,LOWER(T.EmailAddress) AS EmailAddress,T.FirstName,T.LastName,T.Phone,T.Title
	FROM @Table T
	ORDER BY T.EmailAddress

OPEN oCursor
FETCH oCursor INTO @OrganizationID,@EmailAddress,@FirstName,@LastName,@Phone,@Title
WHILE @@fetch_status = 0
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.EmailAddress = @EmailAddress)
		INSERT INTO dbo.Person (OrganizationID,EmailAddress,FirstName,LastName,Phone,Title) VALUES (@OrganizationID,@EmailAddress,@FirstName,@LastName,@Phone,@Title)
	--ENDIF
	
	FETCH oCursor INTO @OrganizationID,@EmailAddress,@FirstName,@LastName,@Phone,@Title

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

--Begin table Dropdown.PSC
SET IDENTITY_INSERT Dropdown.PSC ON
INSERT INTO Dropdown.PSC (PSCID) VALUES (0)
SET IDENTITY_INSERT Dropdown.PSC OFF
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('10','WEAPONS'),
	('1005','GUNS, THROUGH 30MM'),
	('1010','GUNS, OVER 30MM UP TO 75MM'),
	('1015','GUNS, 75MM THROUGH 125MM'),
	('1020','GUNS, OVER 125MM THROUGH 150MM'),
	('1025','GUNS, OVER 150MM THROUGH 200MM'),
	('1030','GUNS, OVER 200MM THROUGH 300MM'),
	('1035','GUNS, OVER 300MM'),
	('1040','CHEMICAL WEAPONS AND EQUIPMENT'),
	('1045','LAUNCHERS, TORPEDO AND DEPTH CHARGE'),
	('1055','LAUNCHERS, ROCKET AND PYROTECHNIC'),
	('1070','NETS AND BOOMS, ORDNANCE'),
	('1075','DEGAUSSING AND MINE SWEEPING EQUIPMENT'),
	('1080','CAMOUFLAGE AND DECEPTION EQUIPMENT'),
	('1090','ASSEMBLIES INTERCHANGEABLE BETWEEN WEAPONS IN TWO OR MORE CLASSES'),
	('1095','MISCELLANEOUS WEAPONS'),
	('11','NUCLEAR ORDNANCE'),
	('1105','NUCLEAR BOMBS'),
	('1110','NUCLEAR PROJECTILES'),
	('1115','NUCLEAR WARHEADS AND WARHEAD SECTIONS'),
	('1120','NUCLEAR DEPTH CHARGES'),
	('1125','NUCLEAR DEMOLITION CHARGES'),
	('1127','NUCLEAR ROCKETS'),
	('1130','CONVERSION KITS, NUCLEAR ORDNANCE'),
	('1135','FUZING AND FIRING DEVICES, NUCLEAR ORDNANCE'),
	('1140','NUCLEAR COMPONENTS'),
	('1145','EXPLOSIVE AND PYROTECHNIC COMPONENTS, NUCLEAR ORDNANCE'),
	('1190','SPECIALIZED TEST AND HANDLING EQUIPMENT, NUCLEAR ORDNANCE'),
	('1195','MISCELLANEOUS NUCLEAR ORDNANCE'),
	('12','FIRE CONTROL EQPT.'),
	('1210','FIRE CONTROL DIRECTORS'),
	('1220','FIRE CONTROL COMPUTING SIGHTS AND DEVICES'),
	('1230','FIRE CONTROL SYSTEMS, COMPLETE'),
	('1240','OPTICAL SIGHTING AND RANGING EQUIPMENT'),
	('1250','FIRE CONTROL STABILIZING MECHANISMS'),
	('1260','FIRE CONTROL DESIGNATING AND INDICATING EQUIPMENT'),
	('1265','FIRE CONTROL TRANSMITTING AND RECEIVING EQUIPMENT, EXCEPT AIRBORNE'),
	('1270','AIRCRAFT GUNNERY FIRE CONTROL COMPONENTS'),
	('1280','AIRCRAFT BOMBING FIRE CONTROL COMPONENTS'),
	('1285','FIRE CONTROL RADAR EQUIPMENT, EXCEPT AIRBORNE'),
	('1287','FIRE CONTROL SONAR EQUIPMENT'),
	('1290','MISCELLANEOUS FIRE CONTROL EQUIPMENT'),
	('13','AMMUNITION AND EXPLOSIVES'),
	('1305','AMMUNITION, THROUGH 30MM'),
	('1310','AMMUNITION, OVER 30MM UP TO 75MM'),
	('1315','AMMUNITION, 75MM THROUGH 125MM'),
	('1320','AMMUNITION, OVER 125MM'),
	('1325','BOMBS'),
	('1330','GRENADES'),
	('1336','GUIDED MISSILE WARHEADS AND EXPLOSIVE COMPONENTS'),
	('1337','GUIDED MISSILE AND SPACE VEHICLE EXPLOSIVE PROPULSION UNITS, SOLID FUEL; AND COMPONENTS'),
	('1338','GUIDED MISSILE AND SPACE VEHICLE INERT PROPULSION UNITS, SOLID FUEL; AND COMPONENTS'),
	('1340','ROCKETS, ROCKET AMMUNITION AND ROCKET COMPONENTS'),
	('1345','LAND MINES'),
	('1346','REMOTE MUNITIONS'),
	('1350','UNDERWATER MINE AND COMPONENTS, INERT'),
	('1351','UNDERWATER MINES AND COMPONENTS, EXPLOSIVE'),
	('1352','UNDERWATER MINE DISPOSAL INERT DEVICES'),
	('1353','UNDERWATER MINE DISPOSAL EXPLOSIVE DEVICES'),
	('1355','TORPEDOS AND COMPONENTS, INERT'),
	('1356','TORPEDOS AND COMPONENTS, EXPLOSIVE'),
	('1360','DEPTH CHARGES AND COMPONENTS, INERT'),
	('1361','DEPTH CHARGES AND COMPONENTS, EXPLOSIVE'),
	('1365','MILITARY CHEMICAL AGENTS'),
	('1367','TACTICAL SETS, KITS, AND OUTFITS'),
	('1370','PYROTECHNICS'),
	('1375','DEMOLITION MATERIALS'),
	('1376','BULK EXPLOSIVES'),
	('1377','CARTRIDGE AND PROPELLANT ACTUATED DEVICES AND COMPONENTS'),
	('1385','SURFACE USE EXPLOSIVE ORDNANCE DISPOSAL TOOLS AND EQUIPMENT'),
	('1386','UNDERWATER USE EXPLOSIVE ORDNANCE DISPOSAL AND SWIMMER WEAPONS SYSTEMS TOOLS AND EQUIPMENT'),
	('1390','FUZES AND PRIMERS'),
	('1395','MISCELLANEOUS AMMUNITION'),
	('1398','SPECIALIZED AMMUNITION HANDLING AND SERVICING EQUIPMENT'),
	('14','GUIDED MISSLES'),
	('1410','GUIDED MISSILES'),
	('1420','GUIDED MISSILE COMPONENTS'),
	('1425','GUIDED MISSILE SYSTEMS, COMPLETE'),
	('1427','GUIDED MISSILE SUBSYSTEMS'),
	('1430','GUIDED MISSILE REMOTE CONTROL SYSTEMS'),
	('1440','LAUNCHERS, GUIDED MISSILE'),
	('1450','GUIDED MISSILE HANDLING AND SERVICING EQUIPMENT'),
	('15','AIRCRAFT/AIRFRAME STRUCTURE COMPTS'),
	('1510','AIRCRAFT, FIXED WING'),
	('1520','AIRCRAFT, ROTARY WING'),
	('1540','GLIDERS'),
	('1550','UNMANNED AIRCRAFT'),
	('1560','AIRFRAME STRUCTURAL COMPONENTS'),
	('16','AIRCRAFT COMPONENTS/ACCESSORIES'),
	('1610','AIRCRAFT PROPELLERS AND COMPONENTS'),
	('1615','HELICOPTER ROTOR BLADES, DRIVE MECHANISMS AND COMPONENTS'),
	('1620','AIRCRAFT LANDING GEAR COMPONENTS'),
	('1630','AIRCRAFT WHEEL AND BRAKE SYSTEMS'),
	('1640','AIRCRAFT CONTROL CABLE PRODUCTS'),
	('1650','AIRCRAFT HYDRAULIC, VACUUM, AND DE-ICING SYSTEM COMPONENTS'),
	('1660','AIRCRAFT AIR CONDITIONING, HEATING, AND PRESSURIZING EQUIPMENT'),
	('1670','PARACHUTES; AERIAL PICK UP, DELIVERY, RECOVERY SYSTEMS; AND CARGO TIE DOWN EQUIPMENT'),
	('1680','MISCELLANEOUS AIRCRAFT ACCESSORIES AND COMPONENTS'),
	('17','AIRCRAFT LAUNCH/LAND/GROUND HANDLE'),
	('1710','AIRCRAFT LANDING EQUIPMENT'),
	('1720','AIRCRAFT LAUNCHING EQUIPMENT'),
	('1730','AIRCRAFT GROUND SERVICING EQUIPMENT'),
	('1740','AIRFIELD SPECIALIZED TRUCKS AND TRAILERS'),
	('18','SPACE VEHICLES'),
	('1810','SPACE VEHICLES'),
	('1820','SPACE VEHICLE COMPONENTS'),
	('1830','SPACE VEHICLE REMOTE CONTROL SYSTEMS'),
	('1840','SPACE VEHICLE LAUNCHERS'),
	('1850','SPACE VEHICLE HANDLING AND SERVICING EQUIPMENT'),
	('1860','SPACE SURVIVAL EQUIPMENT'),
	('19','SHIPS, SMALL CRAFT, PONTOON, DOCKS'),
	('1905','COMBAT SHIPS AND LANDING VESSELS'),
	('1910','TRANSPORT VESSELS, PASSENGER AND TROOP'),
	('1915','CARGO AND TANKER VESSELS'),
	('1920','FISHING VESSELS'),
	('1925','SPECIAL SERVICE VESSELS'),
	('1930','BARGES AND LIGHTERS, CARGO'),
	('1935','BARGES AND LIGHTERS, SPECIAL PURPOSE'),
	('1940','SMALL CRAFT'),
	('1945','PONTOONS AND FLOATING DOCKS'),
	('1950','FLOATING DRYDOCKS'),
	('1955','DREDGES'),
	('1990','MISCELLANEOUS VESSELS'),
	('20','SHIP AND MARINE EQUIPMENT'),
	('2010','SHIP AND BOAT PROPULSION COMPONENTS'),
	('2020','RIGGING AND RIGGING GEAR'),
	('2030','DECK MACHINERY'),
	('2040','MARINE HARDWARE AND HULL ITEMS'),
	('2050','BUOYS'),
	('2060','COMMERCIAL FISHING EQUIPMENT'),
	('2090','MISCELLANEOUS SHIP AND MARINE EQUIPMENT'),
	('22','RAILWAY EQUIPMENT'),
	('2210','LOCOMOTIVES'),
	('2220','RAIL CARS'),
	('2230','RIGHT-OF-WAY CONSTRUCTION AND MAINTENANCE EQUIPMENT, RAILROAD'),
	('2240','LOCOMOTIVE AND RAIL CAR ACCESSORIES AND COMPONENTS'),
	('2250','TRACK MATERIAL, RAILROAD'),
	('23','MOTOR VEHICLES, CYCLES, TRAILERS'),
	('2305','GROUND EFFECT VEHICLES'),
	('2310','PASSENGER MOTOR VEHICLES'),
	('2320','TRUCKS AND TRUCK TRACTORS, WHEELED'),
	('2330','TRAILERS'),
	('2340','MOTORCYCLES, MOTOR SCOOTERS, AND BICYCLES'),
	('2350','COMBAT, ASSAULT, AND TACTICAL VEHICLES, TRACKED'),
	('2355','COMBAT, ASSAULT, AND TACTICAL VEHICLES, WHEELED'),
	('24','TRACTORS'),
	('2410','TRACTOR, FULL TRACKED, LOW SPEED'),
	('2420','TRACTORS, WHEELED'),
	('2430','TRACTORS, FULL TRACKED, HIGH SPEED'),
	('25','VEHICULAR EQUIPMENT COMPONENTS'),
	('2510','VEHICULAR CAB, BODY, AND FRAME STRUCTURAL COMPONENTS'),
	('2520','VEHICULAR POWER TRANSMISSION COMPONENTS'),
	('2530','VEHICULAR BRAKE, STEERING, AXLE, WHEEL, AND TRACK COMPONENTS'),
	('2540','VEHICULAR FURNITURE AND ACCESSORIES'),
	('2541','WEAPONS SYSTEMS SPECIFIC VEHICULAR ACCESSORIES'),
	('2590','MISCELLANEOUS VEHICULAR COMPONENTS'),
	('26','TIRES AND TUBES'),
	('2610','TIRES AND TUBES, PNEUMATIC, EXCEPT AIRCRAFT'),
	('2620','TIRES AND TUBES, PNEUMATIC, AIRCRAFT'),
	('2630','TIRES, SOLID AND CUSHION'),
	('2640','TIRE REBUILDING AND TIRE AND TUBE REPAIR MATERIALS'),
	('28','ENGINES AND TURBINES AND COMPONENT'),
	('2805','GASOLINE RECIPROCATING ENGINES, EXCEPT AIRCRAFT; AND COMPONENTS'),
	('2810','GASOLINE RECIPROCATING ENGINES, AIRCRAFT PRIME MOVER; AND COMPONENTS'),
	('2815','DIESEL ENGINES AND COMPONENTS'),
	('2820','STEAM ENGINES, RECIPROCATING; AND COMPONENTS'),
	('2825','STEAM TURBINES AND COMPONENTS'),
	('2830','WATER TURBINES AND WATER WHEELS; AND COMPONENTS'),
	('2835','GAS TURBINES AND JET ENGINES; NON-AIRCRAFT PRIME MOVER, AIRCRAFT NON-PRIME MOVER, AND COMPONENTS'),
	('2840','GAS TURBINES AND JET ENGINES, AIRCRAFT, PRIME MOVING; AND COMPONENTS'),
	('2845','ROCKET ENGINES AND COMPONENTS'),
	('2850','GASOLINE ROTARY ENGINES AND COMPONENTS'),
	('2895','MISCELLANEOUS ENGINES AND COMPONENTS'),
	('29','ENGINE ACCESSORIES'),
	('2910','ENGINE FUEL SYSTEM COMPONENTS, NONAIRCRAFT'),
	('2915','ENGINE FUEL SYSTEM COMPONENTS, AIRCRAFT AND MISSILE PRIME MOVERS'),
	('2920','ENGINE ELECTRICAL SYSTEM COMPONENTS, NONAIRCRAFT'),
	('2925','ENGINE ELECTRICAL SYSTEM COMPONENTS, AIRCRAFT PRIME MOVING'),
	('2930','ENGINE COOLING SYSTEM COMPONENTS, NONAIRCRAFT'),
	('2935','ENGINE SYSTEM COOLING COMPONENTS, AIRCRAFT PRIME MOVING'),
	('2940','ENGINE AIR AND OIL FILTERS, STRAINERS, AND CLEANERS, NONAIRCRAFT'),
	('2945','ENGINE AIR AND OIL FILTERS, CLEANERS, AIRCRAFT PRIME MOVING'),
	('2950','TURBOSUPERCHARGER AND COMPONENTS'),
	('2990','MISCELLANEOUS ENGINE ACCESSORIES, NONAIRCRAFT'),
	('2995','MISCELLANEOUS ENGINE ACCESSORIES, AIRCRAFT'),
	('30','MECHANICAL POWER TRANSMISSION EQPT'),
	('3010','TORQUE CONVERTERS AND SPEED CHANGERS'),
	('3020','GEARS, PULLEYS, SPROCKETS, AND TRANSMISSION CHAIN'),
	('3030','BELTING, DRIVE BELTS, FAN BELTS, AND ACCESSORIES'),
	('3040','MISCELLANEOUS POWER TRANSMISSION EQUIPMENT'),
	('31','BEARINGS'),
	('3110','BEARINGS, ANTIFRICTION, UNMOUNTED'),
	('3120','BEARINGS, PLAIN, UNMOUNTED'),
	('3130','BEARINGS, MOUNTED'),
	('32','WOODWORKING MACHINERY AND EQPT'),
	('3210','SAWMILL AND PLANING MILL MACHINERY'),
	('3220','WOODWORKING MACHINES'),
	('3230','TOOLS AND ATTACHMENTS FOR WOODWORKING MACHINERY'),
	('34','METALWORKING MACHINERY'),
	('3405','SAWS AND FILING MACHINES'),
	('3408','MACHINING CENTERS AND WAY-TYPE MACHINES'),
	('3410','ELECTRICAL AND ULTRASONIC EROSION MACHINES'),
	('3411','BORING MACHINES'),
	('3412','BROACHING MACHINES'),
	('3413','DRILLING AND TAPPING MACHINES'),
	('3414','GEAR CUTTING AND FINISHING MACHINES'),
	('3415','GRINDING MACHINES'),
	('3416','LATHES'),
	('3417','MILLING MACHINES'),
	('3418','PLANERS AND SHAPERS'),
	('3419','MISCELLANEOUS MACHINE TOOLS'),
	('3422','ROLLING MILLS AND DRAWING MACHINES'),
	('3424','METAL HEAT TREATING AND NON-THERMAL TREATING EQUIPMENT'),
	('3426','METAL FINISHING EQUIPMENT'),
	('3431','ELECTRIC ARC WELDING EQUIPMENT'),
	('3432','ELECTRIC RESISTANCE WELDING EQUIPMENT'),
	('3433','GAS WELDING, HEAT CUTTING, AND METALIZING EQUIPMENT'),
	('3436','WELDING POSITIONERS AND MANIPULATORS'),
	('3438','MISCELLANEOUS WELDING EQUIPMENT'),
	('3439','MISCELLANEOUS WELDING, SOLDERING, AND BRAZING SUPPLIES AND ACCESSORIES'),
	('3441','BENDING AND FORMING MACHINES'),
	('3442','HYDRAULIC AND PNEUMATIC PRESSES, POWER DRIVEN'),
	('3443','MECHANICAL PRESSES, POWER DRIVEN'),
	('3444','MANUAL PRESSES'),
	('3445','PUNCHING AND SHEARING MACHINES'),
	('3446','FORGING MACHINERY AND HAMMERS'),
	('3447','WIRE AND METAL RIBBON FORMING MACHINES'),
	('3448','RIVETING MACHINES'),
	('3449','MISCELLANEOUS SECONDARY METAL FORMING AND CUTTING MACHINES'),
	('3450','MACHINE TOOLS, PORTABLE'),
	('3455','CUTTING TOOLS FOR MACHINE TOOLS'),
	('3456','CUTTING AND FORMING TOOLS FOR SECONDARY METALWORKING MACHINERY'),
	('3460','MACHINE TOOL ACCESSORIES'),
	('3461','ACCESSORIES FOR SECONDARY METALWORKING MACHINERY'),
	('3465','PRODUCTION JIGS, FIXTURES, AND TEMPLATES'),
	('3470','MACHINE SHOP SETS, KITS, AND OUTFITS'),
	('35','SERVICE AND TRADE EQPT'),
	('3510','LAUNDRY AND DRY CLEANING EQUIPMENT'),
	('3520','SHOE REPAIRING EQUIPMENT'),
	('3530','INDUSTRIAL SEWING MACHINES AND MOBILE TEXTILE REPAIR SHOPS'),
	('3540','WRAPPING AND PACKAGING MACHINERY'),
	('3550','VENDING AND COIN OPERATED MACHINES'),
	('3590','MISCELLANEOUS SERVICE AND TRADE EQUIPMENT'),
	('36','SPECIAL INDUSTRY MACHINERY'),
	('3605','FOOD PRODUCTS MACHINERY AND EQUIPMENT'),
	('3610','PRINTING, DUPLICATING, AND BOOKBINDING EQUIPMENT'),
	('3611','INDUSTRIAL MARKING MACHINES'),
	('3615','PULP AND PAPER INDUSTRIES MACHINERY'),
	('3620','RUBBER AND PLASTICS WORKING MACHINERY'),
	('3625','TEXTILE INDUSTRIES MACHINERY'),
	('3630','CLAY AND CONCRETE PRODUCTS INDUSTRIES MACHINERY'),
	('3635','CRYSTAL AND GLASS INDUSTRIES MACHINERY'),
	('3640','TOBACCO MANUFACTURING MACHINERY'),
	('3645','LEATHER TANNING AND LEATHER WORKING INDUSTRIES MACHINERY'),
	('3650','CHEMICAL AND PHARMACEUTICAL PRODUCTS MANUFACTURING MACHINERY'),
	('3655','GAS GENERATING AND DISPENSING SYSTEMS, FIXED OR MOBILE'),
	('3660','INDUSTRIAL SIZE REDUCTION MACHINERY'),
	('3670','SPECIALIZED SEMICONDUCTOR, MICROCIRCUIT, AND PRINTED CIRCUIT BOARD MANUFACTURING MACHINERY'),
	('3680','FOUNDRY MACHINERY, RELATED EQUIPMENT AND SUPPLIES'),
	('3685','SPECIALIZED METAL CONTAINER MANUFACTURING MACHINERY AND RELATED EQUIPMENT'),
	('3690','SPECIALIZED AMMUNITION AND ORDNANCE MACHINERY AND RELATED EQUIPMENT'),
	('3693','INDUSTRIAL ASSEMBLY MACHINES'),
	('3694','CLEAN WORK STATIONS, CONTROLLED ENVIRONMENT, AND RELATED EQUIPMENT'),
	('3695','MISCELLANEOUS SPECIAL INDUSTRY MACHINERY'),
	('37','AGRICULTURAL MACHINERY AND EQPT'),
	('3710','SOIL PREPARATION EQUIPMENT'),
	('3720','HARVESTING EQUIPMENT'),
	('3730','DAIRY, POULTRY, AND LIVESTOCK EQUIPMENT'),
	('3740','PEST, DISEASE, AND FROST CONTROL EQUIPMENT'),
	('3750','GARDENING IMPLEMENTS AND TOOLS'),
	('3770','SADDLERY, HARNESS, WHIPS, AND RELATED ANIMAL FURNISHINGS'),
	('38','CONSTRUCT/MINE/EXCAVATE/HIGHWY EQPT'),
	('3805','EARTH MOVING AND EXCAVATING EQUIPMENT'),
	('3810','CRANES AND CRANE-SHOVELS'),
	('3815','CRANE AND CRANE-SHOVEL ATTACHMENTS'),
	('3820','MINING, ROCK DRILLING, EARTH BORING, AND RELATED EQUIPMENT'),
	('3825','ROAD CLEARING, CLEANING, AND MARKING EQUIPMENT'),
	('3830','TRUCK AND TRACTOR ATTACHMENTS'),
	('3835','PETROLEUM PRODUCTION AND DISTRIBUTION EQUIPMENT'),
	('3895','MISCELLANEOUS CONSTRUCTION EQUIPMENT'),
	('39','MATERIALS HANDLING EQPT'),
	('3910','CONVEYORS'),
	('3915','MATERIALS FEEDERS'),
	('3920','MATERIAL HANDLING EQUIPMENT, NONSELF-PROPELLED'),
	('3930','WAREHOUSE TRUCKS AND TRACTORS, SELF-PROPELLED'),
	('3940','BLOCKS, TACKLE, RIGGING, AND SLINGS'),
	('3950','WINCHES, HOISTS, CRANES, AND DERRICKS'),
	('3960','FREIGHT ELEVATORS'),
	('3990','MISCELLANEOUS MATERIALS HANDLING EQUIPMENT'),
	('40','ROPE, CABLE, CHAIN, FITTINGS'),
	('4010','CHAIN AND WIRE ROPE'),
	('4020','FIBER ROPE, CORDAGE, AND TWINE'),
	('4030','FITTINGS FOR ROPE, CABLE, AND CHAIN'),
	('41','REFRIG, AIR CONDIT/CIRCULAT EQPT'),
	('4110','REFRIGERATION EQUIPMENT'),
	('4120','AIR CONDITIONING EQUIPMENT'),
	('4130','REFRIGERATION AND AIR CONDITIONING COMPONENTS'),
	('4140','FANS, AIR CIRCULATORS, AND BLOWER EQUIPMENT'),
	('4150','VORTEX TUBES AND OTHER RELATED COOLING TUBES'),
	('42','FIRE/RESCUE/SAFETY; ENVIRO PROTECT'),
	('4210','FIRE FIGHTING EQUIPMENT'),
	('4220','MARINE LIFESAVING AND DIVING EQUIPMENT'),
	('4230','DECONTAMINATING AND IMPREGNATING EQUIPMENT'),
	('4235','HAZARDOUS MATERIAL SPILL CONTAINMENT AND CLEAN-UP EQUIPMENT AND MATERIAL'),
	('4240','SAFETY AND RESCUE EQUIPMENT'),
	('4250','RECYCLING AND RECLAMATION EQUIPMENT'),
	('43','PUMPS AND COMPRESSORS'),
	('4310','COMPRESSORS AND VACUUM PUMPS'),
	('4320','POWER AND HAND PUMPS'),
	('4330','CENTRIFUGALS, SEPARATORS, AND PRESSURE AND VACUUM FILTERS'),
	('44','FURNACE/STEAM/DRYING; NUCL REACTOR'),
	('4410','INDUSTRIAL BOILERS'),
	('4420','HEAT EXCHANGERS AND STEAM CONDENSERS'),
	('4430','INDUSTRIAL FURNACES, KILNS, LEHRS, AND OVENS'),
	('4440','DRIERS, DEHYDRATORS, AND ANHYDRATORS'),
	('4460','AIR PURIFICATION EQUIPMENT'),
	('4470','NUCLEAR REACTORS'),
	('45','PLUMBING, HEATING, WASTE DISPOSAL'),
	('4510','PLUMBING FIXTURES AND ACCESSORIES'),
	('4520','SPACE AND WATER HEATING EQUIPMENT'),
	('4530','FUEL BURNING EQUIPMENT UNITS'),
	('4540','WASTE DISPOSAL EQUIPMENT'),
	('46','WATER PURIFICATION/SEWAGE TREATMENT'),
	('4610','WATER PURIFICATION EQUIPMENT'),
	('4620','WATER DISTILLATION EQUIPMENT, MARINE AND INDUSTRIAL'),
	('4630','SEWAGE TREATMENT EQUIPMENT'),
	('47','PIPE, TUBING, HOSE, AND FITTINGS'),
	('4710','PIPE, TUBE AND RIGID TUBING'),
	('4720','HOSE AND FLEXIBLE TUBING'),
	('4730','HOSE, PIPE, TUBE, LUBRICATION, AND RAILING FITTINGS'),
	('48','VALVES'),
	('4810','VALVES, POWERED'),
	('4820','VALVES, NONPOWERED'),
	('49','MAINT/REPAIR SHOP EQPT'),
	('4910','MOTOR VEHICLE MAINTENANCE AND REPAIR SHOP SPECIALIZED EQUIPMENT'),
	('4920','AIRCRAFT MAINTENANCE AND REPAIR SHOP SPECIALIZED EQUIPMENT'),
	('4921','TORPEDO MAINTENANCE, REPAIR, AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4923','DEPTH CHARGES AND UNDERWATER MINES MAINTENANCE, REPAIR, AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4925','AMMUNITION MAINTENANCE, REPAIR, AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4927','ROCKET MAINTENANCE, REPAIR AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4930','LUBRICATION AND FUEL DISPENSING EQUIPMENT'),
	('4931','FIRE CONTROL MAINTENANCE AND REPAIR SHOP SPECIALIZED EQUIPMENT'),
	('4933','WEAPONS MAINTENANCE AND REPAIR SHOP SPECIALIZED EQUIPMENT'),
	('4935','GUIDED MISSILE MAINTENANCE, REPAIR, AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4940','MISCELLANEOUS MAINTENANCE AND REPAIR SHOP SPECIALIZED EQUIPMENT'),
	('4960','SPACE VEHICLE MAINTENANCE, REPAIR, AND CHECKOUT SPECIALIZED EQUIPMENT'),
	('4970','MULTIPLE GUIDED WEAPONS, SPECIALIZED MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('51','HAND TOOLS'),
	('5110','HAND TOOLS, EDGED, NONPOWERED'),
	('5120','HAND TOOLS, NONEDGED, NONPOWERED'),
	('5130','HAND TOOLS, POWER DRIVEN'),
	('5133','DRILL BITS, COUNTERBORES, AND COUNTERSINKS: HAND AND MACHINE'),
	('5136','TAPS, DIES, AND COLLETS; HAND AND MACHINE'),
	('5140','TOOL AND HARDWARE BOXES'),
	('5180','SETS, KITS, AND OUTFITS OF HAND TOOLS'),
	('52','MEASURING TOOLS'),
	('5210','MEASURING TOOLS, CRAFTSMEN''S'),
	('5220','INSPECTION GAGES AND PRECISION LAYOUT TOOLS'),
	('5280','SETS, KITS, AND OUTFITS OF MEASURING TOOLS'),
	('53','HARDWARE AND ABRASIVES'),
	('5305','SCREWS'),
	('5306','BOLTS'),
	('5307','STUDS'),
	('5310','NUTS AND WASHERS'),
	('5315','NAILS, MACHINE KEYS, AND PINS'),
	('5320','RIVETS'),
	('5325','FASTENING DEVICES'),
	('5330','PACKING AND GASKET MATERIALS'),
	('5331','O-RING'),
	('5335','METAL SCREENING'),
	('5340','HARDWARE, COMMERCIAL'),
	('5342','HARDWARE, WEAPON SYSTEM'),
	('5345','DISKS AND STONES, ABRASIVE'),
	('5350','ABRASIVE MATERIALS'),
	('5355','KNOBS AND POINTERS'),
	('5360','COIL, FLAT, LEAF, AND WIRE SPRINGS'),
	('5365','BUSHINGS, RINGS, SHIMS, AND SPACERS'),
	('54','PREFAB STRUCTURES/SCAFFOLDING'),
	('5410','PREFABRICATED AND PORTABLE BUILDINGS'),
	('5411','RIGID WALL SHELTERS'),
	('5419','COLLECTIVE MODULAR SUPPORT SYSTEM'),
	('5420','BRIDGES, FIXED AND FLOATING'),
	('5430','STORAGE TANKS'),
	('5440','SCAFFOLDING EQUIPMENT AND CONCRETE FORMS'),
	('5445','PREFABRICATED TOWER STRUCTURES'),
	('5450','MISCELLANEOUS PREFABRICATED STRUCTURES'),
	('55','LUMBER, MILLWORK, PLYWOOD, VENEER'),
	('5510','LUMBER AND RELATED BASIC WOOD MATERIALS'),
	('5520','MILLWORK'),
	('5530','PLYWOOD AND VENEER'),
	('56','CONSTRUCTION AND BUILDING MATERIAL'),
	('5610','MINERAL CONSTRUCTION MATERIALS, BULK'),
	('5620','TILE, BRICK AND BLOCK'),
	('5630','PIPE AND CONDUIT, NONMETALLIC'),
	('5640','WALLBOARD, BUILDING PAPER, AND THERMAL INSULATION MATERIALS'),
	('5650','ROOFING AND SIDING MATERIALS'),
	('5660','FENCING, FENCES, GATES AND COMPONENTS'),
	('5670','BUILDING COMPONENTS, PREFABRICATED'),
	('5675','NONWOOD CONSTRUCTION LUMBER AND RELATED MATERIALS'),
	('5680','MISCELLANEOUS CONSTRUCTION MATERIALS'),
	('58','COMM/DETECT/COHERENT RADIATION'),
	('5805','TELEPHONE AND TELEGRAPH EQUIPMENT'),
	('5810','COMMUNICATIONS SECURITY EQUIPMENT AND COMPONENTS'),
	('5811','OTHER CRYPTOLOGIC EQUIPMENT AND COMPONENTS'),
	('5815','TELETYPE AND FACSIMILE EQUIPMENT'),
	('5820','RADIO AND TELEVISION COMMUNICATION EQUIPMENT, EXCEPT AIRBORNE'),
	('5821','RADIO AND TELEVISION COMMUNICATION EQUIPMENT, AIRBORNE'),
	('5825','RADIO NAVIGATION EQUIPMENT, EXCEPT AIRBORNE'),
	('5826','RADIO NAVIGATION EQUIPMENT, AIRBORNE'),
	('5830','INTERCOMMUNICATION AND PUBLIC ADDRESS SYSTEMS, EXCEPT AIRBORNE'),
	('5831','INTERCOMMUNICATION AND PUBLIC ADDRESS SYSTEMS, AIRBORNE'),
	('5835','SOUND RECORDING AND REPRODUCING EQUIPMENT'),
	('5836','VIDEO RECORDING AND REPRODUCING EQUIPMENT'),
	('5840','RADAR EQUIPMENT, EXCEPT AIRBORNE'),
	('5841','RADAR EQUIPMENT, AIRBORNE'),
	('5845','UNDERWATER SOUND EQUIPMENT'),
	('5850','VISIBLE AND INVISIBLE LIGHT COMMUNICATION EQUIPMENT'),
	('5855','NIGHT VISION EQUIPMENT, EMITTED AND REFLECTED RADIATION'),
	('5860','STIMULATED COHERENT RADIATION DEVICES, COMPONENTS, AND ACCESSORIES'),
	('5865','ELECTRONIC COUNTERMEASURES, COUNTER-COUNTERMEASURES AND QUICK REACTION CAPABILITY EQUIPMENT'),
	('5895','MISCELLANEOUS COMMUNICATION EQUIPMENT'),
	('59','ELECTRICAL/ELECTRONIC EQPT COMPNTS'),
	('5905','RESISTORS'),
	('5910','CAPACITORS'),
	('5915','FILTERS AND NETWORKS'),
	('5920','FUSES, ARRESTORS, ABSORBERS, AND PROTECTORS'),
	('5925','CIRCUIT BREAKERS'),
	('5930','SWITCHES'),
	('5935','CONNECTORS, ELECTRICAL'),
	('5940','LUGS, TERMINALS, AND TERMINAL STRIPS'),
	('5945','RELAYS AND SOLENOIDS'),
	('5950','COILS AND TRANSFORMERS'),
	('5955','OSCILLATORS AND PIEZOELECTRIC CRYSTALS'),
	('5960','ELECTRON TUBES AND ASSOCIATED HARDWARE'),
	('5961','SEMICONDUCTOR DEVICES AND ASSOCIATED HARDWARE'),
	('5962','MICROCIRCUITS, ELECTRONIC'),
	('5963','ELECTRONIC MODULES'),
	('5965','HEADSETS, HANDSETS, MICROPHONES AND SPEAKERS'),
	('5970','ELECTRICAL INSULATORS AND INSULATING MATERIALS'),
	('5975','ELECTRICAL HARDWARE AND SUPPLIES'),
	('5977','ELECTRICAL CONTACT BRUSHES AND ELECTRODES'),
	('5980','OPTOELECTRONIC DEVICES AND ASSOCIATED HARDWARE'),
	('5985','ANTENNAS, WAVEGUIDES, AND RELATED EQUIPMENT'),
	('5990','SYNCHROS AND RESOLVERS'),
	('5995','CABLE, CORD, AND WIRE ASSEMBLIES: COMMUNICATION EQUIPMENT'),
	('5996','AMPLIFIERS'),
	('5998','ELECTRICAL AND ELECTRONIC ASSEMBLIES, BOARDS, CARDS, AND ASSOCIATED HARDWARE'),
	('5999','MISCELLANEOUS ELECTRICAL AND ELECTRONIC COMPONENTS'),
	('60','FIBER OPTIC'),
	('6010','FIBER OPTIC CONDUCTORS'),
	('6015','FIBER OPTIC CABLES'),
	('6020','FIBER OPTIC CABLE ASSEMBLIES AND HARNESSES'),
	('6021','FIBER OPTIC SWITCHES'),
	('6030','FIBER OPTIC DEVICES'),
	('6032','FIBER OPTIC LIGHT SOURCES AND PHOTO DETECTORS'),
	('6035','FIBER OPTIC LIGHT TRANSFER AND IMAGE TRANSFER DEVICES'),
	('6060','FIBER OPTIC INTERCONNECTORS'),
	('6070','FIBER OPTIC ACCESSORIES AND SUPPLIES'),
	('6080','FIBER OPTIC KITS AND SETS'),
	('6099','MISCELLANEOUS FIBER OPTIC COMPONENTS'),
	('61','ELECTRIC WIRE, POWER DISTRIB EQPT'),
	('6105','MOTORS, ELECTRICAL'),
	('6110','ELECTRICAL CONTROL EQUIPMENT'),
	('6115','GENERATORS AND GENERATOR SETS, ELECTRICAL'),
	('6116','FUEL CELL POWER UNITS, COMPONENTS, AND ACCESSORIES'),
	('6117','SOLAR ELECTRIC POWER SYSTEMS'),
	('6120','TRANSFORMERS: DISTRIBUTION AND POWER STATION'),
	('6125','CONVERTERS, ELECTRICAL, ROTATING'),
	('6130','CONVERTERS, ELECTRICAL, NONROTATING'),
	('6135','BATTERIES, NONRECHARGEABLE'),
	('6140','BATTERIES, RECHARGEABLE'),
	('6145','WIRE AND CABLE, ELECTRICAL'),
	('6150','MISCELLANEOUS ELECTRIC POWER AND DISTRIBUTION EQUIPMENT'),
	('6160','MISCELLANEOUS BATTERY RETAINING FIXTURES, LINERS AND ANCILLARY ITEMS'),
	('62','LIGHTING FIXTURES, LAMPS'),
	('6210','INDOOR AND OUTDOOR ELECTRIC LIGHTING FIXTURES'),
	('6220','ELECTRIC VEHICULAR LIGHTS AND FIXTURES'),
	('6230','ELECTRIC PORTABLE AND HAND LIGHTING EQUIPMENT'),
	('6240','ELECTRIC LAMPS'),
	('6250','BALLASTS, LAMPHOLDERS, AND STARTERS'),
	('6260','NONELECTRICAL LIGHTING FIXTURES'),
	('63','ALARM, SIGNAL, SECURITY DETECTION'),
	('6310','TRAFFIC AND TRANSIT SIGNAL SYSTEMS'),
	('6320','SHIPBOARD ALARM AND SIGNAL SYSTEMS'),
	('6330','RAILROAD SIGNAL AND WARNING DEVICES'),
	('6340','AIRCRAFT ALARM AND SIGNAL SYSTEMS'),
	('6350','MISCELLANEOUS ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('65','MEDICAL/DENTAL/VETERINARY EQPT/SUPP'),
	('6505','DRUGS AND BIOLOGICALS'),
	('6508','MEDICATED COSMETICS AND TOILETRIES'),
	('6509','DRUGS AND BIOLOGICALS, VETERINARY USE'),
	('6510','SURGICAL DRESSING MATERIALS'),
	('6515','MEDICAL AND SURGICAL INSTRUMENTS, EQUIPMENT, AND SUPPLIES'),
	('6520','DENTAL INSTRUMENTS, EQUIPMENT, AND SUPPLIES'),
	('6525','IMAGING EQUIPMENT AND SUPPLIES: MEDICAL, DENTAL, VETERINARY'),
	('6530','HOSPITAL FURNITURE, EQUIPMENT, UTENSILS, AND SUPPLIES'),
	('6532','HOSPITAL AND SURGICAL CLOTHING AND RELATED SPECIAL PURPOSE ITEMS'),
	('6540','OPHTHALMIC INSTRUMENTS, EQUIPMENT, AND SUPPLIES'),
	('6545','REPLENISHABLE FIELD MEDICAL SETS, KITS, AND OUTFITS'),
	('6550','IN VITRO DIAGNOSTIC SUBSTANCES, REAGENTS, TEST KITS AND SETS')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('66','INSTRUMENTS AND LABORATORY EQPT'),
	('6605','NAVIGATIONAL INSTRUMENTS'),
	('6610','FLIGHT INSTRUMENTS'),
	('6615','AUTOMATIC PILOT MECHANISMS AND AIRBORNE GYRO COMPONENTS'),
	('6620','ENGINE INSTRUMENTS'),
	('6625','ELECTRICAL AND ELECTRONIC PROPERTIES MEASURING AND TESTING INSTRUMENTS'),
	('6630','CHEMICAL ANALYSIS INSTRUMENTS'),
	('6635','PHYSICAL PROPERTIES TESTING AND INSPECTION'),
	('6636','ENVIRONMENTAL CHAMBERS AND RELATED EQUIPMENT'),
	('6640','LABORATORY EQUIPMENT AND SUPPLIES'),
	('6645','TIME MEASURING INSTRUMENTS'),
	('6650','OPTICAL INSTRUMENTS, TEST EQUIPMENT, COMPONENTS AND ACCESSORIES'),
	('6655','GEOPHYSICAL INSTRUMENTS'),
	('6660','METEOROLOGICAL INSTRUMENTS AND APPARATUS'),
	('6665','HAZARD-DETECTING INSTRUMENTS AND APPARATUS'),
	('6670','SCALES AND BALANCES'),
	('6675','DRAFTING, SURVEYING, AND MAPPING INSTRUMENTS'),
	('6680','LIQUID AND GAS FLOW, LIQUID LEVEL, AND MECHANICAL MOTION MEASURING INSTRUMENTS'),
	('6685','PRESSURE, TEMPERATURE, AND HUMIDITY MEASURING AND CONTROLLING INSTRUMENTS'),
	('6695','COMBINATION AND MISCELLANEOUS INSTRUMENTS'),
	('67','PHOTOGRAPHIC EQPT'),
	('6710','CAMERAS, MOTION PICTURE'),
	('6720','CAMERAS, STILL PICTURE'),
	('6730','PHOTOGRAPHIC PROJECTION EQUIPMENT'),
	('6740','PHOTOGRAPHIC DEVELOPING AND FINISHING EQUIPMENT'),
	('6750','PHOTOGRAPHIC SUPPLIES'),
	('6760','PHOTOGRAPHIC EQUIPMENT AND ACCESSORIES'),
	('6770','FILM, PROCESSED'),
	('6780','PHOTOGRAPHIC SETS, KITS, AND OUTFITS'),
	('68','CHEMICALS AND CHEMICAL PRODUCTS'),
	('6810','CHEMICALS'),
	('6820','DYES'),
	('6830','GASES: COMPRESSED AND LIQUEFIED'),
	('6840','PEST CONTROL AGENTS AND DISINFECTANTS'),
	('6850','MISCELLANEOUS CHEMICAL SPECIALTIES'),
	('69','TRAINING AIDS AND DEVICES'),
	('6910','TRAINING AIDS'),
	('6920','ARMAMENT TRAINING DEVICES'),
	('6930','OPERATION TRAINING DEVICES'),
	('6940','COMMUNICATION TRAINING DEVICES'),
	('70','ADP EQPT/SOFTWARE/SUPPLIES AND EQPT'),
	('7010','ADPE SYSTEM CONFIGURATION'),
	('7020','ADP CENTRAL PROCESSING UNIT (CPU, COMPUTER), ANALOG'),
	('7021','ADP CENTRAL PROCESSING UNIT (CPU, COMPUTER), DIGITAL'),
	('7022','ADP CENTRAL PROCESSING UNIT (CPU, COMPUTER), HYBRID'),
	('7025','ADP INPUT/OUTPUT AND STORAGE DEVICES'),
	('7030','ADP SOFTWARE'),
	('7035','ADP SUPPORT EQUIPMENT'),
	('7040','PUNCHED CARD EQUIPMENT'),
	('7042','MINI AND MICRO COMPUTER CONTROL DEVICES'),
	('7045','ADP SUPPLIES'),
	('7050','ADP COMPONENTS'),
	('71','FURNITURE'),
	('7105','HOUSEHOLD FURNITURE'),
	('7110','OFFICE FURNITURE'),
	('7125','CABINETS, LOCKERS, BINS, AND SHELVING'),
	('7195','MISCELLANEOUS FURNITURE AND FIXTURES'),
	('72','HOUSEHOLD/COMMERC FURNISH/APPLIANCE'),
	('7210','HOUSEHOLD FURNISHINGS'),
	('7220','FLOOR COVERINGS'),
	('7230','DRAPERIES, AWNINGS, AND SHADES'),
	('7240','HOUSEHOLD AND COMMERCIAL UTILITY CONTAINERS'),
	('7290','MISCELLANEOUS HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('73','FOOD PREPARATION/SERVING EQPT'),
	('7310','FOOD COOKING, BAKING, AND SERVING EQUIPMENT'),
	('7320','KITCHEN EQUIPMENT AND APPLIANCES'),
	('7330','KITCHEN HAND TOOLS AND UTENSILS'),
	('7340','CUTLERY AND FLATWARE'),
	('7350','TABLEWARE'),
	('7360','SETS, KITS, OUTFITS AND MODULES, FOOD PREPERATION AND SERVING'),
	('74','OFFICE MACH/TEXT PROCESS/VISIB REC'),
	('7420','ACCOUNTING AND CALCULATING MACHINES'),
	('7430','TYPEWRITERS AND OFFICE TYPE COMPOSING MACHINES'),
	('7435','OFFICE INFORMATION SYSTEM EQUIPMENT'),
	('7450','OFFICE TYPE SOUND RECORDING AND REPRODUCING MACHINES'),
	('7460','VISIBLE RECORD EQUIPMENT'),
	('7490','MISCELLANEOUS OFFICE MACHINES'),
	('75','OFFICE SUPPLIES AND DEVICES'),
	('7510','OFFICE SUPPLIES'),
	('7520','OFFICE DEVICES AND ACCESSORIES'),
	('7530','STATIONERY AND RECORD FORMS'),
	('7540','STANDARD FORMS'),
	('76','BOOKS, MAPS, OTHER PUBLICATIONS'),
	('7610','BOOKS AND PAMPHLETS'),
	('7630','NEWSPAPERS AND PERIODICALS'),
	('7640','MAPS, ATLASES, CHARTS, AND GLOBES'),
	('7641','AERONAUTICAL MAPS, CHARTS AND GEODETIC PRODUCTS'),
	('7642','HYDROGRAPHIC MAPS, CHARTS AND GEODETIC PRODUCTS'),
	('7643','TOPOGRAPHIC MAPS, CHARTS AND GEODETIC PRODUCTS'),
	('7644','DIGITAL MAPS, CHARTS AND GEODETIC PRODUCTS'),
	('7650','DRAWINGS AND SPECIFICATIONS'),
	('7660','SHEET AND BOOK MUSIC'),
	('7670','MICROFILM, PROCESSED'),
	('7690','MISCELLANEOUS PRINTED MATTER'),
	('77','MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('7710','MUSICAL INSTRUMENTS'),
	('7720','MUSICAL INSTRUMENT PARTS AND ACCESSORIES'),
	('7730','PHONOGRAPHS, RADIOS, AND TELEVISION SETS: HOME TYPE'),
	('7735','PARTS AND ACCESSORIES OF PHONOGRAPHS, RADIOS, AND TELEVISION SET: HOME TYPE'),
	('7740','PHONOGRAPH RECORDS'),
	('78','RECREATIONAL/ATHLETIC EQPT'),
	('7810','ATHLETIC AND SPORTING EQUIPMENT'),
	('7820','GAMES, TOYS, AND WHEELED GOODS'),
	('7830','RECREATIONAL AND GYMNASTIC EQUIPMENT'),
	('79','CLEANING EQPT AND SUPPLIES'),
	('7910','FLOOR POLISHERS AND VACUUM CLEANING EQUIPMENT'),
	('7920','BROOMS, BRUSHES, MOPS, AND SPONGES'),
	('7930','CLEANING AND POLISHING COMPOUNDS AND PREPARATIONS'),
	('80','BRUSHES, PAINTS, SEALERS, ADHESIVES'),
	('8010','PAINTS, DOPES, VARNISHES, AND RELATED PRODUCTS'),
	('8020','PAINT AND ARTISTS'' BRUSHES'),
	('8030','PRESERVATIVE AND SEALING COMPOUNDS'),
	('8040','ADHESIVES'),
	('81','CONTAINERS/PACKAGING/PACKING SUPPL'),
	('8105','BAGS AND SACKS'),
	('8110','DRUMS AND CANS'),
	('8115','BOXES, CARTONS, AND CRATES'),
	('8120','COMMERCIAL AND INDUSTRIAL GAS CYLINDERS'),
	('8125','BOTTLES AND JARS'),
	('8130','REELS AND SPOOLS'),
	('8135','PACKAGING AND PACKING BULK MATERIALS'),
	('8140','AMMUNITION AND NUCLEAR ORDNANCE BOXES, PACKAGES AND SPECIAL CONTAINERS'),
	('8145','SPECIALIZED SHIPPING AND STORAGE CONTAINERS'),
	('8150','FREIGHT CONTAINERS'),
	('83','TEXTILE/LEATHER/FUR; TENT; FLAG'),
	('8305','TEXTILE FABRICS'),
	('8310','YARN AND THREAD'),
	('8315','NOTIONS AND APPAREL FINDINGS'),
	('8320','PADDING AND STUFFING MATERIALS'),
	('8325','FUR MATERIALS'),
	('8330','LEATHER'),
	('8335','SHOE FINDINGS AND SOLING MATERIALS'),
	('8340','TENTS AND TARPAULINS'),
	('8345','FLAGS AND PENNANTS'),
	('84','CLOTHING/INDIVIDUAL EQPT, INSIGNIA'),
	('8405','OUTERWEAR, MEN''S'),
	('8410','OUTERWEAR, WOMEN''S'),
	('8415','CLOTHING, SPECIAL PURPOSE'),
	('8420','UNDERWEAR AND NIGHTWEAR, MEN''S'),
	('8425','UNDERWEAR AND NIGHTWEAR, WOMEN''S'),
	('8430','FOOTWEAR, MEN''S'),
	('8435','FOOTWEAR, WOMEN''S'),
	('8440','HOSIERY, HANDWEAR, AND CLOTHING ACCESSORIES, MEN''S'),
	('8445','HOSIERY, HANDWEAR, AND CLOTHING ACCESSORIES, WOMEN''S'),
	('8450','CHILDREN''S AND INFANTS'' APPAREL AND ACCESSORIES'),
	('8455','BADGES AND INSIGNIA'),
	('8460','LUGGAGE'),
	('8465','INDIVIDUAL EQUIPMENT'),
	('8470','ARMOR, PERSONAL'),
	('8475','SPECIALIZED FLIGHT CLOTHING AND ACCESSORIES'),
	('85','TOILETRIES'),
	('8510','PERFUMES, TOILET PREPARATIONS, AND POWDERS'),
	('8520','TOILET SOAP, SHAVING PREPARATIONS, AND DENTIFRICES'),
	('8530','PERSONAL TOILETRY ARTICLES'),
	('8540','TOILETRY PAPER PRODUCTS'),
	('87','AGRICULTURAL SUPPLIES'),
	('8710','FORAGE AND FEED'),
	('8720','FERTILIZERS'),
	('8730','SEEDS AND NURSERY STOCK'),
	('88','LIVE ANIMALS'),
	('8810','LIVE ANIMALS, RAISED FOR FOOD'),
	('8820','LIVE ANIMALS, NOT RAISED FOR FOOD'),
	('89','SUBSISTENCE'),
	('8905','MEAT, POULTRY, AND FISH'),
	('8910','DAIRY FOODS AND EGGS'),
	('8915','FRUITS AND VEGETABLES'),
	('8920','BAKERY AND CEREAL PRODUCTS'),
	('8925','SUGAR, CONFECTIONERY, AND NUTS'),
	('8930','JAMS, JELLIES, AND PRESERVES'),
	('8935','SOUPS AND BOUILLONS'),
	('8940','SPECIAL DIETARY FOODS AND FOOD SPECIALTY PREPARATIONS'),
	('8945','FOOD, OILS AND FATS'),
	('8950','CONDIMENTS AND RELATED PRODUCTS'),
	('8955','COFFEE, TEA, AND COCOA'),
	('8960','BEVERAGES, NONALCOHOLIC'),
	('8965','BEVERAGES, ALCOHOLIC'),
	('8970','COMPOSITE FOOD PACKAGES'),
	('8975','TOBACCO PRODUCTS'),
	('91','FUELS, LUBRICANTS, OILS, WAXES'),
	('9110','FUELS, SOLID'),
	('9130','LIQUID PROPELLANTS AND FUELS, PETROLEUM BASE'),
	('9135','LIQUID PROPELLANT FUELS AND OXIDIZERS, CHEMICAL BASE'),
	('9140','FUEL OILS'),
	('9150','OILS AND GREASES: CUTTING, LUBRICATING, AND HYDRAULIC'),
	('9160','MISCELLANEOUS WAXES, OILS, AND FATS'),
	('93','NONMETALLIC FABRICATED MATERIALS'),
	('9310','PAPER AND PAPERBOARD'),
	('9320','RUBBER FABRICATED MATERIALS'),
	('9330','PLASTICS FABRICATED MATERIALS'),
	('9340','GLASS FABRICATED MATERIALS'),
	('9350','REFRACTORIES AND FIRE SURFACING MATERIALS'),
	('9390','MISCELLANEOUS FABRICATED NONMETALLIC MATERIALS'),
	('94','NONMETALLIC CRUDE MATERIALS'),
	('9410','CRUDE GRADES OF PLANT MATERIALS'),
	('9420','FIBERS:  VEGETABLE, ANIMAL, AND SYNTHETIC'),
	('9430','MISCELLANEOUS CRUDE ANIMAL PRODUCTS, INEDIBLE'),
	('9440','MISCELLANEOUS CRUDE AGRICULTURAL AND FORESTRY PRODUCTS'),
	('9450','NONMETALLIC SCRAP, EXCEPT TEXTILE'),
	('95','METAL BARS, SHEETS, SHAPES'),
	('9505','WIRE, NONELECTRICAL'),
	('9510','BARS AND RODS'),
	('9515','PLATE, SHEET, STRIP, FOIL, AND LEAF'),
	('9520','STRUCTURAL SHAPES'),
	('9525','WIRE, NONELECTRICAL, NONFERROUS BASE METAL'),
	('9530','BARS AND RODS, NONFERROUS BASE METAL'),
	('9535','PLATE, SHEET, STRIP, AND FOIL; NONFERROUS BASE METAL'),
	('9540','STRUCTURAL SHAPES, NONFERROUS BASE METAL'),
	('9545','PLATE, SHEET, STRIP, FOIL, AND WIRE: PRECIOUS METAL'),
	('96','ORES, MINERALS AND PRIMARY PRODUCTS'),
	('9610','ORES'),
	('9620','MINERALS, NATURAL AND SYNTHETIC'),
	('9630','ADDITIVE METAL MATERIALS'),
	('9640','IRON AND STEEL PRIMARY AND SEMIFINISHED PRODUCTS'),
	('9650','NONFERROUS BASE METAL REFINERY AND INTERMEDIATE FORMS'),
	('9660','PRECIOUS METALS PRIMARY FORMS'),
	('9670','IRON AND STEEL SCRAP'),
	('9680','NONFERROUS SCRAP'),
	('99','MISCELLANEOUS'),
	('9905','SIGNS, ADVERTISING DISPLAYS, AND IDENTIFICATION PLATES'),
	('9910','JEWELRY'),
	('9915','COLLECTORS'' AND/OR HISTORICAL ITEMS'),
	('9920','SMOKERS'' ARTICLES AND MATCHES'),
	('9925','ECCLESIASTICAL EQUIPMENT, FURNISHINGS, AND SUPPLIES'),
	('9930','MEMORIALS; CEMETERIAL AND MORTUARY EQUIPMENT AND SUPPLIES'),
	('9999','MISCELLANEOUS ITEMS'),
	('A','RESEARCH AND DEVELOPMENT'),
	('AA','AGRICULTURE RandD'),
	('AA11','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (BASIC RESEARCH)'),
	('AA12','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AA13','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (ADVANCED DEVELOPMENT)'),
	('AA14','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (ENGINEERING DEVELOPMENT)'),
	('AA15','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AA16','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (MANAGEMENT/SUPPORT)'),
	('AA17','R&D- AGRICULTURE: INSECT AND DISEASE CONTROL (COMMERCIALIZED)'),
	('AA21','R&D- AGRICULTURE: MARKETING (BASIC RESEARCH)'),
	('AA22','R&D- AGRICULTURE: MARKETING (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AA23','R&D- AGRICULTURE: MARKETING (ADVANCED DEVELOPMENT)'),
	('AA24','R&D- AGRICULTURE: MARKETING (ENGINEERING DEVELOPMENT)'),
	('AA25','R&D- AGRICULTURE: MARKETING (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AA26','R&D- AGRICULTURE: MARKETING (MANAGEMENT/SUPPORT)'),
	('AA27','R&D- AGRICULTURE: MARKETING (COMMERCIALIZED)'),
	('AA31','R&D- AGRICULTURE: PRODUCTION (BASIC RESEARCH)'),
	('AA32','R&D- AGRICULTURE: PRODUCTION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AA33','R&D- AGRICULTURE: PRODUCTION (ADVANCED DEVELOPMENT)'),
	('AA34','R&D- AGRICULTURE: PRODUCTION (ENGINEERING DEVELOPMENT)'),
	('AA35','R&D- AGRICULTURE: PRODUCTION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AA36','R&D- AGRICULTURE: PRODUCTION (MANAGEMENT/SUPPORT)'),
	('AA37','R&D- AGRICULTURE: PRODUCTION (COMMERCIALIZED)'),
	('AA91','R&D- AGRICULTURE: OTHER (BASIC RESEARCH)'),
	('AA92','R&D- AGRICULTURE: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AA93','R&D- AGRICULTURE: OTHER (ADVANCED DEVELOPMENT)'),
	('AA94','R&D- AGRICULTURE: OTHER (ENGINEERING DEVELOPMENT)'),
	('AA95','R&D- AGRICULTURE: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AA96','R&D- AGRICULTURE: OTHER (MANAGEMENT/SUPPORT)'),
	('AA97','R&D- AGRICULTURE: OTHER (COMMERCIALIZED)'),
	('AB','COMMUNITY SERVICE/DEV. RandD'),
	('AB11','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (BASIC RESEARCH)'),
	('AB12','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AB13','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (ADVANCED DEVELOPMENT)'),
	('AB14','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (ENGINEERING DEVELOPMENT)'),
	('AB15','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AB16','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (MANAGEMENT/SUPPORT)'),
	('AB17','R&D- COMMUNITY SVC/DEVELOP: CRIME PREVENTION/CONTROL (COMMERCIALIZED)'),
	('AB21','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (BASIC RESEARCH)'),
	('AB22','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AB23','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (ADVANCED DEVELOPMENT)'),
	('AB24','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (ENGINEERING DEVELOPMENT)'),
	('AB25','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AB26','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (MANAGEMENT/SUPPORT)'),
	('AB27','R&D- COMMUNITY SVC/DEVELOP: FIRE PREVENTION/CONTROL (COMMERCIALIZED)'),
	('AB31','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (BASIC RESEARCH)'),
	('AB32','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AB33','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (ADVANCED DEVELOPMENT)'),
	('AB34','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (ENGINEERING DEVELOPMENT)'),
	('AB35','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AB36','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (MANAGEMENT/SUPPORT)'),
	('AB37','R&D- COMMUNITY SERVICE/DEVELOPMENT: RURAL (COMMERCIALIZED)'),
	('AB41','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (BASIC RESEARCH)'),
	('AB42','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AB43','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (ADVANCED DEVELOPMENT)'),
	('AB44','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (ENGINEERING DEVELOPMENT)'),
	('AB45','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AB46','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (MANAGEMENT/SUPPORT)'),
	('AB47','R&D- COMMUNITY SERVICE/DEVELOPMENT: URBAN (COMMERCIALIZED)'),
	('AB91','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (BASIC RESEARCH)'),
	('AB92','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AB93','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (ADVANCED DEVELOPMENT)'),
	('AB94','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (ENGINEERING DEVELOPMENT)'),
	('AB95','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AB96','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (MANAGEMENT/SUPPORT)'),
	('AB97','R&D- COMMUNITY SERVICE/DEVELOPMENT: OTHER (COMMERCIALIZED)'),
	('AC','DEFENSE SYSTEMS RandD'),
	('AC11','R&D- DEFENSE SYSTEM: AIRCRAFT (BASIC RESEARCH)'),
	('AC12','R&D- DEFENSE SYSTEM: AIRCRAFT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC13','R&D- DEFENSE SYSTEM: AIRCRAFT (ADVANCED DEVELOPMENT)'),
	('AC14','R&D- DEFENSE SYSTEM: AIRCRAFT (ENGINEERING DEVELOPMENT)'),
	('AC15','R&D- DEFENSE SYSTEM: AIRCRAFT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC16','R&D- DEFENSE SYSTEM: AIRCRAFT (MANAGEMENT/SUPPORT)'),
	('AC17','R&D- DEFENSE SYSTEM: AIRCRAFT (COMMERCIALIZED)'),
	('AC21','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (BASIC RESEARCH)'),
	('AC22','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC23','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (ADVANCED DEVELOPMENT)'),
	('AC24','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (ENGINEERING DEVELOPMENT)'),
	('AC25','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC26','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (MANAGEMENT/SUPPORT)'),
	('AC27','R&D- DEFENSE SYSTEM: MISSILE/SPACE SYSTEMS (COMMERCIALIZED)'),
	('AC31','R&D- DEFENSE SYSTEM: SHIPS (BASIC RESEARCH)'),
	('AC32','R&D- DEFENSE SYSTEM: SHIPS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC33','R&D- DEFENSE SYSTEM: SHIPS (ADVANCED DEVELOPMENT)'),
	('AC34','R&D- DEFENSE SYSTEM: SHIPS (ENGINEERING DEVELOPMENT)'),
	('AC35','R&D- DEFENSE SYSTEM: SHIPS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC36','R&D- DEFENSE SYSTEM: SHIPS (MANAGEMENT/SUPPORT)'),
	('AC37','R&D- DEFENSE SYSTEM: SHIPS (COMMERCIALIZED)'),
	('AC41','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (BASIC RESEARCH)'),
	('AC42','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC43','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (ADVANCED DEVELOPMENT)'),
	('AC44','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (ENGINEERING DEVELOPMENT)'),
	('AC45','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC46','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (MANAGEMENT/SUPPORT)'),
	('AC47','R&D- DEFENSE SYSTEM: TANK/AUTOMOTIVE (COMMERCIALIZED)'),
	('AC51','R&D- DEFENSE SYSTEM: WEAPONS (BASIC RESEARCH)'),
	('AC52','R&D- DEFENSE SYSTEM: WEAPONS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC53','R&D- DEFENSE SYSTEM: WEAPONS (ADVANCED DEVELOPMENT)'),
	('AC54','R&D- DEFENSE SYSTEM: WEAPONS (ENGINEERING DEVELOPMENT)'),
	('AC55','R&D- DEFENSE SYSTEM: WEAPONS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC56','R&D- DEFENSE SYSTEM: WEAPONS (MANAGEMENT/SUPPORT)'),
	('AC57','R&D- DEFENSE SYSTEM: WEAPONS (COMMERCIALIZED)'),
	('AC61','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (BASIC RESEARCH)'),
	('AC62','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC63','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (ADVANCED DEVELOPMENT)'),
	('AC64','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (ENGINEERING DEVELOPMENT)'),
	('AC65','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC66','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (MANAGEMENT/SUPPORT)'),
	('AC67','R&D- DEFENSE SYSTEM: ELECTRONICS/COMMUNICATION EQUIPMENT (COMMERCIALIZED)'),
	('AC91','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (BASIC RESEARCH)'),
	('AC92','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AC93','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (ADVANCED DEVELOPMENT)'),
	('AC94','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (ENGINEERING DEVELOPMENT)'),
	('AC95','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AC96','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (MANAGEMENT/SUPPORT)'),
	('AC97','R&D- DEFENSE SYSTEM: MISCELLANEOUS HARD GOODS (COMMERCIALIZED)'),
	('AD','DEFENSE (OTHER) RandD'),
	('AD11','R&D- DEFENSE OTHER: AMMUNITION (BASIC RESEARCH)'),
	('AD12','R&D- DEFENSE OTHER: AMMUNITION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD13','R&D- DEFENSE OTHER: AMMUNITION (ADVANCED DEVELOPMENT)'),
	('AD14','R&D- DEFENSE OTHER: AMMUNITION (ENGINEERING DEVELOPMENT)'),
	('AD15','R&D- DEFENSE OTHER: AMMUNITION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD16','R&D- DEFENSE OTHER: AMMUNITION (MANAGEMENT/SUPPORT)'),
	('AD17','R&D- DEFENSE OTHER: AMMUNITION (COMMERCIALIZED)'),
	('AD21','R&D- DEFENSE OTHER: SERVICES (BASIC RESEARCH)'),
	('AD22','R&D- DEFENSE OTHER: SERVICES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD23','R&D- DEFENSE OTHER: SERVICES (ADVANCED DEVELOPMENT)'),
	('AD24','R&D- DEFENSE OTHER: SERVICES (ENGINEERING DEVELOPMENT)'),
	('AD25','R&D- DEFENSE OTHER: SERVICES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD26','R&D- DEFENSE OTHER: SERVICES (MANAGEMENT/SUPPORT)'),
	('AD27','R&D- DEFENSE OTHER: SERVICES (COMMERCIALIZED)'),
	('AD31','R&D- DEFENSE OTHER: SUBSISTENCE (BASIC RESEARCH)'),
	('AD32','R&D- DEFENSE OTHER: SUBSISTENCE (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD33','R&D- DEFENSE OTHER: SUBSISTENCE (ADVANCED DEVELOPMENT)'),
	('AD34','R&D- DEFENSE OTHER: SUBSISTENCE (ENGINEERING DEVELOPMENT)'),
	('AD35','R&D- DEFENSE OTHER: SUBSISTENCE (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD36','R&D- DEFENSE OTHER: SUBSISTENCE (MANAGEMENT/SUPPORT)'),
	('AD37','R&D- DEFENSE OTHER: SUBSISTENCE (COMMERCIALIZED)'),
	('AD41','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (BASIC RESEARCH)'),
	('AD42','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD43','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (ADVANCED DEVELOPMENT)'),
	('AD44','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (ENGINEERING DEVELOPMENT)'),
	('AD45','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD46','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (MANAGEMENT/SUPPORT)'),
	('AD47','R&D- DEFENSE OTHER: TEXTILES/CLOTHING/EQUIPAGE (COMMERCIALIZED)'),
	('AD51','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (BASIC RESEARCH)'),
	('AD52','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD53','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (ADVANCED DEVELOPMENT)'),
	('AD54','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (ENGINEERING DEVELOPMENT)'),
	('AD55','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD56','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (MANAGEMENT/SUPPORT)'),
	('AD57','R&D- DEFENSE OTHER: FUELS/LUBRICANTS (COMMERCIALIZED)'),
	('AD61','R&D- DEFENSE OTHER: CONSTRUCTION (BASIC RESEARCH)'),
	('AD62','R&D- DEFENSE OTHER: CONSTRUCTION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD63','R&D- DEFENSE OTHER: CONSTRUCTION (ADVANCED DEVELOPMENT)'),
	('AD64','R&D- DEFENSE OTHER: CONSTRUCTION (ENGINEERING DEVELOPMENT)'),
	('AD65','R&D- DEFENSE OTHER: CONSTRUCTION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD66','R&D- DEFENSE OTHER: CONSTRUCTION (MANAGEMENT/SUPPORT)'),
	('AD67','R&D- DEFENSE OTHER: CONSTRUCTION (COMMERCIALIZED)'),
	('AD91','R&D- DEFENSE OTHER: OTHER (BASIC RESEARCH)'),
	('AD92','R&D- DEFENSE OTHER: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AD93','R&D- DEFENSE OTHER: OTHER (ADVANCED DEVELOPMENT)'),
	('AD94','R&D- DEFENSE OTHER: OTHER (ENGINEERING DEVELOPMENT)'),
	('AD95','R&D- DEFENSE OTHER: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AD96','R&D- DEFENSE OTHER: OTHER (MANAGEMENT/SUPPORT)'),
	('AD97','R&D- DEFENSE OTHER: OTHER (COMMERCIALIZED)'),
	('AE','ECONOMIC GROWTH/PRODUCTIVITY RandD'),
	('AE11','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (BASIC RESEARCH)'),
	('AE12','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AE13','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (ADVANCED DEVELOPMENT)'),
	('AE14','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (ENGINEERING DEVELOPMENT)'),
	('AE15','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AE16','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (MANAGEMENT/SUPPORT)'),
	('AE17','R&D- ECONOMIC GROWTH: EMPLOYMENT GROWTH/PRODUCTIVITY (COMMERCIALIZED)'),
	('AE21','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (BASIC RESEARCH)'),
	('AE22','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AE23','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (ADVANCED DEVELOPMENT)'),
	('AE24','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (ENGINEERING DEVELOPMENT)'),
	('AE25','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AE26','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (MANAGEMENT/SUPPORT)'),
	('AE27','R&D- ECONOMIC GROWTH: PRODUCT/SERVICE IMPROVEMENT (COMMERCIALIZED)'),
	('AE31','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (BASIC RESEARCH)'),
	('AE32','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AE33','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (ADVANCED DEVELOPMENT)'),
	('AE34','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (ENGINEERING DEVELOPMENT)'),
	('AE35','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AE36','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (MANAGEMENT/SUPPORT)'),
	('AE37','R&D- ECONOMIC GROWTH: MANUFACTURING TECHNOLOGY (COMMERCIALIZED)'),
	('AE91','R&D- ECONOMIC GROWTH: OTHER (BASIC RESEARCH)'),
	('AE92','R&D- ECONOMIC GROWTH: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AE93','R&D- ECONOMIC GROWTH: OTHER (ADVANCED DEVELOPMENT)'),
	('AE94','R&D- ECONOMIC GROWTH: OTHER (ENGINEERING DEVELOPMENT)'),
	('AE95','R&D- ECONOMIC GROWTH: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AE96','R&D- ECONOMIC GROWTH: OTHER (MANAGEMENT/SUPPORT)'),
	('AE97','R&D- ECONOMIC GROWTH: OTHER (COMMERCIALIZED)'),
	('AF','EDUCATION RandD'),
	('AF11','R&D- EDUCATION: EDUCATIONAL (BASIC RESEARCH)'),
	('AF12','R&D- EDUCATION: EDUCATIONAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AF13','R&D- EDUCATION: EDUCATIONAL (ADVANCED DEVELOPMENT)'),
	('AF14','R&D- EDUCATION: EDUCATIONAL (ENGINEERING DEVELOPMENT)'),
	('AF15','R&D- EDUCATION: EDUCATIONAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AF16','R&D- EDUCATION: EDUCATIONAL (MANAGEMENT/SUPPORT)'),
	('AF17','R&D- EDUCATION: EDUCATIONAL (COMMERCIALIZED)'),
	('AG','ENERGY RandD'),
	('AG11','R&D- ENERGY: COAL (BASIC RESEARCH)'),
	('AG12','R&D- ENERGY: COAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG13','R&D- ENERGY: COAL (ADVANCED DEVELOPMENT)'),
	('AG14','R&D- ENERGY: COAL (ENGINEERING DEVELOPMENT)'),
	('AG15','R&D- ENERGY: COAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG16','R&D- ENERGY: COAL (MANAGEMENT/SUPPORT)'),
	('AG17','R&D- ENERGY: COAL (COMMERCIALIZED)'),
	('AG21','R&D- ENERGY: GAS (BASIC RESEARCH)'),
	('AG22','R&D- ENERGY: GAS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG23','R&D- ENERGY: GAS (ADVANCED DEVELOPMENT)'),
	('AG24','R&D- ENERGY: GAS (ENGINEERING DEVELOPMENT)'),
	('AG25','R&D- ENERGY: GAS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG26','R&D- ENERGY: GAS (MANAGEMENT/SUPPORT)'),
	('AG27','R&D- ENERGY: GAS (COMMERCIALIZED)'),
	('AG31','R&D- ENERGY: GEOTHERMAL (BASIC RESEARCH)'),
	('AG32','R&D- ENERGY: GEOTHERMAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG33','R&D- ENERGY: GEOTHERMAL (ADVANCED DEVELOPMENT)'),
	('AG34','R&D- ENERGY: GEOTHERMAL (ENGINEERING DEVELOPMENT)'),
	('AG35','R&D- ENERGY: GEOTHERMAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG36','R&D- ENERGY: GEOTHERMAL (MANAGEMENT/SUPPORT)'),
	('AG37','R&D- ENERGY: GEOTHERMAL (COMMERCIALIZED)'),
	('AG41','R&D- ENERGY: WIND (BASIC RESEARCH)'),
	('AG42','R&D- ENERGY: WIND (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG43','R&D- ENERGY: WIND (ADVANCED DEVELOPMENT)'),
	('AG44','R&D- ENERGY: WIND (ENGINEERING DEVELOPMENT)'),
	('AG45','R&D- ENERGY: WIND (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG46','R&D- ENERGY: WIND (MANAGEMENT/SUPPORT)'),
	('AG47','R&D- ENERGY: WIND (COMMERCIALIZED)'),
	('AG51','R&D- ENERGY: NUCLEAR (BASIC RESEARCH)'),
	('AG52','R&D- ENERGY: NUCLEAR (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG53','R&D- ENERGY: NUCLEAR (ADVANCED DEVELOPMENT)'),
	('AG54','R&D- ENERGY: NUCLEAR (ENGINEERING DEVELOPMENT)'),
	('AG55','R&D- ENERGY: NUCLEAR (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG56','R&D- ENERGY: NUCLEAR (MANAGEMENT/SUPPORT)'),
	('AG57','R&D- ENERGY: NUCLEAR (COMMERCIALIZED)'),
	('AG61','R&D- ENERGY: PETROLEUM (BASIC RESEARCH)'),
	('AG62','R&D- ENERGY: PETROLEUM (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG63','R&D- ENERGY: PETROLEUM (ADVANCED DEVELOPMENT)'),
	('AG64','R&D- ENERGY: PETROLEUM (ENGINEERING DEVELOPMENT)'),
	('AG65','R&D- ENERGY: PETROLEUM (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG66','R&D- ENERGY: PETROLEUM (MANAGEMENT/SUPPORT)'),
	('AG67','R&D- ENERGY: PETROLEUM (COMMERCIALIZED)'),
	('AG71','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (BASIC RESEARCH)'),
	('AG72','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG73','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (ADVANCED DEVELOPMENT)'),
	('AG74','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (ENGINEERING DEVELOPMENT)'),
	('AG75','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG76','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (MANAGEMENT/SUPPORT)'),
	('AG77','R&D- ENERGY: SOLAR/PHOTOVOLTAIC (COMMERCIALIZED)'),
	('AG81','R&D- ENERGY: CONSERVATION (BASIC RESEARCH)'),
	('AG82','R&D- ENERGY: CONSERVATION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG83','R&D- ENERGY: CONSERVATION (ADVANCED DEVELOPMENT)'),
	('AG84','R&D- ENERGY: CONSERVATION (ENGINEERING DEVELOPMENT)'),
	('AG85','R&D- ENERGY: CONSERVATION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG86','R&D- ENERGY: CONSERVATION (MANAGEMENT/SUPPORT)'),
	('AG87','R&D- ENERGY: CONSERVATION (COMMERCIALIZED)'),
	('AG91','R&D- ENERGY: OTHER (BASIC RESEARCH)'),
	('AG92','R&D- ENERGY: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AG93','R&D- ENERGY: OTHER (ADVANCED DEVELOPMENT)'),
	('AG94','R&D- ENERGY: OTHER (ENGINEERING DEVELOPMENT)'),
	('AG95','R&D- ENERGY: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AG96','R&D- ENERGY: OTHER (MANAGEMENT/SUPPORT)'),
	('AG97','R&D- ENERGY: OTHER (COMMERCIALIZED)'),
	('AH','ENVIRONMENTAL PROTECTION RandD'),
	('AH11','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (BASIC RESEARCH)'),
	('AH12','R&D- ENVIRON PROTECTION: POLLUTION CONTROL/ABATEMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AH13','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (ADVANCED DEVELOPMENT)'),
	('AH14','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (ENGINEERING DEVELOPMENT)'),
	('AH15','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AH16','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (MANAGEMENT/SUPPORT)'),
	('AH17','R&D- ENVIRONMENTAL PROTECTION: POLLUTION CONTROL/ABATEMENT (COMMERCIALIZED)')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('AH21','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (BASIC RESEARCH)'),
	('AH22','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AH23','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (ADVANCED DEVELOPMENT)'),
	('AH24','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (ENGINEERING DEVELOPMENT)'),
	('AH25','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AH26','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (MANAGEMENT/SUPPORT)'),
	('AH27','R&D- ENVIRONMENTAL PROTECTION: AIR POLLUTION (COMMERCIALIZED)'),
	('AH31','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (BASIC RESEARCH)'),
	('AH32','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AH33','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (ADVANCED DEVELOPMENT)'),
	('AH34','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (ENGINEERING DEVELOPMENT)'),
	('AH35','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AH36','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (MANAGEMENT/SUPPORT)'),
	('AH37','R&D- ENVIRONMENTAL PROTECTION: WATER POLLUTION (COMMERCIALIZED)'),
	('AH41','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (BASIC RESEARCH)'),
	('AH42','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AH43','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (ADVANCED DEVELOPMENT)'),
	('AH44','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (ENGINEERING DEVELOPMENT)'),
	('AH45','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AH46','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (MANAGEMENT/SUPPORT)'),
	('AH47','R&D- ENVIRONMENTAL PROTECTION: NOISE POLLUTION (COMMERCIALIZED)'),
	('AH91','R&D- ENVIRONMENTAL PROTECTION: OTHER (BASIC RESEARCH)'),
	('AH92','R&D- ENVIRONMENTAL PROTECTION: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AH93','R&D- ENVIRONMENTAL PROTECTION: OTHER (ADVANCED DEVELOPMENT)'),
	('AH94','R&D- ENVIRONMENTAL PROTECTION: OTHER (ENGINEERING DEVELOPMENT)'),
	('AH95','R&D- ENVIRONMENTAL PROTECTION: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AH96','R&D- ENVIRONMENTAL PROTECTION: OTHER (MANAGEMENT/SUPPORT)'),
	('AH97','R&D- ENVIRONMENTAL PROTECTION: OTHER (COMMERCIALIZED)'),
	('AJ','GEN. SCIENCE/TECHNOLOGY RandD'),
	('AJ11','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (BASIC RESEARCH)'),
	('AJ12','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ13','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ14','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (ENGINEERING DEVELOPMENT)'),
	('AJ15','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ16','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ17','R&D- GENERAL SCIENCE/TECHNOLOGY: PHYSICAL SCIENCES (COMMERCIALIZED)'),
	('AJ21','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (BASIC RESEARCH)'),
	('AJ22','R&D- GENERAL SCI/TECH: MATHEMATICAL/COMPUTER SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ23','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ24','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (ENGINEERING DEVELOPMENT)'),
	('AJ25','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ26','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ27','R&D- GENERAL SCIENCE/TECHNOLOGY: MATHEMATICAL/COMPUTER SCIENCES (COMMERCIALIZED)'),
	('AJ31','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (BASIC RESEARCH)'),
	('AJ32','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ33','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ34','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES  (ENGINEERING DEVELOPMENT)'),
	('AJ35','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ36','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ37','R&D- GENERAL SCIENCE/TECHNOLOGY: ENVIRONMENTAL SCIENCES (COMMERCIALIZED)'),
	('AJ41','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (BASIC RESEARCH)'),
	('AJ42','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ43','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (ADVANCED DEVELOPMENT)'),
	('AJ44','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (ENGINEERING DEVELOPMENT)'),
	('AJ45','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ46','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (MANAGEMENT/SUPPORT)'),
	('AJ47','R&D- GENERAL SCIENCE/TECHNOLOGY: ENGINEERING (COMMERCIALIZED)'),
	('AJ51','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (BASIC RESEARCH)'),
	('AJ52','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ53','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ54','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (ENGINEERING DEVELOPMENT)'),
	('AJ55','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ56','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ57','R&D- GENERAL SCIENCE/TECHNOLOGY: LIFE SCIENCES (COMMERCIALIZED)'),
	('AJ61','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (BASIC RESEARCH)'),
	('AJ62','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ63','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ64','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (ENGINEERING DEVELOPMENT)'),
	('AJ65','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ66','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ67','R&D- GENERAL SCIENCE/TECHNOLOGY: PSYCHOLOGICAL SCIENCES (COMMERCIALIZED)'),
	('AJ71','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (BASIC RESEARCH)'),
	('AJ72','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ73','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (ADVANCED DEVELOPMENT)'),
	('AJ74','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (ENGINEERING DEVELOPMENT)'),
	('AJ75','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ76','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (MANAGEMENT/SUPPORT)'),
	('AJ77','R&D- GENERAL SCIENCE/TECHNOLOGY: SOCIAL SCIENCES (COMMERCIALIZED)'),
	('AJ91','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (BASIC RESEARCH)'),
	('AJ92','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AJ93','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (ADVANCED DEVELOPMENT)'),
	('AJ94','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (ENGINEERING DEVELOPMENT)'),
	('AJ95','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AJ96','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (MANAGEMENT/SUPPORT)'),
	('AJ97','R&D- GENERAL SCIENCE/TECHNOLOGY: OTHER (COMMERCIALIZED)'),
	('AK','HOUSING RandD'),
	('AK11','R&D- HOUSING: HOUSING (BASIC RESEARCH)'),
	('AK12','R&D- HOUSING: HOUSING (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AK13','R&D- HOUSING: HOUSING (ADVANCED DEVELOPMENT)'),
	('AK14','R&D- HOUSING: HOUSING (ENGINEERING DEVELOPMENT)'),
	('AK15','R&D- HOUSING: HOUSING (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AK16','R&D- HOUSING: HOUSING (MANAGEMENT/SUPPORT)'),
	('AK17','R&D- HOUSING: HOUSING (COMMERCIALIZED)'),
	('AL','INCOME SECURITY RandD'),
	('AL11','R&D- INCOME SECURITY: EMPLOYMENT (BASIC RESEARCH)'),
	('AL12','R&D- INCOME SECURITY: EMPLOYMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AL13','R&D- INCOME SECURITY: EMPLOYMENT (ADVANCED DEVELOPMENT)'),
	('AL14','R&D- INCOME SECURITY: EMPLOYMENT (ENGINEERING DEVELOPMENT)'),
	('AL15','R&D- INCOME SECURITY: EMPLOYMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AL16','R&D- INCOME SECURITY: EMPLOYMENT (MANAGEMENT/SUPPORT)'),
	('AL17','R&D- INCOME SECURITY: EMPLOYMENT (COMMERCIALIZED)'),
	('AL21','R&D- INCOME SECURITY: INCOME MAINTENANCE (BASIC RESEARCH)'),
	('AL22','R&D- INCOME SECURITY: INCOME MAINTENANCE (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AL23','R&D- INCOME SECURITY: INCOME MAINTENANCE (ADVANCED DEVELOPMENT)'),
	('AL24','R&D- INCOME SECURITY: INCOME MAINTENANCE (ENGINEERING DEVELOPMENT)'),
	('AL25','R&D- INCOME SECURITY: INCOME MAINTENANCE (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AL26','R&D- INCOME SECURITY: INCOME MAINTENANCE (MANAGEMENT/SUPPORT)'),
	('AL27','R&D- INCOME SECURITY: INCOME MAINTENANCE (COMMERCIALIZED)'),
	('AL91','R&D- INCOME SECURITY: OTHER (BASIC RESEARCH)'),
	('AL92','R&D- INCOME SECURITY: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AL93','R&D- INCOME SECURITY: OTHER (ADVANCED DEVELOPMENT)'),
	('AL94','R&D- INCOME SECURITY: OTHER (ENGINEERING DEVELOPMENT)'),
	('AL95','R&D- INCOME SECURITY: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AL96','R&D- INCOME SECURITY: OTHER (MANAGEMENT/SUPPORT)'),
	('AL97','R&D- INCOME SECURITY: OTHER (COMMERCIALIZED)'),
	('AM','INTERNATIONAL AFFAIR/COOPERAT RandD'),
	('AM11','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (BASIC RESEARCH)'),
	('AM12','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AM13','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (ADVANCED DEVELOPMENT)'),
	('AM14','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (ENGINEERING DEVELOPMENT)'),
	('AM15','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AM16','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (MANAGEMENT/SUPPORT)'),
	('AM17','R&D- INTERNATIONAL AFFAIRS AND COOPERATION (COMMERCIALIZED)'),
	('AN','MEDICAL RandD'),
	('AN11','R&D- MEDICAL: BIOMEDICAL (BASIC RESEARCH)'),
	('AN12','R&D- MEDICAL: BIOMEDICAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN13','R&D- MEDICAL: BIOMEDICAL (ADVANCED DEVELOPMENT)'),
	('AN14','R&D- MEDICAL: BIOMEDICAL (ENGINEERING DEVELOPMENT)'),
	('AN15','R&D- MEDICAL: BIOMEDICAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN16','R&D- MEDICAL: BIOMEDICAL (MANAGEMENT/SUPPORT)'),
	('AN17','R&D- MEDICAL: BIOMEDICAL (COMMERCIALIZED)'),
	('AN21','R&D- MEDICAL: DRUG DEPENDENCY (BASIC RESEARCH)'),
	('AN22','R&D- MEDICAL: DRUG DEPENDENCY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN23','R&D- MEDICAL: DRUG DEPENDENCY (ADVANCED DEVELOPMENT)'),
	('AN24','R&D- MEDICAL: DRUG DEPENDENCY (ENGINEERING DEVELOPMENT)'),
	('AN25','R&D- MEDICAL: DRUG DEPENDENCY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN26','R&D- MEDICAL: DRUG DEPENDENCY (MANAGEMENT/SUPPORT)'),
	('AN27','R&D- MEDICAL: DRUG DEPENDENCY (COMMERCIALIZED)'),
	('AN31','R&D- MEDICAL: ALCOHOL DEPENDENCY (BASIC RESEARCH)'),
	('AN32','R&D- MEDICAL: ALCOHOL DEPENDENCY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN33','R&D- MEDICAL: ALCOHOL DEPENDENCY (ADVANCED DEVELOPMENT)'),
	('AN34','R&D- MEDICAL: ALCOHOL DEPENDENCY (ENGINEERING DEVELOPMENT)'),
	('AN35','R&D- MEDICAL: ALCOHOL DEPENDENCY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN36','R&D- MEDICAL: ALCOHOL DEPENDENCY (MANAGEMENT/SUPPORT)'),
	('AN37','R&D- MEDICAL: ALCOHOL DEPENDENCY (COMMERCIALIZED)'),
	('AN41','R&D- MEDICAL: HEALTH SERVICES (BASIC RESEARCH)'),
	('AN42','R&D- MEDICAL: HEALTH SERVICES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN43','R&D- MEDICAL: HEALTH SERVICES (ADVANCED DEVELOPMENT)'),
	('AN44','R&D- MEDICAL: HEALTH SERVICES (ENGINEERING DEVELOPMENT)'),
	('AN45','R&D- MEDICAL: HEALTH SERVICES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN46','R&D- MEDICAL: HEALTH SERVICES (MANAGEMENT/SUPPORT)'),
	('AN47','R&D- MEDICAL: HEALTH SERVICES (COMMERCIALIZED)'),
	('AN51','R&D- MEDICAL: MENTAL HEALTH (BASIC RESEARCH)'),
	('AN52','R&D- MEDICAL: MENTAL HEALTH (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN53','R&D- MEDICAL: MENTAL HEALTH (ADVANCED DEVELOPMENT)'),
	('AN54','R&D- MEDICAL: MENTAL HEALTH (ENGINEERING DEVELOPMENT)'),
	('AN55','R&D- MEDICAL: MENTAL HEALTH (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN56','R&D- MEDICAL: MENTAL HEALTH (MANAGEMENT/SUPPORT)'),
	('AN57','R&D- MEDICAL: MENTAL HEALTH (COMMERCIALIZED)'),
	('AN61','R&D- MEDICAL: REHABILITATIVE ENGINEERING (BASIC RESEARCH)'),
	('AN62','R&D- MEDICAL: REHABILITATIVE ENGINEERING (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN63','R&D- MEDICAL: REHABILITATIVE ENGINEERING (ADVANCED DEVELOPMENT)'),
	('AN64','R&D- MEDICAL: REHABILITATIVE ENGINEERING (ENGINEERING DEVELOPMENT)'),
	('AN65','R&D- MEDICAL: REHABILITATIVE ENGINEERING (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN66','R&D- MEDICAL: REHABILITATIVE ENGINEERING (MANAGEMENT/SUPPORT)'),
	('AN67','R&D- MEDICAL: REHABILITATIVE ENGINEERING (COMMERCIALIZED)'),
	('AN71','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (BASIC RESEARCH)'),
	('AN72','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN73','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (ADVANCED DEVELOPMENT)'),
	('AN74','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (ENGINEERING DEVELOPMENT)'),
	('AN75','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN76','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (MANAGEMENT/SUPPORT)'),
	('AN77','R&D- MEDICAL: SPECIALIZED MEDICAL SERVICES (COMMERCIALIZED)'),
	('AN81','R&D- MEDICAL: AIDS RESEARCH (BASIC RESEARCH)'),
	('AN82','R&D- MEDICAL: AIDS RESEARCH (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN83','R&D- MEDICAL: AIDS RESEARCH (ADVANCED DEVELOPMENT)'),
	('AN84','R&D- MEDICAL: AIDS RESEARCH (ENGINEERING DEVELOPMENT)'),
	('AN85','R&D- MEDICAL: AIDS RESEARCH (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN86','R&D- MEDICAL: AIDS RESEARCH (MANAGEMENT/SUPPORT)'),
	('AN87','R&D- MEDICAL: AIDS RESEARCH (COMMERCIALIZED)'),
	('AN91','R&D- MEDICAL: OTHER (BASIC RESEARCH)'),
	('AN92','R&D- MEDICAL: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AN93','R&D- MEDICAL: OTHER (ADVANCED DEVELOPMENT)'),
	('AN94','R&D- MEDICAL: OTHER (ENGINEERING DEVELOPMENT)'),
	('AN95','R&D- MEDICAL: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AN96','R&D- MEDICAL: OTHER (MANAGEMENT/SUPPORT)'),
	('AN97','R&D- MEDICAL: OTHER (COMMERCIALIZED)'),
	('AP','NATURAL RESOURCES RandD'),
	('AP21','R&D- NATURAL RESOURCE: LAND (BASIC RESEARCH)'),
	('AP22','R&D- NATURAL RESOURCE: LAND (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP23','R&D- NATURAL RESOURCE: LAND (ADVANCED DEVELOPMENT)'),
	('AP24','R&D- NATURAL RESOURCE: LAND (ENGINEERING DEVELOPMENT)'),
	('AP25','R&D- NATURAL RESOURCE: LAND (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP26','R&D- NATURAL RESOURCE: LAND (MANAGEMENT/SUPPORT)'),
	('AP27','R&D- NATURAL RESOURCE: LAND (COMMERCIALIZED)'),
	('AP31','R&D- NATURAL RESOURCE: MINERAL (BASIC RESEARCH)'),
	('AP32','R&D- NATURAL RESOURCE: MINERAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP33','R&D- NATURAL RESOURCE: MINERAL (ADVANCED DEVELOPMENT)'),
	('AP34','R&D- NATURAL RESOURCE: MINERAL (ENGINEERING DEVELOPMENT)'),
	('AP35','R&D- NATURAL RESOURCE: MINERAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP36','R&D- NATURAL RESOURCE: MINERAL (MANAGEMENT/SUPPORT)'),
	('AP37','R&D- NATURAL RESOURCE: MINERAL (COMMERCIALIZED)'),
	('AP41','R&D- NATURAL RESOURCE: RECREATION (BASIC RESEARCH)'),
	('AP42','R&D- NATURAL RESOURCE: RECREATION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP43','R&D- NATURAL RESOURCE: RECREATION (ADVANCED DEVELOPMENT)'),
	('AP44','R&D- NATURAL RESOURCE: RECREATION (ENGINEERING DEVELOPMENT)'),
	('AP45','R&D- NATURAL RESOURCE: RECREATION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP46','R&D- NATURAL RESOURCE: RECREATION (MANAGEMENT/SUPPORT)'),
	('AP47','R&D- NATURAL RESOURCE: RECREATION (COMMERCIALIZED)'),
	('AP51','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (BASIC RESEARCH)'),
	('AP52','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP53','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (ADVANCED DEVELOPMENT)'),
	('AP54','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (ENGINEERING DEVELOPMENT)'),
	('AP55','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP56','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (MANAGEMENT/SUPPORT)'),
	('AP57','R&D- NATURAL RESOURCE: MARINE AND OCEANOGRAPHIC (COMMERCIALIZED)'),
	('AP61','R&D- NATURAL RESOURCE: MARINE FISHERIES (BASIC RESEARCH)'),
	('AP62','R&D- NATURAL RESOURCE: MARINE FISHERIES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP63','R&D- NATURAL RESOURCE: MARINE FISHERIES (ADVANCED DEVELOPMENT)'),
	('AP64','R&D- NATURAL RESOURCE: MARINE FISHERIES (ENGINEERING DEVELOPMENT)'),
	('AP65','R&D- NATURAL RESOURCE: MARINE FISHERIES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP66','R&D- NATURAL RESOURCE: MARINE FISHERIES (MANAGEMENT/SUPPORT)'),
	('AP67','R&D- NATURAL RESOURCE: MARINE FISHERIES (COMMERCIALIZED)'),
	('AP71','R&D- NATURAL RESOURCE: ATMOSPHERIC (BASIC RESEARCH)'),
	('AP72','R&D- NATURAL RESOURCE: ATMOSPHERIC (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP73','R&D- NATURAL RESOURCE: ATMOSPHERIC (ADVANCED DEVELOPMENT)'),
	('AP74','R&D- NATURAL RESOURCE: ATMOSPHERIC (ENGINEERING DEVELOPMENT)'),
	('AP75','R&D- NATURAL RESOURCE: ATMOSPHERIC (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP76','R&D- NATURAL RESOURCE: ATMOSPHERIC (MANAGEMENT/SUPPORT)'),
	('AP77','R&D- NATURAL RESOURCE: ATMOSPHERIC (COMMERCIALIZED)'),
	('AP91','R&D- NATURAL RESOURCE: OTHER (BASIC RESEARCH)'),
	('AP92','R&D- NATURAL RESOURCE: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AP93','R&D- NATURAL RESOURCE: OTHER (ADVANCED DEVELOPMENT)'),
	('AP94','R&D- NATURAL RESOURCE: OTHER (ENGINEERING DEVELOPMENT)'),
	('AP95','R&D- NATURAL RESOURCE: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AP96','R&D- NATURAL RESOURCE: OTHER (MANAGEMENT/SUPPORT)'),
	('AP97','R&D- NATURAL RESOURCE: OTHER (COMMERCIALIZED)'),
	('AQ','SOCIAL SERVICES RandD'),
	('AQ11','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (BASIC RESEARCH)'),
	('AQ12','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AQ13','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (ADVANCED DEVELOPMENT)'),
	('AQ14','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (ENGINEERING DEVELOPMENT)'),
	('AQ15','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AQ16','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (MANAGEMENT/SUPPORT)'),
	('AQ17','R&D- SOCIAL SERVICES: GERIATRIC OTHER THAN MEDICAL (COMMERCIALIZED)'),
	('AQ91','R&D- SOCIAL SERVICES: OTHER (BASIC RESEARCH)'),
	('AQ92','R&D- SOCIAL SERVICES: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AQ93','R&D- SOCIAL SERVICES: OTHER (ADVANCED DEVELOPMENT)'),
	('AQ94','R&D- SOCIAL SERVICES: OTHER (ENGINEERING DEVELOPMENT)'),
	('AQ95','R&D- SOCIAL SERVICES: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AQ96','R&D- SOCIAL SERVICES: OTHER (MANAGEMENT/SUPPORT)'),
	('AQ97','R&D- SOCIAL SERVICES: OTHER (COMMERCIALIZED)'),
	('AR','SPACE RandD'),
	('AR11','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (BASIC RESEARCH)'),
	('AR12','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR13','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (ADVANCED DEVELOPMENT)'),
	('AR14','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (ENGINEERING DEVELOPMENT)'),
	('AR15','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR16','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (MANAGEMENT/SUPPORT)'),
	('AR17','R&D- SPACE: AERONAUTICS/SPACE TECHNOLOGY (COMMERCIALIZED)'),
	('AR21','R&D- SPACE: SCIENCE/APPLICATIONS (BASIC RESEARCH)'),
	('AR22','R&D- SPACE: SCIENCE/APPLICATIONS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR23','R&D- SPACE: SCIENCE/APPLICATIONS (ADVANCED DEVELOPMENT)'),
	('AR24','R&D- SPACE: SCIENCE/APPLICATIONS (ENGINEERING DEVELOPMENT)'),
	('AR25','R&D- SPACE: SCIENCE/APPLICATIONS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR26','R&D- SPACE: SCIENCE/APPLICATIONS (MANAGEMENT/SUPPORT)'),
	('AR27','R&D- SPACE: SCIENCE/APPLICATIONS (COMMERCIALIZED)'),
	('AR31','R&D- SPACE: FLIGHT (BASIC RESEARCH)'),
	('AR32','R&D- SPACE: FLIGHT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR33','R&D- SPACE: FLIGHT (ADVANCED DEVELOPMENT)'),
	('AR34','R&D- SPACE: FLIGHT (ENGINEERING DEVELOPMENT)'),
	('AR35','R&D- SPACE: FLIGHT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR36','R&D- SPACE: FLIGHT (MANAGEMENT/SUPPORT)'),
	('AR37','R&D- SPACE: FLIGHT (COMMERCIALIZED)'),
	('AR41','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (BASIC RESEARCH)'),
	('AR42','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR43','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (ADVANCED DEVELOPMENT)'),
	('AR44','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (ENGINEERING DEVELOPMENT)'),
	('AR45','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR46','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (MANAGEMENT/SUPPORT)'),
	('AR47','R&D- SPACE: OPERATIONS, TRACKING AND DATA ACQUISITION (COMMERCIALIZED)'),
	('AR61','R&D- SPACE: STATION (BASIC RESEARCH)'),
	('AR62','R&D- SPACE: STATION (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR63','R&D- SPACE: STATION (ADVANCED DEVELOPMENT)'),
	('AR64','R&D- SPACE: STATION (ENGINEERING DEVELOPMENT)'),
	('AR65','R&D- SPACE: STATION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR66','R&D- SPACE: STATION (MANAGEMENT/SUPPORT)'),
	('AR67','R&D- SPACE: STATION (COMMERCIALIZED)'),
	('AR71','R&D- SPACE: COMMERCIAL PROGRAMS (BASIC RESEARCH)'),
	('AR72','R&D- SPACE: COMMERCIAL PROGRAMS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR73','R&D- SPACE: COMMERCIAL PROGRAMS (ADVANCED DEVELOPMENT)'),
	('AR74','R&D- SPACE: COMMERCIAL PROGRAMS (ENGINEERING DEVELOPMENT)'),
	('AR75','R&D- SPACE: COMMERCIAL PROGRAMS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR76','R&D- SPACE: COMMERCIAL PROGRAMS (MANAGEMENT/SUPPORT)'),
	('AR77','R&D- SPACE: COMMERCIAL PROGRAMS (COMMERCIALIZED)'),
	('AR91','R&D- SPACE: OTHER (BASIC RESEARCH)'),
	('AR92','R&D- SPACE: OTHER (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AR93','R&D- SPACE: OTHER (ADVANCED DEVELOPMENT)'),
	('AR94','R&D- SPACE: OTHER (ENGINEERING DEVELOPMENT)'),
	('AR95','R&D- SPACE: OTHER (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AR96','R&D- SPACE: OTHER (MANAGEMENT/SUPPORT)'),
	('AR97','R&D- SPACE: OTHER (COMMERCIALIZED)'),
	('AS','TRANSPORTATION (MODAL) RandD'),
	('AS11','R&D- MODAL TRANSPORTATION: AIR (BASIC RESEARCH)'),
	('AS12','R&D- MODAL TRANSPORTATION: AIR (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AS13','R&D- MODAL TRANSPORTATION: AIR (ADVANCED DEVELOPMENT)'),
	('AS14','R&D- MODAL TRANSPORTATION: AIR (ENGINEERING DEVELOPMENT)'),
	('AS15','R&D- MODAL TRANSPORTATION: AIR (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AS16','R&D- MODAL TRANSPORTATION: AIR (MANAGEMENT/SUPPORT)'),
	('AS17','R&D- MODAL TRANSPORTATION: AIR (COMMERCIALIZED)'),
	('AS21','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (BASIC RESEARCH)'),
	('AS22','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AS23','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (ADVANCED DEVELOPMENT)'),
	('AS24','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (ENGINEERING DEVELOPMENT)'),
	('AS25','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AS26','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (MANAGEMENT/SUPPORT)'),
	('AS27','R&D- MODAL TRANSPORTATION: SURFACE MOTOR VEHICLES (COMMERCIALIZED)'),
	('AS31','R&D- MODAL TRANSPORTATION: RAIL (BASIC RESEARCH)'),
	('AS32','R&D- MODAL TRANSPORTATION: RAIL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AS33','R&D- MODAL TRANSPORTATION: RAIL (ADVANCED DEVELOPMENT)'),
	('AS34','R&D- MODAL TRANSPORTATION: RAIL (ENGINEERING DEVELOPMENT)'),
	('AS35','R&D- MODAL TRANSPORTATION: RAIL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AS36','R&D- MODAL TRANSPORTATION: RAIL (MANAGEMENT/SUPPORT)'),
	('AS37','R&D- MODAL TRANSPORTATION: RAIL (COMMERCIALIZED)'),
	('AS41','R&D- MODAL TRANSPORTATION: MARINE (BASIC RESEARCH)'),
	('AS42','R&D- MODAL TRANSPORTATION: MARINE (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AS43','R&D- MODAL TRANSPORTATION: MARINE (ADVANCED DEVELOPMENT)'),
	('AS44','R&D- MODAL TRANSPORTATION: MARINE (ENGINEERING DEVELOPMENT)'),
	('AS45','R&D- MODAL TRANSPORTATION: MARINE (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AS46','R&D- MODAL TRANSPORTATION: MARINE (MANAGEMENT/SUPPORT)'),
	('AS47','R&D- MODAL TRANSPORTATION: MARINE (COMMERCIALIZED)'),
	('AS91','R&D- MODAL TRANSPORTATION: OTHER MODAL (BASIC RESEARCH)'),
	('AS92','R&D- MODAL TRANSPORTATION: OTHER MODAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AS93','R&D- MODAL TRANSPORTATION: OTHER MODAL (ADVANCED DEVELOPMENT)'),
	('AS94','R&D- MODAL TRANSPORTATION: OTHER MODAL (ENGINEERING DEVELOPMENT)'),
	('AS95','R&D- MODAL TRANSPORTATION: OTHER MODAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AS96','R&D- MODAL TRANSPORTATION: OTHER MODAL (MANAGEMENT/SUPPORT)'),
	('AS97','R&D- MODAL TRANSPORTATION: OTHER MODAL (COMMERCIALIZED)'),
	('AT','TRANSPORTATION (OTHER) RandD'),
	('AT11','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (BASIC RESEARCH)'),
	('AT12','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT13','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (ADVANCED DEVELOPMENT)'),
	('AT14','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (ENGINEERING DEVELOPMENT)'),
	('AT15','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT16','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (MANAGEMENT/SUPPORT)'),
	('AT17','R&D- OTHER TRANSPORTATION: HIGHWAYS, ROADS, AND BRIDGES (COMMERCIALIZED)'),
	('AT21','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (BASIC RESEARCH)'),
	('AT22','R&D- OTHER TRANSPORTATION: HUMAN FACTORS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT23','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (ADVANCED DEVELOPMENT)'),
	('AT24','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (ENGINEERING DEVELOPMENT)'),
	('AT25','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT26','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (MANAGEMENT/SUPPORT)'),
	('AT27','R&D- OTHER TRANSPORTATION: HUMAN FACTORS CONCERNING TRANSPORTATION (COMMERCIALIZED)'),
	('AT31','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (BASIC RESEARCH)'),
	('AT32','R&D- OTHER TRANSPORTATION: NAVIGATION/NAVIGATIONAL AIDS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT33','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (ADVANCED DEVELOPMENT)'),
	('AT34','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (ENGINEERING DEVELOPMENT)'),
	('AT35','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT36','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (MANAGEMENT/SUPPORT)'),
	('AT37','R&D- OTHER TRANSPORTATION: NAVIGATION AND NAVIGATIONAL AIDS (COMMERCIALIZED)'),
	('AT41','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (BASIC RESEARCH)'),
	('AT42','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT43','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (ADVANCED DEVELOPMENT)'),
	('AT44','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (ENGINEERING DEVELOPMENT)'),
	('AT45','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT46','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (MANAGEMENT/SUPPORT)'),
	('AT47','R&D- OTHER TRANSPORTATION: PASSENGER SAFETY AND SECURITY (COMMERCIALIZED)'),
	('AT51','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (BASIC RESEARCH)'),
	('AT52','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT53','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (ADVANCED DEVELOPMENT)'),
	('AT54','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (ENGINEERING DEVELOPMENT)'),
	('AT55','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT56','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (MANAGEMENT/SUPPORT)'),
	('AT57','R&D- OTHER TRANSPORTATION: PIPELINE SAFETY (COMMERCIALIZED)'),
	('AT61','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (BASIC RESEARCH)'),
	('AT62','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT63','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (ADVANCED DEVELOPMENT)'),
	('AT64','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (ENGINEERING DEVELOPMENT)'),
	('AT65','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT66','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (MANAGEMENT/SUPPORT)'),
	('AT67','R&D- OTHER TRANSPORTATION: TRAFFIC MANAGEMENT (COMMERCIALIZED)'),
	('AT71','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (BASIC RESEARCH)'),
	('AT72','R&D- OTHER TRANSPORTATION: TUNNELS/OTHER SUBSURF STRUC (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT73','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (ADVANCED DEVELOPMENT)'),
	('AT74','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (ENGINEERING DEVELOPMENT)'),
	('AT75','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT76','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (MANAGEMENT/SUPPORT)'),
	('AT77','R&D- OTHER TRANSPORTATION: TUNNELS AND OTHER SUBSURFACE STRUCTURES (COMMERCIALIZED)'),
	('AT81','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATERIALS (BASIC RESEARCH)'),
	('AT82','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZ MAT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT83','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATERIALS (ADVANCED DEVELOPMENT)'),
	('AT84','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATERIALS (ENGINEERING DEVELOPMENT)'),
	('AT85','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATERIALS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT86','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATERIALS (MANAGEMENT/SUPPORT)'),
	('AT87','R&D- OTHER TRANSPORTATION: TRANSPORTING HAZARDOUS MATS (COMMERCIALIZED)'),
	('AT91','R&D- OTHER TRANSPORTATION: OTHER GENERAL (BASIC RESEARCH)'),
	('AT92','R&D- OTHER TRANSPORTATION: OTHER GENERAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AT93','R&D- OTHER TRANSPORTATION: OTHER GENERAL (ADVANCED DEVELOPMENT)'),
	('AT94','R&D- OTHER TRANSPORTATION: OTHER GENERAL (ENGINEERING DEVELOPMENT)'),
	('AT95','R&D- OTHER TRANSPORTATION: OTHER GENERAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AT96','R&D- OTHER TRANSPORTATION: OTHER GENERAL (MANAGEMENT/SUPPORT)'),
	('AT97','R&D- OTHER TRANSPORTATION: OTHER GENERAL (COMMERCIALIZED)'),
	('AU','TRANSPORTATION (OBSOLETE GROUP)'),
	('AV','MINING RandD'),
	('AV11','R&D- MINING: SUBSURFACE MINING EQUIPMENT (BASIC RESEARCH)'),
	('AV12','R&D- MINING: SUBSURFACE MINING EQUIPMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV13','R&D- MINING: SUBSURFACE MINING EQUIPMENT (ADVANCED DEVELOPMENT)'),
	('AV14','R&D- MINING: SUBSURFACE MINING EQUIPMENT (ENGINEERING DEVELOPMENT)'),
	('AV15','R&D- MINING: SUBSURFACE MINING EQUIPMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV16','R&D- MINING: SUBSURFACE MINING EQUIPMENT (MANAGEMENT/SUPPORT)'),
	('AV17','R&D- MINING: SUBSURFACE MINING EQUIPMENT (COMMERCIALIZED)'),
	('AV21','R&D- MINING: SURFACE MINING EQUIPMENT (BASIC RESEARCH)'),
	('AV22','R&D- MINING: SURFACE MINING EQUIPMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV23','R&D- MINING: SURFACE MINING EQUIPMENT (ADVANCED DEVELOPMENT)'),
	('AV24','R&D- MINING: SURFACE MINING EQUIPMENT (ENGINEERING DEVELOPMENT)'),
	('AV25','R&D- MINING: SURFACE MINING EQUIPMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV26','R&D- MINING: SURFACE MINING EQUIPMENT (MANAGEMENT/SUPPORT)'),
	('AV27','R&D- MINING: SURFACE MINING EQUIPMENT (COMMERCIALIZED)'),
	('AV31','R&D- MINING: SUBSURFACE MINING METHODS (BASIC RESEARCH)'),
	('AV32','R&D- MINING: SUBSURFACE MINING METHODS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV33','R&D- MINING: SUBSURFACE MINING METHODS (ADVANCED DEVELOPMENT)'),
	('AV34','R&D- MINING: SUBSURFACE MINING METHODS (ENGINEERING DEVELOPMENT)'),
	('AV35','R&D- MINING: SUBSURFACE MINING METHODS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV36','R&D- MINING: SUBSURFACE MINING METHODS (MANAGEMENT/SUPPORT)'),
	('AV37','R&D- MINING: SUBSURFACE MINING METHODS (COMMERCIALIZED)'),
	('AV41','R&D- MINING: SURFACE MINING METHODS (BASIC RESEARCH)'),
	('AV42','R&D- MINING: SURFACE MINING METHODS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV43','R&D- MINING: SURFACE MINING METHODS (ADVANCED DEVELOPMENT)'),
	('AV44','R&D- MINING: SURFACE MINING METHODS (ENGINEERING DEVELOPMENT)'),
	('AV45','R&D- MINING: SURFACE MINING METHODS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV46','R&D- MINING: SURFACE MINING METHODS (MANAGEMENT/SUPPORT)'),
	('AV47','R&D- MINING: SURFACE MINING METHODS (COMMERCIALIZED)'),
	('AV51','R&D- MINING: MINING RECLAMATION METHODS (BASIC RESEARCH)'),
	('AV52','R&D- MINING: MINING RECLAMATION METHODS (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV53','R&D- MINING: MINING RECLAMATION METHODS (ADVANCED DEVELOPMENT)'),
	('AV54','R&D- MINING: MINING RECLAMATION METHODS (ENGINEERING DEVELOPMENT)'),
	('AV55','R&D- MINING: MINING RECLAMATION METHODS (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV56','R&D- MINING: MINING RECLAMATION METHODS (MANAGEMENT/SUPPORT)'),
	('AV57','R&D- MINING: MINING RECLAMATION METHODS (COMMERCIALIZED)'),
	('AV61','R&D- MINING: MINING SAFETY (BASIC RESEARCH)'),
	('AV62','R&D- MINING: MINING SAFETY (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV63','R&D- MINING: MINING SAFETY (ADVANCED DEVELOPMENT)'),
	('AV64','R&D- MINING: MINING SAFETY (ENGINEERING DEVELOPMENT)'),
	('AV65','R&D- MINING: MINING SAFETY (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV66','R&D- MINING: MINING SAFETY (MANAGEMENT/SUPPORT)'),
	('AV67','R&D- MINING: MINING SAFETY (COMMERCIALIZED)'),
	('AV71','R&D- MINING: METALLURGICAL (BASIC RESEARCH)'),
	('AV72','R&D- MINING: METALLURGICAL (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV73','R&D- MINING: METALLURGICAL (ADVANCED DEVELOPMENT)'),
	('AV74','R&D- MINING: METALLURGICAL (ENGINEERING DEVELOPMENT)'),
	('AV75','R&D- MINING: METALLURGICAL (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV76','R&D- MINING: METALLURGICAL (MANAGEMENT/SUPPORT)'),
	('AV77','R&D- MINING: METALLURGICAL (COMMERCIALIZED)'),
	('AV91','R&D- MINING: OTHER MINING ACTIVITIES (BASIC RESEARCH)'),
	('AV92','R&D- MINING: OTHER MINING ACTIVITIES (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AV93','R&D- MINING: OTHER MINING ACTIVITIES (ADVANCED DEVELOPMENT)'),
	('AV94','R&D- MINING: OTHER MINING ACTIVITIES (ENGINEERING DEVELOPMENT)'),
	('AV95','R&D- MINING: OTHER MINING ACTIVITIES (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AV96','R&D- MINING: OTHER MINING ACTIVITIES (MANAGEMENT/SUPPORT)'),
	('AV97','R&D- MINING: OTHER MINING ACTIVITIES (COMMERCIALIZED)'),
	('AZ','OTHER RESEARCH/DEVELOPMENT'),
	('AZ11','R&D- OTHER RESEARCH AND DEVELOPMENT (BASIC RESEARCH)'),
	('AZ12','R&D- OTHER RESEARCH AND DEVELOPMENT (APPLIED RESEARCH/EXPLORATORY DEVELOPMENT)'),
	('AZ13','R&D- OTHER RESEARCH AND DEVELOPMENT (ADVANCED DEVELOPMENT)'),
	('AZ14','R&D- OTHER RESEARCH AND DEVELOPMENT (ENGINEERING DEVELOPMENT)'),
	('AZ15','R&D- OTHER RESEARCH AND DEVELOPMENT (OPERATIONAL SYSTEMS DEVELOPMENT)'),
	('AZ16','R&D- OTHER RESEARCH AND DEVELOPMENT (MANAGEMENT/SUPPORT)'),
	('AZ17','R&D- OTHER RESEARCH AND DEVELOPMENT (COMMERCIALIZED)'),
	('B','SPECIAL STUDIES/ANALYSIS, NOT RandD'),
	('B5','SPECIAL STUDIES - NOT R and D'),
	('B502','SPECIAL STUDIES/ANALYSIS- AIR QUALITY'),
	('B503','SPECIAL STUDIES/ANALYSIS- ARCHEOLOGICAL/PALEONTOLOGICAL'),
	('B504','SPECIAL STUDIES/ANALYSIS- CHEMICAL/BIOLOGICAL'),
	('B505','SPECIAL STUDIES/ANALYSIS- COST BENEFIT'),
	('B506','SPECIAL STUDIES/ANALYSIS- DATA (OTHER THAN SCIENTIFIC)'),
	('B507','SPECIAL STUDIES/ANALYSIS- ECONOMIC'),
	('B509','SPECIAL STUDIES/ANALYSIS- ENDANGERED SPECIES: PLANT/ANIMAL'),
	('B510','SPECIAL STUDIES/ANALYSIS- ENVIRONMENTAL ASSESSMENTS'),
	('B513','SPECIAL STUDIES/ANALYSIS- FEASIBILITY (NON-CONSTRUCTION)'),
	('B516','SPECIAL STUDIES/ANALYSIS- ANIMAL/FISHERIES'),
	('B517','SPECIAL STUDIES/ANALYSIS- GEOLOGICAL'),
	('B518','SPECIAL STUDIES/ANALYSIS- GEOPHYSICAL'),
	('B519','SPECIAL STUDIES/ANALYSIS- GEOTECHNICAL'),
	('B520','SPECIAL STUDIES/ANALYSIS- GRAZING/RANGE'),
	('B521','SPECIAL STUDIES/ANALYSIS- HISTORICAL'),
	('B522','SPECIAL STUDIES/ANALYSIS- LEGAL'),
	('B524','SPECIAL STUDIES/ANALYSIS- MATHEMATICAL/STATISTICAL'),
	('B525','SPECIAL STUDIES/ANALYSIS- NATURAL RESOURCE'),
	('B526','SPECIAL STUDIES/ANALYSIS- OCEANOLOGICAL'),
	('B527','SPECIAL STUDIES/ANALYSIS- RECREATION'),
	('B528','SPECIAL STUDIES/ANALYSIS- REGULATORY'),
	('B529','SPECIAL STUDIES/ANALYSIS- SCIENTIFIC DATA'),
	('B530','SPECIAL STUDIES/ANALYSIS- SEISMOLOGICAL'),
	('B532','SPECIAL STUDIES/ANALYSIS- SOIL'),
	('B533','SPECIAL STUDIES/ANALYSIS- WATER QUALITY'),
	('B534','SPECIAL STUDIES/ANALYSIS- WILDLIFE'),
	('B537','SPECIAL STUDIES/ANALYSIS- MEDICAL/HEALTH'),
	('B538','SPECIAL STUDIES/ANALYSIS- INTELLIGENCE'),
	('B539','SPECIAL STUDIES/ANALYSIS- AERONAUTICAL/SPACE'),
	('B540','SPECIAL STUDIES/ANALYSIS- BUILDING TECHNOLOGY')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('B541','SPECIAL STUDIES/ANALYSIS- DEFENSE'),
	('B542','SPECIAL STUDIES/ANALYSIS- EDUCATIONAL'),
	('B543','SPECIAL STUDIES/ANALYSIS- ENERGY'),
	('B544','SPECIAL STUDIES/ANALYSIS- TECHNOLOGY'),
	('B545','SPECIAL STUDIES/ANALYSIS- HOUSING/COMMUNITY DEVELOPMENT'),
	('B546','SPECIAL STUDIES/ANALYSIS- SECURITY (PHYSICAL/PERSONAL)'),
	('B547','SPECIAL STUDIES/ANALYSIS- ACCOUNTING/FINANCIAL MANAGEMENT'),
	('B548','SPECIAL STUDIES/ANALYSIS- TRADE ISSUE'),
	('B549','SPECIAL STUDIES/ANALYSIS- FOREIGN/NATIONAL SECURITY POLICY'),
	('B550','SPECIAL STUDIES/ANALYSIS- ORGANIZATION/ADMINISTRATIVE/PERSONNEL'),
	('B551','SPECIAL STUDIES/ANALYSIS- MOBILIZATION/PREPAREDNESS'),
	('B552','SPECIAL STUDIES/ANALYSIS- MANPOWER'),
	('B553','SPECIAL STUDIES/ANALYSIS- COMMUNICATIONS'),
	('B554','SPECIAL STUDIES/ANALYSIS- ACQUISITION POLICY/PROCEDURES'),
	('B555','SPECIAL STUDIES/ANALYSIS- ELDERLY/HANDICAPPED'),
	('B599','SPECIAL STUDIES/ANALYSIS- OTHER'),
	('C','ARCHITECT/ENGINEER SERVICES'),
	('C1','ARCH-ENG SVCS - CONSTRUCTION'),
	('C1AA','ARCHITECT AND ENGINEERING- CONSTRUCTION: OFFICE BUILDINGS'),
	('C1AB','ARCHITECT AND ENGINEERING- CONSTRUCTION: CONFERENCE SPACE AND FACILITIES'),
	('C1AZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER ADMINISTRATIVE FACILITIES/SERVICE BUILDINGS'),
	('C1BA','ARCHITECT AND ENGINEERING- CONSTRUCTION: AIR TRAFFIC CONTROL TOWERS'),
	('C1BB','ARCHITECT AND ENGINEERING- CONSTRUCTION: AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('C1BC','ARCHITECT AND ENGINEERING- CONSTRUCTION: RADAR AND NAVIGATIONAL FACILITIES'),
	('C1BD','ARCHITECT AND ENGINEERING- CONSTRUCTION: AIRPORT RUNWAYS AND TAXIWAYS'),
	('C1BE','ARCHITECT AND ENGINEERING- CONSTRUCTION: AIRPORT TERMINALS'),
	('C1BF','ARCHITECT AND ENGINEERING- CONSTRUCTION: MISSILE SYSTEM FACILITIES'),
	('C1BG','ARCHITECT AND ENGINEERING- CONSTRUCTION: ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('C1BZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER AIRFIELD STRUCTURES'),
	('C1CA','ARCHITECT AND ENGINEERING- CONSTRUCTION: SCHOOLS'),
	('C1CZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER EDUCATIONAL BUILDINGS'),
	('C1DA','ARCHITECT AND ENGINEERING- CONSTRUCTION: HOSPITALS AND INFIRMARIES'),
	('C1DB','ARCHITECT AND ENGINEERING- CONSTRUCTION: LABORATORIES AND CLINICS'),
	('C1DZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER HOSPITAL BUILDINGS'),
	('C1EA','ARCHITECT AND ENGINEERING- CONSTRUCTION: AMMUNITION FACILITIES'),
	('C1EB','ARCHITECT AND ENGINEERING- CONSTRUCTION: MAINTENANCE BUILDINGS'),
	('C1EC','ARCHITECT AND ENGINEERING- CONSTRUCTION: PRODUCTION BUILDINGS'),
	('C1ED','ARCHITECT AND ENGINEERING- CONSTRUCTION: SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('C1EE','ARCHITECT AND ENGINEERING- CONSTRUCTION: TANK AUTOMOTIVE FACILITIES'),
	('C1EZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER INDUSTRIAL BUILDINGS'),
	('C1FA','ARCHITECT AND ENGINEERING- CONSTRUCTION: FAMILY HOUSING FACILITIES'),
	('C1FB','ARCHITECT AND ENGINEERING- CONSTRUCTION: RECREATIONAL BUILDINGS'),
	('C1FC','ARCHITECT AND ENGINEERING- CONSTRUCTION: TROOP HOUSING FACILITIES'),
	('C1FD','ARCHITECT AND ENGINEERING- CONSTRUCTION: DINING FACILITIES'),
	('C1FE','ARCHITECT AND ENGINEERING- CONSTRUCTION: RELIGIOUS FACILITIES'),
	('C1FF','ARCHITECT AND ENGINEERING- CONSTRUCTION: PENAL FACILITIES'),
	('C1FZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER RESIDENTIAL BUILDINGS'),
	('C1GA','ARCHITECT AND ENGINEERING- CONSTRUCTION: AMMUNITION STORAGE BUILDINGS'),
	('C1GB','ARCHITECT AND ENGINEERING- CONSTRUCTION: FOOD OR GRAIN STORAGE BUILDINGS'),
	('C1GC','ARCHITECT AND ENGINEERING- CONSTRUCTION: FUEL STORAGE BUILDINGS'),
	('C1GD','ARCHITECT AND ENGINEERING- CONSTRUCTION: OPEN STORAGE FACILITIES'),
	('C1GZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER WAREHOUSE BUILDINGS'),
	('C1HA','ARCHITECT AND ENGINEERING- CONSTRUCTION: GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACS'),
	('C1HB','ARCHITECT AND ENGINEERING- CONSTRUCTION: GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACS'),
	('C1HC','ARCHITECT AND ENGINEERING- CONSTRUCTION: GOVT-OWNED CTR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('C1HZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: GOVT-OWNED GOVT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('C1JA','ARCHITECT AND ENGINEERING- CONSTRUCTION: MUSEUMS AND EXHIBITION BUILDINGS'),
	('C1JB','ARCHITECT AND ENGINEERING- CONSTRUCTION: TESTING AND MEASUREMENT BUILDINGS'),
	('C1JZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: MISCELLANEOUS BUILDINGS'),
	('C1KA','ARCHITECT AND ENGINEERING- CONSTRUCTION: DAMS'),
	('C1KB','ARCHITECT AND ENGINEERING- CONSTRUCTION: CANALS'),
	('C1KC','ARCHITECT AND ENGINEERING- CONSTRUCTION: MINE FIRE CONTROL FACILITIES'),
	('C1KD','ARCHITECT AND ENGINEERING- CONSTRUCTION: MINE SUBSIDENCE CONTROL FACILITIES'),
	('C1KE','ARCHITECT AND ENGINEERING- CONSTRUCTION: SURFACE MINE RECLAMATION FACILITIES'),
	('C1KF','ARCHITECT AND ENGINEERING- CONSTRUCTION: DREDGING FACILITIES'),
	('C1KZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('C1LA','ARCHITECT AND ENGINEERING- CONSTRUCTION: AIRPORT SERVICE ROADS'),
	('C1LB','ARCHITECT AND ENGINEERING- CONSTRUCTION: HIGHWAYS, ROADS, STREETS, BRIDGES, AND RAILWAYS'),
	('C1LC','ARCHITECT AND ENGINEERING- CONSTRUCTION: TUNNELS AND SUBSURFACE STRUCTURES'),
	('C1LZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: PARKING FACILITIES'),
	('C1MA','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - COAL'),
	('C1MB','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - GAS'),
	('C1MC','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - GEOTHERMAL'),
	('C1MD','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - HYDRO'),
	('C1ME','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - NUCLEAR'),
	('C1MF','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - PETROLEUM'),
	('C1MG','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - SOLAR'),
	('C1MH','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - WIND'),
	('C1MZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('C1NA','ARCHITECT AND ENGINEERING- CONSTRUCTION: FUEL SUPPLY FACILITIES'),
	('C1NB','ARCHITECT AND ENGINEERING- CONSTRUCTION: HEATING AND COOLING PLANTS'),
	('C1NC','ARCHITECT AND ENGINEERING- CONSTRUCTION: POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('C1ND','ARCHITECT AND ENGINEERING- CONSTRUCTION: SEWAGE AND WASTE FACILITIES'),
	('C1NE','ARCHITECT AND ENGINEERING- CONSTRUCTION: WATER SUPPLY FACILITIES'),
	('C1NZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER UTILITIES'),
	('C1PA','ARCHITECT AND ENGINEERING- CONSTRUCTION: RECREATIONAL FACILITIES (NON-BUILDING)'),
	('C1PB','ARCHITECT AND ENGINEERING- CONSTRUCTION: EXHIBIT DESIGN (NON-BUILDING)'),
	('C1PC','ARCHITECT AND ENGINEERING- CONSTRUCTION: UNIMPROVED REAL PROPERTY (LAND)'),
	('C1PD','ARCHITECT AND ENGINEERING- CONSTRUCTION: WASTE TREATMENT AND STORAGE FACILITIES'),
	('C1PZ','ARCHITECT AND ENGINEERING- CONSTRUCTION: OTHER NON-BUILDING FACILITIES'),
	('C1QA','ARCHITECT AND ENGINEERING- CONSTRUCTION: RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('C2','ARCH-ENG SVCS - GENERAL'),
	('C211','ARCHITECT AND ENGINEERING- GENERAL: LANDSCAPING, INTERIOR LAYOUT, AND DESIGNING'),
	('C212','ARCHITECT AND ENGINEERING- GENERAL: ENGINEERING DRAFTING, NOT CAD/CAM'),
	('C213','ARCHITECT AND ENGINEERING- GENERAL: INSPECTION (NON-CONSTRUCTION)'),
	('C214','ARCHITECT AND ENGINEERING- GENERAL: MANAGEMENT ENGINEERING'),
	('C215','ARCHITECT AND ENGINEERING- GENERAL: PRODUCTION ENGINEERING'),
	('C216','ARCHITECT AND ENGINEERING- GENERAL: MARINE ENGINEERING'),
	('C219','ARCHITECT AND ENGINEERING- GENERAL: OTHER'),
	('C220','ARCHITECT AND ENGINEERING- GENERAL: STRUCTURAL ENGINEERING'),
	('C221','ARCHITECT AND ENGINEERING- GENERAL: PLUMBING SYSTEMS'),
	('C222','ARCHITECT AND ENGINEERING- GENERAL: ELECTRICAL SYSTEMS'),
	('C223','ARCHITECT AND ENGINEERING- GENERAL: MECHANICAL SYSTEMS'),
	('D','ADP AND TELECOMMUNICATIONS'),
	('D3','ADP AND TELECOMMUNICATIONS'),
	('D301','IT AND TELECOM- FACILITY OPERATION AND MAINTENANCE'),
	('D302','IT AND TELECOM- SYSTEMS DEVELOPMENT'),
	('D303','IT AND TELECOM- DATA ENTRY'),
	('D304','IT AND TELECOM- TELECOMMUNICATIONS AND TRANSMISSION'),
	('D305','IT AND TELECOM- TELEPROCESSING, TIMESHARE, AND CLOUD COMPUTING'),
	('D306','IT AND TELECOM- SYSTEMS ANALYSIS'),
	('D307','IT AND TELECOM- IT STRATEGY AND ARCHITECTURE'),
	('D308','IT AND TELECOM- PROGRAMMING'),
	('D309','IT AND TELECOM- INFORMATION AND DATA BROADCASTING OR DATA DISTRIBUTION'),
	('D310','IT AND TELECOM- CYBER SECURITY AND DATA BACKUP'),
	('D311','IT AND TELECOM- DATA CONVERSION'),
	('D312','IT AND TELECOM- OPTICAL SCANNING'),
	('D313','IT AND TELECOM- COMPUTER AIDED DESIGN/COMPUTER AIDED MANUFACTURING (CAD/CAM)'),
	('D314','IT AND TELECOM- SYSTEM ACQUISITION SUPPORT'),
	('D315','IT AND TELECOM- DIGITIZING'),
	('D316','IT AND TELECOM- TELECOMMUNICATIONS NETWORK MANAGEMENT'),
	('D317','IT AND TELECOM- WEB-BASED SUBSCRIPTION'),
	('D318','IT AND TELECOM- INTEGRATED HARDWARE/SOFTWARE/SERVICES SOLUTIONS, PREDOMINANTLY SERVICES'),
	('D319','IT AND TELECOM- ANNUAL SOFTWARE MAINTENANCE SERVICE PLANS'),
	('D320','IT AND TELECOM- ANNUAL HARDWARE MAINTENANCE SERVICE PLANS'),
	('D321','IT AND TELECOM- HELP DESK'),
	('D322','IT AND TELECOM- INTERNET'),
	('D324','IT AND TELECOM- BUSINESS CONTINUITY'),
	('D325','IT AND TELECOM- DATA CENTERS AND STORAGE'),
	('D399','IT AND TELECOM- OTHER IT AND TELECOMMUNICATIONS'),
	('E','PURCHASE OF STRUCTURES/FACILITIES'),
	('E1','PURCHASE BUILDINGS'),
	('E1AA','PURCHASE OF OFFICE BUILDINGS'),
	('E1AB','PURCHASE OF CONFERENCE SPACE AND FACILITIES'),
	('E1AZ','PURCHASE OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('E1BA','PURCHASE OF AIR TRAFFIC CONTROL TOWERS'),
	('E1BB','PURCHASE OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('E1BC','PURCHASE OF RADAR AND NAVIGATIONAL FACILITIES'),
	('E1BD','PURCHASE OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('E1BE','PURCHASE OF AIRPORT TERMINALS'),
	('E1BF','PURCHASE OF MISSILE SYSTEM FACILITIES'),
	('E1BG','PURCHASE OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('E1BZ','PURCHASE OF OTHER AIRFIELD STRUCTURES'),
	('E1CA','PURCHASE OF SCHOOLS'),
	('E1CZ','PURCHASE OF OTHER EDUCATIONAL BUILDINGS'),
	('E1DA','PURCHASE OF HOSPITALS AND INFIRMARIES'),
	('E1DB','PURCHASE OF LABORATORIES AND CLINICS'),
	('E1DZ','PURCHASE OF OTHER HOSPITAL BUILDINGS'),
	('E1EA','PURCHASE OF AMMUNITION FACILITIES'),
	('E1EB','PURCHASE OF MAINTENANCE BUILDINGS'),
	('E1EC','PURCHASE OF PRODUCTION BUILDINGS'),
	('E1ED','PURCHASE OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('E1EE','PURCHASE OF TANK AUTOMOTIVE FACILITIES'),
	('E1EZ','PURCHASE OF OTHER INDUSTRIAL BUILDINGS'),
	('E1FA','PURCHASE OF FAMILY HOUSING FACILITIES'),
	('E1FB','PURCHASE OF RECREATIONAL BUILDINGS'),
	('E1FC','PURCHASE OF TROOP HOUSING FACILITIES'),
	('E1FD','PURCHASE OF DINING FACILITIES'),
	('E1FE','PURCHASE OF RELIGIOUS FACILITIES'),
	('E1FF','PURCHASE OF PENAL FACILITIES'),
	('E1FZ','PURCHASE OF OTHER RESIDENTIAL BUILDINGS'),
	('E1GA','PURCHASE OF AMMUNITION STORAGE BUILDINGS'),
	('E1GB','PURCHASE OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('E1GC','PURCHASE OF FUEL STORAGE BUILDINGS'),
	('E1GD','PURCHASE OF OPEN STORAGE FACILITIES'),
	('E1GZ','PURCHASE OF OTHER WAREHOUSE BUILDINGS'),
	('E1HA','PURCHASE OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('E1HB','PURCHASE OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('E1HC','PURCHASE OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('E1HZ','PURCHASE OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('E1JA','PURCHASE OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('E1JB','PURCHASE OF TESTING AND MEASUREMENT BUILDINGS'),
	('E1JZ','PURCHASE OF MISCELLANEOUS BUILDINGS'),
	('E1KA','PURCHASE OF DAMS'),
	('E1KB','PURCHASE OF CANALS'),
	('E1KC','PURCHASE OF MINE FIRE CONTROL FACILITIES'),
	('E1KD','PURCHASE OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('E1KE','PURCHASE OF SURFACE MINE RECLAMATION FACILITIES'),
	('E1KF','PURCHASE OF DREDGING FACILITIES'),
	('E1KZ','PURCHASE OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('E1LA','PURCHASE OF AIRPORT SERVICE ROADS'),
	('E1LB','PURCHASE OF HIGHWAYS, ROADS, STREETS, BRIDGES, AND RAILWAYS'),
	('E1LC','PURCHASE OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('E1LZ','PURCHASE OF PARKING FACILITIES'),
	('E1MA','PURCHASE OF EPG FACILITIES - COAL'),
	('E1MB','PURCHASE OF EPG FACILITIES - GAS'),
	('E1MC','PURCHASE OF EPG FACILITIES - GEOTHERMAL'),
	('E1MD','PURCHASE OF EPG FACILITIES - HYDRO'),
	('E1ME','PURCHASE OF EPG FACILITIES - NUCLEAR'),
	('E1MF','PURCHASE OF EPG FACILITIES - PETROLEUM'),
	('E1MG','PURCHASE OF EPG FACILITIES - SOLAR'),
	('E1MH','PURCHASE OF EPG FACILITIES - WIND'),
	('E1MZ','PURCHASE OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('E1NA','PURCHASE OF FUEL SUPPLY FACILITIES'),
	('E1NB','PURCHASE OF HEATING AND COOLING PLANTS'),
	('E1NC','PURCHASE OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('E1ND','PURCHASE OF SEWAGE AND WASTE FACILITIES'),
	('E1NE','PURCHASE OF WATER SUPPLY FACILITIES'),
	('E1NZ','PURCHASE OF OTHER UTILITIES'),
	('E1PA','PURCHASE OF RECREATIONAL FACILITIES (NON-BUILDING)'),
	('E1PB','PURCHASE OF EXHIBIT DESIGN (NON-BUILDING)'),
	('E1PC','PURCHASE OF UNIMPROVED REAL PROPERTY (LAND)'),
	('E1PD','PURCHASE OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('E1PZ','PURCHASE OF OTHER NON-BUILDING FACILITIES'),
	('E1QA','PURCHASE OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('E2','PURCHASE FACILITY NOT A BUILDING'),
	('E3','PURCH RESTORATION OF REAL PROPERTY'),
	('F','NATURAL RESOURCES MANAGEMENT'),
	('F0','NATURAL RESOURCE CONSERVERVAT SVCS'),
	('F001','NATURAL RESOURCES/CONSERVATION- AERIAL FERTILIZATION/SPRAYING'),
	('F002','NATURAL RESOURCES/CONSERVATION- AERIAL SEEDING'),
	('F003','NATURAL RESOURCES/CONSERVATION- FOREST-RANGE FIRE SUPPRESSION/PRESUPPRESSION'),
	('F004','NATURAL RESOURCES/CONSERVATION- FOREST/RANGE FIRE REHABILITATION (NON-CONSTRUCTION)'),
	('F005','NATURAL RESOURCES/CONSERVATION- FOREST TREE PLANTING'),
	('F006','NATURAL RESOURCES/CONSERVATION- LAND TREATMENT PRACTICES'),
	('F007','NATURAL RESOURCES/CONSERVATION- RANGE SEEDING (GROUND EQUIPMENT)'),
	('F008','NATURAL RESOURCES/CONSERVATION- RECREATION SITE MAINTENANCE (NON-CONSTRUCTION)'),
	('F009','NATURAL RESOURCES/CONSERVATION- SEED COLLECTION/PRODUCTION'),
	('F010','NATURAL RESOURCES/CONSERVATION- SEEDLING PRODUCTION/TRANSPLANTING'),
	('F011','NATURAL RESOURCES/CONSERVATION- SURFACE MINING RECLAMATION (NON-CONSTRUCTION)'),
	('F012','NATURAL RESOURCES/CONSERVATION- SURVEY LINE CLEARING'),
	('F013','NATURAL RESOURCES/CONSERVATION- TREE BREEDING'),
	('F014','NATURAL RESOURCES/CONSERVATION- TREE THINNING'),
	('F015','NATURAL RESOURCES/CONSERVATION- WELL DRILLING/EXPLORATORY'),
	('F016','NATURAL RESOURCES/CONSERVATION- WILDHORSE/BURRO CONTROL'),
	('F018','NATURAL RESOURCES/CONSERVATION- OTHER FOREST/RANGE IMPROVEMENTS (NON-CONSTRUCTION)'),
	('F019','NATURAL RESOURCES/CONSERVATION- OTHER WILDLIFE MANAGEMENT'),
	('F020','NATURAL RESOURCES/CONSERVATION- FISHERIES RESOURCES MANAGEMENT'),
	('F021','NATURAL RESOURCES/CONSERVATION- SITE PREPARATION'),
	('F022','NATURAL RESOURCES/CONSERVATION- FISH HATCHERY'),
	('F099','NATURAL RESOURCES/CONSERVATION- OTHER'),
	('F1','ENVIRONMENTAL SYSTEMS PROTECTION'),
	('F101','ENVIRONMENTAL SYSTEMS PROTECTION- AIR QUALITY SUPPORT'),
	('F103','ENVIRONMENTAL SYSTEMS PROTECTION- WATER QUALITY SUPPORT'),
	('F105','ENVIRONMENTAL SYSTEMS PROTECTION- PESTICIDES SUPPORT'),
	('F107','ENVIRONMENTAL SYSTEMS PROTECTION- TOXIC AND HAZARDOUS SUBSTANCE ANALYSIS'),
	('F108','ENVIRONMENTAL SYSTEMS PROTECTION- ENVIRONMENTAL REMEDIATION'),
	('F109','ENVIRONMENTAL SYSTEMS PROTECTION- LEAKING UNDERGROUND STORAGE TANK SUPPORT'),
	('F110','ENVIRON SYS PROTECT- DEVELOPMENT OF ENVIRON IMPACT STMTS/ASSESSMENTS, TECH ANALYSIS/ENVIRON AUDITS'),
	('F111','ENVIRONMENTAL SYSTEMS PROTECTION- MULTIPLE POLLUTANT SUPPORT'),
	('F112','ENVIRONMENTAL SYSTEMS PROTECTION- OIL SPILL RESPONSE'),
	('F113','ENVIRONMENTAL SYSTEMS PROTECTION- WETLANDS CONSERVATION AND SUPPORT'),
	('F114','ENVIRONMENTAL SYSTEMS PROTECTION- ENVIRONMENTAL LICENSING AND PERMITTING'),
	('F115','ENVIRONMENTAL SYSTEMS PROTECTION- ENVIRONMENTAL CONSULTING AND LEGAL SUPPORT'),
	('F9','NATURAL RESOURCES - OTHER SVCS'),
	('F999','OTHER ENVIRONMENTAL SERVICES'),
	('G','SOCIAL SERVICES'),
	('G0','SOCIAL SERVICES'),
	('G001','SOCIAL- CARE OF REMAINS AND/OR FUNERAL'),
	('G002','SOCIAL- CHAPLAIN'),
	('G003','SOCIAL- RECREATIONAL'),
	('G004','SOCIAL- SOCIAL REHABILITATION'),
	('G005','SOCIAL- GERIATRIC'),
	('G006','SOCIAL- GOVERNMENT LIFE INSURANCE PROGRAMS'),
	('G007','SOCIAL- GOVERNMENT HEALTH INSURANCE PROGRAMS'),
	('G008','SOCIAL- GOVERNMENT INSURANCE PROGRAMS: OTHER'),
	('G009','SOCIAL- NON-GOVERNMENT INSURANCE PROGRAMS'),
	('G010','SOCIAL- DIRECT AID TO TRIBAL GOVERNMENTS (PL 93-638)'),
	('G099','SOCIAL- OTHER'),
	('H','QUALITY CONTROL, TEST, INSPECTION'),
	('H1','QUALITY CONTROL SERVICES'),
	('H110','QUALITY CONTROL- WEAPONS'),
	('H111','QUALITY CONTROL- NUCLEAR ORDNANCE'),
	('H112','QUALITY CONTROL- FIRE CONTROL EQUIPMENT'),
	('H113','QUALITY CONTROL- AMMUNITION AND EXPLOSIVES'),
	('H114','QUALITY CONTROL- GUIDED MISSILES'),
	('H115','QUALITY CONTROL- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('H116','QUALITY CONTROL- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('H117','QUALITY CONTROL- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('H118','QUALITY CONTROL- SPACE VEHICLES'),
	('H119','QUALITY CONTROL- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('H120','QUALITY CONTROL- SHIP AND MARINE EQUIPMENT'),
	('H122','QUALITY CONTROL- RAILWAY EQUIPMENT'),
	('H123','QUALITY CONTROL- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('H124','QUALITY CONTROL- TRACTORS'),
	('H125','QUALITY CONTROL- VEHICULAR EQUIPMENT COMPONENTS'),
	('H126','QUALITY CONTROL- TIRES AND TUBES'),
	('H128','QUALITY CONTROL- ENGINES, TURBINES, AND COMPONENTS'),
	('H129','QUALITY CONTROL- ENGINE ACCESSORIES'),
	('H130','QUALITY CONTROL- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('H131','QUALITY CONTROL- BEARINGS'),
	('H132','QUALITY CONTROL- WOODWORKING MACHINERY AND EQUIPMENT'),
	('H134','QUALITY CONTROL- METALWORKING MACHINERY'),
	('H135','QUALITY CONTROL- SERVICE AND TRADE EQUIPMENT'),
	('H136','QUALITY CONTROL- SPECIAL INDUSTRY MACHINERY'),
	('H137','QUALITY CONTROL- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('H138','QUALITY CONTROL- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('H139','QUALITY CONTROL- MATERIALS HANDLING EQUIPMENT'),
	('H140','QUALITY CONTROL- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('H141','QUALITY CONTROL- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('H142','QUALITY CONTROL- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('H143','QUALITY CONTROL- PUMPS AND COMPRESSORS'),
	('H144','QUALITY CONTROL- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('H145','QUALITY CONTROL- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('H146','QUALITY CONTROL- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('H147','QUALITY CONTROL- PIPE, TUBING, HOSE, AND FITTINGS'),
	('H148','QUALITY CONTROL- VALVES'),
	('H149','QUALITY CONTROL- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('H151','QUALITY CONTROL- HAND TOOLS'),
	('H152','QUALITY CONTROL- MEASURING TOOLS'),
	('H153','QUALITY CONTROL- HARDWARE AND ABRASIVES'),
	('H154','QUALITY CONTROL- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('H155','QUALITY CONTROL- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('H156','QUALITY CONTROL- CONSTRUCTION AND BUILDING MATERIALS'),
	('H158','QUALITY CONTROL- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('H159','QUALITY CONTROL- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('H160','QUALITY CONTROL- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('H161','QUALITY CONTROL- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('H162','QUALITY CONTROL- LIGHTING FIXTURES AND LAMPS'),
	('H163','QUALITY CONTROL- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('H165','QUALITY CONTROL- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('H166','QUALITY CONTROL- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('H167','QUALITY CONTROL- PHOTOGRAPHIC EQUIPMENT'),
	('H168','QUALITY CONTROL- CHEMICALS AND CHEMICAL PRODUCTS'),
	('H169','QUALITY CONTROL- TRAINING AIDS AND DEVICES'),
	('H170','QUALITY CONTROL- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('H171','QUALITY CONTROL- FURNITURE'),
	('H172','QUALITY CONTROL- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('H173','QUALITY CONTROL- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('H174','QUALITY CONTROL- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('H175','QUALITY CONTROL- OFFICE SUPPLIES AND DEVICES'),
	('H176','QUALITY CONTROL- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('H177','QUALITY CONTROL- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('H178','QUALITY CONTROL- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('H179','QUALITY CONTROL- CLEANING EQUIPMENT AND SUPPLIES'),
	('H180','QUALITY CONTROL- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('H181','QUALITY CONTROL- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('H183','QUALITY CONTROL- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('H184','QUALITY CONTROL- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('H185','QUALITY CONTROL- TOILETRIES'),
	('H187','QUALITY CONTROL- AGRICULTURAL SUPPLIES'),
	('H188','QUALITY CONTROL- LIVE ANIMALS'),
	('H189','QUALITY CONTROL- SUBSISTENCE'),
	('H191','QUALITY CONTROL- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('H193','QUALITY CONTROL- NONMETALLIC FABRICATED MATERIALS'),
	('H194','QUALITY CONTROL- NONMETALLIC CRUDE MATERIALS'),
	('H195','QUALITY CONTROL- METAL BARS, SHEETS, AND SHAPES'),
	('H196','QUALITY CONTROL- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('H199','QUALITY CONTROL- MISCELLANEOUS'),
	('H2','EQUIPMENT AND MATERIALS TESTING'),
	('H210','EQUIPMENT AND MATERIALS TESTING- WEAPONS'),
	('H211','EQUIPMENT AND MATERIALS TESTING- NUCLEAR ORDNANCE'),
	('H212','EQUIPMENT AND MATERIALS TESTING- FIRE CONTROL EQUIPMENT'),
	('H213','EQUIPMENT AND MATERIALS TESTING- AMMUNITION AND EXPLOSIVES'),
	('H214','EQUIPMENT AND MATERIALS TESTING- GUIDED MISSILES'),
	('H215','EQUIPMENT AND MATERIALS TESTING- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('H216','EQUIPMENT AND MATERIALS TESTING- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('H217','EQUIPMENT AND MATERIALS TESTING- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('H218','EQUIPMENT AND MATERIALS TESTING- SPACE VEHICLES'),
	('H219','EQUIPMENT AND MATERIALS TESTING- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('H220','EQUIPMENT AND MATERIALS TESTING- SHIP AND MARINE EQUIPMENT'),
	('H222','EQUIPMENT AND MATERIALS TESTING- RAILWAY EQUIPMENT'),
	('H223','EQUIPMENT AND MATERIALS TESTING- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('H224','EQUIPMENT AND MATERIALS TESTING- TRACTORS'),
	('H225','EQUIPMENT AND MATERIALS TESTING- VEHICULAR EQUIPMENT COMPONENTS'),
	('H226','EQUIPMENT AND MATERIALS TESTING- TIRES AND TUBES'),
	('H228','EQUIPMENT AND MATERIALS TESTING- ENGINES, TURBINES, AND COMPONENTS'),
	('H229','EQUIPMENT AND MATERIALS TESTING- ENGINE ACCESSORIES'),
	('H230','EQUIPMENT AND MATERIALS TESTING- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('H231','EQUIPMENT AND MATERIALS TESTING- BEARINGS'),
	('H232','EQUIPMENT AND MATERIALS TESTING- WOODWORKING MACHINERY AND EQUIPMENT'),
	('H234','EQUIPMENT AND MATERIALS TESTING- METALWORKING MACHINERY'),
	('H235','EQUIPMENT AND MATERIALS TESTING- SERVICE AND TRADE EQUIPMENT'),
	('H236','EQUIPMENT AND MATERIALS TESTING- SPECIAL INDUSTRY MACHINERY'),
	('H237','EQUIPMENT AND MATERIALS TESTING- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('H238','EQUIPMENT AND MATERIALS TESTING- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('H239','EQUIPMENT AND MATERIALS TESTING- MATERIALS HANDLING EQUIPMENT'),
	('H240','EQUIPMENT AND MATERIALS TESTING- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('H241','EQUIPMENT AND MATERIALS TESTING- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('H242','EQUIP/MATERIALS TESTING- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('H243','EQUIPMENT AND MATERIALS TESTING- PUMPS AND COMPRESSORS'),
	('H244','EQUIPMENT AND MATERIALS TESTING- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('H245','EQUIPMENT AND MATERIALS TESTING- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('H246','EQUIPMENT AND MATERIALS TESTING- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('H247','EQUIPMENT AND MATERIALS TESTING- PIPE, TUBING, HOSE, AND FITTINGS'),
	('H248','EQUIPMENT AND MATERIALS TESTING- VALVES'),
	('H249','EQUIPMENT AND MATERIALS TESTING- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('H251','EQUIPMENT AND MATERIALS TESTING- HAND TOOLS'),
	('H252','EQUIPMENT AND MATERIALS TESTING- MEASURING TOOLS'),
	('H253','EQUIPMENT AND MATERIALS TESTING- HARDWARE AND ABRASIVES'),
	('H254','EQUIPMENT AND MATERIALS TESTING- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('H255','EQUIPMENT AND MATERIALS TESTING- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('H256','EQUIPMENT AND MATERIALS TESTING- CONSTRUCTION AND BUILDING MATERIALS'),
	('H258','EQUIPMENT AND MATERIALS TESTING- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('H259','EQUIPMENT AND MATERIALS TESTING- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('H260','EQUIPMENT AND MATERIALS TESTING- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('H261','EQUIPMENT AND MATERIALS TESTING- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('H262','EQUIPMENT AND MATERIALS TESTING- LIGHTING FIXTURES AND LAMPS'),
	('H263','EQUIPMENT AND MATERIALS TESTING- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('H265','EQUIPMENT AND MATERIALS TESTING- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('H266','EQUIPMENT AND MATERIALS TESTING- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('H267','EQUIPMENT AND MATERIALS TESTING- PHOTOGRAPHIC EQUIPMENT'),
	('H268','EQUIPMENT AND MATERIALS TESTING- CHEMICALS AND CHEMICAL PRODUCTS'),
	('H269','EQUIPMENT AND MATERIALS TESTING- TRAINING AIDS AND DEVICES'),
	('H270','EQUIPMENT AND MATERIALS TESTING- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('H271','EQUIPMENT AND MATERIALS TESTING- FURNITURE'),
	('H272','EQUIPMENT AND MATERIALS TESTING- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('H273','EQUIPMENT AND MATERIALS TESTING- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('H274','EQUIPMENT/MATERIALS TESTING- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('H275','EQUIPMENT AND MATERIALS TESTING- OFFICE SUPPLIES AND DEVICES'),
	('H276','EQUIPMENT AND MATERIALS TESTING- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('H277','EQUIPMENT AND MATERIALS TESTING- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('H278','EQUIPMENT AND MATERIALS TESTING- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('H279','EQUIPMENT AND MATERIALS TESTING- CLEANING EQUIPMENT AND SUPPLIES'),
	('H280','EQUIPMENT AND MATERIALS TESTING- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('H281','EQUIPMENT AND MATERIALS TESTING- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('H283','EQUIPMENT AND MATERIALS TESTING- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('H284','EQUIPMENT AND MATERIALS TESTING- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('H285','EQUIPMENT AND MATERIALS TESTING- TOILETRIES'),
	('H287','EQUIPMENT AND MATERIALS TESTING- AGRICULTURAL SUPPLIES'),
	('H288','EQUIPMENT AND MATERIALS TESTING- LIVE ANIMALS'),
	('H289','EQUIPMENT AND MATERIALS TESTING- SUBSISTENCE'),
	('H291','EQUIPMENT AND MATERIALS TESTING- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('H293','EQUIPMENT AND MATERIALS TESTING- NONMETALLIC FABRICATED MATERIALS'),
	('H294','EQUIPMENT AND MATERIALS TESTING- NONMETALLIC CRUDE MATERIALS'),
	('H295','EQUIPMENT AND MATERIALS TESTING- METAL BARS, SHEETS, AND SHAPES'),
	('H296','EQUIPMENT AND MATERIALS TESTING- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('H299','EQUIPMENT AND MATERIALS TESTING- MISCELLANEOUS'),
	('H3','INSPECTION SERVICES'),
	('H310','INSPECTION- WEAPONS'),
	('H311','INSPECTION- NUCLEAR ORDNANCE'),
	('H312','INSPECTION- FIRE CONTROL EQUIPMENT'),
	('H313','INSPECTION- AMMUNITION AND EXPLOSIVES'),
	('H314','INSPECTION- GUIDED MISSILES'),
	('H315','INSPECTION- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('H316','INSPECTION- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('H317','INSPECTION- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('H318','INSPECTION- SPACE VEHICLES'),
	('H319','INSPECTION- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('H320','INSPECTION- SHIP AND MARINE EQUIPMENT'),
	('H322','INSPECTION- RAILWAY EQUIPMENT'),
	('H323','INSPECTION- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('H324','INSPECTION- TRACTORS'),
	('H325','INSPECTION- VEHICULAR EQUIPMENT COMPONENTS'),
	('H326','INSPECTION- TIRES AND TUBES'),
	('H328','INSPECTION- ENGINES, TURBINES, AND COMPONENTS'),
	('H329','INSPECTION- ENGINE ACCESSORIES'),
	('H330','INSPECTION- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('H331','INSPECTION- BEARINGS'),
	('H332','INSPECTION- WOODWORKING MACHINERY AND EQUIPMENT'),
	('H334','INSPECTION- METALWORKING MACHINERY'),
	('H335','INSPECTION- SERVICE AND TRADE EQUIPMENT'),
	('H336','INSPECTION- SPECIAL INDUSTRY MACHINERY'),
	('H337','INSPECTION- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('H338','INSPECTION- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('H339','INSPECTION- MATERIALS HANDLING EQUIPMENT'),
	('H340','INSPECTION- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('H341','INSPECTION- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('H342','INSPECTION- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('H343','INSPECTION- PUMPS AND COMPRESSORS'),
	('H344','INSPECTION- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('H345','INSPECTION- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('H346','INSPECTION- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('H347','INSPECTION- PIPE, TUBING, HOSE, AND FITTINGS'),
	('H348','INSPECTION- VALVES'),
	('H349','INSPECTION- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('H351','INSPECTION- HAND TOOLS'),
	('H352','INSPECTION- MEASURING TOOLS'),
	('H353','INSPECTION- HARDWARE AND ABRASIVES'),
	('H354','INSPECTION- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('H355','INSPECTION- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('H356','INSPECTION- CONSTRUCTION AND BUILDING MATERIALS'),
	('H358','INSPECTION- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('H359','INSPECTION- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('H360','INSPECTION- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('H361','INSPECTION- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('H362','INSPECTION- LIGHTING FIXTURES AND LAMPS'),
	('H363','INSPECTION- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('H365','INSPECTION- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('H366','INSPECTION- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('H367','INSPECTION- PHOTOGRAPHIC EQUIPMENT'),
	('H368','INSPECTION- CHEMICALS AND CHEMICAL PRODUCTS'),
	('H369','INSPECTION- TRAINING AIDS AND DEVICES'),
	('H370','INSPECTION- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('H371','INSPECTION- FURNITURE'),
	('H372','INSPECTION- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('H373','INSPECTION- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('H374','INSPECTION- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('H375','INSPECTION- OFFICE SUPPLIES AND DEVICES'),
	('H376','INSPECTION- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('H377','INSPECTION- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('H378','INSPECTION- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('H379','INSPECTION- CLEANING EQUIPMENT AND SUPPLIES'),
	('H380','INSPECTION- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('H381','INSPECTION- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('H383','INSPECTION- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('H384','INSPECTION- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('H385','INSPECTION- TOILETRIES'),
	('H387','INSPECTION- AGRICULTURAL SUPPLIES'),
	('H388','INSPECTION- LIVE ANIMALS'),
	('H389','INSPECTION- SUBSISTENCE'),
	('H391','INSPECTION- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('H393','INSPECTION- NONMETALLIC FABRICATED MATERIALS'),
	('H394','INSPECTION- NONMETALLIC CRUDE MATERIALS'),
	('H395','INSPECTION- METAL BARS, SHEETS, AND SHAPES'),
	('H396','INSPECTION- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('H399','INSPECTION- MISCELLANEOUS'),
	('H9','OTHER QUALITY, TEST, INSPECT SVCS'),
	('H910','OTHER QC/TEST/INSPECT- WEAPONS'),
	('H911','OTHER QC/TEST/INSPECT- NUCLEAR ORDNANCE')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('H912','OTHER QC/TEST/INSPECT- FIRE CONTROL EQUIPMENT'),
	('H913','OTHER QC/TEST/INSPECT- AMMUNITION AND EXPLOSIVES'),
	('H914','OTHER QC/TEST/INSPECT- GUIDED MISSILES'),
	('H915','OTHER QC/TEST/INSPECT- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('H916','OTHER QC/TEST/INSPECT- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('H917','OTHER QC/TEST/INSPECT- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('H918','OTHER QC/TEST/INSPECT- SPACE VEHICLES'),
	('H919','OTHER QC/TEST/INSPECT- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('H920','OTHER QC/TEST/INSPECT- SHIP AND MARINE EQUIPMENT'),
	('H922','OTHER QC/TEST/INSPECT- RAILWAY EQUIPMENT'),
	('H923','OTHER QC/TEST/INSPECT- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('H924','OTHER QC/TEST/INSPECT- TRACTORS'),
	('H925','OTHER QC/TEST/INSPECT- VEHICULAR EQUIPMENT COMPONENTS'),
	('H926','OTHER QC/TEST/INSPECT- TIRES AND TUBES'),
	('H928','OTHER QC/TEST/INSPECT- ENGINES, TURBINES, AND COMPONENTS'),
	('H929','OTHER QC/TEST/INSPECT- ENGINE ACCESSORIES'),
	('H930','OTHER QC/TEST/INSPECT- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('H931','OTHER QC/TEST/INSPECT- BEARINGS'),
	('H932','OTHER QC/TEST/INSPECT- WOODWORKING MACHINERY AND EQUIPMENT'),
	('H934','OTHER QC/TEST/INSPECT- METALWORKING MACHINERY'),
	('H935','OTHER QC/TEST/INSPECT- SERVICE AND TRADE EQUIPMENT'),
	('H936','OTHER QC/TEST/INSPECT- SPECIAL INDUSTRY MACHINERY'),
	('H937','OTHER QC/TEST/INSPECT- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('H938','OTHER QC/TEST/INSPECT- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('H939','OTHER QC/TEST/INSPECT- MATERIALS HANDLING EQUIPMENT'),
	('H940','OTHER QC/TEST/INSPECT- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('H941','OTHER QC/TEST/INSPECT- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('H942','OTHER QC/TEST/INSPECT- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('H943','OTHER QC/TEST/INSPECT- PUMPS AND COMPRESSORS'),
	('H944','OTHER QC/TEST/INSPECT- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('H945','OTHER QC/TEST/INSPECT- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('H946','OTHER QC/TEST/INSPECT- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('H947','OTHER QC/TEST/INSPECT- PIPE, TUBING, HOSE, AND FITTINGS'),
	('H948','OTHER QC/TEST/INSPECT- VALVES'),
	('H949','OTHER QC/TEST/INSPECT- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('H951','OTHER QC/TEST/INSPECT- HAND TOOLS'),
	('H952','OTHER QC/TEST/INSPECT- MEASURING TOOLS'),
	('H953','OTHER QC/TEST/INSPECT- HARDWARE AND ABRASIVES'),
	('H954','OTHER QC/TEST/INSPECT- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('H955','OTHER QC/TEST/INSPECT- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('H956','OTHER QC/TEST/INSPECT- CONSTRUCTION AND BUILDING MATERIALS'),
	('H958','OTHER QC/TEST/INSPECT- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('H959','OTHER QC/TEST/INSPECT- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('H960','OTHER QC/TEST/INSPECT- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('H961','OTHER QC/TEST/INSPECT- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('H962','OTHER QC/TEST/INSPECT- LIGHTING FIXTURES AND LAMPS'),
	('H963','OTHER QC/TEST/INSPECT- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('H965','OTHER QC/TEST/INSPECT- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('H966','OTHER QC/TEST/INSPECT- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('H967','OTHER QC/TEST/INSPECT- PHOTOGRAPHIC EQUIPMENT'),
	('H968','OTHER QC/TEST/INSPECT- CHEMICALS AND CHEMICAL PRODUCTS'),
	('H969','OTHER QC/TEST/INSPECT- TRAINING AIDS AND DEVICES'),
	('H970','OTHER QC/TEST/INSPECT- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('H971','OTHER QC/TEST/INSPECT- FURNITURE'),
	('H972','OTHER QC/TEST/INSPECT- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('H973','OTHER QC/TEST/INSPECT- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('H974','OTHER QC/TEST/INSPECT- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('H975','OTHER QC/TEST/INSPECT- OFFICE SUPPLIES AND DEVICES'),
	('H976','OTHER QC/TEST/INSPECT- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('H977','OTHER QC/TEST/INSPECT- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('H978','OTHER QC/TEST/INSPECT- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('H979','OTHER QC/TEST/INSPECT- CLEANING EQUIPMENT AND SUPPLIES'),
	('H980','OTHER QC/TEST/INSPECT- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('H981','OTHER QC/TEST/INSPECT- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('H983','OTHER QC/TEST/INSPECT- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('H984','OTHER QC/TEST/INSPECT- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('H985','OTHER QC/TEST/INSPECT- TOILETRIES'),
	('H987','OTHER QC/TEST/INSPECT- AGRICULTURAL SUPPLIES'),
	('H988','OTHER QC/TEST/INSPECT- LIVE ANIMALS'),
	('H989','OTHER QC/TEST/INSPECT- SUBSISTENCE'),
	('H991','OTHER QC/TEST/INSPECT- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('H993','OTHER QC/TEST/INSPECT- NONMETALLIC FABRICATED MATERIALS'),
	('H994','OTHER QC/TEST/INSPECT- NONMETALLIC CRUDE MATERIALS'),
	('H995','OTHER QC/TEST/INSPECT- METAL BARS, SHEETS, AND SHAPES'),
	('H996','OTHER QC/TEST/INSPECT- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('H999','OTHER QC/TEST/INSPECT- MISCELLANEOUS'),
	('J','MAINT, REPAIR, REBUILD EQUIPMENT'),
	('J0','MAINT, REPAIR, REBUILD OF EQUIPMENT'),
	('J010','MAINT/REPAIR/REBUILD OF EQUIPMENT- WEAPONS'),
	('J011','MAINT/REPAIR/REBUILD OF EQUIPMENT- NUCLEAR ORDNANCE'),
	('J012','MAINT/REPAIR/REBUILD OF EQUIPMENT- FIRE CONTROL EQUIPMENT'),
	('J013','MAINT/REPAIR/REBUILD OF EQUIPMENT- AMMUNITION AND EXPLOSIVES'),
	('J014','MAINT/REPAIR/REBUILD OF EQUIPMENT- GUIDED MISSILES'),
	('J015','MAINT/REPAIR/REBUILD OF EQUIPMENT- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('J016','MAINT/REPAIR/REBUILD OF EQUIPMENT- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('J017','MAINT/REPAIR/REBUILD OF EQUIPMENT- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('J018','MAINT/REPAIR/REBUILD OF EQUIPMENT- SPACE VEHICLES'),
	('J019','MAINT/REPAIR/REBUILD OF EQUIPMENT- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('J020','MAINT/REPAIR/REBUILD OF EQUIPMENT- SHIP AND MARINE EQUIPMENT'),
	('J022','MAINT/REPAIR/REBUILD OF EQUIPMENT- RAILWAY EQUIPMENT'),
	('J023','MAINT/REPAIR/REBUILD OF EQUIPMENT- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('J024','MAINT/REPAIR/REBUILD OF EQUIPMENT- TRACTORS'),
	('J025','MAINT/REPAIR/REBUILD OF EQUIPMENT- VEHICULAR EQUIPMENT COMPONENTS'),
	('J026','MAINT/REPAIR/REBUILD OF EQUIPMENT- TIRES AND TUBES'),
	('J028','MAINT/REPAIR/REBUILD OF EQUIPMENT- ENGINES, TURBINES, AND COMPONENTS'),
	('J029','MAINT/REPAIR/REBUILD OF EQUIPMENT- ENGINE ACCESSORIES'),
	('J030','MAINT/REPAIR/REBUILD OF EQUIPMENT- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('J031','MAINT/REPAIR/REBUILD OF EQUIPMENT- BEARINGS'),
	('J032','MAINT/REPAIR/REBUILD OF EQUIPMENT- WOODWORKING MACHINERY AND EQUIPMENT'),
	('J034','MAINT/REPAIR/REBUILD OF EQUIPMENT- METALWORKING MACHINERY'),
	('J035','MAINT/REPAIR/REBUILD OF EQUIPMENT- SERVICE AND TRADE EQUIPMENT'),
	('J036','MAINT/REPAIR/REBUILD OF EQUIPMENT- SPECIAL INDUSTRY MACHINERY'),
	('J037','MAINT/REPAIR/REBUILD OF EQUIPMENT- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('J038','MAINT/REPAIR/REBUILD OF EQUIPMENT- CONSTRUCTION/MINING/EXCAVATING/HIGHWAY MAINTENANCE EQUIPMENT'),
	('J039','MAINT/REPAIR/REBUILD OF EQUIPMENT- MATERIALS HANDLING EQUIPMENT'),
	('J040','MAINT/REPAIR/REBUILD OF EQUIPMENT- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('J041','MAINT/REPAIR/REBUILD OF EQUIPMENT- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('J042','MAINT/REPAIR/REBUILD OF EQUIP- FIRE FIGHTING/RESCUE/SAFETY EQUIP; ENVIRON PROTECT EQUIP/MATLS'),
	('J043','MAINT/REPAIR/REBUILD OF EQUIPMENT- PUMPS AND COMPRESSORS'),
	('J044','MAINT/REPAIR/REBUILD OF EQUIPMENT- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('J045','MAINT/REPAIR/REBUILD OF EQUIPMENT- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('J046','MAINT/REPAIR/REBUILD OF EQUIPMENT- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('J047','MAINT/REPAIR/REBUILD OF EQUIPMENT- PIPE, TUBING, HOSE, AND FITTINGS'),
	('J048','MAINT/REPAIR/REBUILD OF EQUIPMENT- VALVES'),
	('J049','MAINT/REPAIR/REBUILD OF EQUIPMENT- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('J051','MAINT/REPAIR/REBUILD OF EQUIPMENT- HAND TOOLS'),
	('J052','MAINT/REPAIR/REBUILD OF EQUIPMENT- MEASURING TOOLS'),
	('J053','MAINT/REPAIR/REBUILD OF EQUIPMENT- HARDWARE AND ABRASIVES'),
	('J054','MAINT/REPAIR/REBUILD OF EQUIPMENT- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('J055','MAINT/REPAIR/REBUILD OF EQUIPMENT- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('J056','MAINT/REPAIR/REBUILD OF EQUIPMENT- CONSTRUCTION AND BUILDING MATERIALS'),
	('J058','MAINT/REPAIR/REBUILD OF EQUIPMENT- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('J059','MAINT/REPAIR/REBUILD OF EQUIPMENT- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('J060','MAINT/REPAIR/REBUILD OF EQUIPMENT- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('J061','MAINT/REPAIR/REBUILD OF EQUIPMENT- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('J062','MAINT/REPAIR/REBUILD OF EQUIPMENT- LIGHTING FIXTURES AND LAMPS'),
	('J063','MAINT/REPAIR/REBUILD OF EQUIPMENT- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('J065','MAINT/REPAIR/REBUILD OF EQUIPMENT- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('J066','MAINT/REPAIR/REBUILD OF EQUIPMENT- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('J067','MAINT/REPAIR/REBUILD OF EQUIPMENT- PHOTOGRAPHIC EQUIPMENT'),
	('J068','MAINT/REPAIR/REBUILD OF EQUIPMENT- CHEMICALS AND CHEMICAL PRODUCTS'),
	('J069','MAINT/REPAIR/REBUILD OF EQUIPMENT- TRAINING AIDS AND DEVICES'),
	('J070','MAINT/REPAIR/REBUILD OF EQUIPMENT- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('J071','MAINT/REPAIR/REBUILD OF EQUIPMENT- FURNITURE'),
	('J072','MAINT/REPAIR/REBUILD OF EQUIPMENT- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('J073','MAINT/REPAIR/REBUILD OF EQUIPMENT- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('J074','MAINT/REPAIR/REBUILD OF EQUIPMENT- OFFICE MACHINES/TEXT PROCESSING SYS/VISIBLE RECORD EQUIPMENT'),
	('J075','MAINT/REPAIR/REBUILD OF EQUIPMENT- OFFICE SUPPLIES AND DEVICES'),
	('J076','MAINT/REPAIR/REBUILD OF EQUIPMENT- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('J077','MAINT/REPAIR/REBUILD OF EQUIPMENT- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('J078','MAINT/REPAIR/REBUILD OF EQUIPMENT- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('J079','MAINT/REPAIR/REBUILD OF EQUIPMENT- CLEANING EQUIPMENT AND SUPPLIES'),
	('J080','MAINT/REPAIR/REBUILD OF EQUIPMENT- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('J081','MAINT/REPAIR/REBUILD OF EQUIPMENT- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('J083','MAINT/REPAIR/REBUILD OF EQUIPMENT- TEXTILES, LEATHER, FURS, APPAREL/SHOE FINDINGS, TENTS/FLAGS'),
	('J084','MAINT/REPAIR/REBUILD OF EQUIPMENT- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('J085','MAINT/REPAIR/REBUILD OF EQUIPMENT- TOILETRIES'),
	('J087','MAINT/REPAIR/REBUILD OF EQUIPMENT- AGRICULTURAL SUPPLIES'),
	('J088','MAINT/REPAIR/REBUILD OF EQUIPMENT- LIVE ANIMALS'),
	('J089','MAINT/REPAIR/REBUILD OF EQUIPMENT- SUBSISTENCE'),
	('J091','MAINT/REPAIR/REBUILD OF EQUIPMENT- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('J093','MAINT/REPAIR/REBUILD OF EQUIPMENT- NONMETALLIC FABRICATED MATERIALS'),
	('J094','MAINT/REPAIR/REBUILD OF EQUIPMENT- NONMETALLIC CRUDE MATERIALS'),
	('J095','MAINT/REPAIR/REBUILD OF EQUIPMENT- METAL BARS, SHEETS, AND SHAPES'),
	('J096','MAINT/REPAIR/REBUILD OF EQUIPMENT- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('J099','MAINT/REPAIR/REBUILD OF EQUIPMENT- MISCELLANEOUS'),
	('J9','NON-NUCLEAR SHIP REPAIR'),
	('J998','NON-NUCLEAR SHIP REPAIR (EAST)'),
	('J999','NON-NUCLEAR SHIP REPAIR (WEST)'),
	('K','MODIFICATION OF EQUIPMENT'),
	('K0','MODIFICATION OF EQUIPMENT'),
	('K010','MODIFICATION OF EQUIPMENT- WEAPONS'),
	('K011','MODIFICATION OF EQUIPMENT- NUCLEAR ORDNANCE'),
	('K012','MODIFICATION OF EQUIPMENT- FIRE CONTROL EQUIPMENT'),
	('K013','MODIFICATION OF EQUIPMENT- AMMUNITION AND EXPLOSIVES'),
	('K014','MODIFICATION OF EQUIPMENT- GUIDED MISSILES'),
	('K015','MODIFICATION OF EQUIPMENT- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('K016','MODIFICATION OF EQUIPMENT- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('K017','MODIFICATION OF EQUIPMENT- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('K018','MODIFICATION OF EQUIPMENT- SPACE VEHICLES'),
	('K019','MODIFICATION OF EQUIPMENT- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('K020','MODIFICATION OF EQUIPMENT- SHIP AND MARINE EQUIPMENT'),
	('K022','MODIFICATION OF EQUIPMENT- RAILWAY EQUIPMENT'),
	('K023','MODIFICATION OF EQUIPMENT- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('K024','MODIFICATION OF EQUIPMENT- TRACTORS'),
	('K025','MODIFICATION OF EQUIPMENT- VEHICULAR EQUIPMENT COMPONENTS'),
	('K026','MODIFICATION OF EQUIPMENT- TIRES AND TUBES'),
	('K028','MODIFICATION OF EQUIPMENT- ENGINES, TURBINES, AND COMPONENTS'),
	('K029','MODIFICATION OF EQUIPMENT- ENGINE ACCESSORIES'),
	('K030','MODIFICATION OF EQUIPMENT- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('K031','MODIFICATION OF EQUIPMENT- BEARINGS'),
	('K032','MODIFICATION OF EQUIPMENT- WOODWORKING MACHINERY AND EQUIPMENT'),
	('K034','MODIFICATION OF EQUIPMENT- METALWORKING MACHINERY'),
	('K035','MODIFICATION OF EQUIPMENT- SERVICE AND TRADE EQUIPMENT'),
	('K036','MODIFICATION OF EQUIPMENT- SPECIAL INDUSTRY MACHINERY'),
	('K037','MODIFICATION OF EQUIPMENT- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('K038','MODIFICATION OF EQUIPMENT- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('K039','MODIFICATION OF EQUIPMENT- MATERIALS HANDLING EQUIPMENT'),
	('K040','MODIFICATION OF EQUIPMENT- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('K041','MODIFICATION OF EQUIPMENT- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('K042','MODIFICATION OF EQUIPMENT- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('K043','MODIFICATION OF EQUIPMENT- PUMPS AND COMPRESSORS'),
	('K044','MODIFICATION OF EQUIPMENT- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('K045','MODIFICATION OF EQUIPMENT- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('K046','MODIFICATION OF EQUIPMENT- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('K047','MODIFICATION OF EQUIPMENT- PIPE, TUBING, HOSE, AND FITTINGS'),
	('K048','MODIFICATION OF EQUIPMENT- VALVES'),
	('K049','MODIFICATION OF EQUIPMENT- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('K051','MODIFICATION OF EQUIPMENT- HAND TOOLS'),
	('K052','MODIFICATION OF EQUIPMENT- MEASURING TOOLS'),
	('K053','MODIFICATION OF EQUIPMENT- HARDWARE AND ABRASIVES'),
	('K054','MODIFICATION OF EQUIPMENT- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('K055','MODIFICATION OF EQUIPMENT- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('K056','MODIFICATION OF EQUIPMENT- CONSTRUCTION AND BUILDING MATERIALS'),
	('K058','MODIFICATION OF EQUIPMENT- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('K059','MODIFICATION OF EQUIPMENT- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('K060','MODIFICATION OF EQUIPMENT- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('K061','MODIFICATION OF EQUIPMENT- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('K062','MODIFICATION OF EQUIPMENT- LIGHTING FIXTURES AND LAMPS'),
	('K063','MODIFICATION OF EQUIPMENT- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('K065','MODIFICATION OF EQUIPMENT- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('K066','MODIFICATION OF EQUIPMENT- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('K067','MODIFICATION OF EQUIPMENT- PHOTOGRAPHIC EQUIPMENT'),
	('K068','MODIFICATION OF EQUIPMENT- CHEMICALS AND CHEMICAL PRODUCTS'),
	('K069','MODIFICATION OF EQUIPMENT- TRAINING AIDS AND DEVICES'),
	('K070','MODIFICATION OF EQUIPMENT- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('K071','MODIFICATION OF EQUIPMENT- FURNITURE'),
	('K072','MODIFICATION OF EQUIPMENT- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('K073','MODIFICATION OF EQUIPMENT- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('K074','MODIFICATION OF EQUIPMENT- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('K075','MODIFICATION OF EQUIPMENT- OFFICE SUPPLIES AND DEVICES'),
	('K076','MODIFICATION OF EQUIPMENT- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('K077','MODIFICATION OF EQUIPMENT- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('K078','MODIFICATION OF EQUIPMENT- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('K079','MODIFICATION OF EQUIPMENT- CLEANING EQUIPMENT AND SUPPLIES'),
	('K080','MODIFICATION OF EQUIPMENT- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('K081','MODIFICATION OF EQUIPMENT- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('K083','MODIFICATION OF EQUIPMENT- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('K084','MODIFICATION OF EQUIPMENT- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('K085','MODIFICATION OF EQUIPMENT- TOILETRIES'),
	('K087','MODIFICATION OF EQUIPMENT- AGRICULTURAL SUPPLIES'),
	('K088','MODIFICATION OF EQUIPMENT- LIVE ANIMALS'),
	('K089','MODIFICATION OF EQUIPMENT- SUBSISTENCE'),
	('K091','MODIFICATION OF EQUIPMENT- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('K093','MODIFICATION OF EQUIPMENT- NONMETALLIC FABRICATED MATERIALS'),
	('K094','MODIFICATION OF EQUIPMENT- NONMETALLIC CRUDE MATERIALS'),
	('K095','MODIFICATION OF EQUIPMENT- METAL BARS, SHEETS, AND SHAPES'),
	('K096','MODIFICATION OF EQUIPMENT- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('K099','MODIFICATION OF EQUIPMENT- MISCELLANEOUS'),
	('L','TECHNICAL REPRESENTATIVE SVCS.'),
	('L0','TECHNICAL REPRESENTATIVE SERVICES'),
	('L010','TECHNICAL REPRESENTATIVE- WEAPONS'),
	('L011','TECHNICAL REPRESENTATIVE- NUCLEAR ORDNANCE'),
	('L012','TECHNICAL REPRESENTATIVE- FIRE CONTROL EQUIPMENT'),
	('L013','TECHNICAL REPRESENTATIVE- AMMUNITION AND EXPLOSIVES'),
	('L014','TECHNICAL REPRESENTATIVE- GUIDED MISSILES'),
	('L015','TECHNICAL REPRESENTATIVE- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('L016','TECHNICAL REPRESENTATIVE- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('L017','TECHNICAL REPRESENTATIVE- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('L018','TECHNICAL REPRESENTATIVE- SPACE VEHICLES'),
	('L019','TECHNICAL REPRESENTATIVE- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('L020','TECHNICAL REPRESENTATIVE- SHIP AND MARINE EQUIPMENT'),
	('L022','TECHNICAL REPRESENTATIVE- RAILWAY EQUIPMENT'),
	('L023','TECHNICAL REPRESENTATIVE- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('L024','TECHNICAL REPRESENTATIVE- TRACTORS'),
	('L025','TECHNICAL REPRESENTATIVE- VEHICULAR EQUIPMENT COMPONENTS'),
	('L026','TECHNICAL REPRESENTATIVE- TIRES AND TUBES'),
	('L028','TECHNICAL REPRESENTATIVE- ENGINES, TURBINES, AND COMPONENTS'),
	('L029','TECHNICAL REPRESENTATIVE- ENGINE ACCESSORIES'),
	('L030','TECHNICAL REPRESENTATIVE- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('L031','TECHNICAL REPRESENTATIVE- BEARINGS'),
	('L032','TECHNICAL REPRESENTATIVE- WOODWORKING MACHINERY AND EQUIPMENT'),
	('L034','TECHNICAL REPRESENTATIVE- METALWORKING MACHINERY'),
	('L035','TECHNICAL REPRESENTATIVE- SERVICE AND TRADE EQUIPMENT'),
	('L036','TECHNICAL REPRESENTATIVE- SPECIAL INDUSTRY MACHINERY'),
	('L037','TECHNICAL REPRESENTATIVE- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('L038','TECHNICAL REPRESENTATIVE- CONSTRUCTION/MINING/EXCAVATING/HIGHWAY MAINT EQUIP'),
	('L039','TECHNICAL REPRESENTATIVE- MATERIALS HANDLING EQUIPMENT'),
	('L040','TECHNICAL REPRESENTATIVE- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('L041','TECHNICAL REPRESENTATIVE- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('L042','TECHNICAL REP- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('L043','TECHNICAL REPRESENTATIVE- PUMPS AND COMPRESSORS'),
	('L044','TECHNICAL REPRESENTATIVE- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('L045','TECHNICAL REPRESENTATIVE- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('L046','TECHNICAL REPRESENTATIVE- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('L047','TECHNICAL REPRESENTATIVE- PIPE, TUBING, HOSE, AND FITTINGS'),
	('L048','TECHNICAL REPRESENTATIVE- VALVES'),
	('L049','TECHNICAL REPRESENTATIVE- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('L051','TECHNICAL REPRESENTATIVE- HAND TOOLS'),
	('L052','TECHNICAL REPRESENTATIVE- MEASURING TOOLS'),
	('L053','TECHNICAL REPRESENTATIVE- HARDWARE AND ABRASIVES'),
	('L054','TECHNICAL REPRESENTATIVE- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('L055','TECHNICAL REPRESENTATIVE- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('L056','TECHNICAL REPRESENTATIVE- CONSTRUCTION AND BUILDING MATERIALS'),
	('L058','TECHNICAL REPRESENTATIVE- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('L059','TECHNICAL REPRESENTATIVE- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('L060','TECHNICAL REPRESENTATIVE- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('L061','TECHNICAL REPRESENTATIVE- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('L062','TECHNICAL REPRESENTATIVE- LIGHTING FIXTURES AND LAMPS'),
	('L063','TECHNICAL REPRESENTATIVE- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('L065','TECHNICAL REPRESENTATIVE- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('L066','TECHNICAL REPRESENTATIVE- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('L067','TECHNICAL REPRESENTATIVE- PHOTOGRAPHIC EQUIPMENT'),
	('L068','TECHNICAL REPRESENTATIVE- CHEMICALS AND CHEMICAL PRODUCTS'),
	('L069','TECHNICAL REPRESENTATIVE- TRAINING AIDS AND DEVICES'),
	('L070','TECHNICAL REPRESENTATIVE- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('L071','TECHNICAL REPRESENTATIVE- FURNITURE'),
	('L072','TECHNICAL REPRESENTATIVE- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('L073','TECHNICAL REPRESENTATIVE- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('L074','TECHNICAL REPRESENTATIVE- OFFICE MACHINES/TEXT PROCESSING SYS/VISIBLE RECORD EQUIPMENT'),
	('L075','TECHNICAL REPRESENTATIVE- OFFICE SUPPLIES AND DEVICES'),
	('L076','TECHNICAL REPRESENTATIVE- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('L077','TECHNICAL REPRESENTATIVE- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('L078','TECHNICAL REPRESENTATIVE- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('L079','TECHNICAL REPRESENTATIVE- CLEANING EQUIPMENT AND SUPPLIES'),
	('L080','TECHNICAL REPRESENTATIVE- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('L081','TECHNICAL REPRESENTATIVE- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('L083','TECHNICAL REPRESENTATIVE- TEXTILES, LEATHER, FURS, APPAREL/SHOE FINDINGS, TENTS/FLAGS'),
	('L084','TECHNICAL REPRESENTATIVE- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('L085','TECHNICAL REPRESENTATIVE- TOILETRIES'),
	('L087','TECHNICAL REPRESENTATIVE- AGRICULTURAL SUPPLIES'),
	('L088','TECHNICAL REPRESENTATIVE- LIVE ANIMALS'),
	('L089','TECHNICAL REPRESENTATIVE- SUBSISTENCE'),
	('L091','TECHNICAL REPRESENTATIVE- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('L093','TECHNICAL REPRESENTATIVE- NONMETALLIC FABRICATED MATERIALS'),
	('L094','TECHNICAL REPRESENTATIVE- NONMETALLIC CRUDE MATERIALS'),
	('L095','TECHNICAL REPRESENTATIVE- METAL BARS, SHEETS, AND SHAPES'),
	('L096','TECHNICAL REPRESENTATIVE- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('L099','TECHNICAL REPRESENTATIVE- MISCELLANEOUS'),
	('M','OPERATION OF GOVT OWNED FACILITY'),
	('M1','OPERATE GOVT OWNED BUILDINGS'),
	('M1AA','OPERATION OF OFFICE BUILDINGS'),
	('M1AB','OPERATION OF CONFERENCE SPACE AND FACILITIES'),
	('M1AZ','OPERATION OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('M1BA','OPERATION OF AIR TRAFFIC CONTROL TOWERS'),
	('M1BB','OPERATION OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('M1BC','OPERATION OF RADAR AND NAVIGATIONAL FACILITIES'),
	('M1BD','OPERATION OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('M1BE','OPERATION OF AIRPORT TERMINALS'),
	('M1BF','OPERATION OF MISSILE SYSTEM FACILITIES'),
	('M1BG','OPERATION OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('M1BZ','OPERATION OF OTHER AIRFIELD STRUCTURES'),
	('M1CA','OPERATION OF SCHOOLS'),
	('M1CZ','OPERATION OF OTHER EDUCATIONAL BUILDINGS'),
	('M1DA','OPERATION OF HOSPITALS AND INFIRMARIES'),
	('M1DB','OPERATION OF LABORATORIES AND CLINICS'),
	('M1DZ','OPERATION OF OTHER HOSPITAL BUILDINGS'),
	('M1EA','OPERATION OF AMMUNITION FACILITIES'),
	('M1EB','OPERATION OF MAINTENANCE BUILDINGS'),
	('M1EC','OPERATION OF PRODUCTION BUILDINGS'),
	('M1ED','OPERATION OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('M1EE','OPERATION OF TANK AUTOMOTIVE FACILITIES'),
	('M1EZ','OPERATION OF OTHER INDUSTRIAL BUILDINGS'),
	('M1FA','OPERATION OF FAMILY HOUSING FACILITIES'),
	('M1FB','OPERATION OF RECREATIONAL BUILDINGS'),
	('M1FC','OPERATION OF TROOP HOUSING FACILITIES'),
	('M1FD','OPERATION OF DINING FACILITIES'),
	('M1FE','OPERATION OF RELIGIOUS FACILITIES'),
	('M1FF','OPERATION OF PENAL FACILITIES'),
	('M1FZ','OPERATION OF OTHER RESIDENTIAL BUILDINGS'),
	('M1GA','OPERATION OF AMMUNITION STORAGE BUILDINGS'),
	('M1GB','OPERATION OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('M1GC','OPERATION OF FUEL STORAGE BUILDINGS'),
	('M1GD','OPERATION OF OPEN STORAGE FACILITIES'),
	('M1GZ','OPERATION OF OTHER WAREHOUSE BUILDINGS'),
	('M1HA','OPERATION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('M1HB','OPERATION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('M1HC','OPERATION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('M1HZ','OPERATION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('M1JA','OPERATION OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('M1JB','OPERATION OF TESTING AND MEASUREMENT BUILDINGS'),
	('M1JZ','OPERATION OF MISCELLANEOUS BUILDINGS'),
	('M1KA','OPERATION OF DAMS'),
	('M1KB','OPERATION OF CANALS'),
	('M1KC','OPERATION OF MINE FIRE CONTROL FACILITIES'),
	('M1KD','OPERATION OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('M1KE','OPERATION OF SURFACE MINE RECLAMATION FACILITIES'),
	('M1KF','OPERATION OF DREDGING FACILITIES'),
	('M1KZ','OPERATION OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('M1LA','OPERATION OF AIRPORT SERVICE ROADS'),
	('M1LB','OPERATION OF HIGHWAYS, ROADS, STREETS, BRIDGES, AND RAILWAYS'),
	('M1LC','OPERATION OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('M1LZ','OPERATION OF PARKING FACILITIES'),
	('M1MA','OPERATION OF EPG FACILITIES - COAL'),
	('M1MB','OPERATION OF EPG FACILITIES - GAS'),
	('M1MC','OPERATION OF EPG FACILITIES - GEOTHERMAL'),
	('M1MD','OPERATION OF EPG FACILITIES - HYDRO'),
	('M1ME','OPERATION OF EPG FACILITIES - NUCLEAR'),
	('M1MF','OPERATION OF EPG FACILITIES - PETROLEUM'),
	('M1MG','OPERATION OF EPG FACILITIES - SOLAR'),
	('M1MH','OPERATION OF EPG FACILITIES - WIND'),
	('M1MZ','OPERATION OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('M1NA','OPERATION OF FUEL SUPPLY FACILITIES'),
	('M1NB','OPERATION OF HEATING AND COOLING PLANTS'),
	('M1NC','OPERATION OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('M1ND','OPERATION OF SEWAGE AND WASTE FACILITIES'),
	('M1NE','OPERATION OF WATER SUPPLY FACILITIES'),
	('M1NZ','OPERATION OF OTHER UTILITIES'),
	('M1PA','OPERATION OF RECREATION FACILITIES (NON-BUILDING)'),
	('M1PB','OPERATION OF EXHIBIT DESIGN (NON-BUILDING)'),
	('M1PC','OPERATION OF UNIMPROVED REAL PROPERTY (LAND)'),
	('M1PD','OPERATION OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('M1PZ','OPERATION OF OTHER NON-BUILDING FACILITIES'),
	('M1QA','OPERATION OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('M2','OPERATE GOV OWNED NONBUILDING FACIL'),
	('M3','OPERATE RESTORATION ACTIVITIES'),
	('N','INSTALLATION OF EQUIPMENT'),
	('N0','INSTALLATION OF EQUIPMENT'),
	('N010','INSTALLATION OF EQUIPMENT- WEAPONS'),
	('N011','INSTALLATION OF EQUIPMENT- NUCLEAR ORDNANCE'),
	('N012','INSTALLATION OF EQUIPMENT- FIRE CONTROL EQUIPMENT'),
	('N013','INSTALLATION OF EQUIPMENT- AMMUNITION AND EXPLOSIVES'),
	('N014','INSTALLATION OF EQUIPMENT- GUIDED MISSILES'),
	('N015','INSTALLATION OF EQUIPMENT- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('N016','INSTALLATION OF EQUIPMENT- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('N017','INSTALLATION OF EQUIPMENT- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('N018','INSTALLATION OF EQUIPMENT- SPACE VEHICLES'),
	('N019','INSTALLATION OF EQUIPMENT- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('N020','INSTALLATION OF EQUIPMENT- SHIP AND MARINE EQUIPMENT'),
	('N022','INSTALLATION OF EQUIPMENT- RAILWAY EQUIPMENT'),
	('N023','INSTALLATION OF EQUIPMENT- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('N024','INSTALLATION OF EQUIPMENT- TRACTORS'),
	('N025','INSTALLATION OF EQUIPMENT- VEHICULAR EQUIPMENT COMPONENTS'),
	('N026','INSTALLATION OF EQUIPMENT- TIRES AND TUBES'),
	('N028','INSTALLATION OF EQUIPMENT- ENGINES, TURBINES, AND COMPONENTS'),
	('N029','INSTALLATION OF EQUIPMENT- ENGINE ACCESSORIES'),
	('N030','INSTALLATION OF EQUIPMENT- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('N031','INSTALLATION OF EQUIPMENT- BEARINGS'),
	('N032','INSTALLATION OF EQUIPMENT- WOODWORKING MACHINERY AND EQUIPMENT'),
	('N034','INSTALLATION OF EQUIPMENT- METALWORKING MACHINERY'),
	('N035','INSTALLATION OF EQUIPMENT- SERVICE AND TRADE EQUIPMENT'),
	('N036','INSTALLATION OF EQUIPMENT- SPECIAL INDUSTRY MACHINERY'),
	('N037','INSTALLATION OF EQUIPMENT- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('N038','INSTALLATION OF EQUIPMENT- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('N039','INSTALLATION OF EQUIPMENT- MATERIALS HANDLING EQUIPMENT'),
	('N040','INSTALLATION OF EQUIPMENT- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('N041','INSTALLATION OF EQUIPMENT- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('N042','INSTALLATION OF EQUIPMENT- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('N043','INSTALLATION OF EQUIPMENT- PUMPS AND COMPRESSORS'),
	('N044','INSTALLATION OF EQUIPMENT- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('N045','INSTALLATION OF EQUIPMENT- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('N046','INSTALLATION OF EQUIPMENT- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('N047','INSTALLATION OF EQUIPMENT- PIPE, TUBING, HOSE, AND FITTINGS'),
	('N048','INSTALLATION OF EQUIPMENT- VALVES'),
	('N049','INSTALLATION OF EQUIPMENT- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('N051','INSTALLATION OF EQUIPMENT- HAND TOOLS'),
	('N052','INSTALLATION OF EQUIPMENT- MEASURING TOOLS'),
	('N053','INSTALLATION OF EQUIPMENT- HARDWARE AND ABRASIVES'),
	('N054','INSTALLATION OF EQUIPMENT- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('N055','INSTALLATION OF EQUIPMENT- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('N056','INSTALLATION OF EQUIPMENT- CONSTRUCTION AND BUILDING MATERIALS'),
	('N058','INSTALLATION OF EQUIPMENT- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('N059','INSTALLATION OF EQUIPMENT- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('N060','INSTALLATION OF EQUIPMENT- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('N061','INSTALLATION OF EQUIPMENT- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('N062','INSTALLATION OF EQUIPMENT- LIGHTING FIXTURES AND LAMPS'),
	('N063','INSTALLATION OF EQUIPMENT- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('N065','INSTALLATION OF EQUIPMENT- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('N066','INSTALLATION OF EQUIPMENT- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('N067','INSTALLATION OF EQUIPMENT- PHOTOGRAPHIC EQUIPMENT'),
	('N068','INSTALLATION OF EQUIPMENT- CHEMICALS AND CHEMICAL PRODUCTS'),
	('N069','INSTALLATION OF EQUIPMENT- TRAINING AIDS AND DEVICES'),
	('N070','INSTALLATION OF EQUIPMENT- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('N071','INSTALLATION OF EQUIPMENT- FURNITURE'),
	('N072','INSTALLATION OF EQUIPMENT- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('N073','INSTALLATION OF EQUIPMENT- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('N074','INSTALLATION OF EQUIPMENT- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('N075','INSTALLATION OF EQUIPMENT- OFFICE SUPPLIES AND DEVICES'),
	('N076','INSTALLATION OF EQUIPMENT- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('N077','INSTALLATION OF EQUIPMENT- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('N078','INSTALLATION OF EQUIPMENT- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('N079','INSTALLATION OF EQUIPMENT- CLEANING EQUIPMENT AND SUPPLIES'),
	('N080','INSTALLATION OF EQUIPMENT- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('N081','INSTALLATION OF EQUIPMENT- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('N083','INSTALLATION OF EQUIPMENT- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('N084','INSTALLATION OF EQUIPMENT- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('N085','INSTALLATION OF EQUIPMENT- TOILETRIES'),
	('N087','INSTALLATION OF EQUIPMENT- AGRICULTURAL SUPPLIES'),
	('N088','INSTALLATION OF EQUIPMENT- LIVE ANIMALS'),
	('N089','INSTALLATION OF EQUIPMENT- SUBSISTENCE'),
	('N091','INSTALLATION OF EQUIPMENT- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('N093','INSTALLATION OF EQUIPMENT- NONMETALLIC FABRICATED MATERIALS'),
	('N094','INSTALLATION OF EQUIPMENT- NONMETALLIC CRUDE MATERIALS'),
	('N095','INSTALLATION OF EQUIPMENT- METAL BARS, SHEETS, AND SHAPES'),
	('N096','INSTALLATION OF EQUIPMENT- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('N099','INSTALLATION OF EQUIPMENT- MISCELLANEOUS'),
	('P','SALVAGE SERVICES'),
	('P1','DISPOSAL OF EXCESS/SURPLUS PROPERTY'),
	('P100','SALVAGE- PREPARATION AND DISPOSAL OF EXCESS/SURPLUS PROPERTY'),
	('P2','SALVAGE OF AIRCRAFT'),
	('P200','SALVAGE- AIRCRAFT'),
	('P3','SALVAGE OF MARINE VESSELS'),
	('P300','SALVAGE- MARINE VESSELS'),
	('P4','DEMOLITION OF BUILDINGS'),
	('P400','SALVAGE- DEMOLITION OF BUILDINGS'),
	('P5','DEMOLITION OF NONBUILDING FACILITY'),
	('P500','SALVAGE- DEMOLITION OF STRUCTURES/FACILITIES (OTHER THAN BUILDINGS)'),
	('P9','OTHER SALVAGE SERVICES'),
	('P999','SALVAGE- OTHER'),
	('Q','MEDICAL SERVICES'),
	('Q1','DEPENDENT MEDICARE SERVICES'),
	('Q101','MEDICAL- DEPENDENT MEDICARE'),
	('Q2','GENERAL HEALTH CARE SERVICES'),
	('Q201','MEDICAL- GENERAL HEALTH CARE'),
	('Q3','LABORATORY TESTING SERVICES'),
	('Q301','MEDICAL- LABORATORY TESTING'),
	('Q4','NURSING, NURSING HOME, EVAL/SCREEN'),
	('Q401','MEDICAL- NURSING'),
	('Q402','MEDICAL- NURSING HOME CARE CONTRACTS'),
	('Q403','MEDICAL- EVALUATION/SCREENING')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('Q5','MEDICAL, DENTAL, AND SURGICAL SVCS'),
	('Q501','MEDICAL- ANESTHESIOLOGY'),
	('Q502','MEDICAL- CARDIO-VASCULAR'),
	('Q503','MEDICAL- DENTISTRY'),
	('Q504','MEDICAL- DERMATOLOGY'),
	('Q505','MEDICAL- GASTROENTEROLOGY'),
	('Q506','MEDICAL- GERIATRIC'),
	('Q507','MEDICAL- GYNECOLOGY'),
	('Q508','MEDICAL- HEMATOLOGY'),
	('Q509','MEDICAL- INTERNAL MEDICINE'),
	('Q510','MEDICAL- NEUROLOGY'),
	('Q511','MEDICAL- OPHTHALMOLOGY'),
	('Q512','MEDICAL- OPTOMETRY'),
	('Q513','MEDICAL- ORTHOPEDIC'),
	('Q514','MEDICAL- OTOLARYNGOLOGY'),
	('Q515','MEDICAL- PATHOLOGY'),
	('Q516','MEDICAL- PEDIATRIC'),
	('Q517','MEDICAL- PHARMACOLOGY'),
	('Q518','MEDICAL- PHYSICAL MEDICINE/REHABILITATION'),
	('Q519','MEDICAL- PSYCHIATRY'),
	('Q520','MEDICAL- PODIATRY'),
	('Q521','MEDICAL- PULMONARY'),
	('Q522','MEDICAL- RADIOLOGY'),
	('Q523','MEDICAL- SURGERY'),
	('Q524','MEDICAL- THORACIC'),
	('Q525','MEDICAL- UROLOGY'),
	('Q526','MEDICAL- MEDICAL/PSYCHIATRIC CONSULTATION'),
	('Q527','MEDICAL- NUCLEAR MEDICINE'),
	('Q9','OTHER MEDICAL SERVICES'),
	('Q999','MEDICAL- OTHER'),
	('R','SUPPORT SVCS (PROF, ADMIN, MGMT)'),
	('R1','BUILDINGS AND FACILITIES (OBSOLETE)'),
	('R2','ARCHITECT-ENGINEER (OBSOLETE CODES)'),
	('R3','ADP SERVICES (OBSOLETE CODES)'),
	('R4','PROFESSIONAL SERVICES'),
	('R401','SUPPORT- PROFESSIONAL: PERSONAL CARE (NON-MEDICAL)'),
	('R402','SUPPORT- PROFESSIONAL: REAL ESTATE BROKERAGE'),
	('R404','SUPPORT- PROFESSIONAL: LAND SURVEYS-CADASTRAL (NON-CONSTRUCTION)'),
	('R405','SUPPORT- PROFESSIONAL: OPERATIONS RESEARCH/QUANTITATIVE ANALYSIS'),
	('R406','SUPPORT- PROFESSIONAL: POLICY REVIEW/DEVELOPMENT'),
	('R408','SUPPORT- PROFESSIONAL: PROGRAM MANAGEMENT/SUPPORT'),
	('R410','SUPPORT- PROFESSIONAL: PROGRAM EVALUATION/REVIEW/DEVELOPMENT'),
	('R411','SUPPORT- PROFESSIONAL: REAL PROPERTY APPRAISALS'),
	('R412','SUPPORT- PROFESSIONAL: SIMULATION'),
	('R413','SUPPORT- PROFESSIONAL: SPECIFICATIONS DEVELOPMENT'),
	('R415','SUPPORT- PROFESSIONAL: TECHNOLOGY SHARING/UTILIZATION'),
	('R416','SUPPORT- PROFESSIONAL: VETERINARY/ANIMAL CARE'),
	('R418','SUPPORT- PROFESSIONAL: LEGAL'),
	('R420','SUPPORT- PROFESSIONAL: CERTIFICATIONS AND ACCREDITATIONS (OTHER THAN EDUC OR INFO TECH C&A)'),
	('R422','SUPPORT- PROFESSIONAL: MARKET RESEARCH/PUBLIC OPINION'),
	('R423','SUPPORT- PROFESSIONAL: INTELLIGENCE'),
	('R424','SUPPORT- PROFESSIONAL: EXPERT WITNESS'),
	('R425','SUPPORT- PROFESSIONAL: ENGINEERING/TECHNICAL'),
	('R426','SUPPORT- PROFESSIONAL: COMMUNICATIONS'),
	('R427','SUPPORT- PROFESSIONAL: WEATHER REPORTING/OBSERVATION'),
	('R428','SUPPORT- PROFESSIONAL: INDUSTRIAL HYGIENICS'),
	('R429','SUPPORT- PROFESSIONAL: EMERGENCY RESPONSE/DISASTER PLANNING/PREPAREDNESS SUPPORT'),
	('R430','SUPPORT- PROFESSIONAL: PHYSICAL SECURITY AND BADGING'),
	('R431','SUPPORT- PROFESSIONAL: HUMAN RESOURCES'),
	('R497','SUPPORT- PROFESSIONAL: PERSONAL SERVICES CONTRACTS'),
	('R498','SUPPORT- PROFESSIONAL: PATENT AND TRADEMARK'),
	('R499','SUPPORT- PROFESSIONAL: OTHER'),
	('R5','STUDIES (OBSOLETE CODES)'),
	('R6','ADMINISTRATIVE SUPPORT SERVICES'),
	('R602','SUPPORT- ADMINISTRATIVE: COURIER/MESSENGER'),
	('R603','SUPPORT- ADMINISTRATIVE: TRANSCRIPTION'),
	('R604','SUPPORT- ADMINISTRATIVE: MAILING/DISTRIBUTION'),
	('R605','SUPPORT- ADMINISTRATIVE: LIBRARY'),
	('R606','SUPPORT- ADMINISTRATIVE: COURT REPORTING'),
	('R607','SUPPORT- ADMINISTRATIVE: WORD PROCESSING/TYPING'),
	('R608','SUPPORT- ADMINISTRATIVE: TRANSLATION AND INTERPRETING'),
	('R609','SUPPORT- ADMINISTRATIVE: STENOGRAPHIC'),
	('R610','SUPPORT- ADMINISTRATIVE:- PERSONAL PROPERTY MANAGEMENT'),
	('R611','SUPPORT- ADMINISTRATIVE: CREDIT REPORTING'),
	('R612','SUPPORT- ADMINISTRATIVE: INFORMATION RETRIEVAL'),
	('R613','SUPPORT- ADMINISTRATIVE: POST OFFICE'),
	('R614','SUPPORT- ADMINISTRATIVE: PAPER SHREDDING'),
	('R615','SUPPORT- ADMINISTRATIVE: BACKGROUND INVESTIGATION'),
	('R699','SUPPORT- ADMINISTRATIVE: OTHER'),
	('R7','MANAGEMENT SUPPORT SERVICES'),
	('R701','SUPPORT- MANAGEMENT: ADVERTISING'),
	('R702','SUPPORT- MANAGEMENT: DATA COLLECTION'),
	('R703','SUPPORT- MANAGEMENT: ACCOUNTING'),
	('R704','SUPPORT- MANAGEMENT: AUDITING'),
	('R705','SUPPORT- MANAGEMENT: DEBT COLLECTION'),
	('R706','SUPPORT- MANAGEMENT: LOGISTICS SUPPORT'),
	('R707','SUPPORT- MANAGEMENT: CONTRACT/PROCUREMENT/ACQUISITION SUPPORT'),
	('R708','SUPPORT- MANAGEMENT: PUBLIC RELATIONS'),
	('R710','SUPPORT- MANAGEMENT: FINANCIAL'),
	('R711','SUPPORT- MANAGEMENT: BANKING'),
	('R712','SUPPORT- MANAGEMENT: COIN MINTING'),
	('R713','SUPPORT- MANAGEMENT: BANKNOTE PRINTING'),
	('R799','SUPPORT- MANAGEMENT: OTHER'),
	('S','UTILITIES AND HOUSEKEEPING'),
	('S1','UTILITIES'),
	('S111','UTILITIES- GAS'),
	('S112','UTILITIES- ELECTRIC'),
	('S114','UTILITIES- WATER'),
	('S119','UTILITIES- OTHER'),
	('S2','HOUSEKEEPING SERVICES'),
	('S201','HOUSEKEEPING- CUSTODIAL JANITORIAL'),
	('S202','HOUSEKEEPING- FIRE PROTECTION'),
	('S203','HOUSEKEEPING- FOOD'),
	('S204','HOUSEKEEPING- FUELING AND OTHER PETROLEUM'),
	('S205','HOUSEKEEPING- TRASH/GARBAGE COLLECTION'),
	('S206','HOUSEKEEPING- GUARD'),
	('S207','HOUSEKEEPING- INSECT/RODENT CONTROL'),
	('S208','HOUSEKEEPING- LANDSCAPING/GROUNDSKEEPING'),
	('S209','HOUSEKEEPING- LAUNDRY/DRYCLEANING'),
	('S211','HOUSEKEEPING- SURVEILLANCE'),
	('S212','HOUSEKEEPING- SOLID FUEL HANDLING'),
	('S214','HOUSEKEEPING- CARPET LAYING/CLEANING'),
	('S215','HOUSEKEEPING- WAREHOUSING/STORAGE'),
	('S216','HOUSEKEEPING- FACILITIES OPERATIONS SUPPORT'),
	('S217','HOUSEKEEPING- INTERIOR PLANTSCAPING'),
	('S218','HOUSEKEEPING- SNOW REMOVAL/SALT'),
	('S222','HOUSEKEEPING- WASTE TREATMENT/STORAGE'),
	('S299','HOUSEKEEPING- OTHER'),
	('T','PHOTO, MAP, PRINT, PUBLICATION'),
	('T0','PHOTOGR, MAPPING, PRINTING, PUBLISH'),
	('T001','PHOTO/MAP/PRINT/PUBLICATION- ARTS/GRAPHICS'),
	('T002','PHOTO/MAP/PRINT/PUBLICATION- CARTOGRAPHY'),
	('T003','PHOTO/MAP/PRINT/PUBLICATION- CATALOGING'),
	('T004','PHOTO/MAP/PRINT/PUBLICATION- CHARTING'),
	('T005','PHOTO/MAP/PRINT/PUBLICATION- FILM PROCESSING'),
	('T006','PHOTO/MAP/PRINT/PUBLICATION- FILM/VIDEO TAPE PRODUCTION'),
	('T007','PHOTO/MAP/PRINT/PUBLICATION- MICROFORM'),
	('T008','PHOTO/MAP/PRINT/PUBLICATION- PHOTOGRAMMETRY'),
	('T009','PHOTO/MAP/PRINT/PUBLICATION- AERIAL PHOTOGRAPHIC'),
	('T010','PHOTO/MAP/PRINT/PUBLICATION- GENERAL PHOTOGRAPHIC: STILL'),
	('T011','PHOTO/MAP/PRINT/PUBLICATION- PRINT/BINDING'),
	('T012','PHOTO/MAP/PRINT/PUBLICATION- REPRODUCTION'),
	('T013','PHOTO/MAP/PRINT/PUBLICATION- TECHNICAL WRITING'),
	('T014','PHOTO/MAP/PRINT/PUBLICATION- TOPOGRAPHY'),
	('T015','PHOTO/MAP/PRINT/PUBLICATION- GENERAL PHOTOGRAPHIC: MOTION'),
	('T016','PHOTO/MAP/PRINT/PUBLICATION- AUDIO/VISUAL'),
	('T099','PHOTO/MAP/PRINT/PUBLICATION- OTHER'),
	('U','EDUCATION AND TRAINING'),
	('U0','EDUCATION AND TRAINING SERVICES'),
	('U001','EDUCATION/TRAINING- LECTURES'),
	('U002','EDUCATION/TRAINING- PERSONNEL TESTING'),
	('U003','EDUCATION/TRAINING- RESERVE TRAINING (MILITARY)'),
	('U004','EDUCATION/TRAINING- SCIENTIFIC/MANAGEMENT'),
	('U005','EDUCATION/TRAINING- TUITION/REGISTRATION/MEMBERSHIP FEES'),
	('U006','EDUCATION/TRAINING- VOCATIONAL/TECHNICAL'),
	('U007','EDUCATION/TRAINING- FACULTY SALARIES FOR DEPENDENT SCHOOLS'),
	('U008','EDUCATION/TRAINING- TRAINING/CURRICULUM DEVELOPMENT'),
	('U009','EDUCATION/TRAINING- GENERAL'),
	('U010','EDUCATION/TRAINING- CERTIFICATIONS/ACCREDITATIONS FOR EDUCATIONAL INSTITUTIONS'),
	('U011','EDUCATION/TRAINING- AIDS/HIV'),
	('U012','EDUCATION/TRAINING- INFORMATION TECHNOLOGY/TELECOMMUNICATIONS TRAINING'),
	('U013','EDUCATION/TRAINING- COMBAT'),
	('U014','EDUCATION/TRAINING- SECURITY'),
	('U099','EDUCATION/TRAINING- OTHER'),
	('V','TRANSPORT, TRAVEL, RELOCATION'),
	('V0','MOTOR POOL OR PACKING/CRATING'),
	('V001','TRANSPORTATION/TRAVEL/RELOCATION- MOTOR POOL AND PACKING/CRATING: GBL/GTR PROCS'),
	('V002','TRANSPORTATION/TRAVEL/RELOCATION- MOTOR POOL AND PACKING/CRATING: MOTOR POOL OPERATIONS'),
	('V003','TRANSPORTATION/TRAVEL/RELOCATION- MOTOR POOL AND PACKING/CRATING: PACKING/CRATING'),
	('V1','TRANSPORTATION OF THINGS'),
	('V111','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: AIR FREIGHT'),
	('V112','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: MOTOR FREIGHT'),
	('V113','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: RAIL FREIGHT'),
	('V114','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: STEVEDORING'),
	('V115','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: VESSEL FREIGHT'),
	('V119','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: OTHER'),
	('V121','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: AIR CHARTER'),
	('V122','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: MOTOR CHARTER'),
	('V123','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: RAIL CHARTER'),
	('V124','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: MARINE CHARTER'),
	('V125','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: VESSEL TOWING'),
	('V126','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: SPACE TRANSPORTATION/LAUNCH'),
	('V127','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: SECURITY VEHICLE'),
	('V129','TRANSPORTATION/TRAVEL/RELOCATION- TRANSPORTATION: OTHER'),
	('V2','TRAVEL, LODGING, RECRUITMENT SVCS'),
	('V211','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: AIR PASSENGER'),
	('V212','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: MOTOR PASSENGER'),
	('V213','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: RAIL PASSENGER'),
	('V214','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: MARINE PASSENGER'),
	('V221','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: PASSENGER AIR CHARTER'),
	('V222','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: PASSENGER MOTOR CHARTER'),
	('V223','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: PASSENGER RAIL CHARTER'),
	('V224','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: PASSENGER MARINE CHARTER'),
	('V225','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: AMBULANCE'),
	('V226','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: TAXICAB'),
	('V227','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: NAVIGATIONAL AID AND PILOTAGE'),
	('V228','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: PORT OPERATIONS'),
	('V229','TRANSPORT/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUIT: PURCH OF TRANSIT/PUBLIC TRANSPORT FARE MEDIA'),
	('V231','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: LODGING, HOTEL/MOTEL'),
	('V241','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: MILITARY PERSONNEL RECRUITMENT'),
	('V251','TRANSPORTATION/TRAVEL/RELOCATION- TRAVEL/LODGING/RECRUITMENT: CIVILIAN PERSONNEL RECRUITMENT'),
	('V3','RELOCATION OR TRAVEL AGENT SERVICES'),
	('V301','TRANSPORTATION/TRAVEL/RELOCATION- RELOCATION: RELOCATION'),
	('V302','TRANSPORTATION/TRAVEL/RELOCATION- RELOCATION: TRAVEL AGENT'),
	('V9','OTHER TRANSPORT, TRAVEL, RELOCAT SV'),
	('V999','TRANSPORTATION/TRAVEL/RELOCATION- OTHER: OTHER'),
	('W','LEASE/RENT EQUIPMENT'),
	('W0','LEASE OR RENTAL OF EQUIPMENT'),
	('W010','LEASE OR RENTAL OF EQUIPMENT- WEAPONS'),
	('W011','LEASE OR RENTAL OF EQUIPMENT- NUCLEAR ORDNANCE'),
	('W012','LEASE OR RENTAL OF EQUIPMENT- FIRE CONTROL EQUIPMENT'),
	('W013','LEASE OR RENTAL OF EQUIPMENT- AMMUNITION AND EXPLOSIVES'),
	('W014','LEASE OR RENTAL OF EQUIPMENT- GUIDED MISSILES'),
	('W015','LEASE OR RENTAL OF EQUIPMENT- AIRCRAFT AND AIRFRAME STRUCTURAL COMPONENTS'),
	('W016','LEASE OR RENTAL OF EQUIPMENT- AIRCRAFT COMPONENTS AND ACCESSORIES'),
	('W017','LEASE OR RENTAL OF EQUIPMENT- AIRCRAFT LAUNCHING, LANDING, AND GROUND HANDLING EQUIPMENT'),
	('W018','LEASE OR RENTAL OF EQUIPMENT- SPACE VEHICLES'),
	('W019','LEASE OR RENTAL OF EQUIPMENT- SHIPS, SMALL CRAFT, PONTOONS, AND FLOATING DOCKS'),
	('W020','LEASE OR RENTAL OF EQUIPMENT- SHIP AND MARINE EQUIPMENT'),
	('W022','LEASE OR RENTAL OF EQUIPMENT- RAILWAY EQUIPMENT'),
	('W023','LEASE OR RENTAL OF EQUIPMENT- GROUND EFFECT VEHICLES, MOTOR VEHICLES, TRAILERS, AND CYCLES'),
	('W024','LEASE OR RENTAL OF EQUIPMENT- TRACTORS'),
	('W025','LEASE OR RENTAL OF EQUIPMENT- VEHICULAR EQUIPMENT COMPONENTS'),
	('W026','LEASE OR RENTAL OF EQUIPMENT- TIRES AND TUBES'),
	('W028','LEASE OR RENTAL OF EQUIPMENT- ENGINES, TURBINES, AND COMPONENTS'),
	('W029','LEASE OR RENTAL OF EQUIPMENT- ENGINE ACCESSORIES'),
	('W030','LEASE OR RENTAL OF EQUIPMENT- MECHANICAL POWER TRANSMISSION EQUIPMENT'),
	('W031','LEASE OR RENTAL OF EQUIPMENT- BEARINGS'),
	('W032','LEASE OR RENTAL OF EQUIPMENT- WOODWORKING MACHINERY AND EQUIPMENT'),
	('W034','LEASE OR RENTAL OF EQUIPMENT- METALWORKING MACHINERY'),
	('W035','LEASE OR RENTAL OF EQUIPMENT- SERVICE AND TRADE EQUIPMENT'),
	('W036','LEASE OR RENTAL OF EQUIPMENT- SPECIAL INDUSTRY MACHINERY'),
	('W037','LEASE OR RENTAL OF EQUIPMENT- AGRICULTURAL MACHINERY AND EQUIPMENT'),
	('W038','LEASE OR RENTAL OF EQUIPMENT- CONSTRUCTION, MINING, EXCAVATING, AND HIGHWAY MAINTENANCE EQUIPMENT'),
	('W039','LEASE OR RENTAL OF EQUIPMENT- MATERIALS HANDLING EQUIPMENT'),
	('W040','LEASE OR RENTAL OF EQUIPMENT- ROPE, CABLE, CHAIN, AND FITTINGS'),
	('W041','LEASE OR RENTAL OF EQUIPMENT- REFRIGERATION, AIR CONDITIONING, AND AIR CIRCULATING EQUIPMENT'),
	('W042','LEASE OR RENTAL OF EQUIPMENT- FIRE FIGHTING/RESCUE/SAFETY EQUIPMENT; ENVIRON PROTECT EQUIPMENT/MATLS'),
	('W043','LEASE OR RENTAL OF EQUIPMENT- PUMPS AND COMPRESSORS'),
	('W044','LEASE OR RENTAL OF EQUIPMENT- FURNACE, STEAM PLANT, AND DRYING EQUIPMENT; NUCLEAR REACTORS'),
	('W045','LEASE OR RENTAL OF EQUIPMENT- PLUMBING, HEATING, AND WASTE DISPOSAL EQUIPMENT'),
	('W046','LEASE OR RENTAL OF EQUIPMENT- WATER PURIFICATION AND SEWAGE TREATMENT EQUIPMENT'),
	('W047','LEASE OR RENTAL OF EQUIPMENT- PIPE, TUBING, HOSE, AND FITTINGS'),
	('W048','LEASE OR RENTAL OF EQUIPMENT- VALVES'),
	('W049','LEASE OR RENTAL OF EQUIPMENT- MAINTENANCE AND REPAIR SHOP EQUIPMENT'),
	('W051','LEASE OR RENTAL OF EQUIPMENT- HAND TOOLS'),
	('W052','LEASE OR RENTAL OF EQUIPMENT- MEASURING TOOLS'),
	('W053','LEASE OR RENTAL OF EQUIPMENT- HARDWARE AND ABRASIVES'),
	('W054','LEASE OR RENTAL OF EQUIPMENT- PREFABRICATED STRUCTURES  AND SCAFFOLDING'),
	('W055','LEASE OR RENTAL OF EQUIPMENT- LUMBER, MILLWORK, PLYWOOD, AND VENEER'),
	('W056','LEASE OR RENTAL OF EQUIPMENT- CONSTRUCTION AND BUILDING MATERIALS'),
	('W058','LEASE OR RENTAL OF EQUIPMENT- COMMUNICATION, DETECTION, AND COHERENT RADIATION EQUIPMENT'),
	('W059','LEASE OR RENTAL OF EQUIPMENT- ELECTRICAL AND ELECTRONIC EQUIPMENT COMPONENTS'),
	('W060','LEASE OR RENTAL OF EQUIPMENT- FIBER OPTICS MATERIALS, COMPONENTS, ASSEMBLIES, AND ACCESSORIES'),
	('W061','LEASE OR RENTAL OF EQUIPMENT- ELECTRIC WIRE AND POWER DISTRIBUTION EQUIPMENT'),
	('W062','LEASE OR RENTAL OF EQUIPMENT- LIGHTING FIXTURES AND LAMPS'),
	('W063','LEASE OR RENTAL OF EQUIPMENT- ALARM, SIGNAL, AND SECURITY DETECTION SYSTEMS'),
	('W065','LEASE OR RENTAL OF EQUIPMENT- MEDICAL, DENTAL, AND VETERINARY EQUIPMENT AND SUPPLIES'),
	('W066','LEASE OR RENTAL OF EQUIPMENT- INSTRUMENTS AND LABORATORY EQUIPMENT'),
	('W067','LEASE OR RENTAL OF EQUIPMENT- PHOTOGRAPHIC EQUIPMENT'),
	('W068','LEASE OR RENTAL OF EQUIPMENT- CHEMICALS AND CHEMICAL PRODUCTS'),
	('W069','LEASE OR RENTAL OF EQUIPMENT- TRAINING AIDS AND DEVICES'),
	('W070','LEASE OR RENTAL OF EQUIPMENT- ADP EQUIPMENT/SOFTWARE/SUPPLIES/SUPPORT EQUIPMENT'),
	('W071','LEASE OR RENTAL OF EQUIPMENT- FURNITURE'),
	('W072','LEASE OR RENTAL OF EQUIPMENT- HOUSEHOLD AND COMMERCIAL FURNISHINGS AND APPLIANCES'),
	('W073','LEASE OR RENTAL OF EQUIPMENT- FOOD PREPARATION AND SERVING EQUIPMENT'),
	('W074','LEASE OR RENTAL OF EQUIPMENT- OFFICE MACHINES, TEXT PROCESSING SYSTEMS, AND VISIBLE RECORD EQUIPMENT'),
	('W075','LEASE OR RENTAL OF EQUIPMENT- OFFICE SUPPLIES AND DEVICES'),
	('W076','LEASE OR RENTAL OF EQUIPMENT- BOOKS, MAPS, AND OTHER PUBLICATIONS'),
	('W077','LEASE OR RENTAL OF EQUIPMENT- MUSICAL INST/PHONOGRAPH/HOME RADIO'),
	('W078','LEASE OR RENTAL OF EQUIPMENT- RECREATIONAL AND ATHLETIC EQUIPMENT'),
	('W079','LEASE OR RENTAL OF EQUIPMENT- CLEANING EQUIPMENT AND SUPPLIES'),
	('W080','LEASE OR RENTAL OF EQUIPMENT- BRUSHES, PAINTS, SEALERS, AND ADHESIVES'),
	('W081','LEASE OR RENTAL OF EQUIPMENT- CONTAINERS, PACKAGING, AND PACKING SUPPLIES'),
	('W083','LEASE OR RENTAL OF EQUIPMENT- TEXTILES, LEATHER, FURS, APPAREL AND SHOE FINDINGS, TENTS AND FLAGS'),
	('W084','LEASE OR RENTAL OF EQUIPMENT- CLOTHING, INDIVIDUAL EQUIPMENT, AND INSIGNIA'),
	('W085','LEASE OR RENTAL OF EQUIPMENT- TOILETRIES'),
	('W087','LEASE OR RENTAL OF EQUIPMENT- AGRICULTURAL SUPPLIES'),
	('W088','LEASE OR RENTAL OF EQUIPMENT- LIVE ANIMALS'),
	('W089','LEASE OR RENTAL OF EQUIPMENT- SUBSISTENCE'),
	('W091','LEASE OR RENTAL OF EQUIPMENT- FUELS, LUBRICANTS, OILS, AND WAXES'),
	('W093','LEASE OR RENTAL OF EQUIPMENT- NONMETALLIC FABRICATED MATERIALS'),
	('W094','LEASE OR RENTAL OF EQUIPMENT- NONMETALLIC CRUDE MATERIALS'),
	('W095','LEASE OR RENTAL OF EQUIPMENT- METAL BARS, SHEETS, AND SHAPES'),
	('W096','LEASE OR RENTAL OF EQUIPMENT- ORES, MINERALS, AND THEIR PRIMARY PRODUCTS'),
	('W099','LEASE OR RENTAL OF EQUIPMENT- MISCELLANEOUS'),
	('X','LEASE/RENT FACILITIES'),
	('X1','LEASE/RENTAL OF BUILDINGS'),
	('X1AA','LEASE/RENTAL OF OFFICE BUILDINGS'),
	('X1AB','LEASE/RENTAL OF CONFERENCE SPACE AND FACILITIES'),
	('X1AZ','LEASE/RENTAL OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('X1BA','LEASE/RENTAL OF AIR TRAFFIC CONTROL TOWERS'),
	('X1BB','LEASE/RENTAL OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('X1BC','LEASE/RENTAL OF RADAR AND NAVIGATIONAL FACILITIES'),
	('X1BD','LEASE/RENTAL OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('X1BE','LEASE/RENTAL OF AIRPORT TERMINALS'),
	('X1BF','LEASE/RENTAL OF MISSILE SYSTEM FACILITIES'),
	('X1BG','LEASE/RENTAL OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('X1BZ','LEASE/RENTAL OF OTHER AIRFIELD STRUCTURES'),
	('X1CA','LEASE/RENTAL OF SCHOOLS'),
	('X1CZ','LEASE/RENTAL OF OTHER EDUCATIONAL BUILDINGS'),
	('X1DA','LEASE/RENTAL OF HOSPITALS AND INFIRMARIES'),
	('X1DB','LEASE/RENTAL OF LABORATORIES AND CLINICS'),
	('X1DZ','LEASE/RENTAL OF OTHER HOSPITAL BUILDINGS'),
	('X1EA','LEASE/RENTAL OF AMMUNITION FACILITIES'),
	('X1EB','LEASE/RENTAL OF MAINTENANCE BUILDINGS'),
	('X1EC','LEASE/RENTAL OF PRODUCTION BUILDINGS'),
	('X1ED','LEASE/RENTAL OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('X1EE','LEASE/RENTAL OF TANK AUTOMOTIVE FACILITIES'),
	('X1EZ','LEASE/RENTAL OF OTHER INDUSTRIAL BUILDINGS'),
	('X1FA','LEASE/RENTAL OF FAMILY HOUSING FACILITIES'),
	('X1FB','LEASE/RENTAL OF RECREATIONAL BUILDINGS'),
	('X1FC','LEASE/RENTAL OF TROOP HOUSING FACILITIES'),
	('X1FD','LEASE/RENTAL OF DINING FACILITIES'),
	('X1FE','LEASE/RENTAL OF RELIGIOUS FACILITIES'),
	('X1FF','LEASE/RENTAL OF PENAL FACILITIES'),
	('X1FZ','LEASE/RENTAL OF OTHER RESIDENTIAL BUILDINGS'),
	('X1GA','LEASE/RENTAL OF AMMUNITION STORAGE BUILDINGS'),
	('X1GB','LEASE/RENTAL OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('X1GC','LEASE/RENTAL OF FUEL STORAGE BUILDINGS'),
	('X1GD','LEASE/RENTAL OF OPEN STORAGE FACILITIES'),
	('X1GZ','LEASE/RENTAL OF OTHER WAREHOUSE BUILDINGS'),
	('X1HA','LEASE/RENTAL OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('X1HB','LEASE/RENTAL OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('X1HC','LEASE/RENTAL OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('X1HZ','LEASE/RENTAL OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('X1JA','LEASE/RENTAL OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('X1JB','LEASE/RENTAL OF TESTING AND MEASUREMENT BUILDINGS'),
	('X1JZ','LEASE/RENTAL OF MISCELLANEOUS BUILDINGS'),
	('X1KA','LEASE/RENTAL OF DAMS'),
	('X1KB','LEASE/RENTAL OF CANALS'),
	('X1KC','LEASE/RENTAL OF MINE FIRE CONTROL FACILITIES'),
	('X1KD','LEASE/RENTAL OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('X1KE','LEASE/RENTAL OF SURFACE MINE RECLAMATION FACILITIES'),
	('X1KF','LEASE/RENTAL OF DREDGING FACILITIES'),
	('X1KZ','LEASE/RENTAL OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('X1LA','LEASE/RENTAL OF AIRPORT SERVICE ROADS'),
	('X1LB','LEASE/RENTAL OF HIGHWAYS, ROADS, STREETS, BRIDGES, AND RAILWAYS'),
	('X1LC','LEASE/RENTAL OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('X1LZ','LEASE/RENTAL OF PARKING FACILITIES'),
	('X1MA','LEASE/RENTAL OF EPG FACILITIES - COAL'),
	('X1MB','LEASE/RENTAL OF EPG FACILITIES - GAS'),
	('X1MC','LEASE/RENTAL OF EPG FACILITIES - GEOTHERMAL'),
	('X1MD','LEASE/RENTAL OF EPG FACILITIES - HYDRO'),
	('X1ME','LEASE/RENTAL OF EPG FACILITIES - NUCLEAR'),
	('X1MF','LEASE/RENTAL OF EPG FACILITIES - PETROLEUM'),
	('X1MG','LEASE/RENTAL OF EPG FACILITIES - SOLAR'),
	('X1MH','LEASE/RENTAL OF EPG FACILITIES - WIND'),
	('X1MZ','LEASE/RENTAL OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('X1NA','LEASE/RENTAL OF FUEL SUPPLY FACILITIES'),
	('X1NB','LEASE/RENTAL OF HEATING AND COOLING PLANTS'),
	('X1NC','LEASE/RENTAL OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('X1ND','LEASE/RENTAL OF SEWAGE AND WASTE FACILITIES'),
	('X1NE','LEASE/RENTAL OF WATER SUPPLY FACILITIES'),
	('X1NZ','LEASE/RENTAL OF OTHER UTILITIES'),
	('X1PA','LEASE/RENTAL OF RECREATION FACILITIES (NON-BUILDING)'),
	('X1PB','LEASE/RENTAL OF EXHIBIT DESIGN (NON-BUILDING)'),
	('X1PC','LEASE/RENTAL OF UNIMPROVED REAL PROPERTY (LAND)'),
	('X1PD','LEASE/RENTAL OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('X1PZ','LEASE/RENTAL OF OTHER NON-BUILDING FACILITIES'),
	('X1QA','LEASE/RENTAL OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('X2','LEASE/RENTAL NONBUILDING FACILITIES'),
	('X3','LEASE/RENTAL OF RESTORATION ACTIVS'),
	('Y','CONSTRUCT OF STRUCTURES/FACILITIES'),
	('Y1','CONSTRUCTION OF BUILDINGS'),
	('Y1AA','CONSTRUCTION OF OFFICE BUILDINGS'),
	('Y1AB','CONSTRUCTION OF CONFERENCE SPACE AND FACILITIES'),
	('Y1AZ','CONSTRUCTION OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('Y1BA','CONSTRUCTION OF AIR TRAFFIC CONTROL TOWERS'),
	('Y1BB','CONSTRUCTION OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('Y1BC','CONSTRUCTION OF RADAR AND NAVIGATIONAL FACILITIES'),
	('Y1BD','CONSTRUCTION OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('Y1BE','CONSTRUCTION OF AIRPORT TERMINALS'),
	('Y1BF','CONSTRUCTION OF MISSILE SYSTEM FACILITIES'),
	('Y1BG','CONSTRUCTION OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('Y1BZ','CONSTRUCTION OF OTHER AIRFIELD STRUCTURES'),
	('Y1CA','CONSTRUCTION OF SCHOOLS'),
	('Y1CZ','CONSTRUCTION OF OTHER EDUCATIONAL BUILDINGS'),
	('Y1DA','CONSTRUCTION OF HOSPITALS AND INFIRMARIES'),
	('Y1DB','CONSTRUCTION OF LABORATORIES AND CLINICS'),
	('Y1DZ','CONSTRUCTION OF OTHER HOSPITAL BUILDINGS'),
	('Y1EA','CONSTRUCTION OF AMMUNITION FACILITIES'),
	('Y1EB','CONSTRUCTION OF MAINTENANCE BUILDINGS'),
	('Y1EC','CONSTRUCTION OF PRODUCTION BUILDINGS'),
	('Y1ED','CONSTRUCTION OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('Y1EE','CONSTRUCTION OF TANK AUTOMOTIVE FACILITIES'),
	('Y1EZ','CONSTRUCTION OF OTHER INDUSTRIAL BUILDINGS'),
	('Y1FA','CONSTRUCTION OF FAMILY HOUSING FACILITIES'),
	('Y1FB','CONSTRUCTION OF RECREATIONAL BUILDINGS'),
	('Y1FC','CONSTRUCTION OF TROOP HOUSING FACILITIES'),
	('Y1FD','CONSTRUCTION OF DINING FACILITIES'),
	('Y1FE','CONSTRUCTION OF RELIGIOUS FACILITIES'),
	('Y1FF','CONSTRUCTION OF PENAL FACILITIES'),
	('Y1FZ','CONSTRUCTION OF OTHER RESIDENTIAL BUILDINGS'),
	('Y1GA','CONSTRUCTION OF AMMUNITION STORAGE BUILDINGS'),
	('Y1GB','CONSTRUCTION OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('Y1GC','CONSTRUCTION OF FUEL STORAGE BUILDINGS'),
	('Y1GD','CONSTRUCTION OF OPEN STORAGE FACILITIES'),
	('Y1GZ','CONSTRUCTION OF OTHER WAREHOUSE BUILDINGS'),
	('Y1HA','CONSTRUCTION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('Y1HB','CONSTRUCTION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('Y1HC','CONSTRUCTION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('Y1HZ','CONSTRUCTION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('Y1JA','CONSTRUCTION OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('Y1JB','CONSTRUCTION OF TESTING AND MEASUREMENT BUILDINGS'),
	('Y1JZ','CONSTRUCTION OF MISCELLANEOUS BUILDINGS'),
	('Y1KA','CONSTRUCTION OF DAMS'),
	('Y1KB','CONSTRUCTION OF CANALS'),
	('Y1KC','CONSTRUCTION OF MINE FIRE CONTROL FACILITIES'),
	('Y1KD','CONSTRUCTION OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('Y1KE','CONSTRUCTION OF SURFACE MINE RECLAMATION FACILITIES'),
	('Y1KF','CONSTRUCTION OF DREDGING FACILITIES'),
	('Y1KZ','CONSTRUCTION OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('Y1LA','CONSTRUCTION OF AIRPORT SERVICE ROADS'),
	('Y1LB','CONSTRUCTION OF HIGHWAYS, ROADS, STREETS, BRIDGES, AND RAILWAYS'),
	('Y1LC','CONSTRUCTION OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('Y1LZ','CONSTRUCTION OF PARKING FACILITIES'),
	('Y1MA','CONSTRUCTION OF EPG FACILITIES - COAL'),
	('Y1MB','CONSTRUCTION OF EPG FACILITIES - GAS'),
	('Y1MC','CONSTRUCTION OF EPG FACILITIES - GEOTHERMAL'),
	('Y1MD','CONSTRUCTION OF EPG FACILITIES - HYDRO'),
	('Y1ME','CONSTRUCTION OF EPG FACILITIES - NUCLEAR'),
	('Y1MF','CONSTRUCTION OF EPG FACILITIES - PETROLEUM'),
	('Y1MG','CONSTRUCTION OF EPG FACILITIES - SOLAR'),
	('Y1MH','CONSTRUCTION OF EPG FACILITIES - WIND'),
	('Y1MZ','CONSTRUCTION OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('Y1NA','CONSTRUCTION OF FUEL SUPPLY FACILITIES'),
	('Y1NB','CONSTRUCTION OF HEATING AND COOLING PLANTS'),
	('Y1NC','CONSTRUCTION OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('Y1ND','CONSTRUCTION OF SEWAGE AND WASTE FACILITIES'),
	('Y1NE','CONSTRUCTION OF WATER SUPPLY FACILITIES'),
	('Y1NZ','CONSTRUCTION OF OTHER UTILITIES'),
	('Y1PA','CONSTRUCTION OF RECREATION FACILITIES (NON-BUILDING)'),
	('Y1PB','CONSTRUCTION OF EXHIBIT DESIGN (NON-BUILDING)'),
	('Y1PC','CONSTRUCTION OF UNIMPROVED REAL PROPERTY (LAND)'),
	('Y1PD','CONSTRUCTION OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('Y1PZ','CONSTRUCTION OF OTHER NON-BUILDING FACILITIES'),
	('Y1QA','CONSTRUCTION OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('Y2','CONSTRUCT NONBUILDING FACILITIES'),
	('Y3','CONSTRUCT OF RESTORATION ACTIVITIES'),
	('Z','MAINT, REPAIR, ALTER REAL PROPERTY'),
	('Z1','MAINT, ALTER, REPAIR BUILDINGS'),
	('Z1AA','MAINTENANCE OF OFFICE BUILDINGS'),
	('Z1AB','MAINTENANCE OF CONFERENCE SPACE AND FACILITIES'),
	('Z1AZ','MAINTENANCE OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('Z1BA','MAINTENANCE OF AIR TRAFFIC CONTROL TOWERS'),
	('Z1BB','MAINTENANCE OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('Z1BC','MAINTENANCE OF RADAR AND NAVIGATIONAL FACILITIES'),
	('Z1BD','MAINTENANCE OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('Z1BE','MAINTENANCE OF AIRPORT TERMINALS'),
	('Z1BF','MAINTENANCE OF MISSILE SYSTEM FACILITIES'),
	('Z1BG','MAINTENANCE OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('Z1BZ','MAINTENANCE OF OTHER AIRFIELD STRUCTURES'),
	('Z1CA','MAINTENANCE OF SCHOOLS'),
	('Z1CZ','MAINTENANCE OF OTHER EDUCATIONAL BUILDINGS'),
	('Z1DA','MAINTENANCE OF HOSPITALS AND INFIRMARIES'),
	('Z1DB','MAINTENANCE OF LABORATORIES AND CLINICS'),
	('Z1DZ','MAINTENANCE OF OTHER HOSPITAL BUILDINGS'),
	('Z1EA','MAINTENANCE OF AMMUNITION FACILITIES'),
	('Z1EB','MAINTENANCE OF MAINTENANCE BUILDINGS'),
	('Z1EC','MAINTENANCE OF PRODUCTION BUILDINGS'),
	('Z1ED','MAINTENANCE OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('Z1EE','MAINTENANCE OF TANK AUTOMOTIVE FACILITIES'),
	('Z1EZ','MAINTENANCE OF OTHER INDUSTRIAL BUILDINGS'),
	('Z1FA','MAINTENANCE OF FAMILY HOUSING FACILITIES'),
	('Z1FB','MAINTENANCE OF RECREATIONAL BUILDINGS'),
	('Z1FC','MAINTENANCE OF TROOP HOUSING FACILITIES'),
	('Z1FD','MAINTENANCE OF DINING FACILITIES'),
	('Z1FE','MAINTENANCE OF RELIGIOUS FACILITIES'),
	('Z1FF','MAINTENANCE OF PENAL FACILITIES'),
	('Z1FZ','MAINTENANCE OF OTHER RESIDENTIAL BUILDINGS'),
	('Z1GA','MAINTENANCE OF AMMUNITION STORAGE BUILDINGS'),
	('Z1GB','MAINTENANCE OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('Z1GC','MAINTENANCE OF FUEL STORAGE BUILDINGS'),
	('Z1GD','MAINTENANCE OF OPEN STORAGE FACILITIES'),
	('Z1GZ','MAINTENANCE OF OTHER WAREHOUSE BUILDINGS'),
	('Z1HA','MAINTENANCE OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('Z1HB','MAINTENANCE OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('Z1HC','MAINTENANCE OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('Z1HZ','MAINTENANCE OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('Z1JA','MAINTENANCE OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('Z1JB','MAINTENANCE OF TESTING AND MEASUREMENT BUILDINGS'),
	('Z1JZ','MAINTENANCE OF MISCELLANEOUS BUILDINGS'),
	('Z1KA','MAINTENANCE OF DAMS'),
	('Z1KB','MAINTENANCE OF CANALS'),
	('Z1KC','MAINTENANCE OF MINE FIRE CONTROL FACILITIES'),
	('Z1KD','MAINTENANCE OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('Z1KE','MAINTENANCE OF SURFACE MINE RECLAMATION FACILITIES'),
	('Z1KF','MAINTENANCE OF DREDGING FACILITIES'),
	('Z1KZ','MAINTENANCE OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('Z1LA','MAINTENANCE OF AIRPORT SERVICE ROADS'),
	('Z1LB','MAINTENANCE OF HIGHWAYS/ROADS/STREETS/BRIDGES/RAILWAYS'),
	('Z1LC','MAINTENANCE OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('Z1LZ','MAINTENANCE OF PARKING FACILITIES'),
	('Z1MA','MAINTENANCE OF EPG FACILITIES - COAL'),
	('Z1MB','MAINTENANCE OF EPG FACILITIES - GAS'),
	('Z1MC','MAINTENANCE OF EPG FACILITIES - GEOTHERMAL'),
	('Z1MD','MAINTENANCE OF EPG FACILITIES - HYDRO'),
	('Z1ME','MAINTENANCE OF EPG FACILITIES - NUCLEAR'),
	('Z1MF','MAINTENANCE OF EPG FACILITIES - PETROLEUM'),
	('Z1MG','MAINTENANCE OF EPG FACILITIES - SOLAR'),
	('Z1MH','MAINTENANCE OF EPG FACILITIES - WIND'),
	('Z1MZ','MAINTENANCE OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('Z1NA','MAINTENANCE OF FUEL SUPPLY FACILITIES'),
	('Z1NB','MAINTENANCE OF HEATING AND COOLING PLANTS'),
	('Z1NC','MAINTENANCE OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('Z1ND','MAINTENANCE OF SEWAGE AND WASTE FACILITIES'),
	('Z1NE','MAINTENANCE OF WATER SUPPLY FACILITIES'),
	('Z1NZ','MAINTENANCE OF OTHER UTILITIES'),
	('Z1PA','MAINTENANCE OF RECREATION FACILITIES (NON-BUILDING)')
GO

INSERT INTO Dropdown.PSC 	
	(PSCCode, PSCName) 
VALUES 
	('Z1PB','MAINTENANCE OF EXHIBIT DESIGN (NON-BUILDING)'),
	('Z1PC','MAINTENANCE OF UNIMPROVED REAL PROPERTY (LAND)'),
	('Z1PD','MAINTENANCE OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('Z1PZ','MAINTENANCE OF OTHER NON-BUILDING FACILITIES'),
	('Z1QA','MAINTENANCE OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('Z2','MAINT, ALTER, REPAIR NONBUILDINGS'),
	('Z2AA','REPAIR OR ALTERATION OF OFFICE BUILDINGS'),
	('Z2AB','REPAIR OR ALTERATION OF CONFERENCE SPACE AND FACILITIES'),
	('Z2AZ','REPAIR OR ALTERATION OF OTHER ADMINISTRATIVE FACILITIES AND SERVICE BUILDINGS'),
	('Z2BA','REPAIR OR ALTERATION OF AIR TRAFFIC CONTROL TOWERS'),
	('Z2BB','REPAIR OR ALTERATION OF AIR TRAFFIC CONTROL TRAINING FACILITIES'),
	('Z2BC','REPAIR OR ALTERATION OF RADAR AND NAVIGATIONAL FACILITIES'),
	('Z2BD','REPAIR OR ALTERATION OF AIRPORT RUNWAYS AND TAXIWAYS'),
	('Z2BE','REPAIR OR ALTERATION OF AIRPORT TERMINALS'),
	('Z2BF','REPAIR OR ALTERATION OF MISSILE SYSTEM FACILITIES'),
	('Z2BG','REPAIR OR ALTERATION OF ELECTRONIC AND COMMUNICATIONS FACILITIES'),
	('Z2BZ','REPAIR OR ALTERATION OF OTHER AIRFIELD STRUCTURES'),
	('Z2CA','REPAIR OR ALTERATION OF SCHOOLS'),
	('Z2CZ','REPAIR OR ALTERATION OF OTHER EDUCATIONAL BUILDINGS'),
	('Z2DA','REPAIR OR ALTERATION OF HOSPITALS AND INFIRMARIES'),
	('Z2DB','REPAIR OR ALTERATION OF LABORATORIES AND CLINICS'),
	('Z2DZ','REPAIR OR ALTERATION OF OTHER HOSPITAL BUILDINGS'),
	('Z2EA','REPAIR OR ALTERATION OF AMMUNITION FACILITIES'),
	('Z2EB','REPAIR OR ALTERATION OF MAINTENANCE BUILDINGS'),
	('Z2EC','REPAIR OR ALTERATION OF PRODUCTION BUILDINGS'),
	('Z2ED','REPAIR OR ALTERATION OF SHIP CONSTRUCTION AND REPAIR FACILITIES'),
	('Z2EE','REPAIR OR ALTERATION OF TANK AUTOMOTIVE FACILITIES'),
	('Z2EZ','REPAIR OR ALTERATION OF OTHER INDUSTRIAL BUILDINGS'),
	('Z2FA','REPAIR OR ALTERATION OF FAMILY HOUSING FACILITIES'),
	('Z2FB','REPAIR OR ALTERATION OF RECREATIONAL BUILDINGS'),
	('Z2FC','REPAIR OR ALTERATION OF TROOP HOUSING FACILITIES'),
	('Z2FD','REPAIR OR ALTERATION OF DINING FACILITIES'),
	('Z2FE','REPAIR OR ALTERATION OF RELIGIOUS FACILITIES'),
	('Z2FF','REPAIR OR ALTERATION OF PENAL FACILITIES'),
	('Z2FZ','REPAIR OR ALTERATION OF OTHER RESIDENTIAL BUILDINGS'),
	('Z2GA','REPAIR OR ALTERATION OF AMMUNITION STORAGE BUILDINGS'),
	('Z2GB','REPAIR OR ALTERATION OF FOOD OR GRAIN STORAGE BUILDINGS'),
	('Z2GC','REPAIR OR ALTERATION OF FUEL STORAGE BUILDINGS'),
	('Z2GD','REPAIR OR ALTERATION OF OPEN STORAGE FACILITIES'),
	('Z2GZ','REPAIR OR ALTERATION OF OTHER WAREHOUSE BUILDINGS'),
	('Z2HA','REPAIR OR ALTERATION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) R&D FACILITIES'),
	('Z2HB','REPAIR OR ALTERATION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) R&D FACILITIES'),
	('Z2HC','REPAIR OR ALTERATION OF GOVERNMENT-OWNED CONTRACTOR-OPERATED (GOCO) ENVIRONMENTAL LABORATORIES'),
	('Z2HZ','REPAIR OR ALTERATION OF GOVERNMENT-OWNED GOVERNMENT-OPERATED (GOGO) ENVIRONMENTAL LABORATORIES'),
	('Z2JA','REPAIR OR ALTERATION OF MUSEUMS AND EXHIBITION BUILDINGS'),
	('Z2JB','REPAIR OR ALTERATION OF TESTING AND MEASUREMENT BUILDINGS'),
	('Z2JZ','REPAIR OR ALTERATION OF MISCELLANEOUS BUILDINGS'),
	('Z2KA','REPAIR OR ALTERATION OF DAMS'),
	('Z2KB','REPAIR OR ALTERATION OF CANALS'),
	('Z2KC','REPAIR OR ALTERATION OF MINE FIRE CONTROL FACILITIES'),
	('Z2KD','REPAIR OR ALTERATION OF MINE SUBSIDENCE CONTROL FACILITIES'),
	('Z2KE','REPAIR OR ALTERATION OF SURFACE MINE RECLAMATION FACILITIES'),
	('Z2KF','REPAIR OR ALTERATION OF DREDGING FACILITIES'),
	('Z2KZ','REPAIR OR ALTERATION OF OTHER CONSERVATION AND DEVELOPMENT FACILITIES'),
	('Z2LA','REPAIR OR ALTERATION OF AIRPORT SERVICE ROADS'),
	('Z2LB','REPAIR OR ALTERATION OF HIGHWAYS/ROADS/STREETS/BRIDGES/RAILWAYS'),
	('Z2LC','REPAIR OR ALTERATION OF TUNNELS AND SUBSURFACE STRUCTURES'),
	('Z2LZ','REPAIR OR ALTERATION OF PARKING FACILITIES'),
	('Z2MA','REPAIR OR ALTERATION OF EPG FACILITIES - COAL'),
	('Z2MB','REPAIR OR ALTERATION OF EPG FACILITIES - GAS'),
	('Z2MC','REPAIR OR ALTERATION OF EPG FACILITIES - GEOTHERMAL'),
	('Z2MD','REPAIR OR ALTERATION OF EPG FACILITIES - HYDRO'),
	('Z2ME','REPAIR OR ALTERATION OF EPG FACILITIES - NUCLEAR'),
	('Z2MF','REPAIR OR ALTERATION OF EPG FACILITIES - PETROLEUM'),
	('Z2MG','REPAIR OR ALTERATION OF EPG FACILITIES - SOLAR'),
	('Z2MH','REPAIR OR ALTERATION OF EPG FACILITIES - WIND'),
	('Z2MZ','REPAIR OR ALTERATION OF EPG FACILITIES - OTHER, INCLUDING TRANSMISSION'),
	('Z2NA','REPAIR OR ALTERATION OF FUEL SUPPLY FACILITIES'),
	('Z2NB','REPAIR OR ALTERATION OF HEATING AND COOLING PLANTS'),
	('Z2NC','REPAIR OR ALTERATION OF POLLUTION ABATEMENT AND CONTROL FACILITIES'),
	('Z2ND','REPAIR OR ALTERATION OF SEWAGE AND WASTE FACILITIES'),
	('Z2NE','REPAIR OR ALTERATION OF WATER SUPPLY FACILITIES'),
	('Z2NZ','REPAIR OR ALTERATION OF OTHER UTILITIES'),
	('Z2PA','REPAIR OR ALTERATION OF RECREATION FACILITIES (NON-BUILDING)'),
	('Z2PB','REPAIR OR ALTERATION OF EXHIBIT DESIGN (NON-BUILDING)'),
	('Z2PC','REPAIR OR ALTERATION OF UNIMPROVED REAL PROPERTY (LAND)'),
	('Z2PD','REPAIR OR ALTERATION OF WASTE TREATMENT AND STORAGE FACILITIES'),
	('Z2PZ','REPAIR OR ALTERATION OF OTHER NON-BUILDING FACILITIES'),
	('Z2QA','REPAIR OR ALTERATION OF RESTORATION OF REAL PROPERTY (PUBLIC OR PRIVATE)'),
	('Z3','MAINT, ALTER, REPAIR RESTOR ACVIVS')
GO

UPDATE Dropdown.PSC
SET DisplayOrder = PSCID
GO
--End table Dropdown.PSC

--Begin table Dropdown.QuickSearch
INSERT INTO Dropdown.QuickSearch
	(QuickSearchName,QuickSearchDescription,QuickSearchCriteria,IsPublic)
VALUES
	('Contract opportunities awaiting my review','This search will return a list of all contract opportunities that are available for me to review.','{"PendingApprovalPersonID":"PersonID"}', 0),
	('Contract opportunities from my organization','This search will return a list of all contract opportunities that are associated with my organization.','{"OrganizationIDList":"PersonOrganizationID"}', 0),
	('Contract opportunities expiring at the end of the fiscal year','This search will return a list of all contract opportunities with a contract end date during the current fiscal year.','{"ContractDateEnd":"EndOfCurrentFiscalYear"}', 1)
GO	
--End table Dropdown.QuickSearch

--Begin table Dropdown.RequirementType
SET IDENTITY_INSERT Dropdown.RequirementType ON
INSERT INTO Dropdown.RequirementType (RequirementTypeID) VALUES (0)
SET IDENTITY_INSERT Dropdown.RequirementType OFF
GO

INSERT INTO Dropdown.RequirementType 
	(RequirementTypeName) 
VALUES
	('New Requirement'),
	('Option Year'),
	('Recompete')
GO
--End table Dropdown.RequirementType

--Begin table Dropdown.Role
SET IDENTITY_INSERT Dropdown.Role ON
INSERT INTO Dropdown.Role (RoleID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Role OFF
GO

INSERT INTO Dropdown.Role
	(RoleCode,RoleName,RoleWeight,DisplayOrder,IsAdministrator)
VALUES
	('SuperAdministrator','Super Administrator',1000,1,1),
	('OrganizationAdministrator','OpDiv Administrator',750,2,1)
GO	
--End table Dropdown.Role

--Begin table Dropdown.State
SET IDENTITY_INSERT Dropdown.State ON
INSERT INTO Dropdown.State (StateID) VALUES (0)
SET IDENTITY_INSERT Dropdown.State OFF
GO

INSERT INTO Dropdown.State 
	(StateCode,StateName) 
VALUES
	('AL','Alabama'),
	('AK','Alaska'),
	('AZ','Arizona'),
	('AR','Arkansas'),
	('CA','California'),
	('CO','Colorado'),
	('CT','Connecticut'),
	('DE','Delaware'),
	('DC','District of Columbia'),
	('FL','Florida'),
	('GA','Georgia'),
	('GU','Guam'),
	('HI','Hawaii'),
	('ID','Idaho'),
	('IL','Illinois'),
	('IN','Indiana'),
	('IA','Iowa'),
	('KS','Kansas'),
	('KY','Kentucky'),
	('LA','Louisiana'),
	('ME','Maine'),
	('MD','Maryland'),
	('MA','Massachusetts'),
	('MI','Michigan'),
	('MN','Minnesota'),
	('MS','Mississippi'),
	('MO','Missouri'),
	('MT','Montana'),
	('NE','Nebraska'),
	('NV','Nevada'),
	('NH','New Hampshire'),
	('NJ','New Jersey'),
	('NM','New Mexico'),
	('NY','New York'),
	('NC','North Carolina'),
	('ND','North Dakota'),
	('OH','Ohio'),
	('OK','Oklahoma'),
	('OR','Oregon'),
	('PA','Pennsylvania'),
	('PR','Puerto Rico'),
	('RI','Rhode Island'),
	('SC','South Carolina'),
	('SD','South Dakota'),
	('TN','Tennessee'),
	('TX','Texas'),
	('UT','Utah'),
	('VT','Vermont'),
	('VI','Virgin Islands'),
	('VA','Virginia'),
	('WA','Washington'),
	('WV','West Virginia'),
	('WI','Wisconsin'),
	('WY','Wyoming')
GO
--End table Dropdown.State

--Begin table Dropdown.WorkflowStatus
SET IDENTITY_INSERT Dropdown.WorkflowStatus ON
INSERT INTO Dropdown.WorkflowStatus (WorkflowStatusID) VALUES (0)
SET IDENTITY_INSERT Dropdown.WorkflowStatus OFF
GO

INSERT INTO Dropdown.WorkflowStatus
	(WorkflowStatusCode,WorkflowStatusName,DisplayOrder)
VALUES
	('PendingSubmission','Pending Submission', 1),
	('PendingApproval','Pending Approval', 2),
	('Approved','Approved', 3)
GO	
--End table Dropdown.WorkflowStatus

--Begin EventLog Entries for Dropdown.Organization
DECLARE @OrganizationID INT
DECLARE @WorkflowID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.OrganizationID
	FROM Dropdown.Organization O
	WHERE O.OrganizationID > 0
	ORDER BY O.OrganizationID

OPEN oCursor
FETCH oCursor INTO @OrganizationID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogOrganizationEntry @OrganizationID = @OrganizationID, @EventCode = 'Create', @PersonID = 0

	INSERT INTO dbo.Workflow
		(OrganizationID)
	VALUES
		(@OrganizationID)

	SELECT @WorkflowID = W.WorkflowID
	FROM dbo.Workflow W
	WHERE W.OrganizationID = @OrganizationID

	EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Create', @PersonID = 0

	FETCH oCursor INTO @OrganizationID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End EventLog Entries for Dropdown.Organization

--Begin EventLog Entries for dbo.Person
--Begin SuperAdministrator
INSERT dbo.PersonRole 
	(PersonID, RoleID)
SELECT
	P.PersonID,
	(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'SuperAdministrator')
FROM dbo.Person P
WHERE P.EmailAddress IN 
	(
	'christopher.crouch@hhs.gov',
	'linda.waters@hhs.gov',
	'teresa.lewis@hhs.gov',
	'todd.pires@hhs.gov'
	)
ORDER BY P.PersonID
GO

--Begin OrganizationAdministrator
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alec.blakeley@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alexandra.hohensee@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'angela.johnson@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'asha.abraham@fda.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'audrey.mirsky-ashby@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'barbara.miller@fda.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'blenda.perez@samhsa.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bryan.maynard@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'buddy.frye@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OGC'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.randolph@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'daniel.levenson@cms.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.flynn@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.flynn@psc.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'PSC'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.hauge@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'diane.bogusz@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dillonr@od.nih.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'harold.henderson@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OCR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jason.vollmer@cms.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'joseph.pressley@cms.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'juan.wooten@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'karen.robinson@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kevin.cramer@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kevin.greene@hrsa.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'HRSA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lynn.tantardini@samhsa.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'SAMHSA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'matthew.kelly@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OMHA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'maureen.turner-cooper@nih.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'melissa.middleton@acl.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michon.kretschmaier@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASH'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'neil.kaufman@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nikki.bratcher-bowman@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'osmani.banos-diaz@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'DAB'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'pam.sessoms@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASFR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'pete.burr@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phillip.osborne@nih.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'rafael.mack@cms.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'roshawn.majors@ihs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'IHS'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'roy.brunson@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPE'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'shoney.whitfield@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'IEA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sqt8@cdc.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CDC'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'terry.nicolosi@acl.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ACL'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'theresa.evans@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'timothy.barfield@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASFR'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tishawn.miller@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'OASL'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tony.richardson@cms.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CMS'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tri.thai@fda.hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'FDA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'vqh5@cdc.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'CDC'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'wendy.blanke@hhs.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'ASPA'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
INSERT dbo.PersonRole (PersonID,OrganizationID,RoleID) VALUES ((SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'williambro@od.nih.gov'),(SELECT O.OrganizationID FROM Dropdown.Organization O WHERE O.OrganizationName = 'NIH'),(SELECT R.RoleID FROM Dropdown.Role R WHERE R.RoleCode = 'OrganizationAdministrator'))
GO

DECLARE @TargetPersonID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT P.PersonID
	FROM dbo.Person P
	ORDER BY P.PersonID

OPEN oCursor
FETCH oCursor INTO @TargetPersonID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = 'Create', @SourcePersonID = 0
	FETCH oCursor INTO @TargetPersonID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End EventLog Entries for dbo.Person

--Begin EventLog Entries for dbo.Workflow
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'suzanne.cruz@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'letrina.holley@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'suzanne.cruz@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'letrina.holley@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'melissa.middleton@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dan.berger@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ACL'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'terry.nicolosi@acl.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alexandra.hohensee@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dionet.perry@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'paula.formoso@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phyllis.noble@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ronald.depas@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tracy.hall@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'wendy.blanke@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'yvette.alonso@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'anne.morazzanoteeter@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bill.hall@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'cara.berman@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.wilker@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phyllis.noble@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'prudence.goforth@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tracy.hall@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'yvette.alonso@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'anne.morazzanoteeter@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.marquis@hhs,gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phyllis.noble@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'prudence.goforth@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tracy.hall@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'catherine.teti@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPA'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.weber@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'audrey.mirsky-ashby@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'taylortn@mail.nih.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'audrey.mirsky-ashby@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kevin.cramer@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'audrey.mirsky-ashby@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kevin.cramer@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'ASPE'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'roy.brunson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ivey.belton@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'timothy.noonan@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'frank.campbell@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alec.blakeley@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bryan.maynard@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Atlanta Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'harold.henderson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'amanda.merricks@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'susan.rhodes@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'frank.campbell@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alec.blakeley@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bryan.maynard@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Boston Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'harold.henderson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CDC'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'gvr6@cdc.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CDC'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dce1@cdc.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CDC'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'zvq7@cdc.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CDC'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dce1@cdc.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CDC'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'fpp0@cdc.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sherene.vann@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'celeste.davis@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'frank.campbell@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alec.blakeley@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bryan.maynard@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'Chicago Regional Office'),4,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'harold.henderson@hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'aaron.blackshire@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'abby.smith@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'adam.forgione@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alan.fredericks@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alex.jarema@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'algirdas.veliuona@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alice.calabro@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alice.mcgruder@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'allisan.hafner@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'amy.duckworth@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'andrea.ormiston@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'andrew.anuszewski@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'andrew.crochunis@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'andrew.mummert@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'angela.britton@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'angela.reviere@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'anginna.sims@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'antoinette.hazelwood@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'barry.mikesell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'benjamin.simcock@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'benjamin.stidham@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'beth.citeroni@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'beth.marlow@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'beth.waskiewicz@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brenda.clark@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brenda.thomas@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brendan.doherty@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brian.hebbel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brian.humes@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bridget.rineker@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bryce.golwalla@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'candice.savoy@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'carol.sevel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'cassandra.worsham-carey@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'cathryn.kim@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'charlene.barnes@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'charles.brewer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'charles.littleton@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.eberhart@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.heller@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.honey@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christine.meade@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christine.nagengast@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.clark@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.hagepanos@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.tanks@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'corey.lloyd@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'courtney.garnes@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'craig.dash@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'crystal.mcwilliams@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'danie.ridgway@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'daniel.levenson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'danielle.tapp@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.fitton@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.hansen@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dawn.graham@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dawn.wilkins@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'deborah.lester@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'debra.hoffman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'debra.stidham@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'denizejoice.hammond@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'donald.bozimski@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'donald.knode@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'eddie.woodard@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'edward.farmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'edward.hughes@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'eisha.banks@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ella.curtis@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'elliott.sloan@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'erin.crockett@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'erin.sparwasser@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'erin.willford-skipworth@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'evan.seltzer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'evelyn.dixon1@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'famane.brown@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'frederick.filberg@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'gabriel.nah@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'geoffrey.ntosi@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'gillian.goldthwaite@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'greg.gesterling@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'gregory.stark@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'heather.robertson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'herman.harris@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'holly.blake@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'holly.stephens1@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'irina.perl@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'iris.grady@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jacob.reinert@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'james.vanderdonck@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jamie.atwood@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jarrett.brown@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jason.vollmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'javina.wilkinson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jeanette.wadley-mitchell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jeannine.bohlen@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jennifer.hennessy@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jeremy.steel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jessica.sanders@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'john.cruse@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'john.webster@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'johnny.vo@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jonathan.chattler@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'joseph.pressley@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'juanita.wilson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'julia.howard@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'justin.menefee@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'karen.johnson1@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'karmen.lyles@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kathleen.chuhran@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kathy.markman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kelley.williams-vollmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kelly.housein@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kimberly.tatum@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kristen.lawrence@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'latrina.baker@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lauren.teal@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'laurie.lloyd@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'leisa.bodway@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'licinda.peters@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'linda.gmeiner@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'linda.hook@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'louis.anderson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'louise.amburgey1@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'louise.hsu@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lucille.lee@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lyandra.emmanuel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'maiatu.davis@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'malissa.shin@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.smolenski@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.werder@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mary.greene@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mary.jones@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'matthew.clark@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'matthew.levenson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'matthew.waskiewicz@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'meghan.critzman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.connors@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.crow@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.milanese@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.shirk@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michele.lanasa@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michelle.feagins@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mohammed.islam@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nathaniel.barbato@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nathaniel.dean@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nicholas.proy@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nicole.hoey@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'noel.manlove@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'patricia.erskine@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'patricia.ingram@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'patrick.wynne@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'penny.williams@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'peter.haas@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phillip.harrell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phillip.smith@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phyllis.lewis@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'radee.skipworth@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'rafael.mack@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ray.tchoulakian@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'renee.wallace-abney@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'richard.asher@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'richard.potocek1@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'rico.batte@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'roestta.rodwell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ryan.hax@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ryan.kaufman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'ryan.kooy@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'salem.fussell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'samuel.gobrail@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'scott.filipovits@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'scott.yeager@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'shamia.blannks@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sharon.brause2@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sharon.jacksonhall@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sharon.white2@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'shelby.minchew@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sherri.stephens-washington@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sherry.perrier@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'stephanie.coppel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'stephen.stoyer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'stephen.weber@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tasha.logan@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tessie.fitton@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'theresa.schultz@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tiara.freeman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tiffany.richardson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'timothy.kelly@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tina.zanti@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'todd.brown@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tony.richardson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tonya.anderson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tracy.amos@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tyra.jeffries@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'vivian.smith@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'william.diggs@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),1,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'yolanda.charon@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'aaron.blackshire@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'alan.fredericks@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'amy.duckworth@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'andrew.mummert@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'antoinette.hazelwood@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'benjamin.simcock@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brenda.clark@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'brian.hebbel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'bridget.rineker@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'candice.savoy@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'carol.sevel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'charles.littleton@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.eberhart@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.heller@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christina.honey@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'christopher.hagepanos@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'craig.dash@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'david.hansen@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'dawn.wilkins@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'deborah.lester@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'debra.hoffman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'debra.stidham@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'denizejoice.hammond@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'donald.knode@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'edward.farmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'edward.hughes@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'evelyn.dixon@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'famane.brown@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'heather.robertson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'holly.stephens@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jacob.reinert@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'james.vanderdonck@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jason.vollmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'javina.wilkinson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'john.cruse@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'john.webster@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'joseph.pressley@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'juanita.wilson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kathy.markman@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'kimberly.tatum@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'laurie.lloyd@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'leisa.bodway@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'linda.hook@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'louis.anderson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lucille.lee@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'lyandra.emmanuel@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.smolenski@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mark.werder@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mary.greene@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'mary.jones@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'matthew.waskiewicz@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.crow@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'michael.milanese@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'nicole.hoey@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'noel.manlove@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'phyllis.lewis@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'richard.asher@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'rico.batte@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'salem.fussell@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'scott.filipovits@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'sharon.brause2@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tasha.logan@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'theresa.schultz@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tonya.anderson@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'tyra.jeffries@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'william.diggs@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),2,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'yolanda.charon@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'jason.vollmer@cms.hhs.gov'))
INSERT dbo.WorkflowPerson (WorkflowID,WorkflowStepNumber,PersonID) VALUES ((SELECT W.WorkflowID FROM dbo.Workflow W JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID AND O.OrganizationName = 'CMS'),3,(SELECT P.PersonID FROM dbo.Person P WHERE P.EmailAddress = 'joseph.pressley@cms.hhs.gov'))

DECLARE @WorkflowID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT WP.WorkflowID
	FROM dbo.WorkflowPerson WP
	ORDER BY WP.WorkflowID

OPEN oCursor
FETCH oCursor INTO @WorkflowID
WHILE @@fetch_status = 0
	BEGIN

	EXEC dbo.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = 0
	FETCH oCursor INTO @WorkflowID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End EventLog Entries for dbo.Workflow
--End Data Seed
