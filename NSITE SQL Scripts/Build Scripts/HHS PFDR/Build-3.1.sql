USE PFDR
GO

--Begin Tables
--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC Utility.AddColumn @TableName, 'ContractVehicleID', 'INT'
EXEC Utility.SetDefaultConstraint @TableName, 'ContractVehicleID', 'INT', 0
GO
--End table dbo.Contract

--Begin table dbo.Person
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC Utility.AddColumn @TableName, 'LandingPageCode', 'VARCHAR(50)'

EXEC Utility.SetDefaultConstraint @TableName, 'LandingPageCode', 'VARCHAR(50)', 'Contract'
GO
--End table dbo.Person

--Begin table Dropdown.ContractVehicle
DECLARE @TableName VARCHAR(250) = 'Dropdown.ContractVehicle'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ContractVehicle
	(
	ContractVehicleID INT IDENTITY(0,1) NOT NULL,
	ContractVehicleCode VARCHAR(50),
	ContractVehicleName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ContractVehicleID'
EXEC Utility.SetIndexClustered 'IX_ContractVehicle', @TableName, 'DisplayOrder,ContractVehicleName,ContractVehicleID'
GO

SET IDENTITY_INSERT Dropdown.ContractVehicle ON
INSERT INTO Dropdown.ContractVehicle (ContractVehicleID) VALUES (0)
SET IDENTITY_INSERT Dropdown.ContractVehicle OFF
GO

INSERT INTO Dropdown.ContractVehicle 
	(ContractVehicleName) 
VALUES
	('An open market, small purchase (under $25,000)'),
	('Any program giving preferences to a defined class'),
	('Blank Purchase Agreement (BPA)'),
	('Federal Supply Schedule'),
	('GovernmentWide Acquisition Contract (GWAC)'),
	('GSA Schedules'),
	('Indefinite Delivery, Indefinite Quantiy (IDIQ)'),
	('Multiple Award/IDIQ'),
	('NASA Solutions For Enterprise-Wide Procurement (SEWP)'),
	('Sole source, credit card purchase (usually under $2,500)')

GO
--End table Dropdown.ContractVehicle
--End Tables

--Begin Procedures
--Begin procedure dbo.AddEventLogContractEntry
EXEC Utility.DropObject 'dbo.AddEventLogContractEntry'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create Date: 2014.06.10
-- Description:	A stored procedure to add data to the dbo.EventLog table
--
-- Author:			Todd Pires
-- Create Date: 2015.10.14
-- Description:	Added the optional LogDate parameter
-- =====================================================================
CREATE PROCEDURE dbo.AddEventLogContractEntry
@Comments VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@EventCode VARCHAR(50) = NULL,
@PersonID INT = 0,
@LogDateTime DATETIME = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ContractPlacesOfPerformance VARCHAR(MAX) 
	DECLARE @ContractPointsOfContact VARCHAR(MAX) 

	IF @LogDateTime IS NULL
		SET @LogDateTime = getDate()
	--ENDIF

	SELECT 
		@ContractPlacesOfPerformance = COALESCE(@ContractPlacesOfPerformance, '') + D.ContractPlaceOfPerformance 
	FROM
		(
		SELECT
			(SELECT C.* FOR XML RAW('ContractPlaceOfPerformance'), ELEMENTS) AS ContractPlaceOfPerformance
		FROM dbo.ContractPlaceOfPerformance C
		WHERE C.ContractID = @ContractID
		) D
		
	SELECT 
		@ContractPointsOfContact = COALESCE(@ContractPointsOfContact, '') + D.ContractPointOfContact 
	FROM
		(
		SELECT
			(SELECT C.* FOR XML RAW('ContractPointOfContact'), ELEMENTS) AS ContractPointOfContact
		FROM dbo.ContractPointOfContact C
		WHERE C.ContractID = @ContractID
		) D
		
	INSERT INTO dbo.EventLog
		(Comments,EntityID,EntityTypeCode,EventCode,EventData,PersonID,CreateDateTime)
	SELECT
		@Comments,
		C.ContractID,
		'Contract',
		@EventCode,
		(
		SELECT C.*, 
		CAST(('<ContractPlacesOfPerformance>' + @ContractPlacesOfPerformance + '</ContractPlacesOfPerformance>') AS XML), 
		CAST(('<ContractPointsOfContact>' + @ContractPointsOfContact + '</ContractPointsOfContact>') AS XML) 
		FOR XML RAW('Contract'), ELEMENTS
		),
		@PersonID,
		@LogDateTime
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID
	
END
GO
--End procedure dbo.AddEventLogContractEntry

--Begin procedure dbo.ExpireContracts
EXEC Utility.DropObject 'dbo.ExpireContracts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================
-- Author:			Todd Pires
-- Create Date: 2016.08.29
-- Description:	A stored procedure to expire contracts based on a target award date
-- ================================================================================
CREATE PROCEDURE dbo.ExpireContracts

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EndOfCurrentFiscalYear VARCHAR(10) = '10/01/'

	SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) AS CHAR(4))

	UPDATE C
	SET C.IsArchived = 1
	FROM dbo.Contract C
	WHERE C.TargetAwardDate < @EndOfCurrentFiscalYear

END
GO
--End procedure dbo.ExpireContracts

--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Create Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
--
-- Author:			Todd Pires
-- Create Date: 2015.01.19
-- Description:	Added the CanHaveRecall bit
--
-- Author:			Todd Pires
-- Create Date: 2015.08.18
-- Description:	Added the CanHaveArchive and IsArchived bits
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the ContractVehicleID field
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.ContractDateEnd,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		C.ContractDateStart,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		LTRIM(C.ContractName) AS ContractName, 
		C.EstimatedSolicitationQuarter,
		C.IncumbentContractorName, 
		C.IsArchived, 
		C.ReferenceIDV, 
		C.TargetAwardDate, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeID AS FiscalYearContractRangeID, 
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeID AS TotalContractRangeID,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeID, 
		CT1.CompetitionTypeName,
		CT2.ContractTypeID, 
		CT2.ContractTypeName,
		CV.ContractVehicleID, 
		CV.ContractVehicleName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		N.NAICSID,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationID AS ContractingOrganizationID, 
		O1.OrganizationName AS ContractingOrganizationName,
		O1.OrganizationID AS FundingOrganizationID, 
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationID AS OriginatingOrganizationID, 
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCID,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeID, 
		RT.RequirementTypeName,
		UR.CanHaveArchive,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		UR.CanHaveRecall,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.ContractVehicle CV ON CV.ContractVehicleID = C.ContractVehicleID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.GetContractByEventLogID
EXEC Utility.DropObject 'dbo.GetContractByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:		Todd Pires
-- Create Date: 2015.01.20
-- Description:	A stored procedure to get contract data from the dbo.EventLog table based on an EventLogID
--
-- Author:		Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the ContractVehicleID field
-- =======================================================================================================
CREATE PROCEDURE dbo.GetContractByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @oContract XML = (SELECT EL.EventData FROM dbo.EventLog EL WHERE EL.EventLogID = @EventLogID)

	DECLARE @tContract TABLE
		(
		ContractID INT NOT NULL PRIMARY KEY,
		CompetitionTypeID INT NOT NULL DEFAULT 0,
		ContractDateEnd DATE NULL,
		ContractDateStart DATE NULL,
		ContractDescription VARCHAR (MAX) NULL,
		ContractingOfficeName VARCHAR (250) NULL,
		ContractingOrganizationID INT NOT NULL DEFAULT 0,
		ContractName VARCHAR (250) NULL,
		ContractTypeID INT NOT NULL DEFAULT 0,
		ContractVehicleID INT NOT NULL DEFAULT 0,
		CreateDateTime DATETIME,
		EstimatedSolicitationQuarter INT NOT NULL DEFAULT 0,
		FiscalYearContractRangeID INT NOT NULL DEFAULT 0,
		FundingOrganizationID INT NOT NULL DEFAULT 0,
		IncumbentContractorName VARCHAR (250) NULL,
		NAICSID INT NOT NULL DEFAULT 0,
		OriginatingOrganizationID INT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		PSCID INT NOT NULL DEFAULT 0,
		ReferenceIDV VARCHAR (250) NULL,
		RequirementTypeID INT NOT NULL DEFAULT 0,
		TargetAwardDate DATE NULL,
		TotalContractRangeID INT NOT NULL DEFAULT 0,
		TransactionNumber VARCHAR (100) NULL,
		WorkflowStatusID INT NOT NULL DEFAULT 1,
		WorkflowStepNumber INT NOT NULL DEFAULT 1
		)

	INSERT INTO @tContract
		(ContractID,CompetitionTypeID,ContractDateEnd,ContractDateStart,ContractDescription,ContractingOfficeName,ContractingOrganizationID,ContractName,ContractTypeID,ContractVehicleID,EstimatedSolicitationQuarter,FiscalYearContractRangeID,FundingOrganizationID,IncumbentContractorName,NAICSID,OriginatingOrganizationID,PSCID,ReferenceIDV,RequirementTypeID,TargetAwardDate,TotalContractRangeID,TransactionNumber,WorkflowStatusID,WorkflowStepNumber,PersonID,CreateDateTime)
	SELECT
		T.Col.value('ContractID[1]', 'INT'),
		T.Col.value('CompetitionTypeID[1]', 'INT'),
		T.Col.value('ContractDateEnd[1]', 'DATE'),
		T.Col.value('ContractDateStart[1]', 'DATE'),
		T.Col.value('ContractDescription[1]', 'VARCHAR(MAX)'),
		T.Col.value('ContractingOfficeName[1]', 'VARCHAR(250)'),
		T.Col.value('ContractingOrganizationID[1]', 'INT'),
		T.Col.value('ContractName[1]', 'VARCHAR(250)'),
		T.Col.value('ContractTypeID[1]', 'INT'),
		T.Col.value('ContractVehicleID[1]', 'INT'),
		T.Col.value('EstimatedSolicitationQuarter[1]', 'INT'),
		T.Col.value('FiscalYearContractRangeID[1]', 'INT'),
		T.Col.value('FundingOrganizationID[1]', 'INT'),
		T.Col.value('IncumbentContractorName[1]', 'VARCHAR(250)'),
		T.Col.value('NAICSID[1]', 'INT'),
		T.Col.value('OriginatingOrganizationID[1]', 'INT'),
		T.Col.value('PSCID[1]', 'INT'),
		T.Col.value('ReferenceIDV[1]', 'VARCHAR(250)'),
		T.Col.value('RequirementTypeID[1]', 'INT'),
		T.Col.value('TargetAwardDate[1]', 'DATE'),
		T.Col.value('TotalContractRangeID[1]', 'INT'),
		T.Col.value('TransactionNumber[1]', 'VARCHAR(100)'),
		T.Col.value('WorkflowStatusID[1]', 'INT'),
		T.Col.value('WorkflowStepNumber[1]', 'INT'),
		EL.PersonID,
		EL.CreateDateTime
	FROM @oContract.nodes('//Contract') T(Col), dbo.EventLog EL
	WHERE EL.EventLogID = @EventLogID
	
	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.ContractDateEnd,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		C.ContractDateStart,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		LTRIM(C.ContractName) AS ContractName, 
		C.EstimatedSolicitationQuarter, 
		C.IncumbentContractorName, 
		C.ReferenceIDV, 
		C.TargetAwardDate, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeID AS FiscalYearContractRangeID, 
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeID AS TotalContractRangeID,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeID, 
		CT1.CompetitionTypeName,
		CT2.ContractTypeID, 
		CT2.ContractTypeName,
		CV.ContractVehicleID, 
		CV.ContractVehicleName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		N.NAICSID,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationID AS ContractingOrganizationID, 
		O1.OrganizationName AS ContractingOrganizationName,
		O1.OrganizationID AS FundingOrganizationID, 
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationID AS OriginatingOrganizationID, 
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCID,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeID, 
		RT.RequirementTypeName,
		0 AS CanHaveArchive,
		0 AS CanHaveDelete,
		0 AS CanHaveEdit,
		0 AS CanHaveRecall,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName,
		dbo.FormatDateTime(C.CreateDateTime, 'mm/dd/yyyy hh:mm tt') + 
			CASE
				WHEN EXISTS (SELECT 1 FROM dbo.Person P WHERE P.PersonID = C.PersonID AND C.PersonID > 0)
				THEN ' by ' + dbo.GetPersonNameByPersonID(C.PersonID, 'LastFirst')
				ELSE ''
			END AS UpdateDateTimeFormatted
	FROM @tContract C
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.ContractVehicle CV ON CV.ContractVehicleID = C.ContractVehicleID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByEventLogID

--Begin procedure dbo.GetContractsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetContractsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on search criteria
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the NULL screen on ContractingOfficeName and ContractName
--
-- Author:			Todd Pires
-- Update date:	2015.04.10
-- Description:	Added support for the ApproverPersonID, OriginatorPersonID and ReviewerPersonID fields
--
-- Author:			Todd Pires
-- Update date:	2015.08.18
-- Description:	Added support for the IsArchived field
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added support for the ContractVehicleID field
-- ===================================================================================================
CREATE PROCEDURE dbo.GetContractsBySearchCriteria

@ApproverPersonID INT = 0,
@CompetitionTypeIDList VARCHAR(MAX) = NULL,
@ContractDateEndStart DATE = NULL,
@ContractDateEndStop DATE = NULL,
@ContractDateStartStart DATE = NULL,
@ContractDateStartStop DATE = NULL,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationIDList VARCHAR(MAX) = NULL,
@ContractName VARCHAR(250) = NULL,
@ContractTypeIDList VARCHAR(MAX) = NULL,
@ContractVehicleIDList VARCHAR(MAX) = NULL,
@EstimatedSolicitationQuarterList VARCHAR(MAX) = NULL,
@FiscalYearContractRangeIDList VARCHAR(MAX) = NULL,
@FundingOrganizationIDList VARCHAR(MAX) = NULL,
@IsArchived BIT = 0,
@IsForExport BIT = 0,
@Keyword VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'ContractName',
@OrganizationIDList VARCHAR(MAX) = NULL,
@OriginatorPersonID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PendingApprovalPersonID INT = 0,
@PersonID INT = 0,
@PlaceOfPerformanceStateIDList VARCHAR(MAX) = NULL,
@PointOfContactAgency VARCHAR(250) = NULL,
@PointOfContactEmail VARCHAR(320) = NULL,
@PointOfContactName VARCHAR(250) = NULL,
@PointOfContactTitle VARCHAR(50) = NULL,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL,
@RequirementTypeIDList VARCHAR(MAX) = NULL,
@ReviewerPersonID INT = 0,
@TargetAwardDateStart DATE = NULL,
@TargetAwardDateStop DATE = NULL,
@TargetAwardFiscalYearList VARCHAR(MAX) = NULL,
@TotalContractRangeIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT
	
	DECLARE @tContract AS AccessControl.Contract
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetContractsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetContractsBySearchCriteria
	--ENDIF
		
	CREATE TABLE #tGetContractsBySearchCriteria
		(
		ContractID INT PRIMARY KEY NOT NULL,
		PlaceOfPerformanceCity VARCHAR(300)
		)
	
	IF @Keyword IS NULL
		BEGIN
	
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND C.IsArchived = @IsArchived
	
		--Date Fields
		IF @ContractDateEndStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd < @ContractDateEndStart
	
			END
		--ENDIF
		IF @ContractDateEndStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd >= @ContractDateEndStop
	
			END
		--ENDIF
		IF @ContractDateStartStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart < @ContractDateStartStart
	
			END
		--ENDIF
		IF @ContractDateStartStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart >= @ContractDateStartStop
	
			END
		--ENDIF
		IF @TargetAwardDateStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate < @TargetAwardDateStart
	
			END
		--ENDIF
		IF @TargetAwardDateStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate > @TargetAwardDateStop
	
			END
		--ENDIF
	
		--Dropdown Fields
		IF @CompetitionTypeIDList IS NOT NULL AND LEN(RTRIM(@CompetitionTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@CompetitionTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.CompetitionTypeID
						)
	
			END
		--ENDIF
		IF @ContractingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@ContractingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractingOrganizationID
						)
	
			END
		--ENDIF
		IF @ContractTypeIDList IS NOT NULL AND LEN(RTRIM(@ContractTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractTypeID
						)
	
			END
		--ENDIF
		IF @ContractVehicleIDList IS NOT NULL AND LEN(RTRIM(@ContractVehicleIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractVehicleIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractVehicleID
						)
	
			END
		--ENDIF
		IF @EstimatedSolicitationQuarterList IS NOT NULL AND LEN(RTRIM(@EstimatedSolicitationQuarterList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@EstimatedSolicitationQuarterList, ',', 0) LTT 
						WHERE LTT.ListItem = C.EstimatedSolicitationQuarter
						)
	
			END
		--ENDIF
		IF @FiscalYearContractRangeIDList IS NOT NULL AND LEN(RTRIM(@FiscalYearContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FiscalYearContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FiscalYearContractRangeID
						)
	
			END
		--ENDIF
		IF @FundingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@FundingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FundingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FundingOrganizationID
						)
	
			END
		--ENDIF
		IF @NAICSID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.NAICSID <> @NAICSID
	
			END
		--ENDIF
		IF @OrganizationIDList IS NOT NULL AND LEN(RTRIM(@OrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@OrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.OriginatingOrganizationID
						)
	
			END
		--ENDIF
		IF @PlaceOfPerformanceStateIDList IS NOT NULL AND LEN(RTRIM(@PlaceOfPerformanceStateIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@PlaceOfPerformanceStateIDList, ',', 0) LTT 
							JOIN dbo.ContractPlaceOfPerformance CPOP ON CPOP.PlaceOfPerformanceStateID = LTT.ListItem
								AND CPOP.ContractID = C.ContractID
						)
	
			END
		--ENDIF
		IF @PSCID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.PSCID <> @PSCID
	
			END
		--ENDIF
		IF @RequirementTypeIDList IS NOT NULL AND LEN(RTRIM(@RequirementTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@RequirementTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.RequirementTypeID
						)
	
			END
		--ENDIF
		IF @TargetAwardFiscalYearList IS NOT NULL AND LEN(RTRIM(@TargetAwardFiscalYearList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TargetAwardFiscalYearList, ',', 0) LTT 
						WHERE LTT.ListItem = dbo.GetFiscalYearFromDate(C.TargetAwardDate)
						)
	
			END
		--ENDIF
		IF @TotalContractRangeIDList IS NOT NULL AND LEN(RTRIM(@TotalContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TotalContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.TotalContractRangeID
						)
	
			END
		--ENDIF
	
		-- Text Fields (individual)
		IF @ContractingOfficeName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractingOfficeName NOT LIKE '%' + @ContractingOfficeName + '%'
							OR C.ContractingOfficeName IS NULL
						)					
	
			END
		--ENDIF
		IF @ContractName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractName NOT LIKE '%' + @ContractName + '%'
							OR C.ContractName IS NULL
						)
					AND 
						(
						C.ContractDescription NOT LIKE '%' + @ContractName + '%'
							OR C.ContractDescription IS NULL
						)
	
			END
		--ENDIF
		IF @PointOfContactAgency IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactAgency LIKE '%' + @PointOfContactAgency + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactEmail IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactEmail LIKE '%' + @PointOfContactEmail + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactName LIKE '%' + @PointOfContactName + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactTitle IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactTitle LIKE '%' + @PointOfContactTitle + '%'
						)
	
			END
		--ENDIF
		IF @ReferenceIDV IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ReferenceIDV NOT LIKE '%' + @ReferenceIDV + '%'
	
			END
		--ENDIF
	
		-- Workflow field
		IF @PendingApprovalPersonID IS NOT NULL AND @PendingApprovalPersonID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractWorkflowPerson CWP
						WHERE CWP.ContractID = C.ContractID
							AND CWP.WorkflowStepNumber = C.WorkflowStepNumber
							AND CWP.PersonID = @PendingApprovalPersonID
						)
	
			END
		--ENDIF

		-- Workflow Person fields
		IF @ApproverPersonID IS NOT NULL AND @ApproverPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 1
				FROM
					(
					SELECT
						MAX(EL2.EventLogID) AS EventLogID, 
						EL2.EntityID
					FROM dbo.EventLog EL2
						JOIN dbo.Contract C ON C.ContractID = EL2.EntityID
						JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
							AND WS.WorkflowStatusCode = 'Approved'
							AND EL2.EntityTypeCode = 'Contract'
					GROUP BY EL2.EntityID
					) D JOIN dbo.EventLog EL1 ON EL1.EventLogID = D.EventLogID
						AND EL1.EntityID = T1.ContractID
						AND EL1.PersonID = @ApproverPersonID
				)
				
			END
		--ENDIF
		IF @OriginatorPersonID IS NOT NULL AND @OriginatorPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 1
				FROM dbo.EventLog EL
				WHERE EL.EntityTypeCode = 'Contract'
					AND EL.EventCode = 'Create'
					AND EL.EntityID = T1.ContractID
					AND EL.PersonID = @OriginatorPersonID
				)
				
			END
		--ENDIF
		IF @ReviewerPersonID IS NOT NULL AND @ReviewerPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 
					EL.PersonID,
					EL.EntityID,
					A.B.value('.', 'INT') AS WorkflowStepNumber
				FROM dbo.EventLog EL
					OUTER APPLY EL.EventData.nodes('Contract/WorkflowStepNumber') AS A(B)
				WHERE EL.EntityTypeCode = 'Contract'
					AND EL.EventCode IN ('IncrementWorkflow','Update')
					AND A.B.value('.', 'INT') > 1
					AND A.B.value('.', 'INT') <= (SELECT MAX(WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = EL.EntityID)
					AND EL.EntityID = T1.ContractID
					AND EL.PersonID = @ReviewerPersonID
				)
				
			END
		--ENDIF

		END
	ELSE
		BEGIN
	
		SET @Keyword = '%' + @Keyword + '%'

		-- Text Fields (multiple)
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND C.IsArchived = @IsArchived
				AND
					(
					C.ContractingOfficeName LIKE @Keyword
						OR C.ContractName LIKE @Keyword
						OR C.ContractDescription LIKE @Keyword
						OR C.IncumbentContractorName LIKE @Keyword
						OR C.ReferenceIDV LIKE @Keyword
						OR C.TransactionNumber LIKE @Keyword
						OR EXISTS
							(
							SELECT 1
							FROM dbo.ContractPointOfContact CPOC
							WHERE CPOC.ContractID = C.ContractID
								AND 
									(
									CPOC.PointOfContactEmail LIKE @Keyword
										OR CPOC.PointOfContactName LIKE @Keyword
										OR CPOC.PointOfContactPhone LIKE @Keyword
									)
							)
						OR EXISTS
							(
							SELECT 1 
							FROM dbo.ContractPlaceOfPerformance CPOP 
								JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
									AND CPOP.ContractID = C.ContractID
									AND 
										(
										CPOP.PlaceOfPerformanceCity LIKE @Keyword
											OR S.StateCode LIKE @Keyword
											OR S.StateName LIKE @Keyword
										)
							)
					)
	
		END
	--ENDIF
	
	SELECT 
		@TotalRecordCount = COUNT(T1.ContractID)			
	FROM #tGetContractsBySearchCriteria T1
	
	IF @OrderByField = 'PlaceOfPerformanceCity'
		BEGIN
	
		UPDATE T1
		SET 
			T1.PlaceOfPerformanceCity = 
				CASE
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
					THEN 'Multiple'
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
					THEN (SELECT RTRIM(CPOP.PlaceOfPerformanceCity) + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
					ELSE NULL
				END
	
		FROM #tGetContractsBySearchCriteria T1
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
	
		END
	--ENDIF

	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/
	
	IF @PageSize > 0
		BEGIN
	
		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)
	
		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
						CASE WHEN @OrderByField = 'NAICSCode' AND @OrderByDirection = 'ASC' THEN N.NAICSCode END ASC,
						CASE WHEN @OrderByField = 'NAICSCode' AND @OrderByDirection = 'DESC' THEN N.NAICSCode END DESC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
						CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
							T1.ContractID ASC
					) AS RowIndex,
				C.ContractID,
				T1.PlaceOfPerformanceCity
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
				JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
				JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
				JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID				
			)
	
		DELETE T1
		FROM #tGetContractsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.ContractID = T1.ContractID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)
	
		END
	--ENDIF
	
	INSERT INTO @tContract (ContractID) SELECT T1.ContractID FROM #tGetContractsBySearchCriteria T1
	
	IF @IsForExport = 0
		BEGIN
		
		SELECT
			@PageCount AS PageCount,
			@PageIndex AS PageIndex,
			@TotalRecordCount AS TotalRecordCount,
			UR.CanHaveDelete,
			UR.CanHaveEdit,
			C.CompetitionTypeID, 
			LEFT(C.ContractDescription, 50) + 
				CASE
					WHEN LEN(RTRIM(C.ContractDescription)) > 50
					THEN '...' 
					ELSE ''
				END AS ContractDescription, 
					
			C.ContractID, 
			C.ContractingOfficeName, 
			LTRIM(C.ContractName) AS ContractName, 
			C.ContractTypeID, 
			C.EstimatedSolicitationQuarter, 
			C.FiscalYearContractRangeID, 
			C.IncumbentContractorName, 
			C.RequirementTypeID, 
			C.TargetAwardDate, 
			dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
			C.TotalContractRangeID,
			C.ContractDateEnd, 
			dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
			CR1.ContractRangeName AS FiscalYearContractRangeName,
			CR2.ContractRangeName AS TotalContractRangeName,
			CT1.CompetitionTypeName,
			CT2.ContractTypeName,
			CV.ContractVehicleName,
			ISNULL(N.NAICSCode, '') AS NAICSCode,
			N.NAICSID, 
			ISNULL(N.NAICSName, '') AS NAICSName,
			RT.RequirementTypeName,
			ISNULL(T1.PlaceOfPerformanceCity, '') AS PlaceOfPerformanceCity
		FROM #tGetContractsBySearchCriteria T1
			JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
			JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
			JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
			JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
			JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
			JOIN Dropdown.ContractVehicle CV ON CV.ContractVehicleID = C.ContractVehicleID
			JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
			JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		ORDER BY 
			CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
			CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
			CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
			CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
			CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
			CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
			CASE WHEN @OrderByField = 'NAICSCode' AND @OrderByDirection = 'ASC' THEN N.NAICSCode END ASC,
			CASE WHEN @OrderByField = 'NAICSCode' AND @OrderByDirection = 'DESC' THEN N.NAICSCode END DESC,
			CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
			CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
			CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
			CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
			CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
				T1.ContractID ASC

		END
	ELSE
		BEGIN

		SELECT
			dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
			dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
			C.ContractDescription, 
			C.ContractID, 
			C.ContractingOfficeName, 
			LTRIM(C.ContractName) AS ContractName, 
			C.EstimatedSolicitationQuarter, 
			C.IncumbentContractorName, 
			C.ReferenceIDV, 
			dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
			C.TransactionNumber, 
			CR1.ContractRangeName AS FiscalYearContractRangeName,
			CR2.ContractRangeName AS TotalContractRangeName,
			CT1.CompetitionTypeName,
			CT2.ContractTypeName,
			CV.ContractVehicleName,
			dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
			N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
			O1.OrganizationName AS ContractingOrganizationName,
			O2.OrganizationName AS FundingOrganizationName,
			O3.OrganizationName AS OriginatingOrganizationName,
			P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
			POC.PointOfContactAgency, 
			POC.PointOfContactEmail, 
			POC.PointOfContactName, 
			POC.PointOfContactPhone, 
			POC.PointOfContactTitle,
			POP.PlaceOfPerformanceCity,
			POP.StateCode AS PlaceOfPerformanceState,
			RT.RequirementTypeName,
			WS.WorkflowStatusName
		FROM #tGetContractsBySearchCriteria T1
			JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
			JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
			JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
			JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
			JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
			JOIN Dropdown.ContractVehicle CV ON CV.ContractVehicleID = C.ContractVehicleID
			JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
			JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
			JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
			JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
			JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
			JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			OUTER APPLY
				(
				SELECT TOP 1
					CPOC.PointOfContactAgency, 
					CPOC.PointOfContactEmail, 
					CPOC.PointOfContactName, 
					CPOC.PointOfContactPhone, 
					CPOC.PointOfContactTitle
				FROM dbo.ContractPointOfContact CPOC
				WHERE CPOC.ContractID = C.ContractID
				ORDER BY CPOC.IsPrimary DESC, CPOC.ContractPointOfContactID
				) POC
			OUTER APPLY
				(
				SELECT TOP 1
					CPOP.PlaceOfPerformanceCity,
					S.StateCode
				FROM dbo.ContractPlaceOfPerformance CPOP
					JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
						AND CPOP.ContractID = C.ContractID
				ORDER BY CPOP.IsPrimary DESC, CPOP.ContractPlaceOfPerformanceID
				) POP
		ORDER BY C.ContractID

		END
	--ENDIF

	DROP TABLE #tGetContractsBySearchCriteria
	
END
GO
--End procedure dbo.GetContractsBySearchCriteria

--Begin procedure dbo.GetExpiringContracts
EXEC Utility.DropObject 'dbo.GetExpiringContracts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2016.08.26
-- Description:	A stored procedure to get data from the dbo.Contract table
-- =======================================================================
CREATE PROCEDURE dbo.GetExpiringContracts

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EndOfCurrentFiscalYear VARCHAR(10) = '10/01/'

	IF MONTH(getDate()) > 9
		SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) + 1 AS CHAR(4))
	ELSE	
		SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) AS CHAR(4))
	--ENDIF

	;
	WITH EC AS
		(
		SELECT C.ContractID
		FROM dbo.Contract C
		WHERE C.TargetAwardDate < @EndOfCurrentFiscalYear
			AND C.IsArchived = 0
		)

	SELECT 
		C.ContractID,
		LTRIM(C.ContractName) AS ContractName, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		O.OrganizationName AS OriginatingOrganizationName,
		WS.WorkflowStatusName,
		dbo.GetPersonNameByPersonID(WP.PersonID, 'FirstLast') AS PersonNameFormatted,
		P.EmailAddress
	FROM dbo.Contract C
		JOIN EC ON EC.ContractID = C.ContractID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
		JOIN dbo.Workflow W ON W.WorkflowID = C.WorkflowID
		JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID
		JOIN dbo.WorkflowPerson WP ON WP.WorkflowID = C.WorkflowID
			AND WP.WorkflowStepNumber = 1
		JOIN dbo.Person P ON P.PersonID = WP.PersonID
	ORDER BY 6, 2, 1

END
GO
--End procedure dbo.GetExpiringContracts

--Begin procedure dbo.GetExpiringContractsByPersonID
EXEC Utility.DropObject 'dbo.GetExpiringContractsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2016.08.26
-- Description:	A stored procedure to get data from the dbo.Contract table
-- =======================================================================
CREATE PROCEDURE dbo.GetExpiringContractsByPersonID

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EndOfCurrentFiscalYear VARCHAR(10) = '10/01/'

	IF MONTH(getDate()) > 9
		SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) + 1 AS CHAR(4))
	ELSE	
		SET @EndOfCurrentFiscalYear += CAST(YEAR(getDate()) AS CHAR(4))
	--ENDIF

	SELECT
		C.ContractID,
		LTRIM(C.ContractName) AS ContractName,
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		O.OrganizationName AS OriginatingOrganizationName,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
		JOIN dbo.Workflow W ON W.WorkflowID = C.WorkflowID
		JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID
		JOIN dbo.WorkflowPerson WP ON C.WorkflowID = WP.WorkflowID
			AND WP.WorkflowStepNumber = 1
			AND WP.PersonID = @PersonID
			AND C.TargetAwardDate < @EndOfCurrentFiscalYear
			AND C.IsArchived = 0
	ORDER BY C.ContractName, C.ContractID

END
GO
--End procedure dbo.GetExpiringContractsByPersonID

--Begin procedure dbo.GetPersonByHTTPContext
EXEC Utility.DropObject 'dbo.GetPersonByHTTPContext'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.01.23
-- Description:	A stored procedure to get data from the dbo.Person table based on an EmailAddress or a UserName
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the LandingPageCode field
-- ============================================================================================================
CREATE PROCEDURE dbo.GetPersonByHTTPContext
@EmailAddress VARCHAR(320) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PersonID, 
		P.EmailAddress, 
		ISNULL(P.LandingPageCode, 'Contract') AS LandingPageCode,
		P.UserName,
		dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.WorkflowPerson WP WHERE WP.PersonID = P.PersonID AND WP.WorkflowStepNumber = 1)
			THEN 1
			ELSE 0
		END AS IsOriginator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'OrganizationAdministrator')
			THEN 1
			ELSE 0
		END AS IsAdministrator,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM dbo.PersonRole PR JOIN Dropdown.Role R ON R.RoleID = PR.RoleID AND PR.PersonID = P.PersonID AND R.RoleCode = 'SuperAdministrator')
			THEN 1
			ELSE 0
		END AS IsSuperAdministrator

	FROM dbo.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.IsActive = 1
	
END
GO
--End procedure dbo.GetPersonByHTTPContext

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.04.24
-- Description:	A stored procedure to get data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Modify Date: 2014.08.08
-- Description:	Implemented the dbo.IsSuperAdministrator function
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the LandingPageCode field
-- =========================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID
@SourcePersonID INT = 0,
@TargetPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tPerson AS AccessControl.Person

	INSERT INTO @tPerson (PersonID) SELECT P.PersonID FROM dbo.Person P WHERE P.PersonID = @TargetPersonID
	
	SELECT
		O.OrganizationName,
		P.EmailAddress,
		P.FirstName,
		P.IsActive,
		P.LastName,
		ISNULL(P.LandingPageCode, 'Contract') AS LandingPageCode,
		P.OrganizationID,
		P.PersonID,
		P.Phone,
		P.Title,
		P.UserName,
		(SELECT dbo.IsSuperAdministrator(P.PersonID)) IsSuperAdministrator,
		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM dbo.Person P
		JOIN AccessControl.GetPersonRecordsByPersonID(@SourcePersonID, @tPerson) UR ON UR.PersonID = P.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.SaveContract
EXEC Utility.DropObject 'dbo.SaveContractByContractID'
EXEC Utility.DropObject 'dbo.SaveContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the ContractVehicleID field
-- ============================================================================================
CREATE PROCEDURE dbo.SaveContract

@ActiveContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@ActiveContractPointOfContactIDList VARCHAR(MAX) = '',
@CompetitionTypeID INT = 0,
@ContractDateEnd DATE = NULL,
@ContractDateStart DATE = NULL,
@ContractDescription VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationID INT = 0, 
@ContractName VARCHAR(250) = NULL,
@ContractTypeID INT = 0,
@ContractVehicleID INT = 0,
@DeletedContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@DeletedContractPointOfContactIDList VARCHAR(MAX) = '',
@EstimatedSolicitationQuarter INT = 0,
@FiscalYearContractRangeID INT = 0,
@FundingOrganizationID INT = 0, 
@IncumbentContractorName VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OriginatingOrganizationID INT = 0, 
@PersonID INT = 0,
@PrimaryContractPlaceOfPerformanceID INT = 0,
@PrimaryContractPointOfContactID INT = 0,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL, 
@RequirementTypeID INT = 0,
@TargetAwardDate DATE = NULL,
@TotalContractRangeID INT = 0,
@TransactionNumber VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @ContractID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput TABLE (ContractID INT)

		INSERT INTO dbo.Contract
			(
			CompetitionTypeID,
			ContractDateEnd,
			ContractDateStart,
			ContractDescription,
			ContractingOfficeName,
			ContractingOrganizationID,
			ContractName,
			ContractTypeID,
			ContractVehicleID,
			EstimatedSolicitationQuarter,
			FiscalYearContractRangeID,
			FundingOrganizationID,
			IncumbentContractorName,
			NAICSID,
			OriginatingOrganizationID,
			PSCID,
			ReferenceIDV,
			RequirementTypeID,
			TargetAwardDate,
			TotalContractRangeID,
			TransactionNumber
			)
		OUTPUT INSERTED.ContractID INTO @tOutput
		VALUES
			(
			@CompetitionTypeID,
			@ContractDateEnd,
			@ContractDateStart,
			@ContractDescription,
			@ContractingOfficeName,
			@ContractingOrganizationID,
			LTRIM(@ContractName),
			@ContractTypeID,
			@ContractVehicleID,
			@EstimatedSolicitationQuarter,
			@FiscalYearContractRangeID,
			@FundingOrganizationID,
			@IncumbentContractorName,
			@NAICSID,
			@OriginatingOrganizationID,
			@PSCID,
			@ReferenceIDV,
			@RequirementTypeID,
			@TargetAwardDate,
			@TotalContractRangeID,
			@TransactionNumber
			)
		
		SELECT @ContractID = O.ContractID 
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Contract
		SET
			CompetitionTypeID = @CompetitionTypeID,
			ContractDateEnd = @ContractDateEnd,
			ContractDateStart = @ContractDateStart,
			ContractDescription = @ContractDescription,
			ContractingOfficeName = @ContractingOfficeName,
			ContractingOrganizationID = @ContractingOrganizationID,
			ContractName = LTRIM(@ContractName),
			ContractTypeID = @ContractTypeID,
			ContractVehicleID = @ContractVehicleID,
			EstimatedSolicitationQuarter = @EstimatedSolicitationQuarter,
			FiscalYearContractRangeID = @FiscalYearContractRangeID,
			FundingOrganizationID = @FundingOrganizationID,
			IncumbentContractorName = @IncumbentContractorName,
			NAICSID = @NAICSID,
			OriginatingOrganizationID = @OriginatingOrganizationID,
			PSCID = @PSCID,
			ReferenceIDV = @ReferenceIDV,
			RequirementTypeID = @RequirementTypeID,
			TargetAwardDate = @TargetAwardDate,
			TotalContractRangeID = @TotalContractRangeID,
			TransactionNumber = @TransactionNumber
		WHERE ContractID = @ContractID
		
		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPlaceOfPerformanceIDList)) > 0
		BEGIN
	
		UPDATE CPOP
		SET 
			CPOP.ContractID = @ContractID,
			CPOP.IsPrimary = 
				CASE
					WHEN CPOP.ContractPlaceOfPerformanceID = @PrimaryContractPlaceOfPerformanceID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@ActiveContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPointOfContactIDList)) > 0
		BEGIN
	
		UPDATE CPOC
		SET 
			CPOC.ContractID = @ContractID,
			CPOC.IsPrimary = 
				CASE
					WHEN CPOC.ContractPointOfContactID = @PrimaryContractPointOfContactID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@ActiveContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPlaceOfPerformanceIDList)) > 0
		BEGIN
		
		DELETE CPOP
		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@DeletedContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPointOfContactIDList)) > 0
		BEGIN
		
		DELETE CPOC
		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@DeletedContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	EXEC dbo.AddEventLogContractEntry NULL, @ContractID, @EventCode, @PersonID, NULL
	EXEC dbo.GetContractByContractID @ContractID = @ContractID, @PersonID = @PersonID

END
GO
--End procedure dbo.SaveContract

--Begin procedure dbo.SavePerson
EXEC Utility.DropObject 'dbo.SavePerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Person table
--
-- Author:			Todd Pires
-- Update Date: 2014.08.01
-- Description:	Added the check for duplicate email addresses on insert
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the LTRIM() to the VARCHAR params
--
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	Added the LandingPageCode field
-- ====================================================================
CREATE PROCEDURE dbo.SavePerson
@EmailAddress VARCHAR(320) = NULL,
@FirstName VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsSuperAdministrator BIT = NULL,
@LastName VARCHAR(50) = NULL,
@LandingPageCode VARCHAR(50) = 'Contract',
@OrganizationID INT = 0,
@OrganizationRoleIDList VARCHAR(MAX) = NULL,
@Phone VARCHAR(50) = NULL,
@SourcePersonID INT = 0,
@TargetPersonID INT = 0,
@Title VARCHAR(50) = NULL,
@UserName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)

	SET @EmailAddress = LTRIM(@EmailAddress)
	SET @FirstName = LTRIM(@FirstName)
	SET @LastName = LTRIM(@LastName)
	SET @Phone = LTRIM(@Phone)
	SET @Title = LTRIM(@Title)
	SET @UserName = LTRIM(@UserName)
	
	IF @TargetPersonID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput1 TABLE (PersonID INT)
		
		IF NOT EXISTS (SELECT 1 FROM dbo.Person P WHERE P.EmailAddress = @EmailAddress)
			BEGIN
			
			INSERT INTO dbo.Person
				(
				EmailAddress,
				FirstName,
				IsActive,
				LastName,
				LandingPageCode,
				OrganizationID,
				Phone,
				Title,
				UserName
				)
			OUTPUT INSERTED.PersonID INTO @tOutput1
			VALUES
				(
				@EmailAddress,
				@FirstName,
				@IsActive,
				@LastName,
				@LandingPageCode,
				@OrganizationID,
				@Phone,
				@Title,
				@UserName
				)
	
			SELECT @TargetPersonID = O.PersonID 
			FROM @tOutput1 O

			END
		--ENDIF

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Person
		SET
			EmailAddress = @EmailAddress,
			FirstName = @FirstName,
			IsActive = @IsActive,
			LastName = @LastName,
			LandingPageCode = @LandingPageCode,
			OrganizationID = @OrganizationID,
			Phone = @Phone,
			Title = @Title,
			UserName = @UserName
		WHERE PersonID = @TargetPersonID
	
		END
	--ENDIF
	
	IF @TargetPersonID > 0
		BEGIN
		
		DECLARE @tOutput2 TABLE 
			(
			RoleID INT NOT NULL DEFAULT 0,
			OrganizationID INT NOT NULL DEFAULT 0,
			Mode VARCHAR(10)
			)
	
		IF @IsSuperAdministrator = 1
			BEGIN
	
			DELETE PR
			OUTPUT DELETED.RoleID, 0, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM Dropdown.Role R
					WHERE R.RoleCode = 'SuperAdministrator'
						AND R.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, RoleID)
			OUTPUT INSERTED.RoleID, 0, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				R.RoleID
			FROM Dropdown.Role R
			WHERE R.RoleCode = 'SuperAdministrator'
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.RoleID = R.RoleID
					)
			
			END
		ELSE
			BEGIN
			
			DECLARE @tPersonRole TABLE 
				(
				OrganizationID INT NOT NULL DEFAULT 0, 
				RoleID INT NOT NULL DEFAULT 0
				)
	
			INSERT INTO @tPersonRole
				(OrganizationID,RoleID)
			SELECT
				LEFT(LTT.ListItem, CHARINDEX('_', LTT.ListItem) - 1),
				RIGHT(LTT.ListItem, LEN(LTT.ListItem) - CHARINDEX('_', LTT.ListItem))
			FROM Utility.ListToTable(@OrganizationRoleIDList, ',', 0) LTT 
	
			DELETE PR
			OUTPUT DELETED.RoleID, DELETED.OrganizationID, 'DELETED' INTO @tOutput2
			FROM dbo.PersonRole PR
			WHERE PR.PersonID = @TargetPersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tPersonRole TPR
					WHERE TPR.OrganizationID = PR.OrganizationID
						AND TPR.RoleID = PR.RoleID
					)
					
			INSERT INTO dbo.PersonRole
				(PersonID, OrganizationID, RoleID)
			OUTPUT INSERTED.RoleID, INSERTED.OrganizationID, 'INSERTED' INTO @tOutput2
			SELECT
				@TargetPersonID,
				TPR.OrganizationID,
				TPR.RoleID
			FROM @tPersonRole TPR
			WHERE NOT EXISTS
					(
					SELECT 1
					FROM dbo.PersonRole PR
					WHERE PR.PersonID = @TargetPersonID
						AND PR.OrganizationID = TPR.OrganizationID
						AND PR.RoleID = TPR.RoleID
					)
			
			END
		--ENDIF

		EXEC dbo.AddEventLogPersonEntry @TargetPersonID = @TargetPersonID, @EventCode = @EventCode, @SourcePersonID = @SourcePersonID
		
		END
	--ENDIF

END
GO
--End procedure dbo.SavePerson

--Begin procedure dbo.UpdateTargetAwardDateByContractIDList
EXEC Utility.DropObject 'dbo.UpdateTargetAwardDateByContractIDList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================
-- Author:			Todd Pires
-- Create Date: 2016.08.24
-- Description:	A stored procedure to update the increment target award dates of a list of contracts
-- =================================================================================================
CREATE PROCEDURE dbo.UpdateTargetAwardDateByContractIDList

@ContractIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nContractID INT

	UPDATE C
	SET C.TargetAwardDate = DATEADD(YYYY, 1, C.TargetAwardDate)
	FROM dbo.Contract C
		JOIN Utility.ListToTable(@ContractIDList, ',', 0) LTT ON CAST(LTT.ListItem AS INT) = C.ContractID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT C.ContractID
		FROM dbo.Contract C
			JOIN Utility.ListToTable(@ContractIDList, ',', 0) LTT ON CAST(LTT.ListItem AS INT) = C.ContractID
		ORDER BY C.ContractID

	OPEN oCursor
	FETCH oCursor INTO @nContractID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC dbo.AddEventLogContractEntry 'Target award date incremented by one year', @nContractID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nContractID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure dbo.UpdateTargetAwardDateByContractIDList

--Begin procedure Dropdown.GetContractVehicles
EXEC Utility.DropObject 'Dropdown.GetContractVehicles'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================
-- Author:			Todd Pires
-- Create Date: 2016.08.15
-- Description:	A stored procedure to data from the Dropdown.ContractVehicle table
-- ===============================================================================
CREATE PROCEDURE Dropdown.GetContractVehicles

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContractVehicleID,
		T.ContractVehicleName
	FROM Dropdown.ContractVehicle T
	WHERE T.IsActive = 1
		AND T.ContractVehicleID > 0
	ORDER BY T.DisplayOrder, T.ContractVehicleName, T.ContractVehicleID

END
GO
--End procedure Dropdown.GetContractVehicles
--End Procedures
