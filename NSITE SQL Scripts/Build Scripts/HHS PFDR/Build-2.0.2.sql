USE PFDR
GO

--Begin Procedures
--Begin procedure dbo.GetOrganizationAdministratorsByOrganizationID
EXEC Utility.DropObject 'dbo.GetOrganizationAdministratorsByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.10.22
-- Description:	A stored procedure to get Organization Administrators based on an OrganizationID
-- =============================================================================================
CREATE PROCEDURE dbo.GetOrganizationAdministratorsByOrganizationID
@OrganizationID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF (@OrganizationID = 0)
		BEGIN
		
		SELECT
			PR.PersonID,
			P.EmailAddress,
			dbo.GetPersonNameByPersonID(PR.PersonID, 'LastFirst') AS PersonNameFormatted,
			O.OrganizationName
		FROM dbo.PersonRole PR
			JOIN dbo.Person P ON P.PersonID = PR.PersonID
			JOIN Dropdown.Organization O ON O.OrganizationID = PR.OrganizationID
			JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
				AND R.RoleCode = 'OrganizationAdministrator'
		ORDER BY O.OrganizationName, 3

		END
	ELSE
		BEGIN

		SELECT
			D.PersonID,
			P.EmailAddress,
			dbo.GetPersonNameByPersonID(D.PersonID, 'LastFirst') AS PersonNameFormatted
		FROM
			(
			SELECT DISTINCT
				PR.PersonID
			FROM dbo.PersonRole PR
				JOIN Dropdown.Organization O ON O.OrganizationID = PR.OrganizationID
				JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
					AND R.RoleCode = 'OrganizationAdministrator'
					AND EXISTS
						(
						SELECT 1
						FROM Utility.ListToTable((SELECT OH.OrganizationIDLineage FROM Dropdown.OrganizationHierarchy OH WHERE OH.OrganizationID = @OrganizationID), ',', 0) LTT
						WHERE LTT.ListItem = PR.OrganizationID
						)
			) D
			JOIN dbo.Person P ON P.PersonID = D.PersonID
		ORDER BY 3

		END
	--ENDIF	
END
GO
--End procedure dbo.GetOrganizationAdministratorsByOrganizationID
--End Procedures

--Begin Data
IF NOT EXISTS (SELECT 1 FROM Dropdown.ContractType CT WHERE CT.ContractTypeName = 'IDIQ')
	BEGIN

	INSERT INTO Dropdown.ContractType
		(ContractTypeName)
	VALUES 
		('IDIQ')

	UPDATE Dropdown.ContractType
	SET DisplayOrder = DisplayOrder + 1
	WHERE ContractTypeName = 'TBD'

	DECLARE @tTable TABLE (DisplayOrder INT NOT NULL IDENTITY (1,1), ContractTypeID INT)
	
	INSERT INTO @tTable
		(ContractTypeID)
	SELECT
		CT.ContractTypeID
	FROM Dropdown.ContractType CT
	WHERE CT.ContractTypeID <> 0
		AND CT.ContractTypeName <> 'TBD'
	ORDER BY CT.ContractTypeName

	UPDATE CT
	SET CT.DisplayOrder = T.DisplayOrder
	FROM Dropdown.ContractType CT
		JOIN @tTable T ON T.ContractTypeID = CT.ContractTypeID

	END
--ENDIF
GO

UPDATE Dropdown.ContractType
SET IsActive = 0
WHERE ContractTypeName = 'IDIQ'
GO
--End Data
