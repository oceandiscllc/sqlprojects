USE PFDR
GO

--Begin Data
IF NOT EXISTS (SELECT 1 FROM Dropdown.ContractRange CR WHERE CR.ContractRangeName = '> $20K and <= $150K')
	BEGIN

	UPDATE Dropdown.ContractRange
	SET DisplayOrder = DisplayOrder + 1
	WHERE ContractRangeID > 0

	INSERT INTO Dropdown.ContractRange
		(ContractRangeName, DisplayOrder)
	VALUES 
		('> $25K and < $150K', 1)

	END
--ENDIF
GO

UPDATE Dropdown.ContractRange
SET ContractRangeName = '>= $150K and < $500K'
WHERE ContractRangeName = '>  $150K and < $500K'
GO
--End Data