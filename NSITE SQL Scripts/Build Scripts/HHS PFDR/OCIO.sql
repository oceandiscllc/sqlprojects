USE PFDR
GO

--Begin Tables
--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC Utility.AddColumn @TableName, 'ContractITDescription', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @TableName, 'IsITContract', 'BIT'
EXEC Utility.AddColumn @TableName, 'IsITForNewRequirement', 'BIT'
EXEC Utility.AddColumn @TableName, 'ITCategoryID', 'INT'
EXEC Utility.AddColumn @TableName, 'ITInvestmentIdentifier', 'VARCHAR(50)'
EXEC Utility.AddColumn @TableName, 'ITInvestmentType', 'VARCHAR(50)'
EXEC Utility.AddColumn @TableName, 'ITSubcategoryID', 'INT'

EXEC Utility.SetDefaultConstraint @TableName, 'IsITContract', 'BIT', 0, 1
EXEC Utility.SetDefaultConstraint @TableName, 'IsITForNewRequirement', 'BIT', 0, 1
EXEC Utility.SetDefaultConstraint @TableName, 'ITCategoryID', 'INT', 0, 1
EXEC Utility.SetDefaultConstraint @TableName, 'ITSubcategoryID', 'INT', 0, 1
GO
--End table dbo.Contract

--Begin table Dropdown.ITCategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITCategory'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ITCategory
	(
	ITCategoryID INT IDENTITY(0,1) NOT NULL,
	ITCategoryName VARCHAR(250),
	ITCategoryDescription VARCHAR(MAX),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ITCategoryID'
EXEC Utility.SetIndexClustered 'IX_Module', @TableName, 'DisplayOrder,ITCategoryName,ITCategoryID'
GO

SET IDENTITY_INSERT Dropdown.ITCategory ON
INSERT INTO Dropdown.ITCategory (ITCategoryID) VALUES (0)
SET IDENTITY_INSERT Dropdown.ITCategory OFF
GO

INSERT INTO Dropdown.ITCategory 
	(ITCategoryName) 
VALUES
	('Development'),
	('Hardware'),
	('Security'),
	('Services'),
	('Software'),
	('Storage'),
	('Telecom')
GO
--End table Dropdown.ITCategory

--Begin table Dropdown.ITSubcategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITSubcategory'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ITSubcategory
	(
	ITSubcategoryID INT IDENTITY(0,1) NOT NULL,
	ITSubcategoryName VARCHAR(250),
	IsActive BIT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC Utility.SetPrimaryKeyClustered @TableName, 'ITSubcategoryID'
GO

SET IDENTITY_INSERT Dropdown.ITSubcategory ON
INSERT INTO Dropdown.ITSubcategory (ITSubcategoryID) VALUES (0)
SET IDENTITY_INSERT Dropdown.ITSubcategory OFF
GO

INSERT INTO Dropdown.ITSubcategory 
	(ITSubcategoryName) 
VALUES
	('Cloud Storage'),
	('COTS Enhancement'),
	('Desktops and Laptop Systems'),
	('End User Software'),
	('Enterprise Software'),
	('Hardware'),
	('Help Desk Services'),
	('Infrastructure as a Service (IaaS)'),
	('IT Infrastructure Management'),
	('IT Management Support Services'),
	('IT Project Management'),
	('Local Storage'),
	('Mainframe Systems and Servers'),
	('Management Services'),
	('Mobile Devices'),
	('Network Storage'),
	('Other Provisioned Services (non-cloud)'),
	('Other Service Contracts'),
	('Platform as a Service (PaaS)'),
	('Software'),
	('Software as a Service (SaaS)'),
	('Software License Maintenance and Support'),
	('System Integration'),
	('Telecom Data'),
	('Telecom Land-line'),
	('Telecom Mobile'),
	('Telecom Voice'),
	('Web Content, Hosting, or Infrastructure Services')
GO
--End table Dropdown.ITSubcategory

--Begin table Dropdown.ITCategoryITSubcategory
DECLARE @TableName VARCHAR(250) = 'Dropdown.ITCategoryITSubcategory'

EXEC Utility.DropObject @TableName

CREATE TABLE Dropdown.ITCategoryITSubcategory
	(
	ITCategoryITSubcategory INT IDENTITY(1,1) NOT NULL,
	ITCategoryID INT,
	ITSubcategoryID INT,
	DisplayOrder INT
	)

EXEC Utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ITCategoryID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'ITSubcategoryID', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'ITCategoryITSubcategory'
EXEC Utility.SetIndexClustered 'IX_Module', @TableName, 'ITCategoryID,DisplayOrder,ITSubcategoryID'
GO

INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Development'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'COTS Enhancement'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Development'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'IT Project Management'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Development'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Software'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Development'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'System Integration'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Desktops and Laptop Systems'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Help Desk Services'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'IT Infrastructure Management'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Mainframe Systems and Servers'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Mobile Devices'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Hardware'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Service Contracts'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Security'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Hardware'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Security'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Management Services'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Security'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Service Contracts'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Security'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Software'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Help Desk Services'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Infrastructure as a Service (IaaS)'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'IT Management Support Services'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Provisioned Services (non-cloud)'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Platform as a Service (PaaS)'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Software as a Service (SaaS)'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Services'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Web Content, Hosting, or Infrastructure Services'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Software'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'End User Software'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Software'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Enterprise Software'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Software'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Service Contracts'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Software'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Software License Maintenance and Support'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Storage'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Cloud Storage'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Storage'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Local Storage'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Storage'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Network Storage'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Storage'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Service Contracts'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Telecom'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Other Service Contracts'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Telecom'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Telecom Data'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Telecom'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Telecom Land-line'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Telecom'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Telecom Mobile'
INSERT INTO Dropdown.ITCategoryITSubcategory (ITCategoryID,ITSubcategoryID) SELECT (SELECT ITC.ITCategoryID FROM Dropdown.ITCategory ITC WHERE ITC.ITCategoryName = 'Telecom'), ITS.ITSubcategoryID FROM Dropdown.ITSubcategory ITS WHERE ITS.ITSubcategoryName = 'Telecom Voice'
GO

UPDATE AT
SET AT.DisplayOrder = 99
FROM Dropdown.ITCategoryITSubcategory AT
	JOIN Dropdown.ITSubcategory ITS ON ITS.ITSubcategoryID = AT.ITSubcategoryID
		AND ITS.ITSubcategoryName = 'Other Service Contracts'
GO
--End table Dropdown.ITCategoryITSubcategory

--Begin table Dropdown.OperatingDivision
DECLARE @TableName VARCHAR(250) = 'Dropdown.OperatingDivision'

EXEC Utility.DropObject @TableName
GO
--End table Dropdown.OperatingDivision

--End Tables

--Begin Procedures
--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Create Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
--
-- Author:			Todd Pires
-- Create Date: 2014.08.30
-- Description:	Implemented the OCIO Requirements
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		C.ContractDateEnd,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractITDescription,
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.IsITContract, 
		C.IsITForNewRequirement, 
		C.ITCategoryID, 
		C.ITInvestmentIdentifier, 
		C.ITInvestmentType, 
		C.ITSubcategoryID, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		ITC.ITCategoryName,
		ITS.ITSubcategoryName,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.ITCategory ITC ON ITC.ITCategoryID = C.ITCategoryID
		JOIN Dropdown.ITSubcategory ITS ON ITS.ITSubcategoryID = C.ITSubcategoryID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.SaveContract
EXEC Utility.DropObject 'dbo.SaveContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.17
-- Description:	A stored procedure to save data to the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.08.30
-- Description:	Implemented the OCIO Requirements
-- ============================================================================================
CREATE PROCEDURE dbo.SaveContract
@ActiveContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@ActiveContractPointOfContactIDList VARCHAR(MAX) = '',
@CompetitionTypeID INT = 0,
@ContractDateEnd DATE = NULL,
@ContractDateStart DATE = NULL,
@ContractDescription VARCHAR(MAX) = NULL,
@ContractID INT = 0,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationID INT = 0, 
@ContractITDescription VARCHAR(MAX) = NULL,
@ContractName VARCHAR(250) = NULL,
@ContractTypeID INT = 0,
@DeletedContractPlaceOfPerformanceIDList VARCHAR(MAX) = '',
@DeletedContractPointOfContactIDList VARCHAR(MAX) = '',
@EstimatedSolicitationQuarter INT = 0,
@FiscalYearContractRangeID INT = 0,
@FundingOrganizationID INT = 0, 
@IncumbentContractorName VARCHAR(250) = NULL,
@IsITContract BIT = 0,
@IsITForNewRequirement BIT = 0,
@ITCategoryID INT = 0,
@ITInvestmentIdentifier VARCHAR(50) = NULL,
@ITInvestmentType VARCHAR(50) = NULL,
@ITSubcategoryID INT = 0,
@NAICSID INT = 0,
@OriginatingOrganizationID INT = 0, 
@PersonID INT = 0,
@PrimaryContractPlaceOfPerformanceID INT = 0,
@PrimaryContractPointOfContactID INT = 0,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL, 
@RequirementTypeID INT = 0,
@TargetAwardDate DATE = NULL,
@TotalContractRangeID INT = 0,
@TransactionNumber VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EventCode VARCHAR(50)
	
	IF @ContractID = 0
		BEGIN

		SET @EventCode = 'Create'
		
		DECLARE @tOutput TABLE (ContractID INT)

		INSERT INTO dbo.Contract
			(
			CompetitionTypeID,
			ContractDateEnd,
			ContractDateStart,
			ContractDescription,
			ContractingOfficeName,
			ContractingOrganizationID,
			ContractITDescription,
			ContractName,
			ContractTypeID,
			EstimatedSolicitationQuarter,
			FiscalYearContractRangeID,
			FundingOrganizationID,
			IncumbentContractorName,
			IsITContract,
			IsITForNewRequirement,
			ITCategoryID,
			ITInvestmentIdentifier,
			ITInvestmentType,
			ITSubcategoryID,
			NAICSID,
			OriginatingOrganizationID,
			PSCID,
			ReferenceIDV,
			RequirementTypeID,
			TargetAwardDate,
			TotalContractRangeID,
			TransactionNumber
			)
		OUTPUT INSERTED.ContractID INTO @tOutput
		VALUES
			(
			@CompetitionTypeID,
			@ContractDateEnd,
			@ContractDateStart,
			@ContractDescription,
			@ContractingOfficeName,
			@ContractingOrganizationID,
			@ContractITDescription,			
			@ContractName,
			@ContractTypeID,
			@EstimatedSolicitationQuarter,
			@FiscalYearContractRangeID,
			@FundingOrganizationID,
			@IncumbentContractorName,
			@IsITContract,
			@IsITForNewRequirement,
			@ITCategoryID,
			@ITInvestmentIdentifier,
			@ITInvestmentType,
			@ITSubcategoryID,
			@NAICSID,
			@OriginatingOrganizationID,
			@PSCID,
			@ReferenceIDV,
			@RequirementTypeID,
			@TargetAwardDate,
			@TotalContractRangeID,
			@TransactionNumber
			)
		
		SELECT @ContractID = O.ContractID 
		FROM @tOutput O

		END
	ELSE
		BEGIN

		SET @EventCode = 'Update'
	
		UPDATE dbo.Contract
		SET
			CompetitionTypeID = @CompetitionTypeID,
			ContractDateEnd = @ContractDateEnd,
			ContractDateStart = @ContractDateStart,
			ContractDescription = @ContractDescription,
			ContractingOfficeName = @ContractingOfficeName,
			ContractingOrganizationID = @ContractingOrganizationID,
			ContractITDescription = @ContractITDescription,
			ContractName = @ContractName,
			ContractTypeID = @ContractTypeID,
			EstimatedSolicitationQuarter = @EstimatedSolicitationQuarter,
			FiscalYearContractRangeID = @FiscalYearContractRangeID,
			FundingOrganizationID = @FundingOrganizationID,
			IncumbentContractorName = @IncumbentContractorName,
			IsITContract = @IsITContract,
			IsITForNewRequirement = @IsITForNewRequirement,
			ITCategoryID = @ITCategoryID,
			ITInvestmentIdentifier = @ITInvestmentIdentifier,
			ITInvestmentType = @ITInvestmentType,
			ITSubcategoryID = @ITSubcategoryID,
			NAICSID = @NAICSID,
			OriginatingOrganizationID = @OriginatingOrganizationID,
			PSCID = @PSCID,
			ReferenceIDV = @ReferenceIDV,
			RequirementTypeID = @RequirementTypeID,
			TargetAwardDate = @TargetAwardDate,
			TotalContractRangeID = @TotalContractRangeID,
			TransactionNumber = @TransactionNumber
		WHERE ContractID = @ContractID
		
		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPlaceOfPerformanceIDList)) > 0
		BEGIN
	
		UPDATE CPOP
		SET 
			CPOP.ContractID = @ContractID,
			CPOP.IsPrimary = 
				CASE
					WHEN CPOP.ContractPlaceOfPerformanceID = @PrimaryContractPlaceOfPerformanceID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@ActiveContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@ActiveContractPointOfContactIDList)) > 0
		BEGIN
	
		UPDATE CPOC
		SET 
			CPOC.ContractID = @ContractID,
			CPOC.IsPrimary = 
				CASE
					WHEN CPOC.ContractPointOfContactID = @PrimaryContractPointOfContactID
					THEN 1
					ELSE 0
				END

		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@ActiveContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPlaceOfPerformanceIDList)) > 0
		BEGIN
		
		DELETE CPOP
		FROM dbo.ContractPlaceOfPerformance CPOP
			JOIN Utility.ListToTable(@DeletedContractPlaceOfPerformanceIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOP.ContractPlaceOfPerformanceID

		END
	--ENDIF

	IF LEN(RTRIM(@DeletedContractPointOfContactIDList)) > 0
		BEGIN
		
		DELETE CPOC
		FROM dbo.ContractPointOfContact CPOC
			JOIN Utility.ListToTable(@DeletedContractPointOfContactIDList, ',', 0) LTT ON 
				CAST(LTT.ListItem AS INT) = CPOC.ContractPointOfContactID

		END
	--ENDIF

	EXEC dbo.AddEventLogContractEntry @ContractID = @ContractID, @EventCode = @EventCode, @PersonID = @PersonID
	EXEC dbo.GetContractByContractID @ContractID = @ContractID, @PersonID = @PersonID

END
GO
--End procedure dbo.SaveContract

--Begin procedure Dropdown.GetITCategories
EXEC Utility.DropObject 'Dropdown.GetITCategories'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:			Todd Pires
-- Create Date: 2014.08.30
-- Description:	A stored procedure to data from the Dropdown.ITCategory table
-- ==========================================================================
CREATE PROCEDURE Dropdown.GetITCategories

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ITCategoryID,
		T.ITCategoryName,
		T.ITCategoryDescription
	FROM Dropdown.ITCategory T
	WHERE T.IsActive = 1
		AND T.ITCategoryID > 0
	ORDER BY T.DisplayOrder, T.ITCategoryName, T.ITCategoryID

END
GO
--End procedure Dropdown.GetITCategories

--Begin procedure Dropdown.GetITSubcategoriesByITCategoryID
EXEC Utility.DropObject 'Dropdown.GetITSubcategoriesByITCategoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author:			Todd Pires
-- Create Date: 2014.08.30
-- Description:	A stored procedure to data from the Dropdown.ITSubcategory table based on an ITCategoryID
-- ======================================================================================================
CREATE PROCEDURE Dropdown.GetITSubcategoriesByITCategoryID
@ITCategoryID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ITS.ITSubcategoryID,
		ITS.ITSubcategoryName
	FROM Dropdown.ITSubcategory ITS
		JOIN Dropdown.ITCategoryITSubcategory AT ON AT.ITSubcategoryID = ITS.ITSubcategoryID
			AND AT.ITCategoryID = @ITCategoryID
			AND ITS.IsActive = 1
	ORDER BY AT.DisplayOrder, ITS.ITSubcategoryName, ITS.ITSubcategoryID

END
GO
--End procedure Dropdown.GetITSubcategoriesByITCategoryID
--End Procedures
