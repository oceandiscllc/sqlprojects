USE PFDR
GO

--Begin Functions
--Begin function Dropdown.GetAdministerdOrganizationsByPersonID
EXEC Utility.DropObject 'Dropdown.GetAdministerdOrganizationsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.08.06
-- Description:	A function to return the organizations for which a given PersonID is an administrator
--
-- Author:			Todd Pires
-- Update Date:	2014.09.15
-- Description:	A removed node level from the results, made organization id distinct
-- ==================================================================================================

CREATE FUNCTION Dropdown.GetAdministerdOrganizationsByPersonID
(
@PersonID INT
)

RETURNS @tReturn table 
	(
	OrganizationID INT PRIMARY KEY NOT NULL
	) 

AS
BEGIN

	WITH HD (OrganizationID,ParentOrganizationID,NodeLevel)
		AS 
		(
		SELECT
			O.OrganizationID, 
			O.ParentOrganizationID, 
			1 
		FROM Dropdown.Organization O
			JOIN
				(
				SELECT
					R.RoleCode,
					PR.OrganizationID
				FROM dbo.PersonRole PR
					JOIN Dropdown.Role R ON R.RoleID = PR.RoleID
						AND PR.PersonID = @PersonID
						AND EXISTS
							(
							SELECT 1
							FROM Dropdown.Role R
							WHERE R.RoleID = PR.RoleID
								AND R.IsAdministrator = 1
							)
				) D ON D.OrganizationID = O.OrganizationID
	
		UNION ALL
		
		SELECT
			O.OrganizationID, 
			O.ParentOrganizationID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM Dropdown.Organization O
			JOIN HD ON HD.OrganizationID = O.ParentOrganizationID 
		)
	
	INSERT INTO @tReturn
		(OrganizationID)
	SELECT DISTINCT
		HD.OrganizationID
	FROM HD

	RETURN
END
GO
--End function Dropdown.GetAdministerdOrganizationsByPersonID
--End Functions