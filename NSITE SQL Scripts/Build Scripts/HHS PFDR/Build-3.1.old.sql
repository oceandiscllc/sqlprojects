USE PFDR
GO

--Begin Schemas

--Begin Tables
--Begin table dbo.Contract
DECLARE @TableName VARCHAR(250) = 'dbo.Contract'

EXEC Utility.AddColumn @TableName, 'IsArchived', 'BIT'
EXEC Utility.SetDefaultConstraint @TableName, 'IsArchived', 'BIT', 0
GO
--End table dbo.Contract
--End Tables

--Begin Procedures
--Begin procedure dbo.ArchiveContractByContractID
EXEC Utility.DropObject 'dbo.ArchiveContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create Date: 2015.08.18
-- Description:	A stored procedure to archive a contract
-- =====================================================
CREATE PROCEDURE dbo.ArchiveContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE C
	SET C.IsArchived = 1
	FROM dbo.Contract C
	WHERE C.ContractID = @ContractID

	EXEC dbo.AddEventLogContractEntry @ContractID = @ContractID, @EventCode = 'Archive', @PersonID = @PersonID
	
END
GO
--End procedure dbo.ArchiveContractByContractID

--Begin procedure dbo.GetContractByContractID
EXEC Utility.DropObject 'dbo.GetContractByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on a ContractID
--
-- Author:			Todd Pires
-- Create Date: 2014.04.09
-- Description:	Implemented the Contract / PlaceOfPerformanceCity one to many realtionship
--
-- Author:			Todd Pires
-- Create Date: 2014.08.06
-- Description:	Added a CAST to the MAX(CWP.WorkflowStepNumber) call to play nice with dapper
--
-- Author:			Todd Pires
-- Create Date: 2015.01.19
-- Description:	Added the CanHaveRecall bit
--
-- Author:			Todd Pires
-- Create Date: 2015.08.18
-- Description:	Added the CanHaveArchive and IsArchived bits
-- =============================================================================================
CREATE PROCEDURE dbo.GetContractByContractID
@ContractID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContract AS AccessControl.Contract
	INSERT INTO @tContract (ContractID) SELECT C.ContractID FROM dbo.Contract C WHERE C.ContractID = @ContractID

	SELECT
		(SELECT MAX(CWP.WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID) AS WorkflowStepCount,
		C.CompetitionTypeID, 
		dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
		C.ContractDateEnd,
		dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
		C.ContractDateStart,
		C.ContractDescription, 
		C.ContractID, 
		C.ContractingOfficeName, 
		C.ContractingOrganizationID, 
		C.ContractName, 
		C.ContractTypeID, 
		C.EstimatedSolicitationQuarter, 
		C.FiscalYearContractRangeID, 
		C.FundingOrganizationID, 
		C.IncumbentContractorName, 
		C.IsArchived, 
		C.NAICSID,
		C.OriginatingOrganizationID, 
		C.PSCID,
		C.ReferenceIDV, 
		C.RequirementTypeID, 
		dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
		C.TargetAwardDate, 
		C.TotalContractRangeID,
		C.TransactionNumber, 
		C.WorkflowStepNumber,
		CR1.ContractRangeName AS FiscalYearContractRangeName,
		CR2.ContractRangeName AS TotalContractRangeName,
		CT1.CompetitionTypeName,
		CT2.ContractTypeName,
		dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
		N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
		O1.OrganizationName AS ContractingOrganizationName,
		O2.OrganizationName AS FundingOrganizationName,
		O3.OrganizationName AS OriginatingOrganizationName,
		P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
		RT.RequirementTypeName,
		UR.CanHaveArchive,
		UR.CanHaveDelete,
		UR.CanHaveEdit,
		UR.CanHaveRecall,
		WS.WorkflowStatusCode,
		WS.WorkflowStatusName
	FROM dbo.Contract C
		JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = C.ContractID
		JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
		JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
		JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
		JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
		JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
		JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
		JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
		JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
		JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
		JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID

END
GO
--End procedure dbo.GetContractByContractID

--Begin procedure dbo.GetContractsBySearchCriteria
EXEC Utility.DropObject 'dbo.GetContractsBySearchCriteria'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create Date: 2013.11.15
-- Description:	A stored procedure to get data from the dbo.Contract table based on search criteria
--
-- Author:			Todd Pires
-- Update date:	2014.10.08
-- Description:	Added the NULL screen on ContractingOfficeName and ContractName
--
-- Author:			Todd Pires
-- Update date:	2015.04.10
-- Description:	Added support for the ApproverPersonID, OriginatorPersonID and ReviewerPersonID fields
--
-- Author:			Todd Pires
-- Update date:	2015.08.18
-- Description:	Added support for the IsArchived field
-- ===================================================================================================
CREATE PROCEDURE dbo.GetContractsBySearchCriteria

@ApproverPersonID INT = 0,
@CompetitionTypeIDList VARCHAR(MAX) = NULL,
@ContractDateEndStart DATE = NULL,
@ContractDateEndStop DATE = NULL,
@ContractDateStartStart DATE = NULL,
@ContractDateStartStop DATE = NULL,
@ContractingOfficeName VARCHAR(250) = NULL,
@ContractingOrganizationIDList VARCHAR(MAX) = NULL,
@ContractName VARCHAR(250) = NULL,
@ContractTypeIDList VARCHAR(MAX) = NULL,
@EstimatedSolicitationQuarterList VARCHAR(MAX) = NULL,
@FiscalYearContractRangeIDList VARCHAR(MAX) = NULL,
@FundingOrganizationIDList VARCHAR(MAX) = NULL,
@IsArchived BIT = 0,
@IsForExport BIT = 0,
@Keyword VARCHAR(250) = NULL,
@NAICSID INT = 0,
@OrderByDirection VARCHAR(5) = 'ASC',
@OrderByField VARCHAR(50) = 'ContractName',
@OrganizationIDList VARCHAR(MAX) = NULL,
@OriginatorPersonID INT = 0,
@PageIndex INT = 1,
@PageSize INT = 50,
@PendingApprovalPersonID INT = 0,
@PersonID INT = 0,
@PlaceOfPerformanceStateIDList VARCHAR(MAX) = NULL,
@PointOfContactAgency VARCHAR(250) = NULL,
@PointOfContactEmail VARCHAR(320) = NULL,
@PointOfContactName VARCHAR(250) = NULL,
@PointOfContactTitle VARCHAR(50) = NULL,
@PSCID INT = 0,
@ReferenceIDV VARCHAR(250) = NULL,
@RequirementTypeIDList VARCHAR(MAX) = NULL,
@ReviewerPersonID INT = 0,
@TargetAwardDateStart DATE = NULL,
@TargetAwardDateStop DATE = NULL,
@TargetAwardFiscalYearList VARCHAR(MAX) = NULL,
@TotalContractRangeIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RowIndexStart INT = (@PageIndex * @PageSize) - @PageSize + 1
	DECLARE @RowIndexStop INT = @RowIndexStart + @PageSize - 1
	DECLARE @PageCount INT = 0
	DECLARE @TotalRecordCount INT
	
	DECLARE @tContract AS AccessControl.Contract
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#tGetContractsBySearchCriteria', 'u')) IS NOT NULL
		DROP TABLE #tGetContractsBySearchCriteria
	--ENDIF
		
	CREATE TABLE #tGetContractsBySearchCriteria
		(
		ContractID INT PRIMARY KEY NOT NULL,
		PlaceOfPerformanceCity VARCHAR(300)
		)
	
	IF @Keyword IS NULL
		BEGIN
	
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND C.IsArchived = @IsArchived
	
		--Date Fields
		IF @ContractDateEndStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd < @ContractDateEndStart
	
			END
		--ENDIF
		IF @ContractDateEndStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateEnd >= @ContractDateEndStop
	
			END
		--ENDIF
		IF @ContractDateStartStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart < @ContractDateStartStart
	
			END
		--ENDIF
		IF @ContractDateStartStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ContractDateStart >= @ContractDateStartStop
	
			END
		--ENDIF
		IF @TargetAwardDateStart IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate < @TargetAwardDateStart
	
			END
		--ENDIF
		IF @TargetAwardDateStop IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.TargetAwardDate > @TargetAwardDateStop
	
			END
		--ENDIF
	
		--Dropdown Fields
		IF @CompetitionTypeIDList IS NOT NULL AND LEN(RTRIM(@CompetitionTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@CompetitionTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.CompetitionTypeID
						)
	
			END
		--ENDIF
		IF @ContractingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@ContractingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractingOrganizationID
						)
	
			END
		--ENDIF
		IF @ContractTypeIDList IS NOT NULL AND LEN(RTRIM(@ContractTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@ContractTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.ContractTypeID
						)
	
			END
		--ENDIF
		IF @EstimatedSolicitationQuarterList IS NOT NULL AND LEN(RTRIM(@EstimatedSolicitationQuarterList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@EstimatedSolicitationQuarterList, ',', 0) LTT 
						WHERE LTT.ListItem = C.EstimatedSolicitationQuarter
						)
	
			END
		--ENDIF
		IF @FiscalYearContractRangeIDList IS NOT NULL AND LEN(RTRIM(@FiscalYearContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FiscalYearContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FiscalYearContractRangeID
						)
	
			END
		--ENDIF
		IF @FundingOrganizationIDList IS NOT NULL AND LEN(RTRIM(@FundingOrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@FundingOrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.FundingOrganizationID
						)
	
			END
		--ENDIF
		IF @NAICSID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.NAICSID <> @NAICSID
	
			END
		--ENDIF
		IF @OrganizationIDList IS NOT NULL AND LEN(RTRIM(@OrganizationIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@OrganizationIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.OriginatingOrganizationID
						)
	
			END
		--ENDIF
		IF @PlaceOfPerformanceStateIDList IS NOT NULL AND LEN(RTRIM(@PlaceOfPerformanceStateIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@PlaceOfPerformanceStateIDList, ',', 0) LTT 
							JOIN dbo.ContractPlaceOfPerformance CPOP ON CPOP.PlaceOfPerformanceStateID = LTT.ListItem
								AND CPOP.ContractID = C.ContractID
						)
	
			END
		--ENDIF
		IF @PSCID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.PSCID <> @PSCID
	
			END
		--ENDIF
		IF @RequirementTypeIDList IS NOT NULL AND LEN(RTRIM(@RequirementTypeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@RequirementTypeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.RequirementTypeID
						)
	
			END
		--ENDIF
		IF @TargetAwardFiscalYearList IS NOT NULL AND LEN(RTRIM(@TargetAwardFiscalYearList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TargetAwardFiscalYearList, ',', 0) LTT 
						WHERE LTT.ListItem = dbo.GetFiscalYearFromDate(C.TargetAwardDate)
						)
	
			END
		--ENDIF
		IF @TotalContractRangeIDList IS NOT NULL AND LEN(RTRIM(@TotalContractRangeIDList)) > 0 
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1 
						FROM Utility.ListToTable(@TotalContractRangeIDList, ',', 0) LTT 
						WHERE LTT.ListItem = C.TotalContractRangeID
						)
	
			END
		--ENDIF
	
		-- Text Fields (individual)
		IF @ContractingOfficeName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractingOfficeName NOT LIKE '%' + @ContractingOfficeName + '%'
							OR C.ContractingOfficeName IS NULL
						)					
	
			END
		--ENDIF
		IF @ContractName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND 
						(
						C.ContractName NOT LIKE '%' + @ContractName + '%'
							OR C.ContractName IS NULL
						)
					AND 
						(
						C.ContractDescription NOT LIKE '%' + @ContractName + '%'
							OR C.ContractDescription IS NULL
						)
	
			END
		--ENDIF
		IF @PointOfContactAgency IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactAgency LIKE '%' + @PointOfContactAgency + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactEmail IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactEmail LIKE '%' + @PointOfContactEmail + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactName IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactName LIKE '%' + @PointOfContactName + '%'
						)
	
			END
		--ENDIF
		IF @PointOfContactTitle IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractPointOfContact CPOC
						WHERE CPOC.ContractID = C.ContractID
							AND CPOC.PointOfContactTitle LIKE '%' + @PointOfContactTitle + '%'
						)
	
			END
		--ENDIF
		IF @ReferenceIDV IS NOT NULL
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND C.ReferenceIDV NOT LIKE '%' + @ReferenceIDV + '%'
	
			END
		--ENDIF
	
		-- Workflow field
		IF @PendingApprovalPersonID IS NOT NULL AND @PendingApprovalPersonID > 0
			BEGIN
	
			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContractWorkflowPerson CWP
						WHERE CWP.ContractID = C.ContractID
							AND CWP.WorkflowStepNumber = C.WorkflowStepNumber
							AND CWP.PersonID = @PendingApprovalPersonID
						)
	
			END
		--ENDIF

		-- Workflow Person fields
		IF @ApproverPersonID IS NOT NULL AND @ApproverPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 1
				FROM
					(
					SELECT
						MAX(EL2.EventLogID) AS EventLogID, 
						EL2.EntityID
					FROM dbo.EventLog EL2
						JOIN dbo.Contract C ON C.ContractID = EL2.EntityID
						JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
							AND WS.WorkflowStatusCode = 'Approved'
							AND EL2.EntityTypeCode = 'Contract'
					GROUP BY EL2.EntityID
					) D JOIN dbo.EventLog EL1 ON EL1.EventLogID = D.EventLogID
						AND EL1.EntityID = T1.ContractID
						AND EL1.PersonID = @ApproverPersonID
				)
				
			END
		--ENDIF
		IF @OriginatorPersonID IS NOT NULL AND @OriginatorPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 1
				FROM dbo.EventLog EL
				WHERE EL.EntityTypeCode = 'Contract'
					AND EL.EventCode = 'Create'
					AND EL.EntityID = T1.ContractID
					AND EL.PersonID = @OriginatorPersonID
				)
				
			END
		--ENDIF
		IF @ReviewerPersonID IS NOT NULL AND @ReviewerPersonID > 0
			BEGIN

			DELETE T1
			FROM #tGetContractsBySearchCriteria T1
			WHERE NOT EXISTS
				(
				SELECT 
					EL.PersonID,
					EL.EntityID,
					A.B.value('.', 'INT') AS WorkflowStepNumber
				FROM dbo.EventLog EL
					OUTER APPLY EL.EventData.nodes('Contract/WorkflowStepNumber') AS A(B)
				WHERE EL.EntityTypeCode = 'Contract'
					AND EL.EventCode IN ('IncrementWorkflow','Update')
					AND A.B.value('.', 'INT') > 1
					AND A.B.value('.', 'INT') <= (SELECT MAX(WorkflowStepNumber) FROM dbo.ContractWorkflowPerson CWP WHERE CWP.ContractID = EL.EntityID)
					AND EL.EntityID = T1.ContractID
					AND EL.PersonID = @ReviewerPersonID
				)
				
			END
		--ENDIF

		END
	ELSE
		BEGIN
	
		SET @Keyword = '%' + @Keyword + '%'

		-- Text Fields (multiple)
		INSERT INTO #tGetContractsBySearchCriteria
			(ContractID)
		SELECT
			C.ContractID
		FROM dbo.Contract C
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
				AND
					(
					@PersonID > 0
						OR WS.WorkflowStatusCode = 'Approved'
					)
				AND C.IsArchived = @IsArchived
				AND
					(
					C.ContractingOfficeName LIKE @Keyword
						OR C.ContractName LIKE @Keyword
						OR C.ContractDescription LIKE @Keyword
						OR C.IncumbentContractorName LIKE @Keyword
						OR C.ReferenceIDV LIKE @Keyword
						OR C.TransactionNumber LIKE @Keyword
						OR EXISTS
							(
							SELECT 1
							FROM dbo.ContractPointOfContact CPOC
							WHERE CPOC.ContractID = C.ContractID
								AND 
									(
									CPOC.PointOfContactEmail LIKE @Keyword
										OR CPOC.PointOfContactName LIKE @Keyword
										OR CPOC.PointOfContactPhone LIKE @Keyword
									)
							)
						OR EXISTS
							(
							SELECT 1 
							FROM dbo.ContractPlaceOfPerformance CPOP 
								JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
									AND CPOP.ContractID = C.ContractID
									AND 
										(
										CPOP.PlaceOfPerformanceCity LIKE @Keyword
											OR S.StateCode LIKE @Keyword
											OR S.StateName LIKE @Keyword
										)
							)
					)
	
		END
	--ENDIF
	
	SELECT 
		@TotalRecordCount = COUNT(T1.ContractID)			
	FROM #tGetContractsBySearchCriteria T1
	
	IF @OrderByField = 'PlaceOfPerformanceCity'
		BEGIN
	
		UPDATE T1
		SET 
			T1.PlaceOfPerformanceCity = 
				CASE
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
					THEN 'Multiple'
					WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
					THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
					ELSE ''
				END
	
		FROM #tGetContractsBySearchCriteria T1
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
	
		END
	--ENDIF
	
	/*
	To return ALL records, pass @PageSize = 0 as an argument.  NOTE:  Not passing
	a @PageSize argument at all will default @PageSize to 50, so you must
	EXPLICITLY pass @PageSize = 0 to bypass the pagination call
	*/
	
	IF @PageSize > 0
		BEGIN
	
		SET @PageCount = CEILING(CAST(@TotalRecordCount AS NUMERIC(10,2)) / @PageSize)
	
		;
		WITH PD AS
			(
			SELECT
				ROW_NUMBER() OVER 
					(
					ORDER BY 
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
						CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
						CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC, T1.ContractID ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
						CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
						CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
						CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
						CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
							T1.ContractID ASC
					) AS RowIndex,
				C.ContractID
			FROM #tGetContractsBySearchCriteria T1
				JOIN dbo.Contract C ON C.ContractID = T1.ContractID
				JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
				JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
				JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID				
			)
	
		DELETE T1
		FROM #tGetContractsBySearchCriteria T1
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM PD 
			WHERE PD.ContractID = T1.ContractID
				AND PD.RowIndex >= @RowIndexStart 
				AND PD.RowIndex <= @RowIndexStop
			)
	
		END
	--ENDIF
	
	INSERT INTO @tContract (ContractID) SELECT T1.ContractID FROM #tGetContractsBySearchCriteria T1
	
	IF @IsForExport = 0
		BEGIN
		
		SELECT
			@PageCount AS PageCount,
			@PageIndex AS PageIndex,
			@TotalRecordCount AS TotalRecordCount,
			UR.CanHaveDelete,
			UR.CanHaveEdit,
			C.CompetitionTypeID, 
			LEFT(C.ContractDescription, 50) + 
				CASE
					WHEN LEN(RTRIM(C.ContractDescription)) > 50
					THEN '...' 
					ELSE ''
				END AS ContractDescription, 
					
			C.ContractID, 
			C.ContractingOfficeName, 
			C.ContractName, 
			C.ContractTypeID, 
			C.EstimatedSolicitationQuarter, 
			C.FiscalYearContractRangeID, 
			C.IncumbentContractorName, 
			C.NAICSID, 
			C.RequirementTypeID, 
			C.TargetAwardDate, 
			dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
			C.TotalContractRangeID,
			C.ContractDateEnd, 
			dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
			CR1.ContractRangeName AS FiscalYearContractRangeName,
			CR2.ContractRangeName AS TotalContractRangeName,
			CT1.CompetitionTypeName,
			CT2.ContractTypeName,
			N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
			RT.RequirementTypeName,
		
			CASE 
				WHEN @OrderByField = 'PlaceOfPerformanceCity'
				THEN T1.PlaceOfPerformanceCity
				WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) > 1
				THEN 'Multiple'
				WHEN (SELECT COUNT(CPOP.ContractID) AS ItemCount FROM dbo.ContractPlaceOfPerformance CPOP WHERE CPOP.ContractID = C.ContractID) = 1
				THEN (SELECT CPOP.PlaceOfPerformanceCity + ', ' + S.StateCode FROM dbo.ContractPlaceOfPerformance CPOP JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID WHERE CPOP.ContractID = C.ContractID)
				ELSE ''
			END AS PlaceOfPerformanceCity
		
		FROM #tGetContractsBySearchCriteria T1
			JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
			JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
			JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
			JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
			JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
			JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
			JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
		ORDER BY 
			CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'ASC' THEN CT1.CompetitionTypeName END ASC,
			CASE WHEN @OrderByField = 'CompetitionTypeName' AND @OrderByDirection = 'DESC' THEN CT1.CompetitionTypeName END DESC,
			CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'ASC' THEN C.ContractDateEnd END ASC,
			CASE WHEN @OrderByField = 'ContractDateEndFormatted' AND @OrderByDirection = 'DESC' THEN C.ContractDateEnd END DESC, 
			CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'ASC' THEN C.ContractName END ASC,
			CASE WHEN @OrderByField = 'ContractName' AND @OrderByDirection = 'DESC' THEN C.ContractName END DESC,
			CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'ASC' THEN N.NAICSCode + ' - ' + N.NAICSName END ASC,
			CASE WHEN @OrderByField = 'NAICSFormatted' AND @OrderByDirection = 'DESC' THEN N.NAICSCode + ' - ' + N.NAICSName END DESC,
			CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'ASC' THEN T1.PlaceOfPerformanceCity END ASC,
			CASE WHEN @OrderByField = 'PlaceOfPerformanceCity' AND @OrderByDirection = 'DESC' THEN T1.PlaceOfPerformanceCity END DESC,
			CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'ASC' THEN CR2.ContractRangeName END ASC,
			CASE WHEN @OrderByField = 'TotalContractRangeName' AND @OrderByDirection = 'DESC' THEN CR2.ContractRangeName END DESC,
			CASE WHEN @OrderByField <> 'ContractName' THEN C.ContractName END ASC, 
				T1.ContractID ASC

		END
	ELSE
		BEGIN

		SELECT
			dbo.FormatDateTime(C.ContractDateEnd, 'mm/dd/yyyy') AS ContractDateEndFormatted,
			dbo.FormatDateTime(C.ContractDateStart, 'mm/dd/yyyy') AS ContractDateStartFormatted,
			C.ContractDescription, 
			C.ContractID, 
			C.ContractingOfficeName, 
			C.ContractName, 
			C.EstimatedSolicitationQuarter, 
			C.IncumbentContractorName, 
			C.ReferenceIDV, 
			dbo.FormatDateTime(C.TargetAwardDate, 'mm/dd/yyyy') AS TargetAwardDateFormatted,
			C.TransactionNumber, 
			CR1.ContractRangeName AS FiscalYearContractRangeName,
			CR2.ContractRangeName AS TotalContractRangeName,
			CT1.CompetitionTypeName,
			CT2.ContractTypeName,
			dbo.FormatDateTime((SELECT MAX(EL.CreateDateTime) FROM dbo.EventLog EL WHERE EL.EntityTypeCode = 'Contract' AND EL.EntityID = C.ContractID), 'mm/dd/yyyy hh:mm tt') AS UpdateDateTimeFormatted,
			N.NAICSCode + ' - ' + N.NAICSName AS NAICSFormatted,
			O1.OrganizationName AS ContractingOrganizationName,
			O2.OrganizationName AS FundingOrganizationName,
			O3.OrganizationName AS OriginatingOrganizationName,
			P.PSCCode + ' - ' + P.PSCName AS PSCFormatted,
			RT.RequirementTypeName,
			WS.WorkflowStatusName,
			POP.PlaceOfPerformanceCity,
			POP.StateCode AS PlaceOfPerformanceState,
			POC.PointOfContactAgency, 
			POC.PointOfContactEmail, 
			POC.PointOfContactName, 
			POC.PointOfContactPhone, 
			POC.PointOfContactTitle
		FROM #tGetContractsBySearchCriteria T1
			JOIN AccessControl.GetContractRecordsByPersonID(@PersonID, @tContract) UR ON UR.ContractID = T1.ContractID
			JOIN dbo.Contract C ON C.ContractID = T1.ContractID
			JOIN Dropdown.CompetitionType CT1 ON CT1.CompetitionTypeID = C.CompetitionTypeID
			JOIN Dropdown.ContractRange CR1 ON CR1.ContractRangeID = C.FiscalYearContractRangeID
			JOIN Dropdown.ContractRange CR2 ON CR2.ContractRangeID = C.TotalContractRangeID
			JOIN Dropdown.ContractType CT2 ON CT2.ContractTypeID = C.ContractTypeID
			JOIN Dropdown.NAICS N ON N.NAICSID = C.NAICSID
			JOIN Dropdown.Organization O1 ON O1.OrganizationID = C.ContractingOrganizationID
			JOIN Dropdown.Organization O2 ON O2.OrganizationID = C.FundingOrganizationID
			JOIN Dropdown.Organization O3 ON O3.OrganizationID = C.OriginatingOrganizationID
			JOIN Dropdown.PSC P ON P.PSCID = C.PSCID
			JOIN Dropdown.RequirementType RT ON RT.RequirementTypeID = C.RequirementTypeID
			JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			OUTER APPLY
				(
				SELECT TOP 1
					CPOC.PointOfContactAgency, 
					CPOC.PointOfContactEmail, 
					CPOC.PointOfContactName, 
					CPOC.PointOfContactPhone, 
					CPOC.PointOfContactTitle
				FROM dbo.ContractPointOfContact CPOC
				WHERE CPOC.ContractID = C.ContractID
				ORDER BY CPOC.IsPrimary DESC, CPOC.ContractPointOfContactID
				) POC
			OUTER APPLY
				(
				SELECT TOP 1
					CPOP.PlaceOfPerformanceCity,
					S.StateCode
				FROM dbo.ContractPlaceOfPerformance CPOP
					JOIN Dropdown.State S ON S.StateID = CPOP.PlaceOfPerformanceStateID
						AND CPOP.ContractID = C.ContractID
				ORDER BY CPOP.IsPrimary DESC, CPOP.ContractPlaceOfPerformanceID
				) POP
		ORDER BY C.ContractID

		END
	--ENDIF
		
	DROP TABLE #tGetContractsBySearchCriteria
	
END
GO
--End procedure dbo.GetContractsBySearchCriteria
--End Procedures
