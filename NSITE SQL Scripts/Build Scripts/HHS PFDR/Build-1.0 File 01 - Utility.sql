USE PFDR
GO

--Begin Schemas
--Begin schema AccessControl
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'AccessControl')
	BEGIN
	
	DECLARE @SQL VARCHAR(MAX) = 'CREATE SCHEMA AccessControl'
	
	EXEC (@SQL)
	
	END
--ENDIF
GO
--End schema AccessControl

--Begin schema Dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'Dropdown')
	BEGIN
	
	DECLARE @SQL VARCHAR(MAX) = 'CREATE SCHEMA Dropdown'
	
	EXEC (@SQL)
	
	END
--ENDIF
GO
--End schema Dropdown

--Begin schema Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @SQL VARCHAR(MAX) = 'CREATE SCHEMA Utility'
	
	EXEC (@SQL)
	
	END
--ENDIF
GO
--End schema Utility
--End Schemas

--Begin dependencies
--Begin procedure Utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE Utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date: 2012.09.24
-- Description:	A stored procedure to drop a function, stored procedcure or table from the database
--
-- Author:			Todd Pires
-- Modify date: 2013.05.20
-- Description:	Added support for Synonyms, optimized the code
--
-- Author:			Todd Pires
-- Modify date: 2013.08.02
-- Description:	Added support for Schemas & Primary Keys
--
-- Author:			Todd Pires
-- Modify date: 2013.10.21
-- Description:	Added support for Triggers
--
-- Author:			Todd Pires
-- Modify date: 2014.05.07
-- Description:	Added support for Types
-- ================================================================================================
CREATE PROCEDURE Utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @SQL VARCHAR(MAX)
DECLARE @Type VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @Type = O.type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @Type IS NOT NULL
	BEGIN
	
	IF @Type IN ('D', 'PK')
		BEGIN
		
		SELECT
			@SQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@SQL)
		
		END
	ELSE IF @Type IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @SQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@SQL)
		
		END
	ELSE IF @Type IN ('P','PC')
		BEGIN
		
		SET @SQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@SQL)
		
		END
	ELSE IF @Type = 'SN'
		BEGIN
		
		SET @SQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@SQL)
		
		END
	ELSE IF @Type = 'TR'
		BEGIN
		
		SET @SQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@SQL)
		
		END
	ELSE IF @Type = 'U'
		BEGIN
		
		SET @SQL = 'DROP TABLE ' + @ObjectName
		EXEC (@SQL)
		
		END
	ELSE IF @Type = 'V'
		BEGIN
		
		SET @SQL = 'DROP VIEW ' + @ObjectName
		EXEC (@SQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @SQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@SQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @SQL = 'DROP TYPE ' + @ObjectName
	EXEC (@SQL)

	END
--ENDIF
		
END	
GO
--End procedure Utility.DropObject

--Begin function Utility.ListToTable
EXEC Utility.DropObject 'Utility.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
-- ========================================================================

CREATE FUNCTION Utility.ListToTable
(
@List VARCHAR(MAX), 
@Delimiter VARCHAR(5),
@MakeDistinct BIT
)

RETURNS 
@oTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ListItem VARCHAR(MAX))

AS
BEGIN

DECLARE @ListElement VARCHAR(MAX)
DECLARE @DelimiterLength INT
DECLARE @Length INT
DECLARE @Position INT

SET @Delimiter = ISNULL(@Delimiter, ',')
SET @List = @List + @Delimiter
SET @DelimiterLength = LEN(@Delimiter)
SET @Position = CHARINDEX(@Delimiter, @List)

WHILE @Position > 0
	BEGIN
	
	SET @ListElement = LEFT(@List, @Position - 1)

	IF LEN(LTRIM(@ListElement)) > 0
		BEGIN

		IF @MakeDistinct = 0 OR NOT EXISTS (SELECT 1 FROM @oTable WHERE ListItem = CAST(@ListElement AS VARCHAR(MAX)))
			INSERT INTO @oTable (ListItem) VALUES (CAST(@ListElement AS VARCHAR(MAX)))
		--ENDIF
		
		END
	--ENDIF
	
	SET @Length = LEN(@ListElement) + @DelimiterLength
	SET @List = RIGHT(@List, LEN(@List) - @Length)
	SET @Position = CHARINDEX(@Delimiter, @List)

	END
--END WHILE

RETURN 
END
GO
--End function Utility.ListToTable
--End dependencies

--Begin Utility Functions
--Begin function Utility.CreateParameterClassDefinition
EXEC Utility.DropObject 'Utility.CreateParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION Utility.CreateParameterClassDefinition
(
@ParameterName VARCHAR(50),
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @Return VARCHAR(MAX) = 'public '

	IF @DataType <> 'varchar'
		SET @Return += 'Nullable'
	--ENDIF
	
	IF @DataType = 'bit'
		SET @Return += '<bool>'
	ELSE IF @DataType IN ('date','datetime')
		SET @Return += '<System.DateTime>'
	ELSE IF @DataType = 'varchar'
		SET @Return += 'string'
	ELSE
		SET @Return += '<' + LOWER(@DataType) + '>'
	--ENDIF
	
	SET @Return += ' ' + @ParameterName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@Return))
END
GO
--End function Utility.CreateParameterClassDefinition

--Begin function Utility.CreateTableColumnClassDefinition
EXEC Utility.DropObject 'Utility.CreateTableColumnClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2014.04.25
-- Description:	A function to return a string suitable for a class definition
-- ==========================================================================

CREATE FUNCTION Utility.CreateTableColumnClassDefinition
(
@ColumnName VARCHAR(50),
@IsNullable BIT,
@DataType VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @Return VARCHAR(MAX) = 'public '

	IF @DataType = 'bit'
		SET @Return += 'bool'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 0
		SET @Return += 'System.DateTime'
	ELSE IF @DataType IN ('date','datetime') AND @IsNullable = 1
		SET @Return += 'Nullable<System.DateTime>'
	ELSE IF @DataType = 'varchar'
		SET @Return += 'string'
	ELSE
		SET @Return += LOWER(@DataType)
	--ENDIF
	
	SET @Return += ' ' + @ColumnName + ' { get; set; }'
	
	RETURN RTRIM(LTRIM(@Return))
END
GO
--End function Utility.CreateTableColumnClassDefinition

--Begin function Utility.GetStoredProcedureParameters
EXEC Utility.DropObject 'Utility.GetStoredProcedureParameters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================
-- Author:			Todd Pires
-- Create date: 2014.05.01
-- Description:	A helper stored procedure
-- ======================================
CREATE FUNCTION Utility.GetStoredProcedureParameters
(
@StoredProcedureID INT,
@StoredProcedureName VARCHAR(250)
)

RETURNS @tReturn table 
	(
	ParameterName VARCHAR(50),
	DataType VARCHAR(50)
	) 

AS
BEGIN

	IF @StoredProcedureID = 0
		BEGIN

		IF CHARINDEX('.', @StoredProcedureName) = 0
			SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
		--ENDIF
		
		SELECT @StoredProcedureID = O.Object_ID
		FROM sys.Objects O
			JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
				AND S.Name + '.' + O.Name = @StoredProcedureName
				
		END
	--ENDIF

	INSERT INTO @tReturn
		(ParameterName,DataType)
	SELECT 
		REPLACE(P.Parameter_Name, '@', ''),
		P.Data_Type
	FROM INFORMATION_SCHEMA.PARAMETERS P
		JOIN sys.Objects O ON O.Name = P.Specific_Name
			AND O.Object_ID = @StoredProcedureID
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
			AND S.Name = P.Specific_Schema
	ORDER BY P.Parameter_Name
					
	RETURN

END
GO
--End function Utility.GetStoredProcedureParameters

--Begin function Utility.ListGetAt
EXEC Utility.DropObject 'Utility.ListGetAt'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.06.15
-- Description:	A function to return an item at a specific index from a delimted list of values
-- ============================================================================================

CREATE FUNCTION Utility.ListGetAt
(
@List VARCHAR(MAX),
@Position INT, 
@Delimiter VARCHAR(5)
)

RETURNS VARCHAR(max)

AS
BEGIN

DECLARE @Return VARCHAR(max)

SET @Return = 
	(
	SELECT ListItem
	FROM Utility.ListToTable(@List, @Delimiter, 0)
	WHERE ListItemID = @Position
	)

RETURN ISNULL(@Return, '')
END
GO
--End function Utility.ListGetAt
--End Utility Functions

--Begin Utility Procedures
--Begin procedure Utility.AddColumn
EXEC Utility.DropObject 'Utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.AddColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250),
	@DataType VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @SQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@SQL)
		
		END
	--ENDIF

END
GO
--End procedure Utility.AddColumn

--Begin procedure Utility.CreateStoredProcedureObjectArgumentList
EXEC Utility.DropObject 'Utility.CreateStoredProcedureObjectArgumentList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE Utility.CreateStoredProcedureObjectArgumentList
	@StoredProcedureName VARCHAR(250),
	@StoredProcedureObjectName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ParameterName VARCHAR(50)
	DECLARE @Return VARCHAR(MAX) = ''

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SPP.ParameterName
		FROM Utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP
	
	OPEN oCursor
	FETCH oCursor INTO @ParameterName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @Return <> ''
			SET @Return += ', '
		--ENDIF
		
		SET @Return += @ParameterName + ' = ' + @StoredProcedureObjectName + '.' + @ParameterName
		
		FETCH oCursor INTO @ParameterName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT @Return

END
GO
--End procedure Utility.CreateStoredProcedureObjectArgumentList

--Begin procedure Utility.CreateStoredProcedureParameterClassDefinition
EXEC Utility.DropObject 'Utility.CreateStoredProcedureParameterClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE Utility.CreateStoredProcedureParameterClassDefinition
	@StoredProcedureName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @StoredProcedureName) = 0
		SET @StoredProcedureName = 'dbo.' + @StoredProcedureName
	--ENDIF
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('public class ' + REPLACE(PARSENAME(@StoredProcedureName, 1), 'Get', '')),
		('{')
		
	INSERT INTO @tTable
		(TextString) 
	SELECT 
		Utility.CreateParameterClassDefinition(SPP.ParameterName, SPP.DataType)
	FROM Utility.GetStoredProcedureParameters(0, @StoredProcedureName) SPP

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure Utility.CreateStoredProcedureParameterClassDefinition

--Begin procedure Utility.CreateTableClassDefinition
EXEC Utility.DropObject 'Utility.CreateTableClassDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.04.25
-- Description:	A helper stored procedure for class creation
-- =========================================================
CREATE PROCEDURE Utility.CreateTableClassDefinition
	@TableName VARCHAR(250),
	@IncludeAnnotations BIT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColumnName VARCHAR(50)
	DECLARE @DataType VARCHAR(50)
	DECLARE @IsNullable BIT
	
	DECLARE @tTable TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable 
		(TextString) 
	SELECT
		'public class ' + T.Name
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
			AND S.Name + '.' + T.Name = @TableName

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('{')

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			C1.Name AS ColumnName, 
			C1.Is_Nullable AS IsNullable, 
			T1.Name AS DataType
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.schema_ID = O1.schema_ID
			JOIN sys.Columns C1 ON O1.Object_ID = C1.Object_ID
			JOIN sys.Types T1 ON C1.User_Type_ID = T1.User_Type_ID
				AND S1.Name + '.' + O1.Name = @TableName
				AND O1.Type = 'U'
		ORDER BY C1.Name

	
	OPEN oCursor
	FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @IncludeAnnotations = 1
			BEGIN

			IF @DataType IN ('date','datetime')
				BEGIN

				INSERT INTO @tTable 
					(TextString)
				VALUES
					('[DataType(DataType.Date)]'),
        	('[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]')
				
				END
			--ENDIF

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('[Display(Name = "' + @ColumnName + '")]')

			END
		--ENDIF
		
		INSERT INTO @tTable 
			(TextString) 
		SELECT 
			Utility.CreateTableColumnClassDefinition(@ColumnName, @IsNullable, @DataType)

		IF @IncludeAnnotations = 1
			BEGIN

			INSERT INTO @tTable 
				(TextString)
			VALUES
				('')

			END
		--ENDIF
		
		FETCH oCursor INTO @ColumnName, @IsNullable, @DataType
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	INSERT INTO @tTable 
		(TextString) 
	VALUES 
		('}')
		
	SELECT TextString
	FROM @tTable	

END
GO
--End procedure Utility.CreateTableClassDefinition

--Begin procedure Utility.CreateTableInsertUpdateDefinition
EXEC Utility.DropObject 'Utility.CreateTableInsertUpdateDefinition'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2014.05.05
-- Description:	A helper stored procedure for table creation
-- =========================================================
CREATE PROCEDURE Utility.CreateTableInsertUpdateDefinition
	@TableName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tTable1 TABLE
		(
		ColumnName VARCHAR(50), 
		IsIdentity BIT
		)

	DECLARE @tTable2 TABLE
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
		TextString VARCHAR(MAX)
		)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	INSERT INTO @tTable1
		(ColumnName,IsIdentity)
	SELECT
		C.Name,
		C.Is_Identity
	FROM sys.objects O
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.Columns C ON O.Object_ID = C.Object_ID
		JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
			AND S.Name + '.' + O.Name = @TableName
			AND O.Type = 'U'
	ORDER BY C.Name

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('INSERT INTO ' + @TableName),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'OUTPUT INSERTED.' + T1.ColumnName + ' INTO @tOutput'
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		('VALUES'),
		('(')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'@' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)
			
	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(')')

	INSERT INTO @tTable2
		(TextString) 
	VALUES
		(''),
		('UPDATE ' + @TableName),
		('SET')

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		T1.ColumnName + ' = @' + T1.ColumnName + ','
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 0

	UPDATE @tTable2
	SET TextString = REPLACE(TextString, ',', '')
	WHERE RowIndex = (SELECT MAX(RowIndex) FROM @tTable2)

	INSERT INTO @tTable2
		(TextString) 
	SELECT
		'WHERE ' + T1.ColumnName + ' = @' + T1.ColumnName 
	FROM @tTable1 T1
	WHERE T1.IsIdentity = 1
		
	SELECT TextString
	FROM @tTable2

END
GO
--End procedure Utility.CreateTableInsertUpdateDefinition

--Begin procedure Utility.DropColumn
EXEC Utility.DropObject 'Utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2013.08.02
-- Description:	A helper stored procedure for table upgrades
--
-- Author:		Todd Pires
-- Create date: 2014.04.15
-- Description:	Add a check for statistics
-- =========================================================
CREATE PROCEDURE Utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@SQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@SQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@SQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@SQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.[' + S1.Name + ']'
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @SQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@SQL)
		
		FETCH oCursor INTO @SQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @SQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @ColumnName
		EXEC (@SQL)

		END
	--ENDIF

END
GO
--End procedure Utility.DropColumn

--Begin procedure Utility.DropConstraintsAndIndexes
EXEC Utility.DropObject 'Utility.DropConstraintsAndIndexes'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropConstraintsAndIndexes
	@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	DECLARE @SQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @SQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@SQL)
		
		FETCH oCursor INTO @SQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @SQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@SQL)
		
		FETCH oCursor INTO @SQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @SQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@SQL)
		
		FETCH oCursor INTO @SQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @SQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@SQL)
		
		FETCH oCursor INTO @SQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure Utility.DropConstraintsAndIndexes

--Begin procedure Utility.DropIndex
EXEC Utility.DropObject 'Utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2013.04.19
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropIndex
	@TableName VARCHAR(250),
	@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @SQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@SQL)
		
		END
	--ENDIF

END
GO
--End procedure Utility.DropIndex

--Begin procedure Utility.SetDefaultConstraint
EXEC Utility.DropObject 'Utility.SetDefaultConstraint'
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date: 2013.10.12
-- Description:	A helper stored procedure for table upgrades
-- ===============================================================================================================================
CREATE PROCEDURE Utility.SetDefaultConstraint
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250),
	@DataType VARCHAR(250),
	@Default VARCHAR(MAX),
	@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ConstraintName VARCHAR(500)
	DECLARE @SQL VARCHAR(MAX)
	DECLARE @DefaultIsFunction BIT
	DECLARE @DefaultIsNumeric BIT
	DECLARE @Length INT

	SET @DefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @DefaultIsFunction = 1
	--ENDIF
	
	SET @DefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @DefaultIsFunction = 0 AND @DefaultIsNumeric = 0
		SET @SQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ''' + @Default + ''' WHERE ' + @ColumnName + ' IS NULL'
	ELSE
		SET @SQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ' + @Default + ' WHERE ' + @ColumnName + ' IS NULL'
	--ENDIF

	EXEC (@SQL)

	SET @SQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN ' + @ColumnName + ' ' + @DataType + ' NOT NULL'
	EXEC (@SQL)

	SET @Length = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @ConstraintName = 'DF_' + RIGHT(@TableName, @Length) + '_' + @ColumnName
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @SQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @SQL IS NOT NULL
			EXECUTE (@SQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @DefaultIsFunction = 0 AND @DefaultIsNumeric = 0
			SET @SQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @ConstraintName + ' DEFAULT ''' + @Default + ''' FOR ' + @ColumnName
		ELSE
			SET @SQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @ConstraintName + ' DEFAULT ' + @Default + ' FOR ' + @ColumnName
	--ENDIF
	
		EXECUTE (@SQL)

		END
	--ENDIF

	END
GO
--End procedure Utility.SetDefaultConstraint

--Begin procedure Utility.SetIndexClustered
EXEC Utility.DropObject 'Utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @SQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@SQL)

END
GO
--End procedure Utility.SetIndexClustered

--Begin procedure Utility.SetIndexNonClustered
EXEC Utility.DropObject 'Utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:		Todd Pires
-- Modify date: 2013.10.10
-- Description:	Added INCLUDE support
-- =========================================================
CREATE PROCEDURE Utility.SetIndexNonClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX),
	@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @SQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @SQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @SQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@SQL)

END
GO
--End procedure Utility.SetIndexNonClustered

--Begin procedure Utility.SetPrimaryKeyClustered
EXEC Utility.DropObject 'Utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyClustered
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)
	DECLARE @Length INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @Length = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @SQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @Length) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@SQL)

END
GO
--End procedure Utility.SetPrimaryKeyClustered

--Begin procedure Utility.SetPrimaryKeyNonClustered
EXEC Utility.DropObject 'Utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyNonClustered
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL VARCHAR(MAX)
	DECLARE @Length INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @Length = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @SQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @Length) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@SQL)

END
GO
--End procedure Utility.SetPrimaryKeyNonClustered
--End Utility Procedures

--Begin Types
--Begin type AccessControl.Contract
IF NOT EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'AccessControl.Contract')
	BEGIN

	CREATE TYPE AccessControl.Contract AS TABLE 
	(
	ContractID INT NOT NULL PRIMARY KEY
	)

	END
--ENDIF
GO
--End type AccessControl.Contract

--Begin type AccessControl.Organization
IF NOT EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'AccessControl.Organization')
	BEGIN

	CREATE TYPE AccessControl.Organization AS TABLE 
	(
	OrganizationID INT NOT NULL PRIMARY KEY
	)

	END
--ENDIF
GO
--End type AccessControl.Organization

--Begin type AccessControl.Person
IF NOT EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'AccessControl.Person')
	BEGIN

	CREATE TYPE AccessControl.Person AS TABLE 
	(
	PersonID INT NOT NULL PRIMARY KEY
	)

	END
--ENDIF
GO
--End type AccessControl.Person

--Begin type AccessControl.Workflow
IF NOT EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'AccessControl.Workflow')
	BEGIN

	CREATE TYPE AccessControl.Workflow AS TABLE 
	(
	WorkflowID INT NOT NULL PRIMARY KEY
	)

	END
--ENDIF
GO
--End type AccessControl.Workflow
--End Types