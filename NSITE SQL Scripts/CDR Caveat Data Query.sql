DECLARE @cCDRColumnName varchar(50)
DECLARE @cCDRFileColumnName varchar(50)
DECLARE @cCRLF varchar(2)
DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)
DECLARE @cTab1 varchar(1)
DECLARE @cTab2 varchar(2)
DECLARE @nHasColumn bit
DECLARE @nIsPrint bit

--SET @cCDRColumnName = 'Classification'
--SET @cCDRFileColumnName = 'Class'
SET @cCDRColumnName = 'Dissemination'
SET @cCDRFileColumnName = 'Caveat'
--SET @cCDRColumnName = 'ReleasableTo'
--SET @cCDRFileColumnName = 'RelTo'
SET @cCRLF = CHAR(13) + CHAR(10)
SET @cSQL = ''
SET @cTab1 = CHAR(9)
SET @cTab2 = REPLICATE(CHAR(9), 2)
SET @nIsPrint = 0
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN 
		(
		'ACGU',
		'ARMY',
		'CCO',
		'CFMCCLL',
		'DISA',
		'DLA',
		'DOS',
		'DTRA',
		'HPRC',
		'JSCC',
		'NAVY',
		'NCCS',
		'NGA',
		'NGB',
		'NOMI',
		'ORCHID',
		'RPB',
		'SOCOM',
		'SOCOMNEW',
		'USAF',
		'USSOCOM',
		'USUHS'
		)
	ORDER BY DB.Name

IF @nIsPrint = 1
	BEGIN

	SET @cSQL = 'IF (SELECT OBJECT_ID(''tempdb.dbo.#oTable1'', ''u'')) IS NOT NULL'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DROP TABLE #oTable1'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'IF (SELECT OBJECT_ID(''tempdb.dbo.#oTable2'', ''u'')) IS NOT NULL'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DROP TABLE #oTable2'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'CREATE TABLE #oTable1'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DatabaseName varchar(50),'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TableName varchar(50),'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'ColumnName varchar(50),'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'ItemCount int,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'Item varchar(250)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'CREATE TABLE #oTable2'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DatabaseName varchar(50),'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'HasColumn bit'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'
	
	print '--Begin ' + @cDatabaseName + 'Temp table creation'
	print @cSQL
	print '--End ' + @cDatabaseName + 'Temp table creation'
	
	END
ELSE
	BEGIN
	
	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable1', 'u')) IS NOT NULL
		DROP TABLE #oTable1

	IF (SELECT OBJECT_ID('tempdb.dbo.#oTable2', 'u')) IS NOT NULL
		DROP TABLE #oTable2

	CREATE TABLE #oTable1 
		(
		DatabaseName varchar(50),
		TableName varchar(50),
		ColumnName varchar(50),
		ItemCount int,
		Item varchar(250)
		)

	CREATE TABLE #oTable2
		(
		DatabaseName varchar(50),
		HasColumn bit
		)

	END
--ENDIF

OPEN oCursor
FETCH oCursor INTO @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'INSERT INTO #oTable2'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(DatabaseName,HasColumn)'
	SET @cSQL = @cSQL + @cCRLF + 'VALUES'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cDatabaseName + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'ISNULL((SELECT 1 FROM ' + @cDatabaseName + '.sys.columns T WITH (NOLOCK) WHERE T.Object_ID = OBJECT_ID(''dbo.CDRFile'') AND T.Name = ''' + @cCDRFileColumnName + '''), 0)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'

	IF @nIsPrint = 1
		BEGIN
		
		print '--Begin ' + @cDatabaseName + '.dbo.CDRFile column check'
		print @cSQL
		print '--End ' + @cDatabaseName + '.dbo.CDRFile column check'
		print ''
		
		END
	ELSE
		EXEC (@cSQL)
	--ENDIF

	SET @cSQL = 'INSERT INTO #oTable1'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(DatabaseName,TableName,ColumnName,ItemCount,Item)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cDatabaseName + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''CDR'','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cCDRColumnName + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'COUNT(T.' + @cCDRColumnName + '),'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'T.' + @cCDRColumnName
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + '' + @cDatabaseName + '.dbo.CDR T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + 'WHERE T.' + @cCDRColumnName + ' IS NOT NULL'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'AND LEN(LTRIM(T.' + @cCDRColumnName + ')) > 0'
	SET @cSQL = @cSQL + @cCRLF + 'GROUP BY T.' + @cCDRColumnName

	IF @nIsPrint = 1
		BEGIN
		
		print '--Begin ' + @cDatabaseName + '.dbo.CDR'
		print @cSQL
		print '--End ' + @cDatabaseName + '.dbo.CDR'
		print ''
		
		END
	ELSE
		EXEC (@cSQL)
	--ENDIF

	SET @nHasColumn = (SELECT HasColumn FROM #oTable2 WHERE DatabaseName = @cDatabaseName)
	
	IF @nHasColumn = 1
		BEGIN

		SET @cSQL = 'INSERT INTO #oTable1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(DatabaseName,TableName,ColumnName,ItemCount,Item)'
		SET @cSQL = @cSQL + @cCRLF + 'SELECT'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cDatabaseName + ''','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''CDRFile'','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cCDRFileColumnName + ''','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'COUNT(T.' + @cCDRFileColumnName + '),'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'T.' + @cCDRFileColumnName
		SET @cSQL = @cSQL + @cCRLF + 'FROM ' + '' + @cDatabaseName + '.dbo.CDRFile T WITH (NOLOCK)'
		SET @cSQL = @cSQL + @cCRLF + 'WHERE T.' + @cCDRFileColumnName + ' IS NOT NULL'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'AND LEN(LTRIM(T.' + @cCDRFileColumnName + ')) > 0'
		SET @cSQL = @cSQL + @cCRLF + 'GROUP BY T.' + @cCDRFileColumnName

		END
	ELSE
		BEGIN

		SET @cSQL = 'INSERT INTO #oTable1'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(DatabaseName,TableName,ColumnName,ItemCount,Item)'
		SET @cSQL = @cSQL + @cCRLF + 'VALUES'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cDatabaseName + ''','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''CDRFile'','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''' + @cCDRFileColumnName + ''','
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '0,'
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + '''*** NO ' + UPPER(@cCDRFileColumnName) + ' COLUMN EXISTS IN THE CDRFile TABLE IN DATABASE ' + @cDatabaseName + ' ***'''
		SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'

		END
	--ENDIF
	
	IF @nIsPrint = 1
		BEGIN
		
		print '--Begin ' + @cDatabaseName + '.dbo.CDRFile'
		print @cSQL
		print '--End ' + @cDatabaseName + '.dbo.CDRFile'
		print ''
		
		END
	ELSE
		EXEC (@cSQL)
	--ENDIF
	
	FETCH oCursor into @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

IF @nIsPrint = 1
	BEGIN

	SET @cSQL = 'SELECT *'
	SET @cSQL = @cSQL + @cCRLF + 'FROM #oTable1'
	SET @cSQL = @cSQL + @cCRLF + 'ORDER BY Item,DatabaseName,TableName,ItemCount'
	
	print '--Begin  Temp table creation'
	print @cSQL
	print '--End  Temp table creation'
	print ''

	END
ELSE
	BEGIN
	
	SELECT *
	FROM #oTable1
	ORDER BY Item,DatabaseName,TableName,ItemCount
	
	END
--ENDIF