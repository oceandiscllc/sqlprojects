DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
	DROP TABLE #tTable

CREATE TABLE #tTable
	(
	--DatabaseName varchar(50),
	LMSID int,
	Event varchar(250),
	Organization varchar(250),
	MonthNumber int,
	YearNumber int
	)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB
	WHERE DB.Name IN ('ACGU','ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
		AND DB.Name NOT LIKE '%SOCOM%'
	ORDER BY DB.Name
	
OPEN oCursor
FETCH oCursor INTO @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 
		'INSERT INTO #tTable (LMSID, Event, Organization, MonthNumber, YearNumber) '
		+ 'SELECT D.LMSID, E.Event, T.TierLabel, D.MonthNumber, D.YearNumber '
		+ 'FROM (SELECT L.LMSID, L.EventID, L.OriginatingTierID, MONTH(L.CreationDate) AS MonthNumber, YEAR(L.CreationDate) AS YearNumber '
		+ 'FROM JSCC.dbo.LMS L WHERE ImportSource IS NULL OR LEN(LTRIM(RTRIM(ImportSource))) = 0) D '
		+ 'JOIN JSCC.Dropdown.Event E ON E.EventID = D.EventID JOIN JLLIS.dbo.Tier T ON T.TierID = D.OriginatingTierID'

		-- old stuff here
		--'INSERT INTO #tTable (LMSID, Event, Organization, MonthNumber, YearNumber) '
		--+ 'SELECT D.LMSID, E.Event, T.TierLabel, D.MonthNumber, D.YearNumber '
		--+ 'FROM (SELECT L.LMSID, L.EventID, L.OriginatingTierID, MONTH(L.CreationDate) AS MonthNumber, YEAR(L.CreationDate) AS YearNumber FROM '
		--+ 'JSCC.dbo.LMS L WHERE (ImportSource IS NULL OR LEN(LTRIM(RTRIM(ImportSource)) = 0) D JOIN '
		--+ 'JSCC.Dropdown.Event E ON E.EventID = D.EventID JOIN JLLIS.dbo.Tier T ON T.TierID = D.OriginatingTierID '
	
	EXEC(@cSQL)

	FETCH oCursor into @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

SELECT 
	T.DatabaseName AS Instance, 
	T.LMSID, 
	T.Event, 
	T.Organization, 
	LEFT(DATENAME(month, DATEADD(month, T.MonthNumber, 0) - 1), 3) AS [Create Month],
	T.YearNumber AS [Create Year]
FROM #tTable T