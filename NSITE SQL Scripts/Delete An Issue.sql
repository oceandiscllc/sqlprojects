USE JLLIS
GO

DELETE
FROM dbo.Issue
WHERE IssueID = --467
GO

DELETE IT
FROM dbo.IssueBinder IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueCOP IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueDiscussion IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueIssue IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID1
	)
GO

DELETE IT
FROM dbo.IssueIssue IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID2
	)
GO

DELETE IT
FROM dbo.IssueLMS IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueMetadata IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueMilestone IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssuePublication IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueSME IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueStatusHistory IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueTask IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE IT
FROM dbo.IssueTier IT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Issue I
	WHERE I.IssueID = IT.IssueID
	)
GO

DELETE ET
FROM dbo.EntityJLLISFile ET
WHERE ET.EntityTypeCode = 'Issue'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Issue I
		WHERE I.IssueID = ET.EntityID
		)
GO

DELETE ET
FROM dbo.EntityMember ET
WHERE ET.EntityTypeCode = 'Issue'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Issue I
		WHERE I.IssueID = ET.EntityID
		)
GO

DELETE ET
FROM dbo.EntitySubscription ET
WHERE ET.EntityTypeCode = 'Issue'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Issue I
		WHERE I.IssueID = ET.EntityID
		)
GO