SELECT 
	COUNT(TD.DropdownEntityID) AS ItemCount,
	TD.TierID, 
	TD.DropdownEntityID
FROM JSCC.Dropdown.TierDropdown TD
	JOIN JLLIS.Dropdown.DropdownMetaData DMD ON DMD.DropdownMetaDataID = TD.DropdownMetaDataID
		AND DMD.DropdownMetaDataCode = 'EventType'
GROUP BY 
	TD.TierID, 
	TD.DropdownEntityID
HAVING COUNT(TD.DropdownEntityID) > 1
ORDER BY TD.TierID, TD.DropdownEntityID
  