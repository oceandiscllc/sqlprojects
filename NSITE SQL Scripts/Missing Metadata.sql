DECLARE @cDropdownMetadataCode varchar(50)
DECLARE @nDropdownMetadataID int
DECLARE @nTierID int

SET @cDropdownMetadataCode = 'Metadata'
SET @nDropdownMetadataID = (SELECT DMD.DropdownMetadataID FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = @cDropdownMetadataCode)
SET @nTierID = --934

--INSERT INTO JSCC.Dropdown.TierDropdown
--	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	@nTierID,
	@nDropdownMetadataID,
	MD.MetadataID
FROM JSCC.dbo.LMS L
	JOIN JSCC.dbo.LMSMetadata LMD ON LMD.LMSID = L.LMSID
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = LMD.MetadataID
		AND L.OriginatingTierID = @nTierID
		AND NOT EXISTS
			(
			SELECT 1
			FROM JSCC.Dropdown.TierDropdown TD
			WHERE TD.DropdownEntityID = MD.MetadataID
					AND TD.DropdownMetadataID = @nDropdownMetadataID
					AND TD.TierID = @nTierID
			)
ORDER BY MD.MetadataID