/*
   Wednesday, June 02, 20101:55:17 PM
   User: Eric Jones & Todd Pires
   Database: ALL
   Application: JLLIS
   Procedure: alters any column named MSC or TIER2 to be a VARCHAR(250) to help prevent and unify the codebase.
   250 was selected because this was the length of MSC in the JLLIS Database and shoudl be the systemof record this kind of data
*/

DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR FOR
	SELECT
		'ALTER TABLE ' + SS.name + '.' + SO.name + ' ALTER COLUMN ' + SC.name + ' varchar(250)' AS SQLText
	FROM sys.objects SO WITH (NOLOCK)
		JOIN sys.schemas SS ON SS.schema_ID = SO.schema_ID
		JOIN sys.columns SC WITH (NOLOCK) ON SO.object_id = SC.object_id
		JOIN sys.types ST WITH (NOLOCK) ON SC.user_type_id = ST.user_type_id
	WHERE SO.type = 'U' 
		AND SC.name IN ('msc', 'tier2')

OPEN oCursor
FETCH oCursor into @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXECUTE (@cSQL)

	FETCH oCursor into @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO