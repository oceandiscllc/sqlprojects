DECLARE @dDateStart datetime
DECLARE @dDateStop datetime

SET @dDateStart = null--'01/01/2011'
SET @dDateStop = null--'12/31/2011'
	
SELECT 
	COUNT(L.LMSID) AS ItemCount,
	--MONTH(L.CreationDate) AS CreateMonth,
	--YEAR(L.CreationDate) AS CreateYear, 
	T.TierLabel
FROM dbo.LMS L WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON L.OriginatingTierID = T.TierID
		AND (@dDateStart IS NULL OR L.CreationDate >= @dDateStart)
		AND (@dDateStop IS NULL OR L.CreationDate <= @dDateStop)
GROUP BY T.TierLabel--MONTH(L.CreationDate), YEAR(L.CreationDate), T.TierLabel
ORDER BY T.TierLabel--YEAR(L.CreationDate), MONTH(L.CreationDate), T.TierLabel