DECLARE @cTierName varchar(50)
DECLARE @nTierID int

SET @cTierName = 'JFCOM';
SET @nTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)

;
WITH HD (DisplayOrder,TierLineage,TierName,TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(255), T.TierName),
		CAST(T.TierID as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		1
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL
	
	SELECT
		CONVERT(varchar(255), RTRIM(DisplayOrder) + ',' + T.TierName),
		CAST(HD.TierLineage + ',' + CAST(T.TierID as varchar(50)) as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
SELECT 
	HD.TierName,
	JU.JLLISUserID,
	TU.TierUserID,
	JU.LastName,
	JU.FirstName,
	CASE WHEN LEN(LTRIM(JU.DayPhone)) = 0 THEN ' ' ELSE JU.DayPhone END AS DayPhone,
	CASE WHEN LEN(LTRIM(JU.NightPhone)) = 0 THEN ' ' ELSE JU.NightPhone END AS NightPhone,
	CASE WHEN LEN(LTRIM(JU.State)) = 0 THEN ' ' ELSE JU.State END AS State,
	CONVERT(char(11), TU.ActiveDate, 113) AS LastActiveDate,
	CONVERT(char(11), TU.StartDate, 113) AS StartDate,
	JU.Email,
	TU.SecLev,
	SL.Role
FROM HD
	JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.DefaultTierID = HD.TierID
		AND TU.Status = 'Active'
	JOIN JLLIS.dbo.JLLISUser JU WITH (NOLOCK) ON JU.JLLISUserID = TU.JLLISUserID
	JOIN JLLIS.Dropdown.SecLev SL WITH (NOLOCK) ON SL.SecLev = TU.SecLev
ORDER BY HD.DisplayOrder, JU.LastName, JU.FirstName