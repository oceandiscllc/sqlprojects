USE JLLIS
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tJLLISUser', 'u')) IS NOT NULL
	DROP TABLE #tJLLISUser
--ENDIF	
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tTierUser', 'u')) IS NOT NULL
	DROP TABLE #tTierUser
--ENDIF	
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tPassword', 'u')) IS NOT NULL
	DROP TABLE #tPassword
--ENDIF	
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tPasswordRemnant', 'u')) IS NOT NULL
	DROP TABLE #tPasswordRemnant
--ENDIF	
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#tSecurityAnswer', 'u')) IS NOT NULL
	DROP TABLE #tSecurityAnswer
--ENDIF	
GO

CREATE TABLE #tJLLISUser
	(
	jllisuserid int,
	jllisusername varchar(250),
	originatingtierid int NULL,
	usertype varchar(50),
	password varchar(20),
	firstname varchar(25),
	lastname varchar(25),
	email varchar(75),
	sipremail varchar(75),
	dayphone varchar(25),
	nightphone varchar(25),
	cellphone varchar(25),
	tollfreephone varchar(25),
	iridiumphone varchar(25),
	expertise varchar(100),
	mos varchar(50),
	dob datetime NULL,
	datereported datetime NULL,
	bloodtype varchar(10),
	anthrax varchar(10),
	pistolserial varchar(50),
	gasmasksize varchar(10),
	fitrepdue datetime NULL,
	fitreprs varchar(50),
	fitrepro varchar(50),
	company varchar(25),
	unit varchar(50),
	zip varchar(10),
	homepage char(1),
	city varchar(35),
	state char(2),
	country varchar(35),
	shipemail varchar(35),
	fax varchar(20),
	address varchar(100),
	tempint int NULL,
	autoresponderbcc char(1),
	notes text NULL,
	showhelp char(1),
	paygrade varchar(50),
	title varchar(50),
	deploymentcity varchar(50),
	deploymentregion varchar(50),
	deploymentprovince varchar(50),
	deploymentcountry varchar(50),
	deploymentplace varchar(50),
	TaskTrackerToken int NULL,
	defaultcompany varchar(40),
	defaultproject varchar(40),
	multicompany char(1),
	deletetasks char(1),
	deleteevents char(1),
	defaultview varchar(40),
	admin char(1),
	defaultfont varchar(250),
	defaultcolor varchar(40),
	allowedcompanies varchar(40),
	defaultsort varchar(40),
	department varchar(40),
	account varchar(20),
	bulkmail char(1),
	mpassword varchar(15),
	pkiforce char(1),
	confirmation varchar(50),
	accessid varchar(35),
	passwordquestion varchar(500),
	passwordanswer varchar(500),
	email2 varchar(320),
	email3 varchar(320),
	FailedLoginAttempts int,
	FailedLoginDate datetime NULL,
	IsAccountLocked bit,
	IsPasswordResetRequired bit,
	CountryID int
	)
GO

INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (35899,'catherine.hinners',192,NULL,NULL,'Catherine','Hinners','catherine.hinners@nsitellc.com','dummy.test@nsitellc.com','(102) 555-5899','(105) 555-5899','(101) 555-5899','(106) 555-5899','(104) 555-5899',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'35899-0000',' ','Catherineville','FL','USA','dummy.test@nsitellc.com','(103) 555-5899','35899 Main Street',NULL,NULL,NULL,NULL,'E-9','Ms','Catherinedeploycity','Catherinedeploycounty','Catherinedeployprovince','Catherinedeploycountry','Catherinedeployplace',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'HINNERS.CATHERINE.LYNNETTE.1100001655',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F5423D1C-5056-8C16-F5EAAB770EEE10AC',NULL,NULL,'catherine.hinners@nsitellc.com','catherine.hinners@nsitellc.com',0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (49603,'candrews',265,NULL,NULL,'Christopher','Andrews','christopher.andrews@socom.mil','dummy.test@nsitellc.com','813.826.3139','813.826.3139','813.826.3139','826.3139','AVISrara7869&*^^',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'49603-0000',NULL,'Christopherville','FL','USA','dummy.test@nsitellc.com','(103) 555-9603','49603 Main Street',NULL,NULL,NULL,NULL,'CIV','SA','Christopherdeploycity','Christopherdeploycounty','Christopherdeployprovince','Christopherdeploycountry','Christopherdeployplace',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ANDREWS.CHRISTOPHER.R.1299554801',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F25D5646-5056-8C16-F54A7E4C59E20198',NULL,NULL,'christopher.andrews@socom.mil','christopher.andrews@socom.mil',0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (72853,'catherine.hinners2',1163,NULL,NULL,'Catherine','Hinners','catherine.hinners@nsitellc.com','dummy.test@nsitellc.com','(102) 555-2853','(105) 555-2853','(101) 555-2853','(106) 555-2853','(104) 555-2853',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'72853-0000',' ','Catherineville','FL','USA','dummy.test@nsitellc.com','(103) 555-2853','72853 Main Street',NULL,NULL,NULL,NULL,'E-3','Ms','Catherinedeploycity','Catherinedeploycounty','Catherinedeployprovince','Catherinedeploycountry','Catherinedeployplace',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'57A5038D-5056-8C16-F50EE8EED97C8ACE',NULL,NULL,'catherine.hinners@nsitellc.com','catherine.hinners@nsitellc.com',0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94103,'DR-SA-2',196,NULL,'NewPasswordHolder','DR','SA-2','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-10','Sir',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94104,'music.boy',22,NULL,'NewPasswordHolder','Franz','Schubert','christopher.andrews@socom.mil',NULL,'813.826.3139',NULL,NULL,'826.3139','PUNDfitt4221$@@!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CTR','Sir',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'EBCEA375-5056-8C16-F5AD6F6330507CA6',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94107,'dr-authorized-js',192,NULL,'NewPasswordHolder','DR','AUTHORIZED','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-6','SIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'0152EC49-5056-8C16-F5E6DFF7CA7D95DB',NULL,NULL,NULL,NULL,0,NULL,1,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94110,'dr.authorized.js',810,NULL,'NewPasswordHolder','DR-','AUTHORIZED-JS','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CWO-2','SIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'817CF9B5-5056-8C16-F54C7D8A09398E22',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94112,'DR-TEAM-JS',466,NULL,'NewPasswordHolder','DR-','TEAM-JS','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-1','SIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'48E84B09-5056-8C16-F59FD64AF6437176',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94113,'dr-manager-js',9,NULL,'NewPasswordHolder','DR-','MANGER-JS','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-7','SIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'ECDD40CE-5056-8C16-F56C42F146EADDAF',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94117,'dr-admin-js',192,NULL,'NewPasswordHolder','DR-','ADMIN-JS','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-5','SIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F5A751E8-5056-8C16-F586A9B815273ADA',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94141,'QA.CA.AUTH',10,NULL,NULL,'Chris','Andrews - AUTH','chris.andrews.test.authuser@nsitellc.com',NULL,'813.826.3139',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F0916003-5056-8C16-F51193EE400B4B43',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94143,'QA.CA.MGR',9,NULL,NULL,'QA.','CA.MGR','chris.andrews.test.manager@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'F0D7F580-5056-8C16-F59A740DF73A14FB',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94144,'QA.CA.ADMIN',9,NULL,NULL,'QA.','CA.ADMIN','chris.andrews.test.admin@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F0EB6236-5056-8C16-F5C2D9F404858FE5',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94151,'CR-AUTH',9,NULL,NULL,'Christian','Ready','christian.ready.test.authuser@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94152,'CR-CLM',9,NULL,NULL,'Christian','Ready','christian.ready.test.clm@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94153,'CR-MGR',9,NULL,NULL,'Christian','Ready','christian.ready.test.manager@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94154,'CR-ADMIN',9,NULL,NULL,'Christian','Ready','christian.ready.test.admin@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94155,'CR-SA',9,NULL,NULL,'Christian','Ready','christian.ready.test.sa@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'C5A444EE-5056-8C24-A1CC072B28E81704',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94156,'ragmanauth',9,NULL,NULL,'Ragman','Auth User','david.demming.test.authuser@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CA879AB7-5056-8C24-A1B891EA1A647877',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94157,'ragmanclm',9,NULL,NULL,'Ragman','CLM','david.demming.test.clm@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'30F88957-5056-8C24-A123BA97B6839F5E',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94158,'ragmanteam',9,NULL,NULL,'Ragman','Team','david.demming.test.team@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CD6EEB6B-5056-8C24-A1ABBF6A8DD12B03',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94159,'ragmanmgr',9,NULL,NULL,'Ragman','Mgr','david.demming.test.mgr@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CD6245C7-5056-8C24-A16FFD918A39CB92',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94160,'ragmanadmin',9,NULL,NULL,'Ragman','Admin','david.demming.test.admin@nsitellc.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CAC8B47D-5056-8C24-A1536B316A4C7B2B',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94168,'pt1032',323,NULL,'NewPasswordHolder','William','Williamson','christopher.andrews@socom.mil',NULL,'813.826.3139','ARMYarmy1212!@!@',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'E-8','MSG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'C0C23261-5056-8C16-F5FD584573814E7F',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94171,'dr-admin-navy',5,NULL,'NewPasswordHolder','DR-','NAVY ADMIN','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MIDN','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94172,'dr.clm.js',192,NULL,'NewPasswordHolder','DR.','CLM.JS','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-7','TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'B8F246F6-5056-8C16-F56B0BA1C211754A',NULL,NULL,'david.roberts@nsitellc.com','david.roberts@nsitellc.com',0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94178,'franz.haydn',22,NULL,'NewPasswordHolder','Franz','Haydn','christopher.andrews@socom.mil',NULL,'8138263139',NULL,NULL,NULL,'PULAflab1233!@##',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SES','Mr.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'E57B01F8-5056-8C16-F5B2044F3C46393C',NULL,NULL,NULL,NULL,2,CAST(0x0000A181008E243A AS DateTime),0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94187,'qa.airman',1157,NULL,'NewPasswordHolder','Allen','Airman','christopher.andrews@socom.mil',NULL,'000.0000','PINDelpp1234!@#$',NULL,'000.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET','QA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F17A5673-5056-8C16-F520A8D68262DAA9',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94188,'emily.bronte',196,NULL,'NewPasswordHolder','Emily','Bronte','christopher.andrews@socom.mil',NULL,'000.xxx.0000',NULL,NULL,'000.xxxx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CIV','Miss',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94198,'alan.turing',391,NULL,'NewPasswordHolder','Alan','Turing','christopher.andrews@socom.mil',NULL,NULL,NULL,NULL,NULL,'COMPkode114!!$$',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET','Cryptologist',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'7EBCB657-5056-8C16-F5E3FCD091D98AAD',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94201,'ARMY-CALL-KLI-ADMIN',9,NULL,'NewPasswordHolder','ARMY CALL KLI','ADMIN','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-10','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'EE6D8E32-5056-8C16-F5CD7B29AEA99AA1',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94202,'ARMY-CALL-KLI-MANAGER',958,NULL,'NewPasswordHolder','ARMY-CALL-KLI-','MANAGER','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'B63E7691-5056-8C16-F538A6FBD7E97105',NULL,NULL,NULL,NULL,0,NULL,0,0,0)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94204,'ARMY-CALL-KLI-TEAM',958,NULL,'NewPasswordHolder','ARMY-KLI-','TEAM','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CWO-5','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'C1C03993-5056-8C16-F5F301D7FADF8EEC',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94205,'ARMY-CALL-KLI-CLM',958,NULL,'NewPasswordHolder','ARMY-CALL-KLI-','CLM','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-10','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'2AAE33ED-5056-8C16-F55F3EFE1A0F91EE',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94206,'ARMY-CALL-KLI-COI_MANAGER',958,NULL,'NewPasswordHolder','ARMY-CALL-KLI-','COI-MANAGER','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-4','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'B86AE4BA-5056-8C16-F5DAEF141AF89568',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94207,'ARMY-KLI-IMS-GATEKEEPER',958,NULL,'NewPasswordHolder','ARMY-KLI-','IMS-GATEKEEPER','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CTR','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'BC640005-5056-8C16-F521DF467AC7D436',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94208,'ARMY-CALL-KLI-IMS-SME',958,NULL,'NewPasswordHolder','AMRY-CALL-KLI-','IMS-SME','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-7','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'BCC592C4-5056-8C16-F5BF54120C456F63',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94209,'ARMY-CALL-KLI-AUTHORIZED',958,NULL,'NewPasswordHolder','ARMY-CALL-KLI-','AUTHORIZED','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WOCC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SES','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'EC1D0435-5056-8C16-F52FA2C78C85F2A8',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94210,'USAF-HAF-ADMIN',649,NULL,'NewPasswordHolder','USAF-HAF','ADMIN','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-10','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'EC7961A5-5056-8C16-F5D0D0A73F056C76',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94216,'USAF-HAF-TEAM',649,NULL,'NewPasswordHolder','USAF-HAF-','TEAM','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WR-ALC Warner Robins Air Logistics Ctr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0-6','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'1D620723-5056-8C16-F563EDE3EB91B296',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94217,'USAF-HAF-CLM',649,NULL,'NewPasswordHolder','USAF-HAF','CLM','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WR-ALC Warner Robins Air Logistics Ctr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SES','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'F68BCF07-5056-8C16-F51844586245293C',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94218,'USAF-HAF-AUTHORIZED',649,NULL,'NewPasswordHolder','USAF-HAF-','AUTHORIZED','david.roberts@nsitellc.com',NULL,'703.291.1152',NULL,NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CADET','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'B5C33CC2-5056-8C16-F5A03E739445E396',NULL,NULL,NULL,NULL,0,NULL,0,0,0)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94223,'catherine.umberger',187,NULL,'NewPasswordHolder','Catherine','Hinners','catherine.umberger@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CIV','CLM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'2C10A3B8-5056-8C16-F59277A6F7A89696',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
INSERT INTO #tJLLISUser (jllisuserid,jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID) VALUES (94225,'ARMY-CALL-ADMIN',732,NULL,'NewPasswordHolder','ARMY-CALL-','ADMIN','david.roberts@nsitellc.com',NULL,'703.291.1152','703.291.1152',NULL,'703.291.1152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CTR','NSITE QA TESTER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,'EED673AB-5056-8C16-F5F2974F45802DA0',NULL,NULL,NULL,NULL,0,NULL,0,0,233)
GO

CREATE TABLE #tTierUser
	(
	tieruserid int IDENTITY(1,1)NOT NULL,
	jllisuserid int,
	tierid int,
	seclev int,
	st1 varchar(20),
	msc varchar(100),
	startdate datetime NULL,
	activedate datetime NULL,
	status varchar(20),
	search1 varchar(100),
	search2 varchar(100),
	search3 varchar(100),
	search4 varchar(100),
	search5 varchar(100),
	search6 varchar(100),
	search7 varchar(100),
	search8 varchar(100),
	search9 varchar(100),
	search10 varchar(100),
	defaulttierid int
	)
GO

INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (35899,8,7000,'AFRICOM','SPS-PA',CAST(0x00009B3F00000000 AS DateTime),CAST(0x0000A18F00E4645C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,192)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (49603,8,7000,NULL,NULL,CAST(0x00009C1800000000 AS DateTime),CAST(0x0000A18F00CE29BC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,265)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (72853,8,6000,NULL,NULL,CAST(0x00009E5B00000000 AS DateTime),CAST(0x0000A18700D7E074 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1163)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94103,8,7000,NULL,NULL,CAST(0x0000A10300000000 AS DateTime),CAST(0x0000A10300000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,196)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94104,8,2000,NULL,NULL,CAST(0x0000A10300000000 AS DateTime),CAST(0x0000A18000BE2A08 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94107,8,2000,NULL,NULL,CAST(0x0000A10400000000 AS DateTime),CAST(0x0000A11D00AC9AB8 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,192)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94110,8,2000,NULL,NULL,CAST(0x0000A10700000000 AS DateTime),CAST(0x0000A16A00C2013C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,810)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94112,8,6000,NULL,NULL,CAST(0x0000A10700000000 AS DateTime),CAST(0x0000A15000D443D8 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,466)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94113,8,6000,NULL,NULL,CAST(0x0000A10700000000 AS DateTime),CAST(0x0000A18000C647C4 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94117,8,6000,NULL,NULL,CAST(0x0000A10700000000 AS DateTime),CAST(0x0000A18000000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,192)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94141,8,2500,NULL,NULL,CAST(0x0000A11A00000000 AS DateTime),CAST(0x0000A18F00C05C88 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94143,8,4000,NULL,NULL,NULL,CAST(0x0000A18F00C27B1C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94144,8,7000,NULL,NULL,CAST(0x0000A10800000000 AS DateTime),CAST(0x0000A18F00C3111C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94151,8,2000,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94152,8,2500,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94153,8,4000,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94154,8,6000,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94155,8,7000,NULL,NULL,NULL,CAST(0x0000A108009B9808 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94156,8,2000,NULL,NULL,NULL,CAST(0x0000A10800C1203C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94157,8,2500,NULL,NULL,NULL,CAST(0x0000A10A00BCDC0C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94158,8,3000,NULL,NULL,NULL,CAST(0x0000A10800D76D9C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94159,8,4000,NULL,NULL,NULL,CAST(0x0000A10800D70C58 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94160,8,6000,NULL,NULL,NULL,CAST(0x0000A10800C314A0 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94168,8,6000,NULL,NULL,CAST(0x0000A11000000000 AS DateTime),CAST(0x0000A17A00FA0DAC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,323)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94171,8,6000,NULL,NULL,CAST(0x0000A11D00000000 AS DateTime),CAST(0x0000A11D00000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94172,8,2000,NULL,NULL,CAST(0x0000A11F00000000 AS DateTime),CAST(0x0000A157010E4128 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,192)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94178,8,2500,NULL,NULL,CAST(0x0000A14300000000 AS DateTime),CAST(0x0000A180008D9294 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94187,8,4000,NULL,NULL,CAST(0x0000A15000000000 AS DateTime),CAST(0x0000A18000E9B614 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1157)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94188,8,2000,NULL,NULL,CAST(0x0000A15000000000 AS DateTime),CAST(0x0000A15000000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,196)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94198,8,6000,NULL,NULL,CAST(0x0000A15E00000000 AS DateTime),CAST(0x0000A1740095F574 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,391)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94201,8,6000,NULL,NULL,CAST(0x0000A16D00000000 AS DateTime),CAST(0x0000A18500C6D6BC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94202,8,2000,NULL,NULL,CAST(0x0000A16D01064738 AS DateTime),CAST(0x0000A17000C035DC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94204,8,3000,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A17F0106AC88 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94205,8,2500,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A181011584EC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94206,8,2100,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A17000000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94207,8,2400,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A17000EF6A3C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94208,8,2300,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A17000F2583C AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94209,8,2000,NULL,NULL,CAST(0x0000A17000000000 AS DateTime),CAST(0x0000A18000C08334 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,958)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94210,8,6000,NULL,NULL,CAST(0x0000A17200000000 AS DateTime),CAST(0x0000A18000C34830 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,649)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94212,8,4000,NULL,NULL,CAST(0x0000A17200000000 AS DateTime),CAST(0x0000A17200000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,649)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94216,8,3000,NULL,NULL,CAST(0x0000A17200000000 AS DateTime),CAST(0x0000A17200000000 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,649)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94217,8,2500,NULL,NULL,CAST(0x0000A17200000000 AS DateTime),CAST(0x0000A1800110A288 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,649)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94218,8,2000,NULL,NULL,CAST(0x0000A17201015840 AS DateTime),CAST(0x0000A17A00A59AEC AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,649)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94223,8,2500,NULL,NULL,CAST(0x0000A18100000000 AS DateTime),CAST(0x0000A18101202730 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,187)
INSERT INTO #tTierUser (jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid) VALUES (94225,8,6000,NULL,NULL,CAST(0x0000A18500000000 AS DateTime),CAST(0x0000A18500C9FBD0 AS DateTime),'Active',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,732)
GO

CREATE TABLE #tPassword
	(
	PasswordID int,
	JLLISUserID int,
	PasswordHash varchar(250),
	PasswordSalt varchar(250),
	CreationDate datetime,
	IsActive bit
	)
GO

INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11534,35899,'E277A35D488C947613CE2182CFE02C22C75EFEF5C5E886863F422060F30A287D856AD2A0464E4172FB67558F202B874D7457CDFA097DC6640FB1C03B5C5EAF2E','TAdv4Fw7iq+JpVa79K1ZSQ==',CAST(0x0000A0FD00DA90C7 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (5093,35899,'3DAC65255E7F7B4385DF102882231A3AEB53BE105605D97B66689637701FC46541D2697324F6DFD0CC6C960F4959E879C0D0ECD3C223292590F896A9B745D91A','VILMmZ4AiS8886u/Ga9/mg==',CAST(0x0000A0440098C057 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11,35899,'13679F1D386294CBF5137041C68A27D24C401D81A686B1D6EA05570BD269C03A0A5E93CD4E15C1F9DBFF2D63B0041E8B0E118E64E4FC43F844DE01D3E10AB68D','/eKl6vbWXsDKnDczAdII+g==',CAST(0x00009FE90111EB70 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11601,49603,'0366CF9D525055EE309182021DC96F1FF19429D02E9F600867D7F6A3746BBA627625A084E7EFBC78BE4660FBB1486EE6966E9667A9E149091F4A20EBA2673465','Y7Vuvp3yt+OTGAeBFAUI+Q==',CAST(0x0000A14200F25123 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11545,49603,'38D36BD7AF91D72B16BA4A22B11B48984E306AE3C8D23C4601E1CEDA950AEDB3F6196C8463BEE86DA66352074EBD49F916445B869402A5D4EE98401A8B17ACEC','/36cBK8BHwxnqsyDBOws0A==',CAST(0x0000A10200800CA2 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (9616,49603,'C86DFF2172510788D996A4DDB32494011405FE7C092C65FB4B738B49680C46F1CADC3EF07527A1C91A94F060EEE46028859FC39D36CDFBD617751B5AC2B43F44','Im2uz+KNqt0AntSaEL8cDw==',CAST(0x0000A0A0004DF073 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (5201,49603,'8CA507FCA51BF62D617BE124BF9F8B465E03A13DB17402E1F4944898BF402BF09FCEF7740F9C6D6C2959103BE5550D8A5E2DBBB0531E7F686BB3597ED8AA09CA','R/mt7kJ7cbxzJUFXrTWhIw==',CAST(0x0000A04500CBA728 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (296,49603,'734686EC5F64DF8D134247EA7B80A06340AD3B8E3F6450B49CFA3DFB25930F1C423FA3796B2142C87309591FEB2BCF35FE41DF3A850067C80279CA39B599655D','5UxooK8gtMQ3/r25SmjfbA==',CAST(0x00009FEB00CA1306 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11612,94104,'34F770749CE3E7BAB755DF04F3C06DA8C22F2FDF43BCBDB35C515B270F50076BF28D7D0D23DB8B5B630C58E28862F259967FBEDC9DA48B3E7E1378BB6FB53218','7jBKxHxiwTMWq/iQqDYkng==',CAST(0x0000A15000F9F9DA AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11602,94104,'A77F134897EA29559A8C2C6DB872DEB0AEBBCE1632E0F91B7B51AEFAB39494007ECAA3A8A3991AAFC4D03972A124C44DBF55B902CE65B7D713B8E25D6EC61AC5','mYbnBOFCLZ+V/jXbpLhx1Q==',CAST(0x0000A14300E0D6F2 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11555,94104,'4F6E54E910C3354F3C51A2CCFBC74CBE2D5BCFE1EEB5E05784D240152945ECFCBC4A27B181D5EBF3DAD41E25AE1242C9C1407F4E86F35669D6DC3B809A57ABDB','OMzezUjR4AlgHLeBdFFNaA==',CAST(0x0000A10401004B01 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11556,94107,'B279FDE204B83E0D0DD318AC1BA0336A934F3C68B9DDE1B8BD6D2757EC176FA6D911EA4EE7178F1DB1434045ED94A4777BDFB7367D1657B8B93A528B163C5016','YcJy2ZCSgxqbpUr/TDxQEQ==',CAST(0x0000A107009A10D9 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11611,94110,'E964E946BCE07F7317B50A1B6EC0EBB6758CE03B792ACE0603BB659D31E267A8EABA78A81659E87C463DCE248934D8DAE2D5C8693B50DE1E68D269194CAE5BFE','xL/x9Iq1mV+OoCQuw0Ka2A==',CAST(0x0000A15000DC16D7 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11593,94110,'0C06F115FB3EABA3556F03184093A3A55632E8A8846766DFB9CAC6B7F0DF1F0E8887D4F5F9A69526130383F3101CC8BA72A3A72ABF2C65DB166FBA8FD127C5B3','57i8FbynVjfaLVDdjdf1SA==',CAST(0x0000A11E008E3A5A AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11590,94112,'BA6AE7E2C845B9D1B9AF884D70D7110CDEC125F920295E2F4B4A9011449D87A0AEE3EC3C024D9E32A47A3E4B322F78685372656179B5F003147893908552AED9','VqpBxSOXdA354x6X5ALSaA==',CAST(0x0000A11C00FEABD4 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11591,94113,'1ECFC4F3D259085BBF2D0BC9B43BAF034C09BBA2FB3861F4EC2F1F3302EE1149965D59790B119FEEC433EC47A14C3CB1FAA385D365878086C475B51FB211391E','sbshRa/Tx/b+82fyYSKp/A==',CAST(0x0000A11D00B07D0F AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11558,94113,'CB16746555BE3E98BDB94BC0A8ED4077BD7F2CD456B1C05A335870EFD4487B5A0D65EAE6066D144F2370F7D2F939FC8437B1BA5DD137C933101F7B6DE7869822','9/NvUQj5VoOztyb/QRnk9A==',CAST(0x0000A10700D3196C AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11592,94117,'39A2B4E8227867AAA2F9D6539E74CFDB6F12BBAEC41D8CB7427328F06448E7E218FF39169E68E17FE9E4850CA4A76E61D1B788C123D727EC3FF8AC55E10810AD','dRR7xOr/j4mx3qLpD1ni3g==',CAST(0x0000A11D00B3F6C9 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11559,94117,'2466E16DAFDD70F4FC3CBF92EF48E053EECE45399E58FD26DC692379671211C33895367BDFCA338D35337585457E8AB8D5F292B84EF3AE2B6F3C120AFC30BE94','QlsyG51Y9mO2ead9+tRRxw==',CAST(0x0000A10700D8A7A4 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11653,94141,'6911E8B295A12119D55FD07494DE20799E84BB892B84E71026428D47E9C3720AE1D572879BC02EC886B17C0D2552E21CAB39DFAE4CAC5867A6A560A23466552B','hF0o5FRDDo+lnmVbdJX8QA==',CAST(0x0000A18F00C0AEE6 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11582,94141,'464D84629CA861EC68276A9F9F98308303A6C43C106F8F198CD8BA20DBC07B583FA623A05F11F9CAE4C64F23F98E74337E17BBC762CC3C5EC5E783F7B6116550','CM2YSxHX68g16PRkeg+71Q==',CAST(0x0000A10800AEBB61 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11572,94141,'38D36BD7AF91D72B16BA4A22B11B48984E306AE3C8D23C4601E1CEDA950AEDB3F6196C8463BEE86DA66352074EBD49F916445B869402A5D4EE98401A8B17ACEC','/36cBK8BHwxnqsyDBOws0A==',CAST(0x0000A6EE00000000 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11655,94143,'86F1928CC7A620BAF97D169E8A1908A0B54A4E3D79168FEE48C2E0EE09642723CCDD3449E42D9621D20076E8E6C10668251E8E0E8146DB7D9303048B56CD6DC3','GeQugHgvxphSkf0LAreY/w==',CAST(0x0000A18F00C2AACD AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11583,94143,'1A5088779D088C2B04E41E0F4A1DE5CE0FFF2C9C47A0036AF5E3DE465F1B6BCFC65CB52BFCB2E01C0DCE32C2F1C319160341F6BE880E0BD06975468DDFCCA80E','XIed/hKiF0ssxhOs9QTZvw==',CAST(0x0000A10800B0CFDB AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11574,94143,'38D36BD7AF91D72B16BA4A22B11B48984E306AE3C8D23C4601E1CEDA950AEDB3F6196C8463BEE86DA66352074EBD49F916445B869402A5D4EE98401A8B17ACEC','/36cBK8BHwxnqsyDBOws0A==',CAST(0x0000A6EE00000000 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11656,94144,'97F0DDD94844378510F020541F71AB797CC7E4E042C1C84025647E87B8CE390A79725C4DE8AB24942B99E41BA60EB89B759B1BD1DEDD8259373977CE97751DAB','IE6dUcvVcGBCYBULOBy5PQ==',CAST(0x0000A18F00C35846 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11575,94144,'38D36BD7AF91D72B16BA4A22B11B48984E306AE3C8D23C4601E1CEDA950AEDB3F6196C8463BEE86DA66352074EBD49F916445B869402A5D4EE98401A8B17ACEC','/36cBK8BHwxnqsyDBOws0A==',CAST(0x0000A6EE00000000 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11562,94151,'609116D9136CA355751FD1D8BCBC2FC999717C71622138DF628D17F39DD2AFA077524651D99CE1E40A8846AEB8A494FC6F11240E83B93CBA78A5303E8C9C84DC','TSejatC5EWyQo3f7Auh5rQ==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11563,94152,'609116D9136CA355751FD1D8BCBC2FC999717C71622138DF628D17F39DD2AFA077524651D99CE1E40A8846AEB8A494FC6F11240E83B93CBA78A5303E8C9C84DC','TSejatC5EWyQo3f7Auh5rQ==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11564,94153,'609116D9136CA355751FD1D8BCBC2FC999717C71622138DF628D17F39DD2AFA077524651D99CE1E40A8846AEB8A494FC6F11240E83B93CBA78A5303E8C9C84DC','TSejatC5EWyQo3f7Auh5rQ==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11565,94154,'609116D9136CA355751FD1D8BCBC2FC999717C71622138DF628D17F39DD2AFA077524651D99CE1E40A8846AEB8A494FC6F11240E83B93CBA78A5303E8C9C84DC','TSejatC5EWyQo3f7Auh5rQ==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11566,94155,'609116D9136CA355751FD1D8BCBC2FC999717C71622138DF628D17F39DD2AFA077524651D99CE1E40A8846AEB8A494FC6F11240E83B93CBA78A5303E8C9C84DC','TSejatC5EWyQo3f7Auh5rQ==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11567,94156,'7A226B0E5D4B5F5CD9A1867FAA994666513625CE0EE6E07C84AE9B733F8C2E6939BD12A8C4628872BF1109F6C5789C4E3BA98CD66CC0F1ACE8F55E4089CE2B98','byeWYkU2Rv6HwVSQe5rE8A==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11568,94157,'7A226B0E5D4B5F5CD9A1867FAA994666513625CE0EE6E07C84AE9B733F8C2E6939BD12A8C4628872BF1109F6C5789C4E3BA98CD66CC0F1ACE8F55E4089CE2B98','byeWYkU2Rv6HwVSQe5rE8A==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11569,94158,'7A226B0E5D4B5F5CD9A1867FAA994666513625CE0EE6E07C84AE9B733F8C2E6939BD12A8C4628872BF1109F6C5789C4E3BA98CD66CC0F1ACE8F55E4089CE2B98','byeWYkU2Rv6HwVSQe5rE8A==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11570,94159,'7A226B0E5D4B5F5CD9A1867FAA994666513625CE0EE6E07C84AE9B733F8C2E6939BD12A8C4628872BF1109F6C5789C4E3BA98CD66CC0F1ACE8F55E4089CE2B98','byeWYkU2Rv6HwVSQe5rE8A==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11571,94160,'7A226B0E5D4B5F5CD9A1867FAA994666513625CE0EE6E07C84AE9B733F8C2E6939BD12A8C4628872BF1109F6C5789C4E3BA98CD66CC0F1ACE8F55E4089CE2B98','byeWYkU2Rv6HwVSQe5rE8A==',CAST(0x0000A6EE00000000 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11621,94168,'A8A6E70E18EE4581206FDF25A505CDCE749C1CFC5B05B1B87CCE5290DDBD3F118459B81FD03EADEAA4B1AE50F63AE82F2CCD61B1F30793AABC540C3B93E3C451','721x7dtu7tomtjQ269Urrw==',CAST(0x0000A15E00B287D3 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11587,94168,'8FC862344CDE39ABFE4485EED8F015DAEEA7639545970D99A594D74C35EBB666F4338FC95C14FA99CB83268CCEF7569B39A0647BAE3F0ED3EE6465303AEC290A','LghaTma12D8OM5wycP+vUQ==',CAST(0x0000A11000B9BE35 AS DateTime),0)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11594,94172,'89A39A472D3D9695F594D1D4D32755FB99C33744E454BEFF6A6DEDAFC95C444C33686AD829EBAB3625CAF715A872E3BE6FA5E50FA63B942086E4C58EA78B27C0','UBtMrtGIjclQN8hWgkqLwQ==',CAST(0x0000A11F00B589CB AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11626,94178,'DD6BF98E03F7F5674C106E6E5F164EEB72A8B8565A2E12C5028B251E6D51B77B072E8C3093900D43A76B3523FC7AB2DB90DB7967A57BE1B41137DDDC246772A5','aIqaBJaRiTWEhmrpFnwhxg==',CAST(0x0000A16900C3A12E AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11610,94187,'57428368E969AFC96A27555D77B2EA9B0E51E7A8B26999598CA7350ADA3FB3D3062F398394AB883DBBD59847DE17C15A70474735B9D9441C82C78A54BBD8B895','EsCv4EvIN1ZjfJqr8otAQA==',CAST(0x0000A15000DBB593 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11620,94198,'D18658872600337702F512DCACB6B40CF9B15B9D66C882446D93BABD9DF5972D5704B7A40EDD650E9780F67BF7415781EEC8B261F6D3E8C08CBA86A29497C4E9','QdGMEMBVq5yb3bgXbSOHzg==',CAST(0x0000A15E00AE48D5 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11627,94201,'9CE2F18062D7226DBF9AF38A65BF036CB6D50E06A212292A9BF55F87F84CA21C3EB475DFDE3D5DBFB2627DD1BA00A79396561E1BC0A566B1041019FBCD84379E','CCvvfO2PetBxLJvRSCPUNQ==',CAST(0x0000A16D010099D1 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11628,94202,'16F4F88F65AA38DB928676FD1A63A4A47BA7899805A214D6A85C4D1710E5A64A583FAACBBA08A325A84550D0053ADCD9CBEF0657A73FE9929FB3AA9CE4812E63','87YZpzy3T9VsGjdm2OdKVQ==',CAST(0x0000A16D01072F8B AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11629,94204,'2AE7D2537F9B98DEE6B731C6C890843B44FEB055C593068E48CE389568617AC44ACE62DF243F459C2357B2755E8C9014C41E01A38BD26CF9761397E63CA84353','ayK3EQtkKmCSJGjHCYD4xQ==',CAST(0x0000A17000C1B285 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11630,94205,'02D030681BCCC364FDA93D54CE9690C141203AF4B0CD4677DF983C037011E2F90729435DABEC513FC1A63EDC0236254E5B7300B7CC4671F79507F8C851C2D625','yo0zBkhcv77HfMqqUn981w==',CAST(0x0000A17000C5EA25 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11631,94206,'28F988294BB062E727C9177044F0A40CEB305A7BAC89A02469BF5FECF26C104CEC2F427F18A6530735648F7AE435014EB52404576A20F1226446E12040B501FF','3v7b4joUYZgLm1MUu4HJBQ==',CAST(0x0000A17000D11AA1 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11632,94207,'6D3B0634AE9FB33676E39BAE2C5C39268D8B6AAC9515DCE71342D926E258B2ECF4B17A48DD30CD1806B48A779A39F3C840468F87402E049029622C67C51CB45C','cBgt4WKQ8ipC5meW7z9RuQ==',CAST(0x0000A17000EAA59A AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11633,94208,'AA65402BF8630EE236D1FDBDA35C27E4AFF45EFC33D96FDA17467C9FAA500868182E10B367C197D874F59EE7FFB2A587C72D5D5EBD8AA5C02DD6FB6236E08524','Fd3h9AFoLgkHXZaB9naLxw==',CAST(0x0000A17000F0EE3D AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11634,94209,'F754B9BF9E886824E66C53011CD8703028B36E77103596113F4F8375EAFA6D3E85CDD1DBB7E7F15333E7A19298FA4B3DEBB530953116DB0A2660265B286760F2','2q/ng8W+ubtB0YmJbi5AxA==',CAST(0x0000A17000F3CBCB AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11635,94210,'281F3DD2B06E1F93734F023A040E64773A711F440EAFF35D4BD02A8C5894F5325B28446102C3DBE57B0840FABE8682BF8CA6B0DE7DFDD893C043ABAD405703DF','0WOSEP1HvKxBJt4MJnJtoQ==',CAST(0x0000A17200AE7BEC AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11639,94216,'C4E2C064FA2A31441079C080A5268549C20140B44BD2855C1FF9CFF133E8189DF61FAEB80C303AD9DB7F69D1F07FFD1A46BDF9CE6A653FCB7F9AAD4D0985DA85','uazvrNosaoKhPSsbnJnZPA==',CAST(0x0000A17200C071B3 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11640,94217,'CEE1CF4E3CFC7D10A79565013342CF107CD7309AB4BB909FFC5C77A0EB9EFFAACDBCD4720F854F9B121254C9F52E3D3EDDCAE154BA7593883546E7F4D140DA71','/LNCO7Q+UzugrcC32Xn1IA==',CAST(0x0000A17200C284BD AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11642,94218,'17B685D03FD82BF3016B5A8081992E37804C2FBB44A85899C871D58D30D9C9AA9530D810ACB6BD5A8CBB3B7B38F1FBEB89726C540D8DAC14BF8CF414A3861921','s1+7mTRd1K+21xUn2gO27A==',CAST(0x0000A17201017AB1 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11646,94223,'2854DBF211AD53B13C7DBE7D3398FDAD4416C99B6D95C334B0D465524CE3D5F25C31B5FFC9F15C82BBDE4E121E5384239C8F12DA13DF8F083546C81C77F8CE5A','aHspcACj2OwxY0EYcKUkrA==',CAST(0x0000A18101205C05 AS DateTime),1)
INSERT INTO #tPassword (PasswordID,JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive) VALUES (11648,94225,'B9925B7AD1954E8D7B83674E783488DDD608E1981B742815EADE5CDA1961DD522A465467E72DDF9D51932983E81625C6AE5D7F9A3FA92BDD887CA525DB5E9D45','qNCR+ODNgFOlB4AomapKeQ==',CAST(0x0000A18500C84FDA AS DateTime),1)
GO

CREATE TABLE #tSecurityAnswer
	(
	SecurityAnswerID int,
	JLLISUserID int,
	PasswordSecurityQuestionID int,
	SecurityAnswer varchar(250)
	)
GO

INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40522,35899,1,'jon')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40523,35899,5,'paul')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40524,35899,11,'east troy')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40756,49603,1,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40757,49603,5,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40758,49603,9,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40798,94104,1,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40799,94104,5,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40800,94104,9,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40606,94107,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40607,94107,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40608,94107,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40750,94110,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40751,94110,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40752,94110,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40684,94112,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40685,94112,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40686,94112,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40687,94113,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40688,94113,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40689,94113,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40690,94117,1,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40691,94117,5,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40692,94117,9,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40903,94141,1,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40904,94141,5,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40905,94141,9,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40915,94143,1,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40916,94143,5,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40917,94143,9,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40918,94144,1,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40919,94144,5,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40920,94144,9,'qa')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40636,94156,3,'Robert')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40637,94156,5,'Edwin')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40638,94156,12,'Troy')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40633,94157,3,'Robert')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40634,94157,5,'Edwin')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40635,94157,10,'Sherman')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40648,94158,3,'Robert')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40649,94158,5,'Edwin')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40650,94158,12,'Troy')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40645,94159,3,'Robert')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40646,94159,5,'Edwin')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40647,94159,10,'sherman')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40639,94160,3,'Robert')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40640,94160,5,'Edwin')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40641,94160,12,'Troy')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40795,94168,1,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40796,94168,5,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40797,94168,9,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40696,94172,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40697,94172,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40698,94172,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40813,94178,1,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40814,94178,5,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40815,94178,9,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40876,94187,1,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40877,94187,5,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40878,94187,9,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40789,94198,1,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40790,94198,5,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40791,94198,9,'qaserver')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40816,94201,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40817,94201,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40818,94201,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40822,94202,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40823,94202,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40824,94202,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40825,94204,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40826,94204,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40827,94204,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40828,94205,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40829,94205,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40830,94205,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40831,94206,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40832,94206,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40833,94206,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40834,94207,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40835,94207,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40836,94207,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40837,94208,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40838,94208,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40839,94208,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40840,94209,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40841,94209,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40842,94209,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40843,94210,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40844,94210,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40845,94210,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40855,94216,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40856,94216,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40857,94216,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40858,94217,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40859,94217,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40860,94217,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40864,94218,1,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40865,94218,5,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40866,94218,9,'QA')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40882,94223,1,'jon')
INSERT INTO #tSecurityAnswer (SecurityAnswerID,JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer) VALUES (40883,94223,5,'paul')
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID('dbo.FK_TierUser_JLLISUser') AND parent_object_id = OBJECT_ID('dbo.tieruser'))
	ALTER TABLE dbo.tieruser DROP CONSTRAINT FK_TierUser_JLLISUser
--ENDIF	
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID('dbo.FK_TierUser_Tier') AND parent_object_id = OBJECT_ID('dbo.tieruser'))
	ALTER TABLE dbo.tieruser DROP CONSTRAINT FK_TierUser_Tier
--ENDIF	
GO

DELETE
FROM dbo.JLLISUser
WHERE JLLISUserName IN 
	(
	'alan.turing',
	'ARMY-CALL-ADMIN',
	'ARMY-CALL-KLI-ADMIN',
	'ARMY-CALL-KLI-AUTHORIZED',
	'ARMY-CALL-KLI-CLM',
	'ARMY-CALL-KLI-COI_MANAGER',
	'ARMY-CALL-KLI-IMS-SME',
	'ARMY-CALL-KLI-MANAGER',
	'ARMY-CALL-KLI-TEAM',
	'ARMY-KLI-IMS-GATEKEEPER',
	'candrews',
	'catherine.hinners',
	'catherine.hinners2',
	'catherine.umberger',
	'CR-ADMIN',
	'CR-AUTH',
	'CR-CLM',
	'CR-MGR',
	'CR-SA',
	'dr.authorized.js',
	'dr.clm.js',
	'dr-admin-js',
	'dr-admin-navy',
	'dr-authorized-js',
	'dr-manager-js',
	'DR-SA-2',
	'DR-TEAM-JS',
	'emily.bronte',
	'franz.haydn',
	'music.boy',
	'pt1032',
	'qa.airman',
	'QA.CA.ADMIN',
	'QA.CA.Admin.2',
	'QA.CA.AUTH',
	'QA.CA.MGR',
	'QA.CACLM',
	'qa.user.test.andrew',
	'ragmanadmin',
	'ragmanauth',
	'ragmanclm',
	'ragmanmgr',
	'ragmanteam',
	'USAF-HAF-ADMIN',
	'USAF-HAF-AUTHORIZED',
	'USAF-HAF-CLM',
	'USAF-HAF-MANAGER',
	'USAF-HAF-MANAGER2',
	'USAF-HAF-TEAM'
	)
GO
	
DELETE TU	
FROM dbo.TierUser TU
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.JLLISUser JU
	WHERE JU.JLLISUserID = TU.JLLISUserID
	)
GO
	
DELETE P	
FROM PasswordSecurity.Password P
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.JLLISUser JU
	WHERE JU.JLLISUserID = P.JLLISUserID
	)
GO

DELETE PR
FROM PasswordSecurity.PasswordRemnant PR
WHERE NOT EXISTS
	(
	SELECT 1
	FROM PasswordSecurity.Password P
	WHERE P.PasswordID = PR.PasswordID
	)
GO

DELETE SA
FROM PasswordSecurity.SecurityAnswer SA
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.JLLISUser JU
	WHERE JU.JLLISUserID = SA.JLLISUserID
	)
GO

INSERT INTO dbo.JLLISUser
	(jllisusername,originatingtierid,usertype,password,firstname,lastname,email,sipremail,dayphone,nightphone,cellphone,tollfreephone,iridiumphone,expertise,mos,dob,datereported,bloodtype,anthrax,pistolserial,gasmasksize,fitrepdue,fitreprs,fitrepro,company,unit,zip,homepage,city,state,country,shipemail,fax,address,tempint,autoresponderbcc,notes,showhelp,paygrade,title,deploymentcity,deploymentregion,deploymentprovince,deploymentcountry,deploymentplace,TaskTrackerToken,defaultcompany,defaultproject,multicompany,deletetasks,deleteevents,defaultview,admin,defaultfont,defaultcolor,allowedcompanies,defaultsort,department,account,bulkmail,mpassword,pkiforce,confirmation,accessid,passwordquestion,passwordanswer,email2,email3,FailedLoginAttempts,FailedLoginDate,IsAccountLocked,IsPasswordResetRequired,CountryID)
SELECT
	TJU.jllisusername,
	TJU.originatingtierid,
	TJU.usertype,
	TJU.password,
	TJU.firstname,
	TJU.lastname,
	TJU.email,
	TJU.sipremail,
	TJU.dayphone,
	TJU.nightphone,
	TJU.cellphone,
	TJU.tollfreephone,
	TJU.iridiumphone,
	TJU.expertise,
	TJU.mos,
	TJU.dob,
	TJU.datereported,
	TJU.bloodtype,
	TJU.anthrax,
	TJU.pistolserial,
	TJU.gasmasksize,
	TJU.fitrepdue,
	TJU.fitreprs,
	TJU.fitrepro,
	TJU.company,
	TJU.unit,
	TJU.zip,
	TJU.homepage,
	TJU.city,
	TJU.state,
	TJU.country,
	TJU.shipemail,
	TJU.fax,
	TJU.address,
	TJU.tempint,
	TJU.autoresponderbcc,
	TJU.notes,
	TJU.showhelp,
	TJU.paygrade,
	TJU.title,
	TJU.deploymentcity,
	TJU.deploymentregion,
	TJU.deploymentprovince,
	TJU.deploymentcountry,
	TJU.deploymentplace,
	TJU.TaskTrackerToken,
	TJU.defaultcompany,
	TJU.defaultproject,
	TJU.multicompany,
	TJU.deletetasks,
	TJU.deleteevents,
	TJU.defaultview,
	TJU.admin,
	TJU.defaultfont,
	TJU.defaultcolor,
	TJU.allowedcompanies,
	TJU.defaultsort,
	TJU.department,
	TJU.account,
	TJU.bulkmail,
	TJU.mpassword,
	TJU.pkiforce,
	TJU.confirmation,
	TJU.accessid,
	TJU.passwordquestion,
	TJU.passwordanswer,
	TJU.email2,
	TJU.email3,
	TJU.FailedLoginAttempts,
	TJU.FailedLoginDate,
	TJU.IsAccountLocked,
	TJU.IsPasswordResetRequired,
	TJU.CountryID
FROM #tJLLISUser TJU
GO

DECLARE @nMaxTierUserID INT = (SELECT MAX(TU.TierUserID) FROM dbo.TierUser TU)

INSERT INTO dbo.TierUser
	(TierUserID,jllisuserid,tierid,seclev,st1,msc,startdate,activedate,status,search1,search2,search3,search4,search5,search6,search7,search8,search9,search10,defaulttierid)
SELECT 
	TTU.TierUserID + @nMaxTierUserID,
	JU.JLLISUserID,
	TTU.tierid,
	TTU.seclev,
	TTU.st1,
	TTU.msc,
	TTU.startdate,
	TTU.activedate,
	TTU.status,
	TTU.search1,
	TTU.search2,
	TTU.search3,
	TTU.search4,
	TTU.search5,
	TTU.search6,
	TTU.search7,
	TTU.search8,
	TTU.search9,
	TTU.search10,
	TTU.defaulttierid
FROM #tTierUser TTU
	JOIN #tJLLISUser TJU ON TJU.JLLISUserID = TTU.JLLISUserID
	JOIN dbo.JLLISUser JU ON JU.JLLISUserName = TJU.JLLISUserName
GO

INSERT INTO PasswordSecurity.Password
	(JLLISUserID,PasswordHash,PasswordSalt,CreationDate,IsActive)
SELECT
	JU.JLLISUserID,
	TP.PasswordHash,
	TP.PasswordSalt,
	TP.CreationDate,
	TP.IsActive	
FROM #tPassword TP
	JOIN #tJLLISUser TJU ON TJU.JLLISUserID = TP.JLLISUserID
	JOIN dbo.JLLISUser JU ON JU.JLLISUserName = TJU.JLLISUserName	
GO

INSERT INTO PasswordSecurity.SecurityAnswer
	(JLLISUserID,PasswordSecurityQuestionID,SecurityAnswer)
SELECT
	JU.JLLISUserID,
	TSA.PasswordSecurityQuestionID,
	TSA.SecurityAnswer
FROM #tSecurityAnswer TSA
	JOIN #tJLLISUser TJU ON TJU.JLLISUserID = TSA.JLLISUserID
	JOIN dbo.JLLISUser JU ON JU.JLLISUserName = TJU.JLLISUserName	
GO

ALTER TABLE dbo.tieruser WITH NOCHECK ADD CONSTRAINT FK_TierUser_JLLISUser FOREIGN KEY(jllisuserid) REFERENCES dbo.jllisuser (jllisuserid)
GO
ALTER TABLE dbo.tieruser CHECK CONSTRAINT FK_TierUser_JLLISUser
GO

ALTER TABLE dbo.tieruser WITH NOCHECK ADD CONSTRAINT FK_TierUser_Tier FOREIGN KEY(tierid) REFERENCES dbo.tier (tierid)
GO
ALTER TABLE dbo.tieruser CHECK CONSTRAINT FK_TierUser_Tier
GO
