USE SOCOM
GO

SELECT 
	MDU.Email,
	MD.Name
FROM dbo.MetadataUser MDU
	JOIN dbo.Metadata MD ON MD.MDID = MDU.MDID
		AND MDU.Email IS NOT NULL
ORDER BY MDU.Email, MD.Name
		
		
	