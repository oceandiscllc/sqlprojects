USE ARMY
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ArmyCallData'

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	BEGIN
	
	DROP TABLE dbo.ArmyCallData
	
	END
--ENDIF

CREATE TABLE dbo.ArmyCallData
	(
	ArmyCallDataID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	node_id varchar(50),
	Security_Classification varchar(50),
	Supplemental_Security_Marking varchar(50),
	Title_Subject varchar(250),
	Doc_Creation_Date varchar(50),
	Author_Originator varchar(50),
	file_name nvarchar(255),
	TableName varchar(250)
	)
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, 'ArmyCallDataID', 'int'
EXEC Utility.SetDefault @cTableName, 'ArmyCallDataID', 0
EXEC Utility.SetColumnNotNull @cTableName, 'ArmyCallDataID', 'int'
GO

DELETE 
FROM dbo.CDR 
WHERE ImportSource = 'ARMYCall'
GO

DELETE CF
FROM dbo.CDRFile CF
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.CDR C
	WHERE C.CDRID = CF.CDRID
	)
GO

USE ARMYCall
GO

DECLARE @cSchemaNameTableName varchar(50)
DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT S.Name + '.' + O.Name AS SchemaNameTableName
	FROM sys.objects O WITH (NOLOCK)
		JOIN sys.schemas S ON S.schema_ID = O.schema_ID
			AND O.type = 'U'
	ORDER BY S.Name + '.' + O.Name

OPEN oCursor
FETCH oCursor INTO @cSchemaNameTableName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'INSERT INTO ARMY.dbo.ArmyCallData '
	SET @cSQL = @cSQL + '(node_id,Security_Classification,Supplemental_Security_Marking,Title_Subject,Doc_Creation_Date,Author_Originator,file_name,TableName) '
	SET @cSQL = @cSQL + 'SELECT '
	SET @cSQL = @cSQL + 'T.node_id,'
	SET @cSQL = @cSQL + 'LEFT(T.Security_Classification, 50),'
	SET @cSQL = @cSQL + 'LEFT(T.Supplemental_Security_Marking, 50),'
	SET @cSQL = @cSQL + 'LEFT(T.Title_Subject, 250),'
	SET @cSQL = @cSQL + 'Doc_Creation_Date,'
	SET @cSQL = @cSQL + 'LEFT(T.Author_Originator, 50),'
	SET @cSQL = @cSQL + 'T.file_name,'
	SET @cSQL = @cSQL + '''' + @cSchemaNameTableName + ''' '
	SET @cSQL = @cSQL + 'FROM ' + @cSchemaNameTableName + ' T '
	SET @cSQL = @cSQL + 'ORDER BY T.node_id'
	
	EXEC(@cSQL)

	FETCH oCursor into @cSchemaNameTableName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

USE ARMY
GO

DECLARE @cCDRType varchar(250)
DECLARE @nCDRTypeID int
DECLARE @nOriginatingTierID int

SET @cCDRType = 'Historical Documents'

IF NOT EXISTS (SELECT 1 FROM Dropdown.CDRType C WHERE C.CDRType = @cCDRType)
	BEGIN
	
	INSERT INTO Dropdown.CDRType
		(CDRType,OriginatingTierID)
	SELECT
		@cCDRType,
		T.TierID
	FROM JLLIS.dbo.Tier T
	WHERE T.TierName = 'ARMY'
		AND T.IsInstance = 1
		
	END
--ENDIF

SELECT @nCDRTypeID = CDRTypeID
FROM Dropdown.CDRType C 
WHERE C.CDRType = @cCDRType

SELECT @nOriginatingTierID = T.TierID
FROM JLLIS.dbo.Tier T
WHERE T.TierName = 'ARMY'
	AND T.IsInstance = 1

INSERT INTO dbo.CDR
	(ArmyCallDataID,status,creationdate,updatedate,classification,overallCaveat,releasableto,title,lastname,docdate,jointcdr,originatingtierid,CDRTypeID,ImportCode,ImportSource)
SELECT
	ACD.ArmyCallDataID,
	'Active',
	getDate(),
	getDate(),
	ACD.Security_Classification,
	'ARMY Call',
	ACD.Supplemental_Security_Marking,
	ACD.Title_Subject,
	ACD.Author_Originator,
	
	CASE
		WHEN ISDATE(ACD.Doc_Creation_Date) = 0
		THEN getDate()
		ELSE CAST(ACD.Doc_Creation_Date as datetime)
	END,
	
	'Y',
	@nOriginatingTierID,
	@nCDRTypeID,
	ACD.node_id,
	'ARMYCall'
FROM dbo.ArmyCallData ACD
GO

INSERT INTO dbo.CDRFile
	(CDRID,Status,Filename)
SELECT
	C.CDRID,
	'Active',
	ACD.file_name
FROM dbo.ArmyCallData ACD
	JOIN dbo.CDR C ON C.ArmyCallDataID = ACD.ArmyCallDataID
GO

ALTER TABLE dbo.CDR DROP CONSTRAINT DF_CDR_ArmyCallDataID
ALTER TABLE dbo.CDR DROP COLUMN ArmyCallDataID
GO

--DROP TABLE dbo.ArmyCallData
GO

--DROP DATABASE ARMYCall
GO