DECLARE @oTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ListItem VARCHAR(MAX))

INSERT INTO @oTable
	(ListItem)
SELECT 
	i.value('.', 'VARCHAR(MAX)') AS ListItem
FROM 
	(
	SELECT 
		CONVERT
			(
			XML, 
			'<root><i>' + 
			REPLACE('1,2,3,4,5,6,7,8,9,10,1', ',', '</i><i>') 
			+ '</i></root>') AS XMLText
			) D
		CROSS APPLY D.XMLText.nodes('//root/i') AS RECORDS(i)

SELECT *
FROM @oTable