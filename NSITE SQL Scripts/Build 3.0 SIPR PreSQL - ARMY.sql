USE ARMY
GO

DECLARE @nClassificationDataID int
DECLARE @nCDRFileID int
DECLARE @nCDRClassificationDataID int

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		CF.CDRFileID,
		C.CDRClassificationDataID
	FROM dbo.CDRFile CF
		JOIN dbo.CDR C ON C.CDRID = CF.CDRID
			AND C.CDRClassificationDataID > 0
			AND CF.CDRFileClassificationDataID = 0

OPEN oCursor
FETCH oCursor INTO @nCDRFileID, @nCDRClassificationDataID
WHILE @@fetch_status = 0
	BEGIN

	BEGIN TRANSACTION

	INSERT INTO JLLIS.dbo.ClassificationData
		(ClassificationID,CaveatID,ClassificationSource,ClassificationAuthority,IsCompilation,DeclassificationDate)
	SELECT
		CD.ClassificationID,
		CD.CaveatID,
		CD.ClassificationSource,
		CD.ClassificationAuthority,
		CD.IsCompilation,
		CD.DeclassificationDate
	FROM JLLIS.dbo.ClassificationData CD
	WHERE CD.ClassificationDataID = @nCDRClassificationDataID

	SELECT @nClassificationDataID = SCOPE_IDENTITY()
		
	INSERT INTO JLLIS.dbo.ClassificationDataClassificationReason
		(ClassificationDataID,ClassificationReasonID)
	SELECT 
		@nClassificationDataID,
		CDCR.ClassificationReasonID
	FROM JLLIS.dbo.ClassificationDataClassificationReason CDCR
	WHERE CDCR.ClassificationDataID = @nCDRClassificationDataID
		
	INSERT INTO JLLIS.dbo.ClassificationDataCoalition
		(ClassificationDataID,CoalitionID)
	SELECT 
		@nClassificationDataID,
		CDC.CoalitionID
	FROM JLLIS.dbo.ClassificationDataCoalition CDC
	WHERE CDC.ClassificationDataID = @nCDRClassificationDataID
		
	INSERT INTO JLLIS.dbo.ClassificationDataCountry
		(ClassificationDataID,CountryID)
	SELECT 
		@nClassificationDataID,
		CDC.CountryID
	FROM JLLIS.dbo.ClassificationDataCountry CDC
	WHERE CDC.ClassificationDataID = @nCDRClassificationDataID
		
	UPDATE dbo.CDRFile
	SET CDRFileClassificationDataID = @nClassificationDataID
	WHERE CDRFileID = @nCDRFileID

	COMMIT TRANSACTION
	
	FETCH oCursor INTO @nCDRFileID, @nCDRClassificationDataID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
