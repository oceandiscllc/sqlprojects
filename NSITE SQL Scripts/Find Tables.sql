SELECT
	'JLLIS' AS DatabaseName,
	S.Name AS SchemaName,
	O.Name AS TableName
FROM JLLIS.sys.Objects O
	JOIN JLLIS.sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.type = 'U' 
		AND O.Name Like '%Metadata%'

UNION

SELECT
	'JSCC' AS DatabaseName,
	S.Name AS SchemaName,
	O.Name AS TableName
FROM JSCC.sys.Objects O
	JOIN JSCC.sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.type = 'U' 
		AND O.Name Like '%Metadata%'
ORDER BY DatabaseName, S.Name, O.Name
