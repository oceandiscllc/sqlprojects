USE JLLIS
GO

--Begin procedure Utility.GetLastSQLBuildLog
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Utility.GetLastSQLBuildLog') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Utility.GetLastSQLBuildLog
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.04.23
-- Description:	A stored procedure to return the last SQL Build entry for each database
-- ===============================================================================================
CREATE PROCEDURE Utility.GetLastSQLBuildLog

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDatabaseName varchar(50)
	DECLARE @cSpace varchar(32)
	DECLARE @cSQL varchar(max)

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable1', 'u')) IS NOT NULL
		DROP TABLE #tTable1

	CREATE TABLE #tTable1
		(
		DatabaseName varchar(50)
		)

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable2', 'u')) IS NOT NULL
		DROP TABLE #tTable2

	CREATE TABLE #tTable2
		(
		DatabaseName varchar(50),
		BuildKey varchar(250),
		RunDate varchar(20)
		)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT UPPER(DB.Name) 
		FROM sys.Databases DB
		WHERE DB.Name IN ('ACGU','ARMY','CCO','CFMCCLL','DISA','DLA','DOS','DTRA','HPRC','JLLIS','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
		ORDER BY DB.Name
		
	OPEN oCursor
	FETCH oCursor INTO @cDatabaseName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cSQL = 'IF EXISTS (SELECT 1 FROM ' + @cDatabaseName + '.sys.Objects O JOIN ' + @cDatabaseName + '.sys.Schemas S ON S.Schema_ID = O.Schema_ID AND O.type IN (''U'') AND S.Name = ''Utility'' AND O.Name = ''Setup'') INSERT INTO #tTable1 (DatabaseName) VALUES (''' + @cDatabaseName + ''')'
		EXEC(@cSQL)

		FETCH oCursor into @cDatabaseName

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor	

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT DatabaseName
		FROM #tTable1 T1

	OPEN oCursor
	FETCH oCursor INTO @cDatabaseName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cSQL = 'INSERT INTO #tTable2 (DatabaseName,BuildKey,RunDate) SELECT TOP 1 ''' + @cDatabaseName + ''',S.SetupValue,CONVERT(char(20), S.CreateDate, 113) FROM ' + @cDatabaseName + '.Utility.Setup S WHERE S.SetupKey = ''BuildKey'' ORDER BY S.SetupID DESC'
		EXEC(@cSQL)

		FETCH oCursor into @cDatabaseName

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor	

	SELECT
		T2.DatabaseName AS [Database Name],
		T2.BuildKey AS [Build Key],
		LEFT(T2.BuildKey, CHARINDEX('-', T2.BuildKey, 0) - 1) AS [Build Name],
		SUBSTRING(LTRIM(RIGHT(T2.BuildKey, LEN(T2.BuildKey) - CHARINDEX('-', T2.BuildKey, 0) - 1)), 9, 2) + ' ' + 
		LEFT(DATENAME(month, DATEADD(month, CAST(SUBSTRING(LTRIM(RIGHT(T2.BuildKey, LEN(T2.BuildKey) - CHARINDEX('-', T2.BuildKey, 0) - 1)), 6, 2) AS int), 0) - 1), 3) + ' ' + 
		LEFT(LTRIM(RIGHT(T2.BuildKey, LEN(T2.BuildKey) - CHARINDEX('-', T2.BuildKey, 0) - 1)), 4) + ' ' + 
		RIGHT(LTRIM(RIGHT(T2.BuildKey, LEN(T2.BuildKey) - CHARINDEX('-', T2.BuildKey, 0) - 1)), 8) AS [Build Date Time],
		T2.RunDate AS [Run Date Time]
	FROM #tTable2 T2

	DROP TABLE #tTable2
	DROP TABLE #tTable1
END
