USE ARMY
GO

SELECT
	C.CDRID,
	C.Title, 
	C.Status,
	C.Classification, 
	C.ReleasableTo, 
	C.OverallCaveat AS Caveat
FROM dbo.CDR C
WHERE C.Classification LIKE '%NATO%'
	OR C.ReleasableTo LIKE '%NATO%'
	OR C.OverallCaveat LIKE '%NATO%'
