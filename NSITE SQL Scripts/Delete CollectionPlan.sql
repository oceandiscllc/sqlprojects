-- This script will delete a collection plan and its dependencies
-- The <@nCollectionPlanID> is the ID of the Collection Plan to be deleted

USE JLLIS
GO

DECLARE @cSQL VARCHAR(MAX)
DECLARE @nCollectionPlanID INT = <@nCollectionPlanID>

IF (SELECT OBJECT_ID('tempdb.dbo.#tClassificationData', 'u')) IS NOT NULL
	DROP TABLE #tClassificationData
--ENDIF

CREATE TABLE #tClassificationData (ClassificationDataID INT)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'INSERT INTO #tClassificationData (ClassificationDataID) SELECT ' + C.Name + ' FROM ' + S.Name + '.' + T.Name + ' CP WHERE CP.CollectionPlanID = ' + CAST(@nCollectionPlanID AS VARCHAR(10)) AS SQLText
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.schema_ID = T.schema_ID
		JOIN sys.Columns C ON C.Object_ID = T.Object_ID
			AND T.Name = 'CollectionPlan'
			AND S.Name = 'dbo'
			AND C.Name LIKE '%ClassificationDataID'
  
OPEN oCursor
FETCH oCursor INTO @cSQL
  
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQL)

	FETCH oCursor INTO @cSQL
	END
--END WHILE
  
CLOSE oCursor
DEALLOCATE oCursor    

INSERT INTO #tClassificationData 
	(ClassificationDataID) 
SELECT 
	EL.EntityLocationClassificationDataID 
FROM dbo.EntityLocation EL 
WHERE EL.EntityTypeCode = 'CollectionPlan' 
	AND EL.EntityID = @nCollectionPlanID

DELETE CD
FROM dbo.ClassificationData CD
	JOIN #tClassificationData TCD ON TCD.ClassificationDataID = CD.ClassificationDataID

DELETE T FROM dbo.ClassificationDataClassificationReason T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)
DELETE T FROM dbo.ClassificationDataCoalition T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)
DELETE T FROM dbo.ClassificationDataCountry T WHERE NOT EXISTS (SELECT 1 FROM dbo.ClassificationData CD WHERE CD.ClassificationDataID = T.ClassificationDataID)

DELETE T FROM dbo.CollectionPlan T WHERE T.CollectionPlanID = @nCollectionPlanID
DELETE T FROM dbo.CollectionPlanCountry T WHERE NOT EXISTS (SELECT 1 FROM dbo.CollectionPlan CP WHERE CP.CollectionPlanID = T.CollectionPlanID)

DELETE E FROM dbo.EntityFavorite E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityJLLISFile E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityLink E WHERE E.EntityTypeCode1 = 'CollectionPlan' AND E.EntityID1 = @nCollectionPlanID
DELETE E FROM dbo.EntityLink E WHERE E.EntityTypeCode2 = 'CollectionPlan' AND E.EntityID2 = @nCollectionPlanID
DELETE E FROM dbo.EntityLocation E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityMember E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityMetadata E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityMilestone E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityObjective E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityOrganization E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityPublication E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntitySME E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntitySubscription E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntitySystemMetadata E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID
DELETE E FROM dbo.EntityTask E WHERE E.EntityTypeCode = 'CollectionPlan' AND E.EntityID = @nCollectionPlanID

DELETE ELDO FROM dbo.EntityLinkDisplayOrder ELDO WHERE NOT EXISTS (SELECT 1 FROM dbo.EntityLink EL WHERE EL.EntityLinkID = ELDO.EntityLinkID)

DROP TABLE #tClassificationData
GO
