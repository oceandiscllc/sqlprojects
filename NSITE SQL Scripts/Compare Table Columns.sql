SELECT 
	'ALTER TABLE dbo.AAR ALTER COLUMN ' + T.NOMIName + ' varchar(max)'
FROM 
	(
	SELECT TOP 100 PERCENT
		C1.Name AS USSOCOMName,
		C1.Max_Length AS USSOCOMLength,
		T1.Name AS USSOCOMType,
		C2.Name AS NOMIName,
		C2.Max_Length AS NOMILength,
		T2.Name AS NOMIType
	FROM USSOCOM.sys.columns C1 WITH (NOLOCK) 
		JOIN USSOCOM.sys.objects O1 WITH (NOLOCK) ON O1.object_id = C1.object_id 
			AND O1.Name = 'AAR' 
			AND O1.Type = 'U'
		JOIN sys.types T1 ON C1.user_type_id = T1.user_type_id
		JOIN NOMI.sys.columns C2 WITH (NOLOCK) ON C2.Name = C1.Name
		JOIN NOMI.sys.objects O2 WITH (NOLOCK) ON O2.object_id = C2.object_id 
			AND O2.Name = 'AAR' 
			AND O2.Type = 'U'		
		JOIN sys.types T2 ON C2.user_type_id = T2.user_type_id
			AND
				(
				C1.Max_Length <> C2.Max_Length
					OR T1.Name <> T2.Name
				)
	ORDER BY C1.Name		
	) T

	