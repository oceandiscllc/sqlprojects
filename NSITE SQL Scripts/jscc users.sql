DECLARE @cTierName VARCHAR(250)
SET @cTierName = 'USAF'

SELECT 
	JU.JLLISUserID,
	JU.JLLISUserName,
	JU.LastName,
	JU.FirstName,
	JU.Email,
	JU.DayPhone,
	JU.PayGrade, 
	JU.Title, 	
	T2.TierLabel AS MemberOrganization,
	T3.TierLabel AS MemberHomePage
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = TU.TierID
		AND T1.TierName = @cTierName
		AND TU.Status = 'Active'
		AND 
			(
			JU.OriginatingTierID = T1.TierID
				OR TU.DefaultTierID = T1.TierID
			)
	LEFT JOIN JLLIS.dbo.Tier T2 ON T2.TierID = JU.OriginatingTierID
	LEFT JOIN JLLIS.dbo.Tier T3 ON T3.TierID = TU.DefaultTierID
GO