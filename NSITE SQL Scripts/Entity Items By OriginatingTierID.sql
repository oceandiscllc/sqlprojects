SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @nOrigiatingTierID INT = 25
DECLARE @cSQL VARCHAR(MAX)

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
  DROP TABLE #tTable
--ENDIF

CREATE TABLE #tTable 
	(
	TableName VARCHAR(100), 
	PrimaryKey INT
	)

IF (SELECT DB_NAME()) = 'JLLIS'
	BEGIN
	
	INSERT INTO #tTable 
		(TableName,PrimaryKey) 
	SELECT 
		'dbo.TierUser',
		TU.TierUserID
	FROM dbo.TierUser TU
	WHERE TierID = 
		CAST((
		SELECT SS.ServerSetupValue
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'TierUserInstanceID'
		) AS INT)
		AND TU.DefaultTierID = @nOrigiatingTierID

	END
--ENDIF

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'INSERT INTO #tTable (TableName,PrimaryKey) SELECT ''' + S.Name + '.' + O.Name + ''',' + 
			COL_NAME(IC.OBJECT_ID, IC.column_id) + 
			' FROM ' + S.Name + '.' + O.Name + 
			' WHERE OriginatingTierID = ' + CAST(@nOrigiatingTierID AS VARCHAR(10))
	FROM sys.indexes I
		JOIN sys.index_columns IC ON I.OBJECT_ID = IC.OBJECT_ID
			AND I.index_id = IC.index_id
			AND I.is_primary_key = 1
		JOIN sys.objects O ON O.OBJECT_ID = I.OBJECT_ID
		JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID
		JOIN sys.columns C ON O.Object_ID = C.Object_ID
			AND O.Type = 'U' 
			AND C.Name = 'OriginatingTierID'
			AND S.Name NOT IN ('Dropdown','Staging')
	ORDER BY S.Name, O.Name

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXEC(@cSQL)
	FETCH oCursor INTO @cSQL
	
	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

SELECT
	ROW_NUMBER() OVER (ORDER BY T.TableName, T.PrimaryKey) AS LineNumber,
	ROW_NUMBER() OVER (PARTITION BY T.TableName ORDER BY T.TableName, T.PrimaryKey) AS RowIndex,
	@nOrigiatingTierID AS OrigiatingTierID,
	T.TableName,
	T.PrimaryKey
FROM #tTable T
ORDER BY T.TableName, T.PrimaryKey

DROP TABLE #tTable
