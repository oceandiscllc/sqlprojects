SELECT 
	US.DropdownEntityID,
	US.JLLISUserID,
	JLLIS.Utility.GetPersonNameByJLLISUserID(US.JLLISUserID, 'LastFirstTitle') AS JLLISUser,
	MD.Metadata
FROM JSCC.Dropdown.UserSubscription US
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = US.DropdownEntityID
		AND EXISTS
			(
			SELECT 1
			FROM JSCC.Dropdown.Metadata MD
				JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
					AND MDT.MetadataType LIKE 'DOTMLPF%'
					AND MD.MetadataID = US.DropdownEntityID
					AND US.DropdownMetadataCode = 'Metadata'
			)
