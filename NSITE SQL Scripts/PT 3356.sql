--Begin PT 3356
USE JLLIS
GO

IF Utility.GetServerSetupValueByServerSetupKey('NetworkName') = 'SIPR'
	BEGIN
	
	DELETE FROM dbo.IssueLMS WHERE LMSID IN (292424,292425,293097)
	
	DELETE T FROM COP.SectionItem T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityFavorite T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityJLLISFile T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	
	DELETE T FROM dbo.EntityLink T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode1) AND T.EntityID1 IN (292424,292425,293097)
	DELETE T FROM dbo.EntityLink T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode2) AND T.EntityID2 IN (292424,292425,293097)
	DELETE T FROM dbo.EntityLinkDisplayOrder T WHERE NOT EXISTS (SELECT 1 FROM dbo.EntityLink EL WHERE EL.EntityLinkID = T.EntityLinkID)
	
	DELETE T FROM dbo.EntityLocation T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityMember T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityMetadata T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityMilestone T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityObjective T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityOrganization T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityPublication T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntitySME T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntitySubscription T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntitySystemMetadata T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityTask T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.JLLISFeaturedItem T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.LoginToken T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.SubscriptionNotification T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM Social.EntityComment T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM Social.EntityFlag T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM Social.EntityVote T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM Spatial.EntityLayer T WHERE EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)

	END
--ENDIF
GO

USE JSCC
GO

IF JLLIS.Utility.GetServerSetupValueByServerSetupKey('NetworkName') = 'SIPR'
	BEGIN
	
	DELETE FROM dbo.clt	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.f_users	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.lms	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.LMSAFDL	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.LMSAnalysisCode	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.LMSDiscuss WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.lmshistory WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.mobilelms	WHERE LMSID IN (292424,292425,293097)
	DELETE FROM dbo.sodarslms	WHERE LMSID IN (292424,292425,293097)

	DELETE T FROM dbo.Content T WHERE EXISTS (SELECT 1 FROM JLLIS.dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.EntityMembers T WHERE EXISTS (SELECT 1 FROM JLLIS.dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)
	DELETE T FROM dbo.TierEntities T WHERE EXISTS (SELECT 1 FROM JLLIS.dbo.EntityType ET WHERE ET.ActiveEntityTypeCode = 'Observation' AND ET.EntityTypeCode = T.EntityTypeCode) AND T.EntityID IN (292424,292425,293097)

	END
--ENDIF	
GO
--End PT 3356