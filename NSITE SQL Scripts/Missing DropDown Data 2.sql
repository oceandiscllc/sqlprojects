select *
from dropdown 
where category = 'ReleasableTo'

--INSERT INTO dbo.DropDown
--	(DisplayText,Description,Category,DisplayOrder,TierID)
SELECT 
	DisplayText,
	Description,
	Category,
	DisplayOrder,
	(SELECT T2.TierID FROM JLLIS.dbo.Tier T2 WITH (NOLOCK) WHERE T2.TierName = 'ARMYISR')
FROM dbo.DropDown DD WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T1 WITH (NOLOCK) ON T1.TierID = DD.TierID
		AND T1.TierName = 'ARMY'
		AND T1.IsInstance = 1
		AND DD.Category = 'ReleasableTo'