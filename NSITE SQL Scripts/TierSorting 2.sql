USE JLLIS
GO

DECLARE @nTierID int
SET @nTierID = (SELECT T.TierID FROM dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'TestTier');

WITH HD (DisplayOrder,TierLineage,TierName,TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(max), T.TierName),
		CAST(T.TierID as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		1 
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID

	UNION ALL 
	
	SELECT
		CONVERT(varchar(max), RTRIM(DisplayOrder) + ',' + T.TierName),
		CAST(HD.TierLineage + ',' + CAST(T.TierID as varchar(50)) as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)
	
SELECT 
	HD.TierLineage,
	HD.TierName,
	HD.TierID, 
	ABS(HD.NodeLevel) AS NodeLevel
FROM HD
ORDER BY HD.NodeLevel DESC
GO

DECLARE @nMasterTierID int
SET @nMasterTierID = (SELECT T.TierID FROM dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'NAVY');

WITH HD (DisplayOrder,TierLineage,TierName,TierID,ParentTierID,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(max), T.TierName),
		CAST(T.TierID as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		1 
	FROM dbo.Tier T WITH (NOLOCK) 
	WHERE T.ParentTierID = @nMasterTierID

	UNION ALL 
	
	SELECT
		CONVERT(varchar(max), RTRIM(HD.DisplayOrder) + ',' + T.TierName),
		CAST(HD.TierLineage + ',' + CAST(T.TierID as varchar(50)) as varchar(50)),
		T.TierName,
		T.TierID, 
		T.ParentTierID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
SELECT 
	HD.TierLineage,
	HD.TierName,
	HD.TierID, 
	ABS(HD.NodeLevel) AS NodeLevel
FROM HD
ORDER BY DisplayOrder