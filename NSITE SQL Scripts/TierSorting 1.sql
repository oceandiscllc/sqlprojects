DECLARE @cChildName varchar(50)
DECLARE @cDescription varchar(50)
DECLARE @cParentName varchar(50)

SET @cDescription = 'Todd Test'
SET @cParentName = 'MARSOC'

SET @cChildName = 'MARSOC Child 1'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Child 2'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Child 3'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Child 4'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Child 1'

SET @cChildName = 'MARSOC Grand Child 1A'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Grand Child 1B'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Child 2'

SET @cChildName = 'MARSOC Grand Child 2A'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Grand Child 2B'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Grand Child 2C'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Child 4'

SET @cChildName = 'MARSOC Grand Child 4A'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Grand Child 4B'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Grand Child 4C'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Grand Child 1A'

SET @cChildName = 'MARSOC Great Grand Child 1A1'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Great Grand Child 1A2'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Great Grand Child 1A3'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Grand Child 4A'

SET @cChildName = 'MARSOC Great Grand Child 4A1'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Grand Child 4C'

SET @cChildName = 'MARSOC Great Grand Child 4C1'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Great Grand Child 4C2'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC'

SET @cChildName = 'MARSOC Child 5'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cChildName = 'MARSOC Child 6'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Child 6'

SET @cChildName = 'MARSOC Grand Child 6A'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

SET @cParentName = 'MARSOC Child 5'

SET @cChildName = 'MARSOC Grand Child 5A'
INSERT INTO JLLIS.dbo.Tier (TierName,TierDescription,ParentTierID) SELECT @cChildName, @cDescription, T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cParentName AND NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cChildName)

--UPDATE JLLIS.dbo.Tier SET TierName = 'MARSOC Child 0' WHERE TierName = 'MARSOC Child 5'
--UPDATE JLLIS.dbo.Tier SET TierName = 'MARSOC Grand Child 0A' WHERE TierName = 'MARSOC Grand Child 5A'

--UPDATE JLLIS.dbo.Tier SET TierName = 'MARSOC Child 5' WHERE TierName = 'MARSOC Child 0'
--UPDATE JLLIS.dbo.Tier SET TierName = 'MARSOC Grand Child 5A' WHERE TierName = 'MARSOC Grand Child 0A'

SELECT * FROM JLLIS.dbo.Tier ORDER BY TierName
