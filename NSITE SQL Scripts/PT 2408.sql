--Begin PT 2408
USE JLLIS
GO

IF EXISTS (SELECT 1 FROM JLLIS.dbo.ServerSetup SS WHERE SS.ServerSetupKey = 'NetworkName' AND SS.ServerSetupValue NOT IN ('ACGU'))
	BEGIN

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
		DROP TABLE #tTable 
	--ENDIF

	DECLARE @tOutput TABLE (TierID INT)
	
	SELECT T1.* 
	INTO #tTable 
	FROM dbo.Tier T1
	WHERE 1 = 0
	
	INSERT #tTable 
		(autoemail,autoregister,canmanagecontent,canmanagedropdowns,CanManageLessonTabs,canmanagemenu,canmanagemetadata,canmanageopex,canmanagesetup,canmanagetabs,defaultrole,DisplayOrder,DropdownSearchMode,InheritParentPOC,instancedsn,instanceid,instancetype,isactive,IsAssignable,IsForNavigation,isinstance,islegacyinstance,MenuPosition,parenttierid,tierdescription,tierimagename,TierLabel,tiername) 
	SELECT TOP 1
		T.autoemail,
		T.autoregister,
		T.canmanagecontent,
		T.canmanagedropdowns,
		T.CanManageLessonTabs,
		T.canmanagemenu,
		T.canmanagemetadata,
		T.canmanageopex,
		T.canmanagesetup,
		T.canmanagetabs,
		T.defaultrole,
		T.DisplayOrder,
		T.DropdownSearchMode,
		T.InheritParentPOC,
		T.instancedsn,
		0,
		T.instancetype,
		T.isactive,
		T.IsAssignable,
		T.IsForNavigation,
		T.isinstance,
		T.islegacyinstance,
		T.MenuPosition,
		T.parenttierid,
		T.tierdescription,
		T.tierimagename,
		'PFPA',
		'Pentagon Forces Protection Agency'
	FROM dbo.Tier T
	WHERE T.IsActive = 1
		AND T.IsInstance = 1
		AND T.InstanceType = 'SILO'
	ORDER BY T.TierID

	MERGE dbo.Tier T1
	USING (SELECT T.* FROM #tTable T) T2
		ON T2.TierName = T1.TierName
	WHEN MATCHED THEN 
	UPDATE 
		SET 
			T1.autoemail = T2.autoemail,
			T1.autoregister = T2.autoregister,
			T1.canmanagecontent = T2.canmanagecontent,
			T1.canmanagedropdowns = T2.canmanagedropdowns,
			T1.CanManageLessonTabs = T2.CanManageLessonTabs,
			T1.canmanagemenu = T2.canmanagemenu,
			T1.canmanagemetadata = T2.canmanagemetadata,
			T1.canmanageopex = T2.canmanageopex,
			T1.canmanagesetup = T2.canmanagesetup,
			T1.canmanagetabs = T2.canmanagetabs,
			T1.defaultrole = T2.defaultrole,
			T1.DisplayOrder = T2.DisplayOrder,
			T1.DropdownSearchMode = T2.DropdownSearchMode,
			T1.InheritParentPOC = T2.InheritParentPOC,
			T1.instancedsn = T2.instancedsn,
			T1.instancetype = T2.instancetype,
			T1.isactive = T2.isactive,
			T1.IsAssignable = T2.IsAssignable,
			T1.IsForNavigation = T2.IsForNavigation,
			T1.isinstance = T2.isinstance,
			T1.MenuPosition = T2.MenuPosition,
			T1.TierLabel = T2.TierLabel,
			T1.tiername = T2.tiername
	WHEN NOT MATCHED THEN
	INSERT
		(AutoEmail,AutoRegister,CanManageContent,CanManageDropdowns,CanManageLessonTabs,CanManageMenu,CanManageMetadata,CanManageOpEx,CanManageSetup,CanManageTabs,DefaultRole,DisplayOrder,DropdownSearchMode,InheritParentPOC,instancedsn,InstanceType,IsActive,IsAssignable,IsForNavigation,IsInstance,IsLegacyInstance,MenuPosition,ParentTierID,TierLabel,TierName)
	VALUES
		(
		T2.autoemail,
		T2.autoregister,
		T2.canmanagecontent,
		T2.canmanagedropdowns,
		T2.CanManageLessonTabs,
		T2.canmanagemenu,
		T2.canmanagemetadata,
		T2.canmanageopex,
		T2.canmanagesetup,
		T2.canmanagetabs,
		T2.defaultrole,
		T2.DisplayOrder,
		T2.DropdownSearchMode,
		T2.InheritParentPOC,
		T2.instancedsn,
		T2.instancetype,
		T2.isactive,
		T2.IsAssignable,
		T2.IsForNavigation,
		T2.isinstance,
		T2.islegacyinstance,
		T2.MenuPosition,
		T2.parenttierid,
		T2.TierLabel,
		T2.tiername
		)
	OUTPUT INSERTED.TierID INTO @tOutput
	;

	IF EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		UPDATE T
		SET T.InstanceID = O.TierID
		FROM dbo.Tier T
			JOIN @tOutput O ON O.TierID = T.TierID

		END
	--ENDIF
	
	DROP TABLE #tTable

	END
--ENDIF
GO
--End PT 2408
