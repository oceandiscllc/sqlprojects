SELECT 
	L.LMSID,
	
	CASE
		WHEN L.BackgroundClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID)
	END AS CurrentBackgroundClassification,
	
	CASE
		WHEN L.BackgroundClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID)
	END AS CurrentBackgroundCaveat,
	
	CASE
		WHEN L.BackgroundClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID)
	END AS CurrentBackgroundReleasableTo,
	
	L.backgroundClass AS LegacyBackgroundClassification,
	L.discussioncaveat AS LegacyBackgroundCaveat,
	L.backgroundrelto AS LegacyBackgroundReleasableTo,
	
	CASE
		WHEN L.EventDescriptionClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID)
	END AS CurrentEventDescriptionClassification,
	
	CASE
		WHEN L.EventDescriptionClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID)
	END AS CurrentEventDescriptionCaveat,
	
	CASE
		WHEN L.EventDescriptionClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID)
	END AS CurrentEventDescriptionReleasableTo,

	L.eventdescriptionClass AS LegacyEventDescriptionClassification,
	L.eventcaveat AS LegacyEventDescriptionCaveat,
	L.eventdescriptionrelto AS LegacyEventDescriptionReleasableTo,
	
	CASE
		WHEN L.ImplicationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID)
	END AS CurrentImplicationsClassification,
	
	CASE
		WHEN L.ImplicationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID)
	END AS CurrentImplicationsCaveat,
	
	CASE
		WHEN L.ImplicationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID)
	END AS CurrentImplicationsReleasableTo,
	
	L.implicationsClass AS LegacyImplicationsClassification,
	L.implicationcaveat AS LegacyImplicationsCaveat,
	L.implicationsrelto AS LegacyImplicationsReleasableTo,

	CASE
		WHEN L.LMSClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID)
	END AS CurrentLMSClassification,
	
	CASE
		WHEN L.LMSClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID)
	END AS CurrentLMSCaveat,
	
	CASE
		WHEN L.LMSClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID)
	END AS CurrentLMSReleasableTo,

	L.classification AS LegacyLMSClassification,
	L.overallcaveat AS LegacyLMSCaveat,
	L.releasableto AS LegacyLMSReleasableTo,

	CASE
		WHEN L.ObservationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID)
	END AS CurrentObservationsClassification,
	
	CASE
		WHEN L.ObservationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID)
	END AS CurrentObservationsCaveat,
	
	CASE
		WHEN L.ObservationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID)
	END AS CurrentObservationsReleasableTo,

	L.observationsClass AS LegacyObservationsClassification,
	L.observationcaveat AS LegacyObservationsCaveat,
	L.observationsrelto AS LegacyObservationsReleasableTo,

	CASE
		WHEN L.RecommendationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID)
	END AS CurrentRecommendationsClassification,
	

	CASE
		WHEN L.RecommendationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID)
	END AS CurrentRecommendationsCaveat,
	
	CASE
		WHEN L.RecommendationsClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID)
	END AS CurrentRecommendationsReleasableTo,

	L.recommendationsClass AS LegacyRecommendationsClassification,
	L.recommendationcaveat AS LegacyRecommendationsCaveat,
	L.recommendationsrelto AS LegacyRecommendationsReleasableTo,

	CASE
		WHEN L.SummaryClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID)
	END AS CurrentSummaryClassification,
	
	CASE
		WHEN L.SummaryClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID)
	END AS CurrentSummaryCaveat,
	
	CASE
		WHEN L.SummaryClassificationDataID = 0
		THEN 'None'
		ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID)
	END AS CurrentSummaryReleasableTo,
	
	L.summaryClass AS LegacySummaryClassification,
	L.commentcaveat AS LegacySummaryCaveat,
	L.summaryrelto AS LegacySummaryReleasableTo

FROM dbo.LMS L
