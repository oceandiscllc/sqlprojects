DELETE D
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY EntityTypeCode, EntityID, SystemMetadataID ORDER BY EntityID) AS RowIndex
	FROM JLLIS.dbo.EntitySystemMetadata
	) D
WHERE D.RowIndex > 1