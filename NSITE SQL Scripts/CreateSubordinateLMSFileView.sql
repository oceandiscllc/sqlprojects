IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Utility.CreateSubordinateLMSFileView') AND type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CreateSubordinateLMSFileView
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2011.11.09
-- Description:	a stored procedure to drop and create the various LMSFileViews
--							for subordiante tiers
-- ===========================================================================
CREATE PROCEDURE Utility.CreateSubordinateLMSFileView
	@cTierName varchar(250),
	@nPrintSQL bit

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)
	DECLARE @cSQL4 varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cViewName varchar(250)

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)

	SET @cViewName = 'dbo.LMSFileView_' + @cTierName

	SET @cSQL1 = 'IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID(''' + @cViewName + ''')) DROP VIEW ' + @cViewName
	
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		print @cSQL1
	ELSE
		EXEC (@cSQL1)
	--ENDIF

	DECLARE @oTable1 table (RowID int identity(1,1) primary key, SQLText varchar(max))
	DECLARE @oTable2 table (RowID int identity(1,1) primary key, SQLText varchar(max))

	INSERT INTO @oTable1 (SQLText) VALUES ('CREATE VIEW ' + @cViewName + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES ('AS' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES ('WITH HD (TierID,ParentTierID)' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'AS' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + '(' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'SELECT' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.TierID,' )
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'T.ParentTierID' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + 'WHERE T.TierID =' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + '(' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT T1.TierID' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T1 WITH(NOLOCK)' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN JLLIS.dbo.Tier T2 WITH(NOLOCK) ON T2.TierID = T1.ParentTierID' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T1.TierName = ''' + 'Africom' + '''' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.TierName = ''JSCC''' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab4 + 'AND T2.IsInstance = 1' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + ')' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'UNION ALL' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'SELECT' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.TierID,' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'T.ParentTierID' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab2 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab3 + 'JOIN HD ON HD.TierID = T.ParentTierID' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cTab1 + ')' + @cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES (@cCRLF)
	INSERT INTO @oTable1 (SQLText) VALUES ('SELECT' + @cCRLF)

	INSERT INTO @oTable2 exec sp_helptext 'dbo.LMSFileView'

	DELETE
	FROM @oTable2
	WHERE RowID < 
		(
		SELECT TOP 1 RowID
		FROM @oTable2
		WHERE SQLText LIKE '%,%'
		ORDER BY RowID
		)

	UPDATE @oTable2
	SET SQLText = REPLACE(SQLText, '    ', @cTab1)

	INSERT INTO @oTable2 (SQLText) VALUES (@cTab2 + 'AND EXISTS' + @cCRLF)
	INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + '(' + @cCRLF)
	INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'SELECT 1' + @cCRLF)
	INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'FROM HD' + @cCRLF)
	INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + 'WHERE HD.TierID = L.OriginatingTierID' + @cCRLF)
	INSERT INTO @oTable2 (SQLText) VALUES (@cTab3 + ')' + @cCRLF)

	INSERT INTO @oTable1 (SQLText) SELECT T2.SQLText FROM @oTable2 T2

	SET @cSQL1 = ''
	SET @cSQL2 = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SQLText
		FROM @oTable1
		ORDER BY RowID

	OPEN oCursor
	FETCH oCursor into @cSQL2
	WHILE @@fetch_status = 0
		BEGIN
		
		SET @cSQL1 = @cSQL1 + @cSQL2
		
		FETCH oCursor into @cSQL2
			
		END
	--END WHILE
			
	CLOSE oCursor
	DEALLOCATE oCursor
		
	IF @nPrintSQL IS NOT NULL AND @nPrintSQL = 1
		print @cSQL1
	ELSE
		EXEC (@cSQL1)
	--ENDIF
END

GO