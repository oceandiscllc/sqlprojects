DECLARE @cServerSetupKey VARCHAR(MAX) = ''
DECLARE @cSQL VARCHAR(MAX) = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		'' AS ServerSetupKey,
		'TRUNCATE TABLE JLLIS.dbo.ServerSetup' AS SQLText

	UNION

	SELECT TOP 100 PERCENT
		SS.ServerSetupKey,
		'INSERT INTO JLLIS.dbo.ServerSetup (ServerSetupKey,ServerSetupValue) VALUES (''' + SS.ServerSetupKey + ''',''' + SS.ServerSetupValue + ''')' AS SQLText
	FROM JLLIS.dbo.ServerSetup SS
	ORDER BY ServerSetupKey

OPEN oCursor
FETCH oCursor INTO @cServerSetupKey, @cSQL
WHILE @@fetch_status = 0
	BEGIN
	
	--EXEC (@cSQL)
	print @cSQL
	FETCH oCursor INTO @cServerSetupKey, @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
