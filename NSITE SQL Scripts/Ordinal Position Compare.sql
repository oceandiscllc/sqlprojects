WITH CD AS 
	(
	SELECT 
		T.Table_Schema AS SchemaName,
		T.Table_Name AS ArchiveTableName,
		REPLACE(T.Table_Name, '_Archive', '') AS TableName
	FROM INFORMATION_SCHEMA.TABLES T
	WHERE T.Table_Name LIKE '%_Archive'
	)

SELECT
	CD.SchemaName,
	CD.TableName,
	CD.ArchiveTableName,
	C1.Column_Name,
	C1.Ordinal_Position,
	C2.Ordinal_Position
FROM CD
	JOIN INFORMATION_SCHEMA.COLUMNS C1 ON C1.Table_Schema = CD.SchemaName
		AND C1.Table_Name = CD.TableName
	JOIN INFORMATION_SCHEMA.COLUMNS C2 ON C2.Table_Schema = CD.SchemaName
		AND C2.Table_Name = CD.ArchiveTableName
		AND C2.Column_Name = C1.Column_Name
WHERE C1.Ordinal_Position <> C2.Ordinal_Position
ORDER BY 1, 2, 4, 5, 6




