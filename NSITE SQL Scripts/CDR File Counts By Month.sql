DECLARE @cDateStart VARCHAR(10)
DECLARE @cDateStop VARCHAR(10)

SET @cDateStart = '09/01/2012'
SET @cDateStop = '10/01/2012'

SELECT
	D.ItemCount,
	D.CDRID,
	CAST(MONTH(C.CreationDate) AS VARCHAR(20)) + '/' + CAST(YEAR(C.CreationDate) AS VARCHAR(20)) AS MonthYear,
	C.Title,
	T.TierLabel
FROM
	(
	SELECT 
		COUNT(EJF.EntityID) AS ItemCount,
		C.CDRID
	FROM JLLIS.dbo.EntityJLLISFile EJF
		JOIN dbo.CDR C ON C.CDRID = EJF.EntityID
			AND EJF.EntityTypeCode = 'CDR'
			AND C.Status = 'Active'
			AND C.CreationDate BETWEEN @cDateStart AND @cDateStop
	GROUP BY C.CDRID
	) D
	JOIN dbo.CDR C ON C.CDRID = D.CDRID
	JOIN JLLIS.dbo.Tier T ON T.TierID = C.OriginatingTierID
		