USE ARMY
GO

DELETE 
FROM dbo.Content
WHERE TierID IN ()

INSERT INTO dbo.Content
	(Condition,Description,DisplayOrder,Image,ImageCaption,ImageHeight,ImageWidth,Link,MainText,NewWindow,Status,Tease,TierID,Title,Type)
SELECT
	C.Condition,
	C.Description,
	C.DisplayOrder,
	C.Image,
	C.ImageCaption,
	C.ImageHeight,
	C.ImageWidth,
	C.Link,
	C.MainText,
	C.NewWindow,
	C.Status,
	C.Tease,
	T.TierID,
	C.Title,
	C.Type
FROM dbo.Content C WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.InstanceID = C.TierID
		AND T.TierID IN ()
GO

DELETE 
FROM dbo.DropDown
WHERE TierID IN ()

INSERT INTO dbo.DropDown
	(Category,Description,DisplayOrder,DisplayText,Email,EmailNotification,TierID)
SELECT
	DD.Category,
	DD.Description,
	DD.DisplayOrder,
	DD.DisplayText,
	DD.Email,
	DD.EmailNotification,
	T.TierID
FROM dbo.DropDown DD WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.InstanceID = DD.TierID
		AND T.TierID IN ()
		AND DD.Category NOT LIKE '%_Old'
GO

DELETE 
FROM dbo.MetaData
WHERE TierID IN ()

INSERT INTO dbo.MetaData
	(AAR,Abbrev,Description,IsActive,Modified,Name,Sequence,TierID,Type)
SELECT
	MD.AAR,
	MD.Abbrev,
	MD.Description,
	MD.IsActive,
	MD.Modified,
	MD.Name,
	MD.Sequence,
	T.TierID,
	MD.Type
FROM dbo.MetaData MD WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.InstanceID = MD.TierID
		AND T.TierID IN ()
GO

DELETE 
FROM dbo.OpEx
WHERE TierID IN ()

INSERT INTO dbo.OpEx
	(EndDate,Location,OpExName,OpExType,Sponsor,StartDate,TierID)
SELECT
	OX.EndDate,
	OX.Location,
	OX.OpExName,
	OX.OpExType,
	OX.Sponsor,
	OX.StartDate,
	T.TierID
FROM dbo.OpEx OX WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.InstanceID = OX.TierID
		AND T.TierID IN ()
GO

DELETE 
FROM dbo.Setup
WHERE TierID IN ()

INSERT INTO dbo.Setup
	(approvalfrom,bbbcc,bbduebcc,bbduefrom,bboverbcc,bboverdue,bbtechbcc,bgcolor,bgdarkbar,calendar,candc,contactphone,d,dailyactivityreportto,dailydigestfrom,defaultparagraphclass,displaycount,dns,documentrepositorypath,dodotmlpffrom,emailus,erroremail,f,feedbackprimarysupport,feedbacksecondarysupport,feedbackto,fires,fixedWingDivision,forceprotection,forces,functionalareasfrom,intelInfoSystems,intelligence,l,lmsclmfrom,lmsfrom,loginattemptto,logistics,m,maneuver,maritimeRotaryDivision,medical,mscdir,networktype,newuserto,o,p,rfiprimarysupport,rfisecondarysupport,rfito,row1color,row2color,rowover,safety,securestartingurl,site,sitename,sopaarsto,specialprograms,startingurl,t,TierID,ttbcc,ttcalendar,unit,webmasteremail,weeklyrollup)
SELECT
	S.approvalfrom,
	S.bbbcc,
	S.bbduebcc,
	S.bbduefrom,
	S.bboverbcc,
	S.bboverdue,
	S.bbtechbcc,
	S.bgcolor,
	S.bgdarkbar,
	S.calendar,
	S.candc,
	S.contactphone,
	S.d,
	S.dailyactivityreportto,
	S.dailydigestfrom,
	S.defaultparagraphclass,
	S.displaycount,
	S.dns,
	S.documentrepositorypath,
	S.dodotmlpffrom,
	S.emailus,
	S.erroremail,
	S.f,
	S.feedbackprimarysupport,
	S.feedbacksecondarysupport,
	S.feedbackto,
	S.fires,
	S.fixedWingDivision,
	S.forceprotection,
	S.forces,
	S.functionalareasfrom,
	S.intelInfoSystems,
	S.intelligence,
	S.l,
	S.lmsclmfrom,
	S.lmsfrom,
	S.loginattemptto,
	S.logistics,
	S.m,
	S.maneuver,
	S.maritimeRotaryDivision,
	S.medical,
	S.mscdir,
	S.networktype,
	S.newuserto,
	S.o,
	S.p,
	S.rfiprimarysupport,
	S.rfisecondarysupport,
	S.rfito,
	S.row1color,
	S.row2color,
	S.rowover,
	S.safety,
	S.securestartingurl,
	S.site,
	S.sitename,
	S.sopaarsto,
	S.specialprograms,
	S.startingurl,
	S.t,
	T.TierID,
	S.ttbcc,
	S.ttcalendar,
	S.unit,
	S.webmasteremail,
	S.weeklyrollup
FROM dbo.Setup S WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.InstanceID = S.TierID
		AND T.TierID IN ()
GO