USE MASTER
GO

DECLARE @cCRLF varchar(2)
DECLARE @cDatabase varchar(50)
DECLARE @cDelimiter varchar(1)
DECLARE @cFileDate char(8)
DECLARE @cFileName varchar(500)
DECLARE @cFileExt varchar(5)
DECLARE @cFilePathBackup varchar(500)
DECLARE @cFilePathData varchar(500)
DECLARE @cSQL varchar(max)
DECLARE @nRowCount int
DECLARE @nRecordCount int
DECLARE @nSPID int

CREATE TABLE #oBackupFileList (DatabaseName varchar(50), FileDate char(8))

INSERT INTO #oBackupFileList
	(DatabaseName,FileDate)
SELECT 
	UPPER(D.Name),
	'20110729'	
FROM sys.databases D WITH (NOLOCK)
WHERE D.Name IN 
	(
	'ACGU',
	'ARMY',
	'CCO',
	'DISA',
	'DLA',
	'DOS',
	'DTRA',
	'HPRC',
	'JLLIS',
	'JSCC',
	--'JSOC',
	'NAVY',
	'NCCS',
	'NGA',
	'NGB',
	'NOMI',
	'ORCHID',
	'RPB',
	'USAF',
	'USSOCOM',
	'USUHS'
	)

SET @cCRLF = CHAR(13) + CHAR(10)
SET @cFilePathBackup = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\Backup\'
SET @cFilePathData = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\Data\'

CREATE TABLE #oBackupFileDetails
	(
	LogicalName nvarchar(128),
	PhysicalName nvarchar(128),
	Type char(1),
	FileGroupName nvarchar(128),
	Size numeric(20,0),
	MaxSize numeric(20,0),
	FileID bigint,
	CreateLSN numeric(25,0),
	DropLSN numeric(25,0) NULL,
	UniqueID uniqueidentifier,
	ReadOnlyLSN numeric(25,0),
	ReadWriteLSN numeric(25,0),
	BackupSizeInByte bigint,
	SourceBlockSize int,
	FilegroupID int,
	LogGroupGUID uniqueidentifier NULL,
	DifferentialBaseLSN numeric(25,0),
	DifferentialbaseGUID uniqueidentifier,
	IsReadOnly bit,
	IsPresent bit,
	TDEThumbprint varchar(50)
	)

DECLARE oCursor1 CURSOR FOR
	SELECT 
		DatabaseName,
		FileDate
	FROM #oBackupFileList
	ORDER BY DatabaseName

OPEN oCursor1
FETCH oCursor1 INTO @cDatabase, @cFileDate
WHILE @@fetch_status = 0
	BEGIN

	SET @nSPID = (SELECT MIN(spid) FROM Master.dbo.sysprocesses where dbid = db_id(@cDatabase))

	WHILE @nSPID IS NOT NULL
		BEGIN

		EXECUTE ('KILL ' + @nSPID)

		SET @nSPID = (SELECT MIN(spid) FROM Master.dbo.sysprocesses where dbid = db_id(@cDatabase) AND SPID > @nSPID)

		END
	--END WHILE

	SET @cSQL = 'RESTORE FILELISTONLY FROM DISK = ''' + @cFilePathBackup + @cDatabase + '_' + @cFileDate + '.bak'''

	TRUNCATE TABLE #oBackupFileDetails
	INSERT INTO #oBackupFileDetails
	EXEC (@cSQL)

	SET @nRecordCount = (SELECT COUNT(*) FROM #oBackupFileDetails)
	SET @nRowCount = 0

	SET @cSQL = 'ALTER DATABASE ' + @cDatabase + ' SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
	EXEC (@cSQL)

	SET @cSQL = 'RESTORE DATABASE ' + @cDatabase
	SET @cSQL = @cSQL + @cCRLF + 'FROM DISK = ''' + @cFilePathBackup + @cDatabase + '_' + @cFileDate + '.bak'''
	SET @cSQL = @cSQL + @cCRLF + 'WITH REPLACE,'

	DECLARE oCursor2 CURSOR FOR
		SELECT 
			LogicalName,
			LOWER(RIGHT(PhysicalName, 3))
		FROM #oBackupFileDetails
		ORDER BY Type
		
	OPEN oCursor2
	FETCH oCursor2 INTO @cFileName, @cFileExt
	WHILE @@fetch_status = 0
		BEGIN

		SET @cSQL = @cSQL + @cCRLF + 'MOVE ''' + @cFileName + ''' TO ''' + @cFilePathData + @cDatabase + '.' + @cFileExt + ''''
		SET @nRowCount = @nRowCount + 1

		IF (@nRowCount < @nRecordCount)
			SET @cSQL = @cSQL + ','
		--ENDIF
		
		FETCH oCursor2 INTO @cFileName, @cFileExt
		
		END
	--END WHILE
		
	CLOSE oCursor2
	DEALLOCATE oCursor2

	EXEC (@cSQL)

	FETCH oCursor1 INTO @cDatabase, @cFileDate
	
	END
--END WHILE
	
CLOSE oCursor1
DEALLOCATE oCursor1

DROP TABLE #oBackupFileDetails
DROP TABLE #oBackupFileList