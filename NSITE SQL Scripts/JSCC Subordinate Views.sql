USE JSCC
GO

EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'africom', @nPrintSQL = 0
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'centcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'eucom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jcisfa', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jfcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jpra', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'js', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'jscc', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'northcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'pacom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'southcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'stratcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateCDRFileView @cTierName = 'transcom', @nPrintSQL = 0
GO

EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'africom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'centcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'eucom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jcisfa', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jfcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jpra', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'js', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'jscc', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'northcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'pacom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'southcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'stratcom', @nPrintSQL = 0 
EXEC Utility.CreateSubordinateLMSFileView @cTierName = 'transcom', @nPrintSQL = 0
GO