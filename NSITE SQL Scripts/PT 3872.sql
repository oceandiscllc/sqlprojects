USE JLLIS
GO

DELETE JU
FROM dbo.JLLISUser JU
	JOIN dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID
		AND TU.TierID = dbo.GetTierUserInstanceID()
		AND TU.TierUserID IN (122442, 122427)

DELETE TU
FROM dbo.TierUser TU
WHERE TU.TierID = dbo.GetTierUserInstanceID()
	AND TU.TierUserID IN (122442, 122427)
GO
