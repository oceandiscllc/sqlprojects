SELECT
	C.Name AS ColumnName,
	DC.Name AS ConstraintName,
	DC.Definition
FROM sys.default_constraints DC WITH (NOLOCK)
	JOIN sys.Columns C ON C.Column_ID = DC.Parent_Column_ID
		AND C.Object_ID = DC.parent_object_ID
		AND DC.parent_object_ID = OBJECT_ID('f_avatar')
ORDER BY C.name
