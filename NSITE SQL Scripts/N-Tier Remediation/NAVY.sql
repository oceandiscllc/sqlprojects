EXEC Utility.AddColumn 'dbo.PortVisit', 'ViewCount', 'int'
EXEC Utility.SetDefault 'dbo.PortVisit', 'ViewCount', '0'
EXEC Utility.SetColumnNotNull 'dbo.PortVisit', 'ViewCount', 'int'