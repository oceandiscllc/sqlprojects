UPDATE dbo.LMS
SET LMSUnit = MajorCommand_Old
WHERE MajorCommand_Old IS NOT NULL
	AND LEN(LTRIM(MajorCommand_Old)) > 0
GO

UPDATE dbo.LMS
SET OriginatingTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'NOMI' AND T.IsInstance = 1)
GO

INSERT INTO dbo.Unit
	(UnitName)
SELECT DISTINCT 
	L.MajorCommand_Old
FROM dbo.LMS L WITH (NOLOCK)
WHERE MajorCommand_Old IS NOT NULL
	AND LEN(LTRIM(MajorCommand_Old)) > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Unit U
		WHERE U.UnitName = L.MajorCommand_Old
		)
GO

UPDATE dbo.unit
SET ParentUnitID = (SELECT UnitID FROM dbo.unit WHERE UnitName = 'NOMI')
WHERE UnitName <> 'NOMI'
GO

DECLARE @cMetadata varchar(50)
DECLARE @cMetadataType varchar(50)
DECLARE @nOriginatingTierID int

SET @nOriginatingTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'NOMI' AND IsInstance = 1)

SET @cMetadataType = 'DOTMLPF'
IF NOT EXISTS (SELECT 1 FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = @cMetadataType)
	INSERT INTO Dropdown.MetadataType (MetadataType,OriginatingTierID) VALUES (@cMetadataType,@nOriginatingTierID)
--ENDIF

SET @cMetadataType = 'MEDICAL FUNCTIONAL AREAS'
IF NOT EXISTS (SELECT 1 FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = @cMetadataType)
	INSERT INTO Dropdown.MetadataType (MetadataType,OriginatingTierID) VALUES (@cMetadataType,@nOriginatingTierID)
--ENDIF

INSERT INTO Dropdown.Metadata
	(MetadataTypeID,Metadata,Description,OriginatingTierID)
SELECT
	(
	SELECT MDT.MetadataTypeID
	FROM Dropdown.MetadataType MDT WITH (NOLOCK)
	WHERE MDT.MetadataType = 'DOTMLPF'
	),
	MD1.Metadata,
	MD1.Description,
	@nOriginatingTierID
FROM ARMY.Dropdown.Metadata MD1 WITH (NOLOCK)
WHERE MD1.Metadata IN ('DOCTRINE','FACILITIES','LEADERSHIP/EDUCATION','MATERIAL','ORGANIZATION','PERSONNEL','TRAINING')
	AND NOT EXISTS
		(
		SELECT 1
		FROM Dropdown.Metadata MD2
		WHERE MD2.Metadata = MD1.Metadata
		)

SET @cMetadata = 'ADMINISTRATION'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'ANCILLARY CLINICAL SERVICES'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'COMMAND AND CONTROL'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'COMMUNICATION'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'FORCE HEALTH PROTECTION'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'INTELLIGENCE'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'LOGISTICS'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'MEDICAL REGULATING'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)

SET @cMetadata = 'PATIENT CARE/TREATMENT'
INSERT INTO Dropdown.Metadata (MetadataTypeID,Metadata,OriginatingTierID) SELECT MDT.MetadataTypeID,@cMetadata,@nOriginatingTierID FROM Dropdown.MetadataType MDT WITH (NOLOCK) WHERE MDT.MetadataType = 'MEDICAL FUNCTIONAL AREAS' AND NOT EXISTS (SELECT 1 FROM Dropdown.Metadata MD WHERE MD.Metadata = @cMetadata)
GO

INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.isdoctrine= 'Y' AND MD.Metadata = 'DOCTRINE' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.isfacilities= 'Y' AND MD.Metadata = 'FACILITIES' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.isleadershipeducation= 'Y' AND MD.Metadata = 'LEADERSHIP/EDUCATION' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismaterial= 'Y' AND MD.Metadata = 'MATERIAL' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedadmin= 'Y' AND MD.Metadata = 'ADMINISTRATION' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedcandc= 'Y' AND MD.Metadata = 'COMMAND AND CONTROL' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedclinservices= 'Y' AND MD.Metadata = 'ANCILLARY CLINICAL SERVICES' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedcomm= 'Y' AND MD.Metadata = 'COMMUNICATION' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedforchlthprotection= 'Y' AND MD.Metadata = 'FORCE HEALTH PROTECTION' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedintel= 'Y' AND MD.Metadata = 'INTELLIGENCE' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedlogistics= 'Y' AND MD.Metadata = 'LOGISTICS' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedpatientcare= 'Y' AND MD.Metadata = 'PATIENT CARE/TREATMENT' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ismedregulating= 'Y' AND MD.Metadata = 'MEDICAL REGULATING' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.isorganization= 'Y' AND MD.Metadata = 'ORGANIZATION' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.ispersonnel= 'Y' AND MD.Metadata = 'PERSONNEL' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
INSERT INTO dbo.LMSMetadata (LMSID,MetadataID) SELECT L.LMSID,MD.MetaDataID FROM dbo.LMS L WITH (NOLOCK), Dropdown.Metadata MD WITH (NOLOCK) WHERE L.istraining= 'Y' AND MD.Metadata = 'TRAINING' AND NOT EXISTS (SELECT 1 FROM dbo.LMSMetadata LMD WHERE LMD.LMSID = L.LMSID AND LMD.MetaDataID = MD.MetaDataID)
GO

DECLARE @nDropdownMetadataID int
SET @nDropdownMetadataID = (SELECT DMD.DropdownMetadataID FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'MetadataType')

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	L.OriginatingTierID,
	@nDropdownMetadataID,
	MDT.MetadataTypeID
FROM dbo.LMSMetadata LMD WITH (NOLOCK)
	JOIN dbo.LMS L WITH (NOLOCK) ON L.LMSID = LMD.LMSID
	JOIN Dropdown.Metadata MD WITH (NOLOCK) ON MD.MetadataID = LMD.MetadataID
	JOIN Dropdown.MetadataType MDT WITH (NOLOCK) ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.DropdownEntityID = MDT.MetadataTypeID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.TierID = L.OriginatingTierID
			)
ORDER BY MDT.MetadataTypeID, L.OriginatingTierID
GO

DECLARE @nDropdownMetadataID int
SET @nDropdownMetadataID = (SELECT DMD.DropdownMetadataID FROM JLLIS.Dropdown.DropdownMetadata DMD WITH (NOLOCK) WHERE DMD.DropdownMetadataCode = 'Metadata')

INSERT INTO Dropdown.TierDropdown
	(TierID,DropdownMetadataID,DropdownEntityID)
SELECT DISTINCT
	L.OriginatingTierID,
	@nDropdownMetadataID,
	MD.MetadataID
FROM dbo.LMSMetadata LMD WITH (NOLOCK)
	JOIN dbo.LMS L WITH (NOLOCK) ON L.LMSID = LMD.LMSID
	JOIN Dropdown.Metadata MD WITH (NOLOCK) ON MD.MetadataID = LMD.MetadataID
		AND NOT EXISTS
			(
			SELECT 1
			FROM Dropdown.TierDropdown TD
			WHERE TD.DropdownEntityID = MD.MetadataID
				AND TD.DropdownMetadataID = @nDropdownMetadataID
				AND TD.TierID = L.OriginatingTierID
			)
ORDER BY MD.MetadataID, L.OriginatingTierID
GO

DECLARE @cColumnName varchar(50)
DECLARE @cSchemaTableName varchar(100)
DECLARE @cSQL varchar(max)

DECLARE @nTierID int

SET @nTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'NOMI' AND IsInstance = 1)

DELETE 
FROM Dropdown.TierDropdown 
WHERE TierID <> @nTierID

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		S.name + '.' + O.name,
		C.name
	FROM sys.objects O WITH (NOLOCK)
		JOIN sys.schemas S ON S.schema_ID = O.schema_ID
		JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
		JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
	WHERE O.type = 'U' 
		AND 
			(
			C.Name IN ('OriginatingTierID', 'TierID')
			)
	ORDER BY S.name, O.name, C.name
	
OPEN oCursor
FETCH oCursor INTO @cSchemaTableName, @cColumnName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'UPDATE ' + @cSchemaTableName + ' SET ' + @cColumnName + ' = ' + CAST(@nTierID as varchar(10))
	EXEC (@cSQL)

	FETCH oCursor INTO @cSchemaTableName, @cColumnName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

DELETE 
FROM JLLIS.dbo.Tier 
WHERE InstanceID = @nTierID
	AND IsInstance <> 1

DELETE TDMD 
FROM JLLIS.Dropdown.TierDropdownMetadata TDMD 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM JLLIS.dbo.Tier T 
	WHERE T.TierID = TDMD.TierID
	)