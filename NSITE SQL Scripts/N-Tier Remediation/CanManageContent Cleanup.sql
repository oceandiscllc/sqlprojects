DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

DECLARE @cCRLF varchar(2)
DECLARE @cTab1 varchar(1)
DECLARE @cTab2 varchar(2)

SET @cCRLF = CHAR(13) + CHAR(10)
SET @cTab1 = CHAR(9)
SET @cTab2 = REPLICATE(CHAR(9), 2)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT UPPER(DB.Name)		
	FROM sys.Databases DB WITH (NOLOCK) 
		JOIN JLLIS.dbo.Tier T ON T.TierName = UPPER(DB.Name)
			AND T.IsInstance = 1
			AND T.IsLegacyInstance = 0
	WHERE DB.Name IN ('ACGU','ARMY','CCO','CFMCCLL','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'UPDATE T'
	SET @cSQL = @cSQL + @cCRLF + 'SET T.CanManageContent = 1'
	SET @cSQL = @cSQL + @cCRLF + 'FROM JLLIS.dbo.Tier T'
	SET @cSQL = @cSQL + @cCRLF + 'WHERE EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM ' + @cDatabaseName + '.dbo.Content C'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'WHERE C.Type = ''Subordinate Org'''
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND C.TierID = T.TierID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'


	EXEC (@cSQL)
	--print @cSQL
	--print @cCRLF
				
	FETCH oCursor into @cDatabaseName
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor
			

