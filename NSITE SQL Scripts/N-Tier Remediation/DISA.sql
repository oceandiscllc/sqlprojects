DECLARE @cColumnName varchar(50)
DECLARE @cSchemaTableName varchar(100)
DECLARE @cSQL varchar(max)

DECLARE @nTierID int

SET @nTierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'DISA' AND IsInstance = 1)

DELETE 
FROM Dropdown.TierDropdown 
WHERE TierID <> @nTierID

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		S.name + '.' + O.name,
		C.name
	FROM sys.objects O WITH (NOLOCK)
		JOIN sys.schemas S ON S.schema_ID = O.schema_ID
		JOIN sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id
		JOIN sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id
	WHERE O.type = 'U' 
		AND 
			(
			C.Name IN ('OriginatingTierID', 'TierID')
			)
	ORDER BY S.name, O.name, C.name
	
OPEN oCursor
FETCH oCursor INTO @cSchemaTableName, @cColumnName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'UPDATE ' + @cSchemaTableName + ' SET ' + @cColumnName + ' = ' + CAST(@nTierID as varchar(10))
	EXEC (@cSQL)

	FETCH oCursor INTO @cSchemaTableName, @cColumnName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

DELETE 
FROM JLLIS.dbo.Tier 
WHERE InstanceID = @nTierID
	AND IsInstance <> 1

DELETE TDMD 
FROM JLLIS.Dropdown.TierDropdownMetadata TDMD 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM JLLIS.dbo.Tier T 
	WHERE T.TierID = TDMD.TierID
	)