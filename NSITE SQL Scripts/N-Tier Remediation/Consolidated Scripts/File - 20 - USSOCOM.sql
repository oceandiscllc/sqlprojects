USE USSOCOM
GO

--Begin file Common.sql
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.CDRFileView'))
	DROP VIEW dbo.CDRFileView
GO

CREATE VIEW dbo.CDRFileView
AS
SELECT
	C1.CDRID,
	C1.Category,
	C1.City,
	C1.Classification,
	C1.Condition,
	C1.Countryid,
	C1.Creationdate,
	C1.CreatorQualifier,
	C1.DeclassifyOn,
	C1.Dissemination,
	C1.DocCountryDropDownID,
	C1.Docdate,
	C1.EMail,
	C1.FirstName + ' ' + C1.LastName AS AuthorName,
	C1.FirstName,
	'C' AS JointCDR,
	C1.Keywords,
	C1.LastName,
	C1.OriginatorCountryID,
	C1.OverallCaveat,
	C1.Phone,
	C1.Place,
	C1.Province,
	C1.ReleasableTo,
	C1.Status,
	C1.Subtitle,
	C1.Summary,
	C1.ThumbHeight,
	C1.Thumbnail,
	C1.ThumbWidth,
	C1.Title,
	C1.Type,
	C1.UpdateDate,
	C2.Country,
	CF.CDRFileID, 
	CF.CDRID AS CDRFile_CDRID, 
	CF.Condition AS CDRFile_Condition, 
	CF.FileName, 
	CF.Status AS CDRFile_Status,
	T.TierName AS Organization
FROM dbo.CDR C1 WITH (NOLOCK)
	JOIN dbo.CDRFile CF WITH (NOLOCK) ON CF.CDRID = C1.CDRID
		AND CF.Status = 'Active'
	JOIN JLLIS.Dropdown.Country C2 WITH (NOLOCK) ON C2.Countryid = C1.Countryid
	JOIN JLLIS.Dropdown.Status S WITH (NOLOCK) ON S.Status = C1.Status 
		AND C1.JointCDR IN ('C','Y')
		AND S.IsForJointSearch = 1
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = C1.OriginatingTierID
GO

IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.LMSFileView'))
	DROP VIEW dbo.LMSFileView
GO

CREATE VIEW dbo.LMSFileView
AS
SELECT
	C.Country,
	L.actionactive,
	L.activatedate,
	L.background,
	L.backgroundClass,
	L.backgroundrelto,
	L.battleboardids,
	L.bbid,
	L.campaign,
	L.category,
	L.classification,
	L.classifiedby,
	L.commentcaveat,
	L.condition,
	L.countryid,
	L.createdby,
	L.creationdate,
	L.dailydigest,
	L.dateofsource,
	L.dayphone,
	L.declassifiedon,
	L.derivedfrom,
	L.discussioncaveat,
	L.disposition,
	L.distributionechelon,
	L.dnsnumber,
	L.documentDate,
	L.email,
	L.environmentalattributes,
	L.eventcaveat,
	L.eventdate,
	L.eventdescription,
	L.eventdescriptionClass,
	L.eventdescriptionrelto,
	L.eventopex,
	L.eventwx,
	L.exemptedsource,
	L.exercise,
	L.externalid,
	L.firstname + ' ' + L.lastname AS authorname,
	L.firstname,
	L.fixedWingDivision,
	L.forces,
	L.formalactionrequired,
	L.implicationcaveat,
	L.implications,
	L.implicationsClass,
	L.implicationsrelto,
	L.intelInfoSystems,
	L.interimaction,
	L.iscandc,
	L.isdoctrine,
	L.isDraft,
	L.isfacilities,
	L.isfires,
	L.isforceprotection,
	L.isintelligence,
	L.isleadershipeducation,
	L.islogistics,
	L.ismaneuver,
	L.ismaterial,
	L.ismedical,
	L.isorganization,
	L.ispersonnel,
	L.issafety,
	L.istraining,
	'C' AS jointlesson,
	L.lastname,
	L.legacyid,
	L.lessontype,
	L.lmsid,
	L.lmsunit,
	L.maritimeRotaryDivision,
	L.mobileid,
	L.observationcaveat,
	L.observations,
	L.observationsClass,
	L.observationsrelto,
	L.operationalattributes,
	L.operationtype,
	L.overallcaveat,
	L.rank,
	L.recommendationcaveat,
	L.recommendations,
	L.recommendationsClass,
	L.recommendationsrelto,
	L.releasableto,
	L.ruc,
	L.specialprograms,
	L.status,
	L.summary,
	L.summaryClass,
	L.summaryrelto,
	L.taskforce,
	L.topic,
	L.unit,
	L.updatedate,
	L.updatedby,
	L.usereventname,
	LF.condition AS lmsfile_condition, 
	LF.filename, 
	LF.lmsfileid, 
	LF.lmsid AS lmsfile_lmsid, 
	LF.status AS lmsfile_status, 
	T.TierName AS majorcommand
FROM dbo.LMS L WITH (NOLOCK)
	LEFT JOIN dbo.LMSFile LF WITH (NOLOCK) ON LF.LMSID = L.LMSID
		AND LF.Status = 'Active'
	JOIN JLLIS.Dropdown.Country C WITH (NOLOCK) ON C.Countryid = L.Countryid
	JOIN JLLIS.Dropdown.Status S WITH (NOLOCK) ON S.Status = L.Status 
		AND L.JointLesson IN ('C','Y')
		AND S.IsForJointSearch = 1
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = L.OriginatingTierID
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDR'

EXEC Utility.AddColumn @cTableName, 'IsForReportLibrary', 'bit'
EXEC Utility.SetDefault @cTableName, 'IsForReportLibrary', 0
EXEC Utility.SetColumnNotNull @cTableName, 'IsForReportLibrary', 'bit'
GO

ALTER TABLE dbo.CDR ALTER COLUMN Title varchar(250)
GO
  
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.ReportToLibrary') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cSQL varchar(max)

	SET @cSQL = 'UPDATE C SET C.IsForReportLibrary = 1 FROM dbo.CDR C JOIN dbo.ReportToLibrary RTL ON RTL.RefID = C.CDRID AND RTL.Type = ''CDR'''
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO

UPDATE dbo.AAR
SET 
	Conclusion = REPLACE(CAST(Conclusion as varchar(max)), '&mdash;', '-'),
	Description = REPLACE(CAST(Description as varchar(max)), '&mdash;', '-')
GO

UPDATE dbo.AAR
SET 
	Conclusion = REPLACE(CAST(Conclusion as varchar(max)), '&ndash;', '-'),
	Description = REPLACE(CAST(Description as varchar(max)), '&ndash;', '-')
GO

UPDATE dbo.LMS
SET 
	Background = REPLACE(CAST(Background as varchar(max)), '&mdash;', '-'),
	EventDescription = REPLACE(CAST(EventDescription as varchar(max)), '&mdash;', '-'),
	Implications = REPLACE(CAST(Implications as varchar(max)), '&mdash;', '-'),
	Observations = REPLACE(CAST(Observations as varchar(max)), '&mdash;', '-'),
	Recommendations = REPLACE(CAST(Recommendations as varchar(max)), '&mdash;', '-'),
	Summary = REPLACE(CAST(Summary as varchar(max)), '&mdash;', '-')
GO

UPDATE dbo.LMS
SET 
	Background = REPLACE(CAST(Background as varchar(max)), '&ndash;', '-'),
	EventDescription = REPLACE(CAST(EventDescription as varchar(max)), '&ndash;', '-'),
	Implications = REPLACE(CAST(Implications as varchar(max)), '&ndash;', '-'),
	Observations = REPLACE(CAST(Observations as varchar(max)), '&ndash;', '-'),
	Recommendations = REPLACE(CAST(Recommendations as varchar(max)), '&ndash;', '-'),
	Summary = REPLACE(CAST(Summary as varchar(max)), '&ndash;', '-')
GO

UPDATE dbo.SSite
SET 
	Description = REPLACE(CAST(Description as varchar(max)), '&mdash;', '-')
GO

UPDATE dbo.SSite
SET 
	Description = REPLACE(CAST(Description as varchar(max)), '&ndash;', '-')
GO

UPDATE dbo.Binder
SET Title = '[Untitled Binder]'
WHERE Title IS NULL
	OR LEN(LTRIM(Title)) = 0
GO

EXEC Utility.SetDefault 'dbo.CDR', 'CDRCategoryID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'CDRCreatorQualifierID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'CDRTypeID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'CountryID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'CreationDate', 'getDate()'
EXEC Utility.SetDefault 'dbo.CDR', 'DocCountryDropDownID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'IsForReportLibrary', 0
EXEC Utility.SetDefault 'dbo.CDR', 'OriginatingTierID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'OriginatorCountryID', 0
EXEC Utility.SetDefault 'dbo.CDR', 'ThumbHeight', 0
EXEC Utility.SetDefault 'dbo.CDR', 'ThumbWidth', 0
EXEC Utility.SetDefault 'dbo.CDR', 'WarfareMissionID', 0
GO

EXEC Utility.SetColumnNotNull 'dbo.CDR', 'CDRCategoryID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'CDRCreatorQualifierID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'CDRTypeID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'CountryID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'DocCountryDropDownID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'IsForReportLibrary', 'bit'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'OriginatorCountryID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'ThumbHeight', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'ThumbWidth', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'WarfareMissionID', 'int'
GO

EXEC Utility.SetDefault 'dbo.LMS', 'CountryID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'EventID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonIssueID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonTrainingCourseID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonTypeID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'OriginatingTierID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'WarfareMissionID', 0
GO

EXEC Utility.SetColumnNotNull 'dbo.LMS', 'CountryID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'EventID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonIssueID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonTrainingCourseID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonTypeID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'WarfareMissionID', 'int'
GO

EXEC Utility.AddColumn 'dbo.styles', 'hoverRowColor', 'varchar(7)'
EXEC Utility.AddColumn 'dbo.styles', 'rowColor1', 'varchar(7)'
EXEC Utility.AddColumn 'dbo.styles', 'rowColor2', 'varchar(7)'
GO

EXEC Utility.SetDefault 'dbo.styles', 'hoverRowColor', '#E1E1E1'
EXEC Utility.SetDefault 'dbo.styles', 'rowColor1', '#FFFFFF'
EXEC Utility.SetDefault 'dbo.styles', 'rowColor2', '#EBEBEB'
GO

EXEC Utility.SetColumnNotNull 'dbo.styles', 'hoverRowColor', 'varchar(7)'
EXEC Utility.SetColumnNotNull 'dbo.styles', 'rowColor1', 'varchar(7)'
EXEC Utility.SetColumnNotNull 'dbo.styles', 'rowColor2', 'varchar(7)'
GO

UPDATE dbo.MenuItem
SET Link = 'admin/menu/'
WHERE Link = 'index.cfm?disp=menu_admin.cfm'
GO

DELETE 
FROM dbo.bbfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.binderfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.cdrfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.cltfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.lmsdiscussfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.lmsfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.ssitefile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.ttfile 
WHERE Status = 'Deleted' 
GO

DELETE 
FROM dbo.ttpfile 
WHERE Status = 'Deleted'
GO

EXEC Utility.AddColumn 'dbo.AAR', 'ViewCount', 'int'
EXEC Utility.AddColumn 'dbo.Binder', 'ViewCount', 'int'
EXEC Utility.AddColumn 'dbo.CDR', 'ViewCount', 'int'
EXEC Utility.AddColumn 'dbo.LMS', 'ViewCount', 'int'
EXEC Utility.AddColumn 'dbo.SSite', 'ViewCount', 'int'
GO

EXEC Utility.SetDefault 'dbo.AAR', 'ViewCount', '0'
EXEC Utility.SetDefault 'dbo.Binder', 'ViewCount', '0'
EXEC Utility.SetDefault 'dbo.CDR', 'ViewCount', '0'
EXEC Utility.SetDefault 'dbo.LMS', 'ViewCount', '0'
EXEC Utility.SetDefault 'dbo.SSite', 'ViewCount', '0'
GO

EXEC Utility.SetColumnNotNull 'dbo.AAR', 'ViewCount', 'int'
EXEC Utility.SetColumnNotNull 'dbo.Binder', 'ViewCount', 'int'
EXEC Utility.SetColumnNotNull 'dbo.CDR', 'ViewCount', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'ViewCount', 'int'
EXEC Utility.SetColumnNotNull 'dbo.SSite', 'ViewCount', 'int'
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable
--ENDIF

CREATE TABLE #oTable 
	(
	ViewCount int NOT NULL DEFAULT 0,
	EntityID int NOT NULL DEFAULT 0,
	TableName varchar(50)
	)
GO

INSERT INTO #oTable
	(ViewCount,EntityID,TableName)
SELECT
	COUNT(REVERSE(LEFT(LTRIM(REVERSE(EL.Descrip)), CHARINDEX(' ', LTRIM(REVERSE(EL.Descrip)))))),
	CAST(REVERSE(LEFT(LTRIM(REVERSE(EL.Descrip)), CHARINDEX(' ', LTRIM(REVERSE(EL.Descrip))))) as int),
	LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EL.Type, 'Edited', ''), 'Updated', ''), 'lesson', ''), 'Viewed', ''), 'Site', ''), 'COP', 'SSite'), 'COI', 'SSite'))
FROM dbo.EventLog EL WITH (NOLOCK)
WHERE ISNUMERIC(REVERSE(LEFT(LTRIM(REVERSE(EL.Descrip)), CHARINDEX(' ', LTRIM(REVERSE(EL.Descrip)))))) = 1
	AND EL.Type IN
		(
		'Edited AAR',
		'Edited Binder',
		'Edited CDR',
		'Updated LMS lesson',
		'Edited COI',
		'Edited COP',
		'Viewed Binder',
		'Viewed CDR',
		'Viewed COI Site',
		'Viewed COP Site',
		'Viewed LMS lesson'	
		)
GROUP BY LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EL.Type, 'Edited', ''), 'Updated', ''), 'lesson', ''), 'Viewed', ''), 'Site', ''), 'COP', 'SSite'), 'COI', 'SSite')), CAST(REVERSE(LEFT(LTRIM(REVERSE(EL.Descrip)), CHARINDEX(' ', LTRIM(REVERSE(EL.Descrip))))) as int)
ORDER BY LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EL.Type, 'Edited', ''), 'Updated', ''), 'lesson', ''), 'Viewed', ''), 'Site', ''), 'COP', 'SSite'), 'COI', 'SSite')), CAST(REVERSE(LEFT(LTRIM(REVERSE(EL.Descrip)), CHARINDEX(' ', LTRIM(REVERSE(EL.Descrip))))) as int)
GO

DECLARE @cSQL varchar(max)
DECLARE @cTableName varchar(50)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		LTRIM(RTRIM(T.TableName))
	FROM #oTable T
	ORDER BY LTRIM(RTRIM(T.TableName))

OPEN oCursor
FETCH oCursor into @cTableName
WHILE @@fetch_status = 0
	BEGIN
	
	SET @cSQL = 'UPDATE T1 SET T1.ViewCount = T2.ViewCount FROM dbo.' + @cTableName + ' T1 JOIN #oTable T2 ON T2.EntityID = T1.' + @cTableName + 'ID AND T2.TableName = ''' + @cTableName + ''''
	EXEC (@cSQL)
	
	FETCH oCursor into @cTableName
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor

IF (SELECT OBJECT_ID('tempdb.dbo.#oTable', 'u')) IS NOT NULL
  DROP TABLE #oTable
--ENDIF
--End file Common.sql

