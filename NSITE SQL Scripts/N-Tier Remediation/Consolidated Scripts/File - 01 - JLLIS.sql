USE JLLIS
GO

--Begin file JLLIS.sql
UPDATE dbo.Country
SET DisplayOrder = 

	CASE 
		WHEN CountryID = 0
		THEN 0
		WHEN ISOCode2 = 'US'
		THEN 1
		ELSE 2
	END

UPDATE Dropdown.Country
SET DisplayOrder = 

	CASE 
		WHEN CountryID = 0
		THEN 0
		WHEN ISOCode2 = 'US'
		THEN 1
		ELSE 2
	END

GO

--Begin table Dropdown.Status
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Dropdown.Status') AND O.type in (N'U')) 
	DROP TABLE Dropdown.Status
GO

CREATE TABLE Dropdown.Status
	(
	StatusID int IDENTITY(0,1) NOT NULL,
	Status varchar(250) NOT NULL,
	IsActive bit,
	IsForJointSearch bit
	)
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.Status'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'IsForJointSearch', 0
EXEC Utility.SetDefault @cTableName, 'Status', ''

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForJointSearch', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'Status', 'varchar(250)'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'StatusID'
GO

SET IDENTITY_INSERT Dropdown.Status ON
INSERT INTO Dropdown.Status (StatusID) VALUES (0)
SET IDENTITY_INSERT Dropdown.Status OFF
GO

INSERT INTO Dropdown.Status (Status) VALUES ('Active')
INSERT INTO Dropdown.Status (Status) VALUES ('Analysis')
INSERT INTO Dropdown.Status (Status) VALUES ('Canceled')
INSERT INTO Dropdown.Status (Status) VALUES ('Closed')
INSERT INTO Dropdown.Status (Status) VALUES ('Deleted')
INSERT INTO Dropdown.Status (Status) VALUES ('Draft')
INSERT INTO Dropdown.Status (Status) VALUES ('Evaluation')
INSERT INTO Dropdown.Status (Status) VALUES ('Hold')
INSERT INTO Dropdown.Status (Status) VALUES ('Integration')
INSERT INTO Dropdown.Status (Status) VALUES ('Lesson Learned')
INSERT INTO Dropdown.Status (Status) VALUES ('Monitor')
INSERT INTO Dropdown.Status (Status) VALUES ('Pending')
INSERT INTO Dropdown.Status (Status) VALUES ('Pending Mobile')
INSERT INTO Dropdown.Status (Status) VALUES ('Restricted')
INSERT INTO Dropdown.Status (Status) VALUES ('Validated')
INSERT INTO Dropdown.Status (Status) VALUES ('Xfer')
GO

UPDATE Dropdown.Status
SET IsForJointSearch = 1
WHERE Status IN ('Active','Closed','Validated')
GO
--End table Dropdown.Status

--Begin table Dropdown.EntityTypeStatus
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Dropdown.EntityTypeStatus') AND O.type in (N'U')) 
	DROP TABLE Dropdown.EntityTypeStatus
GO

CREATE TABLE Dropdown.EntityTypeStatus
	(
	EntityTypeStatusID int IDENTITY(1,1) NOT NULL,
	EntityTypeCode varchar(50),
	StatusID int,
	DisplayOrder int
	)
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.EntityTypeStatus'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'StatusID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EntityTypeCode', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'StatusID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'EntityTypeStatusID'
EXEC Utility.SetIndexClustered 'IX_EntityTypeStatus', @cTableName, 'EntityTypeCode ASC,DisplayOrder ASC,StatusID ASC'
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'AAR',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Deleted'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'BATTLEBOARD',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending',
	'Validated',
	'Xfer'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'BINDER',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Deleted'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'CDR',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending',
	'Validated',
	'Xfer'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'CONTENT',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending',
	'Validated',
	'Xfer'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'COP',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending',
	'Validated',
	'Xfer'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'ISSUE',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Analysis',
	'Canceled',
	'Closed',
	'Evaluation',
	'Hold',
	'Integration',
	'Lesson Learned',
	'Monitor',
	'Validated'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'LESSON',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending Mobile',
	'Pending',
	'Restricted',
	'Validated'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'MEMBER',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Deleted',
	'Hold'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'PORTVISIT',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Canceled',
	'Closed',
	'Deleted',
	'Draft',
	'Hold',
	'Pending',
	'Validated',
	'Xfer'
	)
GO

INSERT INTO Dropdown.EntityTypeStatus 
	(EntityTypeCode,StatusID)
SELECT
	'USERGROUP',
	S.StatusID
FROM Dropdown.Status S WITH (NOLOCK)
WHERE S.Status IN 
	(
	'Active',
	'Deleted'
	)
GO
--End table Dropdown.EntityTypeStatus

--Begin table Dropdown.DropdownMetaData
EXEC Utility.AddColumn 'Dropdown.DropdownMetaData', 'CodeFieldName', 'varchar(50)'
GO

UPDATE Dropdown.DropdownMetaData
SET 
	CodeFieldName = 'StateCode',
	PrimaryKeyFieldName = 'StateID'
WHERE DropdownMetaDataCode = 'State'
GO	
--End table Dropdown.DropdownMetaData

--Begin table Dropdown.DropdownMetadataFilter
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Dropdown.DropdownMetadataFilter') AND O.type in (N'U')) 
	DROP TABLE Dropdown.DropdownMetadataFilter
GO

CREATE TABLE Dropdown.DropdownMetadataFilter
	(
	DropdownMetadataFilterID int IDENTITY(1,1) NOT NULL,
	DropdownMetadataID int,
	DropdownFilterID int
	)
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Dropdown.DropdownMetadataFilter'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DropdownFilterID', 0
EXEC Utility.SetDefault @cTableName, 'DropdownMetadataID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DropdownFilterID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownMetadataID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'DropdownMetadataFilterID'
EXEC Utility.SetIndexClustered 'IX_DropdownMetadataFilter', @cTableName, 'DropdownMetadataID ASC,DropdownFilterID ASC'
GO

INSERT INTO Dropdown.DropdownMetadataFilter
	(DropdownMetadataID,DropdownFilterID)
SELECT
	(SELECT DMD1.DropdownMetadataID FROM Dropdown.DropdownMetadata DMD1 WITH (NOLOCK) WHERE DMD1.DropdownMetadataCode = 'AnalysisCode'),
	DMD2.DropdownMetadataID
FROM Dropdown.DropdownMetadata DMD2 WITH (NOLOCK) 
WHERE DMD2.DropdownMetadataCode IN ('AnalysisCodeType')
GO

INSERT INTO Dropdown.DropdownMetadataFilter
	(DropdownMetadataID,DropdownFilterID)
SELECT
	(SELECT DMD1.DropdownMetadataID FROM Dropdown.DropdownMetadata DMD1 WITH (NOLOCK) WHERE DMD1.DropdownMetadataCode = 'Event'),
	DMD2.DropdownMetadataID
FROM Dropdown.DropdownMetadata DMD2 WITH (NOLOCK) 
WHERE DMD2.DropdownMetadataCode IN ('EventLocation','EventSponsor','EventType')
GO

INSERT INTO Dropdown.DropdownMetadataFilter
	(DropdownMetadataID,DropdownFilterID)
SELECT
	(SELECT DMD1.DropdownMetadataID FROM Dropdown.DropdownMetadata DMD1 WITH (NOLOCK) WHERE DMD1.DropdownMetadataCode = 'Metadata'),
	DMD2.DropdownMetadataID
FROM Dropdown.DropdownMetadata DMD2 WITH (NOLOCK) 
WHERE DMD2.DropdownMetadataCode IN ('MetadataType')
GO

INSERT INTO Dropdown.DropdownMetadataFilter
	(DropdownMetadataID,DropdownFilterID)
SELECT
	(SELECT DMD1.DropdownMetadataID FROM Dropdown.DropdownMetadata DMD1 WITH (NOLOCK) WHERE DMD1.DropdownMetadataCode = 'Port'),
	DMD2.DropdownMetadataID
FROM Dropdown.DropdownMetadata DMD2 WITH (NOLOCK) 
WHERE DMD2.DropdownMetadataCode IN ('Country')
GO
--End table Dropdown.DropdownMetadataFilter

--Begin CanManageTabs update
UPDATE dbo.Tier
SET CanManageTabs = 1
WHERE IsInstance = 1
--End CanManageTabs update
--End file JLLIS.sql

