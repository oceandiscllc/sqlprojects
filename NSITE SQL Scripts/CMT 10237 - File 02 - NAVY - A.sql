USE NAVY
GO

--Begin Revert Of Reporting.ObservationsByKeyword
--Begin procedure Reporting.ObservationsByKeyword
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationsByKeyword') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationsByKeyword
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.01.05
-- Description:	A stored procedure to return LMS records containing a specified keyword
--
-- Modify Date:	2012.02.01
-- Description:	Added logic to limit the number of characters returned
--
-- Modify Date:	2012.02.13
-- Description:	Added additional fields to the result set
-- ====================================================================================
CREATE PROCEDURE Reporting.ObservationsByKeyword
	@nKeywordSearchID int,
	@cEntityTypeCode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nMaxLength int
	SET @nMaxLength = 32767

	SELECT
		C.Country,
		E.Event, 
		ET.EventType,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.BackgroundClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Background), @nMaxLength) AS Discussion,

		CONVERT(char(11), L.EventDate, 113) as EventDateFormatted,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.EventDescriptionClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription), @nMaxLength) AS EventDescription, 

		CONVERT(char(11), L.DocumentDate, 113) AS DocumentDateFormatted,
		L.CreatedBy,
		L.FirstName + ' ' + L.LastName AS PostedBy,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ImplicationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Implications), @nMaxLength) AS Implications,

		L.ImportCode,
		L.ImportSource,
		L.IsDraft,
		L.JointLesson,
		L.LegacyID,
		L.LMSID,
		L.LMSUnit,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.ObservationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Observations), @nMaxLength) AS Observation,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.RecommendationsClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations), @nMaxLength) AS Recommendation,

		L.Status, 

		JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID) AS CommentCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID) AS CommentClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID) AS CommentReleasableTo,
		LEFT(JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.SummaryClassificationDataID, 1) + ' ' + JLLIS.Utility.StripHTML(L.Summary), @nMaxLength) AS Comment,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS TopicCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS TopicClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS TopicReleasableTo,
		JLLIS.dbo.GetClassificationAbbreviationByClassificationDataID(L.LMSClassificationDataID, 1) + ' ' + L.Topic AS Topic,

		L.Unit, 
		L.ViewCount,
		T.TierLabel AS Organiztion
	FROM dbo.LMS L
		JOIN Dropdown.Event E ON E.EventID = L.EventID
		JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
		JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = L.CountryID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = @cEntityTypeCode 
			AND KSR.EntityID = L.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
	ORDER BY L.LMSID DESC

END	
GO
--End procedure Reporting.ObservationsByKeyword
--End Revert Of Reporting.ObservationsByKeyword

--Begin temp procedure Reporting.ObservationsByKeyword241
--Begin procedure Reporting.ObservationsByKeyword241
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationsByKeyword241') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationsByKeyword241
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.01.05
-- Description:	A stored procedure to return LMS records containing a specified keyword
--
-- Modify Date:	2012.02.01
-- Description:	Added logic to limit the number of characters returned
--
-- Modify Date:	2012.02.13
-- Description:	Added additional fields to the result set
--
-- Modify Date:	2012.02.27
-- Description:	Added an optional truncation bit
--
-- Modify Date:	2012.03.13
-- Description:	Added an optional IsSOLR bit
--
-- Modify Date:	2012.03.15
-- Description:	Standardized the output column names
--
-- Modify Date:	2012.03.19
-- Description:	Added additional fields for CaS support
--
-- Modify Date:	2012.03.27
-- Description:	Added additional fields for Book Format support
-- ====================================================================================
CREATE PROCEDURE Reporting.ObservationsByKeyword241
	@nKeywordSearchID int,
	@cEntityTypeCode varchar(50),
	@nTruncateData bit = 1,
	@nIsSOLR bit = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cDatabaseServerIPV4Address varchar(max)
	DECLARE @nIsSecure bit
	DECLARE @nMaxLength int
	
	SELECT @cDatabaseServerIPV4Address = SS.ServerSetupValue
	FROM JLLIS.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'DatabaseServerIPV4Address'

	SET @nIsSecure = 0
	
	IF EXISTS 
		(
		SELECT 1 
		FROM JLLIS.Dropdown.Classification CL 
		WHERE CL.IsActive = 1 
			AND CL.Classification <> 'Unclassified' 
			AND CL.Classificationid > 0
		) 

		BEGIN

		SET @nIsSecure = 1

		END
	--ENDIF
	
	SET @nMaxLength = 32767

	SELECT
		C.Country,
		E.Event, 
		E.StartDate AS EventStartDate,
		CONVERT(char(11), E.StartDate, 113) AS EventStartDateFormatted,
		E.EndDate AS EventEndDate,
		CONVERT(char(11), E.EndDate, 113) AS EventEndDateFormatted,
		EL.EventLocation,
		ES.EventSponsor,
		ET.EventType,

		L.Background,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background)
		END AS BackgroundFormatted,

		L.CreatedBy,
		L.CreationDate,
		CONVERT(char(11), L.CreationDate, 113) AS CreationDateFormatted,
		L.DocumentDate,
		CONVERT(char(11), L.DocumentDate, 113) AS DocumentDateFormatted,
		L.DayPhone,
		L.DNSNumber,
		L.Email,
		L.EventDate,
		CONVERT(char(11), L.EventDate, 113) AS EventDateFormatted,

		L.EventDescription,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription)
		END AS EventDescriptionFormatted, 

		(SELECT JU.DayPhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateDayPhone,
		(SELECT JU.Email FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateEmail,
		(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateFirstName,
		(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateLastName,
		(SELECT T.TierLabel FROM JLLIS.dbo.Tier T JOIN JLLIS.dbo.JLLISUser JU ON JU.OriginatingTierID = T.TierID JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateOrganization,
		(SELECT JU.PayGrade FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreatePayGrade,
		(SELECT JU.TollFreePhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateTollFreePhone,

		L.FirstName,
		L.FirstName + ' ' + L.LastName AS PostedBy,

		L.Implications,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications)
		END AS ImplicationsFormatted,

		L.ImportCode,
		L.ImportSource,
		L.IsDraft,
		L.JointLesson,
		L.LastName,
		L.LegacyID,
		L.LMSID,
		L.LMSUnit,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS LMSCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS LMSClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS LMSReleasableTo,

		L.Observations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations)
		END AS ObservationsFormatted,

		L.Rank,

		L.Recommendations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations)
		END AS RecommendationsFormatted,

		L.Status, 

		L.Summary,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID) AS SummaryCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID) AS SummaryClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID) AS SummaryReleasableTo,
		
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary)
		END AS SummaryFormatted,

		L.Topic,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS TopicCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS TopicClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS TopicReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) + ' ' + L.Topic AS TopicFormatted,

		JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationAuthority,
		JLLIS.dbo.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationSource,
		JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID) AS LMSDeclassificationDate,
		CONVERT(char(11), JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID), 113) AS LMSDeclassificationDateFormatted,

		L.Unit, 
		L.UpdateDate,
		CONVERT(char(11), L.UpdateDate, 113) AS UpdateDateFormatted,
		L.ViewCount,
		T.TierLabel AS Organization,
		W.WarfareMission,

		--begin book format support
		JLLIS.dbo.GetClassificationReasonListByClassificationDataID(L.LMSClassificationDataID, '     ') as LMSClassifcationReasons,
		JLLIS.dbo.GetClassificationHeaderByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationHeader,
		@nIsSecure AS IsSecure,
		DB_NAME() AS cDatabaseName,
		@nKeywordSearchID AS nKeywordSearchID,
		@cDatabaseServerIPV4Address AS cDatabaseServerIPV4Address
	FROM dbo.LMS L
		JOIN Dropdown.Event E ON E.EventID = L.EventID
		JOIN Dropdown.EventLocation EL ON EL.EventLocationID = E.EventLocationID
		JOIN Dropdown.EventSponsor ES ON ES.EventSponsorID = E.EventSponsorID
		JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
		JOIN Dropdown.WarfareMission W ON W.warfareMissionID = L.WarfareMissionID
		JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = L.CountryID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = @cEntityTypeCode 
			AND KSR.EntityID = L.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
			AND ((@nIsSOLR = 0) OR (@nIsSOLR = 1 AND KSR.IsSOLR = 1))
	ORDER BY L.LMSID DESC

END	
GO
--End procedure Reporting.ObservationsByKeyword241
--End temp procedure Reporting.ObservationsByKeyword241

--Begin procedure Reporting.ObservationExport
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationExport') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationExport
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to export an observation
-- ========================================================
CREATE PROCEDURE Reporting.ObservationExport
	@nLMSID int = 0,
	@dDateStart date = NULL,
	@dDateStop date = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nJLLISUserID int
	DECLARE @nKeywordSearchID int

	SELECT @nJLLISUserID = (MAX(JU.JLLISUserID) * 10)
	FROM JLLIS.dbo.JLLISUser JU
	
	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)
	
	INSERT INTO JLLIS.Reporting.KeywordSearch
		(JLLISUserID)
	VALUES
		(@nJLLISUserID)
	
	SELECT @nKeywordSearchID = SCOPE_IDENTITY()

	IF @dDateStart IS NOT NULL
		BEGIN

		IF @dDateStop IS NULL
			SET @dDateStop = getDate()
			
		SET @dDateStop = DATEADD(d, 1, @dDateStop)
		
		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		SELECT
			@nKeywordSearchID,
			'Observation',
			L.LMSID,
			0
		FROM dbo.LMS L
		WHERE L.CreationDate BETWEEN @dDateStart AND @dDateStop
			OR L.UpdateDate BETWEEN @dDateStart AND @dDateStop
		
		END
	ELSE
		BEGIN

		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		VALUES
			(
			@nKeywordSearchID,
			'Observation',
			@nLMSID,
			0
			)
		
		END
		
	--ENDIF	

	EXEC Reporting.ObservationsByKeyword241 @nKeywordSearchID, 'Observation', 0
	EXEC Reporting.ObservationDiscuss @nKeywordSearchID, 0
	EXEC Reporting.ObservationAnalysisCode @nKeywordSearchID
	EXEC Reporting.ObservationMetadata @nKeywordSearchID
	EXEC Reporting.ObservationTask @nKeywordSearchID

	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)

END
GO
--End procedure Reporting.ObservationExport

--Begin permissions
DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'GRANT EXECUTE ON ' + S.Name + '.' + O.Name + ' To [' + U.Name + ']'
	FROM sys.sysobjects O, dbo.sysusers U
		JOIN sys.schemas S ON S.schema_ID = S.schema_ID
	WHERE S.Name + '.' + O.Name IN
		(
		'Reporting.ObservationExport'
		)
		AND U.Name IN ('CASUser','Dennis.Kretzschmar','navyCas')
	ORDER BY S.Name, O.Name, U.Name

OPEN oCursor
FETCH oCursor into @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXECUTE (@cSQL)

	FETCH oCursor into @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End permissions
