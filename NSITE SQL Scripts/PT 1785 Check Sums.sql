SELECT
	COUNT(ESMD.EntitySystemMetadataID) AS ItemCount,
	ESMD.EntityTypeCode
FROM JLLIS.dbo.EntitySystemMetadata ESMD
GROUP BY ESMD.EntityTypeCode
ORDER BY ESMD.EntityTypeCode

SELECT
	COUNT(CMD.CDRMetadataID) AS ItemCount,
	'CDR' AS EntityTypeCode
FROM JSCC.dbo.CDRMetadata CMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = CMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType LIKE 'DOTMLPF%'
		
UNION

SELECT
	COUNT(IMD.IssueMetadataID) AS ItemCount,
	'Issue' AS EntityTypeCode
FROM JLLIS.dbo.IssueMetadata IMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = IMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType LIKE 'DOTMLPF%'

UNION

SELECT
	COUNT(LMD.LMSMetadataID) AS ItemCount,
	'Observation' AS EntityTypeCode
FROM JSCC.dbo.LMSMetadata LMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = LMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType LIKE 'DOTMLPF%'

SELECT
	COUNT(EMD.EntityMetadataID) AS ItemCount,
	EMD.EntityTypeCode
FROM JLLIS.dbo.EntityMetadata EMD
WHERE EXISTS
	(
	SELECT 1
	FROM JSCC.Dropdown.Metadata MD
	WHERE MD.MetadataID = EMD.MetadataID
	)
GROUP BY EMD.EntityTypeCode
ORDER BY EMD.EntityTypeCode

SELECT
	COUNT(CMD.CDRMetadataID) AS ItemCount,
	'CDR' AS EntityTypeCode
FROM JSCC.dbo.CDRMetadata CMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = CMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType NOT LIKE 'DOTMLPF%'
		
UNION

SELECT
	COUNT(IMD.IssueMetadataID) AS ItemCount,
	'Issue' AS EntityTypeCode
FROM JLLIS.dbo.IssueMetadata IMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = IMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType NOT LIKE 'DOTMLPF%'

UNION

SELECT
	COUNT(LMD.LMSMetadataID) AS ItemCount,
	'Observation' AS EntityTypeCode
FROM JSCC.dbo.LMSMetadata LMD
	JOIN JSCC.Dropdown.Metadata MD ON MD.MetadataID = LMD.MetadataID
	JOIN JSCC.Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		AND MDT.MetadataType NOT LIKE 'DOTMLPF%'

