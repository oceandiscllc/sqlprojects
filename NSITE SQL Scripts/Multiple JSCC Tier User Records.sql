SELECT
	COUNT(TU.TierUserID) AS ItemCount,
	TU.JLLISUserID,
	TU.TierID,
	Utility.GetPersonNameByUserID(TU.JLLISUserID, 0, 'LastFirstTitle') AS JLLISUser
FROM JLLIS.dbo.TierUser TU
	JOIN JLLIS.dbo.Tier T ON T.TierID = TU.TierID
		AND T.TierName = 'JSCC'
		AND T.IsInstance = 1
		AND TU.JLLISUserID > 0
		AND TU.Status = 'Active'
GROUP BY 
	TU.JLLISUserID,
	TU.TierID,
	Utility.GetPersonNameByUserID(TU.JLLISUserID, 0, 'LastFirstTitle')
HAVING COUNT(TU.TierUserID) > 1
ORDER BY 
	TU.JLLISUserID,
	TU.TierID
