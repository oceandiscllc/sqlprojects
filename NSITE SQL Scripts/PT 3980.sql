USE JSCC
GO

--Begin function dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2014.02.28
-- Description:	A function to return an originatingtierid based on an entitytypecode and an entityid

-- Author:			Todd Pires
-- Update Date:	2015.01.27
-- Description:	Added additional entity type codes, moved the function from the JLLIS database to the Instance database
-- ====================================================================================================================

CREATE FUNCTION dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID
(
@cEntityTypeCode VARCHAR(50),
@nEntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nRetVal INT = 0
	
	IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.EntityType ET WHERE ET.EntityTypeCode = @cEntityTypeCode)
		BEGIN
		
		SELECT @cEntityTypeCode = ET.ActiveEntityTypeCode
		FROM JLLIS.dbo.EntityType ET WHERE ET.EntityTypeName = @cEntityTypeCode
		
		END
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM JLLIS.dbo.EntityType ET WHERE ET.EntityTypeCode = @cEntityTypeCode)
		BEGIN

		IF @cEntityTypeCode = 'AAR'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.AAR T WHERE T.AARID = @nEntityID
		ELSE IF @cEntityTypeCode IN ('BB', 'TaskTracker')
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.BB T WHERE T.BBID = @nEntityID
		ELSE IF @cEntityTypeCode IN ('BBComment', 'TaskTrackerCOmment')
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.BB T JOIN dbo.BBComment BBC ON BBC.BBID = T.BBID AND BBC.BBCommentID = @nEntityID
		ELSE IF @cEntityTypeCode = 'Binder'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.Binder T WHERE T.BinderID = @nEntityID
		ELSE IF @cEntityTypeCode = 'CDR'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.CDR T WHERE T.CDRID = @nEntityID
		ELSE IF @cEntityTypeCode = 'CDRDiscuss'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.CDR T JOIN dbo.CDRDiscuss CD ON CD.CDRID = T.CDRID AND CD.CDRDiscussID = @nEntityID
		ELSE IF @cEntityTypeCode = 'CollectionPlan'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.CollectionPlan T WHERE T.CollectionPlanID = @nEntityID
		ELSE IF @cEntityTypeCode IN ('COI', 'COP', 'SSite')
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.COP.COP T WHERE T.COPID = @nEntityID
		ELSE IF @cEntityTypeCode = 'JLLISFile'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.JLLISFile T WHERE T.JLLISFileID = @nEntityID
		ELSE IF @cEntityTypeCode = 'Issue'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.Issue T WHERE T.IssueID = @nEntityID
		ELSE IF @cEntityTypeCode = 'IssueDiscussion'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.Issue T JOIN JLLIS.dbo.IssueDiscussion ID ON ID.IssueID = T.IssueID AND ID.IssueDiscussionID = @nEntityID
		ELSE IF @cEntityTypeCode IN ('Lesson', 'LMS', 'Observation')
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.LMS T WHERE T.LMSID = @nEntityID
		ELSE IF @cEntityTypeCode  = 'LMSDiscuss'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.LMS T JOIN dbo.LMSDiscuss LD ON LD.LMSID = T.LMSID AND LD.LMSDiscussID = @nEntityID
		ELSE IF @cEntityTypeCode IN ('PortVisit', 'PV', 'PVR')
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.PortVisit T WHERE T.PVID = @nEntityID
		ELSE IF @cEntityTypeCode = 'PortVisitDiscuss'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.PortVisit T JOIN dbo.PortVisitDiscuss PVD ON PVD.PVID = T.PVID AND PVD.PortVisitDiscussID = @nEntityID
		ELSE IF @cEntityTypeCode = 'TripReport'
			SELECT @nRetVal = T.OriginatingTierID FROM dbo.TripReport T WHERE T.TripReportID = @nEntityID
		ELSE IF @cEntityTypeCode = 'UserGroup'
			SELECT @nRetVal = T.OriginatingTierID FROM JLLIS.dbo.UserGroup T WHERE T.UserGroupID = @nEntityID
		--ENDIF
	
		END
	--ENDIF
		
	RETURN ISNULL(@nRetVal, 0)
END
GO
--End function dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID

DECLARE @nTotal BIGINT
DECLARE @tTable TABLE (PhysicalFileSize BIGINT, OriginatingTierID INT)

INSERT INTO @tTable
	(PhysicalFileSize, OriginatingTierID)
SELECT 
	JF.PhysicalFileSize,
	dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID(EJF.EntityTypeCode, EJF.EntityID) AS OriginatingTierID
FROM JLLIS.dbo.JLLISFile JF 
	JOIN JLLIS.dbo.EntityJLLISFile EJF ON EJF.JLLISFileID = JF.JLLISFileID
		AND dbo.GetOriginatingTierIDByEntityTypeCodeAndEntityID(EJF.EntityTypeCode, EJF.EntityID) > 0

SELECT @nTotal = SUM(T1.PhysicalFileSize)
FROM @tTable T1

SELECT 
	D.PhysicalFileSize,
	CAST(CAST(D.PhysicalFileSize AS NUMERIC(18,2)) / @nTotal * 100 AS NUMERIC(18,2)) AS Percentage,
	T2.TierLabel
FROM
	(
	SELECT 
		SUM(T1.PhysicalFileSize) AS PhysicalFileSize,
		T2.InstanceID
	FROM @tTable T1
		JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.OriginatingTierID
	GROUP BY T2.InstanceID
	) D
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = D.InstanceID
		AND T2.IsInstance = 1
ORDER BY T2.TierLabel, D.InstanceID

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
	DROP TABLE #tTable
--ENDIF
GO
	
CREATE TABLE #tTable 
	(
	ItemCount INT, 
	InstanceID INT, 
	EntityTypeCode VARCHAR(50)
	)
GO

DECLARE @cSQL VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'INSERT INTO #tTable (ItemCount, InstanceID, EntityTypeCode) SELECT COUNT(T2.InstanceID) AS ItemCount, T2.InstanceID, ''' + ET.EntityTypeCode + ''' AS EntityTypeCode FROM ' + ET.DatabaseName + '.' + ET.SchemaName + '.' + ET.TableName + ' T1 JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.OriginatingTierID GROUP BY T2.InstanceID'
	FROM JLLIS.dbo.EntityType ET
		JOIN
			(
			SELECT
				S.Name AS SchemaName,
				O.Name AS TableName
			FROM JLLIS.sys.Objects O
				JOIN JLLIS.sys.Schemas S ON S.schema_ID = O.schema_ID
				JOIN JLLIS.sys.Columns C ON O.Object_ID = C.Object_ID
				JOIN JLLIS.sys.Types T ON C.User_Type_ID = T.User_Type_ID
					AND O.type = 'U' 
					AND C.Name = 'OriginatingTierID'
					AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')
			) D ON D.SchemaName = ET.SchemaName
				AND D.TableName = ET.TableName
				AND ET.DatabaseName = 'JLLIS'
				AND ET.EntityTypeCode = ET.ActiveEntityTypeCode

	UNION

	SELECT
		'INSERT INTO #tTable (ItemCount, InstanceID, EntityTypeCode) SELECT COUNT(T2.InstanceID) AS ItemCount, T2.InstanceID, ''' + ET.EntityTypeCode + ''' AS EntityTypeCode FROM ' + ET.SchemaName + '.' + ET.TableName + ' T1 JOIN JLLIS.dbo.Tier T2 ON T2.TierID = T1.OriginatingTierID GROUP BY T2.InstanceID'
	FROM JLLIS.dbo.EntityType ET
		JOIN
			(
			SELECT
				S.Name AS SchemaName,
				O.Name AS TableName
			FROM sys.Objects O
				JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
				JOIN sys.Columns C ON O.Object_ID = C.Object_ID
				JOIN sys.Types T ON C.User_Type_ID = T.User_Type_ID
					AND O.type = 'U' 
					AND C.Name = 'OriginatingTierID'
					AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')
			) D ON D.SchemaName = ET.SchemaName
				AND D.TableName = ET.TableName
				AND ET.DatabaseName IS NULL
				AND ET.EntityTypeCode = ET.ActiveEntityTypeCode

	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXEC(@cSQL)
	FETCH oCursor INTO @cSQL
		
	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

DECLARE @nTotal BIGINT

SELECT @nTotal = SUM(T1.ItemCount)
FROM #tTable T1

SELECT 
	D.ItemCount AS StructuredItemCount,
	CAST(CAST(D.ItemCount AS NUMERIC(18,2)) / @nTotal * 100 AS NUMERIC(18,2)) AS Percentage,
	T2.TierLabel
FROM
	(
	SELECT 
		SUM(T1.ItemCount) AS ItemCount,
		T1.InstanceID
	FROM #tTable T1
	GROUP BY T1.InstanceID
	) D
	JOIN JLLIS.dbo.Tier T2 ON T2.TierID = D.InstanceID
		AND T2.IsInstance = 1
ORDER BY T2.TierLabel, D.InstanceID

DROP TABLE #tTable

;
WITH FS AS
	(
  SELECT 
		MF.Database_ID, 
		MF.Type, 
		MF.Size * 8.0 / 1024 AS Size, DB.Name
  FROM sys.master_files MF
		JOIN sys.databases DB ON DB.Database_ID = MF.Database_ID
			AND DB.Name IN ('ACGU','JLLIS','JSCC','JSOC')
	)

SELECT 
	DB.Name AS DatabaseName,
  (SELECT SUM(FS.Size) FROM FS WHERE FS.Type = 0 AND FS.Database_ID = DB.Database_ID) AS DataFileSizeMB,
  (SELECT SUM(FS.Size) FROM FS WHERE FS.Type = 1 AND FS.Database_ID = DB.Database_ID) AS LogFileSizeMB
FROM sys.databases db
WHERE DB.Name IN ('ACGU','JLLIS','JSCC','JSOC')
ORDER BY DB.Name