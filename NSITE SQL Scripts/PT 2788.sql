USE JLLIS
GO

--Begin PT 2788
--Begin procedure Utility.CloneClassificationDataByClassificationDataID
EXEC Utility.DropObject 'Utility.CloneClassificationDataByClassificationDataID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date: 2013.01.24
-- Description:	A stored procedure to clone classification data
--
-- Author:			Todd Pires
-- Create date: 2013.09.03
-- Description:	Replaced SCOPE_IDENTITY() with tOutput
--
-- Author:			Todd Pires
-- Create date: 2013.09.03
-- Description:	Added support for a @nSourceClassificationDataID of 0
-- ==================================================================
CREATE PROCEDURE Utility.CloneClassificationDataByClassificationDataID
	@nSourceClassificationDataID INT,
	@nClonedClassificationDataID INT OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ClassificationDataID INT)

	INSERT INTO JLLIS.dbo.ClassificationData
		(ClassificationID,CaveatID,ClassificationSource,ClassificationAuthority,IsCompilation,DeclassificationDate)
	OUTPUT INSERTED.ClassificationDataID INTO @tOutput
	SELECT
		CD.ClassificationID,
		CD.CaveatID,
		CD.ClassificationSource,
		CD.ClassificationAuthority,
		CD.IsCompilation,
		CD.DeclassificationDate
	FROM JLLIS.dbo.ClassificationData CD
	WHERE CD.ClassificationDataID = @nSourceClassificationDataID

	SELECT @nClonedClassificationDataID = ClassificationDataID
	FROM @tOutput

	SET @nClonedClassificationDataID = ISNULL(@nClonedClassificationDataID, 0)
	
	IF @nClonedClassificationDataID > 0
		BEGIN
		
		INSERT INTO JLLIS.dbo.ClassificationDataClassificationReason
			(ClassificationDataID,ClassificationReasonID)
		SELECT 
			@nClonedClassificationDataID,
			CDCR.ClassificationReasonID
		FROM JLLIS.dbo.ClassificationDataClassificationReason CDCR
		WHERE CDCR.ClassificationDataID = @nSourceClassificationDataID
			
		INSERT INTO JLLIS.dbo.ClassificationDataCoalition
			(ClassificationDataID,CoalitionID)
		SELECT 
			@nClonedClassificationDataID,
			CDC.CoalitionID
		FROM JLLIS.dbo.ClassificationDataCoalition CDC
		WHERE CDC.ClassificationDataID = @nSourceClassificationDataID
			
		INSERT INTO JLLIS.dbo.ClassificationDataCountry
			(ClassificationDataID,CountryID)
		SELECT 
			@nClonedClassificationDataID,
			CDC.CountryID
		FROM JLLIS.dbo.ClassificationDataCountry CDC
		WHERE CDC.ClassificationDataID = @nSourceClassificationDataID

		END
	--ENDIF

	RETURN @nClonedClassificationDataID

END
GO	
--End procedure Utility.CloneClassificationDataByClassificationDataID
--End PT 2788