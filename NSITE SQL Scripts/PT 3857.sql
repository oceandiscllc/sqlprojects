SELECT TOP 25
	(SELECT SS.ServerSetupValue FROM JLLIS.dbo.ServerSetup SS WHERE SS.ServerSetupKey = 'NetworkName') AS Environment,
	'Issue' AS [Entity Type],
	I.IssueID AS [Entity ID],
	T.TierLabel AS Organization,
	CONVERT(CHAR(11), I.CreateDate, 113) AS CreateDateFormatted,
	JLLIS.dbo.GetClassificationHeaderByClassificationDataID(I.IssueClassificationDataID) AS ClassificationHeader,
	I.Title,
	I.Description,
	I.ViewCount
FROM JLLIS.dbo.Issue I
	JOIN JLLIS.dbo.Tier T ON T.TierID = I.OPRTierID
ORDER BY I.ViewCount DESC

SELECT TOP 25
	(SELECT SS.ServerSetupValue FROM JLLIS.dbo.ServerSetup SS WHERE SS.ServerSetupKey = 'NetworkName') AS Environment,
	'Observation' AS [Entity Type],
	L.LMSID AS [Entity ID],
	T.TierLabel AS Organization,
	CONVERT(CHAR(11), L.CreationDate, 113) AS CreateDateFormatted,
	JLLIS.dbo.GetClassificationHeaderByClassificationDataID(L.LMSClassificationDataID) AS ClassificationHeader,
	L.Topic AS Title,
	L.Observations AS Description,
	L.ViewCount
FROM JSCC.dbo.LMS L
	JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
ORDER BY L.ViewCount DESC