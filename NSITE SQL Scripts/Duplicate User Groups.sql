IF (SELECT OBJECT_ID('tempdb.dbo.#oTable1', 'u')) IS NOT NULL
	DROP TABLE #tTable1

CREATE TABLE #tTable1 
	(
	UserGroupName varchar(250),
	InstanceID INT
	)
				
DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

SET @cSQL = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN 
		(
		'ACGU',
		'ARMY',
		'CCO',
		'DISA',
		'DLA',
		'DOS',
		'DTRA',
		'HPRC',
		'JSCC',
		'NAVY',
		'NCCS',
		'NGA',
		'NGB',
		'NOMI',
		'ORCHID',
		'RPB',
		'USAF',
		'USUHS'
		)
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor INTO @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'INSERT INTO #tTable1 (UserGroupName,InstanceID) SELECT UG.UserGroupName, (SELECT S.SetupValue FROM ' 
	SET @cSQL += @cDatabaseName 
	SET @cSQL += '.Utility.Setup S WHERE S.SetupKey = ''InstanceID'') FROM '
	SET @cSQL += @cDatabaseName
	SET @cSQL += '.dbo.usergroups UG'
	
	EXEC (@cSQL)
	
	FETCH oCursor INTO @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

SELECT
	D.ItemCount,
	D.UserGroupName,
	T.TierLabel
FROM
	(
	SELECT 
		COUNT(T1.UserGroupName) AS ItemCount,
		T1.UserGroupName
	FROM #tTable1 T1
	GROUP BY T1.UserGroupName
	HAVING COUNT(T1.UserGroupName) > 1
	) D 
	JOIN #tTable1 T1 ON T1.UserGroupName = D.UserGroupName
	JOIN JLLIS.dbo.Tier T ON T.TierID = T1.InstanceID
ORDER BY D.UserGroupName, T.TierLabel

DROP TABLE #tTable1
