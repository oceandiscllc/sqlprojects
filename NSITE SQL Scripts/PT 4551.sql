USE JLLIS
GO

DECLARE @nTierID INT = <TIERID>

UPDATE T
SET T.InstanceID = @nTierID
FROM dbo.Tier T
	JOIN JLLIS.dbo.GetDescendantTiersByTierID(@nTierID) DT ON DT.TierID = T.TierID

UPDATE T
SET
	T.CanManageContent = 1,
	T.CanManageDropdowns = 1,
	T.CanManageLessonTabs = 1,
	T.CanManageMenu = 1,
	T.CanManageMetadata = 1,
	T.CanManageOpEx = 1,
	T.CanManageSetup = 1,
	T.CanManageTabs = 1,
	T.InheritParentPOC = 0,
	T.InstanceType = 'SILO',
	T.IsAssignable = 1,
	T.IsForNavigation = 1,
	T.Isinstance = 1,
	T.IsProgram = 0,
	T.ParentTierID = 0
FROM dbo.Tier T
WHERE T.TierID = @nTierID
GO