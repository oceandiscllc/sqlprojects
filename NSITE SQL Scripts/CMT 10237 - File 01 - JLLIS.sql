USE JLLIS
GO

--Begin table Reporting.KeywordSearchResult
DECLARE @cTableName varchar(250)

SET @cTableName = 'Reporting.KeywordSearchResult'

EXEC Utility.AddColumn @cTableName, 'IsSOLR', 'bit'
EXEC Utility.SetDefault @cTableName, 'IsSOLR', 0
EXEC Utility.SetColumnNotNull @cTableName, 'IsSOLR', 'bit'
GO
--End table Reporting.KeywordSearchResult

--Begin function dbo.GetClassificationAuthorityByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetClassificationAuthorityByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationAuthorityByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a classification authority for a specific classification data id
-- ==================================================================================================

CREATE FUNCTION dbo.GetClassificationAuthorityByClassificationDataID
(
@nClassificationDataID int
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cRetVal varchar(max)

SELECT 
	@cRetVal = ISNULL(CD.ClassificationAuthority, '')
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @cRetVal
END
GO
--End function dbo.GetClassificationAuthorityByClassificationDataID

--Begin dbo.GetClassificationHeaderByClassificationDataID function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.GetClassificationHeaderByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationHeaderByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2011.10.20
-- Description: A function to return a Classification Header from a ClassificationDataID
--
-- Modify date: 2012.03.13
-- Description: changed the internals to use function calls
-- ====================================================================================
CREATE FUNCTION dbo.GetClassificationHeaderByClassificationDataID
(
@cClassificationDataID int
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cCaveat varchar(250)
DECLARE @cReleasableToList varchar(max)
DECLARE @cRetVal varchar(max)

SET @cRetVal = (SELECT UPPER(dbo.GetClassificationByClassificationDataID(@cClassificationDataID)))
SET @cCaveat = (SELECT dbo.GetCaveatByClassificationDataID(@cClassificationDataID))
SET @cReleasableToList = (SELECT dbo.GetReleasableToListByClassificationDataID(@cClassificationDataID))

IF @cCaveat IS NOT NULL AND LEN(RTRIM(@cCaveat)) > 0
	BEGIN

	SET @cRetVal = @cRetVal + ':' + @cCaveat

	END
--ENDIF
	
IF @cReleasableToList IS NOT NULL AND LEN(RTRIM(@cReleasableToList)) > 0
	BEGIN

	SET @cRetVal = @cRetVal + ' Rel To ' + @cReleasableToList

	END
--ENDIF

RETURN @cRetVal
END

GO
--End dbo.GetClassificationHeaderByClassificationDataID function

--Begin dbo.GetClassificationLabelByClassificationDataID function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.GetClassificationLabelByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationLabelByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2011.10.20
-- Description: A function to return a Classification Label from a ClassificationDataID
--
-- Modify date: 2011.10.20
-- Description: changed the relto delimiter from ":" to "Rel To"
-- ====================================================================================
CREATE FUNCTION dbo.GetClassificationLabelByClassificationDataID
(
@cClassificationDataID int
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cCaveat varchar(250)
DECLARE @cReleasableToList varchar(max)
DECLARE @cRetVal varchar(max)

SET @cRetVal = (SELECT dbo.GetClassificationAbbreviationByClassificationDataID(@cClassificationDataID, 1))
SET @cCaveat = (SELECT dbo.GetCaveatByClassificationDataID(@cClassificationDataID))
SET @cReleasableToList = (SELECT dbo.GetReleasableToListByClassificationDataID(@cClassificationDataID))

IF @cCaveat IS NOT NULL AND LEN(RTRIM(@cCaveat)) > 0
	BEGIN

	SET @cRetVal = @cRetVal + ':' + @cCaveat

	END
--ENDIF
	
IF @cReleasableToList IS NOT NULL AND LEN(RTRIM(@cReleasableToList)) > 0
	BEGIN

	SET @cRetVal = @cRetVal + ' Rel To ' + @cReleasableToList

	END
--ENDIF

RETURN @cRetVal
END

GO
--End dbo.GetClassificationLabelByClassificationDataID function

--Begin function dbo.GetClassificationReasonListByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetClassificationReasonListByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationReasonListByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2012.03.26
-- Description:	A function to return a delimited list of classification reasons for a specific 
-- 							classification data id
-- ===========================================================================================

CREATE FUNCTION dbo.GetClassificationReasonListByClassificationDataID
(
@nClassificationDataID int,
@cDelimiter varchar(10)
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cClassificationReason varchar(max)
DECLARE @cRetVal varchar(max)

SET @cRetVal = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT CR.ClassificationReason
	FROM dbo.ClassificationDataClassificationReason CDCR
		JOIN Dropdown.ClassificationReason CR ON CR.ClassificationReasonID = CDCR.ClassificationReasonID
			AND CDCR.ClassificationDataID = @nClassificationDataID
	ORDER BY CR.ClassificationReason

OPEN oCursor
FETCH oCursor INTO @cClassificationReason
WHILE @@fetch_status = 0
	BEGIN
	
	IF LEN(LTRIM(@cRetVal)) > 0
		BEGIN

		SET @cRetVal = @cRetVal + @cDelimiter

		END
	--ENDIF
		
	SET @cRetVal = @cRetVal + @cClassificationReason
		
	FETCH oCursor into @cClassificationReason
			
	END
--END WHILE
			
CLOSE oCursor
DEALLOCATE oCursor

RETURN RTRIM(LTRIM(@cRetVal))
END
GO
--End function dbo.GetClassificationReasonListByClassificationDataID

--Begin function dbo.GetClassificationSourceByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetClassificationSourceByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetClassificationSourceByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a classification source for a specific classification data id
-- ===============================================================================================

CREATE FUNCTION dbo.GetClassificationSourceByClassificationDataID
(
@nClassificationDataID int
)

RETURNS varchar(50)

AS
BEGIN

DECLARE @cRetVal varchar(50)

SELECT 
	@cRetVal = ISNULL(CD.ClassificationSource, '')
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @cRetVal
END
GO
--End function dbo.GetClassificationSourceByClassificationDataID

--Begin function dbo.GetDeclassificationDateByClassificationDataID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.GetDeclassificationDateByClassificationDataID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetDeclassificationDateByClassificationDataID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.02.27
-- Description:	A function to return a declassification date for a specific classification data id
-- ===============================================================================================

CREATE FUNCTION dbo.GetDeclassificationDateByClassificationDataID
(
@nClassificationDataID int
)

RETURNS datetime

AS
BEGIN

DECLARE @dRetVal datetime

SELECT 
	@dRetVal = DeclassificationDate
FROM dbo.ClassificationData CD 
WHERE CD.ClassificationDataID = @nClassificationDataID

RETURN @dRetVal
END
GO
--End function dbo.GetDeclassificationDateByClassificationDataID
