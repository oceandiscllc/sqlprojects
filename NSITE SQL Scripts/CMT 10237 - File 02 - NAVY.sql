USE Navy
GO
--Begin table Utility.Setup
DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.Setup'

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	DROP TABLE Utility.Setup

CREATE TABLE Utility.Setup
	(
	SetupID int IDENTITY(1,1) NOT NULL,
	SetupKey varchar(250),
	SetupValue varchar(max),
	CreateDate datetime
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CreateDate', 'getDate()'

EXEC Utility.SetColumnNotNull @cTableName, 'CreateDate', 'datetime'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SetupID'
GO

INSERT INTO Utility.Setup
	(SetupKey,SetupValue)
SELECT
	'InstanceID',
	T.TierID
FROM JLLIS.dbo.Tier T
WHERE T.TierName = 'Navy'
	AND IsInstance = 1
GO
--End table Utility.Setup

--Begin table dbo.LMSDiscuss
DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LMSDiscussClassificationDataID'
SET @cTableName = 'dbo.LMSDiscuss'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO

DECLARE @cColumnName varchar(250)
DECLARE @cTableName varchar(250)

SET @cColumnName = 'LMSDiscussFileClassificationDataID'
SET @cTableName = 'dbo.LMSDiscussFile'

EXEC Utility.AddColumn @cTableName, @cColumnName, 'int'
EXEC Utility.SetDefault @cTableName, @cColumnName, 0
EXEC Utility.SetColumnNotNull @cTableName, @cColumnName, 'int'
GO
--Begin table dbo.LMSDiscuss

--Begin procedure Reporting.ObservationsByKeyword
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationsByKeyword') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationsByKeyword
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2012.01.05
-- Description:	A stored procedure to return LMS records containing a specified keyword
--
-- Modify Date:	2012.02.01
-- Description:	Added logic to limit the number of characters returned
--
-- Modify Date:	2012.02.13
-- Description:	Added additional fields to the result set
--
-- Modify Date:	2012.02.27
-- Description:	Added an optional truncation bit
--
-- Modify Date:	2012.03.13
-- Description:	Added an optional IsSOLR bit
--
-- Modify Date:	2012.03.15
-- Description:	Standardized the output column names
--
-- Modify Date:	2012.03.19
-- Description:	Added additional fields for CaS support
--
-- Modify Date:	2012.03.27
-- Description:	Added additional fields for Book Format support
-- ====================================================================================
CREATE PROCEDURE Reporting.ObservationsByKeyword
	@nKeywordSearchID int,
	@cEntityTypeCode varchar(50),
	@nTruncateData bit = 1,
	@nIsSOLR bit = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cDatabaseServerIPV4Address varchar(max)
	DECLARE @nIsSecure bit
	DECLARE @nMaxLength int
	
	SELECT @cDatabaseServerIPV4Address = SS.ServerSetupValue
	FROM JLLIS.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'DatabaseServerIPV4Address'

	SET @nIsSecure = 0
	
	IF EXISTS 
		(
		SELECT 1 
		FROM JLLIS.Dropdown.Classification CL 
		WHERE CL.IsActive = 1 
			AND CL.Classification <> 'Unclassified' 
			AND CL.Classificationid > 0
		) 

		BEGIN

		SET @nIsSecure = 1

		END
	--ENDIF
	
	SET @nMaxLength = 32767

	SELECT
		C.Country,
		E.Event, 
		E.StartDate AS EventStartDate,
		CONVERT(char(11), E.StartDate, 113) AS EventStartDateFormatted,
		E.EndDate AS EventEndDate,
		CONVERT(char(11), E.EndDate, 113) AS EventEndDateFormatted,
		EL.EventLocation,
		ES.EventSponsor,
		ET.EventType,

		L.Background,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.BackgroundClassificationDataID) AS BackgroundReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.BackgroundClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Background)
		END AS BackgroundFormatted,

		L.CreatedBy,
		L.CreationDate,
		CONVERT(char(11), L.CreationDate, 113) AS CreationDateFormatted,
		L.DocumentDate,
		CONVERT(char(11), L.DocumentDate, 113) AS DocumentDateFormatted,
		L.DayPhone,
		L.DNSNumber,
		L.Email,
		L.EventDate,
		CONVERT(char(11), L.EventDate, 113) AS EventDateFormatted,

		L.EventDescription,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.EventDescriptionClassificationDataID) AS EventDescriptionReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.EventDescriptionClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.EventDescription)
		END AS EventDescriptionFormatted, 

		(SELECT JU.DayPhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateDayPhone,
		(SELECT JU.Email FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateEmail,
		(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateFirstName,
		(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateLastName,
		(SELECT T.TierLabel FROM JLLIS.dbo.Tier T JOIN JLLIS.dbo.JLLISUser JU ON JU.OriginatingTierID = T.TierID JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateOrganization,
		(SELECT JU.PayGrade FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreatePayGrade,
		(SELECT JU.TollFreePhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = L.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateTollFreePhone,

		L.FirstName,
		L.FirstName + ' ' + L.LastName AS PostedBy,

		L.Implications,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ImplicationsClassificationDataID) AS ImplicationsReleasableTo,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ImplicationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Implications)
		END AS ImplicationsFormatted,

		L.ImportCode,
		L.ImportSource,
		L.IsDraft,
		L.JointLesson,
		L.LastName,
		L.LegacyID,
		L.LMSID,
		L.LMSUnit,

		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS LMSCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS LMSClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS LMSReleasableTo,

		L.Observations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.ObservationsClassificationDataID) AS ObservationsReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.ObservationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Observations)
		END AS ObservationsFormatted,

		L.Rank,

		L.Recommendations,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.RecommendationsClassificationDataID) AS RecommendationsReleasableTo,
				
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.RecommendationsClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Recommendations)
		END AS RecommendationsFormatted,

		L.Status, 

		L.Summary,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.SummaryClassificationDataID) AS SummaryCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.SummaryClassificationDataID) AS SummaryClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.SummaryClassificationDataID) AS SummaryReleasableTo,
		
		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary), @nMaxLength)
			ELSE JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.SummaryClassificationDataID) + ' ' + JLLIS.Utility.StripHTML(L.Summary)
		END AS SummaryFormatted,

		L.Topic,
		JLLIS.dbo.GetCaveatByClassificationDataID(L.LMSClassificationDataID) AS TopicCaveat,
		JLLIS.dbo.GetClassificationByClassificationDataID(L.LMSClassificationDataID) AS TopicClassification,
		JLLIS.dbo.GetReleasableToListByClassificationDataID(L.LMSClassificationDataID) AS TopicReleasableTo,
		JLLIS.dbo.GetClassificationLabelByClassificationDataID(L.LMSClassificationDataID) + ' ' + L.Topic AS TopicFormatted,

		JLLIS.dbo.GetClassificationAuthorityByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationAuthority,
		JLLIS.dbo.GetClassificationSourceByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationSource,
		JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID) AS LMSDeclassificationDate,
		CONVERT(char(11), JLLIS.dbo.GetDeclassificationDateByClassificationDataID(L.LMSClassificationDataID), 113) AS LMSDeclassificationDateFormatted,

		L.Unit, 
		L.UpdateDate,
		CONVERT(char(11), L.UpdateDate, 113) AS UpdateDateFormatted,
		L.ViewCount,
		T.TierLabel AS Organization,
		W.WarfareMission,

		--begin book format support
		JLLIS.dbo.GetClassificationReasonListByClassificationDataID(L.LMSClassificationDataID, '     ') as LMSClassifcationReasons,
		JLLIS.dbo.GetClassificationHeaderByClassificationDataID(L.LMSClassificationDataID) AS LMSClassificationHeader,
		@nIsSecure AS IsSecure,
		DB_NAME() AS cDatabaseName,
		@nKeywordSearchID AS nKeywordSearchID,
		@cDatabaseServerIPV4Address AS cDatabaseServerIPV4Address
	FROM dbo.LMS L
		JOIN Dropdown.Event E ON E.EventID = L.EventID
		JOIN Dropdown.EventLocation EL ON EL.EventLocationID = E.EventLocationID
		JOIN Dropdown.EventSponsor ES ON ES.EventSponsorID = E.EventSponsorID
		JOIN Dropdown.EventType ET ON ET.EventTypeID = E.EventTypeID
		JOIN Dropdown.WarfareMission W ON W.warfareMissionID = L.WarfareMissionID
		JOIN JLLIS.dbo.Tier T ON T.TierID = L.OriginatingTierID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = L.CountryID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = @cEntityTypeCode 
			AND KSR.EntityID = L.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
			AND ((@nIsSOLR = 0) OR (@nIsSOLR = 1 AND KSR.IsSOLR = 1))
	ORDER BY L.LMSID DESC

END	
GO
--Begin procedure Reporting.ObservationsByKeyword

--Begin procedure Reporting.ObservationDiscuss
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationDiscuss') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationDiscuss
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to return LMSDiscuss records
-- ============================================================
CREATE PROCEDURE Reporting.ObservationDiscuss
	@nKeywordSearchID int,
	@nTruncateData bit = 1,
	@nLMSID int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMaxLength int
	SET @nMaxLength = 32767

	SELECT
		LD.CreatedBy, 
		LD.CreationDate,
		CONVERT(char(11), LD.CreationDate, 113) AS CreationDateFormatted,

		(SELECT JU.DayPhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateDayPhone,
		(SELECT JU.Email FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateEmail,
		(SELECT JU.FirstName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateFirstName,
		(SELECT JU.LastName FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateLastName,
		(SELECT T.TierLabel FROM JLLIS.dbo.Tier T JOIN JLLIS.dbo.JLLISUser JU ON JU.OriginatingTierID = T.TierID JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateOrganization,
		(SELECT JU.PayGrade FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreatePayGrade,
		(SELECT JU.TollFreePhone FROM JLLIS.dbo.JLLISUser JU JOIN JLLIS.dbo.TierUser TU ON TU.JLLISUserID = JU.JLLISUserID AND TU.TierUserID = LD.CreatedBy AND TU.TierID = (SELECT CAST(S.SetupValue as int) FROM Utility.Setup S WHERE S.SetupKey = 'InstanceID')) AS CreateTollFreePhone,

		LD.LMSDiscussID,
		LD.LMSID,

		CASE
			WHEN @nTruncateData = 1
			THEN LEFT(JLLIS.Utility.StripHTML(LD.Discussion), @nMaxLength)
			ELSE JLLIS.Utility.StripHTML(LD.Discussion)
		END AS Discussion,
	
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.Classification 
			ELSE JLLIS.dbo.GetClassificationByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS Classification,
		
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.OverallCaveat
			ELSE JLLIS.dbo.GetCaveatByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS Caveat,
			
		CASE
			WHEN LD.LMSDiscussClassificationDataID = 0
			THEN LD.ReleasableTo
			ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(LD.LMSDiscussClassificationDataID)
		END AS ReleasableTo
	
	FROM dbo.LMSDiscuss LD
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Observation'
			AND KSR.EntityID = LD.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
			AND LD.LMSID = 
			
				CASE
					WHEN @nLMSID = 0
					THEN LD.LMSID
					ELSE @nLMSID
				END

	ORDER BY LD.LMSID, LD.LMSDiscussID DESC

END
GO
--End procedure Reporting.ObservationDiscuss

--Begin procedure Reporting.ObservationAnalysisCode
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationAnalysisCode') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationAnalysisCode
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2012.03.08
-- Description:	A stored procedure to return AnalysisCode records associated with LMS records
-- ==========================================================================================
CREATE PROCEDURE Reporting.ObservationAnalysisCode
	@nKeywordSearchID int,
	@nLMSID int = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LAC.LMSID,
		ACT.AnalysisCodeType,
		AC.AnalysisCode
	FROM dbo.LMSAnalysisCode LAC
		JOIN Dropdown.AnalysisCode AC ON AC.AnalysisCodeID = LAC.AnalysisCodeID
		JOIN Dropdown.AnalysisCodeType ACT ON ACT.AnalysisCodeTypeID = AC.AnalysisCodeTypeID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Observation'
			AND KSR.EntityID = LAC.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID
			AND LAC.LMSID = 
			
				CASE
					WHEN @nLMSID = 0
					THEN LAC.LMSID
					ELSE @nLMSID
				END
	
	ORDER BY LAC.LMSID, ACT.AnalysisCodeType, AC.AnalysisCode

END
GO
--End procedure Reporting.ObservationAnalysisCode

--Begin procedure Reporting.ObservationMetadata
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationMetadata') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationMetadata
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date: 2012.03.08
-- Description:	A stored procedure to return metadata records associated with LMS records
-- ======================================================================================
CREATE PROCEDURE Reporting.ObservationMetadata
	@nKeywordSearchID int,
	@nLMSID int = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LMD.LMSID,
		MDT.MetadataType,
		MD.Metadata
	FROM dbo.LMSMetadata LMD
		JOIN Dropdown.Metadata MD ON MD.MetadataID = LMD.MetadataID
		JOIN Dropdown.MetadataType MDT ON MDT.MetadataTypeID = MD.MetadataTypeID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Observation'
			AND KSR.EntityID = LMD.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID
			AND LMD.LMSID = 
			
				CASE
					WHEN @nLMSID = 0
					THEN LMD.LMSID
					ELSE @nLMSID
				END
	
	ORDER BY LMD.LMSID, MDT.MetadataType, MD.Metadata

END
GO
--End procedure Reporting.ObservationMetadata

--Begin procedure Reporting.ObservationTask
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationTask') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationTask
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date: 2012.03.08
-- Description:	A stored procedure to return metadata records associated with LMS records
-- ======================================================================================
CREATE PROCEDURE Reporting.ObservationTask
	@nKeywordSearchID int,
	@nLMSID int = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LT.LMSID,
		T.TaskNumber,
		T.TaskTitle
	FROM dbo.LMSTask LT
		JOIN dbo.Task T ON T.TaskID = LT.TaskID
		JOIN JLLIS.Reporting.KeywordSearchResult KSR ON KSR.EntityTypeCode = 'Observation'
			AND KSR.EntityID = LT.LMSID
			AND KSR.KeywordSearchID = @nKeywordSearchID	
			AND LT.LMSID = 
			
				CASE
					WHEN @nLMSID = 0
					THEN LT.LMSID
					ELSE @nLMSID
				END
	
	ORDER BY LT.LMSID, T.TaskNumber, T.TaskTitle

END
GO
--End procedure Reporting.ObservationTask

--Begin procedure Reporting.ObservationExport
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Reporting.ObservationExport') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Reporting.ObservationExport
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2012.03.02
-- Description:	A stored procedure to export an observation
-- ========================================================
CREATE PROCEDURE Reporting.ObservationExport
	@nLMSID int = 0,
	@dDateStart date = NULL,
	@dDateStop date = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nJLLISUserID int
	DECLARE @nKeywordSearchID int

	SELECT @nJLLISUserID = (MAX(JU.JLLISUserID) * 10)
	FROM JLLIS.dbo.JLLISUser JU
	
	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)
	
	INSERT INTO JLLIS.Reporting.KeywordSearch
		(JLLISUserID)
	VALUES
		(@nJLLISUserID)
	
	SELECT @nKeywordSearchID = SCOPE_IDENTITY()

	IF @dDateStart IS NOT NULL
		BEGIN

		IF @dDateStop IS NULL
			SET @dDateStop = getDate()
			
		SET @dDateStop = DATEADD(d, 1, @dDateStop)
		
		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		SELECT
			@nKeywordSearchID,
			'Observation',
			L.LMSID,
			0
		FROM dbo.LMS L
		WHERE L.CreationDate BETWEEN @dDateStart AND @dDateStop
			OR L.UpdateDate BETWEEN @dDateStart AND @dDateStop
		
		END
	ELSE
		BEGIN

		INSERT INTO JLLIS.Reporting.KeywordSearchResult
			(KeywordSearchID,EntityTypeCode,EntityID,IsKeywordSearch)
		VALUES
			(
			@nKeywordSearchID,
			'Observation',
			@nLMSID,
			0
			)
		
		END
		
	--ENDIF	

	EXEC Reporting.ObservationsByKeyword @nKeywordSearchID, 'Observation', 0
	EXEC Reporting.ObservationDiscuss @nKeywordSearchID, 0
	EXEC Reporting.ObservationAnalysisCode @nKeywordSearchID
	EXEC Reporting.ObservationMetadata @nKeywordSearchID
	EXEC Reporting.ObservationTask @nKeywordSearchID

	DELETE 
	FROM JLLIS.Reporting.KeywordSearch
	WHERE JLLISUserID = @nJLLISUserID
	
	DELETE KSR
	FROM JLLIS.Reporting.KeywordSearchResult KSR
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM JLLIS.Reporting.KeywordSearch KS
		WHERE KS.KeywordSearchID = KSR.KeywordSearchID
		)

END
GO
--End procedure Reporting.ObservationExport

--Begin permissions
DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'GRANT EXECUTE ON ' + S.Name + '.' + O.Name + ' To [' + U.Name + ']'
	FROM sys.sysobjects O, dbo.sysusers U
		JOIN sys.schemas S ON S.schema_ID = S.schema_ID
	WHERE S.Name + '.' + O.Name IN
		(
		'Reporting.ObservationExport'
		)
		AND U.Name IN ('CASUser','Dennis.Kretzschmar')
	ORDER BY S.Name, O.Name, U.Name

OPEN oCursor
FETCH oCursor into @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXECUTE (@cSQL)

	FETCH oCursor into @cSQL

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End permissions
