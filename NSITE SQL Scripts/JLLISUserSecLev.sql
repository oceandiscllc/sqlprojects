--Begin PT 518
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.JLLISUser'

EXEC Utility.AddColumn @cTableName, 'IsActive', 'BIT'
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'BIT'
GO

UPDATE JU
SET IsActive = 

	CASE
		WHEN EXISTS (SELECT 1 FROM dbo.TierUser TU WHERE TU.JLLISUserID = JU.JLLISUserID AND TU.Status = 'Active')
		THEN 1
		ELSE 0
	END
	
FROM dbo.JLLISUser JU
GO

--Begin table dbo.JLLISUserSecLev
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.JLLISUserSecLev'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	BEGIN

	CREATE TABLE dbo.JLLISUserSecLev
		(
		JLLISUserSecLevID INT IDENTITY(1,1) NOT NULL,
		JLLISUserID INT,
		TierID INT,
		SecLev INT
		)

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'JLLISUserID', 0
EXEC Utility.SetDefault @cTableName, 'SecLev', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'JLLISUserID', 'INT'
EXEC Utility.SetColumnNotNull @cTableName, 'SecLev', 'INT'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'INT'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'JLLISUserSecLevID'
EXEC Utility.SetIndexClustered 'IX_JLLISUserSecLev', @cTableName, 'JLLISUserID ASC, TierID ASC'
GO

INSERT INTO dbo.JLLISUserSecLev
	(JLLISUserID,TierID,SecLev)
SELECT
	TU.JLLISUserID,
	TU.DefaultTierID,
	
	CASE
		WHEN TU.SecLev = 7000
		THEN 6000
		ELSE TU.SecLev
	END

FROM dbo.TierUser TU
WHERE TU.Status = 'Active'
	AND TU.SecLev > 2000
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.JLLISUserSecLev JUSL
		WHERE JUSL.JLLISUserID = TU.JLLISUserID
			AND JUSL.TierID = TU.DefaultTierID
			AND JUSL.SecLev = TU.SecLev
		)
GROUP BY 
	TU.JLLISUserID,
	TU.DefaultTierID,
	TU.SecLev
GO

DECLARE @cNetworkName VARCHAR (MAX)

SELECT @cNetworkName = SS.ServerSetupValue
FROM dbo.ServerSetup SS
WHERE SS.ServerSetupKey = 'NetworkName'

IF @cNetworkName IN ('Development','Local','QA')
	BEGIN

	DECLARE @tTable TABLE (JLLISUserID INT NOT NULL PRIMARY KEY)
	
	INSERT INTO @tTable
		(JLLISUserID)
	SELECT DISTINCT TU.JLLISUserID
	FROM dbo.TierUser TU
		JOIN dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID
			AND 
				(
				JU.Email LIKE '%nsite%'
					OR JU.Email LIKE '%camber%'
					OR JU.Email LIKE '%pentagon%'
				)
			AND JU.LastName + ', ' + JU.FirstName IN
				(
				'Andrews, Chris',
				'Blades, Cutter',
				'Branum, Justin',
				'Burnham, Jonathan',
				'Cropper, Tom',
				'Demming, David',
				'Fuller, Matt',
				'Goldman, Jeff',
				'Green, Brandon',
				'Hawkins, Donald',
				'Hinners, Catherine',
				'Jones, Eric',
				'Kaminsky, Craig',
				'Lecroy, Dana',
				'Lee, Nikki',
				'Leether, Jim',
				'Louisin, Sean',
				'Lyons, John',
				'Mathurin, Willino',
				'McArthur, Dobie',
				'Muradaz, Rey',
				'Parker, Dan',
				'Pires, Todd',
				'Ready, Christian',
				'Reynolds, Harris',
				'Roberts, David',
				'Ross, Kevin',
				'Todd, Ken',
				'Ward, Jane',
				'Wharton, Yancy',
				'Willard, Jeffrey'
				)
			AND TU.Status = 'Active'

	DELETE JUSL
	FROM dbo.JLLISUserSecLev JUSL
		JOIN @tTable T ON T.JLLISUserID = JUSL.JLLISUserID
		
	INSERT INTO dbo.JLLISUserSecLev 
		(JLLISUserID,SecLev)
	SELECT
		T.JLLISUserID,
		7000
	FROM @tTable T
				
	END
--ENDIF
GO

--Begin function dbo.GetJLLISUserSecLevByJLLISUserID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(N'dbo.GetJLLISUserSecLevByJLLISUserID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetJLLISUserSecLevByJLLISUserID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create Date:	2012.09.18
-- Description:	A function to return a table of tierid and seclev pairs for a given JLLISUserID
-- ============================================================================================

CREATE FUNCTION dbo.GetJLLISUserSecLevByJLLISUserID
(
@nJLLISUserID int
)

RETURNS @tReturn table 
	(
	TierID INT PRIMARY KEY NOT NULL,
	SecLev INT NOT NULL DEFAULT 2000
	) 

AS
BEGIN
	
	DECLARE @nIsSuperAdmin BIT = 0
	DECLARE @nSecLev INT = 2000
	
	IF EXISTS (SELECT 1 FROM dbo.JLLISUserSecLev JUSL WHERE JUSL.JLLISUserID = @nJLLISUserID AND JUSL.SecLev = 7000)
		BEGIN
		
		SET @nIsSuperAdmin = 1
		SET @nSecLev = 7000
		
		END
	--ENDIF
	
	INSERT INTO @tReturn
		(TierID,SecLev)
	SELECT 
		T.TierID,
		@nSecLev
	FROM dbo.Tier T

	IF @nIsSuperAdmin = 0
		BEGIN

		SET @nSecLev = 4000
		IF EXISTS (SELECT 1 FROM JUSL.JLLISUserID = @nJLLISUserID AND JUSL.SecLev = @nSecLev)
			BEGIN
			
			;
			WITH HD (TierID,ParentTierID)
				AS 
				(
				SELECT
					T.TierID, 
					T.ParentTierID
				FROM dbo.Tier T
				WHERE T.TierID IN 
					(
					SELECT JUSL.TierID
					FROM dbo.JLLISUserSecLev JUSL
					WHERE JUSL.JLLISUserID = @nJLLISUserID
						AND JUSL.SecLev = @nSecLev
					)
	
				UNION ALL
				
				SELECT
					T.TierID, 
					T.ParentTierID
				FROM dbo.Tier T
					JOIN HD ON HD.TierID = T.ParentTierID 
				)
	
			UPDATE T
			SET T.SecLev = @nSecLev
			FROM @tReturn T
				JOIN HD ON HD.TierID = T.TierID

			END
		--ENDIF

		SET @nSecLev = 6000
		IF EXISTS (SELECT 1 FROM JUSL.JLLISUserID = @nJLLISUserID AND JUSL.SecLev = @nSecLev)
			BEGIN
			
			;
			WITH HD (TierID,ParentTierID)
				AS 
				(
				SELECT
					T.TierID, 
					T.ParentTierID
				FROM dbo.Tier T
				WHERE T.TierID IN 
					(
					SELECT JUSL.TierID
					FROM dbo.JLLISUserSecLev JUSL
					WHERE JUSL.JLLISUserID = @nJLLISUserID
						AND JUSL.SecLev = @nSecLev
					)
	
				UNION ALL
				
				SELECT
					T.TierID, 
					T.ParentTierID
				FROM dbo.Tier T
					JOIN HD ON HD.TierID = T.ParentTierID 
				)
	
			UPDATE T
			SET T.SecLev = @nSecLev
			FROM @tReturn T
				JOIN HD ON HD.TierID = T.TierID

			END
		--ENDIF
		
		UPDATE T
		SET T.SecLev = JUSL.SecLev
		FROM @tReturn T
			JOIN dbo.JLLISUserSecLev JUSL ON JUSL.TierID = T.TierID
				AND JUSL.JLLISUserID = @nJLLISUserID
				AND JUSL.SecLev NOT IN (7000,6000,4000)

		END
	--ENDIF
		
	RETURN
END
GO
--End function dbo.GetJLLISUserSecLevByJLLISUserID

--Begin function dbo.GetCOPRecordsByJLLISUserID
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(N'dbo.GetCOPRecordsByJLLISUserID') AND O.type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetCOPRecordsByJLLISUserID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create Date:	2012.06.06
-- Description:	A function to manage the buisness rules regarding COP table 
--							record visibility based on JLLISUserID
--
-- Modify Date:	2012.08.29
-- Description:	Added the foreign national exclusion
--
-- Modify Date:	2012.09.18
-- Description:	Implemented the dbo.GetJLLISUserSecLevByJLLISUserID function
-- =========================================================================

CREATE FUNCTION dbo.GetCOPRecordsByJLLISUserID
(
@nJLLISUserID int
)

RETURNS @tReturn table 
	(
	COPID int primary key NOT NULL,
	IsContributor bit NOT NULL DEFAULT 0,
	IsFavorite bit NOT NULL DEFAULT 0,
	CanHaveDelete bit NOT NULL DEFAULT 0,
	CanHaveEdit bit NOT NULL DEFAULT 0,
	CanRequestAccess bit NOT NULL DEFAULT 0
	) 

AS
BEGIN
	DECLARE @cISOCountryCode char(3)
	DECLARE @nDefaultTierID int
	DECLARE @nSecLev int
	DECLARE @nTargetTierID int
	DECLARE @nTierID int

	DECLARE @tTable1 TABLE
		(
		TierID int NOT NULL PRIMARY KEY,
		SecLev int NOT NULL DEFAULT 2000
		)

	SELECT @cISOCountryCode = C.ISOCode3
	FROM dbo.JLLISUser JU
		JOIN Dropdown.Country C ON C.CountryID = JU.CountryID
			AND JU.JLLISUserID = @nJLLISUserID
		
	INSERT INTO @tTable1
		(TierID,SecLev)
	SELECT
		T.TierID,
		T.SecLev
	FROM dbo.GetJLLISUserSecLevByJLLISUserID(@nJLLISUserID) T
	
	INSERT INTO @tReturn
		(COPID,CanHaveDelete,CanHaveEdit,IsContributor)
	SELECT
		C.COPID,
		1,
		1,
		0
	FROM COP.COP C
		JOIN @tTable1 T1 ON T1.TierID = C.OriginatingTierID
			AND T1.SecLev IN (7000,6000)

	UNION
	
	SELECT
		C.COPID,
		1,
		1,
		0
	FROM COP.COP C
		JOIN COP.COPJllisUser CJU ON CJU.COPID = C.COPID
		JOIN Dropdown.COPRole CR ON CR.COPRoleID = CJU.COPRoleID
			AND CJU.COPID = C.COPID
			AND CJU.JLLISUserID = @nJLLISUserID
			AND CR.COPRoleCode = 'COPManager'
		JOIN Dropdown.Status S ON S.StatusID = C.StatusID
			AND S.Status <> 'Deleted'

	UNION
	
	SELECT
		C.COPID,
		0,
		0,
		1
	FROM COP.COP C
		JOIN @tTable1 T1 ON T1.TierID = C.OriginatingTierID
			AND T1.SecLev NOT IN (7000,6000)
		JOIN COP.COPJllisUser CJU ON CJU.COPID = C.COPID
		JOIN Dropdown.COPRole CR ON CR.COPRoleID = CJU.COPRoleID
			AND CJU.COPID = C.COPID
			AND CJU.JLLISUserID = @nJLLISUserID
			AND CR.COPRoleCode = 'COPContributor'
		JOIN Dropdown.Status S ON S.StatusID = C.StatusID
			AND S.Status = 'Active'

	INSERT INTO @tReturn
		(COPID)
	SELECT
		C.COPID
	FROM COP.COP C
		JOIN Dropdown.COPType CT ON CT.COPTypeID = C.COPTypeID
			AND CT.COPType IN ('Internal', 'Joint')
		JOIN Dropdown.Status S ON S.StatusID = C.StatusID
			AND S.Status = 'Active'
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tReturn R 
				WHERE R.COPID = C.COPID
				)

	INSERT INTO @tReturn
		(COPID,CanRequestAccess)
	SELECT
		C.COPID,
		1
	FROM COP.COP C
		JOIN Dropdown.COPType CT ON CT.COPTypeID = C.COPTypeID
			AND CT.COPType = 'Restricted'
		JOIN Dropdown.Status S ON S.StatusID = C.StatusID
			AND S.Status = 'Active'
		JOIN dbo.TierSetup TS ON TS.TierID = C.OriginatingTierID
			AND TS.TierSetupKey = 'RestrictedCOPAccessRequestType'
			AND TierSetupValue = 'TierEnabled'
			AND NOT EXISTS
				(
				SELECT 1
				FROM COP.COPJllisUser CJU
				WHERE CJU.COPID = C.COPID
					AND CJU.JLLISUserID = @nJLLISUserID
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tReturn R 
				WHERE R.COPID = C.COPID
				)

	UNION
	
	SELECT
		C.COPID,
		1
	FROM COP.COP C
		JOIN Dropdown.COPType CT ON CT.COPTypeID = C.COPTypeID
			AND CT.COPType = 'Restricted'
		JOIN Dropdown.Status S ON S.StatusID = C.StatusID
			AND S.Status = 'Active'
			AND C.CanRequestAccess = 1
			AND NOT EXISTS
				(
				SELECT 1
				FROM COP.COPJllisUser CJU
				WHERE CJU.COPID = C.COPID
					AND CJU.JLLISUserID = @nJLLISUserID
				)
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tReturn R 
				WHERE R.COPID = C.COPID
				)
	
	UPDATE R
	SET R.IsFavorite = 1
	FROM @tReturn R 
		JOIN COP.Favorite F ON F.COPID = R.COPID
			AND F.JLLISUserID = @nJLLISUserID

	IF @cISOCountryCode <> 'USA'
		BEGIN

		DELETE R1
		FROM @tReturn R1
		WHERE EXISTS
			(
			SELECT 1
			FROM @tReturn R2
				JOIN COP.COP C ON C.COPID = R2.COPID
					AND R2.COPID = R1.COPID
					AND 
						(
						dbo.IsRestrictedCaveat(C.COPClassificationDataID) = 1
							OR LEN(RTRIM(dbo.GetReleasableToListByClassificationDataID(C.COPClassificationDataID))) > 0
						)
			)

		END
	--ENDIF
	
	RETURN
END
GO
--End function dbo.GetCOPRecordsByJLLISUserID
--End PT 518
