USE [JLLIS]
GO
TRUNCATE TABLE [dbo].[ServerSetup]
GO

SET IDENTITY_INSERT [dbo].[ServerSetup] ON 

GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (1, N'ApplicationDSN', N'JSCC', CAST(N'2013-08-20 13:50:45.463' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (3, N'DatabaseServerDomainName', N'Denon.nsitellc.com', CAST(N'2013-08-20 13:50:45.463' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (5, N'JCISFAWebServiceTierID', N'960', CAST(N'2013-08-20 13:50:45.463' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (7, N'NIPRContactEmail', N'js.pentagon.j7.mbx.jllis-coordinator@mail.mil', CAST(N'2013-08-20 13:50:45.463' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (9, N'SIPRContactEmail', N'js.pentagon.j7.mbx.jllis-coordinator@mail.smil.mil', CAST(N'2013-08-20 13:50:45.467' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (11, N'SSRSPassword', N')(*&UIOP0987uiop', CAST(N'2013-08-20 13:50:45.467' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (12, N'SSRSReportServerPath', N'ReportServer_DevCurrent', CAST(N'2013-08-20 13:50:45.467' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (13, N'SSRSUserName', N'ssrsadmin', CAST(N'2013-08-20 13:50:45.467' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (14, N'TierUserInstanceID', N'8', CAST(N'2013-08-20 13:50:45.467' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (16, N'authMethod', N'form', CAST(N'2013-08-20 14:09:30.300' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (18, N'SiteURL', N'https://dev-current.nsitellc.com', CAST(N'2013-08-28 11:55:42.373' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (19, N'NoReplyAtJLLISMil', N'noreply.dev.current@jllis.mil', CAST(N'2013-08-28 12:04:11.050' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (20, N'InstanceDatabaseName', N'JSCC', CAST(N'2013-09-09 10:14:50.823' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (23, N'ContactUsEmail', N'js.pentagon.j7.mbx.jllis-coordinator@mail.mil', CAST(N'2013-10-24 13:29:15.600' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (27, N'EnvironmentClassification', N'UNCLASSIFIED', CAST(N'2013-10-28 10:10:45.540' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (28, N'NetworkName', N'DEVELOPMENT', CAST(N'2013-10-28 10:10:56.073' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (29, N'ListServAdminAddress', N'NoReply@nsitellc.com', CAST(N'2014-01-24 15:38:15.530' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (30, N'ListServOwnerAddress', N'NoReply@nsitellc.com', CAST(N'2014-01-24 15:38:35.630' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (31, N'SolrServerIPV4Address', N'10.10.100.211', CAST(N'2014-05-01 12:57:21.970' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (32, N'DatabaseServerIPV4Address', N' 10.10.100.201\DEVCURRENT', CAST(N'2014-05-01 14:54:19.173' AS DateTime))
GO
INSERT [dbo].[ServerSetup] ([ServerSetupID], [ServerSetupKey], [ServerSetupValue], [CreateDate]) VALUES (34, N'DBAdminPassword', N'muddd0bb3r', CAST(N'2014-06-05 11:28:58.650' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ServerSetup] OFF
GO
