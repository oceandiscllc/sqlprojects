DECLARE @nMetricsReportID bigint

SET @nMetricsReportID = 1

SELECT 
	COUNT(L.LMSID) AS ItemCount,
	CAST(YEAR(L.CreationDate) as char(4)) + '-' + RIGHT('00' + CAST(MONTH(L.CreationDate) as varchar(2)), 2) AS CreateYearMonth, 
	T.TierLabel AS Organization
FROM dbo.LMS L WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON L.OriginatingTierID = T.TierID
		AND L.CreationDate 
			BETWEEN 
				(SELECT MR.CreationDateStart FROM JLLIS.Utility.MetricsReport MR WITH (NOLOCK) WHERE MR.MetricsReportID = @nMetricsReportID)
			AND 
				(SELECT MR.CreationDateStop FROM JLLIS.Utility.MetricsReport MR WITH (NOLOCK) WHERE MR.MetricsReportID = @nMetricsReportID)
		AND EXISTS
			(
			SELECT 1
			FROM JLLIS.Utility.MetricsReportData MRD WITH (NOLOCK)
				JOIN JLLIS.Dropdown.Status S WITH (NOLOCK) ON S.StatusID = MRD.EntityID
					AND L.Status = S.Status
					AND MRD.EntityID = S.StatusID
					AND MRD.EntityTypeCode = 'Status'
					AND MRD.MetricsReportID = @nMetricsReportID
			)
		AND EXISTS
			(
			SELECT 1
			FROM JLLIS.Utility.MetricsReportData MRD WITH (NOLOCK)
			WHERE L.OriginatingTierID = MRD.EntityID
				AND MRD.EntityTypeCode = 'Tier'
				AND MRD.MetricsReportID = @nMetricsReportID
			)
GROUP BY YEAR(L.CreationDate), MONTH(L.CreationDate), T.TierLabel
ORDER BY YEAR(L.CreationDate), MONTH(L.CreationDate), T.TierLabel