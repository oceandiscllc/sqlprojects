﻿USE USSOCOM
GO
PRINT N' Creating Functions For AAR'
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetClassificationWeight') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetClassificationWeight
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:   Todd Pires
-- Create date: 2010.06.22
-- Description: A function to return an integer value representing a varchar classification label
-- ==============================================================================================

CREATE FUNCTION dbo.GetClassificationWeight
(
@cClassificationLabel varchar(50)
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
SET @nRetVal = 0

IF LEN(RTRIM(@cClassificationLabel)) > 0 AND @cClassificationLabel IS NOT NULL
 BEGIN
 
 SET @nRetVal = 
  (
  SELECT classification_weight
  FROM dbo.classifications
  WHERE classification_label = @cClassificationLabel
  )
 
 END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetMaxInteger') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetMaxInteger
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:   Todd Pires/John Lyons
-- Create date: 2010.06.22
-- Description: A function to return the greater (max) of two integers
-- ===================================================================

CREATE FUNCTION dbo.GetMaxInteger
(
@nValue1 int,
@nValue2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int

SET @nValue1 = ISNULL(@nValue1, 0)            
SET @nValue2 = ISNULL(@nValue2, 0)            

IF @nValue1 >= @nValue2                  
 SET @nRetVal = @nValue1
ELSE                  
 SET @nRetVal = @nValue2
--ENDIF

RETURN @nRetVal

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetReleaseableToWeight') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetReleaseableToWeight
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:   Todd Pires
-- Create date: 2010.06.22
-- Description: A function to return an integer value representing a varchar releaseableto label
-- ==============================================================================================

CREATE FUNCTION dbo.GetReleaseableToWeight
(
@creleaseabletoLabel varchar(50)
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
SET @nRetVal = 0

IF LEN(RTRIM(@creleaseabletoLabel)) > 0 AND @creleaseabletoLabel IS NOT NULL
 BEGIN
 
 SET @nRetVal = 
  (
  SELECT releaseableto_weight
  FROM dbo.releaseableto
  WHERE releaseableto_label = @creleaseabletoLabel
  )
 
 END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

Print N' Function Creation Done'
GO 
PRINT N'Creating and Populating Tables'

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.classifications') AND type in (N'U'))
	DROP TABLE dbo.classifications
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.classifications
	(
	classificationid int IDENTITY(1,1) NOT NULL,
	classification_weight int NOT NULL,
	classification_label nvarchar(50) NOT NULL,
	isactive bit NOT NULL,
	CONSTRAINT PK_classifications PRIMARY KEY CLUSTERED 
		(
		classificationid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.releaseableto') AND type in (N'U'))
	DROP TABLE dbo.releaseableto
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.releaseableto
	(
	releaseabletoid int IDENTITY(1,1) NOT NULL,
	releaseableto_weight int NOT NULL,
	releaseableto_label nvarchar(50) NOT NULL,
	isactive bit NOT NULL,
	CONSTRAINT PK_releaseableto PRIMARY KEY CLUSTERED 
		(
			releaseabletoid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

INSERT INTO dbo.classifications(classification_weight, classification_label, isactive) VALUES (3, N'secret', 1)
INSERT INTO dbo.classifications(classification_weight, classification_label, isactive) VALUES (2, N'confidential', 1)
INSERT INTO dbo.classifications(classification_weight, classification_label, isactive) VALUES (1, N'unclassified', 1)
INSERT INTO dbo.classifications(classification_weight, classification_label, isactive) VALUES(0, N'', 1)
GO

INSERT INTO dbo.releaseableto(releaseableto_weight, releaseableto_label, isactive) VALUES (3, N'NOFORN', 1)
INSERT INTO dbo.releaseableto(releaseableto_weight, releaseableto_label, isactive) VALUES (0, N'', 1)
GO