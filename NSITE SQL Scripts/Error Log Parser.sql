INSERT INTO dbo.ErrorLog
	(ErrorCode, ErrorDateTime, DatabaseName, ErrorMessage)
SELECT
	REPLACE(dbo.ListGetAt(Line, 2, ','), '"', ''),
	REPLACE(dbo.ListGetAt(Line, 3, ','), '"', '') + ' ' + REPLACE(dbo.ListGetAt(Line, 4, ','), '"', ''),
	REPLACE(dbo.ListGetAt(Line, 5, ','), '"', ''),
	REPLACE(dbo.ListGetAt(Line, 6, ','), '"', '')
FROM dbo.ReadFileAsTable('C:\www\nsite\html\cmt\Documents', 'exception.log') 
WHERE LEFT(Line, 7) = '"Error"'
	AND NOT EXISTS 
		(
		SELECT 1
		FROM dbo.ErrorLog
		WHERE ErrorCode = REPLACE(dbo.ListGetAt(Line, 2, ','), '"', '')
			AND ErrorDateTime = REPLACE(dbo.ListGetAt(Line, 3, ','), '"', '') + ' ' + REPLACE(dbo.ListGetAt(Line, 4, ','), '"', '')
			AND DatabaseName = REPLACE(dbo.ListGetAt(Line, 5, ','), '"', '')
			AND ErrorMessage = REPLACE(dbo.ListGetAt(Line, 6, ','), '"', '')
		)
ORDER BY Line