DECLARE @cCRLF varchar(2)
DECLARE @cSQL varchar(max)
DECLARE @nServerSetupID int

SET @cCRLF = CHAR(13) + CHAR(10)

SET @cSQL = 'TRUNCATE TABLE JLLIS.dbo.ServerSetup' + @cCRLF
SET @cSQL += 'GO' + @cCRLF

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT SS.ServerSetupID
	FROM JLLIS.dbo.ServerSetup SS
	ORDER BY SS.ServerSetupID
	
OPEN oCursor
FETCH oCursor INTO @nServerSetupID
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL += 
		(	
		SELECT
			'INSERT INTO JLLIS.dbo.ServerSetup (ServerSetupKey, ServerSetupValue) VALUES (' + '''' + SS.ServerSetupKey + ''',' + '''' + SS.ServerSetupValue + ''')' + @cCRLF
		FROM JLLIS.dbo.ServerSetup SS
		WHERE SS.ServerSetupID = @nServerSetupID
		)

	FETCH oCursor INTO @nServerSetupID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

SET @cSQL += 'GO'
	
print @cSQL 
	