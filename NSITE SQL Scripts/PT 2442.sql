--Begin PT 2442
--Begin function dbo.GetSODARSRecordsByUserID
EXEC Utility.DropObject 'dbo.GetSODARSRecordsByUserID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2013.09.17
-- Description:	A function to manage the buisness rules regarding SODARS table record visibility based on TierUserID
--
-- Author:			Todd Pires
-- Modify Date:	2013.10.23
-- Description:	Fixed the seclev 6000 bug and added support for some the additional requirements on PT 2442
-- =================================================================================================================

CREATE FUNCTION dbo.GetSODARSRecordsByUserID
(
@nUserID INT
)

RETURNS @tReturn table 
	(
	SODARSID INT PRIMARY KEY NOT NULL,
	CanHaveDelete BIT NOT NULL DEFAULT 0,
	CanHaveEdit BIT NOT NULL DEFAULT 0,
	CanHaveHistory BIT NOT NULL DEFAULT 0
	) 

AS
BEGIN

	DECLARE @cISOCountryCode CHAR(3)
	DECLARE @nDefaultTierID INT
	DECLARE @nSecLev INT

	SELECT 
		@cISOCountryCode = C.ISOCode3,
		@nDefaultTierID = TU.DefaultTierID,
		@nSecLev = TU.SecLev
	FROM JLLIS.dbo.TierUser TU
		JOIN JLLIS.dbo.JLLISUser JU ON JU.JLLISUserID = TU.JLLISUserID
		JOIN JLLIS.Dropdown.Country C ON C.CountryID = JU.CountryID
			AND TU.TierID = JLLIS.dbo.GetTierUserInstanceID()
			AND TU.TierUserID = @nUserID

	IF @nSecLev = 7000
		BEGIN

		INSERT INTO @tReturn
			(SODARSID,CanHaveDelete,CanHaveEdit,CanHaveHistory)
		SELECT
			S.SODARSID,
			1,
			1,
			1
		FROM dbo.SODARS S
		
		END
	ELSE
		BEGIN

		IF @nSecLev = 6000
			BEGIN

			INSERT INTO @tReturn
				(SODARSID,CanHaveDelete,CanHaveEdit,CanHaveHistory)
			SELECT
				S.SODARSID,
				1,
				1,
				1
			FROM dbo.SODARS S
			WHERE EXISTS
				(
				SELECT 1
				FROM JLLIS.dbo.GetDescendantTiersByTierID((SELECT T.TierID FROM JLLIS.dbo.Tier T WHERE T.TierName LIKE '%SOCOM' AND T.IsInstance = 1)) DT
				WHERE DT.TierID = (@nDefaultTierID)

				UNION
				
				SELECT 1
				FROM JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'SODARS') ME
				WHERE ME.EntityID = S.SODARSID
				)
				
			END
		--ENDIF

		INSERT INTO @tReturn
			(SODARSID,CanHaveEdit)
		SELECT
			S.SODARSID,
			
			CASE
				WHEN S.UserID = @nUserID AND S.Status = 'Draft'
				THEN 1
				ELSE 0
			END

		FROM dbo.SODARS S
		WHERE 
			(
			S.Status IN ('Active','Closed','Validated')
				OR 
					(
					S.UserID = @nUserID 
						AND S.Status = 'Draft'
					)
				OR 
					(
					S.Status = 'Restricted'
						AND EXISTS
							(
							SELECT 1
							FROM JLLIS.dbo.GetMemberEntities(JLLIS.dbo.GetJLLISUserIDFromTierUserID(@nUserID), 'SODARS') ME
							WHERE ME.EntityID = S.SODARSID
							)
					)
			)
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tReturn R
				WHERE R.SODARSID = S.SODARSID
				)

		END
	--ENDIF
					
	RETURN

END
GO
--End function dbo.GetSODARSRecordsByUserID
--End PT 2442