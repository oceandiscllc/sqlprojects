USE JLLIS
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetPersonNameByUserID') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetPersonNameByUserID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2010.06.22
-- Description:	A function to return the name of a user in a specified format from a userid
-- ========================================================================================

CREATE FUNCTION dbo.GetPersonNameByUserID
(
@nUserID int,
@cUserIDType varchar(50),
@cTierName varchar(50),
@cFormat varchar(50)
)

RETURNS varchar(250)

AS
BEGIN

DECLARE @cRetVal varchar(250)
SET @cRetVal = ''

IF @nUserID IS NOT NULL AND @nUserID > 0
	BEGIN
	
	IF @cUserIDType = 'TierUserID'
		BEGIN

		SET @nUserID = 
			(
			SELECT TU.JLLISUserID
			FROM dbo.TierUser TU WITH (NOLOCK)
				JOIN dbo.Tier T WITH (NOLOCK) ON T.TierID = TU.TierID
					AND T.TierName = @cTierName
					AND TU.TierUserID = @nUserID
			)
		
		SET @nUserID = ISNULL(@nUserID, 0)
		
		END
	--ENDIF
	
	SET @cRetVal = 
		(
		SELECT

			CASE
				WHEN @cFormat = 'FirstLast'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.FirstName) + ' ' + LTRIM(JU.LastName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END

				WHEN @cFormat = 'LastFirst'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.LastName) + ', ' + LTRIM(JU.FirstName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END
			END
						
		FROM dbo.JLLISUser JU
		WHERE JU.jllisuserid = @nUserID
		)
		
	END
--ENDIF

RETURN ISNULL(@cRetVal, '')

END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetSecLevByUserIDAndTierName') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetSecLevByUserIDAndTierName
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2010.06.23
-- Description:	A function to return the SecLev of a specific UserID
-- =================================================================

CREATE FUNCTION dbo.GetSecLevByUserIDAndTierName
(
@nUserID int,
@cTierName varchar(50)
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
SET @nRetVal = 0

IF @nUserID IS NOT NULL AND @nUserID > 0
	BEGIN
	
	SET @nRetVal = 
		(
		SELECT TU.seclev
		FROM dbo.TierUser TU WITH (NOLOCK)
			JOIN dbo.Tier AS T WITH (NOLOCK) ON T.TierID = TU.TierID
				AND T.TierName = @cTierName
				AND TU.TierUserID = @nUserID
		)
		
	END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

USE USSOCOM
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.IsEntityMember') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.IsEntityMember
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2010.07.09
-- Description:	A function to return a bit indicating membership in an entity
-- ==========================================================================

CREATE FUNCTION dbo.IsEntityMember
(
@nEntityID int,
@cEntityTypeCode varchar(10),
@nMemberID int,
@cMemberTypeCode varchar(10)
)

RETURNS bit

AS
BEGIN

DECLARE @nRetVal bit
SET @nRetVal = 0

IF @nEntityID IS NOT NULL AND @nEntityID > 0 AND @nMemberID IS NOT NULL AND @nMemberID > 0
	BEGIN
	
	IF EXISTS	-- Is the member associatied with the entity
		(
		SELECT 1
		FROM dbo.EntityMembers EM WITH (NOLOCK)
		WHERE EM.EntityID = @nEntityID
			AND EM.EntityTypeCode = @cEntityTypeCode
			AND EM.MemberID = @nMemberID
			AND EM.MemberTypeCode = @cMemberTypeCode
		)
		
	OR
		(
		@cMemberTypeCode = 'USER' AND -- If the member is a USER
			(
			EXISTS -- Is the member associatied with a unit that is associatied with the entity
				(		
				SELECT 1
				FROM dbo.EntityMembers EM WITH (NOLOCK)
					JOIN dbo.Unit U ON U.UnitID = EM.MemberID
						AND EM.EntityID = @nEntityID
						AND EM.MemberTypeCode = 'UNIT'
					JOIN JLLIS.dbo.JLLISUser JU WITH (NOLOCK) ON JU.Unit = U.UnitName
					JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
						AND TU.TierUserID = @nMemberID
				)

			OR
	
			EXISTS -- Is the member associatied with a user group that is associatied with the entity
				(	
				SELECT 1
				FROM dbo.EntityMembers EM WITH (NOLOCK)
					JOIN dbo.UserGroups UG WITH (NOLOCK) ON UG.UserGroupID = EM.MemberID
						AND EM.EntityID = @nEntityID
						AND EM.MemberID = @nMemberID
						AND EM.MemberTypeCode = 'USERGROUP'
				)

			OR		
		
			EXISTS -- Is the member associatied with a unit that is associatied with a user group that is associatied with the entity
				(
				SELECT 1
				FROM dbo.EntityMembers EM1 WITH (NOLOCK)
					JOIN dbo.UserGroups UG WITH (NOLOCK) ON UG.UserGroupID = EM1.MemberID
						AND EM1.EntityID = @nEntityID
						AND EM1.MemberID = 
							(
							SELECT EM2.EntityID
							FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) 
								JOIN dbo.Unit U ON U.UnitName = JU.Unit
								JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
									AND TU.TierUserID = @nMemberID
								JOIN dbo.EntityMembers EM2 WITH (NOLOCK) ON EM2.MemberID = U.UnitID 
									AND EM2.EntityTypeCode = 'USERGROUP'
									AND EM2.MemberTypeCode = 'UNIT'
							)
						AND EM1.MemberTypeCode = 'USERGROUP'
				)
			)	
		)
		BEGIN
		
		SET @nRetVal = 1
		
		END
	--ENDIF
	
	END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WITH (NOLOCK) WHERE id = OBJECT_ID(N'dbo.entitymembers') AND type in (N'U'))
	DROP TABLE dbo.entitymembers
GO
	
CREATE TABLE dbo.entitymembers
	(
	entitymemberid int IDENTITY(1,1) NOT NULL,
	entityid int CONSTRAINT DF_entitymembers_entityid NOT NULL DEFAULT 0,
	entitytypecode varchar(50) CONSTRAINT DF_entitymembers_entitytypecode NOT NULL,
	memberid int CONSTRAINT DF_entitymembers_memberid NOT NULL DEFAULT 0,
	membertypecode varchar(50) CONSTRAINT DF_entitymembers_membertypecode NOT NULL,
	CONSTRAINT PK_entitymembers PRIMARY KEY NONCLUSTERED 
		(
		entitymemberid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
CREATE CLUSTERED INDEX IX_entitymembers ON dbo.entitymembers 
	(
	entityid ASC,
	entitytypecode ASC,
	memberid ASC,
	membertypecode ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
		
IF EXISTS (SELECT 1 FROM dbo.sysobjects WITH (NOLOCK) WHERE id = OBJECT_ID(N'dbo.usergroups') AND type in (N'U'))
	DROP TABLE dbo.usergroups
GO

CREATE TABLE dbo.usergroups
	(
	usergroupid [int] IDENTITY(1,1) NOT NULL,
	usergroupname varchar(250) NOT NULL,
	usergroupdescription varchar(max) NULL,
	usergroupstatusid int CONSTRAINT DF_usergroups_usergroupstatusid NOT NULL DEFAULT 0,
	createdat datetime CONSTRAINT DF_usergroups_createdat NOT NULL DEFAULT getDate(),
	createdby int CONSTRAINT DF_usergroups_createdby NOT NULL DEFAULT 0,
	updatedat datetime CONSTRAINT DF_usergroups_updatedat NOT NULL DEFAULT getDate(),
	updatedby int CONSTRAINT DF_usergroups_updatedby NOT NULL DEFAULT 0,
	CONSTRAINT [PK_usergroups] PRIMARY KEY CLUSTERED 
		(
		usergroupid ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	
CREATE UNIQUE NONCLUSTERED INDEX IX_UserGroupName ON dbo.usergroups 
	(
	UserGroupName ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

DECLARE @cTitle varchar(50)
SET @cTitle = 'MY GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	IF EXISTS (SELECT 1 FROM dbo.menuitem MI WITH (NOLOCK) WHERE MI.title = 'MY SODARS')
		BEGIN
		
		INSERT INTO dbo.menuitem
			(
			menuid,
			parentid,
			level,
			title
			)
		SELECT
			MI.MenuID,
			MI.parentid,
			MI.Level + 1 AS level,
			@cTitle AS title
		FROM dbo.menuitem MI WITH (NOLOCK)
		WHERE MI.title = 'MY SODARS'
		
		END
	ELSE
		BEGIN

		INSERT INTO dbo.menuitem
			(
			menuid,
			parentid,
			level,
			title
			)
		SELECT
			MI.MenuID,
			MI.parentid,
			MI.Level + 1 AS level,
			@cTitle AS title
		FROM dbo.menuitem MI WITH (NOLOCK)
		WHERE MI.title = 'MY FORMS'

		END
	--ENDIF
			
	END
--ENDIF

SET @cTitle = 'VIEW MY GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID AS parentid,
		1 AS level,
		'index.cfm?disp=UserGroups&Action=ViewMyUserGroups' AS link,
		@cTitle AS title
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
 
	END
--ENDIF

SET @cTitle = 'SEARCH GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID AS parentid,
		2 AS level,
		'index.cfm?disp=UserGroups&Action=SearchUserGroups' AS link,
		@cTitle AS title
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
		
	END
--ENDIF

SET @cTitle = 'ADD NEW GROUP'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID AS parentid,
		3 AS level,
		'index.cfm?disp=UserGroups&Action=AddUpdateUserGroup' AS link,
		@cTitle AS title
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
		
	END
--ENDIF
GO

DECLARE @cCategory varchar(50)
SET @cCategory = 'UserGroupStatus'

DELETE 
FROM dbo.dropdown
WHERE category = @cCategory

INSERT INTO dbo.dropdown (category,displaytext,displayorder) VALUES (@cCategory,'Active',1000)
INSERT INTO dbo.dropdown (category,displaytext,displayorder) VALUES (@cCategory,'Hold',1050)
INSERT INTO dbo.dropdown (category,displaytext,displayorder) VALUES (@cCategory,'Deleted',1100)

IF NOT EXISTS (SELECT 1 FROM dbo.dropdown WHERE ID = 0)
	BEGIN
	
	SET IDENTITY_INSERT dbo.dropdown ON

	INSERT INTO dbo.dropdown (ID) VALUES (0)
	
	SET IDENTITY_INSERT dbo.dropdown OFF
	
	END
--ENDIF
GO

INSERT INTO dbo.EntityMembers
	(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
SELECT
	DisplayOrder AS EntityID,
	R.Type AS EntityTypeCode,
	UserID AS MemberID,
	'USER' AS MemberTypeCode
FROM dbo.Ref R WITH (NOLOCK)
WHERE R.Type IN ('BINDER','COI')
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.EntityMembers EM WITH (NOLOCK)
		WHERE EM.EntityID = R.DisplayOrder
			AND EM.EntityTypeCode = R.Type
			AND EM.MemberID = R.UserID
		)
ORDER BY DisplayOrder, UserID
GO

INSERT INTO dbo.EntityMembers
	(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
SELECT
	DisplayOrder AS EntityID,
	'DYNAMICFORM' AS EntityTypeCode,
	UserID AS MemberID,
	'USER' AS MemberTypeCode
FROM dbo.Ref R WITH (NOLOCK)
WHERE R.Type = 'dynamic_form'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.EntityMembers EM WITH (NOLOCK)
		WHERE EM.EntityID = R.DisplayOrder
			AND EM.EntityTypeCode = 'DYNAMICFORM'
			AND EM.MemberID = R.UserID
		)
ORDER BY DisplayOrder, UserID
GO

INSERT INTO dbo.EntityMembers
	(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
SELECT
	DisplayOrder AS EntityID,
	'SODAR' AS EntityTypeCode,
	UserID AS MemberID,
	'USER' AS MemberTypeCode
FROM dbo.Ref R WITH (NOLOCK)
WHERE R.Type = 'SODARS'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.EntityMembers EM WITH (NOLOCK)
		WHERE EM.EntityID = R.DisplayOrder
			AND EM.EntityTypeCode = 'SODAR'
			AND EM.MemberID = R.UserID
		)
ORDER BY DisplayOrder, UserID
GO