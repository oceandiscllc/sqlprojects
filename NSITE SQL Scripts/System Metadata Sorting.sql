DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(T.SystemMetadataID) AS VARCHAR(50)))
FROM JLLIS.Dropdown.SystemMetadata T

;
WITH HD (DisplayIndex,Lineage,SystemMetadataID,ParentSystemMetadataID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY T.DisplayOrder, T.SystemMetadata) AS VARCHAR(10)), @nPadLength)),
		CAST(T.SystemMetadata AS VARCHAR(MAX)),
		T.SystemMetadataID,
		T.ParentSystemMetadataID,
		1
	FROM JLLIS.Dropdown.SystemMetadata T
	WHERE T.ParentSystemMetadataID = 0
		--AND T.SystemMetadataID = 25

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY T.DisplayOrder, T.SystemMetadata) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.Lineage  + ' > ' + T.SystemMetadata AS VARCHAR(MAX)),
		T.SystemMetadataID,
		T.ParentSystemMetadataID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM JLLIS.Dropdown.SystemMetadata T
		JOIN HD ON HD.SystemMetadataID = T.ParentSystemMetadataID
	)

SELECT
	HD1.DisplayIndex,
	HD1.NodeLevel,
	HD1.ParentSystemMetadataID,
	HD1.Lineage,
	HD1.SystemMetadataID,
	T.SystemMetadata,
	T.ForegroundColor,
	T.BackgroundColor,
	
	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentSystemMetadataID = HD1.SystemMetadataID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN JLLIS.Dropdown.SystemMetadata T ON T.SystemMetadataID = HD1.SystemMetadataID
ORDER BY HD1.DisplayIndex
