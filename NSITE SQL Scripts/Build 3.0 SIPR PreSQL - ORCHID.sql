USE ORCHID
GO

BEGIN TRANSACTION

UPDATE JLLIS.dbo.ClassificationData
SET 
	TempID = 0,
	TempFieldName = NULL

INSERT INTO JLLIS.dbo.ClassificationData
	(ClassificationID,CaveatID,TempID,TempFieldName)
SELECT
	(
	SELECT C1.ClassificationID
	FROM JLLIS.Dropdown.Classification C1
	WHERE C1.Classification = 'Unclassified'
	),
	0,
	GMSF.FileID,
	'GMSFileClassificationDataID'
FROM dbo.GMSFile GMSF
WHERE GMSF.GMSFileClassificationDataID = 0
	
UPDATE GMSF
SET GMSF.GMSFileClassificationDataID = CD.ClassificationDataID
FROM dbo.GMSFile GMSF
	JOIN JLLIS.dbo.ClassificationData CD ON CD.TempID = GMSF.FileID
		AND CD.TempFieldName = 'GMSFileClassificationDataID'
		
COMMIT TRANSACTION