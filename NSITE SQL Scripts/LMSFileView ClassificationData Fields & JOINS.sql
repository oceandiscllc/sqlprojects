SELECT
	'/* Begin ' + CDM.Prefix + ' ' + CDM.ClassificationDataCode + ' */' + 
	
	CASE
		WHEN CDM.ClassificationDataCode = 'ReleasableTo'
		THEN 
			'~CASE' +
			'~^WHEN L.' + CDM.Prefix + 'ClassificationDataID = 0' + 
			'~^THEN L.' + CDM.ColumnName + 
			'~^ELSE JLLIS.dbo.GetReleasableToListByClassificationDataID(' + CDM.Prefix + 'ClassificationData.ClassificationDataID' + ')' + 
			'~END AS ' + CDM.ColumnName + ','
		ELSE 
			'~CASE' +
			'~^WHEN L.' + CDM.Prefix + 'ClassificationDataID = 0' + 
			'~^THEN L.' + CDM.ColumnName + 
			'~^ELSE ' + CDM.Prefix + CDM.ClassificationDataCode + '.' + CDM.ClassificationDataCode +
			'~END AS ' + CDM.ColumnName + ','
	END

	+ '~/* End ' + CDM.Prefix + ' ' + CDM.ClassificationDataCode + ' */~' AS SQLText
FROM JLLIS.Utility.ClassificationDataMapping CDM
WHERE CDM.SchemaName = 'dbo'
	AND CDM.TableName = 'LMS'
ORDER BY CDM.Prefix,CDM.ClassificationDataCode

SELECT
	'/* Begin ' + CDM.Prefix + ' Classification JOIN */' + 
	'~JOIN JLLIS.dbo.ClassificationData ' + CDM.Prefix + 'ClassificationData ON ' + CDM.Prefix + 'ClassificationData.ClassificationDataID = L.' + CDM.Prefix + 'ClassificationDataID' + 
	'~JOIN JLLIS.Dropdown.Caveat ' + CDM.Prefix + 'Caveat ON ' + CDM.Prefix + 'Caveat.CaveatID = ' + CDM.Prefix + 'ClassificationData.CaveatID' + 
	'~JOIN JLLIS.Dropdown.Classification ' + CDM.Prefix + 'Classification ON ' + CDM.Prefix + 'Classification.ClassificationID = ' + CDM.Prefix + 'ClassificationData.ClassificationID' + 
	'~/* End ' + CDM.Prefix + ' Classification JOIN */~' 
FROM JLLIS.Utility.ClassificationDataMapping CDM
WHERE CDM.SchemaName = 'dbo'
	AND CDM.TableName = 'LMS'
	AND CDM.ClassificationDataCode = 'Classification'
ORDER BY CDM.Prefix,CDM.ClassificationDataCode
