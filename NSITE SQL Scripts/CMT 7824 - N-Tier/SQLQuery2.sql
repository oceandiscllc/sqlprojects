DECLARE @cTableName varchar(250)
DECLARE @cSQL varchar(max) 
DECLARE @nInstanceID int
DECLARE @oTargetTables table (TableName varchar(250))

SET @nInstanceID = 5

INSERT INTO @oTargetTables (TableName) VALUES ('dbo.AAR')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.BB')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.Binder')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.CDR')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.LMS')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.SSite')

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT T.TableName 
	FROM @oTargetTables T
	ORDER BY T.TableName

OPEN oCursor
FETCH oCursor INTO @cTableName
WHILE @@fetch_status = 0
	BEGIN

	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'MajorCommand')
		BEGIN
		
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET OriginatingTierID = 0 WHERE MajorCommand IS NOT NULL'
		IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
			BEGIN
			
			SET @cSQL = @cSQL + ' OR ST1 IS NOT NULL'
		
			END
		--ENDIF
		
		EXEC (@cSQL)
	
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.MajorCommand AND T1.OriginatingTierID = 0'
		EXEC (@cSQL)
	
		IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
			BEGIN
			
			SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.ST1 AND T1.OriginatingTierID = 0'
			EXEC (@cSQL)
		
			END
		--ENDIF
		
		END
	ELSE IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'MSC')
		BEGIN
		
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET OriginatingTierID = 0 WHERE MSC IS NOT NULL'
		IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
			BEGIN
			
			SET @cSQL = @cSQL + ' OR ST1 IS NOT NULL'
		
			END
		--ENDIF
	
		EXEC (@cSQL)
	
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.MSC AND T1.OriginatingTierID = 0'
		EXEC (@cSQL)
	
		IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
			BEGIN
			
			SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.ST1 AND T1.OriginatingTierID = 0'
			EXEC (@cSQL)
		
			END
		--ENDIF
		
		END
	ELSE IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
		BEGIN
		
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET OriginatingTierID = 0 WHERE ST1 IS NOT NULL'
		EXEC (@cSQL)
	
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.ST1 AND T1.OriginatingTierID = 0'
		EXEC (@cSQL)
		
		END
	--ENDIF
	
	SET @cSQL = 'UPDATE ' + @cTableName + ' SET OriginatingTierID = ' + CAST(@nInstanceID as varchar(5)) + ' WHERE OriginatingTierID = 0'
	EXEC (@cSQL)

	FETCH oCursor INTO @cTableName
	
	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor