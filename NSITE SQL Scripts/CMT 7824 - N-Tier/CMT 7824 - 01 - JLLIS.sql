USE JLLIS
GO

--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--Begin create utility helper tools
--Begin create IsAncestor helper function
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.IsAncestor') AND O.type in ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION Utility.IsAncestor
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2010.09.27
-- Description:	A function to validate that one tier is an ancestor of another.
--							Parameter @nTierID1 is the ancestral tier.
--							Parameter @nTierID2 is the descendant tier.
--							A call of Utility.IsAncestor(x, y) will return a 1 if x is an 
--							ancestor of y and a 0 if it is not.  If x = y, the function
--							returns 1.
-- ============================================================================

CREATE FUNCTION Utility.IsAncestor
(
@nTierID1 int,
@nTierID2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
DECLARE @oTable table (TierID int)
	
SET @nRetVal = 0;

WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID2

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable
	(TierID)
SELECT HD.TierID
FROM HD 
WHERE HD.TierID = @nTierID1

IF EXISTS (SELECT 1 FROM @oTable)
	SET @nRetVal = 1

RETURN @nRetVal

END
GO
--End create IsAncestor helper function

--Begin create AddColumn helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.AddColumn') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.AddColumn
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.AddColumn
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name = @cColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD ' + @cColumnName + ' ' + @cDataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End create AddColumn helper stored procedure

--Begin create CloneData helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CloneData') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CloneData
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CloneData
	@cTableName varchar(250),
	@cDatabaseName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)

	SET @cSQL1 = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SC.Name 
		FROM dbo.syscolumns SC WITH (NOLOCK) 
		WHERE SC.id = OBJECT_ID(@cTableName) 
		ORDER BY SC.Name

	OPEN oCursor
	FETCH oCursor INTO @cSQL2
	WHILE @@fetch_status = 0
		BEGIN

		IF @cSQL1 = ''
			SET @cSQL1 = @cSQL2
		ELSE
			SET @cSQL1 = @cSQL1 + ',' + @cSQL2

		FETCH oCursor INTO @cSQL2

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	SET @cSQL3 = 'SET IDENTITY_INSERT ' + @cTableName + ' ON'
	SET @cSQL3 = @cSQL3 + ' INSERT INTO ' + @cTableName + ' (' + @cSQL1 + ') SELECT ' + @cSQL1 + ' FROM ' + @cDatabaseName + '.' + @cTableName
	SET @cSQL3 = @cSQL3 + ' SET IDENTITY_INSERT ' + @cTableName + ' OFF'

	EXEC (@cSQL3)

END
GO
--End create CloneData helper stored procedure

--Begin create CompareTableStructures helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CompareTableStructures') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CompareTableStructures
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.22
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CompareTableStructures
	@cTableName varchar(250),
	@cDatabaseName1 varchar(250),
	@cDatabaseName2 varchar(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cNotExists1 varchar(max)
	DECLARE @cNotExists2 varchar(max)
	DECLARE @cNotExists3 varchar(max)
	DECLARE @cSchemaName varchar(50)
	DECLARE @cSelectList1 varchar(max)
	DECLARE @cSelectList2 varchar(max)
	DECLARE @cSQL varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cTab5 varchar(5)
	DECLARE @nPos int

	CREATE table #oTable 
		(
		Description varchar(250) NULL,
		ObjectType varchar(50) NULL, 
		SchemaName varchar(50) NULL, 
		TableName varchar(50) NULL, 
		ColumnName varchar(50) NULL, 
		DataType varchar(50) NULL, 
		Length int NULL, 
		Precision int NULL, 
		Scale int NULL,
		Nullable varchar(50) NULL
		) 

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)
	SET @cTab5 = REPLICATE(CHAR(9), 5)
	
	SET @cNotExists1 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab3 + ')'

	SET @cNotExists2 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND C2.Name = C1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + '('
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'FROM #oTable T2'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'WHERE T2.ObjectType IN (''Table'', ''View'')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab4 + 'AND T2.TableName = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'

	SET @cSchemaName = 'dbo'

	SET @cSelectList1 = @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'WHEN O1.Type = ''U'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'THEN ''Table'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'ELSE ''View'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'END,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'O1.Name'

	SET @cSelectList2 = '''Column'','
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'O1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'T1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Max_Length,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Precision,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Scale,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'WHEN C1.Is_Nullable = 1'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'THEN ''Yes'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'ELSE ''No'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'END'
	SET @cSelectList2 = @cSelectList2 + @cCRLF

	SET @nPos = CHARINDEX('.', @cTableName)

	IF @nPos > 0
		BEGIN
		
		SET @cSchemaName = LEFT(@cTableName, @nPos - 1)
		SET @cTableName = RIGHT(@cTableName, LEN(@cTableName) - @nPos)
		
		END
	--ENDIF

	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 WITH (NOLOCK) ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''' AS Description,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 WITH (NOLOCK) ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1

	EXEC (@cSQL)
	
	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName, ColumnName, DataType, Length, Precision, Scale, Nullable)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.Types T1 WITH (NOLOCK) ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 WITH (NOLOCK) ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF
	
	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.Types T1 WITH (NOLOCK) ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 WITH (NOLOCK) ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2

	EXEC (@cSQL)

	SELECT *
	FROM #oTable T
	ORDER BY T.ObjectType DESC, T.Description, T.SchemaName, T.TableName, T.ColumnName	
	
	DROP TABLE #oTable

END
GO
--End create CompareTableStructures helper stored procedure

--Begin create CreateJointLMSForBinderView stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CreateJointLMSForBinderView') AND O.type in ('P', 'PC'))
	DROP PROCEDURE Utility.CreateJointLMSForBinderView
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2010.12.09
-- Description:	a stored procedure to dorp and create the joint_LMS_for_binder 
--							based on the IsLegacyInstance bit in the dbo.Tier table
-- ===========================================================================
CREATE PROCEDURE Utility.CreateJointLMSForBinderView
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_ID = OBJECT_ID('dbo.Joint_LMS_For_Binder'))
		DROP VIEW dbo.Joint_LMS_For_Binder

	DECLARE @cLegacySelect varchar(max)
	DECLARE @cNTierSelect varchar(max)
	DECLARE @cSQL varchar(max)
	DECLARE @cDatabaseName varchar(50)
	DECLARE @cTierName varchar(50)
	DECLARE @nIsLegacyInstance bit

	DECLARE @cCRLF varchar(2)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)

	SET @cLegacySelect = 'SELECT'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.Classification,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.EventDate,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.EventOpEx,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.JointLesson,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.LMSID,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.LMSUnit,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.MajorCommand,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.Status,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'T.TierID,'
	SET @cLegacySelect = @cLegacySelect + @cCRLF + @cTab1 + 'L.Topic'

	SET @cNTierSelect = 'SELECT'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.Classification,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.EventDate,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.EventOpEx,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.JointLesson,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.LMSID,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.LMSUnit,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'T.TierName AS MajorCommand,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.Status,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.OriginatingTierID AS TierID,'
	SET @cNTierSelect = @cNTierSelect + @cCRLF + @cTab1 + 'L.Topic'

	SET @cSQL = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			UPPER(DB.Name),
			T.IsLegacyInstance,

			CASE
				WHEN UPPER(DB.Name) IN ('SOCOM','SOCOMNEW')
				THEN 'USSOCOM'
				WHEN UPPER(DB.Name) = 'CFMCCLL'
				THEN 'MCCLL'
				ELSE UPPER(DB.Name) 
			END AS TierName

		FROM sys.Databases DB WITH (NOLOCK) 
			JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON UPPER(T.TierName) = 

				CASE 
					WHEN UPPER(DB.Name) IN ('SOCOM','SOCOMNEW') 
					THEN 'USSOCOM'
					WHEN UPPER(DB.Name) = 'CFMCCLL' 
					THEN 'MCCLL'
					ELSE UPPER(DB.Name) 
				END

				AND T.IsInstance = 1
				AND DB.Name IN ('ACGU','ARMY','CCO','CFMCCLL','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
		ORDER BY DB.Name

	OPEN oCursor
	FETCH oCursor into @cDatabaseName, @nIsLegacyInstance, @cTierName
	WHILE @@fetch_status = 0
		BEGIN
		
		IF @cSQL <> ''
			SET @cSQL = @cSQL + @cCRLF + @cCRLF + 'UNION' + @cCRLF + @cCRLF
		--ENDIF
		
		IF @nIsLegacyInstance = 1
			SET @cSQL = @cSQL + @cLegacySelect
		ELSE
			SET @cSQL = @cSQL + @cNTierSelect
		--ENDIF
			
		SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName + '.dbo.LMS L WITH (NOLOCK)'

		IF @nIsLegacyInstance = 1
			SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierName = ''' + @cTierName + ''''
		ELSE
			SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = L.OriginatingTierID'
		--ENDIF

		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND L.Status = ''Active'''
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND L.JointLesson = ''c'''
		
		FETCH oCursor into @cDatabaseName, @nIsLegacyInstance, @cTierName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	SET @cSQL = 'CREATE VIEW dbo.Joint_LMS_For_Binder AS' + @cCRLF + @cSQL
	EXECUTE (@cSQL)
	
END
GO
--End create CreateJointLMSForBinderView stored procedure

--Begin create DropConstraintsAndIndexes helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.DropConstraintsAndIndexes') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.DropConstraintsAndIndexes
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropConstraintsAndIndexes
	@cTableName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL varchar(max)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC WITH (NOLOCK)
		WHERE DC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @cTableName AS SQL
		FROM sys.indexes I WITH (NOLOCK)
		WHERE I.object_ID = OBJECT_ID(@cTableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End create DropConstraintsAndIndexes helper stored procedure

--Begin create RenameDeprecatedCategories helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedCategories') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedCategories
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedCategories
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cSQL = 'UPDATE ' + @cDatabaseName + '.dbo.DropDown'
	ELSE
		SET @cSQL = 'UPDATE dbo.DropDown'
	--ENDIF
	
	IF @cMode = 'FromOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = REPLACE(Category, ''_Old'', '''')'
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC_Old'',''TIER1_Old'',''TIER2_Old'',''SubTier1_Old'',''Unit_Old'')'
		
		END
	ELSE IF @cMode = 'ToOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = Category + ''_Old'''
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC'',''TIER1'',''TIER2'',''SubTier1'',''Unit'')'
		
		END
	--ENDIF
	
	EXEC (@cSQL)
	
END
GO
--End create RenameDeprecatedCategories helper stored procedure

--Begin create RenameDeprecatedColumns helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedColumns') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedColumns
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedColumns
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDatabaseNameWithPeriod varchar(50)
	DECLARE @cSQL varchar(max)
	
	SET @cDatabaseNameWithPeriod = ''
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cDatabaseNameWithPeriod = @cDatabaseName + '.'
	
	SET @cSQL = 'INSERT INTO #oTable (SQLText) SELECT '
	SET @cSQL = @cSQL + '''EXEC ' + @cDatabaseNameWithPeriod + 'dbo.sp_rename '''''''
	SET @cSQL = @cSQL + ' + S.Name + ''.'' + O.Name + ''.'' + C.Name + '''''', '''''' +'
		
	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + ' REPLACE(C.Name, ''_Old'', '''')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + ' C.Name + ''_Old'''
	
	SET @cSQL = @cSQL + ' + '''''', ''''COLUMN'''''' AS SQLText'
	SET @cSQL = @cSQL + '	FROM ' + @cDatabaseNameWithPeriod + 'sys.objects O WITH (NOLOCK)'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.schemas S WITH (NOLOCK) ON S.schema_ID = O.schema_ID'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id'
	SET @cSQL = @cSQL + '	AND O.type = ''U'''

	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand_old'', ''msc_old'', ''st1_old'')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand'', ''msc'', ''st1'')'

	SET @cSQL = @cSQL + '	ORDER BY S.Name, O.Name, C.Name'
	
	CREATE TABLE #oTable (SQLText varchar(max))
	
	EXEC (@cSQL)
	
	DECLARE oRenameDeprecatedColumnsCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SQLText
		FROM #oTable
		
	OPEN oRenameDeprecatedColumnsCursor
	FETCH oRenameDeprecatedColumnsCursor into @cSQL
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC (@cSQL)
	
		FETCH oRenameDeprecatedColumnsCursor into @cSQL
	
		END
	--END WHILE
	
	CLOSE oRenameDeprecatedColumnsCursor
	DEALLOCATE oRenameDeprecatedColumnsCursor
	
	DROP TABLE #oTable

END
GO
--End create RenameDeprecatedColumns helper stored procedure

--Begin create SetColumnNotNull helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetColumnNotNull') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetColumnNotNull
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.01.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetColumnNotNull
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ALTER COLUMN ' + @cColumnName + ' ' + @cDataType + ' NOT NULL'
	EXEC (@cSQL)
		
END
GO
--End create SetColumnNotNull helper stored procedure

--Begin create SetDefault helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetDefault') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetDefault
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetDefault
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDefault varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cConstraintName varchar(500)
	DECLARE @cSQL varchar(max)
	DECLARE @nDefaultIsGetDate bit
	DECLARE @nDefaultIsNumeric bit
	DECLARE @nLength int

	SET @nDefaultIsGetDate = 0

	IF @cDefault = 'getDate()'
		SET @nDefaultIsGetDate = 1
		
	SET @nDefaultIsNumeric = ISNUMERIC(@cDefault)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName
	
	IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ''' + @cDefault + ''' WHERE ' + @cColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ' + @cDefault + ' WHERE ' + @cColumnName + ' IS NULL'

	EXECUTE (@cSQL)

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cConstraintName = 'DF_' + RIGHT(@cTableName, @nLength) + '_' + @cColumnName
	
	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC WITH (NOLOCK) WHERE DC.parent_object_ID = OBJECT_ID(@cTableName) AND DC.Name = @cConstraintName)
		BEGIN	

		IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @cDefault + ''' FOR ' + @cColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @cDefault + ' FOR ' + @cColumnName
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End create SetDefault helper stored procedure

--Begin create SetIndexClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexClustered helper stored procedure

--Begin create SetIndexNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexNonClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexNonClustered helper stored procedure

--Begin create SetPrimaryKeyClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyClustered helper stored procedure

--Begin create SetPrimaryKeyNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyNonClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyNonClustered helper stored procedure
--End create utility helper tools

--Begin deprecated category and column rename
DECLARE @cDatabaseName varchar(50)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS') 
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	EXEC Utility.RenameDeprecatedCategories @cDatabaseName, 'FromOld'
	EXEC Utility.RenameDeprecatedColumns @cDatabaseName, 'FromOld'
	
	FETCH oCursor into @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End deprecated category and column rename

USE JLLIS
GO

--Begin new table creation
--Begin table dbo.Caveat
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.caveat') AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.caveat
		(
		caveatid int IDENTITY(1,1) NOT NULL,
		caveat varchar(50) NOT NULL,
		caveatweight int NOT NULL CONSTRAINT DF_Caveat_CaveatWeight DEFAULT 0,
		displayorder int NOT NULL CONSTRAINT DF_Caveat_DisplayOrder DEFAULT 0,
		isactive bit NOT NULL CONSTRAINT DF_Caveat_IsActive DEFAULT 1,
		CONSTRAINT PK_Caveat PRIMARY KEY CLUSTERED 
			(
			caveatid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	CREATE NONCLUSTERED INDEX IX_Caveat ON dbo.caveat
		(
		displayorder ASC,
		caveat ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	
	END
--ENDIF

IF NOT EXISTS (SELECT 1 FROM dbo.caveat C WITH (NOLOCK) WHERE C.caveat = 'DES')
	INSERT INTO dbo.caveat (caveat) VALUES ('DES')
IF NOT EXISTS (SELECT 1 FROM dbo.caveat C WITH (NOLOCK) WHERE C.caveat = 'FGI Controlled')
	INSERT INTO dbo.caveat (caveat) VALUES ('FGI Controlled')
IF NOT EXISTS (SELECT 1 FROM dbo.caveat C WITH (NOLOCK) WHERE C.caveat = 'FOUO')
	INSERT INTO dbo.caveat (caveat) VALUES ('FOUO')
IF NOT EXISTS (SELECT 1 FROM dbo.caveat C WITH (NOLOCK) WHERE C.caveat = 'NOFORN')
	INSERT INTO dbo.caveat (caveat) VALUES ('NOFORN')
GO
--End table dbo.Caveat

--Begin table dbo.Classification
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.classification') AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.classification
		(
		classificationid int IDENTITY(1,1) NOT NULL,
		classification varchar(50) NOT NULL,
		classificationcolor varchar(50) NOT NULL CONSTRAINT DF_Classification_ClassificationColor DEFAULT '#000000',
		classificationweight int NOT NULL CONSTRAINT DF_Classification_ClassificationWeight DEFAULT 0,
		displayorder int NOT NULL CONSTRAINT DF_Classification_DisplayOrder DEFAULT 0,
		isactive bit NOT NULL CONSTRAINT DF_Classification_IsActive DEFAULT 1,
		CONSTRAINT PK_Classification PRIMARY KEY CLUSTERED 
			(
			classificationid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX IX_Classification ON dbo.classification
		(
		displayorder ASC,
		classification ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	END
--ENDIF

IF NOT EXISTS (SELECT 1 FROM dbo.classification C WITH (NOLOCK) WHERE C.classification = 'Unclassified')
	INSERT INTO dbo.classification (classification, classificationcolor, classificationweight, displayorder) VALUES ('Unclassified', '#008000', 1, 1)
IF NOT EXISTS (SELECT 1 FROM dbo.classification C WITH (NOLOCK) WHERE C.classification = 'Confidential')
	INSERT INTO dbo.classification (classification, classificationcolor, classificationweight, displayorder) VALUES ('Confidential', '#0000FF', 2, 2)
IF NOT EXISTS (SELECT 1 FROM dbo.classification C WITH (NOLOCK) WHERE C.classification = 'Secret')
	INSERT INTO dbo.classification (classification, classificationcolor, classificationweight, displayorder) VALUES ('Secret', '#FF0000', 3, 3)
IF NOT EXISTS (SELECT 1 FROM dbo.classification C WITH (NOLOCK) WHERE C.classification = 'Top Secret')
	INSERT INTO dbo.classification (classification, classificationcolor, classificationweight, displayorder) VALUES ('Top Secret', '#000000', 4, 4)
GO
--End table dbo.Classification

--Begin table dbo.Country
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.country') AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.country
		(
		countryid int IDENTITY(1,1) NOT NULL,
		countryname varchar(250) NOT NULL,
		isocode2 char(2) NULL,
		isocode3 char(3) NULL,
		region varchar(250) NULL,
		subregion varchar(250) NULL,
		cdhid int CONSTRAINT DF_country_cdhid NOT NULL DEFAULT 0,
		lat varchar(10) NULL,
		long varchar(10) NULL,
		displayorder int CONSTRAINT DF_country_displayorder NOT NULL DEFAULT 0,
		createdatetime datetime CONSTRAINT DF_country_createdatetime NOT NULL DEFAULT getDate(),
		createuserid int CONSTRAINT DF_country_createuserid NOT NULL DEFAULT 0,
		updatetime datetime CONSTRAINT DF_country_updatetime NOT NULL DEFAULT getDate(),
		updateuserid int CONSTRAINT DF_country_updateuserid NOT NULL DEFAULT 0,
		CONSTRAINT PK_Country PRIMARY KEY NONCLUSTERED 
			(
			countryid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY] 
	
	CREATE UNIQUE CLUSTERED INDEX IX_CountryName ON dbo.country 
		(
		displayorder ASC,
		countryname ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	SET IDENTITY_INSERT dbo.Country ON
	INSERT INTO dbo.Country (CountryID, CountryName) VALUES (0, '')
	SET IDENTITY_INSERT dbo.Country OFF

	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Afghanistan','AF','AFG','Asia','Southern Asia',2,'33 00 N','65 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('�land Islands','AX','ALA','Europe','Northern Europe',3,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Albania','AL','ALB','Europe','Southern Europe',4,'41 00 N','20 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Algeria','DZ','DZA','Africa','Northern Africa',5,'28 00 N','3 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('American Samoa','AS','ASM','Oceania','Polynesia',6,'14 20 S','170 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Andorra','AD','AND','Europe','Southern Europe',7,'42 30 N','1 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Angola','AO','AGO','Africa','Middle Africa',8,'12 30 S','18 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Anguilla','AI','AIA','Americas','Caribbean',9,'18 15 N','63 10 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Antarctica','AQ','ATA',NULL,NULL,10,'90 00 S','0 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Antigua and Barbuda','AG','ATG','Americas','Caribbean',11,'17 03 N','61 48 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Argentina','AR','ARG','Americas','South America',12,'34 00 S','64 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Armenia','AM','ARM','Asia','Western Asia',13,'40 00 N','45 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Aruba','AW','ABW','Americas','Caribbean',14,'12 30 N','69 58 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Australia','AU','AUS','Oceania','Australia and New Zealand',15,'27 00 S','133 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Austria','AT','AUT','Europe','Western Europe',16,'47 20 N','13 20 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Azerbaijan','AZ','AZE','Asia','Western Asia',17,'40 30 N','47 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bahamas','BS','BHS','Americas','Caribbean',18,'24 15 N','76 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bahrain','BH','BHR','Asia','Western Asia',21,'26 00 N','50 33 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bangladesh','BD','BGD','Asia','Southern Asia',22,'24 00 N','90 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Barbados','BB','BRB','Americas','Caribbean',23,'13 10 N','59 32 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belarus','BY','BLR','Europe','Eastern Europe',24,'53 00 N','28 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belgium','BE','BEL','Europe','Western Europe',25,'50 50 N','4 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Belize','BZ','BLZ','Americas','Central America',26,'17 15 N','88 45 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Benin','BJ','BEN','Africa','Western Africa',27,'9 30 N','2 15 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bermuda','BM','BMU','Americas','Northern America',28,'32 20 N','64 45 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bhutan','BT','BTN','Asia','Southern Asia',29,'27 30 N','90 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bolivia','BO','BOL','Americas','South America',30,'17 00 S','65 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bosnia and Herzegovina','BA','BIH','Europe','Southern Europe',31,'44 00 N','18 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Botswana','BW','BWA','Africa','Southern Africa',32,'22 00 S','24 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bouvet Island','BV','BVT',NULL,NULL,33,'54 26 S','3 24 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Brazil','BR','BRA','Americas','South America',34,'10 00 S','55 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('British Indian Ocean Territory','IO','IOT','Africa','Eastern Africa',35,'6 00 S','71 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Brunei','BN','BRN','Asia','South-Eastern Asia',36,'4 30 N','114 40 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Bulgaria','BG','BGR','Europe','Eastern Europe',37,'43 00 N','25 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Burkina Faso','BF','BFA','Africa','Western Africa',38,'13 00 N','2 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Burundi','BI','BDI','Africa','Eastern Africa',39,'3 30 S','30 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cambodia','KH','KHM','Asia','South-Eastern Asia',40,'13 00 N','105 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cameroon','CM','CMR','Africa','Middle Africa',41,'6 00 N','12 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Canada','CA','CAN','Americas','Northern America',42,'60 00 N','95 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cape Verde','CV','CPV','Africa','Western Africa',43,'16 00 N','24 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cayman Islands','KY','CYM','Americas','Caribbean',44,'19 30 N','80 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Central African Republic','CF','CAF','Africa','Middle Africa',45,'7 00 N','21 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Chad','TD','TCD','Africa','Middle Africa',46,'15 00 N','19 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Chile','CL','CHL','Americas','South America',47,'30 00 S','71 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('China','CN','CHN','Asia','Eastern Asia',48,'35 00 N','105 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Christmas Island','CX','CXR','Oceania','Australia and New Zealand',49,'10 30 S','105 40 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cocos (Keeling) Islands','CC','CCK','Oceania','Australia and New Zealand',50,'12 30 S','96 50 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Colombia','CO','COL','Americas','South America',51,'4 00 N','72 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Comoros','KM','COM','Africa','Eastern Africa',52,'12 10 S','44 15 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Congo, The Democratic Republic Of The','CD','COD','Africa','Middle Africa',54,'0 00 N','25 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Congo','CG','COG','Africa','Middle Africa',53,'1 00 S','15 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cook Islands','CK','COK','Oceania','Polynesia',55,'21 14 S','159 46 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Costa Rica','CR','CRI','Americas','Central America',56,'10 00 N','84 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cote D''Ivoire','CI','CIV','Africa','Western Africa',57,'8 00 N','5 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Croatia','HR','HRV','Europe','Southern Europe',58,'45 10 N','15 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cuba','CU','CUB','Americas','Caribbean',59,'21 30 N','80 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Cyprus','CY','CYP','Asia','Western Asia',60,'35 00 N','33 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Czech Republic','CZ','CZE','Europe','Eastern Europe',61,'49 45 N','15 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Denmark','DK','DNK','Europe','Northern Europe',62,'56 00 N','10 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Djibouti','DJ','DJI','Africa','Eastern Africa',63,'11 30 N','43 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Dominica','DM','DMA','Americas','Caribbean',64,'15 25 N','61 20 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Dominican Republic','DO','DOM','Americas','Caribbean',65,'19 00 N','70 40 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('East Timor','TL','TLS','Asia','South-Eastern Asia',226,'8 50 S','125 55 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ecuador','EC','ECU','Americas','South America',66,'2 00 S','77 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Egypt','EG','EGY','Africa','Northern Africa',67,'27 00 N','30 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('El Salvador','SV','SLV','Americas','Central America',68,'13 50 N','88 55 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Equatorial Guinea','GQ','GNQ','Africa','Middle Africa',69,'2 00 N','10 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Eritrea','ER','ERI','Africa','Eastern Africa',70,'15 00 N','39 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Estonia','EE','EST','Europe','Northern Europe',71,'59 00 N','26 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ethiopia','ET','ETH','Africa','Eastern Africa',73,'8 00 N','38 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Falkland Islands (Malvinas)','FK','FLK','Americas','South America',74,'51 45 S','59 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Faroe Islands','FO','FRO','Europe','Northern Europe',75,'62 00 N','7 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Fiji','FJ','FJI','Oceania','Melanesia',76,'18 00 S','175 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Finland','FI','FIN','Europe','Northern Europe',77,'64 00 N','26 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('France','FR','FRA','Europe','Western Europe',78,'46 00 N','2 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Guiana','GF','GUF','Americas','South America',80,'4 00 N','53 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Polynesia','PF','PYF','Oceania','Polynesia',81,'15 00 S','140 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('French Southern Territories','TF','ATF',NULL,NULL,82,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gabon','GA','GAB','Africa','Middle Africa',83,'1 00 S','11 45 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gambia','GM','GMB','Africa','Western Africa',84,'13 28 N','16 34 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Georgia','GE','GEO','Asia','Western Asia',1,'42 00 N','43 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Germany','DE','DEU','Europe','Western Europe',85,'51 00 N','9 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ghana','GH','GHA','Africa','Western Africa',86,'8 00 N','2 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Gibraltar','GI','GIB','Europe','Southern Europe',87,'36 08 N','5 21 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Greece','GR','GRC','Europe','Southern Europe',88,'39 00 N','22 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Greenland','GL','GRL','Americas','Northern America',89,'72 00 N','40 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Grenada','GD','GRD','Americas','Caribbean',90,'12 07 N','61 40 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guadeloupe','GP','GLP','Americas','Caribbean',91,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guam','GU','GUM','Oceania','Micronesia',92,'13 28 N','144 47 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guatemala','GT','GTM','Americas','Central America',93,'15 30 N','90 15 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guernsey','GG','GGY','Europe','Northern Europe',94,'49 28 N','2 35 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guinea','GN','GIN','Africa','Western Africa',95,'11 00 N','10 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guinea-Bissau','GW','GNB','Africa','Western Africa',96,'12 00 N','15 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Guyana','GY','GUY','Americas','South America',97,'5 00 N','59 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Haiti','HT','HTI','Americas','Caribbean',98,'19 00 N','72 25 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Heard and McDonald Islands','HM','HMD',NULL,NULL,99,'53 06 S','72 31 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Honduras','HN','HND','Americas','Central America',101,'15 00 N','86 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Hong Kong','HK','HKG','Asia','Eastern Asia',102,'22 15 N','114 10 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Hungary','HU','HUN','Europe','Eastern Europe',103,'47 00 N','20 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iceland','IS','ISL','Europe','Northern Europe',104,'65 00 N','18 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('India','IN','IND','Asia','Southern Asia',105,'20 00 N','77 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Indonesia','ID','IDN','Asia','South-Eastern Asia',106,'5 00 S','120 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iran','IR','IRN','Asia','Southern Asia',107,'32 00 N','53 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Iraq','IQ','IRQ','Asia','Western Asia',108,'33 00 N','44 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ireland','IE','IRL','Europe','Northern Europe',109,'53 00 N','8 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Isle of Man','IM','IMN','Europe','Northern Europe',110,'54 15 N','4 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Israel','IL','ISR','Asia','Western Asia',111,'31 30 N','34 45 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Italy','IT','ITA','Europe','Southern Europe',112,'42 50 N','12 50 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jamaica','JM','JAM','Americas','Caribbean',113,'18 15 N','77 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Japan','JP','JPN','Asia','Eastern Asia',114,'36 00 N','138 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jersey','JE','JEY','Europe','Northern Europe',115,'49 15 N','2 10 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Jordan','JO','JOR','Asia','Western Asia',116,'31 00 N','36 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kazakhstan','KZ','KAZ','Asia','Central Asia',117,'48 00 N','68 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kenya','KE','KEN','Africa','Eastern Africa',118,'1 00 N','38 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kiribati','KI','KIR','Oceania','Micronesia',119,'1 25 N','173 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Korea, North','KP','PRK','Asia','Eastern Asia',121,'40 00 N','127 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Korea, South','KR','KOR','Asia','Eastern Asia',120,'37 00 N','127 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kosovo',NULL,NULL,'Europe','Southern Europe',0,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kuwait','KW','KWT','Asia','Western Asia',122,'29 30 N','45 45 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Kyrgyzstan','KG','KGZ','Asia','Central Asia',123,'41 00 N','75 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Laos','LA','LAO','Asia','South-Eastern Asia',124,'18 00 N','105 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Latvia','LV','LVA','Europe','Northern Europe',125,'57 00 N','25 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lebanon','LB','LBN','Asia','Western Asia',126,'33 50 N','35 50 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lesotho','LS','LSO','Africa','Southern Africa',127,'29 30 S','28 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Liberia','LR','LBR','Africa','Western Africa',128,'6 30 N','9 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Libya','LY','LBY','Africa','Northern Africa',129,'25 00 N','17 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Liechtenstein','LI','LIE','Europe','Western Europe',130,'47 16 N','9 32 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Lithuania','LT','LTU','Europe','Northern Europe',131,'56 00 N','24 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Luxembourg','LU','LUX','Europe','Western Europe',132,'49 45 N','6 10 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Macao','MO','MAC','Asia','Eastern Asia',133,'22 10 N','113 33 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Macedonia','MK','MKD','Europe','Southern Europe',134,'41 50 N','22 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Madagascar','MG','MDG','Africa','Eastern Africa',135,'20 00 S','47 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malawi','MW','MWI','Africa','Eastern Africa',136,'13 30 S','34 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malaysia','MY','MYS','Asia','South-Eastern Asia',137,'2 30 N','112 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Maldives','MV','MDV','Asia','Southern Asia',138,'3 15 N','73 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mali','ML','MLI','Africa','Western Africa',139,'17 00 N','4 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Malta','MT','MLT','Europe','Southern Europe',140,'35 50 N','14 35 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Marshall Islands','MH','MHL','Oceania','Micronesia',141,'9 00 N','168 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Martinique','MQ','MTQ','Americas','Caribbean',142,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mauritania','MR','MRT','Africa','Western Africa',143,'20 00 N','12 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mauritius','MU','MUS','Africa','Eastern Africa',144,'20 17 S','57 33 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mayotte','YT','MYT','Africa','Eastern Africa',145,'12 50 S','45 10 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mexico','MX','MEX','Americas','Central America',146,'23 00 N','102 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Micronesia','FM','FSM','Oceania','Micronesia',147,'6 55 N','158 15 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Moldova','MD','MDA','Europe','Eastern Europe',148,'47 00 N','29 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Monaco','MC','MCO','Europe','Western Europe',149,'43 44 N','7 24 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mongolia','MN','MNG','Asia','Eastern Asia',150,'46 00 N','105 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Montenegro','ME','MNE','Europe','Southern Europe',151,'42 30 N','19 18 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Montserrat','MS','MSR','Americas','Caribbean',152,'16 45 N','62 12 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Morocco','MA','MAR','Africa','Northern Africa',153,'32 00 N','5 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Mozambique','MZ','MOZ','Africa','Eastern Africa',154,'18 15 S','35 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Myanmar','MM','MMR','Asia','South-Eastern Asia',155,'22 00 N','98 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Namibia','NA','NAM','Africa','Southern Africa',156,'22 00 S','17 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nauru','NR','NRU','Oceania','Micronesia',157,'0 32 S','166 55 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nepal','NP','NPL','Asia','Southern Asia',158,'28 00 N','84 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Netherlands Antilles','AN','ANT','Americas','Caribbean',160,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Netherlands','NL','NLD','Europe','Western Europe',159,'52 30 N','5 45 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('New Caledonia','NC','NCL','Oceania','Melanesia',161,'21 30 S','165 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('New Zealand','NZ','NZL','Oceania','Australia and New Zealand',162,'41 00 S','174 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nicaragua','NI','NIC','Americas','Central America',163,'13 00 N','85 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Niger','NE','NER','Africa','Western Africa',164,'16 00 N','8 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Nigeria','NG','NGA','Africa','Western Africa',165,'10 00 N','8 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Niue','NU','NIU','Oceania','Polynesia',166,'19 02 S','169 52 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Norfolk Island','NF','NFK','Oceania','Australia and New Zealand',167,'29 02 S','167 57 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Northern Mariana Islands','MP','MNP','Oceania','Micronesia',168,'15 12 N','145 45 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Norway','NO','NOR','Europe','Northern Europe',169,'62 00 N','10 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Oman','OM','OMN','Asia','Western Asia',170,'21 00 N','57 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Pakistan','PK','PAK','Asia','Southern Asia',171,'30 00 N','70 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Palau','PW','PLW','Oceania','Micronesia',172,'7 30 N','134 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Palestinian Territory, Occupied','PS','PSE','Asia','Western Asia',173,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Panama','PA','PAN','Americas','Central America',175,'9 00 N','80 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Papua New Guinea','PG','PNG','Oceania','Melanesia',176,'6 00 S','147 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Paraguay','PY','PRY','Americas','South America',177,'23 00 S','58 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Peru','PE','PER','Americas','South America',178,'10 00 S','76 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Philippines','PH','PHL','Asia','South-Eastern Asia',180,'13 00 N','122 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Pitcairn','PN','PCN','Oceania','Polynesia',182,'25 04 S','130 06 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Poland','PL','POL','Europe','Eastern Europe',183,'52 00 N','20 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Portugal','PT','PRT','Europe','Southern Europe',184,'39 30 N','8 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Puerto Rico','PR','PRI','Americas','Caribbean',185,'18 15 N','66 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Qatar','QA','QAT','Asia','Western Asia',186,'25 30 N','51 15 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('R�union','RE','REU','Africa','Eastern Africa',187,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Romania','RO','ROU','Europe','Eastern Europe',189,'46 00 N','25 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Russia','RU','RUS','Europe','Eastern Europe',190,'60 00 N','100 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Rwanda','RW','RWA','Africa','Eastern Africa',191,'2 00 S','30 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Barth�lemy','BL','BLM','Americas','Caribbean',300,'17 90 N','62 85 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Helena, Ascension and Tristan Da Cunha','SH','SHN','Africa','Western Africa',192,'15 57','5 42 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Kitts And Nevis','KN','KNA','Americas','Caribbean',193,'17 20 N','62 45 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Lucia','LC','LCA','Americas','Caribbean',194,'13 53 N','60 58 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Martin','MF','MAF','Americas','Caribbean',301,'18 05 N','63 57 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Pierre And Miquelon','PM','SPM','Americas','Northern America',181,'46 50 N','56 20 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saint Vincent And The Grenadines','VC','VCT','Americas','Caribbean',195,'13 15 N','61 12 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Samoa','WS','WSM','Oceania','Polynesia',196,'13 35 S','172 20 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('San Marino','SM','SMR','Europe','Southern Europe',197,'43 46 N','12 25 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sao Tome and Principe','ST','STP','Africa','Middle Africa',198,'1 00 N','7 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Saudi Arabia','SA','SAU','Asia','Western Asia',199,'25 00 N','45 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Senegal','SN','SEN','Africa','Western Africa',200,'14 00 N','14 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Serbia','RS','SRB','Europe','Southern Europe',201,'44 00 N','21 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Seychelles','SC','SYC','Africa','Eastern Africa',202,'4 35 S','55 40 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sierra Leone','SL','SLE','Africa','Western Africa',203,'8 30 N','11 30 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Singapore','SG','SGP','Asia','South-Eastern Asia',205,'1 22 N','103 48 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Slovakia','SK','SVK','Europe','Eastern Europe',206,'48 40 N','19 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Slovenia','SI','SVN','Europe','Southern Europe',207,'46 07 N','14 49 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Solomon Islands','SB','SLB','Oceania','Melanesia',208,'8 00 S','159 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Somalia','SO','SOM','Africa','Eastern Africa',209,'10 00 N','49 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('South Africa','ZA','ZAF','Africa','Southern Africa',210,'29 00 S','24 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('South Georgia and the South Sandwich Islands','GS','SGS','Americas','South America',211,'54 30 S','37 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Spain','ES','ESP','Europe','Southern Europe',212,'40 00 N','4 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sri Lanka','LK','LKA','Asia','Southern Asia',213,'7 00 N','81 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sudan','SD','SDN','Africa','Northern Africa',214,'15 00 N','30 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Suriname','SR','SUR','Americas','South America',215,'4 00 N','56 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Svalbard And Jan Mayen','SJ','SJM','Europe','Northern Europe',216,'78 00 N','20 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Swaziland','SZ','SWZ','Africa','Southern Africa',217,'26 30 S','31 30 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Sweden','SE','SWE','Europe','Northern Europe',218,'62 00 N','15 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Switzerland','CH','CHE','Europe','Western Europe',219,'47 00 N','8 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Syria','SY','SYR','Asia','Western Asia',220,'35 00 N','38 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Taiwan','TW','TWN','Asia','Eastern Asia',221,'23 30 N','121 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tajikistan','TJ','TJK','Asia','Central Asia',222,'39 00 N','71 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tanzania','TZ','TZA','Africa','Eastern Africa',223,'6 00 S','35 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Thailand','TH','THA','Asia','South-Eastern Asia',224,'15 00 N','100 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Togo','TG','TGO','Africa','Western Africa',227,'8 00 N','1 10 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tokelau','TK','TKL','Oceania','Polynesia',228,'9 00 S','172 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tonga','TO','TON','Oceania','Polynesia',229,'20 00 S','175 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Trinidad and Tobago','TT','TTO','Americas','Caribbean',230,'11 00 N','61 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tunisia','TN','TUN','Africa','Northern Africa',231,'34 00 N','9 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turkey','TR','TUR','Asia','Western Asia',232,'39 00 N','35 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turkmenistan','TM','TKM','Asia','Central Asia',233,'40 00 N','60 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Turks and Caicos Islands','TC','TCA','Americas','Caribbean',234,'21 45 N','71 35 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Tuvalu','TV','TUV','Oceania','Polynesia',235,'8 00 S','178 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uganda','UG','UGA','Africa','Eastern Africa',236,'1 00 N','32 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Ukraine','UA','UKR','Europe','Eastern Europe',237,'49 00 N','32 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United Arab Emirates','AE','ARE','Asia','Western Asia',238,'24 00 N','54 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United Kingdom','GB','GBR','Europe','Northern Europe',239,'54 00 N','2 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United States Minor Outlying Islands','UM','UMI','Americas','Northern America',241,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('United States','US','USA','Americas','Northern America',240,'38 00 N','97 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uruguay','UY','URY','Americas','South America',242,'33 00 S','56 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Uzbekistan','UZ','UZB','Asia','Central Asia',243,'41 00 N','64 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Vanuatu','VU','VUT','Oceania','Melanesia',244,'16 00 S','167 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Vatican City','VA','VAT','Europe','Southern Europe',100,'41 54 N','12 27 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Venezuela','VE','VEN','Americas','South America',245,'8 00 N','66 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Viet Nam','VN','VNM','Asia','South-Eastern Asia',247,'16 10 N','107 50 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Virgin Islands, British','VG','VGB','Americas','Caribbean',248,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Virgin Islands, U.S.','VI','VIR','Americas','Caribbean',249,NULL,NULL)
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Wallis and Futuna','WF','WLF','Oceania','Polynesia',250,'13 18 S','176 12 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Western Sahara','EH','ESH','Africa','Northern Africa',251,'24 30 N','13 00 W')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Yemen','YE','YEM','Asia','Western Asia',252,'15 00 N','48 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Zambia','ZM','ZMB','Africa','Eastern Africa',255,'15 00 S','30 00 E')
	INSERT INTO dbo.Country (CountryName, ISOCode2, ISOCode3, Region, SubRegion, CDHID, LAT, LONG) VALUES ('Zimbabwe','ZW','ZWE','Africa','Eastern Africa',256,'20 00 S','30 00 E')

	END
--ENDIF
GO

UPDATE dbo.Country
SET CountryName = 'Palestinian Authority'
WHERE ISOCode2 = 'PS'
GO

UPDATE dbo.Country
SET DisplayOrder = 

	CASE 
		WHEN CountryID = 0
		THEN 0
		WHEN ISOCode2 = 'US'
		THEN 1
		ELSE 2
	END

GO
--End table dbo.Country

--Begin table dbo.Paygrade
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.paygrade') AND O.type in ('U'))
	BEGIN

	CREATE TABLE dbo.paygrade
		(
		paygradeid int IDENTITY(1,1) NOT NULL,
		paygrade varchar(50) NOT NULL,
		displayorder int NOT NULL CONSTRAINT DF_PayGrade_DisplayOrder DEFAULT 0,
		isactive bit NOT NULL CONSTRAINT DF_PayGrade_IsActive DEFAULT 1,
		CONSTRAINT PK_PayGrade PRIMARY KEY CLUSTERED 
			(
			paygradeid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
	CREATE NONCLUSTERED INDEX IX_PayGrade ON dbo.paygrade
		(
		displayorder ASC,
		paygrade ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CADET')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CADET', 1)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'MIDN')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('MIDN', 2)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CIV')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CIV', 3)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CTR')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CTR', 4)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-1')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-1', 5)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-2')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-2', 6)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-3')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-3', 7)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-4')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-4', 8)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-5')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-5', 9)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-6')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-6', 10)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-7')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-7', 11)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-8')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-8', 12)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'E-9')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('E-9', 13)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CWO-1')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CWO-1', 14)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CWO-2')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CWO-2', 15)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CWO-3')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CWO-3', 16)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CWO-4')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CWO-4', 17)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'CWO-5')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('CWO-5', 18)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-1')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-1', 19)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-2')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-2', 20)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-3')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-3', 21)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-4')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-4', 22)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-5')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-5', 23)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-6')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-6', 24)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-7')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-7', 25)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-8')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-8', 26)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-9')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-9', 27)
IF NOT EXISTS (SELECT 1 FROM dbo.paygrade P WITH (NOLOCK) WHERE P.paygrade = 'O-10')
	INSERT INTO dbo.paygrade (paygrade, displayorder) VALUES ('0-10', 28)
GO
--End table dbo.Paygrade

--Begin table dbo.SecLev
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.seclev') AND O.type in ('U'))
	BEGIN

	CREATE TABLE dbo.seclev
		(
		seclevid int IDENTITY(1,1) NOT NULL,
		seclev int NOT NULL CONSTRAINT DF_SecLev_SecLev DEFAULT 0,
		role varchar(50) NULL,
		isactive bit NOT NULL CONSTRAINT DF_SecLev_IsActive DEFAULT 1,
		CONSTRAINT PK_SecLev PRIMARY KEY NONCLUSTERED 
			(
			seclevid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	CREATE CLUSTERED INDEX IX_SecLev ON dbo.seclev
		(
		role ASC,
		seclev ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	SET IDENTITY_INSERT dbo.seclev ON
	INSERT INTO dbo.seclev (seclevid,seclev) VALUES (0,0)
	SET IDENTITY_INSERT dbo.seclev OFF
	
	INSERT INTO seclev (SecLev,Role) VALUES (1000,'Registered')
	INSERT INTO seclev (SecLev,Role) VALUES (2000,'Authorized')
	INSERT INTO seclev (SecLev,Role) VALUES (2100,'COI Manager')
	INSERT INTO seclev (SecLev,Role) VALUES (2300,'IMS_SME')
	INSERT INTO seclev (SecLev,Role) VALUES (2400,'IMS_Gatekeeper')
	INSERT INTO seclev (SecLev,Role) VALUES (2500,'CLM')
	INSERT INTO seclev (SecLev,Role) VALUES (3000,'TEAM')
	INSERT INTO seclev (SecLev,Role) VALUES (4000,'Manager')
	INSERT INTO seclev (SecLev,Role) VALUES (6000,'Administrator')
	INSERT INTO seclev (SecLev,Role) VALUES (7000,'Super Administrator')

	END
--ENDIF
GO
--End table dbo.SecLev

--Begin table dbo.Status
IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.status') AND O.type in ('U'))
	BEGIN

	CREATE TABLE dbo.status
		(
		statusid int IDENTITY(1,1) NOT NULL,
		status varchar(50) NOT NULL,
		displayorder int NOT NULL CONSTRAINT DF_Status_DisplayOrder DEFAULT 0,
		isactive bit NOT NULL CONSTRAINT DF_Status_IsActive DEFAULT 1,
		CONSTRAINT PK_Status PRIMARY KEY CLUSTERED 
			(
			statusid ASC
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	CREATE NONCLUSTERED INDEX IX_Status ON dbo.status
		(
		displayorder ASC,
		status ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	END
--ENDIF

IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Active')
	INSERT INTO dbo.status (status) VALUES ('Active')
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Canceled')
	INSERT INTO dbo.status (status) VALUES ('Canceled') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Closed')
	INSERT INTO dbo.status (status) VALUES ('Closed') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Deleted')
	INSERT INTO dbo.status (status) VALUES ('Deleted') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Draft')
	INSERT INTO dbo.status (status) VALUES ('Draft') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Hold')
	INSERT INTO dbo.status (status) VALUES ('Hold') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Pending')
	INSERT INTO dbo.status (status) VALUES ('Pending') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'PendingMobile')
	INSERT INTO dbo.status (status) VALUES ('PendingMobile') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Validated')
	INSERT INTO dbo.status (status) VALUES ('Validated') 
IF NOT EXISTS (SELECT 1 FROM dbo.status S WITH (NOLOCK) WHERE S.Status = 'Xfer')
	INSERT INTO dbo.status (status) VALUES ('Xfer') 
GO
--End table dbo.Status

--Begin table dbo.TierPointOfContact
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.TierPointOfContact'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN

	CREATE TABLE dbo.TierPointOfContact
		(
		TierPointOfContactID int IDENTITY(1,1) NOT NULL,
		TierID int,
		JLLISUserID int,
		IsPrimary bit
		)

	END
--ENDIF
		
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'IsPrimary', 'bit'

EXEC Utility.SetDefault @cTableName, 'IsPrimary', 1
EXEC Utility.SetDefault @cTableName, 'JLLISUserID', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'IsPrimary', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'JLLISUserID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'TierPointOfContactID'
EXEC Utility.SetIndexClustered 'IX_TierPointOfContact', @cTableName, 'TierID ASC,JLLISUserID ASC'
GO
--End table dbo.TierPointOfContact
--End new table creation

--Begin dbo.JLLISUser modification
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.JLLISUser'

EXEC Utility.AddColumn @cTableName, 'email2', 'varchar(320)'
EXEC Utility.AddColumn @cTableName, 'email3', 'varchar(320)'
GO
--End dbo.JLLISUser modification
	
--Begin dbo.tier modification
IF EXISTS (SELECT 1 FROM sys.foreign_keys FK WHERE FK.object_id = OBJECT_ID('dbo.FK_JLLISUser_Tier') AND FK.parent_object_id = OBJECT_ID('dbo.JLLISUser'))
	ALTER TABLE dbo.JLLISUser DROP CONSTRAINT FK_JLLISUser_Tier
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys FK WHERE FK.object_id = OBJECT_ID('dbo.FK_TierUser_Tier') AND FK.parent_object_id = OBJECT_ID('dbo.TierUser'))
	ALTER TABLE dbo.TierUser DROP CONSTRAINT FK_TierUser_Tier
GO

UPDATE dbo.tier
SET autoemail = 0
WHERE autoemail <> 1
GO

UPDATE dbo.tier
SET autoregister = 0
WHERE autoregister <> 1
GO

UPDATE dbo.tier
SET IsActive = 0
WHERE IsActive <> 1
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Tier'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'AutoEmail', 'bit'
EXEC Utility.AddColumn @cTableName, 'AutoRegister', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageContent', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageDropdowns', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageLessonTabs', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageMenu', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageOpEx', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageMetaData', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageSetup', 'bit'
EXEC Utility.AddColumn @cTableName, 'CanManageTabs', 'bit'
EXEC Utility.AddColumn @cTableName, 'DropdownSearchMode', 'int'
EXEC Utility.AddColumn @cTableName, 'InstanceDSN', 'varchar(50)'
EXEC Utility.AddColumn @cTableName, 'InstanceID', 'int'
EXEC Utility.AddColumn @cTableName, 'InstanceType', 'varchar(50)'
EXEC Utility.AddColumn @cTableName, 'IsActive', 'bit'
EXEC Utility.AddColumn @cTableName, 'IsInstance', 'bit'
EXEC Utility.AddColumn @cTableName, 'IsLegacyInstance', 'bit'
EXEC Utility.AddColumn @cTableName, 'MenuPosition', 'varchar(10)'
EXEC Utility.AddColumn @cTableName, 'ParentTierID', 'int'
EXEC Utility.AddColumn @cTableName, 'TierImageName', 'varchar(50)'

ALTER TABLE dbo.tier ALTER COLUMN AutoEmail bit
ALTER TABLE dbo.tier ALTER COLUMN AutoRegister bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageContent bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageLessonTabs bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageDropdowns bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageMenu bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageOpEx bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageMetaData bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageSetup bit
ALTER TABLE dbo.tier ALTER COLUMN CanManageTabs bit
ALTER TABLE dbo.tier ALTER COLUMN IsActive bit
ALTER TABLE dbo.tier ALTER COLUMN IsInstance bit
ALTER TABLE dbo.tier ALTER COLUMN IsLegacyInstance bit

EXEC Utility.SetDefault @cTableName, 'AutoEmail', 0
EXEC Utility.SetDefault @cTableName, 'AutoRegister', 0
EXEC Utility.SetDefault @cTableName, 'CanManageContent', 0
EXEC Utility.SetDefault @cTableName, 'CanManageDropdowns', 0
EXEC Utility.SetDefault @cTableName, 'CanManageLessonTabs', 0
EXEC Utility.SetDefault @cTableName, 'CanManageMenu', 0
EXEC Utility.SetDefault @cTableName, 'CanManageOpEx', 0
EXEC Utility.SetDefault @cTableName, 'CanManageMetaData', 0
EXEC Utility.SetDefault @cTableName, 'CanManageSetup', 0
EXEC Utility.SetDefault @cTableName, 'CanManageTabs', 0
EXEC Utility.SetDefault @cTableName, 'DefaultRole', 2000
EXEC Utility.SetDefault @cTableName, 'DropdownSearchMode', 1
EXEC Utility.SetDefault @cTableName, 'InstanceID', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 0
EXEC Utility.SetDefault @cTableName, 'IsInstance', 0
EXEC Utility.SetDefault @cTableName, 'IsLegacyInstance', 1
EXEC Utility.SetDefault @cTableName, 'MenuPosition', 'Left'
EXEC Utility.SetDefault @cTableName, 'ParentTierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AutoEmail', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'AutoRegister', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageContent', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageDropdowns', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageLessonTabs', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageMenu', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageOpEx', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageMetaData', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageSetup', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'CanManageTabs', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'DropdownSearchMode', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'InstanceID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsInstance', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsLegacyInstance', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'MenuPosition', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'ParentTierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TierID'
GO

UPDATE dbo.tier
SET tierdescription = 
	CASE 
		WHEN tiername = 'NOMI'
		THEN 'Navy Medicine'
		ELSE tiername
	END
WHERE IsInstance = 1
GO
--End dbo.tier modification

--Begin dbo.TierUser modification
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.TierUser'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'DefaultTierID', 'int'

EXEC Utility.SetDefault @cTableName, 'DefaultTierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DefaultTierID', 'int'
GO

ALTER TABLE dbo.TierUser ADD CONSTRAINT PK_TierUser PRIMARY KEY CLUSTERED 
	(
	TierUserID ASC,
	TierID ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE dbo.TierUser WITH NOCHECK ADD CONSTRAINT FK_TierUser_JLLISUser FOREIGN KEY (JLLISUserID) REFERENCES dbo.JLLISUser (JLLISUserID)
GO

ALTER TABLE dbo.TierUser CHECK CONSTRAINT FK_TierUser_JLLISUser
GO

ALTER TABLE dbo.TierUser  WITH NOCHECK ADD CONSTRAINT FK_TierUser_Tier FOREIGN KEY (tierid) REFERENCES dbo.tier (tierid)
GO

ALTER TABLE dbo.TierUser CHECK CONSTRAINT FK_TierUser_Tier
GO
--End dbo.TierUser modification

--Begin create GetClassificationWeight function
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID('dbo.GetSecLevByUserIDAndTierID') AND type in ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION dbo.GetSecLevByUserIDAndTierID
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.GetClassificationWeight') AND O.type in ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION dbo.GetClassificationWeight
GO

-- ========================================================================================
-- Author:   Todd Pires
-- Create date: 2010.06.22
-- Description: A function to return an integer value representing a varchar classification
-- ========================================================================================
CREATE FUNCTION dbo.GetClassificationWeight
(
@cClassification varchar(50)
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
SET @nRetVal = 0

IF LEN(RTRIM(@cClassification)) > 0 AND @cClassification IS NOT NULL
	BEGIN

	SET @nRetVal = 
		(
		SELECT C.ClassificationWeight
		FROM dbo.Classification C WITH (NOLOCK)
		WHERE C.Classification = @cClassification
		)

	END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO
--End create GetClassificationWeight function

--Begin create GetPersonNameByUserID function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.GetPersonNameByUserID') AND O.type IN ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION Utility.GetPersonNameByUserID
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2011.03.25
-- Description:	A function to return the name of a user in a specified format from a tieruserid
-- ============================================================================================

CREATE FUNCTION Utility.GetPersonNameByUserID
(
@nUserID int,
@nInstanceID int,
@cFormat varchar(50)
)

RETURNS varchar(250)

AS
BEGIN

DECLARE @cRetVal varchar(250)
SET @cRetVal = ''

IF @nUserID IS NOT NULL AND @nUserID > 0
	BEGIN
	
	SET @cRetVal = 
		(
		SELECT

			CASE
				WHEN @cFormat = 'FirstLast'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.FirstName) + ' ' + LTRIM(JU.LastName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END

				WHEN @cFormat = 'LastFirst'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.LastName) + ', ' + LTRIM(JU.FirstName)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName)
						ELSE LTRIM(JU.FirstName)
					END

				WHEN @cFormat = 'LastFirstTitle'
				THEN
				
					CASE
						WHEN LEN(RTRIM(JU.FirstName)) > 0 AND LEN(RTRIM(JU.LastName)) > 0
			 			THEN RTRIM(JU.LastName) + ', ' + LTRIM(JU.FirstName) + ' ' + LTRIM(JU.Title)
						WHEN LEN(RTRIM(JU.LastName)) > 0
						THEN LTRIM(JU.LastName) + ' ' + LTRIM(JU.Title)
						ELSE LTRIM(JU.FirstName) + ' ' + LTRIM(JU.Title)
					END
			END
						
		FROM dbo.JLLISUser JU WITH (NOLOCK)
			JOIN dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
				AND TU.TierID = @nInstanceID
				AND TU.TierUserID = @nUserID
			)
		
	END
--ENDIF

RETURN ISNULL(@cRetVal, '')

END

GO
--End create GetPersonNameByUserID function

-- Populate the tier table
DECLARE @cTierName varchar(50)

SET @cTierName = 'ACGU'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'ARMY'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'CCO'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'DISA'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'DLA'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'DOS'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'DTRA'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'HPRC'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'JSCC'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'NAVY'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'NCCS'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'NGA'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'NGB'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'NOMI'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'ORCHID'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'RPB'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'USAF'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'USSOCOM'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)

SET @cTierName = 'USUHS'
IF NOT EXISTS (SELECT 1 FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = @cTierName)
	INSERT INTO JLLIS.dbo.Tier (TierName) VALUES (@cTierName)
GO

-- Set ARMYISR as a child of army
UPDATE JLLIS.dbo.Tier
SET ParentTierID = (SELECT TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'ARMY')
WHERE TierName = 'ARMYISR'
GO

-- Update the IsInstance bit
UPDATE dbo.Tier 
SET 
	InstanceDSN = TierName,
	InstanceID = TierID,
	IsInstance = 1
WHERE ParentTierID = 0
GO

-- Begin Insert the child tiers
DECLARE @cCategory varchar(10)
DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)
DECLARE @cTierName varchar(50)

DECLARE @cCRLF varchar(2)
DECLARE @cTab1 varchar(1)
DECLARE @cTab2 varchar(2)
DECLARE @cTab3 varchar(3)
DECLARE @cTab4 varchar(4)
DECLARE @cTab5 varchar(5)
DECLARE @cTab6 varchar(5)

SET @cCRLF = CHAR(13) + CHAR(10)
SET @cTab1 = CHAR(9)
SET @cTab2 = REPLICATE(CHAR(9), 2)
SET @cTab3 = REPLICATE(CHAR(9), 3)
SET @cTab4 = REPLICATE(CHAR(9), 4)
SET @cTab5 = REPLICATE(CHAR(9), 5)
SET @cTab6 = REPLICATE(CHAR(9), 6)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		UPPER(DB.Name),

		CASE
			WHEN UPPER(DB.Name) IN ('SOCOM','SOCOMNEW')
			THEN 'USSOCOM'
			WHEN UPPER(DB.Name) = 'CFMCCLL'
			THEN 'MCCLL'
			ELSE UPPER(DB.Name) 
		END AS TierName,

		CASE
			WHEN UPPER(DB.Name) IN ('JSCC','NAVY','NGB','USAF')
			THEN 'TIER1'
			ELSE 'MSC'
		END AS Category
		
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('ACGU','ARMY','CCO','CFMCCLL','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor into @cDatabaseName, @cTierName, @cCategory
WHILE @@fetch_status = 0
	BEGIN
		
	SET @cSQL = 'INSERT INTO JLLIS.dbo.Tier'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(TierName,ParentTierID)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT DISTINCT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DD.DisplayText,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SELECT T.TierID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'WHERE T.TierName = ''' + @cTierName + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND T.IsInstance = 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName + '.dbo.DropDown DD WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + 'WHERE DD.Category = ''' + @cCategory + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'AND DD.DisplayText IS NOT NULL'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'AND LEN(RTRIM(DD.DisplayText)) > 0'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'WHERE T.TierName = DD.DisplayText'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'AND T.InstanceID ='
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'SELECT T.TierID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'WHERE T.TierName = ''' + @cTierName + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + 'AND T.IsInstance = 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + ')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + ')'
	SET @cSQL = @cSQL + @cCRLF + 'ORDER BY DD.DisplayText'

	EXEC (@cSQL)
	--print @cSQL
	--print @cCRLF
				
	FETCH oCursor into @cDatabaseName, @cTierName, @cCategory
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		UPPER(DB.Name),
		UPPER(DB.Name) AS TierName,
		'TIER2' AS Category
	FROM sys.Databases DB WITH (NOLOCK) 
	WHERE DB.Name IN ('JSCC','NAVY','NGB','USAF')
	ORDER BY DB.Name


OPEN oCursor
FETCH oCursor into @cDatabaseName, @cTierName, @cCategory
WHILE @@fetch_status = 0
	BEGIN
		
	SET @cSQL = 'INSERT INTO JLLIS.dbo.Tier'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '(TierName,ParentTierID)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT DISTINCT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'DD.DisplayText,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'SELECT T.TierID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'WHERE T.TierName = ''' + @cTierName + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND T.IsInstance = 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + ')'
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName + '.dbo.DropDown DD WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierName = DD.ST1'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND DD.Category = ''' + @cCategory + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND DD.DisplayText IS NOT NULL'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND LEN(RTRIM(DD.DisplayText)) > 0'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'WHERE T.TierName = DD.DisplayText'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'AND T.InstanceID ='
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + 'SELECT T.TierID'
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + 'FROM JLLIS.dbo.Tier T WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + 'WHERE T.TierName = ''' + @cTierName + ''''
	SET @cSQL = @cSQL + @cCRLF + @cTab6 + 'AND T.IsInstance = 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab5 + ')'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + ')'
	SET @cSQL = @cSQL + @cCRLF + 'ORDER BY DD.DisplayText'

	EXEC (@cSQL)
	--print @cSQL
	--print @cCRLF
				
	FETCH oCursor into @cDatabaseName, @cTierName, @cCategory
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor
GO
-- End Insert the child tiers

DELETE TU
FROM JLLIS.dbo.tieruser TU
	JOIN JLLIS.dbo.Tier T ON T.TierID = TU.TierID
		AND T.TierName IN 
			(
			'DCIHKB',
			'DCIKB',
			'DEMO',
			'PKSOI',
			'PKSOI_org'
			)
GO

UPDATE JU
SET JU.OriginatingTierID = 1
FROM JLLIS.dbo.JLLISUser JU
	JOIN JLLIS.dbo.Tier T ON T.TierID = JU.OriginatingTierID
		AND T.TierName IN 
			(
			'DCIHKB',
			'DCIKB',
			'DEMO',
			'PKSOI',
			'PKSOI_org'
			)
GO

DELETE 
FROM JLLIS.dbo.Tier 
WHERE TierName IN 
	(
	'DCIHKB',
	'DCIKB',
	'DEMO',
	'PKSOI',
	'PKSOI_org'
	)
GO

DELETE TU1
FROM JLLIS.dbo.TierUser TU1 WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = TU1.TierID
		AND T.TierName = 'ArmyISR'
		AND EXISTS
			(
			SELECT 1
			FROM JLLIS.dbo.TierUser TU2 WITH (NOLOCK)
				JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = TU2.TierID
					AND T.TierName = 'Army'
					AND TU1.TierUserID = TU2.TierUserID
			)
GO

UPDATE TU
SET TU.TierID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WITH (NOLOCK) WHERE T.TierName = 'Army')
FROM JLLIS.dbo.TierUser TU WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = TU.TierID
		AND T.TierName = 'ArmyISR'
GO

-- Update the InstanceType field
UPDATE T
SET T.InstanceType = 

	CASE
		WHEN TierName IN ('ORCHID')
		THEN 'GLOBAL'
		WHEN TierName IN ('USSOCOM')
		THEN 'HYBRID'
		ELSE 'SILO'
	END
	
FROM dbo.Tier T
WHERE T.IsInstance = 1
GO

-- Begin populate the InstanceID column
DECLARE @nTierID int
  
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
SELECT T.TierID
FROM JLLIS.dbo.Tier T WITH (NOLOCK)
WHERE InstanceID = 0
	
OPEN oCursor
FETCH oCursor INTO @nTierID
WHILE @@fetch_status = 0
	BEGIN

	WITH HD (TierID,ParentTierID)
		AS 
		(
		SELECT
			T.TierID, 
			T.ParentTierID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		WHERE T.TierID = @nTierID
	
		UNION ALL 
		
		SELECT
			T.TierID, 
			T.ParentTierID
		FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
			JOIN HD ON HD.ParentTierID = T.TierID 
		)

	UPDATE JLLIS.dbo.Tier
	SET InstanceID = 
		ISNULL(
			(
			SELECT TOP 1 T.TierID
			FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
				JOIN HD ON HD.TierID = T.TierID 
					AND T.IsInstance = 1
			), 0)
	WHERE TierID = @nTierID

	FETCH oCursor INTO @nTierID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
-- End populate the InstanceID column

-- Update the can manage bits
UPDATE JLLIS.dbo.Tier
SET 
	CanManageContent = 1,
	CanManageDropDowns = 1,
	CanManageLessonTabs = 1,
	CanManageMenu = 1,
	CanManageMetaData = 1,
	CanManageOpEx = 1,
	CanManageSetup = 1
WHERE IsInstance = 1
GO

--Begin Set the default tier id's
UPDATE JLLIS.dbo.TierUser
SET DefaultTierID = 0
GO

UPDATE TU
SET TU.DefaultTierID = T1.TierID
FROM JLLIS.dbo.TierUser TU
	JOIN JLLIS.dbo.Tier T1 ON T1.TierName = LTRIM(RTRIM(TU.MSC))
		AND TU.MSC IS NOT NULL
		AND LEN(RTRIM(TU.MSC)) > 0
		AND TU.DefaultTierID = 0
GO

UPDATE TU
SET TU.DefaultTierID = T1.TierID
FROM JLLIS.dbo.TierUser TU
	JOIN JLLIS.dbo.Tier T1 ON T1.TierName = LTRIM(RTRIM(TU.ST1))
		AND TU.ST1 IS NOT NULL
		AND LEN(RTRIM(TU.ST1)) > 0
		AND TU.DefaultTierID = 0
GO

UPDATE TU
SET TU.DefaultTierID = TU.TierID
FROM JLLIS.dbo.TierUser TU
WHERE TU.DefaultTierID = 0
GO
--End Set the default tier id's

--Begin deprecated category and column rename
DECLARE @cDatabaseName varchar(50)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT UPPER(DB.Name) 
	FROM sys.Databases DB WITH (NOLOCK) 
		JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON UPPER(T.TierName) = UPPER(DB.Name)
			AND T.IsLegacyInstance = 0
			AND DB.Name IN ('ARMY','CCO','DISA','DLA','DOS','DTRA','HPRC','JSCC','NAVY','NCCS','NGA','NGB','NOMI','ORCHID','RPB','SOCOM','SOCOMNEW','USAF','USSOCOM','USUHS')
	ORDER BY DB.Name
		
OPEN oCursor
FETCH oCursor into @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC Utility.RenameDeprecatedCategories @cDatabaseName, 'ToOld'
	EXEC Utility.RenameDeprecatedColumns @cDatabaseName, 'ToOld'
	
	FETCH oCursor into @cDatabaseName
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End deprecated category and column rename