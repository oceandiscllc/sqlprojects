USE ARMY
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF (SELECT OBJECT_ID('tempdb.dbo.#oInstanceDataTable', 'u')) IS NOT NULL
  DROP TABLE #oInstanceDataTable
--ENDIF

CREATE TABLE #oInstanceDataTable (TierID int, TierName varchar(50))
GO

INSERT INTO #oInstanceDataTable 
	(TierID,TierName) 
SELECT 
	T.TierID,
	T.TierName
FROM JLLIS.dbo.Tier T WITH (NOLOCK)
WHERE T.IsInstance = 1
	AND T.TierName = 'ARMY'
GO

--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--Begin create utility helper tools
--Begin create IsAncestor helper function
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.IsAncestor') AND O.type in ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION Utility.IsAncestor
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2010.09.27
-- Description:	A function to validate that one tier is an ancestor of another.
--							Parameter @nTierID1 is the ancestral tier.
--							Parameter @nTierID2 is the descendant tier.
--							A call of Utility.IsAncestor(x, y) will return a 1 if x is an 
--							ancestor of y and a 0 if it is not.  If x = y, the function
--							returns 1.
-- ============================================================================

CREATE FUNCTION Utility.IsAncestor
(
@nTierID1 int,
@nTierID2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
DECLARE @oTable table (TierID int)
	
SET @nRetVal = 0;

WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID2

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable
	(TierID)
SELECT HD.TierID
FROM HD 
WHERE HD.TierID = @nTierID1

IF EXISTS (SELECT 1 FROM @oTable)
	SET @nRetVal = 1

RETURN @nRetVal

END
GO
--End create IsAncestor helper function

--Begin create AddColumn helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.AddColumn') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.AddColumn
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.AddColumn
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name = @cColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD ' + @cColumnName + ' ' + @cDataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End create AddColumn helper stored procedure

--Begin create CloneData helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CloneData') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CloneData
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CloneData
	@cTableName varchar(250),
	@cDatabaseName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)

	SET @cSQL1 = ''

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SC.Name 
		FROM dbo.syscolumns SC WITH (NOLOCK) 
		WHERE SC.id = OBJECT_ID(@cTableName) 
		ORDER BY SC.Name

	OPEN oCursor
	FETCH oCursor INTO @cSQL2
	WHILE @@fetch_status = 0
		BEGIN

		IF @cSQL1 = ''
			SET @cSQL1 = @cSQL2
		ELSE
			SET @cSQL1 = @cSQL1 + ',' + @cSQL2

		FETCH oCursor INTO @cSQL2

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	SET @cSQL3 = 'SET IDENTITY_INSERT ' + @cTableName + ' ON'
	SET @cSQL3 = @cSQL3 + ' INSERT INTO ' + @cTableName + ' (' + @cSQL1 + ') SELECT ' + @cSQL1 + ' FROM ' + @cDatabaseName + '.' + @cTableName
	SET @cSQL3 = @cSQL3 + ' SET IDENTITY_INSERT ' + @cTableName + ' OFF'

	EXEC (@cSQL3)

END
GO
--End create CloneData helper stored procedure

--Begin create CompareTableStructures helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CompareTableStructures') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CompareTableStructures
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.22
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CompareTableStructures
	@cTableName varchar(250),
	@cDatabaseName1 varchar(250),
	@cDatabaseName2 varchar(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cNotExists1 varchar(max)
	DECLARE @cNotExists2 varchar(max)
	DECLARE @cNotExists3 varchar(max)
	DECLARE @cSchemaName varchar(50)
	DECLARE @cSelectList1 varchar(max)
	DECLARE @cSelectList2 varchar(max)
	DECLARE @cSQL varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cTab5 varchar(5)
	DECLARE @nPos int

	CREATE table #oTable 
		(
		Description varchar(250) NULL,
		ObjectType varchar(50) NULL, 
		SchemaName varchar(50) NULL, 
		TableName varchar(50) NULL, 
		ColumnName varchar(50) NULL, 
		DataType varchar(50) NULL, 
		Length int NULL, 
		Precision int NULL, 
		Scale int NULL,
		Nullable varchar(50) NULL
		) 

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)
	SET @cTab5 = REPLICATE(CHAR(9), 5)
	
	SET @cNotExists1 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab3 + ')'

	SET @cNotExists2 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND C2.Name = C1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + '('
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'FROM #oTable T2'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'WHERE T2.ObjectType IN (''Table'', ''View'')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab4 + 'AND T2.TableName = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'

	SET @cSchemaName = 'dbo'

	SET @cSelectList1 = @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'WHEN O1.Type = ''U'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'THEN ''Table'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'ELSE ''View'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'END,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'O1.Name'

	SET @cSelectList2 = '''Column'','
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'O1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'T1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Max_Length,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Precision,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Scale,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'WHEN C1.Is_Nullable = 1'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'THEN ''Yes'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'ELSE ''No'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'END'
	SET @cSelectList2 = @cSelectList2 + @cCRLF

	SET @nPos = CHARINDEX('.', @cTableName)

	IF @nPos > 0
		BEGIN
		
		SET @cSchemaName = LEFT(@cTableName, @nPos - 1)
		SET @cTableName = RIGHT(@cTableName, LEN(@cTableName) - @nPos)
		
		END
	--ENDIF

	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''' AS Description,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1

	EXEC (@cSQL)
	
	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName, ColumnName, DataType, Length, Precision, Scale, Nullable)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.Types T1 ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF
	
	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.Types T1 ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2

	EXEC (@cSQL)

	SELECT *
	FROM #oTable T
	ORDER BY T.ObjectType DESC, T.Description, T.SchemaName, T.TableName, T.ColumnName	
	
	DROP TABLE #oTable

END
GO
--End create CompareTableStructures helper stored procedure


--Begin create DropConstraintsAndIndexes helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.DropConstraintsAndIndexes') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.DropConstraintsAndIndexes
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropConstraintsAndIndexes
	@cTableName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL varchar(max)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC WITH (NOLOCK)
		WHERE DC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @cTableName AS SQL
		FROM sys.indexes I WITH (NOLOCK)
		WHERE I.object_ID = OBJECT_ID(@cTableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End create DropConstraintsAndIndexes helper stored procedure

--Begin create RenameDeprecatedCategories helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedCategories') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedCategories
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedCategories
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cSQL = 'UPDATE ' + @cDatabaseName + '.dbo.DropDown'
	ELSE
		SET @cSQL = 'UPDATE dbo.DropDown'
	--ENDIF
	
	IF @cMode = 'FromOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = REPLACE(Category, ''_Old'', '''')'
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC_Old'',''TIER1_Old'',''TIER2_Old'',''SubTier1_Old'',''Unit_Old'')'
		
		END
	ELSE IF @cMode = 'ToOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = Category + ''_Old'''
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC'',''TIER1'',''TIER2'',''SubTier1'',''Unit'')'
		
		END
	--ENDIF
	
	EXEC (@cSQL)
	
END
GO
--End create RenameDeprecatedCategories helper stored procedure

--Begin create RenameDeprecatedColumns helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedColumns') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedColumns
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedColumns
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDatabaseNameWithPeriod varchar(50)
	DECLARE @cSQL varchar(max)
	
	SET @cDatabaseNameWithPeriod = ''
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cDatabaseNameWithPeriod = @cDatabaseName + '.'
	
	SET @cSQL = 'INSERT INTO #oTable (SQLText) SELECT '
	SET @cSQL = @cSQL + '''EXEC ' + @cDatabaseNameWithPeriod + 'dbo.sp_rename '''''''
	SET @cSQL = @cSQL + ' + S.Name + ''.'' + O.Name + ''.'' + C.Name + '''''', '''''' +'
		
	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + ' REPLACE(C.Name, ''_Old'', '''')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + ' C.Name + ''_Old'''
	
	SET @cSQL = @cSQL + ' + '''''', ''''COLUMN'''''' AS SQLText'
	SET @cSQL = @cSQL + '	FROM ' + @cDatabaseNameWithPeriod + 'sys.objects O WITH (NOLOCK)'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.schemas S ON S.schema_ID = O.schema_ID'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id'
	SET @cSQL = @cSQL + '	AND O.type = ''U'''

	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand_old'', ''msc_old'', ''st1_old'')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand'', ''msc'', ''st1'')'

	SET @cSQL = @cSQL + '	ORDER BY S.Name, O.Name, C.Name'
	
	CREATE TABLE #oTable (SQLText varchar(max))
	
	EXEC (@cSQL)
	
	DECLARE oRenameDeprecatedColumnsCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT SQLText
		FROM #oTable
		
	OPEN oRenameDeprecatedColumnsCursor
	FETCH oRenameDeprecatedColumnsCursor into @cSQL
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC (@cSQL)
	
		FETCH oRenameDeprecatedColumnsCursor into @cSQL
	
		END
	--END WHILE
	
	CLOSE oRenameDeprecatedColumnsCursor
	DEALLOCATE oRenameDeprecatedColumnsCursor
	
	DROP TABLE #oTable

END
GO
--End create RenameDeprecatedColumns helper stored procedure

--Begin create SetColumnNotNull helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetColumnNotNull') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetColumnNotNull
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.01.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetColumnNotNull
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ALTER COLUMN ' + @cColumnName + ' ' + @cDataType + ' NOT NULL'
	EXEC (@cSQL)
		
END
GO
--End create SetColumnNotNull helper stored procedure

--Begin create SetDefault helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetDefault') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetDefault
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetDefault
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDefault varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cConstraintName varchar(500)
	DECLARE @cSQL varchar(max)
	DECLARE @nDefaultIsGetDate bit
	DECLARE @nDefaultIsNumeric bit
	DECLARE @nLength int

	SET @nDefaultIsGetDate = 0

	IF @cDefault = 'getDate()'
		SET @nDefaultIsGetDate = 1
		
	SET @nDefaultIsNumeric = ISNUMERIC(@cDefault)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName
	
	IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ''' + @cDefault + ''' WHERE ' + @cColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ' + @cDefault + ' WHERE ' + @cColumnName + ' IS NULL'

	EXECUTE (@cSQL)

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cConstraintName = 'DF_' + RIGHT(@cTableName, @nLength) + '_' + @cColumnName
	
	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC WITH (NOLOCK) WHERE DC.parent_object_ID = OBJECT_ID(@cTableName) AND DC.Name = @cConstraintName)
		BEGIN	

		IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @cDefault + ''' FOR ' + @cColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @cDefault + ' FOR ' + @cColumnName
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End create SetDefault helper stored procedure

--Begin create SetIndexClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexClustered helper stored procedure

--Begin create SetIndexNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexNonClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexNonClustered helper stored procedure

--Begin create SetPrimaryKeyClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyClustered helper stored procedure

--Begin create SetPrimaryKeyNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyNonClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyNonClustered helper stored procedure
--End create utility helper tools

--Begin table cleanup
DECLARE @cName varchar(250)
DECLARE @cSQL varchar(max)

DECLARE oDropCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 'ALTER TABLE ' + O.Name + ' DROP CONSTRAINT ' + FK.Name AS SQL
	FROM sys.foreign_keys FK
		JOIN sys.Objects O ON O.Object_ID = FK.Parent_Object_ID
			AND O.Name IN
				(
				SELECT T.Name
				FROM sys.tables T WITH (NOLOCK)
				WHERE T.Type = 'U'
					AND 
						(
						T.Name LIKE '%_Old' OR
						T.Name LIKE '%Back%' OR
						T.Name LIKE 'DynamicForm%' OR
						T.Name LIKE 'Sodar%' OR -- Delete this line when Sodar is implemented in n-tier
						T.Name LIKE 'Soflo%' OR -- Delete this line when Soflo is implemented in n-tier
						T.Name IN 
							(
							'areacode',
							'armessage',
							'armessq',
							'classifications',
							'cleandb',
							'commandcountryassociations', -- Delete this line when Sodar is implemented in n-tier
							'commands', -- Delete this line when Sodar is implemented in n-tier
							'countries',
							'etext',
							'lmsopex', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
							'opexdates', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
							'opexlocations', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
							'recall',
							'scripts',
							'searches',
							'testodbc',
							'UG'
							)
						)
				)
	
OPEN oDropCursor
FETCH oDropCursor INTO @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXECUTE (@cSQL)

	FETCH oDropCursor INTO @cSQL

	END
--END WHILE

CLOSE oDropCursor
DEALLOCATE oDropCursor
	
DECLARE oDropCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		T.Name,
		'DROP TABLE ' + T.Name AS SQL
	FROM sys.tables T WITH (NOLOCK)
	WHERE T.Type = 'U'
		AND 
			(
			T.Name LIKE '%_Old' OR
			T.Name LIKE '%Back%' OR
			T.Name LIKE 'DynamicForm%' OR
			T.Name LIKE 'Sodar%' OR -- Delete this line when Sodar is implemented in n-tier
			T.Name LIKE 'Soflo%' OR -- Delete this line when Soflo is implemented in n-tier
			T.Name IN 
				(
				'areacode',
				'armessage',
				'armessq',
				'classifications',
				'cleandb',
				'commandcountryassociations', -- Delete this line when Sodar is implemented in n-tier
				'commands', -- Delete this line when Sodar is implemented in n-tier
				'countries',
				'etext',
				'lmsopex', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
				'opexdates', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
				'opexlocations', -- Delete this line when the OpEx multiple date / location feature is implemented in n-tier
				'recall',
				'scripts',
				'searches',
				'testodbc',
				'UG'
				)
			)
	ORDER BY SQL

OPEN oDropCursor
FETCH oDropCursor INTO @cName, @cSQL
WHILE @@fetch_status = 0
	BEGIN

	EXEC Utility.DropConstraintsAndIndexes @cName
	EXECUTE (@cSQL)

	FETCH oDropCursor INTO @cName, @cSQL

	END
--END WHILE

CLOSE oDropCursor
DEALLOCATE oDropCursor
GO
--End table cleanup

--Begin deprecated category and column rename
EXEC Utility.RenameDeprecatedCategories NULL, 'FromOld'
EXEC Utility.RenameDeprecatedColumns NULL, 'FromOld'
GO
--End deprecated category and column rename

--Begin table updagrades
--Begin table: AAR
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.AAR'

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.AAR ALTER COLUMN conclusion varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN condition varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN description varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN fromline varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN headline1 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN headline2 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN headline3 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN headline4 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN headline5 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN poc varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN refline varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN refline2 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN refline3 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN reportdate varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN sig varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN ssic varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN status varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN title varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN toline varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN type varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN unitposition varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN vialine varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN vialine2 varchar(max)
ALTER TABLE dbo.AAR ALTER COLUMN vialine3 varchar(max)

EXEC Utility.AddColumn @cTableName, 'organization', 'varchar(max) NULL'
EXEC Utility.AddColumn @cTableName, 'originatingtierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'CreatedBy', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'UpdatedBy', 0
EXEC Utility.SetDefault @cTableName, 'UpdateDate', 'getDate()'

EXEC Utility.SetColumnNotNull @cTableName, 'CreatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateDate', 'datetime'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AARID'
GO

UPDATE dbo.AAR
SET
	conclusion = conclusion,
	condition = condition,
	description = description,
	fromline = fromline,
	headline1 = headline1,
	headline2 = headline2,
	headline3 = headline3,
	headline4 = headline4,
	headline5 = headline5,
	poc = poc,
	refline = refline,
	refline2 = refline2,
	refline3 = refline3,
	reportdate = reportdate,
	sig = sig,
	ssic = ssic,
	status = status,
	title = title,
	toline = toline,
	type = type,
	unitposition = unitposition,
	vialine = vialine,
	vialine2 = vialine2,
	vialine3 = vialine3
GO
--End table: AAR

--Begin table: AARItem
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.AARItem'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'displayorder', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'AARID', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'ItemID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AARID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ItemID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'AARItemID'
EXEC Utility.SetIndexClustered 'IX_AARItem', @cTableName, 'aarid ASC,itemid ASC,type ASC'
GO
--End table: AARItem

--Begin table: AFDL
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.AFDL'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1

	CREATE TABLE dbo.afdl
		(
		afdlid int NOT NULL IDENTITY (1, 1),
		Title varchar(255) NULL,
		Subject varchar(255) NULL,
		Description text NULL,
		MajComp text NULL,
		Merit text NULL,
		Functional varchar(50) NULL,
		SubFunction varchar(50) NULL,
		timeframe text NULL,
		def varchar(10) NULL,
		chamption varchar(50) NULL,
		cochamp varchar(50) NULL,
		opr text NULL,
		ocr text NULL,
		poc text NULL,
		dotmlpf varchar(200) NULL,
		add1 text NULL,
		add2 text NULL,
		add3 text NULL,
		add4 text NULL,
		add5 text NULL,
		add6 text NULL,
		add7a text NULL,
		add7b text NULL,
		add8 text NULL,
		status varchar(50) NULL,
		classification varchar(50) NULL,
		cads varchar(100) NULL,
		caps text NULL,
		reldefs text NULL,
		node2035 int NOT NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Node2035', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Node2035', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AFDLID'
EXEC Utility.SetIndexNonClustered 'IX_AFDL', @cTableName, 'Title ASC,AFDLID ASC'

IF @nMigrateData = 1
	EXEC Utility.CloneData @cTableName, 'ARMY'
GO
--End table: AFDL

--Begin table: AnalysisCodes
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.AnalysisCodes'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1

	CREATE TABLE dbo.analysiscodes
		(
		analysiscodeid int IDENTITY(1,1) NOT NULL,
		type varchar(50) NOT NULL,
		description varchar(250) NOT NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AnalysisCodeID'
EXEC Utility.SetIndexNonClustered 'IX_AnalysisCodes', @cTableName, 'type ASC,description ASC,analysiscodeid ASC'

IF @nMigrateData = 1
	EXEC Utility.CloneData @cTableName, 'ARMY'
GO
--End table: AnalysisCodes

--Begin table: Banner
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Banner'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.banner
		(
		id int IDENTITY(1,1) NOT NULL,
		tier varchar(50) NULL,
		imagename varchar(100) NULL,
		bgColor varchar(50) NOT NULL,
		isActive bit NOT NULL,
		bgImage varchar(50) NOT NULL,
		nofade bit NOT NULL,
		tierid int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'nofade', 'bit NULL'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

ALTER TABLE dbo.banner ALTER COLUMN BGColor varchar(50)
ALTER TABLE dbo.banner ALTER COLUMN NoFade bit
ALTER TABLE dbo.banner ALTER COLUMN BGImage varchar(50)

EXEC Utility.SetDefault @cTableName, 'BGColor', 'ffffff'
EXEC Utility.SetDefault @cTableName, 'BGImage', 'ffffff_bg.png'
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'NoFade', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BGColor', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'BGImage', 'varchar(50)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'NoFade', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'
GO
--End table: Banner

--Begin table: BannerBG
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.BannerBG'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1

	CREATE TABLE dbo.bannerbg
		(
		id int IDENTITY(1,1) NOT NULL,
		bgImage varchar(50) NULL,
		bgColor varchar(50) NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName
EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'

IF @nMigrateData = 1
	EXEC Utility.CloneData @cTableName, 'ARMY'
GO
--End table: BannerBG

--Begin table: BB
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.BB'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'coi', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'OriginatingTierID', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'releasableto', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'COI', 0
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'COI', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'BBID'
GO

UPDATE BB
SET BB.OriginatingTierID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)
FROM dbo.BB BB
WHERE BB.OriginatingTierID = 0
--End table: BB

--Begin table: BB_MileStones
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.BB_MileStones'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.bb_milestones
		(
		id int IDENTITY(1,1) NOT NULL,
		bb_id int NOT NULL,
		status varchar(50) NULL,
		date_due datetime NULL,
		date_complete datetime NULL,
		description varchar(255) NULL,
		date_start datetime NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'BB_ID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BB_ID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
EXEC Utility.SetIndexClustered 'IX_BB_MileStones', @cTableName, 'bb_id ASC,id ASC'
GO
--End table: BB_MileStones

--Begin table: BBFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.BBFile'

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'
GO
--End table: BBFile

--Begin table: Binder
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Binder'

EXEC Utility.AddColumn @cTableName, 'eventname', 'varchar(300) NULL'
EXEC Utility.AddColumn @cTableName, 'originatingtierid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'parentbinder', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'ParentBinder', 0

EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ParentBinder', 'int'
GO
--End table: Binder

--Begin table: BinderFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.BinderFile'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.binderfile
		(
		binderfileid int IDENTITY(1,1) NOT NULL,
		binderid int NOT NULL,
		condition varchar(20) NULL,
		status varchar(20) NULL,
		filename varchar(250) NULL,
		description varchar(max) NULL,
		isdefault bit NOT NULL,
		class varchar(50) NULL,
		relto varchar(50) NULL,
		caveat varchar(50) NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'IsDefault', 'bit NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'

ALTER TABLE dbo.BinderFile ALTER COLUMN IsDefault bit

EXEC Utility.SetDefault @cTableName, 'BinderID', 0
EXEC Utility.SetDefault @cTableName, 'IsDefault', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BinderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsDefault', 'bit'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'BinderFileID'
EXEC Utility.SetIndexClustered 'IX_BinderFile', @cTableName, 'binderid ASC,binderfileid ASC'
GO
--End table: BinderFile

--Begin table: BinderItem
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.BinderItem'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'BinderID', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'ItemID', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BinderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'ItemID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'BinderItemID'
EXEC Utility.SetIndexClustered 'IX_BinderItem', @cTableName, 'binderid ASC,itemid ASC,type ASC'
GO
--End table: BinderItem

--Begin table: CDR
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDR'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'countryid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'declassifyon', 'datetime NULL'
EXEC Utility.AddColumn @cTableName, 'doccountrydropdownid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'IsForReportLibrary', 'bit'
EXEC Utility.AddColumn @cTableName, 'overallcaveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'originatorcountryid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'originatingtierid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'thumbheight', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'thumbnail', 'varchar(200) NULL'
EXEC Utility.AddColumn @cTableName, 'thumbwidth', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'CountryID', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'DocCountryDropDownID', 0
EXEC Utility.SetDefault @cTableName, 'IsForReportLibrary', 0
EXEC Utility.SetDefault @cTableName, 'OriginatorCountryID', 0
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'ThumbHeight', 0
EXEC Utility.SetDefault @cTableName, 'ThumbWidth', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CountryID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'DocCountryDropDownID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForReportLibrary', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatorCountryID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ThumbHeight', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ThumbWidth', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'CDRID'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.CDR') AND SC.name='country_Old')
	EXEC sp_rename 'dbo.CDR.country_Old', 'country', 'COLUMN'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.CDR') AND SC.name='country')
	BEGIN

	UPDATE C1
	SET C1.OriginatorCountryID = C2.CountryID
	FROM dbo.CDR C1
		JOIN JLLIS.dbo.Country C2 ON C2.CountryName = C1.Country
			AND C1.Country IS NOT NULL
			AND LEN(RTRIM(C1.Country)) > 0
			AND C1.CountryID = 0
			
	UPDATE C1
	SET C1.OriginatorCountryID = C2.CountryID
	FROM dbo.CDR C1
		JOIN JLLIS.dbo.Country C2 ON C2.ISOCode3 = C1.Country
			AND C1.Country = 'USA'
			AND C1.CountryID = 0

	EXEC sp_rename 'dbo.CDR.country', 'country_Old', 'COLUMN'

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.CDR') AND SC.name='caveat_Old')
	EXEC sp_rename 'dbo.CDR.caveat_Old', 'caveat', 'COLUMN'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.CDR') AND SC.name='caveat')
	BEGIN
	
	DECLARE @cSQL varchar(max)

	SET @cSQL = 'UPDATE C1 SET C1.OverallCaveat = C1.Caveat FROM dbo.CDR C1 WHERE C1.Caveat IS NOT NULL AND C1.OverallCaveat IS NOT NULL AND LEN(RTRIM(C1.Caveat)) > 0 AND EXISTS (SELECT 1 FROM JLLIS.dbo.Caveat C2 WITH (NOLOCK) WHERE C2.Caveat = C1.Caveat)'
	
	EXEC (@cSQL)
	EXEC sp_rename 'dbo.CDR.caveat', 'caveat_Old', 'COLUMN'
	
	END
--ENDIF

IF EXISTS (SELECT 1 FROM dbo.syscolumns WITH (NOLOCK) WHERE id = OBJECT_ID(N'dbo.CDR') AND name='declassifiedon')
	EXEC sp_rename 'dbo.CDR.declassifiedon', 'declassifiedon_Old', 'COLUMN'
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.ReportToLibrary') AND O.type in (N'U')) 
	BEGIN

	DECLARE @cSQL varchar(max)

	SET @cSQL = 'UPDATE C SET C.IsForReportLibrary = 1 FROM dbo.CDR C JOIN dbo.ReportToLibrary RTL ON RTL.RefID = C.CDRID AND RTL.Type = ''CDR'''
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO

ALTER TABLE dbo.CDR ALTER COLUMN Title varchar(250)
GO
--End table: CDR

--Begin table: CDRFeaturedItem
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDRFeaturedItem'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.cdrfeatureditem
		(
		cdrfeatureditemid int IDENTITY(1,1) NOT NULL,
		cdrid int NOT NULL,
		tab varchar(50) NULL,
		sequence int NOT NULL,
		thumbwidth int NOT NULL,
		thumbheight int NOT NULL,
		archived int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'Archived', 0
EXEC Utility.SetDefault @cTableName, 'CDRID', 0
EXEC Utility.SetDefault @cTableName, 'Sequence', 0
EXEC Utility.SetDefault @cTableName, 'ThumbHeight', 0
EXEC Utility.SetDefault @cTableName, 'ThumbWidth', 0
EXEC Utility.SetDefault @cTableName, 'tierid', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Archived', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CDRID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Sequence', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ThumbHeight', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ThumbWidth', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'tierid', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'CDRFeaturedItemID'
EXEC Utility.SetIndexClustered 'IX_CDRFeaturedItem', @cTableName, 'cdrid ASC,tab ASC,sequence ASC'
GO

UPDATE dbo.cdrfeatureditem
SET TierID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)
WHERE TierID = 0
GO
--End table: CDRFeaturedItem

--Begin table: CDRFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CDRFile'

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'
GO
--End table: CDRFile

--Begin table: Content
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Content'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'entityid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'entitytypecode', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'EntityID', 0
EXEC Utility.SetDefault @cTableName, 'ImageHeight', 0
EXEC Utility.SetDefault @cTableName, 'ImageWidth', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'EntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ImageHeight', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ImageWidth', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ContentID'
EXEC Utility.SetIndexClustered 'IX_Content', @cTableName, 'entitytypecode ASC, entityid ASC, displayorder ASC'
GO

DECLARE @cChar char(1)
DECLARE @cSSiteIDText varchar(max)
DECLARE @cSSiteID varchar(max)
DECLARE @cSQL varchar(max)
DECLARE @nContentID int

UPDATE dbo.Content
SET entitytypecode = 'COI'
WHERE Type IN ('COI','COP','RCOI','RCOP')

SET @cSQL = ''

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR	
	SELECT
		C.ContentID,
		REPLACE(SUBSTRING(C.Link, PATINDEX('%ssiteid=%', C.Link), LEN(C.LINK)), 'ssiteid=', '') AS SSiteIDText
	FROM dbo.Content C WITH (NOLOCK)
	WHERE ISNULL(PATINDEX('%ssiteid=%', Link),0) > 0
		AND C.Type IN ('COI','COP','RCOI','RCOP')

OPEN oCursor
FETCH oCursor INTO @nContentID, @cSSiteIDText
WHILE @@fetch_status = 0
	BEGIN

	SET @cChar = LEFT(@cSSiteIDText, 1)
	SET @cSSiteID = ''

	WHILE ISNUMERIC(@cChar) = 1
		BEGIN
		
		SET @cSSiteID = @cSSiteID + @cChar
		SET @cSSiteIDText = RIGHT(@cSSiteIDText, LEN(@cSSiteIDText) - 1)
		SET @cChar = LEFT(@cSSiteIDText, 1)
	
		END
	--ENDWHILE

	IF ISNUMERIC(@cSSiteID) = 1
		SET @cSQL = @cSQL + 'UPDATE dbo.Content SET entityid = ' + @cSSiteID + ' WHERE contentid = ' + CAST(@nContentID AS varchar(10)) + ' '

	FETCH oCursor INTO @nContentID, @cSSiteIDText
	
	END		
--ENDWHILE

CLOSE oCursor
DEALLOCATE oCursor

IF LEN(RTRIM(@cSQL)) > 0
	EXEC (@cSQL)
GO
--End table: Content

--Begin table: CLTFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.CLTFile'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'CLTID', 0
EXEC Utility.SetDefault @cTableName, 'IMSAudienceID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CLTID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IMSAudienceID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'CLTFileID'
EXEC Utility.SetIndexClustered 'IX_CLTFile', @cTableName, 'cltid ASC'
GO
--End table: CLTFile

--Begin table: DefaultBinder
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.DefaultBinder'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.defaultbinder
		(
		defaultbinderid int IDENTITY(1,1) NOT NULL,
		binderid int NOT NULL,
		userid int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'BinderID', 0
EXEC Utility.SetDefault @cTableName, 'UserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BinderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'DefaultBinderID'
EXEC Utility.SetIndexClustered 'IX_DefaultBinder', @cTableName, 'binderid ASC,userid ASC'

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name='BinderTitle')
	ALTER TABLE dbo.DefaultBinder DROP COLUMN BinderTitle
GO
--End table: DefaultBinder

--Begin table: DropDown
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.DropDown'

EXEC Utility.DropConstraintsAndIndexes @cTableName

IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'IsActive')
	ALTER TABLE dbo.dropdown DROP COLUMN isactive

EXEC Utility.AddColumn @cTableName, 'email', 'varchar(max)'
EXEC Utility.AddColumn @cTableName, 'emailnotification', 'varchar(360)'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
EXEC Utility.SetIndexClustered 'IX_DropDown', @cTableName, 'category ASC,tierid ASC,displayorder ASC,displaytext ASC'
GO

ALTER TABLE dbo.DropDown ALTER COLUMN email varchar(max)
GO

UPDATE dbo.DropDown 
SET email = email
GO
--End table: DropDown

--Begin table: EntityMembers
DECLARE @cSQL varchar(max)
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.EntityMembers'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1
	
	CREATE TABLE dbo.entitymembers
		(
		entitymemberid int IDENTITY(1,1) NOT NULL,
		entityid int NOT NULL,
		entitytypecode varchar(50) NOT NULL,
		memberid int NOT NULL,
		membertypecode varchar(50) NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EntityID', 0
EXEC Utility.SetDefault @cTableName, 'MemberID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'MemberID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'EntityMemberID'
EXEC Utility.SetIndexClustered 'IX_EntityMembers', @cTableName, 'entityid ASC,entitytypecode ASC,memberid ASC,membertypecode ASC'

IF @nMigrateData = 1
	BEGIN
	
	IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.AARMembers') AND O.type in (N'U')) 
		BEGIN

		IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) JOIN sys.objects O WITH (NOLOCK) ON O.object_id = C.object_id AND C.Name = 'DisplayOrder' AND O.Name = 'AARMembers' AND O.Type = 'U')
			SET @cSQL = 'INSERT INTO dbo.EntityMembers (EntityID,EntityTypeCode,MemberID,MemberTypeCode) SELECT DisplayOrder AS EntityID,''AAR'' AS EntityTypeCode,UserID AS MemberID,''USER'' AS MemberTypeCode FROM dbo.AARMembers A WITH (NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM dbo.EntityMembers EM WITH (NOLOCK) WHERE EM.EntityID = A.DisplayOrder AND EM.EntityTypeCode = ''AAR'' AND EM.MemberID = A.UserID) ORDER BY DisplayOrder, UserID'
		ELSE IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) JOIN sys.objects O WITH (NOLOCK) ON O.object_id = C.object_id AND C.Name = 'AARID' AND O.Name = 'AARMembers' AND O.Type = 'U')
			SET @cSQL = 'INSERT INTO dbo.EntityMembers (EntityID,EntityTypeCode,MemberID,MemberTypeCode) SELECT AARID AS EntityID,''AAR'' AS EntityTypeCode,UserID AS MemberID,''USER'' AS MemberTypeCode FROM dbo.AARMembers A WITH (NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM dbo.EntityMembers EM WITH (NOLOCK) WHERE EM.EntityID = A.AARID AND EM.EntityTypeCode = ''AAR'' AND EM.MemberID = A.UserID) ORDER BY AARID, UserID'
	--ENDIF

		EXEC (@cSQL)

		END
	--ENDIF
	
	INSERT INTO dbo.EntityMembers
		(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
	SELECT
		DisplayOrder AS EntityID,
		R.Type AS EntityTypeCode,
		UserID AS MemberID,
		'USER' AS MemberTypeCode
	FROM dbo.Ref R WITH (NOLOCK)
	WHERE R.Type IN ('BINDER','COI')
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityID = R.DisplayOrder
				AND EM.EntityTypeCode = R.Type
				AND EM.MemberID = R.UserID
			)
	ORDER BY DisplayOrder, UserID
	
	INSERT INTO dbo.EntityMembers
		(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
	SELECT
		DisplayOrder AS EntityID,
		'COI' AS EntityTypeCode,
		UserID AS MemberID,
		'USER' AS MemberTypeCode
	FROM dbo.Ref R WITH (NOLOCK)
	WHERE R.Type = 'COP'
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityID = R.DisplayOrder
				AND EM.EntityTypeCode = 'COI'
				AND EM.MemberID = R.UserID
			)
	ORDER BY DisplayOrder, UserID

	INSERT INTO dbo.EntityMembers
		(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
	SELECT
		DisplayOrder AS EntityID,
		'DYNAMICFORM' AS EntityTypeCode,
		UserID AS MemberID,
		'USER' AS MemberTypeCode
	FROM dbo.Ref R WITH (NOLOCK)
	WHERE R.Type = 'dynamic_form'
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityID = R.DisplayOrder
				AND EM.EntityTypeCode = 'DYNAMICFORM'
				AND EM.MemberID = R.UserID
			)
	ORDER BY DisplayOrder, UserID

	INSERT INTO dbo.EntityMembers
		(EntityID,EntityTypeCode,MemberID,MemberTypeCode)
	SELECT
		DisplayOrder AS EntityID,
		'SODAR' AS EntityTypeCode,
		UserID AS MemberID,
		'USER' AS MemberTypeCode
	FROM dbo.Ref R WITH (NOLOCK)
	WHERE R.Type = 'SODARS'
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.EntityMembers EM WITH (NOLOCK)
			WHERE EM.EntityID = R.DisplayOrder
				AND EM.EntityTypeCode = 'SODAR'
				AND EM.MemberID = R.UserID
			)
	ORDER BY DisplayOrder, UserID

	END
--ENDIF
GO
--End table: EntityMembers

--Begin table: ERMA
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.ERMA'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1

	CREATE TABLE dbo.erma
		(
		ermaid int IDENTITY(1,1) NOT NULL,
		erma varchar(50),
		description varchar(max)
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.ERMA ALTER COLUMN description varchar(max)

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ErmaID'
EXEC Utility.SetIndexClustered 'IX_ERMA', @cTableName, 'erma ASC,ermaid ASC'

IF @nMigrateData = 1
	EXEC Utility.CloneData @cTableName, 'ARMY'
	
UPDATE dbo.ERMA SET Description = Description
GO
--End table: ERMA

--Begin table: EventLog
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.EventLog'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'itemid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'GoldMineID', 0
EXEC Utility.SetDefault @cTableName, 'ItemID', 0
EXEC Utility.SetDefault @cTableName, 'LogDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'TierID', 0
EXEC Utility.SetDefault @cTableName, 'UserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'GoldMineID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ItemID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LogDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UserID', 'int'

ALTER TABLE dbo.EventLog ALTER COLUMN Descrip varchar(300)
ALTER TABLE dbo.EventLog ALTER COLUMN Type varchar(50)

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'
GO
--End table: EventLog

--Begin table: LessonRollupEmail
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LessonRollupEmail'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.lessonrollupemail
		(
		lessonrollupemailid int IDENTITY(1,1) NOT NULL,
		email varchar(360) NOT NULL,
		date datetime NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'LessonRollupEmailID', 'int IDENTITY(1,1) NOT NULL'

EXEC Utility.SetColumnNotNull @cTableName, 'Date', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'Email', 'varchar(360)'

ALTER TABLE dbo.LessonRollupEmail ALTER COLUMN email varchar(360)

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LessonRollupEmailID'
EXEC Utility.SetIndexClustered 'IX_LessonRollupEmail', @cTableName, 'date ASC,email ASC,lessonrollupemailid ASC'
GO
--End table: LessonRollupEmail

--Begin table: LessonTab
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LessonTab'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LessonTab
		(
		LessonTabID int IDENTITY(1,1) NOT NULL,
		LessonTab varchar(150),
		LessonTabCode varchar(50),
		DisplayOrder int,
		TierID int,
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LessonTabID'
EXEC Utility.SetIndexClustered 'IX_LessonTab', @cTableName, 'TierID ASC,DisplayOrder ASC,LessonTab ASC,LessonTabID ASC'
GO
DECLARE @cLessonTab varchar(150)
DECLARE @cLessonTabCode varchar(50)

SET @cLessonTab = 'Header'
SET @cLessonTabCode = @cLessonTab
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Observation'
SET @cLessonTabCode = @cLessonTab
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Discussion'
SET @cLessonTabCode = @cLessonTab
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Recommendation'
SET @cLessonTabCode = @cLessonTab
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Implications'
SET @cLessonTabCode = 'Implication'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Comments'
SET @cLessonTabCode = 'Comment'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Event Description'
SET @cLessonTabCode = 'Description'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Member Perspectives'
SET @cLessonTabCode = 'Perspective'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Tags'
SET @cLessonTabCode = 'Tag'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Files'
SET @cLessonTabCode = 'File'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Posted By'
SET @cLessonTabCode = 'Originator'
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)

SET @cLessonTab = 'Shared'
SET @cLessonTabCode = @cLessonTab
INSERT INTO dbo.LessonTab (LessonTab,LessonTabCode,TierID) SELECT @cLessonTab,@cLessonTabCode,IDT.TierID FROM #oInstanceDataTable IDT WHERE NOT EXISTS (SELECT 1 FROM dbo.LessonTab LT WITH (NOLOCK) WHERE LT.LessonTabCode = @cLessonTabCode AND LT.TierID = IDT.TierID)
GO

UPDATE dbo.LessonTab
SET DisplayOrder = LessonTabID
WHERE DisplayOrder = 0
GO
--End table: LessonTab

--Begin table: LMS
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMS'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'activatedate', 'datetime	NULL'
EXEC Utility.AddColumn @cTableName, 'backgroundClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'backgroundrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'classifiedby', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'commentcaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'dateofsource', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'declassifiedon', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'derivedfrom', 'varchar(255) NULL'	
EXEC Utility.AddColumn @cTableName, 'discussioncaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'documentDate', 'datetime	NULL'
EXEC Utility.AddColumn @cTableName, 'eventcaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'eventdescriptionClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'eventdescriptionrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'exemptedsource', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'fixedWingDivision', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'forces', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'implicationcaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'implicationsClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'implicationsrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'intelInfoSystems', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'interviewsource', 'varchar(255) NULL'	
EXEC Utility.AddColumn @cTableName, 'iscandc', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'isdraft', 'varchar(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'isfires', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'isforceprotection', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'isintelligence', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'islogistics', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'ismaneuver', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'ismedical', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'issafety', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'legacyid', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'lmsunit', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'location', 'varchar(250) NULL'	
EXEC Utility.AddColumn @cTableName, 'maritimeRotaryDivision', 'char(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'mobileid', 'varchar(32) NULL'	
EXEC Utility.AddColumn @cTableName, 'observationcaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'observationsClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'observationsrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'operationtype', 'varchar(300) NULL'	
EXEC Utility.AddColumn @cTableName, 'originatingtierid', 'int NULL'	
EXEC Utility.AddColumn @cTableName, 'overallcaveat', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'recommendationcaveat', 'varchar(50) NULL'	
EXEC Utility.AddColumn @cTableName, 'recommendationsClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'recommendationsrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'releasableto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'specialprograms', 'varchar(1) NULL'	
EXEC Utility.AddColumn @cTableName, 'summaryClass', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'summaryrelto', 'varchar(100) NULL'	
EXEC Utility.AddColumn @cTableName, 'usereventname', 'varchar(100) NULL'	

ALTER TABLE dbo.LMS ALTER COLUMN fixedWingDivision varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN forces varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN intelInfoSystems varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN iscandc varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN isdraft varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN isfires varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN isforceprotection varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN isintelligence varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN islogistics varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN ismaneuver varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN ismedical varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN issafety varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN maritimeRotaryDivision varchar(1)
ALTER TABLE dbo.LMS ALTER COLUMN specialprograms varchar(1)

EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LMSID'
EXEC Utility.SetIndexNonClustered 'IX_LMSStatus', @cTableName, 'status ASC'

IF EXISTS (SELECT 1 FROM dbo.syscolumns C WITH (NOLOCK) WHERE C.id = OBJECT_ID(@cTableName) AND name='caveat') AND NOT EXISTS (SELECT 1 FROM dbo.syscolumns C WITH (NOLOCK) WHERE id = OBJECT_ID(@cTableName) AND name='overallcaveat')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	
	SET @cSQL = @cTableName + '.caveat'

	EXEC sp_rename @cSQL, 'overallcaveat', 'COLUMN'
	
	END
--ENDIF
GO

EXEC Utility.AddColumn 'dbo.LMS', 'countryid', 'int NULL'
EXEC Utility.SetDefault 'dbo.LMS', 'CountryID', 0
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'CountryID', 'int'
GO

IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name IN ('country','country_old'))
	EXEC Utility.AddColumn 'dbo.LMS', 'country_Old', 'varchar(50) NULL'
GO
	
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name='country_Old')
	EXEC sp_rename 'dbo.LMS.country_Old', 'country', 'COLUMN'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.LMS') AND SC.name='country')
	BEGIN

	UPDATE L
	SET L.CountryID = C.CountryID
	FROM dbo.LMS L
		JOIN JLLIS.dbo.Country C ON C.CountryName = L.Country
			AND L.Country IS NOT NULL
			AND LEN(RTRIM(L.Country)) > 0
			AND L.CountryID = 0
			
	UPDATE L
	SET L.CountryID = C.CountryID
	FROM dbo.LMS L
		JOIN JLLIS.dbo.Country C ON C.ISOCode3 = L.Country
			AND L.Country = 'USA'
			AND L.CountryID = 0

	EXEC sp_rename 'dbo.LMS.country', 'country_Old', 'COLUMN'

	END
--ENDIF
GO
--End table: LMS

--Begin table: LMSAFDL
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSAFDL'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.LMSAFDL
		(
		LMSAFDLID int IDENTITY(1,1) NOT NULL,
		LMSID int NOT NULL,
		AFDLID int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'AFDLID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AFDLID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSAFDLID'
EXEC Utility.SetIndexClustered 'IX_LMSAFDL', @cTableName, 'LMSID ASC,AFDLID ASC'
GO

EXEC sp_rename 'dbo.lmsafdl', 'LMSAFDL'
EXEC sp_rename 'dbo.LMSAFDL.lmsafdlid', 'LMSAFDLID', 'COLUMN'
EXEC sp_rename 'dbo.LMSAFDL.lmsid', 'LMSID', 'COLUMN'
EXEC sp_rename 'dbo.LMSAFDL.afdlid', 'AFDLID', 'COLUMN'
GO
--End table: LMSAFDL

--Begin table: LMSAnalysisCodes
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSAnalysisCodes'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.lmsanalysiscodes
		(
		lmsanalysiscodeid int IDENTITY(1,1) NOT NULL,
		lmsid int NOT NULL,
		analysiscodeid int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'LMSAnalysisCodeID', 'int IDENTITY(1,1) NOT NULL'

EXEC Utility.SetDefault @cTableName, 'AnalysisCodeID', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AnalysisCodeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSAnalysisCodeID'
EXEC Utility.SetIndexClustered 'IX_LMSAnalysisCodes', @cTableName, 'lmsid ASC,analysiscodeid ASC'
GO
--End table: LMSAnalysisCodes

--Begin table: LMSDiscuss
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSDiscuss'

IF (SELECT OBJECTPROPERTY(OBJECT_ID(@cTableName), 'TableHasIdentity')) = 0 AND EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'LMSDiscussID')
	BEGIN
	
	ALTER TABLE dbo.LMSDiscuss DROP COLUMN LMSDiscussID
	ALTER TABLE dbo.LMSDiscuss ADD LMSDiscussID int IDENTITY(1,1) NOT NULL
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'classification', 'varchar(250) NULL'
EXEC Utility.AddColumn @cTableName, 'releasableto', 'varchar(250) NULL'
EXEC Utility.AddColumn @cTableName, 'overallcaveat', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'CreatedBy', 0
EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CreatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSDiscussID'
EXEC Utility.SetIndexClustered 'IX_LMSDiscuss', @cTableName, 'LMSID ASC,LMSDiscussID ASC'
GO
--End table: LMSDiscuss

--Begin table: LMSDiscussFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSDiscussFile'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.lmsdiscussfile
		(
		lmsdiscussfileid int IDENTITY(1,1) NOT NULL,
		lmsdiscussid int NOT NULL,
		condition varchar(20) NULL,
		status varchar(20) NULL,
		filename varchar(250) NULL,
		description varchar(max) NULL,
		class varchar(50) NULL,
		relto varchar(50) NULL,
		caveat varchar(50) NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'LMSDiscussID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LMSDiscussID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSDiscussFileID'
EXEC Utility.SetIndexClustered 'IX_LMSDiscussFile', @cTableName, 'lmsdiscussid ASC,lmsdiscussfileid ASC'
GO
--End table: LMSDiscussFile

--Begin table: LMSFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSFile'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'LMSID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LMSFileID'
GO
--End table: LMSFile

--Begin table: LMSScrubAnalysis
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSScrubAnalysis'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.lmsscrubanalysis
		(
		lmsid int NOT NULL
		) ON [PRIMARY]
	
	END
--ENDIF
GO
--End table: LMSScrubAnalysis

--Begin table: LMSTask
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LMSTask'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.lmstask
		(
		lmstaskid int IDENTITY(1,1) NOT NULL,
		lmsid int,
		taskid int
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'LMSID', 0
EXEC Utility.SetDefault @cTableName, 'TaskID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'LMSID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TaskID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LMSTaskID'
EXEC Utility.SetIndexClustered 'IX_LMSTask', @cTableName, 'LMSID ASC,TaskID ASC'
GO

EXEC sp_rename 'dbo.lmstask', 'LMSTask'
EXEC sp_rename 'dbo.LMSTASK.lmstaskid', 'LMSTaskID', 'COLUMN'
EXEC sp_rename 'dbo.LMSTASK.lmsid', 'LMSID', 'COLUMN'
EXEC sp_rename 'dbo.LMSTASK.taskid', 'TaskID', 'COLUMN'
GO
--End table: LMSTask

--Begin table: LoginUserSetting
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.LoginUserSetting'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.loginusersetting
		(
		loginusersettingid int IDENTITY(1,1) NOT NULL,
		loginuserid int NOT NULL,
		subscribetoweekly varchar(1) NOT NULL,
		liteDefault varchar(1) NULL,
		satphone varchar(25) NULL,
		unitphone varchar(25) NULL,
		jwicsemail varchar(250) NULL,
		nationality varchar(35) NULL,
		surveyKey varchar(35) NULL,
		surveyAdmin bit NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'jwicsemail', 'varchar(250) NULL'
EXEC Utility.AddColumn @cTableName, 'liteDefault', 'varchar(1) NULL'
EXEC Utility.AddColumn @cTableName, 'nationality', 'varchar(35) NULL'
EXEC Utility.AddColumn @cTableName, 'satphone', 'varchar(25) NULL'
EXEC Utility.AddColumn @cTableName, 'surveyAdmin', 'bit NULL'
EXEC Utility.AddColumn @cTableName, 'surveyKey', 'varchar(35) NULL'
EXEC Utility.AddColumn @cTableName, 'unitphone', 'varchar(25) NULL'

ALTER TABLE dbo.LoginUserSetting ALTER COLUMN SubscribeToWeekly varchar(1)

IF EXISTS (SELECT 1 FROM dbo.syscolumns C WITH (NOLOCK) WHERE C.id = OBJECT_ID('dbo.LoginUserSetting') AND C.name='tierid')
	ALTER TABLE dbo.LoginUserSetting DROP COLUMN TierID	

EXEC Utility.SetDefault @cTableName, 'LoginUserID', 0
EXEC Utility.SetDefault @cTableName, 'SubscribeToWeekly', 'Y'

EXEC Utility.SetColumnNotNull @cTableName, 'LoginUserID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'SubscribeToWeekly', 'varchar(1)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'LoginUserSettingID'
EXEC Utility.SetIndexClustered 'IX_LoginUserSetting', @cTableName, 'loginuserid ASC,loginusersettingid ASC'
GO
--End table: LoginUserSetting

--Begin table: Menu
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Menu'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.menu
		(
		menuid int IDENTITY(1,1) NOT NULL,
		title varchar(50) NOT NULL,
		active bit NOT NULL CONSTRAINT DF_Menu_Active DEFAULT 0,
		type varchar(50) NOT NULL
		) ON [PRIMARY]

	INSERT INTO dbo.dropdown (displaytext,category,description,displayorder) VALUES ('Logged In','Menus','',1);
	INSERT INTO dbo.dropdown (displaytext,category,description,displayorder) VALUES ('Logged Out','Menus','',2);
	INSERT INTO dbo.dropdown (displaytext,category,description,displayorder) VALUES ('Menus','DropDownCategory','Menus available on site',1);

	INSERT dbo.menu (title,active,type) VALUES ('Default',1,'Logged In')
	INSERT dbo.menu (title,active,type) VALUES ('Default',1,'Logged Out')

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Active', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MenuID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'MenuID'
GO
--End table: Menu

--Begin table: MenuItem
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.MenuItem'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	EXEC Utility.AddColumn @cTableName, 'Type', 'varchar(50)'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.MenuItem'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.menuitem
		(
		menuitemid int IDENTITY(1,1) NOT NULL,
		menuid int,
		parentid int,
		level int,
		title varchar(100),
		seclev int,
		tooltip varchar(150),
		link varchar(250),
		html varchar(100),
		type varchar(50)
		) ON [PRIMARY]
	
	DECLARE @nMenuID int
	DECLARE @nParentMenuItemID int

	SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Title = 'Default' AND M.Type = 'Logged In')
	SET @nParentMenuItemID = 0
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link) VALUES (@nMenuID,@nParentMenuItemID,1,'Default',NULL,NULL,NULL)

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'Default' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'HOME',NULL,'Return to the Home Page','index.cfm?menudisp=menu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,10,'REPORT GENERATOR',2000,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,11,'TEAM TOOLS',3000,NULL,'index.cfm?menudisp=teammenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,12,'LINKS',2000,NULL,'index.cfm?disp=links.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,13,'ADDITIONAL PRODUCTS',2000,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,14,'PUBLICATIONS',2000,NULL,'index.cfm?disp=publicationTabs.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,15,'MY STUFF',2000,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,17,'CONTACT US',NULL,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,18,'ADMIN',6000,NULL,'index.cfm?menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,8,'SEARCH',2000,'Research information across DoD and other Federal Agencies with the Inter-Agency search engine.',NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,9,'TOOLS',2000,NULL,NULL,NULL)

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SEARCH' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'INTERAGENCY SEARCH',NULL,NULL,'/JKMS/main.jsp?DefaultBinder=1','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'SEARCH OBSERVATIONS',NULL,NULL,'index.cfm?disp=lms.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'TOOLS' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'BINDERS',NULL,NULL,'index.cfm?disp=binder.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'REPORT GENERATOR' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'AAR',NULL,NULL,'index.cfm?disp=aar.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'TEAM TOOLS' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'BATTLE BOARD',NULL,NULL,'index.cfm?disp=bb.cfm&doit=display&menudisp=teammenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'CDR',NULL,NULL,'index.cfm?disp=cdr.cfm&doit=display&menudisp=teammenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'COP SITES',NULL,NULL,'index.cfm?disp=ssite.cfm&doit=display&menudisp=teammenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'DISCUSSION GROUPS',NULL,NULL,'cfbb/index.cfm?page=home&menudisp=teammenu.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'MY STUFF' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'MY DAILY DIGESTS',NULL,NULL,'index.cfm?disp=profile.cfm#dd','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'MY LESSONS',NULL,NULL,'index.cfm?disp=lms.cfm&mylessons=yes','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'MY BINDERS',NULL,NULL,'index.cfm?disp=binder.cfm&myBinders=yes','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'MY REPORTS',NULL,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,5,'MY FILES',2100,NULL,'index.cfm?disp=files.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'CONTACT US' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'FEEDBACK',NULL,NULL,'index.cfm?disp=feedback.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'ADMIN' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'MEMBERS',NULL,NULL,'index.cfm?disp=prospectors.cfm&doit=display&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'SITE MANAGEMENT',NULL,NULL,NULL,NULL)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'CONTENT',NULL,NULL,'index.cfm?disp=content.cfm&doit=display&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'LESSON VIEWS REPORT',NULL,NULL,'index.cfm?disp=rpt-lmsviews.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,5,'LESSON ROLLUP',NULL,NULL,'index.cfm?disp=lessonrollup.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,6,'EVENT LOG',NULL,NULL,'index.cfm?disp=rep-eventlog.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,7,'TIER METRIC REPORT',NULL,NULL,'index.cfm?disp=adminmetrics.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,8,'SEND EMAIL',NULL,NULL,'index.cfm?disp=../admin/sendTier2Email.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,9,'SWITCH ROLE',NULL,NULL,'index.cfm?disp=changeRole.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,10,'MANAGE LOOK/FEEL',NULL,NULL,'index.cfm?disp=lookFeel.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,11,'QUICK ADD REPORT',NULL,NULL,'index.cfm?disp=quickAddReport.cfm','yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'SITE MANAGEMENT' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'MENU MAINTENANCE',NULL,NULL,'index.cfm?disp=menu_admin.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'TIER MAINTENANCE',NULL,NULL,'index.cfm?disp=../admin/tiers/tierList.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'TAB MAINTENANCE',6000,NULL,'index.cfm?disp=tabMaintenance.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,4,'SETUP',NULL,NULL,'index.cfm?disp=../admin/setup.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,5,'DROPDOWNS',NULL,NULL,'index.cfm?disp=dropdown.cfm&doit=display&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,6,'EVENT MAINTENANCE',NULL,NULL,'index.cfm?disp=../admin/opexmaint.cfm&doit=display&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,7,'UNIT MAINTENANCE',NULL,NULL,'index.cfm?disp=../admin/unitmaint.cfm&doit=display&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,8,'METADATA MAINTENANCE',NULL,NULL,'index.cfm?disp=../admin/admin_metadata.cfm&doit=display','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,9,'ANALYSIS MAINTENANCE',NULL,NULL,'index.cfm?disp=analysis_codes.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,10,'UJTL MAINTENANCE',NULL,NULL,'index.cfm?disp=ujtlmaint.cfm&doit=display','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,11,'UPLOAD IMAGES',NULL,NULL,'index.cfm?disp=../admin/uploadimages.cfm&menudisp=adminmenu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,12,'IMAGE LIBRARY',6000,NULL,'index.cfm?disp=../admin/dspFileManager.cfm','yes')

	SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Type = 'Logged Out')
	SET @nParentMenuItemID = 0
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'Default',NULL,NULL,NULL,NULL)

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'Default' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,'REGISTER',NULL,NULL,'index.cfm?disp=register.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,'LOGIN',NULL,NULL,'index.cfm?disp=login.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'HOME',NULL,NULL,'index.cfm?menudisp=menu.cfm','yes')
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,5,'CONTACT US',NULL,NULL,NULL,NULL)

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'CONTACT US' AND MI.MenuID = @nMenuID)
	INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,3,'EMAIL US',NULL,NULL,'mailto:dan.parker@nsite.info,Christopher.Andrews@socom.mil','yes')

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'Type', 'varchar(50)'

ALTER TABLE dbo.menuitem ALTER COLUMN Type varchar(50)

EXEC Utility.SetDefault @cTableName, 'Level', 1
EXEC Utility.SetDefault @cTableName, 'MenuID', 0
EXEC Utility.SetDefault @cTableName, 'ParentID', 0
EXEC Utility.SetDefault @cTableName, 'Type', 'yes'

EXEC Utility.SetColumnNotNull @cTableName, 'Level', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'MenuID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'ParentID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Type', 'varchar(50)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'MenuItemID'
EXEC Utility.SetIndexClustered 'IX_MenuItem', @cTableName, 'menuid ASC,parentid ASC,level ASC,title ASC'
GO

DECLARE @cTitle varchar(250)
DECLARE @nMenuID int
DECLARE @nParentMenuItemID int

DECLARE @cInstanceName varchar(50) 
SET @cInstanceName = (SELECT IDT.TierName FROM #oInstanceDataTable IDT)

IF @cInstanceName = 'NAVY'
	BEGIN
	
	SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Title = 'Default' AND M.Type = 'Logged In')
	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'Default' AND MI.MenuID = @nMenuID)

	SET @cTitle = 'PORT VISIT REPORTS'
	IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = @cTitle AND MI.MenuID = @nMenuID)
		INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,@cTitle,2000,NULL,NULL,'yes')

	SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = @cTitle AND MI.MenuID = @nMenuID)

	SET @cTitle = 'SEARCH PVRs'
	IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = @cTitle AND MI.MenuID = @nMenuID)
		INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,1,@cTitle,2000,NULL,'portvisits/','yes')

	SET @cTitle = 'SUBMIT PVR'
	IF NOT EXISTS (SELECT 1 FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = @cTitle AND MI.MenuID = @nMenuID)
		INSERT INTO dbo.MenuItem (menuid,parentid,level,title,seclev,tooltip,link,type) VALUES (@nMenuID,@nParentMenuItemID,2,@cTitle,2000,NULL,'portvisits/?doit=add','yes')

	END
--ENDIF
GO

DECLARE @nMenuID int
DECLARE @nParentMenuItemID int

DECLARE @oTable table
	(
	Level int NOT NULL identity(1,1) primary key,
	MenuItemID int
	)

SET @nMenuID = (SELECT M.MenuID FROM dbo.Menu M WITH (NOLOCK) WHERE M.Title = 'Default' AND M.Type = 'Logged In')
SET @nParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MI.Title = 'Default' AND MI.MenuID = @nMenuID)

DELETE FROM dbo.MenuItem WHERE MenuID = @nMenuID AND Title IN ('ADDITIONAL PRODUCTS','ANALYSIS MAINTENANCE','EVENT MAINTENANCE','METADATA MAINTENANCE','PORT VISITS','SWITCH ROLE')

UPDATE dbo.MenuItem SET Title = 'EDIT MY PROFILE' WHERE Link = 'index.cfm?disp=profile.cfm#dd'
UPDATE dbo.MenuItem SET ParentID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WITH (NOLOCK) WHERE MenuID = @nMenuID AND MI.Title = 'Tools') WHERE Link = 'index.cfm?disp=cdr.cfm&doit=display&menudisp=teammenu.cfm'
GO
--End table: MenuItem

--Begin table: MetaData
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.MetaData'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.metadata
		(
		mdid int IDENTITY(1,1) NOT NULL,
		type varchar(30),
		name varchar(125),
		abbrev varchar(10),
		description text,
		sequence int,
		tier varchar(20),
		isactive bit,
		modified datetime,
		aar int,
		tierid int
		) ON [PRIMARY]

	END
--ENDIF

UPDATE dbo.metadata
SET IsActive = 0
WHERE IsActive <> 1

UPDATE dbo.metadata
SET IsActive = 1
WHERE IsActive <> 0

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'aar', 'int'
EXEC Utility.AddColumn @cTableName, 'tier', 'varchar(20)'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int'

ALTER TABLE dbo.MetaData ALTER COLUMN IsActive bit

EXEC Utility.SetDefault @cTableName, 'AAR', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 0
EXEC Utility.SetDefault @cTableName, 'Sequence', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'AAR', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'Sequence', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

ALTER TABLE dbo.metadata ALTER COLUMN Name varchar(125) NULL

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'MDID'
EXEC Utility.SetIndexClustered 'IX_MetaData', @cTableName, 'tierid ASC,type ASC,name ASC'
GO
--End table: MetaData

--Begin table: MetaDataUser
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.MetaDataUser'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.MetaDataUser
		(
		mduid int IDENTITY(1,1) NOT NULL,
		mdid int,
		userid int,
		email varchar(320)
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.MetaDataUser ALTER COLUMN EMail varchar(320)
ALTER TABLE dbo.MetaDataUser ALTER COLUMN MDID int

EXEC Utility.SetDefault @cTableName, 'MDID', 0
EXEC Utility.SetDefault @cTableName, 'UserID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'MDID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UserID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'MDUID'
EXEC Utility.SetIndexClustered 'IX_MetaDataUser', @cTableName, 'MDID ASC,UserID ASC'
GO
--End table: MetaDataUser

--Begin table: MobileJLLISMetaData
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.MobileJLLISMetaData'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.mobilejllismetadata
		(
		mobilejllismetadataid int IDENTITY(1,1) NOT NULL,
		referenceid int NOT NULL,
		guid varchar(36) NOT NULL,
		type int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.MobileJLLISMetaData ALTER COLUMN GUID varchar(36)

EXEC Utility.SetDefault @cTableName, 'ReferenceID', 0
EXEC Utility.SetDefault @cTableName, 'Type', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ReferenceID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Type', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'MobileJLLISMetaDataID'
GO
--End table: MobileJLLISMetaData

--Begin table: OpEx
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.OpEx'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.opex
		(
		opexid int IDENTITY(1,1) NOT NULL,
		opexname varchar(300) NULL,
		location varchar(300) NULL,
		opextype varchar(300) NULL,
		sponsor varchar(300) NULL,
		startdate datetime NULL,
		enddate datetime NULL,
		tierid int NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'tierid', 'int null'

EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'OpExID'
GO
--End table: OpEx

--Begin table: PortVisit
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	EXEC Utility.AddColumn 'dbo.PortVisit', 'OriginatingTierID', 'int	NULL'
	EXEC Utility.SetDefault 'dbo.PortVisit', 'OriginatingTierID', 0
	EXEC Utility.SetColumnNotNull 'dbo.PortVisit', 'OriginatingTierID', 'int'
	
	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.OriginatingTierID = T.TierID
	FROM dbo.PortVisit PV
		JOIN JLLIS.dbo.Tier T ON T.TierName = PV.St1
			AND PV.OriginatingTierID = 0

	END
--ENDIF
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.PortVisit') AND O.type in (N'U')) 
	BEGIN

	UPDATE PV
	SET PV.OriginatingTierID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)
	FROM dbo.PortVisit PV
	WHERE PV.OriginatingTierID = 0
	
	END
--ENDIF
GO
--End table: PortVisit

--Begin table: ReleasableTo
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ReleasableTo'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.releasableto
		(
		releasabletoid int IDENTITY(1,1) NOT NULL,
		releasableto_weight int NOT NULL,
		releasableto_label varchar(50) NOT NULL,
		isactive bit NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.ReleasableTo ALTER COLUMN ReleasableTo_Label varchar(50)

EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'ReleasableTo_Weight', 0

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'ReleasableTo_Weight', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ReleasableToID'
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.ReleaseableTo') AND O.type in (N'U')) 
	BEGIN
	
	TRUNCATE TABLE dbo.releasableto
	
	INSERT INTO dbo.releasableto
		(releasableto_weight, releasableto_label, isactive)
	SELECT 
		releaseableto_weight, 
		releaseableto_label, 
		isactive
	FROM dbo.ReleaseableTo
	
	EXEC Utility.DropConstraintsAndIndexes 'dbo.ReleaseableTo'
	DROP TABLE dbo.ReleaseableTo
	
	END
--ENDIF

GO
--End table: ReleasableTo

--Begin table: ReportToMetaData
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ReportToMetaData'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.reporttometadata
		(
		rtmdid int IDENTITY(1,1) NOT NULL,
		reporttype varchar(20),
		reportid int,
		mdid int,
		modified datetime
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'mdid', 0
EXEC Utility.SetDefault @cTableName, 'modified', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'reportid', 0

EXEC Utility.SetColumnNotNull @cTableName, 'mdid', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'modified', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'reportid', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'rtmdid'
EXEC Utility.SetIndexClustered 'IX_ReportToMetaData', @cTableName, 'reporttype ASC, reportid ASC, mdid ASC'
GO
--End table: ReportToMetaData

--Begin table: Setup
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Setup'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'dailyactivityreportto', 'varchar(360) NULL'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'
EXEC Utility.AddColumn @cTableName, 'weeklyrollup', 'varchar(360) NULL'

EXEC Utility.SetDefault @cTableName, 'D', 'Y'
EXEC Utility.SetDefault @cTableName, 'F', 'Y'
EXEC Utility.SetDefault @cTableName, 'L', 'Y'
EXEC Utility.SetDefault @cTableName, 'M', 'N'
EXEC Utility.SetDefault @cTableName, 'O', '4'
EXEC Utility.SetDefault @cTableName, 'P', 'Y'
EXEC Utility.SetDefault @cTableName, 'T', 'N'
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'D', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'F', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'L', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'M', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'O', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'P', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'T', 'varchar(360)'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SetupID'
GO
--End table: Setup

--Begin table: SSite
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.SSite'

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'isjoint', 'bit NULL'
EXEC Utility.AddColumn @cTableName, 'originatingtierid', 'int NULL'

ALTER TABLE dbo.SSite ALTER COLUMN Description varchar(max)
ALTER TABLE dbo.SSite ALTER COLUMN Discussion varchar(1)
ALTER TABLE dbo.SSite ALTER COLUMN IsJoint bit

EXEC Utility.SetDefault @cTableName, 'BinderID', 0
EXEC Utility.SetDefault @cTableName, 'Condition', 'Active'
EXEC Utility.SetDefault @cTableName, 'CreatedBy', 0
EXEC Utility.SetDefault @cTableName, 'CreationDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'Discussion', 'N'
EXEC Utility.SetDefault @cTableName, 'ForumCategoryID', 0
EXEC Utility.SetDefault @cTableName, 'IsJoint', 0
EXEC Utility.SetDefault @cTableName, 'OriginatingTierID', 0
EXEC Utility.SetDefault @cTableName, 'Status', 'Active'
EXEC Utility.SetDefault @cTableName, 'UpdatedBy', 0
EXEC Utility.SetDefault @cTableName, 'UpdateDate', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'Webmaster', 0

EXEC Utility.SetColumnNotNull @cTableName, 'BinderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Condition', 'varchar(20)'
EXEC Utility.SetColumnNotNull @cTableName, 'CreatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreationDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'Discussion', 'varchar(1)'
EXEC Utility.SetColumnNotNull @cTableName, 'ForumCategoryID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsJoint', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Status', 'varchar(20)'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdateDate', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'Webmaster', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SSiteID'
GO

UPDATE dbo.SSite
SET Description = Description
GO
--End table: SSite

--Begin table: SSiteFile
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.SSiteFile'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.ssitefile
		(
		ssitefileid int IDENTITY(1,1) NOT NULL,
		ssiteitemid int NOT NULL,
		condition varchar(20) NULL,
		status varchar(20) NULL,
		filename varchar(250) NULL,
		description text NULL,
		class varchar(50) NULL,
		relto varchar(50) NULL,
		caveat varchar(50) NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'caveat', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'class', 'varchar(50) NULL'
EXEC Utility.AddColumn @cTableName, 'relto', 'varchar(50) NULL'

EXEC Utility.SetDefault @cTableName, 'SSiteItemID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'SSiteItemID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'SSiteFileID'
EXEC Utility.SetIndexClustered 'IX_SSiteFile', @cTableName, 'SSiteItemID ASC'
GO
--End table: SSiteFile

--Begin table: SSiteItem
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.SSiteItem'

EXEC Utility.DropConstraintsAndIndexes @cTableName

ALTER TABLE dbo.SSiteItem ALTER COLUMN MainText varchar(max)
ALTER TABLE dbo.SSiteItem ALTER COLUMN Tease varchar(max)

EXEC Utility.SetDefault @cTableName, 'Condition', 'Active'
EXEC Utility.SetDefault @cTableName, 'ItemID', '0'
EXEC Utility.SetDefault @cTableName, 'SSiteID', 0
EXEC Utility.SetDefault @cTableName, 'Status', 'Active'

EXEC Utility.SetColumnNotNull @cTableName, 'Condition', 'varchar(20)'
EXEC Utility.SetColumnNotNull @cTableName, 'ItemID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'SSiteID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Status', 'varchar(20)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'SSiteItemID'
EXEC Utility.SetIndexClustered 'IX_SSiteItem', @cTableName, 'SSiteID ASC,ItemID ASC'
GO

UPDATE dbo.SSiteItem
SET 
	MainText = MainText,
	Tease = Tease
GO
--End table: SSiteItem

--Begin table: Static_Styles
DECLARE @cTableName varchar(250)
SET @cTableName = 'dbo.Static_Styles'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.static_styles
		(
		id int IDENTITY(1,1) NOT NULL,
		tier varchar(50) NULL,
		bodyBackgrdColor varchar(10) NULL,
		alink varchar(10) NULL,
		avisited varchar(10) NULL,
		aleftnavlink varchar(10) NULL,
		aleftnavvisited varchar(10) NULL,
		aleftnavhover varchar(10) NULL,
		aleftnavactive varchar(10) NULL,
		atopnavlink varchar(10) NULL,
		atopnavvisited varchar(10) NULL,
		atopnavhover varchar(10) NULL,
		atopnavactive varchar(10) NULL,
		afilelinklink varchar(10) NULL,
		afilelinkvisited varchar(10) NULL,
		afilelinkhover varchar(10) NULL,
		afilelinkactive varchar(10) NULL,
		background varchar(10) NULL,
		topprofile varchar(10) NULL,
		hrstyle varchar(10) NULL,
		globaltextcolor varchar(10) NULL,
		worldclockbg varchar(10) NULL,
		clockbg varchar(10) NULL,
		yuimenu varchar(10) NULL,
		yuimenubar varchar(10) NULL,
		clock varchar(10) NULL,
		topmenulinkbkgrd varchar(10) NULL,
		contentheader varchar(10) NULL,
		rowcolor1 varchar(10) NULL,
		rowcolor2 varchar(10) NULL,
		tablesorterhover varchar(10) NULL,
		tablesorterodd varchar(10) NULL,
		tablesorteroddhover varchar(10) NULL,
		even varchar(10) NULL,
		odd varchar(10) NULL,
		tablesortereven varchar(10) NULL,
		tablesorterevenhover varchar(10) NULL,
		tablesortertheadbg varchar(10) NULL,
		tablesorterheadborder varchar(10) NULL,
		tablesorterheadsortup varchar(10) NULL,
		hoverrowcolor varchar(10) NULL,
		menuheadercolor varchar(10) NULL,
		menuheaderbkgrdcolor varchar(10) NULL,
		menuheaderimg varchar(50) NULL,
		pageheader varchar(10) NULL,
		bordertable varchar(10) NULL,
		bottomtext varchar(10) NULL,
		bottomtexttd varchar(10) NULL,
		footerColorTable varchar(10) NULL,
		footercolortablebkgrd varchar(10) NULL,
		style3 varchar(10) NULL,
		style3color varchar(10) NULL,
		style3border varchar(10) NULL,
		style4 varchar(10) NULL,
		style5 varchar(10) NULL,
		wikitopic varchar(10) NULL,
		wikitopicbkgrd varchar(10) NULL,
		wikitext varchar(10) NULL,
		grayit varchar(10) NULL,
		cltmbg1 varchar(10) NULL,
		cltmbg2 varchar(10) NULL,
		cltmsectionbar varchar(10) NULL,
		menubarbg varchar(10) NULL,
		menubar varchar(10) NULL,
		menubarhover varchar(10) NULL,
		menubarvisited varchar(10) NULL,
		headmenu varchar(10) NULL,
		headmenubg varchar(10) NULL,
		submenu varchar(10) NULL,
		submenubg varchar(10) NULL,
		roundbox varchar(10) NULL,
		roundbocimg varchar(50) NULL,
		roundborder varchar(10) NULL,
		contactinfo varchar(10) NULL,
		tierid int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
EXEC Utility.SetIndexClustered 'IX_Static_Styles', @cTableName, 'tierid ASC'
GO
--End table: Static_Styles

--Begin table: Styles
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Styles'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.styles
		(
		id int IDENTITY(1,1) NOT NULL,
		tier varchar(50) NULL,
		cpcolor varchar(10) NULL,
		lpcolor varchar(10) NULL,
		pocboxcolor varchar(10) NULL,
		pocoutlinecolor varchar(10) NULL,
		cfcolor varchar(10) NULL,
		isactive bit NULL,
		roundbox varchar(10) NOT NULL,
		tierid int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'roundbox', 'varchar(10) NULL'
EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'CFColor', '#000000'
EXEC Utility.SetDefault @cTableName, 'CPColor', '#DDD4A8'
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'LPColor', '#95802B'
EXEC Utility.SetDefault @cTableName, 'POCBoxColor', '#786723'
EXEC Utility.SetDefault @cTableName, 'POCOutlineColor', '#BBA035'
EXEC Utility.SetDefault @cTableName, 'RoundBox', '#E5E5E5'
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CFColor', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'CPColor', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'LPColor', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'POCBoxColor', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'POCOutlineColor', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'RoundBox', 'varchar(10)'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
EXEC Utility.SetIndexClustered 'IX_Styles', @cTableName, 'tierid ASC'
GO
--End table: Styles

--Begin table: Surv_Answers
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Answers'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_answers
		(
		id varchar(35) NOT NULL,
		questionidfk varchar(35) NOT NULL,
		answer varchar(255) NOT NULL,
		rank int NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Rank', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Rank', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'
GO
--End table: Surv_Answers

--Begin table: Surv_Questions
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Questions'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_questions
		(
		id varchar(35) NOT NULL,
		surveyidfk varchar(35) NOT NULL,
		question varchar(255) NOT NULL,
		questiontypeidfk varchar(35) NOT NULL,
		rank int NOT NULL,
		required bit NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'Rank', 0
EXEC Utility.SetDefault @cTableName, 'Required', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Rank', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Required', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'
GO
--End table: Surv_Questions

--Begin table: Surv_QuestionTypes
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_QuestionTypes'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_questiontypes
		(
		id varchar(35) NOT NULL,
		name varchar(50) NOT NULL,
		handlerroot varchar(50) NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName
EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ID'
GO
--End table: Surv_QuestionTypes

--Begin table: Surv_Results
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Results'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_results
		(
		owneridfk varchar(320) NOT NULL,
		questionidfk varchar(35) NOT NULL,
		answeridfk varchar(35) NULL,
		truefalse bit NULL,
		textbox varchar(255) NULL,
		textboxmulti varchar(max) NULL,
		other varchar(255) NULL,
		itemidfk varchar(35) NULL
		) ON [PRIMARY]

	END
--ENDIF
GO
--End table: Surv_Results

--Begin table: Surv_Survey_EmailAddresses
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Survey_EmailAddresses'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_survey_emailaddresses
		(
		surveyidfk varchar(35) NOT NULL,
		emailaddress varchar(320) NOT NULL
		) ON [PRIMARY]

	END
--ENDIF
GO
--End table: Surv_Survey_EmailAddresses

--Begin table: Surv_Survey_Results
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Survey_Results'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_survey_results
		(
		surveyidfk varchar(35) NOT NULL,
		ownerid varchar(320) NOT NULL,
		completed datetime NOT NULL
		) ON [PRIMARY]

	END
--ENDIF
GO
--End table: Surv_Survey_Results

--Begin table: Surv_Surveys
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Surveys'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_surveys
		(
		id varchar(35) NOT NULL,
		name varchar(50) NOT NULL,
		description varchar(255) NOT NULL,
		active bit NOT NULL,
		datebegin datetime NULL,
		dateend datetime NULL,
		resultmailto varchar(320) NULL,
		surveypassword varchar(50) NULL,
		thankyoumsg varchar(max) NULL,
		useridfk varchar(35) NOT NULL,
		templateidfk varchar(35) NULL,
		allowembed bit NULL,
		showinpubliclist bit NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName
EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
GO
--End table: Surv_Surveys

--Begin table: Surv_Templates
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Templates'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_templates
		(
		id varchar(35) NOT NULL,
		header varchar(max) NOT NULL,
		footer varchar(max) NOT NULL,
		useridfk varchar(35) NOT NULL,
		name varchar(255) NOT NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName
EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ID'
GO
--End table: Surv_Templates

--Begin table: Surv_Users
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Surv_Users'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.surv_users
		(
		username varchar(255) NOT NULL,
		password varchar(255) NOT NULL,
		isadmin bit NULL,
		id varchar(35) NULL
		) ON [PRIMARY]

	END
--ENDIF
GO
--End table: Surv_Users

--Begin table: Tab
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Tab'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.tab
		(
		tabid int IDENTITY(1,1) NOT NULL,
		codename varchar(20) NULL,
		name varchar(100) NULL,
		description varchar(500) NULL,
		displayorder int NULL,
		status varchar(20) NULL,
		tierid int NULL
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.AddColumn @cTableName, 'tierid', 'int NULL'

EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TabID'
GO

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'Featured')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('Featured', 'Featured', 'These are HOT Observations, Insights, or Recommendations requiring widest and immediate dissemination', 3, 'Visible');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'About')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('About', 'About', 'About', 4, 'Hidden');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'MostViewed')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('MostViewed', 'Most Popular', 'Most Popular Observations', 5, 'Visible');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'Governance')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('Governance', 'Governance', 'Governance', 6, 'Hidden');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'Newsletter')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('Newsletter', 'Newsletter', 'Observation & Recommendations Newsletters', 7, 'Visible');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'ICOI')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('ICOI', 'Internal Communities of Practice', 'Internal Communities of Practice', 1, 'Visible');

IF NOT EXISTS (SELECT 1 FROM dbo.tab T WITH (NOLOCK) WHERE T.CodeName = 'JCOI')
	INSERT INTO dbo.tab(codename, name, description ,displayorder, status) VALUES('JCOI', 'Joint Communities of Practice', 'Joint Communities of Practice', 2, 'Visible');
GO

UPDATE dbo.tab
SET TierID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)
WHERE TierID = 0
GO
--End table: Tab

--Begin table: TierEntities
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.TierEntities'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.tierentities
		(
		tierentityid int IDENTITY(1,1) NOT NULL,
		tierid int NOT NULL,
		entityid int NOT NULL,
		entitytypecode varchar(50) NOT NULL
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'EntityID', 0
EXEC Utility.SetDefault @cTableName, 'TierID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'EntityID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TierID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'TierEntityID'
EXEC Utility.SetIndexClustered 'IX_TierEntities', @cTableName, 'tierid ASC,entityid ASC,entitytypecode ASC'

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name='IsOriginatingTier')
	ALTER TABLE dbo.TierEntities DROP COLUMN IsOriginatingTier
GO
--End table: TierEntities

--Begin table: Task
DECLARE @cTableName varchar(250)
DECLARE @nMigrateData bit

SET @cTableName = 'dbo.Task'
SET @nMigrateData = 0

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	SET @nMigrateData = 1

	CREATE TABLE dbo.task
		(
		taskid int IDENTITY(1,1) NOT NULL,
		tasktype varchar(255),
		taskcategory varchar(255),
		tasknumber varchar(255),
		tasktitle varchar(255),
		taskdefinition varchar(max),
		parenttasknumber varchar(255),
		status varchar(255)
		) ON [PRIMARY]
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TaskID'

IF @nMigrateData = 1
	EXEC Utility.CloneData @cTableName, 'ARMY'

ALTER TABLE dbo.Task ALTER COLUMN parenttasknumber varchar(255)
ALTER TABLE dbo.Task ALTER COLUMN status varchar(255)
ALTER TABLE dbo.Task ALTER COLUMN taskcategory varchar(255)
ALTER TABLE dbo.Task ALTER COLUMN taskdefinition varchar(max)
ALTER TABLE dbo.Task ALTER COLUMN tasknumber varchar(255)
ALTER TABLE dbo.Task ALTER COLUMN tasktitle varchar(255)
ALTER TABLE dbo.Task ALTER COLUMN tasktype varchar(255)

UPDATE dbo.Task
SET taskdefinition = taskdefinition
GO
--End table: Task

--Begin table: Unit
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Unit'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.unit
		(
		unitid int IDENTITY(1,1) NOT NULL,
		unitname varchar(250) NOT NULL,
		unitdescription varchar(250) NULL,
		msc varchar(250) NULL,
		status varchar(25) NULL,
		parentunitid int NOT NULL,
		pocuserid int NOT NULL,
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ParentUnitID', 0
EXEC Utility.SetDefault @cTableName, 'POCUserID', 0
EXEC Utility.SetDefault @cTableName, 'Status', 'Active'

EXEC Utility.SetColumnNotNull @cTableName, 'ParentUnitID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'POCUserID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Status', 'varchar(25)'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'UnitID'
EXEC Utility.SetIndexClustered 'IX_UnitName', @cTableName, 'unitname ASC'
GO

INSERT INTO dbo.Unit
	(UnitName)
SELECT 
	IDT.TierName 
FROM #oInstanceDataTable IDT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Unit U
	WHERE U.UnitName = IDT.TierName 
		AND U.Status = 'Active'
	)
GO

INSERT INTO dbo.Unit
	(UnitName)
SELECT DISTINCT LTRIM(D.DisplayText)
FROM dbo.Dropdown D
WHERE D.Category = 'Unit'
	AND LEN(LTRIM(D.DisplayText)) > 0
	AND D.DisplayText IS NOT NULL
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Unit U
		WHERE U.UnitName = LTRIM(D.DisplayText)
		)
GO

UPDATE dbo.Unit
SET ParentUnitID = 
	(
	SELECT TOP 1 U.UnitID
	FROM dbo.Unit U WITH (NOLOCK)
	WHERE U.UnitName = (SELECT IDT.TierName FROM #oInstanceDataTable IDT)
		AND U.Status = 'Active'
	ORDER BY U.UnitID 
	)
WHERE ParentUnitID = 0
	AND UnitName <> (SELECT IDT.TierName FROM #oInstanceDataTable IDT)
--End table: Unit

--Begin table: UserGroups
DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.UserGroups'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in (N'U')) 
	BEGIN

	CREATE TABLE dbo.usergroups
		(
		usergroupid int IDENTITY(1,1) NOT NULL,
		usergroupname varchar(250) NOT NULL,
		usergroupdescription varchar(max) NULL,
		usergroupstatusid int NOT NULL,
		createdat datetime NOT NULL,
		createdby int NOT NULL,
		updatedat datetime NOT NULL,
		updatedby int NOT NULL,
		) ON [PRIMARY]

	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CreatedAt', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'CreatedBy', 0
EXEC Utility.SetDefault @cTableName, 'UpdatedAt', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'UpdatedBy', 0
EXEC Utility.SetDefault @cTableName, 'UserGroupStatusID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CreatedAt', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'CreatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdatedAt', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'UpdatedBy', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'UserGroupStatusID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'UserGroupID'
CREATE UNIQUE NONCLUSTERED INDEX IX_UserGroupName ON dbo.usergroups
	(
	usergroupname ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
--End table: UserGroups
--End table updagrades

--Begin views
IF EXISTS (SELECT 1 FROM sys.views V WITH (NOLOCK) WHERE V.object_id = OBJECT_ID('dbo.LoginUser'))
	DROP VIEW dbo.LoginUser
GO

DECLARE @cCRLF varchar(2)
DECLARE @cTab1 varchar(1)
DECLARE @cTab2 varchar(2)

DECLARE @cSQL varchar(max)

SET @cCRLF = CHAR(13) + CHAR(10)
SET @cTab1 = CHAR(9)
SET @cTab2 = REPLICATE(CHAR(9), 2)

SET @cSQL = 'CREATE VIEW dbo.LoginUser AS'
SET @cSQL = @cSQL + @cCRLF + 'SELECT'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.TierUserID AS ID,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Account,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.ActiveDate,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Address,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Admin,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.AllowedCompanies,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Anthrax,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.AutoResponderBCC,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.BloodType,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.BulkMail,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.CellPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.City,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Company,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Confirmation,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Country,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DateReported,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DayPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Defaultcolor,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DefaultCompany,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Defaultfont,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DefaultProject,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Defaultsort,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.DefaultTierID,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DefaultView,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeleteEvents,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeleteTasks,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Department,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeploymentCity,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeploymentCountry,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeploymentPlace,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeploymentProvince,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DeploymentRegion,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.DOB,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Email,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Email2,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Email3,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Expertise,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.FAX,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.FirstName,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.FitRepDue,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.FitRepRO,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.FitRepRS,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.GasMaskSize,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.HomePage,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.IridiumPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.JLLISUSerID,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.JWICSEmail,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.LastName,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.LiteDefault,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.LoginUserID,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.LoginUserSettingID,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.MOS,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.MPassword,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.MSC AS MSC_OLD,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.multiCompany,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.Nationality,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.NightPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Notes,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.OriginatingTierID AS ParentService,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Password,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.PayGrade,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.PistolSerial,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.PKIForce,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.SatPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search1,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search2,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search3,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search4,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search5,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search6,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search7,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search8,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search9,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Search10,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.SecLev,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.ShipEmail,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.ShowHelp,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.SIPREmail,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.ST1 AS ST1_OLD,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.StartDate,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.State,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'TU.Status,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.SubscribeToWeekly,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.SurveyAdmin,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.SurveyKey,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.TaskTrackerToken,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.TempInt,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Title,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.TollFreePhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Unit,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LUS.UnitPhone,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.JLLISUserName AS UserName,'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JU.Zip'
SET @cSQL = @cSQL + @cCRLF + 'FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK)'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID'
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = TU.TierID'
SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND T.IsInstance = 1'
SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND T.TierName = ''' + (SELECT IDT.TierName FROM #oInstanceDataTable IDT) + ''''
SET @cSQL = @cSQL + @cCRLF + @cTab1 + 'LEFT JOIN dbo.LoginUserSetting LUS WITH (NOLOCK) ON LUS.LoginUserID = TU.TierUserID'

EXEC (@cSQL)
GO

IF EXISTS (SELECT 1 FROM sys.views SV WITH (NOLOCK) WHERE SV.object_id = OBJECT_ID(N'dbo.tier'))
	DROP VIEW dbo.tier
GO
--End views

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(N'dbo.GetMaxInteger') AND O.type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetMaxInteger
GO

-- ===================================================================
-- Author:   Todd Pires/John Lyons
-- Create date: 2010.06.22
-- Description: A function to return the greater (max) of two integers
-- ===================================================================

CREATE FUNCTION dbo.GetMaxInteger
(
@nValue1 int,
@nValue2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int

SET @nValue1 = ISNULL(@nValue1, 0)            
SET @nValue2 = ISNULL(@nValue2, 0)            

IF @nValue1 >= @nValue2                  
 SET @nRetVal = @nValue1
ELSE                  
 SET @nRetVal = @nValue2
--ENDIF

RETURN @nRetVal

END
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(N'dbo.GetReleaseableToWeight') AND O.type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetReleaseableToWeight
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(N'dbo.GetReleasableToWeight') AND O.type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.GetReleasableToWeight
GO

-- ==============================================================================================
-- Author:   Todd Pires
-- Create date: 2010.06.22
-- Description: A function to return an integer value representing a varchar releasableto label
-- ==============================================================================================

CREATE FUNCTION dbo.GetReleasableToWeight
(
@cReleasableToLabel varchar(50)
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
SET @nRetVal = 0

IF LEN(RTRIM(@cReleasableToLabel)) > 0 AND @cReleasableToLabel IS NOT NULL
 BEGIN
 
 SET @nRetVal = 
  (
  SELECT RT.ReleasableTo_Weight
  FROM dbo.ReleasableTo RT WITH (NOLOCK)
  WHERE RT.ReleasableTo_Label = @cReleasableToLabel
  )
 
 END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(N'dbo.IsEntityMember') AND O.type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.IsEntityMember
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2010.07.09
-- Description:	A function to return a bit indicating membership in an entity
-- ==========================================================================

CREATE FUNCTION dbo.IsEntityMember
(
@nEntityID int,
@cEntityTypeCode varchar(50),
@nMemberID int,
@cMemberTypeCode varchar(50)
)

RETURNS bit

AS
BEGIN

DECLARE @nRetVal bit
SET @nRetVal = 0

IF @nEntityID IS NOT NULL AND @nEntityID > 0 AND @nMemberID IS NOT NULL AND @nMemberID > 0
	BEGIN
	
	IF EXISTS	-- Is the member associatied with the entity
		(
		SELECT 1
		FROM dbo.EntityMembers EM WITH (NOLOCK)
		WHERE EM.EntityID = @nEntityID
			AND EM.EntityTypeCode = @cEntityTypeCode
			AND EM.MemberID = @nMemberID
			AND EM.MemberTypeCode = @cMemberTypeCode
		)
		
	OR
		(
		@cMemberTypeCode = 'USER' AND -- If the member is a USER
			(
			EXISTS -- Is the member associatied with a unit that is associatied with the entity
				(		
				SELECT 1
				FROM dbo.EntityMembers EM WITH (NOLOCK)
					JOIN dbo.Unit U ON U.UnitID = EM.MemberID
						AND EM.EntityID = @nEntityID
						AND EM.MemberTypeCode = 'UNIT'
					JOIN JLLIS.dbo.JLLISUser JU WITH (NOLOCK) ON JU.Unit = U.UnitName
					JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
						AND TU.TierUserID = @nMemberID
				)

			OR
	
			EXISTS -- Is the member associatied with a user group that is associatied with the entity
				(	
				SELECT 1
				FROM dbo.EntityMembers EM WITH (NOLOCK)
					JOIN dbo.UserGroups UG WITH (NOLOCK) ON UG.UserGroupID = EM.MemberID
						AND EM.EntityID = @nEntityID
						AND EM.MemberID = @nMemberID
						AND EM.MemberTypeCode = 'USERGROUP'
				)

			OR		
		
			EXISTS -- Is the member associatied with a unit that is associatied with a user group that is associatied with the entity
				(
				SELECT 1
				FROM dbo.EntityMembers EM1 WITH (NOLOCK)
					JOIN dbo.UserGroups UG WITH (NOLOCK) ON UG.UserGroupID = EM1.MemberID
						AND EM1.EntityID = @nEntityID
						AND EM1.MemberID = 
							(
							SELECT TOP 1 EM2.EntityID
							FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK) 
								JOIN dbo.Unit U ON U.UnitName = JU.Unit
								JOIN JLLIS.dbo.TierUser TU WITH (NOLOCK) ON TU.JLLISUserID = JU.JLLISUserID
									AND TU.TierUserID = @nMemberID
								JOIN dbo.EntityMembers EM2 WITH (NOLOCK) ON EM2.MemberID = U.UnitID 
									AND EM2.EntityTypeCode = 'USERGROUP'
									AND EM2.MemberTypeCode = 'UNIT'
							)
						AND EM1.MemberTypeCode = 'USERGROUP'
				)
			)	
		)
		BEGIN
		
		SET @nRetVal = 1
		
		END
	--ENDIF
	
	END
--ENDIF

RETURN ISNULL(@nRetVal, 0)

END
GO

IF EXISTS (SELECT 1 FROM sys.objects WITH (NOLOCK) WHERE object_id = OBJECT_ID(N'dbo.IsTierEntity') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION dbo.IsTierEntity
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2010.08.20
-- Description:	A function to return a bit indicating a tier entity association
-- ============================================================================

CREATE FUNCTION dbo.IsTierEntity
(
@nTierID int,
@nEntityID int,
@cEntityTypeCode varchar(50)
)

RETURNS bit

AS
BEGIN

DECLARE @nRetVal bit
SET @nRetVal = 0

IF EXISTS	-- Is the entity associated with the tier
	(
	SELECT 1
	FROM dbo.tierentities TE WITH (NOLOCK)
	WHERE TE.TierID = @nTierID
		AND TE.EntityID = @nEntityID
		AND TE.EntityTypeCode = @cEntityTypeCode
	)
	
	BEGIN
	
	SET @nRetVal = 1
	
	END
--ENDIF
	
RETURN @nRetVal

END
GO

--Begin migrate unit poc to tier poc
DECLARE @nJLLISUserID int
DECLARE @nTierID int

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		T.TierID,
		JU.JLLISUserID
	FROM JLLIS.dbo.JLLISUser JU WITH (NOLOCK)
		JOIN dbo.Unit U WITH (NOLOCK) ON U.POCUserID = JU.JLLISUserID
		JOIN JLLIS.dbo.Tier T ON T.TierName = U.MSC
	
OPEN oCursor
FETCH oCursor INTO @nTierID, @nJLLISUserID
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO JLLIS.dbo.TierPointOfContact
		(TierID, JLLISUserID)
	SELECT
		T.TierID,
		@nJLLISUserID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK)
	WHERE T.TierID = @nTierID
		AND NOT EXISTS 
			(
			SELECT 1 
			FROM JLLIS.dbo.TierPointOfContact TPOC WITH (NOLOCK) 
			WHERE TPOC.TierID = T.TierID
			)

	FETCH oCursor INTO @nTierID, @nJLLISUserID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End migrate unit poc to tier poc

--Begin BinderItem TierID Updates
UPDATE BI
SET BI.TierID = T.TierID
FROM dbo.BinderItem BI
	JOIN sys.databases DB ON BI.Link LIKE '%/' + DB.Name + '/%'
	JOIN JLLIS.dbo.Tier T ON T.TierName = DB.Name
		AND BI.TierID = 0
		AND T.IsInstance = 1

UPDATE BI
SET BI.TierID = T.TierID
FROM dbo.BinderItem BI
	JOIN JLLIS.dbo.Tier T ON T.TierName = 'USSOCOM'
		AND BI.Link LIKE '%/ussocom/%'
		AND BI.TierID = 0
		AND T.IsInstance = 1
--End BinderItem TierID Updates

--Begin OriginatingTierID Updates
DECLARE @cSQL varchar(max) 
DECLARE @cTableName varchar(250)
DECLARE @nInstanceID int
DECLARE @oTargetTables table (TableName varchar(250))

SET @nInstanceID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)

INSERT INTO @oTargetTables (TableName) VALUES ('dbo.AAR')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.BB')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.Binder')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.CDR')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.LMS')
INSERT INTO @oTargetTables (TableName) VALUES ('dbo.SSite')

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT T.TableName 
	FROM @oTargetTables T
	ORDER BY T.TableName

OPEN oCursor
FETCH oCursor INTO @cTableName
WHILE @@fetch_status = 0
	BEGIN	
	
	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'MajorCommand')
		BEGIN
		
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.MajorCommand AND T1.OriginatingTierID = 0 AND T1.MajorCommand IS NOT NULL AND LEN(LTRIM(T1.MajorCommand)) > 0 AND T2.InstanceID = ' + CAST(@nInstanceID as varchar(5))
		EXEC (@cSQL)
		
		END
	--ENDIF
		
	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'MSC')
		BEGIN
		
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.MSC AND T1.OriginatingTierID = 0 AND T1.MSC IS NOT NULL AND LEN(LTRIM(T1.MSC)) > 0 AND T2.InstanceID = ' + CAST(@nInstanceID as varchar(5))
		EXEC (@cSQL)
	
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.columns C WITH (NOLOCK) WHERE C.Object_ID = OBJECT_ID(@cTableName) AND C.Name = 'ST1')
		BEGIN
			
		SET @cSQL = 'UPDATE T1 SET T1.OriginatingTierID = T2.TierID FROM ' + @cTableName + ' T1 WITH (NOLOCK) JOIN JLLIS.dbo.Tier T2 WITH (NOLOCK) ON T2.TierName = T1.ST1 AND T1.OriginatingTierID = 0 AND T1.ST1 IS NOT NULL AND LEN(LTRIM(T1.ST1)) > 0 AND T2.InstanceID = ' + CAST(@nInstanceID as varchar(5))
		EXEC (@cSQL)
		
		END
	--ENDIF

	SET @cSQL = 'UPDATE ' + @cTableName + ' SET OriginatingTierID = ' + CAST(@nInstanceID as varchar(5)) + ' WHERE OriginatingTierID = 0'
	EXEC (@cSQL)

	FETCH oCursor INTO @cTableName
	
	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO
--End OriginatingTierID Updates

DECLARE @nInstanceID int

SET @nInstanceID = (SELECT IDT.TierID FROM #oInstanceDataTable IDT)

UPDATE B
SET B.TierID = T.TierID
FROM dbo.Banner B
	JOIN JLLIS.dbo.Tier T ON T.TierName = B.Tier
		AND B.Tier IS NOT NULL
		AND LEN(LTRIM(B.Tier)) > 0
	
UPDATE dbo.Banner
SET TierID = @nInstanceID
WHERE TierID = 0;

WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nInstanceID

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
DELETE
FROM dbo.Banner
WHERE TierID NOT IN 
	(
	SELECT HD.TierID
	FROM HD
	)

DELETE B
FROM dbo.Banner B
	JOIN JLLIS.dbo.Tier T ON T.TierID = B.TierID
		AND T.TierName <> B.Tier
		AND B.Tier IS NOT NULL
		AND LEN(LTRIM(B.Tier)) > 0

UPDATE dbo.Banner
SET IsActive = 1
WHERE ID IN
	(
	SELECT MAX(B1.ID)
	FROM dbo.Banner B1 WITH (NOLOCK)
	WHERE B1.TierID NOT IN
		(
		SELECT B2.TierID
		FROM dbo.Banner B2 WITH (NOLOCK)
		WHERE B2.IsActive = 1
		)
	GROUP BY B1.TierID
	)
		
UPDATE dbo.Content
SET TierID = @nInstanceID
WHERE TierID = 0

UPDATE dbo.DropDown
SET TierID = @nInstanceID
WHERE TierID = 0

UPDATE dbo.DropDown
SET Category = Category + '_Old'
WHERE Category IN ('Caveat','Classification','LMS_Exercise','MSC','LMS_MajorCommand','LMS_Opex','Organization','Rank','Roles','Status','SubTier1','TIER1','TIER2','Unit','UserGroupStatus')

UPDATE dbo.DropDown
SET Category = 'OperationType'
WHERE Category = 'EventType'

UPDATE EL
SET EL.TierID = T.TierID
FROM dbo.EventLog EL
	JOIN JLLIS.dbo.Tier T ON T.TierName = EL.MSC

UPDATE dbo.eventlog
SET TierID = @nInstanceID
WHERE TierID = 0;

WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nInstanceID

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)

DELETE EL
FROM dbo.EventLog EL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM HD
	WHERE HD.TierID = EL.TierID
	)
	
UPDATE MD
SET MD.TierID = T.TierID
FROM dbo.MetaData MD
	JOIN JLLIS.dbo.Tier T ON T.TierName = MD.Tier
	
UPDATE dbo.MetaData
SET TierID = @nInstanceID
WHERE TierID = 0
	
DELETE 
FROM dbo.OpEx
WHERE OpExName IS NULL
	OR LEN(RTRIM(OpExName)) = 0

UPDATE dbo.OpEx
SET TierID = @nInstanceID
WHERE TierID = 0
	
UPDATE dbo.Setup
SET TierID = @nInstanceID
WHERE TierID = 0
	
UPDATE S
SET S.TierID = T.TierID
FROM dbo.Styles S
	JOIN JLLIS.dbo.Tier T ON T.TierName = S.Tier
	
UPDATE dbo.Styles
SET TierID = @nInstanceID
WHERE TierID = 0
	
UPDATE SS
SET SS.TierID = T.TierID
FROM dbo.Static_Styles SS
	JOIN JLLIS.dbo.Tier T ON T.TierName = SS.Tier
	
UPDATE dbo.Static_Styles
SET TierID = @nInstanceID
WHERE TierID = 0

UPDATE dbo.Static_Styles
SET worldclockbg = '#000000'

;
WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nInstanceID

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.TierID = T.ParentTierID 
	)
	
DELETE
FROM dbo.Static_Styles
WHERE TierID NOT IN 
	(
	SELECT HD.TierID
	FROM HD
	)

DELETE SS
FROM dbo.Static_Styles SS
	JOIN JLLIS.dbo.Tier T ON T.TierID = SS.TierID
		AND T.TierName <> SS.Tier
GO

DECLARE @cTitle varchar(50)
SET @cTitle = 'MY GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		title,
		type
		)
	SELECT
		MI.MenuID,
		MI.parentid,
		MI.Level + 1,
		@cTitle,
		'yes'
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.title = 'MY FILES'

	END
--ENDIF

SET @cTitle = 'VIEW MY GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title,
		type
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID,
		1,
		'index.cfm?disp=UserGroups&Action=ViewMyUserGroups',
		@cTitle,
		'yes'
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
 
	END
--ENDIF

SET @cTitle = 'SEARCH GROUPS'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title,
		type
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID,
		2,
		'index.cfm?disp=UserGroups&Action=SearchUserGroups',
		@cTitle AS title,
		'yes'
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
		
	END
--ENDIF

SET @cTitle = 'ADD NEW GROUP'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title,
		type
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID,
		3,
		'index.cfm?disp=UserGroups&Action=AddUpdateUserGroup',
		@cTitle,
		'yes'
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'MY GROUPS'
		
	END
--ENDIF
GO

DECLARE @cTitle varchar(50)
SET @cTitle = 'TIER MAINTENANCE'

IF NOT EXISTS 
	(
	SELECT 1
	FROM dbo.menuitem MI
	WHERE MI.Title = @cTitle
	)
	BEGIN
	
	INSERT INTO dbo.menuitem
		(
		menuid,
		parentid,
		level,
		link,
		title,
		type
		)
	SELECT
		MI.MenuID,
		MI.MenuItemID,
		1,
		'index.cfm?disp=../admin/tiers/tierList.cfm',
		@cTitle,
		'yes'
	FROM dbo.menuitem MI WITH (NOLOCK)
	WHERE MI.Title = 'SITE MANAGEMENT'
 
	END
--ENDIF
GO

DELETE 
FROM dbo.menuitem
WHERE Link LIKE '%clt.cfm%'
GO

UPDATE dbo.MenuItem
SET Title = 'TIER METRICS REPORT'
WHERE Link = 'index.cfm?disp=adminmetrics.cfm&menudisp=adminmenu.cfm'
GO
  
UPDATE UG
SET UG.UserGroupStatusID = S.StatusID
FROM dbo.UserGroups UG
	JOIN dbo.DropDown DD ON DD.ID = UG.UserGroupStatusID
	JOIN JLLIS.dbo.Status S ON S.Status = DD.DisplayText
GO

--Begin deprecated column rename
EXEC Utility.RenameDeprecatedColumns NULL, 'ToOld'
GO
--End deprecated column rename

--Begin IsLegacyInstance and joint_LMS_for_binder updates
UPDATE JLLIS.dbo.tier
SET IsLegacyInstance = 0
WHERE TierName = (SELECT IDT.TierName FROM #oInstanceDataTable IDT)
	AND IsInstance = 1
GO

EXEC JLLIS.Utility.CreateJointLMSForBinderView
GO
--End IsLegacyInstance and joint_LMS_for_binder updates