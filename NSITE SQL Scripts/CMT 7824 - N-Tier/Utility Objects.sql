--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--Begin create utility helper tools
--Begin create IsAncestor helper function
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.IsAncestor') AND O.type in ('FN', 'IF', 'TF', 'FS', 'FT'))
	DROP FUNCTION Utility.IsAncestor
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2010.09.27
-- Description:	A function to validate that one tier is an ancestor of another.
--							Parameter @nTierID1 is the ancestral tier.
--							Parameter @nTierID2 is the descendant tier.
--							A call of Utility.IsAncestor(x, y) will return a 1 if x is an 
--							ancestor of y and a 0 if it is not.  If x = y, the function
--							returns 1.
-- ============================================================================

CREATE FUNCTION Utility.IsAncestor
(
@nTierID1 int,
@nTierID2 int
)

RETURNS int

AS
BEGIN

DECLARE @nRetVal int
DECLARE @oTable table (TierID int)
	
SET @nRetVal = 0;

WITH HD (TierID,ParentTierID)
	AS 
	(
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
	WHERE T.TierID = @nTierID2

	UNION ALL 
	
	SELECT
		T.TierID, 
		T.ParentTierID
	FROM JLLIS.dbo.Tier T WITH (NOLOCK) 
		JOIN HD ON HD.ParentTierID = T.TierID 
	)

INSERT INTO @oTable
	(TierID)
SELECT HD.TierID
FROM HD 
WHERE HD.TierID = @nTierID1

IF EXISTS (SELECT 1 FROM @oTable)
	SET @nRetVal = 1

RETURN @nRetVal

END
GO
--End create IsAncestor helper function

--Begin create AddColumn helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.AddColumn') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.AddColumn
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.AddColumn
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name = @cColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD ' + @cColumnName + ' ' + @cDataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End create AddColumn helper stored procedure

--Begin create CloneData helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CloneData') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CloneData
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CloneData
	@cTableName varchar(250),
	@cDatabaseName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL1 varchar(max)
	DECLARE @cSQL2 varchar(max)
	DECLARE @cSQL3 varchar(max)

	SET @cSQL1 = ''

	DECLARE oCursor CURSOR FOR
		SELECT SC.Name 
		FROM dbo.syscolumns SC WITH (NOLOCK) 
		WHERE SC.id = OBJECT_ID(@cTableName) 
		ORDER BY SC.Name

	OPEN oCursor
	FETCH oCursor INTO @cSQL2
	WHILE @@fetch_status = 0
		BEGIN

		IF @cSQL1 = ''
			SET @cSQL1 = @cSQL2
		ELSE
			SET @cSQL1 = @cSQL1 + ',' + @cSQL2

		FETCH oCursor INTO @cSQL2

		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	SET @cSQL3 = 'SET IDENTITY_INSERT ' + @cTableName + ' ON'
	SET @cSQL3 = @cSQL3 + ' INSERT INTO ' + @cTableName + ' (' + @cSQL1 + ') SELECT ' + @cSQL1 + ' FROM ' + @cDatabaseName + '.' + @cTableName
	SET @cSQL3 = @cSQL3 + ' SET IDENTITY_INSERT ' + @cTableName + ' OFF'

	EXEC (@cSQL3)

END
GO
--End create CloneData helper stored procedure

--Begin create CompareTableStructures helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.CompareTableStructures') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.CompareTableStructures
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.22
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.CompareTableStructures
	@cTableName varchar(250),
	@cDatabaseName1 varchar(250),
	@cDatabaseName2 varchar(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @cCRLF varchar(2)
	DECLARE @cNotExists1 varchar(max)
	DECLARE @cNotExists2 varchar(max)
	DECLARE @cNotExists3 varchar(max)
	DECLARE @cSchemaName varchar(50)
	DECLARE @cSelectList1 varchar(max)
	DECLARE @cSelectList2 varchar(max)
	DECLARE @cSQL varchar(max)
	DECLARE @cTab1 varchar(1)
	DECLARE @cTab2 varchar(2)
	DECLARE @cTab3 varchar(3)
	DECLARE @cTab4 varchar(4)
	DECLARE @cTab5 varchar(5)
	DECLARE @nPos int
	
	CREATE table #oTable 
		(
		Description varchar(250) NULL,
		ObjectType varchar(50) NULL, 
		SchemaName varchar(50) NULL, 
		TableName varchar(50) NULL, 
		ColumnName varchar(50) NULL, 
		DataType varchar(50) NULL, 
		Length int NULL, 
		Precision int NULL, 
		Scale int NULL,
		Nullable varchar(50) NULL
		) 

	SET @cCRLF = CHAR(13) + CHAR(10)
	SET @cTab1 = CHAR(9)
	SET @cTab2 = REPLICATE(CHAR(9), 2)
	SET @cTab3 = REPLICATE(CHAR(9), 3)
	SET @cTab4 = REPLICATE(CHAR(9), 4)
	SET @cTab5 = REPLICATE(CHAR(9), 5)
	
	SET @cNotExists1 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists1 = @cNotExists1 + @cCRLF + @cTab3 + ')'

	SET @cNotExists2 = @cTab5 + 'AND S2.Name = S1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND O2.Name = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab5 + 'AND C2.Name = C1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + '('
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'FROM #oTable T2'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + 'WHERE T2.ObjectType IN (''Table'', ''View'')'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab4 + 'AND T2.TableName = O1.Name'
	SET @cNotExists2 = @cNotExists2 + @cCRLF + @cTab3 + ')'

	SET @cSchemaName = 'dbo'

	SET @cSelectList1 = @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'WHEN O1.Type = ''U'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'THEN ''Table'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab2 +	'ELSE ''View'''
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'END,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList1 = @cSelectList1 + @cCRLF + @cTab1 +	'O1.Name'

	SET @cSelectList2 = '''Column'','
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'S1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'O1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'T1.Name,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Max_Length,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Precision,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'C1.Scale,'
	SET @cSelectList2 = @cSelectList2 + @cCRLF
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'CASE'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'WHEN C1.Is_Nullable = 1'
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'THEN ''Yes'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab2 +	'ELSE ''No'''
	SET @cSelectList2 = @cSelectList2 + @cCRLF + @cTab1 +	'END'
	SET @cSelectList2 = @cSelectList2 + @cCRLF

	SET @nPos = CHARINDEX('.', @cTableName)

	IF @nPos > 0
		BEGIN
		
		SET @cSchemaName = LEFT(@cTableName, @nPos - 1)
		SET @cTableName = RIGHT(@cTableName, LEN(@cTableName) - @nPos)
		
		END
	--ENDIF

	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''' AS Description,'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList1
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Type IN (''U'', ''V'')'
	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists1

	EXEC (@cSQL)
	
	SET @cSQL = 'INSERT INTO #oTable'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'(Description, ObjectType, SchemaName, TableName, ColumnName, DataType, Length, Precision, Scale, Nullable)'
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName2 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName1 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.Types T1 ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName1 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName2 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName2 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'UNION'
	SET @cSQL = @cSQL + @cCRLF
	SET @cSQL = @cSQL + @cCRLF + 'SELECT'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'''Missing from ' + @cDatabaseName1 + ''','
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	@cSelectList2
	SET @cSQL = @cSQL + @cCRLF + 'FROM ' + @cDatabaseName2 + '.sys.columns C1 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.Types T1 ON T1.system_type_id = C1.system_type_id'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.objects O1 WITH (NOLOCK) ON O1.Object_ID = C1.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab1 +	'JOIN ' + @cDatabaseName2 + '.sys.schemas S1 ON S1.schema_ID = O1.schema_ID'

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name = ''' + @cSchemaName + ''''
	ELSE
		SET @cSQL = @cSQL + @cCRLF + @cTab2 +	'AND S1.Name <> ''sys'''
	--ENDIF

	IF @cTableName IS NOT NULL
		SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND O1.Name = ''' + @cTableName + ''''

	SET @cSQL = @cSQL + @cCRLF + @cTab2 + 'AND NOT EXISTS'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + '('
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'SELECT 1'
	SET @cSQL = @cSQL + @cCRLF + @cTab3 + 'FROM ' + @cDatabaseName1 + '.sys.columns C2 WITH (NOLOCK)'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.objects O2 WITH (NOLOCK) ON O2.Object_ID = C2.Object_ID'
	SET @cSQL = @cSQL + @cCRLF + @cTab4 + 'JOIN ' + @cDatabaseName1 + '.sys.schemas S2 WITH (NOLOCK) ON S2.schema_ID = O2.schema_ID'
	SET @cSQL = @cSQL + @cCRLF + @cNotExists2

	EXEC (@cSQL)

	SELECT *
	FROM #oTable T
	ORDER BY T.ObjectType DESC, T.Description, T.SchemaName, T.TableName, T.ColumnName	
	
	DROP TABLE #oTable

END
GO
--End create CompareTableStructures helper stored procedure

--Begin create DropConstraintsAndIndexes helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.DropConstraintsAndIndexes') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.DropConstraintsAndIndexes
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropConstraintsAndIndexes
	@cTableName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL varchar(max)
	DECLARE oCursor CURSOR FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC WITH (NOLOCK)
		WHERE DC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @cTableName AS SQL
		FROM sys.indexes I WITH (NOLOCK)
		WHERE I.object_ID = OBJECT_ID(@cTableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End create DropConstraintsAndIndexes helper stored procedure

--Begin create RenameDeprecatedCategories helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedCategories') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedCategories
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedCategories
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cSQL = 'UPDATE ' + @cDatabaseName + '.dbo.DropDown'
	ELSE
		SET @cSQL = 'UPDATE dbo.DropDown'
	--ENDIF
	
	IF @cMode = 'FromOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = REPLACE(Category, ''_Old'', '''')'
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC_Old'',''TIER1_Old'',''TIER2_Old'',''SubTier1_Old'',''Unit_Old'')'
		
		END
	ELSE IF @cMode = 'ToOld'
		BEGIN
	
		SET @cSQL = @cSQL + ' SET Category = Category + ''_Old'''
		SET @cSQL = @cSQL + ' WHERE Category IN (''MSC'',''TIER1'',''TIER2'',''SubTier1'',''Unit'')'
		
		END
	--ENDIF
	
	EXEC (@cSQL)
	
END
GO
--End create RenameDeprecatedCategories helper stored procedure

--Begin create RenameDeprecatedColumns helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.RenameDeprecatedColumns') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.RenameDeprecatedColumns
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2011.02.10
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.RenameDeprecatedColumns
	@cDatabaseName varchar(50),
	@cMode varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDatabaseNameWithPeriod varchar(50)
	DECLARE @cSQL varchar(max)
	
	SET @cDatabaseNameWithPeriod = ''
	IF @cDatabaseName IS NOT NULL OR LEN(LTRIM(@cDatabaseName)) > 0
		SET @cDatabaseNameWithPeriod = @cDatabaseName + '.'

	SET @cSQL = 'INSERT INTO #oTable (SQLText) SELECT '
	SET @cSQL = @cSQL + '''EXEC ' + @cDatabaseNameWithPeriod + 'dbo.sp_rename '''''''
	SET @cSQL = @cSQL + ' + S.Name + ''.'' + O.Name + ''.'' + C.Name + '''''', '''''' +'
		
	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + ' REPLACE(C.Name, ''_Old'', '''')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + ' C.Name + ''_Old'''
	
	SET @cSQL = @cSQL + ' + '''''', ''''COLUMN'''''' AS SQLText'
	SET @cSQL = @cSQL + '	FROM ' + @cDatabaseNameWithPeriod + 'sys.objects O WITH (NOLOCK)'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.schemas S ON S.schema_ID = O.schema_ID'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.columns C WITH (NOLOCK) ON O.object_id = C.object_id'
	SET @cSQL = @cSQL + '	JOIN ' + @cDatabaseNameWithPeriod + 'sys.types T WITH (NOLOCK) ON C.user_type_id = T.user_type_id'
	SET @cSQL = @cSQL + '	AND O.type = ''U'''

	IF @cMode = 'FromOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand_old'', ''msc_old'', ''st1_old'')'
	ELSE IF @cMode = 'ToOld'
		SET @cSQL = @cSQL + '	AND C.Name IN (''majorcommand'', ''msc'', ''st1'')'

	SET @cSQL = @cSQL + '	ORDER BY S.Name, O.Name, C.Name'
	
	CREATE TABLE #oTable (SQLText varchar(max))
	
	EXEC (@cSQL)
	
	DECLARE oRenameDeprecatedColumnsCursor CURSOR FOR
		SELECT SQLText
		FROM #oTable
		
	OPEN oRenameDeprecatedColumnsCursor
	FETCH oRenameDeprecatedColumnsCursor into @cSQL
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC (@cSQL)
	
		FETCH oRenameDeprecatedColumnsCursor into @cSQL
	
		END
	--END WHILE
	
	CLOSE oRenameDeprecatedColumnsCursor
	DEALLOCATE oRenameDeprecatedColumnsCursor
	
	DROP TABLE #oTable

END
GO
--End create RenameDeprecatedColumns helper stored procedure

--Begin create SetColumnNotNull helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetColumnNotNull') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetColumnNotNull
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.01.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetColumnNotNull
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ALTER COLUMN ' + @cColumnName + ' ' + @cDataType + ' NOT NULL'
	EXEC (@cSQL)
		
END
GO
--End create SetColumnNotNull helper stored procedure

--Begin create SetDefault helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetDefault') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetDefault
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetDefault
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDefault varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cConstraintName varchar(500)
	DECLARE @cSQL varchar(max)
	DECLARE @nDefaultIsGetDate bit
	DECLARE @nDefaultIsNumeric bit
	DECLARE @nLength int

	SET @nDefaultIsGetDate = 0

	IF @cDefault = 'getDate()'
		SET @nDefaultIsGetDate = 1
		
	SET @nDefaultIsNumeric = ISNUMERIC(@cDefault)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName
	
	IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ''' + @cDefault + ''' WHERE ' + @cColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ' + @cDefault + ' WHERE ' + @cColumnName + ' IS NULL'

	EXECUTE (@cSQL)

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cConstraintName = 'DF_' + RIGHT(@cTableName, @nLength) + '_' + @cColumnName
	
	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC WITH (NOLOCK) WHERE DC.parent_object_ID = OBJECT_ID(@cTableName) AND DC.Name = @cConstraintName)
		BEGIN	

		IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @cDefault + ''' FOR ' + @cColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @cDefault + ' FOR ' + @cColumnName
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End create SetDefault helper stored procedure

--Begin create SetIndexClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexClustered helper stored procedure

--Begin create SetIndexNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexNonClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexNonClustered helper stored procedure

--Begin create SetPrimaryKeyClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyClustered helper stored procedure

--Begin create SetPrimaryKeyNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyNonClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyNonClustered helper stored procedure
--End create utility helper tools
