SELECT
	'MC' AS Type,
	L.OriginatingTierID,
	L.MajorCommand_Old AS TypeOldName,
	T1.TierName,
	T2.TierName AS TypeTierName
FROM USAF.dbo.LMS L WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = L.OriginatingTierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierName = L.MajorCommand_Old
		AND 
			(
			L.MajorCommand_Old IS NOT NULL
				AND LEN(LTRIM(L.MajorCommand_Old)) > 0
			)
		AND T2.TierName <> L.MajorCommand_Old

UNION

SELECT
	'ST' AS Type,
	L.OriginatingTierID,
	L.ST1_Old AS TypeOldName,
	T1.TierName,
	T2.TierName AS TypeTierName
FROM USAF.dbo.LMS L WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T1 ON T1.TierID = L.OriginatingTierID
	JOIN JLLIS.dbo.Tier T2 ON T2.TierName = L.ST1_Old
		AND 
			(
			L.ST1_Old IS NOT NULL
				AND LEN(LTRIM(L.ST1_Old)) > 0
			)
		AND T2.TierName <> L.ST1_Old
		
ORDER BY Type, TypeOldName
