EXEC Utility.SetDefault 'dbo.LMS', 'CountryID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'EventID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonIssueID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonTrainingCourseID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'LessonTypeID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'OriginatingTierID', 0
EXEC Utility.SetDefault 'dbo.LMS', 'ViewCount', 0
EXEC Utility.SetDefault 'dbo.LMS', 'WarfareMissionID', 0

EXEC Utility.SetColumnNotNull 'dbo.LMS', 'CountryID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'EventID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonIssueID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonTrainingCourseID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'LessonTypeID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'OriginatingTierID', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'ViewCount', 'int'
EXEC Utility.SetColumnNotNull 'dbo.LMS', 'WarfareMissionID', 'int'
