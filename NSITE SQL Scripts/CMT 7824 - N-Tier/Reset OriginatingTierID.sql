-- ***************************************************************************** --
-- ***** REMEMBER TO RESET THE DB AND THE TABLE AND THE MajorCommand / MSC ***** --
-- ***************************************************************************** --

UPDATE T1 
	SET T1.OriginatingTierID = T2.TierID 
FROM USAF.dbo.LMS T1 
	JOIN JLLIS.dbo.Tier T2 ON T2.TierName = T1.MajorCommand_Old 
		AND T1.MajorCommand_Old IS NOT NULL 
		AND LEN(LTRIM(T1.MajorCommand_Old)) > 0 
		AND T2.InstanceID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WHERE T.TierName = 'USAF' AND T.IsInstance = 1)

UPDATE T1 
	SET T1.OriginatingTierID = T2.TierID 
FROM USAF.dbo.LMS T1 
	JOIN JLLIS.dbo.Tier T2 ON T2.TierName = T1.MSC_Old 
		AND T1.MSC_Old IS NOT NULL 
		AND LEN(LTRIM(T1.MSC_Old)) > 0 
		AND T2.InstanceID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WHERE T.TierName = 'USAF' AND T.IsInstance = 1)

UPDATE T1 
	SET T1.OriginatingTierID = T2.TierID 
FROM USAF.dbo.LMS T1 
	JOIN JLLIS.dbo.Tier T2 ON T2.TierName = T1.ST_Old 
		AND T1.ST_Old IS NOT NULL 
		AND LEN(LTRIM(T1.ST_Old)) > 0 
		AND 
			(
			T1.MajorCommand_Old IS NULL 
				OR LEN(LTRIM(T1.MajorCommand_Old)) = 0
			)
		AND 
			(
			T1.MSC_Old IS NULL 
				OR LEN(LTRIM(T1.MSC_Old)) = 0
			)
		AND T2.InstanceID = (SELECT T.TierID FROM JLLIS.dbo.Tier T WHERE T.TierName = 'USAF' AND T.IsInstance = 1)
