SELECT DISTINCT 
'INSERT INTO dbo.Styles (cfColor,cpColor,isActive,lpColor,pocboxColor,pocoutlineColor,roundbox,tierid) SELECT ''' + S.cfColor
	+ ''',''' + S.cpColor
	+ ''',' + CAST(S.isActive as varchar(5))
	+ ',''' + S.lpColor
	+ ''',''' + S.pocboxColor
	+ ''',''' + S.pocoutlineColor
	+ ''',''' + S.roundbox
	+ ''',T.tierid'
	+ ' FROM JLLIS.dbo.Tier T WHERE T.TierName = '
	+ '''' + T.TierName + ''''
FROM dbo.Styles S WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = S.TierID
		AND S.IsActive = 1