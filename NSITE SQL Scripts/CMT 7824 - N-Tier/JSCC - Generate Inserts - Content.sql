SELECT
'INSERT INTO dbo.Content (condition,description,image,imagecaption,link,maintext,newwindow,status,tease,title,type,displayorder,imageheight,imagewidth,tierid) SELECT ''' + C.condition
	+ ''',''' + C.description
	+	''',''' + C.image
	+	''',''' + C.imagecaption
	+	''',''' + C.link
	+	''',''' + CAST(C.maintext as varchar(max))
	+	''',''' + C.newwindow
	+	''',''' + C.status
	+	''',''' + CAST(C.tease as varchar(max))
	+	''',''' + C.title
	+	''',''' + C.type
	+ ''',' + CAST(C.displayorder as varchar(5))
	+ ',' + CAST(C.imageheight as varchar(10))
	+ ',' + CAST(C.imagewidth as varchar(10))
	+ ',' + CAST(T.Tierid as varchar(5))
	+ ' FROM JLLIS.dbo.Tier T WHERE T.TierName = '
	+ '''' + T.TierName + ''''
FROM dbo.Content C WITH (NOLOCK)
	JOIN JLLIS.dbo.Tier T WITH (NOLOCK) ON T.TierID = C.TierID
		AND C.Status = 'Active'
		AND C.Type = 'Subordinate org'
ORDER BY T.TierName


select * from dbo.Content