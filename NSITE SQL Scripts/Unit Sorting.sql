WITH HD (DisplayOrder,UnitLineage,UnitName,UnitID,ParentUnitID,NodeLevel)
	AS 
	(
	SELECT
		CONVERT(varchar(255), U.UnitName),
		CAST(U.UnitID as varchar(50)),
		U.UnitName,
		U.UnitID, 
		U.ParentUnitID, 
		1
	FROM dbo.Unit U WITH (NOLOCK) 
	WHERE U.ParentUnitID = 0

	UNION ALL
	
	SELECT
		CONVERT(varchar(255), RTRIM(DisplayOrder) + ',' + U.UnitName),
		CAST(HD.UnitLineage + ',' + CAST(U.UnitID as varchar(50)) as varchar(50)),
		U.UnitName,
		U.UnitID, 
		U.ParentUnitID, 
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.Unit U WITH (NOLOCK) 
		JOIN HD ON HD.UnitID = U.ParentUnitID 
	)
	
SELECT 
	HD.UnitLineage,
	HD.UnitName,
	REPLICATE('&nbsp;', HD.NodeLevel - 1) + HD.UnitName AS TreeUnitName,
	HD.UnitID, 
	HD.NodeLevel
FROM HD
ORDER BY HD.DisplayOrder