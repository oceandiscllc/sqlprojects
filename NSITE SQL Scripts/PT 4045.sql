--Begin PT 4045
USE JLLIS
GO

IF NOT EXISTS (SELECT 1 FROM Dropdown.PayGrade PG WHERE PG.PayGrade = 'CIVMAR')
	BEGIN
	
	DECLARE @nDisplayOrder INT
	
	SELECT @nDisplayOrder = PG.DisplayOrder 
	FROM Dropdown.PayGrade PG 
	WHERE PG.PayGrade = 'CIV'

	UPDATE Dropdown.PayGrade
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > @nDisplayOrder
	
	INSERT INTO Dropdown.PayGrade
		(PayGrade,DisplayOrder)
	VALUES
		('CIVMAR', @nDisplayOrder + 1)
		
	END
--ENDIF
--End PT 4045
