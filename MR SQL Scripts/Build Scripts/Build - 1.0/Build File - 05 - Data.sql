USE ManicReaders
GO

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Any', '[[SiteLink]]', 'Embedded Site Link'),
	('Any', '[[SiteURL]]', 'Site URL'),
	('Person', '[[EmailAddress]]', 'Sender Email'),
	('Person', '[[PasswordTokenLink]]', 'Embedded Reset Password Link'),
	('Person', '[[PasswordTokenURL]]', 'Site Reset Password URL'),
	('Person', '[[PersonNameFormattedLong]]', 'Sender Name (Long)'),
	('Person', '[[PersonNameFormattedShort]]', 'Sender Name (Short)')
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Author',
	@EntityTypeName = 'Author',
	@EntityTypeNamePlural = 'Authors',
	@SchemaName = 'author',
	@TableName = 'Author',
	@PrimaryKeyFieldName = 'AuthorID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Book',
	@EntityTypeName = 'Book',
	@EntityTypeNamePlural = 'Books',
	@SchemaName = 'book',
	@TableName = 'Book',
	@PrimaryKeyFieldName = 'BookID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Document',
	@EntityTypeName = 'Document',
	@EntityTypeNamePlural = 'Documents',
	@SchemaName = 'document',
	@TableName = 'Document',
	@PrimaryKeyFieldName = 'DocumentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PublishingCompany',
	@EntityTypeName = 'Publishing Company',
	@EntityTypeNamePlural = 'Publishing Companyies',
	@SchemaName = 'company',
	@TableName = 'PublishingCompany',
	@PrimaryKeyFieldName = 'PublishingCompanyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ReviewingCompany',
	@EntityTypeName = 'Reviewing Company',
	@EntityTypeNamePlural = 'Reviewing Companyies',
	@SchemaName = 'company',
	@TableName = 'ReviewingCompany',
	@PrimaryKeyFieldName = 'ReviewingCompanyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Review',
	@EntityTypeName = 'Review',
	@EntityTypeNamePlural = 'Reviews',
	@SchemaName = 'review',
	@TableName = 'Review',
	@PrimaryKeyFieldName = 'ReviewingID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'FormEncryptionKey', NULL, 'rt6j7NoLd0EHyg8VyRW0rw=='
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@manicreaders.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '365'
EXEC core.SystemSetupAddUpdate 'RoyaltyVendorRequestMailTo', NULL, 'dan@manicreaders.com'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://mrdev.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'ManicReaders'
GO
--End table core.SystemSetup

--Begin table document.FileType
TRUNCATE TABLE document.FileType
GO

INSERT INTO document.FileType
	(Extension, MimeType)
VALUES
	('.doc', 'application/msword'),
	('.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('.gif', 'image/gif'),
	('.htm', 'text/html'),
	('.html', 'text/html'),
	('.jpeg', 'image/jpeg'),
	('.jpg', 'image/jpeg'),
	('.pdf', 'application/pdf'),
	('.png', 'image/png'),
	('.pps', 'application/mspowerpoint'),
	('.ppt', 'application/mspowerpoint'),
	('.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
	('.rtf', 'application/rtf'),
	('.txt', 'text/plain'),
	('.xls', 'application/excel'),
	('.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
GO
--End table document.FileType

--Begin table dropdown.AuthorNewsCategory
TRUNCATE TABLE dropdown.AuthorNewsCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.AuthorNewsCategory', 'AuthorNewsCategoryID', 0
GO

INSERT INTO dropdown.AuthorNewsCategory
	(AuthorNewsCategoryCode, AuthorNewsCategoryName)
VALUES
	('Book', 'Book'),
	('Contract', 'Contract'),
	('General', 'General'),
	('News', 'News'),
	('Promo', 'Promo'),
	('Review', 'Review')
GO
--End table document.AuthorNewsCategory

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder, IsActive) 
VALUES 
	(N'GB', N'GBR', N'United Kingdom', 3, 1),
	(N'AD', N'AND', N'Andorra', 100, 1),
	(N'AE', N'ARE', N'United Arab Emirates', 100, 1),
	(N'AF', N'AFG', N'Afghanistan', 100, 1),
	(N'AG', N'ATG', N'Antigua and Barbuda', 100, 1),
	(N'AI', N'AIA', N'Anguilla', 100, 1),
	(N'AL', N'ALB', N'Albania', 100, 1),
	(N'AM', N'ARM', N'Armenia', 100, 1),
	(N'AO', N'AGO', N'Angola', 100, 1),
	(N'AR', N'ARG', N'Argentina', 100, 1),
	(N'AS', N'ASM', N'American Samoa', 100, 1),
	(N'AT', N'AUT', N'Austria', 100, 1),
	(N'AU', N'AUS', N'Australia', 4, 1),
	(N'AW', N'ABW', N'Aruba', 100, 1),
	(N'AX', N'ALA', N'Åland Islands', 100, 1),
	(N'AZ', N'AZE', N'Azerbaijan', 100, 1),
	(N'BA', N'BIH', N'Bosnia and Herzegovina', 100, 1),
	(N'BB', N'BRB', N'Barbados', 100, 1),
	(N'BD', N'BGD', N'Bangladesh', 100, 1),
	(N'BE', N'BEL', N'Belgium', 100, 1),
	(N'BF', N'BFA', N'Burkina Faso', 100, 1),
	(N'BG', N'BGR', N'Bulgaria', 100, 1),
	(N'BH', N'BHR', N'Bahrain', 100, 1),
	(N'BI', N'BDI', N'Burundi', 100, 1),
	(N'BJ', N'BEN', N'Benin', 100, 1),
	(N'BL', N'BLM', N'Saint Barthélemy', 100, 1),
	(N'BM', N'BMU', N'Bermuda', 100, 1),
	(N'BN', N'BRN', N'Brunei Darussalam', 100, 1),
	(N'BO', N'BOL', N'Bolivia', 100, 1),
	(N'BR', N'BRA', N'Brazil', 100, 1),
	(N'BS', N'BHS', N'Bahamas', 100, 1),
	(N'BT', N'BTN', N'Bhutan', 100, 1),
	(N'BU', N'BES', N'Bonaire, Sint Eustatius and Saba', 100, 1),
	(N'BV', N'BVT', N'Bouvet Island', 100, 1),
	(N'BW', N'BWA', N'Botswana', 100, 1),
	(N'BY', N'BLR', N'Belarus', 100, 1),
	(N'BZ', N'BLZ', N'Belize', 100, 1),
	(N'CA', N'CAN', N'Canada', 2, 1),
	(N'CC', N'CCK', N'Cocos (Keeling) Islands', 100, 1),
	(N'CD', N'COD', N'Congo, The Democratic Republic Of The', 100, 1),
	(N'CF', N'CAF', N'Central African Republic', 100, 1),
	(N'CG', N'COG', N'Congo', 100, 1),
	(N'CH', N'CHE', N'Switzerland', 100, 1),
	(N'CI', N'CIV', N'Côte D''Ivoire', 100, 1),
	(N'CK', N'COK', N'Cook Islands', 100, 1),
	(N'CL', N'CHL', N'Chile', 100, 1),
	(N'CM', N'CMR', N'Cameroon', 100, 1),
	(N'CN', N'CHN', N'China', 100, 1),
	(N'CO', N'COL', N'Colombia', 100, 1),
	(N'CR', N'CRI', N'Costa Rica', 100, 1),
	(N'CU', N'CUB', N'Cuba', 100, 1),
	(N'CV', N'CPV', N'Cape Verde', 100, 1),
	(N'CW', N'CUW', N'Curaçao', 100, 1),
	(N'CX', N'CXR', N'Christmas Island', 100, 1),
	(N'CY', N'CYP', N'Cyprus', 100, 1),
	(N'CZ', N'CZE', N'Czech Republic', 100, 1),
	(N'DE', N'DEU', N'Germany', 100, 1),
	(N'DJ', N'DJI', N'Djibouti', 100, 1),
	(N'DK', N'DNK', N'Denmark', 100, 1),
	(N'DM', N'DMA', N'Dominica', 100, 1),
	(N'DO', N'DOM', N'Dominican Republic', 100, 1),
	(N'DZ', N'DZA', N'Algeria', 100, 1),
	(N'EC', N'ECU', N'Ecuador', 100, 1),
	(N'EE', N'EST', N'Estonia', 100, 1),
	(N'EG', N'EGY', N'Egypt', 100, 1),
	(N'EH', N'ESH', N'Western Sahara', 100, 1),
	(N'ER', N'ERI', N'Eritrea', 100, 1),
	(N'ES', N'ESP', N'Spain', 100, 1),
	(N'ET', N'ETH', N'Ethiopia', 100, 1),
	(N'EU', N'EUR', N'European Union', 100, 1),
	(N'FI', N'FIN', N'Finland', 100, 1),
	(N'FJ', N'FJI', N'Fiji', 100, 1),
	(N'FK', N'FLK', N'Falkland Islands', 100, 1),
	(N'FM', N'FSM', N'Micronesia, Federated States Of', 100, 1),
	(N'FO', N'FRO', N'Faroe Islands', 100, 1),
	(N'FR', N'FRA', N'France', 100, 1),
	(N'GA', N'GAB', N'Gabon', 100, 1),
	(N'GD', N'GRD', N'Grenada', 100, 1),
	(N'GE', N'GEO', N'Georgia', 100, 1),
	(N'GF', N'GUF', N'French Guiana', 100, 1),
	(N'GG', N'GGY', N'Guernsey', 100, 1),
	(N'GH', N'GHA', N'Ghana', 100, 1),
	(N'GI', N'GIB', N'Gibraltar', 100, 1),
	(N'GL', N'GRL', N'Greenland', 100, 1),
	(N'GM', N'GMB', N'Gambia', 100, 1),
	(N'GN', N'GIN', N'Guinea', 100, 1),
	(N'GP', N'GLP', N'Guadeloupe', 100, 1),
	(N'GQ', N'GNQ', N'Equatorial Guinea', 100, 1),
	(N'GR', N'GRC', N'Greece', 100, 1),
	(N'GT', N'GTM', N'Guatemala', 100, 1),
	(N'GU', N'GUM', N'Guam', 100, 1),
	(N'GW', N'GNB', N'Guinea-Bissau', 100, 1),
	(N'GY', N'GUY', N'Guyana', 100, 1),
	(N'HK', N'HKG', N'Hong Kong', 100, 1),
	(N'HM', N'HMD', N'Heard Island and McDonald Islands', 100, 1),
	(N'HN', N'HND', N'Honduras', 100, 1),
	(N'HR', N'HRV', N'Croatia', 100, 1),
	(N'HT', N'HTI', N'Haiti', 100, 1),
	(N'HU', N'HUN', N'Hungary', 100, 1),
	(N'ID', N'IDN', N'Indonesia', 100, 1),
	(N'IE', N'IRL', N'Ireland', 100, 1),
	(N'IL', N'ISR', N'Israel', 100, 1),
	(N'IM', N'IMN', N'Isle of Man', 100, 1),
	(N'IN', N'IND', N'India', 100, 1),
	(N'IO', N'IOT', N'British Indian Ocean Territory', 100, 1),
	(N'IQ', N'IRQ', N'Iraq', 100, 1),
	(N'IR', N'IRN', N'Iran, Islamic Republic Of', 100, 1),
	(N'IS', N'ISL', N'Iceland', 100, 1),
	(N'IT', N'ITA', N'Italy', 100, 1),
	(N'JE', N'JEY', N'Jersey', 100, 1),
	(N'JM', N'JAM', N'Jamaica', 100, 1),
	(N'JO', N'JOR', N'Jordan', 100, 1),
	(N'JP', N'JPN', N'Japan', 100, 1),
	(N'KE', N'KEN', N'Kenya', 100, 1),
	(N'KG', N'KGZ', N'Kyrgyzstan', 100, 1),
	(N'KH', N'KHM', N'Cambodia', 100, 1),
	(N'KI', N'KIR', N'Kiribati', 100, 1),
	(N'KM', N'COM', N'Comoros', 100, 1),
	(N'KN', N'KNA', N'Saint Kitts And Nevis', 100, 1),
	(N'KP', N'PRK', N'Korea, Democratic People''s Republic Of', 100, 1),
	(N'KR', N'KOR', N'Korea, Republic of', 100, 1),
	(N'KW', N'KWT', N'Kuwait', 100, 1),
	(N'KY', N'CYM', N'Cayman Islands', 100, 1),
	(N'KZ', N'KAZ', N'Kazakhstan', 100, 1),
	(N'LA', N'LAO', N'Lao People''s Democratic Republic', 100, 1),
	(N'LB', N'LBN', N'Lebanon', 100, 1),
	(N'LC', N'LCA', N'Saint Lucia', 100, 1),
	(N'LI', N'LIE', N'Liechtenstein', 100, 1),
	(N'LK', N'LKA', N'Sri Lanka', 100, 1),
	(N'LR', N'LBR', N'Liberia', 100, 1),
	(N'LS', N'LSO', N'Lesotho', 100, 1),
	(N'LT', N'LTU', N'Lithuania', 100, 1),
	(N'LU', N'LUX', N'Luxembourg', 100, 1),
	(N'LV', N'LVA', N'Latvia', 100, 1),
	(N'LY', N'LBY', N'Libya', 100, 1),
	(N'MA', N'MAR', N'Morocco', 100, 1),
	(N'MC', N'MCO', N'Monaco', 100, 1),
	(N'MD', N'MDA', N'Moldova, Republic of', 100, 1),
	(N'ME', N'MNE', N'Montenegro', 100, 1),
	(N'MF', N'MAF', N'Saint Martin (French Part)', 100, 1),
	(N'MG', N'MDG', N'Madagascar', 100, 1),
	(N'MH', N'MHL', N'Marshall Islands', 100, 1),
	(N'MK', N'MKD', N'Macedonia', 100, 1),
	(N'ML', N'MLI', N'Mali', 100, 1),
	(N'MM', N'MMR', N'Myanmar', 100, 1),
	(N'MN', N'MNG', N'Mongolia', 100, 1),
	(N'MO', N'MAC', N'Macao', 100, 1),
	(N'MP', N'MNP', N'Northern Mariana Islands', 100, 1),
	(N'MQ', N'MTQ', N'Martinique', 100, 1),
	(N'MR', N'MRT', N'Mauritania', 100, 1),
	(N'MS', N'MSR', N'Montserrat', 100, 1),
	(N'MT', N'MLT', N'Malta', 100, 1),
	(N'MU', N'MUS', N'Mauritius', 100, 1),
	(N'MV', N'MDV', N'Maldives', 100, 1),
	(N'MW', N'MWI', N'Malawi', 100, 1),
	(N'MX', N'MEX', N'Mexico', 100, 1),
	(N'MY', N'MYS', N'Malaysia', 100, 1),
	(N'MZ', N'MOZ', N'Mozambique', 100, 1),
	(N'NA', N'NAM', N'Namibia', 100, 1),
	(N'NC', N'NCL', N'New Caledonia', 100, 1),
	(N'NE', N'NER', N'Niger', 100, 1),
	(N'NF', N'NFK', N'Norfolk Island', 100, 1),
	(N'NG', N'NGA', N'Nigeria', 100, 1),
	(N'NI', N'NIC', N'Nicaragua', 100, 1),
	(N'NL', N'NLD', N'Netherlands', 100, 1),
	(N'NO', N'NOR', N'Norway', 100, 1),
	(N'NP', N'NPL', N'Nepal', 100, 1),
	(N'NR', N'NRU', N'Nauru', 100, 1),
	(N'NU', N'NIU', N'Niue', 100, 1),
	(N'NZ', N'NZL', N'New Zealand', 5, 1),
	(N'OM', N'OMN', N'Oman', 100, 1),
	(N'PA', N'PAN', N'Panama', 100, 1),
	(N'PE', N'PER', N'Peru', 100, 1),
	(N'PF', N'PYF', N'French Polynesia', 100, 1),
	(N'PG', N'PNG', N'Papua New Guinea', 100, 1),
	(N'PH', N'PHL', N'Philippines', 100, 1),
	(N'PK', N'PAK', N'Pakistan', 100, 1),
	(N'PL', N'POL', N'Poland', 100, 1),
	(N'PM', N'SPM', N'Saint Pierre And Miquelon', 100, 1),
	(N'PN', N'PCN', N'Pitcairn', 100, 1),
	(N'PR', N'PRI', N'Puerto Rico', 100, 1),
	(N'PT', N'PRT', N'Portugal', 100, 1),
	(N'PW', N'PLW', N'Palau', 100, 1),
	(N'PY', N'PRY', N'Paraguay', 100, 1),
	(N'QA', N'QAT', N'Qatar', 100, 1),
	(N'RE', N'REU', N'Réunion', 100, 1),
	(N'RO', N'ROU', N'Romania', 100, 1),
	(N'RS', N'SRB', N'Serbia', 100, 1),
	(N'RU', N'RUS', N'Russian Federation', 100, 1),
	(N'RW', N'RWA', N'Rwanda', 100, 1),
	(N'SA', N'SAU', N'Saudi Arabia', 100, 1),
	(N'SB', N'SLB', N'Solomon Islands', 100, 1),
	(N'SC', N'SYC', N'Seychelles', 100, 1),
	(N'SD', N'SDN', N'Sudan', 100, 1),
	(N'SE', N'SWE', N'Sweden', 100, 1),
	(N'SG', N'SGP', N'Singapore', 100, 1),
	(N'SH', N'SHN', N'Saint Helena, Ascension and Tristan Da Cunha', 100, 1),
	(N'SI', N'SVN', N'Slovenia', 100, 1),
	(N'SJ', N'SJM', N'Svalbard And Jan Mayen', 100, 1),
	(N'SK', N'SVK', N'Slovakia', 100, 1),
	(N'SL', N'SLE', N'Sierra Leone', 100, 1),
	(N'SM', N'SMR', N'San Marino', 100, 1),
	(N'SN', N'SEN', N'Senegal', 100, 1),
	(N'SO', N'SOM', N'Somalia', 100, 1),
	(N'SR', N'SUR', N'Suriname', 100, 1),
	(N'SS', N'SSD', N'South Sudan', 100, 1),
	(N'ST', N'STP', N'Sao Tome and Principe', 100, 1),
	(N'SV', N'SLV', N'El Salvador', 100, 1),
	(N'SX', N'SXM', N'Sint Maarten (Dutch part)', 100, 1),
	(N'SY', N'SYR', N'Syrian Arab Republic', 100, 1),
	(N'SZ', N'SWZ', N'Swaziland', 100, 1),
	(N'TC', N'TCA', N'Turks and Caicos Islands', 100, 1),
	(N'TD', N'TCD', N'Chad', 100, 1),
	(N'TF', N'ATF', N'French Southern Territories', 100, 1),
	(N'TG', N'TGO', N'Togo', 100, 1),
	(N'TH', N'THA', N'Thailand', 100, 1),
	(N'TJ', N'TJK', N'Tajikistan', 100, 1),
	(N'TK', N'TKL', N'Tokelau', 100, 1),
	(N'TL', N'TLS', N'Timor-Leste', 100, 1),
	(N'TM', N'TKM', N'Turkmenistan', 100, 1),
	(N'TN', N'TUN', N'Tunisia', 100, 1),
	(N'TO', N'TON', N'Tonga', 100, 1),
	(N'TR', N'TUR', N'Turkey', 100, 1),
	(N'TT', N'TTO', N'Trinidad and Tobago', 100, 1),
	(N'TV', N'TUV', N'Tuvalu', 100, 1),
	(N'TW', N'TWN', N'Taiwan', 100, 1),
	(N'TZ', N'TZA', N'Tanzania, United Republic of', 100, 1),
	(N'UA', N'UKR', N'Ukraine', 100, 1),
	(N'UG', N'UGA', N'Uganda', 100, 1),
	(N'UM', N'UMI', N'United States Minor Outlying Islands', 100, 1),
	(N'US', N'USA', N'United States', 1, 1),
	(N'UY', N'URY', N'Uruguay', 100, 1),
	(N'UZ', N'UZB', N'Uzbekistan', 100, 1),
	(N'VA', N'VAT', N'Holy See (Vatican City State)', 100, 1),
	(N'VC', N'VCT', N'Saint Vincent And The Grenadines', 100, 1),
	(N'VE', N'VEN', N'Venezuela, Bolivarian Republic of', 100, 1),
	(N'VG', N'VGB', N'Virgin Islands, British', 100, 1),
	(N'VI', N'VIR', N'Virgin Islands, U.S.', 100, 1),
	(N'VN', N'VNM', N'Viet Nam', 100, 1),
	(N'VU', N'VUT', N'Vanuatu', 100, 1),
	(N'WF', N'WLF', N'Wallis and Futuna', 100, 1),
	(N'WS', N'WSM', N'Samoa', 100, 1),
	(N'YE', N'YEM', N'Yemen', 100, 1),
	(N'YT', N'MYT', N'Mayotte', 100, 1),
	(N'ZA', N'ZAF', N'South Africa', 100, 1),
	(N'ZM', N'ZMB', N'Zambia', 100, 1),
	(N'ZW', N'ZWE', N'Zimbabwe', 100, 1)
GO
--End table dropdown.Country

--Begin table dropdown.Format
TRUNCATE TABLE dropdown.Format
GO

EXEC utility.InsertIdentityValue 'dropdown.Format', 'FormatID', 0
GO

INSERT INTO dropdown.Format
	(FormatName)
VALUES
	('Audio'),
	('E-Book'),
	('E-Pub'),
	('HTML'),
	('Kindle'),
	('PDF'),
	('Print')
GO
--End table dropdown.Format

--Begin table dropdown.HeatLevel
TRUNCATE TABLE dropdown.HeatLevel
GO

EXEC utility.InsertIdentityValue 'dropdown.HeatLevel', 'HeatLevelID', 0
GO

INSERT INTO dropdown.HeatLevel
	(HeatLevelCode,HeatLevelName,DisplayOrder)
VALUES
	('Erotic','Erotic',1),
	('EroticFF','Erotic - Female/Female',2),
	('EroticMM','Erotic - Male/Male',3),
	('EroticM','Erotic - Menage',4),
	('Hot','Hot',5),
	('Mainstream','Mainstream',6),
	('Sweet','Sweet',7)
GO
--End table document.Format

--Begin table dropdown.MembershipType
TRUNCATE TABLE dropdown.MembershipType
GO

EXEC utility.InsertIdentityValue 'dropdown.MembershipType', 'MembershipTypeID', 0
GO

INSERT INTO dropdown.MembershipType
	(MembershipTerm,MembershipTypeName,MembershipAmount,DisplayOrder,IsActive)
VALUES
	(3, 'Quarterly', 13.47, 3, 1),
	(1, 'Monthly', 5, 4, 1),
	(12, 'Annual', 35.88, 1, 1),
	(6, 'Semi Annual', 35.88, 2, 0)
GO
--End table dropdown.MembershipType

--Begin table dropdown.PublishingCompanyRole
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.PublishingCompanyRole', 'PublishingCompanyRoleID', 0
GO

INSERT INTO dropdown.PublishingCompanyRole
	(PublishingCompanyRoleCode, PublishingCompanyRoleName, DisplayOrder) 
VALUES 
	('Publisher', 'Publisher', 1)
GO
--End table dropdown.PublishingCompanyRole

--Begin table dropdown.ReviewingCompanyRole
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.ReviewingCompanyRole', 'ReviewingCompanyRoleID', 0
GO

INSERT INTO dropdown.ReviewingCompanyRole
	(ReviewingCompanyRoleCode, ReviewingCompanyRoleName, DisplayOrder) 
VALUES 
	('Administrator', 'Administrator', 1),
	('Editor', 'Editor', 2),
	('Reviewer', 'Reviewer', 3)
GO
--End table dropdown.ReviewingCompanyRole

--Begin table dropdown.ReviewRating
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.ReviewRating', 'ReviewRatingID', 0
GO

INSERT INTO dropdown.ReviewRating
	(ReviewRatingCode, ReviewRatingName, DisplayOrder) 
VALUES 
	('5', '5 - The best of the best', 1),
	('4.5', '4.5 - One of the best', 2),
	('4', '4 - Highly recommended', 3),
	('3.5', '3.5 - Definitely worth the time', 4),
	('3', '3 - Average', 5),
	('2.5', '2.5 - Below average', 6),
	('2', '2 - Poor', 7),
	('1.5', '1.5 - Really poor', 8),
	('1', '1 - Just plain bad', 9),
	('.5', '.5 - Not worth the time', 10),
	('0', '0 - Worst of the worst', 11)
GO
--End table dropdown.ReviewRating

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role
	(RoleCode, RoleName, DisplayOrder) 
VALUES 
	('Author', 'Author', 2),
	('PublishingCompany', 'PublishingCompany', 3),
	('Reader', 'Reader', 1),
	('Reviewer', 'Reviewer', 4)
GO
--End table dropdown.Role

--Begin table dropdown.SiteAdLocation
TRUNCATE TABLE dropdown.SiteAdLocation
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdLocation', 'SiteAdLocationID', 0
GO

INSERT INTO dropdown.SiteAdLocation
	(SiteAdLocationCode, SiteAdLocationName)
VALUES
	('About', 'About'),
	('Any', 'Any'),
	('Authors', 'Authors'),
	('Books', 'Books'),
	('Home', 'Home'),
	('Publishers', 'Publishers'),
	('ReviewDetail', 'ReviewDetail')
GO
--End table document.SiteAdLocation

--Begin table dropdown.SiteAdStatus
TRUNCATE TABLE dropdown.SiteAdStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdStatus', 'SiteAdStatusID', 0
GO

INSERT INTO dropdown.SiteAdStatus
	(SiteAdStatusName)
VALUES
	('Active'),
	('Package'),
	('Package End')
GO
--End table document.SiteAdStatus

--Begin table dropdown.SiteAdType
TRUNCATE TABLE dropdown.SiteAdType
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdType', 'SiteAdTypeID', 0
GO

INSERT INTO dropdown.SiteAdType
	(SiteAdTypeName)
VALUES
	('Free'),
	('Package'),
	('Paid')
GO
--End table document.SiteAdType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

EXEC utility.InsertIdentityValue 'dropdown.Status', 'StatusID', 0
GO

INSERT INTO dropdown.Status
	(StatusCode, StatusName, DisplayOrder) 
VALUES 
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Deleted', 'Deleted', 3)
GO
--End table dropdown.Status
