USE ManicReaders
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	EmailTemplateDescription VARCHAR(500),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode'
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplateField'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplateField
	(
	EmailTemplateFieldID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PlaceHolderText VARCHAR(50),
	PlaceHolderDescription VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateFieldID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode,DisplayOrder'
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.SiteAd
DECLARE @TableName VARCHAR(250) = 'core.SiteAd'

EXEC utility.DropObject @TableName

CREATE TABLE core.SiteAd
	(
	SiteAdID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	StartDate DATE, 
	EndDate DATE, 
	SiteAdLocationID INT, 
	SiteAdStatusID INT,
	SiteAdTypeID INT,
	EmailAddress VARCHAR(320),
	Notes VARCHAR(MAX), 
	InvoiceDetails VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SiteAdStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SiteAdLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SiteAdTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SiteAdID'
EXEC utility.SetIndexClustered @TableName, 'IX_SiteAd', 'BookID,StartDate'
GO
--End table core.SiteAd

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE document.Document
		(
		DocumentID INT IDENTITY(1,1) NOT NULL,
		DocumentDate DATE,
		DocumentDescription VARCHAR(1000),
		DocumentGUID VARCHAR(50),
		DocumentTitle VARCHAR(250),
		DocumentTypeID INT,
		Extension VARCHAR(10),
		CreatePersonID INT,
		ContentType VARCHAR(250),
		ContentSubtype VARCHAR(100),
		DocumentData VARBINARY(MAX),
		PhysicalFileSize BIGINT,
		ThumbnailData VARBINARY(MAX),
		ThumbnailSize BIGINT,
		CreateDateTime DATETIME
		)

	EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'ThumbnailSize', 'BIGINT', '0'

	EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'

	EXEC utility.DropFullTextIndex 'document.Document'

	CREATE FULLTEXT INDEX ON document.Document 
		(
		DocumentData TYPE COLUMN Extension LANGUAGE ENGLISH
		) KEY INDEX PK_Document ON ManicReadersCatalog

	END
--ENDIF
--End table document.Document
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE document.DocumentEntity
		(
		DocumentEntityID INT IDENTITY(1,1) NOT NULL,
		DocumentID INT,
		EntityTypeCode VARCHAR(50),
		EntityTypeSubCode VARCHAR(50),
		EntityID INT,
		DocumentEntityCode VARCHAR(50),
		CreateDateTime DATETIME,
		DisplayOrder INT
		)

	EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
	EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'

	END
--ENDIF
GO
--End table document.DocumentEntity

--Begin table document.FileType
DECLARE @TableName VARCHAR(250) = 'document.FileType'

EXEC utility.DropObject @TableName

CREATE TABLE document.FileType
	(
	FileTypeID INT IDENTITY(1,1) NOT NULL,
	Extension VARCHAR(10),
	MimeType VARCHAR(100)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FileTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_FileType', 'Extension,MimeType'
GO
--End table document.FileType

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table syslog.ApplicationErrorLog
DECLARE @TableName VARCHAR(250) = 'syslog.ApplicationErrorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.ApplicationErrorLog
	(
	ApplicationErrorLogID INT IDENTITY(1,1) NOT NULL,
	ErrorDateTime	DATETIME,
	Error	VARCHAR(MAX),
	ErrorRCData	VARCHAR(MAX),
	ErrorSession VARCHAR(MAX),
	ErrorCGIData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ErrorDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationErrorLogID'
GO
--End table syslog.ApplicationErrorLog

--Begin table syslog.BuildLog
DECLARE @TableName VARCHAR(250) = 'syslog.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table syslog.BuildLog

--Begin table utility.Conversion
DECLARE @TableName VARCHAR(250) = 'utility.Conversion'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE utility.Conversion
		(
		ConversionID INT IDENTITY(1,1) NOT NULL,
		EntityTypeCode VARCHAR(50),
		EntityTypeSubCode VARCHAR(50),
		EntityID INT,
		IsConverted BIT
		)

	EXEC utility.SetDefaultConstraint @TableName, 'IsConverted', 'BIT', '0'
	EXEC utility.SetPrimaryKeyClustered @TableName, 'ConversionID'

	END
--ENDIF
GO
--End table utility.Conversion
