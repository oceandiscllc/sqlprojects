USE ManicReaders
GO

--Begin table author.Author
DECLARE @TableName VARCHAR(250) = 'author.Author'

EXEC utility.DropObject @TableName

CREATE TABLE author.Author
	(
	AuthorID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	EmailAddress VARCHAR(320),
	Biography VARCHAR(MAX), 
	IsAdultContent BIT,
	Notes VARCHAR(MAX), 
	Links VARCHAR(MAX), 
	AmazonURL VARCHAR(1000),
	AuthorURL VARCHAR(1000),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	IsSpotlight BIT,
	StatusID INT,
	CanChangeAuthorURL BIT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT,
	MembershipStartDate DATE,
	MembershipEndDate DATE,
	MembershipTerm INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanChangeAuthorURL', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAdultContent', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSpotlight', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipTerm', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuthorID'

EXEC utility.DropFullTextIndex @TableName

CREATE FULLTEXT INDEX ON author.Author
	(
	Biography LANGUAGE ENGLISH
	) KEY INDEX PK_Author ON TextSearchCatalog
GO
--End table author.Author

--Begin table author.AuthorContest
DECLARE @TableName VARCHAR(250) = 'author.AuthorContest'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorContest
	(
	AuthorContestID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	ContestURL VARCHAR(500),
	Title VARCHAR(250),
	Description VARCHAR(MAX),
	IsActive BIT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorContestID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorContest', 'AuthorID,CreateDateTime DESC'
GO
--End table author.AuthorContest

--Begin table author.AuthorEmbeddedMedia
DECLARE @TableName VARCHAR(250) = 'author.AuthorEmbeddedMedia'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'author.EmbeddedMedia'

CREATE TABLE author.AuthorEmbeddedMedia
	(
	AuthorEmbeddedMediaID INT IDENTITY(1,1) NOT NULL,
	AuthorID INT,
	EmbeddedMedia VARCHAR(MAX),
	EmbeddedMediaTypeCode VARCHAR(50),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorEmbeddedMediaID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmbeddedMedia', 'AuthorID,EmbeddedMediaTypeCode,DisplayOrder,AuthorEmbeddedMediaID'
GO
--End table author.AuthorEmbeddedMedia

--Begin table author.AuthorGenre
DECLARE @TableName VARCHAR(250) = 'author.AuthorGenre'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorGenre
	(
	AuthorGenreID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorGenre', 'AuthorID,GenreID'
GO
--End table author.AuthorGenre

--Begin table author.AuthorNews
DECLARE @TableName VARCHAR(250) = 'author.AuthorNews'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorNews
	(
	AuthorNewsID INT IDENTITY(1, 1) NOT NULL,
	AuthorNewsCategoryID INT,
	AuthorID INT,
	Title VARCHAR(250),
	Description VARCHAR(MAX),
	IsActive BIT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AuthorNewsCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorNewsID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorNews', 'AuthorID,CreateDateTime DESC'
GO
--End table author.AuthorNews

--Begin table author.AuthorNote
DECLARE @TableName VARCHAR(250) = 'author.AuthorNote'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorNote
	(
	AuthorNoteID INT IDENTITY(0, 1) NOT NULL,
	AuthorID INT,
	Notes VARCHAR(MAX),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorNoteID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorNote', 'AuthorID, CreateDateTime DESC'
GO
--End table author.AuthorNote

--Begin table author.AuthorRoyalty
DECLARE @TableName VARCHAR(250) = 'author.AuthorRoyalty'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorRoyalty
	(
	AuthorRoyaltyID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	BookID INT,
	StartDate DATE,
	EndDate DATE,
	VendorID INT,
	ListPrice NUMERIC(18,2),
	RoyaltyAmount NUMERIC(18,2),
	QuantitySold INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantitySold', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ListPrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoyaltyAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VendorID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorRoyaltyID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorRoyalty', 'AuthorID, BookID, StartDate DESC, EndDate DESC'
GO
--End table author.AuthorRoyalty

--Begin table book.Book
DECLARE @TableName VARCHAR(250) = 'book.Book'

EXEC utility.DropObject @TableName

CREATE TABLE book.Book
	(
	BookID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(250), 
	PublishingCompanyID INT, 
	CoverArtists VARCHAR(250), 
	ReleaseDate DATE, 
	Summary VARCHAR(MAX), 
	Excerpt VARCHAR(MAX),
	PurchaseURL VARCHAR(1000),
	IsFreeRead BIT, 
	StatusID INT,
	Advance NUMERIC(18,2),
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFreeRead', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishingCompanyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BookID'

CREATE FULLTEXT INDEX ON book.Book
	(
	Excerpt LANGUAGE ENGLISH,
	Summary LANGUAGE ENGLISH
	) KEY INDEX PK_Book ON TextSearchCatalog
GO
--End table book.Book

--Begin table book.BookAuthor
DECLARE @TableName VARCHAR(250) = 'book.BookAuthor'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookAuthor
	(
	BookAuthorID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	AuthorID INT,
	IsForRoyaltyTracker BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsForRoyaltyTracker', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookAuthorID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookAuthor', 'BookID,AuthorID'
GO
--End table book.BookAuthor

--Begin table book.BookFormat
DECLARE @TableName VARCHAR(250) = 'book.BookFormat'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookFormat
	(
	BookFormatID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	FormatID INT,
	ISBN VARCHAR(250),
	ListPrice VARCHAR(250),
	BookLength VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FormatID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookFormatID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookFormat', 'BookID,FormatID'
GO
--End table book.BookFormat

--Begin table book.BookGenre
DECLARE @TableName VARCHAR(250) = 'book.BookGenre'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookGenre
	(
	BookGenreID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookGenre', 'BookID,GenreID'
GO
--End table book.BookGenre

--Begin table book.BookPersonSubscription
DECLARE @TableName VARCHAR(250) = 'book.BookPersonSubscription'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookPersonSubscription
	(
	BookPersonSubscriptionID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	PersonID INT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookPersonSubscriptionID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookPersonSubscription', 'BookID,PersonID'
GO
--End table book.BookPersonSubscription

--Begin table company.PublishingCompany
DECLARE @TableName VARCHAR(250) = 'company.PublishingCompany'

EXEC utility.DropObject @TableName

CREATE TABLE company.PublishingCompany
	(
	PublishingCompanyID INT IDENTITY(1, 1) NOT NULL,
	PublishingCompanyName VARCHAR(250),
	EmailAddress VARCHAR(320),

	CreateDateTime DATETIME,
	CreatePersonID INT,
	Description VARCHAR(MAX),
	Links VARCHAR(MAX),
	AmazonURL VARCHAR(1000),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	StatusID INT,
	SubmissionGuidelines VARCHAR(MAX),
	UpdateDateTime DATETIME,
	UpdatePersonID INT,

	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishingCompanyID'

CREATE FULLTEXT INDEX ON company.PublishingCompany
	(
	Description LANGUAGE ENGLISH
	) KEY INDEX PK_PublishingCompany ON TextSearchCatalog
GO
--End table company.PublishingCompany

--Begin table company.ReviewingCompany
DECLARE @TableName VARCHAR(250) = 'company.ReviewingCompany'

EXEC utility.DropObject @TableName

CREATE TABLE company.ReviewingCompany
	(
	ReviewingCompanyID INT IDENTITY(1, 1) NOT NULL,
	ReviewingCompanyName VARCHAR(250),
	EmailAddress VARCHAR(320),

	CreateDateTime DATETIME,
	CreatePersonID INT,
	Links VARCHAR(MAX),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	LogoURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	StatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT,

	Description VARCHAR(MAX),
	MinimumReviewRatingID INT,
	ReviewPeriodAudio INT,
	ReviewPeriodEBook INT,
	ReviewPeriodEBookDelayed INT,
	ReviewPeriodPrint INT,

	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'MinimumReviewRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewingCompanyID'

CREATE FULLTEXT INDEX ON company.ReviewingCompany
	(
	Description LANGUAGE ENGLISH
	) KEY INDEX PK_ReviewingCompany ON TextSearchCatalog
GO
--End table company.ReviewingCompany

--Begin table company.ReviewingCompanyGenre
DECLARE @TableName VARCHAR(250) = 'company.ReviewingCompanyGenre'

EXEC utility.DropObject @TableName

CREATE TABLE company.ReviewingCompanyGenre
	(
	ReviewingCompanyGenreID INT IDENTITY(1, 1) NOT NULL,
	ReviewingCompanyID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ReviewingCompanyGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_ReviewingCompanyGenre', 'ReviewingCompanyID,GenreID'
GO
--End table company.ReviewingCompanyGenre

--Begin table dropdown.AuthorNewsCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.AuthorNewsCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AuthorNewsCategory
	(
	AuthorNewsCategoryID INT IDENTITY(0,1) NOT NULL,
	AuthorNewsCategoryCode VARCHAR(50),
	AuthorNewsCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuthorNewsCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AuthorNewsCategory', 'DisplayOrder,AuthorNewsCategoryName', 'AuthorNewsCategoryID'
GO
--End table dropdown.AuthorNewsCategory

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	ISOCurrencyCode CHAR(3),
	CountryName NVARCHAR(250),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_Country', 'DisplayOrder,CountryName'
GO
--End table dropdown.Country

--Begin table dropdown.Format
DECLARE @TableName VARCHAR(250) = 'dropdown.Format'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Format
	(
	FormatID INT IDENTITY(0,1) NOT NULL,
	FormatCode VARCHAR(50),
	FormatName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FormatID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Format', 'DisplayOrder,FormatName', 'FormatID'
GO
--End table dropdown.Format

--Begin table dropdown.Genre
DECLARE @TableName VARCHAR(250) = 'dropdown.Genre'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Genre
	(
	GenreID INT IDENTITY(0,1) NOT NULL,
	GenreCode VARCHAR(50),
	GenreName VARCHAR(50),
	HasHeatLevel BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasHeatLevel', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'GenreID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Genre', 'DisplayOrder,GenreName', 'GenreID'
GO
--End table dropdown.Genre

--Begin table dropdown.HeatLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.HeatLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.HeatLevel
	(
	HeatLevelID INT IDENTITY(0,1) NOT NULL,
	HeatLevelCode VARCHAR(50),
	HeatLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'HeatLevelID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_HeatLevel', 'DisplayOrder,HeatLevelName', 'HeatLevelID'
GO
--End table dropdown.HeatLevel

--Begin table dropdown.LinkType
DECLARE @TableName VARCHAR(250) = 'dropdown.LinkType'

EXEC utility.DropObject @TableName
GO
--End table dropdown.LinkType

--Begin table dropdown.MembershipType
DECLARE @TableName VARCHAR(250) = 'dropdown.MembershipType'

EXEC utility.DropObject 'dropdown.MembershipType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MembershipType
	(
	MembershipTypeID INT IDENTITY(0,1) NOT NULL,
	MembershipTypeName VARCHAR(50),
	MembershipTerm INT,
	MembershipAmount NUMERIC(18,2),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipTerm', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MembershipTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MembershipType', 'DisplayOrder,MembershipTypeName', 'MembershipTypeID'
GO
--End table dropdown.MembershipType

--Begin table dropdown.PublishingCompanyRole
DECLARE @TableName VARCHAR(250) = 'dropdown.PublishingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PublishingCompanyRole
	(
	PublishingCompanyRoleID INT IDENTITY(0,1) NOT NULL,
	PublishingCompanyRoleCode VARCHAR(50),
	PublishingCompanyRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishingCompanyRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PublishingCompanyRole', 'DisplayOrder,PublishingCompanyRoleName', 'PublishingCompanyRoleID'
GO
--End table dropdown.PublishingCompanyRole

--Begin table dropdown.ReviewingCompanyRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ReviewingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ReviewingCompanyRole
	(
	ReviewingCompanyRoleID INT IDENTITY(0,1) NOT NULL,
	ReviewingCompanyRoleCode VARCHAR(50),
	ReviewingCompanyRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewingCompanyRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewingCompanyRole', 'DisplayOrder,ReviewingCompanyRoleName', 'ReviewingCompanyRoleID'
GO
--End table dropdown.ReviewingCompanyRole

--Begin table dropdown.ReviewRating
DECLARE @TableName VARCHAR(250) = 'dropdown.ReviewRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ReviewRating
	(
	ReviewRatingID INT IDENTITY(0,1) NOT NULL,
	ReviewRatingCode VARCHAR(50),
	ReviewRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewRating', 'DisplayOrder,ReviewRatingName', 'ReviewRatingID'
GO
--End table dropdown.ReviewRating

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleCode VARCHAR(50),
	RoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role

--Begin table dropdown.RolePermissionableLineage
DECLARE @TableName VARCHAR(250) = 'dropdown.RolePermissionableLineage'
GO
--End table dropdown.RolePermissionableLineage

--Begin table dropdown.SiteAdLocation
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdLocation'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdLocation
	(
	SiteAdLocationID INT IDENTITY(0,1) NOT NULL,
	SiteAdLocationCode VARCHAR(50),
	SiteAdLocationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdLocationID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdLocation', 'DisplayOrder,SiteAdLocationName', 'SiteAdLocationID'
GO
--End table dropdown.SiteAdLocation

--Begin table dropdown.SiteAdStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdStatus
	(
	SiteAdStatusID INT IDENTITY(0,1) NOT NULL,
	SiteAdStatusCode VARCHAR(50),
	SiteAdStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdStatus', 'DisplayOrder,SiteAdStatusName', 'SiteAdStatusID'
GO
--End table dropdown.SiteAdStatus

--Begin table dropdown.SiteAdType
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdType
	(
	SiteAdTypeID INT IDENTITY(0,1) NOT NULL,
	SiteAdTypeCode VARCHAR(50),
	SiteAdTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdType', 'DisplayOrder,SiteAdTypeName', 'SiteAdTypeID'
GO
--End table dropdown.SiteAdType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Status', 'DisplayOrder,StatusName', 'StatusID'
GO
--End table dropdown.Status

--Begin table dropdown.Vendor
DECLARE @TableName VARCHAR(250) = 'dropdown.Vendor'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Vendor
	(
	VendorID INT IDENTITY(0,1) NOT NULL,
	VendorCode VARCHAR(50),
	VendorName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VendorID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Vendor', 'DisplayOrder,VendorName', 'VendorID'
GO
--End table dropdown.Vendor

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	--Begin standard fields
	PersonID INT IDENTITY(0, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsEmailAddressVerified BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	CreateDateTime DATETIME,

	--Begin Address fields
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2),

	--Begin Miscellaneous fields
	PasswordSecurityQuestion VARCHAR(250),
	PasswordSecurityQuestionAnswer VARCHAR(250),
	IsNewsLetterSubscriber BIT,
	IsUpdateSubscriber BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsEmailAddressVerified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNewsLetterSubscriber', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUpdateSubscriber', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonPublishingCompanyRole
DECLARE @TableName VARCHAR(250) = 'person.PersonPublishingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonPublishingCompanyRole
	(
	PersonPublishingCompanyRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	PublishingCompanyID INT,
	PublishingCompanyRoleID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishingCompanyRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPublishingCompanyRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPublishingCompanyRole', 'PersonID,PublishingCompanyID,PublishingCompanyRoleID'
GO
--End table person.PersonPublishingCompanyRole

--Begin table person.PersonReviewingCompanyRole
DECLARE @TableName VARCHAR(250) = 'person.PersonReviewingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonReviewingCompanyRole
	(
	PersonReviewingCompanyRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	ReviewingCompanyID INT,
	ReviewingCompanyRoleID INT,
	PenName VARCHAR(100),
	StatusID INT,
	BookLimit INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookLimit', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonReviewingCompanyRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonReviewingCompanyRole', 'PersonID,ReviewingCompanyID,ReviewingCompanyRoleID'
GO
--End table person.PersonReviewingCompanyRole

--Begin table person.PersonRole
DECLARE @TableName VARCHAR(250) = 'person.PersonRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonRole
	(
	PersonRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	RoleID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonRole', 'PersonID,RoleID'
GO
--End table person.PersonRole

--Begin table review.Review
DECLARE @TableName VARCHAR(250) = 'review.Review'

EXEC utility.DropObject @TableName

CREATE TABLE review.Review
	(
	ReviewID INT IDENTITY(1, 1) NOT NULL,
	ReviewRequestID INT,
	ReviewerPersonID INT,
	ReviewingCompanyID INT,
	ReviewRatingID INT,
	StatusID INT,
	ReviewEditorPersonID INT,
	BookCheckoutDateTime DATETIME,
	ReviewSubmitDateTime DATETIME,
	ReviewDateTime DATETIME,
	NotifyDateTime DATETIME,
	ReviewLink VARCHAR(1000),
	Review VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ReviewRequestID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewEditorPersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ReviewID'
EXEC utility.SetIndexClustered @TableName, 'IX_Review', 'ReviewRequestID,ReviewerPersonID'
GO
--End table review.Review

--Begin table review.ReviewRequest
DECLARE @TableName VARCHAR(250) = 'review.ReviewRequest'

EXEC utility.DropObject @TableName

CREATE TABLE review.ReviewRequest
	(
	ReviewRequestID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	RequestPersonID INT,
	ReviewRequestDateTime DATETIME,
	HeatLevelID INT,
	NotifyEmailAddress VARCHAR(320),
	FormatID INT,
	ReviewRequestNotes VARCHAR(MAX),
	IsArchived BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FormatID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HeatLevelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsArchived', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewRequestDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewRequestID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewRequest', 'BookID'
GO
--End table review.ReviewRequest

