USE ManicReaders
GO

--Begin procedure author.GetAuthorByAuthorID
EXEC Utility.DropObject 'author.GetAuthorByAuthorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.07
-- Description:	A stored procedure to return data from the author.Author table
-- ===========================================================================
CREATE PROCEDURE author.GetAuthorByAuthorID

@AuthorID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Author
	SELECT
		getDate() AS CurrentDate,
		core.FormatDate(getDate()) AS CurrentDateFormatted,
		core.FormatExternalURL(A.AmazonURL) AS AmazonURL,
		A.AuthorID,
		A.AuthorURL,
		author.FormatAuthorNameByAuthorID(A.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		A.Biography,
		A.CanChangeAuthorURL,
		A.CreateDateTime,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.CreatePersonID,
		A.EmailAddress,
		core.FormatExternalURL(A.FacebookURL) AS FacebookURL,
		A.FirstName,
		core.FormatExternalURL(A.InstagramURL) AS InstagramURL,
		A.IsAdultContent,
		A.IsSpotlight,
		A.LastName,
		core.FormatExternalURL(A.LinkedInURL) AS LinkedInURL,
		A.Links,
		A.MembershipEndDate,
		CASE WHEN A.MembershipEndDate >= getDate() THEN 1 ELSE 0 END AS IsMember,
		core.FormatDate(A.MembershipEndDate) AS MembershipEndDateFormatted,
		A.MembershipStartDate,
		core.FormatDate(A.MembershipStartDate) AS MembershipStartDateFormatted,
		A.MiddleName,
		A.Notes,
		A.PersonID,
		core.FormatExternalURL(A.TwitterURL) AS TwitterURL,
		A.UpdateDateTime,
		core.FormatDateTime(A.UpdateDateTime) AS UpdateDateTimeFormatted,
		core.FormatExternalURL(A.WebsiteURL) AS WebsiteURL,
		MT.MembershipTerm,
		MT.MembershipTypeName,
		S.StatusID,
		S.StatusName
	FROM author.Author A
		JOIN dropdown.MembershipType MT ON MT.MembershipTerm = A.MembershipTerm
		JOIN dropdown.Status S ON S.StatusID = A.StatusID
			AND A.AuthorID = @AuthorID

	--AuthorBook
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title,
		CASE WHEN A.MembershipEndDate >= getDate() THEN 1 ELSE BA.IsForRoyaltyTracker END AS IsForRoyaltyTracker,		
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.BookAuthor BA
		JOIN author.Author A ON A.AuthorID = BA.AuthorID
		JOIN book.Book B ON B.BookID = BA.BookID
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND BA.AuthorID = @AuthorID

	--AuthorContest
	SELECT
		newID() AS AuthorContestGUID,
		AC.AuthorContestID,
		AC.CreateDateTime,
		core.FormatDate(AC.CreateDateTime) AS CreateDateTimeFormatted,
		AC.ContestURL,
		AC.Description,
		AC.IsActive,
		AC.Title
	FROM author.AuthorContest AC
	WHERE AC.AuthorID = @AuthorID
	ORDER BY AC.CreateDateTime DESC

	--AuthorDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Author'
			AND DE.EntityTypeSubCode = 'BioPhoto'
			AND DE.EntityID = @AuthorID

	--AuthorEmbeddedMedia
	SELECT
		AEM.AuthorEmbeddedMediaID,
		AEM.EmbeddedMedia,
		AEM.EmbeddedMediaTypeCode
	FROM author.AuthorEmbeddedMedia AEM
	WHERE AEM.AuthorID = @AuthorID
	ORDER BY AEM.EmbeddedMediaTypeCode, AEM.DisplayOrder, AEM.AuthorEmbeddedMediaID

	--AuthorGenre
	SELECT
		G.GenreCode,
		G.GenreID,
		G.GenreName
	FROM author.AuthorGenre AG
		JOIN dropdown.Genre G ON G.GenreID = AG.GenreID
			AND AG.AuthorID = @AuthorID
	ORDER BY G.DisplayOrder, G.GenreName

	--AuthorFreeRead
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title
	FROM book.Book B
	WHERE B.IsFreeRead = 1
		AND EXISTS
			(
			SELECT 1
			FROM book.BookAuthor BA
			WHERE BA.BookID = B.BookID
				AND BA.AuthorID = @AuthorID
			)
	ORDER BY B.Title

	--AuthorNews
	SELECT
		newID() AS AuthorNewsGUID,
		AN.AuthorNewsID,
		AN.CreateDateTime,
		core.FormatDate(AN.CreateDateTime) AS CreateDateTimeFormatted,
		AN.Description,
		AN.IsActive,
		AN.Title,
		ANC.AuthorNewsCategoryID,
		ANC.AuthorNewsCategoryName
	FROM author.AuthorNews AN
		JOIN dropdown.AuthorNewsCategory ANC ON ANC.AuthorNewsCategoryID = AN.AuthorNewsCategoryID
			AND AN.AuthorID = @AuthorID
	ORDER BY AN.CreateDateTime DESC

	--AuthorPhotoGallery
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Author'
			AND DE.EntityTypeSubCode = 'PhotoGallery'
			AND DE.EntityID = @AuthorID

	--AuthorPublishingCompany
	SELECT
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		PC.WebsiteURL
	FROM company.PublishingCompany PC
	WHERE EXISTS
		(
		SELECT 1
		FROM book.Book B
			JOIN book.BookAuthor BA ON BA.BookID = B.BookID
				AND BA.AuthorID = @AuthorID
				AND B.PublishingCompanyID = PC.PublishingCompanyID
		)
	ORDER BY PC.PublishingCompanyName

END
GO
--End procedure author.GetAuthorByAuthorID

--Begin procedure author.GetAuthorRoyaltyBooksByPersonID
EXEC Utility.DropObject 'author.GetAuthorRoyaltyBooksByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the book.BookAuthor table
-- ==========================================================================
CREATE PROCEDURE author.GetAuthorRoyaltyBooksByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--AuthorBook
	SELECT
		B.BookID,
		B.Title,
		BA.AuthorID
	FROM book.BookAuthor BA
		JOIN author.Author A ON A.AuthorID = BA.AuthorID
		JOIN book.Book B ON B.BookID = BA.BookID
			AND A.PersonID = @PersonID
			AND 
				(
				A.MembershipEndDate >= getDate()
					OR BA.IsForRoyaltyTracker = 1
				)
	ORDER BY 2

END
GO
--End procedure author.GetAuthorRoyaltyBooksByPersonID

--Begin procedure author.GetAuthorRoyaltyByAuthorRoyaltyID
EXEC Utility.DropObject 'author.GetAuthorRoyaltyByAuthorRoyaltyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the book.BookAuthor table
-- ==========================================================================
CREATE PROCEDURE author.GetAuthorRoyaltyByAuthorRoyaltyID

@AuthorRoyaltyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		AR.AuthorID,
		AR.AuthorRoyaltyID,
		AR.BookID,
		core.FormatDate(AR.EndDate) AS EndDateFormatted,
		AR.QuantitySold,
		AR.ListPrice,
		AR.RoyaltyAmount,
		core.FormatDate(AR.StartDate) AS StartDateFormatted,
		AR.VendorID
	FROM author.AuthorRoyalty AR
	WHERE AR.AuthorRoyaltyID = @AuthorRoyaltyID

END
GO
--End procedure author.GetAuthorRoyaltyByAuthorRoyaltyID

--Begin procedure author.GetBookSalesGraphData
EXEC Utility.DropObject 'author.GetBookSalesGraphData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.04
-- Description:	A stored procedure to return data from the author.AuthorRoyalty table
-- ==================================================================================
CREATE PROCEDURE author.GetBookSalesGraphData

@AuthorID INT = 0,
@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SUM(AR.QuantitySold) AS QuantitySold,
		core.FormatDate(AR.StartDate) + ' - ' + core.FormatDate(AR.EndDate) AS Period
	FROM author.AuthorRoyalty AR
	WHERE AR.AuthorID = @AuthorID
		AND	AR.BookID = @BookID
	GROUP BY AR.StartDate, AR.EndDate
	ORDER BY AR.StartDate, AR.EndDate

	SELECT B.Title
	FROM book.Book B
	WHERE B.BookID = @BookID

END
GO
--End procedure author.GetBookSalesGraphData

--Begin procedure author.GetSpotlightAuthor
EXEC Utility.DropObject 'author.GetSpotlightAuthor'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.06
-- Description:	A stored procedure to get data from the author.author table
-- ========================================================================
CREATE PROCEDURE author.GetSpotlightAuthor

AS
BEGIN
	SET NOCOUNT ON;

	--Author
	SELECT
		A.AuthorID,
		author.FormatAuthorNameByAuthorID(A.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		A.Biography,
		A.EmailAddress,
		core.FormatExternalURL(A.WebsiteURL) AS WebsiteURL
	FROM author.Author A
	WHERE A.IsSpotlight = 1

	--AuthorDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN author.Author A ON A.AuthorID = DE.EntityID
			AND A.IsSpotlight = 1
			AND DE.EntityTypeCode = 'Author'
			AND DE.EntityTypeSubCode = 'BioPhoto'

END
GO
--End procedure author.GetSpotlightAuthor

--Begin procedure author.ValidateAuthorURL
EXEC Utility.DropObject 'author.ValidateAuthorURL'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the author.author table
-- ========================================================================
CREATE PROCEDURE author.ValidateAuthorURL

@AuthorURL VARCHAR(1000),
@AuthorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT A.AuthorID
	FROM author.Author A
	WHERE A.AuthorURL = @AuthorURL
		AND A.AuthorID <> @AuthorID

END
GO
--End procedure author.ValidateAuthorURL

--Begin procedure book.GetBookByBookID
EXEC Utility.DropObject 'book.GetBookByBookID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.31
-- Description:	A stored procedure to return data from the book.Book table
-- =======================================================================
CREATE PROCEDURE book.GetBookByBookID

@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Book
	SELECT
		B.Advance,
		B.BookID,
		B.CoverArtists,
		B.CreateDateTime,
		core.FormatDateTime(B.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(B.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		B.Excerpt,
		B.IsFreeRead,
		core.FormatExternalURL(B.PurchaseURL) AS PurchaseURL,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Summary,
		B.Title,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.Book B
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND B.BookID = @BookID

	--BookAuthor
	SELECT
		author.FormatAuthorNameByAuthorID(BA.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		BA.AuthorID,
		BA.BookAuthorID
	FROM book.BookAuthor BA
	WHERE BA.BookID = @BookID
	ORDER BY 1, 2

	--BookDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Book'
			AND DE.EntityTypeSubCode IN ('FreeRead', 'FrontCover')
			AND DE.EntityID = @BookID

	--BookFormat
	SELECT
		BF.BookFormatID,
		BF.BookLength,
		BF.ISBN,
		BF.ListPrice,
		F.FormatID,
		F.FormatName
	FROM book.BookFormat BF
		JOIN dropdown.Format F ON F.FormatID = BF.FormatID
			AND BF.BookID = @BookID
	ORDER BY F.FormatName, F.FormatID

	--BookGenre
	SELECT
		G.GenreCode,
		G.GenreID,
		G.GenreName
	FROM book.BookGenre BG
		JOIN dropdown.Genre G ON G.GenreID = BG.GenreID
			AND BG.BookID = @BookID
	ORDER BY G.GenreName, G.GenreID

END
GO
--End procedure book.GetBookByBookID

--Begin procedure company.GetPublishingCompanyByPublishingCompanyID
EXEC utility.DropObject 'company.GetPublishingCompanyByPublishingCompanyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the company.PublishingCompany table based on a PublishingCompanyID
-- ========================================================================================================
CREATE PROCEDURE company.GetPublishingCompanyByPublishingCompanyID

@PublishingCompanyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PublishingCompany
	SELECT
		PC.Address1,
		PC.Address2,
		PC.Address3,
		core.FormatExternalURL(PC.AmazonURL) AS AmazonURL,
		PC.CreateDateTime,
		core.FormatDateTime(PC.CreateDateTime) AS CreateDateTimeFormatted,
		PC.CreatePersonID,
		person.FormatPersonNameByPersonID(PC.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		PC.Description,
		PC.EmailAddress,
		PC.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(PC.ISOCountryCode2) AS CountryName,
		core.FormatExternalURL(PC.FacebookURL) AS FacebookURL,
		core.FormatExternalURL(PC.InstagramURL) AS InstagramURL,
		core.FormatExternalURL(PC.LinkedInURL) AS LinkedInURL,
		PC.Links,
		PC.Municipality,
		PC.PostalCode,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		PC.Region,
		PC.SubmissionGuidelines,
		core.FormatExternalURL(PC.TwitterURL) AS TwitterURL,
		PC.UpdateDateTime,
		core.FormatDateTime(PC.UpdateDateTime) AS UpdateDateTimeFormatted,
		PC.UpdatePersonID,
		person.FormatPersonNameByPersonID(PC.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		core.FormatExternalURL(PC.WebsiteURL) AS WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM company.PublishingCompany PC
		JOIN dropdown.Status S ON S.StatusID = PC.StatusID
			AND PC.PublishingCompanyID = @PublishingCompanyID

	--PublishingCompanyAuthor
	SELECT
		A.AuthorID,
		author.FormatAuthorNameByAuthorID(A.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		core.FormatExternalURL(A.WebsiteURL) AS WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM author.Author A
		JOIN dropdown.Status S ON S.StatusID = A.StatusID
			AND EXISTS
				(
				SELECT 1
				FROM book.BookAuthor BA
					JOIN book.Book B ON B.BookID = BA.BookID
						AND BA.AuthorID = A.AuthorID
						AND B.PublishingCompanyID = @PublishingCompanyID
				)
	ORDER BY 2, 1

	--PublishingCompanyBook
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.Book B
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND B.PublishingCompanyID = @PublishingCompanyID
	ORDER BY B.Title, B.BookID

	--PublishingCompanyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PublishingCompany'
			AND DE.EntityTypeSubCode = 'Logo'
			AND DE.EntityID = @PublishingCompanyID

	--PublishingCompanyFreeRead
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title
	FROM book.Book B
	WHERE B.IsFreeRead = 1
		AND B.PublishingCompanyID = @PublishingCompanyID
	ORDER BY B.Title

END
GO
--End procedure company.GetPublishingCompanyByPublishingCompanyID

--Begin procedure company.GetReviewingCompanyByReviewingCompanyID
EXEC utility.DropObject 'company.GetReviewingCompanyByReviewingCompanyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the company.ReviewingCompany table based on a ReviewingCompanyID
-- ========================================================================================================
CREATE PROCEDURE company.GetReviewingCompanyByReviewingCompanyID

@ReviewingCompanyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ReviewingCompany
	SELECT
		RC.Address1,
		RC.Address2,
		RC.Address3,
		RC.CreateDateTime,
		core.FormatDateTime(RC.CreateDateTime) AS CreateDateTimeFormatted,
		RC.CreatePersonID,
		person.FormatPersonNameByPersonID(RC.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		RC.Description,
		RC.EmailAddress,
		core.FormatExternalURL(RC.FacebookURL) AS FacebookURL,
		core.FormatExternalURL(RC.InstagramURL) AS InstagramURL,
		RC.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(RC.ISOCountryCode2) AS CountryName,
		core.FormatExternalURL(RC.LinkedInURL) AS LinkedInURL,
		RC.Links AS Links,
		RC.LogoURL,
		RC.Municipality,
		RC.PostalCode,
		RC.Region,
		RC.ReviewingCompanyID,
		RC.ReviewingCompanyName,
		RC.ReviewPeriodAudio,
		RC.ReviewPeriodEBook,
		RC.ReviewPeriodEBookDelayed,
		RC.ReviewPeriodPrint,
		RC.StatusID,
		core.FormatExternalURL(RC.TwitterURL) AS TwitterURL,
		RC.UpdateDateTime,
		core.FormatDateTime(RC.UpdateDateTime) AS UpdateDateTimeFormatted,
		RC.UpdatePersonID,
		person.FormatPersonNameByPersonID(RC.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		core.FormatExternalURL(RC.WebsiteURL) AS WebsiteURL,
		RR.ReviewRatingID AS MinimumReviewRatingID,
		RR.ReviewRatingName,
		S.StatusID,
		S.StatusName
	FROM company.ReviewingCompany RC
		JOIN dropdown.ReviewRating RR ON RR.ReviewRatingID = RC.MinimumReviewRatingID
		JOIN dropdown.Status S ON S.StatusID = RC.StatusID
			AND RC.ReviewingCompanyID = @ReviewingCompanyID

	--ReviewingCompanyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ReviewingCompany'
			AND DE.EntityTypeSubCode = 'Logo'
			AND DE.EntityID = @ReviewingCompanyID

	--ReviewingCompanyGenre
	IF @ReviewingCompanyID > 0
		BEGIN

		SELECT
			G.GenreCode,
			G.GenreID,
			G.GenreName
		FROM company.ReviewingCompanyGenre RCG
			JOIN dropdown.Genre G ON G.GenreID = RCG.GenreID
				AND RCG.ReviewingCompanyID = @ReviewingCompanyID
		ORDER BY G.DisplayOrder, G.GenreName

		END
	ELSE
		BEGIN

		SELECT
			G.GenreCode,
			G.GenreID,
			G.GenreName
		FROM dropdown.Genre G
		ORDER BY G.DisplayOrder, G.GenreName

		END
	--ENDIF

END
GO
--End procedure company.GetReviewingCompanyByReviewingCompanyID

--Begin procedure dropdown.GetAuthorNewsCategoryData
EXEC Utility.DropObject 'dropdown.GetAuthorNewsCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.AuthorNewsCategory table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetAuthorNewsCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AuthorNewsCategoryID,
		T.AuthorNewsCategoryName
	FROM dropdown.AuthorNewsCategory T
	WHERE (T.AuthorNewsCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AuthorNewsCategoryName, T.AuthorNewsCategoryID

END
GO
--End procedure dropdown.GetAuthorNewsCategoryData

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetFormatData
EXEC Utility.DropObject 'dropdown.GetFormatData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Format table
-- =================================================================================
CREATE PROCEDURE dropdown.GetFormatData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FormatID,
		T.FormatName
	FROM dropdown.Format T
	WHERE (T.FormatID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FormatName, T.FormatID

END
GO
--End procedure dropdown.GetFormatData

--Begin procedure dropdown.GetGenreData
EXEC Utility.DropObject 'dropdown.GetGenreData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Genre table
-- ============================================================================
CREATE PROCEDURE dropdown.GetGenreData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GenreID,
		T.GenreCode,
		T.GenreName,
		T.HasHeatLevel
	FROM dropdown.Genre T
	WHERE (T.GenreID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GenreName, T.GenreID

END
GO
--End procedure dropdown.GetGenreData

--Begin procedure dropdown.GetHeatLevelData
EXEC Utility.DropObject 'dropdown.GetHeatLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.HeatLevel table
-- ================================================================================
CREATE PROCEDURE dropdown.GetHeatLevelData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.HeatLevelID,
		T.HeatLevelCode,
		T.HeatLevelName
	FROM dropdown.HeatLevel T
	WHERE (T.HeatLevelID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.HeatLevelName, T.HeatLevelID

END
GO
--End procedure dropdown.GetHeatLevelData

--Begin procedure dropdown.GetLinkTypeData
EXEC Utility.DropObject 'dropdown.GetLinkTypeData'
GO
--End procedure dropdown.GetLinkTypeData

--Begin procedure dropdown.GetMembershipTypeData
EXEC Utility.DropObject 'dropdown.GetMembershipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.MembershipType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetMembershipTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MembershipTypeID,
		T.MembershipAmount,
		CAST(T.MembershipAmount / T.MembershipTerm AS NUMERIC(18,2)) AS MembershipAmountMonthly,
		T.MembershipTerm,
		T.MembershipTypeName
	FROM dropdown.MembershipType T
	WHERE (T.MembershipTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MembershipTypeName, T.MembershipTypeID

END
GO
--End procedure dropdown.GetMembershipTypeData

--Begin procedure dropdown.GetPublishingCompanyRoleData
EXEC Utility.DropObject 'dropdown.GetPublishingCompanyRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.PublishingCompanyRole table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetPublishingCompanyRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PublishingCompanyRoleID,
		T.PublishingCompanyRoleCode,
		T.PublishingCompanyRoleName
	FROM dropdown.PublishingCompanyRole T
	WHERE (T.PublishingCompanyRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PublishingCompanyRoleName, T.PublishingCompanyRoleID

END
GO
--End procedure dropdown.GetPublishingCompanyRoleData

--Begin procedure dropdown.GetReviewingCompanyRoleData
EXEC Utility.DropObject 'dropdown.GetReviewingCompanyRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ReviewingCompanyRole table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetReviewingCompanyRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ReviewingCompanyRoleID,
		T.ReviewingCompanyRoleCode,
		T.ReviewingCompanyRoleName
	FROM dropdown.ReviewingCompanyRole T
	WHERE (T.ReviewingCompanyRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ReviewingCompanyRoleName, T.ReviewingCompanyRoleID

END
GO
--End procedure dropdown.GetReviewingCompanyRoleData

--Begin procedure dropdown.GetReviewRatingData
EXEC Utility.DropObject 'dropdown.GetReviewRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ReviewRating table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetReviewRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ReviewRatingID,
		T.ReviewRatingCode,
		T.ReviewRatingName
	FROM dropdown.ReviewRating T
	WHERE (T.ReviewRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ReviewRatingName, T.ReviewRatingID

END
GO
--End procedure dropdown.GetReviewRatingData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ============================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSiteAdLocationData
EXEC Utility.DropObject 'dropdown.GetSiteAdLocationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdLocation table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdLocationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdLocationID,
		T.SiteAdLocationName
	FROM dropdown.SiteAdLocation T
	WHERE (T.SiteAdLocationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdLocationName, T.SiteAdLocationID

END
GO
--End procedure dropdown.GetSiteAdLocationData

--Begin procedure dropdown.GetSiteAdStatusData
EXEC Utility.DropObject 'dropdown.GetSiteAdStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdStatusID,
		T.SiteAdStatusName
	FROM dropdown.SiteAdStatus T
	WHERE (T.SiteAdStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdStatusName, T.SiteAdStatusID

END
GO
--End procedure dropdown.GetSiteAdStatusData

--Begin procedure dropdown.GetSiteAdTypeData
EXEC Utility.DropObject 'dropdown.GetSiteAdTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdTypeID,
		T.SiteAdTypeName
	FROM dropdown.SiteAdType T
	WHERE (T.SiteAdTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdTypeName, T.SiteAdTypeID

END
GO
--End procedure dropdown.GetSiteAdTypeData

--Begin procedure dropdown.GetStatusData
EXEC Utility.DropObject 'dropdown.GetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Status table
-- =============================================================================
CREATE PROCEDURE dropdown.GetStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE (T.StatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure dropdown.GetVendorData
EXEC Utility.DropObject 'dropdown.GetVendorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Vendor table
-- =============================================================================
CREATE PROCEDURE dropdown.GetVendorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VendorID,
		T.VendorCode,
		T.VendorName
	FROM dropdown.Vendor T
	WHERE (T.VendorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VendorName, T.VendorID

END
GO
--End procedure dropdown.GetVendorData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.Address1,
		P.Address2,
		P.Address3,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.EmailAddress,
		P.FirstName,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsEmailAddressVerified,
		P.IsNewsLetterSubscriber,
		P.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.ISOCountryCode2) AS CountryName,
		P.IsSuperAdministrator,
		P.IsUpdateSubscriber,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.Municipality,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PasswordSecurityQuestion,
		P.PasswordSecurityQuestionAnswer,
		P.PersonID,
		P.PostalCode,
		P.Region,
		P.Suffix,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	--PersonRole
	SELECT
		R.RoleID,
		R.RoleCode,
		R.RoleName
	FROM dropdown.Role R
		JOIN person.PersonRole PR ON PR.RoleID = R.RoleID
			AND PR.PersonID = @PersonID
	ORDER BY 3, 2, 1

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure review.GetReviewRequestByReviewRequestID
EXEC Utility.DropObject 'review.GetReviewRequestByReviewRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.02
-- Description:	A stored procedure to return data from the review.ReviewRequest table
-- ==================================================================================
CREATE PROCEDURE review.GetReviewRequestByReviewRequestID

@ReviewRequestID INT = 0,
@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @ReviewRequestID > 0
		SELECT @BookID = RR.BookID FROM review.ReviewRequest RR WHERE RR.ReviewRequestID = @ReviewRequestID
	--ENDIF

	--ReviewRequest
	SELECT
		RR.BookID,
		RR.IsArchived,
		RR.NotifyEmailAddress,
		RR.RequestPersonID,
		RR.ReviewRequestDateTime,
		RR.ReviewRequestID,
		RR.ReviewRequestNotes,
		F.FormatID,
		F.FormatName,
		HL.HeatLevelID,
		HL.HeatLevelName
	FROM review.ReviewRequest RR
		JOIN dropdown.Format F ON F.FormatID = RR.FormatID
		JOIN dropdown.HeatLevel HL ON HL.HeatLevelID = RR.HeatLevelID
			AND RR.ReviewRequestID = @ReviewRequestID

	--ReviewRequestBook
	SELECT 
		B.BookID, 
		B.Title,

		CASE
			WHEN EXISTS (SELECT 1 FROM book.BookGenre BG JOIN dropdown.Genre G ON G.GenreID = BG.GenreID AND BG.BookID = B.BookID AND G.HasHeatLevel = 1)
			THEN 1
			ELSE 0
		END AS HasHeatLevel

	FROM book.Book B
	WHERE B.BookID = @BookID

	--ReviewRequestDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ReviewRequest'
			AND DE.EntityID = @ReviewRequestID

	--ReviewRequestReviewingCompany
	SELECT
		RC.ReviewingCompanyID,
		RC.ReviewingCompanyName,

		CASE
			WHEN EXISTS (SELECT 1 FROM review.Review R WHERE R.ReviewingCompanyID = RC.ReviewingCompanyID AND R.ReviewRequestID = @ReviewRequestID)
			THEN 1
			ELSE 0
		END AS IsSelected

	FROM company.ReviewingCompany RC
	WHERE EXISTS
		(
		SELECT 1
		FROM company.ReviewingCompanyGenre RCG
			JOIN book.BookGenre BG ON BG.GenreID = RCG.GenreID
				AND BG.BookID = @BookID
				AND RCG.ReviewingCompanyID = RC.ReviewingCompanyID

		UNION

		SELECT 1
		FROM review.Review R
		WHERE R.ReviewRequestID = @ReviewRequestID
			AND R.ReviewingCompanyID = RC.ReviewingCompanyID
		)
	ORDER BY RC.ReviewingCompanyName, RC.ReviewingCompanyID

END
GO
--End procedure review.GetReviewRequestByReviewRequestID	
