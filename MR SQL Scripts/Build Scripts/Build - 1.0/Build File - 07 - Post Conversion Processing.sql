USE ManicReaders
GO

UPDATE P
SET P.Password = person.HashPassword(P.Password, P.PasswordSalt)
FROM person.Person P
WHERE P.Password IS NOT NULL
GO
