USE ManicReaders
GO

--Begin function author.FormatAuthorNameByAuthorID
EXEC utility.DropObject 'author.FormatAuthorNameByAuthorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.31
-- Description:	A function to return an author name in a specified format
-- ======================================================================

CREATE FUNCTION author.FormatAuthorNameByAuthorID
(
@AuthorID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cMiddleName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(A.FirstName, ''),
		@cLastName = ISNULL(A.LastName, ''),
		@cMiddleName = ISNULL(A.MiddleName, '')
	FROM author.Author A
	WHERE A.AuthorID = @AuthorID
	
	IF @Format IN ('FirstLast', 'FirstMiddleLast')
		BEGIN
			
		SET @cRetVal = @cFirstName

		IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
			SET @cRetVal += @cMiddleName
		--ENDIF

		SET @cRetVal += ' ' + @cLastName
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE '%Middle' AND LEN(RTRIM(@cMiddleName)) > 0
			SET @cRetVal = @cRetVal + @cMiddleName
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function author.FormatAuthorNameByAuthorID

--Begin function book.FormatBookAuthorNamesByBookID
EXEC utility.DropObject 'book.FormatBookAuthorNamesByBookID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.17
-- Description:	A function to return a list of author names for a book
-- ===================================================================

CREATE FUNCTION book.FormatBookAuthorNamesByBookID
(
@BookID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @cRetVal VARCHAR(MAX)

SELECT @cRetVal = STUFF(
	(	
	SELECT 
		'; ' 
			+ LTRIM(RTRIM(A.LastName)) 
			+ CASE WHEN A.FirstName IS NOT NULL OR A.MiddleName IS NOT NULL THEN ', ' ELSE '' END 
			+ LTRIM(RTRIM(
				ISNULL(LTRIM(RTRIM(A.FirstName)), '') + ' ' + ISNULL(LTRIM(RTRIM(A.MiddleName)), '')
				))
	FROM author.Author A
		JOIN book.BookAuthor BA ON BA.AuthorID = A.AuthorID
			AND BA.BookID = @BookID
	ORDER BY 1
	FOR XML PATH('')
	), 1, 1, '')
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function book.FormatBookAuthorNamesByBookID

--Begin function book.GetGenreNamesByBookID
EXEC utility.DropObject 'book.GetGenreNamesByBookID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.17
-- Description:	A function to return a list of author names for a book
-- ===================================================================

CREATE FUNCTION book.GetGenreNamesByBookID
(
@BookID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(MAX)

	SELECT @cRetVal = STUFF((
		SELECT ', ' + G.GenreName 
		FROM book.BookGenre BG
		JOIN dropdown.Genre G ON G.GenreID = BG.GenreID
			AND BG.BookID = @BookID
		ORDER BY G.GenreName
		FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'), 1, 2, '')
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function book.GetGenreNamesByBookID
