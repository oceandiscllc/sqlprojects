USE ManicReaders
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- ==================================================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- ==================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure core.GetSiteAdBySiteAdID
EXEC Utility.DropObject 'core.GetSiteAdBySiteAdID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SiteAd table
-- ============================================================================
CREATE PROCEDURE core.GetSiteAdBySiteAdID

@SiteAdID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		(SELECT B.Title FROM book.Book B WHERE B.BookID = SA.BookID) AS Title,
		SA.BookID,
		SA.EmailAddress, 
		SA.EndDate, 
		core.FormatDate(SA.EndDate) AS EndDateFormatted,
		SA.InvoiceDetails,
		SA.Notes, 
		SA.SiteAdID, 
		SA.StartDate, 
		core.FormatDate(SA.StartDate) AS StartDateFormatted,
		SAL.SiteAdLocationID,
		SAL.SiteAdLocationName,
		SAS.SiteAdStatusID,
		SAS.SiteAdStatusName,
		SAT.SiteAdTypeID,
		SAT.SiteAdTypeName
	FROM core.SiteAd SA
		JOIN dropdown.SiteAdLocation SAL ON SAL.SiteAdLocationID = SA.SiteAdLocationID
		JOIN dropdown.SiteAdStatus SAS ON SAS.SiteAdStatusID = SA.SiteAdStatusID
		JOIN dropdown.SiteAdType SAT ON SAT.SiteAdTypeID = SA.SiteAdTypeID
			AND SA.SiteAdID = @SiteAdID
	
END
GO
--End procedure core.GetSiteAdBySiteAdID

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin core.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentTitle, 
		D.PhysicalFileSize,
		D.ThumbnailData, 
		D.ThumbnailSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure document.GetFileTypeData
EXEC utility.DropObject 'document.GetFileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.08
-- Description:	A stored procedure to get data from the document.FileType table
-- ============================================================================
CREATE PROCEDURE document.GetFileTypeData

@MimeType VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		FT.Extension,
		LEFT(FT.MimeType, CHARINDEX('/', FT.MimeType) - 1) AS ContentType,
		REVERSE(LEFT(REVERSE(FT.MimeType), CHARINDEX('/', REVERSE(FT.MimeType)) - 1)) AS ContentSubType
	FROM document.FileType FT
	WHERE FT.MimeType = @MimeType

END
GO
--End procedure document.GetFileTypeData

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'core.processGeneralFileUploads'
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentGUID VARCHAR(50) = NULL,
@DocumentID INT = 0,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @DocumentGUID IS NOT NULL
		BEGIN

		SELECT @DocumentID = D.DocumentID
		FROM document.Document D
		WHERE D.DocumentGUID = @DocumentGUID

		END
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE	DE.EntityTypeCode = @EntityTypeCode
		AND (DE.EntityTypeSubCode = @EntityTypeSubCode OR (@EntityTypeSubCode IS NULL AND DE.EntityTypeSubCode IS NULL))
		AND DE.EntityID = @EntityID 
		AND ((@DocumentGUID IS NULL AND @DocumentID = 0) OR DE.DocumentID = @DocumentID)

	IF NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.DocumentID = @DocumentID)
		BEGIN

		DELETE D
		FROM document.Document D
		WHERE D.DocumentID = @DocumentID

		END
	--ENDIF

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure document.SaveEntityDocuments
EXEC Utility.DropObject 'document.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure document.SaveEntityDocuments

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.21
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogAuthorAction
EXEC utility.DropObject 'eventlog.LogAuthorAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAuthorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Author'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cAuthorContestXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorContestXML = COALESCE(@cAuthorContestXML, '') + D.AuthorContest
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorContest'), ELEMENTS) AS AuthorContest
			FROM author.AuthorContest T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorEmbeddedMediaXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorEmbeddedMediaXML = COALESCE(@cAuthorEmbeddedMediaXML, '') + D.AuthorEmbeddedMedia
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorEmbeddedMedia'), ELEMENTS) AS AuthorEmbeddedMedia
			FROM author.AuthorEmbeddedMedia T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorGenreXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorGenreXML = COALESCE(@cAuthorGenreXML, '') + D.AuthorGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorGenre'), ELEMENTS) AS AuthorGenre
			FROM author.AuthorGenre T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorNewsXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorNewsXML = COALESCE(@cAuthorNewsXML, '') + D.AuthorNews
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorNews'), ELEMENTS) AS AuthorNews
			FROM author.AuthorNews T 
			WHERE T.AuthorID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<AuthorContestData>' + ISNULL(@cAuthorContestXML, '') + '</AuthorContestData>' AS XML),
			CAST('<AuthorEmbeddedMediaData>' + ISNULL(@cAuthorEmbeddedMediaXML, '') + '</AuthorEmbeddedMediaData>' AS XML),
			CAST('<AuthorGenreData>' + ISNULL(@cAuthorGenreXML, '') + '</AuthorGenreData>' AS XML),
			CAST('<AuthorNewsData>' + ISNULL(@cAuthorNewsXML, '') + '</AuthorNewsData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Author'), ELEMENTS
			)
		FROM author.Author T
		WHERE T.AuthorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAuthorAction

--Begin procedure eventlog.LogBookAction
EXEC utility.DropObject 'eventlog.LogBookAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogBookAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Book'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cBookAuthorXML VARCHAR(MAX) = ''
		
		SELECT @cBookAuthorXML = COALESCE(@cBookAuthorXML, '') + D.BookAuthor
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookAuthor'), ELEMENTS) AS BookAuthor
			FROM book.BookAuthor T 
			WHERE T.BookID = @EntityID
			) D

		DECLARE @cBookFormatXML VARCHAR(MAX) = ''
		
		SELECT @cBookFormatXML = COALESCE(@cBookFormatXML, '') + D.BookFormat
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookFormat'), ELEMENTS) AS BookFormat
			FROM book.BookFormat T 
			WHERE T.BookID = @EntityID
			) D

		DECLARE @cBookGenreXML VARCHAR(MAX) = ''
		
		SELECT @cBookGenreXML = COALESCE(@cBookGenreXML, '') + D.BookGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookGenre'), ELEMENTS) AS BookGenre
			FROM book.BookGenre T 
			WHERE T.BookID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<BookAuthorData>' + ISNULL(@cBookAuthorXML, '') + '</BookAuthorData>' AS XML),
			CAST('<BookFormatData>' + ISNULL(@cBookFormatXML, '') + '</BookFormatData>' AS XML),
			CAST('<BookGenreData>' + ISNULL(@cBookGenreXML, '') + '</BookGenreData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Book'), ELEMENTS
			)
		FROM book.Book T
		WHERE T.BookID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogBookAction

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonPermissionableXML VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionableXML = COALESCE(@cPersonPermissionableXML, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonPermissionableData>' + ISNULL(@cPersonPermissionableXML, '') + '</PersonPermissionableData>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogPublishingCompanyAction
EXEC utility.DropObject 'eventlog.LogPublishingCompanyAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPublishingCompanyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PublishingCompany'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('PublishingCompany'), ELEMENTS
			)
		FROM company.PublishingCompany T
		WHERE T.PublishingCompanyID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPublishingCompanyAction

--Begin procedure eventlog.LogReviewAction
EXEC utility.DropObject 'eventlog.LogReviewAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Review'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Review'), ELEMENTS
			)
		FROM review.Review T
		WHERE T.ReviewID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewAction

--Begin procedure eventlog.LogReviewingCompanyAction
EXEC utility.DropObject 'eventlog.LogReviewingCompanyAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewingCompanyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ReviewingCompany'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cReviewingCompanyGenreXML VARCHAR(MAX) = ''
		
		SELECT @cReviewingCompanyGenreXML = COALESCE(@cReviewingCompanyGenreXML, '') + D.ReviewingCompanyGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ReviewingCompanyGenre'), ELEMENTS) AS ReviewingCompanyGenre
			FROM company.ReviewingCompanyGenre T 
			WHERE T.ReviewingCompanyID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<ReviewingCompanyGenreData>' + ISNULL(@cReviewingCompanyGenreXML, '') + '</ReviewingCompanyGenreData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('ReviewingCompany'), ELEMENTS
			)
		FROM company.ReviewingCompany T
		WHERE T.ReviewingCompanyID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewingCompanyAction

--Begin procedure eventlog.LogReviewRequestAction
EXEC utility.DropObject 'eventlog.LogReviewRequestAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewRequestAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ReviewRequest'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('ReviewRequest'), ELEMENTS
			)
		FROM review.ReviewRequest T
		WHERE T.ReviewRequestID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewRequestAction

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the person.Person table based on a Token
-- ============================================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@Token VARCHAR(36)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PersonID,
		P.TokenCreateDateTime,

		CASE
			WHEN DATEDIFF("hour", P.TokenCreateDateTime, getdate()) <= 24
			THEN 0
			ELSE 1
		END AS IsTokenExpired

	FROM person.Person P
	WHERE P.Token = @Token

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsMember BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		UserName VARCHAR(250)
		)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '365') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < getDate()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < getDate()
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName
	FROM person.Person P
	WHERE P.UserName = @UserName
		OR P.EmailAddress = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsMember,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,UserName)
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			CASE WHEN EXISTS (SELECT 1 FROM author.Author A WHERE A.PersonID = @nPersonID AND A.MembershipEndDate >= getDate()) THEN 1 ELSE 0 END,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson
			
	SELECT 
		PPCR.PublishingCompanyID,
		PCR.PublishingCompanyRoleCode
	FROM person.PersonPublishingCompanyRole PPCR
		JOIN dropdown.PublishingCompanyRole PCR ON PCR.PublishingCompanyRoleID = PPCR.PublishingCompanyRoleID
			AND PPCR.PersonID = @nPersonID
	ORDER BY 1, 2
			
	SELECT 
		PRCR.ReviewingCompanyID,
		RCR.ReviewingCompanyRoleCode
	FROM person.PersonReviewingCompanyRole PRCR
		JOIN dropdown.ReviewingCompanyRole RCR ON RCR.ReviewingCompanyRoleID = PRCR.ReviewingCompanyRoleID
			AND PRCR.PersonID = @nPersonID
	ORDER BY 1, 2

	SELECT
		R.RoleCode
	FROM person.PersonRole PR
		JOIN dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @nPersonID
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin
