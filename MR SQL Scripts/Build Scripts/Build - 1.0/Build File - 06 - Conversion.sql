USE ManicReaders
GO

--Begin table dropdown.Genre
TRUNCATE TABLE dropdown.Genre
GO

EXEC utility.InsertIdentityValue 'dropdown.Genre', 'GenreID', 0
GO

INSERT INTO dropdown.Genre
	(GenreName)
SELECT DISTINCT
	G.GenreDescription
FROM ManicReadersConv.manicreaders.tblGenre G
WHERE G.GenreDescription IS NOT NULL
ORDER BY G.GenreDescription
GO

UPDATE G
SET G.HasHeatLevel = 1
FROM dropdown.Genre G
WHERE G.GenreName IN
	(
	'African American',
	'Anthology',
	'BDSM',
	'Chick Lit',
	'Contemporary',
	'Contemporary Romance',
	'Erotic Contemporary Romance',
	'Erotic Fantasy',
	'Erotic Historical Romance',
	'Erotic Multiple Partner',
	'Erotic Paranormal Romance',
	'Erotic Romance',
	'Erotic Sci-Fi ',
	'Erotic Suspense',
	'Erotica',
	'Fantasy',
	'Fantasy Romance',
	'Fiction',
	'Futuristic/Time Travel',
	'GLBT',
	'GLBT Young Adult',
	'GLBT Multiple Partner',
	'Historical Fiction',
	'Historical Romance',
	'Interracial Romance',
	'Literary',
	'Literary Fiction',
	'Menage',
	'Occult',
	'Paranormal',
	'Paranormal Romance',
	'Retro',
	'Romance',
	'Romantic Comedy',
	'Romantic Suspense',
	'Rubenesque',
	'Urban Fantasy',
	'Urban Fantasy Romance',
	'Western Contemporary Romance',
	'Western Historical Romance'
	)
--End table dropdown.Genre

--Begin table dropdown.Vendor
TRUNCATE TABLE dropdown.Vendor
GO

EXEC utility.InsertIdentityValue 'dropdown.Vendor', 'VendorID', 0
GO

SET IDENTITY_INSERT dropdown.Vendor ON
GO

INSERT INTO dropdown.Vendor
	(VendorID, VendorName)
SELECT
	RV.RoyaltyVendorID,
	RV.RoyaltyVendorName
FROM ManicReadersConv.manicreaders.tblRoyaltyVendor RV
ORDER BY RV.RoyaltyVendorName
GO
--End table dropdown.Vendor

--Begin table author.Author
TRUNCATE TABLE author.Author
GO

SET IDENTITY_INSERT author.Author ON
GO

INSERT INTO author.Author
	(AuthorID,AuthorURL,Biography,CanChangeAuthorURL,CreateDateTime,EmailAddress,FirstName,IsAdultContent,IsSpotlight,LastName,Links,MiddleName,Notes,PersonID,StatusID,UpdateDateTime,WebsiteURL)
SELECT
	A.AuthorID,
	core.StripCharacters(ISNULL(core.NullIfEmpty(A.FName), '') + ISNULL(core.NullIfEmpty(A.MName), '') + ISNULL(core.NullIfEmpty(A.LName), ''), '^A-Z') AS AuthorURL,
	A.Bio AS Biography,
	1 AS CanChangeAuthorURL,
	ISNULL(A.CreationDate, getDate()) AS CreateDateTime,
	(SELECT U.Email FROM ManicReadersConv.manicreaders.tblUser U WHERE U.UserID = A.UserID) AS EmailAddress,
	A.FName AS FirstName,
	CASE WHEN A.AdultFlag = 1 THEN A.AdultFlag ELSE 0 END AS IsAdultContent,
	CASE WHEN A.Spotlight = 1 THEN A.Spotlight ELSE 0 END AS IsSpotlight,
	A.LName AS LastName,
	A.Links,
	A.MName AS MiddleName,
	A.Note,

	CASE 
		WHEN A.UserID IS NULL OR NOT EXISTS (SELECT 1 FROM ManicReadersConv.manicreaders.tblUser U WHERE U.UserID = A.UserID)
		THEN 0
		ELSE A.UserID
	END AS PersonID,

	CASE
		WHEN A.Status IN ('Active','Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = A.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL(A.LastUpdateDate, getDate()) AS UpdateDateTime,
	A.Website
FROM ManicReadersConv.manicreaders.tblAuthor A
ORDER BY A.AuthorID
GO

SET IDENTITY_INSERT author.Author OFF
GO

UPDATE A
SET 
	A.MembershipStartDate = M.StartDate,
	A.MembershipEndDate = M.EndDate,
	A.MembershipTerm = 
		CASE 
			WHEN M.Duration IN ('Monthly','Monthyl')
			THEN 1
			WHEN M.Duration IN ('Qarterly','Quarteerly','Quarterly')
			THEN 3
			WHEN M.Duration = '6 Month'
			THEN 6
			ELSE 12
		END

FROM manicreadersconv.manicreaders.tblMembership M
	JOIN author.Author A ON A.PersonID = M.UserID
GO
--End table author.Author

--Begin table author.AuthorContest
TRUNCATE TABLE author.AuthorContest
GO

INSERT INTO author.AuthorContest
	(AuthorID,Title,ContestURL,IsActive,Description,CreateDateTime)
SELECT
	TAC.AuthorID,
	TAC.Title,
	TAC.Link,
	CASE WHEN TAC.Status = 'Active' THEN 1 ELSE 0 END AS IsActive,
	TAC.Body AS AuthorContestDescription, 
	TAC.CreateDate AS CreateDateTime
FROM manicreadersconv.manicreaders.tblauthorcontestannouncement TAC
WHERE core.NullIfEmpty(TAC.Body) IS NOT NULL
GO
--End table author.AuthorContest

--Begin table author.AuthorEmbeddedMedia
TRUNCATE TABLE author.AuthorEmbeddedMedia
GO

INSERT INTO author.AuthorEmbeddedMedia
	(AuthorID, EmbeddedMedia, EmbeddedMediaTypeCode)
SELECT
	A.AuthorID,
	A.PodCast,
	'PodCast'
FROM ManicReadersConv.manicreaders.tblAuthor A
WHERE core.NullIfEmpty(A.PodCast) IS NOT NULL

UNION

SELECT
	A.AuthorID,
	A.Video,
	'Video'
FROM ManicReadersConv.manicreaders.tblAuthor A
WHERE core.NullIfEmpty(A.Video) IS NOT NULL

ORDER BY A.AuthorID
GO
--End table author.AuthorEmbeddedMedia

--Begin table author.AuthorGenre
TRUNCATE TABLE author.AuthorGenre
GO

INSERT INTO author.AuthorGenre
	(AuthorID,GenreID)
SELECT
	TAG.AuthorID,
	(SELECT G.GenreID FROM dropdown.Genre G WHERE G.GenreName = TG.GenreDescription)
FROM ManicReadersConv.manicreaders.tblAuthorGenre TAG
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = TAG.GenreID
ORDER BY TAG.AuthorID, TAG.GenreID
GO
--End table author.AuthorGenre

--Begin table author.AuthorNews
TRUNCATE TABLE author.AuthorNews
GO

INSERT INTO author.AuthorNews
	(AuthorID,AuthorNewsCategoryID,IsActive,Description,CreateDateTime)
SELECT
	TAN.AuthorID,
	ISNULL((SELECT ANC.AuthorNewsCategoryID FROM dropdown.AuthorNewsCategory ANC WHERE ANC.AuthorNewsCategoryName = TAN.Title), 0) AS AuthorNewsCategoryID,
	CASE WHEN TAN.Status = 'Active' THEN 1 ELSE 0 END AS IsActive,
	TAN.Body AS AuthorNewsDescription, 
	TAN.CreateDate AS CreateDateTime
FROM manicreadersconv.manicreaders.tblAuthorNews TAN
WHERE core.NullIfEmpty(TAN.Body) IS NOT NULL
GO
--End table author.AuthorNews

--Begin table author.AuthorNote
INSERT INTO author.AuthorNote
	(AuthorID,Notes)
SELECT
	A.AuthorID,
	core.NullIfEmpty(M.Notes) AS Notes
FROM manicreadersconv.manicreaders.tblMembership M
	JOIN author.Author A ON A.PersonID = M.UserID
		AND core.NullIfEmpty(M.Notes) IS NOT NULL
ORDER BY M.UserID
GO
--End table author.AuthorNote

--Begin table author.AuthorRoyalty
TRUNCATE TABLE author.AuthorRoyalty
GO

INSERT INTO author.AuthorRoyalty
	(AuthorID,BookID,StartDate,EndDate,VendorID,ListPrice,RoyaltyAmount,QuantitySold)
SELECT 
	B.AuthorID,
	B.BookID,
	RP.StartDate,
	RP.EndDate,
	RT.RoyaltyVendorID,
	RT.ListPrice,
	RT.RoyaltiesEarned,
	RT.UnitSsold
FROM manicreadersconv.manicreaders.tblroyaltytransaction RT
	 JOIN manicreadersconv.manicreaders.tblroyaltyperiod RP ON RP.royaltyperiodid = RT.royaltyperiodid
	 JOIN manicreadersconv.manicreaders.tblBook B ON B.BookID = RP.BookID
ORDER BY 1
GO
--End table author.AuthorRoyalty

--Begin table book.Book
TRUNCATE TABLE book.Book
GO

SET IDENTITY_INSERT book.Book ON
GO

INSERT INTO book.Book
	(Advance,BookID,CoverArtists,CreateDateTime,IsFreeRead,Excerpt,PublishingCompanyID,PurchaseURL,ReleaseDate,StatusID,Summary,Title)
SELECT
	ISNULL(B.Advance, 0) AS Advance,
	B.BookID,
	B.CoverArtists,
	ISNULL(B.CreationDate, getDate()) AS CreateDateTime,
	CASE WHEN core.NullIfEmpty(B.FreeRead) IS NULL THEN 0 ELSE 1 END,
	B.Excerpt,
	ISNULL(B.PublisherID, 0) AS PublishingCompanyID,
	B.BuyLink,
	B.ReleaseDate,

	CASE
		WHEN B.Status = 'Active'
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = B.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Deleted')
	END AS StatusID,

	B.Blurb AS Summary,
	LTRIM(core.NullIfEmpty(REPLACE(B.Title, CHAR(9), ''))) AS Title 
FROM ManicReadersConv.manicreaders.tblBook B
ORDER BY B.BookID
GO

SET IDENTITY_INSERT book.Book OFF
GO
--End table book.Book

--Begin table book.BookAuthor
TRUNCATE TABLE book.BookAuthor
GO

INSERT INTO book.BookAuthor
	(AuthorID,BookID)
SELECT
	B.AuthorID,
	B.BookID
FROM ManicReadersConv.manicreaders.tblBook B
ORDER BY B.BookID
GO
--End table book.BookAuthor

--Begin table book.BookFormat
TRUNCATE TABLE book.BookFormat
GO

INSERT INTO book.BookFormat
	(BookID,BookLength,FormatID,ISBN,ListPrice)
SELECT
	BF.BookID,
	B.Pages AS BookLength,
	ISNULL(
		(
		SELECT F.FormatID 
		FROM dropdown.Format F 
		WHERE F.FormatName = 
			CASE 
				WHEN BF.Format IN ('E-Book / Print', 'eBook')
				THEN 'E-Book'
				WHEN BF.Format IN ('EPUB')
				THEN 'E-Pub'
				ELSE BF.Format
			END
		), 0) AS FormatID,
	BF.ISBN,
	BF.ListPrice
FROM ManicReadersConv.manicreaders.tblBookFormat BF
	JOIN ManicReadersConv.manicreaders.tblBook B ON B.BookID = BF.BookID
ORDER BY BF.BookID, BF.BookFormatID
GO
--End table book.BookFormat

--Begin table book.BookGenre
TRUNCATE TABLE book.BookGenre
GO

INSERT INTO book.BookGenre
	(BookID,GenreID)
SELECT
	B.BookID,
	(SELECT G.GenreID FROM dropdown.Genre G WHERE G.GenreName = TG.GenreDescription) AS GenreID
FROM ManicReadersConv.manicreaders.tblBook B
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = B.GenreID
ORDER BY B.BookID
GO
--End table book.BookGenre

--Begin table book.BookPersonSubscription
TRUNCATE TABLE book.BookPersonSubscription
GO

INSERT INTO book.BookPersonSubscription
	(BookID,PersonID)
SELECT
	BCS.BookID,
	BCS.UserID
FROM ManicReadersConv.manicreaders.tblBookCommentSubscription BCS
GO
--End table book.BookPersonSubscription

--Begin table company.PublishingCompany
TRUNCATE TABLE company.PublishingCompany
GO

EXEC utility.InsertIdentityValue 'company.PublishingCompany', 'PublishingCompanyID', 0
GO

SET IDENTITY_INSERT company.PublishingCompany ON
GO

INSERT INTO company.PublishingCompany
	(Address1,Description,EmailAddress,Links,Municipality,PublishingCompanyID,PublishingCompanyName,Region,StatusID,SubmissionGuidelines,UpdateDateTime,WebsiteURL)
SELECT
	core.NullIfEmpty(P.Address) AS Address1,
	core.NullIfEmpty(P.Description) AS Description,
	core.NullIfEmpty(P.Email) AS EmailAddress,
	core.NullIfEmpty(P.Links) AS Links,
	core.NullIfEmpty(P.City) AS Municipality,
	P.PublisherID AS PublishingCompanyID,
	LTRIM(core.NullIfEmpty(REPLACE(P.Name, CHAR(9), ''))) AS PublishingCompanyName, 
	core.NullIfEmpty(P.State) AS Region,

	CASE
		WHEN P.Status IN ('Active', 'Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = P.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	core.NullIfEmpty(P.SubmissionGuidelines) AS SubmissionGuidelines,
	core.NullIfEmpty(P.LastUpdateDate) AS UpdateDateTime,
	P.Website
FROM ManicReadersConv.manicreaders.tblPublisher P
ORDER BY P.PublisherID
GO

SET IDENTITY_INSERT company.PublishingCompany OFF
GO
--End table company.PublishingCompany

--Begin table company.ReviewingCompany
TRUNCATE TABLE company.ReviewingCompany
GO

SET IDENTITY_INSERT company.ReviewingCompany ON
GO

INSERT INTO company.ReviewingCompany
	(Description,EmailAddress,Links,LogoURL,MinimumReviewRatingID,ReviewingCompanyID,ReviewingCompanyName,ReviewPeriodAudio,ReviewPeriodEBook,ReviewPeriodEBookDelayed,ReviewPeriodPrint,StatusID,UpdateDateTime,WebsiteURL)
SELECT
	core.NullIfEmpty(R.Bio) AS Description,
	core.NullIfEmpty(R.Email) AS EmailAddress,
	core.NullIfEmpty(R.Links) AS Links,
	core.NullIfEmpty(R.BragIconLink) AS LogoURL,
	ISNULL((SELECT RR.ReviewRatingID FROM dropdown.ReviewRating RR WHERE CAST(RR.ReviewRatingCode AS NUMERIC(3,1)) = CAST(ISNULL(core.NullIfEmpty(R.LowRating), '0') AS NUMERIC(3,1))), 0) AS MinimumReviewRatingID,
	R.ReviewCompanyID AS ReviewingCompanyID,
	LTRIM(core.NullIfEmpty(REPLACE(R.CompanyName, CHAR(9), ''))) AS ReviewingCompanyName, 
	ISNULL(R.AudioDue, 0) AS ReviewPeriodAudio,
 	ISNULL(R.EBookDue, 0) AS ReviewPeriodEBook,
 	ISNULL(R.EBookDelayedDue, 0) AS ReviewPeriodEBookDelayed,
 	ISNULL(R.PrintDue, 0) AS ReviewPeriodPrint,

	CASE
		WHEN R.Status IN ('Active', 'Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = R.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL(core.NullIfEmpty(R.LastUpdateDate), getDate()) AS UpdateDateTime,
	R.Website
FROM ManicReadersConv.manicreaders.tblReviewCompany R
ORDER BY R.ReviewCompanyID
GO

SET IDENTITY_INSERT company.ReviewingCompany OFF
GO
--End table company.ReviewingCompany

--Begin table company.ReviewingCompanyGenre
TRUNCATE TABLE company.ReviewingCompanyGenre
GO

DECLARE @tTable TABLE (ReviewCompanyID INT, GenreID INT NOT NULL DEFAULT 0)

INSERT INTO @tTable
	(ReviewCompanyID,GenreID)
SELECT
	TRCG.ReviewCompanyID,
	TG.GenreID
FROM ManicReadersConv.manicreaders.tblReviewCompanyGenre TRCG, ManicReadersConv.manicreaders.tblGenre TG

DELETE T
FROM @tTable T
WHERE EXISTS
	(
	SELECT 1
	FROM ManicReadersConv.manicreaders.tblReviewCompanyGenre TRCG
		JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = TRCG.GenreID
			AND TRCG.ReviewCompanyID = T.ReviewCompanyID
			AND TRCG.GenreID = T.GenreID
	)

INSERT INTO company.ReviewingCompanyGenre
	(ReviewingCompanyID,GenreID)
SELECT
	T.ReviewCompanyID,
	G.GenreID
FROM @tTable T
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = T.GenreID
	JOIN dropdown.Genre G ON G.GenreName = TG.GenreDescription
ORDER BY T.ReviewCompanyID, T.GenreID
GO
--End table company.ReviewingCompanyGenre

--Begin table core.SiteAd
TRUNCATE TABLE core.SiteAd
GO

INSERT INTO core.SiteAd
	(SiteAdLocationID,SiteAdStatusID,SiteAdTypeID,BookID,EmailAddress,EndDate,InvoiceDetails,Notes,StartDate)
SELECT
	ISNULL((SELECT SAL.SiteAdLocationID FROM dropdown.SiteAdLocation SAL WHERE SAL.SiteAdLocationName = BA.AdPage), 0) AS SiteAdLocationID,
	ISNULL((SELECT SAS.SiteAdStatusID FROM dropdown.SiteAdStatus SAS WHERE SAS.SiteAdStatusName = BA.Status), 0) AS SiteAdStatusID,
	ISNULL((SELECT SAT.SiteAdTypeID FROM dropdown.SiteAdType SAT WHERE SAT.SiteAdTypeName = BA.AdType), 0) AS SiteAdTypeID,
	BA.BookID,
	BA.ContactEmail AS ContactEmailAddress,
	BA.ExpirationDate AS EndDate,
	BA.InvoiceDetails,
	BA.AdNotes AS Notes,
	BA.PostDate AS StartDate
FROM ManicReadersConv.manicreaders.tblBookAd BA
ORDER BY BA.BookID
GO
--End table core.SiteAd

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

EXEC utility.InsertIdentityValue 'person.Person', 'PersonID', 0
GO

SET IDENTITY_INSERT person.Person ON
GO

INSERT INTO person.Person
	(CreateDateTime,EmailAddress,FirstName,InvalidLoginAttempts,IsAccountLockedOut,IsActive,IsEmailAddressVerified,IsNewsLetterSubscriber,IsSuperAdministrator,IsUpdateSubscriber,LastLoginDateTime,LastName,MiddleName,Municipality,Password,PasswordExpirationDateTime,PasswordSalt,PasswordSecurityQuestion,PasswordSecurityQuestionAnswer,PersonID,Region,UserName)
SELECT
	core.NullIfEmpty(U.CreationDate) AS CreateDateTime,
	core.NullIfEmpty(U.Email) AS EmailAddress,
	core.NullIfEmpty(U.FName) AS FirstName,
	0 AS InvalidLoginAttempts,
	0 AS IsAccountLockedOut,
	1 AS IsActive,
	1 AS IsEmailAddressVerified,
	CASE WHEN U.NewsletterEmail = 1 THEN 1 ELSE 0 END AS IsNewsLetterSubscriber,
	0 AS IsSuperAdministrator,
	CASE WHEN U.UpdateEmail = 1 THEN 1 ELSE 0 END AS IsUpdateSubscriber,
	core.NullIfEmpty(U.LastLoginDate) AS LastLoginDateTime,
	core.NullIfEmpty(U.LName) AS LastName,
	core.NullIfEmpty(U.MName) AS MiddleName,
	core.NullIfEmpty(U.City) AS Municipality,
	core.NullIfEmpty(U.Password),
	core.NullIfEmpty(U.LastPasswordChangedDate) AS PasswordExpirationDateTime,
	newID() AS PasswordSalt,
	core.NullIfEmpty(U.PasswordQuestion) AS PasswordSecurityQuestion,
	core.NullIfEmpty(U.PasswordAnswer) AS PasswordSecurityQuestionAnswer,
	U.UserID AS PersonID,
	core.NullIfEmpty(U.State) AS Region,
	core.NullIfEmpty(U.Username)
FROM ManicReadersConv.manicreaders.tblUser U
ORDER BY U.UserID
GO

SET IDENTITY_INSERT person.Person OFF
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, EmailAddress, LastLoginDateTime, InvalidLoginAttempts, IsAccountLockedOut, IsActive, IsSuperAdministrator, Password, PasswordSalt, PasswordExpirationDateTime) 
VALUES
	('John', 'Lyons', 'Mr.', 'jlyons', 'john.lyons@oceandisc.com', NULL, 0, 0, 1, 0, '138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', '71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'kevin.ross@oceandisc.com', NULL, 0, 0, 1, 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Test', 'ReviewingCompany Admin', 'Mr.', 'ReviewingCompanyTestAdmin', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime))
GO

UPDATE P
SET 
	P.Password = person.HashPassword(P.Password, P.PasswordSalt),
	P.UserName = 
		CASE P.UserName 
			WHEN 'authortest' 
			THEN 'AuthorTest' 
			WHEN 'testpublisher' 
			THEN 'PublishingCompanyTest' 
			WHEN 'TestReviewer555' 
			THEN 'ReviewingCompanyTest' 
			ELSE P.UserName 
		END
FROM person.Person P
WHERE P.Password IS NOT NULL
	AND P.UserName IN ('authortest', 'dannoparker', 'testpublisher', 'TestReviewer555')
GO

UPDATE P1
SET 
	P1.Password = (SELECT P2.Password FROM person.Person P2 WHERE P2.UserName = 'ReviewingCompanyTest'),
	P1.PasswordSalt = (SELECT P2.PasswordSalt FROM person.Person P2 WHERE P2.UserName = 'ReviewingCompanyTest')
FROM person.Person P1
WHERE P1.UserName = 'ReviewingCompanyTestAdmin'
GO
 --End table person.Person

--Begin table person.PersonPublishingCompanyRole
INSERT INTO person.PersonPublishingCompanyRole
	(PersonID,PublishingCompanyID,PublishingCompanyRoleID)
SELECT
	PU.UserID AS PersonID,
	PU.PublisherID,
	(SELECT PR.PublishingCompanyRoleID FROM dropdown.PublishingCompanyRole PR WHERE PR.PublishingCompanyRoleCode = 'Publisher')
FROM ManicReadersConv.manicreaders.tblPublisherUser PU
WHERE EXISTS
	(
	SELECT 1
	FROM person.Person P
	WHERE P.PersonID = PU.UserID
	)
ORDER BY PU.UserID
GO
--End table person.PersonPublishingCompanyRole

--Begin table person.PersonReviewingCompanyRole
INSERT INTO person.PersonReviewingCompanyRole
	(PersonID,ReviewingCompanyID,PenName,StatusID,BookLimit,ReviewingCompanyRoleID)
SELECT
	RCU.UserID AS PersonID,
	RCU.ReviewCompanyID AS ReviewingCompanyID,
	RCU.PenName,
	ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = RCU.Status), 0) AS StatusID,
	ISNULL((
	SELECT RBL.BookLimit 
	FROM ManicReadersConv.manicreaders.tblReviewerBookLimit RBL 
		JOIN ManicReadersConv.manicreaders.tblUser U ON U.UserName = RBL.UserName
			AND RBL.ReviewCompanyID = RCU.ReviewCompanyID 
			AND U.UserID = RCU.UserID
	), 0) AS BookLimit,

	CASE
		WHEN RCU.UserRole IN ('Editor', 'Reviewer')
		THEN (SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = RCU.UserRole)
		ELSE (SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = 'Administrator')
	END AS ReviewingCompanyRoleID

FROM ManicReadersConv.manicreaders.tblReviewCompanyUser RCU
WHERE EXISTS
	(
	SELECT 1
	FROM person.Person P
	WHERE P.PersonID = RCU.UserID
	)
ORDER BY RCU.UserID
GO

INSERT INTO person.PersonReviewingCompanyRole
	(PersonID,ReviewingCompanyID,ReviewingCompanyRoleID)
SELECT
	(SELECT P.PersonID FROM person.Person P WHERE P.UserName = 'ReviewingCompanyTestAdmin'),
	RC.ReviewingCompanyID,
	(SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = 'Administrator')
FROM company.ReviewingCompany RC
WHERE RC.ReviewingCompanyName = 'Manic Readers'
GO
--End table person.PersonReviewingCompanyRole

--Begin table person.PersonRole
INSERT INTO person.PersonRole
	(PersonID,RoleID)
SELECT
	U.UserID AS PersonID,

	CASE
		WHEN U.UserRole IN ('Author')
		THEN (SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'Author')
		ELSE (SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'Reader')
	END AS RoleID

FROM ManicReadersConv.manicreaders.tblUser U
ORDER BY U.UserID
GO
--End table person.PersonRole

--Begin table review.ReviewRequest
TRUNCATE TABLE review.ReviewRequest
GO

SET IDENTITY_INSERT review.ReviewRequest ON
GO

INSERT INTO review.ReviewRequest
	(ReviewRequestID, BookID, RequestPersonID, ReviewRequestDateTime, HeatLevelID, NotifyEmailAddress, FormatID, ReviewRequestNotes)
SELECT
	R.MRReviewID AS ReviewRequestID,
	R.BookID,
	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.RequestedBy), 0) AS RequestPersonID,
	ISNULL(R.RequestedDate, '01/01/2000') AS ReviewRequestDateTime,
	ISNULL((SELECT HL.HeatLevelID FROM dropdown.HeatLevel HL WHERE HL.HeatLevelName = R.HeatLevel), 0) AS HeatLevelID,
	R.NotifyEmail AS NotifyEmailAddress,
	ISNULL((SELECT F.FormatID FROM dropdown.Format F WHERE F.FormatName = R.MRBookFormat), 0) AS FormatID,
	R.ReviewerNotes AS ReviewRequestNotes
FROM ManicReadersConv.manicreaders.tblMRReview R
ORDER BY R.MRReviewID
GO

SET IDENTITY_INSERT review.ReviewRequest OFF
GO
--End table review.ReviewRequest

--Begin table review.Review
TRUNCATE TABLE review.Review
GO

INSERT INTO review.Review
	(ReviewRequestID,ReviewerPersonID,ReviewingCompanyID,ReviewRatingID,StatusID,ReviewEditorPersonID,BookCheckoutDateTime,ReviewSubmitDateTime,ReviewDateTime,NotifyDateTime,ReviewLink,Review)
SELECT
	R.MRReviewID AS ReviewRequestID,
	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.Username), 0) AS ReviewerPersonID,
	ISNULL(R.ReviewCompanyID, 0) AS ReviewingCompanyID,
	ISNULL((SELECT RR.ReviewRatingID FROM dropdown.ReviewRating RR WHERE CAST(RR.ReviewRatingCode AS NUMERIC(3,1)) = CAST(ISNULL(core.NullIfEmpty(R.Rating), '0') AS NUMERIC(3,1))), 0) AS Rating,

	CASE
		WHEN R.Status IN ('Active','Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = R.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.Editor), 0) AS ReviewEditorPersonID,
	ISNULL(R.CheckoutDate, '01/01/2000') AS BookCheckoutDateTime,
	ISNULL(R.SubmitDate, '01/01/2000') AS ReviewSubmitDateTime,
	ISNULL(R.ReviewDate, '01/01/2000') AS ReviewDateTime,
	ISNULL(R.NotifyDate, '01/01/2000') AS NotifyDateTime,
	R.ExternalLink AS ReviewLink,
	R.Review
FROM ManicReadersConv.manicreaders.tblMRReview R
ORDER BY R.MRReviewID
GO
--End table review.Review

