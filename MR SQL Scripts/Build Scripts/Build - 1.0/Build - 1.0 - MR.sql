-- File Name:	Build - 1.0 - MR.sql
-- Build Key:	Build - 1.0 - 2018.08.05 19.41.49

USE ManicReaders
GO

-- ==============================================================================================================================
-- Schemas:
--		author
--		book
--		company
--		core
--		document
--		dropdown
--		eventlog
--		person
--		review
--		syslog
--
-- Tables:
--		author.Author
--		author.AuthorContest
--		author.AuthorEmbeddedMedia
--		author.AuthorGenre
--		author.AuthorNews
--		author.AuthorNote
--		author.AuthorRoyalty
--		book.Book
--		book.BookAuthor
--		book.BookFormat
--		book.BookGenre
--		book.BookPersonSubscription
--		company.PublishingCompany
--		company.ReviewingCompany
--		company.ReviewingCompanyGenre
--		core.Announcement
--		core.EmailTemplate
--		core.EmailTemplateField
--		core.EntityType
--		core.SiteAd
--		core.SystemSetup
--		document.Document
--		document.DocumentEntity
--		document.FileType
--		dropdown.AuthorNewsCategory
--		dropdown.Country
--		dropdown.Format
--		dropdown.Genre
--		dropdown.HeatLevel
--		dropdown.MembershipType
--		dropdown.PublishingCompanyRole
--		dropdown.ReviewingCompanyRole
--		dropdown.ReviewRating
--		dropdown.Role
--		dropdown.SiteAdLocation
--		dropdown.SiteAdStatus
--		dropdown.SiteAdType
--		dropdown.Status
--		dropdown.Vendor
--		eventlog.EventLog
--		person.Person
--		person.PersonPublishingCompanyRole
--		person.PersonReviewingCompanyRole
--		person.PersonRole
--		review.Review
--		review.ReviewRequest
--		syslog.ApplicationErrorLog
--		syslog.BuildLog
--		utility.Conversion
--
-- Triggers:
--		document.TR_Document ON document.Document
--
-- Functions:
--		author.FormatAuthorNameByAuthorID
--		book.FormatBookAuthorNamesByBookID
--		core.FormatDate
--		core.FormatDateTime
--		core.FormatExternalURL
--		core.FormatFullTextSearchString
--		core.FormatTime
--		core.GetEntityTypeNameByEntityTypeCode
--		core.GetEntityTypeNamePluralByEntityTypeCode
--		core.GetSystemSetupValueBySystemSetupKey
--		core.ListToTable
--		core.NullIfEmpty
--		core.StripCharacters
--		core.YesNoFormat
--		document.FormatFileSize
--		dropdown.GetCountryNameByISOCountryCode
--		eventlog.GetDocumentEntityXML
--		person.FormatPersonNameByPersonID
--		person.HashPassword
--		person.HasPermission
--		person.IsSuperAdministrator
--		person.ValidatePassword
--
-- Procedures:
--		author.GetAuthorByAuthorID
--		author.GetAuthorRoyaltyBooksByPersonID
--		author.GetAuthorRoyaltyByAuthorRoyaltyID
--		author.GetBookSalesGraphData
--		author.ValidateAuthorURL
--		book.GetBookByBookID
--		company.GetPublishingCompanyByPublishingCompanyID
--		company.GetReviewingCompanyByReviewingCompanyID
--		core.DeleteAnnouncementByAnnouncementID
--		core.EntityTypeAddUpdate
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		core.GetAnnouncementsByDate
--		core.GetEmailTemplateByEmailTemplateID
--		core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
--		core.GetEmailTemplateFieldsByEntityTypeCode
--		core.GetSiteAdBySiteAdID
--		core.GetSystemSetupDataBySystemSetupID
--		core.GetSystemSetupValuesBySystemSetupKey
--		core.GetSystemSetupValuesBySystemSetupKeyList
--		core.SystemSetupAddUpdate
--		document.GetDocumentByDocumentData
--		document.GetDocumentByDocumentGUID
--		document.GetFileTypeData
--		document.processGeneralFileUploads
--		document.PurgeEntityDocuments
--		document.SaveEntityDocuments
--		dropdown.GetAuthorNewsCategoryData
--		dropdown.GetCountryData
--		dropdown.GetFormatData
--		dropdown.GetGenreData
--		dropdown.GetHeatLevelData
--		dropdown.GetMembershipTypeData
--		dropdown.GetPublishingCompanyRoleData
--		dropdown.GetReviewingCompanyRoleData
--		dropdown.GetReviewRatingData
--		dropdown.GetRoleData
--		dropdown.GetSiteAdLocationData
--		dropdown.GetSiteAdStatusData
--		dropdown.GetSiteAdTypeData
--		dropdown.GetStatusData
--		dropdown.GetVendorData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogAnnouncementAction
--		eventlog.LogAuthorAction
--		eventlog.LogBookAction
--		eventlog.LogEmailTemplateAction
--		eventlog.LogLoginAction
--		eventlog.LogPersonAction
--		eventlog.LogPublishingCompanyAction
--		eventlog.LogReviewAction
--		eventlog.LogReviewingCompanyAction
--		eventlog.LogReviewRequestAction
--		person.GeneratePassword
--		person.GetPersonByPersonID
--		person.GetPersonByPersonToken
--		person.SetAccountLockedOut
--		person.ValidateEmailAddress
--		person.ValidateLogin
--		review.GetReviewRequestByReviewRequestID
--		utility.AddColumn
--		utility.AddSchema
--		utility.DropColumn
--		utility.DropConstraintsAndIndexes
--		utility.DropFullTextIndex
--		utility.DropIndex
--		utility.DropObject
--		utility.InsertIdentityValue
--		utility.SetDefaultConstraint
--		utility.SetIndexClustered
--		utility.SetIndexNonClustered
--		utility.SetPrimaryKeyClustered
--		utility.SetPrimaryKeyNonClustered
-- ==============================================================================================================================

--Begin file Build File - 01 - Prerequisites.sql
USE ManicReaders
GO

--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin dependencies

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject
--End dependencies

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN [' + @ColumnName + ']'
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropConstraintsAndIndexes
EXEC Utility.DropObject 'utility.DropConstraintsAndIndexes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropConstraintsAndIndexes

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + DC.Name + ']' AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + FK.Name + ']' AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor
END
GO
--End  procedure utility.DropConstraintsAndIndexes

--Begin procedure utility.DropFullTextIndex
EXEC utility.DropObject 'utility.DropFullTextIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropFullTextIndex

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT * FROM sys.fulltext_indexes FTI WHERE FTI.object_id = OBJECT_ID(@TableName))
		BEGIN

		SET @cSQL = 'ALTER FULLTEXT INDEX ON ' + @TableName + ' DISABLE'
		EXEC (@cSQL)

		SET @cSQL = 'DROP FULLTEXT INDEX ON ' + @TableName
		EXEC (@cSQL)

		END
	--ENDIF
	
END
GO
--End procedure utility.DropFullTextIndex

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex

@TableName VARCHAR(250),
@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX),
@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered

IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs FTC WHERE FTC.Name = 'TextSearchCatalog')
	CREATE FULLTEXT CATALOG TextSearchCatalog WITH ACCENT_SENSITIVITY = ON AS DEFAULT
--ENDIF
GO

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables

EXEC utility.AddSchema 'author'
EXEC utility.AddSchema 'book'
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'company'
EXEC utility.AddSchema 'document'
EXEC utility.AddSchema 'dropdown'
EXEC utility.AddSchema 'eventlog'
EXEC utility.AddSchema 'person'
EXEC utility.AddSchema 'review'
EXEC utility.AddSchema 'syslog'
GO

EXEC utility.DropObject 'publisher'
GO



--End file Build File - 01 - Prerequisites.sql

--Begin file Build File - 02 - Tables - Core.sql
USE ManicReaders
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	EmailTemplateDescription VARCHAR(500),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode'
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplateField'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplateField
	(
	EmailTemplateFieldID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PlaceHolderText VARCHAR(50),
	PlaceHolderDescription VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateFieldID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode,DisplayOrder'
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.SiteAd
DECLARE @TableName VARCHAR(250) = 'core.SiteAd'

EXEC utility.DropObject @TableName

CREATE TABLE core.SiteAd
	(
	SiteAdID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	StartDate DATE, 
	EndDate DATE, 
	SiteAdLocationID INT, 
	SiteAdStatusID INT,
	SiteAdTypeID INT,
	EmailAddress VARCHAR(320),
	Notes VARCHAR(MAX), 
	InvoiceDetails VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SiteAdStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SiteAdLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SiteAdTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SiteAdID'
EXEC utility.SetIndexClustered @TableName, 'IX_SiteAd', 'BookID,StartDate'
GO
--End table core.SiteAd

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE document.Document
		(
		DocumentID INT IDENTITY(1,1) NOT NULL,
		DocumentDate DATE,
		DocumentDescription VARCHAR(1000),
		DocumentGUID VARCHAR(50),
		DocumentTitle VARCHAR(250),
		DocumentTypeID INT,
		Extension VARCHAR(10),
		CreatePersonID INT,
		ContentType VARCHAR(250),
		ContentSubtype VARCHAR(100),
		DocumentData VARBINARY(MAX),
		PhysicalFileSize BIGINT,
		ThumbnailData VARBINARY(MAX),
		ThumbnailSize BIGINT,
		CreateDateTime DATETIME
		)

	EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'ThumbnailSize', 'BIGINT', '0'

	EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'

	END
--ENDIF
--End table document.Document
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET 
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE document.DocumentEntity
		(
		DocumentEntityID INT IDENTITY(1,1) NOT NULL,
		DocumentID INT,
		EntityTypeCode VARCHAR(50),
		EntityTypeSubCode VARCHAR(50),
		EntityID INT,
		DocumentEntityCode VARCHAR(50),
		CreateDateTime DATETIME,
		DisplayOrder INT
		)

	EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
	EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
	EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'

	END
--ENDIF
GO
--End table document.DocumentEntity

--Begin table document.FileType
DECLARE @TableName VARCHAR(250) = 'document.FileType'

EXEC utility.DropObject @TableName

CREATE TABLE document.FileType
	(
	FileTypeID INT IDENTITY(1,1) NOT NULL,
	Extension VARCHAR(10),
	MimeType VARCHAR(100)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FileTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_FileType', 'Extension,MimeType'
GO
--End table document.FileType

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table syslog.ApplicationErrorLog
DECLARE @TableName VARCHAR(250) = 'syslog.ApplicationErrorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.ApplicationErrorLog
	(
	ApplicationErrorLogID INT IDENTITY(1,1) NOT NULL,
	ErrorDateTime	DATETIME,
	Error	VARCHAR(MAX),
	ErrorRCData	VARCHAR(MAX),
	ErrorSession VARCHAR(MAX),
	ErrorCGIData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ErrorDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationErrorLogID'
GO
--End table syslog.ApplicationErrorLog

--Begin table syslog.BuildLog
DECLARE @TableName VARCHAR(250) = 'syslog.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table syslog.BuildLog

--Begin table utility.Conversion
DECLARE @TableName VARCHAR(250) = 'utility.Conversion'

IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE utility.Conversion
		(
		ConversionID INT IDENTITY(1,1) NOT NULL,
		EntityTypeCode VARCHAR(50),
		EntityTypeSubCode VARCHAR(50),
		EntityID INT,
		IsConverted BIT
		)

	EXEC utility.SetDefaultConstraint @TableName, 'IsConverted', 'BIT', '0'
	EXEC utility.SetPrimaryKeyClustered @TableName, 'ConversionID'

	END
--ENDIF
GO
--End table utility.Conversion

--End file Build File - 02 - Tables - Core.sql

--Begin file Build File - 02 - Tables.sql
USE ManicReaders
GO

--Begin table author.Author
DECLARE @TableName VARCHAR(250) = 'author.Author'

EXEC utility.DropObject @TableName

CREATE TABLE author.Author
	(
	AuthorID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Biography VARCHAR(MAX), 
	IsAdultContent BIT,
	Notes VARCHAR(MAX), 
	Links VARCHAR(MAX), 
	AmazonURL VARCHAR(1000),
	AuthorURL VARCHAR(1000),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	IsSpotlight BIT,
	StatusID INT,
	CanChangeAuthorURL BIT,
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT,
	MembershipStartDate DATE,
	MembershipEndDate DATE,
	MembershipTerm INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CanChangeAuthorURL', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAdultContent', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSpotlight', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipTerm', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuthorID'

EXEC utility.DropFullTextIndex @TableName

CREATE FULLTEXT INDEX ON author.Author
	(
	Biography LANGUAGE ENGLISH
	) KEY INDEX PK_Author ON TextSearchCatalog
GO
--End table author.Author

--Begin table author.AuthorContest
DECLARE @TableName VARCHAR(250) = 'author.AuthorContest'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorContest
	(
	AuthorContestID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	ContestURL VARCHAR(500),
	Title VARCHAR(250),
	Description VARCHAR(MAX),
	IsActive BIT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorContestID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorContest', 'AuthorID,CreateDateTime DESC'
GO
--End table author.AuthorContest

--Begin table author.AuthorEmbeddedMedia
DECLARE @TableName VARCHAR(250) = 'author.AuthorEmbeddedMedia'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'author.EmbeddedMedia'

CREATE TABLE author.AuthorEmbeddedMedia
	(
	AuthorEmbeddedMediaID INT IDENTITY(1,1) NOT NULL,
	AuthorID INT,
	EmbeddedMedia VARCHAR(MAX),
	EmbeddedMediaTypeCode VARCHAR(50),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorEmbeddedMediaID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmbeddedMedia', 'AuthorID,EmbeddedMediaTypeCode,DisplayOrder,AuthorEmbeddedMediaID'
GO
--End table author.AuthorEmbeddedMedia

--Begin table author.AuthorGenre
DECLARE @TableName VARCHAR(250) = 'author.AuthorGenre'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorGenre
	(
	AuthorGenreID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorGenre', 'AuthorID,GenreID'
GO
--End table author.AuthorGenre

--Begin table author.AuthorNews
DECLARE @TableName VARCHAR(250) = 'author.AuthorNews'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorNews
	(
	AuthorNewsID INT IDENTITY(1, 1) NOT NULL,
	AuthorNewsCategoryID INT,
	AuthorID INT,
	Title VARCHAR(250),
	Description VARCHAR(MAX),
	IsActive BIT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AuthorNewsCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorNewsID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorNews', 'AuthorID,CreateDateTime DESC'
GO
--End table author.AuthorNews

--Begin table author.AuthorNote
DECLARE @TableName VARCHAR(250) = 'author.AuthorNote'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorNote
	(
	AuthorNoteID INT IDENTITY(0, 1) NOT NULL,
	AuthorID INT,
	Notes VARCHAR(MAX),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorNoteID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorNote', 'AuthorID, CreateDateTime DESC'
GO
--End table author.AuthorNote

--Begin table author.AuthorRoyalty
DECLARE @TableName VARCHAR(250) = 'author.AuthorRoyalty'

EXEC utility.DropObject @TableName

CREATE TABLE author.AuthorRoyalty
	(
	AuthorRoyaltyID INT IDENTITY(1, 1) NOT NULL,
	AuthorID INT,
	BookID INT,
	StartDate DATE,
	EndDate DATE,
	VendorID INT,
	ListPrice NUMERIC(18,2),
	RoyaltyAmount NUMERIC(18,2),
	QuantitySold INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantitySold', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ListPrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoyaltyAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VendorID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AuthorRoyaltyID'
EXEC utility.SetIndexClustered @TableName, 'IX_AuthorRoyalty', 'AuthorID, BookID, StartDate DESC, EndDate DESC'
GO
--End table author.AuthorRoyalty

--Begin table book.Book
DECLARE @TableName VARCHAR(250) = 'book.Book'

EXEC utility.DropObject @TableName

CREATE TABLE book.Book
	(
	BookID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(250), 
	PublishingCompanyID INT, 
	CoverArtists VARCHAR(250), 
	ReleaseDate DATE, 
	Summary VARCHAR(MAX), 
	Excerpt VARCHAR(MAX),
	PurchaseURL VARCHAR(1000),
	IsFreeRead BIT, 
	StatusID INT,
	Advance NUMERIC(18,2),
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFreeRead', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishingCompanyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BookID'

CREATE FULLTEXT INDEX ON book.Book
	(
	Excerpt LANGUAGE ENGLISH,
	Summary LANGUAGE ENGLISH
	) KEY INDEX PK_Book ON TextSearchCatalog
GO
--End table book.Book

--Begin table book.BookAuthor
DECLARE @TableName VARCHAR(250) = 'book.BookAuthor'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookAuthor
	(
	BookAuthorID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	AuthorID INT,
	IsForRoyaltyTracker BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuthorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsForRoyaltyTracker', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookAuthorID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookAuthor', 'BookID,AuthorID'
GO
--End table book.BookAuthor

--Begin table book.BookFormat
DECLARE @TableName VARCHAR(250) = 'book.BookFormat'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookFormat
	(
	BookFormatID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	FormatID INT,
	ISBN VARCHAR(250),
	ListPrice VARCHAR(250),
	BookLength VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FormatID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookFormatID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookFormat', 'BookID,FormatID'
GO
--End table book.BookFormat

--Begin table book.BookGenre
DECLARE @TableName VARCHAR(250) = 'book.BookGenre'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookGenre
	(
	BookGenreID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookGenre', 'BookID,GenreID'
GO
--End table book.BookGenre

--Begin table book.BookPersonSubscription
DECLARE @TableName VARCHAR(250) = 'book.BookPersonSubscription'

EXEC utility.DropObject @TableName

CREATE TABLE book.BookPersonSubscription
	(
	BookPersonSubscriptionID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	PersonID INT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookPersonSubscriptionID'
EXEC utility.SetIndexClustered @TableName, 'IX_BookPersonSubscription', 'BookID,PersonID'
GO
--End table book.BookPersonSubscription

--Begin table company.PublishingCompany
DECLARE @TableName VARCHAR(250) = 'company.PublishingCompany'

EXEC utility.DropObject @TableName

CREATE TABLE company.PublishingCompany
	(
	PublishingCompanyID INT IDENTITY(1, 1) NOT NULL,
	PublishingCompanyName VARCHAR(250),
	EmailAddress VARCHAR(320),

	CreateDateTime DATETIME,
	CreatePersonID INT,
	Description VARCHAR(MAX),
	Links VARCHAR(MAX),
	AmazonURL VARCHAR(1000),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	StatusID INT,
	SubmissionGuidelines VARCHAR(MAX),
	UpdateDateTime DATETIME,
	UpdatePersonID INT,

	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishingCompanyID'

CREATE FULLTEXT INDEX ON company.PublishingCompany
	(
	Description LANGUAGE ENGLISH
	) KEY INDEX PK_PublishingCompany ON TextSearchCatalog
GO
--End table company.PublishingCompany

--Begin table company.ReviewingCompany
DECLARE @TableName VARCHAR(250) = 'company.ReviewingCompany'

EXEC utility.DropObject @TableName

CREATE TABLE company.ReviewingCompany
	(
	ReviewingCompanyID INT IDENTITY(1, 1) NOT NULL,
	ReviewingCompanyName VARCHAR(250),
	EmailAddress VARCHAR(320),

	CreateDateTime DATETIME,
	CreatePersonID INT,
	Links VARCHAR(MAX),
	FacebookURL VARCHAR(1000),
	InstagramURL VARCHAR(1000),
	LinkedInURL VARCHAR(1000),
	LogoURL VARCHAR(1000),
	TwitterURL VARCHAR(1000),
	WebsiteURL VARCHAR(1000),
	StatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT,

	Description VARCHAR(MAX),
	MinimumReviewRatingID INT,
	ReviewPeriodAudio INT,
	ReviewPeriodEBook INT,
	ReviewPeriodEBookDelayed INT,
	ReviewPeriodPrint INT,

	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'MinimumReviewRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewingCompanyID'

CREATE FULLTEXT INDEX ON company.ReviewingCompany
	(
	Description LANGUAGE ENGLISH
	) KEY INDEX PK_ReviewingCompany ON TextSearchCatalog
GO
--End table company.ReviewingCompany

--Begin table company.ReviewingCompanyGenre
DECLARE @TableName VARCHAR(250) = 'company.ReviewingCompanyGenre'

EXEC utility.DropObject @TableName

CREATE TABLE company.ReviewingCompanyGenre
	(
	ReviewingCompanyGenreID INT IDENTITY(1, 1) NOT NULL,
	ReviewingCompanyID INT,
	GenreID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'GenreID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ReviewingCompanyGenreID'
EXEC utility.SetIndexClustered @TableName, 'IX_ReviewingCompanyGenre', 'ReviewingCompanyID,GenreID'
GO
--End table company.ReviewingCompanyGenre

--Begin table dropdown.AuthorNewsCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.AuthorNewsCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AuthorNewsCategory
	(
	AuthorNewsCategoryID INT IDENTITY(0,1) NOT NULL,
	AuthorNewsCategoryCode VARCHAR(50),
	AuthorNewsCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuthorNewsCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AuthorNewsCategory', 'DisplayOrder,AuthorNewsCategoryName', 'AuthorNewsCategoryID'
GO
--End table dropdown.AuthorNewsCategory

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	ISOCurrencyCode CHAR(3),
	CountryName NVARCHAR(250),
	CurrencyName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_Country', 'DisplayOrder,CountryName'
GO
--End table dropdown.Country

--Begin table dropdown.Format
DECLARE @TableName VARCHAR(250) = 'dropdown.Format'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Format
	(
	FormatID INT IDENTITY(0,1) NOT NULL,
	FormatCode VARCHAR(50),
	FormatName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FormatID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Format', 'DisplayOrder,FormatName', 'FormatID'
GO
--End table dropdown.Format

--Begin table dropdown.Genre
DECLARE @TableName VARCHAR(250) = 'dropdown.Genre'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Genre
	(
	GenreID INT IDENTITY(0,1) NOT NULL,
	GenreCode VARCHAR(50),
	GenreName VARCHAR(50),
	HasHeatLevel BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasHeatLevel', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'GenreID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Genre', 'DisplayOrder,GenreName', 'GenreID'
GO
--End table dropdown.Genre

--Begin table dropdown.HeatLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.HeatLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.HeatLevel
	(
	HeatLevelID INT IDENTITY(0,1) NOT NULL,
	HeatLevelCode VARCHAR(50),
	HeatLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'HeatLevelID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_HeatLevel', 'DisplayOrder,HeatLevelName', 'HeatLevelID'
GO
--End table dropdown.HeatLevel

--Begin table dropdown.LinkType
DECLARE @TableName VARCHAR(250) = 'dropdown.LinkType'

EXEC utility.DropObject @TableName
GO
--End table dropdown.LinkType

--Begin table dropdown.MembershipType
DECLARE @TableName VARCHAR(250) = 'dropdown.MembershipType'

EXEC utility.DropObject 'dropdown.MembershipType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MembershipType
	(
	MembershipTypeID INT IDENTITY(0,1) NOT NULL,
	MembershipTypeName VARCHAR(50),
	MembershipTerm INT,
	MembershipAmount NUMERIC(18,2),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipAmount', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MembershipTerm', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MembershipTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MembershipType', 'DisplayOrder,MembershipTypeName', 'MembershipTypeID'
GO
--End table dropdown.MembershipType

--Begin table dropdown.PublishingCompanyRole
DECLARE @TableName VARCHAR(250) = 'dropdown.PublishingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PublishingCompanyRole
	(
	PublishingCompanyRoleID INT IDENTITY(0,1) NOT NULL,
	PublishingCompanyRoleCode VARCHAR(50),
	PublishingCompanyRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishingCompanyRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PublishingCompanyRole', 'DisplayOrder,PublishingCompanyRoleName', 'PublishingCompanyRoleID'
GO
--End table dropdown.PublishingCompanyRole

--Begin table dropdown.ReviewingCompanyRole
DECLARE @TableName VARCHAR(250) = 'dropdown.ReviewingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ReviewingCompanyRole
	(
	ReviewingCompanyRoleID INT IDENTITY(0,1) NOT NULL,
	ReviewingCompanyRoleCode VARCHAR(50),
	ReviewingCompanyRoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewingCompanyRoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewingCompanyRole', 'DisplayOrder,ReviewingCompanyRoleName', 'ReviewingCompanyRoleID'
GO
--End table dropdown.ReviewingCompanyRole

--Begin table dropdown.ReviewRating
DECLARE @TableName VARCHAR(250) = 'dropdown.ReviewRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ReviewRating
	(
	ReviewRatingID INT IDENTITY(0,1) NOT NULL,
	ReviewRatingCode VARCHAR(50),
	ReviewRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewRating', 'DisplayOrder,ReviewRatingName', 'ReviewRatingID'
GO
--End table dropdown.ReviewRating

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleCode VARCHAR(50),
	RoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role

--Begin table dropdown.RolePermissionableLineage
DECLARE @TableName VARCHAR(250) = 'dropdown.RolePermissionableLineage'
GO
--End table dropdown.RolePermissionableLineage

--Begin table dropdown.SiteAdLocation
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdLocation'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdLocation
	(
	SiteAdLocationID INT IDENTITY(0,1) NOT NULL,
	SiteAdLocationCode VARCHAR(50),
	SiteAdLocationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdLocationID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdLocation', 'DisplayOrder,SiteAdLocationName', 'SiteAdLocationID'
GO
--End table dropdown.SiteAdLocation

--Begin table dropdown.SiteAdStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdStatus
	(
	SiteAdStatusID INT IDENTITY(0,1) NOT NULL,
	SiteAdStatusCode VARCHAR(50),
	SiteAdStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdStatus', 'DisplayOrder,SiteAdStatusName', 'SiteAdStatusID'
GO
--End table dropdown.SiteAdStatus

--Begin table dropdown.SiteAdType
DECLARE @TableName VARCHAR(250) = 'dropdown.SiteAdType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SiteAdType
	(
	SiteAdTypeID INT IDENTITY(0,1) NOT NULL,
	SiteAdTypeCode VARCHAR(50),
	SiteAdTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SiteAdTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SiteAdType', 'DisplayOrder,SiteAdTypeName', 'SiteAdTypeID'
GO
--End table dropdown.SiteAdType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Status', 'DisplayOrder,StatusName', 'StatusID'
GO
--End table dropdown.Status

--Begin table dropdown.Vendor
DECLARE @TableName VARCHAR(250) = 'dropdown.Vendor'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Vendor
	(
	VendorID INT IDENTITY(0,1) NOT NULL,
	VendorCode VARCHAR(50),
	VendorName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VendorID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Vendor', 'DisplayOrder,VendorName', 'VendorID'
GO
--End table dropdown.Vendor

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	--Begin standard fields
	PersonID INT IDENTITY(0, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsEmailAddressVerified BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	CreateDateTime DATETIME,

	--Begin Address fields
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	Region VARCHAR(50),
	PostalCode VARCHAR(10),
	ISOCountryCode2 CHAR(2),

	--Begin Miscellaneous fields
	PasswordSecurityQuestion VARCHAR(250),
	PasswordSecurityQuestionAnswer VARCHAR(250),
	IsNewsLetterSubscriber BIT,
	IsUpdateSubscriber BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsEmailAddressVerified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNewsLetterSubscriber', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUpdateSubscriber', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person

--Begin table person.PersonPublishingCompanyRole
DECLARE @TableName VARCHAR(250) = 'person.PersonPublishingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonPublishingCompanyRole
	(
	PersonPublishingCompanyRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	PublishingCompanyID INT,
	PublishingCompanyRoleID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishingCompanyRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPublishingCompanyRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPublishingCompanyRole', 'PersonID,PublishingCompanyID,PublishingCompanyRoleID'
GO
--End table person.PersonPublishingCompanyRole

--Begin table person.PersonReviewingCompanyRole
DECLARE @TableName VARCHAR(250) = 'person.PersonReviewingCompanyRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonReviewingCompanyRole
	(
	PersonReviewingCompanyRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	ReviewingCompanyID INT,
	ReviewingCompanyRoleID INT,
	PenName VARCHAR(100),
	StatusID INT,
	BookLimit INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookLimit', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyRoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonReviewingCompanyRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonReviewingCompanyRole', 'PersonID,ReviewingCompanyID,ReviewingCompanyRoleID'
GO
--End table person.PersonReviewingCompanyRole

--Begin table person.PersonRole
DECLARE @TableName VARCHAR(250) = 'person.PersonRole'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonRole
	(
	PersonRoleID INT IDENTITY(1, 1) NOT NULL,
	PersonID INT,
	RoleID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonRoleID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonRole', 'PersonID,RoleID'
GO
--End table person.PersonRole

--Begin table review.Review
DECLARE @TableName VARCHAR(250) = 'review.Review'

EXEC utility.DropObject @TableName

CREATE TABLE review.Review
	(
	ReviewID INT IDENTITY(1, 1) NOT NULL,
	ReviewRequestID INT,
	ReviewerPersonID INT,
	ReviewingCompanyID INT,
	ReviewRatingID INT,
	StatusID INT,
	ReviewEditorPersonID INT,
	BookCheckoutDateTime DATETIME,
	ReviewSubmitDateTime DATETIME,
	ReviewDateTime DATETIME,
	NotifyDateTime DATETIME,
	ReviewLink VARCHAR(1000),
	Review VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ReviewRequestID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewingCompanyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewEditorPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BookCheckoutDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewSubmitDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'NotifyDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ReviewID'
EXEC utility.SetIndexClustered @TableName, 'IX_Review', 'ReviewRequestID,ReviewerPersonID'
GO
--End table review.Review

--Begin table review.ReviewRequest
DECLARE @TableName VARCHAR(250) = 'review.ReviewRequest'

EXEC utility.DropObject @TableName

CREATE TABLE review.ReviewRequest
	(
	ReviewRequestID INT IDENTITY(1, 1) NOT NULL,
	BookID INT,
	RequestPersonID INT,
	ReviewRequestDateTime DATETIME,
	HeatLevelID INT,
	NotifyEmailAddress VARCHAR(320),
	FormatID INT,
	ReviewRequestNotes VARCHAR(MAX),
	IsArchived BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'BookID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FormatID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HeatLevelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsArchived', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ReviewRequestDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ReviewRequestID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ReviewRequest', 'BookID'
GO
--End table review.ReviewRequest


--End file Build File - 02 - Tables.sql

--Begin file Build File - 03 - Functions - Core.sql
USE ManicReaders
GO

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatExternalURL
EXEC utility.DropObject 'core.FormatExternalURL'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.04.11
-- Description:	A function to ensure that a URL is prepended with http://
-- ======================================================================

CREATE FUNCTION core.FormatExternalURL
(
@ExternalURL VARCHAR(1000)
)

RETURNS VARCHAR(1007)

AS
BEGIN

	RETURN 
		CASE 
			WHEN LEFT(@ExternalURL, 7) = 'http://' OR LEFT(@ExternalURL, 8) = 'https://' 
			THEN @ExternalURL 
			ELSE 'http://' + @ExternalURL 
		END

END
GO
--End function core.FormatExternalURL

--Begin function core.FormatFullTextSearchString
EXEC utility.DropObject 'core.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION core.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10)
)

RETURNS VARCHAR(2000)

AS
BEGIN

	SET @SearchString = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SearchString, '"', ''), ',', ''), '!', ''), ')', ''), '(', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE IF CHARINDEX(' ', @SearchString) > 0
		SET @SearchString = CASE WHEN @MatchTypeCode = 'All' THEN REPLACE(@SearchString, ' ', ' AND ') ELSE REPLACE(@SearchString, ' ', ' OR ') END
	--ENDIF

	RETURN @SearchString

END
GO
--End function core.FormatFullTextSearchString

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	RETURN IIF(@String IS NULL OR LEN(RTRIM(@String)) = 0, NULL, @String)

END
GO
--End function core.NullIfEmpty

--Begin function core.StripCharacters
EXEC utility.DropObject 'core.StripCharacters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.20
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION core.StripCharacters
(
@String NVARCHAR(MAX), 
@Pattern VARCHAR(255)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	SET @Pattern =  '%[' + @Pattern + ']%'

	WHILE PatIndex(@Pattern, @String) > 0
		SET @String = Stuff(@String, PatIndex(@Pattern, @String), 1, '')
	--END WHILE

	RETURN @String

END
GO
--End function core.StripCharacters

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function document.FormatFileSize
EXEC utility.DropObject 'document.FormatFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatFileSize
(
@FileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cFileSize VARCHAR(50)

	SELECT @cFileSize = 

		CASE
			WHEN @FileSize < 1000
			THEN CAST(@FileSize as VARCHAR(50)) + ' b'
			WHEN @FileSize < 1000000
			THEN CAST(ROUND((@FileSize/1000), 2) AS VARCHAR(50)) + ' kb'
			WHEN @FileSize < 1000000000
			THEN CAST(ROUND((@FileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
			WHEN @FileSize < 1000000000000
			THEN CAST(ROUND((@FileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
			ELSE 'More than 1000 gb'
		END

	RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatFileSize

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function eventlog.GetDocumentEntityXML
EXEC utility.DropObject 'eventlog.GetDocumentEntityXML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return document data from an EntitytypeCode and an EntityID
-- ======================================================================================
CREATE FUNCTION eventlog.GetDocumentEntityXML
(
@cEntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cEntityDocuments VARCHAR(MAX) = ''
	
	SELECT @cEntityDocuments = COALESCE(@cEntityDocuments, '') + D.EntityDocument
	FROM
		(
		SELECT
			(SELECT DE.DocumentID FOR XML RAW(''), ELEMENTS) AS EntityDocument
		FROM document.DocumentEntity DE
		WHERE DE.EntityID = @EntityID
			AND DE.EntityTypeCode = @cEntityTypeCode
		) D

	RETURN '<DocumentEntityData>' + ISNULL(@cEntityDocuments, '') + '</DocumentEntityData>'

END
GO
--End function eventlog.GetDocumentEntityXML

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	DECLARE @cSuffix VARCHAR(50)
	DECLARE @cTitle VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cSuffix = ISNULL(P.Suffix, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format LIKE '%FirstLast%'
		BEGIN
			
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @Format LIKE '%Suffix'
			SET @cRetVal += @cSuffix
		--ENDIF
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cRetVal + @cTitle
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.IsSuperAdministrator
EXEC utility.DropObject 'person.IsSuperAdministrator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to format a numeric value for javascript
-- ================================================================

CREATE FUNCTION person.IsSuperAdministrator
(
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	RETURN CASE WHEN EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1) THEN 1 ELSE 0 END

END
GO
--End function person.IsSuperAdministrator

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = P.Password,
		@cPasswordSalt = P.PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword
--End file Build File - 03 - Functions - Core.sql

--Begin file Build File - 03 - Functions.sql
USE ManicReaders
GO

--Begin function author.FormatAuthorNameByAuthorID
EXEC utility.DropObject 'author.FormatAuthorNameByAuthorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.31
-- Description:	A function to return an author name in a specified format
-- ======================================================================

CREATE FUNCTION author.FormatAuthorNameByAuthorID
(
@AuthorID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cMiddleName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(A.FirstName, ''),
		@cLastName = ISNULL(A.LastName, ''),
		@cMiddleName = ISNULL(A.MiddleName, '')
	FROM author.Author A
	WHERE A.AuthorID = @AuthorID
	
	IF @Format IN ('FirstLast', 'FirstMiddleLast')
		BEGIN
			
		SET @cRetVal = @cFirstName

		IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
			SET @cRetVal += @cMiddleName
		--ENDIF

		SET @cRetVal += ' ' + @cLastName
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE '%Middle' AND LEN(RTRIM(@cMiddleName)) > 0
			SET @cRetVal = @cRetVal + @cMiddleName
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function author.FormatAuthorNameByAuthorID

--Begin function book.FormatBookAuthorNamesByBookID
EXEC utility.DropObject 'book.FormatBookAuthorNamesByBookID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.17
-- Description:	A function to return a list of author names for a book
-- ===================================================================

CREATE FUNCTION book.FormatBookAuthorNamesByBookID
(
@BookID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @cRetVal VARCHAR(MAX)

SELECT @cRetVal = STUFF(
	(	
	SELECT 
		'; ' 
			+ LTRIM(RTRIM(A.LastName)) 
			+ CASE WHEN A.FirstName IS NOT NULL OR A.MiddleName IS NOT NULL THEN ', ' ELSE '' END 
			+ LTRIM(RTRIM(
				ISNULL(LTRIM(RTRIM(A.FirstName)), '') + ' ' + ISNULL(LTRIM(RTRIM(A.MiddleName)), '')
				))
	FROM author.Author A
		JOIN book.BookAuthor BA ON BA.AuthorID = A.AuthorID
			AND BA.BookID = @BookID
	ORDER BY 1
	FOR XML PATH('')
	), 1, 1, '')
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function book.FormatBookAuthorNamesByBookID

--Begin function core.GetEntityLinkByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'core.GetEntityLinkByEntityTypeCodeAndEntityID'
GO
--End function core.GetEntityLinkByEntityTypeCodeAndEntityID

--End file Build File - 03 - Functions.sql

--Begin file Build File - 04 - Procedures - Core.sql
USE ManicReaders
GO

--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'core.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- ==================================================================================
-- Author:		Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.EmailTemplateField table
-- ==================================================================================
CREATE PROCEDURE core.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure core.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure core.GetMenuItemsByPersonID
EXEC Utility.DropObject 'core.GetMenuItemsByPersonID'
GO
--End procedure core.GetMenuItemsByPersonID

--Begin procedure core.GetSiteAdBySiteAdID
EXEC Utility.DropObject 'core.GetSiteAdBySiteAdID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SiteAd table
-- ============================================================================
CREATE PROCEDURE core.GetSiteAdBySiteAdID

@SiteAdID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		(SELECT B.Title FROM book.Book B WHERE B.BookID = SA.BookID) AS Title,
		SA.BookID,
		SA.EmailAddress, 
		SA.EndDate, 
		core.FormatDate(SA.EndDate) AS EndDateFormatted,
		SA.InvoiceDetails,
		SA.Notes, 
		SA.SiteAdID, 
		SA.StartDate, 
		core.FormatDate(SA.StartDate) AS StartDateFormatted,
		SAL.SiteAdLocationID,
		SAL.SiteAdLocationName,
		SAS.SiteAdStatusID,
		SAS.SiteAdStatusName,
		SAT.SiteAdTypeID,
		SAT.SiteAdTypeName
	FROM core.SiteAd SA
		JOIN dropdown.SiteAdLocation SAL ON SAL.SiteAdLocationID = SA.SiteAdLocationID
		JOIN dropdown.SiteAdStatus SAS ON SAS.SiteAdStatusID = SA.SiteAdStatusID
		JOIN dropdown.SiteAdType SAT ON SAT.SiteAdTypeID = SA.SiteAdTypeID
			AND SA.SiteAdID = @SiteAdID
	
END
GO
--End procedure core.GetSiteAdBySiteAdID

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin core.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentGUID,
		D.DocumentID, 
		D.DocumentTitle, 
		D.PhysicalFileSize,
		D.ThumbnailData, 
		D.ThumbnailSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentGUID
EXEC utility.DropObject 'document.GetDocumentByDocumentGUID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentGUID

@DocumentGUID VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDescription,
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentGUID = @DocumentGUID
	
END
GO
--End procedure document.GetDocumentByDocumentGUID

--Begin procedure document.GetFileTypeData
EXEC utility.DropObject 'document.GetFileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.05.08
-- Description:	A stored procedure to get data from the document.FileType table
-- ============================================================================
CREATE PROCEDURE document.GetFileTypeData

@MimeType VARCHAR(100)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		FT.Extension,
		LEFT(FT.MimeType, CHARINDEX('/', FT.MimeType) - 1) AS ContentType,
		REVERSE(LEFT(REVERSE(FT.MimeType), CHARINDEX('/', REVERSE(FT.MimeType)) - 1)) AS ContentSubType
	FROM document.FileType FT
	WHERE FT.MimeType = @MimeType

END
GO
--End procedure document.GetFileTypeData

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'core.processGeneralFileUploads'
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT = 0,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	INSERT INTO @tOutput
		(DocumentID)
	SELECT 
		D.DocumentID
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData

	IF NOT EXISTS (SELECT 1 FROM @tOutput O)
		BEGIN

		INSERT INTO document.Document
			(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		VALUES
			(
			@ContentType,
			@ContentSubtype,
			@CreatePersonID,
			@DocumentData,
			@DocumentDescription,
			newID(),
			@DocumentTitle,
			@Extension
			)

		END
	--ENDIF

	INSERT INTO document.DocumentEntity
		(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
	SELECT
		O.DocumentID,
		@DocumentEntityCode,
		@EntityTypeCode,
		@EntityTypeSubCode,
		@EntityID
	FROM @tOutput O
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = O.DocumentID
			AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
		)

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = 0
		AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

	IF @AllowMultipleDocuments = 0
		BEGIN
				
		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND @EntityID > 0
			AND DE.EntityID = @EntityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tOutput O
				WHERE O.DocumentID = DE.DocumentID
				)

		DELETE D
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.EntityID = @EntityID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tOutput O
					WHERE O.DocumentID = DE.DocumentID
					)

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentGUID VARCHAR(50) = NULL,
@DocumentID INT = 0,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @DocumentGUID IS NOT NULL
		BEGIN

		SELECT @DocumentID = D.DocumentID
		FROM document.Document D
		WHERE D.DocumentGUID = @DocumentGUID

		END
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE	DE.EntityTypeCode = @EntityTypeCode
		AND (DE.EntityTypeSubCode = @EntityTypeSubCode OR (@EntityTypeSubCode IS NULL AND DE.EntityTypeSubCode IS NULL))
		AND DE.EntityID = @EntityID 
		AND ((@DocumentGUID IS NULL AND @DocumentID = 0) OR DE.DocumentID = @DocumentID)

	IF NOT EXISTS (SELECT 1 FROM document.DocumentEntity DE WHERE DE.DocumentID = @DocumentID)
		BEGIN

		DELETE D
		FROM document.Document D
		WHERE D.DocumentID = @DocumentID

		END
	--ENDIF

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure document.SaveEntityDocuments
EXEC Utility.DropObject 'document.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		END
	--ENDIF

END
GO
--End procedure document.SaveEntityDocuments

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.21
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogAuthorAction
EXEC utility.DropObject 'eventlog.LogAuthorAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAuthorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Author'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cAuthorContestXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorContestXML = COALESCE(@cAuthorContestXML, '') + D.AuthorContest
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorContest'), ELEMENTS) AS AuthorContest
			FROM author.AuthorContest T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorEmbeddedMediaXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorEmbeddedMediaXML = COALESCE(@cAuthorEmbeddedMediaXML, '') + D.AuthorEmbeddedMedia
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorEmbeddedMedia'), ELEMENTS) AS AuthorEmbeddedMedia
			FROM author.AuthorEmbeddedMedia T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorGenreXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorGenreXML = COALESCE(@cAuthorGenreXML, '') + D.AuthorGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorGenre'), ELEMENTS) AS AuthorGenre
			FROM author.AuthorGenre T 
			WHERE T.AuthorID = @EntityID
			) D

		DECLARE @cAuthorNewsXML VARCHAR(MAX) = ''
		
		SELECT @cAuthorNewsXML = COALESCE(@cAuthorNewsXML, '') + D.AuthorNews
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AuthorNews'), ELEMENTS) AS AuthorNews
			FROM author.AuthorNews T 
			WHERE T.AuthorID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<AuthorContestData>' + ISNULL(@cAuthorContestXML, '') + '</AuthorContestData>' AS XML),
			CAST('<AuthorEmbeddedMediaData>' + ISNULL(@cAuthorEmbeddedMediaXML, '') + '</AuthorEmbeddedMediaData>' AS XML),
			CAST('<AuthorGenreData>' + ISNULL(@cAuthorGenreXML, '') + '</AuthorGenreData>' AS XML),
			CAST('<AuthorNewsData>' + ISNULL(@cAuthorNewsXML, '') + '</AuthorNewsData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Author'), ELEMENTS
			)
		FROM author.Author T
		WHERE T.AuthorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAuthorAction

--Begin procedure eventlog.LogBookAction
EXEC utility.DropObject 'eventlog.LogBookAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogBookAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Book'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cBookAuthorXML VARCHAR(MAX) = ''
		
		SELECT @cBookAuthorXML = COALESCE(@cBookAuthorXML, '') + D.BookAuthor
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookAuthor'), ELEMENTS) AS BookAuthor
			FROM book.BookAuthor T 
			WHERE T.BookID = @EntityID
			) D

		DECLARE @cBookFormatXML VARCHAR(MAX) = ''
		
		SELECT @cBookFormatXML = COALESCE(@cBookFormatXML, '') + D.BookFormat
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookFormat'), ELEMENTS) AS BookFormat
			FROM book.BookFormat T 
			WHERE T.BookID = @EntityID
			) D

		DECLARE @cBookGenreXML VARCHAR(MAX) = ''
		
		SELECT @cBookGenreXML = COALESCE(@cBookGenreXML, '') + D.BookGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('BookGenre'), ELEMENTS) AS BookGenre
			FROM book.BookGenre T 
			WHERE T.BookID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<BookAuthorData>' + ISNULL(@cBookAuthorXML, '') + '</BookAuthorData>' AS XML),
			CAST('<BookFormatData>' + ISNULL(@cBookFormatXML, '') + '</BookFormatData>' AS XML),
			CAST('<BookGenreData>' + ISNULL(@cBookGenreXML, '') + '</BookGenreData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Book'), ELEMENTS
			)
		FROM book.Book T
		WHERE T.BookID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogBookAction

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonPermissionableXML VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionableXML = COALESCE(@cPersonPermissionableXML, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonPermissionableData>' + ISNULL(@cPersonPermissionableXML, '') + '</PersonPermissionableData>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogPublishingCompanyAction
EXEC utility.DropObject 'eventlog.LogPublishingCompanyAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPublishingCompanyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PublishingCompany'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('PublishingCompany'), ELEMENTS
			)
		FROM company.PublishingCompany T
		WHERE T.PublishingCompanyID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPublishingCompanyAction

--Begin procedure eventlog.LogReviewAction
EXEC utility.DropObject 'eventlog.LogReviewAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Review'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('Review'), ELEMENTS
			)
		FROM review.Review T
		WHERE T.ReviewID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewAction

--Begin procedure eventlog.LogReviewingCompanyAction
EXEC utility.DropObject 'eventlog.LogReviewingCompanyAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewingCompanyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ReviewingCompany'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cReviewingCompanyGenreXML VARCHAR(MAX) = ''
		
		SELECT @cReviewingCompanyGenreXML = COALESCE(@cReviewingCompanyGenreXML, '') + D.ReviewingCompanyGenre
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ReviewingCompanyGenre'), ELEMENTS) AS ReviewingCompanyGenre
			FROM company.ReviewingCompanyGenre T 
			WHERE T.ReviewingCompanyID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<ReviewingCompanyGenreData>' + ISNULL(@cReviewingCompanyGenreXML, '') + '</ReviewingCompanyGenreData>' AS XML),
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('ReviewingCompany'), ELEMENTS
			)
		FROM company.ReviewingCompany T
		WHERE T.ReviewingCompanyID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewingCompanyAction

--Begin procedure eventlog.LogReviewRequestAction
EXEC utility.DropObject 'eventlog.LogReviewRequestAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogReviewRequestAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ReviewRequest'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentEntityXML(@cEntityTypeCode, @EntityID) AS XML)
			FOR XML RAW('ReviewRequest'), ELEMENTS
			)
		FROM review.ReviewRequest T
		WHERE T.ReviewRequestID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogReviewRequestAction

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the person.Person table based on a Token
-- ============================================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@Token VARCHAR(36)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PersonID,
		P.TokenCreateDateTime,

		CASE
			WHEN DATEDIFF("hour", P.TokenCreateDateTime, getdate()) <= 24
			THEN 0
			ELSE 1
		END AS IsTokenExpired

	FROM person.Person P
	WHERE P.Token = @Token

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsMember BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		UserName VARCHAR(250)
		)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '365') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < getDate()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < getDate()
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cUserName = P.UserName
	FROM person.Person P
	WHERE P.UserName = @UserName
		OR P.EmailAddress = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsMember,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,UserName)
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			CASE WHEN EXISTS (SELECT 1 FROM author.Author A WHERE A.PersonID = @nPersonID AND A.MembershipEndDate >= getDate()) THEN 1 ELSE 0 END,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * 
	FROM @tPerson
			
	SELECT 
		PPCR.PublishingCompanyID,
		PCR.PublishingCompanyRoleCode
	FROM person.PersonPublishingCompanyRole PPCR
		JOIN dropdown.PublishingCompanyRole PCR ON PCR.PublishingCompanyRoleID = PPCR.PublishingCompanyRoleID
			AND PPCR.PersonID = @nPersonID
	ORDER BY 1, 2
			
	SELECT 
		PRCR.ReviewingCompanyID,
		RCR.ReviewingCompanyRoleCode
	FROM person.PersonReviewingCompanyRole PRCR
		JOIN dropdown.ReviewingCompanyRole RCR ON RCR.ReviewingCompanyRoleID = PRCR.ReviewingCompanyRoleID
			AND PRCR.PersonID = @nPersonID
	ORDER BY 1, 2

	SELECT
		R.RoleCode
	FROM person.PersonRole PR
		JOIN dropdown.Role R ON R.RoleID = PR.RoleID
			AND PR.PersonID = @nPersonID
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--End file Build File - 04 - Procedures - Core.sql

--Begin file Build File - 04 - Procedures.sql
USE ManicReaders
GO

--Begin procedure author.GetAuthorByAuthorID
EXEC Utility.DropObject 'author.GetAuthorByAuthorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.07
-- Description:	A stored procedure to return data from the author.Author table
-- ===========================================================================
CREATE PROCEDURE author.GetAuthorByAuthorID

@AuthorID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Author
	SELECT
		getDate() AS CurrentDate,
		core.FormatDate(getDate()) AS CurrentDateFormatted,
		core.FormatExternalURL(A.AmazonURL) AS AmazonURL,
		A.AuthorID,
		A.AuthorURL,
		author.FormatAuthorNameByAuthorID(A.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		A.Biography,
		A.CanChangeAuthorURL,
		A.CreateDateTime,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.CreatePersonID,
		core.FormatExternalURL(A.FacebookURL) AS FacebookURL,
		A.FirstName,
		core.FormatExternalURL(A.InstagramURL) AS InstagramURL,
		A.IsAdultContent,
		A.IsSpotlight,
		A.LastName,
		core.FormatExternalURL(A.LinkedInURL) AS LinkedInURL,
		A.Links,
		A.MembershipEndDate,
		CASE WHEN A.MembershipEndDate >= getDate() THEN 1 ELSE 0 END AS IsMember,
		core.FormatDate(A.MembershipEndDate) AS MembershipEndDateFormatted,
		A.MembershipStartDate,
		core.FormatDate(A.MembershipStartDate) AS MembershipStartDateFormatted,
		A.MiddleName,
		A.Notes,
		A.PersonID,
		core.FormatExternalURL(A.TwitterURL) AS TwitterURL,
		A.UpdateDateTime,
		core.FormatDateTime(A.UpdateDateTime) AS UpdateDateTimeFormatted,
		core.FormatExternalURL(A.WebsiteURL) AS WebsiteURL,
		MT.MembershipTerm,
		MT.MembershipTypeName,
		S.StatusID,
		S.StatusName
	FROM author.Author A
		JOIN dropdown.MembershipType MT ON MT.MembershipTerm = A.MembershipTerm
		JOIN dropdown.Status S ON S.StatusID = A.StatusID
			AND A.AuthorID = @AuthorID

	--AuthorBook
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title,
		CASE WHEN A.MembershipEndDate >= getDate() THEN 1 ELSE BA.IsForRoyaltyTracker END AS IsForRoyaltyTracker,		
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.BookAuthor BA
		JOIN author.Author A ON A.AuthorID = BA.AuthorID
		JOIN book.Book B ON B.BookID = BA.BookID
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND BA.AuthorID = @AuthorID

	--AuthorContest
	SELECT
		newID() AS AuthorContestGUID,
		AC.AuthorContestID,
		AC.CreateDateTime,
		core.FormatDate(AC.CreateDateTime) AS CreateDateTimeFormatted,
		AC.ContestURL,
		AC.Description,
		AC.IsActive,
		AC.Title
	FROM author.AuthorContest AC
	WHERE AC.AuthorID = @AuthorID
	ORDER BY AC.CreateDateTime DESC

	--AuthorDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Author'
			AND DE.EntityTypeSubCode = 'BioPhoto'
			AND DE.EntityID = @AuthorID

	--AuthorEmbeddedMedia
	SELECT
		AEM.AuthorEmbeddedMediaID,
		AEM.EmbeddedMedia,
		AEM.EmbeddedMediaTypeCode
	FROM author.AuthorEmbeddedMedia AEM
	WHERE AEM.AuthorID = @AuthorID
	ORDER BY AEM.EmbeddedMediaTypeCode, AEM.DisplayOrder, AEM.AuthorEmbeddedMediaID

	--AuthorGenre
	SELECT
		G.GenreCode,
		G.GenreID,
		G.GenreName
	FROM author.AuthorGenre AG
		JOIN dropdown.Genre G ON G.GenreID = AG.GenreID
			AND AG.AuthorID = @AuthorID
	ORDER BY G.DisplayOrder, G.GenreName

	--AuthorFreeRead
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title
	FROM book.Book B
	WHERE B.IsFreeRead = 1
		AND EXISTS
			(
			SELECT 1
			FROM book.BookAuthor BA
			WHERE BA.BookID = B.BookID
				AND BA.AuthorID = @AuthorID
			)
	ORDER BY B.Title

	--AuthorNews
	SELECT
		newID() AS AuthorNewsGUID,
		AN.AuthorNewsID,
		AN.CreateDateTime,
		core.FormatDate(AN.CreateDateTime) AS CreateDateTimeFormatted,
		AN.Description,
		AN.IsActive,
		AN.Title,
		ANC.AuthorNewsCategoryID,
		ANC.AuthorNewsCategoryName
	FROM author.AuthorNews AN
		JOIN dropdown.AuthorNewsCategory ANC ON ANC.AuthorNewsCategoryID = AN.AuthorNewsCategoryID
			AND AN.AuthorID = @AuthorID
	ORDER BY AN.CreateDateTime DESC

	--AuthorPhotoGallery
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Author'
			AND DE.EntityTypeSubCode = 'PhotoGallery'
			AND DE.EntityID = @AuthorID

	--AuthorPublishingCompany
	SELECT
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		PC.WebsiteURL
	FROM company.PublishingCompany PC
	WHERE EXISTS
		(
		SELECT 1
		FROM book.Book B
			JOIN book.BookAuthor BA ON BA.BookID = B.BookID
				AND BA.AuthorID = @AuthorID
				AND B.PublishingCompanyID = PC.PublishingCompanyID
		)
	ORDER BY PC.PublishingCompanyName

END
GO
--End procedure author.GetAuthorByAuthorID

--Begin procedure author.GetAuthorRoyaltyBooksByPersonID
EXEC Utility.DropObject 'author.GetAuthorRoyaltyBooksByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the book.BookAuthor table
-- ==========================================================================
CREATE PROCEDURE author.GetAuthorRoyaltyBooksByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--AuthorBook
	SELECT
		B.BookID,
		B.Title,
		BA.AuthorID
	FROM book.BookAuthor BA
		JOIN author.Author A ON A.AuthorID = BA.AuthorID
		JOIN book.Book B ON B.BookID = BA.BookID
			AND A.PersonID = @PersonID
			AND 
				(
				A.MembershipEndDate >= getDate()
					OR BA.IsForRoyaltyTracker = 1
				)
	ORDER BY 2

END
GO
--End procedure author.GetAuthorRoyaltyBooksByPersonID

--Begin procedure author.GetAuthorRoyaltyByAuthorRoyaltyID
EXEC Utility.DropObject 'author.GetAuthorRoyaltyByAuthorRoyaltyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the book.BookAuthor table
-- ==========================================================================
CREATE PROCEDURE author.GetAuthorRoyaltyByAuthorRoyaltyID

@AuthorRoyaltyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		AR.AuthorID,
		AR.AuthorRoyaltyID,
		AR.BookID,
		core.FormatDate(AR.EndDate) AS EndDateFormatted,
		AR.QuantitySold,
		AR.ListPrice,
		AR.RoyaltyAmount,
		core.FormatDate(AR.StartDate) AS StartDateFormatted,
		AR.VendorID
	FROM author.AuthorRoyalty AR
	WHERE AR.AuthorRoyaltyID = @AuthorRoyaltyID

END
GO
--End procedure author.GetAuthorRoyaltyByAuthorRoyaltyID

--Begin procedure author.GetBookSalesGraphData
EXEC Utility.DropObject 'author.GetBookSalesGraphData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.04
-- Description:	A stored procedure to return data from the author.AuthorRoyalty table
-- ==================================================================================
CREATE PROCEDURE author.GetBookSalesGraphData

@AuthorID INT = 0,
@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SUM(AR.QuantitySold) AS QuantitySold,
		core.FormatDate(AR.StartDate) + ' - ' + core.FormatDate(AR.EndDate) AS Period
	FROM author.AuthorRoyalty AR
	WHERE AR.AuthorID = @AuthorID
		AND	AR.BookID = @BookID
	GROUP BY AR.StartDate, AR.EndDate
	ORDER BY AR.StartDate, AR.EndDate

	SELECT B.Title
	FROM book.Book B
	WHERE B.BookID = @BookID

END
GO
--End procedure author.GetBookSalesGraphData

--Begin procedure author.ValidateAuthorURL
EXEC Utility.DropObject 'author.ValidateAuthorURL'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.30
-- Description:	A stored procedure to get data from the author.author table
-- ========================================================================
CREATE PROCEDURE author.ValidateAuthorURL

@AuthorURL VARCHAR(1000),
@AuthorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT A.AuthorID
	FROM author.Author A
	WHERE A.AuthorURL = @AuthorURL
		AND A.AuthorID <> @AuthorID

END
GO
--End procedure author.ValidateAuthorURL

--Begin procedure book.GetBookByBookID
EXEC Utility.DropObject 'book.GetBookByBookID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.31
-- Description:	A stored procedure to return data from the book.Book table
-- =======================================================================
CREATE PROCEDURE book.GetBookByBookID

@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Book
	SELECT
		B.Advance,
		B.BookID,
		B.CoverArtists,
		B.CreateDateTime,
		core.FormatDateTime(B.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(B.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		B.Excerpt,
		B.IsFreeRead,
		core.FormatExternalURL(B.PurchaseURL) AS PurchaseURL,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Summary,
		B.Title,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.Book B
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND B.BookID = @BookID

	--BookAuthor
	SELECT
		author.FormatAuthorNameByAuthorID(BA.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		BA.AuthorID,
		BA.BookAuthorID
	FROM book.BookAuthor BA
	WHERE BA.BookID = @BookID
	ORDER BY 1, 2

	--BookDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Book'
			AND DE.EntityTypeSubCode IN ('FreeRead', 'FrontCover')
			AND DE.EntityID = @BookID

	--BookFormat
	SELECT
		BF.BookFormatID,
		BF.BookLength,
		BF.ISBN,
		BF.ListPrice,
		F.FormatID,
		F.FormatName
	FROM book.BookFormat BF
		JOIN dropdown.Format F ON F.FormatID = BF.FormatID
			AND BF.BookID = @BookID
	ORDER BY F.FormatName, F.FormatID

	--BookGenre
	SELECT
		G.GenreCode,
		G.GenreID,
		G.GenreName
	FROM book.BookGenre BG
		JOIN dropdown.Genre G ON G.GenreID = BG.GenreID
			AND BG.BookID = @BookID
	ORDER BY G.GenreName, G.GenreID

END
GO
--End procedure book.GetBookByBookID

--Begin procedure company.GetPublishingCompanyByPublishingCompanyID
EXEC utility.DropObject 'company.GetPublishingCompanyByPublishingCompanyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the company.PublishingCompany table based on a PublishingCompanyID
-- ========================================================================================================
CREATE PROCEDURE company.GetPublishingCompanyByPublishingCompanyID

@PublishingCompanyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PublishingCompany
	SELECT
		PC.Address1,
		PC.Address2,
		PC.Address3,
		core.FormatExternalURL(PC.AmazonURL) AS AmazonURL,
		PC.CreateDateTime,
		core.FormatDateTime(PC.CreateDateTime) AS CreateDateTimeFormatted,
		PC.CreatePersonID,
		person.FormatPersonNameByPersonID(PC.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		PC.Description,
		PC.EmailAddress,
		PC.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(PC.ISOCountryCode2) AS CountryName,
		core.FormatExternalURL(PC.FacebookURL) AS FacebookURL,
		core.FormatExternalURL(PC.InstagramURL) AS InstagramURL,
		core.FormatExternalURL(PC.LinkedInURL) AS LinkedInURL,
		PC.Links,
		PC.Municipality,
		PC.PostalCode,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		PC.Region,
		PC.SubmissionGuidelines,
		core.FormatExternalURL(PC.TwitterURL) AS TwitterURL,
		PC.UpdateDateTime,
		core.FormatDateTime(PC.UpdateDateTime) AS UpdateDateTimeFormatted,
		PC.UpdatePersonID,
		person.FormatPersonNameByPersonID(PC.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		core.FormatExternalURL(PC.WebsiteURL) AS WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM company.PublishingCompany PC
		JOIN dropdown.Status S ON S.StatusID = PC.StatusID
			AND PC.PublishingCompanyID = @PublishingCompanyID

	--PublishingCompanyAuthor
	SELECT
		A.AuthorID,
		author.FormatAuthorNameByAuthorID(A.AuthorID, 'FirstMiddleLast') AS AuthorNameFormatted,
		core.FormatExternalURL(A.WebsiteURL) AS WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM author.Author A
		JOIN dropdown.Status S ON S.StatusID = A.StatusID
			AND EXISTS
				(
				SELECT 1
				FROM book.BookAuthor BA
					JOIN book.Book B ON B.BookID = BA.BookID
						AND BA.AuthorID = A.AuthorID
						AND B.PublishingCompanyID = @PublishingCompanyID
				)
	ORDER BY 2, 1

	--PublishingCompanyBook
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title,
		PC.PublishingCompanyID,
		PC.PublishingCompanyName,
		S.StatusID,
		S.StatusName
	FROM book.Book B
		JOIN company.PublishingCompany PC ON PC.PublishingCompanyID = B.PublishingCompanyID
		JOIN dropdown.Status S ON S.StatusID = B.StatusID
			AND B.PublishingCompanyID = @PublishingCompanyID
	ORDER BY B.Title, B.BookID

	--PublishingCompanyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PublishingCompany'
			AND DE.EntityTypeSubCode = 'Logo'
			AND DE.EntityID = @PublishingCompanyID

	--PublishingCompanyFreeRead
	SELECT
		B.BookID,
		B.ReleaseDate,
		core.FormatDate(B.ReleaseDate) AS ReleaseDateFormatted,
		B.Title
	FROM book.Book B
	WHERE B.IsFreeRead = 1
		AND B.PublishingCompanyID = @PublishingCompanyID
	ORDER BY B.Title

END
GO
--End procedure company.GetPublishingCompanyByPublishingCompanyID

--Begin procedure company.GetReviewingCompanyByReviewingCompanyID
EXEC utility.DropObject 'company.GetReviewingCompanyByReviewingCompanyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the company.ReviewingCompany table based on a ReviewingCompanyID
-- ========================================================================================================
CREATE PROCEDURE company.GetReviewingCompanyByReviewingCompanyID

@ReviewingCompanyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ReviewingCompany
	SELECT
		RC.Address1,
		RC.Address2,
		RC.Address3,
		RC.CreateDateTime,
		core.FormatDateTime(RC.CreateDateTime) AS CreateDateTimeFormatted,
		RC.CreatePersonID,
		person.FormatPersonNameByPersonID(RC.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		RC.Description,
		RC.EmailAddress,
		core.FormatExternalURL(RC.FacebookURL) AS FacebookURL,
		core.FormatExternalURL(RC.InstagramURL) AS InstagramURL,
		RC.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(RC.ISOCountryCode2) AS CountryName,
		core.FormatExternalURL(RC.LinkedInURL) AS LinkedInURL,
		RC.Links AS Links,
		RC.LogoURL,
		RC.Municipality,
		RC.PostalCode,
		RC.Region,
		RC.ReviewingCompanyID,
		RC.ReviewingCompanyName,
		RC.ReviewPeriodAudio,
		RC.ReviewPeriodEBook,
		RC.ReviewPeriodEBookDelayed,
		RC.ReviewPeriodPrint,
		RC.StatusID,
		core.FormatExternalURL(RC.TwitterURL) AS TwitterURL,
		RC.UpdateDateTime,
		core.FormatDateTime(RC.UpdateDateTime) AS UpdateDateTimeFormatted,
		RC.UpdatePersonID,
		person.FormatPersonNameByPersonID(RC.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		core.FormatExternalURL(RC.WebsiteURL) AS WebsiteURL,
		RR.ReviewRatingID AS MinimumReviewRatingID,
		RR.ReviewRatingName,
		S.StatusID,
		S.StatusName
	FROM company.ReviewingCompany RC
		JOIN dropdown.ReviewRating RR ON RR.ReviewRatingID = RC.MinimumReviewRatingID
		JOIN dropdown.Status S ON S.StatusID = RC.StatusID
			AND RC.ReviewingCompanyID = @ReviewingCompanyID

	--ReviewingCompanyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ReviewingCompany'
			AND DE.EntityTypeSubCode = 'Logo'
			AND DE.EntityID = @ReviewingCompanyID

	--ReviewingCompanyGenre
	IF @ReviewingCompanyID > 0
		BEGIN

		SELECT
			G.GenreCode,
			G.GenreID,
			G.GenreName
		FROM company.ReviewingCompanyGenre RCG
			JOIN dropdown.Genre G ON G.GenreID = RCG.GenreID
				AND RCG.ReviewingCompanyID = @ReviewingCompanyID
		ORDER BY G.DisplayOrder, G.GenreName

		END
	ELSE
		BEGIN

		SELECT
			G.GenreCode,
			G.GenreID,
			G.GenreName
		FROM dropdown.Genre G
		ORDER BY G.DisplayOrder, G.GenreName

		END
	--ENDIF

END
GO
--End procedure company.GetReviewingCompanyByReviewingCompanyID

--Begin procedure dropdown.GetAuthorNewsCategoryData
EXEC Utility.DropObject 'dropdown.GetAuthorNewsCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.AuthorNewsCategory table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetAuthorNewsCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AuthorNewsCategoryID,
		T.AuthorNewsCategoryName
	FROM dropdown.AuthorNewsCategory T
	WHERE (T.AuthorNewsCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AuthorNewsCategoryName, T.AuthorNewsCategoryID

END
GO
--End procedure dropdown.GetAuthorNewsCategoryData

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetFormatData
EXEC Utility.DropObject 'dropdown.GetFormatData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Format table
-- =================================================================================
CREATE PROCEDURE dropdown.GetFormatData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FormatID,
		T.FormatName
	FROM dropdown.Format T
	WHERE (T.FormatID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FormatName, T.FormatID

END
GO
--End procedure dropdown.GetFormatData

--Begin procedure dropdown.GetGenreData
EXEC Utility.DropObject 'dropdown.GetGenreData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Genre table
-- ============================================================================
CREATE PROCEDURE dropdown.GetGenreData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.GenreID,
		T.GenreCode,
		T.GenreName,
		T.HasHeatLevel
	FROM dropdown.Genre T
	WHERE (T.GenreID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.GenreName, T.GenreID

END
GO
--End procedure dropdown.GetGenreData

--Begin procedure dropdown.GetHeatLevelData
EXEC Utility.DropObject 'dropdown.GetHeatLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.HeatLevel table
-- ================================================================================
CREATE PROCEDURE dropdown.GetHeatLevelData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.HeatLevelID,
		T.HeatLevelCode,
		T.HeatLevelName
	FROM dropdown.HeatLevel T
	WHERE (T.HeatLevelID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.HeatLevelName, T.HeatLevelID

END
GO
--End procedure dropdown.GetHeatLevelData

--Begin procedure dropdown.GetLinkTypeData
EXEC Utility.DropObject 'dropdown.GetLinkTypeData'
GO
--End procedure dropdown.GetLinkTypeData

--Begin procedure dropdown.GetMembershipTypeData
EXEC Utility.DropObject 'dropdown.GetMembershipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.MembershipType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetMembershipTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MembershipTypeID,
		T.MembershipAmount,
		CAST(T.MembershipAmount / T.MembershipTerm AS NUMERIC(18,2)) AS MembershipAmountMonthly,
		T.MembershipTerm,
		T.MembershipTypeName
	FROM dropdown.MembershipType T
	WHERE (T.MembershipTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MembershipTypeName, T.MembershipTypeID

END
GO
--End procedure dropdown.GetMembershipTypeData

--Begin procedure dropdown.GetPublishingCompanyRoleData
EXEC Utility.DropObject 'dropdown.GetPublishingCompanyRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.PublishingCompanyRole table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetPublishingCompanyRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PublishingCompanyRoleID,
		T.PublishingCompanyRoleCode,
		T.PublishingCompanyRoleName
	FROM dropdown.PublishingCompanyRole T
	WHERE (T.PublishingCompanyRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PublishingCompanyRoleName, T.PublishingCompanyRoleID

END
GO
--End procedure dropdown.GetPublishingCompanyRoleData

--Begin procedure dropdown.GetReviewingCompanyRoleData
EXEC Utility.DropObject 'dropdown.GetReviewingCompanyRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ReviewingCompanyRole table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetReviewingCompanyRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ReviewingCompanyRoleID,
		T.ReviewingCompanyRoleCode,
		T.ReviewingCompanyRoleName
	FROM dropdown.ReviewingCompanyRole T
	WHERE (T.ReviewingCompanyRoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ReviewingCompanyRoleName, T.ReviewingCompanyRoleID

END
GO
--End procedure dropdown.GetReviewingCompanyRoleData

--Begin procedure dropdown.GetReviewRatingData
EXEC Utility.DropObject 'dropdown.GetReviewRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ReviewRating table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetReviewRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ReviewRatingID,
		T.ReviewRatingCode,
		T.ReviewRatingName
	FROM dropdown.ReviewRating T
	WHERE (T.ReviewRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ReviewRatingName, T.ReviewRatingID

END
GO
--End procedure dropdown.GetReviewRatingData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ============================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSiteAdLocationData
EXEC Utility.DropObject 'dropdown.GetSiteAdLocationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdLocation table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdLocationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdLocationID,
		T.SiteAdLocationName
	FROM dropdown.SiteAdLocation T
	WHERE (T.SiteAdLocationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdLocationName, T.SiteAdLocationID

END
GO
--End procedure dropdown.GetSiteAdLocationData

--Begin procedure dropdown.GetSiteAdStatusData
EXEC Utility.DropObject 'dropdown.GetSiteAdStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdStatusID,
		T.SiteAdStatusName
	FROM dropdown.SiteAdStatus T
	WHERE (T.SiteAdStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdStatusName, T.SiteAdStatusID

END
GO
--End procedure dropdown.GetSiteAdStatusData

--Begin procedure dropdown.GetSiteAdTypeData
EXEC Utility.DropObject 'dropdown.GetSiteAdTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.SiteAdType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSiteAdTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SiteAdTypeID,
		T.SiteAdTypeName
	FROM dropdown.SiteAdType T
	WHERE (T.SiteAdTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SiteAdTypeName, T.SiteAdTypeID

END
GO
--End procedure dropdown.GetSiteAdTypeData

--Begin procedure dropdown.GetStatusData
EXEC Utility.DropObject 'dropdown.GetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Status table
-- =============================================================================
CREATE PROCEDURE dropdown.GetStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE (T.StatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure dropdown.GetVendorData
EXEC Utility.DropObject 'dropdown.GetVendorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Vendor table
-- =============================================================================
CREATE PROCEDURE dropdown.GetVendorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VendorID,
		T.VendorCode,
		T.VendorName
	FROM dropdown.Vendor T
	WHERE (T.VendorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VendorName, T.VendorID

END
GO
--End procedure dropdown.GetVendorData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.Address1,
		P.Address2,
		P.Address3,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.EmailAddress,
		P.FirstName,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsEmailAddressVerified,
		P.IsNewsLetterSubscriber,
		P.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.ISOCountryCode2) AS CountryName,
		P.IsSuperAdministrator,
		P.IsUpdateSubscriber,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.Municipality,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PasswordSecurityQuestion,
		P.PasswordSecurityQuestionAnswer,
		P.PersonID,
		P.PostalCode,
		P.Region,
		P.Suffix,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	--PersonRole
	SELECT
		R.RoleID,
		R.RoleCode,
		R.RoleName
	FROM dropdown.Role R
		JOIN person.PersonRole PR ON PR.RoleID = R.RoleID
			AND PR.PersonID = @PersonID
	ORDER BY 3, 2, 1

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure review.GetReviewRequestByReviewRequestID
EXEC Utility.DropObject 'review.GetReviewRequestByReviewRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.02
-- Description:	A stored procedure to return data from the review.ReviewRequest table
-- ==================================================================================
CREATE PROCEDURE review.GetReviewRequestByReviewRequestID

@ReviewRequestID INT = 0,
@BookID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @ReviewRequestID > 0
		SELECT @BookID = RR.BookID FROM review.ReviewRequest RR WHERE RR.ReviewRequestID = @ReviewRequestID
	--ENDIF

	--ReviewRequest
	SELECT
		RR.BookID,
		RR.IsArchived,
		RR.NotifyEmailAddress,
		RR.RequestPersonID,
		RR.ReviewRequestDateTime,
		RR.ReviewRequestID,
		RR.ReviewRequestNotes,
		F.FormatID,
		F.FormatName,
		HL.HeatLevelID,
		HL.HeatLevelName
	FROM review.ReviewRequest RR
		JOIN dropdown.Format F ON F.FormatID = RR.FormatID
		JOIN dropdown.HeatLevel HL ON HL.HeatLevelID = RR.HeatLevelID
			AND RR.ReviewRequestID = @ReviewRequestID

	--ReviewRequestBook
	SELECT 
		B.BookID, 
		B.Title,

		CASE
			WHEN EXISTS (SELECT 1 FROM book.BookGenre BG JOIN dropdown.Genre G ON G.GenreID = BG.GenreID AND BG.BookID = B.BookID AND G.HasHeatLevel = 1)
			THEN 1
			ELSE 0
		END AS HasHeatLevel

	FROM book.Book B
	WHERE B.BookID = @BookID

	--ReviewRequestDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
		D.DocumentTitle,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'ReviewRequest'
			AND DE.EntityID = @ReviewRequestID

	--ReviewRequestReviewingCompany
	SELECT
		RC.ReviewingCompanyID,
		RC.ReviewingCompanyName,

		CASE
			WHEN EXISTS (SELECT 1 FROM review.Review R WHERE R.ReviewingCompanyID = RC.ReviewingCompanyID AND R.ReviewRequestID = @ReviewRequestID)
			THEN 1
			ELSE 0
		END AS IsSelected

	FROM company.ReviewingCompany RC
	WHERE EXISTS
		(
		SELECT 1
		FROM company.ReviewingCompanyGenre RCG
			JOIN book.BookGenre BG ON BG.GenreID = RCG.GenreID
				AND BG.BookID = @BookID
				AND RCG.ReviewingCompanyID = RC.ReviewingCompanyID

		UNION

		SELECT 1
		FROM review.Review R
		WHERE R.ReviewRequestID = @ReviewRequestID
			AND R.ReviewingCompanyID = RC.ReviewingCompanyID
		)
	ORDER BY RC.ReviewingCompanyName, RC.ReviewingCompanyID

END
GO
--End procedure review.GetReviewRequestByReviewRequestID	

--End file Build File - 04 - Procedures.sql

--Begin file Build File - 05 - Data.sql
USE ManicReaders
GO

--Begin table core.EmailTemplate
TRUNCATE TABLE core.EmailTemplate
GO

INSERT INTO core.EmailTemplate
	(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
VALUES
	('Person', 'ForgotPasswordRequest', 'Sent when a person has used the forgot password feature', 'DeployAdviser Forgot Password Request', '<p>A forgot password request has been received for your DeployAdviser account.</p><p>If you did not request a password change please report this message to your administrator.</p><p>You may log in to the [[PasswordTokenLink]] system to set reset your password.</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the DeployAdviser system.</p><p>&nbsp;</p><p>Thank you,<br />The DeployAdviser Team</p>')
GO
--End table core.EmailTemplate

--Begin table core.EmailTemplateField
TRUNCATE TABLE core.EmailTemplateField
GO

INSERT INTO core.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription)
VALUES
	('Any', '[[SiteLink]]', 'Embedded Site Link'),
	('Any', '[[SiteURL]]', 'Site URL'),
	('Person', '[[EmailAddress]]', 'Sender Email'),
	('Person', '[[PasswordTokenLink]]', 'Embedded Reset Password Link'),
	('Person', '[[PasswordTokenURL]]', 'Site Reset Password URL'),
	('Person', '[[PersonNameFormattedLong]]', 'Sender Name (Long)'),
	('Person', '[[PersonNameFormattedShort]]', 'Sender Name (Short)')
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Author',
	@EntityTypeName = 'Author',
	@EntityTypeNamePlural = 'Authors',
	@SchemaName = 'author',
	@TableName = 'Author',
	@PrimaryKeyFieldName = 'AuthorID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Book',
	@EntityTypeName = 'Book',
	@EntityTypeNamePlural = 'Books',
	@SchemaName = 'book',
	@TableName = 'Book',
	@PrimaryKeyFieldName = 'BookID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Document',
	@EntityTypeName = 'Document',
	@EntityTypeNamePlural = 'Documents',
	@SchemaName = 'document',
	@TableName = 'Document',
	@PrimaryKeyFieldName = 'DocumentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PublishingCompany',
	@EntityTypeName = 'Publishing Company',
	@EntityTypeNamePlural = 'Publishing Companyies',
	@SchemaName = 'company',
	@TableName = 'PublishingCompany',
	@PrimaryKeyFieldName = 'PublishingCompanyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ReviewingCompany',
	@EntityTypeName = 'Reviewing Company',
	@EntityTypeNamePlural = 'Reviewing Companyies',
	@SchemaName = 'company',
	@TableName = 'ReviewingCompany',
	@PrimaryKeyFieldName = 'ReviewingCompanyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Review',
	@EntityTypeName = 'Review',
	@EntityTypeNamePlural = 'Reviews',
	@SchemaName = 'review',
	@TableName = 'Review',
	@PrimaryKeyFieldName = 'ReviewingID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'FormEncryptionKey', NULL, 'rt6j7NoLd0EHyg8VyRW0rw=='
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@manicreaders.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '365'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://mrdev.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'ManicReaders'
GO
--End table core.SystemSetup

--Begin table document.FileType
TRUNCATE TABLE document.FileType
GO

INSERT INTO document.FileType
	(Extension, MimeType)
VALUES
	('.doc', 'application/msword'),
	('.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('.gif', 'image/gif'),
	('.htm', 'text/html'),
	('.html', 'text/html'),
	('.jpeg', 'image/jpeg'),
	('.jpg', 'image/jpeg'),
	('.pdf', 'application/pdf'),
	('.png', 'image/png'),
	('.pps', 'application/mspowerpoint'),
	('.ppt', 'application/mspowerpoint'),
	('.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
	('.rtf', 'application/rtf'),
	('.txt', 'text/plain'),
	('.xls', 'application/excel'),
	('.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
GO
--End table document.FileType

--Begin table dropdown.AuthorNewsCategory
TRUNCATE TABLE dropdown.AuthorNewsCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.AuthorNewsCategory', 'AuthorNewsCategoryID', 0
GO

INSERT INTO dropdown.AuthorNewsCategory
	(AuthorNewsCategoryCode, AuthorNewsCategoryName)
VALUES
	('Book', 'Book'),
	('Contract', 'Contract'),
	('General', 'General'),
	('News', 'News'),
	('Promo', 'Promo'),
	('Review', 'Review')
GO
--End table document.AuthorNewsCategory

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder, IsActive) 
VALUES 
	(N'GB', N'GBR', N'United Kingdom', 3, 1),
	(N'AD', N'AND', N'Andorra', 100, 1),
	(N'AE', N'ARE', N'United Arab Emirates', 100, 1),
	(N'AF', N'AFG', N'Afghanistan', 100, 1),
	(N'AG', N'ATG', N'Antigua and Barbuda', 100, 1),
	(N'AI', N'AIA', N'Anguilla', 100, 1),
	(N'AL', N'ALB', N'Albania', 100, 1),
	(N'AM', N'ARM', N'Armenia', 100, 1),
	(N'AO', N'AGO', N'Angola', 100, 1),
	(N'AR', N'ARG', N'Argentina', 100, 1),
	(N'AS', N'ASM', N'American Samoa', 100, 1),
	(N'AT', N'AUT', N'Austria', 100, 1),
	(N'AU', N'AUS', N'Australia', 4, 1),
	(N'AW', N'ABW', N'Aruba', 100, 1),
	(N'AX', N'ALA', N'Ãland Islands', 100, 1),
	(N'AZ', N'AZE', N'Azerbaijan', 100, 1),
	(N'BA', N'BIH', N'Bosnia and Herzegovina', 100, 1),
	(N'BB', N'BRB', N'Barbados', 100, 1),
	(N'BD', N'BGD', N'Bangladesh', 100, 1),
	(N'BE', N'BEL', N'Belgium', 100, 1),
	(N'BF', N'BFA', N'Burkina Faso', 100, 1),
	(N'BG', N'BGR', N'Bulgaria', 100, 1),
	(N'BH', N'BHR', N'Bahrain', 100, 1),
	(N'BI', N'BDI', N'Burundi', 100, 1),
	(N'BJ', N'BEN', N'Benin', 100, 1),
	(N'BL', N'BLM', N'Saint BarthÃ©lemy', 100, 1),
	(N'BM', N'BMU', N'Bermuda', 100, 1),
	(N'BN', N'BRN', N'Brunei Darussalam', 100, 1),
	(N'BO', N'BOL', N'Bolivia', 100, 1),
	(N'BR', N'BRA', N'Brazil', 100, 1),
	(N'BS', N'BHS', N'Bahamas', 100, 1),
	(N'BT', N'BTN', N'Bhutan', 100, 1),
	(N'BU', N'BES', N'Bonaire, Sint Eustatius and Saba', 100, 1),
	(N'BV', N'BVT', N'Bouvet Island', 100, 1),
	(N'BW', N'BWA', N'Botswana', 100, 1),
	(N'BY', N'BLR', N'Belarus', 100, 1),
	(N'BZ', N'BLZ', N'Belize', 100, 1),
	(N'CA', N'CAN', N'Canada', 2, 1),
	(N'CC', N'CCK', N'Cocos (Keeling) Islands', 100, 1),
	(N'CD', N'COD', N'Congo, The Democratic Republic Of The', 100, 1),
	(N'CF', N'CAF', N'Central African Republic', 100, 1),
	(N'CG', N'COG', N'Congo', 100, 1),
	(N'CH', N'CHE', N'Switzerland', 100, 1),
	(N'CI', N'CIV', N'CÃ´te D''Ivoire', 100, 1),
	(N'CK', N'COK', N'Cook Islands', 100, 1),
	(N'CL', N'CHL', N'Chile', 100, 1),
	(N'CM', N'CMR', N'Cameroon', 100, 1),
	(N'CN', N'CHN', N'China', 100, 1),
	(N'CO', N'COL', N'Colombia', 100, 1),
	(N'CR', N'CRI', N'Costa Rica', 100, 1),
	(N'CU', N'CUB', N'Cuba', 100, 1),
	(N'CV', N'CPV', N'Cape Verde', 100, 1),
	(N'CW', N'CUW', N'CuraÃ§ao', 100, 1),
	(N'CX', N'CXR', N'Christmas Island', 100, 1),
	(N'CY', N'CYP', N'Cyprus', 100, 1),
	(N'CZ', N'CZE', N'Czech Republic', 100, 1),
	(N'DE', N'DEU', N'Germany', 100, 1),
	(N'DJ', N'DJI', N'Djibouti', 100, 1),
	(N'DK', N'DNK', N'Denmark', 100, 1),
	(N'DM', N'DMA', N'Dominica', 100, 1),
	(N'DO', N'DOM', N'Dominican Republic', 100, 1),
	(N'DZ', N'DZA', N'Algeria', 100, 1),
	(N'EC', N'ECU', N'Ecuador', 100, 1),
	(N'EE', N'EST', N'Estonia', 100, 1),
	(N'EG', N'EGY', N'Egypt', 100, 1),
	(N'EH', N'ESH', N'Western Sahara', 100, 1),
	(N'ER', N'ERI', N'Eritrea', 100, 1),
	(N'ES', N'ESP', N'Spain', 100, 1),
	(N'ET', N'ETH', N'Ethiopia', 100, 1),
	(N'EU', N'EUR', N'European Union', 100, 1),
	(N'FI', N'FIN', N'Finland', 100, 1),
	(N'FJ', N'FJI', N'Fiji', 100, 1),
	(N'FK', N'FLK', N'Falkland Islands', 100, 1),
	(N'FM', N'FSM', N'Micronesia, Federated States Of', 100, 1),
	(N'FO', N'FRO', N'Faroe Islands', 100, 1),
	(N'FR', N'FRA', N'France', 100, 1),
	(N'GA', N'GAB', N'Gabon', 100, 1),
	(N'GD', N'GRD', N'Grenada', 100, 1),
	(N'GE', N'GEO', N'Georgia', 100, 1),
	(N'GF', N'GUF', N'French Guiana', 100, 1),
	(N'GG', N'GGY', N'Guernsey', 100, 1),
	(N'GH', N'GHA', N'Ghana', 100, 1),
	(N'GI', N'GIB', N'Gibraltar', 100, 1),
	(N'GL', N'GRL', N'Greenland', 100, 1),
	(N'GM', N'GMB', N'Gambia', 100, 1),
	(N'GN', N'GIN', N'Guinea', 100, 1),
	(N'GP', N'GLP', N'Guadeloupe', 100, 1),
	(N'GQ', N'GNQ', N'Equatorial Guinea', 100, 1),
	(N'GR', N'GRC', N'Greece', 100, 1),
	(N'GT', N'GTM', N'Guatemala', 100, 1),
	(N'GU', N'GUM', N'Guam', 100, 1),
	(N'GW', N'GNB', N'Guinea-Bissau', 100, 1),
	(N'GY', N'GUY', N'Guyana', 100, 1),
	(N'HK', N'HKG', N'Hong Kong', 100, 1),
	(N'HM', N'HMD', N'Heard Island and McDonald Islands', 100, 1),
	(N'HN', N'HND', N'Honduras', 100, 1),
	(N'HR', N'HRV', N'Croatia', 100, 1),
	(N'HT', N'HTI', N'Haiti', 100, 1),
	(N'HU', N'HUN', N'Hungary', 100, 1),
	(N'ID', N'IDN', N'Indonesia', 100, 1),
	(N'IE', N'IRL', N'Ireland', 100, 1),
	(N'IL', N'ISR', N'Israel', 100, 1),
	(N'IM', N'IMN', N'Isle of Man', 100, 1),
	(N'IN', N'IND', N'India', 100, 1),
	(N'IO', N'IOT', N'British Indian Ocean Territory', 100, 1),
	(N'IQ', N'IRQ', N'Iraq', 100, 1),
	(N'IR', N'IRN', N'Iran, Islamic Republic Of', 100, 1),
	(N'IS', N'ISL', N'Iceland', 100, 1),
	(N'IT', N'ITA', N'Italy', 100, 1),
	(N'JE', N'JEY', N'Jersey', 100, 1),
	(N'JM', N'JAM', N'Jamaica', 100, 1),
	(N'JO', N'JOR', N'Jordan', 100, 1),
	(N'JP', N'JPN', N'Japan', 100, 1),
	(N'KE', N'KEN', N'Kenya', 100, 1),
	(N'KG', N'KGZ', N'Kyrgyzstan', 100, 1),
	(N'KH', N'KHM', N'Cambodia', 100, 1),
	(N'KI', N'KIR', N'Kiribati', 100, 1),
	(N'KM', N'COM', N'Comoros', 100, 1),
	(N'KN', N'KNA', N'Saint Kitts And Nevis', 100, 1),
	(N'KP', N'PRK', N'Korea, Democratic People''s Republic Of', 100, 1),
	(N'KR', N'KOR', N'Korea, Republic of', 100, 1),
	(N'KW', N'KWT', N'Kuwait', 100, 1),
	(N'KY', N'CYM', N'Cayman Islands', 100, 1),
	(N'KZ', N'KAZ', N'Kazakhstan', 100, 1),
	(N'LA', N'LAO', N'Lao People''s Democratic Republic', 100, 1),
	(N'LB', N'LBN', N'Lebanon', 100, 1),
	(N'LC', N'LCA', N'Saint Lucia', 100, 1),
	(N'LI', N'LIE', N'Liechtenstein', 100, 1),
	(N'LK', N'LKA', N'Sri Lanka', 100, 1),
	(N'LR', N'LBR', N'Liberia', 100, 1),
	(N'LS', N'LSO', N'Lesotho', 100, 1),
	(N'LT', N'LTU', N'Lithuania', 100, 1),
	(N'LU', N'LUX', N'Luxembourg', 100, 1),
	(N'LV', N'LVA', N'Latvia', 100, 1),
	(N'LY', N'LBY', N'Libya', 100, 1),
	(N'MA', N'MAR', N'Morocco', 100, 1),
	(N'MC', N'MCO', N'Monaco', 100, 1),
	(N'MD', N'MDA', N'Moldova, Republic of', 100, 1),
	(N'ME', N'MNE', N'Montenegro', 100, 1),
	(N'MF', N'MAF', N'Saint Martin (French Part)', 100, 1),
	(N'MG', N'MDG', N'Madagascar', 100, 1),
	(N'MH', N'MHL', N'Marshall Islands', 100, 1),
	(N'MK', N'MKD', N'Macedonia', 100, 1),
	(N'ML', N'MLI', N'Mali', 100, 1),
	(N'MM', N'MMR', N'Myanmar', 100, 1),
	(N'MN', N'MNG', N'Mongolia', 100, 1),
	(N'MO', N'MAC', N'Macao', 100, 1),
	(N'MP', N'MNP', N'Northern Mariana Islands', 100, 1),
	(N'MQ', N'MTQ', N'Martinique', 100, 1),
	(N'MR', N'MRT', N'Mauritania', 100, 1),
	(N'MS', N'MSR', N'Montserrat', 100, 1),
	(N'MT', N'MLT', N'Malta', 100, 1),
	(N'MU', N'MUS', N'Mauritius', 100, 1),
	(N'MV', N'MDV', N'Maldives', 100, 1),
	(N'MW', N'MWI', N'Malawi', 100, 1),
	(N'MX', N'MEX', N'Mexico', 100, 1),
	(N'MY', N'MYS', N'Malaysia', 100, 1),
	(N'MZ', N'MOZ', N'Mozambique', 100, 1),
	(N'NA', N'NAM', N'Namibia', 100, 1),
	(N'NC', N'NCL', N'New Caledonia', 100, 1),
	(N'NE', N'NER', N'Niger', 100, 1),
	(N'NF', N'NFK', N'Norfolk Island', 100, 1),
	(N'NG', N'NGA', N'Nigeria', 100, 1),
	(N'NI', N'NIC', N'Nicaragua', 100, 1),
	(N'NL', N'NLD', N'Netherlands', 100, 1),
	(N'NO', N'NOR', N'Norway', 100, 1),
	(N'NP', N'NPL', N'Nepal', 100, 1),
	(N'NR', N'NRU', N'Nauru', 100, 1),
	(N'NU', N'NIU', N'Niue', 100, 1),
	(N'NZ', N'NZL', N'New Zealand', 5, 1),
	(N'OM', N'OMN', N'Oman', 100, 1),
	(N'PA', N'PAN', N'Panama', 100, 1),
	(N'PE', N'PER', N'Peru', 100, 1),
	(N'PF', N'PYF', N'French Polynesia', 100, 1),
	(N'PG', N'PNG', N'Papua New Guinea', 100, 1),
	(N'PH', N'PHL', N'Philippines', 100, 1),
	(N'PK', N'PAK', N'Pakistan', 100, 1),
	(N'PL', N'POL', N'Poland', 100, 1),
	(N'PM', N'SPM', N'Saint Pierre And Miquelon', 100, 1),
	(N'PN', N'PCN', N'Pitcairn', 100, 1),
	(N'PR', N'PRI', N'Puerto Rico', 100, 1),
	(N'PT', N'PRT', N'Portugal', 100, 1),
	(N'PW', N'PLW', N'Palau', 100, 1),
	(N'PY', N'PRY', N'Paraguay', 100, 1),
	(N'QA', N'QAT', N'Qatar', 100, 1),
	(N'RE', N'REU', N'RÃ©union', 100, 1),
	(N'RO', N'ROU', N'Romania', 100, 1),
	(N'RS', N'SRB', N'Serbia', 100, 1),
	(N'RU', N'RUS', N'Russian Federation', 100, 1),
	(N'RW', N'RWA', N'Rwanda', 100, 1),
	(N'SA', N'SAU', N'Saudi Arabia', 100, 1),
	(N'SB', N'SLB', N'Solomon Islands', 100, 1),
	(N'SC', N'SYC', N'Seychelles', 100, 1),
	(N'SD', N'SDN', N'Sudan', 100, 1),
	(N'SE', N'SWE', N'Sweden', 100, 1),
	(N'SG', N'SGP', N'Singapore', 100, 1),
	(N'SH', N'SHN', N'Saint Helena, Ascension and Tristan Da Cunha', 100, 1),
	(N'SI', N'SVN', N'Slovenia', 100, 1),
	(N'SJ', N'SJM', N'Svalbard And Jan Mayen', 100, 1),
	(N'SK', N'SVK', N'Slovakia', 100, 1),
	(N'SL', N'SLE', N'Sierra Leone', 100, 1),
	(N'SM', N'SMR', N'San Marino', 100, 1),
	(N'SN', N'SEN', N'Senegal', 100, 1),
	(N'SO', N'SOM', N'Somalia', 100, 1),
	(N'SR', N'SUR', N'Suriname', 100, 1),
	(N'SS', N'SSD', N'South Sudan', 100, 1),
	(N'ST', N'STP', N'Sao Tome and Principe', 100, 1),
	(N'SV', N'SLV', N'El Salvador', 100, 1),
	(N'SX', N'SXM', N'Sint Maarten (Dutch part)', 100, 1),
	(N'SY', N'SYR', N'Syrian Arab Republic', 100, 1),
	(N'SZ', N'SWZ', N'Swaziland', 100, 1),
	(N'TC', N'TCA', N'Turks and Caicos Islands', 100, 1),
	(N'TD', N'TCD', N'Chad', 100, 1),
	(N'TF', N'ATF', N'French Southern Territories', 100, 1),
	(N'TG', N'TGO', N'Togo', 100, 1),
	(N'TH', N'THA', N'Thailand', 100, 1),
	(N'TJ', N'TJK', N'Tajikistan', 100, 1),
	(N'TK', N'TKL', N'Tokelau', 100, 1),
	(N'TL', N'TLS', N'Timor-Leste', 100, 1),
	(N'TM', N'TKM', N'Turkmenistan', 100, 1),
	(N'TN', N'TUN', N'Tunisia', 100, 1),
	(N'TO', N'TON', N'Tonga', 100, 1),
	(N'TR', N'TUR', N'Turkey', 100, 1),
	(N'TT', N'TTO', N'Trinidad and Tobago', 100, 1),
	(N'TV', N'TUV', N'Tuvalu', 100, 1),
	(N'TW', N'TWN', N'Taiwan', 100, 1),
	(N'TZ', N'TZA', N'Tanzania, United Republic of', 100, 1),
	(N'UA', N'UKR', N'Ukraine', 100, 1),
	(N'UG', N'UGA', N'Uganda', 100, 1),
	(N'UM', N'UMI', N'United States Minor Outlying Islands', 100, 1),
	(N'US', N'USA', N'United States', 1, 1),
	(N'UY', N'URY', N'Uruguay', 100, 1),
	(N'UZ', N'UZB', N'Uzbekistan', 100, 1),
	(N'VA', N'VAT', N'Holy See (Vatican City State)', 100, 1),
	(N'VC', N'VCT', N'Saint Vincent And The Grenadines', 100, 1),
	(N'VE', N'VEN', N'Venezuela, Bolivarian Republic of', 100, 1),
	(N'VG', N'VGB', N'Virgin Islands, British', 100, 1),
	(N'VI', N'VIR', N'Virgin Islands, U.S.', 100, 1),
	(N'VN', N'VNM', N'Viet Nam', 100, 1),
	(N'VU', N'VUT', N'Vanuatu', 100, 1),
	(N'WF', N'WLF', N'Wallis and Futuna', 100, 1),
	(N'WS', N'WSM', N'Samoa', 100, 1),
	(N'YE', N'YEM', N'Yemen', 100, 1),
	(N'YT', N'MYT', N'Mayotte', 100, 1),
	(N'ZA', N'ZAF', N'South Africa', 100, 1),
	(N'ZM', N'ZMB', N'Zambia', 100, 1),
	(N'ZW', N'ZWE', N'Zimbabwe', 100, 1)
GO
--End table dropdown.Country

--Begin table dropdown.Format
TRUNCATE TABLE dropdown.Format
GO

EXEC utility.InsertIdentityValue 'dropdown.Format', 'FormatID', 0
GO

INSERT INTO dropdown.Format
	(FormatName)
VALUES
	('Audio'),
	('E-Book'),
	('E-Pub'),
	('HTML'),
	('Kindle'),
	('PDF'),
	('Print')
GO
--End table dropdown.Format

--Begin table dropdown.HeatLevel
TRUNCATE TABLE dropdown.HeatLevel
GO

EXEC utility.InsertIdentityValue 'dropdown.HeatLevel', 'HeatLevelID', 0
GO

INSERT INTO dropdown.HeatLevel
	(HeatLevelCode,HeatLevelName,DisplayOrder)
VALUES
	('Erotic','Erotic',1),
	('EroticFF','Erotic - Female/Female',2),
	('EroticMM','Erotic - Male/Male',3),
	('EroticM','Erotic - Menage',4),
	('Hot','Hot',5),
	('Mainstream','Mainstream',6),
	('Sweet','Sweet',7)
GO
--End table document.Format

--Begin table dropdown.MembershipType
TRUNCATE TABLE dropdown.MembershipType
GO

EXEC utility.InsertIdentityValue 'dropdown.MembershipType', 'MembershipTypeID', 0
GO

INSERT INTO dropdown.MembershipType
	(MembershipTerm,MembershipTypeName,MembershipAmount,DisplayOrder,IsActive)
VALUES
	(3, 'Quarterly', 13.47, 3, 1),
	(1, 'Monthly', 5, 4, 1),
	(12, 'Annual', 35.88, 1, 1),
	(6, 'Semi Annual', 35.88, 2, 0)
GO
--End table dropdown.MembershipType

--Begin table dropdown.PublishingCompanyRole
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.PublishingCompanyRole', 'PublishingCompanyRoleID', 0
GO

INSERT INTO dropdown.PublishingCompanyRole
	(PublishingCompanyRoleCode, PublishingCompanyRoleName, DisplayOrder) 
VALUES 
	('Publisher', 'Publisher', 1)
GO
--End table dropdown.PublishingCompanyRole

--Begin table dropdown.ReviewingCompanyRole
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.ReviewingCompanyRole', 'ReviewingCompanyRoleID', 0
GO

INSERT INTO dropdown.ReviewingCompanyRole
	(ReviewingCompanyRoleCode, ReviewingCompanyRoleName, DisplayOrder) 
VALUES 
	('Administrator', 'Administrator', 1),
	('Editor', 'Editor', 2),
	('Reviewer', 'Reviewer', 3)
GO
--End table dropdown.ReviewingCompanyRole

--Begin table dropdown.ReviewRating
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.ReviewRating', 'ReviewRatingID', 0
GO

INSERT INTO dropdown.ReviewRating
	(ReviewRatingCode, ReviewRatingName, DisplayOrder) 
VALUES 
	('5', '5 - The best of the best', 1),
	('4.5', '4.5 - One of the best', 2),
	('4', '4 - Highly recommended', 3),
	('3.5', '3.5 - Definitely worth the time', 4),
	('3', '3 - Average', 5),
	('2.5', '2.5 - Below average', 6),
	('2', '2 - Poor', 7),
	('1.5', '1.5 - Really poor', 8),
	('1', '1 - Just plain bad', 9),
	('.5', '.5 - Not worth the time', 10),
	('0', '0 - Worst of the worst', 11)
GO
--End table dropdown.ReviewRating

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role
	(RoleCode, RoleName, DisplayOrder) 
VALUES 
	('Author', 'Author', 2),
	('PublishingCompany', 'PublishingCompany', 3),
	('Reader', 'Reader', 1),
	('Reviewer', 'Reviewer', 4)
GO
--End table dropdown.Role

--Begin table dropdown.SiteAdLocation
TRUNCATE TABLE dropdown.SiteAdLocation
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdLocation', 'SiteAdLocationID', 0
GO

INSERT INTO dropdown.SiteAdLocation
	(SiteAdLocationCode, SiteAdLocationName)
VALUES
	('About', 'About'),
	('Any', 'Any'),
	('Authors', 'Authors'),
	('Books', 'Books'),
	('Home', 'Home'),
	('Publishers', 'Publishers'),
	('ReviewDetail', 'ReviewDetail')
GO
--End table document.SiteAdLocation

--Begin table dropdown.SiteAdStatus
TRUNCATE TABLE dropdown.SiteAdStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdStatus', 'SiteAdStatusID', 0
GO

INSERT INTO dropdown.SiteAdStatus
	(SiteAdStatusName)
VALUES
	('Active'),
	('Package'),
	('Package End')
GO
--End table document.SiteAdStatus

--Begin table dropdown.SiteAdType
TRUNCATE TABLE dropdown.SiteAdType
GO

EXEC utility.InsertIdentityValue 'dropdown.SiteAdType', 'SiteAdTypeID', 0
GO

INSERT INTO dropdown.SiteAdType
	(SiteAdTypeName)
VALUES
	('Free'),
	('Package'),
	('Paid')
GO
--End table document.SiteAdType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

EXEC utility.InsertIdentityValue 'dropdown.Status', 'StatusID', 0
GO

INSERT INTO dropdown.Status
	(StatusCode, StatusName, DisplayOrder) 
VALUES 
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Deleted', 'Deleted', 3)
GO
--End table dropdown.Status

--End file Build File - 05 - Data.sql

--Begin file Build File - 06 - Conversion.sql
USE ManicReaders
GO

--Begin table dropdown.Genre
TRUNCATE TABLE dropdown.Genre
GO

EXEC utility.InsertIdentityValue 'dropdown.Genre', 'GenreID', 0
GO

INSERT INTO dropdown.Genre
	(GenreName)
SELECT DISTINCT
	G.GenreDescription
FROM ManicReadersConv.manicreaders.tblGenre G
WHERE G.GenreDescription IS NOT NULL
ORDER BY G.GenreDescription
GO

UPDATE G
SET G.HasHeatLevel = 1
FROM dropdown.Genre G
WHERE G.GenreName IN
	(
	'African American',
	'Anthology',
	'BDSM',
	'Chick Lit',
	'Contemporary',
	'Contemporary Romance',
	'Erotic Contemporary Romance',
	'Erotic Fantasy',
	'Erotic Historical Romance',
	'Erotic Multiple Partner',
	'Erotic Paranormal Romance',
	'Erotic Romance',
	'Erotic Sci-Fi ',
	'Erotic Suspense',
	'Erotica',
	'Fantasy',
	'Fantasy Romance',
	'Fiction',
	'Futuristic/Time Travel',
	'GLBT',
	'GLBT Young Adult',
	'GLBT Multiple Partner',
	'Historical Fiction',
	'Historical Romance',
	'Interracial Romance',
	'Literary',
	'Literary Fiction',
	'Menage',
	'Occult',
	'Paranormal',
	'Paranormal Romance',
	'Retro',
	'Romance',
	'Romantic Comedy',
	'Romantic Suspense',
	'Rubenesque',
	'Urban Fantasy',
	'Urban Fantasy Romance',
	'Western Contemporary Romance',
	'Western Historical Romance'
	)
--End table dropdown.Genre

--Begin table dropdown.Vendor
TRUNCATE TABLE dropdown.Vendor
GO

EXEC utility.InsertIdentityValue 'dropdown.Vendor', 'VendorID', 0
GO

SET IDENTITY_INSERT dropdown.Vendor ON
GO

INSERT INTO dropdown.Vendor
	(VendorID, VendorName)
SELECT
	RV.RoyaltyVendorID,
	RV.RoyaltyVendorName
FROM ManicReadersConv.manicreaders.tblRoyaltyVendor RV
ORDER BY RV.RoyaltyVendorName
GO
--End table dropdown.Vendor

--Begin table author.Author
TRUNCATE TABLE author.Author
GO

SET IDENTITY_INSERT author.Author ON
GO

INSERT INTO author.Author
	(AuthorID,AuthorURL,Biography,CanChangeAuthorURL,CreateDateTime,FirstName,IsAdultContent,IsSpotlight,LastName,Links,MiddleName,Notes,PersonID,StatusID,UpdateDateTime,WebsiteURL)
SELECT
	A.AuthorID,
	core.StripCharacters(ISNULL(core.NullIfEmpty(A.FName), '') + ISNULL(core.NullIfEmpty(A.MName), '') + ISNULL(core.NullIfEmpty(A.LName), ''), '^A-Z') AS AuthorURL,
	A.Bio AS Biography,
	1 AS CanChangeAuthorURL,
	ISNULL(A.CreationDate, getDate()) AS CreateDateTime,
	A.FName AS FirstName,
	CASE WHEN A.AdultFlag = 1 THEN A.AdultFlag ELSE 0 END AS IsAdultContent,
	CASE WHEN A.Spotlight = 1 THEN A.Spotlight ELSE 0 END AS IsSpotlight,
	A.LName AS LastName,
	A.Links,
	A.MName AS MiddleName,
	A.Note,

	CASE 
		WHEN A.UserID IS NULL OR NOT EXISTS (SELECT 1 FROM ManicReadersConv.manicreaders.tblUser U WHERE U.UserID = A.UserID)
		THEN 0
		ELSE A.UserID
	END AS PersonID,

	CASE
		WHEN A.Status IN ('Active','Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = A.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL(A.LastUpdateDate, getDate()) AS UpdateDateTime,
	A.Website
FROM ManicReadersConv.manicreaders.tblAuthor A
ORDER BY A.AuthorID
GO

SET IDENTITY_INSERT author.Author OFF
GO

UPDATE A
SET 
	A.MembershipStartDate = M.StartDate,
	A.MembershipEndDate = M.EndDate,
	A.MembershipTerm = 
		CASE 
			WHEN M.Duration IN ('Monthly','Monthyl')
			THEN 1
			WHEN M.Duration IN ('Qarterly','Quarteerly','Quarterly')
			THEN 3
			WHEN M.Duration = '6 Month'
			THEN 6
			ELSE 12
		END

FROM manicreadersconv.manicreaders.tblMembership M
	JOIN author.Author A ON A.PersonID = M.UserID
GO
--End table author.Author

--Begin table author.AuthorContest
TRUNCATE TABLE author.AuthorContest
GO

INSERT INTO author.AuthorContest
	(AuthorID,Title,ContestURL,IsActive,Description,CreateDateTime)
SELECT
	TAC.AuthorID,
	TAC.Title,
	TAC.Link,
	CASE WHEN TAC.Status = 'Active' THEN 1 ELSE 0 END AS IsActive,
	TAC.Body AS AuthorContestDescription, 
	TAC.CreateDate AS CreateDateTime
FROM manicreadersconv.manicreaders.tblauthorcontestannouncement TAC
WHERE core.NullIfEmpty(TAC.Body) IS NOT NULL
GO
--End table author.AuthorContest

--Begin table author.AuthorEmbeddedMedia
TRUNCATE TABLE author.AuthorEmbeddedMedia
GO

INSERT INTO author.AuthorEmbeddedMedia
	(AuthorID, EmbeddedMedia, EmbeddedMediaTypeCode)
SELECT
	A.AuthorID,
	A.PodCast,
	'PodCast'
FROM ManicReadersConv.manicreaders.tblAuthor A
WHERE core.NullIfEmpty(A.PodCast) IS NOT NULL

UNION

SELECT
	A.AuthorID,
	A.Video,
	'Video'
FROM ManicReadersConv.manicreaders.tblAuthor A
WHERE core.NullIfEmpty(A.Video) IS NOT NULL

ORDER BY A.AuthorID
GO
--End table author.AuthorEmbeddedMedia

--Begin table author.AuthorGenre
TRUNCATE TABLE author.AuthorGenre
GO

INSERT INTO author.AuthorGenre
	(AuthorID,GenreID)
SELECT
	TAG.AuthorID,
	(SELECT G.GenreID FROM dropdown.Genre G WHERE G.GenreName = TG.GenreDescription)
FROM ManicReadersConv.manicreaders.tblAuthorGenre TAG
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = TAG.GenreID
ORDER BY TAG.AuthorID, TAG.GenreID
GO
--End table author.AuthorGenre

--Begin table author.AuthorNews
TRUNCATE TABLE author.AuthorNews
GO

INSERT INTO author.AuthorNews
	(AuthorID,AuthorNewsCategoryID,IsActive,Description,CreateDateTime)
SELECT
	TAN.AuthorID,
	ISNULL((SELECT ANC.AuthorNewsCategoryID FROM dropdown.AuthorNewsCategory ANC WHERE ANC.AuthorNewsCategoryName = TAN.Title), 0) AS AuthorNewsCategoryID,
	CASE WHEN TAN.Status = 'Active' THEN 1 ELSE 0 END AS IsActive,
	TAN.Body AS AuthorNewsDescription, 
	TAN.CreateDate AS CreateDateTime
FROM manicreadersconv.manicreaders.tblAuthorNews TAN
WHERE core.NullIfEmpty(TAN.Body) IS NOT NULL
GO
--End table author.AuthorNews

--Begin table author.AuthorNote
INSERT INTO author.AuthorNote
	(AuthorID,Notes)
SELECT
	A.AuthorID,
	core.NullIfEmpty(M.Notes) AS Notes
FROM manicreadersconv.manicreaders.tblMembership M
	JOIN author.Author A ON A.PersonID = M.UserID
		AND core.NullIfEmpty(M.Notes) IS NOT NULL
ORDER BY M.UserID
GO
--End table author.AuthorNote

--Begin table author.AuthorRoyalty
TRUNCATE TABLE author.AuthorRoyalty
GO

INSERT INTO author.AuthorRoyalty
	(AuthorID,BookID,StartDate,EndDate,VendorID,ListPrice,RoyaltyAmount,QuantitySold)
SELECT 
	B.AuthorID,
	B.BookID,
	RP.StartDate,
	RP.EndDate,
	RT.RoyaltyVendorID,
	RT.ListPrice,
	RT.RoyaltiesEarned,
	RT.UnitSsold
FROM manicreadersconv.manicreaders.tblroyaltytransaction RT
	 JOIN manicreadersconv.manicreaders.tblroyaltyperiod RP ON RP.royaltyperiodid = RT.royaltyperiodid
	 JOIN manicreadersconv.manicreaders.tblBook B ON B.BookID = RP.BookID
ORDER BY 1
GO
--End table author.AuthorRoyalty

--Begin table book.Book
TRUNCATE TABLE book.Book
GO

SET IDENTITY_INSERT book.Book ON
GO

INSERT INTO book.Book
	(Advance,BookID,CoverArtists,CreateDateTime,IsFreeRead,Excerpt,PublishingCompanyID,PurchaseURL,ReleaseDate,StatusID,Summary,Title)
SELECT
	ISNULL(B.Advance, 0) AS Advance,
	B.BookID,
	B.CoverArtists,
	ISNULL(B.CreationDate, getDate()) AS CreateDateTime,
	CASE WHEN core.NullIfEmpty(B.FreeRead) IS NULL THEN 0 ELSE 1 END,
	B.Excerpt,
	ISNULL(B.PublisherID, 0) AS PublishingCompanyID,
	B.BuyLink,
	B.ReleaseDate,

	CASE
		WHEN B.Status = 'Active'
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = B.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Deleted')
	END AS StatusID,

	B.Blurb AS Summary,
	LTRIM(core.NullIfEmpty(REPLACE(B.Title, CHAR(9), ''))) AS Title 
FROM ManicReadersConv.manicreaders.tblBook B
ORDER BY B.BookID
GO

SET IDENTITY_INSERT book.Book OFF
GO
--End table book.Book

--Begin table book.BookAuthor
TRUNCATE TABLE book.BookAuthor
GO

INSERT INTO book.BookAuthor
	(AuthorID,BookID)
SELECT
	B.AuthorID,
	B.BookID
FROM ManicReadersConv.manicreaders.tblBook B
ORDER BY B.BookID
GO
--End table book.BookAuthor

--Begin table book.BookFormat
TRUNCATE TABLE book.BookFormat
GO

INSERT INTO book.BookFormat
	(BookID,BookLength,FormatID,ISBN,ListPrice)
SELECT
	BF.BookID,
	B.Pages AS BookLength,
	ISNULL(
		(
		SELECT F.FormatID 
		FROM dropdown.Format F 
		WHERE F.FormatName = 
			CASE 
				WHEN BF.Format IN ('E-Book / Print', 'eBook')
				THEN 'E-Book'
				WHEN BF.Format IN ('EPUB')
				THEN 'E-Pub'
				ELSE BF.Format
			END
		), 0) AS FormatID,
	BF.ISBN,
	BF.ListPrice
FROM ManicReadersConv.manicreaders.tblBookFormat BF
	JOIN ManicReadersConv.manicreaders.tblBook B ON B.BookID = BF.BookID
ORDER BY BF.BookID, BF.BookFormatID
GO
--End table book.BookFormat

--Begin table book.BookGenre
TRUNCATE TABLE book.BookGenre
GO

INSERT INTO book.BookGenre
	(BookID,GenreID)
SELECT
	B.BookID,
	(SELECT G.GenreID FROM dropdown.Genre G WHERE G.GenreName = TG.GenreDescription) AS GenreID
FROM ManicReadersConv.manicreaders.tblBook B
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = B.GenreID
ORDER BY B.BookID
GO
--End table book.BookGenre

--Begin table book.BookPersonSubscription
TRUNCATE TABLE book.BookPersonSubscription
GO

INSERT INTO book.BookPersonSubscription
	(BookID,PersonID)
SELECT
	BCS.BookID,
	BCS.UserID
FROM ManicReadersConv.manicreaders.tblBookCommentSubscription BCS
GO
--End table book.BookPersonSubscription

--Begin table company.PublishingCompany
TRUNCATE TABLE company.PublishingCompany
GO

EXEC utility.InsertIdentityValue 'company.PublishingCompany', 'PublishingCompanyID', 0
GO

SET IDENTITY_INSERT company.PublishingCompany ON
GO

INSERT INTO company.PublishingCompany
	(Address1,Description,EmailAddress,Links,Municipality,PublishingCompanyID,PublishingCompanyName,Region,StatusID,SubmissionGuidelines,UpdateDateTime,WebsiteURL)
SELECT
	core.NullIfEmpty(P.Address) AS Address1,
	core.NullIfEmpty(P.Description) AS Description,
	core.NullIfEmpty(P.Email) AS EmailAddress,
	core.NullIfEmpty(P.Links) AS Links,
	core.NullIfEmpty(P.City) AS Municipality,
	P.PublisherID AS PublishingCompanyID,
	LTRIM(core.NullIfEmpty(REPLACE(P.Name, CHAR(9), ''))) AS PublishingCompanyName, 
	core.NullIfEmpty(P.State) AS Region,

	CASE
		WHEN P.Status IN ('Active', 'Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = P.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	core.NullIfEmpty(P.SubmissionGuidelines) AS SubmissionGuidelines,
	core.NullIfEmpty(P.LastUpdateDate) AS UpdateDateTime,
	P.Website
FROM ManicReadersConv.manicreaders.tblPublisher P
ORDER BY P.PublisherID
GO

SET IDENTITY_INSERT company.PublishingCompany OFF
GO
--End table company.PublishingCompany

--Begin table company.ReviewingCompany
TRUNCATE TABLE company.ReviewingCompany
GO

SET IDENTITY_INSERT company.ReviewingCompany ON
GO

INSERT INTO company.ReviewingCompany
	(Description,EmailAddress,Links,LogoURL,MinimumReviewRatingID,ReviewingCompanyID,ReviewingCompanyName,ReviewPeriodAudio,ReviewPeriodEBook,ReviewPeriodEBookDelayed,ReviewPeriodPrint,StatusID,UpdateDateTime,WebsiteURL)
SELECT
	core.NullIfEmpty(R.Bio) AS Description,
	core.NullIfEmpty(R.Email) AS EmailAddress,
	core.NullIfEmpty(R.Links) AS Links,
	core.NullIfEmpty(R.BragIconLink) AS LogoURL,
	ISNULL((SELECT RR.ReviewRatingID FROM dropdown.ReviewRating RR WHERE CAST(RR.ReviewRatingCode AS NUMERIC(3,1)) = CAST(ISNULL(core.NullIfEmpty(R.LowRating), '0') AS NUMERIC(3,1))), 0) AS MinimumReviewRatingID,
	R.ReviewCompanyID AS ReviewingCompanyID,
	LTRIM(core.NullIfEmpty(REPLACE(R.CompanyName, CHAR(9), ''))) AS ReviewingCompanyName, 
	ISNULL(R.AudioDue, 0) AS ReviewPeriodAudio,
 	ISNULL(R.EBookDue, 0) AS ReviewPeriodEBook,
 	ISNULL(R.EBookDelayedDue, 0) AS ReviewPeriodEBookDelayed,
 	ISNULL(R.PrintDue, 0) AS ReviewPeriodPrint,

	CASE
		WHEN R.Status IN ('Active', 'Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = R.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL(core.NullIfEmpty(R.LastUpdateDate), getDate()) AS UpdateDateTime,
	R.Website
FROM ManicReadersConv.manicreaders.tblReviewCompany R
ORDER BY R.ReviewCompanyID
GO

SET IDENTITY_INSERT company.ReviewingCompany OFF
GO
--End table company.ReviewingCompany

--Begin table company.ReviewingCompanyGenre
TRUNCATE TABLE company.ReviewingCompanyGenre
GO

DECLARE @tTable TABLE (ReviewCompanyID INT, GenreID INT NOT NULL DEFAULT 0)

INSERT INTO @tTable
	(ReviewCompanyID,GenreID)
SELECT
	TRCG.ReviewCompanyID,
	TG.GenreID
FROM ManicReadersConv.manicreaders.tblReviewCompanyGenre TRCG, ManicReadersConv.manicreaders.tblGenre TG

DELETE T
FROM @tTable T
WHERE EXISTS
	(
	SELECT 1
	FROM ManicReadersConv.manicreaders.tblReviewCompanyGenre TRCG
		JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = TRCG.GenreID
			AND TRCG.ReviewCompanyID = T.ReviewCompanyID
			AND TRCG.GenreID = T.GenreID
	)

INSERT INTO company.ReviewingCompanyGenre
	(ReviewingCompanyID,GenreID)
SELECT
	T.ReviewCompanyID,
	G.GenreID
FROM @tTable T
	JOIN ManicReadersConv.manicreaders.tblGenre TG ON TG.GenreID = T.GenreID
	JOIN dropdown.Genre G ON G.GenreName = TG.GenreDescription
ORDER BY T.ReviewCompanyID, T.GenreID
GO
--End table company.ReviewingCompanyGenre

--Begin table core.SiteAd
TRUNCATE TABLE core.SiteAd
GO

INSERT INTO core.SiteAd
	(SiteAdLocationID,SiteAdStatusID,SiteAdTypeID,BookID,EmailAddress,EndDate,InvoiceDetails,Notes,StartDate)
SELECT
	ISNULL((SELECT SAL.SiteAdLocationID FROM dropdown.SiteAdLocation SAL WHERE SAL.SiteAdLocationName = BA.AdPage), 0) AS SiteAdLocationID,
	ISNULL((SELECT SAS.SiteAdStatusID FROM dropdown.SiteAdStatus SAS WHERE SAS.SiteAdStatusName = BA.Status), 0) AS SiteAdStatusID,
	ISNULL((SELECT SAT.SiteAdTypeID FROM dropdown.SiteAdType SAT WHERE SAT.SiteAdTypeName = BA.AdType), 0) AS SiteAdTypeID,
	BA.BookID,
	BA.ContactEmail AS ContactEmailAddress,
	BA.ExpirationDate AS EndDate,
	BA.InvoiceDetails,
	BA.AdNotes AS Notes,
	BA.PostDate AS StartDate
FROM ManicReadersConv.manicreaders.tblBookAd BA
ORDER BY BA.BookID
GO
--End table core.SiteAd

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

EXEC utility.InsertIdentityValue 'person.Person', 'PersonID', 0
GO

SET IDENTITY_INSERT person.Person ON
GO

INSERT INTO person.Person
	(CreateDateTime,EmailAddress,FirstName,InvalidLoginAttempts,IsAccountLockedOut,IsActive,IsEmailAddressVerified,IsNewsLetterSubscriber,IsSuperAdministrator,IsUpdateSubscriber,LastLoginDateTime,LastName,MiddleName,Municipality,Password,PasswordExpirationDateTime,PasswordSalt,PasswordSecurityQuestion,PasswordSecurityQuestionAnswer,PersonID,Region,UserName)
SELECT
	core.NullIfEmpty(U.CreationDate) AS CreateDateTime,
	core.NullIfEmpty(U.Email) AS EmailAddress,
	core.NullIfEmpty(U.FName) AS FirstName,
	0 AS InvalidLoginAttempts,
	0 AS IsAccountLockedOut,
	1 AS IsActive,
	1 AS IsEmailAddressVerified,
	CASE WHEN U.NewsletterEmail = 1 THEN 1 ELSE 0 END AS IsNewsLetterSubscriber,
	0 AS IsSuperAdministrator,
	CASE WHEN U.UpdateEmail = 1 THEN 1 ELSE 0 END AS IsUpdateSubscriber,
	core.NullIfEmpty(U.LastLoginDate) AS LastLoginDateTime,
	core.NullIfEmpty(U.LName) AS LastName,
	core.NullIfEmpty(U.MName) AS MiddleName,
	core.NullIfEmpty(U.City) AS Municipality,
	core.NullIfEmpty(U.Password),
	core.NullIfEmpty(U.LastPasswordChangedDate) AS PasswordExpirationDateTime,
	newID() AS PasswordSalt,
	core.NullIfEmpty(U.PasswordQuestion) AS PasswordSecurityQuestion,
	core.NullIfEmpty(U.PasswordAnswer) AS PasswordSecurityQuestionAnswer,
	U.UserID AS PersonID,
	core.NullIfEmpty(U.State) AS Region,
	core.NullIfEmpty(U.Username)
FROM ManicReadersConv.manicreaders.tblUser U
ORDER BY U.UserID
GO

SET IDENTITY_INSERT person.Person OFF
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, EmailAddress, LastLoginDateTime, InvalidLoginAttempts, IsAccountLockedOut, IsActive, IsSuperAdministrator, Password, PasswordSalt, PasswordExpirationDateTime) 
VALUES
	('John', 'Lyons', 'Mr.', 'jlyons', 'john.lyons@oceandisc.com', NULL, 0, 0, 1, 0, '138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', '71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'kevin.ross@oceandisc.com', NULL, 0, 0, 1, 0, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Test', 'ReviewingCompany Admin', 'Mr.', 'ReviewingCompanyTestAdmin', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime))
GO

UPDATE P
SET 
	P.Password = person.HashPassword(P.Password, P.PasswordSalt),
	P.UserName = 
		CASE P.UserName 
			WHEN 'authortest' 
			THEN 'AuthorTest' 
			WHEN 'testpublisher' 
			THEN 'PublishingCompanyTest' 
			WHEN 'TestReviewer555' 
			THEN 'ReviewingCompanyTest' 
			ELSE P.UserName 
		END
FROM person.Person P
WHERE P.Password IS NOT NULL
	AND P.UserName IN ('authortest', 'dannoparker', 'testpublisher', 'TestReviewer555')
GO

UPDATE P1
SET 
	P1.Password = (SELECT P2.Password FROM person.Person P2 WHERE P2.UserName = 'ReviewingCompanyTest'),
	P1.PasswordSalt = (SELECT P2.PasswordSalt FROM person.Person P2 WHERE P2.UserName = 'ReviewingCompanyTest')
FROM person.Person P1
WHERE P1.UserName = 'ReviewingCompanyTestAdmin'
GO
 --End table person.Person

--Begin table person.PersonPublishingCompanyRole
INSERT INTO person.PersonPublishingCompanyRole
	(PersonID,PublishingCompanyID,PublishingCompanyRoleID)
SELECT
	PU.UserID AS PersonID,
	PU.PublisherID,
	(SELECT PR.PublishingCompanyRoleID FROM dropdown.PublishingCompanyRole PR WHERE PR.PublishingCompanyRoleCode = 'Publisher')
FROM ManicReadersConv.manicreaders.tblPublisherUser PU
WHERE EXISTS
	(
	SELECT 1
	FROM person.Person P
	WHERE P.PersonID = PU.UserID
	)
ORDER BY PU.UserID
GO
--End table person.PersonPublishingCompanyRole

--Begin table person.PersonReviewingCompanyRole
INSERT INTO person.PersonReviewingCompanyRole
	(PersonID,ReviewingCompanyID,PenName,StatusID,BookLimit,ReviewingCompanyRoleID)
SELECT
	RCU.UserID AS PersonID,
	RCU.ReviewCompanyID AS ReviewingCompanyID,
	RCU.PenName,
	ISNULL((SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = RCU.Status), 0) AS StatusID,
	ISNULL((
	SELECT RBL.BookLimit 
	FROM ManicReadersConv.manicreaders.tblReviewerBookLimit RBL 
		JOIN ManicReadersConv.manicreaders.tblUser U ON U.UserName = RBL.UserName
			AND RBL.ReviewCompanyID = RCU.ReviewCompanyID 
			AND U.UserID = RCU.UserID
	), 0) AS BookLimit,

	CASE
		WHEN RCU.UserRole IN ('Editor', 'Reviewer')
		THEN (SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = RCU.UserRole)
		ELSE (SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = 'Administrator')
	END AS ReviewingCompanyRoleID

FROM ManicReadersConv.manicreaders.tblReviewCompanyUser RCU
WHERE EXISTS
	(
	SELECT 1
	FROM person.Person P
	WHERE P.PersonID = RCU.UserID
	)
ORDER BY RCU.UserID
GO

INSERT INTO person.PersonReviewingCompanyRole
	(PersonID,ReviewingCompanyID,ReviewingCompanyRoleID)
SELECT
	(SELECT P.PersonID FROM person.Person P WHERE P.UserName = 'ReviewingCompanyTestAdmin'),
	RC.ReviewingCompanyID,
	(SELECT R.ReviewingCompanyRoleID FROM dropdown.ReviewingCompanyRole R WHERE R.ReviewingCompanyRoleCode = 'Administrator')
FROM company.ReviewingCompany RC
WHERE RC.ReviewingCompanyName = 'Manic Readers'
GO
--End table person.PersonReviewingCompanyRole

--Begin table person.PersonRole
INSERT INTO person.PersonRole
	(PersonID,RoleID)
SELECT
	U.UserID AS PersonID,

	CASE
		WHEN U.UserRole IN ('Author')
		THEN (SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'Author')
		ELSE (SELECT R.RoleID FROM dropdown.Role R WHERE R.RoleCode = 'Reader')
	END AS RoleID

FROM ManicReadersConv.manicreaders.tblUser U
ORDER BY U.UserID
GO
--End table person.PersonRole

--Begin table review.ReviewRequest
TRUNCATE TABLE review.ReviewRequest
GO

SET IDENTITY_INSERT review.ReviewRequest ON
GO

INSERT INTO review.ReviewRequest
	(ReviewRequestID, BookID, RequestPersonID, ReviewRequestDateTime, HeatLevelID, NotifyEmailAddress, FormatID, ReviewRequestNotes)
SELECT
	R.MRReviewID AS ReviewRequestID,
	R.BookID,
	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.RequestedBy), 0) AS RequestPersonID,
	ISNULL(R.RequestedDate, '01/01/2000') AS ReviewRequestDateTime,
	ISNULL((SELECT HL.HeatLevelID FROM dropdown.HeatLevel HL WHERE HL.HeatLevelName = R.HeatLevel), 0) AS HeatLevelID,
	R.NotifyEmail AS NotifyEmailAddress,
	ISNULL((SELECT F.FormatID FROM dropdown.Format F WHERE F.FormatName = R.MRBookFormat), 0) AS FormatID,
	R.ReviewerNotes AS ReviewRequestNotes
FROM ManicReadersConv.manicreaders.tblMRReview R
ORDER BY R.MRReviewID
GO

SET IDENTITY_INSERT review.ReviewRequest OFF
GO
--End table review.ReviewRequest

--Begin table review.Review
TRUNCATE TABLE review.Review
GO

INSERT INTO review.Review
	(ReviewRequestID,ReviewerPersonID,ReviewingCompanyID,ReviewRatingID,StatusID,ReviewEditorPersonID,BookCheckoutDateTime,ReviewSubmitDateTime,ReviewDateTime,NotifyDateTime,ReviewLink,Review)
SELECT
	R.MRReviewID AS ReviewRequestID,
	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.Username), 0) AS ReviewerPersonID,
	ISNULL(R.ReviewCompanyID, 0) AS ReviewingCompanyID,
	ISNULL((SELECT RR.ReviewRatingID FROM dropdown.ReviewRating RR WHERE CAST(RR.ReviewRatingCode AS NUMERIC(3,1)) = CAST(ISNULL(core.NullIfEmpty(R.Rating), '0') AS NUMERIC(3,1))), 0) AS Rating,

	CASE
		WHEN R.Status IN ('Active','Deleted')
		THEN (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = R.Status)
		ELSE (SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Pending')
	END AS StatusID,

	ISNULL((SELECT P.PersonID FROM person.Person P WHERE P.UserName = R.Editor), 0) AS ReviewEditorPersonID,
	ISNULL(R.CheckoutDate, '01/01/2000') AS BookCheckoutDateTime,
	ISNULL(R.SubmitDate, '01/01/2000') AS ReviewSubmitDateTime,
	ISNULL(R.ReviewDate, '01/01/2000') AS ReviewDateTime,
	ISNULL(R.NotifyDate, '01/01/2000') AS NotifyDateTime,
	R.ExternalLink AS ReviewLink,
	R.Review
FROM ManicReadersConv.manicreaders.tblMRReview R
ORDER BY R.MRReviewID
GO
--End table review.Review


--End file Build File - 06 - Conversion.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2018.08.05 19.41.49')
GO
--End build tracking

