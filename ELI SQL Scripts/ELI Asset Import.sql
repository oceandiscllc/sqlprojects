USE ELI0002
GO

/*
TRUNCATE TABLE asset.Asset
GO
*/
DECLARE @cIntegrationCode VARCHAR(50)
DECLARE @nLatitude FLOAT
DECLARE @nLongitude FLOAT
DECLARE @nTerritoryID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		I.PIXUNIQUEID,
		I.Lat, 
		I.Lon
	FROM asset.Import I
	WHERE I.Lat IS NOT NULL
		AND I.Lon IS NOT NULL
		AND I.TerritoryID = 0
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @cIntegrationCode, @nLatitude, @nLongitude
WHILE @@fetch_status = 0
	BEGIN

	SELECT TOP 1 @nTerritoryID = T.TerritoryID
	FROM ELI0000.territory.Territory T
		JOIN ELI0000.territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.Location.STIntersects(GEOMETRY::STPointFromText('POINT(' + CAST(@nLongitude AS VARCHAR(50)) + ' ' + CAST(@nLatitude AS VARCHAR(50)) + ')', 4326)) = 1
	ORDER BY TT.DisplayOrder DESC

	UPDATE I
	SET I.TerritoryID = ISNULL(@nTerritoryID, 0)
	FROM asset.Import I
	WHERE I.PIXUNIQUEID = @cIntegrationCode

	FETCH oCursor INTO @cIntegrationCode, @nLatitude, @nLongitude
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor

INSERT INTO asset.Asset
	(IntegrationCode, Assetname, AssetTypeID, Location, LastUpdateDate, TerritoryID, ProjectID)
SELECT
	I.PIXUNIQUEID,
	I.Name,
	ISNULL((SELECT AT.AssetTypeID FROM dropdown.AssetType AT WHERE AT.AssetTypeCategory = I.Type AND AT.AssetTypeName = I.SubType), 0),
	GEOMETRY::STPointFromText('POINT(' + CAST(I.Lon AS VARCHAR(50)) + ' ' + CAST(I.Lat AS VARCHAR(50)) + ')', 4326),
	LastUpdateDate,
	TerritoryID,
	1
FROM asset.Import I
WHERE NOT EXISTS
	(
	SELECT 1
	FROM asset.Asset A
	WHERE A.IntegrationCode = I.PIXUNIQUEID
	)
ORDER BY 1
