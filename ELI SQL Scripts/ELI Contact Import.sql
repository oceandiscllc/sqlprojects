DELETE I
FROM contact.Import I
WHERE I.IntegrationCode IS NULL

SELECT * FROM contact.Import

DECLARE @tTable TABLE (ContactExperienceCategory VARCHAR(50), ContactExperienceName VARCHAR(50))

INSERT INTO @tTable
	(ContactExperienceCategory, ContactExperienceName)
VALUES 
	('Security', 'Border Security'),
	('Management', 'Community Development'),
	('Mediation', 'Conflict Resolution (Between Female Prisoners)'),
	('Mediation', 'Conflict Resolution (Business Disputes)'),
	('Mediation', 'Conflict Resolution (Community Disputes)'),
	('Mediation', 'Conflict Resolution (Hostage Situation)'),
	('Security', 'Counterterrorism'),
	('Management', 'Economic Development'),
	('Political', 'Electoral Committee'),
	('Management', 'Experience'),
	('Security', 'Female Prison Investigations'),
	('Security', 'Forensic Analysis'),
	('Political', 'Fundraising'),
	('Professional', 'Islah'),
	('Professional', 'Journalism'),
	('Management', 'Management Experience'),
	('Political', 'National Committee For Women Member'),
	('Management', 'Project Management'),
	('Management', 'Record-Keeping'),
	('Security', 'Republican Guard'),
	('Professional', 'Support For domestic Violence Victims'),
	('Professional', 'Women''s Development')

INSERT INTO ELI0000.dropdown.ContactExperience
	(ContactExperienceCategory, ContactExperienceName)
SELECT
	T.ContactExperienceCategory, 
	T.ContactExperienceName
FROM @tTable T
WHERE NOT EXISTS
	(
	SELECT 1
	FROM ELI0000.dropdown.ContactExperience CE
	WHERE CE.ContactExperienceName = T.ContactExperienceName
	)
GO

EXEC ELI0000.implementer.ImplementerDropdownDataAddUpdate 'ContactExperience', 'ContactExperienceID'
GO

INSERT INTO contact.Contact
	(IntegrationCode, FirstName, MiddleName, LastName, TerritoryID, DateOfBirth, EducationLevelID, IsTrainer, IsAidRecipient)
SELECT
	I.IntegrationCode, 
	I.FirstName, 
	I.MiddleName, 
	I.LastName, 
	ISNULL((SELECT T.TerritoryID FROM territory.Territory T WHERE T.TerritoryName = I.GovernorateName AND T.TerritoryTypeCode = 'Governorate'), 0) AS TerritoryID,
	DATEADD(yyyy, I.Age * -1, '04/01/2017') AS DateOfBirth,
	ISNULL((SELECT EL.EducationLevelID FROM dropdown.EducationLevel EL WHERE EL.EducationLevelName = I.EducationLevelName), 0) AS EducationLevelID,
	CASE WHEN Trainer = 'Y' THEN 1 ELSE 0 END AS IsTrainer, 
	CASE WHEN USAidRecipient = 'Y' THEN 1 ELSE 0 END AS IsAidRecipient
FROM contact.Import I

DECLARE @cContactExperienceNameList VARCHAR(MAX)
DECLARE @cContactExperienceName VARCHAR(50)
DECLARE @nContactID INT
DECLARE @nContactTypeID INT

DECLARE oCursor1 CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		C.ContactID,

		CASE
			WHEN I.Sector = 'C'
			THEN ISNULL((SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Civilian Sector'), 0) 
			ELSE ISNULL((SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Security Sector'), 0) 
		END AS ContactTypeID,

		I.Experience
	FROM contact.Import I
		JOIN contact.Contact C ON C.IntegrationCode = I.IntegrationCode
	ORDER BY 2

OPEN oCursor1
FETCH oCursor1 INTO @nContactID, @nContactTypeID, @cContactExperienceNameList
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO contact.ContactContactType (ContactID, ContactTypeID) SELECT @nContactID, CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Influential Female'
	INSERT INTO contact.ContactContactType (ContactID, ContactTypeID) VALUES (@nContactID, @nContactTypeID)
	
	DECLARE oCursor2 CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			LTRIM(RTRIM(LTT.ListItem))
		FROM core.ListToTable(@cContactExperienceNameList, ';') LTT
		ORDER BY 1

		OPEN oCursor2
		FETCH oCursor2 INTO @cContactExperienceName
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cContactExperienceName = 'conflict resolution (business dispute)'
				SET @cContactExperienceName = 'Conflict Resolution (Business Disputes)'
			--ENDIF
			IF @cContactExperienceName = 'conflict resolution (community dispute)'
				SET @cContactExperienceName = 'Conflict Resolution (Community Disputes)'
			--ENDIF
			IF @cContactExperienceName = 'faciliating humanitarian aid'
				SET @cContactExperienceName = 'Facilitating Humanitarian Aid'
			--ENDIF
			IF @cContactExperienceName IN ('National Committee For Women', 'National Dialogue Conference')
				SET @cContactExperienceName += ' Member'
			--ENDIF

			IF EXISTS (SELECT 1 FROM dropdown.ContactExperience CE WHERE CE.ContactExperienceName = @cContactExperienceName)
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName = @cContactExperienceName

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (armed actor disputes, land disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Armed Actor Disputes)', 'Conflict Resolution (Land Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (community disputes, family disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Community Disputes)', 'Conflict Resolution (Family Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (family disputes, business disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Business Disputes)', 'Conflict Resolution (Family Disputes)')

				END
			ELSE IF @cContactExperienceName IN ('conflict resolution (family disputes, land disputes)', 'conflict resolution (land disputes, family disputes)')
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Family Disputes)', 'Conflict Resolution (Land Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (land disputes, community disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Community Disputes)', 'Conflict Resolution (Land Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (land disputes, community disputes, family disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Community Disputes)', 'Conflict Resolution (Family Disputes)', 'Conflict Resolution (Land Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (land disputes, family disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Family Disputes)', 'Conflict Resolution (Land Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (resource disputes, armed actor disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Armed Actor Disputes)', 'Conflict Resolution (Resource Disputes)')

				END
			ELSE IF @cContactExperienceName = 'conflict resolution (resource disputes, armed actor disputes, land disputes)'
				BEGIN

				INSERT INTO contact.ContactContactExperience 
					(ContactID, ContactExperienceID) 
				SELECT 
					@nContactID, 
					CE.ContactExperienceID 
				FROM dropdown.ContactExperience CE 
				WHERE CE.ContactExperienceName IN ('Conflict Resolution (Armed Actor Disputes)', 'Conflict Resolution (Land Disputes)', 'Conflict Resolution (Resource Disputes)')

				END
			--ENDIF
				
			FETCH oCursor2 INTO @cContactExperienceName
		
			END
		--END WHILE
		
		CLOSE oCursor2
		DEALLOCATE oCursor2

	FETCH oCursor1 INTO @nContactID, @nContactTypeID, @cContactExperienceNameList
	
	END
--END WHILE
		
CLOSE oCursor1
DEALLOCATE oCursor1
GO
