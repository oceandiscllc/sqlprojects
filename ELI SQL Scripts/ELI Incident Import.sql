USE ELI0002
GO

DECLARE @cIncidentSourceName VARCHAR(50) = 
--'JANES'
--'STACSY'

/*
SELECT * 
FROM incident.IncidentImport II
order by II.Date_Year, II.Date_Month, II.Date_Day

SELECT * 
FROM incident.Incident
order by 1 desc

DELETE I
FROM incident.Incident I
	JOIN dropdown.IncidentSource INS ON INS.IncidentSourceID = I.IncidentSourceID
		AND INS.IncidentSourceName = @cIncidentSourceName

DELETE INF
FROM incident.IncidentForce INF
WHERE NOT EXISTS
	(
	SELECT 1
	FROM incident.Incident I
	WHERE I.IncidentID = INF.IncidentID
	)
*/
DECLARE @cIncidentID VARCHAR(50)
DECLARE @nLatitude FLOAT
DECLARE @nLongitude FLOAT
DECLARE @nTerritoryID INT

IF @cIncidentSourceName = 'JANES'
	BEGIN

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			II.Incident_ID,
			II.Lat, 
			II.Lon
		FROM incident.IncidentImport II
		WHERE II.Lat IS NOT NULL
			AND II.Lon IS NOT NULL
		ORDER BY 1

	OPEN oCursor
	FETCH oCursor INTO @cIncidentID, @nLatitude, @nLongitude
	WHILE @@fetch_status = 0
		BEGIN

		SELECT TOP 1 @nTerritoryID = T.TerritoryID
		FROM ELI0000.territory.Territory T
			JOIN ELI0000.territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
				AND T.Location.STIntersects(GEOMETRY::STPointFromText('POINT(' + CAST(@nLongitude AS VARCHAR(50)) + ' ' + CAST(@nLatitude AS VARCHAR(50)) + ')', 4326)) = 1
		ORDER BY TT.DisplayOrder DESC

		UPDATE II
		SET II.TerritoryID = ISNULL(@nTerritoryID, 0)
		FROM incident.IncidentImport II
		WHERE II.Incident_ID = @cIncidentID

		FETCH oCursor INTO @cIncidentID, @nLatitude, @nLongitude
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
;

INSERT INTO incident.Incident
	(IntegrationCode, IncidentDateTime, IncidentTypeID, Summary, TerritoryID, ProjectID, IncidentSourceID, ArabicSummary, Location, ConfidenceLevelID)
SELECT
	II.Incident_ID,
	CAST(CAST(II.Date_Month AS VARCHAR(2)) + '/' + CAST(II.Date_Day AS VARCHAR(2)) + '/' + CAST(II.Date_Year AS VARCHAR(4)) AS DATETIME) AS IncidentDateTime,
	ISNULL((SELECT IT.IncidentTypeID FROM dropdown.IncidentType IT WHERE IT.IncidentTypeName = II.Type), 0) AS IncidentTypeID,
	LTRIM(RTRIM(II.Description_English)) AS Summary,
	
	CASE
		WHEN @cIncidentSourceName = 'JANES'
		THEN II.TerritoryID
		ELSE 
			CASE
				WHEN II.District_Code IS NULL OR CAST(II.District_Code AS INT) = 0
				THEN ISNULL((SELECT T.TerritoryID FROM territory.Territory T WHERE T.TerritoryCode = II.Governorate_Code), 0)
				ELSE ISNULL((SELECT T.TerritoryID FROM territory.Territory T WHERE T.TerritoryCode = II.District_Code), 0)
			END
		END AS TerritoryID,

	1 AS ProjectID,
	(SELECT INS.IncidentSourceID FROM dropdown.IncidentSource INS WHERE INS.IncidentSourceName = @cIncidentSourceName) AS IncidentSourceID,
	LTRIM(RTRIM(II.Description_Arabic)) AS ArabicSummary,

	CASE
		WHEN @cIncidentSourceName = 'JANES'
		THEN GEOMETRY::STPointFromText('POINT(' + CAST(II.Lon AS VARCHAR(50)) + ' ' + CAST(II.Lat AS VARCHAR(50)) + ')', 4326)
		ELSE NULL
	END AS Location,

	ISNULL((SELECT CL.ConfidenceLevelID FROM dropdown.ConfidenceLevel CL WHERE CL.ConfidenceLevelName = II.Confidence), 0) AS ConfidenceLevelID
FROM incident.IncidentImport II
WHERE NOT EXISTS
	(
	SELECT 1
	FROM incident.Incident I
	WHERE I.IntegrationCode = II.Incident_ID
	)
GO

DECLARE @nIncidentID INT
DECLARE @nActor_Hadi INT
DECLARE @nActor_Houthi INT
DECLARE @nActor_Gpcsaleh INT
DECLARE @nActor_Foreign INT
DECLARE @nActor_Tribe INT
DECLARE @nActor_Civilian INT
DECLARE @nActor_Csongo INT
DECLARE @nActor_Fto INT
DECLARE @nActor_Other INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		I.IncidentID,
		II.Actor_Hadi,
		II.Actor_Houthi,
		II.Actor_Gpcsaleh,
		II.Actor_Foreign,
		II.Actor_Tribe,
		II.Actor_Civilian,
		II.Actor_Csongo,
		II.Actor_Fto,

		CASE
			WHEN II.Actor_Other IS NULL OR LEN(LTRIM(RTRIM(II.Actor_Other))) = 0
			THEN 0
			ELSE 1
		END AS Actor_Other
		
	FROM incident.IncidentImport II
		JOIN incident.Incident I ON I.IntegrationCode = II.Incident_ID
	ORDER BY I.IncidentID

OPEN oCursor
FETCH oCursor INTO @nIncidentID, @nActor_Hadi, @nActor_Houthi, @nActor_Gpcsaleh, @nActor_Foreign, @nActor_Tribe, @nActor_Civilian, @nActor_Csongo, @nActor_Fto, @nActor_Other
WHILE @@fetch_status = 0
	BEGIN

	IF @nActor_Hadi > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Hadi'
	IF @nActor_Houthi > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Houthi'
	IF @nActor_Gpcsaleh > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Gpcsaleh'
	IF @nActor_Foreign > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Foreign'
	IF @nActor_Tribe > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Tribe'
	IF @nActor_Civilian > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Civilian'
	IF @nActor_Csongo > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Csongo'
	IF @nActor_Fto > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Fto'
	IF @nActor_Other > 0
		INSERT INTO incident.IncidentForce (IncidentID, ForceID, ProjectID) SELECT @nIncidentID, F.ForceID, 1 FROM force.Force F WHERE F.ForceCode = 'Actor_Other'
	--ENDIF
	
	FETCH oCursor INTO @nIncidentID, @nActor_Hadi, @nActor_Houthi, @nActor_Gpcsaleh, @nActor_Foreign, @nActor_Tribe, @nActor_Civilian, @nActor_Csongo, @nActor_Fto, @nActor_Other
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor
GO
