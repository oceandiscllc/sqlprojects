USE ELI0002
GO

/*
SELECT * FROM conflictstory.Import

TRUNCATE TABLE conflictstory.ConflictStory
GO
*/

DECLARE @cConflictStorySourceName VARCHAR(50) = 'STACSY'

--'JANES'
--'STACSY'

INSERT INTO conflictstory.ConflictStory
	(ConflictStoryName, ConflictStoryYear, ConflictStorySourceID, MediationOutcomeID, TerritoryID, Summary, ProjectID, IntegrationCode)
SELECT
	ISNULL(I.District, '') + ' - ' + CASE WHEN I.ConflictStoryYear IS NULL OR I.ConflictStoryYear = 0 THEN CAST(YEAR(getDate()) AS CHAR(4)) ELSE CAST(I.ConflictStoryYear AS CHAR(4)) END,
	ISNULL(I.ConflictStoryYear, YEAR(getDate())), 
	ISNULL((SELECT CSS.ConflictStorySourceID FROM dropdown.ConflictStorySource CSS WHERE CSS.ConflictStorySourceName = @cConflictStorySourceName), 0) AS ConflictStorySourceID,
	ISNULL((SELECT MO.MediationOutcomeID FROM dropdown.MediationOutcome MO WHERE MO.MediationOutcomeName = I.MediationOutcomeName), 0) AS MediationOutcomeID,
	ISNULL((SELECT T.TerritoryID FROM territory.Territory T WHERE T.TerritoryTypeCode = 'District' AND T.TerritoryName = I.District), 0) AS TerritoryID,
	I.Summary,
	1 AS ProjectID,
	I.IntegrationCode
FROM conflictstory.Import I
WHERE NOT EXISTS
	(
	SELECT 1
	FROM conflictstory.ConflictStory CS
	WHERE CS.IntegrationCode = I.IntegrationCode
	)
	AND (I.ConflictStoryYear IS NOT NULL OR I.District IS NOT NULL)
	
DECLARE @cConflictDriverName VARCHAR(50)
DECLARE @cMediationTypeName VARCHAR(50)
DECLARE @nConflictSoryID INT
DECLARE @tTable TABLE (EntityName VARCHAR(50))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		CS.ConflictStoryID,
		I.ConflictDriverName,
		I.MediationTypeName
	FROM conflictstory.Import I
		JOIN conflictstory.ConflictStory CS ON CS.IntegrationCode = I.IntegrationCode
	ORDER BY CS.ConflictStoryID

OPEN oCursor
FETCH oCursor INTO @nConflictSoryID, @cConflictDriverName, @cMediationTypeName
WHILE @@fetch_status = 0
	BEGIN

	DELETE FROM @tTable

	INSERT INTO @tTable
		(EntityName)
	SELECT
		CASE
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'Civil Disagreement' 
			THEN 'Civil Disagreements'
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'Domestic Issue' 
			THEN 'Domestic Issues'
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'LDA' 
			THEN 'Land Dispute - Agriculture'
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'National Conflict Spillover' 
			THEN 'Conflict Spillover'
			ELSE LTRIM(RTRIM(LTT.ListItem))
		END
	FROM core.ListToTable(@cConflictDriverName, ';') LTT

	IF EXISTS (SELECT 1 FROM @tTable T WHERE T.EntityName = 'Theft and Looting')
		BEGIN

		UPDATE T
		SET T.EntityName = 'Theft'
		FROM @tTable T
		WHERE T.EntityName = 'Theft and Looting'

		INSERT INTO @tTable (EntityName) VALUES ('Looting')

		END
	--ENDIF

	INSERT INTO conflictstory.ConflictStoryConflictDriver
		(ConflictStoryID, ConflictDriverID)
	SELECT
		@nConflictSoryID,
		CD.ConflictDriverID
	FROM dropdown.ConflictDriver CD
		JOIN @tTable T ON T.EntityName = CD.ConflictDriverName
			AND NOT EXISTS
				(
				SELECT 1
				FROM conflictstory.ConflictStoryConflictDriver CSCD
				WHERE CSCD.ConflictStoryID = @nConflictSoryID
					AND CSCD.ConflictDriverID = CD.ConflictDriverID
				)

	DELETE FROM @tTable

	INSERT INTO @tTable
		(EntityName)
	SELECT
		CASE
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'CL' 
			THEN 'Community Leaders'
			WHEN LTRIM(RTRIM(LTT.ListItem)) = 'SA' 
			THEN 'State Authorities'
			ELSE LTRIM(RTRIM(LTT.ListItem))
		END
	FROM core.ListToTable(@cConflictDriverName, ';') LTT

	INSERT INTO conflictstory.ConflictStoryMediationType
		(ConflictStoryID, MediationTypeID)
	SELECT
		@nConflictSoryID,
		MT.MediationTypeID
	FROM dropdown.MediationType MT
		JOIN @tTable T ON T.EntityName = MT.MediationTypeName
			AND NOT EXISTS
				(
				SELECT 1
				FROM conflictstory.ConflictStoryMediationType CSMT
				WHERE CSMT.ConflictStoryID = @nConflictSoryID
					AND CSMT.MediationTypeID = MT.MediationTypeID
				)
	
	FETCH oCursor INTO @nConflictSoryID, @cConflictDriverName, @cMediationTypeName
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor
GO
