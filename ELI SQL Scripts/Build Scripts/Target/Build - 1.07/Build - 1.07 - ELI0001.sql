-- File Name:	ELI0001.sql
-- Build Key:	Build - 1.07 - 2018.01.04 09.01.57

USE ELI0001
GO

-- ==============================================================================================================================
-- Functions:
--		person.CheckFileAccess
--
-- Procedures:
--		[incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]
--		incident.GetMonthlyIncidentCountsByIncidentType
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategory
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0001
GO


--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0001
GO

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has permission to a file
-- ==========================================================================

CREATE FUNCTION person.CheckFileAccess
(
@PersonID INT,
@DocumentName VARCHAR(50),
@CheckForDownloadPermission BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCheckFileAccess BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = @PersonID
			AND 
				(
				PP.PermissionableLineage = 
					(
					SELECT 'Document.View.' + DT.DocumentTypeCode
					FROM document.Document D
						JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
							AND D.DocumentName = @DocumentName
					)
				)
		)	
		AND 
			(
			@CheckForDownloadPermission = 0
				OR EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE PP.PersonID = @PersonID
						AND PP.PermissionableLineage = 'Document.GetDocumentByDocumentName'
					)
			)
		SET @nCheckFileAccess = 1
	--ENDIF
	
	RETURN @nCheckFileAccess

END
GO
--End function person.CheckFileAccess
--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0001
GO

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentType
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentType'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentType

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeID,
		COUNT(I.IncidentID) IncidentCount
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)

	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeID
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentType

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeID,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND 
					(
					@IncidentTypeIDList IS NULL 
						OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
					)
				AND 
					(
					@ProjectIDList IS NULL 
						OR LEN(RTRIM(@ProjectIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
					)
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategory

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeCategory,
		COUNT(I.IncidentID) IncidentCount 
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeCategory
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeCategory,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND 
					(
					@IncidentTypeIDList IS NULL 
						OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
					)
				AND 
					(
					@ProjectIDList IS NULL 
						OR LEN(RTRIM(@ProjectIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
					)
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0001
GO


--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin post process file ELI0001.sql
--Begin strip identity specifications and create composite primary keys
DECLARE @cColumnName VARCHAR(250)
DECLARE @cIndexName VARCHAR(250)
DECLARE @cIndexType VARCHAR(50)
DECLARE @cNewColumnName VARCHAR(250)
DECLARE @cSchemaName VARCHAR(50)
DECLARE @cSchemaNameTableName VARCHAR(300)
DECLARE @cSchemaNameTableNameColumnName VARCHAR(500)
DECLARE @cSQLText VARCHAR(MAX)
DECLARE @cTableName VARCHAR(250)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		S1.Name AS SchemaName,
		T1.Name AS TableName,
		C1.Name AS ColumnName,
		I.IndexName,
		I.IndexType
	FROM sys.Tables T1
		JOIN sys.Schemas S1 ON S1.Schema_ID = T1.Schema_ID
			AND S1.Name NOT IN ('aggregator', 'core', 'document', 'dropdown', 'eventlog', 'integration', 'person', 'reporting', 'syslog', 'workflow')
		JOIN sys.Columns C1 ON C1.Object_ID = T1.Object_ID
			AND C1.Is_Identity = 1
			AND EXISTS
				(
				SELECT 1
				FROM sys.Tables T2
					JOIN sys.Schemas S2 ON S2.Schema_ID = T2.Schema_ID
					JOIN sys.Columns C2 ON C2.Object_ID = T2.Object_ID
						AND S1.Name = S2.Name
						AND T1.Name = T2.Name
						AND C2.Name = 'ProjectID'
				)
		JOIN
			(
			SELECT 
				C3.Name AS ColumnName,
				I.Name AS IndexName,
				I.type_desc AS IndexType
			FROM sys.indexes I
				JOIN sys.index_columns IC ON IC.Object_ID = I.Object_ID 
					AND IC.Index_ID = I.Index_ID
				JOIN sys.Columns C3 ON C3.Object_ID = IC.Object_ID
					AND C3.Column_ID = IC.Column_ID
					AND I.is_primary_key = 1
			) I ON I.ColumnName = C1.Name
	ORDER BY 1, 2, 3

OPEN oCursor
FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
WHILE @@fetch_status = 0
	BEGIN

	SET @cNewColumnName = @cColumnName + '_NoAN'
	SET @cSchemaNameTableName = @cSchemaName + '.' + @cTableName
	SET @cSchemaNameTableNameColumnName = @cSchemaNameTableName + '.' + @cNewColumnName

	EXEC utility.AddColumn @cSchemaNameTableName, @cNewColumnName, 'INT';

	SET @cSQLText = 'UPDATE T SET T.' + @cNewColumnName + ' = T.' + @cColumnName + ' FROM ' + @cSchemaNameTableName + ' T'
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP CONSTRAINT ' + @cIndexName
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP COLUMN ' + @cColumnName
	EXEC (@cSQLText);

	EXEC sp_rename @cSchemaNameTableNameColumnName, @cColumnName, 'COLUMN';
	EXEC utility.SetDefaultConstraint @cSchemaNameTableName, @cColumnName, 'INT', '0', 1;

	IF @cIndexType = 'CLUSTERED'
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	ELSE 
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyNonClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	--ENDIF

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End strip identity specifications and create composite primary keys

--Begin drop triggers
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		'DROP Trigger ' + S.Name + '.' + O.Name AS SQLText
	FROM sysobjects O
		JOIN sys.tables T ON O.Parent_Obj = T.Object_ID
		JOIN sys.schemas S ON T.Schema_ID = S.Schema_ID 
			AND O.Type = 'TR' 

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSQLText
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End drop triggers

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P1.PersonID,
	P2.ProjectID
FROM person.Person P1 
	CROSS APPLY dropdown.Project P2
WHERE P1.IsSuperAdministrator = 1
	AND P2.ProjectID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP
		WHERE PP.PersonID = P1.PersonID
			AND PP.ProjectID = P2.ProjectID
		)
--End post process file ELI0001.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.07 - 2018.01.04 09.01.57')
GO
--End build tracking

