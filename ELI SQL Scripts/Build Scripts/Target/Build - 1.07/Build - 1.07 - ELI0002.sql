-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.07 - 2018.01.04 09.01.57

USE ELI0002
GO

-- ==============================================================================================================================
-- Functions:
--		person.CheckFileAccess
--
-- Procedures:
--		[incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]
--		incident.GetMonthlyIncidentCountsByIncidentType
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategory
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO


--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has permission to a file
-- ==========================================================================

CREATE FUNCTION person.CheckFileAccess
(
@PersonID INT,
@DocumentName VARCHAR(50),
@CheckForDownloadPermission BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCheckFileAccess BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = @PersonID
			AND 
				(
				PP.PermissionableLineage = 
					(
					SELECT 'Document.View.' + DT.DocumentTypeCode
					FROM document.Document D
						JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
							AND D.DocumentName = @DocumentName
					)
				)
		)	
		AND 
			(
			@CheckForDownloadPermission = 0
				OR EXISTS
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE PP.PersonID = @PersonID
						AND PP.PermissionableLineage = 'Document.GetDocumentByDocumentName'
					)
			)
		SET @nCheckFileAccess = 1
	--ENDIF
	
	RETURN @nCheckFileAccess

END
GO
--End function person.CheckFileAccess
--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentType
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentType'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentType

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeID,
		COUNT(I.IncidentID) IncidentCount
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)

	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeID
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentType

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeID,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND 
					(
					@IncidentTypeIDList IS NULL 
						OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
					)
				AND 
					(
					@ProjectIDList IS NULL 
						OR LEN(RTRIM(@ProjectIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
					)
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategory

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeCategory,
		COUNT(I.IncidentID) IncidentCount 
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeCategory
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeCategory,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND 
					(
					@IncidentTypeIDList IS NULL 
						OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
					)
				AND 
					(
					@ProjectIDList IS NULL 
						OR LEN(RTRIM(@ProjectIDList)) = 0 
						OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
					)
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID)
				)
			AND 
				(
				@ProjectIDList IS NULL 
					OR LEN(RTRIM(@ProjectIDList)) = 0 
					OR EXISTS (SELECT 1 FROM core.ListToTable(@ProjectIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.ProjectID)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO


--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.07 - 2018.01.04 09.01.57')
GO
--End build tracking

