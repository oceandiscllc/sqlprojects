-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.01 - 2017.07.31 18.06.13

USE ELI0002
GO

-- ==============================================================================================================================
-- Procedures:
--		conflictstory.GetConflictStoryByConflictStoryID
--		contact.GetContactByContactID
--		document.GetDocumentByDocumentID
--		dropdown.GetConflictStoryYearData
--		eventlog.LogConflictStoryAction
--		territory.GetTerritoryByTerritoryID
--		utility.RenameColumn
--
-- Schemas:
--		conflictstory
--
-- Tables:
--		conflictstory.ConflictStory
--		conflictstory.ConflictStoryConflictDriver
--		conflictstory.ConflictStoryMediationType
--		document.DocumentDocumentIssue
-- ==============================================================================================================================

--Begin file ELI000X - 00 - Prerequisite.sql
USE ELI0002
GO

--Begin procedure utility.RenameColumn
EXEC utility.DropObject 'utility.RenameColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	A procedure to rename a column
-- ===========================================
CREATE PROCEDURE utility.RenameColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@NewColumnName VARCHAR(250)
)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cTableNameColumnName VARCHAR(501) = @TableName + '.' + @ColumnName
	IF EXISTS (SELECT 1 FROM dbo.SysColumns SC WHERE SC.ID = OBJECT_ID(@TableName) AND SC.Name = @ColumnName)
		EXEC sp_RENAME @cTableNameColumnName, @NewColumnName, 'COLUMN';
	--ENDIF

END
GO
--End procedure utility.RenameColumn

EXEC utility.AddSchema 'conflictstory'
GO

--End file ELI000X - 00 - Prerequisite.sql

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO

--Begin table conflictstory.ConflictStory
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStory'

EXEC utility.DropObject 'dbo.ConflictStory'
EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStory
	(
	ConflictStoryID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryName VARCHAR(250),
	ConflictStoryYear INT,
	ConflictStorySourceID INT,
	MediationOutcomeID INT,
	TerritoryID INT,
	Summary VARCHAR(MAX),
	ProjectID INT,
	IntegrataionCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConflictStorySourceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryYear', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MediationOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStory', 'ProjectID,ConflictStoryName'
GO
--End table conflictstory.ConflictStory

--Begin table conflictstory.ConflictStoryConflictDriver
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStoryConflictDriver'

EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStoryConflictDriver
	(
	ConflictStoryConflictDriverID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryID INT,
	ConflictDriverID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConflictDriverID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryConflictDriverID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStoryConflictDriver', 'ConflictStoryID,ConflictDriverID'
GO
--End table conflictstory.ConflictStoryConflictDriver

--Begin table conflictstory.ConflictStoryMediationType
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStoryMediationType'

EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStoryMediationType
	(
	ConflictStoryMediationTypeID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryID INT,
	MediationTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediationTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryMediationTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStoryMediationType', 'ConflictStoryID,MediationTypeID'
GO
--End table conflictstory.ConflictStoryMediationType

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'InfluenceDepth', 'VARCHAR(5)'
EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'

EXEC utility.RenameColumn @TableName, 'EmployerName', 'Alignment'
EXEC utility.RenameColumn @TableName, 'Title', 'Position'
EXEC utility.RenameColumn @TableName, 'Profession', 'Influence'
EXEC utility.RenameColumn @TableName, 'Influence', 'InfluenceReach'

ALTER TABLE contact.Contact ALTER COLUMN InfluenceReach VARCHAR(5)
GO
--End table contact.Contact

--Begin table document.DocumentDocumentIssue
DECLARE @TableName VARCHAR(250) = 'document.DocumentDocumentIssue'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentDocumentIssue
	(
	DocumentDocumentIssueID INT NOT NULL IDENTITY(0,1),
	DocumentID INT,
	DocumentIssueID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentIssueID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentDocumentIssueID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentDocumentIssue', 'DocumentID,DocumentIssueID'
GO
--End table document.DocumentDocumentIssue

--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO


--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO

--Begin procedure conflictstory.GetConflictStoryByConflictStoryID
EXEC utility.DropObject 'conflictstory.GetConflictStoryByConflictStoryID'
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the conflictstory.ConflictStory table
-- ======================================================================================
CREATE PROCEDURE conflictstory.GetConflictStoryByConflictStoryID

@ConflictStoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ConflictStory
	SELECT 
		CS.ConflictStoryID,
		CS.ConflictStoryName,
		CS.ConflictStoryYear,
		CS.ProjectID,
		dropdown.GetProjectNameByProjectID(CS.ProjectID) AS ProjectName,
		CS.Summary,
		CSS.ConflictStorySourceID,
		CSS.ConflictStorySourceName,
		MO.MediationOutcomeID,
		MO.MediationOutcomeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		T.TerritoryName
	FROM conflictstory.ConflictStory CS
		JOIN dropdown.ConflictStorySource CSS ON CSS.ConflictStorySourceID = CS.ConflictStorySourceID
		JOIN dropdown.MediationOutcome MO ON MO.MediationOutcomeID = CS.MediationOutcomeID
		JOIN territory.Territory T ON T.TerritoryID = CS.TerritoryID
			AND CS.ConflictStoryID = @ConflictStoryID
			AND CS.ProjectID = @ProjectID

	--ConflictStoryConflictDriver
	SELECT 
		CSCD.ConflictStoryConflictDriverID,
		CD.ConflictDriverID,
		CD.ConflictDriverName
	FROM conflictstory.ConflictStoryConflictDriver CSCD
		JOIN dropdown.ConflictDriver CD ON CD.ConflictDriverID = CSCD.ConflictDriverID
			AND CSCD.ConflictStoryID = @ConflictStoryID
	ORDER BY CD.ConflictDriverName, CD.ConflictDriverID

	--ConflictStoryMediationType
	SELECT 
		CSMT.ConflictStoryMediationTypeID,
		MT.MediationTypeID,
		MT.MediationTypeName
	FROM conflictstory.ConflictStoryMediationType CSMT
		JOIN dropdown.MediationType MT ON MT.MediationTypeID = CSMT.MediationTypeID
			AND CSMT.ConflictStoryID = @ConflictStoryID
	ORDER BY MT.MediationTypeName, MT.MediationTypeID

END
GO
--End procedure conflictstory.GetConflictStoryByConflictStoryID

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.Alignment,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.InfluenceDepth,
		C1.InfluenceReach,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.Position,
		C1.PostalCode,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
			AND C1.ContactID = @ContactID
			AND C1.ProjectID = @ProjectID

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
			AND CCT.ProjectID = @ProjectID

	--ContactVetting
	SELECT
		CV.VettingDate,
		core.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM contact.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
			AND CV.ProjectID = @ProjectID		
	ORDER BY CV.VettingDate DESC

	--ContactCourse
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM course.Course C
		JOIN course.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID
			AND CC.ProjectID = @ProjectID

	--ContactEquipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND C.ProjectID = @ProjectID
			AND EI.AssigneeTypeCode = 'Contact'
			AND EI.ProjectID = 	C.ProjectID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Document
	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	--DocumentEntity
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName,

		CASE
			WHEN DE.EntityTypeCode = 'Territory'
			THEN territory.FormatTerritoryNameByTerritoryID(DE.EntityID)
			ELSE NULL
		END AS TerritoryNameFormatted

	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID
	
	--DocumentIssue
	SELECT
		DI.DocumentIssueID,
		DI.DocumentIssueName
	FROM dropdown.DocumentIssue DI
		JOIN document.DocumentDocumentIssue DDI ON DDI.DocumentIssueID = DI.DocumentIssueID
			AND DDI.DocumentID = @DocumentID
	ORDER BY DI.DocumentIssueName, DI.DocumentIssueID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure dropdown.GetConflictStoryYearData
EXEC Utility.DropObject 'dropdown.GetConflictStoryYearData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConflictStory table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetConflictStoryYearData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.ConflictStoryYear
	FROM conflictstory.ConflictStory T
	ORDER BY T.ConflictStoryYear DESC

END
GO
--End procedure dropdown.GetConflictStoryYearData

--Begin procedure eventlog.LogConflictStoryAction
EXEC utility.DropObject 'eventlog.LogConflictStoryAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogConflictStoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ConflictStory'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogConflictStoryActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogConflictStoryActionTable
		--ENDIF
		
		SELECT *
		INTO #LogConflictStoryActionTable
		FROM conflictstory.ConflictStory CS
		WHERE CS.ConflictStoryID = @EntityID
		
		ALTER TABLE #LogConflictStoryActionTable DROP COLUMN Location
	
		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, @EntityID)
				
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT
			T.*, 
			CAST(('<Location>' + CAST(CS.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(@cDocuments AS XML)
			FOR XML RAW('ConflictStory'), ELEMENTS
			)
		FROM #LogConflictStoryActionTable T
			JOIN conflictstory.ConflictStory CS ON CS.ConflictStoryID = T.ConflictStoryID

		DROP TABLE #LogConflictStoryActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConflictStoryAction

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
--
-- Author:			Jonathan Burnham
-- Create date:	2017.07.24
-- Description:	Modifying to add children to governants and districts
-- =====================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--Children
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural,
		@ProjectID AS ProjectID	
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.ParentTerritoryID = @TerritoryID
	ORDER BY 4, 5
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID

--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO


--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.01 - 2017.07.31 18.06.13'
GO
--End build tracking

