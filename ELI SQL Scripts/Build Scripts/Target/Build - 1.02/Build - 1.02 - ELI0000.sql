-- File Name:	ELI0000.sql
-- Build Key:	Build - 1.02 - 2017.08.29 19.52.26

USE ELI0000
GO

-- ==============================================================================================================================
-- Functions:
--		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
--
-- Procedures:
--		dropdown.GetConfidenceLevelData
--		dropdown.GetContactExperienceData
--		dropdown.GetEducationLevelData
--		dropdown.GetIncidentDirectionData
--		dropdown.GetTerritoryTypeData
--		implementer.ImplementerDropdownDataAddUpdate
--		territory.GetProjectTerritoryTypesByProjectID
--
-- Tables:
--		dropdown.ConfidenceLevel
--		dropdown.ContactExperience
--		dropdown.EducationLevel
--		dropdown.IncidentDirection
-- ==============================================================================================================================

--Begin file ELI0000 - 00 - Prerequisite.sql
USE ELI0000
GO

--Begin procedure implementer.ImplementerDropdownDataAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerDropdownDataAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new dropdown / dropdown data to the LEO system and grant implemeters access to the new data
-- ====================================================================================================================================
CREATE PROCEDURE implementer.ImplementerDropdownDataAddUpdate

@DropdownCode VARCHAR(50),
@DropdownCodePrimaryKey VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = 'dropdown'

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCodePrimaryKey VARCHAR(60) = ISNULL(@DropdownCodePrimaryKey, @DropdownCode + 'ID')
	DECLARE @cSQL VARCHAR(MAX)

	SET @cSQL = 'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT I.ImplementerCode, '''
	SET @cSQL += @DropdownCode + ''', T.' + @cDropdownCodePrimaryKey + ' FROM ' + @SchemaName + '.' + @DropdownCode
	SET @cSQL += ' T CROSS APPLY implementer.Implementer I WHERE NOT EXISTS'
	SET @cSQL += ' (SELECT 1 FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = I.ImplementerCode AND IDD.DropdownCode = '''
	SET @cSQL += @DropdownCode + ''' AND IDD.DropdownID = T.' + @cDropdownCodePrimaryKey + ')'

	EXEC (@cSQL)

	IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND T.Name = @DropdownCode AND S.Name = 'dropdown')
		BEGIN

		SET @cSQL = 'UPDATE IDD SET IDD.IsActive = T.IsActive FROM implementer.ImplementerDropdownData IDD'
		SET @cSQL += ' JOIN dropdown.' + @DropdownCode + ' T ON T.' + @DropdownCodePrimaryKey + '= IDD.DropdownID'

		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure implementer.ImplementerDropdownDataAddUpdate
--End file ELI0000 - 00 - Prerequisite.sql

--Begin file ELI0000 - 01 - Tables.sql
USE ELI0000
GO

--Begin table dropdown.ConfidenceLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.ConfidenceLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConfidenceLevel
	(
	ConfidenceLevelID INT NOT NULL IDENTITY(0,1),
	ConfidenceLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConfidenceLevelID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConfidenceLevel', 'DisplayOrder,ConfidenceLevelName'
GO
--End table dropdown.ConfidenceLevel

--Begin table dropdown.ContactExperience
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactExperience'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactExperience
	(
	ContactExperienceID INT NOT NULL IDENTITY(0,1),
	ContactExperienceCategory VARCHAR(50),
	ContactExperienceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactExperienceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactExperience', 'ContactExperienceCategory,DisplayOrder,ContactExperienceName'
GO
--End table dropdown.ContactExperience

--Begin table dropdown.EducationLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.EducationLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EducationLevel
	(
	EducationLevelID INT NOT NULL IDENTITY(0,1),
	EducationLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EducationLevelID'
EXEC utility.SetIndexClustered @TableName, 'IX_EducationLevel', 'DisplayOrder,EducationLevelName'
GO
--End table dropdown.EducationLevel

--Begin table dropdown.IncidentDirection
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentDirection'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentDirection
	(
	IncidentDirectionID INT NOT NULL IDENTITY(0,1),
	IncidentDirectionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentDirectionID'
EXEC utility.SetIndexClustered @TableName, 'IX_IncidentDirection', 'DisplayOrder,IncidentDirectionName'
GO
--End table dropdown.IncidentDirection

--End file ELI0000 - 01 - Tables.sql

--Begin file ELI0000 - 02 - Functions.sql
USE ELI0000
GO

--Begin function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
EXEC utility.DropObject 'dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return an ID value for an IncidentTypeCategory
-- =========================================================================
CREATE FUNCTION dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
(
@IncidentTypeCategory VARCHAR(50)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentTypeCategoryID INT = 0
	DECLARE @tTable TABLE (IncidentTypeCategoryID INT NOT NULL IDENTITY(1,1), IncidentTypeCategory VARCHAR(50))

	INSERT INTO @tTable
		(IncidentTypeCategory) 		
	SELECT DISTINCT
		IT.IncidentTypeCategory
	FROM dropdown.IncidentType IT
	WHERE IT.IncidentTypeID > 0
	ORDER BY IncidentTypeCategory

	SELECT @nIncidentTypeCategoryID = T.IncidentTypeCategoryID
	FROM @tTable T
	WHERE T.IncidentTypeCategory = @IncidentTypeCategory

	RETURN ISNULL(@nIncidentTypeCategoryID, 0)

END
GO
--End function contact.GetContactCountByTerritory

--End file ELI0000 - 02 - Functions.sql

--Begin file ELI0000 - 03 - Procedures.sql
USE ELI0000
GO

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConfidenceLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConfidenceLevel'
			AND (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetContactExperienceData
EXEC Utility.DropObject 'dropdown.GetContactExperienceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.ContactExperience table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetContactExperienceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactExperienceID, 
		T.ContactExperienceCategory,
		T.ContactExperienceName
	FROM dropdown.ContactExperience T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactExperienceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactExperience'
			AND (T.ContactExperienceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.ContactExperienceCategory, T.DisplayOrder, T.ContactExperienceName, T.ContactExperienceID

END
GO
--End procedure dropdown.GetContactExperienceData

--Begin procedure dropdown.GetEducationLevelData
EXEC Utility.DropObject 'dropdown.GetEducationLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.EducationLevel table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEducationLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EducationLevelID, 
		T.EducationLevelName
	FROM dropdown.EducationLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EducationLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EducationLevel'
			AND (T.EducationLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EducationLevelName, T.EducationLevelID

END
GO
--End procedure dropdown.GetEducationLevelData

--Begin procedure dropdown.GetIncidentDirectionData
EXEC Utility.DropObject 'dropdown.GetIncidentDirectionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.13
-- Description:	A stored procedure to return data from the dropdown.IncidentDirection table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetIncidentDirectionData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentDirectionID, 
		T.IncidentDirectionName
	FROM dropdown.IncidentDirection T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentDirectionID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentDirection'
			AND (T.IncidentDirectionID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentDirectionName, T.IncidentDirectionID

END
GO
--End procedure dropdown.GetIncidentDirectionData

--Begin procedure dropdown.GetTerritoryTypeData
EXEC Utility.DropObject 'dropdown.GetTerritoryTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.15
-- Description:	A stored procedure to return data from the dropdown.TerritoryType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetTerritoryTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryTypeID,
		T.TerritoryTypeCode,
		T.TerritoryTypeName
	FROM territory.TerritoryType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TerritoryType'
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.TerritoryTypeName, T.TerritoryTypeID

END
GO
--End procedure dropdown.GetTerritoryTypeData

--Begin procedure territory.GetProjectTerritoryTypesByProjectID
EXEC utility.DropObject 'territory.GetProjectTerritoryTypesByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2016.01.23
-- Description:	A stored procedure to get the territory types associated with a project
-- ====================================================================================
CREATE PROCEDURE territory.GetProjectTerritoryTypesByProjectID

@ProjectID INT = 0,
@TerritoryTypeCode VARCHAR(50) = '',
@OrderByDirection VARCHAR(5) = 'DESC'

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		TT1.TerritoryTypeCode,
		TT1.TerritoryTypeCodePlural,
		TT1.TerritoryTypeName,
		TT1.TerritoryTypeNamePlural
	FROM territory.ProjectTerritoryType PTT
		JOIN dropdown.Project P ON P.ProjectID = PTT.ProjectID
		JOIN territory.TerritoryType TT1 ON TT1.TerritoryTypeID = PTT.TerritoryTypeID
			AND PTT.ProjectID = @ProjectID
			AND 
				(
				@TerritoryTypeCode = ''
					OR 
						(
						@OrderByDirection = 'ASC'
							AND TT1.DisplayOrder > 
								(
								SELECT TT2.DisplayOrder
								FROM territory.TerritoryType TT2
								WHERE TT2.TerritoryTypeCode = @TerritoryTypeCode 
								)
						)
					OR 
						(
						@OrderByDirection = 'DESC'
							AND TT1.DisplayOrder < 
								(
								SELECT TT2.DisplayOrder
								FROM territory.TerritoryType TT2
								WHERE TT2.TerritoryTypeCode = @TerritoryTypeCode 
								)
						)
				)
		ORDER BY 
			CASE WHEN @OrderByDirection = 'ASC' THEN TT1.DisplayOrder END ASC,
			CASE WHEN @OrderByDirection <> 'ASC' THEN TT1.DisplayOrder END DESC

END
GO
--End procedure territory.GetProjectTerritoryTypesByProjectID

--End file ELI0000 - 03 - Procedures.sql

--Begin file ELI0000 - 04 - Data.sql
USE ELI0000
GO

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'TerritoryList',
	@AfterMenuItemCode = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'DocumentList',
	@AfterMenuItemCode = 'TerritoryList',
	@NewMenuItemText = 'Document Repository'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'RequestForInformationList',
	@AfterMenuItemCode = 'DocumentList'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'IncidentList',
	@AfterMenuItemCode = 'RequestForInformationList',
	@Icon = 'fa fa-fw fa-bolt'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'ConflictStoryList',
	@AfterMenuItemCode = 'IncidentList',
	@Icon = 'fa fa-fw fa-book'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'Contacts',
	@AfterMenuItemCode = 'ConflictStoryList',
	@Icon = 'fa fa-fw fa-users'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '', 		
	@NewMenuItemCode = 'ForceList', 											
	@AfterMenuItemCode = 'Contacts',	
	@Icon = 'fa fa-fw fa-flag'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '', 		
	@NewMenuItemCode = 'AssetList', 											
	@AfterMenuItemCode = 'ForceList',												
	@Icon = 'fa fa-fw fa-tags'
GO

DELETE MI
FROM core.MenuItem MI
WHERE MI.MenuItemCode = 'ForceAsset'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Activity'
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

EXEC implementer.ImplementerDropdownDataAddUpdate 'TerritoryType', 'TerritoryTypeID', 'territory'
GO

UPDATE IDD
SET IDD.IsActive = 0
FROM implementer.ImplementerDropdownData IDD
	JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = IDD.DropdownID
		AND IDD.DropdownCode = 'TerritoryType'
		AND TT.TerritoryTypeCode NOT IN ('Community', 'District', 'Governorate')
		AND IDD.DropdownID > 0
GO

--Begin data for table dropdown.ConfidenceLevel
TRUNCATE TABLE dropdown.ConfidenceLevel
GO

SET IDENTITY_INSERT dropdown.ConfidenceLevel ON;
INSERT INTO dropdown.ConfidenceLevel (ConfidenceLevelID) VALUES (0);
SET IDENTITY_INSERT dropdown.ConfidenceLevel OFF;

INSERT INTO dropdown.ConfidenceLevel 
	(ConfidenceLevelName, DisplayOrder) 
VALUES 
	('High', 1),
	('Medium', 2),
	('Low', 3)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ConfidenceLevel'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ConfidenceLevel'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetConfidenceLevelData'
GO
--End data for table dropdown.ConfidenceLevel

--Begin data for table dropdown.ContactExperience
TRUNCATE TABLE dropdown.ContactExperience
GO

SET IDENTITY_INSERT dropdown.ContactExperience ON;
INSERT INTO dropdown.ContactExperience (ContactExperienceID) VALUES (0);
SET IDENTITY_INSERT dropdown.ContactExperience OFF;

INSERT INTO dropdown.ContactExperience 
	(ContactExperienceCategory, ContactExperienceName) 
VALUES 
	('Campaigns and Media', 'Awareness Campaigns'),
	('Campaigns and Media', 'Media Relations'),
	('Campaigns and Media', 'Political Campaigns'),
	('Campaigns and Media', 'Youth Engagement'),
	('Management', 'Facilitating Humanitarian Aid'),
	('Management', 'Nonprofit Management'),
	('Management', 'Program Management'),
	('Mediation', 'Conflict Resolution (Armed Actor Disputes)'),
	('Mediation', 'Conflict Resolution (Family Disputes)'),
	('Mediation', 'Conflict Resolution (Land Disputes)'),
	('Mediation', 'Conflict Resolution (Resource Disputes)'),
	('Mediation', 'Conflict Resolution Training'),
	('Political', 'Local Government'),
	('Political', 'National Dialogue Conference Member'),
	('Political', 'Southern Transitional Council Member'),
	('Professional', 'Lawyer'),
	('Professional', 'Medical'),
	('Security', 'Crowd control'),
	('Security', 'DDR'),
	('Security', 'Prisoner Reintegration'),
	('Security', 'UXO removal')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactExperience'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ContactExperience'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetContactExperienceData'
GO
--End data for table dropdown.ContactExperience

--Begin data for table dropdown.ContactType
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Civilian Sector')
	BEGIN

	INSERT INTO dropdown.ContactType 
		(ContactTypeName) 
	VALUES 
		('Civilian Sector'),
		('Influential Female'),
		('Security Actor'),
		('Security Sector')

	END
--ENDIF
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactType', 'ContactTypeID'
GO
--End data for table dropdown.ContactType

--Begin data for table dropdown.EducationLevel
TRUNCATE TABLE dropdown.EducationLevel
GO

SET IDENTITY_INSERT dropdown.EducationLevel ON;
INSERT INTO dropdown.EducationLevel (EducationLevelID) VALUES (0);
SET IDENTITY_INSERT dropdown.EducationLevel OFF;

INSERT INTO dropdown.EducationLevel 
	(EducationLevelName, DisplayOrder) 
VALUES 
	('Primary', 1),
	('Secondary', 2),
	('BA', 3),
	('MA', 4),
	('Law', 5),
	('PhD', 6)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'EducationLevel'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'EducationLevel'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEducationLevelData'
GO
--End data for table dropdown.EducationLevel

--Begin data for table dropdown.IncidentDirection
TRUNCATE TABLE dropdown.IncidentDirection
GO

SET IDENTITY_INSERT dropdown.IncidentDirection ON;
INSERT INTO dropdown.IncidentDirection (IncidentDirectionID, IncidentDirectionName, DisplayOrder) VALUES (0, 'Unknown', 1);
SET IDENTITY_INSERT dropdown.IncidentDirection OFF;

INSERT INTO dropdown.IncidentDirection 
	(IncidentDirectionName, DisplayOrder) 
VALUES 
	('Main Actor', 2),
	('Target', 3)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentDirection'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'IncidentDirection'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIncidentDirectionData'
GO
--End data for table dropdown.IncidentDirection

--Begin data for table dropdown.IncidentSource
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentSource'
GO
--End data for table dropdown.IncidentSource

--Begin data for table dropdown.IncidentType
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType'
GO
--End data for table dropdown.IncidentType

--Begin function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIncidentTypeCategoryIDByIncidentTypeCategory'
GO
--End function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory

--Begin procedure territory.GetProjectTerritoryTypesByProjectID
EXEC implementer.ImplementerSynonymAddUpdate 'territory', 'GetProjectTerritoryTypesByProjectID'
GO
--End procedure territory.GetProjectTerritoryTypesByProjectID

--End file ELI0000 - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Budgets', 'Budgets', 0;
EXEC permissionable.SavePermissionableGroup 'Contacts', 'Contacts', 0;
EXEC permissionable.SavePermissionableGroup 'Documents', 'Documents', 0;
EXEC permissionable.SavePermissionableGroup 'Equipment', 'Equipment', 0;
EXEC permissionable.SavePermissionableGroup 'ForceAssets', 'Forces & Assets', 0;
EXEC permissionable.SavePermissionableGroup 'Research', 'Insight & Understanding', 0;
EXEC permissionable.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0;
EXEC permissionable.SavePermissionableGroup 'ProductDistributor', 'Product Distribution', 0;
EXEC permissionable.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0;
EXEC permissionable.SavePermissionableGroup 'Subcontractor', 'Sub Contractors', 0;
EXEC permissionable.SavePermissionableGroup 'Territories', 'Territories', 0;
EXEC permissionable.SavePermissionableGroup 'Training', 'Training', 0;
EXEC permissionable.SavePermissionableGroup 'Utility', 'Utility', 0;
EXEC permissionable.SavePermissionableGroup 'Workflows', 'Workflows', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the event log', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImplementerSetup', @DESCRIPTION='Add / edit an implementer server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ImplementerSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImplementerSetup', @DESCRIPTION='View the list of implementer server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ImplementerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a system user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of system users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a system user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='addupdate social media info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addupdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.addupdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='social media list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='list', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.list', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='refresh social media data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='refresh', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.refresh', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='socialmedia', @DESCRIPTION='renew token', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='renew', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='socialmedia.renew', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a system server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='View the list of system server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='Add/Update a budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='User can edit all budget data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.AddUpdate.CanEditBudget', @PERMISSIONCODE='CanEditBudget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='User is a spender and can only edit a subset of Budget Item data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.AddUpdate.IsSpender', @PERMISSIONCODE='IsSpender';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View budget list page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View inactive items on the budget list page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.List.CanHaveInactive', @PERMISSIONCODE='CanHaveInactive';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View a budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budgets', @PERMISSIONABLELINEAGE='Budget.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts in the vetting process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Delete unassociated items in the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List.Delete', @PERMISSIONCODE='Delete';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type additional activity in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.AdditionalActivity', @PERMISSIONCODE='AdditionalActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type general in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.General', @PERMISSIONCODE='General';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type legal & policy in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Legal&Policy', @PERMISSIONCODE='Legal&Policy';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type leonardo administration in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LeonardoAdministration', @PERMISSIONCODE='LeonardoAdministration';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type leonardo user guides in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LeonardoUserGuides', @PERMISSIONCODE='LeonardoUserGuides';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type log frame in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.LogFrame', @PERMISSIONCODE='LogFrame';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type m & e plan in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.M&EPlan', @PERMISSIONCODE='M&EPlan';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type media in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type meeting in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Meeting', @PERMISSIONCODE='Meeting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type monthly activty in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.MonthlyActivity', @PERMISSIONCODE='MonthlyActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type other in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type plan in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Plan', @PERMISSIONCODE='Plan';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type presentations in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Presentations', @PERMISSIONCODE='Presentations';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type procurement in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Procurement', @PERMISSIONCODE='Procurement';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type program in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Program', @PERMISSIONCODE='Program';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type quality assurance feedback document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.QualityAssuranceFeedbackDocument', @PERMISSIONCODE='QualityAssuranceFeedbackDocument';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type quarterly activity in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.QuarterlyActivity', @PERMISSIONCODE='QuarterlyActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type reporting in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Reporting', @PERMISSIONCODE='Reporting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type rfi response in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.RFIResponse', @PERMISSIONCODE='RFIResponse';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type situational in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Situational', @PERMISSIONCODE='Situational';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type spot in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Spot', @PERMISSIONCODE='Spot';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type trainer 1 document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Trainer1Document', @PERMISSIONCODE='Trainer1Document';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type trainer 2 document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Trainer2Document', @PERMISSIONCODE='Trainer2Document';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type training in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Training', @PERMISSIONCODE='Training';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of forces', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAssets', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View the list of atmospheric reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConflictStory', @DESCRIPTION='Add / edit a conflict vignette', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ConflictStory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConflictStory', @DESCRIPTION='View the list of conflict vignettes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ConflictStory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConflictStory', @DESCRIPTION='View a conflict vignette', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='ConflictStory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='Add / edit a field report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='View the list of field reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='FieldReport', @DESCRIPTION='View a field report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='FieldReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View the list of findings', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View the list of recommendations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View the list of risks', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export the list of risks', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View the list of indicators', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicator type', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View the list of indicator types', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View the list of milestones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View the objectives overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View the list of objectives', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage objectives', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View objective overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='Add / edit a campaign', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='View the list of campaigns', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Campaign', @DESCRIPTION='View a campaign', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Campaign.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='Add / edit a distributor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='View the list of distributors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Distributor', @DESCRIPTION='View a distributor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Distributor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='Add / edit an Impact Story', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='View the list of Impact Stories', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ImpactStory', @DESCRIPTION='View an Impact Story', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ImpactStory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='Add / edit a product', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View the list of products', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View the product overview', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Product', @DESCRIPTION='View a product', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='Product.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProductReport', @DESCRIPTION='List Product Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProductDistributor', @PERMISSIONABLELINEAGE='ProductReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View the list of program reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='Add / edit a sub-contractor capability', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateCapability', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.AddUpdateCapability', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View the list of sub-contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View the list of Sub-contractor Capabilities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListCapabilities', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.ListCapabilities', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Subcontractor', @DESCRIPTION='View a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='Subcontractor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Community.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit the territory portion of a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Community.AddUpdate.Territory', @PERMISSIONCODE='Territory';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Community.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a the activities tab for a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.View.ActivitiesTab', @PERMISSIONCODE='ActivitiesTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a the insights tab for a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territories', @PERMISSIONABLELINEAGE='Territory.View.InsightsTab', @PERMISSIONCODE='InsightsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='Add / edit an activity', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='View the list of activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Activity', @DESCRIPTION='View an activity', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Activity.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit an activity report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Activity Report Export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReport.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of activity reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View an activity report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReportAggregator', @DESCRIPTION='Add / edit an activity report aggregation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReportAggregator', @DESCRIPTION='ActivityReportAggregator export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReportAggregator', @DESCRIPTION='View the list of aggregated activity reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='ActivityReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a trend report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='trendreport', @DESCRIPTION='trend report pdf export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='trendreport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a trend report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='TrendReportAggregator export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Utility', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.02 - 2017.08.29 19.52.26'
GO
--End build tracking

