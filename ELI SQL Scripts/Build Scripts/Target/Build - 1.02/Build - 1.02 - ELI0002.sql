-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.02 - 2017.08.29 19.52.26

USE ELI0002
GO

-- ==============================================================================================================================
-- Functions:
--		asset.GetAssetCountByTerritory
--		conflictstory.GetConflictStoryCountByTerritory
--		contact.GetContactCountByTerritory
--		incident.GetIncidentCountByTerritory
--		incident.GetIncidentCountByTerritoryByMonth
--
-- Procedures:
--		conflictstory.GetConflictStoryCountsByDistrict
--		conflictstory.GetConflictStoryCountsByGovernorate
--		contact.GetContactByContactID
--		contact.GetContactCountsByDistrict
--		contact.GetContactCountsByGovernorate
--		incident.GetIncidentByIncidentID
--		incident.GetMonthlyIncidentCountsByGovernorate
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategory
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
--		portalupdate.UpdateDataBySchemaName
--		territory.GetTerritoriesForMap
--		territory.GetTerritoryByTerritoryID
--
-- Tables:
--		contact.ContactContactExperience
--		territory.TerritoryEntityCount
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO

--Begin table asset.Asset
DECLARE @TableName VARCHAR(250) = 'asset.Asset'

EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'
GO

ALTER TABLE asset.Asset ALTER COLUMN AssetName NVARCHAR(250)
GO
--End table asset.Asset

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'EducationLevelID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IsAidRecipient', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'IsTrainer', 'BIT', '0'
GO
--End table contact.Contact

--Begin table incident.Incident
DECLARE @TableName VARCHAR(250) = 'incident.Incident'

EXEC utility.AddColumn @TableName, 'ArabicSummary', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Attribution', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ConfidenceLevelID', 'INT', '0'
GO

EXEC utility.DropObject 'incident.TR_Incident'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.08.13
-- Description:	A trigger to update the incident.Incident table
-- ============================================================
CREATE TRIGGER incident.TR_Incident ON incident.Incident FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	UPDATE I1
	SET I1.IncidentName = 

		CASE
			WHEN I2.TerritoryID = 0
			THEN ''
			ELSE (SELECT T.TerritoryName FROM territory.Territory T WHERE T.TerritoryID = I2.TerritoryID) + ' - '
		END + 

		CASE
			WHEN I2.IncidentTypeID = 0
			THEN ''
			ELSE IT.IncidentTypeName + ' - '
		END + 

		CASE
			WHEN I2.IncidentDateTime IS NULL
			THEN ''
			ELSE CONVERT(VARCHAR(10), I2.IncidentDateTime, 101)
		END

	FROM incident.Incident I1
		JOIN INSERTED I2 ON I2.IncidentID = I1.IncidentID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I2.IncidentTypeID

	END
--ENDIF
GO		
--End table incident.Incident

--Begin table contact.ContactContactExperience
DECLARE @TableName VARCHAR(250) = 'contact.ContactContactExperience'

EXEC utility.DropObject @TableName

CREATE TABLE contact.ContactContactExperience
	(
	ContactContactExperienceID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	ContactExperienceID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactExperienceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactContactExperienceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactContactExperience', 'ContactID,ContactExperienceID'
GO
--End table contact.ContactContactExperiencer

--Begin table incident.IncidentForce
DECLARE @TableName VARCHAR(250) = 'incident.IncidentForce'

EXEC utility.AddColumn @TableName, 'IncidentDirectionID', 'INT', '0'
GO
--End table incident.IncidentForce

--Begin table territory.TerritoryEntityCount
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryEntityCount'

EXEC utility.DropObject @TableName

CREATE TABLE territory.TerritoryEntityCount
	(
	TerritoryEntityCountID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	EntityTypeCode VARCHAR(50),
	EntityCount INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryEntityCountID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryEntityCount', 'TerritoryID,ProjectID,EntityTypeCode,EntityCount'
GO
--End table contact.TerritoryEntityCount

--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO

--Begin function asset.GetAssetCountByTerritory
EXEC utility.DropObject 'asset.GetAssetCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of assets in a given territory
-- ========================================================================
CREATE FUNCTION asset.GetAssetCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nAssetCount INT = 0

	SELECT @nAssetCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = A.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = A.ProjectID

	RETURN ISNULL(@nAssetCount, 0)

END
GO
--End function asset.GetAssetCountByTerritory

--Begin function conflictstory.GetConflictStoryCountByTerritory
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of conflict stories in a given territory
-- ==================================================================================
CREATE FUNCTION conflictstory.GetConflictStoryCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nConflictStoryCount INT = 0

	SELECT @nConflictStoryCount = COUNT(CS.ConflictStoryID)
	FROM conflictstory.ConflictStory CS
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = CS.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CS.ProjectID

	RETURN ISNULL(@nConflictStoryCount, 0)

END
GO
--End function conflictstory.GetConflictStoryCountByTerritory

--Begin function contact.GetContactCountByTerritory
EXEC utility.DropObject 'contact.GetContactCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of contacts in a given territory
-- ==========================================================================
CREATE FUNCTION contact.GetContactCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nContactCount INT = 0

	SELECT @nContactCount = COUNT(C.ContactID)
	FROM contact.Contact C
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = C.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProjectID

	RETURN ISNULL(@nContactCount, 0)

END
GO
--End function contact.GetContactCountByTerritory

--Begin function incident.GetIncidentCountByTerritory
EXEC utility.DropObject 'incident.GetIncidentCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory
-- ===========================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritory
(
@TerritoryID INT,
@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0
	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT @nIncidentCount = COUNT(I.IncidentID)
	FROM incident.Incident I
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = I.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritory

--Begin function incident.GetIncidentCountByTerritoryByMonth
EXEC utility.DropObject 'incident.GetIncidentCountByTerritoryByMonth'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory for a given month
-- =============================================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritoryByMonth
(
@TerritoryID INT,
@Month INT,
@Year INT,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0

	DECLARE @DateStart DATE = CAST(CAST(@Month AS VARCHAR(2)) + '/1/' + CAST(@Year AS VARCHAR(4)) AS DATE) 
	DECLARE @DateStop DATE = EOMONTH(@DateStart)

	SELECT @nIncidentCount = incident.GetIncidentCountByTerritory(@TerritoryID, @DateStart, @DateStop, @IncidentTypeIDList, @ProjectIDList)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritoryByMonth

--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.Alignment,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.InfluenceDepth,
		C1.InfluenceReach,
		C1.IsActive,
		C1.IsAidRecipient,
		C1.IsTrainer,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.Position,
		C1.PostalCode,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName,
		EL.EducationLevelID,
		EL.EducationLevelName,
		(
			SELECT DocumentData 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = @ContactID
		) AS BioImageData,
		(
			SELECT DocumentTitle 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = @ContactID
		) AS BioImage,
		(
			SELECT ContentSubType 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = @ContactID
		) AS BioImageType
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.EducationLevel EL ON EL.EducationLevelID = C1.EducationLevelID
			AND C1.ContactID = @ContactID
			AND C1.ProjectID = @ProjectID

	--ContactContactExperience
	SELECT
		CE.ContactExperienceID,
		CE.ContactExperienceName
	FROM contact.ContactContactExperience CCE
		JOIN dropdown.ContactExperience CE ON CE.ContactExperienceID = CCE.ContactExperienceID
			AND CCE.ContactID = @ContactID
			AND CCE.ProjectID = @ProjectID

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
			AND CCT.ProjectID = @ProjectID

	--ContactCourse
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM course.Course C
		JOIN course.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID
			AND CC.ProjectID = @ProjectID

	--ContactEquipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND C.ProjectID = @ProjectID
			AND EI.AssigneeTypeCode = 'Contact'
			AND EI.ProjectID = 	C.ProjectID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID

	--ContactVetting
	SELECT
		CV.VettingDate,
		core.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM contact.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
			AND CV.ProjectID = @ProjectID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure contact.GetContactCountsByDistrict
EXEC utility.DropObject 'contact.GetContactCountsByDistrict'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return contact counts for a district
-- ================================================================
CREATE PROCEDURE contact.GetContactCountsByDistrict

@ParentTerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList) AS ContactCount
	FROM territory.Territory T
		CROSS JOIN contact.Contact C
	WHERE T.TerritoryTypeCode = 'District'
		AND T.ParentTerritoryID = @ParentTerritoryID
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure contact.GetContactCountsByDistrict

--Begin procedure contact.GetContactCountsByGovernorate
EXEC utility.DropObject 'contact.GetContactCountsByGovernorate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return contact counts for the governorates
-- ======================================================================
CREATE PROCEDURE contact.GetContactCountsByGovernorate

@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList) AS ContactCount
	FROM territory.Territory T
		CROSS JOIN contact.Contact C
	WHERE T.TerritoryTypeCode = 'Governorate'
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure contact.GetContactCountsByGovernorate

--Begin procedure conflictstory.GetConflictStoryCountsByDistrict
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountsByDistrict'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return conflictstory counts for a district
-- ======================================================================
CREATE PROCEDURE conflictstory.GetConflictStoryCountsByDistrict

@ParentTerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList) AS ConflictStoryCount
	FROM territory.Territory T
		CROSS JOIN conflictstory.ConflictStory C
	WHERE T.TerritoryTypeCode = 'District'
		AND T.ParentTerritoryID = @ParentTerritoryID
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure conflictstory.GetConflictStoryCountsByDistrict

--Begin procedure conflictstory.GetConflictStoryCountsByGovernorate
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountsByGovernorate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return conflictstory counts for the governorates
-- ============================================================================
CREATE PROCEDURE conflictstory.GetConflictStoryCountsByGovernorate

@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList) AS ConflictStoryCount
	FROM territory.Territory T
		CROSS JOIN conflictstory.ConflictStory C
	WHERE T.TerritoryTypeCode = 'Governorate'
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure conflictstory.GetConflictStoryCountsByGovernorate

--Begin procedure incident.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
EXEC Utility.DropObject 'incident.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetIncidentByIncidentID

@IncidentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Incident
	SELECT
		I.ArabicSummary,
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		INS.IncidentSourceID,
		INS.IncidentSourceName
	FROM incident.Incident I
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = I.ConfidenceLevelID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.IncidentSource INS ON INS.IncidentSourceID = I.IncidentSourceID
			AND I.IncidentID = @IncidentID
			AND I.ProjectID = @ProjectID

	--IncidentForce
	SELECT
		F.ForceID,
		F.ForceName,
		ID.IncidentDirectionID,
		ID.IncidentDirectionName
	FROM incident.IncidentForce INF
		JOIN force.Force F ON F.ForceID = INF.ForceID
		JOIN dropdown.IncidentDirection ID ON ID.IncidentDirectionID = INF.IncidentDirectionID
			AND INF.IncidentID = @IncidentID
			AND INF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

END
GO
--End procedure incident.GetIncidentByIncidentID

--Begin procedure incident.GetIncidentCountsByIncidentTypeCategory
EXEC utility.DropObject 'incident.GetIncidentCountsByIncidentTypeCategory'
GO
--End procedure incident.GetIncidentCountsByIncidentTypeCategory

--Begin procedure incident.GetMonthlyIncidentCountsByGovernorate
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByGovernorate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByGovernorate

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(T.TerritoryID AS VARCHAR(5)) AS ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		T.TerritoryName,
		incident.GetIncidentCountByTerritoryByMonth(T.TerritoryID, DATEPART(MONTH, I.IncidentDateTime), DATEPART(YEAR, I.IncidentDateTime), NULL, @ProjectIDList) AS IncidentCount 
	FROM territory.Territory T	
	CROSS JOIN incident.Incident I
	WHERE 1 = 1
		AND ((@TerritoryID = 0 AND T.TerritoryTypeCode = 'Governorate') OR T.ParentTerritoryID = @TerritoryID)
		AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
		AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
		AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), T.TerritoryName, T.TerritoryID
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT
		T.TerritoryID,
		T.TerritoryName
	FROM territory.Territory T	
	WHERE 1 = 1
		AND ((@TerritoryID = 0 AND T.TerritoryTypeCode = 'Governorate') OR T.ParentTerritoryID = @TerritoryID)
	ORDER BY 2, 1

END
GO
--End procedure incident.GetMonthlyIncidentCountsByGovernorate

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategory

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeCategory,
		COUNT(I.IncidentID) IncidentCount 
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeCategory
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategory

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeCategory,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

--Begin procedure portalupdate.UpdateDataBySchemaName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data for tables in a specific schema
-- =====================================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaName

@SchemaName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cImplementerPortalCode VARCHAR(MAX) = core.GetImplementerSetupValueBySetupKey('ImplementerPortalCode', '')
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTableName VARCHAR(50)

	IF LEN(RTRIM(@cImplementerPortalCode)) > 0
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		IF @PrintSQL = 0
			CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)
		ELSE
			print 'CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)'
		--ENDIF

		SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @SchemaName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @SchemaName + ' T WHERE 1 = 1'
		IF @PrimaryKey > 0
			SET @cSQL += ' AND T.' + @SchemaName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
		--ENDIF

		IF EXISTS (SELECT 1 FROM core.EntityType ET WHERE ET.EntityTypeCode = @SchemaName AND ET.HasWorkflow = 1)
			SET @cSQL += ' AND workflow.GetWorkflowStepNumber(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID) > workflow.GetWorkflowStepCount(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID)'
		--ENDIF

		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				T.Name
			FROM sys.Tables T
				JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
					AND S.Name = @SchemaName
					AND T.Name NOT LIKE '%Import'
			ORDER BY 1

		OPEN oCursor
		FETCH oCursor INTO @cTableName
		WHILE @@fetch_status = 0
			BEGIN	

			SET @cSQL = 'DELETE T1 FROM ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
			IF @PrintSQL = 0
				EXEC (@cSQL)
			ELSE
				print @cSQL
			--ENDIF

			SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', ')
			SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', T1.')

			SET @cSQL = 'INSERT INTO ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @cTableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
			IF @PrintSQL = 0
				EXEC (@cSQL)
			ELSE
				print @cSQL
			--ENDIF

			FETCH oCursor INTO @cTableName

			END
		--END WHILE

		CLOSE oCursor
		DEALLOCATE oCursor

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure portalupdate.UpdateDataBySchemaName

--Begin procedure territory.GetTerritoriesForMap
EXEC utility.DropObject 'territory.GetTerritoriesForDashboardMap'
EXEC utility.DropObject 'territory.GetTerritoriesForMap'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.18
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoriesForMap

@Boundary VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX),
@IncidentTypeIDList VARCHAR(MAX),
@DateStart DATE,
@DateStop DATE,
@ImplementerCode VARCHAR(50),
@TerritoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID,
		T.TerritoryTypeCode,
		T.TerritoryName,
		T.Location.STAsText() AS Location,
		IDD.ProjectID,
		CASE
			WHEN T.TerritoryTypeCode = 'Governorate'
			THEN incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, @IncidentTypeIDList, @ProjectIDList)
			ELSE 0
		END AS FilteredIncidentCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Incident' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredIncidentCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Contact' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredContactCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'ConflictStory' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredConflictStoryCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Asset' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredAssetCount
	FROM territory.Territory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Territory'
			AND (@Boundary IS NULL OR GEOMETRY::STGeomFromText(@Boundary, 4326).STIntersects(T.Location) = 1)
			AND (LEN(RTRIM(@ProjectIDList)) = 0  OR IDD.ProjectID IN (@ProjectIDList))
			AND (@TerritoryID = 0 OR T.TerritoryID = @TerritoryID OR T.ParentTerritoryID = @TerritoryID)
	
END
GO
--End procedure territory.GetTerritoriesForMap

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
--
-- Author:			Jonathan Burnham
-- Create date:	2017.07.24
-- Description:	Modifying to add children to governorates and districts
-- ====================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName,
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName,
		@ProjectID AS ProjectID	
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--Children
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural,
		@ProjectID AS ProjectID	
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.ParentTerritoryID = @TerritoryID
	ORDER BY 2, 1 --DO NOT CHANGE THIS WITHOUT TALKING TO Kevin or Todd - charts need this order
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName,
		@ProjectID AS ProjectID	
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName,
		@ProjectID AS ProjectID	
	FROM Force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName,
		@ProjectID AS ProjectID	
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,
		@ProjectID AS ProjectID	
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName,
		@ProjectID AS ProjectID	
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName,
		@ProjectID AS ProjectID	
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO

TRUNCATE TABLE territory.TerritoryEntityCount
GO

--Asset
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	A.ProjectID,
	T.TerritoryID,
	'Asset',
	asset.GetAssetCountByTerritory(T.TerritoryID, A.ProjectID)
FROM territory.Territory T 
	CROSS JOIN asset.Asset A
GROUP BY A.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, A.ProjectID
GO

--ConflictStory
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	CS.ProjectID,
	T.TerritoryID,
	'ConflictStory',
	conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, CS.ProjectID)
FROM territory.Territory T 
	CROSS JOIN conflictstory.ConflictStory CS
GROUP BY CS.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, CS.ProjectID
GO

--Contact
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	C.ProjectID,
	T.TerritoryID,
	'Contact',
	contact.GetContactCountByTerritory(T.TerritoryID, C.ProjectID)
FROM territory.Territory T 
	CROSS JOIN contact.Contact C
GROUP BY C.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, C.ProjectID
GO

--Incident
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	I.ProjectID,
	T.TerritoryID,
	'Incident',
	incident.GetIncidentCountByTerritory(T.TerritoryID, NULL, NULL, NULL, I.ProjectID)
FROM territory.Territory T 
	CROSS JOIN incident.Incident I
GROUP BY I.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, I.ProjectID
GO
--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.02 - 2017.08.29 19.52.26'
GO
--End build tracking

