-- File Name:	ELI0001.sql
-- Build Key:	Build - 1.05 - 2017.12.03 13.18.27

USE ELI0001
GO

-- ==============================================================================================================================
-- Procedures:
--		[conflictstory].[GetRandomConflictStory]
--		[incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]
--		contact.GetContactByContactID
--		contact.GetRandomContact
--		document.DeleteDocumentByDocumentID
--		incident.GetIncidentByIncidentID
--		incident.GetMonthlyIncidentCountsByIncidentType
--		person.GetNewsFeed
--		territory.GetTerritoriesForMap
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0001
GO

EXEC utility.AddColumn 'contact.Contact', 'SummaryBiography', 'NVARCHAR(MAX)'
GO

--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0001
GO


--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0001
GO

--Begin procedure conflictstory.GetRandomConflictStory
EXEC utility.DropObject 'conflictstory.GetRandomConflictStory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Christopher Crouch
-- Create date:	2017.11.19
-- Description:	This gets a random conflict story
-- ==============================================
CREATE PROCEDURE [conflictstory].[GetRandomConflictStory] 

AS
BEGIN
	SET NOCOUNT ON;

  --ConflictStory
	SELECT TOP 1
		CS.ConflictStoryID,
		CS.ConflictStoryName,
		CS.ConflictStoryYear,
		CS.ProjectID,
		dropdown.GetProjectNameByProjectID(CS.ProjectID) AS ProjectName,
		CS.Summary,
		CSS.ConflictStorySourceID,
		CSS.ConflictStorySourceName,
		MO.MediationOutcomeID,
		MO.MediationOutcomeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		T.TerritoryName
	FROM conflictstory.ConflictStory CS
		JOIN dropdown.ConflictStorySource CSS ON CSS.ConflictStorySourceID = CS.ConflictStorySourceID
		JOIN dropdown.MediationOutcome MO ON MO.MediationOutcomeID = CS.MediationOutcomeID
		JOIN territory.Territory T ON T.TerritoryID = CS.TerritoryID
	ORDER BY newid()

END
GO
--End procedure conflictstory.GetRandomConflictStory

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.Alignment,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.InfluenceDepth,
		C1.InfluenceReach,
		C1.IsActive,
		C1.IsAidRecipient,
		C1.IsTrainer,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.Position,
		C1.PostalCode,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.SummaryBiography,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName,
		EL.EducationLevelID,
		EL.EducationLevelName,
		(
		SELECT DD.DocumentData 
		FROM ELI0000.document.Document DD
			JOIN document.DocumentEntity DE ON DE.DocumentID = DD.DocumentID
				AND DE.EntityTypeCode = 'Contact' AND DE.EntityTypeSubCode = 'BioImage'
				AND DE.EntityID = @ContactID
		) AS BioImageData,
		(
		SELECT DocumentTitle 
		FROM ELI0000.document.Document DD
			JOIN document.DocumentEntity DE ON DE.DocumentID = DD.DocumentID
				AND DE.EntityTypeCode = 'Contact' AND DE.EntityTypeSubCode = 'BioImage'
				AND DE.EntityID = @ContactID
		) AS BioImage,
		(
		SELECT ContentSubType 
		FROM ELI0000.document.Document DD
			JOIN document.DocumentEntity DE ON DE.DocumentID = DD.DocumentID
				AND DE.EntityTypeCode = 'Contact' AND DE.EntityTypeSubCode = 'BioImage'
				AND DE.EntityID = @ContactID
		) AS BioImageType
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.EducationLevel EL ON EL.EducationLevelID = C1.EducationLevelID
			AND C1.ContactID = @ContactID
			AND C1.ProjectID = @ProjectID

	--ContactContactExperience
	SELECT
		CE.ContactExperienceID,
		CE.ContactExperienceName
	FROM contact.ContactContactExperience CCE
		JOIN dropdown.ContactExperience CE ON CE.ContactExperienceID = CCE.ContactExperienceID
			AND CCE.ContactID = @ContactID
			AND CCE.ProjectID = @ProjectID

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
			AND CCT.ProjectID = @ProjectID

	--ContactCourse
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM course.Course C
		JOIN course.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID
			AND CC.ProjectID = @ProjectID

	--ContactEquipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND C.ProjectID = @ProjectID
			AND EI.AssigneeTypeCode = 'Contact'
			AND EI.ProjectID = 	C.ProjectID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID

	--ContactVetting
	SELECT
		CV.VettingDate,
		core.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM contact.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
			AND CV.ProjectID = @ProjectID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure contact.GetRandomContact
EXEC utility.DropObject 'contact.GetRandomContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================
-- Author:			Christopher Crouch
-- Create date:	2017.11.19
-- Description:	This gets a random contact
-- =======================================
CREATE PROCEDURE contact.GetRandomContact

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT TOP 1
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.Alignment,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.InfluenceDepth,
		C1.InfluenceReach,
		C1.IsActive,
		C1.IsAidRecipient,
		C1.IsTrainer,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.Position,
		C1.PostalCode,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName,
		EL.EducationLevelID,
		EL.EducationLevelName,
		(
			SELECT DocumentData 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImageData,
		(
			SELECT DocumentTitle 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImage,
		(
			SELECT ContentSubType 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImageType
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.EducationLevel EL ON EL.EducationLevelID = C1.EducationLevelID
	ORDER BY newid()

END
GO
--End procedure contact.GetRandomContact

--Begin procedure document.DeleteDocumentByDocumentID
EXEC utility.DropObject 'document.DeleteDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Christopher Crouch
-- Create date:	2017.11.26
-- Description:	A stored procedure to delete data from the document tables
-- =======================================================================
CREATE PROCEDURE document.DeleteDocumentByDocumentID

@DocumentID INT

AS
BEGIN

	DELETE
	FROM document.Document
	WHERE DocumentID = @DocumentID

	DELETE
	FROM document.DocumentEntity
	WHERE DocumentID = @DocumentID

	DELETE
	FROM document.DocumentDocumentIssue
	WHERE DocumentID = @DocumentID

END
GO
--End procedure document.DeleteDocumentByDocumentID

--Begin procedure incident.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
EXEC Utility.DropObject 'incident.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetIncidentByIncidentID

@IncidentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Incident
	SELECT
		I.ArabicSummary,
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS TerritoryLocation,
		CL.ConfidenceLevelID,
		CL.ConfidenceLevelName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		INS.IncidentSourceID,
		INS.IncidentSourceName
	FROM incident.Incident I
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = I.ConfidenceLevelID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.IncidentSource INS ON INS.IncidentSourceID = I.IncidentSourceID
		JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
			AND I.IncidentID = @IncidentID
			AND I.ProjectID = @ProjectID

	--IncidentForce
	SELECT
		F.ForceID,
		F.ForceName,
		ID.IncidentDirectionID,
		ID.IncidentDirectionName
	FROM incident.IncidentForce INF
		JOIN force.Force F ON F.ForceID = INF.ForceID
		JOIN dropdown.IncidentDirection ID ON ID.IncidentDirectionID = INF.IncidentDirectionID
			AND INF.IncidentID = @IncidentID
			AND INF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

END
GO
--End procedure incident.GetIncidentByIncidentID

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentType
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentType'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentType

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT 
		CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
		IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
		IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
		IT.IncidentTypeID,
		COUNT(I.IncidentID) IncidentCount
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
	GROUP BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime), IT.IncidentTypeID
	ORDER BY DATEPART(YEAR, I.IncidentDateTime), DATEPART(MONTH, I.IncidentDateTime)

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentType

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [incident].[GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID]

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(IT.IncidentTypeID AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeID,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeID,
		IT.IncidentTypeName
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
	ORDER BY IT.IncidentTypeName

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeAndTerritoryID

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		'fa fa-fw fa-tasks' AS Icon,
		AR.ActivityReportID AS EntityID,
		AR.ActivityReportTitle AS Title,
		AR.UpdateDateTime,
		core.FormatDate(AR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM activityreport.ActivityReport AR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ActivityReport'
		JOIN dropdown.Project P ON P.ProjectID = AR.ProjectID
			AND AR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = AR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-file-text' AS Icon,
		DOC.DocumentID AS EntityID,
		DOC.DocumentTitle AS Title,
		DOC.DocumentDate AS UpdateDateTime,
		core.FormatDate(DOC.DocumentDate) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM ELI0000.document.Document DOC
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'Document'
		JOIN dropdown.Project P ON P.ProjectID = DOC.ProjectID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = DOC.DocumentTypeID
			AND DOC.DocumentDate >= DATEADD(d, -14, getDate())
			AND 
				(
				EXISTS 
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE PP.PersonID = @PersonID
						AND PP.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
					)
				OR DOC.DocumentTypeID = 0	
				)
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = DOC.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-exclamation-circle' AS Icon,
		I.ImpactStoryID AS EntityID,
		I.ImpactStoryName AS Title,
		I.UpdateDateTime,
		core.FormatDate(I.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM impactstory.ImpactStory I
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ImpactStory'
		JOIN dropdown.Project P ON P.ProjectID = I.ProjectID
			AND I.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = I.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM dbo.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed

--Begin procedure territory.GetTerritoriesForMap
EXEC utility.DropObject 'territory.GetTerritoriesForMap'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.18
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoriesForMap

@Boundary VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX),
@ContactTypeIDList VARCHAR(MAX),
@IncidentSourceIDList VARCHAR(MAX),
@IncidentTypeIDList VARCHAR(MAX),
@MediationOutcomeIDList VARCHAR(MAX),
@DateStart DATE,
@DateStop DATE,
@ImplementerCode VARCHAR(50),
@TerritoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID,
		T.TerritoryTypeCode,
		T.TerritoryName,
		T.Location.STAsText() AS Location,
		IDD.ProjectID,
		CASE
			WHEN T.TerritoryTypeCode IN ('Governorate','District')
			THEN incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, @IncidentSourceIDList, @IncidentTypeIDList, @ProjectIDList)
			ELSE 0
		END AS FilteredIncidentCount,
		CASE
			WHEN T.TerritoryTypeCode IN ('Governorate','District')
			THEN contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList, @ContactTypeIDList)
			ELSE 0
		END AS FilteredContactCount,
		CASE
			WHEN T.TerritoryTypeCode IN ('Governorate','District')
			THEN conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList, @MediationOutcomeIDList)
			ELSE 0
		END AS FilteredConflictStoryCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Incident' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredIncidentCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Contact' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredContactCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'ConflictStory' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredConflictStoryCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Asset' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredAssetCount
	FROM territory.Territory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Territory'
			AND (@Boundary IS NULL OR GEOMETRY::STGeomFromText(@Boundary, 4326).STIntersects(T.Location) = 1)
			AND (LEN(RTRIM(@ProjectIDList)) = 0  OR IDD.ProjectID IN (@ProjectIDList))
			AND (@TerritoryID = 0 OR T.TerritoryID = @TerritoryID OR T.ParentTerritoryID = @TerritoryID)
	
END
GO
--End procedure territory.GetTerritoriesForMap
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0001
GO

--Begin table permissionable.Permissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable



--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin post process file ELI0001.sql
--Begin strip identity specifications and create composite primary keys
DECLARE @cColumnName VARCHAR(250)
DECLARE @cIndexName VARCHAR(250)
DECLARE @cIndexType VARCHAR(50)
DECLARE @cNewColumnName VARCHAR(250)
DECLARE @cSchemaName VARCHAR(50)
DECLARE @cSchemaNameTableName VARCHAR(300)
DECLARE @cSchemaNameTableNameColumnName VARCHAR(500)
DECLARE @cSQLText VARCHAR(MAX)
DECLARE @cTableName VARCHAR(250)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		S1.Name AS SchemaName,
		T1.Name AS TableName,
		C1.Name AS ColumnName,
		I.IndexName,
		I.IndexType
	FROM sys.Tables T1
		JOIN sys.Schemas S1 ON S1.Schema_ID = T1.Schema_ID
			AND S1.Name NOT IN ('aggregator', 'core', 'document', 'dropdown', 'eventlog', 'integration', 'person', 'reporting', 'syslog', 'workflow')
		JOIN sys.Columns C1 ON C1.Object_ID = T1.Object_ID
			AND C1.Is_Identity = 1
			AND EXISTS
				(
				SELECT 1
				FROM sys.Tables T2
					JOIN sys.Schemas S2 ON S2.Schema_ID = T2.Schema_ID
					JOIN sys.Columns C2 ON C2.Object_ID = T2.Object_ID
						AND S1.Name = S2.Name
						AND T1.Name = T2.Name
						AND C2.Name = 'ProjectID'
				)
		JOIN
			(
			SELECT 
				C3.Name AS ColumnName,
				I.Name AS IndexName,
				I.type_desc AS IndexType
			FROM sys.indexes I
				JOIN sys.index_columns IC ON IC.Object_ID = I.Object_ID 
					AND IC.Index_ID = I.Index_ID
				JOIN sys.Columns C3 ON C3.Object_ID = IC.Object_ID
					AND C3.Column_ID = IC.Column_ID
					AND I.is_primary_key = 1
			) I ON I.ColumnName = C1.Name
	ORDER BY 1, 2, 3

OPEN oCursor
FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
WHILE @@fetch_status = 0
	BEGIN

	SET @cNewColumnName = @cColumnName + '_NoAN'
	SET @cSchemaNameTableName = @cSchemaName + '.' + @cTableName
	SET @cSchemaNameTableNameColumnName = @cSchemaNameTableName + '.' + @cNewColumnName

	EXEC utility.AddColumn @cSchemaNameTableName, @cNewColumnName, 'INT';

	SET @cSQLText = 'UPDATE T SET T.' + @cNewColumnName + ' = T.' + @cColumnName + ' FROM ' + @cSchemaNameTableName + ' T'
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP CONSTRAINT ' + @cIndexName
	EXEC (@cSQLText);

	SET @cSQLText = 'ALTER TABLE ' + @cSchemaNameTableName + ' DROP COLUMN ' + @cColumnName
	EXEC (@cSQLText);

	EXEC sp_rename @cSchemaNameTableNameColumnName, @cColumnName, 'COLUMN';
	EXEC utility.SetDefaultConstraint @cSchemaNameTableName, @cColumnName, 'INT', '0', 1;

	IF @cIndexType = 'CLUSTERED'
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	ELSE 
		SET @cSQLText = 'EXEC utility.SetPrimaryKeyNonClustered ''' + @cSchemaNameTableName + ''', ' + '''' + @cColumnName + ',ProjectID'''
	--ENDIF

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSchemaName, @cTableName, @cColumnName, @cIndexName, @cIndexType
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End strip identity specifications and create composite primary keys

--Begin drop triggers
DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		'DROP Trigger ' + S.Name + '.' + O.Name AS SQLText
	FROM sysobjects O
		JOIN sys.tables T ON O.Parent_Obj = T.Object_ID
		JOIN sys.schemas S ON T.Schema_ID = S.Schema_ID 
			AND O.Type = 'TR' 

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN

	EXEC (@cSQLText);

	FETCH oCursor INTO @cSQLText
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO
--End drop triggers

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P1.PersonID,
	P2.ProjectID
FROM person.Person P1 
	CROSS APPLY dropdown.Project P2
WHERE P1.IsSuperAdministrator = 1
	AND P2.ProjectID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM person.PersonProject PP
		WHERE PP.PersonID = P1.PersonID
			AND PP.ProjectID = P2.ProjectID
		)
--End post process file ELI0001.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.05 - 2017.12.03 13.18.27')
GO
--End build tracking

