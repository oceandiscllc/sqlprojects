-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.00 - 2017.06.29 22.10.54

USE ELI0002
GO

-- ==============================================================================================================================
-- Functions:
--		spotreport.FormatStaticGoogleMapForSpotReport
--		spotreport.GetSpotReportTerritoriesList
--
-- Procedures:
--		activityreport.GetActivityReportByActivityReportID
--		eventlog.LogIncidentAction
--		force.GetForceByForceID
--		incident.GetIncidentByIncidentID
--		portalupdate.UpdateDataBySchemaName
--		portalupdate.UpdateDataBySchemaNameAndTableName
--		reporting.GetSpotReportBySpotReportID
--		spotreport.GetSpotReportBySpotReportID
--		territory.GetCommunityByTerritoryID
--		territory.GetTerritoryByTerritoryID
--		trendreport.GetTrendReportByTrendReportID
--
-- Schemas:
--		incident
--
-- Tables:
--		incident.IncidentForce
-- ==============================================================================================================================

--Begin file ELI000X - 00 - Prerequisite.sql
USE ELI0002
GO

EXEC utility.AddSchema 'incident'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'dbo' AND T.Name = 'Incident')
	ALTER SCHEMA incident TRANSFER dbo.Incident
--ENDIF
GO

TRUNCATE TABLE syslog.BuildLog
GO
--End file ELI000X - 00 - Prerequisite.sql

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO

--Begin table incident.Incident
DECLARE @TableName VARCHAR(250) = 'incident.Incident'

EXEC utility.DropColumn @TableName, 'Source'
EXEC utility.DropColumn @TableName, 'SourceTypeID'

EXEC utility.AddColumn @TableName, 'IncidentSourceID', 'INT', '0'
GO
--End table incident.Incident

--Begin table incident.IncidentForce
DECLARE @TableName VARCHAR(250) = 'incident.IncidentForce'

EXEC utility.DropObject @TableName

CREATE TABLE incident.IncidentForce
	(
	IncidentForceID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	ForceID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_IncidentForce', 'ProjectID,IncidentID,ForceID'
GO
--End table incident.IncidentForce

--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO

--Begin function spotreport.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'spotreport.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION spotreport.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE 
	  @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@IncidentMarker varchar(max)=''
	
	SELECT
		@IncidentMarker += '&markers=icon:' 
			+ replace(core.GetSystemSetupValueBySetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Location.STY,'') AS VARCHAR(MAX)) 
			+ ','
			 + CAST(ISNULL(I.Location.STX,'') AS VARCHAR(MAX))
	 FROM spotreport.SpotReportIncident SRI
		JOIN incident.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult +=  @IncidentMarker 

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function spotreport.FormatStaticGoogleMapForSpotReport

--Begin function spotreport.GetSpotReportTerritoriesList
EXEC utility.DropObject 'dbo.GetSpotReportTerritoriesList'
EXEC utility.DropObject 'spotreport.GetSpotReportTerritoriesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A function to return territories as a list 
-- =======================================================
CREATE FUNCTION spotreport.GetSpotReportTerritoriesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @TerritoryList VARCHAR(MAX) = ''
	
	SELECT @TerritoryList += territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) + ', '
	FROM incident.Incident I
		JOIN spotreport.SpotReportIncident SRI ON SRI.IncidentID = I.IncidentID 
			AND	SRI.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@TerritoryList)) > 0 
		SET @TerritoryList = RTRIM(SUBSTRING(@TerritoryList, 0, LEN(@TerritoryList)))
	--ENDIF

	RETURN RTRIM(@TerritoryList)

END
GO
--End function spotreport.GetSpotReportTerritoriesList

--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID, @ProjectID)

	--ActivityReport
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail,
		ART.ActivityReportTypeID,
		ART.ActivityReportTypeName
	FROM activityreport.ActivityReport AR
		JOIN dropdown.ActivityReportType ART ON ART.ActivityReportTypeID = AR.ActivityReportTypeID
			AND AR.ActivityReportID = @ActivityReportID
		AND AR.ProjectID = @ProjectID

	--ActivityReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY A.AssetName, A.AssetID

	--ActivityReportAtmospheric
	SELECT
		A.AtmosphericID,
		core.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted,
		ATT.AtmosphericTypeName
	FROM activityreport.ActivityReportAtmospheric ARA
		JOIN atmospheric.Atmospheric A ON A.AtmosphericID = ARA.AtmosphericID
		JOIN dropdown.AtmosphericType ATT ON ATT.AtmosphericTypeID = A.AtmosphericTypeID
			AND ARA.ActivityReportID = @ActivityReportID
			AND ARA.ProjectID = @ProjectID
	ORDER BY ATT.AtmosphericTypeName, A.AtmosphericDate

	--ActivityReportCampaign
	SELECT
		C.CampaignID,
		C.CampaignName
	FROM activityreport.ActivityReportCampaign ARC
		JOIN campaign.Campaign C ON C.CampaignID = ARC.CampaignID
			AND ARC.ActivityReportID = @ActivityReportID
			AND ARC.ProjectID = @ProjectID
	ORDER BY C.CampaignName, C.CampaignID

	--ActivityReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
			AND ARF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

	--ActivityReportImpactStory
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM activityreport.ActivityReportImpactStory ARI
		JOIN impactstory.ImpactStory I ON I.ImpactStoryID = ARI.ImpactStoryID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.ImpactStoryName, I.ImpactStoryID

	--ActivityReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN incident.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
			AND ARI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	--ActivityReportMediaReport
	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM activityreport.ActivityReportMediaReport ARMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = ARMR.MediaReportID
			AND ARMR.ActivityReportID = @ActivityReportID
			AND ARMR.ProjectID = @ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	--ActivityReportProduct
	SELECT
		P.ProductID,
		P.ProductName
	FROM activityreport.ActivityReportProduct ARP
		JOIN product.Product P ON P.ProductID = ARP.ProductID
			AND ARP.ActivityReportID = @ActivityReportID
			AND ARP.ProjectID = @ProjectID
	ORDER BY P.ProductName, P.ProductID

	--ActivityReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM activityreport.ActivityReportRelevantTheme ARRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = ARRT.RelevantThemeID
			AND ARRT.ActivityReportID = @ActivityReportID
			AND ARRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ActivityReportTerritory
	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
			AND ART.ActivityReportID = @ActivityReportID
			AND ART.ProjectID = @ProjectID
	ORDER BY 2, 1

	--ActivityReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'ActivityReport', @ActivityReportID, @ProjectID

	--ActivityReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activityreport.ActivityReport AR ON AR.ActivityReportID = EL.EntityID
			AND AR.ActivityReportID = @ActivityReportID
			AND AR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'ActivityReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--ActivityReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ActivityReport', @ActivityReportID, @ProjectID, @nWorkflowStepNumber

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderContactID,
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		FT.ForceTypeID, 
		FT.ForceTypeName
	FROM Force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
			AND F.ForceID = @ForceID
			AND F.ProjectID = @ProjectID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
			AND FERP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
			AND FFRP.ProjectID = @ProjectID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		FU.ForceUnitID,
		FU.ProjectID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
			AND FU.ProjectID = @ProjectID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure incident.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
EXEC Utility.DropObject 'incident.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetIncidentByIncidentID

@IncidentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Incident
	SELECT
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,		
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		INS.IncidentSourceID,
		INS.IncidentSourceName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.IncidentSource INS ON INS.IncidentSourceID = I.IncidentSourceID
			AND I.IncidentID = @IncidentID
			AND I.ProjectID = @ProjectID

	--IncidentForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM incident.IncidentForce INF
		JOIN force.Force F ON F.ForceID = INF.ForceID
			AND INF.IncidentID = @IncidentID
			AND INF.ProjectID = @ProjectID
	ORDER BY F.ForceName, F.ForceID

END
GO
--End procedure incident.GetIncidentByIncidentID

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Incident'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogIncidentActionTable
		FROM incident.Incident I
		WHERE I.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location
	
		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, @EntityID)
				
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT
			T.*, 
			CAST(('<Location>' + CAST(I.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(@cDocuments AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)
		FROM #LogIncidentActionTable T
			JOIN incident.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure portalupdate.UpdateDataBySchemaName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data for tables in a specific schema
-- =====================================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaName

@SchemaName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cImplementerPortalCode VARCHAR(MAX) = core.GetImplementerSetupValueBySetupKey('ImplementerPortalCode', '')
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTableName VARCHAR(50)

	IF LEN(RTRIM(@cImplementerPortalCode)) > 0
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		IF @PrintSQL = 0
			CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)
		ELSE
			print 'CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)'
		--ENDIF

		SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @SchemaName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @SchemaName + ' T WHERE 1 = 1'
		IF @PrimaryKey > 0
			SET @cSQL += ' AND T.' + @SchemaName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
		--ENDIF

		IF EXISTS (SELECT 1 FROM core.EntityType ET WHERE ET.EntityTypeCode = @SchemaName AND ET.HasWorkflow = 1)
			SET @cSQL += ' AND workflow.GetWorkflowStepNumber(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID) > workflow.GetWorkflowStepCount(''' + @SchemaName + ''', T.' + @SchemaName + 'ID, T.ProjectID)'
		--ENDIF

		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				T.Name
			FROM sys.Tables T
				JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
					AND S.Name = @SchemaName
			ORDER BY 1

		OPEN oCursor
		FETCH oCursor INTO @cTableName
		WHILE @@fetch_status = 0
			BEGIN	

			SET @cSQL = 'DELETE T1 FROM ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
			IF @PrintSQL = 0
				EXEC (@cSQL)
			ELSE
				print @cSQL
			--ENDIF

			SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', ')
			SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @cTableName, ', T1.')

			SET @cSQL = 'INSERT INTO ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @cTableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @cTableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @SchemaName + 'ID AND T2.ProjectID = T1.ProjectID'
			IF @PrintSQL = 0
				EXEC (@cSQL)
			ELSE
				print @cSQL
			--ENDIF

			FETCH oCursor INTO @cTableName

			END
		--END WHILE

		CLOSE oCursor
		DEALLOCATE oCursor

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure portalupdate.UpdateDataBySchemaName

--Begin procedure portalupdate.UpdateDataBySchemaNameAndTableName
EXEC Utility.DropObject 'portalupdate.UpdateDataBySchemaNameAndTableName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to update portal data from local data in a specific table
-- =========================================================================================
CREATE PROCEDURE portalupdate.UpdateDataBySchemaNameAndTableName

@SchemaName VARCHAR(50),
@TableName VARCHAR(50),
@PrimaryKey INT,
@PrintSQL BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cColumnList1 VARCHAR(MAX)
	DECLARE @cColumnList2 VARCHAR(MAX)
	DECLARE @cImplementerPortalCode VARCHAR(MAX) = core.GetImplementerSetupValueBySetupKey('ImplementerPortalCode', '')
	DECLARE @cSQL VARCHAR(MAX)

	IF LEN(RTRIM(@cImplementerPortalCode)) > 0
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		IF @PrintSQL = 0
			CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)
		ELSE
			print 'CREATE TABLE portalupdate.#tPortalUpdate (ID INT NOT NULL PRIMARY KEY, ProjectID INT)'
		--ENDIF

		SET @cSQL = 'INSERT INTO portalupdate.#tPortalUpdate (ID, ProjectID) SELECT T.' + @TableName + 'ID, T.ProjectID FROM ' + @SchemaName + '.' + @TableName + ' T WHERE 1 = 1'
		IF @PrimaryKey > 0
			SET @cSQL += ' AND T.' + @TableName + 'ID = ' + CAST(@PrimaryKey AS VARCHAR(10))
		--ENDIF
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		SET @cSQL = 'DELETE T1 FROM ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		SET @cColumnList1 = portalupdate.GetColumnList(@SchemaName, @TableName, ', ')
		SET @cColumnList2 = portalupdate.GetColumnList(@SchemaName, @TableName, ', T1.')

		SET @cSQL = 'INSERT INTO ' + @cImplementerPortalCode + '.' + @SchemaName + '.' + @TableName + ' (' + @cColumnList1 + ') SELECT ' + @cColumnList2 + ' FROM ' + @SchemaName + '.' + @TableName + ' T1 JOIN portalupdate.#tPortalUpdate T2 ON T2.ID = T1.' + @TableName + 'ID AND T2.ProjectID = T1.ProjectID'
		IF @PrintSQL = 0
			EXEC (@cSQL)
		ELSE
			print @cSQL
		--ENDIF

		IF (SELECT OBJECT_ID('tempdb.portalupdate.#tPortalUpdate', 'U')) IS NOT NULL
			DROP TABLE portalupdate.#tPortalUpdate
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure portalupdate.UpdateDataBySchemaNameAndTableName

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID, @ProjectID ) AS WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID, @ProjectID) AS IsDraft,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		SR.SummaryMap AS GMap,
		spotreport.GetSpotReportTerritoriesList(SR.SpotReportID) AS TerritoryList	
	FROM spotReport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure reporting.GetSpotReportBySpotReportID

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID, @ProjectID)
	
	SELECT
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunicationOpportunities,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SourceDetails,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		core.GetSystemSetupValueBySetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom
	FROM spotreport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN incident.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
			AND SRI.ProjectID = @ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
		AND SRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID, @ProjectID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @ProjectID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND SR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure territory.GetCommunityByTerritoryID
EXEC utility.DropObject 'territory.GetCommunityByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.05.03
-- Description:	A stored procedure to get data from the LEO0000.territory.Territory and LEO000X.territory.TerritoryAnnex tables
-- ============================================================================================================================
CREATE PROCEDURE territory.GetCommunityByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT 
		T.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryNameFormatted,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Location.STAsText() AS Location,
		T.TerritoryName,
		T.TerritoryNameLocal,
		T.Population, 
		T.PopulationSource
	FROM territory.Territory T
	WHERE T.TerritoryID = @TerritoryID

	--TerritoryAnnex
	SELECT 
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.Notes,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName
	FROM territory.TerritoryAnnex TA
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
			AND TA.TerritoryID = @TerritoryID
			AND TA.ProjectID = @ProjectID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.TerritoryID = @TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
			AND C.TerritoryID = @TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
			AND F.TerritoryID = @TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
		WHERE IT.ImpactStoryID = I.ImpactStoryID
			AND IT.TerritoryID = @TerritoryID
			AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND I.TerritoryID = @TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.TerritoryID = @TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
				WHERE T.TerritoryID = @TerritoryID
					AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetCommunityByTerritoryID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to get territory data
-- =====================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		T.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(T.TerritoryID) AS TerritoryName,
		T.Location.STAsText() AS Location,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--Assets
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1
	
	--Courses
	SELECT 
		C.CourseID, 		
		M.ModuleName, 		
		core.FormatDateTime(C.StartDate) AS StartDateFormatted,
		core.FormatDateTime(C.EndDate) AS EndDateFormatted,
		territory.FormatTerritoryNameByTerritoryID(C.TerritoryID) AS TerritoryName
	FROM course.Course C
		JOIN dbo.Module M ON M.ModuleID = C.ModuleID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = C.TerritoryID
			AND C.ProjectID = @ProjectID
	ORDER BY 2, 3, 5, 1

	--Forces
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM Force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--ImpactStories
	SELECT
		I.ImpactStoryID,
		I.ImpactStoryName
	FROM impactstory.ImpactStory I
	WHERE EXISTS 
		(
		SELECT 1
		FROM impactstory.ImpactStoryTerritory IT
			JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = IT.TerritoryID
				AND IT.ImpactStoryID = I.ImpactStoryID
				AND I.ProjectID = @ProjectID
		)
	ORDER BY 2

	--Incidents
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM incident.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--MediaReports
	SELECT 
		MR.MediaReportID,
		MR.MediaReportTitle,
		core.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MRT.MediaReportTypeName,
		territory.FormatTerritoryNameByTerritoryID(MR.TerritoryID) AS TerritoryName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = MR.TerritoryID
			AND MR.ProjectID = @ProjectID
	ORDER BY 2, 4, 5, 3, 1

	--Products
	SELECT 
		P.ProductID,
		P.ProductName,
		core.FormatDate(P.ReleaseDate) AS ReleaseDateFormatted,
		PT.ProductTypeName,
		TA.TargetAudienceName
	FROM product.Product P
		JOIN dropdown.ProductType PT ON PT.ProductTypeID = P.ProductTypeID
		JOIN dropdown.TargetAudience TA ON TA.TargetAudienceID = P.TargetAudienceID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductTerritory T
					JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = T.TerritoryID
						AND T.ProductID = P.ProductID
				)
			AND P.ProjectID = @ProjectID
	ORDER BY 2, 4, 3, 5, 1
	
END
GO
--End procedure territory.GetTerritoryByTerritoryID

--Begin procedure trendreport.GetTrendReportByTrendReportID
EXEC utility.DropObject 'trendreport.GetTrendReportByTrendReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the trendreport.TrendReport table
-- ==================================================================================
CREATE PROCEDURE trendreport.GetTrendReportByTrendReportID

@TrendReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReport', @TrendReportID, @ProjectID)

	--TrendReport
	SELECT
		TR.ProjectID,
		dropdown.GetProjectNameByProjectID(TR.ProjectID) AS ProjectName,
		TR.TrendReportID,
		TR.TrendReportTitle,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Summary,
		TR.ReportDetail,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("TR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		TR.SummaryMapZoom
	FROM trendreport.TrendReport TR
	WHERE TR.TrendReportID = @TrendReportID
		AND TR.ProjectID = @ProjectID

	--TrendReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM trendreport.TrendReportAsset TRA
		JOIN asset.Asset A ON A.AssetID = TRA.AssetID
			AND A.ProjectID = @ProjectID
			AND TRA.TrendReportID = @TrendReportID
			AND TRA.ProjectID = A.ProjectID
	ORDER BY A.AssetName, A.AssetID

	--TrendReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM trendreport.TrendReportForce TRF
		JOIN force.Force F ON F.ForceID = TRF.ForceID
			AND F.ProjectID = @ProjectID
			AND TRF.TrendReportID = @TrendReportID
			AND TRF.ProjectID = F.ProjectID
	ORDER BY F.ForceName, F.ForceID

	--TrendReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM trendreport.TrendReportIncident TRI
		JOIN incident.Incident I ON I.IncidentID = TRI.IncidentID
			AND I.ProjectID = @ProjectID
			AND TRI.TrendReportID = @TrendReportID
			AND TRI.ProjectID = I.ProjectID
	ORDER BY I.IncidentName, I.IncidentID

	--TrendReportMediaReport
	SELECT
		MR.MediaReportID,
		MR.MediaReportTitle
	FROM trendreport.TrendReportMediaReport TRMR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = TRMR.MediaReportID
			AND MR.ProjectID = @ProjectID
			AND TRMR.TrendReportID = @TrendReportID
			AND TRMR.ProjectID = MR.ProjectID
	ORDER BY MR.MediaReportTitle, MR.MediaReportID

	--TrendReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM trendreport.TrendReportRelevantTheme TRRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = TRRT.RelevantThemeID
			AND TRRT.TrendReportID = @TrendReportID
			AND TRRT.ProjectID = @ProjectID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--TrendReportTerritory
	SELECT
		TRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(TRT.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM trendreport.TrendReportTerritory TRT
		JOIN territory.Territory T ON T.TerritoryID = TRT.TerritoryID
			AND TRT.TrendReportID = @TrendReportID
			AND TRT.ProjectID = @ProjectID
	ORDER BY 2, 1

	--TrendReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'TrendReport', @TrendReportID, @ProjectID

	--TrendReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN trendreport.TrendReport TR ON TR.TrendReportID = EL.EntityID
			AND TR.TrendReportID = @TrendReportID
			AND TR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'TrendReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--TrendReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'TrendReport', @TrendReportID, @ProjectID, @nWorkflowStepNumber

END
GO
--End procedure trendreport.GetTrendReportByTrendReportID
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO

EXEC core.ImplementerSetupAddUpdate 'ImplementerPortalCode', 'ELI0001'
GO

--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.00 - 2017.06.29 22.10.54'
GO
--End build tracking

