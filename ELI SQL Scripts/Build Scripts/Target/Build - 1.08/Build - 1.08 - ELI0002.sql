-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.08 - 2018.03.07 20.31.48

USE ELI0002
GO

-- ==============================================================================================================================
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO


--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO


--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO


--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO


--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.08 - 2018.03.07 20.31.48')
GO
--End build tracking

