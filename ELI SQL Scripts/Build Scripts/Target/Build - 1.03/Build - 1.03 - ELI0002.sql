-- File Name:	ELI0002.sql
-- Build Key:	Build - 1.03 - 2017.10.14 15.51.51

USE ELI0002
GO

-- ==============================================================================================================================
-- Functions:
--		[conflictstory].[GetConflictStoryCountByTerritory]
--		[contact].[GetContactCountByTerritory]
--		incident.GetIncidentCountByTerritory
--		incident.GetIncidentCountByTerritoryByMonth
--
-- Procedures:
--		conflictstory.GetConflictStoryCountsByDistrict
--		conflictstory.GetConflictStoryCountsByGovernorate
--		contact.GetContactCountsByDistrict
--		contact.GetContactCountsByGovernorate
--		incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
--		person.GetNewsFeed
--		territory.GetTerritoriesForMap
-- ==============================================================================================================================

--Begin file ELI000X - 01 - Tables.sql
USE ELI0002
GO

EXEC utility.AddColumn 'force.Force', 'ForceCode', 'VARCHAR(50)'
GO
--End file ELI000X - 01 - Tables.sql

--Begin file ELI000X - 02 - Functions.sql
USE ELI0002
GO
									  
--Begin function contact.GetContactCountByTerritory
EXEC utility.DropObject 'contact.GetContactCountByTerritory'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of contacts in a given territory
-- ==========================================================================
CREATE FUNCTION [contact].[GetContactCountByTerritory]
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX),
@ContactTypeIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nContactCount INT = 0

	SELECT @nContactCount = COUNT(C.ContactID)
	FROM contact.Contact C
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = C.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProjectID
			AND 
				(
				@ContactTypeIDList IS NULL 
					OR LEN(RTRIM(@ContactTypeIDList)) = 0 
					OR EXISTS
						(
						SELECT 1
						FROM contact.ContactContactType CCT
						JOIN core.ListToTable(@ContactTypeIDList, ',') LTT ON CCT.ContactTypeID = CAST(LTT.ListItem AS INT)
							AND CCT.ContactID = C.ContactID
						)
				)

	RETURN ISNULL(@nContactCount, 0)

END
GO
--End function contact.GetContactCountByTerritory

--Begin function conflictstory.GetConflictStoryCountByTerritory
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountByTerritory'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of conflict stories in a given territory
-- ==================================================================================
CREATE FUNCTION [conflictstory].[GetConflictStoryCountByTerritory]
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX),
@MediationOutcomeIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nConflictStoryCount INT = 0

	SELECT @nConflictStoryCount = COUNT(CS.ConflictStoryID)
	FROM conflictstory.ConflictStory CS
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = CS.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CS.ProjectID
			AND 
				(
				@MediationOutcomeIDList IS NULL 
					OR LEN(RTRIM(@MediationOutcomeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@MediationOutcomeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = CS.MediationOutcomeID
						)
				)
	RETURN ISNULL(@nConflictStoryCount, 0)

END
GO
--End function conflictstory.GetConflictStoryCountByTerritory

--Begin function incident.GetIncidentCountByTerritory
EXEC utility.DropObject 'incident.GetIncidentCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory
-- ===========================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritory
(
@TerritoryID INT,
@DateStart DATE,
@DateStop DATE,
@IncidentSourceIDList VARCHAR(MAX),
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0
	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT @nIncidentCount = COUNT(I.IncidentID)
	FROM incident.Incident I
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = I.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
			AND
				(
				@IncidentSourceIDList IS NULL
					OR LEN(RTRIM(@IncidentSourceIDList)) = 0
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentSourceIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentSourceID
						)
				)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritory

--Begin function incident.GetIncidentCountByTerritoryByMonth
EXEC utility.DropObject 'incident.GetIncidentCountByTerritoryByMonth'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory for a given month
-- =============================================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritoryByMonth
(
@TerritoryID INT,
@Month INT,
@Year INT,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0

	DECLARE @DateStart DATE = CAST(CAST(@Month AS VARCHAR(2)) + '/1/' + CAST(@Year AS VARCHAR(4)) AS DATE) 
	DECLARE @DateStop DATE = EOMONTH(@DateStart)

	SELECT @nIncidentCount = incident.GetIncidentCountByTerritory(@TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritoryByMonth

--End file ELI000X - 02 - Functions.sql

--Begin file ELI000X - 03 - Procedures.sql
USE ELI0002
GO

--Begin procedure conflictstory.GetConflictStoryCountsByDistrict
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountsByDistrict'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return conflictstory counts for a district
-- ======================================================================
CREATE PROCEDURE conflictstory.GetConflictStoryCountsByDistrict

@ParentTerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList, NULL) AS ConflictStoryCount
	FROM territory.Territory T
		CROSS JOIN conflictstory.ConflictStory C
	WHERE T.TerritoryTypeCode = 'District'
		AND T.ParentTerritoryID = @ParentTerritoryID
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure conflictstory.GetConflictStoryCountsByDistrict

--Begin procedure conflictstory.GetConflictStoryCountsByGovernorate
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountsByGovernorate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return conflictstory counts for the governorates
-- ============================================================================
CREATE PROCEDURE conflictstory.GetConflictStoryCountsByGovernorate

@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList, NULL) AS ConflictStoryCount
	FROM territory.Territory T
		CROSS JOIN conflictstory.ConflictStory C
	WHERE T.TerritoryTypeCode = 'Governorate'
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure conflictstory.GetConflictStoryCountsByGovernorate

--Begin procedure contact.GetContactCountsByDistrict
EXEC utility.DropObject 'contact.GetContactCountsByDistrict'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return contact counts for a district
-- ================================================================
CREATE PROCEDURE contact.GetContactCountsByDistrict

@ParentTerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList, NULL) AS ContactCount
	FROM territory.Territory T
		CROSS JOIN contact.Contact C
	WHERE T.TerritoryTypeCode = 'District'
		AND T.ParentTerritoryID = @ParentTerritoryID
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure contact.GetContactCountsByDistrict

--Begin procedure contact.GetContactCountsByGovernorate
EXEC utility.DropObject 'contact.GetContactCountsByGovernorate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return contact counts for the governorates
-- ======================================================================
CREATE PROCEDURE contact.GetContactCountsByGovernorate

@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryName, 
		contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList, NULL) AS ContactCount
	FROM territory.Territory T
		CROSS JOIN contact.Contact C
	WHERE T.TerritoryTypeCode = 'Governorate'
	GROUP BY T.TerritoryName, T.TerritoryID
	ORDER BY T.TerritoryName
	
END
GO
--End procedure contact.GetContactCountsByGovernorate

--Begin procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID
EXEC utility.DropObject 'incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A procedure to return data from the incident.Incident table
-- ========================================================================
CREATE PROCEDURE incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT
		D.ObjectKey, 
		D.IncidentYear,
		D.IncidentMonth,
		SUM(D.IncidentCount) AS IncidentCount
	FROM
		(
		SELECT 
			CAST(DATEPART(YEAR, I.IncidentDateTime) AS CHAR(4)) + CAST(DATEPART(MONTH, I.IncidentDateTime) - 1 AS VARCHAR(2)) + CAST(dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS VARCHAR(5)) ObjectKey, 
			IncidentYear = DATEPART(YEAR, I.IncidentDateTime),
			IncidentMonth = DATENAME(MONTH, DATEADD(month, DATEPART(MONTH, I.IncidentDateTime), 0) - 1),
			IT.IncidentTypeCategory,
			incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, NULL, @IncidentTypeIDList, @ProjectIDList) AS IncidentCount 
		FROM incident.Incident I
			JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			JOIN territory.Territory T ON T.TerritoryID = I.TerritoryID
				AND T.ParentTerritoryID = @TerritoryID
				AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
				AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
				AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))
		) D
	GROUP BY D.ObjectKey, D.IncidentYear, D.IncidentMonth
	ORDER BY D.ObjectKey

	SELECT DISTINCT 
		IT.IncidentTypeCategory,
		dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory(IT.IncidentTypeCategory) AS IncidentTypeCategoryID
	FROM incident.Incident I
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)
	ORDER BY IT.IncidentTypeCategory

END
GO
--End procedure incident.GetMonthlyIncidentCountsByIncidentTypeCategoryAndTerritoryID

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		'fa fa-fw fa-tasks' AS Icon,
		AR.ActivityReportID AS EntityID,
		AR.ActivityReportTitle AS Title,
		AR.UpdateDateTime,
		core.FormatDate(AR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM activityreport.ActivityReport AR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ActivityReport'
		JOIN dropdown.Project P ON P.ProjectID = AR.ProjectID
			AND AR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = AR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-file-text' AS Icon,
		DOC.DocumentID AS EntityID,
		DOC.DocumentTitle AS Title,
		DOC.DocumentDate AS UpdateDateTime,
		core.FormatDate(DOC.DocumentDate) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM ELI0000.document.Document DOC
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'Document'
		JOIN dropdown.Project P ON P.ProjectID = DOC.ProjectID
			AND DOC.DocumentDate >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = DOC.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-exclamation-circle' AS Icon,
		I.ImpactStoryID AS EntityID,
		I.ImpactStoryName AS Title,
		I.UpdateDateTime,
		core.FormatDate(I.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM impactstory.ImpactStory I
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ImpactStory'
		JOIN dropdown.Project P ON P.ProjectID = I.ProjectID
			AND I.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = I.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM dbo.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed

--Begin procedure territory.GetTerritoriesForMap
EXEC utility.DropObject 'territory.GetTerritoriesForMap'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.18
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoriesForMap

@Boundary VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX),
@ContactTypeIDList VARCHAR(MAX),
@IncidentSourceIDList VARCHAR(MAX),
@IncidentTypeIDList VARCHAR(MAX),
@MediationOutcomeIDList VARCHAR(MAX),
@DateStart DATE,
@DateStop DATE,
@ImplementerCode VARCHAR(50),
@TerritoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID,
		T.TerritoryTypeCode,
		T.TerritoryName,
		T.Location.STAsText() AS Location,
		IDD.ProjectID,
		CASE
			WHEN T.TerritoryTypeCode = 'Governorate'
			THEN incident.GetIncidentCountByTerritory(T.TerritoryID, @DateStart, @DateStop, @IncidentSourceIDList, @IncidentTypeIDList, @ProjectIDList)
			ELSE 0
		END AS FilteredIncidentCount,
		CASE
			WHEN T.TerritoryTypeCode = 'Governorate'
			THEN contact.GetContactCountByTerritory(T.TerritoryID, @ProjectIDList, @ContactTypeIDList)
			ELSE 0
		END AS FilteredContactCount,
		CASE
			WHEN T.TerritoryTypeCode = 'Governorate'
			THEN conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, @ProjectIDList, @MediationOutcomeIDList)
			ELSE 0
		END AS FilteredConflictStoryCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Incident' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredIncidentCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Contact' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredContactCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'ConflictStory' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredConflictStoryCount,
		(SELECT TEC.EntityCount FROM territory.TerritoryEntityCount TEC WHERE TEC.TerritoryID = T.TerritoryID AND TEC.EntityTypeCode = 'Asset' AND TEC.ProjectID IN (@ProjectIDList)) AS UnfilteredAssetCount
	FROM territory.Territory T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'Territory'
			AND (@Boundary IS NULL OR GEOMETRY::STGeomFromText(@Boundary, 4326).STIntersects(T.Location) = 1)
			AND (LEN(RTRIM(@ProjectIDList)) = 0  OR IDD.ProjectID IN (@ProjectIDList))
			AND (@TerritoryID = 0 OR T.TerritoryID = @TerritoryID OR T.ParentTerritoryID = @TerritoryID)
	
END
GO
--End procedure territory.GetTerritoriesForMap
--End file ELI000X - 03 - Procedures.sql

--Begin file ELI000X - 04 - Data.sql
USE ELI0002
GO


--End file ELI000X - 04 - Data.sql

--Begin post process file ELI000X.sql
--Begin table person.PersonPermissionable update
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable update

--Begin table permissionable.PermissionableTemplate cleanup
UPDATE PTP 
SET PTP.PermissionableLineage = P.PermissionableLineage 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin table person.PersonPermissionable cleanup
DELETE PP 
FROM person.PersonPermissionable PP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table person.PersonPermissionable cleanup

--Begin table permissionable.PermissionableTemplate cleanup
DELETE PTP 
FROM permissionable.PermissionableTemplatePermissionable PTP 
WHERE NOT EXISTS 
	(
	SELECT 1 
	FROM ELI0000.permissionable.Permissionable P 
	WHERE P.PermissionableLineage = PTP.PermissionableLineage
	)
GO

UPDATE PTP 
SET PTP.PermissionableID = P.PermissionableID 
FROM permissionable.PermissionableTemplatePermissionable PTP 
	JOIN ELI0000.permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate cleanup

--Begin synonym refresh
EXEC utility.RefreshSynonyms
GO
--End synonym refresh
--End post process file ELI000X.sql

--Begin build tracking
EXEC utility.LogSQLBuild 'Build - 1.03 - 2017.10.14 15.51.51'
GO
--End build tracking

