USE ELI0000
GO

TRUNCATE TABLE dropdown.ForceType
GO

SET IDENTITY_INSERT dropdown.ForceType ON;
INSERT INTO dropdown.ForceType (ForceTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.ForceType OFF;

INSERT INTO dropdown.ForceType (ForceTypeName, HexColor) VALUES ('Other', '#E41A1C')
GO

SET IDENTITY_INSERT dropdown.IncidentSource ON;
INSERT INTO dropdown.IncidentSource (IncidentSourceID) VALUES (0);
SET IDENTITY_INSERT dropdown.IncidentSource OFF;

INSERT INTO dropdown.IncidentSource (IncidentSourceName) VALUES ('Yemeni Actor')
GO

DELETE IDD
FROM implementer.ImplementerDropdownData IDD
WHERE IDD.DropdownCode = 'AreaOfOperationType'
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ForceType'
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentSource'
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'document', 'GetDocumentByDocumentName'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetForceTypeData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ForceType'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIncidentSourceData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'IncidentSource'
GO
