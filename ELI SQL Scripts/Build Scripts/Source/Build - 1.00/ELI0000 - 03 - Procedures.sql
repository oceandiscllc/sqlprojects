USE ELI0000
GO

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.Extension,
		D.DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure dropdown.GetAreaOfOperationTypeData
EXEC Utility.DropObject 'dropdown.GetAreaOfOperationTypeData'
GO
--End procedure dropdown.GetAreaOfOperationTypeData

--Begin procedure dropdown.GetForceTypeData
EXEC Utility.DropObject 'dropdown.GetForceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ForceType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetForceTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ForceTypeID, 
		T.ForceTypeName,
		T.HexColor
	FROM dropdown.ForceType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ForceTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ForceType'
			AND (T.ForceTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ForceTypeName, T.ForceTypeID

END
GO
--End procedure dropdown.GetForceTypeData

--Begin procedure dropdown.GetIncidentSourceData
EXEC Utility.DropObject 'dropdown.GetIncidentSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.IncidentSource table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentSourceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentSourceID, 
		T.IncidentSourceName
	FROM dropdown.IncidentSource T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentSourceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentSource'
			AND (T.IncidentSourceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentSourceName, T.IncidentSourceID

END
GO
--End procedure dropdown.GetIncidentSourceData


