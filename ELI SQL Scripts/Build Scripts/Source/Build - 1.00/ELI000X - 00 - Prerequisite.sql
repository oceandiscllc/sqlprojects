USE [[INSTANCENAME]]
GO

EXEC utility.AddSchema 'incident'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name = 'dbo' AND T.Name = 'Incident')
	ALTER SCHEMA incident TRANSFER dbo.Incident
--ENDIF
GO

TRUNCATE TABLE syslog.BuildLog
GO