USE [[INSTANCENAME]]
GO

--Begin function incident.GetIncidentCountByTerritory
EXEC utility.DropObject 'incident.GetIncidentCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory
-- ===========================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritory
(
@TerritoryID INT,
@StartDate DATE,
@StopDate DATE
@IncidentTypeIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0
	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT @nIncidentCount = COUNT(I.IncidentID)
	FROM incident.Incident I
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = I.TerritoryID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND (@IncidentTypeIDList IS NULL OR LEN(RTRIM(@IncidentTypeIDList)) = 0 OR EXISTS (SELECT 1 FROM core.ListToTable(@IncidentTypeIDList, ',') LTT WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID))

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritory

--Begin function spotreport.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'spotreport.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION spotreport.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE 
	  @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@IncidentMarker varchar(max)=''
	
	SELECT
		@IncidentMarker += '&markers=icon:' 
			+ replace(core.GetSystemSetupValueBySetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Location.STY,'') AS VARCHAR(MAX)) 
			+ ','
			 + CAST(ISNULL(I.Location.STX,'') AS VARCHAR(MAX))
	 FROM spotreport.SpotReportIncident SRI
		JOIN incident.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult +=  @IncidentMarker 

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function spotreport.FormatStaticGoogleMapForSpotReport

--Begin function spotreport.GetSpotReportTerritoriesList
EXEC utility.DropObject 'dbo.GetSpotReportTerritoriesList'
EXEC utility.DropObject 'spotreport.GetSpotReportTerritoriesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A function to return territories as a list 
-- =======================================================
CREATE FUNCTION spotreport.GetSpotReportTerritoriesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @TerritoryList VARCHAR(MAX) = ''
	
	SELECT @TerritoryList += territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) + ', '
	FROM incident.Incident I
		JOIN spotreport.SpotReportIncident SRI ON SRI.IncidentID = I.IncidentID 
			AND	SRI.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@TerritoryList)) > 0 
		SET @TerritoryList = RTRIM(SUBSTRING(@TerritoryList, 0, LEN(@TerritoryList)))
	--ENDIF

	RETURN RTRIM(@TerritoryList)

END
GO
--End function spotreport.GetSpotReportTerritoriesList
