USE [[INSTANCENAME]]
GO

--Begin table incident.Incident
DECLARE @TableName VARCHAR(250) = 'incident.Incident'

EXEC utility.DropColumn @TableName, 'Source'
EXEC utility.DropColumn @TableName, 'SourceTypeID'

EXEC utility.AddColumn @TableName, 'IncidentSourceID', 'INT', '0'
GO
--End table incident.Incident

--Begin table incident.IncidentForce
DECLARE @TableName VARCHAR(250) = 'incident.IncidentForce'

EXEC utility.DropObject @TableName

CREATE TABLE incident.IncidentForce
	(
	IncidentForceID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	ForceID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_IncidentForce', 'ProjectID,IncidentID,ForceID'
GO
--End table incident.IncidentForce
