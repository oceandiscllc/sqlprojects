USE ELI0000
GO

--Begin table dropdown.AreaOfOperationType
DECLARE @TableName VARCHAR(250) = 'dropdown.AreaOfOperationType'

EXEC utility.DropObject @TableName
GO
--End table dropdown.AreaOfOperationType

--Begin table dropdown.ForceType
DECLARE @TableName VARCHAR(250) = 'dropdown.ForceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ForceType
	(
	ForceTypeID INT NOT NULL IDENTITY(0,1),
	ForceTypeName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ForceType', 'DisplayOrder,ForceTypeName'
GO
--End table dropdown.ForceType

--Begin table dropdown.IncidentSource
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentSource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentSource
	(
	IncidentSourceID INT NOT NULL IDENTITY(0,1),
	IncidentSourceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentSourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_IncidentSource', 'DisplayOrder,IncidentSourceName'
GO
--End table dropdown.IncidentSource