USE ELI0000
GO

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConfidenceLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConfidenceLevel'
			AND (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetContactExperienceData
EXEC Utility.DropObject 'dropdown.GetContactExperienceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.ContactExperience table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetContactExperienceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactExperienceID, 
		T.ContactExperienceCategory,
		T.ContactExperienceName
	FROM dropdown.ContactExperience T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ContactExperienceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ContactExperience'
			AND (T.ContactExperienceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.ContactExperienceCategory, T.DisplayOrder, T.ContactExperienceName, T.ContactExperienceID

END
GO
--End procedure dropdown.GetContactExperienceData

--Begin procedure dropdown.GetEducationLevelData
EXEC Utility.DropObject 'dropdown.GetEducationLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.10
-- Description:	A stored procedure to return data from the dropdown.EducationLevel table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEducationLevelData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EducationLevelID, 
		T.EducationLevelName
	FROM dropdown.EducationLevel T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.EducationLevelID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'EducationLevel'
			AND (T.EducationLevelID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EducationLevelName, T.EducationLevelID

END
GO
--End procedure dropdown.GetEducationLevelData

--Begin procedure dropdown.GetIncidentDirectionData
EXEC Utility.DropObject 'dropdown.GetIncidentDirectionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.08.13
-- Description:	A stored procedure to return data from the dropdown.IncidentDirection table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetIncidentDirectionData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentDirectionID, 
		T.IncidentDirectionName
	FROM dropdown.IncidentDirection T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.IncidentDirectionID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'IncidentDirection'
			AND (T.IncidentDirectionID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentDirectionName, T.IncidentDirectionID

END
GO
--End procedure dropdown.GetIncidentDirectionData

--Begin procedure dropdown.GetTerritoryTypeData
EXEC Utility.DropObject 'dropdown.GetTerritoryTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.15
-- Description:	A stored procedure to return data from the dropdown.TerritoryType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetTerritoryTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryTypeID,
		T.TerritoryTypeCode,
		T.TerritoryTypeName
	FROM territory.TerritoryType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.TerritoryTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'TerritoryType'
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.TerritoryTypeName, T.TerritoryTypeID

END
GO
--End procedure dropdown.GetTerritoryTypeData

--Begin procedure territory.GetProjectTerritoryTypesByProjectID
EXEC utility.DropObject 'territory.GetProjectTerritoryTypesByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2016.01.23
-- Description:	A stored procedure to get the territory types associated with a project
-- ====================================================================================
CREATE PROCEDURE territory.GetProjectTerritoryTypesByProjectID

@ProjectID INT = 0,
@TerritoryTypeCode VARCHAR(50) = '',
@OrderByDirection VARCHAR(5) = 'DESC'

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		TT1.TerritoryTypeCode,
		TT1.TerritoryTypeCodePlural,
		TT1.TerritoryTypeName,
		TT1.TerritoryTypeNamePlural
	FROM territory.ProjectTerritoryType PTT
		JOIN dropdown.Project P ON P.ProjectID = PTT.ProjectID
		JOIN territory.TerritoryType TT1 ON TT1.TerritoryTypeID = PTT.TerritoryTypeID
			AND PTT.ProjectID = @ProjectID
			AND 
				(
				@TerritoryTypeCode = ''
					OR 
						(
						@OrderByDirection = 'ASC'
							AND TT1.DisplayOrder > 
								(
								SELECT TT2.DisplayOrder
								FROM territory.TerritoryType TT2
								WHERE TT2.TerritoryTypeCode = @TerritoryTypeCode 
								)
						)
					OR 
						(
						@OrderByDirection = 'DESC'
							AND TT1.DisplayOrder < 
								(
								SELECT TT2.DisplayOrder
								FROM territory.TerritoryType TT2
								WHERE TT2.TerritoryTypeCode = @TerritoryTypeCode 
								)
						)
				)
		ORDER BY 
			CASE WHEN @OrderByDirection = 'ASC' THEN TT1.DisplayOrder END ASC,
			CASE WHEN @OrderByDirection <> 'ASC' THEN TT1.DisplayOrder END DESC

END
GO
--End procedure territory.GetProjectTerritoryTypesByProjectID
