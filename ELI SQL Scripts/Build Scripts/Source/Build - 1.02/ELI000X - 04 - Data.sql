USE [[INSTANCENAME]]
GO

TRUNCATE TABLE territory.TerritoryEntityCount
GO

--Asset
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	A.ProjectID,
	T.TerritoryID,
	'Asset',
	asset.GetAssetCountByTerritory(T.TerritoryID, A.ProjectID)
FROM territory.Territory T 
	CROSS JOIN asset.Asset A
GROUP BY A.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, A.ProjectID
GO

--ConflictStory
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	CS.ProjectID,
	T.TerritoryID,
	'ConflictStory',
	conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, CS.ProjectID)
FROM territory.Territory T 
	CROSS JOIN conflictstory.ConflictStory CS
GROUP BY CS.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, CS.ProjectID
GO

--Contact
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	C.ProjectID,
	T.TerritoryID,
	'Contact',
	contact.GetContactCountByTerritory(T.TerritoryID, C.ProjectID)
FROM territory.Territory T 
	CROSS JOIN contact.Contact C
GROUP BY C.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, C.ProjectID
GO

--Incident
INSERT INTO territory.TerritoryEntityCount
	(ProjectID, TerritoryID, EntityTypeCode, EntityCount)
SELECT 
	I.ProjectID,
	T.TerritoryID,
	'Incident',
	incident.GetIncidentCountByTerritory(T.TerritoryID, NULL, NULL, NULL, I.ProjectID)
FROM territory.Territory T 
	CROSS JOIN incident.Incident I
GROUP BY I.ProjectID, T.TerritoryID
ORDER BY T.TerritoryID, I.ProjectID
GO