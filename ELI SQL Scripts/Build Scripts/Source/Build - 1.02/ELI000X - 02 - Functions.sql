USE [[INSTANCENAME]]
GO

--Begin function asset.GetAssetCountByTerritory
EXEC utility.DropObject 'asset.GetAssetCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of assets in a given territory
-- ========================================================================
CREATE FUNCTION asset.GetAssetCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nAssetCount INT = 0

	SELECT @nAssetCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = A.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = A.ProjectID

	RETURN ISNULL(@nAssetCount, 0)

END
GO
--End function asset.GetAssetCountByTerritory

--Begin function conflictstory.GetConflictStoryCountByTerritory
EXEC utility.DropObject 'conflictstory.GetConflictStoryCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of conflict stories in a given territory
-- ==================================================================================
CREATE FUNCTION conflictstory.GetConflictStoryCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nConflictStoryCount INT = 0

	SELECT @nConflictStoryCount = COUNT(CS.ConflictStoryID)
	FROM conflictstory.ConflictStory CS
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = CS.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CS.ProjectID

	RETURN ISNULL(@nConflictStoryCount, 0)

END
GO
--End function conflictstory.GetConflictStoryCountByTerritory

--Begin function contact.GetContactCountByTerritory
EXEC utility.DropObject 'contact.GetContactCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return a count of contacts in a given territory
-- ==========================================================================
CREATE FUNCTION contact.GetContactCountByTerritory
(
@TerritoryID INT,
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nContactCount INT = 0

	SELECT @nContactCount = COUNT(C.ContactID)
	FROM contact.Contact C
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = C.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProjectID

	RETURN ISNULL(@nContactCount, 0)

END
GO
--End function contact.GetContactCountByTerritory

--Begin function incident.GetIncidentCountByTerritory
EXEC utility.DropObject 'incident.GetIncidentCountByTerritory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory
-- ===========================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritory
(
@TerritoryID INT,
@DateStart DATE,
@DateStop DATE,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0
	DECLARE @tDate TABLE (DateData DATE)

	IF @DateStart IS NOT NULL AND @DateStop IS NOT NULL
		BEGIN

		INSERT INTO @tDate (DateData) SELECT @DateStart UNION SELECT @DateStop
		SELECT @DateStart = MIN(D.DateData) FROM @tDate D
		SELECT @DateStop = MAX(D.DateData) FROM @tDate D

		END
	--ENDIF

	IF @DateStop IS NOT NULL
		SET @DateStop = DATEADD(d, 1, @DateStop)
	--ENDIF

	SELECT @nIncidentCount = COUNT(I.IncidentID)
	FROM incident.Incident I
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) T ON T.TerritoryID = I.TerritoryID
		JOIN core.ListToTable(@ProjectIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.ProjectID
			AND (@DateStart IS NULL OR @DateStart <= I.IncidentDateTime)
			AND (@DateStop IS NULL OR @DateStop >= I.IncidentDateTime)
			AND 
				(
				@IncidentTypeIDList IS NULL 
					OR LEN(RTRIM(@IncidentTypeIDList)) = 0 
					OR EXISTS 
						(
						SELECT 1 
						FROM core.ListToTable(@IncidentTypeIDList, ',') LTT 
						WHERE CAST(LTT.ListItem AS INT) = I.IncidentTypeID
						)
				)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritory

--Begin function incident.GetIncidentCountByTerritoryByMonth
EXEC utility.DropObject 'incident.GetIncidentCountByTerritoryByMonth'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.29
-- Description:	A function to return a count of incidents in a given territory for a given month
-- =============================================================================================
CREATE FUNCTION incident.GetIncidentCountByTerritoryByMonth
(
@TerritoryID INT,
@Month INT,
@Year INT,
@IncidentTypeIDList VARCHAR(MAX),
@ProjectIDList VARCHAR(MAX)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentCount INT = 0

	DECLARE @DateStart DATE = CAST(CAST(@Month AS VARCHAR(2)) + '/1/' + CAST(@Year AS VARCHAR(4)) AS DATE) 
	DECLARE @DateStop DATE = EOMONTH(@DateStart)

	SELECT @nIncidentCount = incident.GetIncidentCountByTerritory(@TerritoryID, @DateStart, @DateStop, @IncidentTypeIDList, @ProjectIDList)

	RETURN ISNULL(@nIncidentCount, 0)

END
GO
--End function incident.GetIncidentCountByTerritoryByMonth
