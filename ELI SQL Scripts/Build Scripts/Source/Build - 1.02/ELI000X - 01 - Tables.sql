USE [[INSTANCENAME]]
GO

--Begin table asset.Asset
DECLARE @TableName VARCHAR(250) = 'asset.Asset'

EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'
GO

ALTER TABLE asset.Asset ALTER COLUMN AssetName NVARCHAR(250)
GO
--End table asset.Asset

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'EducationLevelID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IsAidRecipient', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'IsTrainer', 'BIT', '0'
GO
--End table contact.Contact

--Begin table incident.Incident
DECLARE @TableName VARCHAR(250) = 'incident.Incident'

EXEC utility.AddColumn @TableName, 'ArabicSummary', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Attribution', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ConfidenceLevelID', 'INT', '0'
GO

EXEC utility.DropObject 'incident.TR_Incident'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2017.08.13
-- Description:	A trigger to update the incident.Incident table
-- ============================================================
CREATE TRIGGER incident.TR_Incident ON incident.Incident FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	UPDATE I1
	SET I1.IncidentName = 

		CASE
			WHEN I2.TerritoryID = 0
			THEN ''
			ELSE (SELECT T.TerritoryName FROM territory.Territory T WHERE T.TerritoryID = I2.TerritoryID) + ' - '
		END + 

		CASE
			WHEN I2.IncidentTypeID = 0
			THEN ''
			ELSE IT.IncidentTypeName + ' - '
		END + 

		CASE
			WHEN I2.IncidentDateTime IS NULL
			THEN ''
			ELSE CONVERT(VARCHAR(10), I2.IncidentDateTime, 101)
		END

	FROM incident.Incident I1
		JOIN INSERTED I2 ON I2.IncidentID = I1.IncidentID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I2.IncidentTypeID

	END
--ENDIF
GO		
--End table incident.Incident

--Begin table contact.ContactContactExperience
DECLARE @TableName VARCHAR(250) = 'contact.ContactContactExperience'

EXEC utility.DropObject @TableName

CREATE TABLE contact.ContactContactExperience
	(
	ContactContactExperienceID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	ContactExperienceID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactExperienceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactContactExperienceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactContactExperience', 'ContactID,ContactExperienceID'
GO
--End table contact.ContactContactExperiencer

--Begin table incident.IncidentForce
DECLARE @TableName VARCHAR(250) = 'incident.IncidentForce'

EXEC utility.AddColumn @TableName, 'IncidentDirectionID', 'INT', '0'
GO
--End table incident.IncidentForce

--Begin table territory.TerritoryEntityCount
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryEntityCount'

EXEC utility.DropObject @TableName

CREATE TABLE territory.TerritoryEntityCount
	(
	TerritoryEntityCountID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	EntityTypeCode VARCHAR(50),
	EntityCount INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryEntityCountID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryEntityCount', 'TerritoryID,ProjectID,EntityTypeCode,EntityCount'
GO
--End table contact.TerritoryEntityCount
