USE ELI0000
GO

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'TerritoryList',
	@AfterMenuItemCode = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'DocumentList',
	@AfterMenuItemCode = 'TerritoryList',
	@NewMenuItemText = 'Document Repository'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'RequestForInformationList',
	@AfterMenuItemCode = 'DocumentList'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'IncidentList',
	@AfterMenuItemCode = 'RequestForInformationList',
	@Icon = 'fa fa-fw fa-bolt'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'ConflictStoryList',
	@AfterMenuItemCode = 'IncidentList',
	@Icon = 'fa fa-fw fa-book'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '',
	@NewMenuItemCode = 'Contacts',
	@AfterMenuItemCode = 'ConflictStoryList',
	@Icon = 'fa fa-fw fa-users'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '', 		
	@NewMenuItemCode = 'ForceList', 											
	@AfterMenuItemCode = 'Contacts',	
	@Icon = 'fa fa-fw fa-flag'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = '', 		
	@NewMenuItemCode = 'AssetList', 											
	@AfterMenuItemCode = 'ForceList',												
	@Icon = 'fa fa-fw fa-tags'
GO

DELETE MI
FROM core.MenuItem MI
WHERE MI.MenuItemCode = 'ForceAsset'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Activity'
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

EXEC implementer.ImplementerDropdownDataAddUpdate 'TerritoryType', 'TerritoryTypeID', 'territory'
GO

UPDATE IDD
SET IDD.IsActive = 0
FROM implementer.ImplementerDropdownData IDD
	JOIN territory.TerritoryType TT ON TT.TerritoryTypeID = IDD.DropdownID
		AND IDD.DropdownCode = 'TerritoryType'
		AND TT.TerritoryTypeCode NOT IN ('Community', 'District', 'Governorate')
		AND IDD.DropdownID > 0
GO

--Begin data for table dropdown.ConfidenceLevel
TRUNCATE TABLE dropdown.ConfidenceLevel
GO

SET IDENTITY_INSERT dropdown.ConfidenceLevel ON;
INSERT INTO dropdown.ConfidenceLevel (ConfidenceLevelID) VALUES (0);
SET IDENTITY_INSERT dropdown.ConfidenceLevel OFF;

INSERT INTO dropdown.ConfidenceLevel 
	(ConfidenceLevelName, DisplayOrder) 
VALUES 
	('High', 1),
	('Medium', 2),
	('Low', 3)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ConfidenceLevel'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ConfidenceLevel'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetConfidenceLevelData'
GO
--End data for table dropdown.ConfidenceLevel

--Begin data for table dropdown.ContactExperience
TRUNCATE TABLE dropdown.ContactExperience
GO

SET IDENTITY_INSERT dropdown.ContactExperience ON;
INSERT INTO dropdown.ContactExperience (ContactExperienceID) VALUES (0);
SET IDENTITY_INSERT dropdown.ContactExperience OFF;

INSERT INTO dropdown.ContactExperience 
	(ContactExperienceCategory, ContactExperienceName) 
VALUES 
	('Campaigns and Media', 'Awareness Campaigns'),
	('Campaigns and Media', 'Media Relations'),
	('Campaigns and Media', 'Political Campaigns'),
	('Campaigns and Media', 'Youth Engagement'),
	('Management', 'Facilitating Humanitarian Aid'),
	('Management', 'Nonprofit Management'),
	('Management', 'Program Management'),
	('Mediation', 'Conflict Resolution (Armed Actor Disputes)'),
	('Mediation', 'Conflict Resolution (Family Disputes)'),
	('Mediation', 'Conflict Resolution (Land Disputes)'),
	('Mediation', 'Conflict Resolution (Resource Disputes)'),
	('Mediation', 'Conflict Resolution Training'),
	('Political', 'Local Government'),
	('Political', 'National Dialogue Conference Member'),
	('Political', 'Southern Transitional Council Member'),
	('Professional', 'Lawyer'),
	('Professional', 'Medical'),
	('Security', 'Crowd control'),
	('Security', 'DDR'),
	('Security', 'Prisoner Reintegration'),
	('Security', 'UXO removal')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactExperience'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ContactExperience'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetContactExperienceData'
GO
--End data for table dropdown.ContactExperience

--Begin data for table dropdown.ContactType
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Civilian Sector')
	BEGIN

	INSERT INTO dropdown.ContactType 
		(ContactTypeName) 
	VALUES 
		('Civilian Sector'),
		('Influential Female'),
		('Security Actor'),
		('Security Sector')

	END
--ENDIF
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactType', 'ContactTypeID'
GO
--End data for table dropdown.ContactType

--Begin data for table dropdown.EducationLevel
TRUNCATE TABLE dropdown.EducationLevel
GO

SET IDENTITY_INSERT dropdown.EducationLevel ON;
INSERT INTO dropdown.EducationLevel (EducationLevelID) VALUES (0);
SET IDENTITY_INSERT dropdown.EducationLevel OFF;

INSERT INTO dropdown.EducationLevel 
	(EducationLevelName, DisplayOrder) 
VALUES 
	('Primary', 1),
	('Secondary', 2),
	('BA', 3),
	('MA', 4),
	('Law', 5),
	('PhD', 6)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'EducationLevel'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'EducationLevel'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetEducationLevelData'
GO
--End data for table dropdown.EducationLevel

--Begin data for table dropdown.IncidentDirection
TRUNCATE TABLE dropdown.IncidentDirection
GO

SET IDENTITY_INSERT dropdown.IncidentDirection ON;
INSERT INTO dropdown.IncidentDirection (IncidentDirectionID, IncidentDirectionName, DisplayOrder) VALUES (0, 'Unknown', 1);
SET IDENTITY_INSERT dropdown.IncidentDirection OFF;

INSERT INTO dropdown.IncidentDirection 
	(IncidentDirectionName, DisplayOrder) 
VALUES 
	('Main Actor', 2),
	('Target', 3)
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentDirection'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'IncidentDirection'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIncidentDirectionData'
GO
--End data for table dropdown.IncidentDirection

--Begin data for table dropdown.IncidentSource
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentSource'
GO
--End data for table dropdown.IncidentSource

--Begin data for table dropdown.IncidentType
EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType'
GO
--End data for table dropdown.IncidentType

--Begin function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetIncidentTypeCategoryIDByIncidentTypeCategory'
GO
--End function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory

--Begin procedure territory.GetProjectTerritoryTypesByProjectID
EXEC implementer.ImplementerSynonymAddUpdate 'territory', 'GetProjectTerritoryTypesByProjectID'
GO
--End procedure territory.GetProjectTerritoryTypesByProjectID
