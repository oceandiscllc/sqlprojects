USE ELI0000
GO

--Begin table dropdown.ConfidenceLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.ConfidenceLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConfidenceLevel
	(
	ConfidenceLevelID INT NOT NULL IDENTITY(0,1),
	ConfidenceLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConfidenceLevelID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConfidenceLevel', 'DisplayOrder,ConfidenceLevelName'
GO
--End table dropdown.ConfidenceLevel

--Begin table dropdown.ContactExperience
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactExperience'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactExperience
	(
	ContactExperienceID INT NOT NULL IDENTITY(0,1),
	ContactExperienceCategory VARCHAR(50),
	ContactExperienceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactExperienceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactExperience', 'ContactExperienceCategory,DisplayOrder,ContactExperienceName'
GO
--End table dropdown.ContactExperience

--Begin table dropdown.EducationLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.EducationLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EducationLevel
	(
	EducationLevelID INT NOT NULL IDENTITY(0,1),
	EducationLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EducationLevelID'
EXEC utility.SetIndexClustered @TableName, 'IX_EducationLevel', 'DisplayOrder,EducationLevelName'
GO
--End table dropdown.EducationLevel

--Begin table dropdown.IncidentDirection
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentDirection'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentDirection
	(
	IncidentDirectionID INT NOT NULL IDENTITY(0,1),
	IncidentDirectionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentDirectionID'
EXEC utility.SetIndexClustered @TableName, 'IX_IncidentDirection', 'DisplayOrder,IncidentDirectionName'
GO
--End table dropdown.IncidentDirection
