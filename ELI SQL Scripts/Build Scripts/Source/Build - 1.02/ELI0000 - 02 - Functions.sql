USE ELI0000
GO

--Begin function dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
EXEC utility.DropObject 'dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.08.12
-- Description:	A function to return an ID value for an IncidentTypeCategory
-- =========================================================================
CREATE FUNCTION dropdown.GetIncidentTypeCategoryIDByIncidentTypeCategory
(
@IncidentTypeCategory VARCHAR(50)
)

RETURNS INT

AS
BEGIN

	DECLARE @nIncidentTypeCategoryID INT = 0
	DECLARE @tTable TABLE (IncidentTypeCategoryID INT NOT NULL IDENTITY(1,1), IncidentTypeCategory VARCHAR(50))

	INSERT INTO @tTable
		(IncidentTypeCategory) 		
	SELECT DISTINCT
		IT.IncidentTypeCategory
	FROM dropdown.IncidentType IT
	WHERE IT.IncidentTypeID > 0
	ORDER BY IncidentTypeCategory

	SELECT @nIncidentTypeCategoryID = T.IncidentTypeCategoryID
	FROM @tTable T
	WHERE T.IncidentTypeCategory = @IncidentTypeCategory

	RETURN ISNULL(@nIncidentTypeCategoryID, 0)

END
GO
--End function contact.GetContactCountByTerritory
