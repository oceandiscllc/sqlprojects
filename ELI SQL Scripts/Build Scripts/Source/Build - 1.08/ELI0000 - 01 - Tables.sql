USE ELI0000
GO

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.AddColumn @TableName, 'AssetTypeCategory', 'VARCHAR(50)'
GO
--End table dropdown.AssetType
