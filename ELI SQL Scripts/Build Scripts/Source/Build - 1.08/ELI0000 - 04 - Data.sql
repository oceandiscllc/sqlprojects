USE ELI0000
GO

--Begin table dropdown.AssetType
TRUNCATE TABLE dropdown.AssetType
GO

EXEC utility.InsertIdentityValue 'dropdown.AssetType', 'AssetTypeID', 0
GO

INSERT INTO dropdown.AssetType
	(AssetTypeCategory, AssetTypeName) 
VALUES 
	('Border Infrastructure', 'Border Crossing'),
	('Border Infrastructure', 'Military Border Post'),
	('Commercial Infrastructure', 'Agricultural Facility'),
	('Commercial Infrastructure', 'Factory/Industrial Area'),
	('Commercial Infrastructure', 'Financial Institution'),
	('Commercial Infrastructure', 'Market'),
	('Commercial Infrastructure', 'Mine/Quarry'),
	('Communications Infrastructure', 'Radio Station'),
	('Communications Infrastructure', 'Transmission Facility'),
	('Educational Infrastructure', 'School'),
	('Educational Infrastructure', 'Technical School'),
	('Educational Infrastructure', 'University/College'),
	('Health Infrastructure', 'Hospital'),
	('Health Infrastructure', 'Medical Center/Clinic'),
	('Maritime Infrastructure', 'Dock/Anchorage'),
	('Maritime Infrastructure', 'Navigational Aid'),
	('Maritime Infrastructure', 'Port'),
	('Oil and Gas Infrastructure', 'Fuel Depot'),
	('Oil and Gas Infrastructure', 'Pipeline'),
	('Oil and Gas Infrastructure', 'Refinery'),
	('Political Infrastructure', 'Diplomatic Facility'),
	('Political Infrastructure', 'Government Facility'),
	('Political Infrastructure', 'Other Political Infrastructure'),
	('Power Infrastructure', 'Power Plant'),
	('Power Infrastructure', 'Power Station'),
	('Religious and Cultural Infrastructure', 'Cultural Site'),
	('Religious and Cultural Infrastructure', 'Religious Site'),
	('Security Infrastructure', 'Airbase'),
	('Security Infrastructure', 'Military Base'),
	('Security Infrastructure', 'Other Security Infrastructure'),
	('Security Infrastructure', 'Police Station'),
	('Transportation Infrastructure', 'Airport'),
	('Transportation Infrastructure', 'Bridge'),
	('Transportation Infrastructure', 'Other Transportation Infrastructure'),
	('Transportation Infrastructure', 'Waterway'),
	('Water and Waste Infrastructure', 'Dam'),
	('Water and Waste Infrastructure', 'Waste Treatment Facility'),
	('Water and Waste Infrastructure', 'Water Treatment Facility')
GO

UPDATE AT
SET AT.Icon = 'asset-' + LOWER(REPLACE(REPLACE(AT.AssetTypeName, ' ', '-'), '/', '-')) + '.png'
FROM dropdown.AssetType AT
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'AssetType'
GO
--End table dropdown.AssetType

--Begin data for table dropdown.ConflictDriver
TRUNCATE TABLE dropdown.ConflictDriver
GO

SET IDENTITY_INSERT dropdown.ConflictDriver ON;
INSERT INTO dropdown.ConflictDriver (ConflictDriverID) VALUES (0);
SET IDENTITY_INSERT dropdown.ConflictDriver OFF;

INSERT INTO dropdown.ConflictDriver 
	(ConflictDriverName) 
VALUES 
	('Blood Feud'),
	('Civil Disagreements'),
	('Conflict Spillover'),
	('Demands for State Services'),
	('Domestic Issues'),
	('Jobs and Services'),
	('Land Dispute'),
	('Land Dispute - Agriculture'),
	('Land Dispute - Natural Resources'),
	('Land Dispute - Residential'),
	('Land Dispute - Territorial'),
	('Land Dispute - Water Resources'),
	('Looting'),
	('North-South Divisions'),
	('Political or Civil Disagreement'),
	('Regionalism'),
	('Rent Extraction'),
	('Services'),
	('Theft'),
	('Tribalism'),
	('Wage Dispute'),
	('Water Rights')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ConflictDriver', 'ConflictDriverID'
GO
--End data for table dropdown.ConflictDriver

--Begin data for table dropdown.MediationType
TRUNCATE TABLE dropdown.MediationType
GO

SET IDENTITY_INSERT dropdown.MediationType ON;
INSERT INTO dropdown.MediationType (MediationTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.MediationType OFF;

INSERT INTO dropdown.MediationType 
	(MediationTypeName) 
VALUES 
	('Community Leaders'),
	('Houthis'),
	('State Authorities'),
	('Tribal Sheikhs'),
	('Unidentified Mediator')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'MediationType', 'MediationTypeID'
GO
--End data for table dropdown.MediationType

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetDistrictData'
GO


