USE ELI0000
GO

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeCategory,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.AssetTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'AssetType'
			AND (T.AssetTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND IDD.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeCategory, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetDistrictData
EXEC Utility.DropObject 'dropdown.GetDistrictData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Christopher Crouch
-- Create date:	2018.1.28
-- Description:	A stored procedure to return data from the territory.Territory table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDistrictData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID, 
		T.TerritoryName
	FROM territory.Territory T
	WHERE T.TerritoryTypeCode = 'District'
	ORDER BY T.TerritoryName, T.TerritoryID

END
GO
--End procedure dropdown.GetDistrictData

