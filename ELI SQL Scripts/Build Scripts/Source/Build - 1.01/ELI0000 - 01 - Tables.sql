USE ELI0000
GO

--Begin table dropdown.ConflictDriver
DECLARE @TableName VARCHAR(250) = 'dropdown.ConflictDriver'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConflictDriver
	(
	ConflictDriverID INT NOT NULL IDENTITY(0,1),
	ConflictDriverName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictDriverID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictDriver', 'DisplayOrder,ConflictDriverName'
GO
--End table dropdown.ConflictDriver

--Begin table dropdown.ConflictStorySource
DECLARE @TableName VARCHAR(250) = 'dropdown.ConflictStorySource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConflictStorySource
	(
	ConflictStorySourceID INT NOT NULL IDENTITY(0,1),
	ConflictStorySourceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStorySourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStorySource', 'DisplayOrder,ConflictStorySourceName'
GO
--End table dropdown.ConflictStorySource

--Begin table dropdown.DocumentIssue
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentIssue'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentIssue
	(
	DocumentIssueID INT NOT NULL IDENTITY(0,1),
	DocumentIssueName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentIssueID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentIssue', 'DisplayOrder,DocumentIssueName'
GO
--End table dropdown.DocumentIssue

--Begin table dropdown.MediationOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.MediationOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediationOutcome
	(
	MediationOutcomeID INT NOT NULL IDENTITY(0,1),
	MediationOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediationOutcomeID'
EXEC utility.SetIndexClustered @TableName, 'IX_MediationOutcome', 'DisplayOrder,MediationOutcomeName'
GO
--End table dropdown.MediationOutcome

--Begin table dropdown.MediationType
DECLARE @TableName VARCHAR(250) = 'dropdown.MediationType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediationType
	(
	MediationTypeID INT NOT NULL IDENTITY(0,1),
	MediationTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediationTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_MediationType', 'DisplayOrder,MediationTypeName'
GO
--End table dropdown.MediationType
