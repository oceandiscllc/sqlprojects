USE ELI0000
GO

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ConflictStory', 
	@EntityTypeName = 'Conflict Vignette', 
	@EntityTypeNamePlural = 'Conflict Vignettes',
	@HasWorkflow = 0, 
	@SchemaName = 'conflictstory', 
	@TableName = 'ConflictStory', 
	@PrimaryKeyFieldName = 'ConflictStoryID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate --Conflict Story
	@ParentMenuItemCode = 'Insight', 		
	@NewMenuItemCode = 'ConflictStoryList', 											
	@AfterMenuItemCode = 'IncidentList',												
	@NewMenuItemText = 'Conflict Vignettes', 
	@NewMenuItemLink = '/conflictstory/list',
	@PermissionableLineageList = 'ConflictStory.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

--Begin data for table dropdown.ConflictDriver
TRUNCATE TABLE dropdown.ConflictDriver
GO

SET IDENTITY_INSERT dropdown.ConflictDriver ON;
INSERT INTO dropdown.ConflictDriver (ConflictDriverID) VALUES (0);
SET IDENTITY_INSERT dropdown.ConflictDriver OFF;

INSERT INTO dropdown.ConflictDriver 
	(ConflictDriverName) 
VALUES 
	('Conflict Spillover'),
	('Demand for Jobs or Services'),
	('Land Dispute'),
	('Looting'),
	('Regionalism'),
	('Rent Extraction'),
	('Tribalism'),
	('Wage Dispute')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ConflictDriver'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ConflictDriver'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetConflictDriverData'
GO
--End data for table dropdown.ConflictDriver

--Begin data table dropdown.ConflictStorySource
TRUNCATE TABLE dropdown.ConflictStorySource
GO

SET IDENTITY_INSERT dropdown.ConflictStorySource ON;
INSERT INTO dropdown.ConflictStorySource (ConflictStorySourceID) VALUES (0);
SET IDENTITY_INSERT dropdown.ConflictStorySource OFF;

INSERT INTO dropdown.ConflictStorySource 
	(ConflictStorySourceName) 
VALUES 
	('STACSY')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ConflictStorySource'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'ConflictStorySource'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetConflictStorySourceData'
GO
--End data for table dropdown.ConflictStorySource

DELETE S FROM implementer.Synonym S WHERE S.BaseObjectSchema = 'dropdown' AND S.BaseObjectName = 'ConflictStoryYear'
DELETE S FROM implementer.Synonym S WHERE S.BaseObjectSchema = 'dropdown' AND S.BaseObjectName = 'GetConflictStoryYearData'
GO

--Begin data table dropdown.ContactType
DELETE IDD
FROM implementer.ImplementerDropdownData IDD
WHERE IDD.DropdownCode = 'ContactType'
GO

TRUNCATE TABLE dropdown.ContactType
GO

SET IDENTITY_INSERT dropdown.ContactType ON;
INSERT INTO dropdown.ContactType (ContactTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.ContactType OFF;

INSERT INTO dropdown.ContactType 
	(ContactTypeName) 
VALUES 
	('Mediator'),
	('Pragmatist'),
	('Spoiler')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactType'
GO
--End data for table dropdown.ContactType

--Begin data for table dropdown.DocumentIssue
TRUNCATE TABLE dropdown.DocumentIssue
GO

SET IDENTITY_INSERT dropdown.DocumentIssue ON;
INSERT INTO dropdown.DocumentIssue (DocumentIssueID) VALUES (0);
SET IDENTITY_INSERT dropdown.DocumentIssue OFF;

INSERT INTO dropdown.DocumentIssue 
	(DocumentIssueName) 
VALUES 
	('Issue 1'),
	('Issue 2'),
	('Issue 3')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'DocumentIssue'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetDocumentIssueData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'DocumentIssue'
GO
--End data for table dropdown.DocumentIssue

--Begin data for table dropdown.IncidentType
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Civil Activity', Icon = 'civilian.png' WHERE IncidentTypeName = 'Riot/Protest'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Military Activity - Air', Icon = 'military-air.png' WHERE IncidentTypeName = 'Airstrike'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Military Activity - Ground', Icon = 'military-ground.png' WHERE IncidentTypeName = 'Armed Assault'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Military Activity - Ground', Icon = 'military-ground.png' WHERE IncidentTypeName = 'Bombing/Explosion/Shelling'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Military Activity - Ground', Icon = 'military-ground.png' WHERE IncidentTypeName = 'Ground fighting'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Military Activity - Ground', Icon = 'military-ground.png' WHERE IncidentTypeName = 'Missile/Shelling'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Other', Icon = 'civilian.png' WHERE IncidentTypeName = 'Other/Unknown'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Security Activity', Icon = 'police.png' WHERE IncidentTypeName = 'Arrest/Detention'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Security Activity', Icon = 'police.png' WHERE IncidentTypeName = 'Mediation'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Security Activity', Icon = 'police.png' WHERE IncidentTypeName = 'Security Operation'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Terrorist Activity', Icon = 'crime.png' WHERE IncidentTypeName = 'Assassination'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Terrorist Activity', Icon = 'crime.png' WHERE IncidentTypeName = 'Assassination/Attempt'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Terrorist Activity', Icon = 'crime.png' WHERE IncidentTypeName = 'Hostage/Kidnap/Hijack'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Terrorist Activity', Icon = 'crime.png' WHERE IncidentTypeName = 'Kidnapping/Detention'
UPDATE dropdown.IncidentType SET IncidentTypeCategory = 'Terrorist Activity', Icon = 'crime.png' WHERE IncidentTypeName = 'Terrorist attack'
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType'
GO
--End data for table dropdown.IncidentType

--Begin data for table dropdown.MediationOutcome
TRUNCATE TABLE dropdown.MediationOutcome
GO

SET IDENTITY_INSERT dropdown.MediationOutcome ON;
INSERT INTO dropdown.MediationOutcome (MediationOutcomeID) VALUES (0);
SET IDENTITY_INSERT dropdown.MediationOutcome OFF;

INSERT INTO dropdown.MediationOutcome 
	(MediationOutcomeName) 
VALUES 
	('Successful'),
	('Unsuccessful')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'MediationOutcome'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetMediationOutcomeData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'MediationOutcome'
GO
--End data for table dropdown.MediationOutcome

--Begin data for table dropdown.MediationType
TRUNCATE TABLE dropdown.MediationType
GO

SET IDENTITY_INSERT dropdown.MediationType ON;
INSERT INTO dropdown.MediationType (MediationTypeID) VALUES (0);
SET IDENTITY_INSERT dropdown.MediationType OFF;

INSERT INTO dropdown.MediationType 
	(MediationTypeName) 
VALUES 
	('Community Leaders'),
	('Military or Security Actors'),
	('None'),
	('State Authorities'),
	('Tribal Sheikhs')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'MediationType'
GO

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetMediationTypeData'
EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'MediationType'
GO
--End data for table dropdown.MediationType


