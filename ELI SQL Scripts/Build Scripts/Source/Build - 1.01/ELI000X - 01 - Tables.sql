USE [[INSTANCENAME]]
GO

--Begin table conflictstory.ConflictStory
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStory'

EXEC utility.DropObject 'dbo.ConflictStory'
EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStory
	(
	ConflictStoryID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryName VARCHAR(250),
	ConflictStoryYear INT,
	ConflictStorySourceID INT,
	MediationOutcomeID INT,
	TerritoryID INT,
	Summary VARCHAR(MAX),
	ProjectID INT,
	IntegrataionCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConflictStorySourceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryYear', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MediationOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStory', 'ProjectID,ConflictStoryName'
GO
--End table conflictstory.ConflictStory

--Begin table conflictstory.ConflictStoryConflictDriver
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStoryConflictDriver'

EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStoryConflictDriver
	(
	ConflictStoryConflictDriverID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryID INT,
	ConflictDriverID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConflictDriverID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryConflictDriverID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStoryConflictDriver', 'ConflictStoryID,ConflictDriverID'
GO
--End table conflictstory.ConflictStoryConflictDriver

--Begin table conflictstory.ConflictStoryMediationType
DECLARE @TableName VARCHAR(250) = 'conflictstory.ConflictStoryMediationType'

EXEC utility.DropObject @TableName

CREATE TABLE conflictstory.ConflictStoryMediationType
	(
	ConflictStoryMediationTypeID INT IDENTITY(1,1) NOT NULL,
	ConflictStoryID INT,
	MediationTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediationTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConflictStoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConflictStoryMediationTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConflictStoryMediationType', 'ConflictStoryID,MediationTypeID'
GO
--End table conflictstory.ConflictStoryMediationType

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.AddColumn @TableName, 'InfluenceDepth', 'VARCHAR(5)'
EXEC utility.AddColumn @TableName, 'IntegrationCode', 'VARCHAR(50)'

EXEC utility.RenameColumn @TableName, 'EmployerName', 'Alignment'
EXEC utility.RenameColumn @TableName, 'Title', 'Position'
EXEC utility.RenameColumn @TableName, 'Profession', 'Influence'
EXEC utility.RenameColumn @TableName, 'Influence', 'InfluenceReach'

ALTER TABLE contact.Contact ALTER COLUMN InfluenceReach VARCHAR(5)
GO
--End table contact.Contact

--Begin table document.DocumentDocumentIssue
DECLARE @TableName VARCHAR(250) = 'document.DocumentDocumentIssue'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentDocumentIssue
	(
	DocumentDocumentIssueID INT NOT NULL IDENTITY(0,1),
	DocumentID INT,
	DocumentIssueID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentIssueID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentDocumentIssueID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentDocumentIssue', 'DocumentID,DocumentIssueID'
GO
--End table document.DocumentDocumentIssue
