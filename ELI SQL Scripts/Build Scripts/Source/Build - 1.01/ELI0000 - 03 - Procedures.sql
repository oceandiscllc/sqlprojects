USE ELI0000
GO

--Begin procedure dropdown.GetConflictDriverData
EXEC Utility.DropObject 'dropdown.GetConflictDriverData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConflictDriver table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetConflictDriverData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConflictDriverID, 
		T.ConflictDriverName
	FROM dropdown.ConflictDriver T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConflictDriverID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConflictDriver'
			AND (T.ConflictDriverID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConflictDriverName, T.ConflictDriverID

END
GO
--End procedure dropdown.GetConflictDriverData

--Begin procedure dropdown.GetConflictStorySourceData
EXEC Utility.DropObject 'dropdown.GetConflictStorySourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ConflictStorySource table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetConflictStorySourceData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConflictStorySourceID, 
		T.ConflictStorySourceName
	FROM dropdown.ConflictStorySource T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.ConflictStorySourceID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'ConflictStorySource'
			AND (T.ConflictStorySourceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConflictStorySourceName, T.ConflictStorySourceID

END
GO
--End procedure dropdown.GetConflictStorySourceData

--Begin procedure dropdown.GetDocumentIssueData
EXEC Utility.DropObject 'dropdown.GetDocumentIssueData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DocumentIssue table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetDocumentIssueData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentIssueID, 
		T.DocumentIssueName
	FROM dropdown.DocumentIssue T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.DocumentIssueID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'DocumentIssue'
			AND (T.DocumentIssueID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DocumentIssueName, T.DocumentIssueID

END
GO
--End procedure dropdown.GetDocumentIssueData

--Begin procedure dropdown.GetMediationOutcomeData
EXEC Utility.DropObject 'dropdown.GetMediationOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediationOutcome table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetMediationOutcomeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediationOutcomeID, 
		T.MediationOutcomeName
	FROM dropdown.MediationOutcome T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediationOutcomeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediationOutcome'
			AND (T.MediationOutcomeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediationOutcomeName, T.MediationOutcomeID

END
GO
--End procedure dropdown.GetMediationOutcomeData

--Begin procedure dropdown.GetMediationTypeData
EXEC Utility.DropObject 'dropdown.GetMediationTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.MediationType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetMediationTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediationTypeID, 
		T.MediationTypeName
	FROM dropdown.MediationType T
		JOIN implementer.ImplementerDropdownData IDD ON IDD.DropdownID = T.MediationTypeID
			AND IDD.ImplementerCode = @ImplementerCode
			AND IDD.DropdownCode = 'MediationType'
			AND (T.MediationTypeID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediationTypeName, T.MediationTypeID

END
GO
--End procedure dropdown.GetMediationTypeData

--Begin procedure implementer.ImplementerDropdownDataAddUpdate
EXEC Utility.DropObject 'implementer.ImplementerDropdownDataAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.27
-- Description:	A stored procedure to add a new dropdown / dropdown data to the LEO system and grant implemeters access to the new data
-- ====================================================================================================================================
CREATE PROCEDURE implementer.ImplementerDropdownDataAddUpdate

@DropdownCode VARCHAR(50),
@DropdownCodePrimaryKey VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cDropdownCodePrimaryKey VARCHAR(60) = ISNULL(@DropdownCodePrimaryKey, @DropdownCode + 'ID')
	DECLARE @cSQL VARCHAR(MAX)

	SET @cSQL = 'INSERT INTO implementer.ImplementerDropdownData (ImplementerCode, DropdownCode, DropdownID) SELECT I.ImplementerCode, '''
	SET @cSQL += @DropdownCode + ''', T.' + @cDropdownCodePrimaryKey + ' FROM dropdown.' + @DropdownCode
	SET @cSQL += ' T CROSS APPLY implementer.Implementer I WHERE NOT EXISTS'
	SET @cSQL += ' (SELECT 1 FROM implementer.ImplementerDropdownData IDD WHERE IDD.ImplementerCode = I.ImplementerCode AND IDD.DropdownCode = '''
	SET @cSQL += @DropdownCode + ''' AND IDD.DropdownID = T.' + @cDropdownCodePrimaryKey + ')'

	EXEC (@cSQL)

	SET @cSQL = 'UPDATE IDD SET IDD.IsActive = T.IsActive FROM implementer.ImplementerDropdownData IDD'
	SET @cSQL += ' JOIN dropdown.' + @DropdownCode + ' T ON T.' + @DropdownCodePrimaryKey + '= IDD.DropdownID'

	EXEC (@cSQL)

END
GO
--End procedure implementer.ImplementerDropdownDataAddUpdate