USE ELI0000
GO

EXEC implementer.ImplementerSynonymAddUpdate 'territory', 'GetParentTerritoryNameByTerritoryID'
EXEC implementer.ImplementerSynonymAddUpdate 'territory', 'GetTerritoryNameByTerritoryID'
GO

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Display the actors by governorate chart on the dashboard', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Main.Default.CanHaveContactCountsByTerritoryChart', @PERMISSIONCODE='CanHaveContactCountsByTerritoryChart';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Display the conflict vignettes governorate chart on the dashboard', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Main.Default.ConflictStoryCountsByTerritoryChart', @PERMISSIONCODE='ConflictStoryCountsByTerritoryChart';
GO
--End table permissionable.Permissionable

