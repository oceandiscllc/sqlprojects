USE [[INSTANCENAME]]
GO

--Begin procedure contact.GetRandomContact
EXEC utility.DropObject 'contact.GetRandomContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================
-- Author:			Christopher Crouch
-- Create date:	2017.11.19
-- Description:	This gets a random contact
-- =======================================
CREATE PROCEDURE contact.GetRandomContact

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT TOP 1
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.Alignment,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.InfluenceDepth,
		C1.InfluenceReach,
		C1.IsActive,
		C1.IsAidRecipient,
		C1.IsTrainer,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.Position,
		C1.PostalCode,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.SummaryBiography,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		CS.ContactStatusID,
		CS.ContactStatusName,
		EL.EducationLevelID,
		EL.EducationLevelName,
		(
			SELECT DocumentData 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImageData,
		(
			SELECT DocumentTitle 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImage,
		(
			SELECT ContentSubType 
			FROM ELI0000.document.Document dd
			JOIN document.DocumentEntity de ON de.DocumentID = dd.DocumentID
			AND de.EntityTypeCode = 'Contact' AND de.EntityTypeSubCode = 'BioImage'
			AND de.EntityID = C1.ContactID
		) AS BioImageType
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.EducationLevel EL ON EL.EducationLevelID = C1.EducationLevelID
	ORDER BY newid()

END
GO
--End procedure contact.GetRandomContact

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		'fa fa-fw fa-tasks' AS Icon,
		AR.ActivityReportID AS EntityID,
		AR.ActivityReportTitle AS Title,
		AR.UpdateDateTime,
		core.FormatDate(AR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM activityreport.ActivityReport AR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ActivityReport'
		JOIN dropdown.Project P ON P.ProjectID = AR.ProjectID
			AND AR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = AR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, AR.ActivityReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-file-text' AS Icon,
		DOC.DocumentID AS EntityID,
		DOC.DocumentTitle AS Title,
		DOC.DocumentDate AS UpdateDateTime,
		core.FormatDate(DOC.DocumentDate) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM ELI0000.document.Document DOC
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'Document'
		JOIN dropdown.Project P ON P.ProjectID = DOC.ProjectID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = DOC.DocumentTypeID
			AND DOC.DocumentDate >= DATEADD(d, -14, getDate())
			AND 
				(
				EXISTS 
					(
					SELECT 1
					FROM person.PersonPermissionable PP
					WHERE PP.PersonID = @PersonID
						AND PP.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
					)
				OR DOC.DocumentTypeID > 0	
				)
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = DOC.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, DOC.DocumentID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-exclamation-circle' AS Icon,
		I.ImpactStoryID AS EntityID,
		I.ImpactStoryName AS Title,
		I.UpdateDateTime,
		core.FormatDate(I.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM impactstory.ImpactStory I
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'ImpactStory'
		JOIN dropdown.Project P ON P.ProjectID = I.ProjectID
			AND I.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = I.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, I.ImpactStoryID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM dbo.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID, P.ProjectID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID, P.ProjectID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID, P.ProjectID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed