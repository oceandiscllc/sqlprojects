USE ELI0000
GO

--Begin function territory.GetTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.29
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION territory.GetTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cTerritoryName VARCHAR(250) = ''
	
	SELECT @cTerritoryName = T.TerritoryName
	FROM territory.Territory T 
	WHERE T.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryName, '')
	
END
GO
--End function territory.GetTerritoryNameByTerritoryID

--Begin function territory.GetParentTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.GetParentTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create Date:	2017.12.10
-- Description:	A function to return the name of a parent territory
-- ================================================================

CREATE FUNCTION territory.GetParentTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cParentTerritoryName VARCHAR(250) = ''

	SELECT @cParentTerritoryName = T2.TerritoryName
	FROM territory.Territory T1
		JOIN territory.Territory T2 ON T2.TerritoryID = T1.ParentTerritoryID
			AND T1.TerritoryID = @TerritoryID

	RETURN ISNULL(@cParentTerritoryName, '')
	
END
GO
--End function territory.GetParentTerritoryNameByTerritoryID