USE [[INSTANCENAME]]
GO

--Begin table conflictstory.ConflictStory
EXEC utility.DropObject 'conflictstory.TR_TerritoryConflictStoryCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.11.16
-- Description:	A trigger to update the territory.TerritoryEntityCount table
-- =========================================================================
CREATE TRIGGER conflictstory.TR_TerritoryConflictStoryCount ON conflictstory.ConflictStory AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET ARITHABORT ON;

	DELETE FROM territory.TerritoryEntityCount
	WHERE EntityTypeCode = 'ConflictStory';

	INSERT INTO territory.TerritoryEntityCount
		(ProjectID,TerritoryID,EntityTypeCode,EntityCount)
	SELECT 
		CS.ProjectID,
		T.TerritoryID,
		'ConflictStory',
		conflictstory.GetConflictStoryCountByTerritory(T.TerritoryID, CS.ProjectID, NULL)
	FROM territory.Territory T 
		CROSS JOIN conflictstory.ConflictStory CS
	GROUP BY CS.ProjectID, T.TerritoryID
	ORDER BY T.TerritoryID, CS.ProjectID

	ALTER TABLE conflictstory.ConflictStory ENABLE TRIGGER TR_TerritoryConflictStoryCount

END
GO
--End table conflictstory.ConflictStory

--Begin table territory.TerritoryEntityCount
EXEC utility.DropObject 'territory.TR_TerritoryIncidentCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Kevin Ross
-- Create date:	2017.11.16
-- Description:	A trigger to update the territory.TerritoryEntityCount table
-- =========================================================================
CREATE TRIGGER territory.TR_TerritoryIncidentCount ON territory.TerritoryEntityCount AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET ARITHABORT ON;

	DELETE FROM territory.TerritoryEntityCount
	WHERE EntityTypeCode = 'Incident';

	INSERT INTO territory.TerritoryEntityCount
		(ProjectID,TerritoryID,EntityTypeCode,EntityCount)
	SELECT 
		I.ProjectID,
		T.TerritoryID,
		'Incident',
		incident.GetIncidentCountByTerritory(T.TerritoryID, NULL, NULL, NULL, NULL, I.ProjectID)
	FROM territory.Territory T 
		CROSS JOIN incident.Incident I
	GROUP BY I.ProjectID, T.TerritoryID
	ORDER BY T.TerritoryID, I.ProjectID

	ALTER TABLE territory.TerritoryEntityCount ENABLE TRIGGER TR_TerritoryIncidentCount

END
GO
--End table territory.TerritoryEntityCount
