USE ELI0000
GO

--Begin table dropdown.DocumentType
INSERT INTO dropdown.DocumentType
	(DocumentCategoryID, DocumentTypeCode, DocumentTypeName)
SELECT
	DC.DocumentCategoryID,
	'UNDocuments',
	'Documents for the United Nations'
FROM dropdown.DocumentCategory DC
WHERE DC.DocumentCategoryName = 'Reports'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dropdown.DocumentType DT
		WHERE DT.DocumentTypeCode = 'UNDocuments'
		)

UNION

SELECT
	DC.DocumentCategoryID,
	'ProgressReports',
	'Progress Reports'
FROM dropdown.DocumentCategory DC
WHERE DC.DocumentCategoryName = 'Reports'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dropdown.DocumentType DT
		WHERE DT.DocumentTypeCode = 'ProgressReports'
		)

UNION

SELECT
	DC.DocumentCategoryID,
	'SecurityIncidentData',
	'Security Incident Data'
FROM dropdown.DocumentCategory DC
WHERE DC.DocumentCategoryName = 'Reports'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dropdown.DocumentType DT
		WHERE DT.DocumentTypeCode = 'SecurityIncidentData'
		)

UNION

SELECT
	DC.DocumentCategoryID,
	'SecurityIncidentTrends',
	'SecurityIncidentTrends'
FROM dropdown.DocumentCategory DC
WHERE DC.DocumentCategoryName = 'Reports'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dropdown.DocumentType DT
		WHERE DT.DocumentTypeCode = 'SecurityIncidentTrends'
		)
GO

UPDATE DT
SET DT.IsActive = 0
FROM dropdown.DocumentType DT
	JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = DT.DocumentCategoryID
		AND DC.DocumentCategoryName <> 'Reports'
GO

UPDATE DT
SET DT.IsActive = 0
FROM dropdown.DocumentType DT
	JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = DT.DocumentCategoryID
		AND DC.DocumentCategoryName = 'Reports'
		AND DT.DocumentTypeCode NOT IN ('Actors', 'Atmospheric', 'ConflictDrivers', 'Influencers', 'ProgressReports', 'UNDocuments')
GO

UPDATE DT
SET DT.DocumentTypeName = 'Releasable but Limited Distribution Documents'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'UNDocuments'
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'DocumentType', 'DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.IncidentSource
TRUNCATE TABLE dropdown.IncidentSource
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentSource', 'IncidentSourceID', 0
GO

INSERT INTO dropdown.IncidentSource
	(IncidentSourceName)
VALUES
	('JANES'),
	('STACSY')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentSource', 'IncidentSourceID'
GO
--End table dropdown.IncidentSource

--Begin table dropdown.IncidentType
TRUNCATE TABLE dropdown.IncidentType
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentType', 'IncidentTypeID', 0
GO

INSERT INTO dropdown.IncidentType
	(IncidentTypeCategory, IncidentTypeName, Icon)
VALUES
	('Civil Activity', 'Civil Violence', 'civilian.png'),
	('Civil Activity', 'Peaceful Protest', 'civilian.png'),
	('Civil Activity', 'Tribal or Civil Mediation', 'civilian.png'),
	('Military Activity', 'Airstrike', 'military-air.png'),
	('Military Activity', 'Disengaegment of Forces', 'military-ground.png'),
	('Military Activity', 'Ground Attack', 'military-ground.png'),
	('Military Activity', 'Mobilization of Forces', 'military-ground.png'),
	('Other', 'Undefined Incident', 'civilian.png'),
	('Terrorist Activity', 'Attack by an FTO', 'crime.png')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType', 'IncidentTypeID'
GO
--End table dropdown.IncidentType

--Begin table territory.Territory
UPDATE T
SET T.TerritoryName = 'Finesse'
FROM territory.Territory T
WHERE T.TerritoryCode = '1826'
GO
--End table territory.Territory
