USE [[INSTANCENAME]]
GO

EXEC utility.AddColumn 'contact.Contact', 'SummaryBiography', 'NVARCHAR(MAX)'
GO
