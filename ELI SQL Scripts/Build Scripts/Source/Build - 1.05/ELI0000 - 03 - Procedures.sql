USE ELI0000
GO

--Begin procedure dropdown.GetGovernorateData
EXEC Utility.DropObject 'dropdown.GetGovernorateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.11.30
-- Description:	A stored procedure to return data from the territory.Territory table
-- =================================================================================
CREATE PROCEDURE dropdown.GetGovernorateData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryID, 
		T.TerritoryName
	FROM territory.Territory T
	WHERE T.TerritoryTypeCode = 'Governorate'
	ORDER BY T.TerritoryName, T.TerritoryID

END
GO
--End procedure dropdown.GetGovernorateData
