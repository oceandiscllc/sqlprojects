USE ELI0000
GO

--Begin table dropdown.ContactType
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'None')
	BEGIN

	INSERT INTO dropdown.ContactType
		(ContactTypeName)
	VALUES
		('None')

	END
--ENDIF
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'ContactType', 'ContactTypeID'
GO
--End table dropdown.ContactType

--Begin table dropdown.DocumentType
UPDATE DT
SET DT.DocumentTypeName = 'Security Incident Trends'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'SecurityIncidentTrends'
GO

UPDATE DT
SET DT.IsActive = 
	CASE 
		WHEN DC.DocumentCategoryName = 'Reports' AND DT.DocumentTypeCode IN ('Actors', 'Atmospheric', 'ConflictDrivers', 'Influencers', 'ProgressReports', 'SecurityIncidentData', 'SecurityIncidentTrends', 'UNDocuments')
		THEN 1
		ELSE 0
	END

FROM dropdown.DocumentType DT
	JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = DT.DocumentCategoryID
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'DocumentType', 'DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.IncidentType
UPDATE IT
SET IT.IncidentTypeName = 'Disengagement of Forces'
FROM dropdown.IncidentType IT
WHERE IT.IncidentTypeName = 'Disengagment of Forces'
GO
--End table dropdown.IncidentType

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Edit contact notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ShowNotes', @PERMISSIONCODE='ShowNotes';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Edit contact biography summary', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.AddUpdate.SummaryBiography', @PERMISSIONCODE='SummaryBiography';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contact notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.View.ShowNotes', @PERMISSIONCODE='ShowNotes';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contact biography summary', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contacts', @PERMISSIONABLELINEAGE='Contact.View.SummaryBiography', @PERMISSIONCODE='SummaryBiography';
GO
--End table permissionable.Permissionable

EXEC implementer.ImplementerSynonymAddUpdate 'dropdown', 'GetGovernorateData'
GO