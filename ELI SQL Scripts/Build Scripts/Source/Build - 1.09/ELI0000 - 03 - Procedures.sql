USE ELI0000
GO

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.Extension,
		REPLACE(REPLACE(REPLACE(D.DocumentTitle, '-', '_'), ' ', '_'), ',', '_') AS DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

