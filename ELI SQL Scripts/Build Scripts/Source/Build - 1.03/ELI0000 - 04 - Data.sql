USE ELI0000
GO

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate --Actors
	@NewMenuItemCode = 'Contacts', 											
	@NewMenuItemText = 'Actors'
GO

EXEC core.MenuItemAddUpdate --Actors
	@NewMenuItemCode = 'ContactList', 											
	@NewMenuItemText = 'Actors'
GO
--End table core.MenuItem

UPDATE DT
SET 
	DT.DocumentTypeCode = 'Atmospheric',
	DT.DocumentTypeName = 'Atmospheric'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'Situational'
GO

UPDATE DT
SET DT.DisplayOrder = 0
FROM dropdown.DocumentType DT
GO

UPDATE DT
SET DT.IsActive = 0
FROM dropdown.DocumentType DT
	JOIN dropdown.DocumentCategory DC ON DC.DocumentCategoryID = DT.DocumentCategoryID
		AND DC.DocumentCategoryName = 'Reports'
		AND DT.DocumentTypeCode IN ('AdditionalActivity', 'Media', 'Meeting', 'MonthlyActivity', 'QuarterlyActivity', 'RFIResponse', 'Spot', 'Training')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'Actors')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentCategoryID, DocumentTypeCode, DocumentTypeName)
	SELECT
		DC.DocumentCategoryID,
		'Actors',
		'Actor Biographies or Profiles'
	FROM dropdown.DocumentCategory DC
	WHERE DC.DocumentCategoryName = 'Reports'

	INSERT INTO dropdown.DocumentType
		(DocumentCategoryID, DocumentTypeCode, DocumentTypeName)
	SELECT
		DC.DocumentCategoryID,
		'ConflictDrivers',
		'Conflict Drivers and Mediation Mechanisms'
	FROM dropdown.DocumentCategory DC
	WHERE DC.DocumentCategoryName = 'Reports'

	INSERT INTO dropdown.DocumentType
		(DocumentCategoryID, DocumentTypeCode, DocumentTypeName)
	SELECT
		DC.DocumentCategoryID,
		'Influencers',
		'Mapping of Key Influencers'
	FROM dropdown.DocumentCategory DC
	WHERE DC.DocumentCategoryName = 'Reports'

	END
--ENDIF
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'DocumentType', 'DocumentTypeID'
GO

TRUNCATE TABLE dropdown.IncidentType
GO

SET IDENTITY_INSERT dropdown.IncidentType ON
GO

INSERT INTO dropdown.IncidentType (IncidentTypeID) VALUES (0)
GO

SET IDENTITY_INSERT dropdown.IncidentType OFF
GO

INSERT INTO dropdown.IncidentType
	(IncidentTypeCategory, IncidentTypeName, Icon)
VALUES
	('Civil Activity', 'Peaceful Protest', 'civilian.png'),
	('Civil Activity', 'Tribal or Civil Mediation', 'civilian.png'),
	('Military Activity - Air', 'Airstrike', 'military-air.png'),
	('Military Activity - Ground', 'Disengagment of Forces', 'military-ground.png'),
	('Military Activity - Ground', 'FTO Attack', 'military-ground.png'),
	('Military Activity - Ground', 'Ground Attack', 'military-ground.png'),
	('Military Activity - Ground', 'Mobilization of Forces', 'military-ground.png'),
	('Other', 'Undefined Incident', 'civilian.png'),
	('Terrorist Activity', 'Civil Violence', 'crime.png'),
	('Terrorist Activity', 'FTO Attack', 'crime.png')
GO

EXEC implementer.ImplementerDropdownDataAddUpdate 'IncidentType', 'IncidentTypeID'
GO

UPDATE T SET T.TerritoryCode = '1209' FROM territory.Territory T WHERE T.TerritoryID = 66
UPDATE T SET T.TerritoryCode = '1201' FROM territory.Territory T WHERE T.TerritoryID = 50
UPDATE T SET T.TerritoryCode = '1208' FROM territory.Territory T WHERE T.TerritoryID = 61
UPDATE T SET T.TerritoryCode = '1203' FROM territory.Territory T WHERE T.TerritoryID = 53
UPDATE T SET T.TerritoryCode = '1211' FROM territory.Territory T WHERE T.TerritoryID = 68
UPDATE T SET T.TerritoryCode = '1204' FROM territory.Territory T WHERE T.TerritoryID = 56
UPDATE T SET T.TerritoryCode = '1202' FROM territory.Territory T WHERE T.TerritoryID = 51
UPDATE T SET T.TerritoryCode = '1206' FROM territory.Territory T WHERE T.TerritoryID = 57
UPDATE T SET T.TerritoryCode = '1207' FROM territory.Territory T WHERE T.TerritoryID = 58
UPDATE T SET T.TerritoryCode = '1205' FROM territory.Territory T WHERE T.TerritoryID = 54
UPDATE T SET T.TerritoryCode = '1210' FROM territory.Territory T WHERE T.TerritoryID = 59
UPDATE T SET T.TerritoryCode = '2404' FROM territory.Territory T WHERE T.TerritoryID = 270
UPDATE T SET T.TerritoryCode = '2406' FROM territory.Territory T WHERE T.TerritoryID = 37
UPDATE T SET T.TerritoryCode = '2403' FROM territory.Territory T WHERE T.TerritoryID = 35
UPDATE T SET T.TerritoryCode = '2402' FROM territory.Territory T WHERE T.TerritoryID = 269
UPDATE T SET T.TerritoryCode = '2405' FROM territory.Territory T WHERE T.TerritoryID = 253
UPDATE T SET T.TerritoryCode = '2401' FROM territory.Territory T WHERE T.TerritoryID = 268
UPDATE T SET T.TerritoryCode = '2408' FROM territory.Territory T WHERE T.TerritoryID = 271
UPDATE T SET T.TerritoryCode = '2407' FROM territory.Territory T WHERE T.TerritoryID = 259
UPDATE T SET T.TerritoryCode = '1416' FROM territory.Territory T WHERE T.TerritoryID = 83
UPDATE T SET T.TerritoryCode = '1410' FROM territory.Territory T WHERE T.TerritoryID = 78
UPDATE T SET T.TerritoryCode = '1409' FROM territory.Territory T WHERE T.TerritoryID = 70
UPDATE T SET T.TerritoryCode = '1420' FROM territory.Territory T WHERE T.TerritoryID = 87
UPDATE T SET T.TerritoryCode = '1414' FROM territory.Territory T WHERE T.TerritoryID = 82
UPDATE T SET T.TerritoryCode = '1418' FROM territory.Territory T WHERE T.TerritoryID = 86
UPDATE T SET T.TerritoryCode = '1404' FROM territory.Territory T WHERE T.TerritoryID = 72
UPDATE T SET T.TerritoryCode = '1419' FROM territory.Territory T WHERE T.TerritoryID = 88
UPDATE T SET T.TerritoryCode = '1411' FROM territory.Territory T WHERE T.TerritoryID = 79
UPDATE T SET T.TerritoryCode = '1407' FROM territory.Territory T WHERE T.TerritoryID = 77
UPDATE T SET T.TerritoryCode = '1405' FROM territory.Territory T WHERE T.TerritoryID = 52
UPDATE T SET T.TerritoryCode = '1406' FROM territory.Territory T WHERE T.TerritoryID = 73
UPDATE T SET T.TerritoryCode = '1403' FROM territory.Territory T WHERE T.TerritoryID = 69
UPDATE T SET T.TerritoryCode = '1408' FROM territory.Territory T WHERE T.TerritoryID = 74
UPDATE T SET T.TerritoryCode = '1401' FROM territory.Territory T WHERE T.TerritoryID = 46
UPDATE T SET T.TerritoryCode = '1402' FROM territory.Territory T WHERE T.TerritoryID = 48
UPDATE T SET T.TerritoryCode = '1413' FROM territory.Territory T WHERE T.TerritoryID = 81
UPDATE T SET T.TerritoryCode = '1412' FROM territory.Territory T WHERE T.TerritoryID = 80
UPDATE T SET T.TerritoryCode = '1417' FROM territory.Territory T WHERE T.TerritoryID = 85
UPDATE T SET T.TerritoryCode = '1415' FROM territory.Territory T WHERE T.TerritoryID = 84
UPDATE T SET T.TerritoryCode = '3006' FROM territory.Territory T WHERE T.TerritoryID = 344
UPDATE T SET T.TerritoryCode = '3008' FROM territory.Territory T WHERE T.TerritoryID = 346
UPDATE T SET T.TerritoryCode = '3009' FROM territory.Territory T WHERE T.TerritoryID = 347
UPDATE T SET T.TerritoryCode = '3005' FROM territory.Territory T WHERE T.TerritoryID = 343
UPDATE T SET T.TerritoryCode = '3004' FROM territory.Territory T WHERE T.TerritoryID = 342
UPDATE T SET T.TerritoryCode = '3002' FROM territory.Territory T WHERE T.TerritoryID = 339
UPDATE T SET T.TerritoryCode = '3007' FROM territory.Territory T WHERE T.TerritoryID = 345
UPDATE T SET T.TerritoryCode = '3001' FROM territory.Territory T WHERE T.TerritoryID = 340
UPDATE T SET T.TerritoryCode = '3003' FROM territory.Territory T WHERE T.TerritoryID = 341
UPDATE T SET T.TerritoryCode = '1809' FROM territory.Territory T WHERE T.TerritoryID = 159
UPDATE T SET T.TerritoryCode = '1814' FROM territory.Territory T WHERE T.TerritoryID = 164
UPDATE T SET T.TerritoryCode = '1825' FROM territory.Territory T WHERE T.TerritoryID = 176
UPDATE T SET T.TerritoryCode = '1811' FROM territory.Territory T WHERE T.TerritoryID = 161
UPDATE T SET T.TerritoryCode = '1823' FROM territory.Territory T WHERE T.TerritoryID = 171
UPDATE T SET T.TerritoryCode = '1821' FROM territory.Territory T WHERE T.TerritoryID = 170
UPDATE T SET T.TerritoryCode = '1820' FROM territory.Territory T WHERE T.TerritoryID = 169
UPDATE T SET T.TerritoryCode = '1802' FROM territory.Territory T WHERE T.TerritoryID = 149
UPDATE T SET T.TerritoryCode = '1816' FROM territory.Territory T WHERE T.TerritoryID = 166
UPDATE T SET T.TerritoryCode = '1813' FROM territory.Territory T WHERE T.TerritoryID = 163
UPDATE T SET T.TerritoryCode = '1822' FROM territory.Territory T WHERE T.TerritoryID = 173
UPDATE T SET T.TerritoryCode = '1808' FROM territory.Territory T WHERE T.TerritoryID = 154
UPDATE T SET T.TerritoryCode = '1805' FROM territory.Territory T WHERE T.TerritoryID = 156
UPDATE T SET T.TerritoryCode = '1806' FROM territory.Territory T WHERE T.TerritoryID = 157
UPDATE T SET T.TerritoryCode = '1804' FROM territory.Territory T WHERE T.TerritoryID = 155
UPDATE T SET T.TerritoryCode = '1815' FROM territory.Territory T WHERE T.TerritoryID = 165
UPDATE T SET T.TerritoryCode = '1807' FROM territory.Territory T WHERE T.TerritoryID = 158
UPDATE T SET T.TerritoryCode = '1801' FROM territory.Territory T WHERE T.TerritoryID = 152
UPDATE T SET T.TerritoryCode = '1810' FROM territory.Territory T WHERE T.TerritoryID = 160
UPDATE T SET T.TerritoryCode = '1817' FROM territory.Territory T WHERE T.TerritoryID = 167
UPDATE T SET T.TerritoryCode = '1812' FROM territory.Territory T WHERE T.TerritoryID = 162
UPDATE T SET T.TerritoryCode = '1819' FROM territory.Territory T WHERE T.TerritoryID = 172
UPDATE T SET T.TerritoryCode = '1818' FROM territory.Territory T WHERE T.TerritoryID = 168
UPDATE T SET T.TerritoryCode = '1803' FROM territory.Territory T WHERE T.TerritoryID = 153
UPDATE T SET T.TerritoryCode = '1824' FROM territory.Territory T WHERE T.TerritoryID = 174
UPDATE T SET T.TerritoryCode = '1826' FROM territory.Territory T WHERE T.TerritoryID = 175
UPDATE T SET T.TerritoryCode = '1608' FROM territory.Territory T WHERE T.TerritoryID = 117
UPDATE T SET T.TerritoryCode = '1602' FROM territory.Territory T WHERE T.TerritoryID = 108
UPDATE T SET T.TerritoryCode = '1605' FROM territory.Territory T WHERE T.TerritoryID = 114
UPDATE T SET T.TerritoryCode = '1609' FROM territory.Territory T WHERE T.TerritoryID = 118
UPDATE T SET T.TerritoryCode = '1607' FROM territory.Territory T WHERE T.TerritoryID = 116
UPDATE T SET T.TerritoryCode = '1603' FROM territory.Territory T WHERE T.TerritoryID = 110
UPDATE T SET T.TerritoryCode = '1606' FROM territory.Territory T WHERE T.TerritoryID = 115
UPDATE T SET T.TerritoryCode = '1604' FROM territory.Territory T WHERE T.TerritoryID = 113
UPDATE T SET T.TerritoryCode = '1610' FROM territory.Territory T WHERE T.TerritoryID = 119
UPDATE T SET T.TerritoryCode = '1612' FROM territory.Territory T WHERE T.TerritoryID = 121
UPDATE T SET T.TerritoryCode = '1601' FROM territory.Territory T WHERE T.TerritoryID = 109
UPDATE T SET T.TerritoryCode = '1611' FROM territory.Territory T WHERE T.TerritoryID = 120
UPDATE T SET T.TerritoryCode = '2804' FROM territory.Territory T WHERE T.TerritoryID = 310
UPDATE T SET T.TerritoryCode = '2806' FROM territory.Territory T WHERE T.TerritoryID = 316
UPDATE T SET T.TerritoryCode = '2802' FROM territory.Territory T WHERE T.TerritoryID = 313
UPDATE T SET T.TerritoryCode = '2803' FROM territory.Territory T WHERE T.TerritoryID = 314
UPDATE T SET T.TerritoryCode = '2809' FROM territory.Territory T WHERE T.TerritoryID = 318
UPDATE T SET T.TerritoryCode = '2805' FROM territory.Territory T WHERE T.TerritoryID = 311
UPDATE T SET T.TerritoryCode = '2808' FROM territory.Territory T WHERE T.TerritoryID = 317
UPDATE T SET T.TerritoryCode = '2807' FROM territory.Territory T WHERE T.TerritoryID = 315
UPDATE T SET T.TerritoryCode = '2801' FROM territory.Territory T WHERE T.TerritoryID = 312
UPDATE T SET T.TerritoryCode = '2704' FROM territory.Territory T WHERE T.TerritoryID = 306
UPDATE T SET T.TerritoryCode = '2709' FROM territory.Territory T WHERE T.TerritoryID = 309
UPDATE T SET T.TerritoryCode = '2708' FROM territory.Territory T WHERE T.TerritoryID = 303
UPDATE T SET T.TerritoryCode = '2703' FROM territory.Territory T WHERE T.TerritoryID = 305
UPDATE T SET T.TerritoryCode = '2702' FROM territory.Territory T WHERE T.TerritoryID = 304
UPDATE T SET T.TerritoryCode = '2707' FROM territory.Territory T WHERE T.TerritoryID = 308
UPDATE T SET T.TerritoryCode = '2706' FROM territory.Territory T WHERE T.TerritoryID = 302
UPDATE T SET T.TerritoryCode = '2705' FROM territory.Territory T WHERE T.TerritoryID = 307
UPDATE T SET T.TerritoryCode = '2701' FROM territory.Territory T WHERE T.TerritoryID = 301
UPDATE T SET T.TerritoryCode = '2904' FROM territory.Territory T WHERE T.TerritoryID = 323
UPDATE T SET T.TerritoryCode = '2903' FROM territory.Territory T WHERE T.TerritoryID = 322
UPDATE T SET T.TerritoryCode = '2906' FROM territory.Territory T WHERE T.TerritoryID = 324
UPDATE T SET T.TerritoryCode = '2915' FROM territory.Territory T WHERE T.TerritoryID = 333
UPDATE T SET T.TerritoryCode = '2914' FROM territory.Territory T WHERE T.TerritoryID = 332
UPDATE T SET T.TerritoryCode = '2913' FROM territory.Territory T WHERE T.TerritoryID = 331
UPDATE T SET T.TerritoryCode = '2920' FROM territory.Territory T WHERE T.TerritoryID = 338
UPDATE T SET T.TerritoryCode = '2909' FROM territory.Territory T WHERE T.TerritoryID = 327
UPDATE T SET T.TerritoryCode = '2908' FROM territory.Territory T WHERE T.TerritoryID = 326
UPDATE T SET T.TerritoryCode = '2901' FROM territory.Territory T WHERE T.TerritoryID = 319
UPDATE T SET T.TerritoryCode = '2902' FROM territory.Territory T WHERE T.TerritoryID = 320
UPDATE T SET T.TerritoryCode = '2918' FROM territory.Territory T WHERE T.TerritoryID = 336
UPDATE T SET T.TerritoryCode = '2912' FROM territory.Territory T WHERE T.TerritoryID = 330
UPDATE T SET T.TerritoryCode = '2919' FROM territory.Territory T WHERE T.TerritoryID = 337
UPDATE T SET T.TerritoryCode = '2910' FROM territory.Territory T WHERE T.TerritoryID = 328
UPDATE T SET T.TerritoryCode = '2916' FROM territory.Territory T WHERE T.TerritoryID = 334
UPDATE T SET T.TerritoryCode = '2911' FROM territory.Territory T WHERE T.TerritoryID = 329
UPDATE T SET T.TerritoryCode = '2907' FROM territory.Territory T WHERE T.TerritoryID = 325
UPDATE T SET T.TerritoryCode = '2905' FROM territory.Territory T WHERE T.TerritoryID = 321
UPDATE T SET T.TerritoryCode = '2917' FROM territory.Territory T WHERE T.TerritoryID = 335
UPDATE T SET T.TerritoryCode = '2001' FROM territory.Territory T WHERE T.TerritoryID = 207
UPDATE T SET T.TerritoryCode = '2012' FROM territory.Territory T WHERE T.TerritoryID = 217
UPDATE T SET T.TerritoryCode = '2010' FROM territory.Territory T WHERE T.TerritoryID = 215
UPDATE T SET T.TerritoryCode = '2011' FROM territory.Territory T WHERE T.TerritoryID = 216
UPDATE T SET T.TerritoryCode = '2008' FROM territory.Territory T WHERE T.TerritoryID = 214
UPDATE T SET T.TerritoryCode = '2003' FROM territory.Territory T WHERE T.TerritoryID = 206
UPDATE T SET T.TerritoryCode = '2002' FROM territory.Territory T WHERE T.TerritoryID = 208
UPDATE T SET T.TerritoryCode = '2004' FROM territory.Territory T WHERE T.TerritoryID = 209
UPDATE T SET T.TerritoryCode = '2009' FROM territory.Territory T WHERE T.TerritoryID = 210
UPDATE T SET T.TerritoryCode = '2005' FROM territory.Territory T WHERE T.TerritoryID = 211
UPDATE T SET T.TerritoryCode = '2006' FROM territory.Territory T WHERE T.TerritoryID = 212
UPDATE T SET T.TerritoryCode = '2007' FROM territory.Territory T WHERE T.TerritoryID = 213
UPDATE T SET T.TerritoryCode = '1914' FROM territory.Territory T WHERE T.TerritoryID = 191
UPDATE T SET T.TerritoryCode = '1922' FROM territory.Territory T WHERE T.TerritoryID = 197
UPDATE T SET T.TerritoryCode = '1906' FROM territory.Territory T WHERE T.TerritoryID = 183
UPDATE T SET T.TerritoryCode = '1930' FROM territory.Territory T WHERE T.TerritoryID = 205
UPDATE T SET T.TerritoryCode = '1929' FROM territory.Territory T WHERE T.TerritoryID = 203
UPDATE T SET T.TerritoryCode = '1907' FROM territory.Territory T WHERE T.TerritoryID = 184
UPDATE T SET T.TerritoryCode = '1903' FROM territory.Territory T WHERE T.TerritoryID = 181
UPDATE T SET T.TerritoryCode = '1921' FROM territory.Territory T WHERE T.TerritoryID = 196
UPDATE T SET T.TerritoryCode = '1913' FROM territory.Territory T WHERE T.TerritoryID = 190
UPDATE T SET T.TerritoryCode = '1912' FROM territory.Territory T WHERE T.TerritoryID = 178
UPDATE T SET T.TerritoryCode = '1915' FROM territory.Territory T WHERE T.TerritoryID = 188
UPDATE T SET T.TerritoryCode = '1925' FROM territory.Territory T WHERE T.TerritoryID = 200
UPDATE T SET T.TerritoryCode = '1918' FROM territory.Territory T WHERE T.TerritoryID = 204
UPDATE T SET T.TerritoryCode = '1917' FROM territory.Territory T WHERE T.TerritoryID = 193
UPDATE T SET T.TerritoryCode = '1927' FROM territory.Territory T WHERE T.TerritoryID = 202
UPDATE T SET T.TerritoryCode = '1905' FROM territory.Territory T WHERE T.TerritoryID = 180
UPDATE T SET T.TerritoryCode = '1924' FROM territory.Territory T WHERE T.TerritoryID = 199
UPDATE T SET T.TerritoryCode = '1926' FROM territory.Territory T WHERE T.TerritoryID = 201
UPDATE T SET T.TerritoryCode = '1928' FROM territory.Territory T WHERE T.TerritoryID = 354
UPDATE T SET T.TerritoryCode = '1916' FROM territory.Territory T WHERE T.TerritoryID = 192
UPDATE T SET T.TerritoryCode = '1920' FROM territory.Territory T WHERE T.TerritoryID = 195
UPDATE T SET T.TerritoryCode = '1901' FROM territory.Territory T WHERE T.TerritoryID = 177
UPDATE T SET T.TerritoryCode = '1909' FROM territory.Territory T WHERE T.TerritoryID = 186
UPDATE T SET T.TerritoryCode = '1910' FROM territory.Territory T WHERE T.TerritoryID = 187
UPDATE T SET T.TerritoryCode = '1908' FROM territory.Territory T WHERE T.TerritoryID = 185
UPDATE T SET T.TerritoryCode = '1911' FROM territory.Territory T WHERE T.TerritoryID = 189
UPDATE T SET T.TerritoryCode = '1902' FROM territory.Territory T WHERE T.TerritoryID = 179
UPDATE T SET T.TerritoryCode = '1919' FROM territory.Territory T WHERE T.TerritoryID = 194
UPDATE T SET T.TerritoryCode = '1923' FROM territory.Territory T WHERE T.TerritoryID = 198
UPDATE T SET T.TerritoryCode = '1904' FROM territory.Territory T WHERE T.TerritoryID = 182
UPDATE T SET T.TerritoryCode = '1724' FROM territory.Territory T WHERE T.TerritoryID = 144
UPDATE T SET T.TerritoryCode = '1704' FROM territory.Territory T WHERE T.TerritoryID = 124
UPDATE T SET T.TerritoryCode = '1714' FROM territory.Territory T WHERE T.TerritoryID = 135
UPDATE T SET T.TerritoryCode = '1710' FROM territory.Territory T WHERE T.TerritoryID = 112
UPDATE T SET T.TerritoryCode = '1708' FROM territory.Territory T WHERE T.TerritoryID = 128
UPDATE T SET T.TerritoryCode = '1717' FROM territory.Territory T WHERE T.TerritoryID = 138
UPDATE T SET T.TerritoryCode = '1715' FROM territory.Territory T WHERE T.TerritoryID = 136
UPDATE T SET T.TerritoryCode = '1716' FROM territory.Territory T WHERE T.TerritoryID = 137
UPDATE T SET T.TerritoryCode = '1725' FROM territory.Territory T WHERE T.TerritoryID = 145
UPDATE T SET T.TerritoryCode = '1721' FROM territory.Territory T WHERE T.TerritoryID = 142
UPDATE T SET T.TerritoryCode = '1712' FROM territory.Territory T WHERE T.TerritoryID = 131
UPDATE T SET T.TerritoryCode = '1701' FROM territory.Territory T WHERE T.TerritoryID = 122
UPDATE T SET T.TerritoryCode = '1727' FROM territory.Territory T WHERE T.TerritoryID = 146
UPDATE T SET T.TerritoryCode = '1729' FROM territory.Territory T WHERE T.TerritoryID = 150
UPDATE T SET T.TerritoryCode = '1728' FROM territory.Territory T WHERE T.TerritoryID = 147
UPDATE T SET T.TerritoryCode = '1702' FROM territory.Territory T WHERE T.TerritoryID = 123
UPDATE T SET T.TerritoryCode = '1705' FROM territory.Territory T WHERE T.TerritoryID = 125
UPDATE T SET T.TerritoryCode = '1711' FROM territory.Territory T WHERE T.TerritoryID = 130
UPDATE T SET T.TerritoryCode = '1722' FROM territory.Territory T WHERE T.TerritoryID = 143
UPDATE T SET T.TerritoryCode = '1718' FROM territory.Territory T WHERE T.TerritoryID = 139
UPDATE T SET T.TerritoryCode = '1709' FROM territory.Territory T WHERE T.TerritoryID = 129
UPDATE T SET T.TerritoryCode = '1707' FROM territory.Territory T WHERE T.TerritoryID = 127
UPDATE T SET T.TerritoryCode = '1720' FROM territory.Territory T WHERE T.TerritoryID = 141
UPDATE T SET T.TerritoryCode = '1703' FROM territory.Territory T WHERE T.TerritoryID = 111
UPDATE T SET T.TerritoryCode = '1706' FROM territory.Territory T WHERE T.TerritoryID = 126
UPDATE T SET T.TerritoryCode = '1726' FROM territory.Territory T WHERE T.TerritoryID = 133
UPDATE T SET T.TerritoryCode = '1713' FROM territory.Territory T WHERE T.TerritoryID = 134
UPDATE T SET T.TerritoryCode = '1731' FROM territory.Territory T WHERE T.TerritoryID = 151
UPDATE T SET T.TerritoryCode = '1719' FROM territory.Territory T WHERE T.TerritoryID = 140
UPDATE T SET T.TerritoryCode = '1723' FROM territory.Territory T WHERE T.TerritoryID = 132
UPDATE T SET T.TerritoryCode = '1730' FROM territory.Territory T WHERE T.TerritoryID = 148
UPDATE T SET T.TerritoryCode = '1119' FROM territory.Territory T WHERE T.TerritoryID = 47
UPDATE T SET T.TerritoryCode = '1107' FROM territory.Territory T WHERE T.TerritoryID = 30
UPDATE T SET T.TerritoryCode = '1118' FROM territory.Territory T WHERE T.TerritoryID = 24
UPDATE T SET T.TerritoryCode = '1101' FROM territory.Territory T WHERE T.TerritoryID = 22
UPDATE T SET T.TerritoryCode = '1111' FROM territory.Territory T WHERE T.TerritoryID = 38
UPDATE T SET T.TerritoryCode = '1104' FROM territory.Territory T WHERE T.TerritoryID = 26
UPDATE T SET T.TerritoryCode = '1103' FROM territory.Territory T WHERE T.TerritoryID = 25
UPDATE T SET T.TerritoryCode = '1114' FROM territory.Territory T WHERE T.TerritoryID = 42
UPDATE T SET T.TerritoryCode = '1106' FROM territory.Territory T WHERE T.TerritoryID = 28
UPDATE T SET T.TerritoryCode = '1115' FROM territory.Territory T WHERE T.TerritoryID = 40
UPDATE T SET T.TerritoryCode = '1105' FROM territory.Territory T WHERE T.TerritoryID = 27
UPDATE T SET T.TerritoryCode = '1113' FROM territory.Territory T WHERE T.TerritoryID = 41
UPDATE T SET T.TerritoryCode = '1116' FROM territory.Territory T WHERE T.TerritoryID = 43
UPDATE T SET T.TerritoryCode = '1110' FROM territory.Territory T WHERE T.TerritoryID = 31
UPDATE T SET T.TerritoryCode = '1109' FROM territory.Territory T WHERE T.TerritoryID = 36
UPDATE T SET T.TerritoryCode = '1108' FROM territory.Territory T WHERE T.TerritoryID = 32
UPDATE T SET T.TerritoryCode = '1120' FROM territory.Territory T WHERE T.TerritoryID = 49
UPDATE T SET T.TerritoryCode = '1112' FROM territory.Territory T WHERE T.TerritoryID = 39
UPDATE T SET T.TerritoryCode = '1117' FROM territory.Territory T WHERE T.TerritoryID = 45
UPDATE T SET T.TerritoryCode = '1102' FROM territory.Territory T WHERE T.TerritoryID = 23
UPDATE T SET T.TerritoryCode = '2514' FROM territory.Territory T WHERE T.TerritoryID = 278
UPDATE T SET T.TerritoryCode = '2501' FROM territory.Territory T WHERE T.TerritoryID = 272
UPDATE T SET T.TerritoryCode = '2513' FROM territory.Territory T WHERE T.TerritoryID = 286
UPDATE T SET T.TerritoryCode = '2512' FROM territory.Territory T WHERE T.TerritoryID = 284
UPDATE T SET T.TerritoryCode = '2508' FROM territory.Territory T WHERE T.TerritoryID = 280
UPDATE T SET T.TerritoryCode = '2503' FROM territory.Territory T WHERE T.TerritoryID = 274
UPDATE T SET T.TerritoryCode = '2509' FROM territory.Territory T WHERE T.TerritoryID = 281
UPDATE T SET T.TerritoryCode = '2510' FROM territory.Territory T WHERE T.TerritoryID = 283
UPDATE T SET T.TerritoryCode = '2505' FROM territory.Territory T WHERE T.TerritoryID = 276
UPDATE T SET T.TerritoryCode = '2506' FROM territory.Territory T WHERE T.TerritoryID = 277
UPDATE T SET T.TerritoryCode = '2507' FROM territory.Territory T WHERE T.TerritoryID = 279
UPDATE T SET T.TerritoryCode = '2511' FROM territory.Territory T WHERE T.TerritoryID = 282
UPDATE T SET T.TerritoryCode = '2515' FROM territory.Territory T WHERE T.TerritoryID = 285
UPDATE T SET T.TerritoryCode = '2502' FROM territory.Territory T WHERE T.TerritoryID = 273
UPDATE T SET T.TerritoryCode = '2504' FROM territory.Territory T WHERE T.TerritoryID = 275
UPDATE T SET T.TerritoryCode = '2611' FROM territory.Territory T WHERE T.TerritoryID = 298
UPDATE T SET T.TerritoryCode = '2607' FROM territory.Territory T WHERE T.TerritoryID = 294
UPDATE T SET T.TerritoryCode = '2605' FROM territory.Territory T WHERE T.TerritoryID = 292
UPDATE T SET T.TerritoryCode = '2609' FROM territory.Territory T WHERE T.TerritoryID = 296
UPDATE T SET T.TerritoryCode = '2604' FROM territory.Territory T WHERE T.TerritoryID = 289
UPDATE T SET T.TerritoryCode = '2614' FROM territory.Territory T WHERE T.TerritoryID = 290
UPDATE T SET T.TerritoryCode = '2603' FROM territory.Territory T WHERE T.TerritoryID = 291
UPDATE T SET T.TerritoryCode = '2610' FROM territory.Territory T WHERE T.TerritoryID = 297
UPDATE T SET T.TerritoryCode = '2601' FROM territory.Territory T WHERE T.TerritoryID = 287
UPDATE T SET T.TerritoryCode = '2613' FROM territory.Territory T WHERE T.TerritoryID = 300
UPDATE T SET T.TerritoryCode = '2612' FROM territory.Territory T WHERE T.TerritoryID = 299
UPDATE T SET T.TerritoryCode = '2602' FROM territory.Territory T WHERE T.TerritoryID = 288
UPDATE T SET T.TerritoryCode = '2608' FROM territory.Territory T WHERE T.TerritoryID = 295
UPDATE T SET T.TerritoryCode = '2606' FROM territory.Territory T WHERE T.TerritoryID = 293
UPDATE T SET T.TerritoryCode = '3103' FROM territory.Territory T WHERE T.TerritoryID = 350
UPDATE T SET T.TerritoryCode = '3106' FROM territory.Territory T WHERE T.TerritoryID = 353
UPDATE T SET T.TerritoryCode = '3102' FROM territory.Territory T WHERE T.TerritoryID = 349
UPDATE T SET T.TerritoryCode = '3101' FROM territory.Territory T WHERE T.TerritoryID = 348
UPDATE T SET T.TerritoryCode = '3105' FROM territory.Territory T WHERE T.TerritoryID = 352
UPDATE T SET T.TerritoryCode = '3104' FROM territory.Territory T WHERE T.TerritoryID = 351
UPDATE T SET T.TerritoryCode = '2207' FROM territory.Territory T WHERE T.TerritoryID = 242
UPDATE T SET T.TerritoryCode = '2213' FROM territory.Territory T WHERE T.TerritoryID = 248
UPDATE T SET T.TerritoryCode = '2212' FROM territory.Territory T WHERE T.TerritoryID = 247
UPDATE T SET T.TerritoryCode = '2201' FROM territory.Territory T WHERE T.TerritoryID = 235
UPDATE T SET T.TerritoryCode = '2204' FROM territory.Territory T WHERE T.TerritoryID = 238
UPDATE T SET T.TerritoryCode = '2208' FROM territory.Territory T WHERE T.TerritoryID = 243
UPDATE T SET T.TerritoryCode = '2214' FROM territory.Territory T WHERE T.TerritoryID = 250
UPDATE T SET T.TerritoryCode = '2210' FROM territory.Territory T WHERE T.TerritoryID = 244
UPDATE T SET T.TerritoryCode = '2203' FROM territory.Territory T WHERE T.TerritoryID = 237
UPDATE T SET T.TerritoryCode = '2202' FROM territory.Territory T WHERE T.TerritoryID = 236
UPDATE T SET T.TerritoryCode = '2205' FROM territory.Territory T WHERE T.TerritoryID = 241
UPDATE T SET T.TerritoryCode = '2215' FROM territory.Territory T WHERE T.TerritoryID = 240
UPDATE T SET T.TerritoryCode = '2211' FROM territory.Territory T WHERE T.TerritoryID = 246
UPDATE T SET T.TerritoryCode = '2209' FROM territory.Territory T WHERE T.TerritoryID = 245
UPDATE T SET T.TerritoryCode = '2206' FROM territory.Territory T WHERE T.TerritoryID = 239
UPDATE T SET T.TerritoryCode = '2308' FROM territory.Territory T WHERE T.TerritoryID = 258
UPDATE T SET T.TerritoryCode = '2309' FROM territory.Territory T WHERE T.TerritoryID = 260
UPDATE T SET T.TerritoryCode = '2315' FROM territory.Territory T WHERE T.TerritoryID = 266
UPDATE T SET T.TerritoryCode = '2302' FROM territory.Territory T WHERE T.TerritoryID = 251
UPDATE T SET T.TerritoryCode = '2313' FROM territory.Territory T WHERE T.TerritoryID = 264
UPDATE T SET T.TerritoryCode = '2314' FROM territory.Territory T WHERE T.TerritoryID = 265
UPDATE T SET T.TerritoryCode = '2304' FROM territory.Territory T WHERE T.TerritoryID = 254
UPDATE T SET T.TerritoryCode = '2307' FROM territory.Territory T WHERE T.TerritoryID = 257
UPDATE T SET T.TerritoryCode = '2306' FROM territory.Territory T WHERE T.TerritoryID = 255
UPDATE T SET T.TerritoryCode = '2301' FROM territory.Territory T WHERE T.TerritoryID = 249
UPDATE T SET T.TerritoryCode = '2316' FROM territory.Territory T WHERE T.TerritoryID = 267
UPDATE T SET T.TerritoryCode = '2312' FROM territory.Territory T WHERE T.TerritoryID = 263
UPDATE T SET T.TerritoryCode = '2310' FROM territory.Territory T WHERE T.TerritoryID = 261
UPDATE T SET T.TerritoryCode = '2303' FROM territory.Territory T WHERE T.TerritoryID = 252
UPDATE T SET T.TerritoryCode = '2311' FROM territory.Territory T WHERE T.TerritoryID = 262
UPDATE T SET T.TerritoryCode = '2305' FROM territory.Territory T WHERE T.TerritoryID = 256
UPDATE T SET T.TerritoryCode = '1306' FROM territory.Territory T WHERE T.TerritoryID = 63
UPDATE T SET T.TerritoryCode = '1305' FROM territory.Territory T WHERE T.TerritoryID = 62
UPDATE T SET T.TerritoryCode = '1304' FROM territory.Territory T WHERE T.TerritoryID = 67
UPDATE T SET T.TerritoryCode = '1307' FROM territory.Territory T WHERE T.TerritoryID = 64
UPDATE T SET T.TerritoryCode = '1309' FROM territory.Territory T WHERE T.TerritoryID = 33
UPDATE T SET T.TerritoryCode = '1303' FROM territory.Territory T WHERE T.TerritoryID = 55
UPDATE T SET T.TerritoryCode = '1310' FROM territory.Territory T WHERE T.TerritoryID = 44
UPDATE T SET T.TerritoryCode = '1308' FROM territory.Territory T WHERE T.TerritoryID = 65
UPDATE T SET T.TerritoryCode = '1301' FROM territory.Territory T WHERE T.TerritoryID = 60
UPDATE T SET T.TerritoryCode = '1302' FROM territory.Territory T WHERE T.TerritoryID = 29
UPDATE T SET T.TerritoryCode = '2106' FROM territory.Territory T WHERE T.TerritoryID = 224
UPDATE T SET T.TerritoryCode = '2115' FROM territory.Territory T WHERE T.TerritoryID = 234
UPDATE T SET T.TerritoryCode = '2104' FROM territory.Territory T WHERE T.TerritoryID = 221
UPDATE T SET T.TerritoryCode = '2112' FROM territory.Territory T WHERE T.TerritoryID = 230
UPDATE T SET T.TerritoryCode = '2102' FROM territory.Territory T WHERE T.TerritoryID = 219
UPDATE T SET T.TerritoryCode = '2113' FROM territory.Territory T WHERE T.TerritoryID = 231
UPDATE T SET T.TerritoryCode = '2107' FROM territory.Territory T WHERE T.TerritoryID = 225
UPDATE T SET T.TerritoryCode = '2101' FROM territory.Territory T WHERE T.TerritoryID = 218
UPDATE T SET T.TerritoryCode = '2114' FROM territory.Territory T WHERE T.TerritoryID = 232
UPDATE T SET T.TerritoryCode = '2111' FROM territory.Territory T WHERE T.TerritoryID = 229
UPDATE T SET T.TerritoryCode = '2103' FROM territory.Territory T WHERE T.TerritoryID = 220
UPDATE T SET T.TerritoryCode = '2108' FROM territory.Territory T WHERE T.TerritoryID = 226
UPDATE T SET T.TerritoryCode = '2109' FROM territory.Territory T WHERE T.TerritoryID = 227
UPDATE T SET T.TerritoryCode = '2116' FROM territory.Territory T WHERE T.TerritoryID = 233
UPDATE T SET T.TerritoryCode = '2110' FROM territory.Territory T WHERE T.TerritoryID = 228
UPDATE T SET T.TerritoryCode = '2105' FROM territory.Territory T WHERE T.TerritoryID = 223
UPDATE T SET T.TerritoryCode = '2117' FROM territory.Territory T WHERE T.TerritoryID = 222
UPDATE T SET T.TerritoryCode = '1505' FROM territory.Territory T WHERE T.TerritoryID = 93
UPDATE T SET T.TerritoryCode = '1521' FROM territory.Territory T WHERE T.TerritoryID = 106
UPDATE T SET T.TerritoryCode = '1522' FROM territory.Territory T WHERE T.TerritoryID = 107
UPDATE T SET T.TerritoryCode = '1511' FROM territory.Territory T WHERE T.TerritoryID = 98
UPDATE T SET T.TerritoryCode = '1517' FROM territory.Territory T WHERE T.TerritoryID = 34
UPDATE T SET T.TerritoryCode = '1518' FROM territory.Territory T WHERE T.TerritoryID = 75
UPDATE T SET T.TerritoryCode = '1515' FROM territory.Territory T WHERE T.TerritoryID = 100
UPDATE T SET T.TerritoryCode = '1513' FROM territory.Territory T WHERE T.TerritoryID = 101
UPDATE T SET T.TerritoryCode = '1514' FROM territory.Territory T WHERE T.TerritoryID = 102
UPDATE T SET T.TerritoryCode = '1520' FROM territory.Territory T WHERE T.TerritoryID = 104
UPDATE T SET T.TerritoryCode = '1506' FROM territory.Territory T WHERE T.TerritoryID = 94
UPDATE T SET T.TerritoryCode = '1516' FROM territory.Territory T WHERE T.TerritoryID = 103
UPDATE T SET T.TerritoryCode = '1508' FROM territory.Territory T WHERE T.TerritoryID = 95
UPDATE T SET T.TerritoryCode = '1512' FROM territory.Territory T WHERE T.TerritoryID = 99
UPDATE T SET T.TerritoryCode = '1504' FROM territory.Territory T WHERE T.TerritoryID = 92
UPDATE T SET T.TerritoryCode = '1509' FROM territory.Territory T WHERE T.TerritoryID = 96
UPDATE T SET T.TerritoryCode = '1501' FROM territory.Territory T WHERE T.TerritoryID = 89
UPDATE T SET T.TerritoryCode = '1507' FROM territory.Territory T WHERE T.TerritoryID = 71
UPDATE T SET T.TerritoryCode = '1523' FROM territory.Territory T WHERE T.TerritoryID = 105
UPDATE T SET T.TerritoryCode = '1510' FROM territory.Territory T WHERE T.TerritoryID = 97
UPDATE T SET T.TerritoryCode = '1519' FROM territory.Territory T WHERE T.TerritoryID = 76
UPDATE T SET T.TerritoryCode = '1503' FROM territory.Territory T WHERE T.TerritoryID = 91
UPDATE T SET T.TerritoryCode = '1502' FROM territory.Territory T WHERE T.TerritoryID = 90
UPDATE T SET T.TerritoryCode = '12' FROM territory.Territory T WHERE T.TerritoryID = 2
UPDATE T SET T.TerritoryCode = '24' FROM territory.Territory T WHERE T.TerritoryID = 11
UPDATE T SET T.TerritoryCode = '14' FROM territory.Territory T WHERE T.TerritoryID = 3
UPDATE T SET T.TerritoryCode = '30' FROM territory.Territory T WHERE T.TerritoryID = 17
UPDATE T SET T.TerritoryCode = '18' FROM territory.Territory T WHERE T.TerritoryID = 6
UPDATE T SET T.TerritoryCode = '16' FROM territory.Territory T WHERE T.TerritoryID = 20
UPDATE T SET T.TerritoryCode = '28' FROM territory.Territory T WHERE T.TerritoryID = 15
UPDATE T SET T.TerritoryCode = '27' FROM territory.Territory T WHERE T.TerritoryID = 14
UPDATE T SET T.TerritoryCode = '29' FROM territory.Territory T WHERE T.TerritoryID = 16
UPDATE T SET T.TerritoryCode = '20' FROM territory.Territory T WHERE T.TerritoryID = 8
UPDATE T SET T.TerritoryCode = '19' FROM territory.Territory T WHERE T.TerritoryID = 7
UPDATE T SET T.TerritoryCode = '17' FROM territory.Territory T WHERE T.TerritoryID = 5
UPDATE T SET T.TerritoryCode = '11' FROM territory.Territory T WHERE T.TerritoryID = 1
UPDATE T SET T.TerritoryCode = '25' FROM territory.Territory T WHERE T.TerritoryID = 12
UPDATE T SET T.TerritoryCode = '26' FROM territory.Territory T WHERE T.TerritoryID = 13
UPDATE T SET T.TerritoryCode = '31' FROM territory.Territory T WHERE T.TerritoryID = 18
UPDATE T SET T.TerritoryCode = '22' FROM territory.Territory T WHERE T.TerritoryID = 21
UPDATE T SET T.TerritoryCode = '23' FROM territory.Territory T WHERE T.TerritoryID = 10
UPDATE T SET T.TerritoryCode = '13' FROM territory.Territory T WHERE T.TerritoryID = 19
UPDATE T SET T.TerritoryCode = '21' FROM territory.Territory T WHERE T.TerritoryID = 9
UPDATE T SET T.TerritoryCode = '15' FROM territory.Territory T WHERE T.TerritoryID = 4
GO