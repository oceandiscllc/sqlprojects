USE ELI0002
GO

DECLARE @nAssetID INT
DECLARE @gLocation GEOMETRY

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT
		A.AssetID,
		A.ProjectID
	FROM asset.Asset A
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @nAssetID, @gLocation
WHILE @@fetch_status = 0
	BEGIN

	UPDATE A
	SET A.TerritoryID = ISNULL(
		(
		SELECT TOP 1 
			T.TerritoryID
		FROM ELI0000.territory.Territory T
			JOIN ELI0000.territory.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
				AND T.Location.STIntersects(@gLocation) = 1
		ORDER BY TT.DisplayOrder DESC
		), 0)
	FROM asset.Asset A
	WHERE A.AssetID = @nAssetID

	FETCH oCursor INTO @nAssetID, @gLocation
		
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor
GO
