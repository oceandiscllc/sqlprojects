USE GIZ
GO

--Begin table dropdown.IEGInfluence
DECLARE @TableName VARCHAR(250) = 'dropdown.IEGInfluence'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IEGInfluence
	(
	IEGInfluenceID INT IDENTITY(0,1) NOT NULL,
	IEGInfluenceName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IEGInfluenceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IEGInfluence', 'DisplayOrder,IEGInfluenceName', 'IEGInfluenceID'
GO
--End table dropdown.IEGInfluence

--Begin table dropdown.SituationChange
DECLARE @TableName VARCHAR(250) = 'dropdown.SituationChange'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SituationChange
	(
	SituationChangeID INT IDENTITY(0,1) NOT NULL,
	SituationChangeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SituationChangeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SituationChange', 'DisplayOrder,SituationChangeName', 'SituationChangeID'
GO
--End table dropdown.SituationChange

--Begin table trendreport.TrendReport
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReport'

EXEC utility.AddColumn @TableName, 'EconomicSituation', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IEGInfluenceID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'InternationalResponse', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SecuritySituation', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SituationChangeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'TerritoryID', 'INT', '0'
GO
--End table trendreport.TrendReport

--Begin table trendreport.TrendReportAsset
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportAsset'

EXEC utility.DropObject @TableName
GO
--End table trendreport.TrendReportAsset

--Begin table trendreport.TrendReportIncident
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportIncident'

EXEC utility.DropObject @TableName
GO
--End table trendreport.TrendReportIncident

--Begin table trendreport.TrendReportTerritory
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportTerritory'

EXEC utility.DropObject @TableName
GO
--End table trendreport.TrendReportTerritory
