USE GIZ
GO

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCode,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCode,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCode,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CS.ContactStatusID,
		CS.ContactStatusName
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
			AND C1.ContactID = @ContactID

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	--ContactCourse
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM training.Course C
		JOIN training.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN training.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID

	--ContactEquipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND EI.AssigneeTypeCode = 'Contact'
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND C.ProjectID = EC.ProjectID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure dropdown.GetIEGInfluenceData
EXEC Utility.DropObject 'dropdown.GetIEGInfluenceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.IEGInfluence table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIEGInfluenceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IEGInfluenceID,
		T.IEGInfluenceName,
		T.HexColor
	FROM dropdown.IEGInfluence T
	WHERE (T.IEGInfluenceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IEGInfluenceName, T.IEGInfluenceID

END
GO
--End procedure dropdown.GetIEGInfluenceData

--Begin procedure dropdown.GetSituationChangeData
EXEC Utility.DropObject 'dropdown.GetSituationChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.SituationChange table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSituationChangeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SituationChangeID,
		T.SituationChangeName
	FROM dropdown.SituationChange T
	WHERE (T.SituationChangeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SituationChangeName, T.SituationChangeID

END
GO
--End procedure dropdown.GetSituationChangeData

--Begin procedure eventlog.LogTrendReportAction
EXEC utility.DropObject 'eventlog.LogTrendReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'TrendReport'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cTrendReportForces VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportForces = COALESCE(@cTrendReportForces, '') + D.TrendReportForce 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportForce'), ELEMENTS) AS TrendReportForce
			FROM trendreport.TrendReportForce T 
			WHERE T.TrendReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogTrendReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogTrendReportActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogTrendReportActionTable
		FROM trendreport.TrendReport T
		WHERE T.TrendReportID = @EntityID
		
		ALTER TABLE #LogTrendReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<TrendReportForces>' + ISNULL(@cTrendReportForces, '') + '</TrendReportForces>') AS XML)
			FOR XML RAW('TrendReport'), ELEMENTS
			),
			TR.ProjectID
		FROM #LogTrendReportActionTable T
			JOIN trendreport.TrendReport TR ON TR.TerritoryID = T.TrendReportID

		DROP TABLE #LogTerritoryActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAction

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure trendreport.GetTrendReportByTrendReportID
EXEC Utility.DropObject 'trendreport.GetTrendReportByTrendReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the trendreport.TrendReport table
-- ==================================================================================
CREATE PROCEDURE trendreport.GetTrendReportByTrendReportID

@TrendReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReport', @TrendReportID)

	--TrendReport
	SELECT
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		TR.EconomicSituation,
		TR.InternationalResponse,
		TR.ProjectID,
		dropdown.GetProjectNameByProjectID(TR.ProjectID) AS ProjectName,
		TR.ReportDetail,
		TR.SecuritySituation,
		TR.Summary,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("TR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		TR.SummaryMapZoom,
		TR.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(TR.TerritoryID) AS TerritoryNameFormatted,
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.TrendReportID,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportTitle
	FROM trendreport.TrendReport TR
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TR.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TR.SituationChangeID
			AND TR.TrendReportID = @TrendReportID

	--TrendReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM trendreport.TrendReportForce TRF
		JOIN force.Force F ON F.ForceID = TRF.ForceID
			AND TRF.TrendReportID = @TrendReportID
	ORDER BY F.ForceName, F.ForceID

	--TrendReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM trendreport.TrendReportRelevantTheme TRRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = TRRT.RelevantThemeID
			AND TRRT.TrendReportID = @TrendReportID
	ORDER BY RT.RelevantThemeName, RT.RelevantThemeID

	--TrendReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'TrendReport', @TrendReportID

	--TrendReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN trendreport.TrendReport TR ON TR.TrendReportID = EL.EntityID
			AND TR.TrendReportID = @TrendReportID
			AND EL.EntityTypeCode = 'TrendReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--TrendReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'TrendReport', @TrendReportID, @nWorkflowStepNumber

END
GO
--End procedure trendreport.GetTrendReportByTrendReportID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow