USE GIZ
GO

--Begin table dropdown.ForceType
UPDATE dropdown.ForceType
SET HexColor = '#336600'
WHERE ForceTypeName = 'Local Partners'
GO

UPDATE dropdown.ForceType
SET HexColor = '#FF6600'
WHERE ForceTypeName = 'Other Local Organisations'
GO

UPDATE dropdown.ForceType
SET HexColor = '#FFFF00'
WHERE ForceTypeName = 'International Organisations'
GO
--End table dropdown.ForceType

--Begin table dropdown.IncidentType
UPDATE IT
SET IT.IncidentTypeName = IT.IncidentTypeName + 'n'
FROM dropdown.IncidentType IT
WHERE IT.IncidentTypeName IN ('Airstrike - Coalitio','Airstrike - Russia','Ceasefire/Truce violatio','Confiscatio','Corruptio','Defectio','Detentio','Economic Situatio','Executio','Extortio','Hit and Ru','Illegal Confiscatio','Illegal Detentio''Internet cafe shut dow','Leaflet Distributio','Utility Disruptio')
GO
--End table dropdown.IncidentType

--Begin table dropdown.IEGInfluence
TRUNCATE TABLE dropdown.IEGInfluence
GO

SET IDENTITY_INSERT dropdown.IEGInfluence ON
GO

INSERT INTO dropdown.IEGInfluence 
	(IEGInfluenceID, IEGInfluenceName, HEXColor, DisplayOrder) 
VALUES
	(0, 'Unknown', '#d3d3d3', 1),
	(1, 'IEGS do not compromise activities', '#32cd32', 2),
	(2, 'IEGs have limited influence on activities', '#ffff00', 3),
	(3, 'IEGs compromise activities', '#ff0000', 4)
GO

SET IDENTITY_INSERT dropdown.IEGInfluence OFF
GO
--End table dropdown.IEGInfluence

--Begin table dropdown.SituationChange
TRUNCATE TABLE dropdown.SituationChange
GO

EXEC utility.InsertIdentityValue 'dropdown.SituationChange', 'SituationChangeID', 0
GO

INSERT INTO dropdown.SituationChange 
	(SituationChangeName, DisplayOrder) 
VALUES
	('Situation Improved', 1),
	('No change', 2),
	('Situation Worsened', 3)
GO
--End table dropdown.SituationChange
