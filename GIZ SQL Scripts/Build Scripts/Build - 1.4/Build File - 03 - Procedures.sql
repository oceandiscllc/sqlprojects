USE GIZ
GO

--Begin procedure reporting.GetTerritories
EXEC utility.DropObject 'reporting.GetTerritories'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Inderjeet Kaur
-- Create date:	2018.01.29
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE reporting.GetTerritories

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		AP.ActivityPermittedName,
		IEG.IEGInfluenceName,
		SC.SituationChangeName,
		P.ProjectName,
		T1.TerritoryName,
		T1.TerritoryNameLocal,
		T1.TerritoryTypeCode,
		(SELECT T2.TerritoryName FROM territory.Territory T2 WHERE T2.TerritoryID = T1.ParentTerritoryID) AS ParentTerritoryName,
		TS.TerritoryStatusName
	FROM territory.Territory T1
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T1.TerritoryID
		JOIN dropdown.ActivityPermitted AP ON AP.ActivityPermittedID = TA.ActivityPermittedID
		JOIN dropdown.IEGInfluence IEG ON IEG.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.Project P ON P.ProjectID = TA.ProjectID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID 
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Territory'
			AND SR.EntityID = T1.TerritoryID
			AND SR.PersonID = @PersonID
	ORDER BY P.ProjectName, T1.TerritoryName, T1.TerritoryID

END
GO
--End procedure reporting.GetTerritories

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		AP.ActivityPermittedID,
		AP.ActivityPermittedName,
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Population, 
		T.PopulationSource,
		T.TerritoryName,
		T.TerritoryNameLocal,
		TA.Notes,
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural
	FROM territory.Territory T
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
			AND TA.ProjectID = @ProjectID
			AND TA.IsActive = 1
		JOIN dropdown.ActivityPermitted AP ON AP.ActivityPermittedID = TA.ActivityPermittedID
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--TerritoryAsset
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryForce
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryIncident
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--TerritorySpotReport
	SELECT
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle
	FROM spotreport.SpotReport SR
	WHERE SR.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM spotreport.SpotReportTerritory SRT
			WHERE SRT.SpotReportID = SRT.SpotReportID
			)
		AND person.CanHaveAccess(@PersonID, 'SpotReport', SR.SpotReportID) = 1
	ORDER BY 3, 1

	--TerritoryTrendReport
	SELECT
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportID,
		TR.TrendReportTitle,
		territory.FormatTerritoryNameByTerritoryID(TR.TerritoryID) AS TerritoryNameFormatted
	FROM trendreport.TrendReport TR
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = TR.TerritoryID
			AND TR.ProjectID = @ProjectID
			AND person.CanHaveAccess(@PersonID, 'TrendReport', TR.TrendReportID) = 1
	ORDER BY 3, 1, 4

END
GO
--End procedure territory.GetTerritoryByTerritoryID
