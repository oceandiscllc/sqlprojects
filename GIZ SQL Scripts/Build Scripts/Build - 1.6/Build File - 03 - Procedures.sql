USE GIZ
GO

--Begin procedure person.GetPersonUsernameByEmailAddress
EXEC utility.DropObject 'person.GetPersonUsernameByEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.04.07
-- Description:	A stored procedure to return a username from the person.Person table based on an email address
-- ===========================================================================================================
CREATE PROCEDURE person.GetPersonUsernameByEmailAddress

@EmailAddress VARCHAR(320)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.UserName,
		P.EmailAddress
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

END
GO
--End procedure person.GetPersonUsernameByEmailAddress