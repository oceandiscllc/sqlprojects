USE GIZ
GO

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = '')
	BEGIN

	INSERT INTO dropdown.DocumentType 
		(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName) 
	VALUES
		('Resource Documents', 'InitialRiskAssessment', 'Initial Risk Assessment'),
		('Resource Documents', 'SpotReport', 'Spot Report'),
		('Resource Documents', 'StatusReport', 'Status Report')

	END
--ENDIF
GO
--End table dropdown.DocumentType
