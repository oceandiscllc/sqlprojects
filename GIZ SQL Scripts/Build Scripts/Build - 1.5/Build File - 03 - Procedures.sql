USE GIZ
GO

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName,

		CASE
			WHEN ET.EntityTypeCode = 'Territory'
			THEN territory.FormatTerritoryNameByTerritoryID(DE.EntityID)
			ELSE NULL
		END AS TerritoryNameFormatted

	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		AP.ActivityPermittedID,
		AP.ActivityPermittedName,
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Population, 
		T.PopulationSource,
		T.TerritoryName,
		T.TerritoryNameLocal,
		TA.Notes,
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural
	FROM territory.Territory T
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
			AND TA.ProjectID = @ProjectID
			AND TA.IsActive = 1
		JOIN dropdown.ActivityPermitted AP ON AP.ActivityPermittedID = TA.ActivityPermittedID
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--TerritoryAsset
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryForce
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryIncident
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

	--TerritorySpotReport
	SELECT
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle
	FROM spotreport.SpotReport SR
	WHERE SR.ProjectID = @ProjectID
		AND EXISTS
			(
			SELECT 1
			FROM spotreport.SpotReportTerritory SRT
			WHERE SRT.SpotReportID = SR.SpotReportID
				AND SRT.TerritoryID = @TerritoryID
			)
		AND person.CanHaveAccess(@PersonID, 'SpotReport', SR.SpotReportID) = 1
	ORDER BY 3, 1

	--TerritoryTrendReport
	SELECT
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportID,
		TR.TrendReportTitle,
		territory.FormatTerritoryNameByTerritoryID(TR.TerritoryID) AS TerritoryNameFormatted
	FROM trendreport.TrendReport TR
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = TR.TerritoryID
			AND TR.ProjectID = @ProjectID
			AND person.CanHaveAccess(@PersonID, 'TrendReport', TR.TrendReportID) = 1
	ORDER BY 3, 1, 4

END
GO
--End procedure territory.GetTerritoryByTerritoryID
