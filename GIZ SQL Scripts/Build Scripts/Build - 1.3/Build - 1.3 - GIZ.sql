-- File Name:	Build - 1.3 - GIZ.sql
-- Build Key:	Build - 1.3 - 2018.01.15 17.06.29

USE GIZ
GO

-- ==============================================================================================================================
-- Tables:
--		dropdown.ActivityPermitted
--		reporting.SearchResult
--
-- Functions:
--		dbo.GetSpotReportTerritoriesList
--		reporting.IsDraftReport
--
-- Procedures:
--		aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
--		dropdown.GetActivityPermittedData
--		person.GeneratePassword
--		reporting.GetSpotReportBySpotReportID
--		territory.GetTerritoryByTerritoryID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE GIZ
GO

--Begin table aggregator.TrendReportAggregator
DECLARE @TableName VARCHAR(250) = 'aggregator.TrendReportAggregator'

EXEC utility.AddColumn @TableName, 'Highlights', 'NVARCHAR(MAX)'
GO
--End table aggregator.TrendReportAggregator

--Begin table dropdown.DocumentType
ALTER TABLE dropdown.DocumentType ALTER COLUMN DocumentTypeCategory VARCHAR(250)
ALTER TABLE dropdown.DocumentType ALTER COLUMN DocumentTypeName VARCHAR(250)
GO
--End table dropdown.DocumentType

--Begin table dropdown.ActivityPermitted
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityPermitted'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityPermitted
	(
	ActivityPermittedID INT IDENTITY(0,1) NOT NULL,
	ActivityPermittedName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityPermittedID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ActivityPermitted', 'DisplayOrder,ActivityPermittedName', 'ActivityPermittedID'
GO
--End table dropdown.ActivityPermitted

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchResult
	(
	SearchResultID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeGroupCode VARCHAR(50),
	EntityData VARCHAR(MAX),
	EntityID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchResultID'
EXEC utility.SetIndexClustered @TableName, 'IX_SearchResult', 'EntityTypeCode,EntityTypeGroupCode,EntityID,PersonID'
GO
--End table reporting.SearchResult

--Begin table territory.TerritoryAnnex
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryAnnex'

EXEC utility.AddColumn @TableName, 'ActivityPermittedID', 'INT', '0'
GO
--End table territory.TerritoryAnnex


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE GIZ
GO

--Begin function dbo.GetSpotReportTerritoriesList
EXEC utility.DropObject 'dbo.GetSpotReportTerritoriesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:			John Lyons
-- Create date:	2018.01.15
-- Description:	A function to return territories as a list 
-- =======================================================
CREATE FUNCTION dbo.GetSpotReportTerritoriesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @TerritoryList VARCHAR(MAX) = ''
	
	SELECT @TerritoryList += territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) + ', '
	FROM core.Incident I
		JOIN spotreport.SpotReportIncident SRI ON SRI.IncidentID = I.IncidentID 
			AND	SRI.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@TerritoryList)) > 0 
		SET @TerritoryList = RTRIM(SUBSTRING(@TerritoryList, 0, LEN(@TerritoryList)))
	--ENDIF

	RETURN RTRIM(@TerritoryList)

END
GO
--End function dbo.GetSpotReportTerritoriesList

--Begin function reporting.IsDraftReport
EXEC utility.DropObject 'reporting.IsDraftReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION reporting.IsDraftReport
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nIsDraft BIT = 1
	
	IF @EntityTypeCode = 'SpotReport'
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)) = 1 THEN 1 ELSE 0 END 
	ELSE IF @EntityTypeCode = 'WeeklyReport'
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)) = 1 THEN 1 ELSE 0 END
	--ENDIF
	
	RETURN @nIsDraft

END
GO
--End function reporting.IsDraftReport


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE GIZ
GO

--Begin procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
EXEC utility.DropObject 'aggregator.GetTrendReportAggregatorByTrendReportAggregatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.TrendReportAggregator table
--
-- Author:			Todd Pires
-- Create date:	2017.11.23
-- Description:	Added hte Highlights field
-- ===========================================================================================
CREATE PROCEDURE aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

@TrendReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReportAggregator', @TrendReportAggregatorID)
	
	SELECT
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Highlights,
		TR.TrendReportAggregatorID,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportTitle,
		TR.ReportDetail,
		TR.Summary
	FROM aggregator.TrendReportAggregator TR
	WHERE TR.TrendReportAggregatorID = @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'TrendReportAggregator', @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowPeople 'TrendReportAggregator', @TrendReportAggregatorID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Situational Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Situational Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Situational Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Situational Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.TrendReportAggregator TR ON TR.TrendReportAggregatorID = EL.EntityID
			AND TR.TrendReportAggregatorID = @TrendReportAggregatorID
			AND EL.EntityTypeCode = 'TrendReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

--Begin procedure dropdown.GetActivityPermittedData
EXEC Utility.DropObject 'dropdown.GetActivityPermittedData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.10
-- Description:	A stored procedure to return data from the dropdown.ActivityPermitted table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetActivityPermittedData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityPermittedID,
		T.ActivityPermittedName
	FROM dropdown.ActivityPermitted T
	WHERE (T.ActivityPermittedID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityPermittedName, T.ActivityPermittedID

END
GO
--End procedure dropdown.GetActivityPermittedData

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.SourceDetails,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID ) AS WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		SR.SummaryMap AS GMap,
		dbo.GetSpotReportTerritoriesList(SR.SpotReportID) AS TerritoryList	
	FROM spotReport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure reporting.GetSpotReportBySpotReportID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		AP.ActivityPermittedID,
		AP.ActivityPermittedName,
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Population, 
		T.PopulationSource,
		T.TerritoryName,
		T.TerritoryNameLocal,
		TA.Notes,
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural
	FROM territory.Territory T
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
			AND TA.ProjectID = @ProjectID
			AND TA.IsActive = 1
		JOIN dropdown.ActivityPermitted AP ON AP.ActivityPermittedID = TA.ActivityPermittedID
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--TerritoryAsset
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryForce
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryIncident
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

END
GO
--End procedure territory.GetTerritoryByTerritoryID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE GIZ
GO

--Begin table dropdown.ActivityPermitted
TRUNCATE TABLE dropdown.ActivityPermitted
GO

EXEC utility.InsertIdentityValue 'dropdown.ActivityPermitted', 'ActivityPermittedID', 0
GO

INSERT INTO dropdown.ActivityPermitted 
	(ActivityPermittedName) 
VALUES
	('Not Permitted'),
	('On Hold'),
	('Permitted')
GO
--End table dropdown.ActivityPermitted

--Begin table dropdown.DocumentType
DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'Document.View.%'
GO

DELETE PP
FROM person.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

TRUNCATE TABLE dropdown.DocumentType
GO

EXEC utility.InsertIdentityValue 'dropdown.DocumentType', 'DocumentTypeID', 0
GO

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName) 
VALUES
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'AleppoSecurity', 'Aleppo Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'HamaSecurity', 'Hama Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'HomsSecurity', 'Homs Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'IdlebSecurity', 'Idleb Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'Supplemental', 'Supplemental Reporting Documentation'),
	('Resource Documents', 'Informational', 'Informational Resource Documents'),
	('Resource Documents', 'USTSReporting', 'USTS Reporting Locations List')
GO
--End table dropdown.DocumentType

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName) 
VALUES
	('Agriculture'),
	('Education'),
	('Food Security and Livelihoods'),
	('Local Consultation Processes'),
	('Vocational Training')
GO
--End table dropdown.RelevantTheme

--Begin table trendreport.TrendReportRelevantTheme
TRUNCATE TABLE trendreport.TrendReportRelevantTheme
GO
--End table trendreport.TrendReportRelevantTheme

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Edit the activity permitted field', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the activity permitted field', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table dropdown.ActivityPermitted

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Groups & Assets', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Insight', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Edit the activity permitted field', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the activity permitted field', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type aleppo security, access, and atmospherics monthly report in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.AleppoSecurity', @PERMISSIONCODE='AleppoSecurity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type hama security, access, and atmospherics monthly report in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HamaSecurity', @PERMISSIONCODE='HamaSecurity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type homs security, access, and atmospherics monthly report in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HomsSecurity', @PERMISSIONCODE='HomsSecurity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type idleb security, access, and atmospherics monthly report in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.IdlebSecurity', @PERMISSIONCODE='IdlebSecurity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type informational resource documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Informational', @PERMISSIONCODE='Informational';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type supplemental reporting documentation in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Supplemental', @PERMISSIONCODE='Supplemental';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type usts reporting locations list in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.USTSReporting', @PERMISSIONCODE='USTSReporting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of groups', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalog item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalog item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.3 - 2018.01.15 17.06.29')
GO
--End build tracking

