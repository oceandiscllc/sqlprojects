USE GIZ
GO

--Begin function dbo.GetSpotReportTerritoriesList
EXEC utility.DropObject 'dbo.GetSpotReportTerritoriesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:			John Lyons
-- Create date:	2018.01.15
-- Description:	A function to return territories as a list 
-- =======================================================
CREATE FUNCTION dbo.GetSpotReportTerritoriesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @TerritoryList VARCHAR(MAX) = ''
	
	SELECT @TerritoryList += territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) + ', '
	FROM core.Incident I
		JOIN spotreport.SpotReportIncident SRI ON SRI.IncidentID = I.IncidentID 
			AND	SRI.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@TerritoryList)) > 0 
		SET @TerritoryList = RTRIM(SUBSTRING(@TerritoryList, 0, LEN(@TerritoryList)))
	--ENDIF

	RETURN RTRIM(@TerritoryList)

END
GO
--End function dbo.GetSpotReportTerritoriesList

--Begin function reporting.IsDraftReport
EXEC utility.DropObject 'reporting.IsDraftReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION reporting.IsDraftReport
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nIsDraft BIT = 1
	
	IF @EntityTypeCode = 'SpotReport'
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)) = 1 THEN 1 ELSE 0 END 
	ELSE IF @EntityTypeCode = 'WeeklyReport'
		SELECT @nIsDraft = CASE WHEN (SELECT workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)) = 1 THEN 1 ELSE 0 END
	--ENDIF
	
	RETURN @nIsDraft

END
GO
--End function reporting.IsDraftReport

