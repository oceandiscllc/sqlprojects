USE GIZ
GO

--Begin table dropdown.ActivityPermitted
TRUNCATE TABLE dropdown.ActivityPermitted
GO

EXEC utility.InsertIdentityValue 'dropdown.ActivityPermitted', 'ActivityPermittedID', 0
GO

INSERT INTO dropdown.ActivityPermitted 
	(ActivityPermittedName) 
VALUES
	('Not Permitted'),
	('On Hold'),
	('Permitted')
GO
--End table dropdown.ActivityPermitted

--Begin table dropdown.DocumentType
DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'Document.View.%'
GO

DELETE PP
FROM person.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

TRUNCATE TABLE dropdown.DocumentType
GO

EXEC utility.InsertIdentityValue 'dropdown.DocumentType', 'DocumentTypeID', 0
GO

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName) 
VALUES
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'AleppoSecurity', 'Aleppo Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'HamaSecurity', 'Hama Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'HomsSecurity', 'Homs Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'IdlebSecurity', 'Idleb Security, Access, and Atmospherics Monthly Report'),
	('DT Security, Access, and Atmospherics Monthly Report - Syria', 'Supplemental', 'Supplemental Reporting Documentation'),
	('Resource Documents', 'Informational', 'Informational Resource Documents'),
	('Resource Documents', 'USTSReporting', 'USTS Reporting Locations List')
GO
--End table dropdown.DocumentType

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName) 
VALUES
	('Agriculture'),
	('Education'),
	('Food Security and Livelihoods'),
	('Local Consultation Processes'),
	('Vocational Training')
GO
--End table dropdown.RelevantTheme

--Begin table trendreport.TrendReportRelevantTheme
TRUNCATE TABLE trendreport.TrendReportRelevantTheme
GO
--End table trendreport.TrendReportRelevantTheme

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Edit the activity permitted field', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the activity permitted field', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View.ActivityPermitted', @PERMISSIONCODE='ActivityPermitted';

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table dropdown.ActivityPermitted
