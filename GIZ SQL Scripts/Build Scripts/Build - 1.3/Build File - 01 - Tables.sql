USE GIZ
GO

--Begin table aggregator.TrendReportAggregator
DECLARE @TableName VARCHAR(250) = 'aggregator.TrendReportAggregator'

EXEC utility.AddColumn @TableName, 'Highlights', 'NVARCHAR(MAX)'
GO
--End table aggregator.TrendReportAggregator

--Begin table dropdown.DocumentType
ALTER TABLE dropdown.DocumentType ALTER COLUMN DocumentTypeCategory VARCHAR(250)
ALTER TABLE dropdown.DocumentType ALTER COLUMN DocumentTypeName VARCHAR(250)
GO
--End table dropdown.DocumentType

--Begin table dropdown.ActivityPermitted
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityPermitted'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityPermitted
	(
	ActivityPermittedID INT IDENTITY(0,1) NOT NULL,
	ActivityPermittedName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityPermittedID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ActivityPermitted', 'DisplayOrder,ActivityPermittedName', 'ActivityPermittedID'
GO
--End table dropdown.ActivityPermitted

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchResult
	(
	SearchResultID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeGroupCode VARCHAR(50),
	EntityData VARCHAR(MAX),
	EntityID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchResultID'
EXEC utility.SetIndexClustered @TableName, 'IX_SearchResult', 'EntityTypeCode,EntityTypeGroupCode,EntityID,PersonID'
GO
--End table reporting.SearchResult

--Begin table territory.TerritoryAnnex
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryAnnex'

EXEC utility.AddColumn @TableName, 'ActivityPermittedID', 'INT', '0'
GO
--End table territory.TerritoryAnnex

