USE GIZ
GO

--Begin procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID
EXEC utility.DropObject 'aggregator.GetTrendReportAggregatorByTrendReportAggregatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the aggregator.TrendReportAggregator table
--
-- Author:			Todd Pires
-- Create date:	2017.11.23
-- Description:	Added hte Highlights field
-- ===========================================================================================
CREATE PROCEDURE aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

@TrendReportAggregatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReportAggregator', @TrendReportAggregatorID)
	
	SELECT
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Highlights,
		TR.TrendReportAggregatorID,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportTitle,
		TR.ReportDetail,
		TR.Summary
	FROM aggregator.TrendReportAggregator TR
	WHERE TR.TrendReportAggregatorID = @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowData 'TrendReportAggregator', @TrendReportAggregatorID

	EXEC workflow.GetEntityWorkflowPeople 'TrendReportAggregator', @TrendReportAggregatorID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Situational Report Aggregation'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Situational Report Aggregation'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Situational Report Aggregation'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Situational Report Aggregation'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN aggregator.TrendReportAggregator TR ON TR.TrendReportAggregatorID = EL.EntityID
			AND TR.TrendReportAggregatorID = @TrendReportAggregatorID
			AND EL.EntityTypeCode = 'TrendReportAggregator'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure aggregator.GetTrendReportAggregatorByTrendReportAggregatorID

--Begin procedure dropdown.GetActivityPermittedData
EXEC Utility.DropObject 'dropdown.GetActivityPermittedData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.10
-- Description:	A stored procedure to return data from the dropdown.ActivityPermitted table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetActivityPermittedData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityPermittedID,
		T.ActivityPermittedName
	FROM dropdown.ActivityPermitted T
	WHERE (T.ActivityPermittedID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityPermittedName, T.ActivityPermittedID

END
GO
--End procedure dropdown.GetActivityPermittedData

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Justin Branum
-- Create date:	2016.02.27
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.SourceDetails,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID ) AS WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		SR.SummaryMap AS GMap,
		dbo.GetSpotReportTerritoriesList(SR.SpotReportID) AS TerritoryList	
	FROM spotReport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure reporting.GetSpotReportBySpotReportID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		AP.ActivityPermittedID,
		AP.ActivityPermittedName,
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Population, 
		T.PopulationSource,
		T.TerritoryName,
		T.TerritoryNameLocal,
		TA.Notes,
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural
	FROM territory.Territory T
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
			AND TA.ProjectID = @ProjectID
			AND TA.IsActive = 1
		JOIN dropdown.ActivityPermitted AP ON AP.ActivityPermittedID = TA.ActivityPermittedID
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--TerritoryAsset
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryForce
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryIncident
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

END
GO
--End procedure territory.GetTerritoryByTerritoryID
