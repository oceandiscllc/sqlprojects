USE GIZ
GO

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
 BEGIN

	 DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'add'
			THEN 'Add'
			WHEN @EventCode = 'read'
			THEN 'Read'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'IncrementWorkflow'
			THEN 'Increment Workflow'
			WHEN @EventCode = 'addupdate'
			THEN 'Update'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'view'
			THEN 'View'
			ELSE @EventCodeName
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO
--End function person.HashPassword


