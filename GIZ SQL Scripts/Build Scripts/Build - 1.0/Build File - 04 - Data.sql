﻿/* Build File - 04 - Data */
USE GIZ
GO

--Begin table core.EmailTemplate 
INSERT core.EmailTemplate 
	(EntityTypeCode, EmailTemplateCode, WorkflowActionCode, EmailSubject, EmailText) 
VALUES 
	('DashboardData', NULL, N'RFAI', N'A Request for Additional Information Has Been Made', N'<p>A request for additional information regarding a dashboard map item has been made by [[FullName]]</p><p><strong>Item:</strong> [[EntityTypeName]]</p><p><strong>Title:</strong> [[Title]]</p><p><strong>ID:</strong> [[EntityID]]</p><p>You are receiving this email notification because you have been assigned to recieve these requests.<br />The item is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the LEO Knowledge Managment System.<br /><br />Thank you,</p>'),
	('RequestForInformation', NULL, N'Amend', N'A Previously Completed Response To A Request For Information Response Has Been Amended', N'<p>An Amended response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p><p>&nbsp;</p>'),
	('RequestForInformation', NULL, N'Complete', N'A Response To A Request For Information Has Been Completed', N'<p>A response is hereby provided to the following request for information recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p><p>Summary Answer: [[SummaryAnswer]]</p><br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,<p>&nbsp;</p>'),
	('RequestForInformation', NULL, N'PointOfContactUpdate', N'RFI Assigned for Action', N'<p>Dear [[FullName]] <br /><br />RfI [[Title]] has been assigned to you for action. The deadline for a response is [[IncidentDateFormatted]]. The RfI request is for [[SummaryAnswer]] and can be viewed here [[TitleLink]] <br /><br />Many thanks<br />[[FullName]]</p>'),
	('RequestForInformation', NULL, N'Submit', N'A Request For Information Has Been Submitted', N'<p>A request for information has been recieved from: [[FullName]]</p><p>Date: [[IncidentDateFormatted]]</p><p>Territory: [[TerritoryName]]</p><p>Incident/Event: [[Title]]</p><p>Known details of event: [[KnownDetails]]</p><p>Information Requested: [[InformationRequested]]</p>'),
	('SpotReport', NULL, N'DecrementWorkflow', N'A Spot Report Has Been Disapproved', N'<p>A previously submitted activity report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>'),
	('SpotReport', NULL, N'IncrementWorkflow', N'A Spot Report Is Ready For Your Review', N'<p>A spot report has been submitted for your review:</p><p><strong>Spot Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the spot reports workflow. Please log in and click the link above to review this spot report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>'),
	('SpotReport', NULL, N'Release', N'A Spot Report Has Been Released', N'<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>'),
	('TrendReport', NULL, N'DecrementWorkflow', N'A Trend Report Has Been Disapproved', N'<p>A previously submitted trend report has been disapproved</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>'),
	('TrendReport', NULL, N'IncrementWorkflow', N'A Trend Report Is Ready For Your Review', N'<p>A trend report has been submitted for your review:</p><p><strong>Trend Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[Comments]]</p><p>You are receiving this email notification from the system because you have been assigned as a member of the trend reports workflow. Please log in and click the link above to review this trend report.</p><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you,</p>'),
	('TrendReport', NULL, N'Release', N'A Trend Report Has Been Released', N'<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Portal.</p><p>Please do not reply to this email as it is generated automatically by the system.</p><p>Thank you,</p>')
GO
--End table core.EmailTemplate 

--Begin table core.EmailTemplateField
INSERT core.EmailTemplateField 
	(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder) 
VALUES 
	('DashboardData', '[[EntityID]]', 'Item ID Number', 0),
	('DashboardData', '[[EntityTypeName]]', 'Item Type Name', 0),
	('DashboardData', '[[FullName]]', 'Requested By', 0),
	('DashboardData', '[[Title]]', 'Title', 0),
	('DashboardData', '[[TitleLink]]', 'Title Link', 0),
	('RequestForInformation', '[[FullName]]', 'Requested By', 0),
	('RequestForInformation', '[[IncidentDateFormatted]]', 'Incident Date', 0),
	('RequestForInformation', '[[InformationRequested]]', 'Information Requested', 0),
	('RequestForInformation', '[[KnownDetails]]', 'Details', 0),
	('RequestForInformation', '[[SummaryAnswer]]', 'Response', 0),
	('RequestForInformation', '[[TerritoryName]]', 'Territory', 0),
	('RequestForInformation', '[[Title]]', 'Title', 0),
	('RequestForInformation', '[[TitleLink]]', 'Title Link', 0),
	('SpotReport', '[[Comments]]', 'Comments', 0),
	('SpotReport', '[[Title]]', 'Title', 0),
	('SpotReport', '[[TitleLink]]', 'Title Link', 0),
	('TrendReport', '[[Comments]]', 'Comments', 0),
	('TrendReport', '[[Title]]', 'Title', 0),
	('TrendReport', '[[TitleLink]]', 'Title Link', 0)
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Asset', 
	@EntityTypeName = 'Asset', 
	@EntityTypeNamePlural = 'Assets',
	@SchemaName = 'asset', 
	@TableName = 'Asset', 
	@PrimaryKeyFieldName = 'AssetID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Contact', 
	@EntityTypeName = 'Contact', 
	@EntityTypeNamePlural = 'Contacts',
	@SchemaName = 'contact', 
	@TableName = 'Contact', 
	@PrimaryKeyFieldName = 'ContactID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Course', 
	@EntityTypeName = 'Course', 
	@EntityTypeNamePlural = 'Courses',
	@SchemaName = 'training', 
	@TableName = 'Course', 
	@PrimaryKeyFieldName = 'CourseID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Document', 
	@EntityTypeName = 'Document', 
	@EntityTypeNamePlural = 'Documents',
	@SchemaName = 'document', 
	@TableName = 'Document', 
	@PrimaryKeyFieldName = 'DocumentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EmailTemplate', 
	@EntityTypeName = 'Email Template', 
	@EntityTypeNamePlural = 'Email Templates',
	@SchemaName = 'core', 
	@TableName = 'EmailTemplate', 
	@PrimaryKeyFieldName = 'EmailTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EquipmentCatalog', 
	@EntityTypeName = 'Equipment Catalog', 
	@EntityTypeNamePlural = 'Equipment Catalog',
	@SchemaName = 'procurement', 
	@TableName = 'EquipmentCatalog', 
	@PrimaryKeyFieldName = 'EquipmentCatalogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EquipmentInventory', 
	@EntityTypeName = 'Equipment Inventory', 
	@EntityTypeNamePlural = 'Equipment Inventory',
	@SchemaName = 'procurement', 
	@TableName = 'EquipmentInventory', 
	@PrimaryKeyFieldName = 'EquipmentInventoryID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'core', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Force', 
	@EntityTypeName = 'Group', 
	@EntityTypeNamePlural = 'Groups',
	@SchemaName = 'force', 
	@TableName = 'Force', 
	@PrimaryKeyFieldName = 'ForceID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Incident', 
	@EntityTypeName = 'Incident', 
	@EntityTypeNamePlural = 'Incidents',
	@SchemaName = 'core', 
	@TableName = 'Incident', 
	@PrimaryKeyFieldName = 'IncidentID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Module', 
	@EntityTypeName = 'Module', 
	@EntityTypeNamePlural = 'Modules',
	@SchemaName = 'training', 
	@TableName = 'Module', 
	@PrimaryKeyFieldName = 'ModuleID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Permissionable', 
	@EntityTypeName = 'Permissionable', 
	@EntityTypeNamePlural = 'Permissionables',
	@SchemaName = 'permissionable', 
	@TableName = 'Permissionable', 
	@PrimaryKeyFieldName = 'PermissionableID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PermissionableTemplate', 
	@EntityTypeName = 'Permissionable Template', 
	@EntityTypeNamePlural = 'Permissionable Templates',
	@SchemaName = 'permissionable', 
	@TableName = 'PermissionableTemplate', 
	@PrimaryKeyFieldName = 'PermissionableTemplateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'User', 
	@EntityTypeNamePlural = 'Users',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'RequestForInformation', 
	@EntityTypeName = 'Request For Information', 
	@EntityTypeNamePlural = 'Requests For Information',
	@SchemaName = 'core', 
	@TableName = 'RequestForInformation', 
	@PrimaryKeyFieldName = 'RequestForInformationID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SpotReport', 
	@EntityTypeName = 'Spot Report', 
	@EntityTypeNamePlural = 'Spot Reports',
	@HasWorkflow = 1,
	@SchemaName = 'spotreport', 
	@TableName = 'SpotReport', 
	@PrimaryKeyFieldName = 'SpotReportID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Territory', 
	@EntityTypeName = 'Territory', 
	@EntityTypeNamePlural = 'Territories',
	@SchemaName = 'territory', 
	@TableName = 'Territory', 
	@PrimaryKeyFieldName = 'TerritoryID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'TrendReport', 
	@EntityTypeName = 'Situational Report', 
	@EntityTypeNamePlural = 'Situational Reports',
	@HasWorkflow = 1,
	@SchemaName = 'trendreport', 
	@TableName = 'TrendReport', 
	@PrimaryKeyFieldName = 'TrendReportID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'TrendReportAggregator', 
	@EntityTypeName = 'Situational Report Aggregator', 
	@EntityTypeNamePlural = 'Situational Reports Aggregator',
	@HasWorkflow = 1,
	@SchemaName = 'aggregator', 
	@TableName = 'TrendReportAggregator', 
	@PrimaryKeyFieldName = 'TrendReportAggregatorID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Workflow', 
	@EntityTypeName = 'Workflow', 
	@EntityTypeNamePlural = 'Workflows',
	@SchemaName = 'workflow', 
	@TableName = 'Workflow', 
	@PrimaryKeyFieldName = 'WorkflowID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-folder',
	@NewMenuItemCode = 'DocumentList',
	@NewMenuItemLink = '/document/list',
	@NewMenuItemText = 'Reference Library',
	@PermissionableLineageList = 'Document.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'DocumentList',
	@Icon = 'fa fa-fw fa-map',
	@NewMenuItemCode = 'TerritoryList',
	@NewMenuItemLink = '/territory/list',
	@NewMenuItemText = 'Territories',
	@PermissionableLineageList = 'Territory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TerritoryList',
	@Icon = 'fa fa-fw fa-question',
	@NewMenuItemCode = 'RequestForInformationList',
	@NewMenuItemLink = '/requestforinformation/list',
	@NewMenuItemText = 'Requests For Information',
	@PermissionableLineageList = 'RequestForInformation.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'RequestForInformationList',
	@Icon = 'fa fa-fw fa-flag',
	@NewMenuItemCode = 'ForceAsset',
	@NewMenuItemText = 'Groups & Assets'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ForceList',
	@NewMenuItemLink = '/force/list',
	@NewMenuItemText = 'Groups',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Force.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceList',
	@NewMenuItemCode = 'AssetList',
	@NewMenuItemLink = '/asset/list',
	@NewMenuItemText = 'Assets',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Asset.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceAsset',
	@Icon = 'fa fa-fw fa-briefcase',
	@NewMenuItemCode = 'Insight',
	@NewMenuItemText = 'Insight'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'IncidentList',
	@NewMenuItemLink = '/incident/list',
	@NewMenuItemText = 'Incidents',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'Incident.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'IncidentList',
	@NewMenuItemCode = 'SpotReportList',
	@NewMenuItemLink = '/spotreport/list',
	@NewMenuItemText = 'Spot Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'SpotReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SpotReportList',
	@NewMenuItemCode = 'TrendReportList',
	@NewMenuItemLink = '/trendreport/list',
	@NewMenuItemText = 'Situational Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TrendReportList',
	@NewMenuItemCode = 'TrendReportAggregatorList',
	@NewMenuItemLink = '/trendreportaggregator/list',
	@NewMenuItemText = 'Situational Report Aggregator',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReportAggregator.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Insight',
	@Icon = 'fa fa-fw fa-bars',
	@NewMenuItemCode = 'Activity',
	@NewMenuItemText = 'Activity'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Equipment',
	@NewMenuItemText = 'Equipment'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemLink = '/equipmentcatalog/list',
	@NewMenuItemText = 'Equipment Catalog',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentCatalog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemCode = 'EquipmentInventoryList',
	@NewMenuItemLink = '/equipmentinventory/list',
	@NewMenuItemText = 'Equipment Inventory',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentInventory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Equipment',
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Training',
	@NewMenuItemText = 'Training'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ModuleList',
	@NewMenuItemLink = '/Module/list',
	@NewMenuItemText = 'Modules',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Module.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ModuleList',
	@NewMenuItemCode = 'CourseList',
	@NewMenuItemLink = '/Course/list',
	@NewMenuItemText = 'Courses',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Course.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Training',
	@NewMenuItemCode = 'ContactList',
	@NewMenuItemLink = '/contact/list',
	@NewMenuItemText = 'Contacts',
	@ParentMenuItemCode = 'Activity',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Activity',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SystemSetupList',
	@NewMenuItemCode = 'WorkflowList',
	@NewMenuItemLink = '/workflow/list',
	@NewMenuItemText = 'Workflows',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Workflow.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Activity'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'ForceAsset'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationKey', NULL, 'DIUZQ1ICXUIMHKKK6TCX'
EXEC core.SystemSetupAddUpdate 'DuoAdminIntegrationSecretKey', NULL, 'MylcNoqlXVduD3LBn981ZHRY7OGOcKjsZ8LDTOTH'
EXEC core.SystemSetupAddUpdate 'DuoApiEndPoint', NULL, 'api-8a6e671f.duosecurity.com'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationKey', NULL, 'DIT96UH6EI8OVAUXJVT2'
EXEC core.SystemSetupAddUpdate 'DuoAuthIntegrationSecretKey', NULL, 'q7NjG8UbJzEMEDKNTH9m3FFBTSkEmAxUodib025p'
EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,john.lyons@oceandisc.com,kevin.ross@oceandisc.com'
EXEC core.SystemSetupAddUpdate 'GMapClientID', 'The Google Maps API Key', 'AIzaSyCq7wqiKnE1dBkm0z9oGK46tucTyM9ewJk'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@giz.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'PasswordDuration', NULL, '30'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Print', NULL, '/assets/img/giz-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Regular', NULL, '/assets/img/giz-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteLogo-Small', NULL, '/assets/img/giz-logo-regular.png'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'https://giz.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'GIZ'
EXEC core.SystemSetupAddUpdate 'TwoFactorEnabled', NULL, '0'
GO
--End table core.SystemSetup

--Begin table document.FileType
TRUNCATE TABLE document.FileType
GO

INSERT INTO document.FileType
	(Extension, MimeType)
VALUES
	('.doc', 'application/msword'),
	('.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('.gif', 'image/gif'),
	('.htm', 'text/html'),
	('.html', 'text/html'),
	('.jpeg', 'image/jpeg'),
	('.jpg', 'image/jpeg'),
	('.pdf', 'application/pdf'),
	('.png', 'image/png'),
	('.pps', 'application/mspowerpoint'),
	('.ppt', 'application/mspowerpoint'),
	('.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
	('.rtf', 'application/rtf'),
	('.txt', 'text/plain'),
	('.xls', 'application/excel'),
	('.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
GO
--End table document.FileType

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Groups & Assets', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Insight', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalog item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a group', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of groups', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a group', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';

EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='trendreport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
GO

EXEC document.UpdateDocumentPermissions
GO
--End table permissionable.Permissionable

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person 
	(FirstName, LastName, Title, UserName, EmailAddress, RoleID, IsSuperAdministrator, DefaultProjectID, Password, PasswordSalt, PasswordExpirationDateTime) 
VALUES
	('Nameer', 'Al-Hadithi', 'Mr.', 'Nameer', 'nalhadithi@skotkonung.com', 1, 1, 1, '214676C4D305E151B0FB4FA87637F0C356750B288C6DA3FE7259CDCDFEE2E709', '500918F4-F98F-4DEC-80FB-1087E993A021', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Burnhan', 'Mr.', 'jburnham', 'jonathanburnham@gmail.com', 1, 1, 1, 'EB065F0E9CFE4AA399624ADFEF14427FA39070F25A92B0DFF210D0F13C11B46F', 'AA47A377-16D5-4319-9C92-EEB2C67C05E5', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Jonathan', 'Cole', 'Mr.', 'JCole', 'jcole@skotkonung.com', 1, 1, 1, '67589DA2DA1F9EFEB1B78C407E1A66B4CDE8577D259BBAA420FB17C798B6D3C5', 'D2A94546-77A0-448B-B754-E78A6C458CC9', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Nicholas', 'Crawford', 'Mr.', 'NickC', 'ncrawford@skotkonung.com', 1, 1, 1, '964E87C60649F8319B247984905E049EA9F43D4E9D185B8297114BCEEAA6FF72', '4BAF7689-6653-429F-B15D-6852A7492E16', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Christopher', 'Crouch', 'Mr.', 'christopher.crouch', 'stchris2opher@gmail.com', 1, 1, 1, '2EB50FF5E58F8A4A1575C35A0FFDB0863438B678EF46BD0B75BD444E80B8540A', '6F079855-A34A-44AD-9C04-1556195B85BE', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Brandon', 'Green', 'Mr.', 'bgreen', 'bjgreen10@gmail.com', 1, 1, 1, 'E2DC6D23743CAE5A998F2DD05E7B2CA2EA7BFEB5680E4A10F4ED0FE0252CAF49', '7586520A-C89D-47BA-822C-336A4F2525C8', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Eric', 'Jones', 'Mr.', 'eric.jones', 'gigado@gmail.com', 1, 1, 1, '4527C0E43DECE243722F9E9B6D3D5C9165EAEEF859F5CC1E012A2615BE7124A8', '91779E4B-C5F5-4574-A6EF-F4ADF1834BE1', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('John', 'Lyons', 'Mr.', 'jlyons', 'john.lyons@oceandisc.com', 1, 1, 1, '138A8BBF15BB51E9FF313431819314DA9150A96B92D8C57FCFDA4965DBFDB748', '71430ED2-DE2B-4FB5-9C1B-0AE9A831388D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Usamah', 'Mahmood', 'Mr.', 'usamah', 'usamah852@gmail.com', 1, 1, 1, '71F3FF82541088FEAAD1651D1CB5A50B70A7C955FD20F12462D6C1E74278A9B9', 'B4EABF7F-315A-48CF-BDC8-DD84093DF57C', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Damon', 'Miller', 'Mr.', 'dmiller', 'damonmiller513@gmail.com', 1, 1, 1, 'B09F895AF1360AFB6C4CCD107B51010704903B12938ACB8193264BF320D7393E', '8FE8FEAB-EE29-4FA2-AF7A-BF67D66C1D82', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'toddpires', 'todd.pires@oceandisc.com', 1, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('David', 'Roberts', 'Mr.', 'DaveR', 'djrobertsjr@comcast.net', 1, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Kevin', 'Ross', 'Mr.', 'kevin', 'kevin.ross@oceandisc.com', 1, 1, 1, 'F3DB965CB9DC5462A0F1407BF997CA645398B46106DFCE1E4A4C62B32B4D287B', 'B9EF686D-261D-462F-AD81-24157BC4C522', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Rabaa', 'Tarhouni', 'Miss', 'Rabaa', 'rabaat@skotkonung.com', 1, 1, 1, 'FD494E548827046AEB64E2932AD08DA110C07574FAB879625AE1AB9C79A2B4A7', '01233550-0E18-41C4-BD4A-A44FE94A570D', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Ian', 'van Mourik', 'Mr.', 'IanVM', 'ian.van.mourik@gmail.com', 1, 1, 1, 'DC8013389868E63E539C553780046DB1DF3A80090FC66A2972A5416643E26D23', 'CCB4FD4C-0453-4245-A43C-01F1C620939C', CAST('2020-01-01T00:00:00.000' AS DateTime))
GO
--End table person.Person

--Begin table person.PersonPasswordSecurity
INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	1,
	'Scott'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	6,
	'Deane'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO

INSERT INTO person.PersonPasswordSecurity
	(PersonID,PasswordSecurityQuestionID,PasswordSecurityQuestionAnswer)
SELECT
	P.PersonID,
	12,
	'Norwalk'
FROM person.Person P
WHERE P.LastName = 'Pires'
GO
--End table person.PersonPasswordSecurity

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

--Begin table person.PersonProject
TRUNCATE TABLE person.PersonProject
GO

INSERT INTO person.PersonProject
	(PersonID, ProjectID)
SELECT
	P1.PersonID,
	P2.ProjectID
FROM person.Person P1, dropdown.Project P2
WHERE P2.ProjectID > 0
GO
--End table person.PersonProject