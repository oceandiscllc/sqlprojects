/* Build File - 03 - Procedures - EventLog */
USE GIZ
GO

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.05
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogAssetAction
EXEC utility.DropObject 'eventlog.LogAssetAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Asset'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cAssetEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetEquipmentResourceProviders = COALESCE(@cAssetEquipmentResourceProviders, '') + D.AssetEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetEquipmentResourceProvider'), ELEMENTS) AS AssetEquipmentResourceProvider
			FROM asset.AssetEquipmentResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		DECLARE @cAssetFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cAssetFinancialResourceProviders = COALESCE(@cAssetFinancialResourceProviders, '') + D.AssetFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetFinancialResourceProvider'), ELEMENTS) AS AssetFinancialResourceProvider
			FROM asset.AssetFinancialResourceProvider T 
			WHERE T.AssetID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogAssetActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogAssetActionTable
		FROM asset.Asset T
		WHERE T.AssetID = @EntityID
		
		ALTER TABLE #LogAssetActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<Location>' + CAST(A.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<AssetEquipmentResourceProviders>' + ISNULL(@cAssetEquipmentResourceProviders, '') + '</AssetEquipmentResourceProviders>') AS XML),
			CAST(('<AssetFinancialResourceProviders>' + ISNULL(@cAssetFinancialResourceProviders, '') + '</AssetFinancialResourceProviders>') AS XML)
			FOR XML RAW('Asset'), ELEMENTS
			),
			A.ProjectID
		FROM #LogAssetActionTable T
			JOIN asset.Asset A ON A.AssetID = T.AssetID

		DROP TABLE #LogAssetActionTable

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			A.ProjectID
		FROM asset.Asset A
		WHERE A.AssetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAssetAction

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Contact'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cContactCourses VARCHAR(MAX) 
	
		SELECT 
			@cContactCourses = COALESCE(@cContactCourses, '') + D.ContactCourse 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactCourse'), ELEMENTS) AS ContactCourse
			FROM training.CourseContact T 
			WHERE T.ContactID = @EntityID
			) D

		DECLARE @cContactEquipmentInventory VARCHAR(MAX) 
	
		SELECT 
			@cContactEquipmentInventory = COALESCE(@cContactEquipmentInventory, '') + D.ContactEquipmentInventory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW(''), ELEMENTS) AS ContactEquipmentInventory
			FROM procurement.EquipmentInventory T 
			WHERE T.AssigneeTypeCode = 'Contact'
				AND T.AssigneeID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ContactCourses>' + ISNULL(@cContactCourses, '') + '</ContactCourses>') AS XML),
			CAST(('<ContactEquipmentInventory>' + ISNULL(@cContactEquipmentInventory, '') + '</ContactEquipmentInventory>') AS XML)
			FOR XML RAW('Contact'), ELEMENTS
			),
			T.ProjectID
		FROM training.Contact T 
		WHERE T.ContactID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM training.Contact T
		WHERE T.ContactID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogCourseAction
EXEC utility.DropObject 'eventlog.LogCourseAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCourseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Course'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cCourseContacts VARCHAR(MAX) 
	
		SELECT 
			@cCourseContacts = COALESCE(@cCourseContacts, '') + D.CourseContact 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CourseContact'), ELEMENTS) AS CourseContact
			FROM training.CourseContact T 
			WHERE T.CourseID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<CourseContacts>' + ISNULL(@cCourseContacts, '') + '</CourseContacts>') AS XML)
			FOR XML RAW('Course'), ELEMENTS
			),
			T.ProjectID
		FROM training.Course T 
		WHERE T.CourseID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM training.Course T
		WHERE T.CourseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCourseAction

--Begin procedure eventlog.LogEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentCatalogAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentCatalog'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentCatalog'), ELEMENTS
			),
			T.ProjectID
		FROM procurement.EquipmentCatalog T 
		WHERE T.EquipmentCatalogID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM procurement.EquipmentCatalog T
		WHERE T.EquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentCatalogAction

--Begin procedure eventlog.LogEquipmentInventoryAction
EXEC utility.DropObject 'eventlog.LogEquipmentInventoryAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentInventoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentInventory'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentInventory'), ELEMENTS
			),
			EC.ProjectID
		FROM procurement.EquipmentInventory T
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = T.EquipmentCatalogID
				AND T.EquipmentInventoryID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			EC.ProjectID
		FROM procurement.EquipmentInventory T
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = T.EquipmentCatalogID
				AND T.EquipmentInventoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentInventoryAction

--Begin procedure eventlog.LogForceAction
EXEC utility.DropObject 'eventlog.LogForceAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogForceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Force'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cForceEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceEquipmentResourceProviders = COALESCE(@cForceEquipmentResourceProviders, '') + D.ForceEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceEquipmentResourceProvider'), ELEMENTS) AS ForceEquipmentResourceProvider
			FROM force.ForceEquipmentResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceFinancialResourceProviders = COALESCE(@cForceFinancialResourceProviders, '') + D.ForceFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceFinancialResourceProvider'), ELEMENTS) AS ForceFinancialResourceProvider
			FROM force.ForceFinancialResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogForceActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogForceActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogForceActionTable
		FROM force.Force T
		WHERE T.ForceID = @EntityID
		
		ALTER TABLE #LogForceActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<Location>' + CAST(F.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<ForceEquipmentResourceProviders>' + ISNULL(@cForceEquipmentResourceProviders, '') + '</ForceEquipmentResourceProviders>') AS XML),
			CAST(('<ForceFinancialResourceProviders>' + ISNULL(@cForceFinancialResourceProviders, '') + '</ForceFinancialResourceProviders>') AS XML)
			FOR XML RAW('Force'), ELEMENTS
			),
			F.ProjectID
		FROM #LogForceActionTable T
			JOIN force.Force F ON F.ForceID = T.ForceID

		DROP TABLE #LogForceActionTable

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			F.ProjectID
		FROM force.Force F
		WHERE F.ForceID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogForceAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Incident'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogIncidentActionTable
		FROM core.Incident T
		WHERE T.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<Location>' + CAST(I.Location AS VARCHAR(MAX)) + '</Location>') AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			),
			I.ProjectID
		FROM #LogIncidentActionTable T
			JOIN core.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			I.ProjectID
		FROM core.Incident I
		WHERE I.IncidentID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogModuleAction
EXEC utility.DropObject 'eventlog.LogModuleAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogModuleAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Module'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Module'), ELEMENTS
			),
			T.ProjectID
		FROM training.Module T 
		WHERE T.ModuleID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM training.Module T
		WHERE T.ModuleID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogModuleAction

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PermissionableTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPermissionableTemplatePermissionables VARCHAR(MAX) = ''
		
		SELECT @cPermissionableTemplatePermissionables = COALESCE(@cPermissionableTemplatePermissionables, '') + D.PermissionableTemplatePermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PermissionableTemplatePermissionable
			FROM permissionable.PermissionableTemplatePermissionable T 
			WHERE T.PermissionableTemplateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PermissionableTemplatePermissionables>' + ISNULL(@cPermissionableTemplatePermissionables, '') + '</PermissionableTemplatePermissionables>' AS XML)
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T
		WHERE T.PermissionableTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonPasswordSecurity VARCHAR(MAX) = ''
		
		SELECT @cPersonPasswordSecurity = COALESCE(@cPersonPasswordSecurity, '') + D.PersonPasswordSecurity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPasswordSecurity'), ELEMENTS) AS PersonPasswordSecurity
			FROM person.PersonPasswordSecurity T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		DECLARE @cPersonProjects VARCHAR(MAX) = ''

		SELECT @cPersonProjects = COALESCE(@cPersonProjects, '') + D.PersonProject
		FROM
			(
			SELECT
				(SELECT T.ProjectID FOR XML RAW(''), ELEMENTS) AS PersonProject
			FROM person.PersonProject T 
			WHERE T.PersonID = 1
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PasswordSecurity>' + ISNULL(@cPersonPasswordSecurity, '') + '</PasswordSecurity>' AS XML),
			CAST('<Permissionables>' + ISNULL(@cPersonPermissionables, '') + '</Permissionables>' AS XML),
			CAST('<PersonProjects>' + ISNULL(@cPersonProjects, '') + '</PersonProjects>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogRequestForInformationAction
EXEC utility.DropObject 'eventlog.LogRequestForInformationAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRequestForInformationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'RequestForInformation'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('RequestForInformation'), ELEMENTS
			),
			T.ProjectID
		FROM core.RequestForInformation T 
		WHERE T.RequestForInformationID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM core.RequestForInformation T
		WHERE T.RequestForInformationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRequestForInformationAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'SpotReport'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM spotreport.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportTerritories = COALESCE(@cSpotReportTerritories, '') + D.SpotReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportTerritory'), ELEMENTS) AS SpotReportTerritory
			FROM spotreport.SpotReportTerritory T 
			WHERE T.SpotReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogSpotReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogSpotReportActionTable
		--ENDIF

		SELECT T.*
		INTO #LogSpotReportActionTable
		FROM spotreport.SpotReport T
		WHERE T.SpotReportID = @EntityID
		
		ALTER TABLE #LogSpotReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SummaryMap>' + CAST(SR.SpotReportID AS VARCHAR(MAX)) + '</SummaryMap>') AS XML), 
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportTerritories>' + ISNULL(@cSpotReportTerritories, '') + '</SpotReportTerritories>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			),
			SR.ProjectID
		FROM #LogSpotReportActionTable T
			JOIN spotreport.SpotReport SR ON SR.SpotReportID = T.SpotReportID

		DROP TABLE #LogSpotReportActionTable

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			SR.ProjectID
		FROM spotreport.SpotReport SR
		WHERE SR.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure eventlog.LogTerritoryAction
EXEC utility.DropObject 'eventlog.LogTerritoryAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTerritoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Territory'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cTerritoryAnnex VARCHAR(MAX) 
	
		SELECT 
			@cTerritoryAnnex = COALESCE(@cTerritoryAnnex, '') + D.TerritoryAnnex 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW(''), ELEMENTS) AS TerritoryAnnex
			FROM territory.TerritoryAnnex T 
			WHERE T.TerritoryID = @EntityID
				AND T.IsActive = 1
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogTerritoryActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogTerritoryActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogTerritoryActionTable
		FROM territory.Territory T
		WHERE T.TerritoryID = @EntityID
		
		ALTER TABLE #LogTerritoryActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T1.*,
			CAST(('<Location>' + CAST(T2.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<TerritoryAnnex>' + CAST(@cTerritoryAnnex AS VARCHAR(MAX)) + '</TerritoryAnnex>') AS XML)
			FOR XML RAW('Territory'), ELEMENTS
			)
		FROM #LogTerritoryActionTable T1
			JOIN territory.Territory T2 ON T2.TerritoryID = T1.TerritoryID

		DROP TABLE #LogTerritoryActionTable

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
		FROM territory.Territory T
		WHERE T.TerritoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTerritoryAction

--Begin procedure eventlog.LogTrendReportAction
EXEC utility.DropObject 'eventlog.LogTrendReportAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'TrendReport'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cTrendReportAssets VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportAssets = COALESCE(@cTrendReportAssets, '') + D.TrendReportAsset 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportAsset'), ELEMENTS) AS TrendReportAsset
			FROM trendreport.TrendReportAsset T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportForces VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportForces = COALESCE(@cTrendReportForces, '') + D.TrendReportForce 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportForce'), ELEMENTS) AS TrendReportForce
			FROM trendreport.TrendReportForce T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportIncidents = COALESCE(@cTrendReportIncidents, '') + D.TrendReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportIncident'), ELEMENTS) AS TrendReportIncident
			FROM trendreport.TrendReportIncident T 
			WHERE T.TrendReportID = @EntityID
			) D

		DECLARE @cTrendReportTerritories VARCHAR(MAX) 
	
		SELECT 
			@cTrendReportTerritories = COALESCE(@cTrendReportTerritories, '') + D.TrendReportTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TrendReportTerritory'), ELEMENTS) AS TrendReportTerritory
			FROM trendreport.TrendReportTerritory T 
			WHERE T.TrendReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogTrendReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogTrendReportActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogTrendReportActionTable
		FROM trendreport.TrendReport T
		WHERE T.TrendReportID = @EntityID
		
		ALTER TABLE #LogTrendReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<TrendReportAssets>' + ISNULL(@cTrendReportAssets, '') + '</TrendReportAssets>') AS XML),
			CAST(('<TrendReportForces>' + ISNULL(@cTrendReportForces, '') + '</TrendReportForces>') AS XML),
			CAST(('<TrendReportIncidents>' + ISNULL(@cTrendReportIncidents, '') + '</TrendReportIncidents>') AS XML),
			CAST(('<TrendReportTerritories>' + ISNULL(@cTrendReportTerritories, '') + '</TrendReportTerritories>') AS XML)
			FOR XML RAW('TrendReport'), ELEMENTS
			),
			TR.ProjectID
		FROM #LogTrendReportActionTable T
			JOIN trendreport.TrendReport TR ON TR.TerritoryID = T.TrendReportID

		DROP TABLE #LogTerritoryActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAction

--Begin procedure eventlog.LogTrendReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogTrendReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'TrendReportAggregator'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM workflow.WorkflowStepGroup T
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM workflow.WorkflowStepGroupPerson T 
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('TrendReportAggregator'), ELEMENTS
			)
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAggregatorAction

--Begin procedure eventlog.LogWorkflowAction
EXEC utility.DropObject 'eventlog.LogWorkflowAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkflowAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Workflow'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM workflow.WorkflowStepGroup T
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM workflow.WorkflowStepGroupPerson T 
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<WorkflowSteps>' + ISNULL(@cWorkflowSteps, '') + '</WorkflowSteps>') AS XML),
			CAST(('<WorkflowStepGroups>' + ISNULL(@cWorkflowStepGroups, '') + '</WorkflowStepGroups>') AS XML),
			CAST(('<WorkflowStepGroupPersons>' + ISNULL(@cWorkflowStepGroupPersons, '') + '</WorkflowStepGroupPersons>') AS XML)
			FOR XML RAW('Workflow'), ELEMENTS
			),
			T.ProjectID
		FROM workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkflowAction