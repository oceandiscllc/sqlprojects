/* Build File - 03 - Procedures - Workflow */
USE GIZ
GO

--Begin procedure workflow.DecrementWorkflow
EXEC utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount

END
GO
--End procedure workflow.DecrementWorkflow

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT TOP 1
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			SELECT
				'Approved' AS WorkflowStepName,
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
			
			END
		--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return people assigned to an entityy's current workflow step
-- ========================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowPeople

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WSG.WorkflowStepGroupName,
			WSGP.PersonID,
			person.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
		ORDER BY 1, 3
	
		END
	ELSE
		BEGIN
	
		SELECT
			EWSGP.WorkflowStepGroupName, 
			EWSGP.PersonID,
			person.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber
		ORDER BY 1, 3
	
		END
	--ENDIF
	
END
GO
--End procedure workflow.GetEntityWorkflowPeople

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.IncrementWorkflow
EXEC utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure workflow.IncrementWorkflow

--Begin procedure workflow.InitializeEntityWorkflow
EXEC utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1

END
GO
--End procedure workflow.InitializeEntityWorkflow

--Begin procedure workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to indicate if a personid exists in the current step of an entity's workflow
-- =====================================================================================================

CREATE PROCEDURE workflow.IsPersonInCurrentWorkflowStep

@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN

		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND W.IsActive = 1

		END
	ELSE
		BEGIN

		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
			AND EWSGP.PersonID = @PersonID

		END
	--ENDIF
	
END
GO
--End procedure workflow.IsPersonInCurrentWorkflowStep

--Begin procedure workflow.IsWorkflowComplete
EXEC utility.DropObject 'workflow.IsWorkflowComplete'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.06.03
-- Description:	A stored procedure to return a flag indicating if a workflow is complete
-- =====================================================================================
CREATE PROCEDURE workflow.IsWorkflowComplete

@EntityTypeCode VARCHAR(50), 
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN @EntityID > 0 AND workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) > 0
			THEN 1
			ELSE 0
		END AS IsWorkflowComplete

END
GO
--End procedure workflow.IsWorkflowComplete

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		PersonID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		E.PersonID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.PersonID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson
