/* Build File - 03 - Procedures - Other */
USE GIZ
GO

--Begin procedure asset.GetAssetByAssetID
EXEC utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the asset.Asset table
-- ==================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AssetDescription, 		
		A.AssetID, 		
		A.AssetName, 		
		A.Comments, 		
		A.DeputyManagerContactID, 		
		contact.FormatContactNameByContactID(A.DeputyManagerContactID, 'LastFirstMiddle') AS DeputyManagerFullName,		
		A.History, 		
		A.IsActive,
		A.LastUpdateDate,
		core.FormatDate(A.LastUpdateDate) AS LastUpdateDateFormatted,
		A.Location.STAsText() AS Location,
		A.ManagerContactID,		
		contact.FormatContactNameByContactID(A.ManagerContactID, 'LastFirstMiddle') AS ManagerFullName,		
		A.Notes, 		
		A.ProjectID,
		dropdown.GetProjectNameByProjectID(A.ProjectID) AS ProjectName,
		A.TerritoryID, 		
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName,		
		A.WebLinks,		
		AT.AssetTypeID, 
		AT.AssetTypeName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetEquipmentResourceProvider AERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AERP.ResourceProviderID
			AND AERP.AssetID = @AssetID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM asset.AssetFinancialResourceProvider AFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = AFRP.ResourceProviderID
			AND AFRP.AssetID = @AssetID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID
	
END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure contact.GetContactByContactID
EXEC utility.DropObject 'contact.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the contact.Contact table
-- ======================================================================
CREATE PROCEDURE contact.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.AssetID,
		(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = C1.AssetID) AS AssetName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCode,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		core.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCode,
		C1.FirstName,
		C1.ForceID,
		(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = C1.ForceID) AS ForceName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		core.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCode,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProjectID,
		dropdown.GetProjectNameByProjectID(C1.ProjectID) AS ProjectName,		
		C1.SkypeUserName,
		C1.StartDate,
		core.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(C1.TerritoryID) AS TerritoryName,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		C6.CountryID AS PlaceOfBirthCountryID,
		C6.CountryName AS PlaceOfBirthCountryName,		
		C7.CountryID AS PassportCountryID,
		C7.CountryName AS PassportCountryName,
		CS.ContactStatusID,
		CS.ContactStatusName
	FROM contact.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PassportCountryID
		JOIN dropdown.ContactStatus CS ON CS.ContactStatusID = C1.ContactStatusID
			AND C1.ContactID = @ContactID

	--ContactCourse
	SELECT 
		C.courseID, 
		M.ModuleName, 
		P.ProjectName 
	FROM training.Course C
		JOIN training.CourseContact CC ON CC.CourseID = C.CourseID
		JOIN training.Module M ON M.ModuleID = C.ModuleID
		JOIN dropdown.project P ON P.ProjectID = C.ProjectID
			AND CC.ContactID = @ContactID

	--ContactEquipment
	SELECT 
		EI.EquipmentInventoryID, 
		EC.ItemName, 
		EC.ItemDescription, 
		EC.Notes 
	FROM procurement.EquipmentInventory EI
		JOIN contact.Contact C ON C.ContactID = EI.AssigneeID
			AND C.ContactID = @ContactID
			AND EI.AssigneeTypeCode = 'Contact'
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND C.ProjectID = EC.ProjectID

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM contact.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
	
END
GO
--End procedure contact.GetContactByContactID

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure document.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'document.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
			AND D.ProjectID = @ProjectID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure document.GetEntityDocuments
EXEC utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get records from the document.DocumentEntity table
-- =====================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@ProjectID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
		SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentType, 
		D.ContentSubtype,
		D.DocumentDate, 
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND D.ProjectID = @ProjectID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure document.PurgeEntityDocuments
EXEC utility.DropObject 'document.PurgeEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.PurgeEntityDocuments

@DocumentID INT,
@EntityID INT,
@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.DocumentID = @DocumentID
		AND DE.EntityTypeCode = @EntityTypeCode
		AND DE.EntityTypeSubCode = @EntityTypeSubCode
		AND DE.EntityID = @EntityID 

END
GO
--End procedure document.PurgeEntityDocuments

--Begin procedure document.SaveEntityDocuments
EXEC utility.DropObject 'document.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to manage records in the document.DocumentEntity table
-- ======================================================================================
CREATE PROCEDURE document.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	DELETE DE
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		END
	--ENDIF
	
END
GO
--End procedure document.SaveEntityDocuments

--Begin procedure document.UpdateDocumentPermissions
EXEC utility.DropObject 'document.UpdateDocumentPermissions'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.09.28
-- Description:	A stored procedure to manage document permissions
-- ==============================================================
CREATE PROCEDURE document.UpdateDocumentPermissions

AS
BEGIN
	SET NOCOUNT ON;

	DELETE P
	FROM permissionable.Permissionable P
	WHERE P.ControllerName = 'Document'
		AND P.MethodName = 'View'
		AND EXISTS
			(
			SELECT 1
			FROM dropdown.DocumentType DT
			WHERE DT.DocumentTypeCode = P.PermissionCode
			)

	INSERT INTO permissionable.Permissionable
		(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description)
	SELECT
		'Document',
		'View',
		DT.DocumentTypeCode,
		(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = 'Document'),
		'View documents of type ' + LOWER(DT.DocumentTypeName) + ' in the library'
	FROM dropdown.DocumentType DT
	WHERE DT.DocumentTypeCode IS NOT NULL
		AND DT.DocumentTypeID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
			)

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PP.PermissionableLineage
		)
	
END
GO
--End procedure document.UpdateDocumentPermissions
	
--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.LastUpdateDate,
		core.FormatDate(F.LastUpdateDate) AS LastUpdateDateFormatted,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryNameFormatted,
		F.WebLinks,
		FT.ForceTypeID, 
		FT.ForceTypeName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
			AND F.ForceID = @ForceID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC Utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date: 2017.01.01
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.CurrencyID,
		EC.EquipmentCatalogID, 
		EC.IsActive, 
		IIF(EC.IsActive = 1, 'Yes ', 'No ') AS IsActiveFormatted,
		EC.ItemName, 
		EC.Notes, 
		EC.ProjectID,
		dropdown.GetProjectNameByProjectID(EC.ProjectID) AS ProjectName,
		EC.UnitCost,
		EC.UnitOfIssue,
		CONCAT(C.ISOCurrencyCode, ' ', EC.UnitCost) AS UnitCostFormatted
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.Currency C ON C.CurrencyID = EC.CurrencyID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.21
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
-- =====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EC.ProjectID,
		dropdown.GetProjectNameByProjectID(EC.ProjectID) AS ProjectName,
		CONCAT(C.ISOCurrencyCode, ' ', EC.UnitCost) AS UnitCostFormatted,
		EI.AssigneeID, 
		EI.AssigneeTypeCode, 
		
		CASE
			WHEN EI.AssigneeTypeCode = 'Asset'
			THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EI.AssigneeID)
			WHEN EI.AssigneeTypeCode = 'Contact'
			THEN contact.FormatContactNameByContactID(EI.AssigneeID, 'LastFirstMiddle')
			ELSE ''
		END AS AssigneeName,

		EI.AssignmentDate,
		core.FormatDate(EI.AssignmentDate) AS AssignmentDateFormatted,
		EI.EquipmentInventoryID, 
		EI.InvoiceDate,
		core.FormatDate(EI.InvoiceDate) AS InvoiceDateFormatted,
		EI.InvoiceNumber,
		EI.Notes, 
		EI.Quantity, 
		EI.SerialNumber
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.Currency C ON C.CurrencyID = EC.CurrencyID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectID INT = (SELECT SR.ProjectID FROM spotreport.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)
	
	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunicationOpportunities,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SourceDetails,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom
	FROM spotreport.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN core.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure territory.GetTerritoryByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.08
-- Description:	A stored procedure to data from the territory.Territory table
-- ==========================================================================
CREATE PROCEDURE territory.GetTerritoryByTerritoryID

@TerritoryID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Territory
	SELECT
		I.IEGInfluenceID,
		I.IEGInfluenceName,
		SC.SituationChangeID,
		SC.SituationChangeName,
		T.Location.STAsText() AS Location,
		T.TerritoryID,
		territory.FormatParentTerritoryNameByTerritoryID(T.TerritoryID) AS ParentTerritoryNameFormatted,
		T.Population, 
		T.PopulationSource,
		T.TerritoryName,
		T.TerritoryNameLocal,
		TA.Notes,
		TA.ProjectID,
		dropdown.GetProjectNameByProjectID(TA.ProjectID) AS ProjectName,
		TA.TerritoryAnnexID,
		TS.TerritoryStatusID,
		TS.TerritoryStatusName,
		TT.TerritoryTypeCode,
		TT.TerritoryTypeName,
		TT.TerritoryTypeNamePlural		
	FROM territory.Territory T
		JOIN territory.TerritoryAnnex TA ON TA.TerritoryID = T.TerritoryID
			AND TA.ProjectID = @ProjectID
			AND TA.IsActive = 1
		JOIN dropdown.IEGInfluence I ON I.IEGInfluenceID = TA.IEGInfluenceID
		JOIN dropdown.SituationChange SC ON SC.SituationChangeID = TA.SituationChangeID
		JOIN dropdown.TerritoryStatus TS ON TS.TerritoryStatusID = TA.TerritoryStatusID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID

	--TerritoryAsset
	SELECT 
		A.AssetID, 		
		A.AssetName, 		
		AT.AssetTypeName,
		territory.FormatTerritoryNameByTerritoryID(A.TerritoryID) AS TerritoryName
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = A.TerritoryID
			AND A.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryForce
	SELECT 
		F.ForceID, 
		F.ForceName, 
		FT.ForceTypeName,
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = F.TerritoryID
			AND F.ProjectID = @ProjectID
	ORDER BY 2, 3, 4, 1

	--TerritoryIncident
	SELECT
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeName,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN territory.GetDescendantTerritoriesByByTerritoryID(@TerritoryID) DT ON DT.TerritoryID = I.TerritoryID
			AND I.ProjectID = @ProjectID
	ORDER BY 3, 4, 1, 5

END
GO
--End procedure territory.GetTerritoryByTerritoryID

--Begin procedure territory.GetParentTerritoryByTerritoryTypeCodeAndLocation
EXEC utility.DropObject 'territory.GetParentTerritoryByTerritoryTypeCodeAndLocation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.29
-- Description:	A stored procedure to associate a community to a project
-- =====================================================================
CREATE PROCEDURE territory.GetParentTerritoryByTerritoryTypeCodeAndLocation

@Latitude NUMERIC(18,14),
@Longitude NUMERIC(18,14),
@TerritoryTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		T.TerritoryID,
		T.ISOCountryCode2
	FROM territory.Territory T
	WHERE T.TerritoryTypeCode = @TerritoryTypeCode
		AND T.Location.STIntersects(GEOMETRY::STGeomFromText('POINT(' + CAST(@Longitude AS VARCHAR(50)) + ' ' + CAST(@Latitude AS VARCHAR(50)) + ')', 4326)) = 1		
	
END
GO
--End procedure territory.GetParentTerritoryByTerritoryTypeCodeAndLocation

--Begin procedure training.GetCourseByCourseID
EXEC utility.DropObject 'training.GetCourseByCourseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the training.Course table
-- ======================================================================
CREATE PROCEDURE training.GetCourseByCourseID

@CourseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CourseID,
		C.CoursePointOfContact,
		C.TerritoryID,
		(territory.FormatTerritoryNameByTerritoryID(C.TerritoryID)) AS TerritoryNameFormatted,
		C.EndDate,
		core.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Instructor1,
		C.Instructor1Comments,
		C.Instructor2,
		C.Instructor2Comments,
		C.Location,
		C.ProjectID,
		dropdown.GetProjectNameByProjectID(C.ProjectID) AS ProjectName,
		C.Seats,
		C.StartDate,
		core.FormatDate(C.StartDate) AS StartDateFormatted,
		C.StudentFeedbackSummary,
		C.QualityAssuranceFeedback,
		M.ModuleID,
		M.ModuleName
	FROM training.Course C
		JOIN training.Module M ON M.ModuleID = C.ModuleID
			AND C.CourseID = @CourseID
	
	SELECT
		territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID) AS TerritoryNameFormatted,
		CO.ContactID,
		core.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		contact.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS ContactNameFormatted,
		IIF (CO.AssetID > 0,
			(SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = CO.AssetID), 
			IIF (CO.ForceID > 0,
				(SELECT F.ForceName FROM force.Force F WHERE F.ForceID = CO.ForceID),
				IIF (CO.TerritoryID > 0,
					territory.FormatTerritoryNameByTerritoryID(CO.TerritoryID),
					'')) ) AS ParentOrganizationName,
		STUFF((
			SELECT ', ' + CT.ContactTypeName
			FROM contact.ContactContactType CCT
			JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = CC.ContactID
			FOR XML PATH('')
		), 1, 2, '') AS ContactTypeNamesList
	FROM training.CourseContact CC
		JOIN training.Course CL ON CL.CourseID = CC.CourseID
		JOIN contact.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.CourseID = @CourseID

END
GO
--End procedure training.GetCourseByCourseID

--Begin procedure training.GetModuleByModuleID
EXEC utility.DropObject 'training.GetModuleByModuleID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the training.Module table
-- ======================================================================
CREATE PROCEDURE training.GetModuleByModuleID

@ModuleID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		M.ActivityCode,
		M.LearnerProfileTypeID,
		M.ModuleID,
		M.ModuleName,
		M.ModuleTypeID,
		M.Notes,
		M.ProgramTypeID,
		M.ProjectID,
		dropdown.GetProjectNameByProjectID(M.ProjectID) AS ProjectName,
		M.SponsorName,
		M.Summary,
		MT.ModuleTypeID,
		MT.ModuleTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName
	FROM training.Module M
		JOIN dropdown.ModuleType MT ON MT.ModuleTypeID = M.ModuleTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = M.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = M.ProgramTypeID
			AND M.ModuleID = @ModuleID
		
END
GO
--End procedure training.GetModuleByModuleID