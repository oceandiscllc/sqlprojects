/* Build File - 01 - Tables - Other*/
USE GIZ
GO

--Begin table aggregator.TrendReportAggregator
DECLARE @TableName VARCHAR(250) = 'aggregator.TrendReportAggregator'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'aggregator.TrendReport'

CREATE TABLE aggregator.TrendReportAggregator
	(
	TrendReportAggregatorID INT NOT NULL IDENTITY(1,1),
	TrendReportTitle VARCHAR(250),
	TrendReportStartDate DATE,
	TrendReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'TrendReportAggregatorID'
GO
--End table aggregator.TrendReportAggregator

--Begin table asset.Asset
DECLARE @TableName VARCHAR(250) = 'asset.Asset'

EXEC utility.DropObject @TableName

CREATE TABLE asset.Asset
	(
	AssetID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	AssetName NVARCHAR(250),
	AssetDescription VARCHAR(MAX),
	AssetTypeID INT,
	Location GEOMETRY,
	ManagerContactID INT,
	DeputyManagerContactID INT,
	Notes VARCHAR(MAX),
	History VARCHAR(MAX),
	Comments VARCHAR(MAX),
	WebLinks NVARCHAR(MAX),
	ProjectID INT,
	LastUpdateDate DATE,
	IsActive BIT,
	IntegrationCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DeputyManagerContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ManagerContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetID'
GO
--End table asset.Asset

--Begin table asset.AssetEquipmentResourceProvider
DECLARE @TableName VARCHAR(250) = 'asset.AssetEquipmentResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetEquipmentResourceProvider
	(
	AssetEquipmentResourceProviderID INT IDENTITY(1,1) NOT NULL,
	AssetID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetEquipmentResourceProviderID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetEquipmentResourceProvider', 'AssetID,ResourceProviderID'
GO
--End table asset.AssetEquipmentResourceProvider

--Begin table asset.AssetFinancialResourceProvider
DECLARE @TableName VARCHAR(250) = 'asset.AssetFinancialResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetFinancialResourceProvider
	(
	AssetFinancialResourceProviderID INT IDENTITY(1,1) NOT NULL,
	AssetID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetFinancialResourceProviderID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetFinancialResourceProvider', 'AssetID,ResourceProviderID'
GO
--End table asset.AssetFinancialResourceProvider

--Begin table contact.Contact
DECLARE @TableName VARCHAR(250) = 'contact.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE contact.Contact
	(
	ContactID INT IDENTITY(1,1) NOT NULL,
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Address1 VARCHAR(100),
	Address2 VARCHAR(100),
	City VARCHAR(100),
	State VARCHAR(100),
	PostalCode VARCHAR(10),
	CountryID INT,
	PhoneNumberCountryCallingCode INT,
	PhoneNumber VARCHAR(20),
	CellPhoneNumberCountryCallingCode INT,
	CellPhoneNumber VARCHAR(20),
	FaxNumberCountryCallingCode INT,
	FaxNumber VARCHAR(20),
	EmailAddress VARCHAR(320),
	SkypeUserName VARCHAR(50),
	FaceBookPageURL VARCHAR(500),
	PlaceOfBirth VARCHAR(100),
	PlaceOfBirthCountryID INT,
	DateOfBirth DATE,
	Gender VARCHAR(10),
	Aliases NVARCHAR(MAX),
	CitizenshipCountryID1 INT,
	CitizenshipCountryID2 INT,
	GovernmentIDNumber VARCHAR(20),
	GovernmentIDNumberCountryID INT,
	PassportNumber VARCHAR(20),
	PassportExpirationDate DATE,
	PassportCountryID INT,
	EmployerName VARCHAR(50),
	ProjectID INT,
	Title VARCHAR(50),
	TerritoryID INT,
	AssetID INT,
	ForceID INT,
	Profession VARCHAR(50),
	ContactStatusID INT,
	IsActive BIT,
	IsValid BIT,
	StartDate DATE,
	Notes VARCHAR(MAX),
	ArabicFirstName NVARCHAR(200),
	ArabicMiddleName NVARCHAR(200),
	ArabicLastName NVARCHAR(200)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CellPhoneNumberCountryCallingCode', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CitizenshipCountryID1', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CitizenshipCountryID2', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FaxNumberCountryCallingCode', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GovernmentIDNumberCountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsValid', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PassportCountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhoneNumberCountryCallingCode', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlaceOfBirthCountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactID'
GO
--End table contact.Contact

--Begin table contact.ContactContactType
DECLARE @TableName VARCHAR(250) = 'contact.ContactContactType'

EXEC utility.DropObject @TableName

CREATE TABLE contact.ContactContactType
	(
	ContactContactTypeID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	ContactTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactContactTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactContactType', 'ContactID,ContactTypeID'
GO
--End table contact.Contact

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.DropObject @TableName

CREATE TABLE document.Document
	(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	DocumentDate DATE,
	DocumentDescription VARCHAR(1000),
	DocumentName VARCHAR(50),
	DocumentTitle NVARCHAR(250),
	DocumentTypeID INT,
	ProjectID INT,
	Extension VARCHAR(10),
	CreatePersonID INT,
	ContentType VARCHAR(250),
	ContentSubtype VARCHAR(100),
	PhysicalFileSize BIGINT,
	DocumentData VARBINARY(MAX),
	Thumbnail VARBINARY(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentID'
GO

EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @nDocumentID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			I.ContentType,
			I.ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			SET @cFileExtenstion = @cExtenstion
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			END
		--ENDIF

		UPDATE D
		SET D.Extension = ISNULL(@cFileExtenstion, @cExtenstion)
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		FETCH oCursor INTO @nDocumentID, @cContentType, @cContentSubType, @cExtenstion
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table document.Document

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentEntity
	(
	DocumentEntityID INT IDENTITY(1,1) NOT NULL,
	DocumentID INT,
	EntityTypeCode VARCHAR(50),
	EntityTypeSubCode VARCHAR(50),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DocumentEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_DocumentEntity', 'DocumentID,EntityTypeCode,EntityTypeSubCode'
GO
--End table document.DocumentEntity

--Begin table document.FileType
DECLARE @TableName VARCHAR(250) = 'document.FileType'

EXEC utility.DropObject @TableName

CREATE TABLE document.FileType
	(
	FileTypeID INT IDENTITY(1,1) NOT NULL,
	Extension VARCHAR(10),
	MimeType VARCHAR(100)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FileTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_FileType', 'Extension,MimeType'
GO
--End table document.FileType

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	ProjectID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.DropObject @TableName

CREATE TABLE force.Force
	(
	ForceID INT IDENTITY(1,1) NOT NULL,
	TerritoryID INT,
	ForceName NVARCHAR(250),
	ForceDescription VARCHAR(MAX),
	ForceTypeID INT,
	Location GEOMETRY,
	Notes VARCHAR(MAX),
	History VARCHAR(MAX),
	Comments VARCHAR(MAX),
	WebLinks NVARCHAR(MAX),
	ProjectID INT,
	LastUpdateDate DATE,
	IsActive BIT,
	CommanderFullName NVARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceID'
GO
--End table force.Force

--Begin table force.ForceEquipmentResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceEquipmentResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceEquipmentResourceProvider
	(
	ForceEquipmentResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceEquipmentResourceProviderID'
EXEC utility.SetIndexClustered @TableName, 'IX_ForceEquipmentResourceProvider', 'ForceID,ResourceProviderID'
GO
--End table force.ForceEquipmentResourceProvider

--Begin table force.ForceFinancialResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceFinancialResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceFinancialResourceProvider
	(
	ForceFinancialResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceFinancialResourceProviderID'
EXEC utility.SetIndexClustered @TableName, 'IX_ForceFinancialResourceProvider', 'ForceID,ResourceProviderID'
GO
--End table force.ForceFinancialResourceProvider

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceUnit
	(
	ForceUnitID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	UnitName VARCHAR(250),
	UnitTypeID INT,
	TerritoryID INT,
	CommanderFullName NVARCHAR(250),
	DeputyCommanderFullName NVARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UnitTypeID', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceUnitID'
EXEC utility.SetIndexClustered @TableName, 'IX_ForceUnit', 'ForceID,UnitName'
GO
--End table force.ForceUnit

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.Permissionable
	(
	PermissionableID INT IDENTITY(1,1) NOT NULL,
	ControllerName VARCHAR(50),
	MethodName VARCHAR(250),
	PermissionCode VARCHAR(50),
	PermissionableLineage VARCHAR(355),
	PermissionableGroupID INT,
	Description VARCHAR(MAX),
	IsActive BIT,
	IsGlobal BIT,
	IsSuperAdministrator BIT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsGlobal', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableID'
GO

EXEC utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @PermissionableLineage VARCHAR(355)
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID
	WHILE @@fetch_status = 0
		BEGIN

		UPDATE P
		SET P.PermissionableLineage = 			
			P.ControllerName 
				+ '.' 
				+ P.MethodName
				+ CASE
						WHEN P.PermissionCode IS NOT NULL
						THEN '.' + P.PermissionCode
						ELSE ''
					END

		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		FETCH oCursor INTO @nPermissionableID
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableGroup
	(
	PermissionableGroupID INT IDENTITY(1,1) NOT NULL,
	PermissionableGroupCode VARCHAR(50),
	PermissionableGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableGroup', 'DisplayOrder,PermissionableGroupName,PermissionableGroupID'
GO
--Begin table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplate
	(
	PermissionableTemplateID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplatePermissionable
	(
	PermissionableTemplatePermissionableID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableTemplateID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableTemplatePermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableTemplatePermissionable', 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table procurement.EquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentCatalog
	(
	EquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	ItemName VARCHAR(250),
	ItemDescription VARCHAR(500),
	UnitOfIssue VARCHAR(25),
	UnitCost NUMERIC(18, 2),
	Notes VARCHAR(MAX),
	IsActive BIT,
	CurrencyID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CurrencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UnitCost', 'NUMERIC(18, 2)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentCatalogID'
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentInventory
	(
	EquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
	EquipmentCatalogID INT,
	Quantity INT,
	SerialNumber VARCHAR(50),
	Notes VARCHAR(MAX),
	AssigneeTypeCode VARCHAR(50),
	AssigneeID INT,
	AssignmentDate DATE,
	InvoiceNumber VARCHAR(250),
	InvoiceDate DATE,
	EquipmentUsageGB INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssigneeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentUsageGB', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentInventoryID'
GO
--End table procurement.EquipmentInventory

--Begin table spotreport.SpotReport
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReport'

EXEC utility.DropObject @TableName

CREATE TABLE spotreport.SpotReport
	(
	SpotReportID INT IDENTITY(1,1) NOT NULL,
	SpotReportTitle VARCHAR(100),
	SpotReportDate DATE,
	Summary NVARCHAR(MAX),
	IncidentDetails NVARCHAR(MAX),
	Implications NVARCHAR(MAX),
	Resourcing NVARCHAR(MAX),
	ResourcingStatus NVARCHAR(MAX),
	RiskMitigation NVARCHAR(MAX),
	AnalystComments NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	AdditionalInformation NVARCHAR(MAX),
	ImpactDecisionID INT,
	StatusChangeID INT,
	IsCritical BIT,
	CommunicationOpportunities VARCHAR(MAX),
	SourceDetails VARCHAR(MAX),
	ProjectID INT,
	UpdateDateTime DATETIME,
	SummaryMap VARBINARY(MAX),
	SummaryMapZoom INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsCritical', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SpotReportID'
GO
--End table spotreport.SpotReport

--Begin table spotreport.SpotReportIncident
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE spotreport.SpotReportIncident
	(
	SpotReportIncidentID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_SpotReportIncident', 'SpotReportID,IncidentID'
GO
--End table spotreport.SpotReportIncident

--Begin table spotreport.SpotReportTerritory
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE spotreport.SpotReportTerritory
	(
	SpotReportTerritoryID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_SpotReportTerritory', 'SpotReportID,TerritoryID'
GO
--End table spotreport.SpotReportTerritory

--Begin table territory.Territory
DECLARE @TableName VARCHAR(250) = 'territory.Territory'

EXEC utility.DropObject @TableName

CREATE TABLE territory.Territory
	(
	TerritoryID INT IDENTITY(1,1) NOT NULL,
	ParentTerritoryID INT,
	ISOCountryCode2 CHAR(2),
	TerritoryTypeCode VARCHAR(50),
	TerritoryName NVARCHAR(250),
	PCode VARCHAR(25),
	Area NUMERIC(18, 2),
	Location GEOMETRY,
	Population INT,
	PopulationSource VARCHAR(250),
	TerritoryNameLocal NVARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'Area', 'NUMERIC(18, 2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentTerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Population', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TerritoryID'
GO

EXEC utility.DropObject 'territory.TR_Territory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2017.09.29
-- Description:	A trigger to update the territory.Territory table
-- ==============================================================
CREATE TRIGGER territory.TR_Territory ON territory.Territory AFTER INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO territory.TerritoryAnnex
		(TerritoryID, ProjectID)
	SELECT
		I.TerritoryID,
		P.ProjectID
	FROM INSERTED I, dropdown.Project P
	WHERE P.ProjectID > 0
		AND P.IsActive = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM territory.TerritoryAnnex TA
			WHERE TA.TerritoryID = I.TerritoryID
				AND TA.ProjectID = P.ProjectID
			)

	END
--ENDIF
GO
--End table territory.Territory

--Begin table territory.TerritoryAnnex
DECLARE @TableName VARCHAR(250) = 'territory.TerritoryAnnex'

EXEC utility.DropObject @TableName

CREATE TABLE territory.TerritoryAnnex
	(
	TerritoryAnnexID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryID INT,
	TerritoryStatusID INT,
	IEGInfluenceID INT,
	SituationChangeID INT,
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IEGInfluenceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SituationChangeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TerritoryAnnexID'
EXEC utility.SetIndexClustered @TableName, 'IX_TerritoryAnnex', 'ProjectID,TerritoryID'
GO
--End table territory.TerritoryAnnex

--Begin table training.Course
DECLARE @TableName VARCHAR(250) = 'training.Course'

EXEC utility.DropObject @TableName

CREATE TABLE training.Course
	(
	CourseID INT IDENTITY(1,1) NOT NULL,
	ModuleID INT,
	TerritoryID INT,
	StartDate DATE,
	EndDate DATE,
	Location VARCHAR(250),
	CoursePointOfContact VARCHAR(500),
	Instructor1 NVARCHAR(100),
	Instructor2 NVARCHAR(100),
	Instructor1Comments NVARCHAR(MAX),
	Instructor2Comments NVARCHAR(MAX),
	StudentFeedbackSummary VARCHAR(MAX),
	Seats INT,
	QualityAssuranceFeedback VARCHAR(MAX),
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ModuleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Seats', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CourseID'
GO
--End table training.Course

--Begin table training.CourseContact
DECLARE @TableName VARCHAR(250) = 'training.CourseContact'

EXEC utility.DropObject @TableName

CREATE TABLE training.CourseContact
	(
	CourseContactID INT IDENTITY(1,1),
	CourseID INT,
	ContactID INT,
	Comments VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CourseID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CourseContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_CourseContact', 'CourseID,ContactID'
GO
--End table training.CourseContact

--Begin table training.Module
DECLARE @TableName VARCHAR(250) = 'training.Module'

EXEC utility.DropObject @TableName

CREATE TABLE training.Module
	(
	ModuleID INT IDENTITY(1,1) NOT NULL,
	ActivityCode VARCHAR(50),
	ModuleName VARCHAR(200),
	Summary VARCHAR(MAX),
	ModuleTypeID INT,
	LearnerProfileTypeID INT,
	ProgramTypeID INT,
	SponsorName VARCHAR(250),
	Notes VARCHAR(MAX),
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LearnerProfileTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ModuleTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProgramTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ModuleID'
GO
--Begin table training.Module
