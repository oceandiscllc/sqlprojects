/* Build File - 04 - Data - Dropdown */
USE GIZ
GO

--Begin table dropdown.AssetType
TRUNCATE TABLE dropdown.AssetType
GO

EXEC utility.InsertIdentityValue 'dropdown.AssetType', 'AssetTypeID', 0
GO

INSERT INTO dropdown.AssetType 
	(AssetTypeName, Icon) 
VALUES
	('Asset Type 1', 'civilian.png'),
	('Asset Type 2', 'civilian.png'),
	('Asset Type 3', 'civilian.png')
GO
--End table dropdown.AssetType

--Begin table dropdown.ContactStatus
TRUNCATE TABLE dropdown.ContactStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactStatus', 'ContactStatusID', 0
GO

INSERT INTO dropdown.ContactStatus 
	(ContactStatusName) 
VALUES
	('Active'),
	('Active - Mobile')
GO
--End table dropdown.ContactStatus

--Begin table dropdown.ContactType
TRUNCATE TABLE dropdown.ContactType
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactType', 'ContactTypeID', 0
GO

INSERT INTO dropdown.ContactType 
	(ContactTypeName) 
VALUES
	('Commander'),
	('Deputy'),
	('Stringer'),
	('Media Officer'),
	('Point of Contact'),
	('Staff/Partner'),
	('Beneficiary'),
	('Credible Voice'),
	('Key Influencer')
GO
--End table dropdown.ContactType

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country 
	(ISOCountryCode3, ISOCurrencyCode, CountryName, CurrencyName) 
VALUES
	('ABW', 'AWG', 'Aruba', 'Aruban Florin'),
	('AFG', 'AFN', 'Afghanistan', 'Afghani Afghani'),
	('AGO', 'AOA', 'Angola', 'Angolan Kwanza'),
	('AIA', 'XCD', 'Anguilla', 'East Caribbean Dollar'),
	('ALA', 'EUR', N'Åland Islands', 'Euro'),
	('ALB', 'ALL', 'Albania', 'Albanian Lek'),
	('AND', 'EUR', 'Andorra', 'Euro'),
	('ARE', 'AED', 'United Arab Emirates', 'UAE Dirham'),
	('ARG', 'ARS', 'Argentina', 'Argentine Peso'),
	('ARM', 'AMD', 'Armenia', 'Armenian Dram'),
	('ASM', 'USD', 'American Samoa', 'US Dollar'),
	('ATF', 'EUR', 'French Southern Territories', 'Euro'),
	('ATG', 'XCD', 'Antigua and Barbuda', 'East Caribbean Dollar'),
	('AUS', 'AUD', 'Australia', 'Australian Dollar'),
	('AUT', 'EUR', 'Austria', 'Euro'),
	('AZE', 'AZN', 'Azerbaijan', 'Azerbaijanian Manat'),
	('BDI', 'BIF', 'Burundi', 'Burundi Franc'),
	('BEL', 'EUR', 'Belgium', 'Euro'),
	('BEN', 'XOF', 'Benin', 'CFA Franc BCEAO'),
	('BES', 'USD', 'Bonaire, Sint Eustatius and Saba', 'US Dollar'),
	('BFA', 'XOF', 'Burkina Faso', 'CFA Franc BCEAO'),
	('BGD', 'BDT', 'Bangladesh', 'Bangladeshi Taka'),
	('BGR', 'BGN', 'Bulgaria', 'Bulgarian Lev'),
	('BHR', 'BHD', 'Bahrain', 'Bahraini Dinar'),
	('BHS', 'BSD', 'Bahamas', 'Bahamian Dollar'),
	('BIH', 'BAM', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina Convertible Mark'),
	('BLM', 'EUR', 'Saint Barthélemy', 'Euro'),
	('BLR', 'BYR', 'Belarus', 'Belarussian Ruble'),
	('BLZ', 'BZD', 'Belize', 'Belize Dollar'),
	('BMU', 'BMD', 'Bermuda', 'Bermudian Dollar'),
	('BOL', 'BOB', 'Bolivia', 'Bolivian Boliviano'),
	('BRA', 'BRL', 'Brazil', 'Brazilian Real'),
	('BRB', 'BBD', 'Barbados', 'Barbados Dollar'),
	('BRN', 'BND', 'Brunei Darussalam', 'Brunei Dollar'),
	('BTN', 'BTN', 'Bhutan', 'Bhutanise Ngultrum'),
	('BVT', 'NOK', 'Bouvet Island', 'Norwegian Krone'),
	('BWA', 'BWP', 'Botswana', 'Botswanan Pula'),
	('CAF', 'XAF', 'Central African Republic', 'CFA Franc BEAC'),
	('CAN', 'CAD', 'Canada', 'Canadian Dollar'),
	('CCK', 'AUD', 'Cocos (Keeling) Islands', 'Australian Dollar'),
	('CHE', 'CHF', 'Switzerland', 'Swiss Franc'),
	('CHL', 'CLP', 'Chile', 'Chilean Peso'),
	('CHN', 'CNY', 'China', 'Chinese Yuan Renminbi'),
	('CIV', 'XOF', N'Côte D''Ivoire', 'CFA Franc BCEAO'),
	('CMR', 'XAF', 'Cameroon', 'CFA Franc BEAC'),
	('COD', 'CDF', 'Congo, The Democratic Republic Of The', 'Congolais Franc'),
	('COG', 'XAF', 'Congo', 'CFA Franc BEAC'),
	('COK', 'NZD', 'Cook Islands', 'New Zealand Dollar'),
	('COL', 'COP', 'Colombia', 'Colombian Peso'),
	('COM', 'KMF', 'Comoros', 'Comoro Franc'),
	('CPV', 'CVE', 'Cape Verde', 'Cape Verde Escudo'),
	('CRI', 'CRC', 'Costa Rica', 'Costa Rican Colon'),
	('CUB', 'CUP', 'Cuba', 'Cuban Peso'),
	('CUW', 'ANG', 'Curaçao', 'Netherlands Antillean Guilder'),
	('CXR', 'AUD', 'Christmas Island', 'Australian Dollar'),
	('CYM', 'KYD', 'Cayman Islands', 'Cayman Islands Dollar'),
	('CYP', 'EUR', 'Cyprus', 'Euro'),
	('CZE', 'CZK', 'Czech Republic', 'Czech Koruna'),
	('DEU', 'EUR', 'Germany', 'Euro'),
	('DJI', 'DJF', 'Djibouti', 'Djibouti Franc'),
	('DMA', 'XCD', 'Dominica', 'East Caribbean Dollar'),
	('DNK', 'DKK', 'Denmark', 'Danish Krone'),
	('DOM', 'DOP', 'Dominican Republic', 'Dominican Peso'),
	('DZA', 'DZD', 'Algeria', 'Algerian Dinar'),
	('ECU', 'USD', 'Ecuador', 'US Dollar'),
	('EGY', 'EGP', 'Egypt', 'Egyptian Pound'),
	('ERI', 'ERN', 'Eritrea', 'Eritrean Nakfa'),
	('ESH', 'MAD', 'Western Sahara', 'Moroccan Dirham'),
	('ESP', 'EUR', 'Spain', 'Euro'),
	('EST', 'EUR', 'Estonia', 'Euro'),
	('ETH', 'ETB', 'Ethiopia', 'Ethiopian Birr'),
	('EUR', 'EUR', 'European Union', 'Euro'),
	('FIN', 'EUR', 'Finland', 'Euro'),
	('FJI', 'FJD', 'Fiji', 'Fiji Dollar'),
	('FLK', 'FKP', 'Falkland Islands', 'Falkland Islands Pound'),
	('FRA', 'EUR', 'France', 'Euro'),
	('FRO', 'DKK', 'Faroe Islands', 'Danish Krone'),
	('FSM', 'USD', 'Micronesia, Federated States Of', 'US Dollar'),
	('GAB', 'XAF', 'Gabon', 'CFA Franc BEAC'),
	('GBR', 'GBP', 'United Kingdom', 'UK Pound Sterling'),
	('GEO', 'GEL', 'Georgia', 'Geogrian Lari'),
	('GGY', 'GBP', 'Guernsey', 'UK Pound Sterling'),
	('GHA', 'GHS', 'Ghana', 'Ghanan Cedi'),
	('GIB', 'GIP', 'Gibraltar', 'Gibraltar Pound'),
	('GIN', 'GNF', 'Guinea', 'Guinea Franc'),
	('GLP', 'EUR', 'Guadeloupe', 'Euro'),
	('GMB', 'GMD', 'Gambia', 'Gambian Dalasi'),
	('GNB', 'XOF', 'Guinea-Bissau', 'CFA Franc BCEAO'),
	('GNQ', 'XAF', 'Equatorial Guinea', 'CFA Franc BEAC'),
	('GRC', 'EUR', 'Greece', 'Euro'),
	('GRD', 'XCD', 'Grenada', 'East Caribbean Dollar'),
	('GRL', 'DKK', 'Greenland', 'Danish Krone'),
	('GTM', 'GTQ', 'Guatemala', 'Guatemalian Quetzal'),
	('GUF', 'EUR', 'French Guiana', 'Euro'),
	('GUM', 'USD', 'Guam', 'US Dollar'),
	('GUY', 'GYD', 'Guyana', 'Guyana Dollar'),
	('HKG', 'HKD', 'Hong Kong', 'Hong Kong Dollar'),
	('HMD', 'AUD', 'Heard Island and McDonald Islands', 'Australian Dollar'),
	('HND', 'HNL', 'Honduras', 'Honduran Lempira'),
	('HRV', 'HRK', 'Croatia', 'Croatian Kuna'),
	('HTI', 'HTG', 'Haiti', 'Hatian Gourde'),
	('HUN', 'HUF', 'Hungary', 'Hungarian Forint'),
	('IDN', 'IDR', 'Indonesia', 'Indonesian Rupiah'),
	('IMN', 'GBP', 'Isle of Man', 'UK Pound Sterling'),
	('IND', 'INR', 'India', 'Indian Rupee'),
	('IOT', 'USD', 'British Indian Ocean Territory', 'US Dollar'),
	('IRL', 'EUR', 'Ireland', 'Euro'),
	('IRN', 'IRR', 'Iran, Islamic Republic Of', 'Iranian Rial'),
	('IRQ', 'IQD', 'Iraq', 'Iraqi Dinar'),
	('ISL', 'ISK', 'Iceland', 'Iceland Krona'),
	('ISR', 'ILS', 'Israel', 'Israeli New Shekel'),
	('ITA', 'EUR', 'Italy', 'Euro'),
	('JAM', 'JMD', 'Jamaica', 'Jamaican Dollar'),
	('JEY', 'GBP', 'Jersey', 'UK Pound Sterling'),
	('JOR', 'JOD', 'Jordan', 'Jordanian Dinar'),
	('JPN', 'JPY', 'Japan', 'Japanese Yen'),
	('KAZ', 'KZT', 'Kazakhstan', 'Kazakhstan Tenge'),
	('KEN', 'KES', 'Kenya', 'Kenyan Shilling'),
	('KGZ', 'KGS', 'Kyrgyzstan', 'Kyrgyzstan Som'),
	('KHM', 'KHR', 'Cambodia', 'Cambodian Riel'),
	('KIR', 'AUD', 'Kiribati', 'Australian Dollar'),
	('KNA', 'XCD', 'Saint Kitts And Nevis', 'East Caribbean Dollar'),
	('KOR', 'KRW', 'Korea, Republic of', 'South Korean Won'),
	('KWT', 'KWD', 'Kuwait', 'Kuwaiti Dinar'),
	('LAO', 'LAK', 'Lao People''s Democratic Republic', 'Lao Kip'),
	('LBN', 'LBP', 'Lebanon', 'Lebanese Pound'),
	('LBR', 'LRD', 'Liberia', 'Liberian Dollar'),
	('LBY', 'LYD', 'Libya', 'Libyan Dinar'),
	('LCA', 'XCD', 'Saint Lucia', 'East Caribbean Dollar'),
	('LIE', 'CHF', 'Liechtenstein', 'Swiss Franc'),
	('LKA', 'LKR', 'Sri Lanka', 'Sri Lanka Rupee'),
	('LSO', 'LSL', 'Lesotho', 'Lesotho Loti'),
	('LTU', 'LTL', 'Lithuania', 'Lithuanian Litas'),
	('LUX', 'EUR', 'Luxembourg', 'Euro'),
	('LVA', 'LVL', 'Latvia', 'Latvian Lats'),
	('MAC', 'MOP', 'Macao', 'Macaoan Pataca'),
	('MAF', 'EUR', 'Saint Martin (French Part)', 'Euro'),
	('MAR', 'MAD', 'Morocco', 'Moroccan Dirham'),
	('MCO', 'EUR', 'Monaco', 'Euro'),
	('MDA', 'MDL', 'Moldova, Republic of', 'Moldovan Leu'),
	('MDG', 'MGA', 'Madagascar', 'Malagasy Ariary'),
	('MDV', 'MVR', 'Maldives', 'Maldiven Rufiyaa'),
	('MEX', 'MXN', 'Mexico', 'Mexican Peso'),
	('MHL', 'USD', 'Marshall Islands', 'US Dollar'),
	('MKD', 'MKD', 'Macedonia', 'Macedonian Denar'),
	('MLI', 'XOF', 'Mali', 'CFA Franc BCEAO'),
	('MLT', 'EUR', 'Malta', 'Euro'),
	('MMR', 'MMK', 'Myanmar', 'Myanmar Kyat'),
	('MNE', 'EUR', 'Montenegro', 'Euro'),
	('MNG', 'MNT', 'Mongolia', 'Mongolian Tugrik'),
	('MNP', 'USD', 'Northern Mariana Islands', 'US Dollar'),
	('MOZ', 'MZN', 'Mozambique', 'Mozambique Metical'),
	('MRT', 'MRO', 'Mauritania', 'Maurutanian Ouguiya'),
	('MSR', 'XCD', 'Montserrat', 'East Caribbean Dollar'),
	('MTQ', 'EUR', 'Martinique', 'Euro'),
	('MUS', 'MUR', 'Mauritius', 'Mauritius Rupee'),
	('MWI', 'MWK', 'Malawi', 'Malawian Kwacha'),
	('MYS', 'MYR', 'Malaysia', 'Malaysian Ringgit'),
	('MYT', 'EUR', 'Mayotte', 'Euro'),
	('NAM', 'NAD', 'Namibia', 'Namibia Dollar'),
	('NCL', 'XPF', 'New Caledonia', 'CFP Franc'),
	('NER', 'XOF', 'Niger', 'CFA Franc BCEAO'),
	('NFK', 'AUD', 'Norfolk Island', 'Australian Dollar'),
	('NGA', 'NGN', 'Nigeria', 'Nigerian Naira'),
	('NIC', 'NIO', 'Nicaragua', 'Nicaraguan Cordoba Oro'),
	('NIU', 'NZD', 'Niue', 'New Zealand Dollar'),
	('NLD', 'EUR', 'Netherlands', 'Euro'),
	('NOR', 'NOK', 'Norway', 'Norwegian Krone'),
	('NPL', 'NPR', 'Nepal', 'Nepalese Rupee'),
	('NRU', 'AUD', 'Nauru', 'Australian Dollar'),
	('NZL', 'NZD', 'New Zealand', 'New Zealand Dollar'),
	('OMN', 'OMR', 'Oman', 'Omani Rial'),
	('PAK', 'PKR', 'Pakistan', 'Pakistan Rupee'),
	('PAN', 'PAB', 'Panama', 'Panamean Balboa'),
	('PCN', 'NZD', 'Pitcairn', 'New Zealand Dollar'),
	('PER', 'PEN', 'Peru', 'Puruvian Nuevo Sol'),
	('PHL', 'PHP', 'Philippines', 'Philippine Peso'),
	('PLW', 'USD', 'Palau', 'US Dollar'),
	('PNG', 'PGK', 'Papua New Guinea', 'Papa New Guinean Kina'),
	('POL', 'PLN', 'Poland', 'Polish Zloty'),
	('PRI', 'USD', 'Puerto Rico', 'US Dollar'),
	('PRK', 'KPW', 'Korea, Democratic People''s Republic Of', 'North Korean Won'),
	('PRT', 'EUR', 'Portugal', 'Euro'),
	('PRY', 'PYG', 'Paraguay', 'Paraguay Guarani'),
	('PYF', 'XPF', 'French Polynesia', 'CFP Franc'),
	('QAT', 'QAR', 'Qatar', 'Qatari Rial'),
	('REU', 'EUR', N'Réunion', 'Euro'),
	('ROU', 'RON', 'Romania', 'New Romanian Leu'),
	('RUS', 'RUB', 'Russian Federation', 'Russian Ruble'),
	('RWA', 'RWF', 'Rwanda', 'Rwandan Franc'),
	('SAU', 'SAR', 'Saudi Arabia', 'Saudi Riyal'),
	('SDN', 'SDG', 'Sudan', 'Sudanese Pound'),
	('SEN', 'XOF', 'Senegal', 'CFA Franc BCEAO'),
	('SGP', 'SGD', 'Singapore', 'Singapore Dollar'),
	('SHN', 'SHP', 'Saint Helena, Ascension and Tristan Da Cunha', 'Saint Helena Pound'),
	('SJM', 'NOK', 'Svalbard And Jan Mayen', 'Norwegian Krone'),
	('SLB', 'SBD', 'Solomon Islands', 'Solomon Islands Dollar'),
	('SLE', 'SLL', 'Sierra Leone', 'Sierra Leone Leone'),
	('SLV', 'SVC', 'El Salvador', 'El Salvador Colon'),
	('SMR', 'EUR', 'San Marino', 'Euro'),
	('SOM', 'SOS', 'Somalia', 'Somali Shilling'),
	('SPM', 'EUR', 'Saint Pierre And Miquelon', 'Euro'),
	('SRB', 'RSD', 'Serbia', 'Serbian Dinar'),
	('SSD', 'SSP', 'South Sudan', 'South Sudanese Pound'),
	('STP', 'STD', 'Sao Tome and Principe', 'Sao Tome and Principe Dobra'),
	('SUR', 'SRD', 'Suriname', 'Surinam Dollar'),
	('SVK', 'EUR', 'Slovakia', 'Euro'),
	('SVN', 'EUR', 'Slovenia', 'Euro'),
	('SWE', 'SEK', 'Sweden', 'Swedish Krona'),
	('SWZ', 'SZL', 'Swaziland', 'Swaziland Lilangeni'),
	('SXM', 'ANG', 'Sint Maarten (Dutch part)', 'Netherlands Antillean Guilder'),
	('SYC', 'SCR', 'Seychelles', 'Seychelles Rupee'),
	('SYR', 'SYP', 'Syrian Arab Republic', 'Syrian Pound'),
	('TCA', 'USD', 'Turks and Caicos Islands', 'US Dollar'),
	('TCD', 'XAF', 'Chad', 'CFA Franc BEAC'),
	('TGO', 'XOF', 'Togo', 'CFA Franc BCEAO'),
	('THA', 'THB', 'Thailand', 'Thai Baht'),
	('TJK', 'TJS', 'Tajikistan', 'Tajikistan Somoni'),
	('TKL', 'NZD', 'Tokelau', 'New Zealand Dollar'),
	('TKM', 'TMT', 'Turkmenistan', 'Turkmenistan New Manat'),
	('TLS', 'USD', 'Timor-Leste', 'US Dollar'),
	('TON', 'TOP', 'Tonga', 'Tongan Pa''anga'),
	('TTO', 'TTD', 'Trinidad and Tobago', 'Trinidad and Tobago Dollar'),
	('TUN', 'TND', 'Tunisia', 'Tunisian Dinar'),
	('TUR', 'TRY', 'Turkey', 'Turkish Lira'),
	('TUV', 'AUD', 'Tuvalu', 'Australian Dollar'),
	('TWN', 'TWD', 'Taiwan', 'New Taiwan Dollar'),
	('TZA', 'TZS', 'Tanzania, United Republic of', 'Tanzanian Shilling'),
	('UGA', 'UGX', 'Uganda', 'Uganda Shilling'),
	('UKR', 'UAH', 'Ukraine', 'Ukranian Hryvnia'),
	('UMI', 'USD', 'United States Minor Outlying Islands', 'US Dollar'),
	('URY', 'UYU', 'Uruguay', 'Uragyan Peso Uruguayo'),
	('USA', 'USD', 'United States', 'US Dollar'),
	('UZB', 'UZS', 'Uzbekistan', 'Uzbekistan Sum'),
	('VAT', 'EUR', 'Holy See (Vatican City State)', 'Euro'),
	('VCT', 'XCD', 'Saint Vincent And The Grenadines', 'East Caribbean Dollar'),
	('VEN', 'VEF', 'Venezuela, Bolivarian Republic of', 'Venezuelan Bolivar Fuerte'),
	('VGB', 'USD', 'Virgin Islands, British', 'US Dollar'),
	('VIR', 'USD', 'Virgin Islands, U.S.', 'US Dollar'),
	('VNM', 'VND', 'Viet Nam', 'Vietnamise Dong'),
	('VUT', 'VUV', 'Vanuatu', 'Vanuatun Vatu'),
	('WLF', 'XPF', 'Wallis and Futuna', 'CFP Franc'),
	('WSM', 'WST', 'Samoa', 'Samoan Tala'),
	('YEM', 'YER', 'Yemen', 'Yemeni Rial'),
	('ZAF', 'ZAR', 'South Africa', 'South African Rand'),
	('ZMB', 'ZMW', 'Zambia', 'New Zambian Kwacha'),
	('ZWE', 'ZWL', 'Zimbabwe', 'Zimbabwe Dollar')
GO

UPDATE C
SET C.DisplayOrder = 
	CASE
		WHEN C.ISOCountryCode3 = 'DEU'
		THEN 1
		WHEN C.ISOCountryCode3 = 'GBR'
		THEN 2
		WHEN C.ISOCountryCode3 = 'USA'
		THEN 3
		ELSE C.DisplayOrder
	END
FROM dropdown.Country C
GO

UPDATE C1
SET C1.ISOCountryCode2 = C2.ISOCountryCode2
FROM dropdown.Country C1
	JOIN LEO0000.dropdown.Country C2 ON C2.ISOCountryCode3 = C1.ISOCountryCode3
GO

UPDATE C
SET C.ISOCountryCode2 = 'BU'
FROM dropdown.Country C
WHERE C.ISOCountryCode3 = 'BES'
GO

UPDATE C
SET C.ISOCountryCode2 = 'EU'
FROM dropdown.Country C
WHERE C.ISOCountryCode3 = 'EUR'
GO

UPDATE C
SET C.ISOCountryCode2 = 'SH'
FROM dropdown.Country C
WHERE C.ISOCountryCode3 = 'SHN'
GO

EXEC utility.DropColumn 'dropdown.Country', 'ISOCurrencyCode'
EXEC utility.DropColumn 'dropdown.Country', 'CurrencyName'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
TRUNCATE TABLE dropdown.CountryCallingCode
GO

EXEC utility.InsertIdentityValue 'dropdown.CountryCallingCode', 'CountryCallingCodeID', 0
GO

INSERT INTO dropdown.CountryCallingCode
	(ISOCountryCode2, CountryCallingCode)
VALUES
	('AD', 376),
	('AE', 971),
	('AF', 93),
	('AG', 1268),
	('AI', 1264),
	('AL', 355),
	('AM', 374),
	('AO', 244),
	('AR', 54),
	('AS', 1684),
	('AT', 43),
	('AU', 61),
	('AW', 297),
	('AX', 358),
	('AZ', 994),
	('BA', 387),
	('BB', 1246),
	('BD', 880),
	('BE', 32),
	('BF', 226),
	('BG', 359),
	('BH', 973),
	('BI', 257),
	('BJ', 229),
	('BL', 590),
	('BM', 1441),
	('BN', 673),
	('BO', 591),
	('BR', 55),
	('BS', 1242),
	('BT', 975),
	('BW', 267),
	('BY', 375),
	('BZ', 501),
	('CA', 1),
	('CC', 61),
	('CD', 243),
	('CF', 236),
	('CG', 242),
	('CH', 41),
	('CI', 225),
	('CK', 682),
	('CL', 56),
	('CM', 237),
	('CN', 86),
	('CO', 57),
	('CR', 506),
	('CU', 53),
	('CV', 238),
	('CW', 5999),
	('CX', 61),
	('CY', 357),
	('CZ', 420),
	('DE', 49),
	('DJ', 253),
	('DK', 45),
	('DM', 1767),
	('DO', 180),
	('DO', 291),
	('DO', 849),
	('DO', 918),
	('DZ', 213),
	('EC', 593),
	('EE', 372),
	('EG', 20),
	('EH', 212),
	('ER', 291),
	('ES', 34),
	('ET', 251),
	('FI', 358),
	('FJ', 679),
	('FK', 500),
	('FM', 691),
	('FO', 298),
	('FR', 33),
	('GA', 241),
	('GB', 44),
	('GD', 1473),
	('GE', 995),
	('GF', 594),
	('GG', 44),
	('GH', 233),
	('GI', 350),
	('GL', 299),
	('GM', 220),
	('GN', 224),
	('GP', 590),
	('GQ', 240),
	('GR', 30),
	('GT', 502),
	('GU', 1671),
	('GW', 245),
	('GY', 592),
	('HK', 852),
	('HN', 504),
	('HR', 385),
	('HT', 509),
	('HU', 36),
	('ID', 62),
	('IE', 353),
	('IL', 972),
	('IM', 44),
	('IN', 91),
	('IO', 246),
	('IQ', 964),
	('IR', 98),
	('IS', 354),
	('IT', 39),
	('JE', 44),
	('JM', 1876),
	('JO', 962),
	('JP', 81),
	('KE', 254),
	('KG', 996),
	('KH', 855),
	('KI', 686),
	('KM', 269),
	('KN', 1869),
	('KP', 850),
	('KR', 82),
	('KW', 965),
	('KY', 1345),
	('KZ', 76),
	('KZ', 77),
	('LA', 856),
	('LB', 961),
	('LC', 1758),
	('LI', 423),
	('LK', 94),
	('LR', 231),
	('LS', 266),
	('LT', 370),
	('LU', 352),
	('LV', 371),
	('LY', 218),
	('MA', 212),
	('MC', 377),
	('MD', 373),
	('ME', 382),
	('MF', 590),
	('MG', 261),
	('MH', 692),
	('MK', 389),
	('ML', 223),
	('MM', 95),
	('MN', 976),
	('MO', 853),
	('MP', 1670),
	('MQ', 596),
	('MR', 222),
	('MS', 1664),
	('MT', 356),
	('MU', 230),
	('MV', 960),
	('MW', 265),
	('MX', 52),
	('MY', 60),
	('MZ', 258),
	('NA', 264),
	('NC', 687),
	('NE', 227),
	('NF', 672),
	('NG', 234),
	('NI', 505),
	('NL', 31),
	('NO', 47),
	('NP', 977),
	('NR', 674),
	('NU', 683),
	('NZ', 64),
	('OM', 968),
	('PA', 507),
	('PE', 51),
	('PF', 689),
	('PG', 675),
	('PH', 63),
	('PK', 92),
	('PL', 48),
	('PM', 508),
	('PN', 64),
	('PR', 17),
	('PR', 871),
	('PR', 939),
	('PT', 351),
	('PW', 680),
	('PY', 595),
	('QA', 974),
	('RE', 262),
	('RO', 40),
	('RS', 381),
	('RU', 7),
	('RW', 250),
	('SA', 966),
	('SB', 677),
	('SC', 248),
	('SD', 249),
	('SE', 46),
	('SG', 65),
	('SI', 386),
	('SJ', 4779),
	('SK', 421),
	('SL', 232),
	('SM', 378),
	('SN', 221),
	('SO', 252),
	('SR', 597),
	('SS', 211),
	('ST', 239),
	('SV', 503),
	('SX', 1721),
	('SY', 963),
	('SZ', 268),
	('TC', 1649),
	('TD', 235),
	('TG', 228),
	('TH', 66),
	('TJ', 992),
	('TK', 690),
	('TL', 670),
	('TM', 993),
	('TN', 216),
	('TO', 676),
	('TR', 90),
	('TT', 1868),
	('TV', 688),
	('TW', 886),
	('TZ', 255),
	('UA', 380),
	('UG', 256),
	('US', 1),
	('UY', 598),
	('UZ', 998),
	('VA', 3),
	('VA', 379),
	('VA', 698),
	('VA', 906),
	('VC', 1784),
	('VE', 58),
	('VG', 1284),
	('VI', 1340),
	('VN', 84),
	('VU', 678),
	('WF', 681),
	('WS', 685),
	('YE', 967),
	('YT', 262),
	('ZA', 27),
	('ZM', 260),
	('ZW', 263)
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
TRUNCATE TABLE dropdown.Currency
GO

EXEC utility.InsertIdentityValue 'dropdown.Currency', 'CurrencyID', 0
GO

INSERT INTO dropdown.Currency 
	(ISOCurrencyCode, CurrencyName, DisplayOrder) 
VALUES 
	('USD', 'US Dollar', 1),
	('AED', 'UAE Dirham', 6),
	('AZN', 'Azerbaijanian Manat', 100),
	('NGN', 'Nigerian Naira', 100),
	('NIO', 'Nicaraguan Cordoba Oro', 100),
	('NOK', 'Norwegian Krone', 100),
	('NPR', 'Nepalese Rupee', 100),
	('NZD', 'New Zealand Dollar', 100),
	('OMR', 'Omani Rial', 100),
	('PAB', 'Panamean Balboa', 100),
	('PEN', 'Puruvian Nuevo Sol', 100),
	('PGK', 'Papa New Guinean Kina', 100),
	('PHP', 'Philippine Peso', 100),
	('BAM', 'Bosnia and Herzegovina Convertible Mark', 100),
	('PKR', 'Pakistan Rupee', 100),
	('PLN', 'Polish Zloty', 100),
	('PYG', 'Paraguay Guarani', 100),
	('QAR', 'Qatari Rial', 100),
	('RON', 'New Romanian Leu', 100),
	('RSD', 'Serbian Dinar', 100),
	('RUB', 'Russian Ruble', 100),
	('RWF', 'Rwandan Franc', 100),
	('SAR', 'Saudi Riyal', 100),
	('SBD', 'Solomon Islands Dollar', 100),
	('BBD', 'Barbados Dollar', 100),
	('SCR', 'Seychelles Rupee', 100),
	('SDG', 'Sudanese Pound', 100),
	('SEK', 'Swedish Krona', 100),
	('SGD', 'Singapore Dollar', 100),
	('SHP', 'Saint Helena Pound', 100),
	('SLL', 'Sierra Leone Leone', 100),
	('SOS', 'Somali Shilling', 100),
	('SRD', 'Surinam Dollar', 100),
	('SSP', 'South Sudanese Pound', 100),
	('STD', 'Sao Tome and Principe Dobra', 100),
	('BDT', 'Bangladeshi Taka', 100),
	('SVC', 'El Salvador Colon', 100),
	('SYP', 'Syrian Pound', 100),
	('SZL', 'Swaziland Lilangeni', 100),
	('THB', 'Thai Baht', 100),
	('TJS', 'Tajikistan Somoni', 100),
	('TMT', 'Turkmenistan New Manat', 100),
	('TND', 'Tunisian Dinar', 100),
	('TOP', 'Tongan PaÃ¢â?¬â?¢anga', 100),
	('TRY', 'Turkish Lira', 4),
	('TTD', 'Trinidad and Tobago Dollar', 100),
	('BGN', 'Bulgarian Lev', 100),
	('TWD', 'New Taiwan Dollar', 100),
	('TZS', 'Tanzanian Shilling', 100),
	('UAH', 'Ukranian Hryvnia', 100),
	('UGX', 'Uganda Shilling', 100),
	('UYU', 'Uragyan Peso Uruguayo', 100),
	('UZS', 'Uzbekistan Sum', 100),
	('VEF', 'Venezuelan Bolivar Fuerte', 100),
	('VND', 'Vietnamise Dong', 100),
	('VUV', 'Vanuatun Vatu', 100),
	('BHD', 'Bahraini Dinar', 100),
	('WST', 'Samoan Tala', 100),
	('XAF', 'CFA Franc BEAC', 100),
	('XCD', 'East Caribbean Dollar', 100),
	('XOF', 'CFA Franc BCEAO', 100),
	('XPF', 'CFP Franc', 100),
	('YER', 'Yemeni Rial', 100),
	('ZAR', 'South African Rand', 100),
	('ZMW', 'New Zambian Kwacha', 100),
	('ZWL', 'Zimbabwe Dollar', 100),
	('BIF', 'Burundi Franc', 100),
	('BMD', 'Bermudian Dollar', 100),
	('BND', 'Brunei Dollar', 100),
	('BOB', 'Bolivian Boliviano', 100),
	('AFN', 'Afghani Afghani', 100),
	('BRL', 'Brazilian Real', 100),
	('BSD', 'Bahamian Dollar', 100),
	('BTN', 'Bhutanise Ngultrum', 100),
	('BWP', 'Botswanan Pula', 100),
	('BYR', 'Belarussian Ruble', 100),
	('BZD', 'Belize Dollar', 100),
	('CAD', 'Canadian Dollar', 100),
	('CDF', 'Congolais Franc', 100),
	('CHF', 'Swiss Franc', 100),
	('CLP', 'Chilean Peso', 100),
	('ALL', 'Albanian Lek', 100),
	('CNY', 'Chinese Yuan Renminbi', 100),
	('COP', 'Colombian Peso', 100),
	('CRC', 'Costa Rican Colon', 100),
	('CUP', 'Cuban Peso', 100),
	('CVE', 'Cape Verde Escudo', 100),
	('CZK', 'Czech Koruna', 100),
	('DJF', 'Djibouti Franc', 100),
	('DKK', 'Danish Krone', 100),
	('DOP', 'Dominican Peso', 100),
	('DZD', 'Algerian Dinar', 100),
	('AMD', 'Armenian Dram', 100),
	('EGP', 'Egyptian Pound', 100),
	('ERN', 'Eritrean Nakfa', 100),
	('ETB', 'Ethiopian Birr', 100),
	('EUR', 'Euro', 3),
	('FJD', 'Fiji Dollar', 100),
	('FKP', 'Falkland Islands Pound', 100),
	('GBP', 'UK Pound Sterling', 2),
	('GEL', 'Geogrian Lari', 100),
	('GHS', 'Ghanan Cedi', 100),
	('GIP', 'Gibraltar Pound', 100),
	('ANG', 'Netherlands Antillean Guilder', 100),
	('GMD', 'Gambian Dalasi', 100),
	('GNF', 'Guinea Franc', 100),
	('GTQ', 'Guatemalian Quetzal', 100),
	('GYD', 'Guyana Dollar', 100),
	('HKD', 'Hong Kong Dollar', 100),
	('HNL', 'Honduran Lempira', 100),
	('HRK', 'Croatian Kuna', 100),
	('HTG', 'Hatian Gourde', 100),
	('HUF', 'Hungarian Forint', 100),
	('IDR', 'Indonesian Rupiah', 100),
	('AOA', 'Angolan Kwanza', 100),
	('ILS', 'Israeli New Shekel', 100),
	('INR', 'Indian Rupee', 100),
	('IQD', 'Iraqi Dinar', 100),
	('IRR', 'Iranian Rial', 100),
	('ISK', 'Iceland Krona', 100),
	('JMD', 'Jamaican Dollar', 100),
	('JOD', 'Jordanian Dinar', 5),
	('JPY', 'Japanese Yen', 100),
	('KES', 'Kenyan Shilling', 100),
	('KGS', 'Kyrgyzstan Som', 100),
	('ARS', 'Argentine Peso', 100),
	('KHR', 'Cambodian Riel', 100),
	('KMF', 'Comoro Franc', 100),
	('KPW', 'North Korean Won', 100),
	('KRW', 'South Korean Won', 100),
	('KWD', 'Kuwaiti Dinar', 100),
	('KYD', 'Cayman Islands Dollar', 100),
	('KZT', 'Kazakhstan Tenge', 100),
	('LAK', 'Lao Kip', 100),
	('LBP', 'Lebanese Pound', 100),
	('LKR', 'Sri Lanka Rupee', 100),
	('AUD', 'Australian Dollar', 100),
	('LRD', 'Liberian Dollar', 100),
	('LSL', 'Lesotho Loti', 100),
	('LTL', 'Lithuanian Litas', 100),
	('LVL', 'Latvian Lats', 100),
	('LYD', 'Libyan Dinar', 100),
	('MAD', 'Moroccan Dirham', 100),
	('MDL', 'Moldovan Leu', 100),
	('MGA', 'Malagasy Ariary', 100),
	('MKD', 'Macedonian Denar', 100),
	('MMK', 'Myanmar Kyat', 100),
	('AWG', 'Aruban Florin', 100),
	('MNT', 'Mongolian Tugrik', 100),
	('MOP', 'Macaoan Pataca', 100),
	('MRO', 'Maurutanian Ouguiya', 100),
	('MUR', 'Mauritius Rupee', 100),
	('MVR', 'Maldiven Rufiyaa', 100),
	('MWK', 'Malawian Kwacha', 100),
	('MXN', 'Mexican Peso', 100),
	('MYR', 'Malaysian Ringgit', 100),
	('MZN', 'Mozambique Metical', 100),
	('NAD', 'Namibia Dollar', 100)
GO
--End table dropdown.Currency

--Begin table dropdown.DocumentType
TRUNCATE TABLE dropdown.DocumentType
GO

EXEC utility.InsertIdentityValue 'dropdown.DocumentType', 'DocumentTypeID', 0
GO

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName) 
VALUES
	('Document Type Category 1', 'DT11', 'Document Type 11'),
	('Document Type Category 1', 'DT12', 'Document Type 12'),
	('Document Type Category 1', 'DT13', 'Document Type 13'),
	('Document Type Category 2', 'DT21', 'Document Type 21'),
	('Document Type Category 2', 'DT22', 'Document Type 22'),
	('Document Type Category 2', 'DT23', 'Document Type 23'),
	('Document Type Category 3', 'DT31', 'Document Type 31'),
	('Document Type Category 3', 'DT32', 'Document Type 32'),
	('Document Type Category 3', 'DT33', 'Document Type 33')
GO
--End table dropdown.DocumentType

--Begin table dropdown.ForceType
TRUNCATE TABLE dropdown.ForceType
GO

EXEC utility.InsertIdentityValue 'dropdown.ForceType', 'ForceTypeID', 0
GO

INSERT INTO dropdown.ForceType 
	(ForceTypeName, HexColor) 
VALUES
	('Armed Groups', '#ff0000'),
	('Local Partners', '#32cd32'),
	('Other Local Organisations', '#ffff00'),
	('International Organisations', '#0000ff')
GO
--End table dropdown.ForceType

--Begin table dropdown.FundingSource
TRUNCATE TABLE dropdown.FundingSource
GO

EXEC utility.InsertIdentityValue 'dropdown.FundingSource', 'FundingSourceID', 0
GO

INSERT INTO dropdown.FundingSource 
	(FundingSourceName) 
VALUES
	('DFID'),
	('FCO'),
	('MOD'),
	('Other HMG'),
	('Other')
GO
--End table dropdown.FundingSource

--Begin table dropdown.ImpactDecision
TRUNCATE TABLE dropdown.ImpactDecision
GO

SET IDENTITY_INSERT dropdown.ImpactDecision ON
GO

INSERT INTO dropdown.ImpactDecision 
	(ImpactDecisionID, ImpactDecisionName, HexColor, DisplayOrder) 
VALUES
	(0, 'Unknown', '#d3d3d3', 0),
	(1, 'Disengagement', '#ff0000', 1),
	(2, 'Partial Disengagement', '#ffa500', 2),
	(3, 'Engagement With Caution', '#ffff00', 3),
	(4, 'Engagement', '#32cd32', 4)
GO

SET IDENTITY_INSERT dropdown.ImpactDecision OFF
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.IncidentType
TRUNCATE TABLE dropdown.IncidentType
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentType', 'IncidentTypeID', 0
GO

INSERT INTO dropdown.IncidentType 
	(IncidentTypeCategory, IncidentTypeName, Icon, DisplayOrder, IsActive) 
VALUES 
	('Civil Activity', 'Civil Activity', 'civilian.png', 0, 1),
	('Civil Activity', 'Civil Disorder', 'civilian.png', 0, 1),
	('Civil Activity', 'Economic Situatio', 'civilian.png', 0, 1),
	('Civil Activity', 'Protest', 'civilian.png', 0, 1),
	('Civil Activity', 'Utility Disruptio', 'civilian.png', 0, 1),
	('Civil-Activity', 'Black Market Activity', 'civilian.png', 0, 1),
	('Civil-Activity', 'General Calm', 'civilian.png', 0, 1),
	('Communications', 'Graffiti', 'communication.png', 0, 1),
	('Communications', 'Internet cafe shut dow', 'communication.png', 0, 1),
	('Communications', 'Leaflet Distributio', 'communication.png', 0, 1),
	('Communications', 'Loudspeaker', 'communication.png', 0, 1),
	('Communications', 'Mosque Loudspeaker', 'communication.png', 0, 1),
	('Communications', 'Ongoing Negotiations', 'communication.png', 0, 1),
	('Communications', 'Posters', 'communication.png', 0, 1),
	('Communications', 'Published statements', 'communication.png', 0, 1),
	('Communications', 'Radio announcement', 'communication.png', 0, 1),
	('Communications', 'TV announcement', 'communication.png', 0, 1),
	('Communications', 'TV satellite dishes dismantled', 'communication.png', 0, 1),
	('Communications', 'USBs/CDs', 'communication.png', 0, 1),
	('Communications', 'Video', 'communication.png', 0, 1),
	('Communications', 'WiFi dismantled', 'communication.png', 0, 1),
	('Criminality', 'Arms Smuggling', 'crime.png', 0, 1),
	('Criminality', 'Beating', 'crime.png', 0, 1),
	('Criminality', 'Confiscatio', 'crime.png', 0, 1),
	('Criminality', 'Executio', 'crime.png', 0, 1),
	('Criminality', 'Murder', 'crime.png', 0, 1),
	('Criminality', 'Oil smuggling', 'crime.png', 0, 1),
	('Criminality', 'Robbery', 'crime.png', 0, 1),
	('Criminality', 'Smuggling', 'crime.png', 0, 1),
	('IED', 'Mines', 'ied.png', 0, 1),
	('IED', 'RCIED (Remote Controlled IED)', 'ied.png', 0, 1),
	('IED', 'SIED (Suicide IED)', 'ied.png', 0, 1),
	('IED', 'SVBIED (Suicide Vehicle-borne IED)', 'ied.png', 0, 1),
	('IED', 'UVIED (Under Vehicle IED)', 'ied.png', 0, 1),
	('IED', 'VBIED (Vehicle-borne IED)', 'ied.png', 0, 1),
	('Insurgent Tactics', 'Assassinations', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Casualty', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Detainees Released', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Forced Recruitment', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Hit and Ru', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Illegal Confiscatio', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Kidnapping', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Mortality', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Poisoning', 'insurgent.png', 0, 1),
	('Insurgent Tactics', 'Sabotage', 'insurgent.png', 0, 1),
	('Military Activity - Air', 'Airstrike - Coalitio', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Airstrike - Israeli', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Airstrike - Other/Undefined', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Airstrike - Russia', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Airstrike - SAAF', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Airstrike - Turkish', 'military-air.png', 0, 1),
	('Military Activity - Air', 'Barrel Bomb', 'military-air.png', 0, 1),
	('Military Activity - Ground', 'Ambush', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Armed Clashes - Ground defense', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Armed Clashes - Ground offensive', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Artillery/Mortar', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Besiegement', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Ceasefire/Truce violatio', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Ceasefire/Truce', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Grenade', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'New Appointment', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Raid', 'military-ground.png', 0, 1),
	('Military Activity - Ground', 'Rocket Strike', 'military-ground.png', 0, 1),
	('Military Activity-Ground', 'Alliance', 'military-ground.png', 0, 1),
	('Military Activity-Ground', 'Defectio', 'military-ground.png', 0, 1),
	('Military Activity-Ground', 'Troop Movement', 'military-ground.png', 0, 1),
	('Security Sector Activity', 'Arrest', 'police.png', 0, 1),
	('Security Sector Activity', 'Corruptio', 'police.png', 0, 1),
	('Security Sector Activity', 'Detentio', 'police.png', 0, 1),
	('Security Sector Activity', 'Extortio', 'police.png', 0, 1),
	('Security Sector Activity', 'Illegal Detentio', 'police.png', 0, 1),
	('Security Sector Activity', 'Municipal incapacity/incapability', 'police.png', 0, 1),
	('Security Sector Activity', 'Surrender', 'police.png', 0, 1)
GO
--End table dropdown.IncidentType

--Begin table dropdown.InformationValidity
TRUNCATE TABLE dropdown.InformationValidity
GO

EXEC utility.InsertIdentityValue 'dropdown.InformationValidity', 'InformationValidityID', 0
GO

INSERT INTO dropdown.InformationValidity 
	(InformationValidityName, DisplayOrder, IsActive) 
VALUES
	('Known to be true without reservation', 1, 1),
	('The information is known personally by the resource but not to the person reporting', 2, 1),
	('The information is not known personally to the resource but can be corroborated by other information', 3, 1),
	('The information cannot be judged', 4, 1),
	('Suspected to be false', 5, 1)
GO
--End table dropdown.InformationValidity

--Begin table dropdown.LearnerProfileType
TRUNCATE TABLE dropdown.LearnerProfileType
GO

EXEC utility.InsertIdentityValue 'dropdown.LearnerProfileType', 'LearnerProfileTypeID', 0
GO

INSERT INTO dropdown.LearnerProfileType 
	(LearnerProfileTypeName, DisplayOrder, IsActive) 
VALUES
	('Stringers', 1, 1),
	('MAO Media Officers', 2, 1),
	('MAO Leadership', 3, 1),
	('Internal Staff', 4, 1),
	('Consultants', 5, 1),
	('Syrian Public', 6, 1)
GO
--End table dropdown.LearnerProfileType

--Begin table dropdown.ModuleType
TRUNCATE TABLE dropdown.ModuleType
GO

EXEC utility.InsertIdentityValue 'dropdown.ModuleType', 'ModuleTypeID', 0
GO

INSERT INTO dropdown.ModuleType 
	(ModuleTypeName) 
VALUES
	('Training'),
	('Workshop')
GO
--End table dropdown.ModuleType

--Begin table dropdown.PasswordSecurityQuestion
TRUNCATE TABLE dropdown.PasswordSecurityQuestion
GO

EXEC utility.InsertIdentityValue 'dropdown.PasswordSecurityQuestion', 'PasswordSecurityQuestionID', 0
GO

INSERT INTO dropdown.PasswordSecurityQuestion 
	(GroupID, PasswordSecurityQuestionName, DisplayOrder) 
VALUES 
	(1, 'What is the name of your first niece/nephew?', 1),
	(1, 'What is your maternal grandmother''s first name?', 2),
	(1, 'What is your maternal grandfather''s first name?', 3),
	(1, 'What was the name of your first pet?', 4),
	(2, 'What is your father''s middle name?', 1),
	(2, 'What is your mother''s middle name?', 2),
	(2, 'In what city were you married?', 3),
	(2, 'What is the first name of your first child?', 4),
	(3, 'In what city was your mother born? (Enter full name of city only)', 1),
	(3, 'In what city was your father born?  (Enter full name of city only)', 2),
	(3, 'In what city was your high school? (Enter only "Charlotte" for Charlotte High School)', 3),
	(3, 'In what city were you born? (Enter full name of city only)', 4)
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.ProgramType
TRUNCATE TABLE dropdown.ProgramType
GO

EXEC utility.InsertIdentityValue 'dropdown.ProgramType', 'ProgramTypeID', 0
GO

INSERT INTO dropdown.ProgramType 
	(ProgramTypeName, DisplayOrder, IsActive) 
VALUES
	('Production', 1, 1),
	('Trauma', 2, 1),
	('Security', 3, 1),
	('Media Awareness', 4, 1)
GO
--End table dropdown.ProgramType

--Begin table dropdown.Project
TRUNCATE TABLE dropdown.Project
GO

EXEC utility.InsertIdentityValue 'dropdown.Project', 'ProjectID', 0
GO

INSERT INTO dropdown.Project 
	(ProjectCode, ProjectName) 
VALUES
	('Project1', 'Project 1'),
	('Project2', 'Project 2'),
	('Project3', 'Project 3')
GO
--End table dropdown.Project

--Begin table dropdown.ResourceProvider
TRUNCATE TABLE dropdown.ResourceProvider
GO

EXEC utility.InsertIdentityValue 'dropdown.ResourceProvider', 'ResourceProviderID', 0
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderName, DisplayOrder) 
VALUES

	('GOI', 0),
	('GOK', 0),
	('Groups', 0),
	('IC North', 0),
	('IC South', 0),
	('Individuals', 0),
	('Iran', 0),
	('Israel', 0),
	('Qatar', 0),
	('Russia', 0),
	('Saudi Arabia', 0),
	('Turkey', 0),
	('UAE', 0),
	('UK', 0),
	('US', 0),
	('Other', 99)
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName) 
VALUES
	('Extremism and extremist groups'),
	('Foreign intervention'),
	('Fragmentation of regime control'),
	('Gender and women''s issues'),
	('Humanitarian relief'),
	('Livelihoods and economy'),
	('Military advances/retreats'),
	('Opposition fragmentation'),
	('Political shifts'),
	('Religious minorities'),
	('Sectarianism'),
	('Services (e.g. electricity, water)'),
	('Truces and evacuation agreements')
GO
--End table dropdown.RelevantTheme

--Begin table dropdown.RequestForInformationResultType
TRUNCATE TABLE dropdown.RequestForInformationResultType
GO

EXEC utility.InsertIdentityValue 'dropdown.RequestForInformationResultType', 'RequestForInformationResultTypeID', 0
GO

INSERT INTO dropdown.RequestForInformationResultType 
	(RequestForInformationResultTypeCode,RequestForInformationResultTypeName,DisplayOrder)
VALUES
	('RFIResponse', 'RFI Response', 1),
	('SpotReport', 'Spot Report', 2),
	('Assessment', 'Critical Assessment', 3)
GO
--End table dropdown.RequestForInformationResultType

--Begin table dropdown.RequestForInformationStatus
TRUNCATE TABLE dropdown.RequestForInformationStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.RequestForInformationStatus', 'RequestForInformationStatusID', 0
GO

INSERT INTO dropdown.RequestForInformationStatus 
	(RequestForInformationStatusCode,RequestForInformationStatusName,DisplayOrder)
VALUES
	('New', 'New', 1),
	('InProgress', 'In Progress', 2),
	('Completed', 'Completed', 3)
GO
--End table dropdown.RequestForInformationStatus

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleName) 
VALUES
	('Administrator'),
	('Analyst'),
	('Donor'),
	('Project Management'),
	('Program Officer')
GO
--End table dropdown.Role

--Begin table dropdown.SourceType
TRUNCATE TABLE dropdown.SourceType
GO

EXEC utility.InsertIdentityValue 'dropdown.SourceType', 'SourceTypeID', 0
GO

INSERT INTO dropdown.SourceType 
	(SourceTypeName, DisplayOrder) 
VALUES
	('Media', 0),
	('Network', 0),
	('Other', 99),
	('Stringer', 0),
	('Unit', 0)
GO
--End table dropdown.SourceType

--Begin table dropdown.StatusChange
TRUNCATE TABLE dropdown.StatusChange
GO

EXEC utility.InsertIdentityValue 'dropdown.StatusChange', 'StatusChangeID', 0
GO

INSERT INTO dropdown.StatusChange 
	(StatusChangeName, HexColor, Direction, DisplayOrder, IsActive) 
VALUES
	('Continued Engagement', '#3333CC', 'left', 3, 1),
	('Continued No Engagement', '#663333', 'left', 5, 1),
	('Engage to No Engagement', '#663333', 'down', 2, 1),
	('No Engagement to Engage', '#66FF99', 'up', 1, 1),
	('Unknown/Pending RAP', '#999999', 'circle', 4, 1)
GO
--End table dropdown.StatusChange

--Begin table dropdown.TerritoryStatus
TRUNCATE TABLE dropdown.TerritoryStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.TerritoryStatus', 'TerritoryStatusID', 0
GO

INSERT INTO dropdown.TerritoryStatus 
	(TerritoryStatusCode, TerritoryStatusName, DisplayOrder)
VALUES
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Inactive', 'Inactive', 3)
GO
--End table dropdown.TerritoryStatus

--Begin table dropdown.TerritoryType
TRUNCATE TABLE dropdown.TerritoryType
GO

INSERT dropdown.TerritoryType 
	(TerritoryTypeCode, TerritoryTypeCodePlural, TerritoryTypeName, TerritoryTypeNamePlural, IsReadOnly, DisplayOrder) 
VALUES 
	('Governorate', 'Governorates', 'Governorate', 'Governorates', 1, 1),
	('Community', 'Communities', 'Community', 'Communities', 0, 2)
GO
--End table dropdown.TerritoryType

--Begin table dropdown.UnitType
TRUNCATE TABLE dropdown.UnitType
GO

EXEC utility.InsertIdentityValue 'dropdown.UnitType', 'UnitTypeID', 0
GO

INSERT INTO dropdown.UnitType 
	(UnitTypeName, DisplayOrder, IsActive) 
VALUES
	('Division', 1, 1),
	('Brigade', 2, 1),
	('Unit', 3, 1),
	('Sub-Unit', 4, 1),
	('Operations Room', 5, 1),
	('Other', 6, 1)
GO
--End table dropdown.UnitType
