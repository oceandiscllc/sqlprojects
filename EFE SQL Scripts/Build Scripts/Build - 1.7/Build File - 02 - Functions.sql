USE EFE
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
--
-- Author:			Adam Davis
-- Create date:	2015.10.19
-- Description:	fixed reference to dbo.Contact (and fixed back)
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal NVARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF @Format = 'LastFirstMiddle' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID

--Begin function kpi.GetEnrolledCount
EXEC Utility.DropObject 'kpi.GetEnrolledCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of enrolled students
-- ===========================================================

CREATE FUNCTION kpi.GetEnrolledCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nEnrolledCount INT = 0

	SELECT @nEnrolledCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName <> 'Withdrawn'
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL on PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nEnrolledCount, 0)

END
GO
--End function kpi.GetEnrolledCount

--Begin function kpi.GetGraduatedCount
EXEC Utility.DropObject 'kpi.GetGraduatedCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of graduated students
-- ============================================================

CREATE FUNCTION kpi.GetGraduatedCount
(
@AffiliateID INT,
@ProgramID INT,
@Gender NVARCHAR(20)
)

RETURNS INT

AS
BEGIN

	DECLARE @nGraduatedCount INT = 0

	SELECT @nGraduatedCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName = 'Alumni'
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR CL.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND (@Gender = '' OR CO.Gender = @Gender)

	RETURN ISNULL(@nGraduatedCount, 0)

END
GO
--End function kpi.GetGraduatedCount

--Begin function kpi.GetRegisteredCount
EXEC Utility.DropObject 'kpi.GetRegisteredCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- =============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of registered students
-- =============================================================

CREATE FUNCTION kpi.GetRegisteredCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nRegisteredCount INT = 0

	SELECT @nRegisteredCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC 
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nRegisteredCount, 0)

END
GO
--End function kpi.GetRegisteredCount
