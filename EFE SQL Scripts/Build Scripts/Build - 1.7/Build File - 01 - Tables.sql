USE EFE
GO

--Begin table program.ProgramClass
DECLARE @TableName VARCHAR(250) = 'program.ProgramClass'

EXEC utility.DropObject @TableName

CREATE TABLE program.ProgramClass
	(
	ProgramClassID INT NOT NULL IDENTITY(1,1),
	ProgramID INT,
	ClassID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProgramClassID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProgramClass', 'ProgramID,ClassID'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID JOIN sys.Columns C ON C.Object_ID = T.Object_ID AND S.Name = 'program' AND T.Name = 'Class' AND C.Name = 'ProgrmaID')
	BEGIN

	INSERT INTO program.ProgramClass
		(ProgramID,ClassID)
	SELECT
		C.ProgramID,
		C.ClassID
	FROM program.Class C
	WHERE C.ProgramID > 0
		AND C.ClassID > 0

	END
--ENDIF
GO
--End table program.ProgramClass

--Begin table program.Class
DECLARE @TableName VARCHAR(250) = 'program.Class'

EXEC utility.DropColumn @TableName, 'ProgramID'
GO
--End table program.Class
