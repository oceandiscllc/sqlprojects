-- File Name:	Build-1.7 File 01 - EFE.sql
-- Build Key:	Build-1.7 File 01 - EFE - 2016.10.17 10.27.30

USE EFE
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatContactNameByContactID
--		kpi.GetEnrolledCount
--		kpi.GetGraduatedCount
--		kpi.GetRegisteredCount
--
-- Procedures:
--		eventlog.LogProgramAction
--		form.GetFormData
--		program.GetClassByClassID
--		program.GetClasses
--		program.GetProgramByProgramID
--
-- Tables:
--		program.ProgramClass
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table program.ProgramClass
DECLARE @TableName VARCHAR(250) = 'program.ProgramClass'

EXEC utility.DropObject @TableName

CREATE TABLE program.ProgramClass
	(
	ProgramClassID INT NOT NULL IDENTITY(1,1),
	ProgramID INT,
	ClassID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProgramClassID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProgramClass', 'ProgramID,ClassID'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID JOIN sys.Columns C ON C.Object_ID = T.Object_ID AND S.Name = 'program' AND T.Name = 'Class' AND C.Name = 'ProgrmaID')
	BEGIN

	INSERT INTO program.ProgramClass
		(ProgramID,ClassID)
	SELECT
		C.ProgramID,
		C.ClassID
	FROM program.Class C
	WHERE C.ProgramID > 0
		AND C.ClassID > 0

	END
--ENDIF
GO
--End table program.ProgramClass

--Begin table program.Class
DECLARE @TableName VARCHAR(250) = 'program.Class'

EXEC utility.DropColumn @TableName, 'ProgramID'
GO
--End table program.Class

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
--
-- Author:			Adam Davis
-- Create date:	2015.10.19
-- Description:	fixed reference to dbo.Contact (and fixed back)
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal NVARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF @Format = 'LastFirstMiddle' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID

--Begin function kpi.GetEnrolledCount
EXEC Utility.DropObject 'kpi.GetEnrolledCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of enrolled students
-- ===========================================================

CREATE FUNCTION kpi.GetEnrolledCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nEnrolledCount INT = 0

	SELECT @nEnrolledCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName <> 'Withdrawn'
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL on PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nEnrolledCount, 0)

END
GO
--End function kpi.GetEnrolledCount

--Begin function kpi.GetGraduatedCount
EXEC Utility.DropObject 'kpi.GetGraduatedCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of graduated students
-- ============================================================

CREATE FUNCTION kpi.GetGraduatedCount
(
@AffiliateID INT,
@ProgramID INT,
@Gender NVARCHAR(20)
)

RETURNS INT

AS
BEGIN

	DECLARE @nGraduatedCount INT = 0

	SELECT @nGraduatedCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName = 'Alumni'
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR CL.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND (@Gender = '' OR CO.Gender = @Gender)

	RETURN ISNULL(@nGraduatedCount, 0)

END
GO
--End function kpi.GetGraduatedCount

--Begin function kpi.GetRegisteredCount
EXEC Utility.DropObject 'kpi.GetRegisteredCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- =============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of registered students
-- =============================================================

CREATE FUNCTION kpi.GetRegisteredCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nRegisteredCount INT = 0

	SELECT @nRegisteredCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC 
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nRegisteredCount, 0)

END
GO
--End function kpi.GetRegisteredCount

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure eventlog.LogProgramAction
EXEC Utility.DropObject 'eventlog.LogProgramAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:			Eric Jones
-- Create date: 2016.01.03
-- Description:	altered the ProgramClass section since classes are no longer many-many with programs
--
-- Author:			Eric Jones
-- Create date: 2016.01.03
-- Description:	altered the ProgramClass section since classes are NOW many-many with programs
-- =================================================================================================
CREATE PROCEDURE eventlog.LogProgramAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Program',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cProgramClasses VARCHAR(MAX) 
	
		SELECT 
			@cProgramClasses = COALESCE(@cProgramClasses, '') + D.ProgramClass 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProgramClass'), ELEMENTS) AS ProgramClass
			FROM program.ProgramClass T 
			WHERE T.ProgramID = @EntityID
			) D

		DECLARE @cProgramOrganizations VARCHAR(MAX) 
	
		SELECT 
			@cProgramOrganizations = COALESCE(@cProgramOrganizations, '') + D.ProgramOrganization 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProgramOrganization'), ELEMENTS) AS ProgramOrganization
			FROM program.ProgramOrganization T 
			WHERE T.ProgramID = @EntityID
			) D

		DECLARE @cProgramSubAwards VARCHAR(MAX) 
	
		SELECT 
			@cProgramSubAwards = COALESCE(@cProgramSubAwards, '') + D.ProgramSubAward 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProgramSubAward'), ELEMENTS) AS ProgramSubAward
			FROM program.ProgramSubAward T 
			WHERE T.ProgramID = @EntityID
			) D

		DECLARE @cProgramSurveys VARCHAR(MAX) 
	
		SELECT 
			@cProgramSurveys = COALESCE(@cProgramSurveys, '') + D.ProgramSurvey 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProgramSurvey'), ELEMENTS) AS ProgramSurvey
			FROM program.ProgramSurvey T 
			WHERE T.ProgramID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog

			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Program',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProgramClasses>' + ISNULL(@cProgramClasses, '') + '</ProgramClasses>') AS XML),
			CAST(('<ProgramOrganizations>' + ISNULL(@cProgramOrganizations, '') + '</ProgramOrganizations>') AS XML),
			CAST(('<ProgramSubAwards>' + ISNULL(@cProgramSubAwards, '') + '</ProgramSubAwards>') AS XML),
			CAST(('<ProgramSurveys>' + ISNULL(@cProgramSurveys, '') + '</ProgramSurveys>') AS XML)
			FOR XML RAW('Program'), ELEMENTS
			)
		FROM program.Program T
		WHERE T.ProgramID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProgramAction

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						4 AS GroupNumber, 
						T4.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T4 ON T4.TrainerID = C.TrainerID4
							AND C.TrainerID4 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						5 AS GroupNumber, 
						T5.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T5 ON T5.TrainerID = C.TrainerID5
							AND C.TrainerID5 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 

		CASE
			WHEN TQ.ObjectCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(TQ.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE TQ.Text
		END AS TEXT,

		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText,
		ISNULL((SELECT Q.MaximumResponseCount FROM form.Question Q WHERE Q.QuestionCode = TQ.ObjectCode), 0) AS MaximumResponseCount
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			WHEN RD.QuestionCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(RD.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'



							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure program.GetClassByClassID
EXEC Utility.DropObject 'program.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Class table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added studentStatus data
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added status date and formatted status date
--
-- Author:			Todd Pires
-- Create date:	2016.04.07
-- Description:	Implemented the TrainerID columns
--
-- Author:			Todd Pires
-- Create date:	2016.08.09
-- Description:	Implemented more TrainerID columns
--
-- Author:			Kevin Ross
-- Create date:	2016.08.16
-- Description:	Implemented the StatusReason columns
-- ===============================================================================================
CREATE PROCEDURE program.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(CL.AffiliateID) AS AffiliateName,
		CL.ClassID,
		CL.ClassName,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location,
		CL.Seats,
		CL.Sessions,
		CL.StartDate,
		CL.EndDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CO.CourseID,
		CO.CourseName,
		T1.TrainerID AS TrainerID1,
		T1.TrainerName AS TrainerName1,
		T2.TrainerID AS TrainerID2,
		T2.TrainerName AS TrainerName2,
		T3.TrainerID AS TrainerID3,
		T3.TrainerName AS TrainerName3,
		T4.TrainerID AS TrainerID4,
		T4.TrainerName AS TrainerName4,
		T5.TrainerID AS TrainerID5,
		T5.TrainerName AS TrainerName5
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN dropdown.Trainer T1 ON T1.TrainerID = CL.TrainerID1
		JOIN dropdown.Trainer T2 ON T2.TrainerID = CL.TrainerID2
		JOIN dropdown.Trainer T3 ON T3.TrainerID = CL.TrainerID3
		JOIN dropdown.Trainer T4 ON T4.TrainerID = CL.TrainerID4
		JOIN dropdown.Trainer T5 ON T5.TrainerID = CL.TrainerID5
	WHERE CL.ClassID = @ClassID
	
	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		CC.StudentStatusID,
		SS.StudentStatusName,
		CC.StatusDate,
		CC.StatusReason,
		dbo.FormatDate(CC.StatusDate) AS StatusDateFormatted
	FROM program.ClassContact CC
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID= CC.StudentStatusID
			AND CL.ClassID = @ClassID

	SELECT
		P.ProgramName,
		P.ProgramID
	FROM program.ProgramClass PC
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
			AND CL.ClassID = @ClassID

END
GO
--End procedure program.GetClassByClassID

--Begin procedure program.GetClasses
EXEC Utility.DropObject 'program.GetClasses'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:		Eric Jones
-- Create date:	2015.12.29
-- Description:	A stored procedure to get data from the dbo.Class table
-- =====================================================================
CREATE PROCEDURE program.GetClasses

@ProgramID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ClassID,
		C.ClassName as ClassName1,
		CO.CourseName as ClassName
	FROM program.Class C 
		JOIN program.Course CO ON CO.CourseID = C.CourseID
		JOIN program.ProgramClass PC ON PC.ClassID = C.ClassID
	WHERE PC.programID != @ProgramID
	ORDER BY ClassName, C.ClassID

END
GO
--End procedure program.GetClasses

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2016.09.21
-- Description:	converted from 1 to many programs to many to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		FL.FormText AS FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode 
			AND FL.Locale = @Locale
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate
	FROM program.ProgramClass PC
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		C.ContactID
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID

	--ProgramTypeForms
	SELECT
		F.FormID,
		FL.FormText AS FormName
	FROM form.Form F
		JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = @Locale
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'main', 'SSRSLink', NULL, 'main.SSRSLink', 'main.SSRSLink', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'AssociateAll', 'Contact.Associate.AssociateAll', 'Enable the Associate All button', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'NonBeneficiary', 'Contact.List.NonBeneficiary', 'View the list of contacts not of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', 'Budget', 'Award.AddUpdate.Budget', 'Add / edit an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', 'Budget', 'Award.View.Budget', 'View an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'Budget', 'Proposal.AddUpdate.Budget', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', 'Budget', 'Proposal.View.Budget', 'View a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', 'Budget', 'SubAward.AddUpdate.Budget', 'Add / edit a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', 'Budget', 'SubAward.View.Budget', 'View a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'AdministerForm', NULL, 'Form.AdministerForm', 'Create responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'SaveForm', NULL, 'Form.SaveForm', 'Save responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.7 File 01 - EFE - 2016.10.17 10.27.30')
GO
--End build tracking

