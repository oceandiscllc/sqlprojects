-- File Name:	Build-1.8 File 01 - EFE.sql
-- Build Key:	Build-1.8 File 01 - EFE - 2016.10.30 22.27.09

USE EFE
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetApplicantManagementFilterData
--		dbo.GetContactByContactID
--		dropdown.GetAffiliateData
--
-- Tables:
--		dbo.ContactProgramType
--		form.KPIApplicantManagement
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table dbo.ContactProgramType
DECLARE @TableName VARCHAR(250) = 'dbo.ContactProgramType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactProgramType
	(
	ContactProgramTypeID INT NOT NULL IDENTITY(1,1),
	ContactID INT,
	ProgramTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramTypeID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_ContactProgramType', 'ContactID,ProgramTypeID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactProgramTypeID'
GO
--End table dbo.ContactProgramType

--Begin table dropdown.Affiliate
DECLARE @TableName VARCHAR(250) = 'dropdown.Affiliate'

EXEC utility.AddColumn @TableName, 'AffiliateCountryCode', 'CHAR(2)'
GO

UPDATE A
SET A.AffiliateCountryCode = C.ISOCountryCode2
FROM dropdown.Affiliate A
	JOIN dropdown.Country C ON C.CountryName = A.AffiliateName
GO
--End table dropdown.Affiliate

--Begin table form.KPIApplicantManagement
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIApplicantManagement')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIApplicantManagement'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIApplicantManagement
		(
		KPIApplicantManagementID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID VARCHAR(50),
		ContactID VARCHAR(50),
		Q007 VARCHAR(50),
		Q009 VARCHAR(50),
		Q011 VARCHAR(MAX),
		Q013 VARCHAR(MAX),
		Q018 VARCHAR(50),
		Q020 VARCHAR(50),
		Q021 VARCHAR(50),
		Q023 VARCHAR(50),
		Q180 VARCHAR(50)
/*
		HighestLevelOfEducation VARCHAR(50),
		EnrolledAtUniversity VARCHAR(50),
		Specialization VARCHAR(MAX),
		Languages VARCHAR(MAX),
		TimeJobSeeking VARCHAR(50),
		AveragePersonalMonthlyIncome VARCHAR(50),
		AverageHouseholdMonthlyIncome VARCHAR(50),
		HadPaidJob VARCHAR(50),
		CurrentPrincipalActivity VARCHAR(50)
*/
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIApplicantManagement', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIApplicantManagementID'

	END
--ENDIF
GO
--End table form.KPIApplicantManagement

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure dbo.GetApplicantManagementFilterData
EXEC Utility.DropObject 'dbo.GetApplicantManagementFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.27
-- Description:	A stored procedure to get applicant management filter data
-- =======================================================================
CREATE PROCEDURE dbo.GetApplicantManagementFilterData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		Q.QuestionCode,
		QL.QuestionText,
		QRL.AffiliateCountryCode,
		QRL.QuestionResponseCode,
		QRL.QuestionResponseText
	FROM form.Question Q
		JOIN form.QuestionLabel QL ON QL.QuestionCode = Q.QuestionCode
		JOIN form.QuestionResponse QR ON QR.QuestionCode = Q.QuestionCode
		JOIN form.QuestionResponseLabel QRL ON QRL.QuestionResponseCode = QR.QuestionResponseCode
			AND Q.QuestionCode IN ('Q007','Q009','Q011','Q013','Q018','Q020','Q021','Q023','Q180')
			AND QL.Locale = 'EN'
			AND QRL.Locale = QL.Locale
	ORDER BY Q.QuestionCode, QRL.DisplayOrder

END
GO
--End procedure dbo.GetApplicantManagementFilterData

--Begin dropdown.GetAffiliateData
EXEC Utility.DropObject 'dropdown.GetAffiliateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.30
-- Description:	A stored procedure to return data from the dropdown.Affiliate table
--
-- Author:			Todd Pires
-- Create date:	2016.10.28
-- Description:	Added the AffiliateCountryCode field
-- ================================================================================
CREATE PROCEDURE dropdown.GetAffiliateData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AffiliateID, 
		T.AffiliateCountryCode,
		T.AffiliateName
	FROM dropdown.Affiliate T
	WHERE (T.AffiliateID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AffiliateName, T.AffiliateID

END
GO
--End dropdown.GetAffiliateData

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.01
-- Description:	A stored procedure to data from the contact.Contact table
--
-- Author:			Adam Davis
-- Create date: 2015.10.19
-- Description: Fixed references to OtherPhoneNumber and dbo.Contact
-- ======================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.LastName,
		C1.MiddleName,
		C1.OtherPhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.SkypeName,
		C1.Username,
		C1.InvalidLoginAttempts,
		C1.IsAccountLockedOut,
		C1.IsActive,
		C1.AffiliateID,
		C2.CountryID AS CitizenshipCountryID,
		C2.CountryName AS CitizenshipCountryName,
		C3.CountryID,
		C3.CountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCodeCode,
		CCC1.CountryCallingCodeID AS CellPhoneNumberCountryCallingCodeID,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCodeCode,
		CCC2.CountryCallingCodeID AS FaxNumberCountryCallingCodeID,
		CCC3.CountryCallingCode AS OtherPhoneNumberCountryCallingCodeCode,
		CCC3.CountryCallingCodeID AS OtherPhoneNumberCountryCallingCodeID,
		MS.MaritalStatusID,
		MS.MaritalStatusName,
		CT.ContactTypeID,
		CT.ContactTypeName,
		C1.AffiliateID,
		A.AffiliateName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.OtherPhoneNumberCountryCallingCodeID
		JOIN dropdown.MaritalStatus MS ON MS.MaritalStatusID = C1.MaritalStatusID
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = C1.ContactTypeID
		JOIN dropdown.Affiliate A on A.AffiliateID = C1.AffiliateID
			AND C1.ContactID = @ContactID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID

	SELECT
		CPT.ContactProgramTypeID,
		PT.ProgramTypeID,
		PT.ProgramTypeName
	FROM dbo.ContactProgramType CPT 
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = CPT.ProgramTypeID
			AND CPT.ContactID = @ContactID

	SELECT
		KPI.KPIContactDetailID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.ContactRoles, 'EN') AS ContactRolesLabel,
		form.GetQuestionResponseLabel(KPI.PersonalIncomeBand, 'EN') AS PersonalIncomeBandLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIContactDetail KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIEnterpriseID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.BusinessEndDate) AS BusinessEndDateFormatted, 
		dbo.FormatDate(KPI.BusinessStartDate) AS BusinessStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.BusinessFinancing, 'EN') AS BusinessFinancingLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRegistered, 'EN') AS BusinessRegisteredLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRevenueGenerating, 'EN') AS BusinessRevenueGeneratingLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIEnterprise KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPlacementID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.EmploymentEndDate) AS EmploymentEndDateFormatted, 
		dbo.FormatDate(KPI.EmploymentStartDate) AS EmploymentStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.HadPreviosJob, 'EN') AS HadPreviosJobLabel,
		form.GetQuestionResponseLabel(KPI.JobType, 'EN') AS JobTypeLabel,
		form.GetQuestionResponseLabel(KPI.WorkedThreeMonths, 'EN') AS WorkedThreeMonthsLabel,
		KPI.Employer,
		KPI.JobTitle,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPlacement KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPrepID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.CareerPath, 'EN') AS CareerPathLabel,
		form.GetQuestionResponseLabel(KPI.InterviewingSkills, 'EN') AS InterviewingSkillsLabel,
		form.GetQuestionResponseLabel(KPI.JobSearch, 'EN') AS JobSearchLabel,
		form.GetQuestionResponseLabel(KPI.WritingResume, 'EN') AS WritingResumeLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPrep KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

END
GO
--End procedure dbo.GetContactByContactID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--Begin table form.KPIApplicantManagement
IF NOT EXISTS (SELECT 1 FROM form.KPIQuestion KPIQ WHERE KPIQ.KPICategory = 'KPIApplicantManagement')
	BEGIN

	INSERT INTO form.KPIQuestion
		(QuestionCode, KPICategory, KPICode)
	SELECT 
		Q.QuestionCode,
		'KPIApplicantManagement',
		Q.QuestionCode

/*
		CASE
			WHEN Q.QuestionCode IN ('Q007')
			THEN 'HighestLevelOfEducation'
			WHEN Q.QuestionCode IN ('Q009')
			THEN 'EnrolledAtUniversity'
			WHEN Q.QuestionCode IN ('Q011')
			THEN 'Specialization'
			WHEN Q.QuestionCode IN ('Q013')
			THEN 'Languages'
			WHEN Q.QuestionCode IN ('Q018')
			THEN 'TimeJobSeeking'
			WHEN Q.QuestionCode IN ('Q020')
			THEN 'AveragePersonalMonthlyIncome'
			WHEN Q.QuestionCode IN ('Q021')
			THEN 'AverageHouseholdMonthlyIncome'
			WHEN Q.QuestionCode IN ('Q023')
			THEN 'HadPaidJob'
			WHEN Q.QuestionCode IN ('Q180')
			THEN 'CurrentPrincipalActivity'
		END
*/

	FROM form.Question Q
	WHERE Q.QuestionCode IN ('Q007','Q009','Q011','Q013','Q018','Q020','Q021','Q023','Q180')

	END
--ENDIF
GO
--End table form.KPIApplicantManagement

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'main', 'SSRSLink', NULL, 'main.SSRSLink', 'main.SSRSLink', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'AddContactStipendPaymentContacts', NULL, 'Contact.AddContactStipendPaymentContacts', 'Add contacts to a stipend payment list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'CashHandOverExport', 'Contact.JusticePaymentList.CashHandOverExport', 'Allow access to the cash hand over report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'OpFundsReport', 'Contact.JusticePaymentList.OpFundsReport', 'Allow access to the op funds report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendActivity', 'Contact.JusticePaymentList.StipendActivity', 'Allow access to the stipend activity report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendPaymentReport', 'Contact.JusticePaymentList.StipendPaymentReport', 'Allow access to the stipend payment report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ApplicantManagement', NULL, 'Contact.ApplicantManagement', 'Associate contacts with programs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'AssociateAll', 'Contact.Associate.AssociateAll', 'Enable the Associate All button', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'NonBeneficiary', 'Contact.List.NonBeneficiary', 'View the list of contacts not of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', 'Budget', 'Award.AddUpdate.Budget', 'Add / edit an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', 'Budget', 'Award.View.Budget', 'View an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'Budget', 'Proposal.AddUpdate.Budget', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', 'Budget', 'Proposal.View.Budget', 'View a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', 'Budget', 'SubAward.AddUpdate.Budget', 'Add / edit a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', 'Budget', 'SubAward.View.Budget', 'View a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'AdministerForm', NULL, 'Form.AdministerForm', 'Create responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'SaveForm', NULL, 'Form.SaveForm', 'Save responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.8 File 01 - EFE - 2016.10.30 22.27.09')
GO
--End build tracking

