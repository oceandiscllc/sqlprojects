USE EFE
GO

--Begin table dbo.ContactProgramType
DECLARE @TableName VARCHAR(250) = 'dbo.ContactProgramType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactProgramType
	(
	ContactProgramTypeID INT NOT NULL IDENTITY(1,1),
	ContactID INT,
	ProgramTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramTypeID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_ContactProgramType', 'ContactID,ProgramTypeID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactProgramTypeID'
GO
--End table dbo.ContactProgramType

--Begin table dropdown.Affiliate
DECLARE @TableName VARCHAR(250) = 'dropdown.Affiliate'

EXEC utility.AddColumn @TableName, 'AffiliateCountryCode', 'CHAR(2)'
GO

UPDATE A
SET A.AffiliateCountryCode = C.ISOCountryCode2
FROM dropdown.Affiliate A
	JOIN dropdown.Country C ON C.CountryName = A.AffiliateName
GO
--End table dropdown.Affiliate

--Begin table form.KPIApplicantManagement
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIApplicantManagement')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIApplicantManagement'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIApplicantManagement
		(
		KPIApplicantManagementID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID VARCHAR(50),
		ContactID VARCHAR(50),
		Q007 VARCHAR(50),
		Q009 VARCHAR(50),
		Q011 VARCHAR(MAX),
		Q013 VARCHAR(MAX),
		Q018 VARCHAR(50),
		Q020 VARCHAR(50),
		Q021 VARCHAR(50),
		Q023 VARCHAR(50),
		Q180 VARCHAR(50)
/*
		HighestLevelOfEducation VARCHAR(50),
		EnrolledAtUniversity VARCHAR(50),
		Specialization VARCHAR(MAX),
		Languages VARCHAR(MAX),
		TimeJobSeeking VARCHAR(50),
		AveragePersonalMonthlyIncome VARCHAR(50),
		AverageHouseholdMonthlyIncome VARCHAR(50),
		HadPaidJob VARCHAR(50),
		CurrentPrincipalActivity VARCHAR(50)
*/
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIApplicantManagement', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIApplicantManagementID'

	END
--ENDIF
GO
--End table form.KPIApplicantManagement
