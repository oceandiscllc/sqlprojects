USE EFE
GO

--Begin table form.KPIApplicantManagement
IF NOT EXISTS (SELECT 1 FROM form.KPIQuestion KPIQ WHERE KPIQ.KPICategory = 'KPIApplicantManagement')
	BEGIN

	INSERT INTO form.KPIQuestion
		(QuestionCode, KPICategory, KPICode)
	SELECT 
		Q.QuestionCode,
		'KPIApplicantManagement',
		Q.QuestionCode

/*
		CASE
			WHEN Q.QuestionCode IN ('Q007')
			THEN 'HighestLevelOfEducation'
			WHEN Q.QuestionCode IN ('Q009')
			THEN 'EnrolledAtUniversity'
			WHEN Q.QuestionCode IN ('Q011')
			THEN 'Specialization'
			WHEN Q.QuestionCode IN ('Q013')
			THEN 'Languages'
			WHEN Q.QuestionCode IN ('Q018')
			THEN 'TimeJobSeeking'
			WHEN Q.QuestionCode IN ('Q020')
			THEN 'AveragePersonalMonthlyIncome'
			WHEN Q.QuestionCode IN ('Q021')
			THEN 'AverageHouseholdMonthlyIncome'
			WHEN Q.QuestionCode IN ('Q023')
			THEN 'HadPaidJob'
			WHEN Q.QuestionCode IN ('Q180')
			THEN 'CurrentPrincipalActivity'
		END
*/

	FROM form.Question Q
	WHERE Q.QuestionCode IN ('Q007','Q009','Q011','Q013','Q018','Q020','Q021','Q023','Q180')

	END
--ENDIF
GO
--End table form.KPIApplicantManagement
