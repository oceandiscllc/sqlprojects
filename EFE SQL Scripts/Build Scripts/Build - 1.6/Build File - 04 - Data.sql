﻿USE EFE
GO

--Begin table dropdown.ApplicantStatus
UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Applicant'
WHERE ApplicantStatusName = 'Long List'
GO

UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Eligible'
WHERE ApplicantStatusName = 'Short List'
GO

UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Registered'
WHERE ApplicantStatusName = 'Student'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ApplicantStatus A WHERE A.ApplicantStatusName = 'Accepted')
	BEGIN

	INSERT INTO dropdown.ApplicantStatus 
		(ApplicantStatusName)
	VALUES 
		('Accepted')

	END
--ENDIF
GO
--End table dropdown.ApplicantStatus

--Begin table dropdown.StudentStatus
UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Enrolled'
WHERE StudentStatusName = 'Active'
GO

UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Graduate'
WHERE StudentStatusName = 'Alumni'
GO

UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Withdrawn'
WHERE StudentStatusName = 'Withdrawal'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.StudentStatus SS WHERE SS.StudentStatusName = 'No Show')
	BEGIN

	INSERT INTO dropdown.StudentStatus 
		(StudentStatusName)
	VALUES 
		('No Show'), 
		('No Pass')

	END
--ENDIF
GO
--End table dropdown.StudentStatus

UPDATE dbo.EmailTemplate
SET EmailSubject = 'EFE Survey Request - [[Title]]'
WHERE EntityTypeCode = 'Survey'
GO

UPDATE dbo.EntityType
SET 
	EntityTypeName = 'Curriculum',
	EntityTypeNamePlural = 'Curricula'
WHERE EntityTypeCode = 'Course'
GO

UPDATE dbo.MenuItem
SET MenuItemText = 'Curricula'
WHERE MenuItemCode = 'CourseList'
GO

UPDATE FQL
SET FQL.QuestionText = 
	CASE
		WHEN FQL.Locale = 'AR'
		THEN N'هل عملت لدى [[SameEmployer]] خلال الثلاثة أشهر الماضية؟'
		WHEN FQL.Locale = 'AREG'
		THEN N'هل اشتغلت مع [[SameEmployer]] خلال التلات شهور يلي فاتت؟'
		WHEN FQL.Locale = 'EN'
		THEN N'Have you worked for the [[SameEmployer]] over the past three months?'
		WHEN FQL.Locale = 'FR'
		THEN N'Avez-vous travaillé pour [[SameEmployer]] au cours des trois derniers mois?'
	END

FROM form.QuestionLabel FQL
WHERE FQL.QUestionCode = 'Q127'
GO

IF NOT EXISTS (SELECT 1 FROM form.EntityDataGroup EDG WHERE EDG.EntityTypeCode = 'ClassTrainer' AND EDG.GroupNumber > 3)
	BEGIN

	DECLARE @cNewQuestionCode VARCHAR(50)
	DECLARE @cOldQuestionCode VARCHAR(50)

	DECLARE @nNewQuestionCode INT
	DECLARE @nOldQuestionCode INT

	DECLARE @tTable TABLE (QuestionID INT NOT NULL IDENTITY(1,1), OldQuestionCode VARCHAR(50), NewQuestionCode VARCHAR(50))

	SELECT TOP 1 @cNewQuestionCode = QuestionCode 
	FROM form.Question Q
	ORDER BY Q.QuestionCode DESC

	SELECT TOP 1 @cOldQuestionCode = QuestionCode 
	FROM form.EntityDataGroup EDG
	WHERE EDG.EntityTypeCode = 'ClassTrainer'
	ORDER BY EDG.QuestionCode

	SET @nNewQuestionCode = CAST(REPLACE(@cNewQuestionCode, 'Q', '') AS INT)
	SET @nOldQuestionCode = CAST(REPLACE(@cOldQuestionCode, 'Q', '') AS INT)

	INSERT INTO @tTable
		(OldQuestionCode)
	SELECT
		Q.QuestionCode
	FROM form.Question Q
		JOIN form.EntityDataGroup EDG ON EDG.QuestionCode = Q.QuestionCode
			AND EDG.EntityTypeCode = 'ClassTrainer'
			AND EDG.GroupNumber IN (1, 2)

	UPDATE T
	SET NewQuestionCode = 'Q' + CAST(T.QuestionID + @nNewQuestionCode AS VARCHAR(10))
	FROM @tTable T

	INSERT INTO form.Question
		(QuestionCode, QuestionResponseTypeCode, EntityTypeCode, MaximumResponseCount)
	SELECT
		T.NewQuestionCode,
		Q.QuestionResponseTypeCode, 
		Q.EntityTypeCode, 
		Q.MaximumResponseCount
	FROM form.Question Q
		JOIN @tTable T ON T.OldQuestionCode = Q.QuestionCode

	INSERT INTO form.QuestionLabel
		(QuestionCode, Locale, QuestionText)
	SELECT
		T.NewQuestionCode,
		QL.Locale,
		QL.QuestionText
	FROM form.QuestionLabel QL
		JOIN @tTable T ON T.OldQuestionCode = QL.QuestionCode

	INSERT INTO form.FormQuestion
		(FormCode, QuestionCode, TextCode, DisplayOrder, IsRequired)
	SELECT
		'TR1',
		T.NewQuestionCode,
		FQ.TextCode,
		FQ.DisplayOrder,
		FQ.IsRequired
	FROM form.FormQuestion FQ
		JOIN @tTable T ON T.OldQuestionCode = FQ.QuestionCode

	INSERT INTO form.EntityDataGroup
		(EntityTypeCode, GroupNumber, QuestionCode, IsFirstQuestion, IsLastQuestion)
	SELECT
		EDG.EntityTypeCode,
		EDG.GroupNumber + 3,
		T.NewQuestionCode,
		EDG.IsFirstQuestion,
		EDG.IsLastQuestion
	FROM form.EntityDataGroup EDG
		JOIN @tTable T ON T.OldQuestionCode = EDG.QuestionCode

	INSERT INTO form.FormQuestionResponseMap
		(FormCode, QuestionCode, NextQuestionCode)
	SELECT
		'TR1',
		T.NewQuestionCode,
		'Q' + CAST(CAST(REPLACE(T.NewQuestionCode, 'Q', '') AS INT) + 1 AS VARCHAR(10))
	FROM form.FormQuestionResponseMap FQRM
		JOIN @tTable T ON T.OldQuestionCode = FQRM.QuestionCode

	UPDATE FQRM
	SET FQRM.NextQuestionCode = (SELECT TOP 1 T.NewQuestionCode FROM @tTable T ORDER BY T.QuestionID)
	FROM form.FormQuestionResponseMap FQRM
	WHERE FQRM.FormCode = 'TR1' 
		AND FQRM.NextQuestionCode = 'Q085'

	UPDATE FQRM
	SET FQRM.NextQuestionCode = 'Q085'
	FROM form.FormQuestionResponseMap FQRM
	WHERE FQRM.FormCode = 'TR1' 
		AND FQRM.QuestionCode = (SELECT TOP 1 T.NewQuestionCode FROM @tTable T ORDER BY T.QuestionID DESC)

	END
--ENDIF