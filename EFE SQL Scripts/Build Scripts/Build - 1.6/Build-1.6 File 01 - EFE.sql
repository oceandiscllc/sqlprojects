-- File Name:	Build-1.6 File 01 - EFE.sql
-- Build Key:	Build-1.6 File 01 - EFE - 2016.08.23 16.42.18

USE EFE
GO

-- ==============================================================================================================================
-- Procedures:
--		contact.ValidateEmailAddress
--		dbo.GetOrganizationJobByOrganizationJobID
--		form.GetFormData
--		program.GetClassByClassID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table dbo.OrganizationJob
DECLARE @TableName VARCHAR(250) = 'dbo.OrganizationJob' 

EXEC utility.AddColumn @TableName, 'IsPreCommitted', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsPreCommitted', 'BIT', 0
GO
--End table dbo.OrganizationJob

--Begin table program.Class
DECLARE @TableName VARCHAR(250) = 'program.Class' 

EXEC utility.AddColumn @TableName, 'TrainerID4', 'INT'
EXEC utility.AddColumn @TableName, 'TrainerID5', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TrainerID4', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TrainerID5', 'INT', 0
GO
--End table program.Class

--Begin table program.ClassContact
DECLARE @TableName VARCHAR(250) = 'program.ClassContact' 

EXEC utility.AddColumn @TableName, 'StatusReason', 'VARCHAR(MAX)'
GO
--End table program.ClassContact
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure contact.ValidateEmailAddress
EXEC Utility.DropObject 'contact.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Kevin Ross
-- Create date:	2016.08.16
-- Description:	A stored procedure to validate an email address
-- ============================================================
CREATE PROCEDURE contact.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ContactCount
	FROM dbo.Contact C
	WHERE C.ContactID <> @ContactID
		AND 
			(
			C.EmailAddress = @EmailAddress
				OR C.UserName = @EmailAddress
			)

END
GO
--End procedure contact.ValidateEmailAddress

--Begin procedure dbo.GetOrganizationJobByOrganizationJobID
EXEC Utility.DropObject 'dbo.GetOrganizationJobByOrganizationJobID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.29
-- Description:	A stored procedure to data from the dbo.OrganizationJob table
-- ==========================================================================
CREATE PROCEDURE dbo.GetOrganizationJobByOrganizationJobID

@OrganizationJobID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		OJ.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(OJ.AffiliateID) AS AffiliateName,
		O.OrganizationName,
		OJ.Address,
		OJ.AvailablePositions,
		OJ.CutoffDate,
		dbo.FormatDate(OJ.CutoffDate) AS CutoffDateFormatted,
		LEFT(DATENAME(month, OJ.CutoffDate), 3) + '-' + RIGHT(CAST(YEAR(OJ.CutoffDate) AS CHAR(4)), 2) AS CutoffDateFormattedShort,
		OJ.DistanceRequirements,
		OJ.IsPreCommitted,
		OJ.JobDescription,
		OJ.JobLocation,
		OJ.OrganizationID,
		OJ.OrganizationJobID,
		OJ.OrganizationJobName,
		OJ.SkillsetID,
		OJ.TimingRequirements,
		SS.SkillsetName,
		OJ.ContactID,
		dbo.FormatContactNameByContactID(OJ.ContactID, 'LastFirst') AS ContactName
	FROM dbo.OrganizationJob OJ
		JOIN dbo.Organization O ON O.OrganizationID = OJ.OrganizationID
		JOIN dropdown.Skillset SS ON SS.SkillsetID = OJ.SkillsetID
			AND OJ.OrganizationJobID = @OrganizationJobID

	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		COJ.UpdateDate,
		dbo.FormatDate(COJ.UpdateDate) AS UpdateDateFormatted,
		COJS.ContactOrganizationJobStatusID,
		COJS.ContactOrganizationJobStatusName
	FROM dbo.ContactOrganizationJob COJ
		JOIN dbo.OrganizationJob OJ ON OJ.OrganizationJobID = COJ.OrganizationJobID
		JOIN dbo.Contact CO ON CO.ContactID = COJ.ContactID
		JOIN dropdown.ContactOrganizationJobStatus COJS ON COJS.ContactOrganizationJobStatusID = COJ.ContactOrganizationJobStatusID
			AND OJ.OrganizationJobID = @OrganizationJobID

END
GO
--End procedure dbo.GetOrganizationJobByOrganizationJobID

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						4 AS GroupNumber, 
						T4.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T4 ON T4.TrainerID = C.TrainerID4
							AND C.TrainerID4 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						5 AS GroupNumber, 
						T5.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T5 ON T5.TrainerID = C.TrainerID5
							AND C.TrainerID5 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 
		TQ.Text, 
		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText,
		ISNULL((SELECT Q.MaximumResponseCount FROM form.Question Q WHERE Q.QuestionCode = TQ.ObjectCode), 0) AS MaximumResponseCount
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			WHEN RD.QuestionCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(RD.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'�?�?س صاحب ا�?ع�?�?')
							WHEN @Locale = 'AREG'



							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'�?�?س صاحب ا�?ع�?�?')
							WHEN @Locale = 'EN'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'le même employeur')
						END
				END
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure program.GetClassByClassID
EXEC Utility.DropObject 'program.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:		Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Class table
--
-- Author:		Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:		Eric Jones
-- Create date:	2015.12.30
-- Description:	added studentStatus data
--
-- Author:		Eric Jones
-- Create date:	2015.12.30
-- Description:	added status date and formatted status date
--
-- Author:		Todd Pires
-- Create date:	2016.04.07
-- Description:	Implemented the TrainerID columns
--
-- Author:		Todd Pires
-- Create date:	2016.08.09
-- Description:	Implemented more TrainerID columns
--
-- Author:		Kevin Ross
-- Create date:	2016.08.16
-- Description:	Implemented the StatusReason columns
-- ===============================================================================================
CREATE PROCEDURE program.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(CL.AffiliateID) AS AffiliateName,
		CL.ClassID,
		CL.ClassName,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location,
		CL.Seats,
		CL.Sessions,
		CL.StartDate,
		CL.EndDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CO.CourseID,
		CO.CourseName,
		P.ProgramName,
		P.ProgramID,
		T1.TrainerID AS TrainerID1,
		T1.TrainerName AS TrainerName1,
		T2.TrainerID AS TrainerID2,
		T2.TrainerName AS TrainerName2,
		T3.TrainerID AS TrainerID3,
		T3.TrainerName AS TrainerName3,
		T4.TrainerID AS TrainerID4,
		T4.TrainerName AS TrainerName4,
		T5.TrainerID AS TrainerID5,
		T5.TrainerName AS TrainerName5
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN dropdown.Trainer T1 ON T1.TrainerID = CL.TrainerID1
		JOIN dropdown.Trainer T2 ON T2.TrainerID = CL.TrainerID2
		JOIN dropdown.Trainer T3 ON T3.TrainerID = CL.TrainerID3
		JOIN dropdown.Trainer T4 ON T4.TrainerID = CL.TrainerID4
		JOIN dropdown.Trainer T5 ON T5.TrainerID = CL.TrainerID5
		LEFT JOIN program.program P ON P.ProgramID = CL.ProgramID
	WHERE CL.ClassID = @ClassID
	
	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		CC.StudentStatusID,
		SS.StudentStatusName,
		CC.StatusDate,
		CC.StatusReason,
		dbo.FormatDate(CC.StatusDate) AS StatusDateFormatted
	FROM program.ClassContact CC
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID= CC.StudentStatusID
			AND CL.ClassID = @ClassID

END
GO
--End procedure program.GetClassByClassID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--Begin table dropdown.ApplicantStatus
UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Applicant'
WHERE ApplicantStatusName = 'Long List'
GO

UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Eligible'
WHERE ApplicantStatusName = 'Short List'
GO

UPDATE dropdown.ApplicantStatus
SET ApplicantStatusName = 'Registered'
WHERE ApplicantStatusName = 'Student'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ApplicantStatus A WHERE A.ApplicantStatusName = 'Accepted')
	BEGIN

	INSERT INTO dropdown.ApplicantStatus 
		(ApplicantStatusName)
	VALUES 
		('Accepted')

	END
--ENDIF
GO
--End table dropdown.ApplicantStatus

--Begin table dropdown.StudentStatus
UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Enrolled'
WHERE StudentStatusName = 'Active'
GO

UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Graduate'
WHERE StudentStatusName = 'Alumni'
GO

UPDATE dropdown.StudentStatus
SET StudentStatusName = 'Withdrawn'
WHERE StudentStatusName = 'Withdrawal'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.StudentStatus SS WHERE SS.StudentStatusName = 'No Show')
	BEGIN

	INSERT INTO dropdown.StudentStatus 
		(StudentStatusName)
	VALUES 
		('No Show'), 
		('No Pass')

	END
--ENDIF
GO
--End table dropdown.StudentStatus

UPDATE dbo.EmailTemplate
SET EmailSubject = 'EFE Survey Request - [[Title]]'
WHERE EntityTypeCode = 'Survey'
GO

UPDATE dbo.EntityType
SET 
	EntityTypeName = 'Curriculum',
	EntityTypeNamePlural = 'Curricula'
WHERE EntityTypeCode = 'Course'
GO

UPDATE dbo.MenuItem
SET MenuItemText = 'Curricula'
WHERE MenuItemCode = 'CourseList'
GO

UPDATE FQL
SET FQL.QuestionText = 
	CASE
		WHEN FQL.Locale = 'AR'
		THEN N'?? ???? ??? [[SameEmployer]] ???? ??????? ???? ????????'
		WHEN FQL.Locale = 'AREG'
		THEN N'?? ?????? ?? [[SameEmployer]] ???? ?????? ???? ??? ?????'
		WHEN FQL.Locale = 'EN'
		THEN N'Have you worked for the [[SameEmployer]] over the past three months?'
		WHEN FQL.Locale = 'FR'
		THEN N'Avez-vous travaill� pour [[SameEmployer]] au cours des trois derniers mois?'
	END

FROM form.QuestionLabel FQL
WHERE FQL.QUestionCode = 'Q127'
GO

IF NOT EXISTS (SELECT 1 FROM form.EntityDataGroup EDG WHERE EDG.EntityTypeCode = 'ClassTrainer' AND EDG.GroupNumber > 3)
	BEGIN

	DECLARE @cNewQuestionCode VARCHAR(50)
	DECLARE @cOldQuestionCode VARCHAR(50)

	DECLARE @nNewQuestionCode INT
	DECLARE @nOldQuestionCode INT

	DECLARE @tTable TABLE (QuestionID INT NOT NULL IDENTITY(1,1), OldQuestionCode VARCHAR(50), NewQuestionCode VARCHAR(50))

	SELECT TOP 1 @cNewQuestionCode = QuestionCode 
	FROM form.Question Q
	ORDER BY Q.QuestionCode DESC

	SELECT TOP 1 @cOldQuestionCode = QuestionCode 
	FROM form.EntityDataGroup EDG
	WHERE EDG.EntityTypeCode = 'ClassTrainer'
	ORDER BY EDG.QuestionCode

	SET @nNewQuestionCode = CAST(REPLACE(@cNewQuestionCode, 'Q', '') AS INT)
	SET @nOldQuestionCode = CAST(REPLACE(@cOldQuestionCode, 'Q', '') AS INT)

	INSERT INTO @tTable
		(OldQuestionCode)
	SELECT
		Q.QuestionCode
	FROM form.Question Q
		JOIN form.EntityDataGroup EDG ON EDG.QuestionCode = Q.QuestionCode
			AND EDG.EntityTypeCode = 'ClassTrainer'
			AND EDG.GroupNumber IN (1, 2)

	UPDATE T
	SET NewQuestionCode = 'Q' + CAST(T.QuestionID + @nNewQuestionCode AS VARCHAR(10))
	FROM @tTable T

	INSERT INTO form.Question
		(QuestionCode, QuestionResponseTypeCode, EntityTypeCode, MaximumResponseCount)
	SELECT
		T.NewQuestionCode,
		Q.QuestionResponseTypeCode, 
		Q.EntityTypeCode, 
		Q.MaximumResponseCount
	FROM form.Question Q
		JOIN @tTable T ON T.OldQuestionCode = Q.QuestionCode

	INSERT INTO form.QuestionLabel
		(QuestionCode, Locale, QuestionText)
	SELECT
		T.NewQuestionCode,
		QL.Locale,
		QL.QuestionText
	FROM form.QuestionLabel QL
		JOIN @tTable T ON T.OldQuestionCode = QL.QuestionCode

	INSERT INTO form.FormQuestion
		(FormCode, QuestionCode, TextCode, DisplayOrder, IsRequired)
	SELECT
		'TR1',
		T.NewQuestionCode,
		FQ.TextCode,
		FQ.DisplayOrder,
		FQ.IsRequired
	FROM form.FormQuestion FQ
		JOIN @tTable T ON T.OldQuestionCode = FQ.QuestionCode

	INSERT INTO form.EntityDataGroup
		(EntityTypeCode, GroupNumber, QuestionCode, IsFirstQuestion, IsLastQuestion)
	SELECT
		EDG.EntityTypeCode,
		EDG.GroupNumber + 3,
		T.NewQuestionCode,
		EDG.IsFirstQuestion,
		EDG.IsLastQuestion
	FROM form.EntityDataGroup EDG
		JOIN @tTable T ON T.OldQuestionCode = EDG.QuestionCode

	INSERT INTO form.FormQuestionResponseMap
		(FormCode, QuestionCode, NextQuestionCode)
	SELECT
		'TR1',
		T.NewQuestionCode,
		'Q' + CAST(CAST(REPLACE(T.NewQuestionCode, 'Q', '') AS INT) + 1 AS VARCHAR(10))
	FROM form.FormQuestionResponseMap FQRM
		JOIN @tTable T ON T.OldQuestionCode = FQRM.QuestionCode

	UPDATE FQRM
	SET FQRM.NextQuestionCode = (SELECT TOP 1 T.NewQuestionCode FROM @tTable T ORDER BY T.QuestionID)
	FROM form.FormQuestionResponseMap FQRM
	WHERE FQRM.FormCode = 'TR1' 
		AND FQRM.NextQuestionCode = 'Q085'

	UPDATE FQRM
	SET FQRM.NextQuestionCode = 'Q085'
	FROM form.FormQuestionResponseMap FQRM
	WHERE FQRM.FormCode = 'TR1' 
		AND FQRM.QuestionCode = (SELECT TOP 1 T.NewQuestionCode FROM @tTable T ORDER BY T.QuestionID DESC)

	END
--ENDIF
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'NonBeneficiary', 'Contact.List.NonBeneficiary', 'View the list of contacts not of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', 'Budget', 'Award.AddUpdate.Budget', 'Add / edit an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', 'Budget', 'Award.View.Budget', 'View an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'Budget', 'Proposal.AddUpdate.Budget', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', 'Budget', 'Proposal.View.Budget', 'View a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', 'Budget', 'SubAward.AddUpdate.Budget', 'Add / edit a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', 'Budget', 'SubAward.View.Budget', 'View a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'AdministerForm', NULL, 'Form.AdministerForm', 'Create responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'SaveForm', NULL, 'Form.SaveForm', 'Save responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.6 File 01 - EFE - 2016.08.23 16.42.18')
GO
--End build tracking

