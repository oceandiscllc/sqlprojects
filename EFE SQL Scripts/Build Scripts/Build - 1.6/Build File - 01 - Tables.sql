﻿USE EFE
GO

--Begin table dbo.OrganizationJob
DECLARE @TableName VARCHAR(250) = 'dbo.OrganizationJob' 

EXEC utility.AddColumn @TableName, 'IsPreCommitted', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsPreCommitted', 'BIT', 0
GO
--End table dbo.OrganizationJob

--Begin table program.Class
DECLARE @TableName VARCHAR(250) = 'program.Class' 

EXEC utility.AddColumn @TableName, 'TrainerID4', 'INT'
EXEC utility.AddColumn @TableName, 'TrainerID5', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TrainerID4', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TrainerID5', 'INT', 0
GO
--End table program.Class

--Begin table program.ClassContact
DECLARE @TableName VARCHAR(250) = 'program.ClassContact' 

EXEC utility.AddColumn @TableName, 'StatusReason', 'VARCHAR(MAX)'
GO
--End table program.ClassContact