USE EFE
GO

--Begin procedure contact.ValidateLogin
EXEC Utility.DropObject 'contact.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE contact.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nCountryID INT
	DECLARE @nContactID INT
	DECLARE @nAffiliateID INT

	DECLARE @tContact TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		ContactID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		CountryID INT NOT NULL DEFAULT 0,
		AffiliateID INT NOT NULL DEFAULT 0
		)

	SELECT
		@cEmailAddress = C.EmailAddress,
		@cFullName = dbo.FormatContactNameByContactID(C.ContactID, 'FirstLast'),
		@cPasswordDB = C.Password,
		@cPasswordSalt = C.PasswordSalt,
		@nInvalidLoginAttempts = C.InvalidLoginAttempts,
		@bIsAccountLockedOut = C.IsAccountLockedOut,
		@bIsActive = C.IsActive,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN C.PasswordExpirationDateTime IS NOT NULL AND C.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN C.EmailAddress IS NULL OR LEN(LTRIM(C.EmailAddress)) = 0
					OR C.FirstName IS NULL OR LEN(LTRIM(C.FirstName)) = 0
					OR C.LastName IS NULL OR LEN(LTRIM(C.LastName)) = 0
				THEN 1
				ELSE 0
			END,

		@nContactID = ISNULL(C.ContactID, 0),
		@cUserName = C.UserName,
		@nCountryCallingCodeID = C.PhoneNumberCountryCallingCodeID,
		@cPhone =C.PhoneNumber,
		@nCountryID = ISNULL(C.CountryID, 0),
		@nAffiliateID = ISNULL(C.AffiliateID, 0)
	FROM dbo.Contact C
		WHERE C.UserName = @UserName

	SET @nContactID = ISNULL(@nContactID, 0)
	SET @bIsValidUserName = CASE WHEN @nContactID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tContact 
			(ContactID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tContact 
			(ContactID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,CountryID,AffiliateID) 
		VALUES 
			(
			@nContactID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@nCountryID,
			@nAffiliateID
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Contact
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE ContactID = @nContactID
			
			UPDATE @tContact
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE ContactID = @nContactID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tContact
				SET IsPasswordExpired = 1
				WHERE ContactID = @nContactID

				END
			ELSE
				BEGIN

				UPDATE dbo.Contact
				SET InvalidLoginAttempts = 0
				WHERE ContactID = @nContactID

				END
			--ENDIF

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tContact
	
END
GO
--End procedure contact.ValidateLogin

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.01
-- Description:	A stored procedure to data from the contact.Contact table
--
-- Author:			Adam Davis
-- Create date: 2015.10.19
-- Description: Fixed references to OtherPhoneNumber and dbo.Contact
-- ======================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.LastName,
		C1.MiddleName,
		C1.OtherPhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.SkypeName,
		C1.Username,
		C1.InvalidLoginAttempts,
		C1.IsAccountLockedOut,
		C1.IsActive,
		C1.AffiliateID,
		C2.CountryID AS CitizenshipCountryID,
		C2.CountryName AS CitizenshipCountryName,
		C3.CountryID,
		C3.CountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCodeCode,
		CCC1.CountryCallingCodeID AS CellPhoneNumberCountryCallingCodeID,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCodeCode,
		CCC2.CountryCallingCodeID AS FaxNumberCountryCallingCodeID,
		CCC3.CountryCallingCode AS OtherPhoneNumberCountryCallingCodeCode,
		CCC3.CountryCallingCodeID AS OtherPhoneNumberCountryCallingCodeID,
		MS.MaritalStatusID,
		MS.MaritalStatusName,
		CT.ContactTypeID,
		CT.ContactTypeName,
		C1.AffiliateID,
		A.AffiliateName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.OtherPhoneNumberCountryCallingCodeID
		JOIN dropdown.MaritalStatus MS ON MS.MaritalStatusID = C1.MaritalStatusID
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = C1.ContactTypeID
		JOIN dropdown.Affiliate A on A.AffiliateID = C1.AffiliateID
			AND C1.ContactID = @ContactID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID

END
GO
--End procedure dbo.GetContactByContactID

--Begin dbo.GetDocumentByPhysicalFileName
EXEC Utility.DropObject 'dbo.GetDocumentByPhysicalFileName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dbo.Document table
-- ==========================================================================
CREATE PROCEDURE dbo.GetDocumentByPhysicalFileName

@PhysicalFileName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubType,
		D.ContentType,
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.PhysicalFileSize
	FROM dbo.Document D
	WHERE D.PhysicalFileName = @PhysicalFileName

END
GO
--End dbo.GetDocumentByPhysicalFileName

--Begin procedure dbo.GetOrganizationJobByOrganizationJobID
EXEC Utility.DropObject 'dbo.GetOrganizationJobByOrganizationJobID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.29
-- Description:	A stored procedure to data from the dbo.OrganizationJob table
-- ==========================================================================
CREATE PROCEDURE dbo.GetOrganizationJobByOrganizationJobID

@OrganizationJobID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		OJ.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(OJ.AffiliateID) AS AffiliateName,
		O.OrganizationName,
		OJ.Address,
		OJ.AvailablePositions,
		OJ.CutoffDate,
		dbo.FormatDate(OJ.CutoffDate) AS CutoffDateFormatted,
		LEFT(DATENAME(month, OJ.CutoffDate), 3) + '-' + RIGHT(CAST(YEAR(OJ.CutoffDate) AS CHAR(4)), 2) AS CutoffDateFormattedShort,
		OJ.DistanceRequirements,
		OJ.JobDescription,
		OJ.JobLocation,
		OJ.OrganizationID,
		OJ.OrganizationJobID,
		OJ.OrganizationJobName,
		OJ.SkillsetID,
		OJ.TimingRequirements,
		SS.SkillsetName,
		OJ.ContactID,
		dbo.FormatContactNameByContactID(OJ.ContactID, 'LastFirst') AS ContactName
	FROM dbo.OrganizationJob OJ
		JOIN dbo.Organization O ON O.OrganizationID = OJ.OrganizationID
		JOIN dropdown.Skillset SS ON SS.SkillsetID = OJ.SkillsetID
			AND OJ.OrganizationJobID = @OrganizationJobID

	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		COJS.ContactOrganizationJobStatusID,
		COJS.ContactOrganizationJobStatusName
	FROM dbo.ContactOrganizationJob COJ
		JOIN dbo.OrganizationJob OJ ON OJ.OrganizationJobID = COJ.OrganizationJobID
		JOIN dbo.Contact CO ON CO.ContactID = COJ.ContactID
		JOIN dropdown.ContactOrganizationJobStatus COJS ON COJS.ContactOrganizationJobStatusID = COJ.ContactOrganizationJobStatusID
			AND OJ.OrganizationJobID = @OrganizationJobID

END
GO
--End procedure dbo.GetOrganizationJobByOrganizationJobID

--Begin procedure dropdown.GetContactOrganizationJobStatusData
EXEC Utility.DropObject 'dropdown.GetContactOrganizationJobStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.ContactOrganizationJobStatus table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetContactOrganizationJobStatusData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactOrganizationJobStatusID, 
		T.ContactOrganizationJobStatusName,
		T.IsActive
	FROM dropdown.ContactOrganizationJobStatus T
	WHERE (T.ContactOrganizationJobStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactOrganizationJobStatusName, T.ContactOrganizationJobStatusID

END
GO
--End procedure dropdown.GetContactOrganizationJobStatusData

--Begin procedure dropdown.GetDateFilterData
EXEC Utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData
@Locale VARCHAR(4) = 'EN',
@IsForFuture BIT = 0,
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		TL.DateFilterName,
		T.DateNumber
	FROM dropdown.DateFilter T
		JOIN dropdown.DateFilterLabel TL ON TL.DateFilterID = T.DateFilterID
			AND TL.Locale = @Locale
			AND (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
			AND (T.DateNumber IN (0,1) OR TL.IsForFuture = @IsForFuture)
	ORDER BY T.DisplayOrder, T.DateFilterID
		
END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetFormData
EXEC Utility.DropObject 'dropdown.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A stored procedure to return data from the form.Form table
-- =======================================================================
CREATE PROCEDURE dropdown.GetFormData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FormID, 
		T.FormCode, 
		T.FormName
	FROM form.Form T
	WHERE (T.FormID > 0 OR @IncludeZero = 1)
	ORDER BY T.FormName, T.FormID

END
GO
--End procedure dropdown.GetFormData

--Begin procedure dropdown.GetQuestionData
EXEC Utility.DropObject 'dropdown.GetQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A stored procedure to return data from the form.Question table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetQuestionData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT T.QuestionCode AS ObjectCode
	FROM form.Question T
	WHERE (T.QuestionID > 0 OR @IncludeZero = 1)

	UNION
	
	SELECT T.QuestionResponseCode AS ObjectCode
	FROM form.QuestionResponse T
	WHERE (T.QuestionResponseID > 0 OR @IncludeZero = 1)

	UNION
	
	SELECT T.TextCode AS ObjectCode
	FROM form.Text T
	WHERE (T.TextID > 0 OR @IncludeZero = 1)
	
	ORDER BY 1

END
GO
--End procedure dropdown.GetQuestionData

--Begin procedure dropdown.GetTrainerData
EXEC Utility.DropObject 'dropdown.GetTrainerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.Trainer table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetTrainerData
@AffiliateID INT = 0,
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TrainerID, 
		T.TrainerName,
		T.IsActive
	FROM dropdown.Trainer T
	WHERE (T.TrainerID > 0 OR @IncludeZero = 1)
		AND (T.AffiliateID = @AffiliateID OR @AffiliateID = 0)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TrainerName, T.TrainerID

END
GO
--End procedure dropdown.GetTrainerData

--Begin procedure eventlog.LogFormQuestionAction
EXEC Utility.DropObject 'eventlog.LogFormQuestionAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFormQuestionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FormQuestion',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cQuestionLabels NVARCHAR(MAX) 
	
		SELECT 
			@cQuestionLabels = COALESCE(@cQuestionLabels, '') + D.QuestionLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('QuestionLabels'), ELEMENTS) AS QuestionLabels
			FROM form.QuestionLabel T
				JOIN form.Question Q ON Q.QuestionCode = T.QuestionCode
					AND Q.QuestionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FormQuestion',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<QuestionLabels>' + ISNULL(@cQuestionLabels, '') + '</QuestionLabels>') AS XML)
			FOR XML RAW('Form'), ELEMENTS
			)
		FROM form.Question T
		WHERE T.QuestionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFormQuestionAction

--Begin procedure eventlog.LogFormQuestionResponseAction
EXEC Utility.DropObject 'eventlog.LogFormQuestionResponseAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFormQuestionResponseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FormQuestionResponse',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cQuestionResponseLabels NVARCHAR(MAX) 
	
		SELECT 
			@cQuestionResponseLabels = COALESCE(@cQuestionResponseLabels, '') + D.QuestionResponseLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('QuestionResponseLabels'), ELEMENTS) AS QuestionResponseLabels
			FROM form.QuestionResponseLabel T 
				JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = T.QuestionResponseCode
					AND QR.QuestionResponseID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FormQuestionResponse',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<QuestionResponseLabels>' + ISNULL(@cQuestionResponseLabels, '') + '</QuestionResponseLabels>') AS XML)
			FOR XML RAW('Form'), ELEMENTS
			)
		FROM form.QuestionResponse T
		WHERE T.QuestionResponseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFormQuestionResponseAction

--Begin procedure form.FormQuestionAddUpdate
EXEC Utility.DropObject 'form.FormQuestionAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.26
-- Description:	A procedure to add / update data in the form.FormQuestion table
-- ============================================================================
CREATE PROCEDURE form.FormQuestionAddUpdate

@FormCode VARCHAR(50),
@DisplayOrderList VARCHAR(MAX) = NULL,
@NotRequiredList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DELETE FQ
	FROM form.FormQuestion FQ
	WHERE FQ.FormCode = @FormCode
	
	INSERT INTO form.FormQuestion 
		(FormCode, QuestionCode) 
	
	SELECT 
		@FormCode,
		FQRM.QuestionCode
	FROM form.FormQuestionResponseMap FQRM
	WHERE LEFT(FQRM.QuestionCode, 1) = 'Q'
		AND FQRM.FormCode = @FormCode
	
	UNION
	
	SELECT 
		@FormCode,
		FQRM.NextQuestionCode
	FROM form.FormQuestionResponseMap FQRM
	WHERE LEFT(FQRM.NextQuestionCode, 1) = 'Q'
		AND FQRM.FormCode = @FormCode
	
	ORDER BY 1

	INSERT INTO form.FormQuestion 
		(FormCode, TextCode, IsRequired) 
	
	SELECT 
		@FormCode,
		FQRM.QuestionCode,
		0
	FROM form.FormQuestionResponseMap FQRM
	WHERE LEFT(FQRM.QuestionCode, 1) = 'T'
		AND FQRM.FormCode = @FormCode
	
	UNION
	
	SELECT 
		@FormCode,
		FQRM.NextQuestionCode,
		0
	FROM form.FormQuestionResponseMap FQRM
	WHERE LEFT(FQRM.NextQuestionCode, 1) = 'T'
		AND FQRM.FormCode = @FormCode
	
	ORDER BY 1

	IF @DisplayOrderList IS NOT NULL
		BEGIN

		UPDATE FQ
		SET FQ.DisplayOrder = LTT.ListItemID
		FROM form.FormQuestion FQ
			JOIN dbo.ListToTable(@DisplayOrderList, ',') LTT ON LTT.ListItem = FQ.QuestionCode
				AND FQ.FormCode = @FormCode
			
		UPDATE FQ
		SET FQ.DisplayOrder = LTT.ListItemID
		FROM form.FormQuestion FQ
			JOIN dbo.ListToTable(@DisplayOrderList, ',') LTT ON LTT.ListItem = FQ.TextCode
				AND FQ.FormCode = @FormCode
			
		END
	--ENDIF

	IF @NotRequiredList IS NOT NULL
		BEGIN
			
		UPDATE FQ
		SET FQ.IsRequired = 0
		FROM form.FormQuestion FQ
			JOIN dbo.ListToTable(@NotRequiredList, ',') LTT ON LTT.ListItem = FQ.QuestionCode
			
		END
	--ENDIF
			
END
GO
--End procedure form.FormQuestionAddUpdate

--Begin procedure form.FormQuestionResponseMapAddUpdate
EXEC Utility.DropObject 'form.FormQuestionResponseMapAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.11
-- Description:	A procedure to add / update data in the form.FormQuestionResponseMap table
-- =======================================================================================
CREATE PROCEDURE form.FormQuestionResponseMapAddUpdate

@FormCode VARCHAR(50),
@QuestionCode VARCHAR(50) = NULL,
@QuestionResponseCodeList VARCHAR(MAX) = NULL,
@NextQuestionCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	IF @QuestionResponseCodeList IS NULL
		BEGIN

		MERGE INTO form.FormQuestionResponseMap FQRM 
		USING (SELECT @QuestionCode AS QuestionCode) Q 
			ON FQRM.QuestionCode = Q.QuestionCode
				AND FQRM.FormCode = @FormCode
		WHEN MATCHED THEN
			UPDATE SET 
				FQRM.NextQuestionCode = @NextQuestionCode
		WHEN NOT MATCHED THEN
			INSERT
				(FormCode, QuestionCode, NextQuestionCode)
			VALUES
				(@FormCode, @QuestionCode, @NextQuestionCode)
		;
		
		END
	ELSE IF LEN(@QuestionResponseCodeList) > 0
		BEGIN

		MERGE INTO form.FormQuestionResponseMap FQRM 
		USING (SELECT QR1.QuestionResponseCode FROM form.QuestionResponse QR1 JOIN dbo.ListToTable(@QuestionResponseCodeList, ',') LTT ON @QuestionCode + '-' + RIGHT('00' + LTT.ListItem, 2) = QR1.QuestionResponseCode) QR2 
			ON FQRM.QuestionCode = @QuestionCode
				AND FQRM.FormCode = @FormCode
				AND FQRM.QuestionResponseCode = QR2.QuestionResponseCode
		WHEN MATCHED THEN
			UPDATE SET 
				FQRM.NextQuestionCode = @NextQuestionCode
		WHEN NOT MATCHED THEN
			INSERT
				(FormCode, QuestionCode, QuestionResponseCode, NextQuestionCode, IsBranchedQuestion)
			VALUES
				(@FormCode, @QuestionCode, QR2.QuestionResponseCode, @NextQuestionCode, 1)
		;

		END
	--ENDIF	
	
END
GO
--End procedure form.FormQuestionResponseMapAddUpdate

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 
		TQ.Text, 
		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure form.GetQuestionByQuestionCode
EXEC Utility.DropObject 'form.GetQuestionByQuestionCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A procedure to get form question data
-- ==================================================
CREATE PROCEDURE form.GetQuestionByQuestionCode

@QuestionCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		'Question' AS ObjectTypeCode, 					
		Q.QuestionCode AS ObjectCode
	FROM form.Question Q
	WHERE Q.QuestionCode = @QuestionCode
	
	SELECT
		QL.Locale,
		NULL AS AffiliateCountryCode,
		QL.QuestionText AS Text
	FROM form.QuestionLabel QL
	WHERE QL.QuestionCode = @QuestionCode
	ORDER BY QL.Locale

END
GO
--End procedure form.GetQuestionByQuestionCode

--Begin procedure form.GetQuestionResponseByQuestionResponseCode
EXEC Utility.DropObject 'form.GetQuestionResponseByQuestionResponseCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A procedure to get form QuestionResponse data
-- ==========================================================
CREATE PROCEDURE form.GetQuestionResponseByQuestionResponseCode

@QuestionResponseCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		'QuestionResponse' AS ObjectTypeCode, 					
		QR.QuestionResponseCode AS ObjectCode
	FROM form.QuestionResponse QR
	WHERE QR.QuestionResponseCode = @QuestionResponseCode
	
	SELECT
		QRL.Locale,
		QRL.AffiliateCountryCode,
		QRL.QuestionResponseText AS Text
	FROM form.QuestionResponseLabel QRL
	WHERE QRL.QuestionResponseCode = @QuestionResponseCode
	ORDER BY QRL.Locale

END
GO
--End procedure form.GetQuestionResponseByQuestionResponseCode

--Begin procedure form.GetTextByTextCode
EXEC Utility.DropObject 'form.GetTextByTextCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A procedure to get form Text data
-- ==============================================
CREATE PROCEDURE form.GetTextByTextCode

@TextCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		'Text' AS ObjectTypeCode, 					
		T.TextCode AS ObjectCode
	FROM form.Text T
	WHERE T.TextCode = @TextCode
	
	SELECT
		TL.Locale,
		NULL AS AffiliateCountryCode,
		TL.TextText AS Text
	FROM form.TextLabel TL
	WHERE TL.TextCode = @TextCode
	ORDER BY TL.Locale

END
GO
--End procedure form.GetTextByTextCode

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to get data from the permissionable.PermissionableGroup table
-- =============================================================================================
CREATE PROCEDURE permissionable.GetPermissionableGroups

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PG.PermissionableGroupID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		PG.DisplayOrder,
		(SELECT COUNT(P.PermissionableGroupID) FROM permissionable.Permissionable P WHERE P.PermissionableGroupID = PG.PermissionableGroupID) AS ItemCount
	FROM permissionable.PermissionableGroup PG
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID
		
END
GO
--End procedure permissionable.GetPermissionableGroups

--Begin person.HasFileAccess
EXEC Utility.DropObject 'person.HasFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2015.07.18
-- Description:	A stored procedure to check access to a file
-- =========================================================
CREATE PROCEDURE person.HasFileAccess

@PersonID INT,
@PhysicalFileName VARCHAR(50)

AS
BEGIN

	IF person.CheckFileAccess(@PersonID, @PhysicalFileName) = 1
		SELECT 1
	ELSE
		SELECT 1 WHERE 0 = 1
	--ENDIF

END
GO
--End procedure person.HasFileAccess

--Begin procedure program.GetClassByClassID
EXEC Utility.DropObject 'program.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Class table
--
-- Author:		Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:		Eric Jones
-- Create date:	2015.12.30
-- Description:	added studentStatus data
--
-- Author:		Eric Jones
-- Create date:	2015.12.30
-- Description:	added status date and formatted status date
--
-- Author:		Todd Pires
-- Create date:	2016.04.07
-- Description:	Implemented teh TrainerID columns
-- ===============================================================================================
CREATE PROCEDURE program.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(CL.AffiliateID) AS AffiliateName,
		CL.ClassID,
		CL.ClassName,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location,
		CL.Seats,
		CL.Sessions,
		CL.StartDate,
		CL.EndDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CO.CourseID,
		CO.CourseName,
		P.ProgramName,
		P.ProgramID,
		T1.TrainerID AS TrainerID1,
		T1.TrainerName AS TrainerName1,
		T2.TrainerID AS TrainerID2,
		T2.TrainerName AS TrainerName2,
		T3.TrainerID AS TrainerID3,
		T3.TrainerName AS TrainerName3
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN dropdown.Trainer T1 ON T1.TrainerID = CL.TrainerID1
		JOIN dropdown.Trainer T2 ON T2.TrainerID = CL.TrainerID2
		JOIN dropdown.Trainer T3 ON T3.TrainerID = CL.TrainerID3
		LEFT JOIN program.program P ON P.ProgramID = CL.ProgramID
	WHERE CL.ClassID = @ClassID
	
	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		CC.StudentStatusID,
		SS.StudentStatusName,
		CC.StatusDate,
		dbo.FormatDate(CC.StatusDate) AS StatusDateFormatted
	FROM program.ClassContact CC
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID= CC.StudentStatusID
			AND CL.ClassID = @ClassID

END
GO
--End procedure program.GetClassByClassID

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		F.FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = CL.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		C.ContactID
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID

	--ProgramTypeForms
	SELECT
		F.FormID,
		F.FormName
	FROM form.Form F
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID