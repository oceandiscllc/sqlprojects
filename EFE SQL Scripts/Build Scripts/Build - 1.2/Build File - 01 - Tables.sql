USE EFE
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'AffiliateID', 'INT'
EXEC utility.DropColumn @TableName, 'FormDate'
EXEC utility.DropColumn @TableName, 'FormID'

EXEC utility.SetDefaultConstraint @TableName, 'AffiliateID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.ContactOrganizationJob
DECLARE @TableName VARCHAR(250) = 'dbo.ContactOrganizationJob'

EXEC utility.AddColumn @TableName, 'ContactOrganizationJobStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ContactOrganizationJobStatusID', 'INT', 0
GO
--End table dbo.ContactOrganizationJob

--Begin table dbo.OrganizationJob
DECLARE @TableName VARCHAR(250) = 'dbo.OrganizationJob'

EXEC utility.AddColumn @TableName, 'ContactID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
GO
--End table dbo.OrganizationJob

--Begin table dropdown.ContactOrganizationJobStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactOrganizationJobStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactOrganizationJobStatus
	(
	ContactOrganizationJobStatusID INT NOT NULL IDENTITY(0,1),
	ContactOrganizationJobStatusName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetIndexClustered @TableName, 'IX_ContactOrganizationJobStatus', 'DisplayOrder,ContactOrganizationJobStatusName'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactOrganizationJobStatusID'
GO

SET IDENTITY_INSERT dropdown.ContactOrganizationJobStatus ON
GO

INSERT INTO dropdown.ContactOrganizationJobStatus (ContactOrganizationJobStatusID, ContactOrganizationJobStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.ContactOrganizationJobStatus OFF
GO

INSERT INTO dropdown.ContactOrganizationJobStatus
	(ContactOrganizationJobStatusName,DisplayOrder,IsActive)
VALUES
	('Submitted',0,1),
	('Shortlisted',0,1),
	('Offered',0,1),
	('Accepted',0,1),
	('Rejected',0,1),
	('Withdrawn',0,1),
	('Declined',0,1)
GO
--End table dropdown.ContactOrganizationJobStatus

--Begin table dropdown.DateFilter
DECLARE @TableName VARCHAR(250) = 'dropdown.DateFilter'

UPDATE DF 
SET DF.DateNumber = 0 
FROM dropdown.DateFilter DF 
WHERE DF.DateFilterID = 0

EXEC utility.DropIndex @TableName, 'IX_DateFilter'

EXEC utility.DropColumn @TableName, 'DateFilterName'

EXEC utility.SetIndexNonClustered @TableName, 'IX_DateFilter', 'DisplayOrder'
GO
--End table dropdown.DateFilter

--Begin table dropdown.DateFilterLabel
DECLARE @TableName VARCHAR(250) = 'dropdown.DateFilterLabel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DateFilterLabel
	(
	DateFilterLabelID INT NOT NULL IDENTITY(1,1),
	DateFilterID INT,
	Locale VARCHAR(4),
	DateFilterName NVARCHAR(250),
	IsForFuture BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsForFuture', 'BIT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_DateFilterLabel', 'DateFilterID,Locale'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DateFilterLabelID'
GO
--End table dropdown.DateFilterLabel

--Begin table dropdown.Trainer
DECLARE @TableName VARCHAR(250) = 'dropdown.Trainer'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Trainer
	(
	TrainerID INT NOT NULL IDENTITY(0,1),
	TrainerName NVARCHAR(250),
	AffiliateID INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AffiliateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetIndexClustered @TableName, 'IX_Trainer', 'DisplayOrder,TrainerName'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrainerID'
GO

SET IDENTITY_INSERT dropdown.Trainer ON
GO

INSERT INTO dropdown.Trainer (TrainerID, TrainerName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.Trainer OFF
GO
--End table dropdown.Trainer
 
--Begin table form.EntityDataGroup
DECLARE @TableName VARCHAR(250) = 'form.EntityDataGroup'

EXEC utility.DropObject @TableName

CREATE TABLE form.EntityDataGroup
	(
	EntityDataGroupID INT NOT NULL IDENTITY(1,1),
	EntityTypeCode VARCHAR(50),
	GroupNumber INT,
	QuestionCode VARCHAR(50),
	IsFirstQuestion BIT,
	IsLastQuestion BIT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'GroupNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFirstQuestion', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsLastQuestion', 'BIT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_EntityDataGroup', 'EntityTypeCode,GroupNumber'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EntityDataGroupID'
GO
--End table form.EntityDataGroup

--Begin table form.Form
DECLARE @TableName VARCHAR(250) = 'form.Form'

EXEC utility.DropObject @TableName

CREATE TABLE form.Form
	(
	FormID INT NOT NULL IDENTITY(1,1),
	FormName VARCHAR(250),
	FormCode VARCHAR(50),
	FormDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'FormDate', 'DATE', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FormID'
GO
--End table form.Form

--Begin table form.FormContact
DECLARE @TableName VARCHAR(250) = 'form.FormContact'

EXEC utility.DropObject @TableName

CREATE TABLE form.FormContact
	(
	FormContactID INT NOT NULL IDENTITY(1,1),
	FormID INT,
	ContactID INT,
	FormDate DATE,
	ProgramID INT, 
	EntityTypeCode VARCHAR(50), 
	EntityID INT, 
	IsActive BIT, 
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FormDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'FormID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_FormContact', 'FormID,ContactID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FormContactID'
GO
--End table form.FormContact

--Begin table form.FormQuestion
DECLARE @TableName VARCHAR(250) = 'form.FormQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE form.FormQuestion
	(
	FormQuestionID INT NOT NULL IDENTITY(1,1),
	FormCode VARCHAR(50),
	QuestionCode VARCHAR(50),
	TextCode VARCHAR(50),
	DisplayOrder INT,
	IsRequired BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 999
EXEC utility.SetDefaultConstraint @TableName, 'IsRequired', 'BIT', 1

EXEC utility.SetIndexClustered @TableName, 'IX_FormQuestion', 'FormCode,QuestionCode,TextCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FormQuestionID'
GO
--End table form.FormQuestion

--Begin table form.FormQuestionResponseMap
DECLARE @TableName VARCHAR(250) = 'form.FormQuestionResponseMap'

EXEC utility.DropObject @TableName

CREATE TABLE form.FormQuestionResponseMap
	(
	FormQuestionResponseMapID INT NOT NULL IDENTITY(1,1),
	FormCode VARCHAR(50),
	QuestionCode VARCHAR(50),
	QuestionResponseCode VARCHAR(50),
	NextQuestionCode VARCHAR(50),
	IsBranchedQuestion BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsBranchedQuestion', 'BIT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_FormQuestionResponseMap', 'FormCode,QuestionCode,QuestionResponseCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FormQuestionResponseMapID'
GO
--End table form.FormQuestionResponseMap

--Begin table form.Question
DECLARE @TableName VARCHAR(250) = 'form.Question'

EXEC utility.DropObject @TableName

CREATE TABLE form.Question
	(
	QuestionID INT NOT NULL IDENTITY(1,1),
	QuestionCode VARCHAR(50),
	QuestionResponseTypeCode VARCHAR(50),
	EntityTypeCode VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'QuestionID'
GO
--End table form.Question

--Begin table form.QuestionLabel
DECLARE @TableName VARCHAR(250) = 'form.QuestionLabel'

EXEC utility.DropObject @TableName

CREATE TABLE form.QuestionLabel
	(
	QuestionLabelID INT NOT NULL IDENTITY(1,1),
	QuestionCode VARCHAR(50),
	Locale VARCHAR(4),
	QuestionText NVARCHAR(MAX)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionLabel', 'QuestionCode,Locale'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QuestionLabelID'
GO
--End table form.QuestionLabel

--Begin table form.QuestionResponse
DECLARE @TableName VARCHAR(250) = 'form.QuestionResponse'

EXEC utility.DropObject @TableName

CREATE TABLE form.QuestionResponse
	(
	QuestionResponseID INT NOT NULL IDENTITY(1,1),
	QuestionResponseCode VARCHAR(50),
	QuestionCode VARCHAR(50)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionResponse', 'QuestionCode,QuestionResponseCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QuestionResponseID'
GO
--End table form.QuestionResponse

--Begin table form.QuestionResponseLabel
DECLARE @TableName VARCHAR(250) = 'form.QuestionResponseLabel'

EXEC utility.DropObject @TableName

CREATE TABLE form.QuestionResponseLabel
	(
	QuestionResponseLabelID INT NOT NULL IDENTITY(1,1),
	QuestionResponseCode VARCHAR(50),
	Locale VARCHAR(4),
	AffiliateCountryCode CHAR(2),
	QuestionResponseText NVARCHAR(MAX),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionResponseLabel1', 'DisplayOrder,QuestionResponseCode,Locale'
EXEC utility.SetIndexNonClustered @TableName, 'IX_QuestionResponseLabel2', 'QuestionResponseCode,Locale', 'AffiliateCountryCode,QuestionResponseText,DisplayOrder'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QuestionResponseLabelID'
GO
--End table form.QuestionResponseLabel

--Begin table form.QuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'form.QuestionResponseChoice'

EXEC utility.DropObject @TableName

CREATE TABLE form.QuestionResponseChoice
	(
	QuestionResponseChoiceID INT NOT NULL IDENTITY(1,1),
	ContactID INT,
	FormID INT,
	QuestionCode VARCHAR(50),
	QuestionResponseCode VARCHAR(50),
	QuestionResponseDate DATE,
	QuestionResponseText NVARCHAR(MAX),
	EntityID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FormID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuestionResponseDate', 'DATE', 'getDate()'

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionResponseChoice', 'ContactID,FormID,QuestionResponseDate,QuestionCode,QuestionResponseCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QuestionResponseChoiceID'
GO
--End table form.QuestionResponseChoice

--Begin table form.QuestionText
DECLARE @TableName VARCHAR(250) = 'form.QuestionText'

EXEC utility.DropObject @TableName

CREATE TABLE form.QuestionText
	(
	QuestionTextID INT NOT NULL IDENTITY(1,1),
	QuestionCode VARCHAR(50),
	TextCode VARCHAR(50),
	IsAfter BIT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsAfter', 'BIT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'QuestionTextID'
EXEC utility.SetIndexClustered @TableName, 'IX_QuestionText', 'QuestionCode,TextCode'
GO
--End table form.QuestionText

--Begin table form.Text
DECLARE @TableName VARCHAR(250) = 'form.Text'

EXEC utility.DropObject @TableName

CREATE TABLE form.Text
	(
	TextID INT NOT NULL IDENTITY(1,1),
	TextCode VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'TextID'
GO
--End table form.Text

--Begin table form.TextLabel
DECLARE @TableName VARCHAR(250) = 'form.TextLabel'

EXEC utility.DropObject @TableName

CREATE TABLE form.TextLabel
	(
	TextLabelID INT NOT NULL IDENTITY(1,1),
	TextCode VARCHAR(50),
	Locale VARCHAR(4),
	TextText NVARCHAR(MAX)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_TextLabel', 'TextCode,Locale'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TextLabelID'
GO
--End table form.TextLabel

--Begin table program.Class
DECLARE @TableName VARCHAR(250) = 'program.Class'

EXEC utility.AddColumn @TableName, 'TrainerID1', 'INT'
EXEC utility.AddColumn @TableName, 'TrainerID2', 'INT'
EXEC utility.AddColumn @TableName, 'TrainerID3', 'INT'

EXEC utility.DropColumn @TableName, 'Instructor1'
EXEC utility.DropColumn @TableName, 'Instructor2'

EXEC utility.SetDefaultConstraint @TableName, 'TrainerID1', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TrainerID2', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TrainerID3', 'INT', 0
GO
--End table program.Class

--Begin table program.Program
DECLARE @TableName VARCHAR(250) = 'program.Program'

EXEC utility.AddColumn @TableName, 'FormID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'FormID', 'INT', 0
GO
--End table program.Program

--Begin table program.ProgramTypeForm
DECLARE @TableName VARCHAR(250) = 'program.ProgramTypeForm'

EXEC utility.DropObject @TableName

CREATE TABLE program.ProgramTypeForm
	(
	ProgramTypeFormID INT NOT NULL IDENTITY(1,1),
	ProgramTypeID INT,
	FormID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FormID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramTypeID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_ProgramTypeForm', 'ProgramTypeID,FormID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProgramTypeFormID'
GO
--End table program.ProgramTypeForm
