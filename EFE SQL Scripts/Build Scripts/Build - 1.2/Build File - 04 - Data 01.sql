﻿USE EFE
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='TranslationList', @AfterMenuItemCode='EventLog', @NewMenuItemLink='/form/listTranslation', @NewMenuItemText='Translations', @PermissionableLineageList='Form.ListTranslation'
GO
--End table dbo.MenuItem

--Begin table dropdown.SurveyQuestionResponseType
IF NOT EXISTS (SELECT 1 FROM dropdown.SurveyQuestionResponseType SQRT WHERE SQRT.SurveyQuestionResponseTypeCode = 'SelectMultipleShortText')
	INSERT INTO dropdown.SurveyQuestionResponseType (SurveyQuestionResponseTypeCode, SurveyQuestionResponseTypeName) VALUES ('SelectMultipleShortText', 'Select (Multiple) With Short Text')
--ENDIF	
GO
IF NOT EXISTS (SELECT 1 FROM dropdown.SurveyQuestionResponseType SQRT WHERE SQRT.SurveyQuestionResponseTypeCode = 'SelectShortText')
	INSERT INTO dropdown.SurveyQuestionResponseType (SurveyQuestionResponseTypeCode, SurveyQuestionResponseTypeName) VALUES ('SelectShortText', 'Select With Short Text')
--ENDIF	
GO
--End table dropdown.SurveyQuestionResponseType

--Begin table dropdown.Trainer
TRUNCATE TABLE dropdown.Trainer
GO

INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Raba''a Al Haj Hassan', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abdallah Mammou', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abdelhak ETTAHIRI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abdelhakim QACHAR', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abdelillah EL KHALIFI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abdelmonèem Riadh Derouiche', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abderrahman ESSAKHI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abderrahmen Somrani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abderrahmen Somrani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Abderrazek El Ghali', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Adel Ayed', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Adel Gassoumi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Adil ADMI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ahmad Naji Hammad Mohammad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ahmed Housni', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Aicha KORTBI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ala’ Juma’ Mohammad Hamdan', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amina AMHARECH', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amine ISMAILI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amira Guermazi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amira Guermazi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amr Badr', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Amro Abu Alia ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Anas Elmelki', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Anis Allagui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Anis Allagui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Anis Allagui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Arbi Ben Abdallah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Atef daoud', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Aymen Dahmen', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'BACHIR JOUEID', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Bilal Alashqar', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Bilel Bellaj ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dana Hamdallah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dana Qaimary Siaj', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dana Qaimary', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dania EL GHISSASSI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Diana Onaizat', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Diana Sabat Nassar', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'DIMA HAMDALLAH', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dina Abdelkhalek', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dina El Hossainy', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Doaa Naguib', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Dorra Triki', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ebaa’ Al-Sai’d', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Emad Zenhom', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Enas Attari', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Faicel Zouaoui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Faouzi BOUSSEDRA', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fatima Ayaidah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fatima JOUMAL ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fatima zahra ASSIFI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fatma Kallel', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fatna   CHATER  ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fawzi Garchi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fayçal Zarrai  ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ferihane Boussoffara', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Fouad ZNAIDI ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hala Al Maaytah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Halim Absi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Halim Absi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hamdi Khalfaoui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hamida   MOUJI  ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Haneen Abu Doush', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Haneen J. Tawalbeh', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hanen Rabhi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hatem Kraiem', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'haya Ashour', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hayet Ben Saad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hazem Al Fouqaha', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hend Galal ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hisham Shkoukani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Husam Nazzal', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Hussam Asmar', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ibrahim Hamadeen', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ilham BAROUDI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Iman Al Yamani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Imane JOTI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Imen Mhamdi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ines Nasri', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Jamel Grassa', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Jehaad Shojaeah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Kaouther machta ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Karim KANOUTE', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Kawtar EL BAZ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Khadija EL AMRANI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Khaled Sakr', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Khalid  HANEFIOUI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'khalid COHEN', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Khalid SRAIDI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Khalid ZIAD', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'LAILA BAAMEL', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Lamiae CHIBANI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Lana Batarseh', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Lana Tayseer Qtaishat', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Latifa BARAKAT', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'LINA MOHAMMED MASOUD', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Lotfi Soltani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ma’moun Abu Rayyan', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mahmoud Ibrahim', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Makrem Saidi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Malek abdelkrim', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Malika GOUGNI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Manal Aladdin', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Marian Nady ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mehdi BARGACH', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mehdi Haddada', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mehdi Ouarche', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mehdi Ouarche', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mejda Bourguiba', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Meriem  BENZAKOUR', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Meryem BENSLIMANE', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mira Nimri', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamad Amairaa ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Abdel Aziz', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Ali Chniti', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed AOUJIL', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Belhaj', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Belhaj', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed BENNANI MEZIANE', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Boushila', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed El Kamel El Kamel', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed El Khames Ben Heni ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed galloub', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Ghazi Khenissi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed HAIDI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Maher', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Ridha Baroudi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Said', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohamed Taher Chebbi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'MOHAMED TOUFIK EL–AMRANI ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammad Fo’ad Samara Al-Zo’ubi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammad Joulany', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammad Marie', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammad Najeeb Madi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'MOHAMMED A.H. AL AFRANJI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammed CHAFIK', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammed HADINE', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammed Nabil ABOUNAFAA', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mohammed SAHLI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Moncef makni', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mouna ARIF', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'MOUNA EL BAOMRANI ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mouna Haouachi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Murad  Abdullah', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Musada  Abu Mahfouz', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mustapha EL MALKI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Mustapha LEFJEL', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'My Hafid BENSLIMANE ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nabiha bayoudh', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nadhem Touili', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Najia BEDOUI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Najwan Obeidat', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nashaat Tahboub ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nedal Rafiq Shtawi ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nejib Ben Moussa ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nezha ALAMI ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nezha EL FELLAH EL IDRISSI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nizar Nouri', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Noor Al Kayyali', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Nora Thabit', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Norya BENHAMOU', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Noureddine JBARA', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Olfat abu jarad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Omar Assi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Omar El Shamy', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Omar Omran', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Qasem Awad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Raed Rajab', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Raida Hamdi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Raja Cherif', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Rami Adwan', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Rami Al Darawish', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ratiba ABIDALLAH', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Reda BEJJTIT', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Reham Al Jallad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Riad Mustafa', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Riadh Bouzaouache', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ruba Al Natour', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Ruba Rahim', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Said BENHANNI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Said BENMBAREK', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Said TRITAH', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Sally Tayel', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Egypt'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Salma TAMIM', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Sami Yazidi', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Samia Ben Youssef', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Samir EL FILALI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Samir medhioub', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Sandra M. Alsaad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Saoussen Lakhel', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Saoussen Lakhel', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Shadi Sharif', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Slaheddine bouraoui', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Slim Mhiri', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Sofien Safraou', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Sonia Jehad Abu Sharar', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Suha bahbouh', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Taghreed Al Waked', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Tahar Tlili', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Touria JAOUHAR', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Urieb A. Samad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wafaa KHAMLICHI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wafae HAJAJI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wala’ F. AL Jallad', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Walid Bouzir', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wided Hasnaoui Allani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wided Hasnaoui Allani', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wiem ben hassine', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Wojdan Farraj', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Palestine'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Yassine BOUDI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Yazan AL Azzam', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Yosra Torjmen Mekni', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Youness BENKIRANE', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'YOUSSEF AMIMRI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zaid Shannak', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Jordan'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zine Ben Hamida', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Tunisia'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zineb   OUAZZANI TOUHAMI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zineb  EL KOHEN', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zineb  GHELLAB ', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zineb EL HAMMOUMI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
INSERT INTO dropdown.Trainer (TrainerName, AffiliateID) SELECT 'Zineb EL OMARI', A.AffiliateID FROM dropdown.Affiliate A WHERE A.AffiliateName = 'Morocco'
GO
--End table dropdown.Trainer

--Begin table form.Form
TRUNCATE TABLE form.Form
GO

INSERT INTO form.Form (FormCode) SELECT 'Entrepreneurship' + CAST(N.Number AS VARCHAR(5)) FROM EFEUtility.dbo.Number N WHERE N.Number <= 5
INSERT INTO form.Form (FormCode) SELECT 'FJIJ' + CAST(N.Number AS VARCHAR(5)) FROM EFEUtility.dbo.Number N WHERE N.Number <= 4
INSERT INTO form.Form (FormCode) SELECT 'JTP' + CAST(N.Number AS VARCHAR(5)) FROM EFEUtility.dbo.Number N WHERE N.Number <= 5
INSERT INTO form.Form (FormCode) VALUES ('SelfEsteem')
GO

UPDATE form.Form
SET FormName = FormCode
GO
--End table form.Form

--Begin table form.Question
TRUNCATE TABLE form.Question
GO

INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q003', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q004', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q005', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q006', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q007', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q008', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q009', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q010', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q011', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q012', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q013', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q014', 'SelectMultiple')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q015', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q016', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q017', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q018', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q019', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q020', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q021', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q022', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q023', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q024', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q025', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q026', 'SelectShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q027', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q028', 'SelectShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q029', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q030', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q031', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q032', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q033', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q034', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q035', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q036', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q037', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q038', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q039', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q040', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q041', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q042', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q043', 'SelectMultiple')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q044', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q045', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q046', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q047', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q048', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q049', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q050', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q051', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q052', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q053', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q054', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q055', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q056', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q057', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q058', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q059', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q060', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q061', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q062', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q063', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q064', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q065', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q066', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q067', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q068', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q069', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q071', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q072', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q073', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q074', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q075', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q076', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q077', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q078', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q079', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q080', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q081', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q082', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q083', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q084', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q085', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q086', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q087', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q088', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q089', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q090', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q091', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q092', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q093', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q094', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q095', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q096', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q097', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q098', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q099', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q100', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q101', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q102', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q103', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q104', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q105', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q106', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q107', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q108', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q109', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q110', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q111', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q113', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q114', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q115', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q116', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q117', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q118', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q119', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q120', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q121', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q122', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q123', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q125', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q126', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q127', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q128', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q129', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q130', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q131', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q132', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q133', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q134', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q135', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q136', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q137', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q138', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q139', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q140', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q142', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q143', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q144', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q145', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q146', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q147', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q148', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q149', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q150', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q151', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q152', 'ShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q153', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q154', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q155', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q156', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q157', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q158', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q159', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q160', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q161', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q162', 'Select')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q163', 'DatePicker')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q164', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q165', 'SelectMultiple')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q166', 'SelectMultiple')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q167', 'YesNo')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q168', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q169', 'SelectMultipleShortText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q170', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q171', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q172', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q173', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q174', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q175', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q176', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q177', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q178', 'NumericRange')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q179', 'LongText')
INSERT INTO form.Question (QuestionCode, QuestionresponseTypeCode) VALUES ('Q180', 'Select')
GO

UPDATE Q
SET Q.EntityTypeCode = 'ClassTrainer'
FROM form.Question Q
WHERE Q.QuestionCode IN ('Q080','Q081','Q082','Q083','Q084','Q170','Q171','Q172','Q173','Q174','Q175','Q176','Q177','Q178','Q179')
GO
--End table form.Question

--Begin table form.Text
TRUNCATE TABLE form.Text
GO

INSERT INTO form.Text
	(TextCode)
SELECT
	'T' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2)
FROM EFEUtility.dbo.Number N
WHERE N.Number <= 21
GO
--End table form.Text

--Begin table form.QuestionResponse
TRUNCATE TABLE form.QuestionResponse
GO

INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q003' AND N.Number <= 20
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q004' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q005' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q007' AND N.Number <= 6
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q009' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q011' AND N.Number <= 17
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q013' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q014' AND N.Number <= 10
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q017' AND N.Number <= 3
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q018' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q019' AND N.Number <= 10
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q020' AND N.Number <= 56
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q021' AND N.Number <= 56
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q022' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q023' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q024' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q025' AND N.Number <= 11
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q026' AND N.Number <= 11
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q027' AND N.Number <= 51
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q028' AND N.Number <= 9
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q029' AND N.Number <= 18
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q030' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q031' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q032' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q033' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q035' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q036' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q037' AND N.Number <= 51
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q038' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q040' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q041' AND N.Number <= 9
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q042' AND N.Number <= 14
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q043' AND N.Number <= 3
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q045' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q046' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q047' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q048' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q049' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q050' AND N.Number <= 13
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q051' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q053' AND N.Number <= 11
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q054' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q055' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q056' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q057' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q058' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q059' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q060' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q061' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q062' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q063' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q064' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q065' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q066' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q067' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q068' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q069' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q071' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q072' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q073' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q074' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q075' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q076' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q077' AND N.Number <= 9
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q080' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q081' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q082' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q083' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q086' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q087' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q091' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q092' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q093' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q094' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q095' AND N.Number <= 18
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q096' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q098' AND N.Number <= 14
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q099' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q100' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q101' AND N.Number <= 51
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q103' AND N.Number <= 6
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q107' AND N.Number <= 18
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q108' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q109' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q110' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q113' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q115' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q116' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q117' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q118' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q119' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q120' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q121' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q122' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q123' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q125' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q126' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q127' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q131' AND N.Number <= 11
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q132' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q133' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q134' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q135' AND N.Number <= 12
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q136' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q137' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q138' AND N.Number <= 10
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q140' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q143' AND N.Number <= 3
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q144' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q145' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q146' AND N.Number <= 51
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q147' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q149' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q150' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q151' AND N.Number <= 51
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q153' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q154' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q155' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q156' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q157' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q158' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q159' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q160' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q161' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q162' AND N.Number <= 4
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q164' AND N.Number <= 14
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q165' AND N.Number <= 8
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q166' AND N.Number <= 7
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q167' AND N.Number <= 2
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q169' AND N.Number <= 18
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q170' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q171' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q172' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q173' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q175' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q176' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q177' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q178' AND N.Number <= 5
INSERT INTO form.QuestionResponse (QuestionCode, QuestionResponseCode) SELECT Q.QuestionCode, Q.QuestionCode + '-' + RIGHT('00' + CAST(N.Number AS VARCHAR(5)), 2) FROM form.Question Q CROSS APPLY EFEUtility.dbo.Number N WHERE Q.QuestionCode = 'Q180' AND N.Number <= 10
GO
--End table form.QuestionResponse

--Begin table form.QuestionText
TRUNCATE TABLE form.QuestionText
GO

INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T09', 1 FROM form.Question Q WHERE Q.QuestionCode IN ('Q080','Q081','Q082','Q083','Q084')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T16', 0 FROM form.Question Q WHERE Q.QuestionCode IN ('Q030','Q031','Q032')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T17', 0 FROM form.Question Q WHERE Q.QuestionCode IN ('Q054','Q055','Q056','Q057','Q058','Q064','Q065','Q066','Q067','Q068','Q069','Q071','Q072','Q073','Q074','Q075','Q076')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T18', 2 FROM form.Question Q WHERE Q.QuestionCode IN ('Q059','Q060','Q061','Q062','Q063','Q080','Q081','Q082','Q083','Q170','Q171','Q172','Q173','Q175','Q176','Q177','Q178')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T19', 0 FROM form.Question Q WHERE Q.QuestionCode IN ('Q091','Q092','Q093','Q094')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T20', 1 FROM form.Question Q WHERE Q.QuestionCode IN ('Q170','Q171','Q172','Q173','Q174')
INSERT INTO form.QuestionText (QuestionCode, TextCode, DisplayOrder) SELECT Q.QuestionCode, 'T21', 1 FROM form.Question Q WHERE Q.QuestionCode IN ('Q175','Q176','Q177','Q178','Q179')
GO
--End table form.QuestionText

--Begin table form.FormQuestion / form.FormQuestionResponseMap
TRUNCATE TABLE form.FormQuestion
GO

TRUNCATE TABLE form.FormQuestionResponseMap
GO

--Begin form Entrepreneurship1
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q003', NULL, 'Q004'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q004', NULL, 'Q005'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q005', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q006', NULL, 'Q007'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q007', '1', 'Q008'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q007', '2,3,4,5', 'Q009'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q007', '6', 'Q013'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q008', NULL, 'Q013'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q009', '1', 'Q010'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q009', '2', 'Q011'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q010', NULL, 'Q012'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q011', NULL, 'Q008'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q012', NULL, 'Q011'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q013', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q015', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q017', NULL, 'Q035'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q018', NULL, 'Q040'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q022', NULL, 'Q038'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q023', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q024', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q026', NULL, 'Q042'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q033', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q034', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q035', NULL, 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q036', '2', 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q037', NULL, 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q038', '1', 'Q039'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q038', '2', 'Q041'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q039', NULL, 'Q041'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q040', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q041', NULL, 'Q026'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q042', NULL, 'Q043'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q043', NULL, 'Q044'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q044', NULL, 'Q033'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q180', '3,4,7,8', 'Q015'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q180', '1,2,9,10', 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'Q180', '6', 'Q034'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship1', 'T01', NULL, 'Q003'
GO

EXEC form.FormQuestionAddUpdate 'Entrepreneurship1', 'T01,Q003', 'Q42,Q43,Q44'
GO
--End form Entrepreneurship1

--Begin form Entrepreneurship2
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q055', NULL, 'Q066'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q064', NULL, 'Q065'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q065', NULL, 'Q055'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q066', NULL, 'Q067'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q067', NULL, 'Q068'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q068', NULL, 'Q069'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'Q069', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship2', 'T03', NULL, 'Q064'
GO

EXEC form.FormQuestionAddUpdate 'Entrepreneurship2', 'T03,Q064'
GO
--End form Entrepreneurship2

--Begin form Entrepreneurship3 
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q055', NULL, 'Q066'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q064', NULL, 'Q065'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q065', NULL, 'Q055'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q066', NULL, 'Q067'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q067', NULL, 'Q068'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q068', NULL, 'Q069'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q069', NULL, 'T86'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q078', NULL, 'Q079'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q079', NULL, 'Q080'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q080', NULL, 'Q081'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q081', NULL, 'Q082'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q082', NULL, 'Q083'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q083', NULL, 'Q084'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q084', NULL, 'Q170'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q085', NULL, 'T10'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q086', NULL, 'Q078'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q170', NULL, 'Q171'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q171', NULL, 'Q172'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q172', NULL, 'Q173'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q173', NULL, 'Q174'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q174', NULL, 'Q175'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q175', NULL, 'Q176'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q176', NULL, 'Q177'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q177', NULL, 'Q178'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q178', NULL, 'Q179'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'Q179', NULL, 'Q085'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship3', 'T06', NULL, 'Q064'
GO

EXEC form.FormQuestionAddUpdate 'Entrepreneurship3', 'T06,Q064'
GO
--Begin form Entrepreneurship3

--Begin form Entrepreneurship4
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q006', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q017', NULL, 'Q035'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q019', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q022', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q035', NULL, 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q036', '2', 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q037', NULL, 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q045', NULL, 'Q165'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q049', NULL, 'Q050'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q050', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q088', NULL, 'Q089'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q089', NULL, 'Q090'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q090', NULL, 'Q091'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q091', NULL, 'Q092'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q092', NULL, 'Q093'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q093', NULL, 'Q094'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q094', NULL, 'Q095'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q095', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q096', NULL, 'Q097'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q097', NULL, 'Q098'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q098', NULL, 'Q125'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q099', '2', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q099', '1', 'Q100'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q100', '3,4', 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q100', '1,2', 'Q102'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q101', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q102', NULL, 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q103', '6', 'Q045'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q103', '1,2,3,4,5', 'Q163'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q104', NULL, 'Q105'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q105', NULL, 'Q106'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q106', NULL, 'Q107'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q107', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q125', '2', 'Q099'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q125', '1', 'Q126'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q126', NULL, 'Q099'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q163', NULL, 'Q104'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q165', NULL, 'Q166'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q166', NULL, 'Q019'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q180', '1,2', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q180', '10', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q180', '3,4,6,7,8', 'Q088'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'Q180', '9', 'Q103'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship4', 'T13', NULL, 'Q014'
GO

EXEC form.FormQuestionAddUpdate 'Entrepreneurship4', 'T13,Q14'
GO
--End form Entrepreneurship4

--Begin form Entrepreneurship5
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q006', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q022', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q036', '2', 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q037', NULL, 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q045', NULL, 'Q165'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q049', NULL, 'Q050'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q050', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q088', NULL, 'Q089'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q089', NULL, 'Q090'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q090', NULL, 'Q091'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q091', NULL, 'Q092'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q092', NULL, 'Q093'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q093', NULL, 'Q094'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q094', NULL, 'Q095'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q095', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q096', NULL, 'Q097'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q097', NULL, 'Q099'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q099', '1', 'Q100'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q099', '2', 'Q140'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q100', '3,4', 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q100', '1,2', 'Q102'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q101', NULL, 'Q140'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q102', NULL, 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q125', '2', 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q125', '1', 'Q126'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q126', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q139', NULL, 'Q140'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q140', '2', 'Q125'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q140', '1', 'Q142'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q142', NULL, 'Q143'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q143', NULL, 'Q144'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q144', NULL, 'Q145'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q145', '1', 'Q146'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q145', '2', 'Q147'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q146', NULL, 'Q147'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q147', NULL, 'Q148'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q148', NULL, 'Q164'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q149', '2', 'Q125'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q149', '1', 'Q150'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q150', '3,4', 'Q151'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q150', '1,2', 'Q152'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q151', NULL, 'Q125'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q152', NULL, 'Q151'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q164', NULL, 'Q149'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q165', NULL, 'Q166'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q166', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q167', '1', 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q167', '2', 'Q139'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q180', '1,2,5', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q180', '9', 'Q045'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q180', '10', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'Q180', '3,4,6,7,8', 'Q088'
EXEC form.FormQuestionResponseMapAddUpdate 'Entrepreneurship5', 'T13', NULL, 'Q167'
GO

EXEC form.FormQuestionAddUpdate 'Entrepreneurship5', 'T13,Q167'
GO
--End form Entrepreneurship5

--Begin form FJIJ1
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q003', NULL, 'Q004'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q006', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q007', '2,3,4,5', 'Q008'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q007', '1,6', 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q008', NULL, 'Q009'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q009', '1', 'Q010'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q009', '2', 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q010', NULL, 'Q011'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q011', NULL, 'Q012'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q012', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q015', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q017', NULL, 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q018', '1,2,3,4,5,6', 'Q046'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q018', '7', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q019', NULL, 'Q051'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q022', NULL, 'Q007'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q023', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q024', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q026', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q034', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q046', NULL, 'Q047'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q047', NULL, 'Q048'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q048', NULL, 'Q019'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q049', NULL, 'T50'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q050', NULL, 'Q051'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q051', '1', 'Q052'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q051', '2', 'Q053'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q052', NULL, 'Q053'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q180', '3,4,7,8', 'Q015'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q180', '1,2,9,10', 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'Q180', '6', 'Q034'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ1', 'T01', NULL, 'Q003'
GO

EXEC form.FormQuestionAddUpdate 'FJIJ1', 'T01,Q003'
GO
--End form FJIJ1

--Begin form FJIJ2
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q057', NULL, 'Q071'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q071', NULL, 'Q072'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q072', NULL, 'Q073'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q073', NULL, 'Q074'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q074', NULL, 'Q075'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q075', NULL, 'Q076'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'Q076', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ2', 'T05', NULL, 'Q057'
GO

EXEC form.FormQuestionAddUpdate 'FJIJ2', 'T05,Q57'
GO
--End form FJIJ2

--Begin form FJIJ3
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q057', NULL, 'Q071'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q071', NULL, 'Q072'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q072', NULL, 'Q073'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q073', NULL, 'Q074'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q074', NULL, 'Q075'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q075', NULL, 'Q076'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q076', NULL, 'Q087'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q078', NULL, 'Q079'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q079', NULL, 'Q080'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q080', NULL, 'Q081'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q081', NULL, 'Q082'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q082', NULL, 'Q083'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q083', NULL, 'Q084'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q084', NULL, 'Q170'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q085', NULL, 'T10'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q087', NULL, 'Q078'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q170', NULL, 'Q171'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q171', NULL, 'Q172'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q172', NULL, 'Q173'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q173', NULL, 'Q174'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q174', NULL, 'Q175'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q175', NULL, 'Q176'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q176', NULL, 'Q177'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q177', NULL, 'Q178'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q178', NULL, 'Q179'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'Q179', NULL, 'Q085'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ3', 'T08', NULL, 'Q057'
GO

EXEC form.FormQuestionAddUpdate 'FJIJ3', 'T08,Q057'
GO
--End form FJIJ3

--Begin form FJIJ4
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q006', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q012', NULL, 'Q113'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q018', '7', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q018', '1,2,3,4,5,6', 'Q120'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q022', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q035', NULL, 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q036', '2', 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q049', NULL, 'Q050'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q050', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q096', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q110', '2', 'Q012'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q110', '1', 'Q111'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q111', NULL, 'Q113'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q113', '2', 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q113', '1', 'Q114'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q114', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q115', '1', 'Q116'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q115', '2', 'Q118'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q116', '1', 'Q117'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q116', '2', 'Q118'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q118', NULL, 'Q119'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q118', NULL, 'Q119'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q119', NULL, 'Q117'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q120', NULL, 'Q121'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q121', NULL, 'Q122'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q122', NULL, 'Q019'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q180', '1,2,9', 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q180', '10', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'Q180', '3,4,6,7,8', 'Q115'
EXEC form.FormQuestionResponseMapAddUpdate 'FJIJ4', 'T12', NULL, 'Q110'
GO

EXEC form.FormQuestionAddUpdate 'FJIJ4', 'T12,Q110'
GO
--End form FJIJ4

--Begin form JTP1
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q003', NULL, 'Q004'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q004', NULL, 'Q005'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q005', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q006', NULL, 'Q007'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q007', '1', 'Q008'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q007', '2,3,4,5', 'Q009'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q007', '6', 'Q013'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q008', NULL, 'Q013'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q009', '1', 'Q010'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q009', '2', 'Q011'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q010', NULL, 'Q012'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q011', NULL, 'Q008'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q012', NULL, 'Q011'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q013', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q015', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q017', NULL, 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q018', '1,2,3,4,5,6', 'Q019'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q018', '7', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q019', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q021', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q022', NULL, 'Q025'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q023', NULL, 'Q018'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q024', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q025', NULL, 'Q026'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q026', NULL, 'Q027'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q027', NULL, 'Q028'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q028', NULL, 'T29'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q029', NULL, 'Q030'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q030', NULL, 'Q031'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q031', NULL, 'Q032'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q032', NULL, 'Q033'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q033', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q034', NULL, 'Q023'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q180', '3,4,7,10', 'Q015'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q180', '1,2,8,9', 'Q024'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'Q180', '6', 'Q034'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP1', 'T01', NULL, 'Q003'
GO

EXEC form.FormQuestionAddUpdate 'JTP1', 'T01,Q003', 'Q028,Q029'
GO
--End form JTP1

--Begin form JTP2
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q054', NULL, 'Q055'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q055', NULL, 'Q056'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q056', NULL, 'Q057'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q057', NULL, 'Q058'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q058', NULL, 'Q059'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q059', NULL, 'Q060'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q060', NULL, 'Q061'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q061', NULL, 'Q062'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q062', NULL, 'Q063'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'Q063', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP2', 'T04', NULL, 'Q054'
GO

EXEC form.FormQuestionAddUpdate 'JTP2', 'T04,Q054'
GO
--End form JTP2

--Begin form JTP3
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q054', NULL, 'Q055'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q055', NULL, 'Q056'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q056', NULL, 'Q057'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q057', NULL, 'Q058'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q058', NULL, 'Q059'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q059', NULL, 'Q060'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q060', NULL, 'T61'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q061', NULL, 'Q062'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q062', NULL, 'Q063'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q063', NULL, 'Q077'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q077', NULL, 'Q078'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q078', NULL, 'Q079'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q079', NULL, 'Q080'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q080', NULL, 'Q081'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q081', NULL, 'Q082'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q082', NULL, 'Q083'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q083', NULL, 'Q084'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q084', NULL, 'Q170'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q085', NULL, 'T10'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q170', NULL, 'Q171'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q171', NULL, 'Q172'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q172', NULL, 'Q173'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q173', NULL, 'Q174'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q174', NULL, 'Q175'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q175', NULL, 'Q176'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q176', NULL, 'Q177'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q177', NULL, 'Q178'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q178', NULL, 'Q179'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'Q179', NULL, 'Q085'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP3', 'T07', NULL, 'Q054'
GO

EXEC form.FormQuestionAddUpdate 'JTP3', 'T07,Q054'
GO
--End form JTP3

--Begin form JTP4
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q006', NULL, 'Q059'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q017', NULL, 'Q035'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q019', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q022', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q035', NULL, 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q036', '2', 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q037', NULL, 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q045', NULL, 'Q165'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q049', NULL, 'Q050'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q050', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q059', NULL, 'Q060'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q060', NULL, 'Q061'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q061', NULL, 'Q062'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q062', NULL, 'Q063'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q063', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q088', NULL, 'Q089'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q089', NULL, 'Q090'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q090', NULL, 'Q091'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q091', NULL, 'Q092'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q092', NULL, 'Q093'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q093', NULL, 'Q094'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q094', NULL, 'Q095'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q095', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q096', NULL, 'Q097'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q097', NULL, 'Q098'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q098', NULL, 'Q099'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q099', '2', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q099', '1', 'Q100'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q100', '3,4', 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q100', '1,2', 'Q102'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q101', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q102', NULL, 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q103', '6', 'Q045'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q103', '1,2,3,4,5', 'Q163'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q104', NULL, 'Q105'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q105', NULL, 'Q106'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q106', NULL, 'Q107'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q107', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q163', NULL, 'Q104'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q165', NULL, 'Q166'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q166', NULL, 'Q019'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q180', '1,2', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q180', '10', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q180', '3,4,6,7,8', 'Q088'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'Q180', '9', 'Q103'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP4', 'T11', NULL, 'Q014'
GO

EXEC form.FormQuestionAddUpdate 'JTP4', 'T11,Q014'
GO
--End form JTP4

--Begin form JTP5
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q006', NULL, 'Q059'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q014', NULL, 'Q180'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q016', NULL, 'Q017'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q017', NULL, 'Q035'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q020', NULL, 'Q021'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q021', NULL, 'Q022'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q022', NULL, 'Q006'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q035', NULL, 'Q036'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q036', '1', 'Q037'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q036', '2', 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q037', NULL, 'Q096'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q049', NULL, 'Q050'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q050', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q059', NULL, 'Q060'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q060', NULL, 'Q061'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q061', NULL, 'Q062'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q062', NULL, 'Q063'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q063', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q091', NULL, 'Q092'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q092', NULL, 'Q093'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q093', NULL, 'Q094'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q094', NULL, 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q096', NULL, 'Q097'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q097', NULL, 'Q098'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q098', NULL, 'Q099'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q099', '2', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q099', '1', 'Q100'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q100', '3,4', 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q100', '1,2', 'Q102'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q101', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q102', NULL, 'Q101'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q123', '2', 'Q014'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q123', '1', 'Q129'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q127', '2', 'Q128'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q127', '1', 'Q133'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q128', NULL, 'Q131'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q129', NULL, 'Q130'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q130', NULL, 'Q168'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q132', NULL, 'Q091'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q133', NULL, 'Q134'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q134', NULL, 'Q091'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q135', NULL, 'Q136'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q136', NULL, 'Q137'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q137', NULL, 'Q138'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q138', NULL, 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q168', NULL, 'Q169'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q169', NULL, 'Q132'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q180', '5', 'Q016'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q180', '1,2,3,4,6,7,8', 'Q020'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q180', '10', 'Q049'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'Q180', '9', 'Q135'
EXEC form.FormQuestionResponseMapAddUpdate 'JTP5', 'T11', NULL, 'Q127'
GO

EXEC form.FormQuestionAddUpdate 'JTP5', 'T11,Q127'
GO
--End form JTP5

--Begin form SelfEsteem
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q153', NULL, 'Q154'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q154', NULL, 'Q155'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q155', NULL, 'Q156'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q156', NULL, 'Q157'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q157', NULL, 'Q158'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q158', NULL, 'Q159'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q159', NULL, 'Q160'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q160', NULL, 'Q161'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q161', NULL, 'Q162'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'Q162', NULL, 'T02'
EXEC form.FormQuestionResponseMapAddUpdate 'SelfEsteem', 'T14', NULL, 'Q153'
GO

EXEC form.FormQuestionAddUpdate 'SelfEsteem', 'T14,Q153'
GO
--End form SelfEsteem

UPDATE FQRM
SET FQRM.IsBranchedQuestion = 
	CASE
		WHEN FQRM.QuestionResponseCode IS NULL
		THEN 0
		ELSE 1
	END 
FROM form.FormQuestionResponseMap FQRM
GO
--End table form.FormQuestion / form.FormQuestionResponseMap

--Begin table form.EntityDataGroup
TRUNCATE TABLE form.EntityDataGroup
GO

INSERT INTO form.EntityDataGroup (EntityTypeCode, GroupNumber, QuestionCode) SELECT 'ClassTrainer', 1, Q.QuestionCode FROM form.Question Q WHERE Q.QuestionCode IN ('Q080','Q081','Q082','Q083','Q084')
INSERT INTO form.EntityDataGroup (EntityTypeCode, GroupNumber, QuestionCode) SELECT 'ClassTrainer', 2, Q.QuestionCode FROM form.Question Q WHERE Q.QuestionCode IN ('Q170','Q171','Q172','Q173','Q174')
INSERT INTO form.EntityDataGroup (EntityTypeCode, GroupNumber, QuestionCode) SELECT 'ClassTrainer', 3, Q.QuestionCode FROM form.Question Q WHERE Q.QuestionCode IN ('Q175','Q176','Q177','Q178','Q179')
GO

UPDATE EDG
SET EDG.IsFirstQuestion = 1
FROM form.EntityDataGroup EDG
WHERE EDG.QuestionCode IN ('Q080','Q170','Q175')
GO

UPDATE EDG
SET EDG.IsLastQuestion = 1
FROM form.EntityDataGroup EDG
WHERE EDG.QuestionCode IN ('Q084','Q174','Q179')
GO
--End table form.EntityDataGroup


