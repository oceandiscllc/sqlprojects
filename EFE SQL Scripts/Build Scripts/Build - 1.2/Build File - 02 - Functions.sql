USE EFE
GO

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has permission to a file
-- ==========================================================================

CREATE FUNCTION person.CheckFileAccess
(
@PersonID INT,
@PhysicalFileName VARCHAR(50)
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCheckFileAccess BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = @PersonID
			AND 
				(
				PP.PermissionableLineage = 
					(
					SELECT 'Document.View.' + DT.DocumentTypeCode
					FROM dbo.Document D
						JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
							AND D.PhysicalFileName = @PhysicalFileName
					)
					OR
						( 
						(SELECT D.DocumentTypeID FROM dbo.Document D WHERE D.PhysicalFileName = @PhysicalFileName) = 0
						)
				)
		)		
		SET @nCheckFileAccess = 1
	--ENDIF
	
	RETURN @nCheckFileAccess

END
GO
--End function person.CheckFileAccess