USE EFE
GO

--Begin table form.Question
DECLARE @TableName VARCHAR(250) = 'form.Question'

EXEC utility.AddColumn @TableName, 'MaximumResponseCount', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'MaximumResponseCount', 'INT', 0
GO

UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q029'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q042'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q077'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q086'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q087'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q095'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q098'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q107'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q117'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q131'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q132'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q164'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q169'
GO
--End table form.Question

--Begin table form.QuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'form.QuestionResponseChoice'

EXEC utility.DropIndex @TableName, 'IX_QuestionResponseChoice'

EXEC utility.DropColumn @TableName, 'ContactID'
EXEC utility.DropColumn @TableName, 'FormID'

EXEC utility.AddColumn @TableName, 'FormContactID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'FormContactID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionResponseChoice', 'FormContactID,QuestionResponseDate,DisplayOrder,QuestionCode,QuestionResponseCode'
GO
--End table form.QuestionResponseChoice

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.AddColumn @TableName, 'IsSuperAdministrator', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
GO
--End table permissionable.Permissionable
