-- File Name:	Build-1.3 File 01 - EFE.sql
-- Build Key:	Build-1.3 File 01 - EFE - 2016.05.13 21.55.43

USE EFE
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetOrganizationJobByOrganizationJobID
--		eventlog.LogFormContactAction
--		eventlog.LogFormTextAction
--		form.GetFormData
--		form.GetFormResponseData
--		permissionable.GetPermissionableByPermissionableID
--		permissionable.GetPermissionables
--		person.ValidateLogin
--		survey.GetSurveysByProgramIDAndContactID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table form.Question
DECLARE @TableName VARCHAR(250) = 'form.Question'

EXEC utility.AddColumn @TableName, 'MaximumResponseCount', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'MaximumResponseCount', 'INT', 0
GO

UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q029'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q042'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q077'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q086'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q087'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q095'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q098'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q107'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q117'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q131'
UPDATE form.Question SET MaximumResponseCount = 3 WHERE QuestionCode = 'Q132'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q164'
UPDATE form.Question SET MaximumResponseCount = 2 WHERE QuestionCode = 'Q169'
GO
--End table form.Question

--Begin table form.QuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'form.QuestionResponseChoice'

EXEC utility.DropIndex @TableName, 'IX_QuestionResponseChoice'

EXEC utility.DropColumn @TableName, 'ContactID'
EXEC utility.DropColumn @TableName, 'FormID'

EXEC utility.AddColumn @TableName, 'FormContactID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'FormContactID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_QuestionResponseChoice', 'FormContactID,QuestionResponseDate,DisplayOrder,QuestionCode,QuestionResponseCode'
GO
--End table form.QuestionResponseChoice

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.AddColumn @TableName, 'IsSuperAdministrator', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
GO
--End table permissionable.Permissionable

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure dbo.GetOrganizationJobByOrganizationJobID
EXEC Utility.DropObject 'dbo.GetOrganizationJobByOrganizationJobID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.29
-- Description:	A stored procedure to data from the dbo.OrganizationJob table
-- ==========================================================================
CREATE PROCEDURE dbo.GetOrganizationJobByOrganizationJobID

@OrganizationJobID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		OJ.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(OJ.AffiliateID) AS AffiliateName,
		O.OrganizationName,
		OJ.Address,
		OJ.AvailablePositions,
		OJ.CutoffDate,
		dbo.FormatDate(OJ.CutoffDate) AS CutoffDateFormatted,
		LEFT(DATENAME(month, OJ.CutoffDate), 3) + '-' + RIGHT(CAST(YEAR(OJ.CutoffDate) AS CHAR(4)), 2) AS CutoffDateFormattedShort,
		OJ.DistanceRequirements,
		OJ.JobDescription,
		OJ.JobLocation,
		OJ.OrganizationID,
		OJ.OrganizationJobID,
		OJ.OrganizationJobName,
		OJ.SkillsetID,
		OJ.TimingRequirements,
		SS.SkillsetName,
		OJ.ContactID,
		dbo.FormatContactNameByContactID(OJ.ContactID, 'LastFirst') AS ContactName
	FROM dbo.OrganizationJob OJ
		JOIN dbo.Organization O ON O.OrganizationID = OJ.OrganizationID
		JOIN dropdown.Skillset SS ON SS.SkillsetID = OJ.SkillsetID
			AND OJ.OrganizationJobID = @OrganizationJobID

	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		COJ.UpdateDate,
		dbo.FormatDate(COJ.UpdateDate) AS UpdateDateFormatted,
		COJS.ContactOrganizationJobStatusID,
		COJS.ContactOrganizationJobStatusName
	FROM dbo.ContactOrganizationJob COJ
		JOIN dbo.OrganizationJob OJ ON OJ.OrganizationJobID = COJ.OrganizationJobID
		JOIN dbo.Contact CO ON CO.ContactID = COJ.ContactID
		JOIN dropdown.ContactOrganizationJobStatus COJS ON COJS.ContactOrganizationJobStatusID = COJ.ContactOrganizationJobStatusID
			AND OJ.OrganizationJobID = @OrganizationJobID

END
GO
--End procedure dbo.GetOrganizationJobByOrganizationJobID

--Begin procedure eventlog.LogFormContactAction
EXEC Utility.DropObject 'eventlog.LogFormContactAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.05.03
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFormContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FormContact',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFormContactAction

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 
		TQ.Text, 
		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText,
		ISNULL((SELECT Q.MaximumResponseCount FROM form.Question Q WHERE Q.QuestionCode = TQ.ObjectCode), 0) AS MaximumResponseCount
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure form.GetFormResponseData
EXEC Utility.DropObject 'form.GetFormResponseData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.03
-- Description:	A procedure to get form & form reqponse data for a form render
-- ===========================================================================
CREATE PROCEDURE form.GetFormResponseData

@FormContactID INT,
@Locale VARCHAR(4) = 'EN'


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), EntityID INT, GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50))

	SELECT TOP 1
		@cEntityTypeCode = FC.EntityTypeCode,
		@nEntityID = FC.EntityID
	FROM form.FormContact FC
	WHERE FC.FormContactID = @FormContactID

	SET @nEntityID = ISNULL(@nEntityID, 0)

	IF @cEntityTypeCode = 'ClassTrainer'
		BEGIN

		INSERT INTO @tEntityDataGroup
			(EntityTypeCode, EntityText, EntityID, GroupNumber, QuestionCode)
		SELECT
			EDG.EntityTypeCode, 
			D.TrainerName,
			D.TrainerID,
			EDG.GroupNumber,
			EDG.QuestionCode
		FROM form.EntityDataGroup EDG
			JOIN 
				(
				SELECT
					'ClassTrainer' AS EntityTypeCode,
					1 AS GroupNumber, 
					T1.TrainerName,
					T1.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
						AND C.TrainerID1 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					2 AS GroupNumber, 
					T2.TrainerName,
					T2.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
						AND C.TrainerID2 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					3 AS GroupNumber, 
					T3.TrainerName,
					T3.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
						AND C.TrainerID3 > 0
						AND C.ClassID = @nEntityID
				) D ON D.EntityTypeCode = EDG.EntityTypeCode
					AND D.GroupNumber = EDG.GroupNumber

		END
	--ENDIF

	SELECT
		Q.QuestionCode AS ObjectCode,
		QL.QuestionText AS Text,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN 
			(
			SELECT DISTINCT
				QRC.QuestionCode,
				QRC.DisplayOrder
			FROM form.QuestionResponseChoice QRC
			WHERE QRC.FormContactID = @FormContactID
			) D ON D.QuestionCode = QL.QuestionCode
				AND QL.Locale = @Locale
	ORDER BY D.DisplayOrder

	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
			JOIN form.QuestionResponseChoice QRC ON QRC.QuestionCode = QT.QuestionCode
				AND QRC.FormContactID = @FormContactID
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		QRL.DisplayOrder
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.QuestionResponseChoice QRC ON QRC.QuestionResponseCode = QR.QuestionResponseCode
			AND QRC.FormContactID = @FormContactID
			AND QRL.Locale = @Locale

	UNION

	SELECT
		QRC.QuestionCode,
		QRC.QuestionResponseText AS Text,
		1 AS DisplayOrder
	FROM form.QuestionResponseChoice QRC
	WHERE QRC.QuestionResponseCode IS NULL
		AND QRC.FormContactID = @FormContactID

	ORDER BY QuestionCode, DisplayOrder
	
END
GO
--End procedure form.GetFormResponseData

--Begin procedure eventlog.LogFormTextAction
EXEC Utility.DropObject 'eventlog.LogFormTextAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFormTextAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FormText',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cTextLabels NVARCHAR(MAX) 
	
		SELECT 
			@cTextLabels = COALESCE(@cTextLabels, '') + D.TextLabels 
		FROM
			(
			SELECT
				(SELECT TL.* FOR XML RAW('TextLabels'), ELEMENTS) AS TextLabels
			FROM form.TextLabel TL
				JOIN form.Text T ON T.TextCode = TL.TextCode
					AND T.TextID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FormText',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<TextLabels>' + ISNULL(@cTextLabels, '') + '</TextLabels>') AS XML)
			FOR XML RAW('Form'), ELEMENTS
			)
		FROM form.Text T
		WHERE T.TextID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFormTextAction

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Brandon Green
-- Create date:	2015.10.02
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.09
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableID = P.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Added password expiration support
--
-- Author:			Todd Pires
-- Create date:	2016.05.08
-- Description:	Added the IsSuperAdministrator bit
-- ================================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone = P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE person.Person
				SET InvalidLoginAttempts = 0
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

END
GO
--End procedure person.ValidateLogin

--Begin procedure survey.GetSurveysByProgramIDAndContactID
EXEC Utility.DropObject 'survey.GetSurveysByProgramIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.19
-- Description:	A stored procedure to survey data assoicated with a programid and a contactid
-- ==========================================================================================
CREATE PROCEDURE survey.GetSurveysByProgramIDAndContactID

@ProgramID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PS.SurveyID,
		survey.GetSurveyNameBySurveyID(PS.SurveyID) AS SurveyName,
		ISNULL(OASR.SurveyResponseID, 0) AS SurveyResponseID,
		ISNULL(OASR.SurveyResponseStatus, 'Not Started') AS SurveyResponseStatus,
		survey.GetCorrectResponseCount(@ProgramID, PS.SurveyID, @ContactID) AS CorrectResponseCount,
		(
		SELECT 
			COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID) 
		FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		WHERE SQRCK.ProgramID = 
			CASE
				WHEN ST.SurveyTypeCode = 'ApplicationTest'
				THEN 0
				ELSE @ProgramID 
			END 
			AND SQRCK.SurveyID = PS.SurveyID 
			AND SQRCK.IsForScore = 1
		) AS TotalGradedQuestionCount,
		'Survey' AS EntityTypeCode
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
	OUTER APPLY
		(
		SELECT 
			SR.SurveyResponseID,
	
			CASE
				WHEN SR.IsSubmitted = 1
				THEN 'Submitted'
				ELSE 'Not Submitted'
			END AS SurveyResponseStatus
	
		FROM survey.SurveyResponse SR 
		WHERE SR.SurveyID = PS.SurveyID 
			AND SR.ContactID = @ContactID
		) OASR
	WHERE PS.ProgramID = @ProgramID
		
	UNION

	SELECT 
		F.FormID AS SurveyID,
		F.FormName AS SurveyName,
		FC.FormContactID AS SurveyResponseID,
		CASE
			WHEN ISNULL(FC.IsComplete, 0) = 1
			THEN 'Submitted'
			ELSE 'Not Started'
		END AS SurveyResponseStatus,
		0 AS CorrectResponseCount,
		0 AS TotalGradedQuestionCount,
		'Form' AS EntityTypeCode
	FROM program.Program P
		JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormContact FC ON FC.FormID = F.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID
		
END
GO
--End procedure survey.GetSurveysByProgramIDAndContactID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'BudgetTab', 'Proposal.AddUpdate.BudgetTab', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.3 File 01 - EFE - 2016.05.13 21.55.43')
GO
--End build tracking

