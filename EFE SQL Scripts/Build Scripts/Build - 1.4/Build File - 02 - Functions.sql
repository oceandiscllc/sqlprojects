USE EFE
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO-- ==============================================================================================
-- Author:		Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:		Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
--
-- Author:		Adam Davis
-- Create date:	2015.10.19
-- Description:	fixed reference to dbo.Contact (and fixed back)
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal NVARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF @Format = 'LastFirstMiddle' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID

--Begin function form.getEnglishResponseTextByQuestionResponseCode
EXEC utility.DropObject 'form.getEnglishResponseTextByQuestionResponseCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.29
-- Description:	A function to return an english response to a responsecode
-- =======================================================================
CREATE FUNCTION form.getEnglishResponseTextByQuestionResponseCode
(
@QuestionResponseCode VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cResponseText VARCHAR(MAX)

	SELECT @cResponseText = QRL.QuestionResponseText
	FROM form.QuestionResponseLabel QRL
	WHERE QRL.QuestionResponseCode = @QuestionResponseCode
		AND QRL.Locale = 'EN'

	RETURN @cResponseText

END
GO
--End function form.getEnglishResponseTextByQuestionResponseCode

--Begin function form.getProgramTypeNameByContactIDAndFormID
EXEC utility.DropObject 'form.getProgramTypeNameByContactIDAndFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:			Kevin Ross
-- Create date:	2016.05.31
-- Description:	A function to return an program type name
-- ======================================================
CREATE FUNCTION form.getProgramTypeNameByContactIDAndFormID
(
@ContactID INT,
@FormID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @ProgramTypeName VARCHAR(250)

	SELECT TOP 1 @ProgramTypeName = PT.ProgramTypeName
	FROM dropdown.ProgramType PT
	JOIN program.Program P ON P.ProgramTypeID = PT.ProgramTypeID
	JOIN form.ContactNextForm CNF ON CNF.ProgramID = P.ProgramID
	JOIN form.Form F ON F.FormCode = CNF.FormCode
		AND CNF.ContactID = @ContactID
		AND F.FormID = @FormID

	RETURN @ProgramTypeName

END
GO
--End function form.getProgramTypeNameByContactIDAndFormID



