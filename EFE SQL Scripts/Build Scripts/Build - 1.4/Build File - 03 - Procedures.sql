USE EFE
GO

--Begin procedure dbo.GetOrganizationByOrganizationID
EXEC Utility.DropObject 'dbo.GetOrganizationByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to data from the dbo.Organization table
--
-- Author:			Adam Davis
-- Create date:	2015.11.03
-- Description:	Added support for dbo.Organization Website column
-- ==========================================================================
CREATE PROCEDURE dbo.GetOrganizationByOrganizationID

@OrganizationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CCC.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		C1.CellPhoneNumber,
		C1.EmailAddress AS PrimaryContactEmailAddress,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS PrimaryContactName,
		C2.CountryID AS AddressCountryID,
		C2.CountryName AS AddressCountryName,
		C3.CountryID AS RegistrationCountryID,
		C3.CountryName AS RegistrationCountryName,
		O.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(O.AffiliateID) AS AffiliateName,
		O.Address,
		O.RegistrationNumber,
		O.OrganizationID,
		O.OrganizationName,
		O.TaxNumber,
		O.Comments,
		O.Website,
		ORT.OrganizationRelationshipTypeID,
		ORT.OrganizationRelationshipTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Organization') AS EntityTypeName
	FROM dbo.Organization O
		LEFT JOIN dbo.OrganizationContact OC ON OC.OrganizationID = O.OrganizationID 
			AND OC.IsPrimary = 1

		LEFT JOIN dbo.Contact C1 ON C1.ContactID = OC.ContactID
		LEFT JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		LEFT JOIN dropdown.Country C2 ON C2.CountryID = O.AddressCountryID
		LEFT JOIN dropdown.Country C3 ON C3.CountryID = O.RegistrationCountryID
		LEFT JOIN dropdown.OrganizationRelationshipType ORT ON ORT.OrganizationRelationshipTypeID = O.OrganizationRelationshipTypeID
		WHERE  O.OrganizationID = @OrganizationID

	SELECT
		CCC.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		C.CellPhoneNumber,
		C.EmailAddress AS OrganizationContactEmailAddress,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS OrganizationContactName,
		OC.OrganizationContactID,
		OC.IsPrimary,
		OC.OrganizationContactRoleID,
		CR.OrganizationContactRoleName,
		C.ContactID
	FROM dbo.OrganizationContact OC
		JOIN dbo.Contact C ON C.ContactID = OC.ContactID 
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.OrganizationContactRole CR ON CR.OrganizationContactRoleID = OC.OrganizationContactRoleID
	WHERE OC.OrganizationID = @OrganizationID

	SELECT 
		OT.OrganizationTypeID,
		OT.OrganizationTypeName
	FROM dbo.OrganizationOrganizationType OOT
		JOIN dropdown.OrganizationType OT ON OT.OrganizationTypeID = OOT.OrganizationTypeID
	WHERE OOT.OrganizationID = @OrganizationID

END
GO
--End procedure dbo.GetOrganizationJobByOrganizationJobID

--Begin procedure dropdown.GetFormData
EXEC Utility.DropObject 'dropdown.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A stored procedure to return data from the form.Form table
-- ==========================================================================
CREATE PROCEDURE dropdown.GetFormData

@IncludeZero BIT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FormID, 
		T.FormCode, 
		TL.FormText AS FormName
	FROM form.Form T
		JOIN form.FormLabel TL ON TL.FormCode = T.FormCode
			AND TL.Locale = @Locale
	WHERE (T.FormID > 0 OR @IncludeZero = 1)
	ORDER BY TL.FormText, T.FormID

END
GO
--End procedure dropdown.GetFormData

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		FL.FormText AS FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode 
			AND FL.Locale = @Locale
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = CL.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		C.ContactID
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID

	--ProgramTypeForms
	SELECT
		F.FormID,
		FL.FormText AS FormName
	FROM form.Form F
		JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = @Locale
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID

--Begin procedure proposal.GetProposalByProposalID
EXEC Utility.DropObject 'proposal.GetProposalByProposalID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================================
-- Author:		Adam Davis
-- Create date:	2015.10.16
-- Description: A stored procedure to return data from the dbo.Proposal table based on a ProposalID
--
-- Author:		Greg Yingling
-- Create date:	2016.03.15
-- Description: Add support for documents
-- ===============================================================================================
CREATE PROCEDURE proposal.GetProposalByProposalID

@ProposalID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nAffiliateID INT = (SELECT P.AffiliateID FROM dbo.Proposal P WHERE P.ProposalID = @ProposalID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Proposal', @ProposalID)

	SELECT
		Proposal.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(Proposal.AffiliateID) AS AffiliateName,
		Proposal.ProposalID
		,Proposal.DonorID
		,Proposal.PrincipalID
		,Proposal.BudgetID
		,Budget.BudgetName
		,Budget.CurrencyID
		,Proposal.WorkplanID
		,Proposal.DeliveryLeadID
		,Proposal.ProposalTypeID
		,Proposal.StartDate
		,dbo.FormatDate(Proposal.StartDate) AS StartDateFormatted
		,Proposal.EndDate
		,dbo.FormatDate(Proposal.EndDate) AS EndDateFormatted
		,Proposal.ProposalName
		,Proposal.ExecutiveSummary
		,Proposal.RationaleAndContext
		,Proposal.ProjectVisionAndDescription
		,Proposal.MonitoringAndEvaluation
		,Proposal.MarketingAndCommunications
		,Proposal.OrganisationalHistoryAndTrackRecord
		,Proposal.Annex1
		,Proposal.RiskManagement
		,Proposal.KnowledgeSharing
		,Proposal.ReferenceForMarketingCommunicationsSupport
		,Proposal.ProposalOutcome,
		Proposal.NarrativeTitle1,
		Proposal.NarrativeTitle2,
		Proposal.NarrativeTitle3,
		Proposal.NarrativeTitle4,
		Proposal.NarrativeTitle5,
		Proposal.NarrativeText1,
		Proposal.NarrativeText2,
		Proposal.NarrativeText3,
		Proposal.NarrativeText4,
		Proposal.NarrativeText5
		,Donor.OrganizationName AS DonorName
		,Donor.Website
		,Donor.Address AS DonorAddress
		,DonorCountry.CountryName AS DonorCountry
		,person.FormatPersonNameByPersonID(DeliveryLead.PersonID, 'lastfirst') AS DeliveryLeadName
		,dbo.FormatContactNameByContactID(Principal.ContactID, 'lastfirstmiddle') AS PrincipalName
		,Principal.Address1 AS PrincipalAddress1
		,Principal.Address2 AS PrincipalAddress2
		,Principal.City AS PrincipalCity
		,Principal.PostalCode AS PrincipalPostalCode
		,PrincipalCountry.CountryName AS PrincipalCountry
		,Principal.EmailAddress AS PrincipalEmailAddress
		,Principal.SkypeName 
		,PrincipalPhoneCallingCode.CountryCallingCode AS PhoneCallingCode
		,Principal.PhoneNumber
		,PrincipalCellCallingCode.CountryCallingCode AS CellCallingCode
		,Principal.CellPhoneNumber
		,ProposalType.ProposalTypeName
		,Workplan.WorkplanID
		,Workplan.WorkplanTitle
		,Award.AwardID
	FROM 
		dbo.Proposal AS Proposal
	LEFT JOIN
		dbo.Organization AS Donor ON Proposal.DonorID = Donor.OrganizationID
	LEFT JOIN 
		dropdown.Country AS DonorCountry ON Donor.AddressCountryID = DonorCountry.CountryID
	LEFT JOIN
		dbo.Contact AS Principal ON Principal.ContactID = Proposal.PrincipalID
	LEFT JOIN
		dropdown.Country AS PrincipalCountry ON Principal.CountryID = PrincipalCountry.CountryID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalPhoneCallingCode ON PrincipalPhoneCallingCode.CountryCallingCodeID = Principal.PhoneNumberCountryCallingCodeID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalCellCallingCode ON PrincipalCellCallingCode.CountryCallingCodeID = Principal.CellPhoneNumberCountryCallingCodeID
	LEFT JOIN
		person.Person AS DeliveryLead ON DeliveryLead.PersonID = Proposal.DeliveryLeadID
	INNER JOIN
		dropdown.ProposalType AS ProposalType ON ProposalType.ProposalTypeID = Proposal.ProposalTypeID
	LEFT JOIN
		workplan.Workplan AS Workplan ON Workplan.WorkplanID = Proposal.WorkplanID
	LEFT JOIN 
		budget.Budget AS Budget ON Budget.BudgetID = Proposal.BudgetID
	LEFT JOIN 
		dbo.Award AS Award ON Award.ProposalID = Proposal.ProposalID
	WHERE
		Proposal.ProposalID = @ProposalID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Proposal'
			AND DE.EntityID = @ProposalID

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('Proposal', @ProposalID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('Proposal', @ProposalID, @nWorkflowStepNumber, @nAffiliateID) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Proposal'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Proposal'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Proposal'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Proposal'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Proposal'
		AND EL.EntityID = @ProposalID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure proposal.GetProposalByProposalID

--Begin procedure reporting.GetProposalByProposalID
EXEC Utility.DropObject 'reporting.GetProposalByProposalID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Justin Brnaum
-- Create date:	2016.02.03
-- Description: A stored procedure to return data from the dbo.Proposal table based on a ProposalID
-- ================================================================================================
CREATE PROCEDURE reporting.GetProposalByProposalID

@ProposalID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		Proposal.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(Proposal.AffiliateID) AS AffiliateName,
		Proposal.ProposalID
		,Proposal.DonorID
		,Proposal.PrincipalID
		,Proposal.BudgetID
		,Budget.BudgetName
		,Budget.CurrencyID
		,Proposal.WorkplanID
		,Proposal.DeliveryLeadID
		,Proposal.ProposalTypeID
		,Proposal.StartDate
		,dbo.FormatDate(Proposal.StartDate) AS StartDateFormatted
		,Proposal.EndDate
		,dbo.FormatDate(Proposal.EndDate) AS EndDateFormatted
		,Proposal.ProposalName
		,Proposal.ExecutiveSummary
		,Proposal.RationaleAndContext
		,Proposal.ProjectVisionAndDescription
		,Proposal.MonitoringAndEvaluation
		,Proposal.MarketingAndCommunications
		,Proposal.OrganisationalHistoryAndTrackRecord
		,Proposal.Annex1
		,Proposal.RiskManagement
		,Proposal.KnowledgeSharing
		,Proposal.ReferenceForMarketingCommunicationsSupport
		,Proposal.ProposalOutcome
		,Donor.OrganizationName AS DonorName
		,Donor.Website
		,Donor.Address AS DonorAddress
		,DonorCountry.CountryName AS DonorCountry
		,person.FormatPersonNameByPersonID(DeliveryLead.PersonID, 'lastfirst') AS DeliveryLeadName
		,dbo.FormatContactNameByContactID(Principal.ContactID, 'lastfirstmiddle') AS PrincipalName
		,Principal.Address1 AS PrincipalAddress1
		,Principal.Address2 AS PrincipalAddress2
		,Principal.City AS PrincipalCity
		,Principal.PostalCode AS PrincipalPostalCode
		,PrincipalCountry.CountryName AS PrincipalCountry
		,Principal.EmailAddress AS PrincipalEmailAddress
		,Principal.SkypeName 
		,PrincipalPhoneCallingCode.CountryCallingCode AS PhoneCallingCode
		,Principal.PhoneNumber
		,PrincipalCellCallingCode.CountryCallingCode AS CellCallingCode
		,Principal.CellPhoneNumber
		,ProposalType.ProposalTypeName
		,Workplan.WorkplanTitle
		,Award.AwardID
		,Proposal.NarrativeTitle1
		,Proposal.NarrativeTitle2
		,Proposal.NarrativeTitle3
		,Proposal.NarrativeTitle4
		,Proposal.NarrativeTitle5
		,Proposal.NarrativeText1
		,Proposal.NarrativeText2
		,Proposal.NarrativeText3
		,Proposal.NarrativeText4
		,Proposal.NarrativeText5
	FROM 
		dbo.Proposal AS Proposal
	LEFT JOIN
		dbo.Organization AS Donor ON Proposal.DonorID = Donor.OrganizationID
	LEFT JOIN 
		dropdown.Country AS DonorCountry ON Donor.AddressCountryID = DonorCountry.CountryID
	LEFT JOIN
		dbo.Contact AS Principal ON Principal.ContactID = Proposal.PrincipalID
	LEFT JOIN
		dropdown.Country AS PrincipalCountry ON Principal.CountryID = PrincipalCountry.CountryID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalPhoneCallingCode ON PrincipalPhoneCallingCode.CountryCallingCodeID = Principal.PhoneNumberCountryCallingCodeID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalCellCallingCode ON PrincipalCellCallingCode.CountryCallingCodeID = Principal.CellPhoneNumberCountryCallingCodeID
	LEFT JOIN
		person.Person AS DeliveryLead ON DeliveryLead.PersonID = Proposal.DeliveryLeadID
	INNER JOIN
		dropdown.ProposalType AS ProposalType ON ProposalType.ProposalTypeID = Proposal.ProposalTypeID
	LEFT JOIN
		workplan.Workplan AS Workplan ON Workplan.WorkplanID = Proposal.WorkplanID
	LEFT JOIN 
		budget.Budget AS Budget ON Budget.BudgetID = Proposal.BudgetID
	LEFT JOIN 
		dbo.Award AS Award ON Award.ProposalID = Proposal.ProposalID
	WHERE
		Proposal.ProposalID = @ProposalID

END
GO
--End procedure reporting.GetProposalByProposalID