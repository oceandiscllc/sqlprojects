-- File Name:	Build-1.4 File 01 - EFE.sql
-- Build Key:	Build-1.4 File 01 - EFE - 2016.06.15 19.36.51

USE EFE
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatContactNameByContactID
--		form.getEnglishResponseTextByQuestionResponseCode
--		form.getProgramTypeNameByContactIDAndFormID
--
-- Procedures:
--		dbo.GetOrganizationByOrganizationID
--		dropdown.GetFormData
--		program.GetProgramByProgramID
--		proposal.GetProposalByProposalID
--		reporting.GetProposalByProposalID
--
-- Tables:
--		dbo.OrganizationOrganizationType
--		form.ContactNextForm
--		form.FormLabel
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table dbo.OrganizationOrganizationType
DECLARE @TableName VARCHAR(250) = 'dbo.OrganizationOrganizationType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.OrganizationOrganizationType
	(
	OrganizationOrganizationTypeID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT,
	OrganizationTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'OrganizationTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'OrganizationOrganizationTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_OrganizationOrganizationType', 'OrganizationID,OrganizationTypeID'
GO
--End table dbo.OrganizationOrganizationType

--Begin table dbo.Proposal
DECLARE @TableName VARCHAR(250) = 'dbo.Proposal'

EXEC utility.AddColumn @TableName, 'NarrativeTitle1', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle2', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle3', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle4', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle5', 'VARCHAR(250)'

EXEC utility.AddColumn @TableName, 'NarrativeText1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText5', 'VARCHAR(MAX)'
GO
--End table dbo.Proposal

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.AddColumn @TableName, 'ContactTypeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.ContactType
SET ContactTypeCode = 'Beneficiary'
WHERE ContactTypeName = 'Beneficiary'
GO
--End table dropdown.ContactType

--Begin form.ContactNextForm
DECLARE @TableName VARCHAR(250) = 'form.ContactNextForm'

EXEC utility.DropObject @TableName

CREATE TABLE form.ContactNextForm
	(
	ContactNextFormID INT NOT NULL IDENTITY(1,1),
	ContactID INT,
	ProgramID INT,
	ClassID INT,
	FormCode VARCHAR(50),
	SendDate DATE,
	NextFormSendDate DATE,
	SubmitDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_FormLabel', 'ContactID,ProgramID,ContactNextFormID DESC'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactNextFormID'
GO
--End table form.ContactNextForm

--Begin form.Form
DECLARE @TableName VARCHAR(250) = 'form.Form'

EXEC utility.DropColumn @TableName, 'Formname'
GO
--End form.Form

--Begin form.FormLabel
DECLARE @TableName VARCHAR(250) = 'form.FormLabel'

EXEC utility.DropObject @TableName

CREATE TABLE form.FormLabel
	(
	FormLabelID INT NOT NULL IDENTITY(1,1),
	FormCode VARCHAR(50),
	Locale VARCHAR(4),
	FormText NVARCHAR(MAX)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_FormLabel', 'FormCode,Locale'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FormLabelID'
GO
--End table form.FormLabel

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO-- ==============================================================================================
-- Author:		Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:		Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
--
-- Author:		Adam Davis
-- Create date:	2015.10.19
-- Description:	fixed reference to dbo.Contact (and fixed back)
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal NVARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF @Format = 'LastFirstMiddle' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID

--Begin function form.getEnglishResponseTextByQuestionResponseCode
EXEC utility.DropObject 'form.getEnglishResponseTextByQuestionResponseCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.29
-- Description:	A function to return an english response to a responsecode
-- =======================================================================
CREATE FUNCTION form.getEnglishResponseTextByQuestionResponseCode
(
@QuestionResponseCode VARCHAR(50)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cResponseText VARCHAR(MAX)

	SELECT @cResponseText = QRL.QuestionResponseText
	FROM form.QuestionResponseLabel QRL
	WHERE QRL.QuestionResponseCode = @QuestionResponseCode
		AND QRL.Locale = 'EN'

	RETURN @cResponseText

END
GO
--End function form.getEnglishResponseTextByQuestionResponseCode

--Begin function form.getProgramTypeNameByContactIDAndFormID
EXEC utility.DropObject 'form.getProgramTypeNameByContactIDAndFormID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:			Kevin Ross
-- Create date:	2016.05.31
-- Description:	A function to return an program type name
-- ======================================================
CREATE FUNCTION form.getProgramTypeNameByContactIDAndFormID
(
@ContactID INT,
@FormID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @ProgramTypeName VARCHAR(250)

	SELECT TOP 1 @ProgramTypeName = PT.ProgramTypeName
	FROM dropdown.ProgramType PT
	JOIN program.Program P ON P.ProgramTypeID = PT.ProgramTypeID
	JOIN form.ContactNextForm CNF ON CNF.ProgramID = P.ProgramID
	JOIN form.Form F ON F.FormCode = CNF.FormCode
		AND CNF.ContactID = @ContactID
		AND F.FormID = @FormID

	RETURN @ProgramTypeName

END
GO
--End function form.getProgramTypeNameByContactIDAndFormID




--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure dbo.GetOrganizationByOrganizationID
EXEC Utility.DropObject 'dbo.GetOrganizationByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to data from the dbo.Organization table
--
-- Author:			Adam Davis
-- Create date:	2015.11.03
-- Description:	Added support for dbo.Organization Website column
-- ==========================================================================
CREATE PROCEDURE dbo.GetOrganizationByOrganizationID

@OrganizationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CCC.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		C1.CellPhoneNumber,
		C1.EmailAddress AS PrimaryContactEmailAddress,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS PrimaryContactName,
		C2.CountryID AS AddressCountryID,
		C2.CountryName AS AddressCountryName,
		C3.CountryID AS RegistrationCountryID,
		C3.CountryName AS RegistrationCountryName,
		O.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(O.AffiliateID) AS AffiliateName,
		O.Address,
		O.RegistrationNumber,
		O.OrganizationID,
		O.OrganizationName,
		O.TaxNumber,
		O.Comments,
		O.Website,
		ORT.OrganizationRelationshipTypeID,
		ORT.OrganizationRelationshipTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Organization') AS EntityTypeName
	FROM dbo.Organization O
		LEFT JOIN dbo.OrganizationContact OC ON OC.OrganizationID = O.OrganizationID 
			AND OC.IsPrimary = 1

		LEFT JOIN dbo.Contact C1 ON C1.ContactID = OC.ContactID
		LEFT JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		LEFT JOIN dropdown.Country C2 ON C2.CountryID = O.AddressCountryID
		LEFT JOIN dropdown.Country C3 ON C3.CountryID = O.RegistrationCountryID
		LEFT JOIN dropdown.OrganizationRelationshipType ORT ON ORT.OrganizationRelationshipTypeID = O.OrganizationRelationshipTypeID
		WHERE  O.OrganizationID = @OrganizationID

	SELECT
		CCC.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		C.CellPhoneNumber,
		C.EmailAddress AS OrganizationContactEmailAddress,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS OrganizationContactName,
		OC.OrganizationContactID,
		OC.IsPrimary,
		OC.OrganizationContactRoleID,
		CR.OrganizationContactRoleName,
		C.ContactID
	FROM dbo.OrganizationContact OC
		JOIN dbo.Contact C ON C.ContactID = OC.ContactID 
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.OrganizationContactRole CR ON CR.OrganizationContactRoleID = OC.OrganizationContactRoleID
	WHERE OC.OrganizationID = @OrganizationID

	SELECT 
		OT.OrganizationTypeID,
		OT.OrganizationTypeName
	FROM dbo.OrganizationOrganizationType OOT
		JOIN dropdown.OrganizationType OT ON OT.OrganizationTypeID = OOT.OrganizationTypeID
	WHERE OOT.OrganizationID = @OrganizationID

END
GO
--End procedure dbo.GetOrganizationJobByOrganizationJobID

--Begin procedure dropdown.GetFormData
EXEC Utility.DropObject 'dropdown.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.21
-- Description:	A stored procedure to return data from the form.Form table
-- ==========================================================================
CREATE PROCEDURE dropdown.GetFormData

@IncludeZero BIT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FormID, 
		T.FormCode, 
		TL.FormText AS FormName
	FROM form.Form T
		JOIN form.FormLabel TL ON TL.FormCode = T.FormCode
			AND TL.Locale = @Locale
	WHERE (T.FormID > 0 OR @IncludeZero = 1)
	ORDER BY TL.FormText, T.FormID

END
GO
--End procedure dropdown.GetFormData

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		FL.FormText AS FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode 
			AND FL.Locale = @Locale
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = CL.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		C.ContactID
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID

	--ProgramTypeForms
	SELECT
		F.FormID,
		FL.FormText AS FormName
	FROM form.Form F
		JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = @Locale
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID

--Begin procedure proposal.GetProposalByProposalID
EXEC Utility.DropObject 'proposal.GetProposalByProposalID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================================
-- Author:		Adam Davis
-- Create date:	2015.10.16
-- Description: A stored procedure to return data from the dbo.Proposal table based on a ProposalID
--
-- Author:		Greg Yingling
-- Create date:	2016.03.15
-- Description: Add support for documents
-- ===============================================================================================
CREATE PROCEDURE proposal.GetProposalByProposalID

@ProposalID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nAffiliateID INT = (SELECT P.AffiliateID FROM dbo.Proposal P WHERE P.ProposalID = @ProposalID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Proposal', @ProposalID)

	SELECT
		Proposal.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(Proposal.AffiliateID) AS AffiliateName,
		Proposal.ProposalID
		,Proposal.DonorID
		,Proposal.PrincipalID
		,Proposal.BudgetID
		,Budget.BudgetName
		,Budget.CurrencyID
		,Proposal.WorkplanID
		,Proposal.DeliveryLeadID
		,Proposal.ProposalTypeID
		,Proposal.StartDate
		,dbo.FormatDate(Proposal.StartDate) AS StartDateFormatted
		,Proposal.EndDate
		,dbo.FormatDate(Proposal.EndDate) AS EndDateFormatted
		,Proposal.ProposalName
		,Proposal.ExecutiveSummary
		,Proposal.RationaleAndContext
		,Proposal.ProjectVisionAndDescription
		,Proposal.MonitoringAndEvaluation
		,Proposal.MarketingAndCommunications
		,Proposal.OrganisationalHistoryAndTrackRecord
		,Proposal.Annex1
		,Proposal.RiskManagement
		,Proposal.KnowledgeSharing
		,Proposal.ReferenceForMarketingCommunicationsSupport
		,Proposal.ProposalOutcome,
		Proposal.NarrativeTitle1,
		Proposal.NarrativeTitle2,
		Proposal.NarrativeTitle3,
		Proposal.NarrativeTitle4,
		Proposal.NarrativeTitle5,
		Proposal.NarrativeText1,
		Proposal.NarrativeText2,
		Proposal.NarrativeText3,
		Proposal.NarrativeText4,
		Proposal.NarrativeText5
		,Donor.OrganizationName AS DonorName
		,Donor.Website
		,Donor.Address AS DonorAddress
		,DonorCountry.CountryName AS DonorCountry
		,person.FormatPersonNameByPersonID(DeliveryLead.PersonID, 'lastfirst') AS DeliveryLeadName
		,dbo.FormatContactNameByContactID(Principal.ContactID, 'lastfirstmiddle') AS PrincipalName
		,Principal.Address1 AS PrincipalAddress1
		,Principal.Address2 AS PrincipalAddress2
		,Principal.City AS PrincipalCity
		,Principal.PostalCode AS PrincipalPostalCode
		,PrincipalCountry.CountryName AS PrincipalCountry
		,Principal.EmailAddress AS PrincipalEmailAddress
		,Principal.SkypeName 
		,PrincipalPhoneCallingCode.CountryCallingCode AS PhoneCallingCode
		,Principal.PhoneNumber
		,PrincipalCellCallingCode.CountryCallingCode AS CellCallingCode
		,Principal.CellPhoneNumber
		,ProposalType.ProposalTypeName
		,Workplan.WorkplanID
		,Workplan.WorkplanTitle
		,Award.AwardID
	FROM 
		dbo.Proposal AS Proposal
	LEFT JOIN
		dbo.Organization AS Donor ON Proposal.DonorID = Donor.OrganizationID
	LEFT JOIN 
		dropdown.Country AS DonorCountry ON Donor.AddressCountryID = DonorCountry.CountryID
	LEFT JOIN
		dbo.Contact AS Principal ON Principal.ContactID = Proposal.PrincipalID
	LEFT JOIN
		dropdown.Country AS PrincipalCountry ON Principal.CountryID = PrincipalCountry.CountryID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalPhoneCallingCode ON PrincipalPhoneCallingCode.CountryCallingCodeID = Principal.PhoneNumberCountryCallingCodeID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalCellCallingCode ON PrincipalCellCallingCode.CountryCallingCodeID = Principal.CellPhoneNumberCountryCallingCodeID
	LEFT JOIN
		person.Person AS DeliveryLead ON DeliveryLead.PersonID = Proposal.DeliveryLeadID
	INNER JOIN
		dropdown.ProposalType AS ProposalType ON ProposalType.ProposalTypeID = Proposal.ProposalTypeID
	LEFT JOIN
		workplan.Workplan AS Workplan ON Workplan.WorkplanID = Proposal.WorkplanID
	LEFT JOIN 
		budget.Budget AS Budget ON Budget.BudgetID = Proposal.BudgetID
	LEFT JOIN 
		dbo.Award AS Award ON Award.ProposalID = Proposal.ProposalID
	WHERE
		Proposal.ProposalID = @ProposalID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Proposal'
			AND DE.EntityID = @ProposalID

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('Proposal', @ProposalID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('Proposal', @ProposalID, @nWorkflowStepNumber, @nAffiliateID) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Proposal'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Proposal'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Proposal'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Proposal'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Proposal'
		AND EL.EntityID = @ProposalID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure proposal.GetProposalByProposalID

--Begin procedure reporting.GetProposalByProposalID
EXEC Utility.DropObject 'reporting.GetProposalByProposalID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Justin Brnaum
-- Create date:	2016.02.03
-- Description: A stored procedure to return data from the dbo.Proposal table based on a ProposalID
-- ================================================================================================
CREATE PROCEDURE reporting.GetProposalByProposalID

@ProposalID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		Proposal.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(Proposal.AffiliateID) AS AffiliateName,
		Proposal.ProposalID
		,Proposal.DonorID
		,Proposal.PrincipalID
		,Proposal.BudgetID
		,Budget.BudgetName
		,Budget.CurrencyID
		,Proposal.WorkplanID
		,Proposal.DeliveryLeadID
		,Proposal.ProposalTypeID
		,Proposal.StartDate
		,dbo.FormatDate(Proposal.StartDate) AS StartDateFormatted
		,Proposal.EndDate
		,dbo.FormatDate(Proposal.EndDate) AS EndDateFormatted
		,Proposal.ProposalName
		,Proposal.ExecutiveSummary
		,Proposal.RationaleAndContext
		,Proposal.ProjectVisionAndDescription
		,Proposal.MonitoringAndEvaluation
		,Proposal.MarketingAndCommunications
		,Proposal.OrganisationalHistoryAndTrackRecord
		,Proposal.Annex1
		,Proposal.RiskManagement
		,Proposal.KnowledgeSharing
		,Proposal.ReferenceForMarketingCommunicationsSupport
		,Proposal.ProposalOutcome
		,Donor.OrganizationName AS DonorName
		,Donor.Website
		,Donor.Address AS DonorAddress
		,DonorCountry.CountryName AS DonorCountry
		,person.FormatPersonNameByPersonID(DeliveryLead.PersonID, 'lastfirst') AS DeliveryLeadName
		,dbo.FormatContactNameByContactID(Principal.ContactID, 'lastfirstmiddle') AS PrincipalName
		,Principal.Address1 AS PrincipalAddress1
		,Principal.Address2 AS PrincipalAddress2
		,Principal.City AS PrincipalCity
		,Principal.PostalCode AS PrincipalPostalCode
		,PrincipalCountry.CountryName AS PrincipalCountry
		,Principal.EmailAddress AS PrincipalEmailAddress
		,Principal.SkypeName 
		,PrincipalPhoneCallingCode.CountryCallingCode AS PhoneCallingCode
		,Principal.PhoneNumber
		,PrincipalCellCallingCode.CountryCallingCode AS CellCallingCode
		,Principal.CellPhoneNumber
		,ProposalType.ProposalTypeName
		,Workplan.WorkplanTitle
		,Award.AwardID
		,Proposal.NarrativeTitle1
		,Proposal.NarrativeTitle2
		,Proposal.NarrativeTitle3
		,Proposal.NarrativeTitle4
		,Proposal.NarrativeTitle5
		,Proposal.NarrativeText1
		,Proposal.NarrativeText2
		,Proposal.NarrativeText3
		,Proposal.NarrativeText4
		,Proposal.NarrativeText5
	FROM 
		dbo.Proposal AS Proposal
	LEFT JOIN
		dbo.Organization AS Donor ON Proposal.DonorID = Donor.OrganizationID
	LEFT JOIN 
		dropdown.Country AS DonorCountry ON Donor.AddressCountryID = DonorCountry.CountryID
	LEFT JOIN
		dbo.Contact AS Principal ON Principal.ContactID = Proposal.PrincipalID
	LEFT JOIN
		dropdown.Country AS PrincipalCountry ON Principal.CountryID = PrincipalCountry.CountryID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalPhoneCallingCode ON PrincipalPhoneCallingCode.CountryCallingCodeID = Principal.PhoneNumberCountryCallingCodeID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalCellCallingCode ON PrincipalCellCallingCode.CountryCallingCodeID = Principal.CellPhoneNumberCountryCallingCodeID
	LEFT JOIN
		person.Person AS DeliveryLead ON DeliveryLead.PersonID = Proposal.DeliveryLeadID
	INNER JOIN
		dropdown.ProposalType AS ProposalType ON ProposalType.ProposalTypeID = Proposal.ProposalTypeID
	LEFT JOIN
		workplan.Workplan AS Workplan ON Workplan.WorkplanID = Proposal.WorkplanID
	LEFT JOIN 
		budget.Budget AS Budget ON Budget.BudgetID = Proposal.BudgetID
	LEFT JOIN 
		dbo.Award AS Award ON Award.ProposalID = Proposal.ProposalID
	WHERE
		Proposal.ProposalID = @ProposalID

END
GO
--End procedure reporting.GetProposalByProposalID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.MenuItemCode = 'ContactListBeneficiary',
	MI.MenuItemLink = '/contact/list?ContactTypeCode=Beneficiary'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
	
INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT
	MI.MenuItemID,
	'Contact.List.Beneficiary'	
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
GO	

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @NewMenuItemCode='ContactListBeneficiary', @NewMenuItemLink='/contact/list?ContactTypeCode=Beneficiary', @NewMenuItemText='Beneficiaries', @PermissionableLineageList='Contact.List.Beneficiary'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @NewMenuItemCode='ContactListNonBeneficiary', @NewMenuItemLink='/contact/list?ContactTypeCode=NonBeneficiary', @AfterMenuItemCode='ContactListBeneficiary', @NewMenuItemText='Organization Contacts', @PermissionableLineageList='Contact.List.NonBeneficiary'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='ProgramManagement'
GO
--End table dbo.MenuItem

--Begin table dropdown.Role
IF NOT EXISTS (SELECT 1 FROM dropdown.Role R WHERE R.RoleName = 'Coordinator')
	INSERT INTO dropdown.Role (RoleName) VALUES ('Coordinator')
--ENDIF
GO
--End dropdown.Role

--Begin table form.FormLabel
TRUNCATE TABLE form.FormLabel
GO

--Begin AR
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'AR', N'??? ??????? ?????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'AR', N'??????? ??? ??? ?????? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'AR', N'??????? ??? ?????? ?????? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'AR', N'??????? ????? ????? ?????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'AR', N'??????? ?????? ???? ????? ?????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'AR', N' ??? ??????? ?????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'AR', N'??????? ??? ??? ?????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'AR', N'??????? ??? ?????? ?????? ????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'AR', N'??????? ?????? 4 ???? ??? ?????? ?? ????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'AR', N'??????? ??????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'AR', N'??????? ??? ??? ?????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'AR', N'??????? ??? ?????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'AR', N'??????? ?? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'AR', N'??????? ?? ???????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'AR', N'??????? ?? ????? ?????')
GO

--Begin AREG
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'AREG', N'??? ??????? ?????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'AREG', N'??????? ??? ??? ?????? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'AREG', N'??????? ??? ?????? ?????? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'AREG', N'??????? ????? ????? ?????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'AREG', N'??????? ?????? ???? ????? ?????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'AREG', N' ??? ??????? ?????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'AREG', N'??????? ??? ??? ?????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'AREG', N'??????? ??? ?????? ?????? ????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'AREG', N'??????? ?????? 4 ???? ??? ?????? ?? ????? "????? ?? ??? ?? ???"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'AREG', N'??????? ??????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'AREG', N'??????? ??? ??? ?????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'AREG', N'??????? ??? ?????? ??????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'AREG', N'??????? ?? ??????? ???????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'AREG', N'??????? ?? ???????? ????????')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'AREG', N'??????? ?? ????? ?????')
GO

--Begin EN
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'EN', N'Entrepreneurship Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'EN', N'Entrepreneurship Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'EN', N'Entrepreneurship Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'EN', N'Business Creation Follow-Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'EN', N'Business Follow-Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'EN', N'Finding a Job is a Job Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'EN', N'Finding a Job is a Job Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'EN', N'Finding a Job is a Job Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'EN', N'Finding a Job is a Job Four-Month Follow Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'EN', N'Job Training and Placement Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'EN', N'Job Training and Placement Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'EN', N'Job Training and Placement Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'EN', N'Job Placement Status Check')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'EN', N'Job Retention Status Check')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'EN', N'Self-Esteem Questionnaire')
GO

--Begin FR
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'FR', N'Demande d''Inscription au Programme d''Entrepreneuriat')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'FR', N'Programme Entreprenariat: Questionnaire Pr�-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'FR', N'Programme Entreprenariat: Questionnaire Post-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'FR', N'Questionnaire de Suivi de la Cr�ation d''Entreprise')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'FR', N'Questionnaire de Suivi du Projet d''Entreprise')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'FR', N'Demande d''Inscription au Programme "Trouver un Emploi est un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'FR', N'Questionnaire Pr�-formation du Programme "Trouver un Emploi est Un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'FR', N'Questionnaire Post-Formation du Programme  "Trouver un Emploi est Un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'FR', N'Questionnaire de Suivi 4 Mois Apr�s la Formation "Trouver un Emploi est un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'FR', N'Demande d''Inscription � la Formation Insertion Professionnelle')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'FR', N'Formation - Insertion Professionnelle: Questionnaire Pr�-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'FR', N'Formation - Insertion Professionnelle: Questionnaire Post-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'FR', N'Questionnaire sur la Situation Professionnelle')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'FR', N'Questionnaire sur la R�tention du Poste')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'FR', N'Questionnaire sur l''Estime de Soi')
GO
--End table form.FormLabel

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'NonBeneficiary', 'Contact.List.NonBeneficiary', 'View the list of contacts not of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', 'Budget', 'Award.AddUpdate.Budget', 'Add / edit an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', 'Budget', 'Award.View.Budget', 'View an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'Budget', 'Proposal.AddUpdate.Budget', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', 'Budget', 'Proposal.View.Budget', 'View a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', 'Budget', 'SubAward.AddUpdate.Budget', 'Add / edit a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', 'Budget', 'SubAward.View.Budget', 'View a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.4 File 01 - EFE - 2016.06.15 19.36.51')
GO
--End build tracking

