USE EFE
GO

--Begin table dbo.OrganizationOrganizationType
DECLARE @TableName VARCHAR(250) = 'dbo.OrganizationOrganizationType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.OrganizationOrganizationType
	(
	OrganizationOrganizationTypeID INT IDENTITY(1,1) NOT NULL,
	OrganizationID INT,
	OrganizationTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'OrganizationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'OrganizationTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'OrganizationOrganizationTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_OrganizationOrganizationType', 'OrganizationID,OrganizationTypeID'
GO
--End table dbo.OrganizationOrganizationType

--Begin table dbo.Proposal
DECLARE @TableName VARCHAR(250) = 'dbo.Proposal'

EXEC utility.AddColumn @TableName, 'NarrativeTitle1', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle2', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle3', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle4', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'NarrativeTitle5', 'VARCHAR(250)'

EXEC utility.AddColumn @TableName, 'NarrativeText1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'NarrativeText5', 'VARCHAR(MAX)'
GO
--End table dbo.Proposal

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.AddColumn @TableName, 'ContactTypeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.ContactType
SET ContactTypeCode = 'Beneficiary'
WHERE ContactTypeName = 'Beneficiary'
GO
--End table dropdown.ContactType

--Begin form.ContactNextForm
DECLARE @TableName VARCHAR(250) = 'form.ContactNextForm'

EXEC utility.DropObject @TableName

CREATE TABLE form.ContactNextForm
	(
	ContactNextFormID INT NOT NULL IDENTITY(1,1),
	ContactID INT,
	ProgramID INT,
	ClassID INT,
	FormCode VARCHAR(50),
	SendDate DATE,
	NextFormSendDate DATE,
	SubmitDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_FormLabel', 'ContactID,ProgramID,ContactNextFormID DESC'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactNextFormID'
GO
--End table form.ContactNextForm

--Begin form.Form
DECLARE @TableName VARCHAR(250) = 'form.Form'

EXEC utility.DropColumn @TableName, 'Formname'
GO
--End form.Form

--Begin form.FormLabel
DECLARE @TableName VARCHAR(250) = 'form.FormLabel'

EXEC utility.DropObject @TableName

CREATE TABLE form.FormLabel
	(
	FormLabelID INT NOT NULL IDENTITY(1,1),
	FormCode VARCHAR(50),
	Locale VARCHAR(4),
	FormText NVARCHAR(MAX)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_FormLabel', 'FormCode,Locale'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FormLabelID'
GO
--End table form.FormLabel
