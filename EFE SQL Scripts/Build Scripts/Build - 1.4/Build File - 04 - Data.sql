﻿USE EFE
GO

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.MenuItemCode = 'ContactListBeneficiary',
	MI.MenuItemLink = '/contact/list?ContactTypeCode=Beneficiary'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
	
INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT
	MI.MenuItemID,
	'Contact.List.Beneficiary'	
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList', 'ContactListBeneficiary')
GO	

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @NewMenuItemCode='ContactListBeneficiary', @NewMenuItemLink='/contact/list?ContactTypeCode=Beneficiary', @NewMenuItemText='Beneficiaries', @PermissionableLineageList='Contact.List.Beneficiary'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @NewMenuItemCode='ContactListNonBeneficiary', @NewMenuItemLink='/contact/list?ContactTypeCode=NonBeneficiary', @AfterMenuItemCode='ContactListBeneficiary', @NewMenuItemText='Organization Contacts', @PermissionableLineageList='Contact.List.NonBeneficiary'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='ProgramManagement'
GO
--End table dbo.MenuItem

--Begin table dropdown.Role
IF NOT EXISTS (SELECT 1 FROM dropdown.Role R WHERE R.RoleName = 'Coordinator')
	INSERT INTO dropdown.Role (RoleName) VALUES ('Coordinator')
--ENDIF
GO
--End dropdown.Role

--Begin table form.FormLabel
TRUNCATE TABLE form.FormLabel
GO

--Begin AR
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'AR', N'طلب التقديم لتدريب الريادة')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'AR', N'استبيان قبل بدء برنامج التدريب الريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'AR', N'استبيان بعد انتهاء برنامج التدريب الريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'AR', N'استبيان إنشاء مشروع ريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'AR', N'استبيان متابعة حالة مشروع ريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'AR', N' طلب التقديم لتدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'AR', N'استبيان قبل بدء برنامج "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'AR', N'استبيان بعد انتهاء برنامج تدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'AR', N'استبيان متابعة 4 أشهر بعد التخرج من تدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'AR', N'التقديم لبرنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'AR', N'استبيان قبل بدء برنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'AR', N'استبيان بعد برنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'AR', N'استبيان عن الوضعية المهنية')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'AR', N'استبيان عن الاحتفاظ بالوظيفة')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'AR', N'استبيان عن تقدير الذات')
GO

--Begin AREG
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'AREG', N'طلب التقديم لتدريب الريادة')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'AREG', N'استبيان قبل بدء برنامج التدريب الريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'AREG', N'استبيان بعد انتهاء برنامج التدريب الريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'AREG', N'استبيان إنشاء مشروع ريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'AREG', N'استبيان متابعة حالة مشروع ريادي')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'AREG', N' طلب التقديم لتدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'AREG', N'استبيان قبل بدء برنامج "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'AREG', N'استبيان بعد انتهاء برنامج تدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'AREG', N'استبيان متابعة 4 أشهر بعد التخرج من تدريب "البحث عن عمل هو عمل"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'AREG', N'التقديم لبرنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'AREG', N'استبيان قبل بدء برنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'AREG', N'استبيان بعد برنامج التدريب والتوظيف')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'AREG', N'استبيان عن الوضعية المهنية')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'AREG', N'استبيان عن الاحتفاظ بالوظيفة')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'AREG', N'استبيان عن تقدير الذات')
GO

--Begin EN
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'EN', N'Entrepreneurship Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'EN', N'Entrepreneurship Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'EN', N'Entrepreneurship Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'EN', N'Business Creation Follow-Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'EN', N'Business Follow-Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'EN', N'Finding a Job is a Job Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'EN', N'Finding a Job is a Job Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'EN', N'Finding a Job is a Job Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'EN', N'Finding a Job is a Job Four-Month Follow Up Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'EN', N'Job Training and Placement Program Application')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'EN', N'Job Training and Placement Pre-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'EN', N'Job Training and Placement Post-Training Questionnaire')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'EN', N'Job Placement Status Check')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'EN', N'Job Retention Status Check')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'EN', N'Self-Esteem Questionnaire')
GO

--Begin FR
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship1', 'FR', N'Demande d''Inscription au Programme d''Entrepreneuriat')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship2', 'FR', N'Programme Entreprenariat: Questionnaire Pré-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship3', 'FR', N'Programme Entreprenariat: Questionnaire Post-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship4', 'FR', N'Questionnaire de Suivi de la Création d''Entreprise')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('Entrepreneurship5', 'FR', N'Questionnaire de Suivi du Projet d''Entreprise')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ1', 'FR', N'Demande d''Inscription au Programme "Trouver un Emploi est un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ2', 'FR', N'Questionnaire Pré-formation du Programme "Trouver un Emploi est Un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ3', 'FR', N'Questionnaire Post-Formation du Programme  "Trouver un Emploi est Un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('FJIJ4', 'FR', N'Questionnaire de Suivi 4 Mois Après la Formation "Trouver un Emploi est un Emploi"')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP1', 'FR', N'Demande d''Inscription à la Formation Insertion Professionnelle')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP2', 'FR', N'Formation - Insertion Professionnelle: Questionnaire Pré-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP3', 'FR', N'Formation - Insertion Professionnelle: Questionnaire Post-Formation')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP4', 'FR', N'Questionnaire sur la Situation Professionnelle')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('JTP5', 'FR', N'Questionnaire sur la Rétention du Poste')
INSERT INTO form.FormLabel (FormCode, Locale, FormText) VALUES ('SelfEsteem', 'FR', N'Questionnaire sur l''Estime de Soi')
GO
--End table form.FormLabel
