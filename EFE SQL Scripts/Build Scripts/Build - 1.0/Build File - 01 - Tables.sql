USE EFE
GO

--Begin table dbo.SendMail
DECLARE @TableName VARCHAR(250) = 'dbo.SendMail'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SendMail
	(
	SendMailID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	EmailAddress VARCHAR(320),
	EmailSubject NVARCHAR(500),
	EmailMessage NVARCHAR(MAX),
	CreateDateTime DATETIME,
	SendDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SendMailID'
EXEC utility.SetIndexClustered @TableName, 'IX_SendMail', 'SendDateTime DESC,SendMailID'
GO
--End table dbo.SendMail