-- File Name:	Build-1.0 File 01 - EFE.sql
-- Build Key:	Build-1.0 File 01 - EFE - 2016.01.21 21.15.05

USE EFE
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetEventNameByEventCode
--		survey.GetCorrectResponseCount
--
-- Procedures:
--		survey.GetSurveyQuestionResponsesBySurveyResponseID
--		survey.GetSurveysByProgramIDAndContactID
--
-- Schemas:
--		integration
--
-- Tables:
--		dbo.SendMail
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE EFE
GO

--Begin Schemas
EXEC utility.AddSchema integration
GO
--End Schemas

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table dbo.SendMail
DECLARE @TableName VARCHAR(250) = 'dbo.SendMail'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SendMail
	(
	SendMailID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	EmailAddress VARCHAR(320),
	EmailSubject NVARCHAR(500),
	EmailMessage NVARCHAR(MAX),
	CreateDateTime DATETIME,
	SendDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SendMailID'
EXEC utility.SetIndexClustered @TableName, 'IX_SendMail', 'SendDateTime DESC,SendMailID'
GO
--End table dbo.SendMail
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode IN ('listsurveyresponses', 'read')
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE ''
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function survey.GetCorrectResponseCount
EXEC utility.DropObject 'survey.GetCorrectResponseCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return a count of correct responses for a given programid, surveyid and contactid
-- ============================================================================================================

CREATE FUNCTION survey.GetCorrectResponseCount
(
@ProgramID INT,
@SurveyID INT,
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID)
	FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		JOIN survey.SurveySurveyQuestionResponse SSQR ON SSQR.SurveyQuestionID = SQRCK.SurveyQuestionID
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.Survey S ON S.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND SR.SurveyID = @SurveyID
			AND SR.ContactID = @ContactID
			AND SQRCK.ProgramID = 
				CASE
					WHEN ST.SurveyTypeCode = 'ApplicationTest'
					THEN 0
					ELSE @ProgramID 
				END 
			AND SQRCK.SurveyID = @SurveyID
			AND SSQR.SurveyQuestionID = SQRCK.SurveyQuestionID
			AND	SSQR.SurveyQuestionResponse = SQRCK.SurveyQuestionResponseChoiceValue
			AND SQRCK.IsForScore = 1
	
	RETURN ISNULL(@nReturn, 0)

END
GO
--End function survey.GetCorrectResponseCount

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure survey.GetSurveyQuestionResponsesBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to get survey responses from the database
-- =========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponsesBySurveyResponseID

@SurveyResponseID INT,
@ProgramID INT = 0

AS
BEGIN

	SELECT
		SSQR.SurveySurveyQuestionResponseID,
		SSQR.SurveyResponseID,
		SSQR.SurveyQuestionID,
		SSQR.SurveyQuestionResponse,
		SQT.SurveyQuestionResponseTypeCode,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'DatePicker'
			THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
			WHEN SQT.SurveyQuestionResponseTypeCode IN ('Select','SelectMultiple')
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'YesNo'
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionCode = SQT.SurveyQuestionResponseTypeCode AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE SSQR.SurveyQuestionResponse
		END AS SurveyQuestionResponseFormatted,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'SelectMultiple'
			THEN (SELECT SQRC.DisplayOrder FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE 0
		END AS SurveyQuestionResponseDisplayOrder,
	
		SSQ.DisplayOrder AS SurveyQuestionDisplayOrder,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionResponseChoiceKey SQRCK WHERE SQRCK.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRCK.ProgramID = @ProgramID AND SQRCK.SurveyID = SR.SurveyID)
			THEN ISNULL((SELECT 1 FROM survey.SurveyQuestionResponseChoiceKey SQRCK WHERE SQRCK.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRCK.ProgramID = @ProgramID AND SQRCK.SurveyID = SR.SurveyID AND SQRCK.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse), 0)
			ELSE -1
		END AS IsResponseCorrect

	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SSQR.SurveyQuestionID
			AND SSQ.SurveyID = SR.SurveyID
		JOIN survey.Survey S on S.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQR.SurveyResponseID = @SurveyResponseID
		JOIN dropdown.SurveyType ST on ST.SurveyTypeID = S.SurveyTypeID
	ORDER BY SurveyQuestionDisplayOrder, SurveyQuestionResponseDisplayOrder, SurveyQuestionResponse
	
END
GO
--End procedure survey.GetSurveyQuestionResponsesBySurveyResponseID

--Begin procedure survey.GetSurveysByProgramIDAndContactID
EXEC Utility.DropObject 'survey.GetSurveysByProgramIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.19
-- Description:	A stored procedure to survey data assoicated with a programid and a contactid
-- ==========================================================================================
CREATE PROCEDURE survey.GetSurveysByProgramIDAndContactID

@ProgramID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PS.SurveyID,
		survey.GetSurveyNameBySurveyID(PS.SurveyID) AS SurveyName,
		ISNULL(OASR.SurveyResponseID, 0) AS SurveyResponseID,
		ISNULL(OASR.SurveyResponseStatus, 'Not Started') AS SurveyResponseStatus,
		survey.GetCorrectResponseCount(@ProgramID, PS.SurveyID, @ContactID) AS CorrectResponseCount,
		(
		SELECT 
			COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID) 
		FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		WHERE SQRCK.ProgramID = 
			CASE
				WHEN ST.SurveyTypeCode = 'ApplicationTest'
				THEN 0
				ELSE @ProgramID 
			END 
			AND SQRCK.SurveyID = PS.SurveyID 
			AND SQRCK.IsForScore = 1
		) AS TotalGradedQuestionCount
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
	OUTER APPLY
		(
		SELECT 
			SR.SurveyResponseID,
	
			CASE
				WHEN SR.IsSubmitted = 1
				THEN 'Submitted'
				ELSE 'Not Submitted'
			END AS SurveyResponseStatus
	
		FROM survey.SurveyResponse SR 
		WHERE SR.SurveyID = PS.SurveyID 
			AND SR.ContactID = @ContactID
		) OASR
	WHERE PS.ProgramID = @ProgramID
		
END
GO
--End procedure survey.GetSurveysByProgramIDAndContactID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--Begin table dbo.EntityType
--EXEC utility.EntityTypeAddUpdate 'Class', 'Class'
--GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
--EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BudgetList', @ParentMenuItemCode=NULL, @AfterMenuItemCode='Dashboard', @NewMenuItemText='Budget', @NewMenuItemLink='/budget/list', @Icon='fa fa-fw fa-money', @PermissionableLineageList='Budget.List'
--GO

--EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
--GO
--End table dbo.MenuItem
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment Inventory', 0
GO
EXEC utility.SavePermissionableGroup 'OrganizationJob', 'Organization Jobs', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.0 File 01 - EFE - 2016.01.21 21.15.05')
GO
--End build tracking

