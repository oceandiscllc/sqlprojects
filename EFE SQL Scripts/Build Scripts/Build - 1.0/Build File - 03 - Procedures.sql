USE EFE
GO

--Begin procedure survey.GetSurveyQuestionResponsesBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to get survey responses from the database
-- =========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponsesBySurveyResponseID

@SurveyResponseID INT,
@ProgramID INT = 0

AS
BEGIN

	SELECT
		SSQR.SurveySurveyQuestionResponseID,
		SSQR.SurveyResponseID,
		SSQR.SurveyQuestionID,
		SSQR.SurveyQuestionResponse,
		SQT.SurveyQuestionResponseTypeCode,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'DatePicker'
			THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
			WHEN SQT.SurveyQuestionResponseTypeCode IN ('Select','SelectMultiple')
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'YesNo'
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionCode = SQT.SurveyQuestionResponseTypeCode AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE SSQR.SurveyQuestionResponse
		END AS SurveyQuestionResponseFormatted,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'SelectMultiple'
			THEN (SELECT SQRC.DisplayOrder FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE 0
		END AS SurveyQuestionResponseDisplayOrder,
	
		SSQ.DisplayOrder AS SurveyQuestionDisplayOrder,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionResponseChoiceKey SQRCK WHERE SQRCK.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRCK.ProgramID = @ProgramID AND SQRCK.SurveyID = SR.SurveyID)
			THEN ISNULL((SELECT 1 FROM survey.SurveyQuestionResponseChoiceKey SQRCK WHERE SQRCK.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRCK.ProgramID = @ProgramID AND SQRCK.SurveyID = SR.SurveyID AND SQRCK.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse), 0)
			ELSE -1
		END AS IsResponseCorrect

	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SSQR.SurveyQuestionID
			AND SSQ.SurveyID = SR.SurveyID
		JOIN survey.Survey S on S.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQR.SurveyResponseID = @SurveyResponseID
		JOIN dropdown.SurveyType ST on ST.SurveyTypeID = S.SurveyTypeID
	ORDER BY SurveyQuestionDisplayOrder, SurveyQuestionResponseDisplayOrder, SurveyQuestionResponse
	
END
GO
--End procedure survey.GetSurveyQuestionResponsesBySurveyResponseID

--Begin procedure survey.GetSurveysByProgramIDAndContactID
EXEC Utility.DropObject 'survey.GetSurveysByProgramIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.19
-- Description:	A stored procedure to survey data assoicated with a programid and a contactid
-- ==========================================================================================
CREATE PROCEDURE survey.GetSurveysByProgramIDAndContactID

@ProgramID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PS.SurveyID,
		survey.GetSurveyNameBySurveyID(PS.SurveyID) AS SurveyName,
		ISNULL(OASR.SurveyResponseID, 0) AS SurveyResponseID,
		ISNULL(OASR.SurveyResponseStatus, 'Not Started') AS SurveyResponseStatus,
		survey.GetCorrectResponseCount(@ProgramID, PS.SurveyID, @ContactID) AS CorrectResponseCount,
		(
		SELECT 
			COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID) 
		FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		WHERE SQRCK.ProgramID = 
			CASE
				WHEN ST.SurveyTypeCode = 'ApplicationTest'
				THEN 0
				ELSE @ProgramID 
			END 
			AND SQRCK.SurveyID = PS.SurveyID 
			AND SQRCK.IsForScore = 1
		) AS TotalGradedQuestionCount
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
	OUTER APPLY
		(
		SELECT 
			SR.SurveyResponseID,
	
			CASE
				WHEN SR.IsSubmitted = 1
				THEN 'Submitted'
				ELSE 'Not Submitted'
			END AS SurveyResponseStatus
	
		FROM survey.SurveyResponse SR 
		WHERE SR.SurveyID = PS.SurveyID 
			AND SR.ContactID = @ContactID
		) OASR
	WHERE PS.ProgramID = @ProgramID
		
END
GO
--End procedure survey.GetSurveysByProgramIDAndContactID
