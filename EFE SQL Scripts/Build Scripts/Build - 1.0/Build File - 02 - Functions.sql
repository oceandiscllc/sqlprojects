USE EFE
GO

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode IN ('listsurveyresponses', 'read')
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE ''
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function survey.GetCorrectResponseCount
EXEC utility.DropObject 'survey.GetCorrectResponseCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return a count of correct responses for a given programid, surveyid and contactid
-- ============================================================================================================

CREATE FUNCTION survey.GetCorrectResponseCount
(
@ProgramID INT,
@SurveyID INT,
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID)
	FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		JOIN survey.SurveySurveyQuestionResponse SSQR ON SSQR.SurveyQuestionID = SQRCK.SurveyQuestionID
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.Survey S ON S.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND SR.SurveyID = @SurveyID
			AND SR.ContactID = @ContactID
			AND SQRCK.ProgramID = 
				CASE
					WHEN ST.SurveyTypeCode = 'ApplicationTest'
					THEN 0
					ELSE @ProgramID 
				END 
			AND SQRCK.SurveyID = @SurveyID
			AND SSQR.SurveyQuestionID = SQRCK.SurveyQuestionID
			AND	SSQR.SurveyQuestionResponse = SQRCK.SurveyQuestionResponseChoiceValue
			AND SQRCK.IsForScore = 1
	
	RETURN ISNULL(@nReturn, 0)

END
GO
--End function survey.GetCorrectResponseCount
