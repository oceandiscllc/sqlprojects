USE EFE
GO

--Begin table dbo.EntityType
--EXEC utility.EntityTypeAddUpdate 'Class', 'Class'
--GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
--EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BudgetList', @ParentMenuItemCode=NULL, @AfterMenuItemCode='Dashboard', @NewMenuItemText='Budget', @NewMenuItemLink='/budget/list', @Icon='fa fa-fw fa-money', @PermissionableLineageList='Budget.List'
--GO

--EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
--GO
--End table dbo.MenuItem