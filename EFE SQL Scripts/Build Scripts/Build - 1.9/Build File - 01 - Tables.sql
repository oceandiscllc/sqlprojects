USE EFE
GO

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.SavePermissionable
EXEC utility.DropObject 'utility.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE utility.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionable

--Begin table form.Form
DECLARE @TableName VARCHAR(250) = 'form.Form'

EXEC utility.AddColumn @TableName, 'IsRestricted', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsRestricted', 'BIT', '0'
GO
--End table form.Form
