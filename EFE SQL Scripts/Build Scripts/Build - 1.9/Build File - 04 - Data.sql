USE EFE
GO

--Begin table form.Form
UPDATE form.Form 
SET IsRestricted = 1
WHERE FormCode NOT IN ('Entrepreneurship1','FJIJ1','JTP1')
GO
--End table form.Form
