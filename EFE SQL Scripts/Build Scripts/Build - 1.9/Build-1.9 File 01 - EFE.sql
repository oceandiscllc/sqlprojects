-- File Name:	Build-1.9 File 01 - EFE.sql
-- Build Key:	Build-1.9 File 01 - EFE - 2016.12.07 11.33.02

USE [EFE]
GO

-- ==============================================================================================================================
-- Procedures:
--		form.GetFormData
--		form.GetFormResponseData
--		utility.AddColumn
--		utility.SavePermissionable
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.SavePermissionable
EXEC utility.DropObject 'utility.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE utility.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionable

--Begin table form.Form
DECLARE @TableName VARCHAR(250) = 'form.Form'

EXEC utility.AddColumn @TableName, 'IsRestricted', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsRestricted', 'BIT', '0'
GO
--End table form.Form

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						4 AS GroupNumber, 
						T4.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T4 ON T4.TrainerID = C.TrainerID4
							AND C.TrainerID4 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						5 AS GroupNumber, 
						T5.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T5 ON T5.TrainerID = C.TrainerID5
							AND C.TrainerID5 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT TOP 1
		F.FormID,
		F.FormCode,
		F.IsRestricted,
		FC.ProgramID,
		ISNULL(FC.ContactID, 0) AS ContactID
	FROM form.Form F
		LEFT JOIN form.FormContact FC ON FC.FormID = F.FormID
			AND FC.ContactID = @ContactID
			AND FC.IsComplete = 0
	WHERE F.FormID = @FormID
	ORDER BY IsActive DESC, FormContactID DESC
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 

		CASE
			WHEN TQ.ObjectCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(TQ.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE TQ.Text
		END AS TEXT,

		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText,
		ISNULL((SELECT Q.MaximumResponseCount FROM form.Question Q WHERE Q.QuestionCode = TQ.ObjectCode), 0) AS MaximumResponseCount
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			WHEN RD.QuestionCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(RD.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'



							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure form.GetFormResponseData
EXEC Utility.DropObject 'form.GetFormResponseData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.03
-- Description:	A procedure to get form & form reqponse data for a form render
-- ===========================================================================
CREATE PROCEDURE form.GetFormResponseData

@FormContactID INT,
@Locale VARCHAR(4) = 'EN'


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), EntityID INT, GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50))

	SELECT TOP 1
		@cEntityTypeCode = FC.EntityTypeCode,
		@nEntityID = FC.EntityID
	FROM form.FormContact FC
	WHERE FC.FormContactID = @FormContactID

	SET @nEntityID = ISNULL(@nEntityID, 0)

	IF @cEntityTypeCode = 'ClassTrainer'
		BEGIN

		INSERT INTO @tEntityDataGroup
			(EntityTypeCode, EntityText, EntityID, GroupNumber, QuestionCode)
		SELECT
			EDG.EntityTypeCode, 
			D.TrainerName,
			D.TrainerID,
			EDG.GroupNumber,
			EDG.QuestionCode
		FROM form.EntityDataGroup EDG
			JOIN 
				(
				SELECT
					'ClassTrainer' AS EntityTypeCode,
					1 AS GroupNumber, 
					T1.TrainerName,
					T1.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
						AND C.TrainerID1 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					2 AS GroupNumber, 
					T2.TrainerName,
					T2.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
						AND C.TrainerID2 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					3 AS GroupNumber, 
					T3.TrainerName,
					T3.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
						AND C.TrainerID3 > 0
						AND C.ClassID = @nEntityID
				) D ON D.EntityTypeCode = EDG.EntityTypeCode
					AND D.GroupNumber = EDG.GroupNumber

		END
	--ENDIF

	SELECT
		Q.QuestionCode AS ObjectCode,
		QL.QuestionText AS Text,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN 
			(
			SELECT DISTINCT
				QRC.QuestionCode,
				QRC.DisplayOrder
			FROM form.QuestionResponseChoice QRC
			WHERE QRC.FormContactID = @FormContactID
			) D ON D.QuestionCode = QL.QuestionCode
				AND QL.Locale = @Locale
	ORDER BY D.DisplayOrder

	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
			JOIN form.QuestionResponseChoice QRC ON QRC.QuestionCode = QT.QuestionCode
				AND QRC.FormContactID = @FormContactID
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionCode,

		CASE
			WHEN CHARINDEX('Other', QRL.QuestionResponseText) > 0
			THEN QRL.QuestionResponseText + ' ' + QRC.QuestionResponseText
			ELSE QRL.QuestionResponseText
		END AS Text,

		QRL.DisplayOrder
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.QuestionResponseChoice QRC ON QRC.QuestionResponseCode = QR.QuestionResponseCode
			AND QRC.FormContactID = @FormContactID
			AND QRL.Locale = @Locale

	UNION

	SELECT
		QRC.QuestionCode,
		QRC.QuestionResponseText AS Text,
		1 AS DisplayOrder
	FROM form.QuestionResponseChoice QRC
	WHERE QRC.QuestionResponseCode IS NULL
		AND QRC.FormContactID = @FormContactID

	ORDER BY QuestionCode, DisplayOrder
	
END
GO
--End procedure form.GetFormResponseData
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--Begin table form.Form
UPDATE form.Form 
SET IsRestricted = 1
WHERE FormCode NOT IN ('Entrepreneurship1','FJIJ1','JTP1')
GO
--End table form.Form

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the email template list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Add / edit a form object translation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateTranslation', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Form.AddUpdateTranslation', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='View the list of objects with translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListTranslation', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Form.ListTranslation', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='main', @DESCRIPTION='main.SSRSLink', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SSRSLink', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='main.SSRSLink', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / Update Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.addUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='list', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.list', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add or Update System User', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List System Users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View System User', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='serversetup', @DESCRIPTION='Edit the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addupdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='serversetup.addupdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='View the server setup keys', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='Add / edit a budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Budget.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View the budget list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Budget.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='Add / edit an expenditure', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='View the expenditure list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='View an expenditure', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend payment report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with programs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ApplicantManagement', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.ApplicantManagement', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Enable the Associate All button', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.AssociateAll', @PERMISSIONCODE='AssociateAll'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with jobs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.OrganizationJob', @PERMISSIONCODE='OrganizationJob'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with mentors', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.OrganizationMentor', @PERMISSIONCODE='OrganizationMentor'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with surveys', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.Survey', @PERMISSIONCODE='Survey'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.Beneficiary', @PERMISSIONCODE='Beneficiary'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts not of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonBeneficiary', @PERMISSIONCODE='NonBeneficiary'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the event log', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Edit a contact', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View the email template list', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='Edit an organization', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='View the list of organizations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='View an organization', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='Add / Edit an organization job', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='View an organization job', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='Add / Edit an organization mentor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='View an organization mentor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='View the list of organization jobs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='View the list of organization mentors', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='Add / Edit a Program', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='View the list of programs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='View a Program', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / Edit a Course', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / Edit a Class', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View the list of classes', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View a Class', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='Edit an award', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='Add / edit an award Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.AddUpdate.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View the list of awards', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View an award Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.View.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='Add / edit a proposal', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='Add / edit a proposal Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.AddUpdate.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View the list of proposals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View a proposal Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.View.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='Edit a subaward', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='Add / edit a subaward Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.AddUpdate.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View the list of subawards', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View a subaward Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.View.Budget', @PERMISSIONCODE='Budget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View an award', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View a proposal', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View a subaward', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Create responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerForm', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.AdministerForm', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Save responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveForm', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.SaveForm', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='View responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewResponse', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.ViewResponse', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Add/Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='AddUpdate Survey Thresholds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurveyThreshold', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateSurveyThreshold', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Administer Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AdministerSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey List Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey Save Question ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Save Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Save Survey Thresholds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurveyThreshold', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveSurveyThreshold', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey View Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Add Update Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Add Update Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Administer Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Clone Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CloneSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.CloneSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Export Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management List Surveys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Save Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.SaveSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Save Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.SaveSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management View Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management View Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View a budget', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Budget.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View the survey question bank', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListQuestions', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Add / edit survey questions', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View surveys', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListSurveys', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='District', @DESCRIPTION='View the list of districts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='District.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='District', @DESCRIPTION='View a district', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='District.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Front', @DESCRIPTION='View a front', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='Front.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Governorate', @DESCRIPTION='View the list of governorates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='Governorate.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Governorate', @DESCRIPTION='View a governorate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='Governorate.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubDistrict', @DESCRIPTION='View the list of subdistricts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='SubDistrict.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubDistrict', @DESCRIPTION='View a sub district', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='SubDistrict.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Front', @DESCRIPTION='View the list of fronts', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Territory', @PERMISSIONABLELINEAGE='Front.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the workflow list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View the tree of workplan items', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Tree', @PERMISSIONABLEGROUPCODE='Workplan', @PERMISSIONABLELINEAGE='Workplan.Tree', @PERMISSIONCODE=NULL
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.9 File 01 - EFE - 2016.12.07 11.33.02')
GO
--End build tracking

