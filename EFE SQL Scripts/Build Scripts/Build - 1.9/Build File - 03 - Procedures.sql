USE EFE
GO

--Begin procedure form.GetFormData
EXEC Utility.DropObject 'form.GetFormData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.04.15
-- Description:	A procedure to get form data for a form render
-- ===========================================================
CREATE PROCEDURE form.GetFormData

@FormID INT,
@Locale VARCHAR(4),
@AffiliateID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @cFirstQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cFirstQuestionCodeRelative VARCHAR(50)
	DECLARE @cLastQuestionCodeAbsolute VARCHAR(50)
	DECLARE @cLastQuestionCodeRelative VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50), IsLastQuestion BIT)
	DECLARE @tQuestion TABLE (ObjectType VARCHAR(50), ObjectCode VARCHAR(50), QuestionResponseTypeCode VARCHAR(50), Text NVARCHAR(MAX), DisplayOrder INT, IsRequired BIT, NextQuestionCode VARCHAR(50), HasText BIT)

	IF @ContactID > 0
		BEGIN

		SELECT TOP 1
			@cEntityTypeCode = FC.EntityTypeCode,
			@nEntityID = FC.EntityID
		FROM form.FormContact FC
		WHERE FC.ContactID = @ContactID
			AND FC.IsActive = 1
		ORDER BY FC.FormContactID DESC

		SET @nEntityID = ISNULL(@nEntityID, 0)

		IF @cEntityTypeCode = 'ClassTrainer'
			BEGIN

			INSERT INTO @tEntityDataGroup
				(EntityTypeCode, EntityText, GroupNumber, QuestionCode, IsLastQuestion)
			SELECT
				EDG.EntityTypeCode, 
				D.TrainerName,
				EDG.GroupNumber,
				EDG.QuestionCode,
				EDG.IsLastQuestion
			FROM form.EntityDataGroup EDG
				JOIN 
					(
					SELECT
						'ClassTrainer' AS EntityTypeCode,
						1 AS GroupNumber, 
						T1.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
							AND C.TrainerID1 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						2 AS GroupNumber, 
						T2.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
							AND C.TrainerID2 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						3 AS GroupNumber, 
						T3.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
							AND C.TrainerID3 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						4 AS GroupNumber, 
						T4.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T4 ON T4.TrainerID = C.TrainerID4
							AND C.TrainerID4 > 0
							AND C.ClassID = @nEntityID

					UNION

					SELECT 
						'ClassTrainer' AS EntityTypeCode,
						5 AS GroupNumber, 
						T5.TrainerName
					FROM program.Class C
						JOIN dropdown.Trainer T5 ON T5.TrainerID = C.TrainerID5
							AND C.TrainerID5 > 0
							AND C.ClassID = @nEntityID
					) D ON D.EntityTypeCode = EDG.EntityTypeCode
						AND D.GroupNumber = EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsFirstQuestion = 1
				ORDER BY EDG.GroupNumber

				SELECT TOP 1 @cFirstQuestionCodeRelative = FQRM.QuestionCode
				FROM form.FormQuestionResponseMap FQRM
					JOIN form.Form F ON F.FormCode = FQRM.FormCode
						AND F.FormID = @FormID
						AND FQRM.NextQuestionCode = @cFirstQuestionCodeAbsolute

				SELECT TOP 1 @cLastQuestionCodeAbsolute = EDG.QuestionCode
				FROM form.EntityDataGroup EDG
				WHERE EDG.EntityTypeCode = @cEntityTypeCode
					AND EDG.IsLastQuestion = 1
				ORDER BY EDG.GroupNumber DESC

				SELECT TOP 1 @cLastQuestionCodeRelative = TEDG.QuestionCode
				FROM @tEntityDataGroup TEDG
				WHERE TEDG.IsLastQuestion = 1
				ORDER BY TEDG.GroupNumber DESC
					
			END
		--ENDIF

		END
	--ENDIF

	INSERT INTO @tQuestion
		(ObjectType, ObjectCode, QuestionResponseTypeCode, Text, DisplayOrder, IsRequired, NextQuestionCode, HasText)
	SELECT
		'Question' AS ObjectType,
		Q.QuestionCode AS ObjectCode,
		Q.QuestionResponseTypeCode,
		QL.QuestionText AS Text,
		FQ.DisplayOrder,
		FQ.IsRequired,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = Q.QuestionCode AND FQRM.IsBranchedQuestion = 0) AS NextQuestionCode,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = Q.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QL.Locale = @Locale
			AND
				(
				Q.EntityTypeCode IS NULL
					OR EXISTS

						(
						SELECT 1
						FROM @tEntityDataGroup TEDG
						WHERE TEDG.QuestionCode = Q.QuestionCode
						)
				)
	
	UNION
	
	SELECT
		'Text' AS ObjectType,
		T.TextCode AS ObjectCode,
		NULL,
		TL.TextText AS Text,
		FQ.DisplayOrder,
		0,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionCode = T.TextCode) AS NextQuestionCode,
		0
	FROM form.TextLabel TL
		JOIN form.Text T ON T.TextCode = TL.TextCode
		JOIN form.FormQuestion FQ ON FQ.TextCode = T.TextCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND TL.Locale = @Locale

	IF EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG) AND @cLastQuestionCodeAbsolute <> @cLastQuestionCodeRelative
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cLastQuestionCodeRelative		
		
		END
	ELSE IF @cEntityTypeCode IS NOT NULL AND NOT EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG)
		BEGIN
		
		UPDATE TQ
		SET TQ.NextQuestionCode = 
			(
			SELECT FQRM.NextQuestionCode
			FROM form.FormQuestionResponseMap FQRM
				JOIN form.Form F ON F.FormCode = FQRM.FormCode
					AND F.FormID = @FormID
					AND FQRM.QuestionCode = @cLastQuestionCodeAbsolute
			)
		FROM @tQuestion TQ
		WHERE TQ.ObjectType = 'Question'
			AND TQ.ObjectCode = @cFirstQuestionCodeRelative

		END
	--ENDIF	
	
	SELECT TOP 1
		F.FormID,
		F.FormCode,
		F.IsRestricted,
		FC.ProgramID,
		ISNULL(FC.ContactID, 0) AS ContactID
	FROM form.Form F
		LEFT JOIN form.FormContact FC ON FC.FormID = F.FormID
			AND FC.ContactID = @ContactID
			AND FC.IsComplete = 0
	WHERE F.FormID = @FormID
	ORDER BY IsActive DESC, FormContactID DESC
	
	SELECT
		TQ.ObjectType, 
		TQ.ObjectCode, 
		TQ.QuestionResponseTypeCode, 

		CASE
			WHEN TQ.ObjectCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(TQ.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(TQ.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE TQ.Text
		END AS TEXT,

		TQ.DisplayOrder, 
		TQ.IsRequired, 
		TQ.NextQuestionCode, 
		TQ.HasText,
		ISNULL((SELECT Q.MaximumResponseCount FROM form.Question Q WHERE Q.QuestionCode = TQ.ObjectCode), 0) AS MaximumResponseCount
	FROM @tQuestion TQ
	ORDER BY TQ.DisplayOrder, TQ.ObjectCode
	
	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
				AND EXISTS
					(
					SELECT 1
					FROM form.FormQuestion FQ
						JOIN form.Form F ON F.FormCode = FQ.FormCode
							AND F.FormID = @FormID
							AND FQ.QuestionCode = QT.QuestionCode
						JOIN form.Question Q ON Q.QuestionCode = QT.QuestionCode
							AND
								(
								Q.EntityTypeCode IS NULL
									OR EXISTS
										(
										SELECT 1
										FROM @tEntityDataGroup TEDG
										WHERE TEDG.QuestionCode = Q.QuestionCode
										)
								)
					)
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			WHEN RD.QuestionCode = 'Q127'
			THEN
				CASE 
					WHEN EXISTS (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC)
					THEN REPLACE(RD.Text, '[[SameEmployer]]', (SELECT TOP 1 KPIJP.Employer FROM form.KPIJobPlacement KPIJP WHERE KPIJP.ContactID = @ContactID ORDER BY KPIJP.FormSubmitDate DESC))
					ELSE
						CASE
							WHEN @Locale = 'AR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'AREG'



							THEN REPLACE(RD.Text, '[[SameEmployer]]', N'??? ???? ?????')
							WHEN @Locale = 'EN'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'the same employer')
							WHEN @Locale = 'FR'
							THEN REPLACE(RD.Text, '[[SameEmployer]]', 'le m�me employeur')
						END
				END
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionResponseCode,
		QR.QuestionCode,
		QRL.QuestionResponseText AS Text,
		(SELECT FQRM.NextQuestionCode FROM form.FormQuestionResponseMap FQRM WHERE FQRM.FormCode = FQ.FormCode AND FQRM.QuestionResponseCode = QR.QuestionResponseCode) AS NextQuestionCode
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.FormQuestion FQ ON FQ.QuestionCode = QR.QuestionCode
		JOIN form.Form F ON F.FormCode = FQ.FormCode
			AND F.FormID = @FormID
			AND QRL.Locale = @Locale
			AND 
				(
				QRL.AffiliateCountryCode IS NULL
					OR QRL.AffiliateCountryCode = (SELECT C.ISOCountryCode2 FROM dropdown.Country C JOIN dropdown.Affiliate A ON A.AffiliateName = C.CountryName AND A.AffiliateID = @AffiliateID)
				)
	ORDER BY QR.QuestionCode, QRL.DisplayOrder
	
END
GO
--End procedure form.GetFormData

--Begin procedure form.GetFormResponseData
EXEC Utility.DropObject 'form.GetFormResponseData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.03
-- Description:	A procedure to get form & form reqponse data for a form render
-- ===========================================================================
CREATE PROCEDURE form.GetFormResponseData

@FormContactID INT,
@Locale VARCHAR(4) = 'EN'


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @tEntityDataGroup TABLE (EntityDataGroupID INT NOT NULL IDENTITY(1,1), EntityTypeCode VARCHAR(50), EntityID INT, GroupNumber INT, EntityText NVARCHAR(MAX), QuestionCode VARCHAR(50))

	SELECT TOP 1
		@cEntityTypeCode = FC.EntityTypeCode,
		@nEntityID = FC.EntityID
	FROM form.FormContact FC
	WHERE FC.FormContactID = @FormContactID

	SET @nEntityID = ISNULL(@nEntityID, 0)

	IF @cEntityTypeCode = 'ClassTrainer'
		BEGIN

		INSERT INTO @tEntityDataGroup
			(EntityTypeCode, EntityText, EntityID, GroupNumber, QuestionCode)
		SELECT
			EDG.EntityTypeCode, 
			D.TrainerName,
			D.TrainerID,
			EDG.GroupNumber,
			EDG.QuestionCode
		FROM form.EntityDataGroup EDG
			JOIN 
				(
				SELECT
					'ClassTrainer' AS EntityTypeCode,
					1 AS GroupNumber, 
					T1.TrainerName,
					T1.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T1 ON T1.TrainerID = C.TrainerID1
						AND C.TrainerID1 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					2 AS GroupNumber, 
					T2.TrainerName,
					T2.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T2 ON T2.TrainerID = C.TrainerID2
						AND C.TrainerID2 > 0
						AND C.ClassID = @nEntityID

				UNION

				SELECT 
					'ClassTrainer' AS EntityTypeCode,
					3 AS GroupNumber, 
					T3.TrainerName,
					T3.TrainerID
				FROM program.Class C
					JOIN dropdown.Trainer T3 ON T3.TrainerID = C.TrainerID3
						AND C.TrainerID3 > 0
						AND C.ClassID = @nEntityID
				) D ON D.EntityTypeCode = EDG.EntityTypeCode
					AND D.GroupNumber = EDG.GroupNumber

		END
	--ENDIF

	SELECT
		Q.QuestionCode AS ObjectCode,
		QL.QuestionText AS Text,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM form.QuestionText QT WHERE QT.QuestionCode = QL.QuestionCode)
			THEN 1
			ELSE 0
		END AS HasText

	FROM form.QuestionLabel QL
		JOIN form.Question Q ON Q.QuestionCode = QL.QuestionCode
		JOIN 
			(
			SELECT DISTINCT
				QRC.QuestionCode,
				QRC.DisplayOrder
			FROM form.QuestionResponseChoice QRC
			WHERE QRC.FormContactID = @FormContactID
			) D ON D.QuestionCode = QL.QuestionCode
				AND QL.Locale = @Locale
	ORDER BY D.DisplayOrder

	;
	WITH RD AS
		(
		SELECT
			QT.QuestionCode,
			QT.TextCode,
			QT.DisplayOrder,
			QT.IsAfter,
			TL.TextText AS Text
		FROM form.QuestionText QT
			JOIN form.TextLabel TL ON TL.TextCode = QT.TextCode
				AND TL.Locale = @Locale
			JOIN form.QuestionResponseChoice QRC ON QRC.QuestionCode = QT.QuestionCode
				AND QRC.FormContactID = @FormContactID
		)

	SELECT
		RD.QuestionCode,
		RD.TextCode,
		RD.DisplayOrder,
		RD.IsAfter,

		CASE
			WHEN EXISTS (SELECT 1 FROM @tEntityDataGroup TEDG WHERE TEDG.QuestionCode = RD.QuestionCode)
			THEN REPLACE(RD.Text, '[[' + OATEDG.EntityTypeCode + ']]', OATEDG.EntityText)
			ELSE RD.Text
		END AS Text

	FROM RD
		OUTER APPLY
			(
			SELECT 
				TEDG.EntityTypeCode,
				TEDG.EntityText,
				TEDG.QuestionCode
			FROM @tEntityDataGroup TEDG
			WHERE TEDG.QuestionCode = RD.QuestionCode
			) OATEDG
	ORDER BY RD.QuestionCode, RD.IsAfter, RD.DisplayOrder

	SELECT
		QR.QuestionCode,

		CASE
			WHEN CHARINDEX('Other', QRL.QuestionResponseText) > 0
			THEN QRL.QuestionResponseText + ' ' + QRC.QuestionResponseText
			ELSE QRL.QuestionResponseText
		END AS Text,

		QRL.DisplayOrder
	FROM form.QuestionResponseLabel QRL
		JOIN form.QuestionResponse QR ON QR.QuestionResponseCode = QRL.QuestionResponseCode
		JOIN form.QuestionResponseChoice QRC ON QRC.QuestionResponseCode = QR.QuestionResponseCode
			AND QRC.FormContactID = @FormContactID
			AND QRL.Locale = @Locale

	UNION

	SELECT
		QRC.QuestionCode,
		QRC.QuestionResponseText AS Text,
		1 AS DisplayOrder
	FROM form.QuestionResponseChoice QRC
	WHERE QRC.QuestionResponseCode IS NULL
		AND QRC.FormContactID = @FormContactID

	ORDER BY QuestionCode, DisplayOrder
	
END
GO
--End procedure form.GetFormResponseData