USE EFE
GO

--Begin procedure kpi.GetKPIData
EXEC Utility.DropObject 'kpi.GetKPIData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A stored procedure to return KPI data
-- ==================================================
CREATE PROCEDURE kpi.GetKPIData

AS
BEGIN
	SET NOCOUNT ON;

	-- AcceptedPercentage
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetAcceptedPercentage(0, 0) AS Percentage1,
		100 - kpi.GetAcceptedPercentage(0, 0) AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetAcceptedPercentage(A.AffiliateID, 0) AS Percentage1,
		100 - kpi.GetAcceptedPercentage(A.AffiliateID, 0) AS Percentage2
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--RegisteredCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetRegisteredCount(0, 0) AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetRegisteredCount(A.AffiliateID, 0) AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--EnrolledCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetEnrolledCount(0, 0) AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetEnrolledCount(A.AffiliateID, 0) AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatedCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetGraduatedCount(0, 0, '') AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatePercentageByGender
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,

		CASE
			WHEN kpi.GetGraduatedCount(0, 0, '') > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(0, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetGraduatedCount(0, 0, '') > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(0, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,

		CASE
			WHEN kpi.GetGraduatedCount(A.AffiliateID, 0, '') > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetGraduatedCount(A.AffiliateID, 0, '') > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatedPercentage
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,

		CASE
			WHEN kpi.GetRegisteredCount(0, 0) > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(0, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetRegisteredCount(0, 0) > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(0, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,

		CASE
			WHEN kpi.GetRegisteredCount(A.AffiliateID, 0) > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(A.AffiliateID, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetRegisteredCount(A.AffiliateID, 0) > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(A.AffiliateID, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

END
GO
--End procedure kpi.GetKPIData

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE permissionable.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'utility.SavePermissionableGroup'
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE permissionable.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure person.GetAffiliatesByPersonID
EXEC Utility.DropObject 'person.GetAffiliatesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.22
-- Description:	A stored procedure to return data from the Person table based on a PersonID
-- ========================================================================================
CREATE PROCEDURE person.GetAffiliatesByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		A.AffiliateID,
		A.AffiliateName,
		A.AffiliateCountryCode,
		
		CASE
			WHEN P.DefaultAffiliateID = A.AffiliateID
			THEN 1
			ELSE 0
		END AS IsDefaultAffiliate

	FROM person.PersonAffiliate PA
		JOIN person.Person P ON P.PersonID = PA.PersonID
		JOIN dropdown.Affiliate A ON A.AffiliateID = PA.AffiliateID
			AND PA.PersonID = @PersonID
	ORDER BY A.AffiliateName

END
GO
--End procedure person.GetAffiliatesByPersonID

--Begin procedure program.GetClassByClassID
EXEC Utility.DropObject 'program.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Class table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added studentStatus data
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added status date and formatted status date
--
-- Author:			Todd Pires
-- Create date:	2016.04.07
-- Description:	Implemented the TrainerID columns
--
-- Author:			Todd Pires
-- Create date:	2016.08.09
-- Description:	Implemented more TrainerID columns
--
-- Author:			Kevin Ross
-- Create date:	2016.08.16
-- Description:	Implemented the StatusReason columns
-- ===============================================================================================
CREATE PROCEDURE program.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(CL.AffiliateID) AS AffiliateName,
		CL.ClassID,
		CL.ClassName,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location,
		CL.Seats,
		CL.Sessions,
		CL.StartDate,
		CL.EndDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CO.CourseID,
		CO.CourseName,
		T1.TrainerID AS TrainerID1,
		T1.TrainerName AS TrainerName1,
		T2.TrainerID AS TrainerID2,
		T2.TrainerName AS TrainerName2,
		T3.TrainerID AS TrainerID3,
		T3.TrainerName AS TrainerName3,
		T4.TrainerID AS TrainerID4,
		T4.TrainerName AS TrainerName4,
		T5.TrainerID AS TrainerID5,
		T5.TrainerName AS TrainerName5
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN dropdown.Trainer T1 ON T1.TrainerID = CL.TrainerID1
		JOIN dropdown.Trainer T2 ON T2.TrainerID = CL.TrainerID2
		JOIN dropdown.Trainer T3 ON T3.TrainerID = CL.TrainerID3
		JOIN dropdown.Trainer T4 ON T4.TrainerID = CL.TrainerID4
		JOIN dropdown.Trainer T5 ON T5.TrainerID = CL.TrainerID5
	WHERE CL.ClassID = @ClassID
	
	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		CC.StudentStatusID,
		SS.StudentStatusName,
		CC.StatusDate,
		CC.StatusReason,
		dbo.FormatDate(CC.StatusDate) AS StatusDateFormatted
	FROM program.ClassContact CC
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID= CC.StudentStatusID
			AND CL.ClassID = @ClassID

	SELECT
		P.ProgramName,
		P.ProgramID,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramContact PC ON PC.ContactID = CC.ContactID AND PC.ProgramID = P.ProgramID AND CC.ClassID = @ClassID)
			THEN 1
			ELSE 0
		END AS HasClassContact
	FROM program.ProgramClass PC
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
			AND CL.ClassID = @ClassID

END
GO
--End procedure program.GetClassByClassID

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2016.09.21
-- Description:	converted from 1 to many programs to many to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		FL.FormText AS FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode 
			AND FL.Locale = @Locale
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramContact PC ON PC.ContactID = CC.ContactID AND PC.ProgramID = @ProgramID AND CC.ClassID = CL.ClassID)
			THEN 1
			ELSE 0
		END AS HasClassContact
	FROM program.ProgramClass PC
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		CAST(CCC.CountryCallingCode AS varchar)+', '+CAST(C.CellPhoneNumber AS varchar) AS CellPhoneNumberWithCountryCode,
		C.ContactID,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramClass PC ON PC.ClassID = CC.ClassID AND PC.ProgramID = @ProgramID AND CC.ContactID = C.ContactID)
			THEN 1
			ELSE 0
		END AS IsClassContact
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C.CellPhoneNumberCountryCallingCodeID
			AND PC.ProgramID = @ProgramID

	--Contact counts
	SELECT
		CASE
			WHEN ISNULL(APS.ApplicantStatusName,'') = ''
			THEN 'No Status'
			ELSE APS.ApplicantStatusName
		END AS ApplicantStatusName,
		COUNT(C.ContactID) AS ContactCount
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID
	GROUP BY APS.ApplicantStatusName

	--ProgramTypeForms
	SELECT
		F.FormID,
		FL.FormText AS FormName
	FROM form.Form F
		JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = @Locale
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID
