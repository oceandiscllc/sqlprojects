USE EFE
GO

--Begin function kpi.GetAcceptedPercentage
EXEC utility.DropObject 'kpi.GetAcceptedPercentage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get the percentage of accepted applicants
-- ====================================================================

CREATE FUNCTION kpi.GetAcceptedPercentage
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nAcceptedCount INT = 0
	DECLARE @nAcceptedPercentage NUMERIC(18,2) = 0
	DECLARE @nTotalCount INT = 0

	SELECT @nAcceptedCount = COUNT(PC.ProgramContactID)
	FROM program.ProgramContact PC 
		JOIN dropdown.ApplicantStatus A ON A.ApplicantStatusID = PC.ApplicantStatusID 
			AND A.ApplicantStatusName = 'Registered'
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND (@AffiliateID = 0 OR P.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR P.ProgramID = @ProgramID)

	SELECT @nTotalCount = COUNT(PC.ProgramContactID)
	FROM program.ProgramContact PC 
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND (@AffiliateID = 0 OR P.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR P.ProgramID = @ProgramID)

	IF @nTotalCount > 0
		SET @nAcceptedPercentage = CAST(@nAcceptedCount AS FLOAT) / CAST(@nTotalCount AS FLOAT) * 100
	ELSE
		SET @nAcceptedPercentage = @nTotalCount
	--ENDIF

	RETURN CAST(@nAcceptedPercentage AS NUMERIC(18,2))

END
GO
--End function kpi.GetAcceptedPercentage

--Begin function kpi.GetEnrolledCount
EXEC Utility.DropObject 'kpi.GetEnrolledCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of enrolled students
-- ===========================================================

CREATE FUNCTION kpi.GetEnrolledCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nEnrolledCount INT = 0

	SELECT @nEnrolledCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName NOT IN ('No Show', 'Withdrawn')
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL on PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nEnrolledCount, 0)

END
GO
--End function kpi.GetEnrolledCount

--Begin function kpi.GetGraduatedCount
EXEC Utility.DropObject 'kpi.GetGraduatedCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of graduated students
-- ============================================================

CREATE FUNCTION kpi.GetGraduatedCount
(
@AffiliateID INT,
@ProgramID INT,
@Gender NVARCHAR(20)
)

RETURNS INT

AS
BEGIN

	DECLARE @nGraduatedCount INT = 0

	SELECT @nGraduatedCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName = 'Graduate'
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR CL.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND (@Gender = '' OR CO.Gender = @Gender)

	RETURN ISNULL(@nGraduatedCount, 0)

END
GO
--End function kpi.GetGraduatedCount