-- File Name:	Build - 2.0 - EFE.sql
-- Build Key:	Build - 2.0 - 2017.09.03 19.43.58

USE EFE
GO

-- ==============================================================================================================================
-- Functions:
--		kpi.GetAcceptedPercentage
--		kpi.GetEnrolledCount
--		kpi.GetGraduatedCount
--
-- Procedures:
--		kpi.GetKPIData
--		permissionable.SavePermissionable
--		permissionable.SavePermissionableGroup
--		person.GetAffiliatesByPersonID
--		program.GetClassByClassID
--		program.GetProgramByProgramID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--Begin function kpi.GetAcceptedPercentage
EXEC utility.DropObject 'kpi.GetAcceptedPercentage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get the percentage of accepted applicants
-- ====================================================================

CREATE FUNCTION kpi.GetAcceptedPercentage
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nAcceptedCount INT = 0
	DECLARE @nAcceptedPercentage NUMERIC(18,2) = 0
	DECLARE @nTotalCount INT = 0

	SELECT @nAcceptedCount = COUNT(PC.ProgramContactID)
	FROM program.ProgramContact PC 
		JOIN dropdown.ApplicantStatus A ON A.ApplicantStatusID = PC.ApplicantStatusID 
			AND A.ApplicantStatusName = 'Registered'
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND (@AffiliateID = 0 OR P.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR P.ProgramID = @ProgramID)

	SELECT @nTotalCount = COUNT(PC.ProgramContactID)
	FROM program.ProgramContact PC 
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND (@AffiliateID = 0 OR P.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR P.ProgramID = @ProgramID)

	IF @nTotalCount > 0
		SET @nAcceptedPercentage = CAST(@nAcceptedCount AS FLOAT) / CAST(@nTotalCount AS FLOAT) * 100
	ELSE
		SET @nAcceptedPercentage = @nTotalCount
	--ENDIF

	RETURN CAST(@nAcceptedPercentage AS NUMERIC(18,2))

END
GO
--End function kpi.GetAcceptedPercentage

--Begin function kpi.GetEnrolledCount
EXEC Utility.DropObject 'kpi.GetEnrolledCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of enrolled students
-- ===========================================================

CREATE FUNCTION kpi.GetEnrolledCount
(
@AffiliateID INT,
@ProgramID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nEnrolledCount INT = 0

	SELECT @nEnrolledCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName NOT IN ('No Show', 'Withdrawn')
		JOIN program.Class C ON C.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL on PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR C.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)

	RETURN ISNULL(@nEnrolledCount, 0)

END
GO
--End function kpi.GetEnrolledCount

--Begin function kpi.GetGraduatedCount
EXEC Utility.DropObject 'kpi.GetGraduatedCount'
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A function to get a count of graduated students
-- ============================================================

CREATE FUNCTION kpi.GetGraduatedCount
(
@AffiliateID INT,
@ProgramID INT,
@Gender NVARCHAR(20)
)

RETURNS INT

AS
BEGIN

	DECLARE @nGraduatedCount INT = 0

	SELECT @nGraduatedCount = COUNT(CC.ClassStudentID)
	FROM program.ClassContact CC
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID = CC.StudentStatusID
			AND SS.StudentStatusName = 'Graduate'
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN program.ProgramClass PCL ON PCL.ClassID = CC.ClassID
			AND (@AffiliateID = 0 OR CL.AffiliateID = @AffiliateID)
			AND (@ProgramID = 0 OR PCL.ProgramID = @ProgramID)
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND (@Gender = '' OR CO.Gender = @Gender)

	RETURN ISNULL(@nGraduatedCount, 0)

END
GO
--End function kpi.GetGraduatedCount
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure kpi.GetKPIData
EXEC Utility.DropObject 'kpi.GetKPIData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2016.01.17
-- Description:	A stored procedure to return KPI data
-- ==================================================
CREATE PROCEDURE kpi.GetKPIData

AS
BEGIN
	SET NOCOUNT ON;

	-- AcceptedPercentage
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetAcceptedPercentage(0, 0) AS Percentage1,
		100 - kpi.GetAcceptedPercentage(0, 0) AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetAcceptedPercentage(A.AffiliateID, 0) AS Percentage1,
		100 - kpi.GetAcceptedPercentage(A.AffiliateID, 0) AS Percentage2
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--RegisteredCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetRegisteredCount(0, 0) AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetRegisteredCount(A.AffiliateID, 0) AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--EnrolledCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetEnrolledCount(0, 0) AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetEnrolledCount(A.AffiliateID, 0) AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatedCount
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,
		kpi.GetGraduatedCount(0, 0, '') AS ItemCount

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,
		kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS ItemCount
	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatePercentageByGender
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,

		CASE
			WHEN kpi.GetGraduatedCount(0, 0, '') > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(0, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetGraduatedCount(0, 0, '') > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(0, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,

		CASE
			WHEN kpi.GetGraduatedCount(A.AffiliateID, 0, '') > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetGraduatedCount(A.AffiliateID, 0, '') > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, 'Male') AS NUMERIC(18,2)) / CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

	--GraduatedPercentage
	SELECT
		-1 AS DisplayOrder,
		'Overall' AS ItemTypeCode,

		CASE
			WHEN kpi.GetRegisteredCount(0, 0) > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(0, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetRegisteredCount(0, 0) > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(0, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(0, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	UNION

	SELECT
		A.DisplayOrder,
		A.AffiliateName AS ItemTypeCode,

		CASE
			WHEN kpi.GetRegisteredCount(A.AffiliateID, 0) > 0
			THEN CAST((CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(A.AffiliateID, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage1,

		CASE
			WHEN kpi.GetRegisteredCount(A.AffiliateID, 0) > 0
			THEN CAST(100 - (CAST(kpi.GetGraduatedCount(A.AffiliateID, 0, '') AS NUMERIC(18,2)) / CAST(kpi.GetRegisteredCount(A.AffiliateID, 0) AS NUMERIC(18,2))) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS Percentage2

	FROM dropdown.Affiliate A 
	WHERE A.AffiliateID > 0

	ORDER BY 1, 2

END
GO
--End procedure kpi.GetKPIData

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE permissionable.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'utility.SavePermissionableGroup'
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE permissionable.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure person.GetAffiliatesByPersonID
EXEC Utility.DropObject 'person.GetAffiliatesByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.22
-- Description:	A stored procedure to return data from the Person table based on a PersonID
-- ========================================================================================
CREATE PROCEDURE person.GetAffiliatesByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		A.AffiliateID,
		A.AffiliateName,
		A.AffiliateCountryCode,
		
		CASE
			WHEN P.DefaultAffiliateID = A.AffiliateID
			THEN 1
			ELSE 0
		END AS IsDefaultAffiliate

	FROM person.PersonAffiliate PA
		JOIN person.Person P ON P.PersonID = PA.PersonID
		JOIN dropdown.Affiliate A ON A.AffiliateID = PA.AffiliateID
			AND PA.PersonID = @PersonID
	ORDER BY A.AffiliateName

END
GO
--End procedure person.GetAffiliatesByPersonID

--Begin procedure program.GetClassByClassID
EXEC Utility.DropObject 'program.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Class table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added studentStatus data
--
-- Author:			Eric Jones
-- Create date:	2015.12.30
-- Description:	added status date and formatted status date
--
-- Author:			Todd Pires
-- Create date:	2016.04.07
-- Description:	Implemented the TrainerID columns
--
-- Author:			Todd Pires
-- Create date:	2016.08.09
-- Description:	Implemented more TrainerID columns
--
-- Author:			Kevin Ross
-- Create date:	2016.08.16
-- Description:	Implemented the StatusReason columns
-- ===============================================================================================
CREATE PROCEDURE program.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(CL.AffiliateID) AS AffiliateName,
		CL.ClassID,
		CL.ClassName,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location,
		CL.Seats,
		CL.Sessions,
		CL.StartDate,
		CL.EndDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CO.CourseID,
		CO.CourseName,
		T1.TrainerID AS TrainerID1,
		T1.TrainerName AS TrainerName1,
		T2.TrainerID AS TrainerID2,
		T2.TrainerName AS TrainerName2,
		T3.TrainerID AS TrainerID3,
		T3.TrainerName AS TrainerName3,
		T4.TrainerID AS TrainerID4,
		T4.TrainerName AS TrainerName4,
		T5.TrainerID AS TrainerID5,
		T5.TrainerName AS TrainerName5
	FROM program.Class CL
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN dropdown.Trainer T1 ON T1.TrainerID = CL.TrainerID1
		JOIN dropdown.Trainer T2 ON T2.TrainerID = CL.TrainerID2
		JOIN dropdown.Trainer T3 ON T3.TrainerID = CL.TrainerID3
		JOIN dropdown.Trainer T4 ON T4.TrainerID = CL.TrainerID4
		JOIN dropdown.Trainer T5 ON T5.TrainerID = CL.TrainerID5
	WHERE CL.ClassID = @ClassID
	
	SELECT
		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') AS FullName,
		CC.StudentStatusID,
		SS.StudentStatusName,
		CC.StatusDate,
		CC.StatusReason,
		dbo.FormatDate(CC.StatusDate) AS StatusDateFormatted
	FROM program.ClassContact CC
		JOIN program.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
		JOIN dropdown.StudentStatus SS ON SS.StudentStatusID= CC.StudentStatusID
			AND CL.ClassID = @ClassID

	SELECT
		P.ProgramName,
		P.ProgramID,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramContact PC ON PC.ContactID = CC.ContactID AND PC.ProgramID = P.ProgramID AND CC.ClassID = @ClassID)
			THEN 1
			ELSE 0
		END AS HasClassContact
	FROM program.ProgramClass PC
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
			AND CL.ClassID = @ClassID

END
GO
--End procedure program.GetClassByClassID

--Begin procedure program.GetProgramByProgramID
EXEC Utility.DropObject 'program.GetProgramByProgramID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Greg Yingling
-- Create date:	2015.09.25
-- Description:	A stored procedure to select data from the program.Program table
--
-- Author:			Eric Jones
-- Create date:	2015.12.28
-- Description:	converted from many to many programs to 1 to many
--
-- Author:			Eric Jones
-- Create date:	2016.09.21
-- Description:	converted from 1 to many programs to many to many
-- =============================================================================
CREATE PROCEDURE program.GetProgramByProgramID

@ProgramID INT,
@ContactID INT = 0,
@Locale VARCHAR(4) = 'EN'

AS
BEGIN
	SET NOCOUNT ON;

	--Programs
	SELECT
		P.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(P.AffiliateID) AS AffiliateName,
		P.AreSurveysEnabled,
		P.EndDate,
		dbo.FormatDate(P.EndDate) AS EndDateFormatted,
		P.Location,
		P.PreCommittedJobs,
		P.Process,
		P.ProgramID,
		P.ProgramName,
		P.StartDate,
		dbo.FormatDate(P.StartDate) AS StartDateFormatted,
		P.SurveyEndDateTime,
		dbo.FormatDate(P.SurveyEndDateTime) AS SurveyEndDateTimeFormatted,
		P.SurveyStartDateTime,
		dbo.FormatDate(P.SurveyStartDateTime) AS SurveyStartDateTimeFormatted,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		P.CountryID,
		C.CountryName,
		F.FormID,
		FL.FormText AS FormName,
		ISNULL(FC.IsComplete, 0) AS FormIsComplete
	FROM program.Program P
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = P.ProgramTypeID
		JOIN dropdown.Country C ON C.CountryID = P.CountryID
		LEFT JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode 
			AND FL.Locale = @Locale
		LEFT JOIN form.FormContact FC ON FC.FormID = P.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
	WHERE P.ProgramID = @ProgramID

	--Classes
	SELECT
		CL.ClassID,
		CL.ClassName as ClassName1,
		CO.CourseName as ClassName,
		CL.EndDate,
		CL.StartDate,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramContact PC ON PC.ContactID = CC.ContactID AND PC.ProgramID = @ProgramID AND CC.ClassID = CL.ClassID)
			THEN 1
			ELSE 0
		END AS HasClassContact
	FROM program.ProgramClass PC
		JOIN program.Class CL ON CL.ClassID = PC.ClassID
		JOIN program.Course CO ON CO.CourseID = CL.CourseID
		JOIN program.Program P ON P.ProgramID = PC.ProgramID
			AND P.ProgramID = @ProgramID

	--Organizations
	SELECT
		O.OrganizationID,
		O.OrganizationName
	FROM program.ProgramOrganization PO
		JOIN program.Program P ON P.ProgramID = PO.ProgramID
		JOIN dbo.Organization O ON O.OrganizationID = PO.OrganizationID
			AND P.ProgramID = @ProgramID		

	--Surveys
	SELECT
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			ELSE (SELECT SL.SurveyName FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = (SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID))
		END AS SurveyName,

		CASE
			WHEN @ContactID > 0 AND EXISTS (SELECT 1 FROM [survey].[SurveyResponse] SR WHERE SR.[SurveyID] = PS.SurveyID AND SR.ContactID = @ContactID AND SR.IsSubmitted = 1)
			THEN '<button class="btn btn-warning">Completed</button>'
			WHEN EXISTS (SELECT 1 FROM survey.SurveyLabel SL WHERE SL.SurveyID = S.SurveyID AND SL.LanguageCode = 'EN')
			THEN '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode=EN" target="_blank">Complete Form</a>'
			ELSE '<a class="btn btn-info" href="/public/?SurveyID='+ CAST(S.SurveyID AS VARCHAR(10)) +'&LanguageCode='+ CAST((SELECT TOP 1 SL.LanguageCode FROM survey.SurveyLanguage SL WHERE SL.SurveyID = S.SurveyID) AS VARCHAR(10)) +'" target="_blank">Complete Form</a>'
		END AS SurveyURL,

		CASE
			WHEN ST.SurveyTypeCode = 'Test'
			THEN ''
			ELSE '<a class="btn btn-info" href="/survey/addupdatesurveythreshold/id/'+ CAST(S.SurveyID AS VARCHAR(10)) +'/programid/'+ CAST(PS.ProgramID AS VARCHAR(10)) +'" target="_blank">Thresholds</a>' 
		END AS VerifyAnswersHTML,

		
		ST.SurveyTypeID,
		ST.SurveyTypeName
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
			AND PS.ProgramID = @ProgramID
	ORDER BY 2, 1

	--Contacts
	SELECT
		APS.ApplicantStatusName,
		APS.ApplicantStatusID,
		dbo.FormatContactNameByContactID(PC.ContactID,'FirstLast') AS ContactName,
		CAST(CCC.CountryCallingCode AS varchar)+', '+CAST(C.CellPhoneNumber AS varchar) AS CellPhoneNumberWithCountryCode,
		C.ContactID,
		CASE
			WHEN EXISTS (SELECT 1 FROM program.ClassContact CC JOIN program.ProgramClass PC ON PC.ClassID = CC.ClassID AND PC.ProgramID = @ProgramID AND CC.ContactID = C.ContactID)
			THEN 1
			ELSE 0
		END AS IsClassContact
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = C.CellPhoneNumberCountryCallingCodeID
			AND PC.ProgramID = @ProgramID

	--Contact counts
	SELECT
		CASE
			WHEN ISNULL(APS.ApplicantStatusName,'') = ''
			THEN 'No Status'
			ELSE APS.ApplicantStatusName
		END AS ApplicantStatusName,
		COUNT(C.ContactID) AS ContactCount
	FROM dbo.Contact C
		JOIN program.ProgramContact PC ON PC.ContactID = C.ContactID
		JOIN dropdown.ApplicantStatus APS ON APS.ApplicantStatusID = PC.ApplicantStatusID
			AND PC.ProgramID = @ProgramID
	GROUP BY APS.ApplicantStatusName

	--ProgramTypeForms
	SELECT
		F.FormID,
		FL.FormText AS FormName
	FROM form.Form F
		JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = @Locale
		JOIN program.ProgramTypeForm PTF ON PTF.FormID = F.FormID
		JOIN program.Program P ON P.ProgramTypeID = PTF.ProgramTypeID
			AND P.ProgramID = @ProgramID

END
GO
--End procedure program.GetProgramByProgramID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Admin', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Budget', 'Budgets', 0;
EXEC permissionable.SavePermissionableGroup 'Contact', 'Contacts', 0;
EXEC permissionable.SavePermissionableGroup 'Organization', 'Organizations', 0;
EXEC permissionable.SavePermissionableGroup 'Program', 'Program', 0;
EXEC permissionable.SavePermissionableGroup 'Proposal', 'Proposals', 0;
EXEC permissionable.SavePermissionableGroup 'Survey', 'Surveys', 0;
EXEC permissionable.SavePermissionableGroup 'Workflow', 'Workflows', 0;
EXEC permissionable.SavePermissionableGroup 'Workplan', 'Workplan', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the email template list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Add / edit a form object translation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateTranslation', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Form.AddUpdateTranslation', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='View the list of objects with translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListTranslation', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Form.ListTranslation', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='main', @DESCRIPTION='main.SSRSLink', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SSRSLink', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='main.SSRSLink', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / Update Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.addUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='list', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.list', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View Permission Templates', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add or Update System User', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List System Users', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View System User', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='serversetup', @DESCRIPTION='Edit the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='addupdate', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='serversetup.addupdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='View the server setup keys', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Admin', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='Add / edit a budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Budget.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View the budget list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Budget.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='Add / edit an expenditure', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='View the expenditure list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Expenditure', @DESCRIPTION='View an expenditure', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Budget', @PERMISSIONABLELINEAGE='Expenditure.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend payment report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with programs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ApplicantManagement', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.ApplicantManagement', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Enable the Associate All button', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.AssociateAll', @PERMISSIONCODE='AssociateAll';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with jobs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.OrganizationJob', @PERMISSIONCODE='OrganizationJob';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with mentors', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.OrganizationMentor', @PERMISSIONCODE='OrganizationMentor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Associate contacts with surveys', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Associate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.Associate.Survey', @PERMISSIONCODE='Survey';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.Beneficiary', @PERMISSIONCODE='Beneficiary';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts not of type beneficiary', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonBeneficiary', @PERMISSIONCODE='NonBeneficiary';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the event log', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Edit a contact', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View the email template list', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='Edit an organization', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='View the list of organizations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Organization', @DESCRIPTION='View an organization', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='Organization.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='Add / Edit an organization job', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='View an organization job', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='Add / Edit an organization mentor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='View an organization mentor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationJob', @DESCRIPTION='View the list of organization jobs', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationJob.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='OrganizationMentor', @DESCRIPTION='View the list of organization mentors', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Organization', @PERMISSIONABLELINEAGE='OrganizationMentor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='Add / Edit a Program', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='View the list of programs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Program', @DESCRIPTION='View a Program', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Program.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / Edit a Course', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / Edit a Class', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View the list of classes', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View a Class', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Program', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='Edit an award', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='Add / edit an award Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.AddUpdate.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View the list of awards', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View an award Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.View.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='Add / edit a proposal', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='Add / edit a proposal Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.AddUpdate.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View the list of proposals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View a proposal Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.View.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='Edit a subaward', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='Add / edit a subaward Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.AddUpdate.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View the list of subawards', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View a subaward Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.View.Budget', @PERMISSIONCODE='Budget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Award', @DESCRIPTION='View an award', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Award.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Proposal', @DESCRIPTION='View a proposal', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='Proposal.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubAward', @DESCRIPTION='View a subaward', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Proposal', @PERMISSIONABLELINEAGE='SubAward.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Create responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerForm', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.AdministerForm', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='Save responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveForm', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.SaveForm', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Form', @DESCRIPTION='View responses to forms', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewResponse', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Form.ViewResponse', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Add/Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='AddUpdate Survey Thresholds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurveyThreshold', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateSurveyThreshold', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Administer Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AdministerSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey List Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey Save Question ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Save Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Save Survey Thresholds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurveyThreshold', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.SaveSurveyThreshold', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Survey View Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ViewSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Add Update Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Add Update Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Administer Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Clone Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CloneSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.CloneSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Export Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management List Surveys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Save Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.SaveSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management Save Survey Responses', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SaveSurveyResponses', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.SaveSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management View Question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Management View Survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Budget', @DESCRIPTION='View a budget', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Budget.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View the survey question bank', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListQuestions', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='Add / edit survey questions', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.AddUpdateQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Survey', @DESCRIPTION='View surveys', @DISPLAYORDER=3, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Survey', @PERMISSIONABLELINEAGE='Survey.ListSurveys', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the workflow list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflow', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View the tree of workplan items', @DISPLAYORDER=2, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Tree', @PERMISSIONABLEGROUPCODE='Workplan', @PERMISSIONABLELINEAGE='Workplan.Tree', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table dbo.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO utility.BuildLog (BuildKey) VALUES ('Build - 2.0 - 2017.09.03 19.43.58')
GO
--End build tracking

