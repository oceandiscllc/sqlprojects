USE EFE
GO

--Begin table dbo.Proposal
DECLARE @TableName VARCHAR(250) = 'dbo.Proposal'

EXEC utility.DropColumn @TableName, 'ProposalDocument1ID'
EXEC utility.DropColumn @TableName, 'ProposalDocument2ID'
GO
--End table dbo.Proposal

--Begin table permissionable.PermissionableTemplatePermissionable
EXEC utility.AddColumn 'permissionable.PermissionableTemplatePermissionable', 'PermissionableLineage', 'VARCHAR(MAX)'
GO

UPDATE PTP
SET PTP.PermissionableLineage = P.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP
	JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplatePermissionable
