USE EFE
GO

--Begin procedure eventlog.LogContactSurveyAction
EXEC Utility.DropObject 'eventlog.LogContactSurveyAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		Greg Yingling
-- Create date: 2016.02.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- =========================================================================
CREATE PROCEDURE eventlog.LogContactSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ContactSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ContactSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ContactSurvey'), ELEMENTS
			)
		FROM dbo.ContactSurvey T
		WHERE T.ContactSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactSurveyAction

--Begin procedure proposal.GetProposalByProposalID
EXEC Utility.DropObject 'proposal.GetProposalByProposalID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================================
-- Author:		Adam Davis
-- Create date:	2015.10.16
-- Description: A stored procedure to return data from the dbo.Proposal table based on a ProposalID
--
-- Author:		Greg Yingling
-- Create date:	2016.03.15
-- Description: Add support for documents
-- ===============================================================================================
CREATE PROCEDURE proposal.GetProposalByProposalID

@ProposalID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nAffiliateID INT = (SELECT P.AffiliateID FROM dbo.Proposal P WHERE P.ProposalID = @ProposalID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Proposal', @ProposalID)

	SELECT
		Proposal.AffiliateID,
		dropdown.GetAffiliateNameByAffiliateID(Proposal.AffiliateID) AS AffiliateName,
		Proposal.ProposalID
		,Proposal.DonorID
		,Proposal.PrincipalID
		,Proposal.BudgetID
		,Budget.BudgetName
		,Budget.CurrencyID
		,Proposal.WorkplanID
		,Proposal.DeliveryLeadID
		,Proposal.ProposalTypeID
		,Proposal.StartDate
		,dbo.FormatDate(Proposal.StartDate) AS StartDateFormatted
		,Proposal.EndDate
		,dbo.FormatDate(Proposal.EndDate) AS EndDateFormatted
		,Proposal.ProposalName
		,Proposal.ExecutiveSummary
		,Proposal.RationaleAndContext
		,Proposal.ProjectVisionAndDescription
		,Proposal.MonitoringAndEvaluation
		,Proposal.MarketingAndCommunications
		,Proposal.OrganisationalHistoryAndTrackRecord
		,Proposal.Annex1
		,Proposal.RiskManagement
		,Proposal.KnowledgeSharing
		,Proposal.ReferenceForMarketingCommunicationsSupport
		,Proposal.ProposalOutcome
		,Donor.OrganizationName AS DonorName
		,Donor.Website
		,Donor.Address AS DonorAddress
		,DonorCountry.CountryName AS DonorCountry
		,person.FormatPersonNameByPersonID(DeliveryLead.PersonID, 'lastfirst') AS DeliveryLeadName
		,dbo.FormatContactNameByContactID(Principal.ContactID, 'lastfirstmiddle') AS PrincipalName
		,Principal.Address1 AS PrincipalAddress1
		,Principal.Address2 AS PrincipalAddress2
		,Principal.City AS PrincipalCity
		,Principal.PostalCode AS PrincipalPostalCode
		,PrincipalCountry.CountryName AS PrincipalCountry
		,Principal.EmailAddress AS PrincipalEmailAddress
		,Principal.SkypeName 
		,PrincipalPhoneCallingCode.CountryCallingCode AS PhoneCallingCode
		,Principal.PhoneNumber
		,PrincipalCellCallingCode.CountryCallingCode AS CellCallingCode
		,Principal.CellPhoneNumber
		,ProposalType.ProposalTypeName
		,Workplan.WorkplanID
		,Workplan.WorkplanTitle
		,Award.AwardID
	FROM 
		dbo.Proposal AS Proposal
	LEFT JOIN
		dbo.Organization AS Donor ON Proposal.DonorID = Donor.OrganizationID
	LEFT JOIN 
		dropdown.Country AS DonorCountry ON Donor.AddressCountryID = DonorCountry.CountryID
	LEFT JOIN
		dbo.Contact AS Principal ON Principal.ContactID = Proposal.PrincipalID
	LEFT JOIN
		dropdown.Country AS PrincipalCountry ON Principal.CountryID = PrincipalCountry.CountryID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalPhoneCallingCode ON PrincipalPhoneCallingCode.CountryCallingCodeID = Principal.PhoneNumberCountryCallingCodeID
	LEFT JOIN
		dropdown.CountryCallingCode AS PrincipalCellCallingCode ON PrincipalCellCallingCode.CountryCallingCodeID = Principal.CellPhoneNumberCountryCallingCodeID
	LEFT JOIN
		person.Person AS DeliveryLead ON DeliveryLead.PersonID = Proposal.DeliveryLeadID
	INNER JOIN
		dropdown.ProposalType AS ProposalType ON ProposalType.ProposalTypeID = Proposal.ProposalTypeID
	LEFT JOIN
		workplan.Workplan AS Workplan ON Workplan.WorkplanID = Proposal.WorkplanID
	LEFT JOIN 
		budget.Budget AS Budget ON Budget.BudgetID = Proposal.BudgetID
	LEFT JOIN 
		dbo.Award AS Award ON Award.ProposalID = Proposal.ProposalID
	WHERE
		Proposal.ProposalID = @ProposalID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Proposal'
			AND DE.EntityID = @ProposalID

	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('Proposal', @ProposalID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('Proposal', @ProposalID, @nWorkflowStepNumber, @nAffiliateID) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Proposal'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Proposal'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Proposal'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Proposal'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Proposal'
		AND EL.EntityID = @ProposalID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure proposal.GetProposalByProposalID
