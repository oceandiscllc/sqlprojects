USE EFE
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.01
-- Description:	A stored procedure to data from the contact.Contact table
--
-- Author:			Adam Davis
-- Create date: 2015.10.19
-- Description: Fixed references to OtherPhoneNumber and dbo.Contact
-- ======================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.LastName,
		C1.MiddleName,
		C1.OtherPhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.SkypeName,
		C1.Username,
		C1.InvalidLoginAttempts,
		C1.IsAccountLockedOut,
		C1.IsActive,
		C1.AffiliateID,
		C2.CountryID AS CitizenshipCountryID,
		C2.CountryName AS CitizenshipCountryName,
		C3.CountryID,
		C3.CountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCodeCode,
		CCC1.CountryCallingCodeID AS CellPhoneNumberCountryCallingCodeID,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCodeCode,
		CCC2.CountryCallingCodeID AS FaxNumberCountryCallingCodeID,
		CCC3.CountryCallingCode AS OtherPhoneNumberCountryCallingCodeCode,
		CCC3.CountryCallingCodeID AS OtherPhoneNumberCountryCallingCodeID,
		MS.MaritalStatusID,
		MS.MaritalStatusName,
		CT.ContactTypeID,
		CT.ContactTypeName,
		C1.AffiliateID,
		A.AffiliateName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.OtherPhoneNumberCountryCallingCodeID
		JOIN dropdown.MaritalStatus MS ON MS.MaritalStatusID = C1.MaritalStatusID
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = C1.ContactTypeID
		JOIN dropdown.Affiliate A on A.AffiliateID = C1.AffiliateID
			AND C1.ContactID = @ContactID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID

	SELECT
		KPI.KPIContactDetailID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.ContactRoles, 'EN') AS ContactRolesLabel,
		form.GetQuestionResponseLabel(KPI.PersonalIncomeBand, 'EN') AS PersonalIncomeBandLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIContactDetail KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIEnterpriseID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.BusinessEndDate) AS BusinessEndDateFormatted, 
		dbo.FormatDate(KPI.BusinessStartDate) AS BusinessStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.BusinessFinancing, 'EN') AS BusinessFinancingLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRegistered, 'EN') AS BusinessRegisteredLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRevenueGenerating, 'EN') AS BusinessRevenueGeneratingLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIEnterprise KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPlacementID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.EmploymentEndDate) AS EmploymentEndDateFormatted, 
		dbo.FormatDate(KPI.EmploymentStartDate) AS EmploymentStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.HadPreviosJob, 'EN') AS HadPreviosJobLabel,
		form.GetQuestionResponseLabel(KPI.JobType, 'EN') AS JobTypeLabel,
		form.GetQuestionResponseLabel(KPI.WorkedThreeMonths, 'EN') AS WorkedThreeMonthsLabel,
		KPI.Employer,
		KPI.JobTitle,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPlacement KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPrepID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.CareerPath, 'EN') AS CareerPathLabel,
		form.GetQuestionResponseLabel(KPI.InterviewingSkills, 'EN') AS InterviewingSkillsLabel,
		form.GetQuestionResponseLabel(KPI.JobSearch, 'EN') AS JobSearchLabel,
		form.GetQuestionResponseLabel(KPI.WritingResume, 'EN') AS WritingResumeLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPrep KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure survey.GetSurveysByProgramIDAndContactID
EXEC Utility.DropObject 'survey.GetSurveysByProgramIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.19
-- Description:	A stored procedure to survey data assoicated with a programid and a contactid
-- ==========================================================================================
CREATE PROCEDURE survey.GetSurveysByProgramIDAndContactID

@ProgramID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PS.SurveyID,
		survey.GetSurveyNameBySurveyID(PS.SurveyID) AS SurveyName,
		ISNULL(OASR.SurveyResponseID, 0) AS SurveyResponseID,
		ISNULL(OASR.SurveyResponseStatus, 'Not Started') AS SurveyResponseStatus,
		survey.GetCorrectResponseCount(@ProgramID, PS.SurveyID, @ContactID) AS CorrectResponseCount,
		(
		SELECT 
			COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID) 
		FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		WHERE SQRCK.ProgramID = 
			CASE
				WHEN ST.SurveyTypeCode = 'ApplicationTest'
				THEN 0
				ELSE @ProgramID 
			END 
			AND SQRCK.SurveyID = PS.SurveyID 
			AND SQRCK.IsForScore = 1
		) AS TotalGradedQuestionCount,
		'Survey' AS EntityTypeCode
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
	OUTER APPLY
		(
		SELECT 
			SR.SurveyResponseID,
	
			CASE
				WHEN SR.IsSubmitted = 1
				THEN 'Submitted'
				ELSE 'Not Submitted'
			END AS SurveyResponseStatus
	
		FROM survey.SurveyResponse SR 
		WHERE SR.SurveyID = PS.SurveyID 
			AND SR.ContactID = @ContactID
		) OASR
	WHERE PS.ProgramID = @ProgramID
		
	UNION

	SELECT 
		F.FormID AS SurveyID,
		FL.FormText AS SurveyName,
		FC.FormContactID AS SurveyResponseID,
		CASE
			WHEN ISNULL(FC.IsComplete, 0) = 1
			THEN 'Submitted'
			ELSE 'Not Started'
		END AS SurveyResponseStatus,
		0 AS CorrectResponseCount,
		0 AS TotalGradedQuestionCount,
		'Form' AS EntityTypeCode
	FROM program.Program P
		JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormContact FC ON FC.FormID = F.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = 'EN'
	WHERE P.ProgramID = @ProgramID
		
END
GO
--End procedure survey.GetSurveysByProgramIDAndContactID