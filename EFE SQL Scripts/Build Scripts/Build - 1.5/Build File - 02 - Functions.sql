USE EFE
GO

--Begin function form.GetQuestionResponseLabel
EXEC Utility.DropObject 'form.GetQuestionResponseLabel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.07.31
-- Description:	A function to return a localized label based on a question response code
-- ==========================================================================================
CREATE FUNCTION form.GetQuestionResponseLabel
(
@QuestionRepsonseCodeList VARCHAR(MAX),
@Locale VARCHAR(4)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(MAX) = ''

	IF CHARINDEX(',', @QuestionRepsonseCodeList) > 0
		BEGIN

		SELECT @cReturn = COALESCE(@cReturn + ', ', '') + QRL.QuestionResponseText
		FROM form.QuestionResponseLabel QRL
			JOIN dbo.ListToTable(@QuestionRepsonseCodeList, ',') LTT ON LTT.ListItem = QRL.QuestionResponseCode
				AND QRL.LOCALE = @Locale

		SET @cReturn = STUFF(@cReturn, 1, 2, '')
		END
	ELSE
		BEGIN

		SELECT @cReturn = QRL.QuestionResponseText
		FROM form.QuestionResponseLabel QRL
		WHERE QRL.QuestionResponseCode = @QuestionRepsonseCodeList
			AND QRL.LOCALE = @Locale

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
--End function form.GetQuestionResponseLabel


