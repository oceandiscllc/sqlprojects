-- File Name:	Build-1.5 File 01 - EFE.sql
-- Build Key:	Build-1.5 File 01 - EFE - 2016.08.23 17.14.50

USE EFE
GO

-- ==============================================================================================================================
-- Functions:
--		form.GetQuestionResponseLabel
--
-- Procedures:
--		dbo.GetContactByContactID
--		survey.GetSurveysByProgramIDAndContactID
--
-- Tables:
--		form.KPIContactDetail
--		form.KPIEnterprise
--		form.KPIJobPlacement
--		form.KPIJobPrep
--		form.KPIQuestion
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE EFE
GO

--Begin table form.KPIQuestion
DECLARE @TableName VARCHAR(250) = 'form.KPIQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE form.KPIQuestion
	(
	KPIQuestionID INT NOT NULL IDENTITY(1,1),
	QuestionCode VARCHAR(50),
	KPICategory VARCHAR(50),
	KPICode VARCHAR(50)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_KPIQuestion', 'QuestionCode,KPICategory,KPICode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIQuestionID'
GO

INSERT INTO form.KPIQuestion
	(QuestionCode, KPICategory, KPICode)
SELECT 
	Q.QuestionCode,

	CASE
		WHEN Q.QuestionCode IN ('Q014','Q020')
		THEN 'KPIContactDetail'
		WHEN Q.QuestionCode IN ('Q016','Q035','Q036','Q100','Q139','Q142')
		THEN 'KPIEnterprise'
		WHEN Q.QuestionCode IN ('Q015','Q034','Q088','Q089','Q090','Q103','Q105','Q106','Q127','Q128','Q129','Q130','Q163','Q168','Q180')
		THEN 'KPIJobPlacement'
		WHEN Q.QuestionCode IN ('Q057','Q071','Q072','Q074')
		THEN 'KPIJobPrep'
	END,

	CASE
		WHEN Q.QuestionCode IN ('Q014')
		THEN 'ContactRoles'
		WHEN Q.QuestionCode IN ('Q020')
		THEN 'PersonalIncomeBand'

		WHEN Q.QuestionCode IN ('Q016','Q142')
		THEN 'BusinessStartDate'
		WHEN Q.QuestionCode IN ('Q035')
		THEN 'BusinessRegistered'
		WHEN Q.QuestionCode IN ('Q036')
		THEN 'BusineesRevenueGenerating'
		WHEN Q.QuestionCode IN ('Q100')
		THEN 'BusinessFinancing'
		WHEN Q.QuestionCode IN ('Q139')
		THEN 'BusinessEndDate'

		WHEN Q.QuestionCode IN ('Q015','Q034','Q088','Q129','Q163')
		THEN 'EmploymentStartDate'
		WHEN Q.QuestionCode IN ('Q089','Q105','Q130')
		THEN 'Employer'
		WHEN Q.QuestionCode IN ('Q090','Q106''Q168')
		THEN 'JobTitle'
		WHEN Q.QuestionCode IN ('Q103')
		THEN 'HadPreviosJob'
		WHEN Q.QuestionCode IN ('Q127')
		THEN 'WorkedThreeMonths'
		WHEN Q.QuestionCode IN ('Q128')
		THEN 'EmploymentEndDate'
		WHEN Q.QuestionCode IN ('Q180')
		THEN 'JobType'

		WHEN Q.QuestionCode IN ('Q057')
		THEN 'CareerPath'
		WHEN Q.QuestionCode IN ('Q071')
		THEN 'WritingResume'
		WHEN Q.QuestionCode IN ('Q072')
		THEN 'JobSearch'
		WHEN Q.QuestionCode IN ('Q074')
		THEN 'InterviewingSkills'
	END

FROM form.Question Q
WHERE Q.QuestionCode IN ('Q014','Q015','Q016','Q020','Q034','Q035','Q036','Q057','Q071','Q072','Q074','Q088','Q089','Q090','Q100','Q103','Q105','Q106','Q127','Q128','Q129','Q130','Q139','Q142','Q163','Q168','Q180')
GO
--End table form.KPIQuestion

--Begin table form.KPIContactDetail
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIContactDetail')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIContactDetail'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIContactDetail
		(
		KPIContactDetailID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID VARCHAR(50),
		ContactID VARCHAR(50),
		PersonalIncomeBand VARCHAR(50),
		ContactRoles VARCHAR(MAX)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIContactDetail', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIContactDetailID'

	END
--ENDIF
GO
--End table form.KPIContactDetail

--Begin table form.KPIEnterprise
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIEnterprise')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIEnterprise'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIEnterprise
		(
		KPIEnterpriseID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		BusinessRevenueGenerating VARCHAR(50),
		BusinessEndDate DATE,
		BusinessFinancing VARCHAR(50),
		BusinessRegistered VARCHAR(50),
		BusinessStartDate DATE
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIEnterprise', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIEnterpriseID'

	END
--ENDIF
GO
--End table form.KPIEnterprise

--Begin table form.KPIJobPlacement
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIJobPlacement')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIJobPlacement'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIJobPlacement
		(
		KPIJobPlacementID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		Employer NVARCHAR(MAX),
		EmploymentEndDate DATE,
		EmploymentStartDate DATE,
		HadPreviosJob VARCHAR(50),
		JobTitle NVARCHAR(MAX),
		JobType VARCHAR(50),
		WorkedThreeMonths VARCHAR(50)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIJobPlacement', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIJobPlacementID'

	END
--ENDIF
GO
--End table form.KPIJobPlacement

--Begin table form.KPIJobPrep
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIJobPrep')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIJobPrep'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIJobPrep
		(
		KPIJobPrepID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		CareerPath VARCHAR(50),
		InterviewingSkills VARCHAR(50),
		JobSearch VARCHAR(50),
		WritingResume VARCHAR(50)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIJobPrepID', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIJobPrepID'

	END
--ENDIF
GO
--End table form.KPIJobPrep

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE EFE
GO

--Begin function form.GetQuestionResponseLabel
EXEC Utility.DropObject 'form.GetQuestionResponseLabel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.07.31
-- Description:	A function to return a localized label based on a question response code
-- ==========================================================================================
CREATE FUNCTION form.GetQuestionResponseLabel
(
@QuestionRepsonseCodeList VARCHAR(MAX),
@Locale VARCHAR(4)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(MAX) = ''

	IF CHARINDEX(',', @QuestionRepsonseCodeList) > 0
		BEGIN

		SELECT @cReturn = COALESCE(@cReturn + ', ', '') + QRL.QuestionResponseText
		FROM form.QuestionResponseLabel QRL
			JOIN dbo.ListToTable(@QuestionRepsonseCodeList, ',') LTT ON LTT.ListItem = QRL.QuestionResponseCode
				AND QRL.LOCALE = @Locale

		SET @cReturn = STUFF(@cReturn, 1, 2, '')
		END
	ELSE
		BEGIN

		SELECT @cReturn = QRL.QuestionResponseText
		FROM form.QuestionResponseLabel QRL
		WHERE QRL.QuestionResponseCode = @QuestionRepsonseCodeList
			AND QRL.LOCALE = @Locale

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
--End function form.GetQuestionResponseLabel



--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE EFE
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.09.01
-- Description:	A stored procedure to data from the contact.Contact table
--
-- Author:			Adam Davis
-- Create date: 2015.10.19
-- Description: Fixed references to OtherPhoneNumber and dbo.Contact
-- ======================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.LastName,
		C1.MiddleName,
		C1.OtherPhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.SkypeName,
		C1.Username,
		C1.InvalidLoginAttempts,
		C1.IsAccountLockedOut,
		C1.IsActive,
		C1.AffiliateID,
		C2.CountryID AS CitizenshipCountryID,
		C2.CountryName AS CitizenshipCountryName,
		C3.CountryID,
		C3.CountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCodeCode,
		CCC1.CountryCallingCodeID AS CellPhoneNumberCountryCallingCodeID,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCodeCode,
		CCC2.CountryCallingCodeID AS FaxNumberCountryCallingCodeID,
		CCC3.CountryCallingCode AS OtherPhoneNumberCountryCallingCodeCode,
		CCC3.CountryCallingCodeID AS OtherPhoneNumberCountryCallingCodeID,
		MS.MaritalStatusID,
		MS.MaritalStatusName,
		CT.ContactTypeID,
		CT.ContactTypeName,
		C1.AffiliateID,
		A.AffiliateName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.OtherPhoneNumberCountryCallingCodeID
		JOIN dropdown.MaritalStatus MS ON MS.MaritalStatusID = C1.MaritalStatusID
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = C1.ContactTypeID
		JOIN dropdown.Affiliate A on A.AffiliateID = C1.AffiliateID
			AND C1.ContactID = @ContactID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentDescription,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID

	SELECT
		KPI.KPIContactDetailID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.ContactRoles, 'EN') AS ContactRolesLabel,
		form.GetQuestionResponseLabel(KPI.PersonalIncomeBand, 'EN') AS PersonalIncomeBandLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIContactDetail KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIEnterpriseID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.BusinessEndDate) AS BusinessEndDateFormatted, 
		dbo.FormatDate(KPI.BusinessStartDate) AS BusinessStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.BusinessFinancing, 'EN') AS BusinessFinancingLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRegistered, 'EN') AS BusinessRegisteredLabel,
		form.GetQuestionResponseLabel(KPI.BusinessRevenueGenerating, 'EN') AS BusinessRevenueGeneratingLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIEnterprise KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPlacementID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		dbo.FormatDate(KPI.EmploymentEndDate) AS EmploymentEndDateFormatted, 
		dbo.FormatDate(KPI.EmploymentStartDate) AS EmploymentStartDateFormatted, 
		form.GetQuestionResponseLabel(KPI.HadPreviosJob, 'EN') AS HadPreviosJobLabel,
		form.GetQuestionResponseLabel(KPI.JobType, 'EN') AS JobTypeLabel,
		form.GetQuestionResponseLabel(KPI.WorkedThreeMonths, 'EN') AS WorkedThreeMonthsLabel,
		KPI.Employer,
		KPI.JobTitle,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPlacement KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

	SELECT
		KPI.KPIJobPrepID, 
		dbo.FormatDate(KPI.FormSubmitDate) AS FormSubmitDateFormatted, 
		form.GetQuestionResponseLabel(KPI.CareerPath, 'EN') AS CareerPathLabel,
		form.GetQuestionResponseLabel(KPI.InterviewingSkills, 'EN') AS InterviewingSkillsLabel,
		form.GetQuestionResponseLabel(KPI.JobSearch, 'EN') AS JobSearchLabel,
		form.GetQuestionResponseLabel(KPI.WritingResume, 'EN') AS WritingResumeLabel,
		ISNULL((SELECT P.ProgramName FROM program.Program P WHERE P.ProgramID = KPI.ProgramID), '') AS ProgramName
	FROM form.KPIJobPrep KPI
	WHERE KPI.ContactID = @ContactID
	ORDER BY 2 DESC

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure survey.GetSurveysByProgramIDAndContactID
EXEC Utility.DropObject 'survey.GetSurveysByProgramIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.19
-- Description:	A stored procedure to survey data assoicated with a programid and a contactid
-- ==========================================================================================
CREATE PROCEDURE survey.GetSurveysByProgramIDAndContactID

@ProgramID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PS.SurveyID,
		survey.GetSurveyNameBySurveyID(PS.SurveyID) AS SurveyName,
		ISNULL(OASR.SurveyResponseID, 0) AS SurveyResponseID,
		ISNULL(OASR.SurveyResponseStatus, 'Not Started') AS SurveyResponseStatus,
		survey.GetCorrectResponseCount(@ProgramID, PS.SurveyID, @ContactID) AS CorrectResponseCount,
		(
		SELECT 
			COUNT(SQRCK.SurveyQuestionResponseChoiceKeyID) 
		FROM survey.SurveyQuestionResponseChoiceKey SQRCK
		WHERE SQRCK.ProgramID = 
			CASE
				WHEN ST.SurveyTypeCode = 'ApplicationTest'
				THEN 0
				ELSE @ProgramID 
			END 
			AND SQRCK.SurveyID = PS.SurveyID 
			AND SQRCK.IsForScore = 1
		) AS TotalGradedQuestionCount,
		'Survey' AS EntityTypeCode
	FROM program.ProgramSurvey PS
		JOIN survey.Survey S ON S.SurveyID = PS.SurveyID
		JOIN dropdown.SurveyType ST ON ST.SurveyTypeID = S.SurveyTypeID
	OUTER APPLY
		(
		SELECT 
			SR.SurveyResponseID,
	
			CASE
				WHEN SR.IsSubmitted = 1
				THEN 'Submitted'
				ELSE 'Not Submitted'
			END AS SurveyResponseStatus
	
		FROM survey.SurveyResponse SR 
		WHERE SR.SurveyID = PS.SurveyID 
			AND SR.ContactID = @ContactID
		) OASR
	WHERE PS.ProgramID = @ProgramID
		
	UNION

	SELECT 
		F.FormID AS SurveyID,
		FL.FormText AS SurveyName,
		FC.FormContactID AS SurveyResponseID,
		CASE
			WHEN ISNULL(FC.IsComplete, 0) = 1
			THEN 'Submitted'
			ELSE 'Not Started'
		END AS SurveyResponseStatus,
		0 AS CorrectResponseCount,
		0 AS TotalGradedQuestionCount,
		'Form' AS EntityTypeCode
	FROM program.Program P
		JOIN form.Form F ON F.FormID = P.FormID
		LEFT JOIN form.FormContact FC ON FC.FormID = F.FormID
			AND FC.ContactID = @ContactID
			AND FC.ProgramID = @ProgramID
		LEFT JOIN form.FormLabel FL ON FL.FormCode = F.FormCode
			AND FL.Locale = 'EN'
	WHERE P.ProgramID = @ProgramID
		
END
GO
--End procedure survey.GetSurveysByProgramIDAndContactID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE EFE
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Admin', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Budget', 'Budgets', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contacts', 0
GO
EXEC utility.SavePermissionableGroup 'Organization', 'Organizations', 0
GO
EXEC utility.SavePermissionableGroup 'Program', 'Program', 0
GO
EXEC utility.SavePermissionableGroup 'Proposal', 'Proposals', 0
GO
EXEC utility.SavePermissionableGroup 'Survey', 'Surveys', 0
GO
EXEC utility.SavePermissionableGroup 'Territory', 'Territory', 0
GO
EXEC utility.SavePermissionableGroup 'Workflow', 'Workflows', 0
GO
EXEC utility.SavePermissionableGroup 'Workplan', 'Workplan', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Access to the export utility', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'AddUpdate', NULL, 'EmailTemplate.AddUpdate', 'Add / edit an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'List', NULL, 'EmailTemplate.List', 'View the email template list', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EmailTemplate', 'View', NULL, 'EmailTemplate.View', 'View an email template', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'AddUpdateTranslation', NULL, 'Form.AddUpdateTranslation', 'Add / edit a form object translation', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Form', 'ListTranslation', NULL, 'Form.ListTranslation', 'View the list of objects with translations', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors', 1, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'addUpdate', NULL, 'PermissionableTemplate.addUpdate', 'Add / Update Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'list', NULL, 'PermissionableTemplate.list', 'List Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View Permission Templates', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add or Update System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List System Users', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View System User', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'serversetup', 'addupdate', NULL, 'serversetup.addupdate', 'Edit the server setup keys', 0, 0, 'Admin'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View an event log', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'View the server setup keys', 0, 1, 'Admin'
GO
EXEC utility.SavePermissionable 'Budget', 'AddUpdate', NULL, 'Budget.AddUpdate', 'Add / edit a budget', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Budget', 'List', NULL, 'Budget.List', 'View the budget list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'View the equipment inventory list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View equipment inventory', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'AddUpdate', NULL, 'Expenditure.AddUpdate', 'Add / edit an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'List', NULL, 'Expenditure.List', 'View the expenditure list', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Expenditure', 'View', NULL, 'Expenditure.View', 'View an expenditure', 0, 0, 'Budget'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationJob', 'Contact.Associate.OrganizationJob', 'Associate contacts with jobs', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'OrganizationMentor', 'Contact.Associate.OrganizationMentor', 'Associate contacts with mentors', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'Associate', 'Survey', 'Contact.Associate.Survey', 'Associate contacts with surveys', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', NULL, 'Contact.List', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'View the list of contacts of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'NonBeneficiary', 'Contact.List.NonBeneficiary', 'View the list of contacts not of type beneficiary', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'View the event log', 0, 1, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', NULL, 'Contact.AddUpdate', 'Edit a contact', 0, 2, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', NULL, 'Contact.View', 'View a contact', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'View the email template list', 0, 3, 'Contact'
GO
EXEC utility.SavePermissionable 'Organization', 'AddUpdate', NULL, 'Organization.AddUpdate', 'Edit an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'List', NULL, 'Organization.List', 'View the list of organizations', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'Organization', 'View', NULL, 'Organization.View', 'View an organization', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'AddUpdate', NULL, 'OrganizationJob.AddUpdate', 'Add / Edit an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'View', NULL, 'OrganizationJob.View', 'View an organization job', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'AddUpdate', NULL, 'OrganizationMentor.AddUpdate', 'Add / Edit an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'View', NULL, 'OrganizationMentor.View', 'View an organization mentor', 0, 0, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationJob', 'List', NULL, 'OrganizationJob.List', 'View the list of organization jobs', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'OrganizationMentor', 'List', NULL, 'OrganizationMentor.List', 'View the list of organization mentors', 0, 1, 'Organization'
GO
EXEC utility.SavePermissionable 'Program', 'AddUpdate', NULL, 'Program.AddUpdate', 'Add / Edit a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'List', NULL, 'Program.List', 'View the list of programs', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Program', 'View', NULL, 'Program.View', 'View a Program', 0, 0, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit a Course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'View the course catalog', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View a course', 0, 1, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'View the list of classes', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View a Class', 0, 2, 'Program'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', NULL, 'Award.AddUpdate', 'Edit an award', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'AddUpdate', 'Budget', 'Award.AddUpdate.Budget', 'Add / edit an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'List', NULL, 'Award.List', 'View the list of awards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', 'Budget', 'Award.View.Budget', 'View an award Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', NULL, 'Proposal.AddUpdate', 'Add / edit a proposal', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'AddUpdate', 'Budget', 'Proposal.AddUpdate.Budget', 'Add / edit a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'List', NULL, 'Proposal.List', 'View the list of proposals', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', 'Budget', 'Proposal.View.Budget', 'View a proposal Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', NULL, 'SubAward.AddUpdate', 'Edit a subaward', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'AddUpdate', 'Budget', 'SubAward.AddUpdate.Budget', 'Add / edit a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'List', NULL, 'SubAward.List', 'View the list of subawards', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', 'Budget', 'SubAward.View.Budget', 'View a subaward Budget', 0, 0, 'Proposal'
GO
EXEC utility.SavePermissionable 'Award', 'View', NULL, 'Award.View', 'View an award', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Proposal', 'View', NULL, 'Proposal.View', 'View a proposal', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'SubAward', 'View', NULL, 'SubAward.View', 'View a subaward', 0, 1, 'Proposal'
GO
EXEC utility.SavePermissionable 'Form', 'AdministerForm', NULL, 'Form.AdministerForm', 'Create responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'SaveForm', NULL, 'Form.SaveForm', 'Save responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Form', 'ViewResponse', NULL, 'Form.ViewResponse', 'View responses to forms', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurvey', NULL, 'Survey.AddUpdateSurvey', 'Add/Update', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateSurveyThreshold', NULL, 'Survey.AddUpdateSurveyThreshold', 'AddUpdate Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AdministerSurvey', NULL, 'Survey.AdministerSurvey', 'Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveyResponses', NULL, 'Survey.ListSurveyResponses', 'Survey List Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveQuestion', NULL, 'Survey.SaveQuestion', 'Survey Save Question ', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurvey', NULL, 'Survey.SaveSurvey', 'Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'SaveSurveyThreshold', NULL, 'Survey.SaveSurveyThreshold', 'Save Survey Thresholds', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewQuestion', NULL, 'Survey.ViewQuestion', 'View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurvey', NULL, 'Survey.ViewSurvey', 'View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ViewSurveyResponses', NULL, 'Survey.ViewSurveyResponses', 'Survey View Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Management Add Update Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Management Add Update Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Management Administer Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'CloneSurvey', NULL, 'SurveyManagement.CloneSurvey', 'Management Clone Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Management Export Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Management List Surveys', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurvey', NULL, 'SurveyManagement.SaveSurvey', 'Management Save Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'SaveSurveyResponses', NULL, 'SurveyManagement.SaveSurveyResponses', 'Management Save Survey Responses', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'Management View Question', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'Management View Survey', 0, 0, 'Survey'
GO
EXEC utility.SavePermissionable 'Budget', 'View', NULL, 'Budget.View', 'View a budget', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListQuestions', NULL, 'Survey.ListQuestions', 'View the survey question bank', 0, 1, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'AddUpdateQuestion', NULL, 'Survey.AddUpdateQuestion', 'Add / edit survey questions', 0, 2, 'Survey'
GO
EXEC utility.SavePermissionable 'Survey', 'ListSurveys', NULL, 'Survey.ListSurveys', 'View surveys', 0, 3, 'Survey'
GO
EXEC utility.SavePermissionable 'District', 'List', NULL, 'District.List', 'View the list of districts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'District', 'View', NULL, 'District.View', 'View a district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'View', NULL, 'Front.View', 'View a front', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'List', NULL, 'Governorate.List', 'View the list of governorates', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Governorate', 'View', NULL, 'Governorate.View', 'View a governorate', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'List', NULL, 'SubDistrict.List', 'View the list of subdistricts', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'SubDistrict', 'View', NULL, 'SubDistrict.View', 'View a sub district', 0, 0, 'Territory'
GO
EXEC utility.SavePermissionable 'Front', 'List', NULL, 'Front.List', 'View the list of fronts', 0, 1, 'Territory'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the workflow list', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflow'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'View the list of workplans', 0, 1, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'Tree', NULL, 'Workplan.Tree', 'View the tree of workplan items', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View the a workplan', 0, 2, 'Workplan'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add / edit a workplan', 0, 3, 'Workplan'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table person.PersonPermissionable
DELETE PP FROM person.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.5 File 01 - EFE - 2016.08.23 17.14.50')
GO
--End build tracking

