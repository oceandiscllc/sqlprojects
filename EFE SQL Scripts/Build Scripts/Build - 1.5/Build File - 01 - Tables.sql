USE EFE
GO

--Begin table form.KPIQuestion
DECLARE @TableName VARCHAR(250) = 'form.KPIQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE form.KPIQuestion
	(
	KPIQuestionID INT NOT NULL IDENTITY(1,1),
	QuestionCode VARCHAR(50),
	KPICategory VARCHAR(50),
	KPICode VARCHAR(50)
	)

EXEC utility.SetIndexClustered @TableName, 'IX_KPIQuestion', 'QuestionCode,KPICategory,KPICode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIQuestionID'
GO

INSERT INTO form.KPIQuestion
	(QuestionCode, KPICategory, KPICode)
SELECT 
	Q.QuestionCode,

	CASE
		WHEN Q.QuestionCode IN ('Q014','Q020')
		THEN 'KPIContactDetail'
		WHEN Q.QuestionCode IN ('Q016','Q035','Q036','Q100','Q139','Q142')
		THEN 'KPIEnterprise'
		WHEN Q.QuestionCode IN ('Q015','Q034','Q088','Q089','Q090','Q103','Q105','Q106','Q127','Q128','Q129','Q130','Q163','Q168','Q180')
		THEN 'KPIJobPlacement'
		WHEN Q.QuestionCode IN ('Q057','Q071','Q072','Q074')
		THEN 'KPIJobPrep'
	END,

	CASE
		WHEN Q.QuestionCode IN ('Q014')
		THEN 'ContactRoles'
		WHEN Q.QuestionCode IN ('Q020')
		THEN 'PersonalIncomeBand'

		WHEN Q.QuestionCode IN ('Q016','Q142')
		THEN 'BusinessStartDate'
		WHEN Q.QuestionCode IN ('Q035')
		THEN 'BusinessRegistered'
		WHEN Q.QuestionCode IN ('Q036')
		THEN 'BusineesRevenueGenerating'
		WHEN Q.QuestionCode IN ('Q100')
		THEN 'BusinessFinancing'
		WHEN Q.QuestionCode IN ('Q139')
		THEN 'BusinessEndDate'

		WHEN Q.QuestionCode IN ('Q015','Q034','Q088','Q129','Q163')
		THEN 'EmploymentStartDate'
		WHEN Q.QuestionCode IN ('Q089','Q105','Q130')
		THEN 'Employer'
		WHEN Q.QuestionCode IN ('Q090','Q106''Q168')
		THEN 'JobTitle'
		WHEN Q.QuestionCode IN ('Q103')
		THEN 'HadPreviosJob'
		WHEN Q.QuestionCode IN ('Q127')
		THEN 'WorkedThreeMonths'
		WHEN Q.QuestionCode IN ('Q128')
		THEN 'EmploymentEndDate'
		WHEN Q.QuestionCode IN ('Q180')
		THEN 'JobType'

		WHEN Q.QuestionCode IN ('Q057')
		THEN 'CareerPath'
		WHEN Q.QuestionCode IN ('Q071')
		THEN 'WritingResume'
		WHEN Q.QuestionCode IN ('Q072')
		THEN 'JobSearch'
		WHEN Q.QuestionCode IN ('Q074')
		THEN 'InterviewingSkills'
	END

FROM form.Question Q
WHERE Q.QuestionCode IN ('Q014','Q015','Q016','Q020','Q034','Q035','Q036','Q057','Q071','Q072','Q074','Q088','Q089','Q090','Q100','Q103','Q105','Q106','Q127','Q128','Q129','Q130','Q139','Q142','Q163','Q168','Q180')
GO
--End table form.KPIQuestion

--Begin table form.KPIContactDetail
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIContactDetail')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIContactDetail'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIContactDetail
		(
		KPIContactDetailID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID VARCHAR(50),
		ContactID VARCHAR(50),
		PersonalIncomeBand VARCHAR(50),
		ContactRoles VARCHAR(MAX)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIContactDetail', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIContactDetailID'

	END
--ENDIF
GO
--End table form.KPIContactDetail

--Begin table form.KPIEnterprise
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIEnterprise')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIEnterprise'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIEnterprise
		(
		KPIEnterpriseID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		BusinessRevenueGenerating VARCHAR(50),
		BusinessEndDate DATE,
		BusinessFinancing VARCHAR(50),
		BusinessRegistered VARCHAR(50),
		BusinessStartDate DATE
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIEnterprise', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIEnterpriseID'

	END
--ENDIF
GO
--End table form.KPIEnterprise

--Begin table form.KPIJobPlacement
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIJobPlacement')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIJobPlacement'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIJobPlacement
		(
		KPIJobPlacementID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		Employer NVARCHAR(MAX),
		EmploymentEndDate DATE,
		EmploymentStartDate DATE,
		HadPreviosJob VARCHAR(50),
		JobTitle NVARCHAR(MAX),
		JobType VARCHAR(50),
		WorkedThreeMonths VARCHAR(50)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIJobPlacement', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIJobPlacementID'

	END
--ENDIF
GO
--End table form.KPIJobPlacement

--Begin table form.KPIJobPrep
IF NOT EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'KPIJobPrep')
	BEGIN

	DECLARE @TableName VARCHAR(250) = 'form.KPIJobPrep'

	EXEC utility.DropObject @TableName

	CREATE TABLE form.KPIJobPrep
		(
		KPIJobPrepID INT NOT NULL IDENTITY(1,1),
		FormSubmitDate DATE,
		FormCode VARCHAR(50),
		ProgramID INT,
		ContactID INT,
		CareerPath VARCHAR(50),
		InterviewingSkills VARCHAR(50),
		JobSearch VARCHAR(50),
		WritingResume VARCHAR(50)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'FormSubmitDate', 'DATE', 'getDate()'
	EXEC utility.SetDefaultConstraint @TableName, 'ProgramID', 'INT', 0

	EXEC utility.SetIndexClustered @TableName, 'IX_KPIJobPrepID', 'ProgramID,ContactID'
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KPIJobPrepID'

	END
--ENDIF
GO
--End table form.KPIJobPrep
