USE EFE
GO

DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
FROM dbo.MenuItem MI

;
WITH HD (DisplayIndex,Lineage,MenuItemID,ParentMenuItemID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
		CAST(MI.MenuItemText AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		1
	FROM dbo.MenuItem MI
	WHERE MI.ParentMenuItemID = 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.Lineage  + ' > ' + MI.MenuItemText AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM dbo.MenuItem MI
		JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
	)

SELECT
	HD1.DisplayIndex,
	MI.DisplayOrder,
	HD1.NodeLevel,
	HD1.ParentMenuItemID,
	HD1.MenuItemID,
	HD1.Lineage,
	MI2.MenuItemCode AS ParentMenuItemCode,
	MI.MenuItemCode,
	MI.MenuItemText,
	MI.MenuItemLink,
	MI.Icon,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
	LEFT JOIN dbo.MenuItem MI2 ON MI2.MenuItemID = MI.ParentMenuItemID
WHERE MI.IsActive = 1
ORDER BY HD1.DisplayIndex

--select * from dbo.MenuItem MI order by 4


/*
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='GrantManagement', @NewMenuItemText='Grants Management', @AfterMenuItemCode='Dashboard', @Icon='fa fa-fw fa-book'
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='GrantManagement', @NewMenuItemCode='ProposalList', @Icon=''
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='GrantManagement', @AfterMenuItemCode='ProposalList', @NewMenuItemCode='AwardList', @Icon=''
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProgramManagement', @NewMenuItemText='Program Management', @AfterMenuItemCode='GrantManagement', @Icon='fa fa-fw fa-line-chart'
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @NewMenuItemCode='SubAwardList', @Icon=''
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='ProgramManagement', @AfterMenuItemCode='SubAwardList', @NewMenuItemCode='AwardList', @Icon=''

@AfterMenuItemCode, 
@BeforeMenuItemCode, 
@DeleteMenuItemCode, 
@Icon, 
@IsActive, 
@NewMenuItemCode, 
@NewMenuItemLink, 
@NewMenuItemText, 
@ParentMenuItemCode, 
@PermissionableLineageList
*/