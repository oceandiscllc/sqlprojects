WITH D AS
	(
	SELECT 
		MAX(C.[Test Date]) AS ConcussionTestDate,
		S.StudentID,
		[dbo].[GetStudentNameByStudentID](S.StudentID, 'LastFirst') AS StudentName
	FROM dbo.Concussion C
		JOIN dbo.Student S ON [dbo].[GetStudentNameByStudentID](S.StudentID, 'LastFirst') = LTRIM(RTRIM(C.Name))
		AND S.BirthDate = C.[Date Of Birth]
	GROUP BY S.StudentID
	)

UPDATE S
SET S.ConcussionTestDate = D.ConcussionTestDate
FROM dbo.Student S
	JOIN D ON D.StudentID = S.StudentID
