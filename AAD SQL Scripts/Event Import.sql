USE AAD
GO

DELETE FROM dbo.Event WHERE EventID > 671
DELETE FROM dbo.EventCalendarType WHERE EventID > 671
GO

DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.Event'

EXEC Utility.AddColumn @cTableName, 'ESJEventID', 'INT'
GO

DECLARE @cCalendarTypeIDList VARCHAR(250)
DECLARE @nEventID INT

DECLARE @tOutput TABLE (EventID INT NOT NULL PRIMARY KEY, ESJEventID INT)

INSERT INTO dbo.Event
	(ESJEventID,EventName,EventDate,EventStartTime,EventEndTime,LocationID,IsAllDayEvent,IsWebApproved,EventPointOfContact,EventDescription,RequestorEmail,RequestorName,RequestorPhone)
OUTPUT INSERTED.EventID, INSERTED.ESJEventID INTO @tOutput
SELECT
	EE.ESJEventID,
	EE.EventName, 
	EE.EventDate, 
	EE.EventStartTime, 
	EE.EventEndTime, 
	EE.LocationID, 
	EE.IsAllDayEvent,
	1, 
	EE.EventPointOfContact, 
	EE.EventDescription, 
	EE.RequestorEmail, 
	EE.RequestorName, 
	EE.RequestorPhone
FROM dbo.ESJEvents EE

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		O.EventID,
		EE.CalendarTypeID
	FROM @tOutput O
		JOIN dbo.ESJEvents EE ON EE.ESJeventID = O.ESJeventID

OPEN oCursor
FETCH oCursor INTO @nEventID, @cCalendarTypeIDList 

WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO dbo.EventCalendarType
		(EventID,CalendarTypeID)
	SELECT
		@nEventID,
		CAST(LTT.ListItem AS INT)
	FROM dbo.ListToTable(@cCalendarTypeIDList, 1, ',') LTT
	
	FETCH oCursor INTO @nEventID, @cCalendarTypeIDList

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	
