USE AAD
GO

SELECT 
	SP.StudentParticipationID, 
	SP.StudentID, 
	SP.SchoolYearStart, 
	SP.SportID, 
	SP.LevelID, 
	SP.TeamID,
	SP.GenderID,
	S.SportName,
	L.LevelID,
	L.LevelName,
	(SELECT T.TeamNameWeb FROM [dbo].[Team] T WHERE T.TeamID = SP.TeamID) AS TeamNameWeb,
	[dbo].[GetStudentNameByStudentID](SP.StudentID, 'LastFirst') AS StudentName,
	ST.GenderID
FROM [aad].[dbo].[StudentParticipation] SP
	JOIN [dbo].[Sport] S ON S.SportID = SP.SportID
	JOIN [dbo].[Level] L ON L.LevelID = SP.LevelID
	JOIN dbo.Student ST ON ST.StudentID = SP.StudentID
		AND SP.SchoolYearStart = 2018
		AND SP.TeamID IN (67,68)
		--AND SP.StudentID = 8852
		--AND SP.SportID = 9
		--AND SP.LevelID = 6
order by ST.GenderID, StudentName, SchoolYearStart


/*
SELECT *
FROM [dbo].[Student] S
WHERE S.LastName IN ('Abbott','Thornton', 'Whelan')

INSERT INTO [aad].[dbo].[StudentParticipation]
	(StudentID, SchoolYearStart, SportID, LevelID, TeamID)
VALUES
	(7738,2010,9,6,26)

UPDATE SA
SET LevelID = 4
FROM [aad].[dbo].[StudentAward] SA
WHERE SA.StudentAwardID = 567

UPDATE SP
SET LevelID = 0
FROM [aad].[dbo].[StudentParticipation] SP
WHERE SP.SchoolYearStart = 2018
		AND SP.TeamID IN (67,68)
		AND SP.SportID = 4

delete from aad.[dbo].[StudentParticipation] where StudentParticipationID = 23454
*/