SELECT 
	'1 Sport' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20112012 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) = 1
	) D
	
UNION

SELECT 
	'2 Sports' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		2 AS Category,
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20112012 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) = 2
	) D
	
UNION

SELECT 
	'3 Sports' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		3 AS Category,
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20112012 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) >= 3
	) D

ORDER BY Category

SELECT
	PD.Sport + ' - ' + G.GenderName,
	COUNT(PD.StudentID) AS ItemCount
FROM dbo.EHSParticipationData20112012 PD
	JOIN dbo.Student S ON S.StudentID = PD.StudentID
	JOIN dbo.Gender G ON G.GenderID = S.GenderID
GROUP BY
	PD.Sport,
	G.GenderName
ORDER BY
	PD.Sport,
	G.GenderName

