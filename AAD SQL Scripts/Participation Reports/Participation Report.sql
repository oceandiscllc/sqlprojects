SELECT 
	COUNT(A.ImportID) AS ItemCount,
	A.GenderCode,
	A.School
FROM
	(
	SELECT 
		S.ImportID,
		G.GenderCode,

		CASE
			WHEN S.Grade < 9
			THEN 'Upper'
			ELSE 'Lower'
		END AS School
		
	FROM dbo.Student S
		JOIN dbo.Gender G ON G.GenderID = S.GenderID
	WHERE EXISTS
		(
		SELECT 1
		FROM dbo.StudentParticipationRaw SPR
		WHERE SPR.SchoolYearStart = 2011
			AND SPR.ImportID = S.ImportID
		)
	) A
GROUP BY A.GenderCode, A.School
ORDER BY A.GenderCode, A.School

SELECT 
	COUNT(A.ImportID) AS ItemCount,
	A.GenderCode,
	A.School
FROM
	(
	SELECT 
		S.ImportID,
		G.GenderCode,

		CASE
			WHEN S.Grade < 9
			THEN 'Upper'
			ELSE 'Lower'
		END AS School
		
FROM dbo.Student S
	JOIN dbo.EHSFinancialAidData20112012 EFAD ON EFAD.StudentID = S.ImportID
	JOIN dbo.Gender G ON G.GenderID = S.GenderID
	) A
GROUP BY A.GenderCode, A.School
ORDER BY A.GenderCode, A.School
GO

DECLARE @nImportID int
DECLARE @cSportName varchar(50)

DECLARE @tTable table (ImportID int, SportList varchar(max))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		SPR.ImportID,
		S.SportName
	FROM dbo.StudentParticipationRaw SPR
		JOIN dbo.Student ST ON ST.ImportID = SPR.ImportID
			AND SPR.SchoolYearStart = 2011
			AND ST.Grade > 0
		JOIN dbo.Sport S ON S.SportCode = SPR.SportCode
	ORDER BY	
		SPR.ImportID,S.SportName

OPEN oCursor
FETCH oCursor INTO @nImportID, @cSportName
WHILE @@fetch_status = 0
	BEGIN

	IF NOT EXISTS (SELECT 1 FROM @tTable T WHERE T.ImportID = @nImportID)
		BEGIN
		
		INSERT INTO @tTable
			(ImportID, SportList)
		VALUES
			(@nImportID, @cSportName)
			
		END
	ELSE
		BEGIN
		
		UPDATE @tTable
		SET SportList = SportList + ', ' + @cSportName
		WHERE ImportID = @nImportID
			AND SportList NOT LIKE '%' + @cSportName + '%'
			
		END
	--ENDIF	

	FETCH oCursor INTO @nImportID, @cSportName
			
	END
--END WHILE
			
CLOSE oCursor
DEALLOCATE oCursor

SELECT 
	COUNT(T.ImportID) AS ItemCount,
	T.SportList
FROM @tTable T
GROUP BY T.SportList
ORDER BY T.SportList, COUNT(T.ImportID)
GO

