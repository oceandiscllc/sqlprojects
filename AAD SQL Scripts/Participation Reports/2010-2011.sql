SELECT 
	'1 Sport' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20102011 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) = 1
	) D
	
UNION

SELECT 
	'2 Sports' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		2 AS Category,
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20102011 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) = 2
	) D
	
UNION

SELECT 
	'3 Sports' AS Category,
	COUNT(D.ItemCount)
FROM
	(
	SELECT 
		3 AS Category,
		COUNT(PD.StudentID) AS ItemCount
	FROM dbo.EHSParticipationData20102011 PD
	GROUP BY PD.StudentID
	HAVING COUNT(PD.StudentID) >= 3
	) D

ORDER BY Category

IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
	DROP TABLE #tTable

CREATE TABLE #tTable 
	(
	Sport varchar(50), 
	Total int NOT NULL DEFAULT 0,
	FAC1 int NOT NULL DEFAULT 0,
	FAC2 int NOT NULL DEFAULT 0,
	FAC3 int NOT NULL DEFAULT 0,
	FAC4 int NOT NULL DEFAULT 0,
	FAC5 int NOT NULL DEFAULT 0,
	GOALT8 int NOT NULL DEFAULT 0,
	GOA8 int NOT NULL DEFAULT 0,
	GOA9 int NOT NULL DEFAULT 0,
	GOAGT9 int NOT NULL DEFAULT 0
	)

INSERT INTO #tTable
	(Sport)
SELECT
	PD.Sport + ' - ' + G.GenderName AS Sport
FROM dbo.EHSParticipationData20102011 PD
	JOIN dbo.Student S ON S.StudentID = PD.StudentID
	JOIN dbo.Gender G ON G.GenderID = S.GenderID

UNION

SELECT
	PD.Sport + ' - ' + G.GenderName AS Sport
FROM dbo.EHSParticipationData20112012 PD
	JOIN dbo.Student S ON S.StudentID = PD.StudentID
	JOIN dbo.Gender G ON G.GenderID = S.GenderID

ORDER BY Sport

UPDATE T
SET T.Total = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.FAC1 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND PD.FinancialAidCode = 1
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.FAC2 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND PD.FinancialAidCode = 2
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.FAC3 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND PD.FinancialAidCode = 3
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.FAC4 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND PD.FinancialAidCode = 4
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.FAC5 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND PD.FinancialAidCode = 5
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.GOALT8 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.GradeOfAdmission < 8
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.GOA8 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.GradeOfAdmission = 8
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.GOA9 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.GradeOfAdmission = 9
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

UPDATE T
SET T.GOAGT9 = D.ItemCount
FROM #tTable T
	JOIN 
		(
		SELECT
			PD.Sport + ' - ' + G.GenderName AS Sport,
			COUNT(PD.StudentID) AS ItemCount
		FROM dbo.EHSParticipationData20102011 PD
			JOIN dbo.Student S ON S.StudentID = PD.StudentID
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.GradeOfAdmission > 9
		GROUP BY
			PD.Sport,
			G.GenderName
		) D ON D.Sport = T.Sport

SELECT *
FROM #tTable