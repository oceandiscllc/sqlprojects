--DELETE SPR
SELECT *
FROM AADNew.dbo.StudentParticipationRaw SPR
WHERE EXISTS
	(
	SELECT 1
	FROM
		(
		SELECT 
			MAX(SP1.StudentParticipationRawID) AS MaxStudentParticipationRawID,
			SP1.StudentID,
			SP1.SchoolYearStart,
			SP1.SchoolYearStop,
			SP1.SportCode,
			SP1.LevelCode,
			SP1.GenderCode
		FROM AADNew.dbo.StudentParticipationRaw SP1
		WHERE EXISTS
			(	
			SELECT 1
			FROM
				(
				SELECT 
					COUNT(StudentParticipationRawID) AS ItemCount,
					StudentID,
					SchoolYearStart,
					SchoolYearStop,
					SportCode,
					LevelCode,
					GenderCode
				FROM AADNew.dbo.StudentParticipationRaw
				GROUP BY 
					StudentID,
					SchoolYearStart,
					SchoolYearStop,
					SportCode,
					LevelCode,
					GenderCode
				HAVING COUNT(StudentParticipationRawID) > 1
				) SP2 
			WHERE SP2.StudentID = SP1.StudentID
				AND SP2.SchoolYearStart = SP1.SchoolYearStart
				AND SP2.SchoolYearStop = SP1.SchoolYearStop
				AND SP2.SportCode = SP1.SportCode
				AND SP2.LevelCode = SP1.LevelCode
				AND SP2.GenderCode = SP1.GenderCode
			)
		GROUP BY 
			SP1.StudentID,
			SP1.SchoolYearStart,
			SP1.SchoolYearStop,
			SP1.SportCode,
			SP1.LevelCode,
			SP1.GenderCode
		) AS D
	WHERE D.MaxStudentParticipationRawID = SPR.StudentParticipationRawID
	)


