SELECT 
	SA.SchoolYearStart,
	SA.StudentID,
	dbo.GetStudentNameByStudentID(SA.StudentID, 'LastFirstPreferred') AS StudentName,
	S2.SportName,
	L.LevelName,
	A.Awardname
FROM dbo.StudentAward SA
	JOIN dbo.Student S1 ON S1.StudentID = SA.StudentID
	JOIN dbo.Sport S2 ON S2.SportID = SA.SportID
	JOIN dbo.Level L ON L.LevelID = SA.LevelID
	JOIN dbo.Gender G ON G.GenderID = S1.GenderID
	JOIN dbo.Award A ON A.AwardID = SA.AwardID
		AND SportName LIKE '%cheer%'
ORDER BY SA.SchoolYearStart, S2.SportName, L.LevelName, StudentName
