USE AAD
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Begin procedure Utility.DropColumn
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Utility.DropColumn') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE Utility.DropColumn
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2012.07.27
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropColumn
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC WITH (NOLOCK)
			JOIN sys.columns C ON C.column_id = DC.Parent_Column_ID
				AND DC.parent_object_ID = OBJECT_ID(@cTableName)
				AND C.Name = @cColumnName
		ORDER BY DC.Name		

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name = @cColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @cTableName + ' DROP COLUMN ' + @cColumnName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID('dbo.Person') AND SC.name = 'POSM1CompletionDate')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'sp_RENAME ''Person.POSM1CompletionDate'', ''POSMCompletionDate'', ''COLUMN'''
	EXEC (@cSQL)
	
	END
--ENDIF
GO

EXEC Utility.AddColumn 'dbo.Person', 'AthleticHandbookDate', 'date'
EXEC Utility.AddColumn 'dbo.Person', 'FHSAAConcussionCourseCompletionDate', 'date'
EXEC Utility.AddColumn 'dbo.Person', 'FingerprintsDate', 'date'
EXEC Utility.AddColumn 'dbo.Person', 'HasCDLLiscense', 'bit'
EXEC Utility.AddColumn 'dbo.Person', 'HasDrugPolicy', 'bit'
EXEC Utility.AddColumn 'dbo.Person', 'HasSRCovenant', 'bit'
EXEC Utility.AddColumn 'dbo.Person', 'HasVECHSForm', 'bit'
EXEC Utility.AddColumn 'dbo.Person', 'I9VerificationDate', 'date'
GO

EXEC Utility.SetDefault 'dbo.Person', 'HasCDLLiscense', 0
EXEC Utility.SetDefault 'dbo.Person', 'HasDrugPolicy', 0
EXEC Utility.SetDefault 'dbo.Person', 'HasSRCovenant', 0
EXEC Utility.SetDefault 'dbo.Person', 'HasVECHSForm', 0
GO

EXEC Utility.SetColumnNotNull 'dbo.Person', 'HasCDLLiscense', 'bit'
EXEC Utility.SetColumnNotNull 'dbo.Person', 'HasDrugPolicy', 'bit'
EXEC Utility.SetColumnNotNull 'dbo.Person', 'HasSRCovenant', 'bit'
EXEC Utility.SetColumnNotNull 'dbo.Person', 'HasVECHSForm', 'bit'
GO

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'AthleticHandbookDateFormatted')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','AthleticHandbookDateFormatted','Athletic Handbook Date')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'FHSAAConcussionCourseCompletionDate')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FHSAAConcussionCourseCompletionDateFormatted','FHSAA Concussion Course Date')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'FingerprintsDateFormatted')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FingerprintsDateFormatted','Fingerprints Date')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'HasCDLLiscense')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasCDLLiscense','CDL Liscense')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'HasDrugPolicy')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasDrugPolicy','Drug Policy')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'HasSRCovenant')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasSRCovenant','SR Covenant')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'HasVECHSForm')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasVECHSForm','VECHS Form')

IF NOT EXISTS (SELECT 1 FROM Utility.DataDictionary DD WHERE DD.EntityTypeCode = 'Person' AND DD.ColumnName = 'I9VerificationDateFormatted')
	INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','I9VerificationDateFormatted','I-9 Form Date')
GO

UPDATE Utility.DataDictionary
SET 
	ColumnName = 'POSMCompletionDateFormatted',
	DisplayName = 'POSM Completion Date'
WHERE ColumnName LIKE 'POSM1%'
GO

DELETE
FROM Utility.DataDictionary
WHERE EntityTypeCode = 'Person'
	AND
		(
		ColumnName LIKE 'ClassCDLExpirationDate%'
			OR ColumnName LIKE 'HasBackgroundCheck%'
			OR ColumnName LIKE 'HasCoachGuidelines%'
			OR ColumnName LIKE 'HasFingerPrints%'
			OR ColumnName LIKE 'POSM2%'
			OR ColumnName LIKE 'POSM3%'
		)
GO

EXEC Utility.DropColumn 'dbo.Person', 'ClassCDLExpirationDate'
EXEC Utility.DropColumn 'dbo.Person', 'HasBackgroundCheck'
EXEC Utility.DropColumn 'dbo.Person', 'HasCoachGuidelines'
EXEC Utility.DropColumn 'dbo.Person', 'HasFingerPrints'
EXEC Utility.DropColumn 'dbo.Person', 'POSM2CompletionDate'
EXEC Utility.DropColumn 'dbo.Person', 'POSM3CompletionDate'
GO