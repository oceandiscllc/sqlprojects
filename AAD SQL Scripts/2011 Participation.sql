SELECT 
	SP.StudentID,
	dbo.GetStudentNameByStudentID(SP.StudentID, 'LastFirstPreferred') AS StudentName,
	G.GenderCode AS StudentGender,
	S2.SportName,
	L.LevelName
FROM dbo.StudentParticipation SP
	JOIN dbo.Student S1 ON S1.StudentID = SP.StudentID
	JOIN dbo.Sport S2 ON S2.SportID = SP.SportID
	JOIN dbo.Level L ON L.LevelID = SP.LevelID
	JOIN dbo.Gender G ON G.GenderID = S1.GenderID
		AND SP.SchoolYearStart = 2011
		AND EXISTS
			(
			SELECT 1
			FROM dbo.Team T
			WHERE T.LevelID = 0
				AND T.SportID = SP.SportID
			)
ORDER BY 
	S2.SportName,
	L.DisplayOrder,
	G.GenderCode,
	StudentName