USE AAD
GO

DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.CalendarType'

EXEC Utility.AddColumn @cTableName, 'IsExternalDefault', 'BIT'
EXEC Utility.SetDefault @cTableName, 'IsExternalDefault', 0
EXEC Utility.SetColumnNotNull @cTableName, 'IsExternalDefault', 'BIT'
