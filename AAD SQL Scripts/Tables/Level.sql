DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Level'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Level
	
CREATE TABLE dbo.Level
	(
	LevelID int IDENTITY(0,1) NOT NULL,
	LevelCode varchar(10),
	LevelName varchar(50),
	DisplayOrder int,	
	IsActive bit,
	OldID int
	)
	
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LevelID'
EXEC Utility.SetIndexNonClustered 'IX_Level', @cTableName, 'DisplayOrder ASC,LevelName ASC'
GO

SET IDENTITY_INSERT dbo.Level ON
INSERT INTO dbo.Level (LevelID,DisplayOrder) VALUES (0,99)
SET IDENTITY_INSERT dbo.Level OFF
GO

INSERT INTO dbo.Level 
	(OldID,LevelCode,LevelName,DisplayOrder)
SELECT 
	LevelID,
	LevelCode,
	LevelName,
	ISNULL(DisplaySequence,0)
FROM
	(
	SELECT 
		O.ObjectID AS LevelID, 
		'LevelName' AS AttributeName, 
		O.ObjectName AS AttributeValue
	FROM AAD.AAD1001.Objects O
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Level'

	UNION

	SELECT 
		A.ObjectID AS LevelID, 
		A.AttributeName, 
		A.AttributeValue
	FROM AAD.AAD1001.Attributes A
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Level'
			AND A.AttributeName IN ('LevelCode','LevelName','DisplaySequence')
	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (LevelName,LevelCode,DisplaySequence)
	) AS PVT
GO

UPDATE dbo.Level
SET DisplayOrder = 99
GO

UPDATE dbo.Level
SET DisplayOrder = 1
WHERE LevelCode = 'V'
GO

UPDATE dbo.Level
SET DisplayOrder = 2
WHERE LevelCode = 'JV'
GO

UPDATE dbo.Level
SET DisplayOrder = 3
WHERE LevelCode = 'F'
GO

UPDATE dbo.Level
SET DisplayOrder = 4
WHERE LevelCode = 'JRH'
GO

UPDATE dbo.Level
SET DisplayOrder = 5
WHERE LevelCode = 'MS'
GO

UPDATE dbo.Level
SET DisplayOrder = 6
WHERE LevelCode = '8'
GO

UPDATE dbo.Level
SET DisplayOrder = 7
WHERE LevelCode = '7'
GO

UPDATE dbo.Level
SET DisplayOrder = 8
WHERE LevelCode = '6/7'
GO

UPDATE dbo.Level
SET DisplayOrder = 9
WHERE LevelCode = '6'
GO
