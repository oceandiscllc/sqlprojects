DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Person'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Person

CREATE TABLE dbo.Person
	(
	PersonID int IDENTITY(0,1) NOT NULL,
	FirstName varchar(50),
	LastName varchar(50),
	Title varchar(10),
	EmailAddress varchar(320),
	HomePhone varchar(50),
	CellPhone varchar(50),
	WorkPhone varchar(50),
	MailAddressLine1 varchar(50),
	MailAddressLine2 varchar(50),
	MailCity varchar(50),
	MailState varchar(2),
	MailZip varchar(50),
	Login varchar(50),
	Password varchar(50),
	AEDExpirationDate date,
	ClassCDLExpirationDate date,
	CPRExpirationDate date,
	FirstAidExpirationDate date,
	HasBackgroundCheck bit,
	HasCoachGuidelines bit,
	HasFingerPrints bit,
	HasW4 bit,
	IsAdjunct bit,
	POSM1CompletionDate date,
	POSM2CompletionDate date,
	POSM3CompletionDate date,
	StartDate date,
	FallContractIssueDate date,
	FallContractReturnDate date,
	WinterContractIssueDate date,
	WinterContractReturnDate date,
	SpringContractIssueDate date,
	SpringContractReturnDate date,
	SummerContractIssueDate date,
	SummerContractReturnDate date
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'HasBackgroundCheck', 0
EXEC Utility.SetDefault @cTableName, 'HasCoachGuidelines', 0
EXEC Utility.SetDefault @cTableName, 'HasFingerPrints', 0
EXEC Utility.SetDefault @cTableName, 'HasW4', 0
EXEC Utility.SetDefault @cTableName, 'IsAdjunct', 0

EXEC Utility.SetColumnNotNull @cTableName, 'HasBackgroundCheck', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'HasCoachGuidelines', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'HasFingerPrints', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'HasW4', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsAdjunct', 'bit'
	
EXEC Utility.SetPrimaryKeyClustered @cTableName, 'PersonID'
EXEC Utility.SetIndexNonClustered 'IX_Person', @cTableName, 'LastName ASC,FirstName ASC'
GO

SET IDENTITY_INSERT dbo.Person ON

INSERT INTO dbo.Person 
	(PersonID,FirstName,LastName,EmailAddress,HomePhone,CellPhone,WorkPhone,MailAddressLine1,MailAddressLine2,MailCity,MailState,MailZip,Login,Password)
SELECT 
	P.PersonID,
	P.FirstName,
	P.LastName,
	P.EmailAddress,
	P.HomePhone,
	P.CellPhone,
	P.WorkPhone,
	P.MailAddressLine1,
	P.MailAddressLine2,
	P.MailCity,
	P.MailState,
	P.MailZip,
	P.Login,
	P.Password
FROM AAD.AAD1001.People P
GO

SET IDENTITY_INSERT dbo.Person OFF
GO

UPDATE P
SET 
	P.AEDExpirationDate = CASE WHEN LEN(RTRIM(E.AED)) = 10 THEN CAST(E.AED as date) ELSE NULL END,
	P.ClassCDLExpirationDate = CASE WHEN LEN(RTRIM(E.ClassC)) = 10 THEN CAST(E.ClassC as date) ELSE NULL END,
	P.CPRExpirationDate = CASE WHEN LEN(RTRIM(E.CPR)) = 10 THEN CAST(E.CPR as date) ELSE NULL END,
	P.FirstAidExpirationDate = CASE WHEN LEN(RTRIM(E.FirstAid)) = 10 THEN CAST(E.FirstAid as date) ELSE NULL END,
	P.HasBackgroundCheck = CASE WHEN E.BackgroundCheck = '1' THEN 1 ELSE 0 END,
	P.HasCoachGuidelines = CASE WHEN E.CoachGuidelines = '1' THEN 1 ELSE 0 END,
	P.HasFingerPrints = CASE WHEN E.FingerPrints = '1' THEN 1 ELSE 0 END,
	P.HasW4 = CASE WHEN E.W4 = '1' THEN 1 ELSE 0 END,
	P.IsAdjunct = CASE WHEN E.Adjunct = '1' THEN 1 ELSE 0 END,
	P.POSM1CompletionDate = CASE WHEN LEN(RTRIM(E.POSM1)) = 10 THEN CAST(E.POSM1 as date) ELSE NULL END,
	P.POSM2CompletionDate = CASE WHEN LEN(RTRIM(E.POSM2)) = 10 THEN CAST(E.POSM2 as date) ELSE NULL END,
	P.POSM3CompletionDate = CASE WHEN LEN(RTRIM(E.POSM3)) = 10 THEN CAST(E.POSM3 as date) ELSE NULL END,
	P.StartDate = CASE WHEN LEN(RTRIM(E.StartDate)) = 10 THEN CAST(E.StartDate as date) ELSE NULL END,
	P.FallContractIssueDate = CASE WHEN LEN(RTRIM(E.ContractIssueDateFall)) = 10 THEN CAST(E.ContractIssueDateFall as date) ELSE NULL END,
	P.FallContractReturnDate = CASE WHEN LEN(RTRIM(E.ContractReturnDateFall)) = 10 THEN CAST(E.ContractReturnDateFall as date) ELSE NULL END,
	P.WinterContractIssueDate = CASE WHEN LEN(RTRIM(E.ContractIssueDateSpring)) = 10 THEN CAST(E.ContractIssueDateSpring as date) ELSE NULL END,
	P.WinterContractReturnDate = CASE WHEN LEN(RTRIM(E.ContractReturnDateWinter)) = 10 THEN CAST(E.ContractReturnDateWinter as date) ELSE NULL END,
	P.SpringContractIssueDate = CASE WHEN LEN(RTRIM(E.ContractIssueDateWinter)) = 10 THEN CAST(E.ContractIssueDateWinter as date) ELSE NULL END,
	P.SpringContractReturnDate = CASE WHEN LEN(RTRIM(E.ContractReturnDateSpring)) = 10 THEN CAST(E.ContractReturnDateSpring as date) ELSE NULL END,
	P.SummerContractIssueDate = CASE WHEN LEN(RTRIM(E.ContractIssueDateSummer)) = 10 THEN CAST(E.ContractIssueDateSummer as date) ELSE NULL END,
	P.SummerContractReturnDate = CASE WHEN LEN(RTRIM(E.ContractReturnDateSummer)) = 10 THEN CAST(E.ContractReturnDateSummer as date) ELSE NULL END
FROM dbo.Person P
	JOIN
		(
		SELECT *
		FROM
			(
			SELECT
				P.PersonID,
				A.AttributeName, 
				A.AttributeValue
			FROM AAD.AAD1001.Attributes A
				JOIN AADNew.dbo.Person P ON P.PersonID = A.ObjectID
					AND P.PersonID > 100

			) AS PD
		PIVOT
			( 
			MAX(PD.AttributeValue)
			FOR PD.AttributeName IN (Adjunct,AED,BackgroundCheck,ClassC,CoachGuidelines,ContractIssueDateFall,ContractIssueDateSpring,ContractIssueDateSummer,ContractIssueDateWinter,ContractReturnDateFall,ContractReturnDateSpring,ContractReturnDateSummer,ContractReturnDateWinter,CPR,FingerPrints,FirstAid,POSM1,POSM2,POSM3,RulesMeeting,StartDate,W4)
			) AS D
		) E ON E.PersonID = P.PersonID
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.PersonTeam'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.PersonTeam
	
CREATE TABLE dbo.PersonTeam
	(
	PersonTeamID int IDENTITY(1,1) NOT NULL,
	PersonID int,
	TeamID int,
	RelationshipCode varchar(50)
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'PersonID', 0
EXEC Utility.SetDefault @cTableName, 'TeamID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'PersonID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TeamID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'PersonTeamID'
EXEC Utility.SetIndexClustered 'IX_PersonTeam', @cTableName, 'PersonID ASC,TeamID ASC'
GO