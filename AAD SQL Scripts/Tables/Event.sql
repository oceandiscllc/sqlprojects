DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Event'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN

	DROP TABLE dbo.Event
	
	END
--ENDIF
	
CREATE TABLE dbo.Event
	(
	EventID int IDENTITY(1,1) NOT NULL,
	EventName varchar(500),
	EventDate date,
	EventStartTime time,
	EventEndTime time,
	LocationID int,
	IsAllDayEvent bit,
	IsCanceled bit,
	IsTransportationRequired bit,
	IsWebApproved bit,
	OldID int
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsAllDayEvent', 0
EXEC Utility.SetDefault @cTableName, 'IsCanceled', 0
EXEC Utility.SetDefault @cTableName, 'IsTransportationRequired', 0
EXEC Utility.SetDefault @cTableName, 'IsWebApproved', 0
EXEC Utility.SetDefault @cTableName, 'LocationID', 0
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'IsAllDayEvent', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsCanceled', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsTransportationRequired', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsWebApproved', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'LocationID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'EventID'
EXEC Utility.SetIndexNonClustered 'IX_Event', @cTableName, 'EventDate ASC, EventStartTime ASC, EventID ASC'
GO

INSERT INTO dbo.Event
	(OldID,EventName,EventDate,EventStartTime,EventEndTime,IsWebApproved,LocationID,IsAllDayEvent)
SELECT 
	E.EventID,
	E.EventName,
	E.EventDate,
	
	CASE
		WHEN ISDATE(E.StartTime) = 1
		THEN E.StartTime
		ELSE NULL
	END,
	
	CASE
		WHEN ISDATE(E.EndTime) = 1
		THEN E.EndTime
		ELSE NULL
	END,

	E.IsWebApproved,
	L.LocationID,
	
	CASE
		WHEN ISDATE(E.StartTime) = 1 AND dbo.TimeFormat(E.StartTime) = '12:00 AM' AND ISDATE(E.EndTime) = 1 AND dbo.TimeFormat(E.EndTime) = '12:00 AM'
		THEN 1
		ELSE 0
	END
	
FROM 
	(
	SELECT 
		A1.ObjectID AS EventID, 
		A1.AttributeName, 
		A1.AttributeValue
	FROM AAD.AAD1001.Attributes A1
		JOIN AAD.AAD1001.Objects O1 ON O1.ObjectID = A1.ObjectID
			AND O1.EffectiveDate >= 20110701
		JOIN AAD.AAD.Types T1 ON T1.TypeID = O1.ObjectTypeID
			AND T1.TypeCategory = 'Object'
			AND T1.TypeName = 'Event'

	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (EndTime,EventDate,Eventname,IsWebApproved,StartTime)) AS E
JOIN AAD.AAD1001.Associations A3 ON A3.Node1ID = E.EventID
	JOIN AAD.AAD.Types T3 ON T3.TypeID = A3.AssociationTypeID
	AND T3.TypeCategory = 'ObjectAssociation'
	AND T3.TypeName = 'Object - Venue'
JOIN AAD.AAD1001.Objects O3 ON O3.ObjectID = A3.Node2ID
JOIN dbo.Location L ON L.OldID = A3.Node2ID

ORDER BY EventDate, StartTime, EventName, EventID
GO

UPDATE AADNew.dbo.Event
SET EventStartTime = NULL
WHERE EventStartTime = '00:00:00.0000000'
GO

UPDATE AADNew.dbo.Event
SET EventEndTime = NULL
WHERE EventEndTime = '00:00:00.0000000'
GO

UPDATE dbo.Event
SET
	EventStartTime = NULL,
	EventEndTime = NULL
WHERE IsAllDayEvent = 1
GO
