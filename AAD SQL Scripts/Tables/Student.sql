DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Student'

IF NOT (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.Student
		(
		StudentID int,
		FirstName varchar(50),
		MiddleName varchar(50),
		LastName varchar(50),
		Suffix varchar (10),
		PreferredName varchar(50),
		FatherEmail varchar(320),
		MotherEmail varchar(320),
		StudentEmail varchar(320),
		MailName varchar(100),
		MailAddressLine1 varchar(50),
		MailAddressLine2 varchar(50),
		MailCity varchar(50),
		MailState varchar(2),
		MailZip varchar(50),
		Phone varchar(20),
		BirthDate date,
		Grade int,
		GradeOfAdmission int,
		YearOfAdmission int,
		YearOfGraduation int,
		CameFromFeederSchool bit,
		GenderID int,
		FatherPreferredName varchar(50),
		MotherPreferredName varchar(50),
		ParentSalutation varchar(100),
		FinancialAidCode int,
		HasConsent bit,
		HasPhysical bit,
		IsActive bit
		)

	END
--ENDIF
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CameFromFeederSchool', 0
EXEC Utility.SetDefault @cTableName, 'FinancialAidCode', 0
EXEC Utility.SetDefault @cTableName, 'GenderID', 0
EXEC Utility.SetDefault @cTableName, 'Grade', 0
EXEC Utility.SetDefault @cTableName, 'GradeOfAdmission', 0
EXEC Utility.SetDefault @cTableName, 'HasConsent', 0
EXEC Utility.SetDefault @cTableName, 'HasPhysical', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'YearOfAdmission', 0
EXEC Utility.SetDefault @cTableName, 'YearOfGraduation', 0

EXEC Utility.SetColumnNotNull @cTableName, 'CameFromFeederSchool', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'FinancialAidCode', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'GenderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'Grade', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'GradeOfAdmission', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'HasConsent', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'HasPhysical', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'StudentID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'YearOfAdmission', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'YearOfGraduation', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'StudentID'
EXEC Utility.SetIndexNonClustered 'IX_Student', @cTableName, 'IsActive DESC,LastName ASC,FirstName ASC'
GO