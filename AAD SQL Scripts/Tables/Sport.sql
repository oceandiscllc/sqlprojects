DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Sport'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Sport
	
CREATE TABLE dbo.Sport
	(
	SportID int IDENTITY(0,1) NOT NULL,
	SportCode varchar(10),
	SportName varchar(50),
	DisplayOrder int,	
	ContestResultTypeID int,
	IsActive bit,
	OldID int
	)
	
EXEC Utility.SetDefault @cTableName, 'ContestResultTypeID', 1
EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestResultTypeID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SportID'
EXEC Utility.SetIndexNonClustered 'IX_Sport', @cTableName, 'DisplayOrder ASC,SportName ASC'
GO

SET IDENTITY_INSERT dbo.Sport ON
INSERT INTO dbo.Sport (SportID) VALUES (0)
SET IDENTITY_INSERT dbo.Sport OFF
GO

INSERT INTO dbo.Sport 
	(OldID,SportCode,SportName,ContestResultTypeID)
SELECT 
	PVT.SportID,
	PVT.SportCode,
	PVT.SportName,
	
	CASE
		WHEN SportCode IN ('BA','BB','FB','FF','LC','SC','SB','VB','WP')
		THEN 1
		WHEN SportCode IN ('CH')
		THEN 3
		ELSE 2
	END AS ContestResultTypeID
	
FROM
	(
	SELECT 
		O.ObjectID AS SportID, 
		'SportName' AS AttributeName, 
		O.ObjectName AS AttributeValue
	FROM AAD.AAD1001.Objects O
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Sport'

	UNION

	SELECT 
		A.ObjectID AS SportID, 
		A.AttributeName, 
		A.AttributeValue
	FROM AAD.AAD1001.Attributes A
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Sport'
			AND A.AttributeName IN ('SportCode','SportName')
	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (SportName,SportCode)
	) AS PVT
ORDER BY PVT.SportName
GO
	
INSERT INTO dbo.Sport 
	(OldID,SportCode,SportName,ContestResultTypeID)
SELECT
	0 AS SportID,
	'DV' AS SportCode,
	'Diving' AS SportName,
	2 AS ContestResultTypeID
GO