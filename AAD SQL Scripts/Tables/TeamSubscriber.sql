DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.TeamSubscriber'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.TeamSubscriber
	
CREATE TABLE dbo.TeamSubscriber
	(
	TeamSubscriberID int IDENTITY(1,1) NOT NULL,
	TeamID int,
	EmailAddress varchar(320)
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'TeamID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'TeamID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TeamSubscriberID'
GO