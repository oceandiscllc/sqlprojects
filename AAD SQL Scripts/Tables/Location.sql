DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Location'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Location
	
CREATE TABLE dbo.Location
	(
	LocationID int IDENTITY(0,1) NOT NULL,
	LocationName varchar(250),
	MailName varchar(250),
	AddressLine1 varchar(50),
	AddressLine2 varchar(50),
	City varchar(50),
	State varchar(2),
	Zip varchar(10),
	IsOpponent bit,
	IsOwnFacility bit,
	IsActive bit,
	OldID int
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'IsOpponent', 0
EXEC Utility.SetDefault @cTableName, 'IsOwnFacility', 0
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsOpponent', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsOwnFacility', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'LocationID'
EXEC Utility.SetIndexNonClustered 'IX_Location', @cTableName, 'LocationName ASC'
GO

SET IDENTITY_INSERT dbo.Location ON
INSERT INTO dbo.Location (LocationID) VALUES (0)
SET IDENTITY_INSERT dbo.Location OFF
GO

INSERT INTO dbo.Location
	(LocationName,AddressLine1,AddressLine2,City,State,Zip,IsOwnFacility,OldID)
SELECT
	ObjectName,	
	PhysicalAddressLine1,	
	PhysicalAddressLine2,	
	PhysicalCity,	
	PhysicalState,	
	PhysicalZip,
	ISNULL(IsOwnFacility,	0),
	ObjectID
FROM
	(
	SELECT 
		O.ObjectName,
		PVT.*
	FROM 
		(
		SELECT 
			A.ObjectID, 
			A.AttributeName, 
			A.AttributeValue
		FROM AAD.AAD1001.Attributes A
			JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
			JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
				AND T.TypeCategory = 'Object'
				AND T.TypeName = 'Facility'
	
		UNION

		SELECT 
			O.ObjectID, 
			O.ObjectName,
			''
		FROM AAD.AAD1001.Objects O
			JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
				AND T.TypeCategory = 'Object'
				AND T.TypeName = 'Facility'
		) AS OA
	PIVOT
		( 
		MAX(OA.AttributeValue)
		FOR OA.AttributeName IN 
			(
			IsActive,IsOwnFacility,PhysicalAddressLine1,PhysicalAddressLine2,PhysicalCity,PhysicalState,PhysicalZip
			)
		) AS PVT
	JOIN AAD.AAD1001.Objects O ON O.ObjectID = PVT.ObjectID
	) D
ORDER BY ObjectName, ObjectID
GO

INSERT INTO dbo.Location
	(LocationName,MailName,AddressLine1,AddressLine2,City,State,Zip,IsOpponent,OldID)
SELECT
	ObjectName,	
	LegalName,
	PhysicalAddressLine1,	
	PhysicalAddressLine2,	
	PhysicalCity,	
	PhysicalState,	
	PhysicalZip,
	1,	
	ObjectID
FROM
	(
	SELECT 
		O.ObjectName,
		PVT.*
	FROM 
		(
		SELECT 
			A.ObjectID, 
			A.AttributeName, 
			A.AttributeValue
		FROM AAD.AAD1001.Attributes A
			JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
			JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
				AND T.TypeCategory = 'Object'
				AND T.TypeName = 'ContestOpponent'
	
		UNION

		SELECT 
			O.ObjectID, 
			O.ObjectName,
			''
		FROM AAD.AAD1001.Objects O
			JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
				AND T.TypeCategory = 'Object'
				AND T.TypeName = 'ContestOpponent'
		) AS OA
	PIVOT
		( 
		MAX(OA.AttributeValue)
		FOR OA.AttributeName IN 
			(
			CommonName,LegalName,IsActive,PhysicalAddressLine1,PhysicalAddressLine2,PhysicalCity,PhysicalState,PhysicalZip
			)
		) AS PVT
	JOIN AAD.AAD1001.Objects O ON O.ObjectID = PVT.ObjectID
	) D
ORDER BY CommonName, ObjectID

