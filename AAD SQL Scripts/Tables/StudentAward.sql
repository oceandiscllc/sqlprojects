DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.StudentAward'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.StudentAward
	
CREATE TABLE dbo.StudentAward
	(
	StudentAwardID int IDENTITY(1,1),
	StudentID int,
	SchoolYearStart int,
	SchoolYearStop int,
	SportID int,
	LevelID int,
	GenderID int,
	AwardID int
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'GenderID', 0
EXEC Utility.SetDefault @cTableName, 'LevelID', 0
EXEC Utility.SetDefault @cTableName, 'SchoolYearStart', 0
EXEC Utility.SetDefault @cTableName, 'SchoolYearStop', 0
EXEC Utility.SetDefault @cTableName, 'SportID', 0
EXEC Utility.SetDefault @cTableName, 'StudentID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'GenderID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LevelID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'SchoolYearStart', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'SchoolYearStop', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'SportID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'StudentID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'StudentAwardID'
GO