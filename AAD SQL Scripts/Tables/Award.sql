DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Award'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Award
	
CREATE TABLE dbo.Award
	(
	AwardID int IDENTITY(0,1) NOT NULL,
	AwardName varchar(50),
	AwardTypeCode varchar(50),
	IsActive bit
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'AwardID'
EXEC Utility.SetIndexNonClustered 'IX_Award', @cTableName, 'AwardName ASC'
GO

SET IDENTITY_INSERT dbo.Award ON
INSERT INTO dbo.Award (AwardID) VALUES (0)
SET IDENTITY_INSERT dbo.Award OFF
GO

INSERT INTO dbo.Award (AwardName,AwardTypeCode) VALUES ('Celeste Hampton Distinguished Character Award','Student')
INSERT INTO dbo.Award (AwardName,AwardTypeCode) VALUES ('Coach','Student')
INSERT INTO dbo.Award (AwardName,AwardTypeCode) VALUES ('MVP','Student')
INSERT INTO dbo.Award (AwardName,AwardTypeCode) VALUES ('John Ryan Assistant Coach Award','Coach')
INSERT INTO dbo.Award (AwardName,AwardTypeCode) VALUES ('John Ryan Head Coach Award','Coach')
GO