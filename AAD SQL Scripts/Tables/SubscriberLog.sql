DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.SubscriberLog'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE dbo.SubscriberLog
	
	END
--ENDIF

CREATE TABLE dbo.SubscriberLog
	(
	SubscriberLogID int IDENTITY(1,1) NOT NULL,
	PersonID int,
	SubscriberAction varchar(50),
	ContestID int,
	EmailAddress varchar(320),
	EmailSubject varchar(500),
	EmailMessage varchar(max),
	CreateDateTime datetime,
	SendDateTime datetime
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ContestID', 0
EXEC Utility.SetDefault @cTableName, 'CreateDateTime', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'PersonID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'CreateDateTime', 'datetime'
EXEC Utility.SetColumnNotNull @cTableName, 'PersonID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SubscriberLogID'
GO
