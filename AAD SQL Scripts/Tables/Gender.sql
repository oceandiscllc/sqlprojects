DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Gender'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Gender
	
CREATE TABLE dbo.Gender
	(
	GenderID int IDENTITY(0,1) NOT NULL,
	GenderCode varchar(10),
	GenderName varchar(50),
	DisplayOrder int,	
	IsActive bit,
	OldID int
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'GenderID'
EXEC Utility.SetIndexNonClustered 'IX_Gender', @cTableName, 'GenderName ASC'
GO

SET IDENTITY_INSERT dbo.Gender ON
INSERT INTO dbo.Gender (GenderID) VALUES (0)
SET IDENTITY_INSERT dbo.Gender OFF
GO

INSERT INTO dbo.Gender 
	(OldID,GenderCode,GenderName,DisplayOrder)
SELECT 
	GenderID,
	GenderCode,
	GenderName,
	ISNULL(DisplaySequence,0)
FROM
	(
	SELECT 
		O.ObjectID AS GenderID, 
		'GenderName' AS AttributeName, 
		O.ObjectName AS AttributeValue
	FROM AAD.AAD1001.Objects O
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Gender'

	UNION

	SELECT 
		A.ObjectID AS GenderID, 
		A.AttributeName, 
		A.AttributeValue
	FROM AAD.AAD1001.Attributes A
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName = 'Gender'
			AND A.AttributeName IN ('GenderCode','GenderName','DisplaySequence')
	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (GenderName,GenderCode,DisplaySequence)
	) AS PVT
GO