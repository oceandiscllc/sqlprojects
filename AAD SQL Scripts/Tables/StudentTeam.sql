DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.StudentTeam'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.StudentTeam
	
CREATE TABLE dbo.StudentTeam
	(
	StudentTeamID int IDENTITY(1,1) NOT NULL,
	SchoolYearStart int,
	StudentID int,
	Jersey varchar(10),
	TeamID int
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'SchoolYearStart', 0
EXEC Utility.SetDefault @cTableName, 'StudentID', 0
EXEC Utility.SetDefault @cTableName, 'TeamID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'SchoolYearStart', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'StudentID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TeamID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'StudentTeamID'
GO
