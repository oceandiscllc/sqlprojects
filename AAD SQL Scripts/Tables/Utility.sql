--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

--End create utility helper tools
--Begin create AddColumn helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.AddColumn') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.AddColumn
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.AddColumn
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WITH (NOLOCK) WHERE SC.id = OBJECT_ID(@cTableName) AND SC.name = @cColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD ' + @cColumnName + ' ' + @cDataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End create AddColumn helper stored procedure

--Begin create DropConstraintsAndIndexes helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.DropConstraintsAndIndexes') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.DropConstraintsAndIndexes
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.DropConstraintsAndIndexes
	@cTableName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	DECLARE @cSQL varchar(max)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC WITH (NOLOCK)
		WHERE DC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @cTableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@cTableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @cTableName AS SQL
		FROM sys.indexes I WITH (NOLOCK)
		WHERE I.object_ID = OBJECT_ID(@cTableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End create DropConstraintsAndIndexes helper stored procedure

--Begin create SetColumnNotNull helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetColumnNotNull') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetColumnNotNull
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.01.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetColumnNotNull
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDataType varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ALTER COLUMN ' + @cColumnName + ' ' + @cDataType + ' NOT NULL'
	EXEC (@cSQL)
		
END
GO
--End create SetColumnNotNull helper stored procedure

--Begin create SetDefault helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetDefault') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetDefault
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetDefault
	@cTableName varchar(250),
	@cColumnName varchar(250),
	@cDefault varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cConstraintName varchar(500)
	DECLARE @cSQL varchar(max)
	DECLARE @nDefaultIsGetDate bit
	DECLARE @nDefaultIsNumeric bit
	DECLARE @nLength int

	SET @nDefaultIsGetDate = 0

	IF @cDefault = 'getDate()'
		SET @nDefaultIsGetDate = 1
		
	SET @nDefaultIsNumeric = ISNUMERIC(@cDefault)

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName
	
	IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ''' + @cDefault + ''' WHERE ' + @cColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ' + @cDefault + ' WHERE ' + @cColumnName + ' IS NULL'

	EXECUTE (@cSQL)

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cConstraintName = 'DF_' + RIGHT(@cTableName, @nLength) + '_' + @cColumnName
	
	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC WITH (NOLOCK) WHERE DC.parent_object_ID = OBJECT_ID(@cTableName) AND DC.Name = @cConstraintName)
		BEGIN	

		IF @nDefaultIsGetDate = 0 AND @nDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @cDefault + ''' FOR ' + @cColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @cDefault + ' FOR ' + @cColumnName
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End create SetDefault helper stored procedure

--Begin create SetIndexClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexClustered helper stored procedure

--Begin create SetIndexNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetIndexNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetIndexNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetIndexNonClustered
	@cIndexName varchar(250),
	@cTableName varchar(250),
	@cColumns varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @cIndexName + ' ON ' + @cTableName + ' (' + @cColumns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'

	EXECUTE (@cSQL)

END
GO
--End create SetIndexNonClustered helper stored procedure

--Begin create SetPrimaryKeyClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyClustered helper stored procedure

--Begin create SetPrimaryKeyNonClustered helper stored procedure
IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('Utility.SetPrimaryKeyNonClustered') AND O.type in (N'P', N'PC'))
	DROP PROCEDURE Utility.SetPrimaryKeyNonClustered
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE Utility.SetPrimaryKeyNonClustered
	@cTableName varchar(250),
	@cColumnName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL varchar(max)
	DECLARE @nLength int
	
	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT PK_' + RIGHT(@cTableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @cColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End create SetPrimaryKeyNonClustered helper stored procedure
--End create utility helper tools