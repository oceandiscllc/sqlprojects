--Begin dbo.DateFormat function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.DateFormat') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.DateFormat
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date: 2012.03.31
-- Description: A function to format Date data
-- ===========================================
CREATE FUNCTION dbo.DateFormat
(
@dDate Date
)

RETURNS varchar(10)

AS
BEGIN

RETURN CONVERT(varchar(10), @dDate, 101)
END

GO
--End dbo.DateFormat function

IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.GetLocationNameByLocationID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetLocationNameByLocationID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2012.03.24
-- Description: A function to return an Location school name
-- =========================================================
CREATE FUNCTION dbo.GetLocationNameByLocationID
(
@nLocationID int,
@cFormat varchar(50)
)

RETURNS varchar(max)

AS
BEGIN

DECLARE @cLocationName varchar(250)
DECLARE @cCity varchar(50)
DECLARE @cState varchar(2)

SELECT
	@cLocationName = L.LocationName,
	@cCity = ISNULL(L.City, ''),
	@cState = ISNULL(L.State, '')
FROM dbo.Location L
WHERE L.LocationID = @nLocationID

IF @cFormat = 'List'
	BEGIN
	
	IF LEN(RTRIM(@cCity)) > 0 OR LEN(RTRIM(@cState)) > 0
		BEGIN
		
		SET @cLocationName = @cLocationName + ' ('
		
		IF LEN(RTRIM(@cCity)) > 0
			BEGIN

			SET @cLocationName = @cLocationName + @cCity
			
			END
		--ENDIF
		
		IF LEN(RTRIM(@cCity)) > 0 AND LEN(RTRIM(@cState)) > 0
			BEGIN
		
			SET @cLocationName = @cLocationName + ', '
		
			END
		--ENDIF
		
		IF LEN(RTRIM(@cState)) > 0
			BEGIN

			SET @cLocationName = @cLocationName + @cState
			
			END
		--ENDIF

		SET @cLocationName = @cLocationName + ')'

		END
	--ENDIF
	
	END
--ENDIF

RETURN @cLocationName
END

GO
--End dbo.GetLocationNameByLocationID function

IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.GetPersonNameByPersonID') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.GetPersonNameByPersonID
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2012.04.07
-- Description:	A function to return the name of a person in a specified format from a PersonID
-- ============================================================================================

CREATE FUNCTION dbo.GetPersonNameByPersonID
(
@nPersonID int,
@cFormat varchar(50)
)

RETURNS varchar(250)

AS
BEGIN

DECLARE @cFirstName varchar(25)
DECLARE @cLastName varchar(25)
DECLARE @cTitle varchar(10)
DECLARE @cRetVal varchar(250)

SET @cRetVal = ''

IF @nPersonID IS NOT NULL AND @nPersonID > 0
	BEGIN
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM dbo.Person P
	WHERE P.PersonID = @nPersonID

	IF @cFormat = 'FirstLast' OR @cFormat = 'TitleFirstLast'
		BEGIN
		
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @cFormat = 'TitleFirstLast' AND LEN(RTRIM(@cTitle)) > 0
			BEGIN
			
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))

			END
		--ENDIF
		
		END
	--ENDIF
		
	IF @cFormat = 'LastFirst' OR @cFormat = 'LastFirstTitle'
		BEGIN
		
		IF LEN(RTRIM(@cLastName)) > 0
			BEGIN
			
			SET @cRetVal = @cLastName + ', '

			END
		--ENDIF
			
		SET @cRetVal = @cRetVal + @cFirstName + ' '

		IF @cFormat = 'LastFirstTitle' AND LEN(RTRIM(@cTitle)) > 0
			BEGIN
			
			SET @cRetVal = @cRetVal + @cTitle

			END
		--ENDIF
		
		END
	--ENDIF
	END
--ENDIF

RETURN RTRIM(LTRIM(@cRetVal))
END

GO

--Begin function dbo.ListToTable
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.ListToTable') AND O.Type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.ListToTable
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
-- ========================================================================

CREATE FUNCTION dbo.ListToTable
(
@cList varchar(max),
@nMakeDistinct bit, 
@cDelimiter varchar(5)
)

RETURNS 
@oTable TABLE (ListItem varchar(max))

AS
BEGIN

DECLARE @cListElement varchar(max)
DECLARE @nDelimiterLength int
DECLARE @nLength int
DECLARE @nPosition int

SET @cDelimiter = ISNULL(@cDelimiter, ',')
SET @cList = @cList + @cDelimiter
SET @nDelimiterLength = LEN(@cDelimiter)
SET @nPosition = CHARINDEX(@cDelimiter, @cList)

WHILE @nPosition > 0
	BEGIN
	
	SET @cListElement = LEFT(@cList, @nPosition - 1)

	IF LEN(LTRIM(@cListElement)) > 0
		BEGIN
		
		IF @nMakeDistinct = 0 OR NOT EXISTS (SELECT 1 FROM @oTable WHERE ListItem = CAST(@cListElement as varchar(max)))
			BEGIN
			
			INSERT INTO @oTable (ListItem) VALUES (CAST(@cListElement as varchar(max)))
			
			END
		--ENDIF
		
		END
	--ENDIF
	
	SET @nLength = LEN(@cListElement) + @nDelimiterLength
	SET @cList = RIGHT(@cList, LEN(@cList) - @nLength)
	SET @nPosition = CHARINDEX(@cDelimiter, @cList)

	END
--END WHILE

RETURN 
END

GO
--End function dbo.ListToTable

--Begin dbo.SchoolYearFormat function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.SchoolYearFormat') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.SchoolYearFormat
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date: 2012.04.15
-- Description: A function to return a school year from a date
-- ===========================================================
CREATE FUNCTION dbo.SchoolYearFormat
(
@dDate date
)

RETURNS varchar(11)

AS
BEGIN

DECLARE @cSchoolYear varchar(11)

SET @cSchoolYear = 

	CASE
		WHEN MONTH(@dDate) > 6
		THEN CAST(YEAR(@dDate) as char(4))
		ELSE CAST(YEAR(@dDate) - 1 as char(4))
	END
	
	+ ' - ' + 

	CASE
		WHEN MONTH(@dDate) > 6
		THEN CAST(YEAR(@dDate) + 1 as char(4))
		ELSE CAST(YEAR(@dDate) as char(4))
	END

RETURN @cSchoolYear
END

GO
--End dbo.SchoolYearFormat function

--Begin dbo.TimeFormat function
IF  EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID('dbo.TimeFormat') AND O.type IN ('FN','IF','TF','FS','FT'))
	DROP FUNCTION dbo.TimeFormat
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date: 2012.03.31
-- Description: A function to format time data
-- ===========================================
CREATE FUNCTION dbo.TimeFormat
(
@tTime time
)

RETURNS varchar(8)

AS
BEGIN

DECLARE @cTimeFormatted varchar(8)

SET @cTimeFormatted = CONVERT(varchar(7), @tTime, 0)
SET @cTimeFormatted = REPLACE(REPLACE(@cTimeFormatted, 'AM', ' AM'), 'PM', ' PM')

RETURN @cTimeFormatted
END

GO
--End dbo.TimeFormat function

--Begin procedure dbo.ContestSubscriptionData
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.ContestSubscriptionData') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE dbo.ContestSubscriptionData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2012.04.18
-- Description:	A stored procedure to return data for contest subscription messages
-- ================================================================================
CREATE PROCEDURE dbo.ContestSubscriptionData
	@nContestID int,
	@nIncludeDataDictionary bit

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		dbo.DateFormat(C.ContestDate) AS ContestDateFormatted,
		LEFT(DATENAME(weekday, C.ContestDate), 3) AS ContestDay,
		C.ContestID,
		C.ContestName,
		dbo.TimeFormat(C.ContestTime) AS ContestTimeFormatted,
		C.IsWebApproved,
		C.OwnScore,
		C.OpponentScore,
		dbo.TimeFormat(C.PreContestTime) AS PreContestTimeFormatted,
		C.Results,
		C.Site,
		CRS.ContestResultStatusName,
		CRT.ContestResultTypeCode,
		L.LocationName AS Venue,
		T.TeamNameWeb
	FROM dbo.Contest C
		JOIN dbo.ContestResultStatus CRS ON CRS.ContestResultStatusID = C.ContestResultStatusID
		JOIN dbo.Location L ON L.LocationID = C.LocationID
		JOIN dbo.Team T ON T.TeamID = C.TeamID
		JOIN dbo.Sport S ON S.SportID = T.SportID
		JOIN dbo.ContestResultType CRT ON CRT.ContestResultTypeID = S.ContestResultTypeID
			AND C.ContestID = @nContestID

	SELECT
		L.LocationID AS OpponentID,
		L.LocationName AS OpponentName
	FROM dbo.Location L
		JOIN dbo.ContestOpponent CO ON CO.LocationID = L.LocationID
			AND CO.ContestID = @nContestID
	ORDER BY L.LocationName
	
	IF @nIncludeDataDictionary = 1
		BEGIN

		SELECT 
			DD.ColumnName,
			DD.DisplayName
		FROM Utility.DataDictionary DD
		WHERE DD.EntityTypeCode = 'Contest'
		
		END
	--ENDIF
	
END	
GO
--End procedure dbo.ContestSubscriptionData

--Begin procedure dbo.ParticipationMetricsData
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.ParticipationMetricsData') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE dbo.ParticipationMetricsData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date: 2012.04.18
-- Description:	A stored procedure to return data for participation metrics
-- ========================================================================
CREATE PROCEDURE dbo.ParticipationMetricsData
	@nSchoolYearStart int

AS
BEGIN
	SET NOCOUNT ON;
	
	-- Get the upper / lower school total counts
	SELECT
		COUNT(D.StudentID) AS ItemCount,
		D.GenderName,
		D.Division
	FROM
		(
		SELECT
			S.StudentID,
			G.GenderName,
			
			CASE
				WHEN S.Grade < 9
				THEN 'Middle School'
				ELSE 'Upper School'
			END AS Division
	
		FROM dbo.Student S
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.IsActive = 1
		) D
	GROUP BY D.Division, D.GenderName
	ORDER BY D.Division, D.GenderName

	-- Get the upper / lower school athlete counts
	SELECT
		COUNT(D.StudentID) AS ItemCount,
		D.GenderName,
		D.Division
	FROM
		(
		SELECT
			S.StudentID,
			G.GenderName,
			
			CASE
				WHEN S.Grade < 9
				THEN 'Middle School'
				ELSE 'Upper School'
			END AS Division
	
		FROM dbo.Student S
			JOIN dbo.Gender G ON G.GenderID = S.GenderID
				AND S.IsActive = 1
				AND EXISTS
					(
					SELECT 1
					FROM dbo.StudentParticipation SPR
					WHERE SPR.SchoolYearStart = @nSchoolYearStart
						AND SPR.StudentID = S.StudentID
					)
		) D
	GROUP BY D.Division, D.GenderName
	ORDER BY D.Division, D.GenderName

	-- Get the multi sport athlete counts
	SELECT
		COUNT(D.ItemCount) AS SportCount,
		D.ItemCount
	FROM
		(
		SELECT
			COUNT(SPR.StudentParticipationID) AS ItemCount,
			SPR.StudentID
		FROM dbo.StudentParticipation SPR
			JOIN dbo.Student S ON S.StudentID = SPR.StudentID
				AND SPR.SchoolYearStart = @nSchoolYearStart
				AND S.IsActive = 1
		GROUP BY SPR.StudentID
		) D
	GROUP BY D.ItemCount
	ORDER BY D.ItemCount

	-- Get the financial aid total counts
	SELECT
		COUNT(S.StudentID) AS ItemCount,
		S.FinancialAidCode
	FROM dbo.Student S
	WHERE S.IsActive = 1
	GROUP BY S.FinancialAidCode
	ORDER BY S.FinancialAidCode

	-- Get the financial aid athlete counts
	SELECT
		COUNT(S.StudentID) AS ItemCount,
		S.FinancialAidCode
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.StudentParticipation SPR
			WHERE SPR.SchoolYearStart = @nSchoolYearStart
				AND SPR.StudentID = S.StudentID
			)
	GROUP BY S.FinancialAidCode
	ORDER BY S.FinancialAidCode

	-- Get the grade of admission total counts
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOALT8F' AS GradeOfAdmission,
		7 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (6,7)
		AND S.CameFromFeederSchool = 1
	
	UNION
	
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOALT8O' AS GradeOfAdmission,
		7 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (6,7)
		AND S.CameFromFeederSchool = 0
	
	UNION
		
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOA' + CAST(S.GradeOfAdmission as varchar(5)) AS GradeOfAdmission,
		S.GradeOfAdmission AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (8,9)
	GROUP BY S.GradeOfAdmission
	
	UNION
		
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOAGT9' AS GradeOfAdmission,
		10 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission > 9
	
	ORDER BY DisplayOrder, GradeOfAdmission

	-- Get the grade of admission athlete counts
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOALT8F' AS GradeOfAdmission,
		7 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (6,7)
		AND S.CameFromFeederSchool = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.StudentParticipation SPR
			WHERE SPR.SchoolYearStart = @nSchoolYearStart
				AND SPR.StudentID = S.StudentID
			)
	
	UNION
	
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOALT8O' AS GradeOfAdmission,
		7 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (6,7)
		AND S.CameFromFeederSchool = 0
		AND EXISTS
			(
			SELECT 1
			FROM dbo.StudentParticipation SPR
			WHERE SPR.SchoolYearStart = @nSchoolYearStart
				AND SPR.StudentID = S.StudentID
			)

	UNION
		
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOA' + CAST(S.GradeOfAdmission as varchar(5)) AS GradeOfAdmission,
		S.GradeOfAdmission AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission IN (8,9)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.StudentParticipation SPR
			WHERE SPR.SchoolYearStart = @nSchoolYearStart
				AND SPR.StudentID = S.StudentID
			)
	GROUP BY S.GradeOfAdmission
	
	UNION
		
	SELECT 
		COUNT(S.GradeOfAdmission) AS ItemCount,
		'GOAGT9' AS GradeOfAdmission,
		10 AS DisplayOrder
	FROM dbo.Student S
	WHERE S.IsActive = 1
		AND S.GradeOfAdmission > 9
		AND EXISTS
			(
			SELECT 1
			FROM dbo.StudentParticipation SPR
			WHERE SPR.SchoolYearStart = @nSchoolYearStart
				AND SPR.StudentID = S.StudentID
			)
	
	ORDER BY DisplayOrder, GradeOfAdmission

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable', 'u')) IS NOT NULL
		DROP TABLE #tTable
	
	CREATE TABLE #tTable 
		(
		SportID int, 
		GenderCode char(1), 
		Total int NOT NULL DEFAULT 0,
		MS int NOT NULL DEFAULT 0,
		FAC1 int NOT NULL DEFAULT 0,
		FAC2 int NOT NULL DEFAULT 0,
		FAC3 int NOT NULL DEFAULT 0,
		FAC4 int NOT NULL DEFAULT 0,
		FAC5 int NOT NULL DEFAULT 0,
		GOALT8F int NOT NULL DEFAULT 0,
		GOALT8O int NOT NULL DEFAULT 0,
		GOA8 int NOT NULL DEFAULT 0,
		GOA9 int NOT NULL DEFAULT 0,
		GOAGT9 int NOT NULL DEFAULT 0
		)
	
	INSERT INTO #tTable
		(SportID,GenderCode)
	SELECT DISTINCT
		SP.SportID,
		G.GenderCode
	FROM dbo.StudentParticipation SP
		JOIN dbo.Student S ON S.StudentID = SP.StudentID
		JOIN dbo.Gender G ON G.GenderID = S.GenderID
			AND S.IsActive = 1
			AND SP.SchoolYearStart = @nSchoolYearStart
	ORDER BY 
		SP.SportID,
		G.GenderCode
	
	UPDATE T
	SET T.Total = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.MS = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP1.SportID,
				G.GenderCode,
				COUNT(SP1.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP1
				JOIN dbo.Student S ON S.StudentID = SP1.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP1.SchoolYearStart = 2011--@nSchoolYearStart
					AND EXISTS
						(
						SELECT 1
						FROM dbo.StudentParticipation SP2
						WHERE SP2.StudentID = SP1.StudentID
							AND SP2.SchoolYearStart = 2011--@nSchoolYearStart
							AND SP2.SportID <> SP1.SportID
						)
			GROUP BY
				SP1.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.FAC1 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND SP.FinancialAidCode = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
			
	UPDATE T
	SET T.FAC2 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND SP.FinancialAidCode = 2
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.FAC3 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND SP.FinancialAidCode = 3
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.FAC4 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND SP.FinancialAidCode = 4
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.FAC5 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND S.IsActive = 1
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND SP.FinancialAidCode = 5
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.GOALT8F = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND S.CameFromFeederSchool = 1
					AND S.GradeOfAdmission < 8
					AND S.IsActive = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.GOALT8O = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND S.CameFromFeederSchool = 0
					AND S.GradeOfAdmission < 8
					AND S.IsActive = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.GOA8 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND S.GradeOfAdmission = 8
					AND S.IsActive = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.GOA9 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND S.GradeOfAdmission = 9
					AND S.IsActive = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	UPDATE T
	SET T.GOAGT9 = D.ItemCount
	FROM #tTable T
		JOIN 
			(
			SELECT
				SP.SportID,
				G.GenderCode,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Student S ON S.StudentID = SP.StudentID
				JOIN dbo.Gender G ON G.GenderID = S.GenderID
					AND SP.SchoolYearStart = @nSchoolYearStart
					AND S.GradeOfAdmission > 9
					AND S.IsActive = 1
			GROUP BY
				SP.SportID,
				G.GenderCode
			) D ON D.SportID = T.SportID
				AND D.GenderCode = T.GenderCode
	
	SELECT 
		S.SportName + ' - ' + G.GenderName AS Sport,
		T.Total,
		T.MS,
		T.FAC1,
		T.FAC2,
		T.FAC3,
		T.FAC4,
		T.FAC5,
		T.GOALT8F,
		T.GOALT8O,
		T.GOA8,
		T.GOA9,
		T.GOAGT9
	FROM #tTable T
		JOIN dbo.Sport S ON S.SportID = T.SportID
		JOIN dbo.Gender G ON G.GenderCode = T.GenderCode
	
	DROP TABLE #tTable	
END	
GO
--End procedure dbo.ParticipationMetricsData

--Begin procedure dbo.SeniorAwardsData
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('dbo.SeniorAwardsData') AND O.type IN ('P', 'PC'))
	DROP PROCEDURE dbo.SeniorAwardsData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2012.05.02
-- Description:	A stored procedure T1o return data for senior athletic awards
-- =========================================================================
CREATE PROCEDURE dbo.SeniorAwardsData
	@nYearofGraduation int

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nSchoolYearStart int

	SET @nSchoolYearStart = @nYearofGraduation - 4

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable1', 'u')) IS NOT NULL
		DROP TABLE #tTable1
	
	CREATE TABLE #tTable1 
		(
		StudentID int, 
		SportCountV int NOT NULL DEFAULT 0,
		SportCountJV int NOT NULL DEFAULT 0,
		AwardCountMVP int NOT NULL DEFAULT 0,
		AwardCountOther int NOT NULL DEFAULT 0,
		Points int NOT NULL DEFAULT 0,
		)
	
	INSERT INTO #tTable1
		(StudentID)
	SELECT 
		S.StudentID
	FROM dbo.Student S
	WHERE S.YearOfGraduation = @nYearofGraduation
		AND S.IsActive = 1
		AND EXISTS (SELECT 1 FROM dbo.StudentParticipation SP WHERE SP.StudentID = S.StudentID AND SP.SchoolYearStart = @nYearofGraduation - 1)
	
	UPDATE T1
	SET T1.SportCountV = D.ItemCount
	FROM #tTable1 T1
		JOIN 
			(
			SELECT 
				SP.StudentID,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Level L ON L.LevelID = SP.LevelID
					AND L.LevelCode = 'V'
					AND SP.SchoolYearStart >= @nSchoolYearStart
			GROUP BY SP.StudentID
			) D ON D.StudentID = T1.StudentID
	
	UPDATE T1
	SET T1.SportCountJV = D.ItemCount
	FROM #tTable1 T1
		JOIN 
			(
			SELECT 
				SP.StudentID,
				COUNT(SP.StudentID) AS ItemCount
			FROM dbo.StudentParticipation SP
				JOIN dbo.Level L ON L.LevelID = SP.LevelID
					AND L.LevelCode = 'JV'
					AND SP.SchoolYearStart >= @nSchoolYearStart
			GROUP BY SP.StudentID
			) D ON D.StudentID = T1.StudentID
	
	UPDATE T1
	SET T1.AwardCountMVP = D.ItemCount
	FROM #tTable1 T1
		JOIN 
			(
			SELECT 
				SA1.StudentID,
				COUNT(SA1.StudentID) AS ItemCount
			FROM dbo.StudentAward SA1
				JOIN dbo.Award A ON A.AwardID = SA1.AwardID
					AND A.Awardname = 'MVP'
				JOIN dbo.Level L ON L.LevelID = SA1.LevelID
					AND L.LevelCode = 'V'
					AND SA1.SchoolYearStart >= @nSchoolYearStart
			GROUP BY SA1.StudentID
			) D ON D.StudentID = T1.StudentID
	
	UPDATE T1
	SET T1.AwardCountOther = D.ItemCount
	FROM #tTable1 T1
		JOIN 
			(
			SELECT 
				SA1.StudentID,
				COUNT(SA1.StudentID) AS ItemCount
			FROM dbo.StudentAward SA1
			WHERE SA1.SchoolYearStart >= @nSchoolYearStart
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.StudentAward SA2
						JOIN dbo.Award A ON A.AwardID = SA2.AwardID
							AND A.Awardname = 'MVP'
						JOIN dbo.Level L ON L.LevelID = SA2.LevelID
							AND L.LevelCode = 'V'
							AND SA1.SchoolYearStart >= @nSchoolYearStart
							AND SA2.SchoolYearStart >= @nSchoolYearStart
							AND SA2.StudentID = SA1.StudentID
					)
			GROUP BY SA1.StudentID
			) D ON D.StudentID = T1.StudentID
	
	UPDATE T1
	SET T1.Points = (T1.SportCountV * 3) + (T1.SportCountJV) + (T1.AwardCountMVP * 3) + AwardCountOther
	FROM #tTable1 T1
	
	SELECT 
		S.StudentID,
		S.LastName,
		S.FirstName,
		S.MiddleName,
		S.StudentEmail,
		S.MotherEmail,
		S.FatherEmail,
		S.YearOfAdmission,
		T1.SportCountV,
		T1.SportCountJV,
		T1.AwardCountMVP,
		T1.AwardCountOther,
		T1.Points,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM dbo.StudentParticipation SP WHERE SP.StudentID = S.StudentID AND SP.SchoolYearStart = @nYearofGraduation - 1)
			THEN 'Y'
			ELSE 'N'
		END AS ParticipatedG12,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM dbo.StudentParticipation SP WHERE SP.StudentID = S.StudentID AND SP.SchoolYearStart = @nYearofGraduation - 2)
			THEN 'Y'
			ELSE 'N'
		END AS ParticipatedG11,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM dbo.StudentParticipation SP WHERE SP.StudentID = S.StudentID AND SP.SchoolYearStart = @nYearofGraduation - 3)
			THEN 'Y'
			ELSE 'N'
		END AS ParticipatedG10,
		
		CASE 
			WHEN EXISTS (SELECT 1 FROM dbo.StudentParticipation SP WHERE SP.StudentID = S.StudentID AND SP.SchoolYearStart = @nYearofGraduation - 4)
			THEN 'Y'
			ELSE 'N'
		END AS ParticipatedG09
		
	FROM #tTable1 T1
		JOIN dbo.Student S ON S.StudentID = T1.StudentID
	ORDER BY 
		S.LastName,
		S.FirstName,
		S.MiddleName,
		S.StudentID

	IF (SELECT OBJECT_ID('tempdb.dbo.#tTable2', 'u')) IS NOT NULL
		DROP TABLE #tTable2
	
	CREATE TABLE #tTable2 
		(
		StudentID int,
		SchoolYearStart int,
		AwardName varchar(50),
		SportName varchar(50),
		LevelCode varchar(5),
		ParticipationCount int,
		Points int NOT NULL DEFAULT 0,
		TotalPoints int NOT NULL DEFAULT 0
		)
	
	INSERT INTO #tTable2 
		(StudentID,SportName,LevelCode,ParticipationCount,Points)
	SELECT
		SP1.StudentID,
		S1.SportName,
		L.LevelCode,
		COUNT(S1.SportCode + '-' + L.LevelCode),
	
		CASE
			WHEN L.LevelCode = 'V'
			THEN COUNT(S1.SportCode + '-' + L.LevelCode) * 3
			ELSE COUNT(S1.SportCode + '-' + L.LevelCode)
		END
	
	FROM AADNew.dbo.StudentParticipation SP1
		JOIN AADNew.dbo.Sport S1 ON S1.SportID = SP1.SportID
		JOIN dbo.Level L ON L.LevelID = SP1.LevelID
			AND L.LevelCode IN ('V','JV')
			AND SP1.SchoolYearStart >= @nSchoolYearStart
			AND EXISTS
				(
				SELECT 1
				FROM AADNew.dbo.Student S2
				WHERE S2.StudentID = SP1.StudentID
					AND S2.IsActive = 1
					AND S2.YearOfGraduation = @nYearofGraduation
					AND EXISTS (SELECT 1 FROM AADNew.dbo.StudentParticipation SP2 WHERE SP2.StudentID = S2.StudentID AND SP2.SchoolYearStart = @nYearofGraduation - 1)
				)		
	GROUP BY
		SP1.StudentID,
		S1.SportName,
		L.LevelCode
	
	INSERT INTO #tTable2 
		(StudentID,SchoolYearStart,AwardName,SportName,LevelCode,ParticipationCount,Points)
	SELECT 
		SA1.StudentID,
		SA1.SchoolYearStart,
		A.AwardName,
		S1.SportName,
		L.LevelCode,
		1,
	
		CASE
			WHEN L.LevelCode = 'V' AND A.AwardName = 'MVP'
			THEN 3
			ELSE 1
		END
	
	FROM AADNew.dbo.StudentAward SA1
		JOIN AADNew.dbo.Award A ON A.AwardID = SA1.AwardID
		JOIN AADNew.dbo.Sport S1 ON S1.SportID = SA1.SportID
		JOIN dbo.Level L ON L.LevelID = SA1.LevelID
			AND L.LevelCode IN ('V','JV')
			AND SA1.SchoolYearStart >= @nSchoolYearStart
			AND EXISTS
				(
				SELECT 1
				FROM AADNew.dbo.Student S2
				WHERE S2.StudentID = SA1.StudentID
					AND S2.IsActive = 1
					AND S2.YearOfGraduation = @nYearofGraduation
					AND EXISTS (SELECT 1 FROM AADNew.dbo.StudentParticipation SP2 WHERE SP2.StudentID = S2.StudentID AND SP2.SchoolYearStart = @nYearofGraduation - 1)
				)		
	
	UPDATE T21
	SET T21.TotalPoints = T23.TotalPoints
	FROM #tTable2 T21
		JOIN
			(
			SELECT 
				T22.StudentID,
				SUM(T22.Points) AS TotalPoints
			FROM #tTable2 T22
			GROUP BY 
				T22.StudentID
			) T23 ON T23.StudentID = T21.StudentID
	
	SELECT 
		T2.StudentID,
		S.LastName,
		S.FirstName,
		S.MiddleName,
		T2.SchoolYearStart,
		T2.AwardName,
		T2.SportName,
		T2.LevelCode,
		T2.ParticipationCount,
		T2.Points,
		T2.TotalPoints
	FROM #tTable2 T2
		JOIN dbo.Student S ON S.StudentID = T2.StudentID
	ORDER BY 
		S.LastName,
		S.FirstName,
		S.MiddleName,
		T2.StudentID
		
	DROP TABLE #tTable2
	DROP TABLE #tTable1

	END
GO
--End procedure dbo.SeniorAwardsData