DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.Setup'

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type IN ('U'))
	BEGIN

	DROP TABLE Utility.Setup
	
	END
--ENDIF

CREATE TABLE Utility.Setup
	(
	SetupID int IDENTITY(1,1) NOT NULL,
	SetupKey varchar(250),
	SetupValue varchar(max),
	CreateDate datetime
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'CreateDate', 'getDate()'

EXEC Utility.SetColumnNotNull @cTableName, 'CreateDate', 'datetime'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SetupID'
GO

INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('Environment','Development')
INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('MailFromName','noreply@AssistantAD.com')
INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('MailPassword','')
INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('MailPort','0')
INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('MailServer','localhost')
INSERT INTO Utility.Setup (SetupKey,SetupValue) VALUES ('MailUserName','noreply@AssistantAD.com')
GO