TRUNCATE TABLE AADNew.dbo.ContractElement
GO

INSERT INTO AADNew.dbo.ContractElement
	(ContractElementTypeCode,ContractElementTitle,ContractElementText)
SELECT
	REPLACE(ObjectName, 'Contract', ''),
	ContractElementName,
	ContractElementText
FROM 
	(
	SELECT 
		O.ObjectID, 
		'ObjectName' AS AttributeName,
		CONVERT(VARCHAR(max), O.ObjectName) AS AttributeValue
	FROM AAD.AAD1001.Objects O
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName LIKE 'Contract%'

	UNION
		
	SELECT 
		A.ObjectID, 
		A.AttributeName, 
		A.AttributeValue
	FROM AAD.AAD1001.Attributes A
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A.ObjectID
		JOIN AAD.AAD.Types T ON T.TypeID = O.ObjectTypeID
			AND T.TypeCategory = 'Object'
			AND T.TypeName LIKE 'Contract%'
	) AS OA
PIVOT
	( 
	MAX(OA.AttributeValue)
	FOR OA.AttributeName IN 
		(
		ObjectName,ContractElementName,ContractElementText
		)
	) AS PVT
	
SELECT *
FROM 	AADNew.dbo.ContractElement