DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Season'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.Season
	
CREATE TABLE dbo.Season
	(
	SeasonID int IDENTITY(0,1) NOT NULL,
	SeasonName varchar(50),
	DisplayOrder int,
	IsActive bit
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'SeasonID'
EXEC Utility.SetIndexNonClustered 'IX_Season', @cTableName, 'SeasonName ASC'
GO

SET IDENTITY_INSERT dbo.Season ON
INSERT INTO dbo.Season (SeasonID) VALUES (0)
SET IDENTITY_INSERT dbo.Season OFF
GO

INSERT INTO dbo.Season (SeasonName,DisplayOrder) VALUES ('Fall',1)
INSERT INTO dbo.Season (SeasonName,DisplayOrder) VALUES ('Spring',3)
INSERT INTO dbo.Season (SeasonName,DisplayOrder) VALUES ('Winter',2)
GO