--Begin schema validation: Utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Utility')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Utility

DECLARE @cTableName varchar(250)

SET @cTableName = 'Utility.DataDictionary'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE Utility.DataDictionary
	
	END
--ENDIF

CREATE TABLE Utility.DataDictionary
	(
	DataDictionaryID int IDENTITY(1,1) NOT NULL,
	EntityTypeCode varchar(50),
	ColumnName varchar(50),
	DisplayName varchar(50),
	IsActive bit
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'DataDictionaryID'
EXEC Utility.SetIndexClustered 'IX_DataDictionary', @cTableName, 'EntityTypeCode ASC,DisplayName ASC'
GO

INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestDateFormatted','Contest Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestDay','Contest Day')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestID','Contest ID')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestName','Contest Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestResultStatusName','Contest Result Status')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','ContestTimeFormatted','Contest Time')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','IsCanceled','Canceled')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','IsDistrictContest','IMSC/District Contest')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','IsForRecord','For Record')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','IsWebApproved','Web Approved')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','LevelCode','Level Code')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','OpponentScore','Opponent Score')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','OwnScore','Own Score')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','PreContestTimeFormatted','WI / WU Time')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','Results','Results')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','Site','Site')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','TeamCode','Team Code')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Contest','Venue','Venue')
GO

INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','AEDExpirationDateFormatted','AED Expiration Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','CellPhone','Cell Phone')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','ClassCDLExpirationDateFormatted','Class C D/L Expiration Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','CPRExpirationDateFormatted','CPR Expiration Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','EmailAddress','Email Address')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FallContractIssueDateFormatted','Fall Contract Issue Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FallContractReturnDateFormatted','Fall Contract Return Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FirstAidExpirationDateFormatted','First Aid Expiration Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','FirstName','First Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasBackgroundCheck','Background Check')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasCoachGuidelines','Coach Guidelines')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasFingerPrints','Finger Prints')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HasW4','W4')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','HomePhone','Home Phone')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','IsAdjunct','Adjunct')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','LastName','Last Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','MailAddressLine1','Mailing Address 1')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','MailAddressLine2','Mailing Address 2')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','MailCity','City')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','MailState','State')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','MailZip','Zip')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','PersonID','Person ID')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','POSM1CompletionDateFormatted','POSM1 Completion Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','POSM2CompletionDateFormatted','POSM2 Completion Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','POSM3CompletionDateFormatted','POSM3 Completion Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','SpringContractIssueDateFormatted','Spring Contract Issue Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','SpringContractReturnDateFormatted','Spring ContractReturn Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','StartDateFormatted','Start Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','SummerContractIssueDateFormatted','Summer Contract Issue Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','SummerContractReturnDateFormatted','Summer Contract Return Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','Title','Title')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','WinterContractIssueDateFormatted','Winter Contract Issue Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','WinterContractReturnDateFormatted','Winter Contract Return Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Person','WorkPhone','Work Phone')
GO

INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','IsActive','Active')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','BirthDateFormatted','Birth Date')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','CameFromFeederSchool','Came From Feeder School')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MailCity','City')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','HasConsent','Consent')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','FatherEmail','Father''s Email')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','FatherPreferredName','Father''s Preferred Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','FirstName','First Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','GenderCode','Gender Code')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','Grade','Grade')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','GradeOfAdmission','Grade Of Admission')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','LastName','Last Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MailAddressLine1','Mailing Address 1')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MailAddressLine2','Mailing Address 2')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MiddleName','Middle Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MotherEmail','Mother''s Email')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MotherPreferredName','Mother''s Preferred Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','ParentSalutation','Parent Salutation')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','Phone','Phone')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','HasPhysical','Physical')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','PreferredName','Preferred Name')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MailState','State')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','StudentID','Student ID')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','StudentEmail','Student''s Email')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','Suffix','Suffix')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','YearOfAdmission','Year Of Admission')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','YearOfGraduation','Year Of Graduation')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Student','MailZip','Zip')
GO

INSERT INTO Utility.DataDictionary 
	(EntityTypeCode,ColumnName,DisplayName) 
SELECT
	'Event',
	REPLACE(DD.ColumnName, 'Contest', 'Event'),
	REPLACE(DD.DisplayName, 'Contest', 'Event')
FROM Utility.DataDictionary DD
WHERE DD.ColumnName IN
	(
	'IsCanceled',
	'ContestID',
	'ContestDateFormatted',
	'ContestDay',
	'ContestName', 
	'Venue',
	'IsWebApproved' 
	)
ORDER BY REPLACE(DD.DisplayName, 'Contest', 'Event')
GO

INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Event','EventStartTimeFormatted','Start Time')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Event','EventEndTimeFormatted','End Time')
INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Event','IsAllDayEvent','All Day Event')
GO

INSERT INTO Utility.DataDictionary 
	(EntityTypeCode,ColumnName,DisplayName) 
SELECT
	'Roster',
	DD.ColumnName,
	DD.DisplayName
FROM Utility.DataDictionary DD
WHERE DD.EntityTypeCode = 'Student'
GO

INSERT INTO Utility.DataDictionary (EntityTypeCode,ColumnName,DisplayName) VALUES ('Roster','Jersey','Jersey #')
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.PersonSelection'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	DROP TABLE dbo.PersonSelection
	
CREATE TABLE dbo.PersonSelection
	(
	PersonSelectionID int IDENTITY(1,1) NOT NULL,
	PersonID int,
	EntityTypeCode varchar(50),
	DataDictionaryIDList varchar(max),
	SelectionName varchar(50)
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'PersonID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'PersonID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'PersonSelectionID'
EXEC Utility.SetIndexClustered 'IX_PersonSelection', @cTableName, 'PersonID ASC,SelectionName ASC'
GO
