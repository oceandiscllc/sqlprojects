INSERT INTO AADNew.dbo.StudentAwardRaw
	(StudentID,SchoolYearStart,SchoolYearStop,AwardName,SportCode,LevelCode,GenderCode,SourceCode)
SELECT
	S.StudentID,
	2011,
	2012,
	--'Coach',
	--'Celeste Hampton Distinguished Character Award',
	'MVP',
	'BA',
	'V',
	'B',
	'ESJ'
FROM AADNew.dbo.Student S
WHERE S.LastName = 'Koslowski'
	AND S.FirstName LIKE '%Nat%'
GO