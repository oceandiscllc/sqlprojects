USE AAD
GO

--Begin procedure utility.SetDefaultConstraint
EXEC utility.DropObject 'utility.SetDefaultConstraint'
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date: 2013.10.12
-- Description:	A helper stored procedure for table upgrades
-- ===============================================================================================================================
CREATE PROCEDURE utility.SetDefaultConstraint
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250),
	@DataType VARCHAR(250),
	@Default VARCHAR(MAX),
	@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ''' + @Default + ''' WHERE ' + @ColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ' + @Default + ' WHERE ' + @ColumnName + ' IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN ' + @ColumnName + ' ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + @ColumnName
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR ' + @ColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR ' + @ColumnName
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End procedure utility.SetDefaultConstraint

--Begin table dbo.Event
DECLARE @TableName VARCHAR(250) = 'dbo.Event'

EXEC utility.AddColumn @TableName, 'EstimatedNumberOfVehicles', 'VARCHAR(5)'
EXEC utility.AddColumn @TableName, 'EventHasAlcohol', 'BIT'
EXEC utility.AddColumn @TableName, 'EventPointOfContactCellPhone', 'VARCHAR(20)'
EXEC utility.AddColumn @TableName, 'IsParkingOrSecurityRequired', 'BIT'
EXEC utility.AddColumn @TableName, 'ParkingOrSecurityInstructions', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'RequestorDepartment', 'VARCHAR(50)'

EXEC utility.SetDefaultConstraint @TableName, 'EventHasAlcohol', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EstimatedNumberOfVehicles', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsParkingOrSecurityRequired', 'BIT', 0
GO
--End table dbo.Event

--Begin table dbo.DefaultEmailAddress
INSERT INTO dbo.DefaultEmailAddress
	(EntityTypeCode, EntityID, EmailAddress)
VALUES
	('ParkingAndSecuity', 0, 'greend@esj.org')
GO
--End table dbo.DefaultEmailAddress
