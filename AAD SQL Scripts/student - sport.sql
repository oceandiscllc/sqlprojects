SELECT 
	SP.SchoolYearStart,
	S1.LastName,
	S1.FirstName + 
		CASE
			WHEN S1.FirstName <> S1.PreferredName
			THEN ' (' + S1.PreferredName + ')'
			ELSE ''
		END AS FirstName,
		
	S2.SportName,
	L.LevelName
FROM dbo.Student S1
	JOIN dbo.StudentParticipation SP ON SP.StudentID = S1.StudentID
	JOIN dbo.Sport S2 ON S2.SportID = SP.SportID
		AND SP.SportID = 9
		AND S1.GenderID = 1
		AND SP.SchoolYearStart = 2016
	JOIN dbo.Level L ON L.LevelID = SP.LevelID
ORDER BY SP.SchoolYearStart, L.DisplayOrder, S1.LastName, S1.FirstName, S1.PreferredName, S1.StudentID
