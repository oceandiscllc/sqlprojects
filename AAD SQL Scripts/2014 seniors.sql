SELECT *
  FROM [aad].[dbo].[Student]


SELECT distinct SP.StudentID
FROM [dbo].[StudentParticipation] SP
	JOIN [dbo].[Student] S ON S.StudentID = SP.StudentID
		AND SP.[SchoolYearStart] = 2013
		AND S.[YearOfGraduation] = 2014