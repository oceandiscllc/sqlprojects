USE [aad]
GO

TRUNCATE TABLE [Utility].[Setup]
GO

SET IDENTITY_INSERT [Utility].[Setup] ON
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (1, 'Environment', 'Development', CAST(0x0000A0380123D342 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (2, 'MailFromName', 'noreply@AssistantAD.com', CAST(0x0000A0380123D342 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (3, 'MailPassword', '', CAST(0x0000A0380123D342 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (4, 'MailPort', '25', CAST(0x0000A0380123D343 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (5, 'MailServer', 'localhost', CAST(0x0000A0380123D343 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (6, 'MailUserName', 'noreply@AssistantAD.com', CAST(0x0000A0380123D343 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (7, 'ClientCommonName', 'Episcopal', CAST(0x0000A05600B7D932 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (8, 'ClientLegalName', 'Episcopal School of Jacksonville', CAST(0x0000A05600B7EF17 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (9, 'ClientMailAddress1', '4455 Atlantic Boulevard', CAST(0x0000A05600B81712 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (10, 'ClientMailAddress2', NULL, CAST(0x0000A05600B81D81 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (11, 'ClientMailCity', 'Jacksonville', CAST(0x0000A05600B8254A AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (12, 'ClientMailState', 'FL', CAST(0x0000A05600B82AFB AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (13, 'ClientMailZip', '32207', CAST(0x0000A05600B835BE AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (14, 'ClientPhone', '(904) 396-7133', CAST(0x0000A05600B845C4 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (15, 'ClientFax', '(904) 399-1983', CAST(0x0000A05600B8576B AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (16, 'DocumentRepositoryPath', '\DocumentRepository', CAST(0x0000A2300145285D AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (17, 'ICSFilesPath', '\WebContentPublic\AAD1001\ICSFiles', CAST(0x0000A23001453AE8 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (18, 'SitePath', 'C:\www\aad\html', CAST(0x0000A230014542E8 AS DateTime))
INSERT [Utility].[Setup] ([SetupID], [SetupKey], [SetupValue], [CreateDate]) VALUES (19, 'TempFilesPath', '\Files\Temp', CAST(0x0000A23001455AB6 AS DateTime))
SET IDENTITY_INSERT [Utility].[Setup] OFF
GO
