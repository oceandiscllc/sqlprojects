TRUNCATE TABLE dbo.StudentAward
GO

INSERT INTO dbo.StudentAward
	(StudentID,SchoolYearStart,SportID,LevelID,AwardID)
SELECT
	SAR.StudentID,
	SAR.SchoolYearStart,
	S.SportID,
	L.LevelID,
	A.AwardID
FROM dbo.StudentAwardRaw SAR
	JOIN dbo.Award A ON A.AwardName = SAR.AwardName
	JOIN dbo.Sport S ON S.SportCode = SAR.SportCode
	JOIN dbo.Level L ON L.LevelCode = SAR.LevelCode

