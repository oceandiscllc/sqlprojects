--UPDATE AADNew.dbo.esj20112012
--SET Award = 
--	CASE
--		WHEN Award Like '%Coach%'
--		THEN 'Coach'
--		WHEN Award Like '%MVP%' OR Award LIKE '%MSDA%'
--		THEN 'MVP'
--	END

/*		
UPDATE AADNew.dbo.esj20112012
SET Sport = LTRIM(REPLACE(REPLACE(REPLACE(Sport, 'Boys', ''), 'Girls', ''), 'Coed', ''))

UPDATE AADNew.dbo.esj20112012
SET SportLevel = 
	CASE 
		WHEN Sport LIKE 'Varsity%'
		THEN 'Varsity'
		WHEN Sport LIKE 'Junior Varsity%' OR Sport LIKE 'Freshman%' 
		THEN 'Junior Varsity'
		ELSE 'Middle School'
	END

UPDATE AADNew.dbo.esj20112012
SET SportLevel = 'Middle School'
WHERE SportLevel = 'MiddleSchool'
	
UPDATE AADNew.dbo.esj20112012
SET Sport = LTRIM(REPLACE(Sport, SportLevel, ''))

UPDATE AADNew.dbo.esj20112012
SET Award = Award3
WHERE Award3 IN ('MVP','CHDCA','MSDA')

UPDATE AADNew.dbo.esj20112012
SET AwardLevel = 
	CASE 
		WHEN Award LIKE 'V%'
		THEN 'Varsity'
		WHEN Award LIKE 'JV%' OR Award LIKE 'Fr%' 
		THEN 'Junior Varsity'
		WHEN Award LIKE 'MS%' AND Award <> 'MSDA'
		THEN 'Middle School'
		ELSE NULL
	END

UPDATE AADNew.dbo.esj20112012
SET AwardLevel = SportLevel
WHERE Award IS NOT NULL AND AwardLevel IS NULL

UPDATE AADNew.dbo.esj20112012
SET Award = 
	CASE 
		WHEN Award LIKE '%MVP%' OR Award = 'MSDA'
		THEN 'MVP'
		WHEN Award LIKE '%Coach%'
		THEN 'Coach'
		ELSE Award
	END

UPDATE AADNew.dbo.esj20112012
SET Sport = LTRIM(REPLACE(REPLACE(REPLACE(Sport, '7th', ''), '8th', ''), 'Freshman', ''))

UPDATE AADNew.dbo.esj20112012
SET Award = 'Celeste Hampton Distinguished Character Award'
WHERE Award = 'CHDCA'
*/

SELECT *
FROM AADNew.dbo.esj20112012

DELETE FROM dbo.StudentParticipationRaw
WHERE SchoolYearStart = 2011

INSERT INTO dbo.StudentParticipationRaw
	(StudentID,SchoolYearStart,FinancialAidCode,SportCode,LevelCode,SourceCode)
SELECT
	ESJ.StudentID,
	2011,
	ST.FinancialAidCode,
	SP.SportCode,
	L.LevelCode,
	'ESJ'
FROM dbo.esj20112012 ESJ
	JOIN dbo.Student ST ON ST.StudentID = ESJ.StudentID
	JOIN dbo.Sport SP ON SP.SPortName = ESJ.Sport
	JOIN dbo.Level L ON L.LevelName = ESJ.SportLevel
	
DELETE FROM dbo.StudentAwardRaw
WHERE SchoolYearStart = 2011

INSERT INTO dbo.StudentAwardRaw
	(StudentID,SchoolYearStart,AwardName,SportCode,LevelCode,SourceCode)
SELECT
	ESJ.StudentID,
	2011,
	ESJ.Award,
	SP.SportCode,
	L.LevelCode,
	'ESJ'
FROM dbo.esj20112012 ESJ
	JOIN dbo.Student ST ON ST.StudentID = ESJ.StudentID
	JOIN dbo.Sport SP ON SP.SportName = ESJ.Sport
	JOIN dbo.Level L ON L.LevelName = ESJ.AwardLevel
WHERE ESJ.Award IS NOT NULL		
	
	
