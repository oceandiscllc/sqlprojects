USE AADNew
GO

--Begin schema validation: Archive
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WITH (NOLOCK) WHERE S.Name = 'Archive')
	BEGIN
	
	DECLARE @cSQL varchar(max)
	SET @cSQL = 'CREATE SCHEMA Archive'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema validation: Archive

DECLARE @cTableName varchar(250)

SET @cTableName = 'Archive.Contest'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE Archive.Contest
	
	END
--ENDIF

CREATE TABLE Archive.Contest
	(
	ContestID int IDENTITY(1,1) NOT NULL,
	ContestName varchar(500),
	ContestDate date,
	ContestTime time,
	PreContestTime time,
	Site varchar(5),
	Venue varchar(250),
	TeamName varchar(250),
	IsDistrictContest bit,
	IsForRecord bit,
	ContestResultStatusName varchar(50),
	OwnScore varchar(20),
	OpponentScore varchar(20),
	Results varchar(500),
	OldID int
	)

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'IsDistrictContest', 0
EXEC Utility.SetDefault @cTableName, 'IsForRecord', 0
EXEC Utility.SetDefault @cTableName, 'OldID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'IsDistrictContest', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForRecord', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ContestID'
EXEC Utility.SetIndexClustered 'IX_Contest', @cTableName, 'TeamName ASC, ContestDate ASC'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'Archive.ContestOpponent'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE Archive.ContestOpponent
	
	END
--ENDIF

CREATE TABLE Archive.ContestOpponent
	(
	ContestOpponentID int IDENTITY(1,1) NOT NULL,
	ContestID int,
	OpponentName varchar(250)
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ContestID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ContestOpponentID'
EXEC Utility.SetIndexClustered 'IX_ContestOpponent', @cTableName, 'ContestID ASC, OpponentName ASC'
GO

INSERT INTO AADNew.Archive.Contest
	(OldID,ContestName,ContestDate,ContestTime,PreContestTime,Site,Venue,TeamName,IsDistrictContest)
SELECT 
	E.EventID,
	E.EventName,
	CAST(LTRIM(RTRIM(E.EventDate)) as date) AS EventDate,
	CASE WHEN E.StartTime = 'TBA' OR E.StartTime IS NULL THEN NULL ELSE CAST(LTRIM(RTRIM(E.StartTime)) as time) END AS StartTime,
	CASE WHEN E.PreEventTime = 'TBA' OR E.PreEventTime IS NULL THEN NULL ELSE CAST(LTRIM(RTRIM(E.PreEventTime)) as time) END AS PreEventTime,
	E.Site,
	L.LocationName,
	
	SLG.SportName +
		
		CASE
			WHEN SLG.LevelName IS NOT NULL AND LEN(LTRIM(SLG.LevelName)) > 0
			THEN + ' ' + SLG.LevelName
			ELSE ''
		END + 
			
		CASE
			WHEN SLG.GenderCode IS NOT NULL AND LEN(LTRIM(SLG.GenderCode)) > 0
			THEN + ' (' + SLG.GenderCode + ')'
			ELSE ''
		END,

	CASE 
		WHEN SLG.LevelCode = 'V'
		THEN ISNULL(CAST(E.IsDistrictContest AS bit), 0)
		ELSE 0
	END AS IsDistrictContest

FROM 
	(
	SELECT 
		A1.ObjectID AS EventID, 
		A1.AttributeName, 
		A1.AttributeValue
	FROM AAD.AAD1001.Attributes A1
		JOIN AAD.AAD1001.Objects O1 ON O1.ObjectID = A1.ObjectID
			--AND O1.EffectiveDate >= @nDateNumberStart
			AND O1.EffectiveDate <= 20110630
		JOIN AAD.AAD.Types T1 ON T1.TypeID = O1.ObjectTypeID
			AND T1.TypeCategory = 'Object'
			AND T1.TypeName = 'Contest'

	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (EventDate,EventName,IsCanceled,IsDistrictContest,PreEventTime,Site,StartTime)) AS E
JOIN AAD.AAD1001.Associations A3 ON A3.Node1ID = E.EventID
	JOIN AAD.AAD.Types T3 ON T3.TypeID = A3.AssociationTypeID
	AND T3.TypeCategory = 'ObjectAssociation'
	AND T3.TypeName = 'Object - Venue'
JOIN AAD.AAD1001.Associations A5 ON A5.Node1ID = E.EventID
JOIN AAD.AAD.Types T5 ON T5.TypeID = A5.AssociationTypeID
	AND T5.TypeCategory = 'ObjectAssociation'
	AND T5.TypeName = 'Object - Sport - Level - Gender'
JOIN (SELECT * FROM AAD.AAD1001.GetSportLevelGenderRS()) SLG ON SLG.SportID = A5.Node2ID
	AND SLG.LevelID = A5.Node3ID
	AND SLG.GenderID = A5.Node4ID
JOIN AADNew.dbo.Location L ON L.OldID = A3.Node2ID
	AND 
		(
		E.IsCanceled IS NULL
			OR CAST(E.IsCanceled as bit) = 0
		)
	AND EventDate IS NOT NULL
ORDER BY EventID
GO

TRUNCATE TABLE AADNew.Archive.ContestOpponent
GO

INSERT INTO AADNew.Archive.ContestOpponent
	(ContestID, OpponentName)
SELECT 
	C.ContestID,
	L.MailName
FROM AAD.AAD1001.Associations A
	JOIN AAD.AAD.Types T ON T.TypeID = A.AssociationTypeID
		AND T.TypeCategory = 'ObjectAssociation'
		AND T.TypeName = 'Contest - ContestOpponent'
	JOIN AADNew.Archive.Contest C ON C.OldID = A.Node1ID
	JOIN AADNew.dbo.Location L ON L.OldID = A.Node2ID	
GO

DECLARE @nEventID int
DECLARE @cContestOutcomeName varchar(50)
DECLARE @nIsForRecord bit
DECLARE @cDescription varchar(250)
DECLARE @cOpponentScore varchar(20)
DECLARE @cOwnScore varchar(20)
DECLARE @cText varchar(500)

DECLARE @tTable TABLE 
	(
	EventID int, 
	DisplaySequence int, 
	ContestOutcomeName varchar(50),
	IsForRecord bit,
	Description varchar(250),
	OpponentScore varchar(20),
	OwnScore varchar(20)
	)

DECLARE @tResults TABLE 
	(
	EventID int, 
	IsForRecord bit,
	Results varchar(500)
	)

INSERT INTO @tTable
	(
	EventID,DisplaySequence,ContestOutcomeName,IsForRecord,Description,OpponentScore,OwnScore
	)
SELECT 
	A2.Node1ID AS EventID,
	A2.DisplaySequence,
	CO.ContestOutcomeName,
	PVT.IsForRecord,
	PVT.Description,
	PVT.OpponentScore,
	PVT.OwnScore
FROM 
	(
	SELECT 
		A1.ObjectID, 
		A1.AttributeName, 
		A1.AttributeValue
	FROM AAD.AAD1001.Attributes A1
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A1.ObjectID
		JOIN AAD.AAD.Types T1 ON T1.TypeID = O.ObjectTypeID
			AND T1.TypeCategory = 'Object'
			AND T1.TypeName = 'ContestResult'
	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (Description,IsForRecord,OpponentScore,OwnScore)) AS PVT
		JOIN AAD.AAD1001.Associations A2 ON A2.Node2ID = PVT.ObjectID
		JOIN AAD.AAD.Types T2 ON T2.TypeID = A2.AssociationTypeID
			AND T2.TypeCategory = 'ObjectAssociation'
			AND T2.TypeName = 'Contest - ContestResult - ContestOutcome'
		JOIN AAD.AAD1001.Associations A3 ON A3.Node1ID = A2.Node1ID
		JOIN AAD.AAD.Types T3 ON T3.TypeID = A3.AssociationTypeID
			AND T3.TypeCategory = 'ObjectAssociation'
			AND T3.TypeName = 'Object - Sport - Level - Gender'
		JOIN (SELECT * FROM AAD.AAD.GetContestOutcomeRS()) CO ON CO.ContestOutcomeID = A2.Node3ID
ORDER BY EventID, DisplaySequence		

DELETE 
FROM @tTable 
WHERE Description IS NULL 
	AND OpponentScore IS NULL 
	AND OwnScore IS NULL 

UPDATE C
SET 
	C.ContestResultStatusName = T1.ContestOutcomeName,
	C.Results = T1.Description,
	C.IsForRecord = T1.IsForRecord,
	C.OwnScore = T1.OwnScore,
	C.OpponentScore = T1.OpponentScore
FROM AADNew.Archive.Contest C
	JOIN @tTable T1 ON T1.EventID = C.OldID
		AND EXISTS
			(
			SELECT 
				COUNT(EventID),
				EventID 
			FROM @tTable T2
			WHERE T1.EventID = T2.EventID
			GROUP BY EventID
			HAVING COUNT(EventID) = 1
			)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		EventID,
		ContestOutcomeName,
		IsForRecord,
		Description,
		OpponentScore,
		OwnScore
	FROM @tTable T1
	WHERE EXISTS
		(
		SELECT 
			COUNT(EventID),
			EventID 
		FROM @tTable T2
		WHERE T1.EventID = T2.EventID
		GROUP BY EventID
		HAVING COUNT(EventID) > 1
		)
	ORDER BY EventID, DisplaySequence	

OPEN oCursor
FETCH oCursor INTO @nEventID,@cContestOutcomeName,@nIsForRecord,@cDescription,@cOpponentScore,@cOwnScore
WHILE @@fetch_status = 0
	BEGIN	
	
	SET @cText = ''

	IF EXISTS (SELECT 1 FROM @tResults R WHERE R.EventID = @nEventID)
		BEGIN
		
		SELECT @cText = R.Results
		FROM @tResults R 
		WHERE R.EventID = @nEventID

		UPDATE @tResults
		SET 
			IsForRecord = @nIsForRecord,
			Results = LTRIM(RTRIM(@cText + ' ' + ISNULL(@cContestOutcomeName, '') + ' ' + ISNULL(@cDescription, '') + ' ' + ISNULL(@cOwnScore, '') + ' / ' + ISNULL(@cOpponentScore, '')))
		WHERE EventID = @nEventID
		
		END
	ELSE
		BEGIN

		INSERT INTO @tResults
			(EventID,IsForRecord,Results)
		VALUES
			(
			@nEventID,
			@nIsForRecord,
			LTRIM(RTRIM(@cText + 
				ISNULL(@cContestOutcomeName, '') + ' ' + 
				ISNULL(@cDescription, '') + ' ' + 
				ISNULL(@cOwnScore, '') + ' / ' + 
				ISNULL(@cOpponentScore, '')
				))
			)
		
		END
	--ENDIF
	
	FETCH oCursor INTO @nEventID,@cContestOutcomeName,@nIsForRecord,@cDescription,@cOpponentScore,@cOwnScore

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE C
SET 
	C.ContestResultStatusName = 'Reported / Not Required',
	C.Results = R.Results,
	C.IsForRecord = R.IsForRecord
FROM AADNew.Archive.Contest C
	JOIN @tResults R ON R.EventID = C.OldID