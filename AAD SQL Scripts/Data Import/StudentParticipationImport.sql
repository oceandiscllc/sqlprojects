TRUNCATE TABLE dbo.StudentParticipation
GO

INSERT INTO dbo.StudentParticipation
	(StudentID,SchoolYearStart,SportID,LevelID)
SELECT
	SAR.StudentID,
	SAR.SchoolYearStart,
	S.SportID,
	L.LevelID
FROM dbo.StudentParticipationRaw SAR
	JOIN dbo.Sport S ON S.SportCode = SAR.SportCode
	JOIN dbo.Level L ON L.LevelCode = SAR.LevelCode
	
UPDATE SP
SET SP.TeamID = T1.TeamID
FROM dbo.StudentParticipation SP
	JOIN dbo.Team T1 ON T1.SportID = SP.SportID
		AND T1.LevelID = SP.LevelID
		AND 
			(
			T1.GenderID = SP.GenderID
				OR T1.GenderID = 0
			)
		AND EXISTS
			(
			SELECT D.TeamID
			FROM
				(
				SELECT
					COUNT(T2.TeamID) AS ItemCount,
					T2.TeamID,
					T2.SportID,
					T2.LevelID,
					T2.GenderID
				FROM dbo.Team T2
				GROUP BY T2.TeamID, T2.SportID,	T2.LevelID, T2.GenderID
				HAVING COUNT(T2.TeamID) = 1
				) D
			WHERE D.TeamID = T1.TeamID
			)
		AND SP.SchoolYearStart = 2011
	
UPDATE SP
SET SP.FinancialAidCode = S.FinancialAidCode
FROM dbo.StudentParticipation SP
	JOIN dbo.Student S ON S.StudentID = SP.StudentID
		AND SP.SchoolYearStart = 2011
