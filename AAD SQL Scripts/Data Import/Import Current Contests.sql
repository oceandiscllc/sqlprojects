USE AADNew
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.Contest'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE dbo.Contest
	
	END
--ENDIF

CREATE TABLE dbo.Contest
	(
	ContestID int IDENTITY(1,1) NOT NULL,
	ContestName varchar(500),
	ContestDate date,
	ContestTime time,
	PreContestTime time,
	Site varchar(5),
	LocationID int,
	TeamID int,
	IsCanceled bit,
	IsDistrictContest bit,
	IsForRecord bit,
	IsTransportationRequired bit,
	IsWebApproved bit,
	ContestResultStatusID int,
	OwnScore varchar(20),
	OpponentScore varchar(20),
	Results varchar(500),
	OldID int
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ContestResultStatusID', 1
EXEC Utility.SetDefault @cTableName, 'IsCanceled', 0
EXEC Utility.SetDefault @cTableName, 'IsDistrictContest', 0
EXEC Utility.SetDefault @cTableName, 'IsForRecord', 1
EXEC Utility.SetDefault @cTableName, 'IsTransportationRequired', 0
EXEC Utility.SetDefault @cTableName, 'IsWebApproved', 0
EXEC Utility.SetDefault @cTableName, 'LocationID', 0
EXEC Utility.SetDefault @cTableName, 'OldID', 0
EXEC Utility.SetDefault @cTableName, 'TeamID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestResultStatusID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsCanceled', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsDistrictContest', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsForRecord', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsTransportationRequired', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'IsWebApproved', 'bit'
EXEC Utility.SetColumnNotNull @cTableName, 'LocationID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'OldID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'TeamID', 'int'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ContestID'
EXEC Utility.SetIndexNonClustered 'IX_Contest', @cTableName, 'ContestDate ASC, ContestTime ASC, ContestID ASC'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ContestOpponent'

IF NOT EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	CREATE TABLE dbo.ContestOpponent
		(
		ContestOpponentID int IDENTITY(1,1) NOT NULL,
		ContestID int,
		LocationID int
		)
	
	END
--ENDIF

EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'ContestID', 0
EXEC Utility.SetDefault @cTableName, 'LocationID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestID', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'LocationID', 'int'

EXEC Utility.SetPrimaryKeyNonClustered @cTableName, 'ContestOpponentID'
EXEC Utility.SetIndexClustered 'IX_ContestOpponent', @cTableName, 'ContestID ASC, LocationID ASC'
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ContestResultStatus'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN
	
	DROP TABLE dbo.ContestResultStatus
	
	END
--ENDIF

CREATE TABLE dbo.ContestResultStatus
	(
	ContestResultStatusID int IDENTITY(1,1) NOT NULL,
	ContestResultStatusName varchar(50),
	ContestResultStatusCode varchar(50),
	DisplayOrder int,
	IsActive bit
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 0

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ContestResultStatusID'
EXEC Utility.SetIndexNonClustered 'IX_Contest', @cTableName, 'DisplayOrder ASC, ContestResultStatusName ASC'
GO

INSERT INTO dbo.ContestResultStatus (ContestResultStatusName,ContestResultStatusCode,DisplayOrder) VALUES ('Not Reported','NotReported',1)
INSERT INTO dbo.ContestResultStatus (ContestResultStatusName,ContestResultStatusCode,DisplayOrder) VALUES ('Reported / Not Required','NotRequired',2)
INSERT INTO dbo.ContestResultStatus (ContestResultStatusName,ContestResultStatusCode,DisplayOrder) VALUES ('Win','Win',3)
INSERT INTO dbo.ContestResultStatus (ContestResultStatusName,ContestResultStatusCode,DisplayOrder) VALUES ('Loss','Loss',4)
INSERT INTO dbo.ContestResultStatus (ContestResultStatusName,ContestResultStatusCode,DisplayOrder) VALUES ('Tie','Tie',5)
GO

DECLARE @cTableName varchar(250)

SET @cTableName = 'dbo.ContestResultType'

IF EXISTS (SELECT 1 FROM sys.objects O WITH (NOLOCK) WHERE O.object_id = OBJECT_ID(@cTableName) AND O.type in ('U'))
	BEGIN

	DROP TABLE dbo.ContestResultType

	END
--ENDIF
	
CREATE TABLE dbo.ContestResultType
	(
	ContestResultTypeID int IDENTITY(1,1) NOT NULL,
	ContestResultTypeName varchar(50),
	ContestResultTypeCode varchar(50),
	DisplayOrder int,
	IsActive bit
	)
	
EXEC Utility.DropConstraintsAndIndexes @cTableName

EXEC Utility.SetDefault @cTableName, 'DisplayOrder', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'DisplayOrder', 'int'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'bit'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'ContestResultTypeID'
EXEC Utility.SetIndexNonClustered 'IX_Contest', @cTableName, 'DisplayOrder ASC, ContestResultTypeName ASC'
GO

INSERT INTO dbo.ContestResultType (ContestResultTypeName,ContestResultTypeCode,DisplayOrder) VALUES ('Scores','Scores',1)
INSERT INTO dbo.ContestResultType (ContestResultTypeName,ContestResultTypeCode,DisplayOrder) VALUES ('Free Text','Text',2)
INSERT INTO dbo.ContestResultType (ContestResultTypeName,ContestResultTypeCode,DisplayOrder) VALUES ('None','None',3)
GO

INSERT INTO AADNew.dbo.Contest
	(OldID,ContestName,ContestDate,ContestTime,PreContestTime,Site,LocationID,TeamID,IsCanceled,IsDistrictContest,IsForRecord,IsWebApproved)
SELECT 
	E.EventID,

	CASE
		WHEN E.EventName IS NULL OR LEN(RTRIM(E.EventName)) = 0
		THEN NULL
		ELSE E.EventName
	END AS EventName,
	
	E.EventDate,
	CASE WHEN E.StartTime = 'TBA' OR E.StartTime IS NULL THEN NULL ELSE CAST(LTRIM(RTRIM(E.StartTime)) as time) END AS StartTime,
	CASE WHEN E.PreEventTime = 'TBA' OR E.PreEventTime IS NULL THEN NULL ELSE CAST(LTRIM(RTRIM(E.PreEventTime)) as time) END AS PreEventTime,
	E.Site,
	ISNULL((
	SELECT L.LocationID
	FROM AADNew.dbo.Location L
	WHERE L.OldID = O3.ObjectID
	), 0) AS LocationID,

	(
	SELECT T.TeamID
	FROM AADNew.dbo.Team T
		JOIN AADNew.dbo.SLGTeam SLGT ON SLGT.SportCode = SLG.SportCode
			AND SLGT.LevelCode = SLG.LevelCode
			AND SLGT.GenderCode = SLG.GenderCode
			AND SLGT.TeamCode = T.TeamCode
	) AS TeamID,

--SLG.SportCode,
--SLG.LevelCode,
--SLG.GenderCode,

	CASE
		WHEN E.IsCanceled = '1'
		THEN 1
		ELSE 0
	END AS IsCanceled,

	CASE 
		WHEN SLG.LevelCode = 'V'
		THEN CAST(E.IsDistrictContest AS int)
		ELSE 0
	END AS IsDistrict,
	
	CAST(E.IsWebApproved AS bit) AS IsForRecord,
	CAST(E.IsWebApproved AS bit) AS IsWebApproved

FROM 
	(
	SELECT 
		A1.ObjectID AS EventID, 
		A1.AttributeName, 
		A1.AttributeValue
	FROM AAD.AAD1001.Attributes A1
		JOIN AAD.AAD1001.Objects O1 ON O1.ObjectID = A1.ObjectID
			AND O1.EffectiveDate >= 20110701--@nDateNumberStart
			--AND O1.EffectiveDate <= 20120630--@nDateNumberStop
			--AND O1.ObjectID IN (SELECT ID FROM AAD.AAD1001.GetAssociationRS(@cSLGID, 'Object - Sport - Level - Gender'))
		JOIN AAD.AAD.Types T1 ON T1.TypeID = O1.ObjectTypeID
			AND T1.TypeCategory = 'Object'
			AND T1.TypeName = 'Contest'

	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (EventDate,EventName,IsCanceled,IsDistrictContest,IsForRecord,IsWebApproved,PreEventTime,Site,StartTime)) AS E

JOIN AAD.AAD1001.Associations A3 ON A3.Node1ID = E.EventID
JOIN AAD.AAD.Types T3 ON T3.TypeID = A3.AssociationTypeID
	AND T3.TypeCategory = 'ObjectAssociation'
	AND T3.TypeName = 'Object - Venue'
JOIN AAD.AAD1001.Objects O3 ON O3.ObjectID = A3.Node2ID
JOIN AAD.AAD1001.Associations A5 ON A5.Node1ID = E.EventID
JOIN AAD.AAD.Types T5 ON T5.TypeID = A5.AssociationTypeID
	AND T5.TypeCategory = 'ObjectAssociation'
	AND T5.TypeName = 'Object - Sport - Level - Gender'
JOIN (SELECT * FROM AAD.AAD1001.GetSportLevelGenderRS()) SLG ON SLG.SportID = A5.Node2ID
	AND SLG.LevelID = A5.Node3ID
	AND SLG.GenderID = A5.Node4ID
	AND NOT EXISTS
		(
		SELECT 1
		FROM AADNew.dbo.Contest C
		WHERE C.OldID = E.EventID
		)
GO

DECLARE @nEventID int
DECLARE @cContestOutcomeName varchar(50)
DECLARE @nIsForRecord bit
DECLARE @cDescription varchar(250)
DECLARE @cOpponentScore varchar(20)
DECLARE @cOwnScore varchar(20)
DECLARE @cText varchar(500)

DECLARE @tTable TABLE 
	(
	EventID int, 
	DisplaySequence int, 
	ContestOutcomeName varchar(50),
	IsForRecord bit,
	Description varchar(250),
	OpponentScore varchar(20),
	OwnScore varchar(20)
	)

DECLARE @tResults TABLE 
	(
	EventID int, 
	IsForRecord bit,
	Results varchar(500)
	)

INSERT INTO @tTable
	(
	EventID,DisplaySequence,ContestOutcomeName,IsForRecord,Description,OpponentScore,OwnScore
	)
SELECT 
	A2.Node1ID AS EventID,
	A2.DisplaySequence,
	CO.ContestOutcomeName,
	PVT.IsForRecord,
	PVT.Description,
	PVT.OpponentScore,
	PVT.OwnScore
FROM 
	(
	SELECT 
		A1.ObjectID, 
		A1.AttributeName, 
		A1.AttributeValue
	FROM AAD.AAD1001.Attributes A1
		JOIN AAD.AAD1001.Objects O ON O.ObjectID = A1.ObjectID
		JOIN AAD.AAD.Types T1 ON T1.TypeID = O.ObjectTypeID
			AND T1.TypeCategory = 'Object'
			AND T1.TypeName = 'ContestResult'
	) AS PD
PIVOT
	( 
	MAX(PD.AttributeValue)
	FOR PD.AttributeName IN (Description,IsForRecord,OpponentScore,OwnScore)) AS PVT
		JOIN AAD.AAD1001.Associations A2 ON A2.Node2ID = PVT.ObjectID
		JOIN AAD.AAD.Types T2 ON T2.TypeID = A2.AssociationTypeID
			AND T2.TypeCategory = 'ObjectAssociation'
			AND T2.TypeName = 'Contest - ContestResult - ContestOutcome'
		JOIN AAD.AAD1001.Associations A3 ON A3.Node1ID = A2.Node1ID
		JOIN AAD.AAD.Types T3 ON T3.TypeID = A3.AssociationTypeID
			AND T3.TypeCategory = 'ObjectAssociation'
			AND T3.TypeName = 'Object - Sport - Level - Gender'
		JOIN (SELECT * FROM AAD.AAD.GetContestOutcomeRS()) CO ON CO.ContestOutcomeID = A2.Node3ID
ORDER BY EventID, DisplaySequence		

DELETE 
FROM @tTable 
WHERE Description IS NULL 
	AND OpponentScore IS NULL 
	AND OwnScore IS NULL 

UPDATE C
SET 
	C.ContestResultStatusID = 
	
		CASE
			WHEN EXISTS (SELECT 1 FROM AADNew.dbo.ContestResultStatus CRS WHERE CRS.ContestResultStatusName = T1.ContestOutcomeName)
			THEN (SELECT CRS.ContestResultStatusID FROM AADNew.dbo.ContestResultStatus CRS WHERE CRS.ContestResultStatusName = T1.ContestOutcomeName)
			ELSE 1
		END,
		
	C.Results = T1.Description,
	C.IsForRecord = T1.IsForRecord,
	C.OwnScore = T1.OwnScore,
	C.OpponentScore = T1.OpponentScore
FROM AADNew.dbo.Contest C
	JOIN @tTable T1 ON T1.EventID = C.OldID
		AND EXISTS
			(
			SELECT 
				COUNT(EventID),
				EventID 
			FROM @tTable T2
			WHERE T1.EventID = T2.EventID
			GROUP BY EventID
			HAVING COUNT(EventID) = 1
			)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		EventID,
		ContestOutcomeName,
		IsForRecord,
		Description,
		OpponentScore,
		OwnScore
	FROM @tTable T1
	WHERE EXISTS
		(
		SELECT 
			COUNT(EventID),
			EventID 
		FROM @tTable T2
		WHERE T1.EventID = T2.EventID
		GROUP BY EventID
		HAVING COUNT(EventID) > 1
		)
	ORDER BY EventID, DisplaySequence	

OPEN oCursor
FETCH oCursor INTO @nEventID,@cContestOutcomeName,@nIsForRecord,@cDescription,@cOpponentScore,@cOwnScore
WHILE @@fetch_status = 0
	BEGIN	
	
	SET @cText = ''

	IF EXISTS (SELECT 1 FROM @tResults R WHERE R.EventID = @nEventID)
		BEGIN
		
		SELECT @cText = R.Results
		FROM @tResults R 
		WHERE R.EventID = @nEventID

		UPDATE @tResults
		SET 
			IsForRecord = @nIsForRecord,
			Results = LTRIM(RTRIM(@cText + ' ' + ISNULL(@cContestOutcomeName, '') + ' ' + ISNULL(@cDescription, '') + ' ' + ISNULL(@cOwnScore, '') + ' / ' + ISNULL(@cOpponentScore, '')))
		WHERE EventID = @nEventID
		
		END
	ELSE
		BEGIN

		INSERT INTO @tResults
			(EventID,IsForRecord,Results)
		VALUES
			(
			@nEventID,
			@nIsForRecord,
			LTRIM(RTRIM(@cText + 
				ISNULL(@cContestOutcomeName, '') + ' ' + 
				ISNULL(@cDescription, '') + ' ' + 
				ISNULL(@cOwnScore, '') + ' / ' + 
				ISNULL(@cOpponentScore, '')
				))
			)
		
		END
	--ENDIF
	
	FETCH oCursor INTO @nEventID,@cContestOutcomeName,@nIsForRecord,@cDescription,@cOpponentScore,@cOwnScore

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor

UPDATE C
SET 
	C.ContestResultStatusID = (SELECT CRS.ContestResultStatusID FROM AADNew.dbo.ContestResultStatus CRS WHERE CRS.ContestResultStatusCode = 'NotRequired'),
	C.Results = R.Results,
	C.IsForRecord = R.IsForRecord
FROM AADNew.dbo.Contest C
	JOIN @tResults R ON R.EventID = C.OldID
GO

UPDATE AADNew.dbo.Contest
SET ContestTime = NULL
WHERE ContestTime = '00:00:00.0000000'
GO

UPDATE AADNew.dbo.Contest
SET PreContestTime = NULL
WHERE PreContestTime = '00:00:00.0000000'
GO

TRUNCATE TABLE AADNew.dbo.ContestOpponent
GO

INSERT INTO AADNew.dbo.ContestOpponent
	(ContestID,LocationID)
SELECT
	C.ContestID,
	L.LocationID
FROM AAD.AAD1001.Associations A1
	JOIN AAD.AAD.Types T2 ON T2.TypeID = A1.AssociationTypeID
		AND T2.TypeCategory = 'ObjectAssociation'
		AND T2.TypeName = 'Contest - ContestOpponent'
	JOIN AADNew.dbo.Contest C ON C.OldID = A1.Node1ID
	JOIN AADNew.dbo.Location L ON L.OldID = A1.Node2ID
GO