USE AAD
GO

--Begin procedure Utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('Utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE Utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2012.11.14
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE Utility.DropObject
	@cObjectName VARCHAR(max)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)

IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cObjectName) AND O.type IN ('FN','IF','TF','FS','FT'))
	BEGIN
	
	SET @cSQL = 'DROP FUNCTION ' + @cObjectName
	EXEC (@cSQL)
	
	END
ELSE IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cObjectName) AND O.type IN ('P','PC'))
	BEGIN
	
	SET @cSQL = 'DROP PROCEDURE ' + @cObjectName
	EXEC (@cSQL)
	
	END
ELSE IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cObjectName) AND O.type IN ('U'))
	BEGIN
	
	SET @cSQL = 'DROP TABLE ' + @cObjectName
	EXEC (@cSQL)
	
	END
ELSE IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID(@cObjectName) AND O.type IN ('V'))
	BEGIN
	
	SET @cSQL = 'DROP VIEW ' + @cObjectName
	EXEC (@cSQL)
	
	END
--ENDIF
	
END	
GO
--End procedure Utility.DropObject

--Begin create SetDefault helper stored procedure
EXEC Utility.DropObject 'Utility.SetDefault'
GO

-- ==============================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
--
-- Modify date: 2013.01.13
-- Description:	Added functionality to default a UNIQUEIDENTIFIER
-- ==============================================================
CREATE PROCEDURE Utility.SetDefault
	@cTableName VARCHAR(250),
	@cColumnName VARCHAR(250),
	@cDefault VARCHAR(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(max)
	DECLARE @nDefaultIsNumeric BIT
	DECLARE @nLength INT

	SET @nDefaultIsNumeric = ISNUMERIC(@cDefault)
	IF @cDefault IN ('getDate()','NewID()')
		SET @nDefaultIsNumeric = 1
	--ENDIF

	IF CHARINDEX('.', @cTableName) = 0
		SET @cTableName = 'dbo.' + @cTableName
	
	IF @nDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ''' + @cDefault + ''' WHERE ' + @cColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @cTableName + ' SET ' + @cColumnName + ' = ' + @cDefault + ' WHERE ' + @cColumnName + ' IS NULL'

	EXECUTE (@cSQL)

	SET @nLength = LEN(@cTableName) - CHARINDEX('.', @cTableName)
	SET @cConstraintName = 'DF_' + RIGHT(@cTableName, @nLength) + '_' + @cColumnName
	
	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC WITH (NOLOCK) WHERE DC.parent_object_ID = OBJECT_ID(@cTableName) AND DC.Name = @cConstraintName)
		BEGIN	

		IF @nDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @cDefault + ''' FOR ' + @cColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @cTableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @cDefault + ' FOR ' + @cColumnName
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End create SetDefault helper stored procedure

--Begin table dbo.Vehicle
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.Vehicle'

EXEC Utility.DropObject @cTableName

CREATE TABLE dbo.Vehicle
	(
	VehicleID INT IDENTITY(1,1) NOT NULL,
	VendorID INT,
	VehicleType VARCHAR(50),
	VehicleName VARCHAR(250),
	Capacity INT,
	IsActive BIT
	)

EXEC Utility.SetDefault @cTableName, 'Capacity', 0
EXEC Utility.SetDefault @cTableName, 'IsActive', 1
EXEC Utility.SetDefault @cTableName, 'VendorID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'Capacity', 'INT'
EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'BIT'
EXEC Utility.SetColumnNotNull @cTableName, 'VendorID', 'INT'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'VehicleID'
GO

SET IDENTITY_INSERT dbo.Vehicle ON
INSERT INTO dbo.Vehicle (VehicleID) VALUES (0)
SET IDENTITY_INSERT dbo.Vehicle OFF
GO
--End table dbo.Vehicle

--Begin table dbo.Vendor
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.Vendor'

EXEC Utility.DropObject @cTableName

CREATE TABLE dbo.Vendor
	(
	VendorID INT IDENTITY(1,1) NOT NULL,
	VendorName VARCHAR(250),
	IsActive BIT
	)

EXEC Utility.SetDefault @cTableName, 'IsActive', 1

EXEC Utility.SetColumnNotNull @cTableName, 'IsActive', 'BIT'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'VendorID'
GO
--End table dbo.Vendor

--Begin table dbo.VendorVehicle
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.VendorVehicle'

EXEC Utility.DropObject @cTableName
--End table dbo.VendorVehicle

EXEC Utility.AddColumn 'dbo.Person', 'CDLExpirationDate', 'DATE'
GO

--Begin table dbo.TransportationRequest
DECLARE @cTableName VARCHAR(250)
SET @cTableName = 'dbo.TransportationRequest'

EXEC Utility.DropObject @cTableName

CREATE TABLE dbo.TransportationRequest
	(
	TransportationRequestID INT IDENTITY(1,1) NOT NULL,
	ContestID INT,
	DepartureDate DATE,
	DepartureTime TIME,
	Destination VARCHAR(250),
	DriverName VARCHAR(250),
	EventDate DATE,
	EventTime TIME,
	HaveDriver BIT,
	PassengerCount INT,
	RequestDateTime DATETIME,
	RequestorComments VARCHAR(MAX),
	RequestorConfirmationCode UNIQUEIDENTIFIER,
	RequestorDepartment VARCHAR(250),
	RequestorEmail VARCHAR(320),
	RequestorGroup VARCHAR(250),
	RequestorName VARCHAR(250),
	ReturnDate DATE,
	ReturnTime TIME,
	TripType VARCHAR(50),
	Status VARCHAR(50),
	AdministratorComments VARCHAR(MAX),
	VehicleID INT,
	VehicleBlockStartDateTime DATETIME,
	VehicleBlockStopDateTime DATETIME
	)

EXEC Utility.SetDefault @cTableName, 'ContestID', 0
EXEC Utility.SetDefault @cTableName, 'HaveDriver', 0
EXEC Utility.SetDefault @cTableName, 'PassengerCount', 0
EXEC Utility.SetDefault @cTableName, 'RequestDateTime', 'getDate()'
EXEC Utility.SetDefault @cTableName, 'RequestorConfirmationCode', 'NewID()'
EXEC Utility.SetDefault @cTableName, 'VehicleID', 0

EXEC Utility.SetColumnNotNull @cTableName, 'ContestID', 'INT'
EXEC Utility.SetColumnNotNull @cTableName, 'HaveDriver', 'BIT'
EXEC Utility.SetColumnNotNull @cTableName, 'PassengerCount', 'INT'
EXEC Utility.SetColumnNotNull @cTableName, 'RequestDateTime', 'DATETIME'
EXEC Utility.SetColumnNotNull @cTableName, 'RequestorConfirmationCode', 'UNIQUEIDENTIFIER'
EXEC Utility.SetColumnNotNull @cTableName, 'VehicleID', 'INT'

EXEC Utility.SetPrimaryKeyClustered @cTableName, 'TransportationRequestID'
GO
--End table dbo.TransportationRequest

--Begin Function dbo.RequestorConfirmationCodeFormat
EXEC Utility.DropObject 'dbo.RequestorConfirmationCodeFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date: 2013.01.13
-- Description: A function to format RequestorConfirmationCode data
-- ================================================================
CREATE FUNCTION dbo.RequestorConfirmationCodeFormat
(
@cRequestorConfirmationCode UNIQUEIDENTIFIER
)

RETURNS VARCHAR(14)

AS
BEGIN

DECLARE @cRequestorConfirmationCodeFormatted varchar(14)

SET @cRequestorConfirmationCodeFormatted = ''

IF @cRequestorConfirmationCode IS NOT NULL
	SET @cRequestorConfirmationCodeFormatted = STUFF(STUFF(RIGHT(@cRequestorConfirmationCode, 12), 9, 0, '-'), 5, 0, '-')
--ENDIF

RETURN @cRequestorConfirmationCodeFormatted
END
GO
--End Function dbo.RequestorConfirmationCodeFormat