USE HermisCloud
GO

DECLARE @nPadLength INT

SELECT @nPadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
FROM core.MenuItem MI

;
WITH HD (DisplayIndex,Lineage,MenuItemID,ParentMenuItemID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
		CAST(MI.MenuItemText AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		1
	FROM core.MenuItem MI
	WHERE MI.ParentMenuItemID = 0

	UNION ALL

	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
		CAST(HD.Lineage  + ' > ' + MI.MenuItemText AS VARCHAR(MAX)),
		MI.MenuItemID,
		MI.ParentMenuItemID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM core.MenuItem MI
		JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
	)

SELECT
	HD1.DisplayIndex,
	MI.DisplayOrder,
	HD1.NodeLevel,
	HD1.ParentMenuItemID,
	HD1.MenuItemID,
	HD1.Lineage,
	MI2.MenuItemCode AS ParentMenuItemCode,
	MI.MenuItemCode,
	MI.MenuItemText,
	MI.MenuItemLink,
	(SELECT STUFF((SELECT ', ' + MIPL.PermissionableLineage FROM core.MenuItemPermissionableLineage MIPL WHERE MIPL.MenuItemID = MI.MenuItemID ORDER BY MIPL.MenuItemID FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'),1,2,'')) AS PermissionableLineageList,
	MI.Icon,

	CASE
		WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
		THEN 1
		ELSE 0
	END AS HasChildren

FROM HD HD1
	JOIN core.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
	LEFT JOIN core.MenuItem MI2 ON MI2.MenuItemID = MI.ParentMenuItemID
WHERE MI.IsActive = 1
ORDER BY HD1.DisplayIndex

/*
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Community', @NewMenuItemLink=NULL
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Community', @NewMenuItemCode='CommunityList', @NewMenuItemLink='/community/list', @NewMenuItemText='Communities', @PermissionableLineageList='Community.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Community', @NewMenuItemCode='AssetList', @NewMenuItemLink='/community/list', @AfterMenuItemCode='CommunityList'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Community'
GO

@AfterMenuItemCode, 
@BeforeMenuItemCode, 
@DeleteMenuItemCode, 
@Icon, 
@IsActive, 
@NewMenuItemCode, 
@NewMenuItemLink, 
@NewMenuItemText, 
@ParentMenuItemCode, 
@PermissionableLineageList
*/	
