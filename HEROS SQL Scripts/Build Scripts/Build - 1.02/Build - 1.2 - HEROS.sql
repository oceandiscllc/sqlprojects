-- File Name:	Build - 1.2 - HEROS.sql
-- Build Key:	Build - 1.2 - 2018.06.08 21.15.30

USE HEROS
GO

-- ==============================================================================================================================
-- Tables:
--		territory.ProjectTerritory
--		territory.ProjectTerritoryType
--
-- Procedures:
--		person.GeneratePassword
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE HEROS
GO

--Begin table territory.ProjectTerritory
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritory
	(
	ProjectTerritoryID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritory', 'ProjectID,TerritoryID'
GO
--End table territory.ProjectTerritoryType

--Begin table territory.ProjectTerritoryType
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritoryType'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritoryType
	(
	ProjectTerritoryTypeID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryTypeID INT,
	ParentTerritoryTypeCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryTypeID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritoryType', 'TerritoryTypeID, ParentTerritoryTypeCode'
GO
--End table territory.ProjectTerritoryType
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE HEROS
GO

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO
--End function person.HashPassword


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE HEROS
GO

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE HEROS
GO

--Begin table dropdown.Project
UPDATE P
SET P.ProjectName = 'DFID - Chase'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project1'
GO

UPDATE P
SET P.ProjectName = 'SU'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project2'
GO

UPDATE P
SET P.ProjectName = 'Syria Response Exemplar'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project3'
GO
--End table dropdown.Project

--Begin table dropdown.TerritoryType
IF NOT EXISTS (SELECT 1 FROM dropdown.TerritoryType TT WHERE TT.TerritoryTypeCode = 'Country')
	BEGIN

	INSERT INTO dropdown.TerritoryType
		(TerritoryTypeCode, TerritoryTypeCodePlural, TerritoryTypeName, TerritoryTypeNamePlural)
	VALUES
		('Country', 'Countries', 'Country', 'Countries')

	END
--ENDIF
GO

UPDATE TT
SET TT.IsReadOnly = 1
FROM dropdown.TerritoryType TT
GO
--End table dropdown.TerritoryType

--Begin table territory.ProjectTerritory
TRUNCATE TABLE territory.ProjectTerritory
GO

INSERT INTO territory.ProjectTerritory
	(ProjectID, TerritoryID)
SELECT
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P, territory.Territory T
WHERE P.ProjectCode IN ('Project1', 'Project2')
	AND T.TerritoryTypeCode = 'Country'
GO

INSERT INTO territory.ProjectTerritory
	(ProjectID, TerritoryID)
SELECT
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P, territory.Territory T
WHERE P.ProjectCode = 'Project3'
	AND T.TerritoryTypeCode IN ('Governorate', 'Community')
GO
--End table territory.ProjectTerritory

--Begin table territory.ProjectTerritoryType
INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID
FROM dropdown.Project P, dropdown.TerritoryType TT
WHERE P.ProjectCode IN ('Project1', 'Project2')
	AND TT.TerritoryTypeCode = 'Country'
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID, ParentTerritoryTypeCode)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID,
		
	CASE
		WHEN TT.TerritoryTypeCode = 'Community'
		THEN 'Governorate'
		ELSE 'Country'
	END
			
FROM dropdown.Project P, dropdown.TerritoryType TT
WHERE P.ProjectCode = 'Project3'
	AND TT.TerritoryTypeCode IN ('Governorate', 'Community')
GO
--End table territory.ProjectTerritoryType

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.2 - 2018.06.08 21.15.30')
GO
--End build tracking

