USE HEROS
GO

--Begin table dropdown.Project
UPDATE P
SET P.ProjectName = 'DFID - Chase'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project1'
GO

UPDATE P
SET P.ProjectName = 'SU'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project2'
GO

UPDATE P
SET P.ProjectName = 'Syria Response Exemplar'
FROM dropdown.Project P
WHERE P.ProjectCode = 'Project3'
GO
--End table dropdown.Project

--Begin table dropdown.TerritoryType
IF NOT EXISTS (SELECT 1 FROM dropdown.TerritoryType TT WHERE TT.TerritoryTypeCode = 'Country')
	BEGIN

	INSERT INTO dropdown.TerritoryType
		(TerritoryTypeCode, TerritoryTypeCodePlural, TerritoryTypeName, TerritoryTypeNamePlural)
	VALUES
		('Country', 'Countries', 'Country', 'Countries')

	END
--ENDIF
GO

UPDATE TT
SET TT.IsReadOnly = 1
FROM dropdown.TerritoryType TT
GO
--End table dropdown.TerritoryType

--Begin table territory.ProjectTerritory
TRUNCATE TABLE territory.ProjectTerritory
GO

INSERT INTO territory.ProjectTerritory
	(ProjectID, TerritoryID)
SELECT
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P, territory.Territory T
WHERE P.ProjectCode IN ('Project1', 'Project2')
	AND T.TerritoryTypeCode = 'Country'
GO

INSERT INTO territory.ProjectTerritory
	(ProjectID, TerritoryID)
SELECT
	P.ProjectID,
	T.TerritoryID
FROM dropdown.Project P, territory.Territory T
WHERE P.ProjectCode = 'Project3'
	AND T.TerritoryTypeCode IN ('Governorate', 'Community')
GO
--End table territory.ProjectTerritory

--Begin table territory.ProjectTerritoryType
INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID
FROM dropdown.Project P, dropdown.TerritoryType TT
WHERE P.ProjectCode IN ('Project1', 'Project2')
	AND TT.TerritoryTypeCode = 'Country'
GO

INSERT INTO territory.ProjectTerritoryType
	(ProjectID, TerritoryTypeID, ParentTerritoryTypeCode)
SELECT
	P.ProjectID,
	TT.TerritoryTypeID,
		
	CASE
		WHEN TT.TerritoryTypeCode = 'Community'
		THEN 'Governorate'
		ELSE 'Country'
	END
			
FROM dropdown.Project P, dropdown.TerritoryType TT
WHERE P.ProjectCode = 'Project3'
	AND TT.TerritoryTypeCode IN ('Governorate', 'Community')
GO
--End table territory.ProjectTerritoryType
