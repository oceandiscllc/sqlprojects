USE HEROS
GO

--Begin table territory.ProjectTerritory
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritory
	(
	ProjectTerritoryID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritory', 'ProjectID,TerritoryID'
GO
--End table territory.ProjectTerritoryType

--Begin table territory.ProjectTerritoryType
DECLARE @TableName VARCHAR(250) = 'territory.ProjectTerritoryType'

EXEC utility.DropObject @TableName

CREATE TABLE territory.ProjectTerritoryType
	(
	ProjectTerritoryTypeID INT NOT NULL IDENTITY(1,1),
	ProjectID INT,
	TerritoryTypeID INT,
	ParentTerritoryTypeCode VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryTypeID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectTerritoryTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectTerritoryType', 'TerritoryTypeID, ParentTerritoryTypeCode'
GO
--End table territory.ProjectTerritoryType