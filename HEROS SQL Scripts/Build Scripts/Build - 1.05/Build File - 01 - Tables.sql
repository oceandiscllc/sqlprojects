USE HEROS
GO

--Begin table dropdown.Commodity
DECLARE @TableName VARCHAR(250) = 'dropdown.Commodity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Commodity
	(
	CommodityID INT IDENTITY(0,1) NOT NULL,
	CommodityCode VARCHAR(50),
	CommodityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommodityID'
EXEC utility.SetIndexClustered @TableName, 'IX_Commodity', 'DisplayOrder,CommodityName'
GO
--End table dropdown.Commodity

--Begin table dropdown.EquipmentConsignmentDispositionStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.EquipmentConsignmentDispositionStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EquipmentConsignmentDispositionStatus
	(
	EquipmentConsignmentDispositionStatusID INT IDENTITY(0,1) NOT NULL,
	EquipmentConsignmentDispositionStatusCode VARCHAR(50),
	EquipmentConsignmentDispositionStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentDispositionStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignmentDispositionStatus', 'DisplayOrder,EquipmentConsignmentDispositionStatusName'
GO
--End table dropdown.EquipmentConsignmentDispositionStatus

--Begin table dropdown.SubCommodity
DECLARE @TableName VARCHAR(250) = 'dropdown.SubCommodity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubCommodity
	(
	SubCommodityID INT IDENTITY(0,1) NOT NULL,
	CommodityID INT,
	SubCommodityCode VARCHAR(50),
	SubCommodityName VARCHAR(50),
	SubCommodityTypeCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommodityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SubCommodityID'
EXEC utility.SetIndexClustered @TableName, 'IX_SubCommodity', 'DisplayOrder,SubCommodityName'
GO
--End table dropdown.SubCommodity

--Begin table procurement.EquipmentConsignmentDisposition
DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'ALTER TABLE procurement.EquipmentConsignmentDisposition DROP CONSTRAINT ' + DC.Name
FROM sys.default_constraints DC
WHERE DC.Parent_Object_ID = OBJECT_ID('procurement.EquipmentConsignmentDisposition')
	AND DC.Name LIKE '%DateTime%'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'EXEC sp_RENAME ''' + S.Name + '.' + T.Name + '.' + C.Name + ''', ''EquipmentConsignmentDispositionDate'', ''COLUMN'''
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND S.Name = 'procurement'
		AND T.Name = 'EquipmentConsignmentDisposition'
		AND C.Name = 'EquipmentConsignmentDispositionDateTime'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'EXEC sp_RENAME ''' + S.Name + '.' + T.Name + '.' + C.Name + ''', ''QuantityReceived'', ''COLUMN'''
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND S.Name = 'procurement'
		AND T.Name = 'EquipmentConsignmentDisposition'
		AND C.Name = 'QuantityRecieved'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

ALTER TABLE procurement.EquipmentConsignmentDisposition ALTER COLUMN EquipmentConsignmentDispositionDate DATE
GO

EXEC utility.SetDefaultConstraint 'procurement.EquipmentConsignmentDisposition', 'EquipmentConsignmentDispositionDate', 'DATE', 'getDate()'
GO

EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'CreatePersonID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'ParentEquipmentConsignmentDispositionID', 'INT', '0'
GO

EXEC utility.SetDefaultConstraint 'procurement.EquipmentConsignmentDisposition', 'CreateDateTime', 'DATETIME', 'getDate()', 1
GO

DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignmentDisposition'

EXEC utility.DropColumn @TableName, 'StatusID'
EXEC utility.AddColumn @TableName, 'EquipmentConsignmentDispositionStatusID', 'INT', '0'
GO
--End table procurement.EquipmentConsignmentDisposition

--Begin table procurement.EquipmentItem
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentItem'

EXEC utility.AddColumn @TableName, 'SubCommodityID', 'INT', '0'
GO
--End table procurement.EquipmentItem