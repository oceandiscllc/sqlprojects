USE HEROS
GO

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID VARCHAR(MAX) = NULL,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nDocumentID INT
	DECLARE @tEntity TABLE (EntityID INT NOT NULL PRIMARY KEY)
	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	IF @EntityID IS NOT NULL
		BEGIN

		INSERT INTO @tEntity (EntityID) SELECT CAST(LTT.ListItem AS INT) FROM core.ListToTable(@EntityID, ',') LTT

		INSERT INTO @tOutput
			(DocumentID)
		SELECT 
			D.DocumentID
		FROM document.Document D
		WHERE D.DocumentData = @DocumentData

		IF NOT EXISTS (SELECT 1 FROM @tOutput O)
			BEGIN

			INSERT INTO document.Document
				(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
			OUTPUT INSERTED.DocumentID INTO @tOutput
			VALUES
				(
				@ContentType,
				@ContentSubtype,
				@CreatePersonID,
				@DocumentData,
				@DocumentDescription,
				newID(),
				@DocumentTitle,
				@Extension
				)

			END
		--ENDIF

		SELECT @nDocumentID = O.DocumentID FROM @tOutput O

		INSERT INTO document.DocumentEntity
			(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			@nDocumentID,
			@DocumentEntityCode,
			@EntityTypeCode,
			@EntityTypeSubCode,
			E.EntityID
		FROM @tEntity E
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM document.DocumentEntity DE
			WHERE DE.DocumentID = @nDocumentID
				AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND DE.EntityID = E.EntityID
			)

		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = 0
			AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

		IF @AllowMultipleDocuments = 0
			BEGIN
					
			DELETE DE
			FROM document.DocumentEntity DE
			WHERE DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.DocumentID = @nDocumentID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tEntity E
					WHERE E.EntityID = DE.EntityID
					)

			DELETE D
			FROM document.Document D
				JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
					AND DE.EntityTypeCode = @EntityTypeCode
					AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
					AND @EntityID > 0
					AND DE.DocumentID = @nDocumentID
					AND NOT EXISTS
						(
						SELECT 1
						FROM @tEntity E
						WHERE E.EntityID = DE.EntityID
						)

			END
		--ENDIF

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure dropdown.GetCommodityData
EXEC Utility.DropObject 'dropdown.GetCommodityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.Commodity table
-- ================================================================================
CREATE PROCEDURE dropdown.GetCommodityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommodityID,
		T.CommodityCode,
		T.CommodityName
	FROM dropdown.Commodity T
	WHERE (T.CommodityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommodityName, T.CommodityID

END
GO
--End procedure dropdown.GetCommodityData

--Begin procedure dropdown.GetSubCommodityData
EXEC Utility.DropObject 'dropdown.GetSubCommodityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.SubCommodity table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSubCommodityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommodityCode,
		C.CommodityName,
		T.SubCommodityID,
		T.SubCommodityCode,
		T.SubCommodityName
	FROM dropdown.SubCommodity T
		JOIN dropdown.Commodity C ON C.CommodityID = T.CommodityID
			AND ((T.CommodityID > 0 AND T.SubCommodityID > 0) OR @IncludeZero = 1)
			AND C.IsActive = 1 
			AND T.IsActive = 1
	ORDER BY C.DisplayOrder, C.CommodityName, C.CommodityID, T.DisplayOrder, T.SubCommodityName, T.SubCommodityID

END
GO
--End procedure dropdown.GetSubCommodityData

--Begin procedure dropdown.GetEquipmentConsignmentDispositionStatusData
EXEC Utility.DropObject 'dropdown.GetEquipmentConsignmentDispositionStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.07
-- Description:	A stored procedure to return data from the dropdown.EquipmentConsignmentDispositionStatus table
-- ============================================================================================================
CREATE PROCEDURE dropdown.GetEquipmentConsignmentDispositionStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentConsignmentDispositionStatusID,
		T.EquipmentConsignmentDispositionStatusCode,
		T.EquipmentConsignmentDispositionStatusName
	FROM dropdown.EquipmentConsignmentDispositionStatus T
	WHERE (T.EquipmentConsignmentDispositionStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EquipmentConsignmentDispositionStatusName, T.EquipmentConsignmentDispositionStatusID

END
GO
--End procedure dropdown.GetEquipmentConsignmentDispositionStatusData

--Begin procedure dropdown.GetEquipmentOrderData
EXEC Utility.DropObject 'dropdown.GetEquipmentOrderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.31
-- Description:	A stored procedure to return data from the procurement.EquipmentOrder table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetEquipmentOrderData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentOrderID,
		T.EquipmentOrderName
	FROM procurement.EquipmentOrder T
	ORDER BY T.EquipmentOrderName, T.EquipmentOrderID

END
GO
--End procedure dropdown.GetEquipmentOrderData

--Begin procedure eventlog.LogEquipmentConsignmentAction
EXEC utility.DropObject 'eventlog.LogEquipmentConsignmentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2018.08.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentConsignmentAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentConsignment'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, T.EquipmentConsignmentID) AS XML)
			FOR XML RAW('EquipmentConsignment'), ELEMENTS
			)
		FROM procurement.EquipmentConsignment T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments
		FROM procurement.EquipmentConsignment T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentConsignmentAction

--Begin procedure eventlog.LogEquipmentConsignmentDispositionAction
EXEC utility.DropObject 'eventlog.LogEquipmentConsignmentDispositionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2018.08.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentConsignmentDispositionAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentConsignmentDisposition'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, T.EquipmentConsignmentDispositionID) AS XML)
			FOR XML RAW('EquipmentConsignmentDisposition'), ELEMENTS
			)
		FROM procurement.EquipmentConsignmentDisposition T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentDispositionID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments
		FROM procurement.EquipmentConsignmentDisposition T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentDispositionID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentConsignmentDispositionAction

--Begin procedure procurement.GetEquipmentConsignmentByEquipmentConsignmentID
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignment table
-- ==============================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentByEquipmentConsignmentID

@EquipmentConsignmentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentConsignmentID,
		EC.EquipmentItemID,
		EC.Quantity,
		EO.EquipmentOrderName,
		EOCI.ItemDescription,
		ISNULL(EC.Quantity, 0) - ISNULL(ECD.QuantityDisposed, 0) AS QuantityOutstanding,
		FORMAT(ROUND(((EOCI.UnitCost * EC.Quantity) * ((100 + EOCI.VAT)/100)), 2), 'C', 'en-GB') AS TotalCostWithVAT
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
			AND EC.EquipmentConsignmentID = @EquipmentConsignmentID
		LEFT JOIN procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID(@EquipmentConsignmentID) ECD ON ECD.EquipmentConsignmentID = EC.EquipmentConsignmentID

END
GO
--End procedure procurement.GetEquipmentConsignmentByEquipmentConsignmentID

--Begin procedure procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.11
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignmentDisposition table
-- =========================================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID

@EquipmentConsignmentDispositionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentConsignmentID,
		EC.EquipmentItemID,
		ECD.EquipmentConsignmentDispositionID,
		ECD.EquipmentConsignmentDispositionStatusID,

		CASE
			WHEN ECD.QuantityReceived > 0
			THEN ECD.QuantityReceived
			WHEN ECD.QuantityShrink > 0
			THEN ECD.QuantityShrink
			WHEN ECD.QuantityWaste > 0
			THEN ECD.QuantityWaste
			ELSE 0
		END AS Quantity,

		ECD.RecipientEntityTypeCode,
		ECD.RecipientEntityID,
		EO.EquipmentOrderName,
		EOCI.ItemDescription
	FROM procurement.EquipmentConsignmentDisposition ECD
		JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
			AND ECD.EquipmentConsignmentDispositionID = @EquipmentConsignmentDispositionID

END
GO
--End procedure procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID

--Begin procedure procurement.GetEquipmentConsignmentFilterData
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignment table
-- ==============================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentFilterData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT EC.ConsignmentCode
	FROM procurement.EquipmentConsignment EC
	ORDER BY 1

	SELECT DISTINCT 
		EC.EquipmentItemID,
		ISNULL(ISNULL(EOCI.ItemDescription, EI.ItemDescription), 'None Listed') AS ItemDescription
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
		JOIN procurement.EquipmentItem EI ON EI.EquipmentItemID = EC.EquipmentItemID
	ORDER BY 2, 1

	SELECT DISTINCT 
		EC.EquipmentOrderID,
		EO.EquipmentOrderName
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
	ORDER BY 2, 1

END
GO
--End procedure procurement.GetEquipmentConsignmentFilterData

--Begin procedure procurement.GetEquipmentItemByEquipmentItemID
EXEC Utility.DropObject 'procurement.GetEquipmentItemByEquipmentItemID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE procurement.GetEquipmentItemByEquipmentItemID

@EquipmentItemID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EI.EquipmentItemID,
		EI.ItemName,
		EI.ItemDescription,
		EI.BudgetValue,
		EI.IsActive,
		IIF(EI.IsActive = 1, 'Yes ', 'No ') AS IsActiveFormatted,
		EI.IsPerishable,
		IIF(EI.IsPerishable = 1, 'Yes ', 'No ') AS IsPerishableFormatted,
		EI.IsColdChainRequired,
		IIF(EI.IsColdChainRequired = 1, 'Yes ', 'No ') AS IsColdChainRequiredFormatted,
		EI.IsReturnable,
		IIF(EI.IsReturnable = 1, 'Yes ', 'No ') AS IsReturnableFormatted,
		EI.IsControlledItem,
		IIF(EI.IsControlledItem = 1, 'Yes ', 'No ') AS IsControlledItemFormatted,
		EI.IsLotControlled,
		IIF(EI.IsLotControlled = 1, 'Yes ', 'No ') AS IsLotControlledFormatted,
		EI.CurrencyID,
		EI.MaximumShelfLife,
		EI.MinimumQuantity,
		EI.Model,
		EI.Size,
		EI.Length,
		EI.Height,
		EI.Width,
		EI.Weight,
		EI.ProjectID,
		dropdown.GetProjectNameByProjectID(EI.ProjectID) AS ProjectName,
		EIT.EquipmentItemTypeID,
		EIT.EquipmentItemTypeName,
		GDU.GovernmentDepartmentUnitID,
		GDU.GovernmentDepartmentUnitName,
		SC.SubCommodityID,
		SC.SubCommodityName
	FROM procurement.EquipmentItem EI
		JOIN dropdown.GovernmentDepartmentUnit GDU ON GDU.GovernmentDepartmentUnitID = EI.GovernmentDepartmentUnitID
		JOIN dropdown.EquipmentItemType EIT ON EIT.EquipmentItemTypeID = EI.EquipmentItemTypeID
		JOIN dropdown.SubCommodity SC ON SC.SubCommodityID = EI.SubCommodityID
			AND EI.EquipmentItemID = @EquipmentItemID
		
END
GO
--End procedure procurement.GetEquipmentItemByEquipmentItemID

--Begin procedure procurement.GetEquipmentOrdersByConsignmentCode
EXEC Utility.DropObject 'procurement.GetEquipmentOrdersByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A stored procedure to return data from the procurement.EquipmentOrder table
-- ========================================================================================
CREATE PROCEDURE procurement.GetEquipmentOrdersByConsignmentCode

@ConsignmentCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EO.EquipmentOrderID,
		EO.EquipmentOrderName,
		EOT.EquipmentOrderTypeName,
		procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID(EO.RecipientEntityTypeCode, EO.RecipientEntityID) AS RecipientName,
		C.CountryName AS RecipientCountryName,
		P.ProjectCode,
		EO.TaskOrderNumber,
		EO.ClientPONumber,
		person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted,
		core.FormatDate(EO.OrderDate) AS OrderDateFormatted,
		core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted
	FROM procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EOT.EquipmentOrderTypeID = EO.EquipmentOrderTypeID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = EO.RecipientISOCountryCode2
		JOIN dropdown.Project P ON P.ProjectID = EO.ProjectID
			AND EXISTS
				(
				SELECT 1
				FROM procurement.EquipmentConsignment EC
				WHERE EC.ConsignmentCode = @ConsignmentCode
					AND EC.EquipmentOrderID = EO.EquipmentOrderID
				)
	ORDER BY EO.EquipmentOrderName, EO.EquipmentOrderID

END
GO
--End procedure procurement.GetEquipmentOrdersByConsignmentCode
